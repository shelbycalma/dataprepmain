@echo off

echo.
echo Updates Data and Developer solutions to use local debug versions of Data repo packages.
echo Local debug version of package is built, packed and placed into Local package source.
echo.

call ph.cmd Update Local -S All -T DD PD
pause