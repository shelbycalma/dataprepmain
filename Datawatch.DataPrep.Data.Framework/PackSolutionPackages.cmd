@echo off

echo.
echo Packs all the packages of this solution.
echo.

..\.ph\PackageHelper.exe Pack -s DDF -b -c ..\ph.config
