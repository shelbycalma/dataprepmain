﻿using System;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework
{
    public class Utils
    {
        public static bool IsEqual<T>(T a, T b)
        {
            if (a == null)
            {
                if (b == null)
                {
                    return true;
                }
                return false;
            }
            if (b == null)
            {
                return false;
            }
            return a.Equals(b);
        }

        public static bool IsEqual<T>(T[] array1, T[] array2)
        {
            if (array1 == null)
            {
                if (array2 == null)
                {
                    return true;
                }
                return false;
            }

            if (array2 == null)
            {
                return false;
            }

            if (array1.Length != array2.Length)
            {
                return false;
            }

            for (int i = 0; i < array1.Length; i++)
            {
                if (!IsEqual(array1[i], array2[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool SetEquals<T>(T[] a, T[] b)
        {
            if (a == null)
            {
                if (b == null)
                {
                    return true;
                }
                return false;
            }
            if (b == null)
            {
                return false;
            }
            if (a.Length != b.Length) return false;

            HashSet<T> aSet = new HashSet<T>(a);
            HashSet<T> bSet = new HashSet<T>(b);
            return aSet.SetEquals(bSet);
        }

        public static string PackGuid(Guid guid)
        {
            // This produces a 22-character string representation of a GUID. It contains
            // only alphanumerics, - and _. These characters aren't affected by URL encoding,
            // which makes them a tad easier to pass around in query strings and suchlike.

            // The Base64 encoded version of the GUID always ends in two equals signs,
            // so removing them loses no uniqueness.
            return Convert.ToBase64String(
                guid.ToByteArray()).Replace('+', '_').
                    Replace('/', '-').Replace("=", "");
        }
    }
}
