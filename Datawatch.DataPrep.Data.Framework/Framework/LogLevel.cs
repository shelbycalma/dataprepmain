namespace Datawatch.DataPrep.Data.Framework
{
    public enum LogLevel
    {
        Exception = 1,
        Info = 2,
        Warning = 3,
        Error = 4
    }
}
