﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public interface IParameterEncoder
    {
        // TODO rename this to FormatString
        string SourceString { get; set; }
        IEnumerable<ParameterValue> Parameters { get; set; }
        string Encoded();
    }
}
