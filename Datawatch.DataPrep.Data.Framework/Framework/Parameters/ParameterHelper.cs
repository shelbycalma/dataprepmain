﻿namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class ParameterHelper
    {
        public static bool IsEqual(TypedParameterValue typedValue, object obj)
        {
            if (typedValue is StringParameterValue)
            {
                return string.Equals(((StringParameterValue)typedValue).Value, obj);
            }
            if (typedValue is NumberParameterValue)
            {
                return ((NumberParameterValue)typedValue).Value.Equals(obj);
            }
            if (typedValue is DateTimeParameterValue)
            {
                return ((DateTimeParameterValue)typedValue).Value.Equals(obj);
            }
            if (typedValue is ArrayParameterValue)
            {
                ArrayParameterValue arrayValue = (ArrayParameterValue) typedValue;
                foreach (TypedParameterValue typedMember in arrayValue.Values)
                {
                    if (IsEqual(typedMember, obj))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static string RemoveSingleQuotes(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }
            int offset = 0;
            int length = value.Length;
            if (value.StartsWith("'"))
            {
                offset++;
                length--;
            }
            if (value.EndsWith("'"))
            {
                length--;
            }
            value = value.Substring(offset, length);
            return value;
        }
    }
}
