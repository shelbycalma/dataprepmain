﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class ParameterValue : INamedObject, INotifyPropertyChanged
    {
        private string name;
        private TypedParameterValue typedValue;

        public ParameterValue()
        {
        }

        [Obsolete("Use TypedValue property.")]
        public ParameterValue(string name, string value)
        {
            this.name = name;
            typedValue = new StringParameterValue {Value = value};
        }

        public ParameterValue(string name, TypedParameterValue typedValue)
        {
            this.name = name;
            this.typedValue = typedValue;
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            ParameterValue parameterValue = obj as ParameterValue;
            if (parameterValue == null) return false;

            //TODO:- Not sure giving a datatable parameter with
            //Snapshot name causing TypedValue to be null, 
            //this is to resolve ddtv-1139
            if (parameterValue.TypedValue == null) return false;

            return Utils.IsEqual(name, parameterValue.name) &&
                parameterValue.TypedValue.Equals(TypedValue);
        }

        public override int GetHashCode()
        {
            int nameHash = name != null ? name.GetHashCode() : 5;
            int valueHash = TypedValue != null ? TypedValue.GetHashCode() : 7;
            return nameHash ^ valueHash;
        }

        public override string ToString()
        {
            return string.Format("[{0}={1}]", Name, TypedValue);
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    string oldName = name;
                    name = value;
                    if (NameChanged != null)
                    {
                        NameChanged(this,
                            new NamedObjectEventArgs(oldName, name));
                    }
                    FirePropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Get always returns null.
        /// Set converts the typed value to a string if its not null and
        /// removes single quotes.
        /// </summary>
        [DataMember(IsRequired = false)]
        [Obsolete("Use TypedValue.")]
        public string Value
        {
            get { return null; }
            set
            {
                if (value == null) return;
                TypedValue = new StringParameterValue
                {
                    Value = ParameterHelper.RemoveSingleQuotes(value)
                };
            }
        }

        [DataMember(IsRequired = false)]
        public TypedParameterValue TypedValue
        {
            get { return typedValue; }
            set
            {
                if (typedValue == value) return;
                typedValue = value;
                FirePropertyChanged("TypedValue");
            }
        }

        protected void FirePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        #region INamedObject Members

        public event NamedObjectEventHandler NameChanged;

        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
