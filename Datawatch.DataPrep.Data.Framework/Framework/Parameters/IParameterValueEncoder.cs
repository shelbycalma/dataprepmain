namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public interface IParameterValueEncoder
    {
        string Encode(string unEncoded);
    }
}