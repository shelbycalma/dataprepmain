﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    [KnownType(typeof(StringParameterValue))]
    [KnownType(typeof(DateTimeParameterValue))]
    [KnownType(typeof(NumberParameterValue))]
    [KnownType(typeof(ArrayParameterValue))]
    [XmlInclude(typeof(StringParameterValue))]
    [XmlInclude(typeof(DateTimeParameterValue))]
    [XmlInclude(typeof(NumberParameterValue))]
    [XmlInclude(typeof(ArrayParameterValue))]
    [XmlInclude(typeof(EncryptedStringParameterValue))]
    public abstract class TypedParameterValue : IFormattable
    {
        public abstract override bool Equals(object obj);

        public abstract override int GetHashCode();

        public abstract override string ToString();

        public abstract string ToString(string format,
            IFormatProvider formatProvider);
    }

    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    [KnownType(typeof(EncryptedStringParameterValue))]
    public class StringParameterValue : TypedParameterValue
    {
        public StringParameterValue()
        {
            
        }

        public StringParameterValue(string value)
        {
            Value = value;
        }

        [DataMember]
        public string Value { get; set; }

        public override string ToString()
        {
            return ToString(null, null);
        }

        public override string ToString(string format,
            IFormatProvider formatProvider)
        {
            return Value ?? "";
        }

        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            if (!(obj is StringParameterValue)) return false;
            return Utils.IsEqual(Value, ((StringParameterValue)obj).Value);
        }

        public override int GetHashCode()
        {
            return (Value != null ? Value.GetHashCode() : 0);
        }
    }

    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class EncryptedStringParameterValue : StringParameterValue
    {
        public EncryptedStringParameterValue()
        {
            
        }

        public EncryptedStringParameterValue(string value)
        {
            Value = value;
        }
    }

    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class ArrayParameterValue : TypedParameterValue
    {
        private const string DefaultSeparator = ",";
        private List<TypedParameterValue> values;

        public ArrayParameterValue()
        {
            InitializeList();
        }

        [OnDeserializing]
        internal void OnDeserializingMethod(StreamingContext context)
        {
            InitializeList();
        }

        private void InitializeList()
        {
            values = new List<TypedParameterValue>();
        }

        [DataMember]
        public List<TypedParameterValue> Values
        {
            get { return values; }
        }

        public override string ToString()
        {
            string s = "ArrayParameterValue[";
            for (int i = 0; i < values.Count; i++)
            {
                if (i > 0)
                {
                    s += DefaultSeparator;
                }
                s += values[i];
            }
            s += "]";
            return s;
        }

        public override string ToString(string format,
            IFormatProvider formatProvider)
        {
            return string.Format(
                formatProvider ?? new ParameterFormatProvider(),
                format ?? "{0:" + DefaultSeparator + "}", this);
        }

        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            if (!(obj is ArrayParameterValue)) return false;
            // TODO: Could this be set equals instead?
            return Utils.IsEqual(values.ToArray(),
                ((ArrayParameterValue)obj).values.ToArray());
        }

        public override int GetHashCode()
        {
            int code = 0;
            foreach (TypedParameterValue value in values)
            {
                // TODO: Is the null check necessary?
                if (value != null)
                {
                    code ^= value.GetHashCode();
                }
            }
            return code;
        }

        public void Add(TypedParameterValue typedParameterValue)
        {
            values.Add(typedParameterValue);
        }
    }

    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class NumberParameterValue : TypedParameterValue
    {
        public NumberParameterValue()
        {
        }

        public NumberParameterValue(double value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return ToString(null, null);
        }

        public override string ToString(string format,
            IFormatProvider formatProvider)
        {
            return Value.ToString(format,
                formatProvider ?? CultureInfo.InvariantCulture);
        }

        [DataMember]
        public double Value { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            if (!(obj is NumberParameterValue)) return false;
            return Value.Equals(((NumberParameterValue)obj).Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }

    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class DateTimeParameterValue : TypedParameterValue
    {
        public DateTimeParameterValue()
        {
        }

        public DateTimeParameterValue(DateTime value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return ToString(null, null);
        }

        public override string ToString(string format,
            IFormatProvider formatProvider)
        {
            return Value.ToString(format,
                formatProvider ?? CultureInfo.InvariantCulture);
        }

        [DataMember]
        public DateTime Value { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == this) return true;
            if (!(obj is DateTimeParameterValue)) return false;
            return Value.Equals(((DateTimeParameterValue)obj).Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
