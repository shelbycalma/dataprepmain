﻿using System;
using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class ParameterFormatProvider : IFormatProvider, ICustomFormatter
    {
        public bool EnforceSingleQuoteEnclosure { get; set; }
        public string NullOrEmptyString { get; set; }
        public IParameterValueEncoder ValueEncoder { get; set; }
        public bool EnforceSingleQuoteHandling { get; set; }

        public ParameterFormatProvider()
        {
            EnforceSingleQuoteHandling = true;
        }

        public object GetFormat(Type formatType)
        {
            if (formatType == typeof(ICustomFormatter))
                return this;
            return null;
        }

        public string Format(string format, object arg,
            IFormatProvider formatProvider)
        {
            if (arg == null) return "";

            string formatted;
            if (arg.GetType() == typeof(ArrayParameterValue))
            {
                TypedParameterValue[] valArr =
                    ((ArrayParameterValue)arg).Values.ToArray();
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < valArr.Length; i++)
                {
                    if (i > 0) stringBuilder.Append(format);
                    stringBuilder.Append("{" + i + "}");
                }
                formatted = string.Format(
                    formatProvider, stringBuilder.ToString(), valArr);
            }
            else
            {
                formatted = HandleDefaultFormats(format, arg);
                if (ValueEncoder != null)
                    formatted = ValueEncoder.Encode(formatted);

                if (EnforceSingleQuoteHandling)
                {
                    // never enclose numbers with single quotes, regardless of what flag says
                    bool enforceSingleQuotes =
                        EnforceSingleQuoteEnclosure
                        && !(arg is NumberParameterValue);
                    formatted = HandleSingleQuotes(formatted, enforceSingleQuotes);
                }
            }
            return string.IsNullOrEmpty(formatted)
                ? NullOrEmptyString : formatted;
        }

        private static string HandleDefaultFormats(string format, object arg)
        {
            if (arg is IFormattable)
                return ((IFormattable)arg).ToString(format, null);
            if (arg != null)
                return arg.ToString();
            return String.Empty;
        }

        private static string HandleSingleQuotes(string formatted, bool enforceSingleQuotes)
        {
            if (string.IsNullOrEmpty(formatted)) return null;

            int offset = 0;
            int length = formatted.Length;
            if (enforceSingleQuotes)
            {
                if (!formatted.StartsWith("'"))
                {
                    formatted = formatted.Insert(0, "'");
                    length++;
                }
                if (!formatted.EndsWith("'"))
                {
                    formatted = formatted.Insert(length, "'");
                }
            }
            else
            {
                if (formatted.StartsWith("'"))
                {
                    offset++;
                    length--;
                }
                if (formatted.EndsWith("'"))
                {
                    length--;
                }
                formatted = formatted.Substring(offset, length);
            }
            return formatted;
        }
    }
}
