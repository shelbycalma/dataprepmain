﻿using System;
using System.Runtime.Serialization;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class OnDemandParameters
    {
        private string[] breakdownColumns;
        private OnDemandAggregate[] aggregates;
        private string[] auxiliaryColumns;
        private string[] drillPath;
        private int visibleDepth;
        private string[] domains;
        private Predicate[] predicates;
        private Bucketing[] bucketings;
        private string consumerId;

        public OnDemandParameters()
        {
            breakdownColumns = null;
            aggregates = null;
            drillPath = null;
            visibleDepth = -1;
            InitializeLists();
        }

        [OnDeserializing]
        internal void OnDeserializingMethod(StreamingContext context)
        {
            InitializeLists();
        }

        private void InitializeLists()
        {
        }

        [DataMember]
        public string ConsumerId
        {
            get { return consumerId; }
            set { consumerId = value; }
        }

        [DataMember]
        public string[] AuxiliaryColumns
        {
            get { return auxiliaryColumns; }
            set { auxiliaryColumns = value; }
        }

        [DataMember]
        public string[] BreakdownColumns
        {
            get { return breakdownColumns; }
            set { breakdownColumns = value; }
        }

        [DataMember]
        public Bucketing[] Bucketings
        {
            get { return bucketings; }
            set { bucketings = value; }
        }

        [DataMember]
        public OnDemandAggregate[] Aggregates
        {
            get { return aggregates; }
            set { aggregates = value; }
        }

        [DataMember]
        public string[] Domains
        {
            get { return domains; }
            set { domains = value; }
        }

        [DataMember]
        public string[] DrillPath
        {
            get { return drillPath; }
            set { drillPath = value; }
        }

        [DataMember]
        public Predicate[] Predicates
        {
            get { return predicates; }
            set { predicates = value; }
        }

        [DataMember]
        public int VisibleDepth
        {
            get { return visibleDepth; }
            set { visibleDepth = value; }
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is OnDemandParameters)) return false;

            OnDemandParameters odp = (OnDemandParameters)obj;

            if (Utils.IsEqual(odp.BreakdownColumns, BreakdownColumns) &&
                Utils.SetEquals(odp.Aggregates, Aggregates) &&
                Utils.IsEqual(odp.DrillPath, DrillPath) &&
                Utils.SetEquals(odp.AuxiliaryColumns, AuxiliaryColumns) &&
                Utils.SetEquals(odp.Predicates, Predicates) &&
                Utils.SetEquals(odp.Domains, Domains) &&
                Utils.SetEquals(odp.Bucketings, Bucketings) &&
                Utils.IsEqual(odp.ConsumerId, consumerId) &&
                odp.VisibleDepth == VisibleDepth)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            int code = 0;
            if (breakdownColumns != null) 
            {
                foreach (string s in breakdownColumns) 
                {
                    code ^= s.GetHashCode();
                }
            }
            if (aggregates != null) 
            {
                foreach (OnDemandAggregate odci in aggregates) 
                {
                    code ^= odci.GetHashCode();
                }
            }
            if (drillPath != null) 
            {
                foreach (string s in drillPath) 
                {
                    code ^= s != null ? s.GetHashCode() : 4211;
                }
            }
            if (auxiliaryColumns != null)
            {
                foreach (string column in auxiliaryColumns)
                {
                    code ^= column.GetHashCode();
                }
            }
            if (predicates != null)
            {
                foreach (Predicate predicate in predicates)
                {
                    code ^= predicate.GetHashCode();
                }
            }
            if (domains != null)
            {
                foreach (string domain in domains)
                {
                    code ^= domain.GetHashCode();
                }
            }

            if (bucketings != null)
            {
                foreach (Bucketing bucketing in bucketings)
                {
                    code ^= bucketing.GetHashCode();
                }
            }

            code ^= visibleDepth.GetHashCode();

            if (consumerId != null)
            {
                code ^= consumerId.GetHashCode();
            }
            
            return code;
        }

        public bool IsEmpty
        {
            get
            {
                return 
                    Depth <= 0 &&
                    NullOrEmpty(aggregates) &&
                    NullOrEmpty(auxiliaryColumns) && 
                    NullOrEmpty(domains) &&
                    NullOrEmpty(bucketings);
            }
        }

        public int Depth
        {
            get
            {
                int depth = breakdownColumns != null ?
                    breakdownColumns.Length : 0;

                if (visibleDepth != -1)
                {
                    depth = VisibleDepth;
                    if (BreakdownColumns != null)
                    {
                        depth = Math.Min(depth, breakdownColumns.Length);
                    }
                    else
                    {
                        depth = -1;
                    }
                }

                return depth;
            }
        }

        private static bool NullOrEmpty(Array a)
        {
            return a == null || a.Length == 0;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (consumerId != null)
            {
                sb.Append(consumerId);
                sb.Append(":");
            }

            if (breakdownColumns != null)
            {
                foreach (string s in breakdownColumns)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append("/");
                    }
                    sb.Append(s);
                }
            }

            if (drillPath != null)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                foreach (string s in drillPath)
                {
                    if (sb.Length > 0)
                    {
                        sb.AppendFormat("'{0}'", s);
                    }
                }
            }

            if (visibleDepth != -1)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.AppendFormat("Visible depth: {0}", visibleDepth);
            }

            if (aggregates != null)
            {
                foreach (OnDemandAggregate ci in aggregates)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(ci.ToString());
                }
            }

            if (auxiliaryColumns != null)
            {
                foreach (string column in auxiliaryColumns)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(column.ToString());
                }
            }
            if (predicates != null)
            {
                foreach (Predicate p in predicates)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(p.ToString());
                }
            }

            if (domains != null)
            {
                foreach (string domain in domains)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(domain);
                }
            }
            if (bucketings != null)
            {
                foreach (Bucketing bucketing in bucketings)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(", ");
                    }
                    sb.Append(bucketing);
                }
            }

            return sb.ToString();
        }
    }
}
