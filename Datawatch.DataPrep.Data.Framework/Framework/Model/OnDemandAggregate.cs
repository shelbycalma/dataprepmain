﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class OnDemandAggregate
    {
        private string alias;
        private string key;
        private string[] columns;
        private FunctionType functionType = FunctionType.Sum;

        public OnDemandAggregate()
        {
        }

        public OnDemandAggregate(INumericColumn column) : 
            this(FunctionType.Sum, new[] {column}, "Sum(" + column.Name+ ")")
        {
        }

        public OnDemandAggregate(FunctionType functionType, 
            IEnumerable<INumericColumn> columns, string key)
        {
            List<string> columnNames = new List<string>();
            foreach (INumericColumn col in columns)
            {
                columnNames.Add(col.Name);
            }
            this.columns = columnNames.ToArray();
            this.functionType = functionType;
            this.key = key;
        }

        [DataMember]
        public string Alias
        {
            get { return alias; }
            set { alias = value; }
        }

        [DataMember]
        public string[] Columns
        {
            get { return columns; }
            set { columns = value; }
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is OnDemandAggregate)) return false;

            OnDemandAggregate odci = (OnDemandAggregate)obj;

            return Utils.IsEqual(columns, odci.columns) &&                
                Utils.IsEqual(functionType, odci.functionType) &&
                Utils.IsEqual(key, odci.key) &&
                Utils.IsEqual(alias, odci.alias);
        }

        [DataMember]
        public string Key
        {
            get { return key; }
            set { key = value; }
        }

        [DataMember]
        public FunctionType FunctionType
        {
            get { return functionType; }
            set { functionType = value; }
        }

        public override int GetHashCode()
        {
            int code = 17;

            code = code * 23 + functionType.GetHashCode();

            if (columns != null) 
            {
                foreach (string column in columns)
                {
                    code = code * 23 + column.GetHashCode();
                }
            }
            if (key != null) 
            {
                code = code * 23 + key.GetHashCode();
            }
            if (alias != null)
            {
                code = code * 23 + alias.GetHashCode();
            }
            
            return code;
        }
 
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(FunctionType);
            sb.Append("(");
            for (int i=0; i<columns.Length; i++)
            {
                if (i > 0) 
                {
                    sb.Append(",");
                }
                sb.Append(columns[i]);
            }
            sb.Append(")");
            return sb.ToString();
        }
    }
}
