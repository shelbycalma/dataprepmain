﻿using System.ComponentModel;
using System.Runtime.Serialization;
using DescriptionAttribute = System.ComponentModel.DescriptionAttribute;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum FilterOperator
    {
        [Description("UiFilterOperatorNone")]
        [EnumMember]
        None,
        [Description("UiFilterOperatorGreaterThan")]
        [EnumMember(Value =">")]
        GreaterThan,
        [Description("UiFilterOperatorGreaterThanEqual")]
        [EnumMember(Value = ">=")]
        GreaterThanEqual,
        [Description("UiFilterOperatorLessThan")]
        [EnumMember(Value = "<")]
        LessThan,
        [Description("UiFilterOperatorLessThanEqual")]
        [EnumMember(Value = "<")]
        LessThanEqual,
        [Description("UiFilterOperatorEqualTo")]
        [EnumMember(Value = "=")]
        EqualTo,
        [Description("UiFilterOperatorNotEqualTo")]
        [EnumMember(Value = "<>")]
        NotEqualTo,
        [Description("UiFilterOperatorStartsWith")]
        [EnumMember(Value = "StartsWith")]
        StartsWith,
        [Description("UiFilterOperatorContains")]
        [EnumMember(Value = "Contains")]
        Contains,
        [Description("UiFilterOperatorEndsWith")]
        [EnumMember(Value = "EndsWith")]
        EndsWith
    }
}
