using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    public class ObservableLookupCollection<T> : ObservableCollection<T> where T : INamedObject
    {
        private readonly Dictionary<string, T> lookup = 
            new Dictionary<string, T>();

        public bool ContainsKey(string key)
        {
            return lookup.ContainsKey(key);
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    OnAdd(e);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    OnRemove(e);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    OnReset(e);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    OnReplace(e);
                    break;
            }
            base.OnCollectionChanged(e);
        }

        public T this[string name]
        {
            get
            {
                T item;
                lookup.TryGetValue(name, out item);
                return item;
            }
        }

        void item_NameChanged(object source, NamedObjectEventArgs args)
        {
            T item = (T)source;
            //if (lookup.ContainsKey(args.NewName))
            //{
            //    throw new Exception("If we continue with the name change then the lookup will be out of sync with the collection");
            //}
            lookup.Remove(args.OldName);
            lookup[args.NewName] = item;
        }

        public void Remove(string name)
        {
            Remove(this[name]);
        }

        protected void OnAdd(NotifyCollectionChangedEventArgs e)
        {
            foreach (T item in e.NewItems)
            {
                if (item == null) continue;                
                lookup[item.Name] = item;
                item.NameChanged += item_NameChanged;
            }
        }

        protected void OnRemove(NotifyCollectionChangedEventArgs e)
        {
            foreach (T item in e.OldItems)
            {
                lookup.Remove(item.Name);
                item.NameChanged -= item_NameChanged;
            }
        }

        protected void OnReset(NotifyCollectionChangedEventArgs e)
        {
            foreach (T item in lookup.Values)
            {
                item.NameChanged -= item_NameChanged;
            }

            lookup.Clear();
        }

        protected void OnReplace(NotifyCollectionChangedEventArgs e)
        {
            foreach (T item in e.OldItems)
            {
                lookup.Remove(item.Name);
                item.NameChanged -= item_NameChanged;
            }
            foreach (T item in e.NewItems)
            {
                lookup[item.Name] = item;
                item.NameChanged += item_NameChanged;
            }
        }
    }
}
