﻿using System.ComponentModel;
using System.Runtime.Serialization;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class NumericBucketing : Bucketing
    {
        private NumericBucketingType numericBucketingType;
        private PropertyBag settings;
        private string[] labels;
        private NumericInterval[] intervals;

        [DataMember]
        public NumericBucketingType NumericBucketingType
        {
            get { return numericBucketingType; }
            set
            {
                if (value != numericBucketingType)
                {
                    numericBucketingType = value;
                    OnPropertyChanged("NumericBucketingType");
                }
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is NumericBucketing)) return false;
            NumericBucketing other = (NumericBucketing)obj;
            return Equals(numericBucketingType, other.numericBucketingType) &&
                   Utils.IsEqual(labels, other.labels) &&
                   Utils.IsEqual(intervals, other.Intervals) &&
                   Equals(settings, other.settings);

        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            code += code * 23 + numericBucketingType.GetHashCode();
            if (settings != null)
            {
                code += code * 23 + settings.GetHashCode();
            }
            if (labels != null)
            {
                foreach (string label in labels)
                {
                    code += code * 23 + label.GetHashCode();
                }
            }
            if (intervals != null)
            {
                foreach (NumericInterval interval in intervals)
                {
                    code += code * 23 + interval.GetHashCode();
                }
            }
            return code;
        }

        [DataMember]
        public string[] Labels
        {
            get { return labels; }
            set
            {
                if (value != labels)
                {
                    labels = value;
                    OnPropertyChanged("Labels");
                }
            }
        }

        [DataMember]
        public NumericInterval[] Intervals
        {
            get { return intervals; }
            set
            {
                if (value != intervals)
                {
                    intervals = value;
                    OnPropertyChanged("Intervals");
                }
            }
        }

        [DataMember]
        public PropertyBag Settings
        {
            get { return settings; }
            set
            {
                if (settings != null)
                {
                    settings.PropertyChanged -= Settings_PropertyChanged;
                }

                settings = value;

                if (settings != null)
                {
                    settings.PropertyChanged += Settings_PropertyChanged;
                }
                OnPropertyChanged("Settings");
            }
        }

        private void Settings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            OnPropertyChanged("Settings." + e.PropertyName);
        }
    }
}
