﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class PropertyBag : INamedObject, INotifyPropertyChanged
    {
        public event NamedObjectEventHandler NameChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        private PropertyValueCollection values = new PropertyValueCollection();
        private PropertyBagCollection subGroups = new PropertyBagCollection();

        public PropertyBag()
        {
            InitCollections();
        }

        public PropertyBag(PropertyBag bag)
        {
            name = bag.name;
            this.Merge(bag);
        }

        [OnDeserializing]
        internal void OnDeserializingMethod(StreamingContext context)
        {
            InitCollections();
        }

        private void InitCollections()
        {
            values = new PropertyValueCollection();
            values.CollectionChanged += values_CollectionChanged;
            subGroups = new PropertyBagCollection();
            subGroups.CollectionChanged += subGroups_CollectionChanged;
        }

        public void Clear()
        {
            values.Clear();
            subGroups.Clear();
        }

        public override int GetHashCode()
        {
            int hashCode = 17;

            if (!string.IsNullOrEmpty(name))
            {
                hashCode = hashCode*23+name.GetHashCode();
            }

            hashCode *= 23;
            foreach (PropertyValue propertyValue in values)
            {
                hashCode += propertyValue.GetHashCode();
            }
            hashCode *= 23;
            foreach (PropertyBag subGroup in subGroups)
            {
                hashCode += subGroup.GetHashCode();
            }
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            PropertyBag bag = obj as PropertyBag;

            if (bag == null)
            {
                return false;
            }

            if (name != null)
            {
                if (bag.Name == null)
                {
                    return false;
                }
                if (!name.Equals(bag.Name))
                {
                    return false;
                }
            }
            else
            {
                if (bag.Name != null)
                {
                    return false;
                }
            }

            if (values.Count != bag.Values.Count)
            {
                return false;
            }

            for (int i = 0; i < values.Count; i++)
            {
                PropertyValue propertyValue = values[i];
                string  value = values[i].Value;
                string value2 = bag.Values[propertyValue.Name];

                if (value != null)
                {
                    if (value2 == null)
                    {
                        return false;
                    }
                    if (!value.Equals(value2))
                    {
                        return false;
                    }
                }
                else
                {
                    if (value2 != null)
                    {
                        return false;
                    }
                }
            }

            if (subGroups.Count != bag.SubGroups.Count)
            {
                return false;
            }

            for (int i = 0; i < subGroups.Count; i++)
            {
                PropertyBag subGroup = subGroups[i];
                PropertyBag subGroup2 = bag.SubGroups[subGroup.Name];

                if (subGroup2 == null)
                {
                    return false;
                }

                if (!subGroup.Equals(subGroup2))
                {
                    return false;
                }
            }

            return true;
        }

        [DataMember]
        public PropertyValueCollection Values
        {
            get
            {
                return values;
            }
        }

        [DataMember(IsRequired=true)]
        public PropertyBagCollection SubGroups
        {
            get
            {
                return subGroups;
            }
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    string oldName = name;
                    name = value;
                    if (NameChanged != null)
                    {
                        NameChanged(this,
                            new NamedObjectEventArgs(oldName, name));
                    }
                    FirePropertyChanged("Name");
                }
            }
        }

        public bool IsEmpty()
        {
            return Values.Count == 0 && SubGroups.Count == 0;
        }

        public void Trim()
        {
            List<string> valuesToRemove = new List<string>();
            List<string> groupsToRemove = new List<string>();
            foreach (PropertyValue value in Values)
            {
                if (value.Value == null)
                {
                    valuesToRemove.Add(value.Name);
                }
            }
            foreach (PropertyBag group in SubGroups)
            {
                group.Trim();
                if (group.IsEmpty())
                {
                    groupsToRemove.Add(group.Name);
                }
            }
            foreach (string valueName in valuesToRemove)
            {
                Values.Remove(valueName);
            }
            foreach (string groupName in groupsToRemove)
            {
                SubGroups.Remove(groupName);
            }
        }

        public void Exclude(PropertyBag bag)
        {
            List<string> valuesToRemove = new List<string>();
            foreach (PropertyValue value in bag.Values)
            {
                if (Values[value.Name] == value.Value)
                {
                    valuesToRemove.Add(value.Name);
                }
            }
            List<string> groupsToRemove = new List<string>();
            foreach (PropertyBag subGroup in bag.SubGroups)
            {
                if (SubGroups.ContainsKey(subGroup.Name))
                {
                    SubGroups[subGroup.Name].Exclude(subGroup);
                    if (SubGroups[subGroup.Name].IsEmpty())
                    {
                        groupsToRemove.Add(subGroup.Name);
                    }
                }
            }
            foreach (string groupToRemove in groupsToRemove)
            {
                SubGroups.Remove(groupToRemove);
            }
            foreach (string valueToRemove in valuesToRemove)
            {
                Values.Remove(valueToRemove);
            }
        }

        public void Merge(PropertyBag bag)
        {
            foreach (PropertyValue value in bag.Values)
            {
                if (Values[value.Name] != value.Value)
                {
                    Values[value.Name] = value.Value;
                }
            }
            foreach (PropertyBag subGroup in bag.SubGroups)
            {
                if (!SubGroups.ContainsKey(subGroup.Name))
                {
                    SubGroups[subGroup.name] = new PropertyBag();
                }
                SubGroups[subGroup.Name].Merge(subGroup);
            }
        }

        public static PropertyBag Merge(PropertyBag a, PropertyBag b)
        {
            if (a == null && b == null)
            {
                return null;
            }

            PropertyBag merged = new PropertyBag();
            if (a != null)
            {
                merged.Merge(a);
            }
            if (b != null)
            {
                merged.Merge(b);
            }
            return merged;
        }

        private void subGroups_CollectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (PropertyBag subGroup in e.OldItems)
                {
                    if (subGroup == null) continue;
                    subGroup.PropertyChanged -=
                        new PropertyChangedEventHandler(
                            SubGroup_PropertyChanged);
                }
            }
            if (e.NewItems != null)
            {
                foreach (PropertyBag subGroup in e.NewItems)
                {
                    if (subGroup == null) continue;                    
                    subGroup.PropertyChanged += 
                        new PropertyChangedEventHandler(
                            SubGroup_PropertyChanged);
                }
            }
            FirePropertyChanged("SubGroups");
        }

        void SubGroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            FirePropertyChanged("SubGroup[" +  ((PropertyBag)sender).Name + 
                "]." + e.PropertyName);
        }

        private void values_CollectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            if (e.OldItems != null)
            {
                foreach (PropertyValue value in e.OldItems)
                {
                    value.PropertyChanged -=
                        new PropertyChangedEventHandler(
                            Value_PropertyChanged);
                }
            }
            if (e.NewItems != null)
            {
                foreach (PropertyValue value in e.NewItems)
                {
                    value.PropertyChanged += 
                        new PropertyChangedEventHandler(
                            Value_PropertyChanged);
                }
            } 
            FirePropertyChanged("Values");
        }

        void Value_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            FirePropertyChanged("Value[" + ((PropertyValue)sender).Name +
                "]." + e.PropertyName);            
        }


        protected void FirePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

    }
}
