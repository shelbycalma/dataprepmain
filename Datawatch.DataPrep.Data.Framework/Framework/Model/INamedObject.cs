using System;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    public interface INamedObject
    {
        string Name { get; }
        event NamedObjectEventHandler NameChanged;
    }

    public delegate void NamedObjectEventHandler(object source, NamedObjectEventArgs args);

    public class NamedObjectEventArgs : EventArgs 
    {
        public readonly string OldName;
        public readonly string NewName;

        private NamedObjectEventArgs()
        {
        }

        public NamedObjectEventArgs(string oldName, string newName) 
        {
            OldName = oldName;
            NewName = newName;
        }
    }
}
