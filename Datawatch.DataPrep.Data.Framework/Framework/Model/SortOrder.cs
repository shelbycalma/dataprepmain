﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum SortOrder
    {
        [EnumMember]
        [Description("UiSortOrderAscending")]
        Ascending = 0,

        [EnumMember]
        [Description("UiSortOrderDescending")]
        Descending = 1,

        /// <summary>
        /// No sorting is done and the order in which elements were read from
        /// the source is preserved.
        /// </summary>
        [EnumMember]
        [Description("UiSortOrderNone")]
        None = 2
    }
}
