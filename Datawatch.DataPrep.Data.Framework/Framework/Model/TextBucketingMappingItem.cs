﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class TextBucketingMappingItem : INotifyPropertyChanged
    {
        private string sourceColumnValue;
        private string groupName;

        public event PropertyChangedEventHandler PropertyChanged;

        [DataMember]
        public string SourceColumnValue
        {
            get { return sourceColumnValue; }
            set
            {
                if (sourceColumnValue == value) return;
                sourceColumnValue = value;
                FirePropertyChanged("SourceColumnValue");
            }
        }

        [DataMember]
        public string GroupName
        {
            get { return groupName; }
            set
            {
                if (groupName == value) return;
                groupName = value;
                FirePropertyChanged("GroupName");
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TextBucketingMappingItem))
            {
                return false;
            }

            TextBucketingMappingItem other = (TextBucketingMappingItem)obj;

            if (sourceColumnValue != null)
            {
                if (!sourceColumnValue.Equals(other.sourceColumnValue))
                {
                    return false;
                }
            }
            else
            {
                if (other.sourceColumnValue != null)
                {
                    return false;
                }
            }

            if (groupName != null)
            {
                if (!groupName.Equals(other.groupName))
                {
                    return false;
                }
            }
            else
            {
                if (other.groupName != null)
                {
                    return false;
                }
            }

            return true;
        }

        public override int GetHashCode()
        {
            return (sourceColumnValue != null ? sourceColumnValue.GetHashCode() : 5)
                ^ (groupName != null ? groupName.GetHashCode() : 7);
        }
        
        private void FirePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
