﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum FunctionType
    {
        [EnumMember]
        AbsSum,
        [EnumMember]
        ConditionalSum,
        [EnumMember]
        Max,
        [EnumMember]
        Min,
        [EnumMember]
        Samples,
        [EnumMember]
        Sum,
        [EnumMember]
        SumOfProducts,
        [EnumMember]
        SumOfSquares,
        [EnumMember]
        First,
        [EnumMember]
        Count,
        [EnumMember]
        NegativeSum,
        [EnumMember]
        PositiveSum,
        [EnumMember]
        SumOfQuotients,
        [EnumMember]
        SumOfWeightedQuotients,
        [EnumMember]
        NonZeroSamples,
        [EnumMember]
        Last,
        [EnumMember]
        External,
        [EnumMember]
        Mean
    }
}
