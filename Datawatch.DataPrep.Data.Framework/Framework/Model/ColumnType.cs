﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum ColumnType
    {
        [EnumMember]
        Numeric,
        [EnumMember]
        Text,
        [EnumMember]
        Time,
		[EnumMember]
		Other
	}
}
