﻿namespace Datawatch.DataPrep.Data.Framework.Model
{
    public class PropertyValueCollection : ObservableLookupCollection<PropertyValue>
    {
        public new string this[string name] 
        {
            get
            {
                PropertyValue value = base[name];
                if (value != null)
                {
                    return value.Value;
                }
                return null;
            }
            set
            {
                if (name != null)
                {
                    PropertyValue propertyValue = base[name];
                    
                    if (propertyValue != null)
                    {
                        if (string.Equals(propertyValue.Value, value))
                        {
                            return;
                        }
                        Remove(propertyValue);
                    }
                    if (value != null)
                    {
                        Add(new PropertyValue(name, value));
                    }
                }
            }
        }
    }
}
