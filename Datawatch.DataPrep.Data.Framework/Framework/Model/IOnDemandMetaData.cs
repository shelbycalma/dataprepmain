﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    /// <summary>
    /// Marker interface signaling that the column derives from query-on-demand.
    /// </summary>
    public interface IOnDemandMetaData : IColumnMetaData
    {
    }
}
