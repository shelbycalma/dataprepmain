﻿using System.Runtime.Serialization;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class TimeBucketing : Bucketing
    {
        private TimePart timePart;
        private string format;

        public TimeBucketing()
        {
        }

        public TimeBucketing(string name, string title, string sourceColumnName, TimePart timePart)
            : base(name, sourceColumnName)
        {
            Title = title;
            this.timePart = timePart;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is TimeBucketing)) return false;
            TimeBucketing other = (TimeBucketing)obj;
            return Equals(timePart, other.timePart) &&
                Equals(format, other.format);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            code += code * 23 + timePart.GetHashCode();
            if (!string.IsNullOrEmpty(format))
            {
                code += code * 23 + format.GetHashCode();
            }
            return code;
        }

        [DataMember]
        public TimePart TimePart
        {
            get { return timePart; }
            set
            {
                if (timePart != value)
                {
                    timePart = value;
                    OnPropertyChanged("TimePart");
                }
            }
        }

        [DataMember]
        public string Format
        {
            get { return format; }
            set
            {
                if (format != value)
                {
                    format = value;
                    OnPropertyChanged("Format");
                }
            }
        }
    }
}
