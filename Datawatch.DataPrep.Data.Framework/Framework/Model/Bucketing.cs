﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    [KnownType(typeof(TimeBucketing))]
    [KnownType(typeof(NumericBucketing))]
    [KnownType(typeof(TextBucketing))]
    public class Bucketing : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        private string title;
        private string sourceColumnName;

        public Bucketing()
        {
            name = Guid.NewGuid().ToString("D");
        }

        public Bucketing(string name, string sourceColumnName)
        {
            this.name = name;
            this.sourceColumnName = sourceColumnName;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!(obj is Bucketing)) return false;
            Bucketing other = (Bucketing) obj;
            return Equals(other.name, name) && 
                Equals(other.sourceColumnName, sourceColumnName) &&
                Equals(other.title, title);
        }

        public override int GetHashCode()
        {
            int code = 17;

            if (!string.IsNullOrEmpty(name))
            {
                code += code * 23 + name.GetHashCode();
            }
            if (!string.IsNullOrEmpty(sourceColumnName))
            {
                code += code * 23 + sourceColumnName.GetHashCode();
            }
            if (!string.IsNullOrEmpty(title))
            {
                code += code * 23 + title.GetHashCode();
            }
            return code;
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set
            {
                if (name != value)
                {
                    name = value;
                    OnPropertyChanged("Name");
                }
            }
        }

        [DataMember]
        public string Title
        {
            get { return title; }
            set
            {
                if (title != value)
                {
                    title = value;
                    OnPropertyChanged("Title");
                }
            }
        }

        [DataMember]
        public string SourceColumnName
        {
            get { return sourceColumnName; }
            set
            {
                if (sourceColumnName != value)
                {
                    sourceColumnName = value;
                    OnPropertyChanged("SourceColumnName");
                }
            }
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
