﻿namespace Datawatch.DataPrep.Data.Framework.Model
{
    /// <summary>
    /// Marker interface signaling that the on-demand parameters are meant for data preview.
    /// </summary>
    public interface IOnDemandPreviewParameters
    {
    }
}
