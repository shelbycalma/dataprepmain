namespace Datawatch.DataPrep.Data.Framework.Model
{
    public interface IValueSeparatorProvider : INamedObject
    {
        string ValueSeparator { get; set; }
    }
}
