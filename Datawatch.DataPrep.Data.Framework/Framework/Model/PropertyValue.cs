﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class PropertyValue : INamedObject, INotifyPropertyChanged
    {
        public event NamedObjectEventHandler NameChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        private string value;

        public PropertyValue()
        {
        }

        public PropertyValue(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        [DataMember]
        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    string oldName = name;
                    name = value;
                    if (NameChanged != null)
                    {
                        NameChanged(this,
                            new NamedObjectEventArgs(oldName, name));
                    }
                    FirePropertyChanged("Name");
                }
            }
        }

        [DataMember]
        public string Value
        {
            get { return value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    FirePropertyChanged("Value");
                }
            }
        }

        protected void FirePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }

        public override int GetHashCode()
        {
            int hashCode = 17;
            if (!string.IsNullOrEmpty(name))
            {
                hashCode = hashCode * 23 + name.GetHashCode();
            }
            if (!string.IsNullOrEmpty(value))
            {
                hashCode = hashCode * 23 + value.GetHashCode();
            }
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            if (!(obj is PropertyValue)) return false;

            PropertyValue pv = (PropertyValue)obj;

            return Utils.IsEqual(name, pv.Name)
                && Utils.IsEqual(value, pv.Value);
        }

        public override string ToString()
        {
            return "PropertyValue(" + name + ":" + value + ")";
        }
    }
}
