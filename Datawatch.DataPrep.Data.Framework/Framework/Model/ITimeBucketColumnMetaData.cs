﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    public interface ITimeBucketColumnMetaData : IBucketColumnMetaData
    {
        ITimeColumn TimeColumn { get; }
        TimePart TimePart { get; }
    }
}
