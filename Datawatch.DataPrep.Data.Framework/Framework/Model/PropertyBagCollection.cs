﻿namespace Datawatch.DataPrep.Data.Framework.Model
{
    public class PropertyBagCollection : ObservableLookupCollection<PropertyBag>
    {
        public new PropertyBag this[string name]
        {
            get
            {
                return base[name];
            }
            set
            {
                if (name != null)
                {
                    PropertyBag propertyBag = base[name];
                    if (propertyBag != null)
                    {
                        Remove(propertyBag);
                    }
                    if (value != null)
                    {
                        value.Name = name;
                        Add(value);
                    }
                }
            }
        }
    }
}
