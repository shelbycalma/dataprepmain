﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    public interface IBucketColumnMetaData : IColumnMetaData
    {
        IColumn AuxColumn { get; }
    }
}
