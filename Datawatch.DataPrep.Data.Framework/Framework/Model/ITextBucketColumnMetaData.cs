﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    public interface ITextBucketColumnMetaData : IBucketColumnMetaData
    {
        ITextColumn TextColumn { get; }
    }
}
