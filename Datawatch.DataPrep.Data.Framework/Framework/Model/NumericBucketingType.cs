﻿using System.ComponentModel;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum NumericBucketingType
    {
        [Description("UiBucketingModeSign")]
        [EnumMember]
        Sign,
        
        [Description("UiBucketingModeEqualDistance")]
        [EnumMember]
        EqualDistance,

        [Description("UiBucketingModeEqualDensity")]
        [EnumMember]
        EqualDensity,
        
        [Description("UiBucketingModeId")]
        [EnumMember]
        Id,

        [Description("UiBucketingModeManual")]
        [EnumMember]
        Manual
    }
}
