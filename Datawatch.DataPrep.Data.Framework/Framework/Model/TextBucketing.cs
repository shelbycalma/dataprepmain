﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Model
{
    [DataContract(Namespace =
        "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class TextBucketing : Bucketing
    {
        private TextBucketingMappingItem[] mapping;

        [DataMember]
        public TextBucketingMappingItem[] Mapping
        {
            get
            {
                return mapping;
            }
            set
            {
                if (mapping == value) return;
                mapping = value;
                OnPropertyChanged("Mapping");
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is TextBucketing)) return false;
            TextBucketing other = (TextBucketing)obj;
            return Utils.IsEqual(mapping, other.mapping);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (mapping != null)
            {
                foreach (TextBucketingMappingItem mappingItem in mapping)
                {
                    code += code * 23 + mappingItem.GetHashCode();
                }
            }

            return code;
        }
    }
}
