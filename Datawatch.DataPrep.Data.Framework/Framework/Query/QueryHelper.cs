﻿using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class QueryHelper
    {
        public static string QuoteColumn(string s, SqlDialect dialect)
        {
            StringBuilder sb = new StringBuilder(s.Length + 2);
            sb.Append(dialect.LeftQuote);
            sb.Append(s);
            sb.Append(dialect.RightQuote);
            return sb.ToString();
        }

        public static string QuoteColumn(SchemaAndTable schemaAndTable,
            string column, SqlDialect dialect)
        {
            if (schemaAndTable != null)
            {
                StringBuilder schemaName = new StringBuilder();
                if (schemaAndTable.Schema != null)
                {
                    schemaName.Append(dialect.LeftQuote);
                    schemaName.Append(schemaAndTable.Schema);
                    schemaName.Append(dialect.RightQuote);
                    schemaName.Append(".");
                }
                schemaName.Append(dialect.LeftQuote);
                schemaName.Append(schemaAndTable.Table);
                schemaName.Append(dialect.RightQuote);

                StringBuilder columnName = new StringBuilder();
                columnName.Append(dialect.LeftQuote);
                columnName.Append(column);
                columnName.Append(dialect.RightQuote);
                return string.Format(dialect.GetQueryPart(
                    QueryPart.ColumnWrap), 
                    schemaName, columnName);
            }
            return QuoteColumn(column, dialect);
        }

        public static string QuoteTextValue(string p, SqlDialect dialect)
        {
            if (p == null) return dialect.GetQueryPart(
                QueryPart.NullSymbol);
            string leftQuote = dialect.GetQueryPart(
                QueryPart.ValueQuoteLeft);
            string rightQuote = dialect.GetQueryPart(
                QueryPart.ValueQuoteRight);
            if (p.Length > 2 
                && p.StartsWith(leftQuote)
                && p.EndsWith(rightQuote))
            {
                return p;
            }

            StringBuilder sb = new StringBuilder(p.Length + 2);
            sb.Append(leftQuote);
            //Escape single quote values
            if (p.Contains("'"))
            {
                p = p.Replace("'", "''");
            }
            if (p.Contains("\"") && 
                dialect.CheckFormattingOptionsApplied(
                    SqlDialectFormattingOptions.DoubleQuoteEscaping))
            {
                // TODO check if we need this for other dialects.
                p = p.Replace("\"", "\\\"");
            }
            sb.Append(p);
            sb.Append(rightQuote);
            return sb.ToString();
        }
    }
}
