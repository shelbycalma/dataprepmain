﻿using System.Runtime.Serialization;
using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class NotOperator : Predicate
    {
        [DataMember]
        public Predicate A { get; set; }

        public NotOperator(Predicate a)
        {
            A = a;
        }

        public override string ToString()
        {
            return ToString(SqlDialectFactoryBase.AnsiSql);
        }

        public override string ToString(SqlDialect dialect)
        {
            if (A == null)
            {
                return "";
            }
            StringBuilder sb = new StringBuilder(
                dialect.GetQueryPart(QueryPart.NotSymbol) + "(");
            sb.AppendFormat(A.ToString(dialect));
            sb.Append(")");
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(NotOperator)) return false;
            return Equals((NotOperator)obj);
        }

        public bool Equals(NotOperator other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Equals(other.A, A);
        }

        public override int GetHashCode()
        {
            return (A != null ? A.GetHashCode() : 0);
        }
    }
}
