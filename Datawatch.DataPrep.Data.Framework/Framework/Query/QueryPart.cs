﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum QueryPart
    {
        
        [EnumMember]
        Select = 0,
        [EnumMember]
        From = 1,
        [EnumMember]
        Where = 2,
        [EnumMember]
        GroupBy = 3,
        [EnumMember]
        Distinct = 4,
        [EnumMember]
        Top = 5,
        [EnumMember]
        Limit = 6,
        [EnumMember]
        AllColumns = 7,
        [EnumMember]
        Alias = 8,
        [EnumMember]
        List = 9,
        [EnumMember]
        ListItemSeparator = 10,
        [EnumMember]
        EmptyList = 11,
        [EnumMember]
        LikeSymbol = 12,
        [EnumMember]
        InSymbol = 13,
        [EnumMember]
        NullSymbol = 14,
        [EnumMember]
        AndSymbol = 15,
        [EnumMember]
        OrSymbol = 16,
        [EnumMember]
        ValueQuoteLeft = 17,
        [EnumMember]
        ValueQuoteRight = 18,
        [EnumMember]
        ValueWrap = 19,
        [EnumMember]
        ColumnWrap = 20,
        [EnumMember]
        NotSymbol = 21,
        [EnumMember]
        GroupColumnAlias = 22,
        [EnumMember]
        EqualsValueWrap = 23,
        [EnumMember]
        PredicateSeparator = 24,
        [EnumMember]
        EqualsSymbol = 25,
        [EnumMember]
        EqualsNullSymbol = 26,
        [EnumMember]
        PredicateLimit = 27
    }
}
