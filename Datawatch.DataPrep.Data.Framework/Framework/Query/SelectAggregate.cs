﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class SelectAggregate : SelectItem
    {
        private readonly string[] columns;
        private readonly FunctionType functionType;

        public SelectAggregate(string[] columns, 
            FunctionType functionType, string alias)
            : base(alias)
        {
            this.columns = columns;
            this.functionType = functionType;
        }

        public FunctionType FunctionType
        {
            get { return functionType; }
        }

        protected override string GetItemString(SchemaAndTable schemaAndTable,
            SqlDialect dialect)
        {
            string[] quotedColumns = new string[columns.Length];

            for (int i=0; i<columns.Length; i++) 
            {
                quotedColumns[i] = QueryHelper.QuoteColumn(
                    schemaAndTable, columns[i], dialect);
            }
            string functionPart = dialect.GetFunctionType(functionType);
            if (functionPart != null) 
            {
                return string.Format(functionPart, quotedColumns);
            }
            switch (functionType)
            {
                case FunctionType.AbsSum:
                    return string.Format("SUM(ABS({0}))",
                        quotedColumns[0]);
                case FunctionType.ConditionalSum:
                    return string.Format("SUM({0})", 
                        string.Format(dialect.IfThenElse, 
                            quotedColumns[1] + "<>0", quotedColumns[0], 0));
                case FunctionType.Max:
                    return string.Format("MAX({0})", quotedColumns[0]);
                case FunctionType.Min:
                    return string.Format("MIN({0})", quotedColumns[0]);
                case FunctionType.Samples:
                    return string.Format("SUM({0})", 
                        string.Format(dialect.IfThenElse, 
                            string.Format("{0} IS NULL", quotedColumns[0]),
                                "0", "1"));
                case FunctionType.Sum:
                    return string.Format("SUM({0})", quotedColumns[0]);
                case FunctionType.SumOfProducts:
                    return string.Format("SUM({0}*{1})", 
                        quotedColumns[0], quotedColumns[1]);
                case FunctionType.SumOfSquares:
                    return string.Format("SUM({0}*{0})", quotedColumns[0]);
                case FunctionType.First:
                    return string.Format("FIRST({0})", quotedColumns[0]);
                case FunctionType.Count:
                    return string.Format("COUNT({0})", quotedColumns[0]);
                case FunctionType.NegativeSum:
                    return string.Format("SUM({0})", 
                        string.Format(dialect.IfThenElse, 
                            string.Format("{0} < 0", quotedColumns[0]),
                                quotedColumns[0], "0"));
                case FunctionType.PositiveSum:
                    return string.Format("SUM({0})",
                        string.Format(dialect.IfThenElse,
                            string.Format("{0} > 0", quotedColumns[0]),
                                quotedColumns[0], "0"));
                case FunctionType.SumOfQuotients:
                    return string.Format("SUM({0})",
                        string.Format(dialect.IfThenElse,
                            string.Format("{0}<>0", quotedColumns[0]),
                            string.Format("1/{0}", quotedColumns[0]), "0"));
                case FunctionType.SumOfWeightedQuotients:
                    return string.Format("SUM({0})",
                        string.Format(dialect.IfThenElse,
                            string.Format("{0}<>0", quotedColumns[1]),
                            string.Format("{0}/{1}", quotedColumns[0], 
                                quotedColumns[1]), "0"));
                case FunctionType.NonZeroSamples:
                    return string.Format("SUM({0})",
                        string.Format(dialect.IfThenElse,
                            string.Format("{0} <> 0", quotedColumns[0]),
                            "1", "0"));
                default:
                    throw Exceptions.FunctionNotSupported(functionType);
            }
        }

        public override string Name
        {
            get { return Alias; }
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj))
            {
                return true;
            }
            if (!(obj is SelectAggregate))
            {
                return false;
            }
            SelectAggregate other = (SelectAggregate)obj;

            return 
                Utils.IsEqual(columns, other.columns) &&
                functionType == other.functionType;
        }

        public override int GetHashCode()
        {
            int code = 17;

            if (columns != null)
            {
                foreach (string column in columns)
                {
                    code = code * 23 + column.GetHashCode();
                }
            }
            code = code * 23 + functionType.GetHashCode();

            return code;
        }
    }
}
