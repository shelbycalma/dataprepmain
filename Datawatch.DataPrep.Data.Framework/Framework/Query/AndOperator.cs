﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class AndOperator : BinaryOperator
    {
        public AndOperator()
        {
        }

        public AndOperator(Predicate a, Predicate b) : base(a, b)
        {
        }

        protected override string Key
        {
            get { return "AND"; }
        }

        public bool UsePredicateSeparator
        {
            get;set;
        }

        protected override string GetKey(SqlDialect dialect)
        {
            if (UsePredicateSeparator)
            {
                return dialect.GetQueryPart(QueryPart.PredicateSeparator);
            }
            return dialect.GetQueryPart(QueryPart.AndSymbol);
        }
    }
}
