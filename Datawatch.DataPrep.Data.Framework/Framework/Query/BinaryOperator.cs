﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public abstract class BinaryOperator : Predicate
    {
        [DataMember]
        public Predicate A { get; set; }
        
        [DataMember]
        public Predicate B { get; set; }

        protected abstract string Key { get; }

        protected abstract string GetKey(SqlDialect dialect);

        protected BinaryOperator()
        {
        }

        protected BinaryOperator(Predicate a, Predicate b)
        {
            A = a;
            B = b;
        }

        public override string ToString()
        {
            return ToString(SqlDialectFactoryBase.AnsiSql);
        }

        public override string ToString(SqlDialect dialect)
        {
            if (A == null)
            {
                if (B == null)
                {
                    return "";
                }
                return B.ToString(dialect);
            }
            if (B == null)
            {
                return A.ToString(dialect);
            }

            StringBuilder sb = new StringBuilder();
            List<Predicate> list = Flatten();
            string key = " " + GetKey(dialect) + " ";
            foreach (Predicate p in list)
            {
                if (sb.Length > 0)
                {
                    sb.Append(key);
                }
                sb.Append("(");
                sb.Append(p.ToString(dialect));
                sb.Append(")");
            }
            return sb.ToString();
        }

        protected List<Predicate> Flatten()
        {
            List<Predicate> list = new List<Predicate>();
            if (A is BinaryOperator && A.GetType() == GetType())
            {
                list.AddRange(((BinaryOperator)A).Flatten());
            } 
            else if (A != null)
            {
                list.Add(A);
            }
            if (B is BinaryOperator && B.GetType() == GetType())
            {
                list.AddRange(((BinaryOperator)B).Flatten());
            } 
            else if (B != null)
            {
                list.Add(B);
            }
            return list;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            if (!(other is BinaryOperator)) return false;
            return Equals((BinaryOperator) other);
        }

        public bool Equals(BinaryOperator other)
        {
            return Equals(Key, other.Key) &&
                   Equals(A, other.A) &&
                   Equals(B, other.B);
        }

        public override int GetHashCode()
        {
            int hashCode = Key.GetHashCode();

            if (A != null)
            {
                hashCode = hashCode * 23 + A.GetHashCode();
            }

            if (B != null)
            {
                hashCode = hashCode * 23 + B.GetHashCode();
            }

            return hashCode;
        }
    }
}
