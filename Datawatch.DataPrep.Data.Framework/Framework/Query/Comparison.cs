﻿using System.Runtime.Serialization;
using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class Comparison : Predicate
    {
        [DataMember]
        public SqlParameter Left { get; set; }
        
        [DataMember]
        public SqlParameter Right { get; set; }

        [DataMember]
        public Operator Operator { get; set; }

        public Comparison()
        {
        }

        public Comparison(Operator op, SqlParameter l, SqlParameter r)
        {
            Left = l;
            Operator = op;
            Right = r;
        }

        public override string ToString()
        {
            return ToString(SqlDialectFactoryBase.AnsiSql);
        }

        public override string ToString(SqlDialect dialect)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Left.ToString(dialect));
            string right = Right.ToString(dialect);
            switch (Operator)
            {
                case Operator.Equals:
                    sb.Append(dialect.GetEqualsSymbol(right));

                    if (Right is StringParameter)
                    {
                        right = string.Format(dialect.GetQueryPart(
                            QueryPart.EqualsValueWrap), right);
                    }
                    break;
                case Operator.GreaterThan:
                    sb.Append(" > ");
                    break;
                case Operator.LessThan:
                    sb.Append(" < ");
                    break;
                case Operator.GreaterThanOrEqualsTo:
                    sb.Append(" >= ");
                    break;
                case Operator.LessThanOrEqualsTo:
                    sb.Append(" <= ");
                    break;
                case Operator.NotEqualTo:
                    sb.Append(" <> ");
                    break;
                case Operator.In:
                    sb.Append(dialect.GetQueryPart(QueryPart.InSymbol));
                    break;
                case Operator.Like:
                    sb.Append(dialect.GetQueryPart(QueryPart.LikeSymbol));
                    break;
            }
            sb.Append(right);
            return sb.ToString();
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is Comparison)) return false;

            Comparison comp = (Comparison)obj;

            return Operator == comp.Operator && 
                Utils.IsEqual(Left, comp.Left) && 
                    Utils.IsEqual(Right, comp.Right);
        }

        public override int GetHashCode()
        {
            int hashCode = 17;
            
            hashCode = hashCode * 23 + Operator.GetHashCode();

            if (Left != null)
            {
                hashCode = hashCode * 23 + Left.GetHashCode();
            }
            
            if (Right != null)
            {
                hashCode = hashCode * 23 + Right.GetHashCode();
            }

            return hashCode;
        }
    }
}
