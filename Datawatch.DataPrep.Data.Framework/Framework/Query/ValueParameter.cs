﻿using System;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class ValueParameter : SqlParameter
    {
        public ValueParameter()
        {
        }


        [DataMember]
        public TypedParameterValue Value { get; set; }

        public ValueParameter(string value) : 
            this(new StringParameterValue(value))
        {
        }

        public ValueParameter(double value) : 
            this(new NumberParameterValue(value))
        {
        }

        public ValueParameter(DateTime value) :
            this(new DateTimeParameterValue(value))
        {
        }

        public ValueParameter(TypedParameterValue value) 
        {
            Value = value;
        }

        public override string ToString(SqlDialect dialect)
        {
            return ToString(Value, dialect);
        }

        private static string ToString(TypedParameterValue typedValue, 
            SqlDialect dialect)
        {
            if (typedValue is StringParameterValue)
            {
                StringParameterValue stringValue = (StringParameterValue)typedValue;
                return stringValue.Value;
            }
            if (typedValue is DateTimeParameterValue)
            {
                DateTimeParameterValue dateTimeValue =
                    (DateTimeParameterValue)typedValue;
                return dialect.QuoteDateTime(dateTimeValue.Value);
            }
            if (typedValue is NumberParameterValue)
            {
                NumberParameterValue numberValue = (NumberParameterValue)typedValue;
                // Fix fo DDTV-734, Kdb supports IEEE standard scientific notation,
                // but weird thing is that, it error with numbers having capital E 
                // like 5.5511151231257827E-17
                // and works with a small e like
                // 5.5511151231257827e-17

                string numberAsString = XmlConvert.ToString(numberValue.Value);
                if (dialect.CheckFormattingOptionsApplied(
                        SqlDialectFormattingOptions.LowerCaseNumbers))
                {
                    return numberAsString.ToLower();
                }
                return numberAsString;
            }
            if (typedValue is ArrayParameterValue)
            {
                ArrayParameterValue arrayValue = (ArrayParameterValue)typedValue;
                StringBuilder sb = new StringBuilder();
                foreach (TypedParameterValue value in arrayValue.Values)
                {
                    if (sb.Length > 1)
                    {
                        sb.Append(",");
                    }
                    sb.Append(ToString(value, dialect));
                }
            }
            return typedValue.ToString();            
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is ValueParameter)) return false;

            return Utils.IsEqual(Value, ((ValueParameter)obj).Value);
        }

        public override int GetHashCode()
        {
            if (Value == null) return 0;
            return Value.GetHashCode();
        }
    }
}
