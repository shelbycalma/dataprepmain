﻿using System.Collections.Generic;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class GroupByClause
    {
        private readonly ISet<GroupItem> groupColumns = new HashSet<GroupItem>();

        public ISet<GroupItem> GroupColumns
        {
            get { return groupColumns; }
        }

        public void AddColumn(string column)
        {
            groupColumns.Add(new GroupColumn(column));
        }

        public void AddTimePart(string column, TimePart part, string alias = "")
        {
            groupColumns.Add(new GroupTimePart(column, part, alias));
        }

        public string ToString(SqlDialect dialect)
        {
            if (groupColumns.Count > 0)
            {
                StringBuilder sb = new StringBuilder(
                    dialect.GetQueryPart(QueryPart.GroupBy));

                int i = 0;                
                foreach (GroupItem item in groupColumns)
                {
                    if (i > 0)
                    {
                        sb.Append(",");
                    }
                    if (string.IsNullOrEmpty(item.Alias))
                    {
                        sb.Append(item.ToString(dialect));
                    }
                    else
                    {
                        sb.AppendFormat(dialect.GetQueryPart(
                            QueryPart.GroupColumnAlias),
                            item.ToString(dialect), item.Alias);
                    }
                    i++;
                }

                return sb.ToString();
            }
            return string.Empty;
        }

        public abstract class GroupItem
        {
            private string alias;

            public string Alias
            {
                get { return alias; }
                set { alias = value; }
            }

            public abstract string ColumnName { get; }
            public abstract string ToString(SqlDialect dialect);

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (!(obj is GroupItem)) return false;
                GroupItem other = (GroupItem)obj;
                return Equals(other.alias, alias);
            }

            public override int GetHashCode()
            {
                return alias != null ? alias.GetHashCode() : 17;
            }
        }

        public class GroupColumn : GroupItem
        {
            private readonly string column;

            public override string ColumnName 
            {
                get { return column; }
            }

            public GroupColumn(string column)
            {
                this.column = column;
            }

            public override string ToString()
            {
                return ToString(SqlDialectFactoryBase.AnsiSql);
            }

            public override string ToString(SqlDialect dialect)
            {
                return QueryHelper.QuoteColumn(column, dialect);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (!base.Equals(obj)) return false;
                if (!(obj is GroupColumn)) return false;
                GroupColumn other = (GroupColumn) obj;
                return Equals(other.column, column);
            }

            public override int GetHashCode()
            {
                int code = base.GetHashCode();
                if (column != null)
                {
                    code = code*23 + column.GetHashCode();
                }
                return code;
            }
        }

        public class GroupTimePart : GroupItem
        {
            private readonly string column;
            private readonly TimePart part;

            public GroupTimePart(string column, TimePart part, string alias)
            {
                this.column = column;
                this.part = part;
                this.Alias = alias;
            }

            public override string ColumnName
            {
                get { return column; }
            }

            public override string ToString()
            {
                return ToString(SqlDialectFactoryBase.AnsiSql);
            }

            public override string ToString(SqlDialect dialect)
            {
                string quotedColumn = QueryHelper.QuoteColumn(column, dialect);
                return dialect.GetDatePart(quotedColumn, part);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                if (ReferenceEquals(this, obj)) return true;
                if (!base.Equals(obj)) return false;
                if (!(obj is GroupTimePart)) return false;
                GroupTimePart other = (GroupTimePart)obj;
                return Equals(other.column, column) && Equals(other.part, part);
            }

            public override int GetHashCode()
            {
                int code = base.GetHashCode();

                code = code*23 + part.GetHashCode();
                if (column != null)
                {
                    code = code*23 + column.GetHashCode();
                }

                return code;
            }
        }
    }
}
