﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class OrOperator : BinaryOperator
    {
        public OrOperator()
        {
        }

        public OrOperator(Predicate a, Predicate b) : base(a, b)
        {
        }

        protected override string Key
        {
            get { return "OR"; }
        }

        protected override string GetKey(SqlDialect dialect)
        {
            return dialect.GetQueryPart(QueryPart.OrSymbol);
        }
    }
}
