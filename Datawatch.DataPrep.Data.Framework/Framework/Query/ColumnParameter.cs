﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{

    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class ColumnParameter : SqlParameter
    {
        [DataMember]
        public string ColumnName { get; set; }

        public ColumnParameter()
        {
        }

        public ColumnParameter(string columnName)
        {
            ColumnName = columnName;
        }

        public override string ToString(SqlDialect dialect)
        {
            return QueryHelper.QuoteColumn(ColumnName, dialect);
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is ColumnParameter)) return false;

            return Utils.IsEqual(ColumnName, ((ColumnParameter)obj).ColumnName);
        }

        public override int GetHashCode()
        {
            if (ColumnName == null) return 0;
            return ColumnName.GetHashCode();
        }
    }
}
