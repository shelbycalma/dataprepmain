﻿using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class SelectClause
    {
        public bool IsDistinct { get; set; }

        private readonly ObservableLookupCollection<SelectItem> selectItems = 
            new ObservableLookupCollection<SelectItem>();

        private int top = -1;

        public int Top
        {
            get { return top; }
            set { top = value; }
        }

        public void AddAggregate(string[] columns, FunctionType functionType,
            string alias)
        {
            SelectAggregate selectAggregate =
                new SelectAggregate(columns, functionType, alias);
            if (selectItems.ContainsKey(selectAggregate.Alias) &&
                !selectItems[selectAggregate.Name].Equals(selectAggregate))
            {                
                throw Exceptions.AliasAlreadyInUse(alias);
            }

            if (!ContainsColumn(alias))
            {
                selectItems.Add(selectAggregate);
            }
        }

        public void AddColumn(string column)
        {
            AddColumn(column, null);
        }

        public void AddColumn(string column, string alias)
        {
            if (!ContainsColumn(column))
            {
                selectItems.Add(new SelectColumn(column, alias));
            }
        }

        public void RemoveColumn(string column)
        {
            if (ContainsColumn(column))
            {
                selectItems.Remove(column);
            }
        }

        public SelectItem GetColumn(string column)
        {
            if (ContainsColumn(column))
            {
                return selectItems[column];
            }
            return null;
        }

        public void AddStar()
        {
            if (!ContainsColumn("*"))
            {
                selectItems.Add(new SelectStar());
            }
        }

        public void AddTimePart(string column, TimePart part, string alias)
        {
            SelectTimePart selectTimePart = new SelectTimePart(column, part, alias);
            if (selectItems.ContainsKey(alias) &&
                !selectItems[selectTimePart.Alias].Equals(selectTimePart))
            {
                throw Exceptions.AliasAlreadyInUse(alias);
            }       
            selectItems.Add(selectTimePart);
        }

        public bool ContainsColumn(string columnName)
        {
            return selectItems.ContainsKey(columnName);
        }

        public void Clear()
        {
            selectItems.Clear();
        }

        public bool IsEmpty
        {
            get { return selectItems.Count == 0; }
        }

        public string ToString(SchemaAndTable schemaAndTable,
            SqlDialect dialect)
        {
            StringBuilder sb = new StringBuilder(
                dialect.GetQueryPart(QueryPart.Select));

            if (IsDistinct)
            {
                sb.Append(dialect.GetQueryPart(QueryPart.Distinct));
            }

            if (top != -1)
            {
                sb.AppendFormat(dialect.GetQueryPart(QueryPart.Top), top);
            }

            if (selectItems.Count > 0)
            {
                int addedItems = 0;
                for (int i = 0; i < selectItems.Count; i++)
                {
                    SelectItem item = selectItems[i];
                    if (addedItems > 0)
                    {
                        sb.Append(",");
                    }
                    try
                    {
                        sb.Append(item.ToString(schemaAndTable, dialect));
                        addedItems++;
                    }
                    catch
                    {
                        
                    }
                }
            }
            else
            {
                sb.Append(dialect.GetQueryPart(QueryPart.AllColumns));
            }
            return sb.ToString();
        }
    }
}
