﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    [KnownType(typeof(ValueParameter))]
    [KnownType(typeof(ColumnParameter))]
    [KnownType(typeof(ListParameter))]
    [KnownType(typeof(StringParameter))]
    [KnownType(typeof(TimePartParameter))]
    public abstract class SqlParameter
    {
        public abstract string ToString(SqlDialect dialect);
    }
}
