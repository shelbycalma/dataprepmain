﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Query
{

    /// <summary>
    /// Additional formatting options for SqlDialect.
    /// </summary>
    /// <remarks>
    /// To extend options within the Data solution inherit from this class and 
    /// add the required values as public static readonly SqlDialectFormattingOptions 
    /// with an int value equal to next power of two.
    /// </remarks>
    public class SqlDialectFormattingOptions
    {
        /// <summary>
        /// No options specified.
        /// </summary>
        public static readonly SqlDialectFormattingOptions None = (SqlDialectFormattingOptions) 0;

        /// <summary>
        /// Textual number representations should be in lower case.
        /// </summary>
        /// <remarks>
        /// Some dialects (e.g. Kdb) support IEEE standard scientific notation,
        /// but they might fail with numbers having capital E 
        /// like 5.5511151231257827E-17 and work with a small e like 
        /// 5.5511151231257827e-17
        /// </remarks>
        public static readonly SqlDialectFormattingOptions LowerCaseNumbers = (SqlDialectFormattingOptions) 1;

        /// <summary>
        /// Escaping of double quote is required.
        /// </summary>
        public static readonly SqlDialectFormattingOptions DoubleQuoteEscaping = (SqlDialectFormattingOptions)2;

        protected SqlDialectFormattingOptions(int value)
        {
            this.Value = value;
        }

        public bool HasFlag(SqlDialectFormattingOptions flag)
        {
            if (flag == null)
                return false;

            return (this.Value & flag.Value) == flag.Value;
        }

        public int Value { get; private set; }

        public static bool operator ==(SqlDialectFormattingOptions op1, SqlDialectFormattingOptions op2)
        {
            if (ReferenceEquals(op1, op2) == true)
                return true;

            if (((Object)op1 == null) && ((Object)op2 == null))
                return true;

            if (((Object)op1 == null) || ((Object)op2 == null))
                return false;

            return op1.Value == op2.Value;
        }

        public static bool operator !=(SqlDialectFormattingOptions op1, SqlDialectFormattingOptions op2)
        {
            return !(op1 == op2);
        }

        public static SqlDialectFormattingOptions operator |(SqlDialectFormattingOptions op1, SqlDialectFormattingOptions op2)
        {
            if (op1 == null)
                op1 = SqlDialectFormattingOptions.None;
            if (op2 == null)
                op2 = SqlDialectFormattingOptions.None;

            return new SqlDialectFormattingOptions(op1.Value | op2.Value);
        }
        public static SqlDialectFormattingOptions operator &(SqlDialectFormattingOptions op1, SqlDialectFormattingOptions op2)
        {
            if (op1 == null || op2 == null)
                return SqlDialectFormattingOptions.None;

            return new SqlDialectFormattingOptions(op1.Value & op2.Value);
        }

        public static explicit operator SqlDialectFormattingOptions(int value)
        {
            return new SqlDialectFormattingOptions(value);
        }

        public static explicit operator int(SqlDialectFormattingOptions options)
        {
            return options != null
                ? options.Value
                : 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            SqlDialectFormattingOptions op = obj as SqlDialectFormattingOptions;
            if (op == null)
                return false;

            return this.Value == op.Value;
        }

        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }
    }
}