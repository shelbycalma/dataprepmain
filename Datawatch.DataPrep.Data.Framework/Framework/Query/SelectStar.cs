﻿namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class SelectStar : SelectItem
    {
        public SelectStar()
            : base("*")
        {
        }

        protected override string GetItemString(SchemaAndTable schemaAndTable, 
            SqlDialect dialect)
        {
            return "*";
        }

        public override string Name
        {
            get { return "*"; }
        }

        public override string ToString(SchemaAndTable schemaAndTable, SqlDialect dialect)
        {
            return "*";
        }
    }
}
