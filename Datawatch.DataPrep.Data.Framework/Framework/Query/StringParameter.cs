﻿using System;
using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class StringParameter : SqlParameter
    {
        [DataMember]
        public String TextValue { get; set; }

        [DataMember]
        public bool IsQuoteValue { get; set; }

        public StringParameter()
        {
        }

        public StringParameter(string s, bool isQuoteValue = true)
        {
            TextValue = s;
            IsQuoteValue = isQuoteValue;
        }

        public override string ToString(SqlDialect dialect)
        {
            string textValue = TextValue;
            if (IsQuoteValue)
            { 
                textValue = QueryHelper.QuoteTextValue(TextValue, dialect);
            }
            return string.Format(dialect.GetQueryPart(
                QueryPart.ValueWrap), textValue);
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is StringParameter)) return false;

            StringParameter param = (StringParameter)obj;
            return (Utils.IsEqual(TextValue, param.TextValue) &&
                IsQuoteValue == param.IsQuoteValue);
        }

        public override int GetHashCode()
        {
            if (TextValue == null) return 0;
            return TextValue.GetHashCode() ^ IsQuoteValue.GetHashCode();
        }
    }
}
