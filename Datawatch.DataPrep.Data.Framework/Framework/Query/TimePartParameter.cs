﻿using System.Runtime.Serialization;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class TimePartParameter : SqlParameter
    {
        public TimePartParameter()
        {
        }

        [DataMember]
        public TimePart TimePart { get; set; }

        [DataMember]
        public string Column { get; set; }

        public TimePartParameter(string column, TimePart timePart)
        {
            Column = column;
            TimePart = timePart;
        }

        public override string ToString()
        {
            return ToString(SqlDialectFactoryBase.AnsiSql);
        }

        public override string ToString(SqlDialect dialect)
        {
            return dialect.GetDatePart(QueryHelper.QuoteColumn(Column, dialect), 
                TimePart);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(obj, null)) return false;
            if (ReferenceEquals(obj, this)) return true;
            if (!(obj is TimePartParameter)) return false;
            TimePartParameter other = (TimePartParameter) obj;
            return Equals(other.TimePart, TimePart) &&
                   Equals(other.Column, Column);
        }

        public override int GetHashCode()
        {
            int code = TimePart.GetHashCode();
            if (Column != null)
            {
                code = code*23 + Column.GetHashCode();
            }
            return code;
        }
    }
}
