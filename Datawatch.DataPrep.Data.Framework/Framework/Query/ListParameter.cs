﻿using System.Runtime.Serialization;
using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Query
{

    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public class ListParameter : SqlParameter
    {
        [DataMember]
        public string[] Values { get; set; }

        [DataMember]
        public bool QuoteValue  { get; set; } 

        public ListParameter()
        {
        }

        public ListParameter(string[] v, bool isQuoteValue = true)
        {
            Values = v;
            QuoteValue = isQuoteValue;
        }

        public override string ToString(SqlDialect dialect)
        {
            StringBuilder sb = new StringBuilder();
            if (Values == null || Values.Length == 0)
            {
                //no need to quote empty list value for a dialect with empty quote marks
                if (dialect.IsQuotationRequired)
                {
                    sb.Append(QueryHelper.QuoteTextValue(
                        dialect.GetQueryPart(QueryPart.EmptyList), dialect));
                }
            }
            else
            {
                for (int i = 0; i < Values.Length; i++)
                {
                    string value = Values[i];
                    if (i > 0)
                    {
                        sb.Append(dialect.GetQueryPart(
                            QueryPart.ListItemSeparator));
                    }
                    if (QuoteValue)
                    {
                        sb.Append(QueryHelper.QuoteTextValue(value, dialect));
                    }
                    else
                    {
                        sb.Append(value);
                    }
                }
            }
            return string.Format(dialect.GetQueryPart(
                QueryPart.List), sb.ToString());
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;

            if (!(obj is ListParameter)) return false;

            return Utils.IsEqual(Values, ((ListParameter)obj).Values);
        }

        public override int GetHashCode()
        {
            int hashCode = 17;

            if (Values != null)
            {
                foreach (string value in Values)
                {
                    if (value != null)
                    {
                        hashCode = hashCode * 23 + value.GetHashCode();
                    }
                }
            }
            return hashCode;
        }
    }
}
