﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    class SelectTimePart : SelectItem
    {
        private readonly string column;
        private readonly TimePart part;

        public SelectTimePart(string column, TimePart part, string alias) 
            : base(alias)
        {
            this.column = column;
            this.part = part;
        }

        protected override string GetItemString(SchemaAndTable schemaAndTable, 
            SqlDialect dialect)
        {
            string quotedColumn = QueryHelper.QuoteColumn(
                    schemaAndTable, column, dialect);

            return dialect.GetDatePart(quotedColumn, part);
        }

        public override string Name
        {
            get { return Alias; }
        }
    }
}
