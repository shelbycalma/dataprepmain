﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    [KnownType(typeof(Comparison))]
    [KnownType(typeof(AndOperator))]
    [KnownType(typeof(OrOperator))]
    [KnownType(typeof(NotOperator))]
    public abstract class Predicate
    {
        public abstract string ToString(SqlDialect dialect);
    }
}
