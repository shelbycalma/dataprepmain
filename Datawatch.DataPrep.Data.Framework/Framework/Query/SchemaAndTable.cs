﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class SchemaAndTable : IComparable<SchemaAndTable>
    {
        public string Schema;
        public string Table;

        public SchemaAndTable(string schema, string table)
        {
            Schema = schema;
            Table = table;
        }

        public override string ToString()
        {
            if (!string.IsNullOrEmpty(Schema))
            {
                return Schema + "." + Table;
            }
            return Table;
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            if (!(obj is SchemaAndTable)) return false;
            SchemaAndTable sat = (SchemaAndTable)obj;
            return Utils.IsEqual(sat.Schema, Schema) &&
                Utils.IsEqual(sat.Table, Table);
        }

        public override int GetHashCode()
        {
            if (Schema != null)
            {
                return Schema.GetHashCode() ^ Table.GetHashCode();
            }
            return Table.GetHashCode();
        }

        public static SchemaAndTable Parse(string s)
        {
            int i = s.IndexOf('.');
            if (i != -1)
            {
                return new SchemaAndTable(s.Substring(0, i), s.Substring(i + 1));
            }
            return new SchemaAndTable(null, s);
        }

        public int CompareTo(SchemaAndTable t)
        {
            //return this.Table.CompareTo(t.Table);
            return this.ToString().CompareTo(t.ToString());
        }
    }
}
