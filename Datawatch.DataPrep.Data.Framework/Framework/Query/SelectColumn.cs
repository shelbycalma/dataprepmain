﻿namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class SelectColumn : SelectItem
    {
        private readonly string columnName;

        public SelectColumn(string name, string alias) : base(alias)
        {
            columnName = name;
        }

        public string ColumnName
        {
            get { return columnName; }
        }

        protected override string GetItemString(SchemaAndTable schemaAndTable,
            SqlDialect dialect)
        {
            return QueryHelper.QuoteColumn(columnName, dialect);
        }

        public override string Name
        {
            get { return Alias != null ? Alias : columnName; }
        }
    }
}
