﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    /// <summary>
    /// SQL dialect factory base.
    /// </summary>
    /// <remarks>
    /// This factory should provide only ANSI SQL dialect as a generic one. 
    /// Other dialects should be provided by higher level (Data.Core or higher level) 
    /// factories which inherit from this class. 
    /// See <see cref="Datawatch.DataPrep.Data.Core.Query.SqlDialectFactory"/> for details.
    /// </remarks>
    public class SqlDialectFactoryBase
    {
        protected const string AnsiSqlDialectName = "AnsiSQL";

        protected const string AnsiDateTimeConcat = "({0} + {1})";
        protected const string AnsiDatePartFunction = "CAST(EXTRACT({0} FROM {1}) AS CHAR(10))";
        protected static readonly string DefaultDateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

        protected static readonly IDictionary<TimePart, string> ansiDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "YEAR" },
                { TimePart.Month, "MONTH" },
                { TimePart.Day, "DAY" },
                { TimePart.Hour, "HOUR" },
                { TimePart.Minute, "MINUTE" },
                { TimePart.Second, "SECOND" },
                { TimePart.Quarter, "QUARTER" }
            };

        protected static readonly IDictionary<QueryPart, string> ansiLimitQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Top, "" },
                { QueryPart.Limit, " LIMIT {0} " }
            };

        protected static readonly IDictionary<QueryPart, string> sqlQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Select, "SELECT " },
                { QueryPart.From, " FROM " },
                { QueryPart.GroupBy, " GROUP BY " },
                { QueryPart.Where, " WHERE " },
                { QueryPart.Distinct, "DISTINCT " },
                { QueryPart.Top, "TOP {0} " },
                { QueryPart.Limit, "" },
                { QueryPart.Alias, "{0} AS {1} " },
                { QueryPart.AllColumns, "*" },
                { QueryPart.List, "({0})" },
                { QueryPart.ListItemSeparator, "," },
                { QueryPart.EmptyList, "###foo###" },
                { QueryPart.LikeSymbol, " LIKE " },
                { QueryPart.InSymbol, " IN " },
                { QueryPart.NullSymbol," NULL " },
                { QueryPart.AndSymbol,"AND" },
                { QueryPart.OrSymbol,"OR" },
                { QueryPart.NotSymbol,"NOT " },
                { QueryPart.ValueQuoteLeft,"'" },
                { QueryPart.ValueQuoteRight,"'" },
                { QueryPart.ValueWrap,"{0}" },
                { QueryPart.ColumnWrap,"{0}.{1}" },
                { QueryPart.EqualsValueWrap,"{0}" },
                { QueryPart.GroupColumnAlias,"{0}" },
                { QueryPart.PredicateSeparator," AND " },
                { QueryPart.EqualsSymbol," = " },
                { QueryPart.EqualsNullSymbol," IS " },
                { QueryPart.PredicateLimit,"" }
            };

        protected static readonly IDictionary<string, SqlDialect> dialects =
            new Dictionary<string, SqlDialect>
            {
                {
                    AnsiSqlDialectName,
                    new SqlDialect(AnsiSqlDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "'{0}'", AnsiDatePartFunction, ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts)
                }
            };

        protected SqlDialectFactoryBase()
        {

        }

        /// <summary>
        /// Gets the ANSI SQL dialect.
        /// </summary>
        /// <value>
        /// The ANSI SQL dialect instance.
        /// </value>
        public static SqlDialect AnsiSql
        {
            get { return dialects[AnsiSqlDialectName]; }
        }
    }
}