﻿namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class WhereClause
    {
        private Predicate predicate;
        private int limit = -1;
        
        public int Limit
        {
            get { return limit; }
            set { limit = value; }
        }

        public Predicate Predicate
        {
            get { return predicate; }
            set { predicate = value; }
        }

        public string ToString(SqlDialect dialect)
        {
            if (predicate == null && limit == -1) return "";
            string predicateString = string.Empty;
            string limitQuery = string.Empty;

            if (predicate != null)
            {
                predicateString = predicate.ToString(dialect);
            }

            if (string.IsNullOrEmpty(predicateString))
            {
                predicateString = string.Empty;
            }

            if (limit != -1)
            {
                limitQuery =
                string.Format(dialect.GetQueryPart(QueryPart.PredicateLimit),
                limit);
            }

            if (!string.IsNullOrEmpty(predicateString))
            {
                predicateString = dialect.GetQueryPart(QueryPart.Where) +
                    predicateString;
            }

            if (string.IsNullOrEmpty(limitQuery)) return predicateString;

            if (string.IsNullOrEmpty(predicateString))
            {
                predicateString += dialect.GetQueryPart(QueryPart.Where);
            }
            else
            {
                predicateString += dialect.GetQueryPart(QueryPart.AndSymbol);
            }
            predicateString += limitQuery;

            return predicateString;
        }
    }
}
