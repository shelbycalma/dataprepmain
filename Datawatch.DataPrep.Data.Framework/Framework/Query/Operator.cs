﻿using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum Operator
    {
        [EnumMember]
        Equals,
        [EnumMember]
        GreaterThan,
        [EnumMember]
        LessThan,
        [EnumMember]
        GreaterThanOrEqualsTo,
        [EnumMember]
        LessThanOrEqualsTo,
        [EnumMember]
        NotEqualTo,
        [EnumMember]
        In,
        [EnumMember]
        Like
    }
}
