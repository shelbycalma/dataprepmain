﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public class SqlDialect
    {
        private readonly string name;
        private readonly string leftQuote;
        private readonly string rightQuote;
        private readonly string ifThenElse;
        private readonly string sourceQuery;
        private readonly string dateTimeConvert;
        private readonly string datePartFunction;
        private readonly IDictionary<TimePart, string> dateParts;
        private readonly string dateTimeConcat;
        private readonly string dateTimeFormat;
        private readonly IDictionary<QueryPart, string> queryParts;
        private readonly IDictionary<FunctionType, string> functionTypes;
        private readonly SqlDialectFormattingOptions options;

        public SqlDialect(string name,
            string left, string right,
            string ifthenelse, string sourceQuery,
            string dateTimeConvert,
            string datePartFunction,
            IDictionary<TimePart, string> timePartMapping,
            string dateTimeConcat,
            string dateTimeFormat,
            IDictionary<QueryPart, string> queryPartMapping = null,
            IDictionary<QueryPart, string> partialQueryPartMapping = null,
            IDictionary<FunctionType, string> functionTypeMapping = null,
            SqlDialectFormattingOptions options = null)
        {
            if (queryPartMapping == null)
                throw new ArgumentNullException(nameof(queryPartMapping));

            this.name = name;
            this.leftQuote = left;
            this.rightQuote = right;
            this.ifThenElse = ifthenelse;
            this.sourceQuery = sourceQuery;
            this.dateTimeConvert = dateTimeConvert;
            this.datePartFunction = datePartFunction;
            this.dateTimeFormat = dateTimeFormat;

            this.dateParts = timePartMapping != null
                ? timePartMapping.ToDictionary(x => x.Key, x => x.Value)
                : new Dictionary<TimePart, string>();

            this.dateTimeConcat = dateTimeConcat;
            var tmpQueryParts = queryPartMapping.ToDictionary(x => x.Key, x => x.Value);
            if (partialQueryPartMapping != null)
            {
                foreach (var map in partialQueryPartMapping)
                {
                    tmpQueryParts[map.Key] = map.Value;
                }
            }

            this.queryParts = tmpQueryParts;
            this.functionTypes = functionTypeMapping != null
                ? functionTypeMapping.ToDictionary(x => x.Key, x => x.Value)
                : new Dictionary<FunctionType, string>();

            this.options = options ?? SqlDialectFormattingOptions.None;
        }

        public string Name
        {
            get { return name; }
        }

        public string LeftQuote
        {
            get { return leftQuote; }
        }

        public string RightQuote
        {
            get { return rightQuote; }
        }

        public string IfThenElse
        {
            get { return ifThenElse; }
        }

        public string SourceQuery
        {
            get { return sourceQuery; }
        }

        public string DatePartFunction
        {
            get { return datePartFunction; }
        }

        public string DateTimeConvert
        {
            get { return dateTimeConvert; }
        }

        public string DateTimeFormat
        {
            get { return dateTimeFormat; }
        }

        public string ConcatDateAndTime(string dateColumn, string timeColumn)
        {
            dateColumn = QueryHelper.QuoteColumn(dateColumn, this);
            timeColumn = QueryHelper.QuoteColumn(timeColumn, this);

            return string.Format(dateTimeConcat, dateColumn, timeColumn);
        }

        public string GetDatePart(string column, TimePart part)
        {
            return string.Format(DatePartFunction, ToString(part), column);
        }

        private string ToString(TimePart timePart)
        {
            if (!dateParts.ContainsKey(timePart))
            {
                throw new Exception("Unsupported TimePart!");
            }
            return dateParts[timePart];
        }

        public string GetQueryPart(QueryPart queryPart)
        {
            if (!queryParts.ContainsKey(queryPart))
            {
                throw new Exception("Unsupported QueryPart!");
            }
            return queryParts[queryPart];
        }

        public string GetEqualsSymbol(string p)
        {
            QueryPart queryPart = QueryPart.EqualsSymbol;

            if (p.Equals(GetQueryPart(QueryPart.NullSymbol)))
            {
                queryPart = QueryPart.EqualsNullSymbol;
            }
            return GetQueryPart(queryPart);
        }

        public string GetFunctionType(FunctionType functionType)
        {
            if (!functionTypes.ContainsKey(functionType))
            {
                return null;
            }
            return functionTypes[functionType];
        }

        public string QuoteDateTime(DateTime dateTime)
        {
            return string.Format(dateTimeConvert,
                dateTime.ToString("yyyy-MM-dd HH:mm:ss",
                    System.Globalization.CultureInfo.InvariantCulture));
        }

        public bool IsQuotationRequired
        {
            get
            {
                return
                    string.IsNullOrEmpty(LeftQuote) == false ||
                    string.IsNullOrEmpty(RightQuote) == false;
            }
        }

        //public bool CheckFormattingOptionsApplied(SqlDialectFormattingOptions optionsToCheck)
        //{
        //    return (this.options & optionsToCheck) == optionsToCheck;
        //}

        public bool CheckFormattingOptionsApplied(SqlDialectFormattingOptions optionsToCheck)
        {
            return this.options.HasFlag(optionsToCheck);
        }
    }
 }

