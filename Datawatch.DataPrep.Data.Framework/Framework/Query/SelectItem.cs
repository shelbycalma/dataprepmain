﻿using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Query
{
    public abstract class SelectItem : INamedObject
    {
        public event NamedObjectEventHandler NameChanged;

        private readonly string alias;

        public SelectItem(string alias)
        {
            this.alias = alias;
        }

        public string Alias
        {
            get { return alias; }
        }

        abstract protected string GetItemString(SchemaAndTable schemaAndTable,
            SqlDialect dialect);

        protected void OnNameChanged(NamedObjectEventArgs e)
        {
            if (NameChanged != null) {
                NameChanged(this, e);
            }
        }

        public virtual string ToString(
            SchemaAndTable schemaAndTable, SqlDialect dialect)
        {            
            StringBuilder sb = new StringBuilder(
                GetItemString(schemaAndTable, dialect));
            if (alias != null)
            {
                return string.Format(dialect.GetQueryPart(QueryPart.Alias), 
                    sb.ToString(), QueryHelper.QuoteColumn(alias, dialect));
            }
            return sb.ToString();
        }

        public abstract string Name
        {
            get;
        }
    }
}
