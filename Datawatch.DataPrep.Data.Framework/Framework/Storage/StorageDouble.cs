﻿using System;
using System.Diagnostics;

namespace Datawatch.DataPrep.Data.Framework.Storage
{
    // TODO: Would this be faster if we hard-coded NumericValue.Empty
    // as the value to initialize and clear with?
    /// <summary>
    /// A large storage for 64-bit floating point numbers.
    /// </summary>
    /// <remarks>
    /// <para>The storage classes are a replacement for regular arrays that are
    /// more efficient for a large number of elements, or when the capacity is
    /// changed often.</para>
    /// <para>The storage is implemented as an array of fixed-size array
    /// segments, which makes memory use more efficient for very large storages
    /// since it avoids allocating a single large object, and also makes
    /// resizing faster since most of the segments can be left untouched.</para>
    /// <para>All segments are allocated immediately when a storage is created
    /// as this is more efficient than doing it as they are needed. Storages are
    /// always fixed size and does not grow or shrink dynamically, but the
    /// capacity can be changed on an existing storage.</para>
    /// </remarks>
    public sealed class StorageDouble : StorageBase<double>
    {
        /// <summary>
        /// If <c>true</c>, unused elements (the ones corresponding to holes in
        /// a <see cref="Compact(int[])"/> operation) are moved to the end of
        /// the storage after the compact and reused.
        /// </summary>
        public static readonly bool ReuseOnCompact = false;

        /// <summary>
        /// The minimum capacity for the storage - you cannot create a storage
        /// with smaller capacity than this.
        /// </summary>
        public static readonly int MinCapacity = 1;

        /// <summary>
        /// The number of bits used for addressing within a single segment.
        /// </summary>
        /// <remarks>
        /// <para>This value determines the <see cref="SegmentSize"/> for the
        /// storage, and should be chosen so that the individual array segments
        /// of the storage are as large as possible without being allocated on
        /// the .NET large object heap.</para>
        /// <para>Object on the large object heap will not be moved by the
        /// garbage collector which causes severe memory fragmentation over
        /// time, and from experiments it looks like accessing memory on the
        /// large object heap is a lot slower than on the regular heap.</para>
        /// </remarks>
        private const int SegmentBits = 9;

        /// <summary>
        /// The number of elements in a single segment (as determined by the
        /// <see cref="SegmentBits"/> constant).
        /// </summary>
        public const int SegmentSize = 1 << SegmentBits;

        /// <summary>
        /// Bit mask used to quickly get the segment-local part of an element
        /// address.
        /// </summary>
        private const int SegmentRecordMask = SegmentSize - 1;


        /// <summary>
        /// The actual storage elements as an array of arrays.
        /// </summary>
        private double[][] segments;

        /// <summary>
        /// The current capacity of the storage.
        /// </summary>
        private int capacity;


        /// <summary>
        /// Creates a new storage and allocates all memory for it.
        /// </summary>
        /// <param name="capacity">The requested capacity as number of
        /// elements (if less than <see cref="MinCapacity"/> that value will be
        /// used instead).</param>
        public StorageDouble(int capacity)
        {
            // Cap capacity to min capacity.
            capacity = Math.Max(capacity, MinCapacity);
            // Calculate number of segments.
            int segmentCount = ((capacity - 1) >> SegmentBits) + 1;
            // Allocate the array of segments (but not the segments).
            segments = new double[segmentCount][];
            
            // Allocate all segments except the last.
            for (int i = 0; i < segmentCount - 1; i++) {
                segments[i] = new double[SegmentSize];
            }
            
            // Calculate size of the last segment.
            int lastSegmentSize = capacity - (segmentCount - 1) * SegmentSize;
            Debug.Assert(lastSegmentSize <= SegmentSize,
                "The last segment cannot be larger than SegmentSize");
            
            // Allocate the last segment.
            segments[segmentCount - 1] = new double[lastSegmentSize];
            
            this.capacity = capacity;
            Debug.Assert(this.capacity ==
                (segments.Length - 1) * SegmentSize +
                segments[segments.Length - 1].Length,
                "The actual capacity is incorrect");
        }

        /// <summary>
        /// Gets or sets the storage's current capacity as number of elements.
        /// </summary>
        /// <remarks>
        /// <para>The existing elements in the storage will be kept when the
        /// capacity is changed (but possibly truncated).</para>
        /// </remarks>
        public override int Capacity
        {
            get { return capacity; }
            set {
                // Quick exit if capacity is the same.
                if (value == capacity) {
                    return;
                }
                // Cap value to min capacity.
                value = Math.Max(value, MinCapacity);

                // Calculate old and new segment count and size of last segment.
                int oldSegmentCount = segments.Length;
                int newSegmentCount = ((value - 1) >> SegmentBits) + 1;
                int oldLastSegmentSize = segments[oldSegmentCount - 1].Length;
                int newLastSegmentSize =
                    value - (newSegmentCount - 1) * SegmentSize;
                
                // The new storage.
                double[][] newSegments = new double[newSegmentCount][];

                // There are four different possible cases here:
                //
                // #1:  There are more segements after resize than before. This
                //      means the old last segment will go into a full new
                //      segment (and obviously fit).
                // #2:  There are fewer segements after resize than before. So
                //      the new last segment will hold as much as it can of the
                //      corresponding old segment, and old elements on higher
                //      addresses will be discarded.
                // #3a: There are equally many segments after as before, and the
                //      new last segment is smaller than the old (so the old
                //      will be truncated.
                // #3b: Same as #3a but the new last segment is larger than the
                //      old.

                if (newSegmentCount > oldSegmentCount) {
                    // Case #1.
                    // Copy old full segments first.
                    for (int i = 0; i < oldSegmentCount - 1; i++) {
                        newSegments[i] = segments[i];
                    }
                    // Possibly not full last old segment.
                    double[] oldLastSegment = segments[oldSegmentCount - 1];
                    double[] newLastSegment;
                    if (oldLastSegmentSize == SegmentSize) {
                        newLastSegment = oldLastSegment;
                    } else {
                        // oldLastSegmentSize < SegmentSize.
                        newLastSegment = new double[SegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, oldLastSegmentSize);
                    }
                    newSegments[oldSegmentCount - 1] = newLastSegment;
                    // New empty full segments.
                    for (int i = oldSegmentCount;
                        i < newSegmentCount - 1; i++) {
                        newSegments[i] = new double[SegmentSize];
                    }
                    // New last (possibly not full) empty segment.
                    newSegments[newSegmentCount - 1] =
                        new double[newLastSegmentSize];
                }
                else if (newSegmentCount < oldSegmentCount) {
                    // Case #2.
                    // Copy old full segments first.
                    for (int i = 0; i < newSegmentCount - 1; i++) {
                        newSegments[i] = segments[i];
                    }
                    // Possibly not full new last segment.
                    double[] oldLastSegment = segments[newSegmentCount - 1];
                    double[] newLastSegment;
                    if (newLastSegmentSize == SegmentSize) {
                        newLastSegment = oldLastSegment;
                    } else {
                        // oldLastSegmentSize == SegmentSize, and
                        // newLastSegmentSize < SegmentSize.
                        newLastSegment = new double[newLastSegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, newLastSegmentSize);
                    }
                    newSegments[newSegmentCount - 1] = newLastSegment;
                }
                else {
                    // Case #3.
                    // newSegmentCount == oldSegmentCount.
                    // Copy old full segments first.
                    for (int i = 0; i < newSegmentCount - 1; i++) {
                        newSegments[i] = segments[i];
                    }
                    // Last segment, may be shorter or longer.
                    double[] oldLastSegment = segments[newSegmentCount - 1];
                    double[] newLastSegment;
                    if (newLastSegmentSize == oldLastSegmentSize) {
                        newLastSegment = oldLastSegment;
                    } else if (newLastSegmentSize < oldLastSegmentSize) {
                        // Case #3a.
                        newLastSegment = new double[newLastSegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, newLastSegmentSize);
                    } else {
                        // Case #3b.
                        newLastSegment = new double[newLastSegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, oldLastSegmentSize);
                    }
                    newSegments[newSegmentCount - 1] = newLastSegment;
                }

                this.segments = newSegments;
                this.capacity = value;
                Debug.Assert(this.capacity ==
                    (segments.Length - 1) * SegmentSize +
                    segments[segments.Length - 1].Length,
                    "The actual capacity is incorrect");
            }
        }

        /// <summary>
        /// Clears the storage by setting all elements to a single value.
        /// </summary>
        /// <param name="value">The value to clear with.</param>
        public void Clear(double value)
        {
            if (value == 0.0) {
                // Special case for 0.0 as we can use Array.Clear.
                for (int i = 0; i < segments.Length - 1; i++) {
                    Array.Clear(segments[i], 0, SegmentSize);
                }
                double[] lastSegment = segments[segments.Length - 1];
                Array.Clear(lastSegment, 0, lastSegment.Length);
            } else {
                // We need to go through and set all elements.
                // Do the full segments first (as they're fixed size).
                for (int i = 0; i < segments.Length - 1; i++) {
                    double[] segment = segments[i];
                    for (int j = 0; j < SegmentSize; j++) {
                        segment[j] = value;
                    }
                }
                // Now do the last segment.
                double[] lastSegment = segments[segments.Length - 1];
                int lastSegmentSize = lastSegment.Length;
                for (int j = 0; j < lastSegmentSize; j++) {
                    lastSegment[j] = value;
                }
            }
        }

        // TODO: Optimize (do not go through this[int]).
        /// <summary>
        /// Clears a range of elements in the storage by settings them to
        /// a single value.
        /// </summary>
        /// <param name="index">Index of the first element to clear.</param>
        /// <param name="count">Number of elements to clear.</param>
        /// <param name="value">The value to clear with.</param>
        public void Clear(int index, int count, double value)
        {
            for (int i = index + count - 1; i >= index; i--) {
                this[i] = value;
            }
        }

        /// <summary>
        /// Removes a number of elements and moves the remaining elements down
        /// (to lower indices) to fill the holes.
        /// </summary>
        /// <param name="holes">Indices of the elements to remove and an extra
        /// entry at the end for the total number of records (so it points just
        /// past the last used record or hole), must be in ascending
        /// order.</param>
        /// <remarks>
        /// <para>This does not change the storage's capacity.</para>
        /// <para>Depending on <see cref="ReuseOnCompact"/>, the removed values
        /// may be moved to the end of the storage for reuse.</para>
        /// </remarks>
        public override void Compact(int[] holes)
        {
            // Caller cannot ask us to do this for more records that
            // there is capacity for.
            Debug.Assert(holes[holes.Length - 1] <= capacity,
                "More elements to remove than current capacity");

            // TODO: Optimization opportunity.
            // This code can be written without the extra "reuse" array by just
            // swapping elements around (so all that's needed is a "T temp"
            // variable). Observe that this is a permutation with one cycle per
            // hole. So start at each hole (beginning with the first), follow
            // the cycle through back to the hole.

            int holeCount = holes.Length - 1;

            double[] reuse = null;
            if (ReuseOnCompact) {
                // Copy out the removed records.
                reuse = new double[holeCount];
                for (int i = 0; i < holeCount; i++) {
                    reuse[i] = this[holes[i]];
                }
            }

            // Move the live records down to fill the holes.
            for (int i = 0; i < holeCount; i++) {
                // For each run between holes, including the run between the
                // last hole and the end of the records.
                int first = holes[i];
                int last = holes[i + 1];
                // Number of steps to move the records (the same as the
                // number of holes we have passed).
                int step = i + 1;
                for (int j = first + 1; j < last; j++) {
                    this[j - step] = this[j];
                }
            }

            if (ReuseOnCompact) {
                // Copy the removed records back in.
                int target = holes[holes.Length - 1] - holeCount;
                for (int i = 0; i < holeCount; i++) {
                    this[target++] = reuse[i];
                }
            }
        }

        /// <summary>
        /// Makes a copy of the storage.
        /// </summary>
        /// <param name="storage">An optional existing storage that will be
        /// reused for the copy (may be <c>null</c>).</param>
        /// <returns>The storage copy.</returns>
        public StorageDouble CopyTo(StorageDouble storage)
        {
            if (storage == null) {
                storage = new StorageDouble(capacity);
            } else {
                storage.Capacity = capacity;
            }

            // The copy now has the exact same dimensions as this one, so this
            // is simple: just array copy each segment.
            for (int i = 0; i < segments.Length; i++) {
                Array.Copy(segments[i], storage.segments[i],
                    segments[i].Length);
            }
            return storage;
        }

        /// <summary>
        /// Copies a range of elements from the storage into a regular array.
        /// </summary>
        /// <param name="index">Index of first element to copy.</param>
        /// <param name="array">Target array for the copied elements.</param>
        /// <param name="arrayIndex">First index in target array to copy
        /// elements to.</param>
        /// <param name="count">Number of elements to copy.</param>
        internal void CopyTo(int index,
            double[] array, int arrayIndex, int count)
        {
            // TODO: Get rid of or speed up.
            for (int i = 0; i < count; i++) {
                array[arrayIndex + i] = this[index + i];
            }
        }

        /// <summary>
        /// Gets or sets a specific element value.
        /// </summary>
        /// <param name="record">Index (address) of the element to set.</param>
        /// <returns>The element value.</returns>
        public override double this[int record]
        {
            get {
                // Shift out SegmentBits from the address to get the segment
                // index, and and it with the mask to get the segment-local
                // index.
                return segments[record >> SegmentBits]
                    [record & SegmentRecordMask];
            }
            set {
                segments[record >> SegmentBits]
                    [record & SegmentRecordMask] = value;
            }
        }
    }
}
