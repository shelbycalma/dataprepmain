﻿using System;
using System.Diagnostics;

namespace Datawatch.DataPrep.Data.Framework.Storage
{
    /// <summary>
    /// A large storage for 32-bit integers.
    /// </summary>
    /// <remarks>
    /// <para>The storage classes are a replacement for regular arrays that are
    /// more efficient for a large number of elements, or when the capacity is
    /// changed often.</para>
    /// <para>The storage is implemented as an array of fixed-size array
    /// segments, which makes memory use more efficient for very large storages
    /// since it avoids allocating a single large object, and also makes
    /// resizing faster since most of the segments can be left untouched.</para>
    /// <para>All segments are allocated immediately when a storage is created
    /// as this is more efficient than doing it as they are needed. Storages are
    /// always fixed size and does not grow or shrink dynamically, but the
    /// capacity can be changed on an existing storage.</para>
    /// </remarks>
    public sealed class StorageInt : StorageBase<int>
    {
        /// <summary>
        /// If <c>true</c>, unused elements (the ones corresponding to holes in
        /// a <see cref="Compact(int[])"/> operation) are moved to the end of
        /// the storage after the compact and reused.
        /// </summary>
        public static readonly bool ReuseOnCompact = false;

        /// <summary>
        /// The minimum capacity for the storage - you cannot create a storage
        /// with smaller capacity than this.
        /// </summary>
        public static readonly int MinCapacity = 1;

        /// <summary>
        /// The number of bits used for addressing within a single segment.
        /// </summary>
        /// <remarks>
        /// <para>This value determines the <see cref="SegmentSize"/> for the
        /// storage, and should be chosen so that the individual array segments
        /// of the storage are as large as possible without being allocated on
        /// the .NET large object heap.</para>
        /// <para>Object on the large object heap will not be moved by the
        /// garbage collector which causes severe memory fragmentation over
        /// time, and from experiments it looks like accessing memory on the
        /// large object heap is a lot slower than on the regular heap.</para>
        /// </remarks>
        private const int SegmentBits = 14;

        /// <summary>
        /// The number of elements in a single segment (as determined by the
        /// <see cref="SegmentBits"/> constant).
        /// </summary>
        public const int SegmentSize = 1 << SegmentBits;

        /// <summary>
        /// Bit mask used to quickly get the segment-local part of an element
        /// address.
        /// </summary>
        private const int SegmentRecordMask = SegmentSize - 1;


        /// <summary>
        /// The actual storage elements as an array of arrays.
        /// </summary>
        private int[][] segments;

        /// <summary>
        /// The current capacity of the storage.
        /// </summary>
        private int capacity;


        /// <summary>
        /// Creates a new storage and allocates all memory for it.
        /// </summary>
        /// <param name="capacity">The requested capacity as number of
        /// elements (if less than <see cref="MinCapacity"/> that value will be
        /// used instead).</param>
        public StorageInt(int capacity)
        {
            // Cap capacity to min capacity.
            capacity = Math.Max(capacity, MinCapacity);
            // Calculate number of segments.
            int segmentCount = ((capacity - 1) >> SegmentBits) + 1;
            // Allocate the array of segments (but not the segments).
            segments = new int[segmentCount][];
            
            // Allocate all segments except the last.
            for (int i = 0; i < segmentCount - 1; i++) {
                segments[i] = new int[SegmentSize];
            }
            
            // Calculate size of the last segment.
            int lastSegmentSize = capacity - (segmentCount - 1) * SegmentSize;
            Debug.Assert(lastSegmentSize <= SegmentSize,
                "The last segment cannot be larger than SegmentSize");
            
            // Allocate the last segment.
            segments[segmentCount - 1] = new int[lastSegmentSize];
            
            this.capacity = capacity;
            Debug.Assert(this.capacity ==
                (segments.Length - 1) * SegmentSize +
                segments[segments.Length - 1].Length,
                "The actual capacity is incorrect");
        }

        /// <summary>
        /// Supports <see cref="StorageBool.And(StorageBool)"/>.
        /// </summary>
        internal void BitwiseAnd(StorageInt storage)
        {
            Debug.Assert(storage.capacity == this.capacity,
                "This assumes both storages have the same size.");

            for (int i = 0; i < segments.Length - 1; i++) {
                int[] thisSegment = segments[i];
                int[] thatSegment = storage.segments[i];
                for (int j = 0; j < SegmentSize; j++) {
                    thisSegment[j] &= thatSegment[j];
                }
            }
            int[] thisLastSegment = segments[segments.Length - 1];
            int[] thatLastSegment = storage.segments[segments.Length - 1];
            for (int j = thisLastSegment.Length - 1; j >= 0; j--) {
                thisLastSegment[j] &= thatLastSegment[j];
            }
        }

        /// <summary>
        /// Supports <see cref="StorageBool.AndNot(StorageBool)"/>.
        /// </summary>
        internal void BitwiseAndNot(StorageInt storage)
        {
            Debug.Assert(storage.capacity == this.capacity,
                "This assumes both storages have the same size.");

            for (int i = 0; i < segments.Length - 1; i++) {
                int[] thisSegment = segments[i];
                int[] thatSegment = storage.segments[i];
                for (int j = 0; j < SegmentSize; j++) {
                    thisSegment[j] = thisSegment[j] & ~thatSegment[j];
                }
            }
            int[] thisLastSegment = segments[segments.Length - 1];
            int[] thatLastSegment = storage.segments[segments.Length - 1];
            for (int j = thisLastSegment.Length - 1; j >= 0; j--) {
                thisLastSegment[j] = thisLastSegment[j] & ~thatLastSegment[j];
            }
        }

        /// <summary>
        /// Gets or sets the storage's current capacity as number of elements.
        /// </summary>
        /// <remarks>
        /// <para>The existing elements in the storage will be kept when the
        /// capacity is changed (but possibly truncated).</para>
        /// </remarks>
        public override int Capacity
        {
            get { return capacity; }
            
            set {
                // Quick exit if capacity is the same.
                if (value == capacity) {
                    return;
                }
                // Cap value to min capacity.
                value = Math.Max(value, MinCapacity);

                // Calculate old and new segment count and size of last segment.
                int oldSegmentCount = segments.Length;
                int newSegmentCount = ((value - 1) >> SegmentBits) + 1;
                int oldLastSegmentSize = segments[oldSegmentCount - 1].Length;
                int newLastSegmentSize =
                    value - (newSegmentCount - 1) * SegmentSize;
                
                // The new storage.
                int[][] newSegments = new int[newSegmentCount][];

                // There are four different possible cases here:
                //
                // #1:  There are more segements after resize than before. This
                //      means the old last segment will go into a full new
                //      segment (and obviously fit).
                // #2:  There are fewer segements after resize than before. So
                //      the new last segment will hold as much as it can of the
                //      corresponding old segment, and old elements on higher
                //      addresses will be discarded.
                // #3a: There are equally many segments after as before, and the
                //      new last segment is smaller than the old (so the old
                //      will be truncated.
                // #3b: Same as #3a but the new last segment is larger than the
                //      old.

                if (newSegmentCount > oldSegmentCount) {
                    // Case #1.
                    // Copy old full segments first.
                    for (int i = 0; i < oldSegmentCount - 1; i++) {
                        newSegments[i] = segments[i];
                    }
                    // Possibly not full last old segment.
                    int[] oldLastSegment = segments[oldSegmentCount - 1];
                    int[] newLastSegment;
                    if (oldLastSegmentSize == SegmentSize) {
                        newLastSegment = oldLastSegment;
                    } else {
                        // oldLastSegmentSize < SegmentSize.
                        newLastSegment = new int[SegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, oldLastSegmentSize);
                    }
                    newSegments[oldSegmentCount - 1] = newLastSegment;
                    // New empty full segments.
                    for (int i = oldSegmentCount;
                        i < newSegmentCount - 1; i++) {
                        newSegments[i] = new int[SegmentSize];
                    }
                    // New last (possibly not full) empty segment.
                    newSegments[newSegmentCount - 1] =
                        new int[newLastSegmentSize];
                }
                else if (newSegmentCount < oldSegmentCount) {
                    // Case #2.
                    // Copy old full segments first.
                    for (int i = 0; i < newSegmentCount - 1; i++) {
                        newSegments[i] = segments[i];
                    }
                    // Possibly not full new last segment.
                    int[] oldLastSegment = segments[newSegmentCount - 1];
                    int[] newLastSegment;
                    if (newLastSegmentSize == SegmentSize) {
                        newLastSegment = oldLastSegment;
                    } else {
                        // oldLastSegmentSize == SegmentSize, and
                        // newLastSegmentSize < SegmentSize.
                        newLastSegment = new int[newLastSegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, newLastSegmentSize);
                    }
                    newSegments[newSegmentCount - 1] = newLastSegment;
                }
                else {
                    // Case #3.
                    // newSegmentCount == oldSegmentCount.
                    // Copy old full segments first.
                    for (int i = 0; i < newSegmentCount - 1; i++) {
                        newSegments[i] = segments[i];
                    }
                    // Last segment, may be shorter or longer.
                    int[] oldLastSegment = segments[newSegmentCount - 1];
                    int[] newLastSegment;
                    if (newLastSegmentSize == oldLastSegmentSize) {
                        newLastSegment = oldLastSegment;
                    } else if (newLastSegmentSize < oldLastSegmentSize) {
                        // Case #3a.
                        newLastSegment = new int[newLastSegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, newLastSegmentSize);
                    } else {
                        // Case #3b.
                        newLastSegment = new int[newLastSegmentSize];
                        Array.Copy(oldLastSegment,
                            newLastSegment, oldLastSegmentSize);
                    }
                    newSegments[newSegmentCount - 1] = newLastSegment;
                }

                this.segments = newSegments;
                this.capacity = value;
                Debug.Assert(this.capacity ==
                    (segments.Length - 1) * SegmentSize +
                    segments[segments.Length - 1].Length,
                    "The actual capacity is incorrect");
            }
        }

        /// <summary>
        /// Clears the storage by setting all elements to a single value.
        /// </summary>
        /// <param name="value">The value to clear with.</param>
        public void Clear(int value)
        {
            if (value == 0) {
                // Special case for 0 as we can use Array.Clear.
                for (int i = 0; i < segments.Length - 1; i++) {
                    Array.Clear(segments[i], 0, SegmentSize);
                }
                int[] lastSegment = segments[segments.Length - 1];
                Array.Clear(lastSegment, 0, lastSegment.Length);
            } else {
                // We need to go through and set all elements.
                // Do the full segments first (as they're fixed size).
                for (int i = 0; i < segments.Length - 1; i++) {
                    int[] segment = segments[i];
                    for (int j = 0; j < SegmentSize; j++) {
                        segment[j] = value;
                    }
                }
                // Now do the last segment.
                int[] lastSegment = segments[segments.Length - 1];
                int lastSegmentSize = lastSegment.Length;
                for (int j = 0; j < lastSegmentSize; j++) {
                    lastSegment[j] = value;
                }
            }
        }

        // TODO: Optimize (do not go through this[int]).
        /// <summary>
        /// Clears a range of elements in the storage by settings them to
        /// a single value.
        /// </summary>
        /// <param name="index">Index of the first element to clear.</param>
        /// <param name="count">Number of elements to clear.</param>
        /// <param name="value">The value to clear with.</param>
        public void Clear(int index, int count, int value)
        {
            for (int i = index + count - 1; i >= index; i--) {
                this[i] = value;
            }
        }

        /// <summary>
        /// Removes a number of elements and moves the remaining elements down
        /// (to lower indices) to fill the holes.
        /// </summary>
        /// <param name="holes">Indices of the elements to remove and an extra
        /// entry at the end for the total number of records (so it points just
        /// past the last used record or hole), must be in ascending
        /// order.</param>
        /// <remarks>
        /// <para>This does not change the storage's capacity.</para>
        /// <para>Depending on <see cref="ReuseOnCompact"/>, the removed values
        /// may be moved to the end of the storage for reuse.</para>
        /// </remarks>
        public override void Compact(int[] holes)
        {
            // Caller cannot ask us to do this for more records that
            // there is capacity for.
            Debug.Assert(holes[holes.Length - 1] <= capacity,
                "More elements to remove than current capacity");

            // TODO: Optimization opportunity.
            // This code can be written without the extra "reuse" array by just
            // swapping elements around (so all that's needed is a "T temp"
            // variable). Observe that this is a permutation with one cycle per
            // hole. So start at each hole (beginning with the first), follow
            // the cycle through back to the hole.

            int holeCount = holes.Length - 1;

            int[] reuse = null;
            if (ReuseOnCompact) {
                // Copy out the removed records.
                reuse = new int[holeCount];
                for (int i = 0; i < holeCount; i++) {
                    reuse[i] = this[holes[i]];
                }
            }

            // Move the live records down to fill the holes.
            for (int i = 0; i < holeCount; i++) {
                // For each run between holes, including the run between the
                // last hole and the end of the records.
                int first = holes[i];
                int last = holes[i + 1];
                // Number of steps to move the records (the same as the
                // number of holes we have passed).
                int step = i + 1;
                for (int j = first + 1; j < last; j++) {
                    this[j - step] = this[j];
                }
            }

            if (ReuseOnCompact) {
                // Copy the removed records back in.
                int target = holes[holes.Length - 1] - holeCount;
                for (int i = 0; i < holeCount; i++) {
                    this[target++] = reuse[i];
                }
            }
        }

        /// <summary>
        /// Makes a copy of the storage.
        /// </summary>
        /// <param name="storage">An optional existing storage that will be
        /// reused for the copy (may be <c>null</c>).</param>
        /// <returns>The storage copy.</returns>
        public StorageInt CopyTo(StorageInt storage)
        {
            if (storage == null) {
                storage = new StorageInt(capacity);
            } else {
                storage.Capacity = capacity;
            }

            // The copy now has the exact same dimensions as this one, so this
            // is simple: just array copy each segment.
            for (int i = 0; i < segments.Length; i++) {
                Array.Copy(segments[i], storage.segments[i],
                    segments[i].Length);
            }
            return storage;
        }

        /// <summary>
        /// Makes a copy of specific elements from this storage.
        /// </summary>
        /// <param name="map">Index map of elements to copy (the element in this
        /// storage with the index at address zero in the map will end up at
        /// address zero in the copy).</param>
        /// <param name="count">The number of elements to copy (the map's
        /// capacity needs to be at least as large as this value).</param>
        /// <param name="storage">An optional existing storage that will be
        /// reused for the copy (may be <c>null</c>).</param>
        /// <returns>The storage copy.</returns>
        public StorageInt CopyTo(StorageInt map, int count, StorageInt storage)
        {
            Debug.Assert(count <= map.capacity,
                "The map must be at least as large as the number to copy");
            Debug.Assert(count <= capacity,
                "Cannot copy more elements than are in this storage");

            if (storage == null) {
                storage = new StorageInt(count);
            } else {
                storage.Capacity = count;
            }
            
            // Total number of segments (in the map, and target) to copy.
            int segmentCount = ((count - 1) >> SegmentBits) + 1;
            // Copy the full (map and target) segments first.
            for (int i = 0; i < segmentCount - 1; i++) {
                int[] mapSegment = map.segments[i];
                int[] storageSegment = storage.segments[i];
                for (int j = 0; j < SegmentSize; j++) {
                    // Get address of element.
                    int record = mapSegment[j];
                    // Get the element from this.
                    int[] segment = segments[record >> SegmentBits];
                    int value = segment[record & SegmentRecordMask];
                    // Set output in copy.
                    storageSegment[j] = value;
                }
            }
            // Now do the last segment.
            int lastSegmentSize = count - (segmentCount - 1) * SegmentSize;
            int[] lastMapSegment = map.segments[segmentCount - 1];
            int[] lastStorageSegment = storage.segments[segmentCount - 1];
            for (int j = 0; j < lastSegmentSize; j++) {
                // Get address of element.
                int record = lastMapSegment[j];
                // Get the element from this.
                int[] segment = segments[record >> SegmentBits];
                int value = segment[record & SegmentRecordMask];
                // Set output in copy.
                lastStorageSegment[j] = value;
            }
            return storage;
        }

        /// <summary>
        /// Copies a range of elements from the storage into a regular array.
        /// </summary>
        /// <param name="index">Index of first element to copy.</param>
        /// <param name="array">Target array for the copied elements.</param>
        /// <param name="arrayIndex">First index in target array to copy
        /// elements to.</param>
        /// <param name="count">Number of elements to copy.</param>
        public void CopyTo(int index, int[] array, int arrayIndex, int count)
        {
            // TODO: Get rid of or speed up.
            for (int i = 0; i < count; i++) {
                array[arrayIndex + i] = this[index + i];
            }
        }

        /// <summary>
        /// For each distinct element value in the storage, count the number
        /// of elements with that value.
        /// </summary>
        /// <param name="count">The number of elements to include in the count
        /// (starting with the one at index zero).</param>
        /// <param name="counts">Target array for the counts.</param>
        /// <remarks>
        /// <para>This is a utility method for counting sort.</para>
        /// <para>The first element in the target array will be the number of
        /// zeroes in the storage, the second the number of ones, and so on.
        /// The target array must be large enough to accommodate the largest
        /// element in the storage.</para>
        /// <para>The target array should be initialized with zeroes. If not,
        /// the existing values will be incremented by the counts.</para>
        /// </remarks>
        public void Count(int count, int[] counts)
        {
            // Number of segments we will go through.
            int segmentCount = ((count - 1) >> SegmentBits) + 1;
            // Do full segments first.
            for (int i = 0; i < segmentCount - 1; i++) {
                int[] segment = segments[i];
                for (int j = 0; j < SegmentSize; j++) {
                    counts[segment[j]]++;
                }
            }
            // Then do the last segment.
            int lastSegmentCount = count - (segmentCount - 1) * SegmentSize;
            int[] lastSegment = segments[segmentCount - 1];
            for (int j = lastSegmentCount - 1; j >= 0; j--) {
                counts[lastSegment[j]]++;
            }
        }

        // TODO: Make instance method, but for whom?
        /// <summary>
        /// Performs a counting sort, given existing counts.
        /// </summary>
        /// <param name="keys">The integer keys for the values in the
        /// <paramref name="items"/> storage.</param>
        /// <param name="items">The items to sort.</param>
        /// <param name="count">The number of items to sort.</param>
        /// <param name="counts">Accumulated counts for keys in
        /// <paramref name="keys"/>. The values in this array will not be
        /// destroyed by the sort.</param>
        /// <param name="temp">A temporary storage that will be used during the
        /// sort (needs to have capacity larger than
        /// <paramref name="count"/>).</param>
        /// <remarks>
        /// <para>The <paramref name="counts"/> array should have one entry for
        /// each distinct key in <paramref name="keys"/>. The value in
        /// <c>counts[k]</c> should be the number of keys that are equal to or
        /// less than <c>k</c>.</para>
        /// <para>The <paramref name="keys"/> storage is NOT rearranged by this
        /// method. This is so multiple item storages can be sorted in the same
        /// order by consecutive calls to this method. If you need to sort the
        /// keys too, call this method a second time and pass the keys both as
        /// <paramref name="keys"/> and <paramref name="items"/>.</para>
        /// </remarks>
        public static void CountingSort(
            StorageInt keys, ref StorageInt items, int count,
            int[] counts, ref StorageInt temp)
        {
            // Number of segments to sort.
            int segmentCount = ((count - 1) >> SegmentBits) + 1;
            // Do last segment first.
            int[] keySegment = keys.segments[segmentCount - 1];
            int[] itemSegment = items.segments[segmentCount - 1];
            int lastSegmentCount = count - (segmentCount - 1) * SegmentSize;
            // For each element in the last segment.
            for (int source = lastSegmentCount - 1; source >= 0; source--) {
                // The integer key for the element.
                int key = keySegment[source];
                // Target address (can be in any segment).
                int target = counts[key] - 1;
                int targetSegmentIndex = target >> SegmentBits;
                int[] targetSegment = temp.segments[targetSegmentIndex];
                int targetRecordIndex = target & SegmentRecordMask;
                // Copy over the element.
                targetSegment[targetRecordIndex] = itemSegment[source];
                // Reduce count for the key.
                counts[key]--;
            }
            // Now do the full segments.
            for (int i = segmentCount - 2; i >= 0; i--) {
                keySegment = keys.segments[i];
                itemSegment = items.segments[i];
                // For each element in the segment.
                for (int source = SegmentSize - 1; source >= 0; source--) {
                    int key = keySegment[source];
                    int target = counts[key] - 1;
                    // Target address (can be in any segment).
                    int targetSegmentIndex = target >> SegmentBits;
                    int[] targetSegment = temp.segments[targetSegmentIndex];
                    int targetRecordIndex = target & SegmentRecordMask;
                    // Copy over the element.
                    targetSegment[targetRecordIndex] = itemSegment[source];
                    // Reduce count for the key.
                    counts[key]--;
                }
            }
            
            // Fix up the counts array (restore it).
            for (int i = 0; i < counts.Length - 1; i++) {
                counts[i] = counts[i + 1];
            }
            counts[counts.Length - 1] = count;

            // Switch around the storages so result is in 'items' parameter.
            StorageInt t = items;
            items = temp;
            temp = t;
        }

        // TODO: A little bit strange that this method lives here.
        /// <summary>
        /// Builds a <see cref="KeyIndex"/> for a string storage.
        /// </summary>
        /// <param name="items">The string storage to index.</param>
        /// <param name="count">The number of strings to include.</param>
        /// <param name="index">An optional existing key index that will be
        /// reused (may be <c>null</c>).</param>
        /// <returns>The built key index.</returns>
        public static KeyIndex GetIndex(StoragePointer<string> items, int count,
            KeyIndex index)
        {
            // Make sure we have a target and it's large enough.
            if (index == null) {
                index = new KeyIndex(count);
            } else {
                index.keys.Capacity = count;
                index.Count = count;
            }

            // TODO: Reusable?
            // Build map of distinct strings in here.
            StringIndex distinct = new StringIndex(count);
            // Direct access to the target's key storage.
            StorageInt keys = index.keys;

// Disable "unreachable code" warning, which we will always get here. This if
// statement checks the size of string pointers and optimizes for when it's the
// same as integers (so on x86 builds only).
#pragma warning disable 0162
            if (StorageInt.SegmentSize == StoragePointer<string>.SegmentSize) {
                // sizeof(string*) is same as sizeof(int), so we can use the
                // same segment addressing for both.
                int segmentCount = ((count - 1) >> SegmentBits) + 1;
                for (int i = 0; i < segmentCount; i++) {
                    // Get the segment with the strings.
                    string[] valueSegment = items.GetSegment(i);
                    // Get the target segment (where to put keys).
                    int[] indexSegment = keys.segments[i];
                    // Get the segment size.
                    int segmentSize = i < segmentCount - 1 ?
                        SegmentSize : count - (segmentCount - 1) * SegmentSize;
                    for (int j = 0; j < segmentSize; j++) {
                        // This is the string value.
                        string item = valueSegment[j];
                        // Use the StringIndex to get the key.
                        indexSegment[j] = distinct.GetKey(item);
                    }
                }
            } else {
                // sizeof(string*) is different from sizeof(int), so use
                // this[int] on 'items' to get the strings (slower).
                int segmentCount = ((count - 1) >> SegmentBits) + 1;
                // Current string item address.
                int itemIndex = 0;
                // Go through all segments in the key storage.
                for (int i = 0; i < segmentCount; i++) {
                    // Target key segment.
                    int[] indexSegment = keys.segments[i];
                    // Get the segment size.
                    int segmentSize = i < segmentCount - 1 ?
                        SegmentSize : count - (segmentCount - 1) * SegmentSize;
                    for (int j = 0; j < segmentSize; j++) {
                        // This is the string value.
                        string item = items[itemIndex++];
                        // Use the StringIndex to get the key.
                        indexSegment[j] = distinct.GetKey(item);
                    }
                }
            }
#pragma warning restore 0162
            
            // Set the other index properties.
            // We built the 'distinct' StringIndex from scratch, so we know
            // what the max key is.
            index.MaxKey = distinct.Count - 1;
            // And we also know if they were all distinct.
            index.IsDistinct = distinct.Count == count;

            return index;
        }

        // TODO: A little bit strange that this method lives here.
        /// <summary>
        /// Builds a <see cref="KeyIndex"/> for selected elements from a
        /// string storage.
        /// </summary>
        /// <param name="map">Address map that defines the string elements to
        /// include in the index.</param>
        /// <param name="items">The string storage to index.</param>
        /// <param name="count">The number of strings to include.</param>
        /// <param name="index">An optional existing key index that will be
        /// reused (may be <c>null</c>).</param>
        /// <returns>The built key index.</returns>
        public static KeyIndex GetIndex(
            StorageInt map, StoragePointer<string> items, int count,
            KeyIndex index)
        {
            Debug.Assert(count <= map.capacity,
                "The map must be at least as large as the number to index");
            Debug.Assert(count <= items.Capacity,
                "Cannot index more elements than are in the string storage");

            // Make sure we have a target and it's large enough.
            if (index == null) {
                index = new KeyIndex(count);
            } else {
                index.keys.Capacity = count;
                index.Count = count;
            }

            // TODO: Reusable?
            // Build map of distinct strings in here.
            StringIndex distinct = new StringIndex(count);
            // Direct access to the target's key storage.
            StorageInt keys = index.keys;

            // No fancy x86/x64 optimizations here.

            // Number of segments (in target key index) to copy.
            int segmentCount = ((count - 1) >> SegmentBits) + 1;
            for (int i = 0; i < segmentCount; i++) {
                // Current map and target key segments.
                int[] mapSegment = map.segments[i];
                int[] indexSegment = keys.segments[i];
                // Get size of segment.
                int segmentSize = i < segmentCount - 1 ?
                    SegmentSize : count - (segmentCount - 1) * SegmentSize;
                for (int j = 0; j < segmentSize; j++) {
                    // Look up the string value through the map.
                    string item = items[mapSegment[j]];
                    // Use the StringIndex to get the key.
                    indexSegment[j] = distinct.GetKey(item);
                }
            }
            
            // Set the other index properties.
            // We built the 'distinct' StringIndex from scratch, so we know
            // what the max key is.
            index.MaxKey = distinct.Count - 1;
            // And we also know if they were all distinct.
            index.IsDistinct = distinct.Count == count;

            return index;
        }

        /// <summary>
        /// Returns the maximum element value in the storage.
        /// </summary>
        /// <param name="count">Number of elements to include.</param>
        /// <returns>The maximum value.</returns>
        public int GetMax(int count)
        {
            Debug.Assert(count > 0, "Count must be larger than zero");
            Debug.Assert(count <= capacity,
                "Count cannot be larger than the storage's capacity");

            int max = int.MinValue;
            int segmentCount = ((count - 1) >> SegmentBits) + 1;
            for (int i = 0; i < segmentCount - 1; i++) {
                int[] segment = segments[i];
                for (int j = 0; j < SegmentSize; j++) {
                    max = Math.Max(max, segment[j]);
                }
            }
            int[] lastSegment = segments[segmentCount - 1];
            int lastSegmentSize = count - (segmentCount - 1) * SegmentSize;
            for (int j = 0; j < lastSegmentSize; j++) {
                max = Math.Max(max, lastSegment[j]);
            }
            return max;
        }

        /// <summary>
        /// Sets all elements in the storage to a running count.
        /// </summary>
        /// <param name="count">The number of elements to set.</param>
        /// <remarks>
        /// <para>The element at address zero will be set to zero, at address
        /// one to one, and so on.</para>
        /// </remarks>
        public void SetIncrementing(int count)
        {
            Debug.Assert(count > 0, "Count must be larger than zero");
            Debug.Assert(count <= capacity,
                "Count cannot be larger than the storage's capacity");

            int value;
            int segmentCount = ((count - 1) >> SegmentBits) + 1;
            // Do the full segments first.
            for (int i = 0; i < segmentCount - 1; i++) {
                int[] segment = segments[i];
                value = i << SegmentBits;
                for (int j = 0; j < SegmentSize; j++) {
                    segment[j] = value++;
                }
            }
            int[] lastSegment = segments[segmentCount - 1];
            int lastSegmentSize = count - (segmentCount - 1) * SegmentSize;
            value = count - 1;
            // Do the last segment (in reverse, since that's more fun).
            for (int j = lastSegmentSize - 1; j >= 0; j--) {
                lastSegment[j] = value--;
            }
        }
            
        /// <summary>
        /// Gets or sets a specific element value.
        /// </summary>
        /// <param name="record">Index (address) of the element to set.</param>
        /// <returns>The element value.</returns>
        public override int this[int record]
        {
            get {
                // Shift out SegmentBits from the address to get the segment
                // index, and and it with the mask to get the segment-local
                // index.
                return segments[record >> SegmentBits]
                    [record & SegmentRecordMask];
            }
            set {
                segments[record >> SegmentBits]
                    [record & SegmentRecordMask] = value;
            }
        }
    }
}
