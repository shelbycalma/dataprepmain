﻿
namespace Datawatch.DataPrep.Data.Framework.Storage
{
    /// <summary>
    /// Maintains an ordered set of integer keys.
    /// </summary>
    /// <remarks>
    /// <para>This is basically a <see cref="StorageInt"/> with some extra
    /// meta data properties.</para>
    /// </remarks>
    public sealed class KeyIndex
    {
        /// <summary>
        /// The storage for the keys.
        /// </summary>
        internal StorageInt keys;

        /// <summary>
        /// The number of keys in the storage.
        /// </summary>
        private int count;

        /// <summary>
        /// The largest key stored.
        /// </summary>
        private int maxKey;

        /// <summary>
        /// If set, <c>true</c> means every single key is distinct and
        /// <c>false</c> means there are duplicates (if not set you don't know).
        /// </summary>
        private bool? distinct;


        /// <summary>
        /// Creates a new key index.
        /// </summary>
        /// <param name="count">The number of keys in the index.</param>
        public KeyIndex(int count)
        {
            keys = new StorageInt(count);
            this.count = count;
        }

        /// <summary>
        /// Gets or sets the number of keys in the index.
        /// </summary>
        /// <remarks>
        /// <para>Setting this property does not affect the capacity of the
        /// <see cref="Keys"/> storage, so make sure to update that too.</para>
        /// </remarks>
        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        /// <summary>
        /// If set, <c>true</c> means every single key is distinct and
        /// <c>false</c> means there are duplicates (if not set you don't know).
        /// </summary>
        /// <remarks>
        /// <para>This property only carries meta data and is not calculated by
        /// the index itself.</para>
        /// </remarks>
        public bool? IsDistinct
        {
            get { return distinct; }
            set { distinct = value; }
        }

        /// <summary>
        /// The largest key stored in the index.
        /// </summary>
        /// <remarks>
        /// <para>This property only carries meta data and is not calculated by
        /// the index itself.</para>
        /// </remarks>
        public int MaxKey
        {
            get { return maxKey; }
            set { maxKey = value; }
        }

        /// <summary>
        /// The storage that holds the keys themselves.
        /// </summary>
        public StorageInt Keys
        {
            get { return keys; }
            set { keys = value; }
        }
    }
}
