﻿namespace Datawatch.DataPrep.Data.Framework.Storage
{
    /// <summary>
    /// Base class for large storages.
    /// </summary>
    /// <remarks>
    /// <para>The storage classes are a replacement for regular arrays that are
    /// more efficient for a large number of elements, or when the capacity is
    /// changed often.</para>
    /// <para>A storage is implemented as an array of fixed-size array segments,
    /// which makes memory use more efficient for very large storages since it
    /// avoids allocating a single large object, and also makes resizing faster
    /// since most of the segments can be left untouched.</para>
    /// <para>All segments are allocated immediately when a storage is created
    /// as this is more efficient than doing it as they are needed. Storages are
    /// always fixed size and does not grow or shrink dynamically, but the
    /// capacity can be changed on an existing storage.</para>
    /// </remarks>
    public abstract class StorageBase<T>
    {
        /// <summary>
        /// Gets or sets the storage's current capacity as number of elements.
        /// </summary>
        /// <remarks>
        /// <para>The existing elements in the storage will be kept when the
        /// capacity is changed (but possibly truncated).</para>
        /// </remarks>
        public abstract int Capacity { get; set; }

        /// <summary>
        /// Removes a number of elements and moves the remaining elements down
        /// (to lower indices) to fill the holes.
        /// </summary>
        /// <param name="holes">Indices of the elements to remove and an extra
        /// entry at the end for the total number of records (so it points just
        /// past the last used record or hole), must be in ascending
        /// order.</param>
        /// <remarks>
        /// <para>This does not change the storage's capacity.</para>
        /// </remarks>
        public abstract void Compact(int[] holes);

        /// <summary>
        /// Gets or sets a specific element value.
        /// </summary>
        /// <param name="record">Index (address) of the element to set.</param>
        /// <returns>The element value.</returns>
        public abstract T this[int record] { get; set; }
    }
}
