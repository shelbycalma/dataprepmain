﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Storage
{
    /// <summary>
    /// A large storage for booleans.
    /// </summary>
    /// <remarks>
    /// <para>This storage is very memory-efficient as it packs the boolean
    /// values as bits into integers.</para>
    /// <para>The storage classes are a replacement for regular arrays that are
    /// more efficient for a large number of elements, or when the capacity is
    /// changed often.</para>
    /// <para>The storage is implemented as an array of fixed-size array
    /// segments, which makes memory use more efficient for very large storages
    /// since it avoids allocating a single large object, and also makes
    /// resizing faster since most of the segments can be left untouched.</para>
    /// <para>All segments are allocated immediately when a storage is created
    /// as this is more efficient than doing it as they are needed. Storages are
    /// always fixed size and does not grow or shrink dynamically, but the
    /// capacity can be changed on an existing storage.</para>
    /// </remarks>
    public sealed class StorageBool : StorageBase<bool>
    {
        /// <summary>
        /// The minimum capacity for the storage - you cannot create a storage
        /// with smaller capacity than this.
        /// </summary>
        public static readonly int MinCapacity = 32;

        
        /// <summary>
        /// The integer storage into which we pack the booleans as bits.
        /// </summary>
        private readonly StorageInt storage;


        /// <summary>
        /// Creates a new storage and allocates all memory for it.
        /// </summary>
        /// <param name="capacity">The requested capacity as number of
        /// elements (if less than <see cref="MinCapacity"/> that value will be
        /// used instead).</param>
        public StorageBool(int capacity)
        {
            // Cap capacity to min capacity.
            capacity = Math.Max(capacity, MinCapacity);

            // Allocate the integer storage - 32 booleans will fit into each
            // 32-bit integer so this divides by 32 (and rounds up).
            storage = new StorageInt(((capacity - 1) >> 5) + 1);
        }

        /// <summary>
        /// Performs a bitwise logical AND with another storage and updates
        /// this storage with the result.
        /// </summary>
        /// <param name="storage">Storage to and with.</param>
        /// <remarks>
        /// <para>The method changes the contents of this storage, but leaves
        /// the argument storage untouched.</para>
        /// </remarks>
        public void And(StorageBool storage)
        {
            if (this.storage.Capacity != storage.storage.Capacity) {
                throw Exceptions.StorageCapacitiesDifferent();
            }
            this.storage.BitwiseAnd(storage.storage);
        }

        /// <summary>
        /// Performs a bitwise logical AND NOT with another storage and
        /// updates this storage with the result.
        /// </summary>
        /// <param name="storage">Storage to and not with.</param>
        /// <remarks>
        /// <para>After the operation, a bit will be set in this storage iff
        /// it was set before and the corresponding bit in the argument
        /// storage was not set.</para>
        /// <para>The method changes the contents of this storage, but leaves
        /// the argument storage untouched.</para>
        /// </remarks>
        public void AndNot(StorageBool storage)
        {
            if (this.storage.Capacity != storage.storage.Capacity) {
                throw Exceptions.StorageCapacitiesDifferent();
            }
            this.storage.BitwiseAndNot(storage.storage);
        }

        /// <summary>
        /// Gets or sets the storage's current capacity as number of elements.
        /// </summary>
        /// <remarks>
        /// <para>The capacity for this storage can only be set to multiples of
        /// 32 (any other value will be rounded up).</para>
        /// <para>The existing elements in the storage will be kept when the
        /// capacity is changed (but possibly truncated).</para>
        /// </remarks>
        public override int Capacity
        {
            get {
                // Multiple number of integers by 32.
                return storage.Capacity << 5;
            }
            set {
                // Cap value to min capacity.
                value = Math.Max(value, MinCapacity);
                // Round up to a multiple of 32 (and divide down).
                storage.Capacity = ((value - 1) >> 5) + 1;
            }
        }

        /// <summary>
        /// Clears the storage by setting all elements to a single value.
        /// </summary>
        /// <param name="value">The value to clear with.</param>
        public void Clear(bool value)
        {
            if (!value) {
                storage.Clear(0);
            } else {
                storage.Clear(-1);
            }
        }

        /// <summary>
        /// This storage does not support compacting, and will throw an
        /// exception if you call this method.
        /// </summary>
        /// <param name="holes">Not used.</param>
        public override void Compact(int[] holes)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Makes a copy of the storage.
        /// </summary>
        /// <param name="storage">An optional existing storage that will be
        /// reused for the copy (may be <c>null</c>).</param>
        /// <returns>The storage copy.</returns>
        public StorageBool CopyTo(StorageBool storage)
        {
            if (storage == null) {
                storage = new StorageBool(this.storage.Capacity << 5);
            } else {
                storage.Capacity = this.storage.Capacity << 5;
            }

            this.storage.CopyTo(storage.storage);

            return storage;
        }

        /// <summary>
        /// Gets or sets a specific element value.
        /// </summary>
        /// <param name="record">Index (address) of the element to set.</param>
        /// <returns>The element value.</returns>
        public override bool this[int record]
        {
            get {
                // Divide by 32 to get the integer, then AND with bit
                // mask to see if bit is set.
                return (storage[record >> 5] &
                    (1 << (record & 0x1f))) != 0;
            }
            set {
                // Divide by 32 to get the integer, then either...
                int bits = storage[record >> 5];
                if (value) {
                    // ...OR with bit mask to set bit or...
                    bits |= 1 << (record & 0x1f);
                } else {
                    // ...AND with negated mask to clear it.
                    bits &= ~(1 << (record & 0x1f));
                }
                // Put updated bits back in.
                storage[record >> 5] = bits;
            }
        }
    }
}
