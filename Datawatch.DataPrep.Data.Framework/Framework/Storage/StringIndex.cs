﻿using System;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Storage
{
    // TODO: Optimization opportunity: implement custom hash table.
    /// <summary>
    /// Keeps track of distinct string values and assigns each an integer key.
    /// </summary>
    /// <remarks>
    /// <para>The index uses the <see cref="StringComparer.Ordinal"/> comparer
    /// to test for string equality, so it will not handle special characters
    /// encoded with different Unicode escape sequences.</para>
    /// </remarks>
    public sealed class StringIndex
    {
        /// <summary>
        /// String value to integer key lookup.
        /// </summary>
        /// <remarks>
        /// <para>Would be readonly if it wasn't for
        /// <see cref="CopyTo(StringIndex)"/>.</para>
        /// </remarks>
        private Dictionary<string, int> index;
        
        /// <summary>
        /// Integer key for the <c>null</c> string value, set to <c>-1</c> if
        /// the <c>null</c> string is not in the index.
        /// </summary>
        private int nullKey;

        /// <summary>
        /// Next (unused) integer key value.
        /// </summary>
        private int nextKey;


        /// <summary>
        /// Creates a new index.
        /// </summary>
        /// <param name="capacity">The initial capacity (as number of distinct
        /// string values) for the index.</param>
        public StringIndex(int capacity)
        {
            index = new Dictionary<string, int>(
                capacity, StringComparer.Ordinal);
            
            nullKey = -1;
            nextKey = 0;
        }

        /// <summary>
        /// Creates a new index with an initial set of values.
        /// </summary>
        /// <param name="values">The values to populate the index with.</param>
        /// <param name="count">The number of values to use.</param>
        public StringIndex(StoragePointer<string> values, int count)
            : this(count)
        {
            for (int i = count - 1; i >= 0; i--) {
                string value = values[i];
                if (value != null) {
                    index.Add(value, i);
                } else {
                    nullKey = i;
                }
            }
            nextKey = count;
        }

        /// <summary>
        /// Copies the index into another index object.
        /// </summary>
        /// <param name="target">The index to populate with the contents of
        /// this one.</param>
        /// <remarks>
        /// <para>This clears any existing contents in the
        /// <paramref name="target"/> index before copying.</para>
        /// </remarks>
        public void CopyTo(StringIndex target)
        {
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }

            target.index = new Dictionary<string, int>(index);
            target.nullKey = nullKey;
            target.nextKey = nextKey;
        }

        /// <summary>
        /// Gets the number of distinct string values in the index.
        /// </summary>
        public int Count
        {
            get { return nextKey; }
        }

        /// <summary>
        /// Gets the integer key for a string value (adds the value and assigns
        /// a new key if it wasn't in the index already).
        /// </summary>
        /// <param name="value">The string value to look up.</param>
        /// <returns>The integer key that identifies the value.</returns>
        public int GetKey(string value)
        {
            int key;
            if (value != null) {
                // Not the null value, check if we've seen it already.
                if (!index.TryGetValue(value, out key)) {
                    // Nope, so give it a new key and add it.
                    key = nextKey++;
                    index.Add(value, key);
                }
                // If it was already in the index, we have the key.
            } else {
                // The null value, check if we've seen it already.
                if ((key = nullKey) < 0) {
                    // Nope, so give it a new key and set the null key field.
                    key = nullKey = nextKey++;
                }
                // If it was already in the index, we have the key.
            }
            return key;
        }

        /// <summary>
        /// Gets the integer key for a string value.
        /// </summary>
        /// <param name="value">The string value to look up.</param>
        /// <param name="key">The integer key that identifies the value.</param>
        /// <returns><c>True</c> if the value was added to the index in this
        /// call, <c>false</c> if it wasn't (the value was already in the
        /// index).</returns>
        /// <remarks>
        /// <para>This method is like <see cref="GetKey(string)"/> but also
        /// returns a flag indicating if the value was in the index or not. It
        /// still adds the value, so on exit from the method it's guaranteed to
        /// be in there.</para>
        /// </remarks>
        public bool GetKey(string value, out int key)
        {
            if (value != null) {
                // Not the null value, check if we've seen it already.
                if (!index.TryGetValue(value, out key)) {
                    // Nope, so give it a new key and add it.
                    key = nextKey++;
                    index.Add(value, key);
                    // Added, so return true.
                    return true;
                }
                // Already in the index and we have the key.
            } else {
                // The null value, check if we've seen it already.
                if ((key = nullKey) < 0) {
                    // Nope, so give it a new key and set the null key field.
                    key = nullKey = nextKey++;
                    // Added, so return true.
                    return true;
                }
                // Already in the index and we have the key.
            }
            // Already in the index, not added, return false.
            return false;
        }
    }
}
