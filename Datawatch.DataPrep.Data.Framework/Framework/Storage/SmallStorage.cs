﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Datawatch.DataPrep.Data.Framework.Storage
{
    /// <summary>
    /// A small storage for any class or value type.
    /// </summary>
    /// <typeparam name="T">The type of values to store.</typeparam>
    /// <remarks>
    /// <para>This class is implemented as a single array. This means it doesn't
    /// have the extra overhead that comes with the array-of-arrays storage, but
    /// do not make it too large or it will end up on the .NET large object heap
    /// (in other words only use it for capacities smaller than
    /// <see cref="StoragePointer{T}.SegmentSize"/> for class types).</para>
    /// <para>All segments are allocated immediately when a storage is created
    /// as this is more efficient than doing it as they are needed. Storages are
    /// always fixed size and does not grow or shrink dynamically, but the
    /// capacity can be changed on an existing storage.</para>
    /// </remarks>
    public sealed class SmallStorage<T> : StorageBase<T>
    {
        /// <summary>
        /// If <c>true</c>, unused elements (the ones corresponding to holes in
        /// a <see cref="Compact(int[])"/> operation) are moved to the end of
        /// the storage after the compact and reused.
        /// </summary>
        public static readonly bool ReuseOnCompact = true;

        /// <summary>
        /// The minimum capacity for the storage - you cannot create a storage
        /// with smaller capacity than this.
        /// </summary>
        public static readonly int MinCapacity = 1;


        /// <summary>
        /// The actual storage elements as a single array.
        /// </summary>
        private T[] records;

        /// <summary>
        /// The current capacity of the storage.
        /// </summary>
        private int capacity;


        /// <summary>
        /// Creates a new storage and allocates all memory for it.
        /// </summary>
        /// <param name="capacity">The requested capacity as number of
        /// elements (if less than <see cref="MinCapacity"/> that value will be
        /// used instead).</param>
        public SmallStorage(int capacity)
        {
            // Cap capacity to min capacity.
            capacity = Math.Max(capacity, MinCapacity);

            // Allocate the single segment.
            records = new T[capacity];

            this.capacity = capacity;
        }

        /// <summary>
        /// Gets or sets the storage's current capacity as number of elements.
        /// </summary>
        /// <remarks>
        /// <para>The existing elements in the storage will be kept when the
        /// capacity is changed (but possibly truncated).</para>
        /// </remarks>
        public override int Capacity
        {
            get { return capacity; }
            set {
                // Quick exit if capacity is the same.
                if (value == capacity) {
                    return;
                }
                // Cap value to min capacity.
                value = Math.Max(value, MinCapacity);

                // The new storage.
                T[] newRecords = new T[value];
                if (value > capacity) {
                    // Growing.
                    Array.Copy(records, newRecords, capacity);
                } else {
                    // Shrinking (and truncating).
                    Array.Copy(records, newRecords, value);
                }

                this.records = newRecords;
                this.capacity = value;
            }
        }

        /// <summary>
        /// Clears the storage by setting all elements to a single value.
        /// </summary>
        /// <param name="value">The value to clear with.</param>
        public void Clear(T value)
        {
            // Need to use EqualityComparer as there are no restrictions on
            // the T parameter (can be class, struct, or anything).
            if (EqualityComparer<T>.Default.Equals(value, default(T))) {
                // Special case as we can use Array.Clear.
                Array.Clear(records, 0, capacity);
            } else {
                // We need to go through and set all elements.
                for (int i = 0; i < capacity; i++) {
                    records[i] = value;
                }
            }
        }

        /// <summary>
        /// Clears a range of elements in the storage by settings them to
        /// a single value.
        /// </summary>
        /// <param name="index">Index of the first element to clear.</param>
        /// <param name="count">Number of elements to clear.</param>
        /// <param name="value">The value to clear with.</param>
        public void Clear(int index, int count, T value)
        {
            // Need to use EqualityComparer as there are no restrictions on
            // the T parameter (can be class, struct, or anything).
            if (EqualityComparer<T>.Default.Equals(value, default(T))) {
                // Special case as we can use Array.Clear.
                Array.Clear(records, index, count);
            } else {
                // We need to go through and set all elements.
                for (int i = index + count - 1; i >= index; i--) {
                    records[i] = value;
                }
            }
        }

        /// <summary>
        /// Removes a number of elements and moves the remaining elements down
        /// (to lower indices) to fill the holes.
        /// </summary>
        /// <param name="holes">Indices of the elements to remove and an extra
        /// entry at the end for the total number of records (so it points just
        /// past the last used record or hole), must be in ascending
        /// order.</param>
        /// <remarks>
        /// <para>This does not change the storage's capacity.</para>
        /// <para>Depending on <see cref="ReuseOnCompact"/>, the removed values
        /// may be moved to the end of the storage for reuse.</para>
        /// </remarks>
        public override void Compact(int[] holes)
        {
            // Caller cannot ask us to do this for more records that
            // there is capacity for.
            Debug.Assert(holes[holes.Length - 1] <= capacity,
                "More elements to remove than current capacity");

            // TODO: Optimization opportunity.
            // This code can be written without the extra "reuse" array by just
            // swapping elements around (so all that's needed is a "T temp"
            // variable). Observe that this is a permutation with one cycle per
            // hole. So start at each hole (beginning with the first), follow
            // the cycle through back to the hole.

            int holeCount = holes.Length - 1;

            T[] reuse = null;
            if (ReuseOnCompact) {
                // Copy out the removed records.
                reuse = new T[holeCount];
                for (int i = 0; i < holeCount; i++) {
                    reuse[i] = records[holes[i]];
                }
            }

            // Move the live records down to fill the holes.
            for (int i = 0; i < holeCount; i++) {
                // For each run between holes, including the run between the
                // last hole and the end of the records.
                int first = holes[i];
                int last = holes[i + 1];
                // Are the holes consecutive?
                if (first + 1 < last) {
                    // Number of steps to move the records (the same as the
                    // number of holes we have passed).
                    int step = i + 1;
                    Array.Copy(records, first + 1,
                        records, first + 1 - step, last - first - 1);
                }
            }

            if (ReuseOnCompact) {
                // Copy the removed records back in.
                int target = holes[holes.Length - 1] - holeCount;
                Array.Copy(reuse, 0, records, target, holeCount);
            }
        }

        /// <summary>
        /// Gets or sets a specific element value.
        /// </summary>
        /// <param name="record">Index (address) of the element to set.</param>
        /// <returns>The element value.</returns>
        public override T this[int record]
        {
            get { return records[record]; }
            set { records[record] = value; }
        }
    }
}
