using System;
using System.Globalization;

/* 
 * Unix Time:
 * **********
 * Unix Time is the number of seconds that have passed since the Unix Epoch,
 * 1970-1-1 00:00:00. Unix Time is adjusted to leap seconds.
 * 
 * Unix time (also called POSIX time) is defined as the number of seconds since 
 * Jan 1st 1970, 00:00 UTC, but without leap seconds. Although it is called Unix 
 * time , this timekeeping standard is used widely in many other operating systems 
 * and computing devices. Unix time simply counts the number of seconds since the 
 * epoch. A date/time expressed in those seconds is often called a Unix time stamp. 
 * Not supporting leap seconds means that Unix time does not have any way to represent 
 * the leap second in the form of 23:59:60. Instead it just uses the same second 
 * twice. In practical terms, that means that Unix time progressed like this during 
 * the year transition: 1230767999, 1230768000, 1230768000 and 1230768001. The first 
 * 1230768000 represents 2008-12-31 23:59:60 UTC, and the second 1230768000 represents 
 * 2009-01-01 00:00:00 UTC. For applications there is no way to differentiate between 
 * the two distinct times, although many applications will accept both as input.
 * http://derickrethans.nl/leap-seconds-and-what-to-do-with-them.html
 * 
 * DateTime.Ticks
 * The value of this property represents the number of 100-nanosecond intervals that 
 * have elapsed since 12:00:00 midnight, January 1, 0001, which represents 
 * DateTime.MinValue. It does not include the number of ticks that are attributable to 
 * leap seconds.
 * http://msdn.microsoft.com/en-us/library/system.datetime.ticks.aspx
 * 
 * To being able to view a correct UTC time out of a Unix Time number we need to manually 
 * add all the seconds that have passed sinces the Unix Epoch (1970). 
 * 
 * Leap seconds are from time to time added to the UTC Time Standard to keep it's time 
 * close to mean solar time. Without adding leap time UTC time will slowely be more and 
 * more out of synch with solar time.
 * 
 * IERS (International Earth Rotation and Reference Systems Service) publish a 
 * report every 6 month if a leap second need to be added or not, well in advance.
 * Here, the latest report can be checked: 
 * ftp://hpiers.obspm.fr/iers/bul/bulc/bulletinc.dat
 * 
 * Check here to see a table with all so far added leap seconds:
 * http://en.wikipedia.org/wiki/Leap_second
 * 
 * More links:
 * http://en.wikipedia.org/wiki/Unix_Epoch
 * 
 */

namespace Datawatch.DataPrep.Data.Framework
{
    // Create a DateParser for each ColumnDefinition.
    public class DateParser
    {
        public const string Posix = "POSIX";
        public static readonly DateTime UnixEpochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        private string dateFormat;
        private bool isUnixEpoch;
        private CultureInfo cultureInfo;
        private IFormatProvider formatProvider;

        public DateParser(string format)
        {
            DateFormat = format;
            cultureInfo = CultureInfo.InvariantCulture;
        }

        public CultureInfo CultureInfo
        {
            get { return cultureInfo; }
            set
            {
                if (cultureInfo != value)
                {
                    cultureInfo = value;
                }
            }
        }

        public IFormatProvider FormatProvider
        {
            get { return formatProvider; }
            set { formatProvider = value; }
        }

        public string DateFormat
        {
            set
            {
                isUnixEpoch = Posix == value;

                this.dateFormat = value;
            }

            get { return dateFormat; }
        }

        public bool IsPosix
        {
            get { return isUnixEpoch; }
        }

        public DateTime Parse(string date)
        {
            DateTime newDate;
            // Parser versus string format
            if (string.IsNullOrEmpty(dateFormat))
            {
                // No format is set for the ColumnDefinition so do the standard Date parsing
                if (DateTime.TryParse(date, out newDate))
                {
                    return newDate;
                }
                return DateTime.MinValue;
            }
            // Format is set for the ColumnDefinition
            try
            {
                if (isUnixEpoch)
                {
                    double unixEpochDouble = Convert.ToDouble(date);
                    return UnixEpochStart.AddSeconds(unixEpochDouble);
                }
                newDate = DateTime.ParseExact(date, dateFormat, cultureInfo);
                return newDate;
            }
            catch
            {
            }
            return DateTime.MinValue;
        }

        public string ToString(DateTime date)
        {
            try
            {
                if (isUnixEpoch)
                {
                    return Convert.ToString(date.Subtract(UnixEpochStart).TotalSeconds);
                }
                if (formatProvider != null)
                {
                    return date.ToString(dateFormat, formatProvider);
                }
                return date.ToString(dateFormat);
            } catch
            {   
            }
            return Properties.Resources.UiTimeValueEmptyText;
        }
    }
}
