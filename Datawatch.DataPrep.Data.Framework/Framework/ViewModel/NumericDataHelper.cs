﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.ViewModel
{
    public class NumericDataHelper : PropertyBagViewModel
    {
        private char cachedDecimalSeparator;
        static List<char> allowedCharacters = new List<char>() { '-', 'E', '+' };

        public NumericDataHelper(PropertyBag bag)
            : base(bag)
        {
            if (bag == null)
            {
                throw Exceptions.ArgumentNull("bag");
            }

            string decimalSep = GetInternal("DecimalSeparator");
            if (string.IsNullOrEmpty(decimalSep))
            {
                SetInternal("DecimalSeparator",
                    CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator);
            }
            cachedDecimalSeparator = DecimalSeparator;
        }

        public char DecimalSeparator
        {
            get { return GetInternal("DecimalSeparator")[0]; }
            set { 
                SetInternal("DecimalSeparator", value.ToString());
                cachedDecimalSeparator = DecimalSeparator;
            }
        }

        public bool IsNumber(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return false;
            }
            int len = value.Length;
            char[] charArray = new char[len];
            //typically a thousand separator is either a comma or dot depending
            //upon decimalSeparator.
            char typicalSeparator = cachedDecimalSeparator == ',' ? '.' : ',';
            int separatorCount = 0;
            int charCount = 0;
            for (int i = 0; i < len; i++)
            {
                char c = value[i];
                if (Char.IsLetter(c) && !allowedCharacters.Contains(c))
                {
                    return false;
                }

                if (c == cachedDecimalSeparator)
                {
                    separatorCount++;
                    //more than one separator in source means invalid number
                    if (separatorCount > 1)
                    {
                        return false;
                    }

                    charArray[charCount++] = '.';
                    
                }
                else if (c != typicalSeparator && c != ' ' && c != '\'')
                {
                    charArray[charCount++] = c;
                }
            }

            if (charCount == 0)
            {
                return false;
            }

            try
            {
                double.Parse(new String(charArray, 0, charCount),
                    CultureInfo.InvariantCulture);
                return true;
            }
            catch { }

            return false;
        }

        /// <summary>
        /// Tries to Convert string to double by removing all characters except
        /// digits and decimal separator,
        /// returns NaN if source string can't be converted to double even after
        /// stripping out non numeric characters. 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public double ParseNumber(string value)
        {
            double result = double.NaN;
            if(string.IsNullOrEmpty(value))
            {
                return result;
            }

            int len = value.Length;
            char[] charArray = new char[len];
            int separatorCount = 0;
            int charCount = 0;
            for (int i = 0; i < len; i++)
            {
                char c = value[i];

                if (Char.IsDigit(c) || allowedCharacters.Contains(c))
                {
                    charArray[charCount++] = c;
                }
                else if (c == cachedDecimalSeparator)
                {
                    separatorCount++;
                    //more than one separator in source means invalid number
                    if (separatorCount > 1)
                    {
                        return result;
                    }

                    charArray[charCount++] = '.';
                    
                }
            }
            
            if (charCount == 0)
            {
                return result;
            }

            try
            {
                result = double.Parse(new String(charArray, 0, charCount),
                    CultureInfo.InvariantCulture);
            }
            catch { }

            return result;
        }

        public static char[] DecimalSeparators
        {
            get
            {

                return new char[]
                {
                    ',',
                    '.'
                };
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            if (!(obj is NumericDataHelper)) return false;

            NumericDataHelper ndh = (NumericDataHelper)obj;

            return char.Equals(DecimalSeparator, ndh.DecimalSeparator);
        }

        public override int GetHashCode()
        {
            return DecimalSeparator.GetHashCode();
        }
    }
}
