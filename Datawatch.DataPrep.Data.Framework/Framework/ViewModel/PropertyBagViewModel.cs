﻿using System;
using System.ComponentModel;
using System.Xml;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.ViewModel
{
    public class PropertyBagViewModel : ViewModelBase
    {
        protected readonly PropertyBag propertyBag;

        public PropertyBagViewModel(PropertyBag bag)
        {
            propertyBag = bag;
        }

        /// <summary>
        /// Gets a string setting from the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to retrieve.</param>
        /// <returns>The value or null if not present.</returns>
        protected string GetInternal(string name)
        {
            return propertyBag.Values[name];
        }

        /// <summary>
        /// Gets an string setting from the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to retrieve.</param>
        /// <param name="defaultValue">Value to return if the setting is not
        /// present.</param>
        /// <returns>The value or default.</returns>
        protected string GetInternal(string name, string defaultValue)
        {
            string value = propertyBag.Values[name];
            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        protected bool GetInternalBool(string name, bool defaultValue)
        {
            string s = propertyBag.Values[name];
            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    return XmlConvert.ToBoolean(s);
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        protected System.Drawing.Color GetInternalColor(string name)
        {
            return GetInternalColor(name, System.Drawing.Color.Black);
        }

        protected System.Drawing.Color GetInternalColor(string name,
            System.Drawing.Color defaultValue)
        {
            // code copied from VariableHelper.CreateColor().
            string argbStr = GetInternal(name);
            if (argbStr == null || argbStr.Length != 8)
            {
                return defaultValue;
            }

            string substr;

            substr = argbStr.Substring(0, 2);
            int a = Convert.ToInt32(substr, 16);

            substr = argbStr.Substring(2, 2);
            int r = Convert.ToInt32(substr, 16);

            substr = argbStr.Substring(4, 2);
            int g = Convert.ToInt32(substr, 16);

            substr = argbStr.Substring(6, 2);
            int b = Convert.ToInt32(substr, 16);

            System.Drawing.Color c = System.Drawing.Color.FromArgb(a, r, g, b);
            return c;    
        }

        protected double GetInternalDouble(string name, double defaultValue)
        {
            string s = propertyBag.Values[name];
            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    return XmlConvert.ToDouble(s);
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        protected T GetInternalEnum<T>(string name, T defaultValue)
        {
            string s = propertyBag.Values[name];
            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    int index = XmlConvert.ToInt32(s);
                    return (T)Enum.GetValues(typeof(T)).GetValue(index);
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        protected float GetInternalFloat(string name, float defaultValue)
        {
            string s = propertyBag.Values[name];
            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    return XmlConvert.ToSingle(s);
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Gets an integer setting from the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to retrieve.</param>
        /// <param name="defaultValue">Value to return if the setting is not
        /// present or cannot be converted to an int.</param>
        /// <returns>The value or default.</returns>
        protected int GetInternalInt(string name, int defaultValue)
        {
            string s = propertyBag.Values[name];
            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    return XmlConvert.ToInt32(s);
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Sets a string setting in the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to store.</param>
        /// <param name="value">The setting's new value.</param>
        /// <remarks>
        /// <para>This method will fire the <see cref="INotifyPropertyChanged.PropertyChanged"/> event
        /// if the new value is different from any existing value.</para>
        /// </remarks>
        protected void SetInternal(string name, string value)
        {
            if (propertyBag.Values[name] != value)
            {
                propertyBag.Values[name] = value;
                OnPropertyChanged(name);
            }
        }

        protected void SetInternalBool(string name, bool value)
        {
            SetInternal(name, XmlConvert.ToString(value));
        }

        protected void SetInternalColor(string name, System.Drawing.Color value)
        {
            // code copied from VariableHelper.StoreColor().
            string argbStr = string.Empty;
            argbStr += Convert.ToString(value.A, 16).PadLeft(2, '0');
            argbStr += Convert.ToString(value.R, 16).PadLeft(2, '0');
            argbStr += Convert.ToString(value.G, 16).PadLeft(2, '0');
            argbStr += Convert.ToString(value.B, 16).PadLeft(2, '0');
            SetInternal(name, argbStr);
        }

        protected void SetInternalDouble(string name, double value)
        {
            SetInternal(name, XmlConvert.ToString(value));
        }

        protected void SetInternalFloat(string name, float value)
        {
            SetInternal(name, XmlConvert.ToString(value));
        }

        protected void SetInternalEnum(string name, Enum value)
        {
            int index = Array.IndexOf(Enum.GetValues(value.GetType()), value);
            SetInternalInt(name, index);
        }

        /// <summary>
        /// Sets an integer setting in the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to store.</param>
        /// <param name="value">The setting's new value.</param>
        /// <remarks>
        /// <para>This method will fire the <see cref="INotifyPropertyChanged.PropertyChanged"/> event
        /// if the new value is different from any existing value.</para>
        /// </remarks>
        protected void SetInternalInt(string name, int value)
        {
            SetInternal(name, XmlConvert.ToString(value));
        }

        protected void SetInternalSubGroup(string name, PropertyBag bag)
        {
            propertyBag.SubGroups[name] = bag;
        }

        protected PropertyBag GetInternalSubGroup(string name)
        {
            return propertyBag.SubGroups[name];
        }

        /// <summary>
        /// Converts the name of all the values and subgroups to major camel case.
        /// </summary>
        /// <param name="sourceBag">Bag to convert.</param>
        /// <remarks>
        /// Note that this ONLY makes the first character upper case,
        /// casing of ALL the other characters are preserved!
        /// </remarks>
        /// <returns>The new bag with corrected names.</returns>
        public static PropertyBag ConvertNamesToMajorCamelCase(PropertyBag sourceBag)
        {
            if (sourceBag == null)
            {
                return null;
            }
            PropertyBag targetBag = new PropertyBag();

            // convert name
            string name = sourceBag.Name;
            if (name != null)
            {
                targetBag.Name = toMajorCamelCase(name);
            }
            foreach (PropertyValue propertyValue in sourceBag.Values)
            {
                // convert name
                name = toMajorCamelCase(propertyValue.Name);

                // write to new bag
                targetBag.Values[name] = propertyValue.Value;
            }
            foreach (PropertyBag subGroup in sourceBag.SubGroups)
            {
                // recursively handle subgroups (names and values)
                PropertyBag bag = ConvertNamesToMajorCamelCase(subGroup);

                // write to new bag
                targetBag.SubGroups.Add(bag);
            }
            return targetBag;
        }

        private static string toMajorCamelCase(string name)
        {
            if (name == null)
            {
                return null;
            }
            if (name == string.Empty)
            {
                return string.Empty;
            }
            string newName = char.ToUpper(name[0]).ToString();
            if (name.Length > 1)
            {
                newName += name.Substring(1);
            }
            return newName;
        }

        /// <summary>
        /// Converts a single value in the PropertyBag, enums are sometimes
        /// represented as text and sometimes as an indexed integer.
        /// This function converts it to an integer (if possible).
        /// </summary>
        /// <param name="sourceBag">PropertyBag to modify.</param>
        /// <param name="name">
        /// Name of the value to be converted.
        /// If not major camel case, will be made major camel case.
        /// </param>
        public static void ConvertEnumToIndexedInteger<T>(
            PropertyBag sourceBag,
            string name)
        {
            if (sourceBag == null || string.IsNullOrEmpty(name))
            {
                return;
            }
            if (!sourceBag.Values.ContainsKey(name))
            {
                return;
            }

            string value = sourceBag.Values[name];
            sourceBag.Values.Remove(name);
            name = toMajorCamelCase(name);
            int valueAsInt;

            if (int.TryParse(value, out valueAsInt))
            {
                // value is already int, assume propertybag is already updated
                sourceBag.Values[name] = valueAsInt.ToString();
            }
            else
            {
                foreach (object o in Enum.GetValues(typeof(T)))
                {
                    // find enum value with matching name
                    if (!o.ToString().Equals(value))
                    {
                        continue;
                    }
                    // convert to index and write
                    Enum enumValue = (Enum)o;
                    int index = Array.IndexOf(
                        Enum.GetValues(enumValue.GetType()),
                        enumValue);
                    sourceBag.Values[name] = XmlConvert.ToString(index);
                    return;
                }
            }
        }

        protected void RenamePropertyValue(string from, string to)
        {
            string value = GetInternal(from);
            if (!string.IsNullOrEmpty(value))
            {
                SetInternal(to, value);
            }
            propertyBag.Values.Remove(from);
        }
    }
}
