﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.ViewModel
{
    public class PluginHostSettingsViewModel : PropertyBagViewModel
    {
        private const string PluginHostSettings = "PluginHostSettings";

        public PluginHostSettingsViewModel(PropertyBag globalSettings)
            : base(CreateSubGroup(globalSettings))
        {
        }

        private static PropertyBag CreateSubGroup(PropertyBag globalSettings)
        {
            if (globalSettings == null)
            {
                throw Exceptions.ArgumentNull("globalSettings");
            }

            PropertyBag bag = globalSettings.SubGroups[PluginHostSettings];
            if (bag == null)
            {
                bag = new PropertyBag();
                globalSettings.SubGroups[PluginHostSettings] = bag;
            }
            return bag;
        }

        public bool IsParameterSupported
        {
            get { return GetInternalBool("IsParameterSupported", true); }
            set { SetInternalBool("IsParameterSupported", value); }
        }

        public bool IsTimezoneSupported
        {
            get { return GetInternalBool("IsTimezoneSupported", true); }
            set { SetInternalBool("IsTimezoneSupported", value); }
        }

        public bool IsOnDemandSupported
        {
            get { return GetInternalBool("IsOnDemandSupported", true); }
            set { SetInternalBool("IsOnDemandSupported", value); }
        }

        public bool IsAggregateSupported
        {
            get { return GetInternalBool("IsAggregateSupported", true); }
            set { SetInternalBool("IsAggregateSupported", value); }
        }

        public bool IsFreeformSQLSupported
        {
            get { return GetInternalBool("IsFreeformSQLSupported", true); }
            set { SetInternalBool("IsFreeformSQLSupported", value); }
        }

        public bool IsHelpButtonVisibile
        {
            get { return GetInternalBool("IsHelpButtonVisibile", true); }
            set { SetInternalBool("IsHelpButtonVisibile", value); }
        }
    }
}
