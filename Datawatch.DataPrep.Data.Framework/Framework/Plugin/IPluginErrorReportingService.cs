﻿namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    /// <summary>
    /// Error reporting service interface for plugins.
    /// </summary>
    public interface IPluginErrorReportingService
    {
        /// <summary>
        /// Reports an error with the specified error category.
        /// </summary>
        /// <param name="errorCategory">The error category.</param>
        /// <param name="errorMessage">The error message.</param>
        void Report(string errorCategory, string errorMessage);

        /// <summary>
        /// Reports a formatted error with the specified error category.
        /// </summary>
        /// <param name="errorCategory">The error category.</param>
        /// <param name="errorMessageFormat">The error message format.</param>
        /// <param name="errorMessageArgs">The error message arguments.</param>
        void Report(string errorCategory, string errorMessageFormat, params object[] errorMessageArgs);
    }
}
