﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IRealtimeDataPlugin : IDataPlugin
    {
        event EventHandler<RealtimeDataPluginEventArgs> Updating;
        event EventHandler<RealtimeDataPluginEventArgs> Updated;
        event EventHandler<RealtimeStatusEventArgs> StreamingStatusChanged;

        bool IsRealtime(PropertyBag settings);
        void StartRealtimeUpdates(ITable table, PropertyBag settings);
        void StopRealtimeUpdates(ITable table, PropertyBag settings);        
    }
}
