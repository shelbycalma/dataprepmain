﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    /// <summary>
    /// Base interface for all plugins.
    /// </summary>
    public interface IPlugin : IDisposable
    {
        /// <summary>
        /// Gets the plug-in identifier.
        /// </summary>
        /// <remarks>
        /// <para>The identifier needs to be unique among all plugins of
        /// the same type.</para>
        /// </remarks>
        string Id { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IPlugin"/> instance
        /// is licensed.
        /// </summary>
        bool IsLicensed { get; }
        
        /// <summary>
        /// Gets a value indicating whether this <see cref="IPlugin"/> should be
        /// choosable when creating new plugins or just just kept for backwards
        /// compatibility.
        /// </summary>
        bool IsObsolete { get; }

        /// <summary>
        /// Gets a <see cref="PropertyBag"/> containing the settings for the 
        /// <see cref="IPlugin"/>.
        /// </summary>
        PropertyBag Settings { get; }

        /// <summary>
        /// Gets a string representing the plug-in title.
        /// </summary>
        string Title { get; }
        
    }
}
