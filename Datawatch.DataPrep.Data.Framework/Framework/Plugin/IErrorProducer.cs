﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IErrorProducer
    {
        Exception Error { get; set; }
    }
}
