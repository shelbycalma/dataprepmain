﻿using System;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public class RealtimeDataPluginEventArgs : EventArgs
    {
        public readonly ITable Table;
        public bool IsCancelled { get; set; }

        public RealtimeDataPluginEventArgs(ITable table)
        {
            this.Table = table;
        }
    }
}
