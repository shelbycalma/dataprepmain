﻿using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IQueryBuilderTableClient : IQueryBuilderClient
    {
        bool CanLoadTables { get; }
        SchemaAndTable[] Tables { get; }
    }
}
