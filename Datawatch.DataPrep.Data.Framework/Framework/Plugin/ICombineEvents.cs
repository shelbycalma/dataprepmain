﻿namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface ICombineEvents<E>
    {
        bool DoCombineEvents { get; }
        E Combine(E event1, E event2);
    }
}
