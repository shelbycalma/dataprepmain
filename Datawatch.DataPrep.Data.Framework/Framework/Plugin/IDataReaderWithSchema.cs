﻿using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IDataReaderWithSchema : IDataReader
    {
        ITable SchemaTable { get; }
    }
}