﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Properties;
using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public static class ErrorMessages
    {
        public static string GetDisplayMessage(string errorEncountered)
        {
            string displaymessage = errorEncountered;
            
            if ((errorEncountered != null && errorEncountered.Length > 0))
            {
                foreach (Errors errmsg in Enum.GetValues(typeof(Errors)))
                {
                    Regex reg = new Regex(errmsg.GetAttributeOfType<DescriptionAttribute>().Description);
                    Match match = reg.Match(errorEncountered.ToUpper());
                    if (match.Success)
                    {
                        displaymessage = Properties.Resources.ResourceManager.GetString(errmsg.GetAttributeOfType<EnumMemberAttribute>().Value.ToString());
                        break;
                    }
                }
            }
            
            return displaymessage;          

        }
        private enum Errors
        {
            [Description("ACCESS DENIED - INSUFFICIENT PERMISSION")]
            [EnumMember(Value = "ExAccessDenied")]
            ExAccessDenied,
            [Description("NO CONNECT PERMISSION")]
            [EnumMember(Value = "ExNoConnectPermission")]
            ExNoConnectPermission,
            [Description("COULDN'T AUTHENTICATE YOU")]
            [EnumMember(Value = "ExCannotAuthenticate")]
            ExCannotAuthenticate,
            [Description("CANNOT EXTRACT REQUIRED INFORMATION FROM IDCRL HEADER")]
            [EnumMember(Value = "ExCannotExtractInformation")]
            ExCannotExtractInformation,
            [Description("CANNOT LOAD TRUST STORE")]
            [EnumMember(Value = "ExCannotLoadTrustStore")]
            ExCannotLoadTrustStore,
            [Description("COLUMN.*.DOES NOT EXIST")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName1,
            [Description("COULDN'T RESOLVE COLUMN REFERENCE")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName2,
            [Description("UNRECOGNIZED COLUMN")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName3,
            [Description("UNKNOWN COLUMN")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName4,
            [Description("INVALID COLUMN NAME")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName5,
            [Description("NO FIELD FOUND WITH THE NAME")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName6,
            [Description(".*.NOT COLUMN OF INSERTED/UPDATED TABLE")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName7,
            [Description("COLUMN.*.NOT FOUND")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName8,
            [Description("CANNOT RESOLVE.*.GIVEN INPUT COLUMNS")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName9,
            [Description("INVALID TABLE ALIAS OR COLUMN REFERENCE")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName10,
            [Description("ORA.*.INVALID IDENTIFIER")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName11,
            [Description("400:UNRECOGNIZED NAME:.*.")]
            [EnumMember(Value = "ExInvalidColumnName")]
            ExInvalidColumnName12,
            [Description("CONNECTION REFUSED")]
            [EnumMember(Value = "ExConnectionRefused")]
            ExConnectionRefused1,
            [Description(".*.MACHINE ACTIVELY REFUSED IT.*.")]
            [EnumMember(Value = "ExConnectionRefused")]
            ExConnectionRefused2,
            [Description("USER_ERROR.*.COULD NOT DETERMINE CUSTOMER COMPID")]
            [EnumMember(Value = "ExInvalidCustomerCompID")]
            ExInvalidCustomerCompID,
            [Description("SCHEMA.*.NOT EXIST")]
            [EnumMember(Value = "ExInvalidSchemaName")]
            ExInvalidSchemaName,
            [Description(".NOT A VALID DATABASE NAME")]
            [EnumMember(Value = "ExInvalidDatabaseName")]
            ExInvalidDatabaseName1,
            [Description("DATABASE.*.NOT EXIST")]
            [EnumMember(Value = "ExInvalidDatabaseName")]
            ExInvalidDatabaseName2,
            [Description("DATABASE NOT FOUND")]
            [EnumMember(Value = "ExInvalidDatabaseName")]
            ExInvalidDatabaseName3,
            [Description(".*.CANNOT OPEN DATABASE.*.")]
            [EnumMember(Value = "ExInvalidDatabaseName")]
            ExInvalidDatabaseName4,
            [Description("UNKNOWN DATABASE.*.")]
            [EnumMember(Value = "ExInvalidDatabaseName")]
            ExInvalidDatabaseName5,
            [Description("HTTP PROTOCOL ERROR. 400 BAD REQUEST.*.AUTHENTICATIONERROR.CUSTOMER_NOT_FOUND")]
            [EnumMember(Value = "ExHTTPProtocolError400Auth")]
            ExHTTPProtocolError400Auth,
            [Description("HTTP PROTOCOL ERROR. 400 BAD REQUEST.*.QUOTACHECKERROR.INVALID_TOKEN_HEADER")]
            [EnumMember(Value = "ExHTTPProtocolError400QuotaCheck")]
            ExHTTPProtocolError400QuotaCheck,
            [Description("HTTP PROTOCOL ERROR. 400 BAD REQUEST.*.")]
            [EnumMember(Value = "ExHTTPProtocolError400")]
            ExHTTPProtocolError400,
            [Description("HTTP PROTOCOL ERROR.*.401 UNAUTHORIZED.*.")]
            [EnumMember(Value = "ExHTTPProtocolError401")]
            ExHTTPProtocolError401,
            [Description("HTTP PROTOCOL ERROR.*.403 FORBIDDEN.*.")]
            [EnumMember(Value = "ExHTTPProtocolError403")]
            ExHTTPProtocolError403,
            [Description("HTTP PROTOCOL ERROR.*.404 NOT FOUND.*.")]
            [EnumMember(Value = "ExHTTPProtocolError404")]
            ExHTTPProtocolError404,
            [Description(".*.QUERY FAILED TO EXECUTE.*.")]
            [EnumMember(Value = "ExFailedToExecuteQuery")]
            ExFailedToExecuteQuery,
            [Description("ERROR PARSING DOUBLE VALUE")]
            [EnumMember(Value = "ExErrorParsingDouble")]
            ExErrorParsingDouble,
            [Description("SELECTED DIMENSIONS AND METRICS CANNOT BE QUERIED TOGETHER")]
            [EnumMember(Value = "ExGARequestError")]
            ExGARequestError1,
            [Description(".*.INVALID DIMENSIONS OR METRICS.*.")]
            [EnumMember(Value = "ExGARequestError")]
            ExGARequestError2,
            [Description("INVALID MARKUP. EXPECTED XML OR JSON, BUT INSTEAD FOUND")]
            [EnumMember(Value = "ExOdataAuthenticationFailed")]
            ExOdataAuthenticationFailed,
            [Description("FAILED TO AUTHENTICATE WITH SUGAR CRM:.*.")]
            [EnumMember(Value = "ExSugarCRMFailed")]
            ExSugarCRMFailed1,
            [Description("INVALID JSON RESPONSE. PLEASE CHECK THAT YOUR SUGARCRM VERSION IS")]
            [EnumMember(Value = "ExSugarCRMFailed")]
            ExSugarCRMFailed2,
            [Description(".*.THE RESOURCE YOU TRIED TO ACCESS WAS NOT FOUND")]
            [EnumMember(Value = "ExZendeskAuthenticationFailed")]
            ExZendeskAuthenticationFailed,
            [Description("[DATAWATCH][ODBC APACHE HIVE WIRE PROTOCOL DRIVER]GENERAL ERROR.DRIVER WAS UNABLE TO CONNECT TO THE SERVER. PLEASE CHECK YOUR HIVE SERVER IS RUNNING AND THE HOST/PORT ARE CONFIGURED CORRECTLY.")]
            [EnumMember(Value = "ExHiveGeneralError")]
            ExHiveGeneralError,
            [Description("INSUFFICIENT INFORMATION TO CONNECT TO THE DATA SOURCE")]
            [EnumMember(Value = "ExInsufficientInformation")]
            ExInsufficientInformation,
            [Description("404.*.NOT FOUND: DATASET.*.")]
            [EnumMember(Value = "ExInvalidDataSetID")]
            ExInvalidDataSetID,
            [Description("404.*.NOT FOUND: PROJECT.*.")]
            [EnumMember(Value = "ExInvalidProjectID")]
            ExInvalidProjectID1,
            [Description("400.*.INVALID PROJECT.*.")]
            [EnumMember(Value = "ExInvalidProjectID")]
            ExInvalidProjectID2,
            [Description("THE URL MUST BEGIN WITH HTTP:// OR HTTPS://")]
            [EnumMember(Value = "ExInvalidURL")]
            ExInvalidURL1,
            [Description("ERROR.*.SYSTEM ERROR: NO SUCH HOST IS KNOWN")]
            [EnumMember(Value = "ExInvalidURL")]
            ExInvalidURL2,
            [Description("LIST DOES NOT EXIST")]
            [EnumMember(Value = "ExListDoesNotExist")]
            ExListDoesNotExist,
            [Description(".*.LOGIN FAILED")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed1,
            [Description(".*.USER.*.PASSWORD INVALID")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed2,
            [Description(".*.INVALID USERNAME.*.PASSWORD")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed3,
            [Description(".*.INCORRECT PASSWORD OR USER.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed4,
            [Description("ACCESS DENIED FOR USER.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed5,
            [Description(".*.YOU MUST SPECIFY A VALID USERNAME AND PASSWORD")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed6,
            [Description(".*.ERROR AUTHENTICATING USER.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed7,
            [Description("YOU HAVE ENTERED AN INVALID EMAIL ADDRESS OR PASSWORD.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed8,
            [Description(".*.NEED_LOGIN.*.SPECIFY VALID USERNAME AND PASSWORD.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed9,
            [Description(".*.NOT A VALID EMAIL OR USER ID.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed10,
            [Description(".*.INVALID USERNAME, PASSWORD, SECURITY TOKEN.*.")]
            [EnumMember(Value = "ExLoginFailed")]
            ExLoginFailed11,
            [Description(".*.IT WILL PRODUCE A CARTESIAN PRODUCT")]
            [EnumMember(Value = "ExCartesianProduct")]
            ExCartesianProduct,
            [Description("YOUR ROLE DOES NOT GIVE YOU PERMISSION TO VIEW THIS PAGE")]
            [EnumMember(Value = "ExNetSuiteInvalidRole")]
            ExNetSuiteInvalidRole,
            [Description("UNABLE TO DYNAMICALLY LOAD CLIENT DLL.*.")]
            [EnumMember(Value = "ExGSSCError")]
            ExGSSCError,
            [Description("UNSPECIFIED ERROR AT THE GSS LAYER")]
            [EnumMember(Value = "ExGSSLayerError")]
            ExGSSLayerError,
            [Description(".*.MALFORMED SQL STATEMENT.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt1,
            [Description(".*.CONTAINS ILLEGAL CHARACTER.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt2,
            [Description(".*.STRING CONSTANT BEGINNING.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt3,
            [Description("ERROR WHILE COMPILING STATEMENT: FAILED: PARSEEXCEPTION.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt4,
            [Description("YOU HAVE AN ERROR IN YOUR SQL SYNTAX.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt5,
            [Description(".*.FROM KEYWORD NOT FOUND WHERE EXPECTED")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt6,
            [Description("MISSING EXPRESSION")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt7,
            [Description(".*.UNTERMINATED QUOTED IDENTIFIER AT OR NEAR.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt8,
            [Description(".*.UNTERMINATED QUOTED STRING AT.*.")]
            [EnumMember(Value = "ExMalformedSQLStmt")]
            ExMalformedSQLStmt9,
            [Description("MUST SPECIFY A FILE, FOLDER, OR TEAM DRIVE ID WHEN CALLING THIS METHOD")]
            [EnumMember(Value = "ExGoogleDrivePermission")]
            ExGoogleDrivePermission,
            [Description("NUMBER TOO LARGE.*.")]
            [EnumMember(Value = "ExNumberTooLarge")]
            ExNumberTooLarge,
            [Description("PERMISSION VIOLATION:")]
            [EnumMember(Value = "ExPermissionViolation")]
            ExPermissionViolation,
            [Description("QUOTA EXCEEDED FOR QUOTA GROUP")]
            [EnumMember(Value = "ExQuotaExceeded")]
            ExQuotaExceeded,
            [Description("SOCKET CLOSED")]
            [EnumMember(Value = "ExSocketClosed")]
            ExSocketClosed,
            [Description("SSL IS REQUIRED, BUT WAS NOT REQUESTED")]
            [EnumMember(Value = "ExSSLRequired")]
            ExSSLRequired,
            [Description("SYNTAX ERROR")]
            [EnumMember(Value = "ExSyntaxError")]
            ExSyntaxError1,
            [Description(".*.ORA-00972: IDENTIFIER IS TOO LONG")]
            [EnumMember(Value = "ExSyntaxError")]
            ExSyntaxError2,
            [Description(".*.ORA-01740: MISSING DOUBLE QUOTE IN IDENTIFIER")]
            [EnumMember(Value = "ExSyntaxError")]
            ExSyntaxError3,
            [Description("INCORRECT SYNTAX NEAR")]
            [EnumMember(Value = "ExSyntaxError")]
            ExSyntaxError4,
            [Description(".*.INVALID CONNECTION DATA.*.")]
            [EnumMember(Value = "ExInvalidConnectionData")]
            ExInvalidConnectionData,
            [Description("INFORMIXSERVER DOES NOT MATCH EITHER DBSERVERNAME OR DBSERVERALIASES")]
            [EnumMember(Value = "ExInformixInvalidServer")]
            ExInformixInvalidServer,
            [Description("THE SPECIFIED TABLE.*.IS NOT IN THE DATABASE")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist1,
            [Description("A TABLE COULD NOT BE FOUND THAT MATCHES.*.")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist2,
            [Description("TABLE.*.DOES NOT EXIST")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist3,
            [Description(".*.TABLE.*.NOT FOUND")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist4,
            [Description("TABLE.*.DOESN'T EXIST.*.")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist5,
            [Description("UNABLE TO RETRIEVE COLUMNS FOR TABLE.*.")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist6,
            [Description(".*.IS AN UNDEFINED NAME.*.")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist7,
            [Description("GET TABLE SCHEMA FAILED.*.404.*.NOT FOUND: TABLE.*.")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist8,
            [Description("INVALID OBJECT NAME.*.")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist9,
            [Description("RELATION.*.DOES NOT EXIST")]
            [EnumMember(Value = "ExTableViewDoesNotExist")]
            ExTableDoesNotExist10,
            [Description("THE CONNECTION FAILED. NO CLUSTER NODE IS AVAILABLE")]
            [EnumMember(Value = "ExConnectionFailedNoClusterNode")]
            ExConnectionFailedNoClusterNode,
            [Description("THE DRIVER REQUIRES A USER PROMPT FOR OAUTH AUTHENTICATION WHICH IS NOT ALLOWED BY THIS APPLICATION")]
            [EnumMember(Value = "ExRequiresOAuth")]
            ExRequiresOAuth,
            [Description("403.*.ACCESS DENIED: DATASET.*.THE USER.*.DOES NOT HAVE BIGQUERY.TABLES.LIST PERMISSION FOR DATASET")]
            [EnumMember(Value = "ExBigQueryAccessDenied")]
            ExBigQueryAccessDenied,
            [Description("THIS OAUTH-TOKEN.*.DOES NOT HAVE PROPER PERMISSIONS!")]
            [EnumMember(Value = "ExHubSpotPermissionError")]
            ExHubSpotPermissionError1,
            [Description("THE AUTHORIZED TOKEN DOES NOT HAVE PROPER PERMISSIONS!.*.")]
            [EnumMember(Value = "ExHubSpotPermissionError")]
            ExHubSpotPermissionError2,
            [Description(".*.TNS:LISTENER COULD NOT RESOLVE.*.GIVEN IN CONNECT DESCRIPTOR")]
            [EnumMember(Value = "ExListenerNotResolvedSID")]
            ExListenerNotResolvedSID,
            [Description("TO QUERY THIS TABLE YOU HAVE TO SPECIFY A PRIMARY KEY OR AN ITEMID")]
            [EnumMember(Value = "ExNoPrimaryKeySpecified")]
            ExNoPrimaryKeySpecified,
            [Description("TOO MANY REQUESTS SPREADSHEETID:")]
            [EnumMember(Value = "ExTooManyRequest")]
            ExTooManyRequest,
            [Description("UNEXPECTED NETWORK ERROR")]
            [EnumMember(Value = "ExUnexpectedNetworkError")]
            ExUnexpectedNetworkError,
            [Description("UNRECOGNIZED COLUMN")]
            [EnumMember(Value = "ExUnrecognizedColumn")]
            ExUnrecognizedColumn,
            [Description("UNRECOGNIZED RESPONSE WHEN GETTING SHAREPOINT ONLINE SECURITY COOKIES, PLEASE CHECK YOUR CREDENTIALS")]
            [EnumMember(Value = "ExWrongCredential")]
            ExWrongCredential,            
            [Description("SECURITY SERVICES ERROR: NO CREDENTIALS WERE FOUND FOR THE SPECIFIED NAME")]
            [EnumMember(Value = "ExSecurityServicesError")]
            ExSecurityServicesError,
            [Description("WINDOWS LOGINS ARE NOT SUPPORTED IN THIS VERSION OF SQL SERVER")]
            [EnumMember(Value = "ExVersionNotSupported")]
            ExVersionNotSupported,
            [Description("OPTIONAL FEATURE NOT IMPLEMENTED")]
            [EnumMember(Value = "ExOptionalFeatureNotImplemented")]
            ExOptionalFeatureNotImplemented,
            [Description("405.*.METHOD NOT ALLOWED")]
            [EnumMember(Value = "ExBoxMethodNotAllowed")]
            ExBoxMethodNotAllowed,
            [Description("404.*.AUTHORIZATION FAILED")]
            [EnumMember(Value = "ExBoxAuthFailed")]
            ExBoxAuthFailed,
            [Description("YOU ARE NOT AUTHORIZED FOR THIS OPERATION")]
            [EnumMember(Value = "ExOperationNotAuthorized")]
            ExOperationNotAuthorized,
            [Description("INVALID JSON MARKUP. EXPECTED JSON, BUT INSTEAD FOUND")]
            [EnumMember(Value = "ExJiraAuthenticationFailed")]
            ExJiraAuthenticationFailed,
            [Description("OBJECT REFERENCE NOT SET TO AN INSTANCE OF AN OBJECT")]
            [EnumMember(Value = "ExObjectReference")]
            ExObjectReference,
            [Description(".*.TIMEOUT.*.")]
            [EnumMember(Value = "ExTimeoutError")]
            ExTimeoutError1,
            [Description("CONNECTION TIMED OUT")]
            [EnumMember(Value = "ExTimeoutError")]
            ExTimeoutError2,
            [Description("PARAMETER VALIDATION FAILED FOR \"METRICS\"")]
            [EnumMember(Value = "ExGAInvalidMetrics")]
            ExGAInvalidMetrics,
            [Description("ERROR PARSING CONNECT STRING AT OFFSET.*.")]
            [EnumMember(Value = "ExErrorParsingConnString")]
            ExErrorParsingConnString,
            [Description("RESOURCE NOT FOUND FOR THE SEGMENT.*.")]
            [EnumMember(Value = "ExResourceNotFound")]
            ExResourceNotFound,
            [Description("ERROR PERFORMING OPERATION.*.")]
            [EnumMember(Value = "ExErrorPerformingOperation")]
            ExErrorPerformingOperation,
            [Description("AT LEAST ISSUEKEY, OR ISSUEID SHOULD BE SPECIFIED IN THIS SQL STATEMENT.")]
            [EnumMember(Value = "ExJiraIssueKeyRequired")]
            ExJiraIssueKeyRequired,
            [Description("OPERATION NOT ALLOWED ON SCHEMA")]
            [EnumMember(Value = "ExOperationNotAllowed")]
            ExOperationNotAllowed,
            [Description("YOU HAVE TO SPECIFY AN ID, FOLDERID OR A GROUPID")]
            [EnumMember(Value = "ExBoxQueryCollab")]
            ExBoxQueryCollab,
            [Description("YOU HAVE TO SPECIFY A PRIMARY KEY OR A FILEID")]
            [EnumMember(Value = "ExBoxQueryComments")]
            ExBoxQueryComments,
            [Description("YOU HAVE TO SPECIFY A PRIMARY KEY, USERID OR GROUPID")]
            [EnumMember(Value = "ExBoxMemberships")]
            ExBoxMemberships,
            [Description("YOU HAVE TO SPECIFY A SHAREDLINK")]
            [EnumMember(Value = "ExBoxSharedItems")]
            ExBoxSharedItems,
            [Description("YOU HAVE TO SPECIFY A PRIMARY KEY OR A TASKID")]
            [EnumMember(Value = "ExBoxTaskAssignments")]
            ExBoxTaskAssignments,
            [Description("ANNOUNCEMENT REQUIRES A FILTER BY AN ID, FEEDITEMID OR PARENTID USING THE EQUALS OPERATOR")]
            [EnumMember(Value = "ExSalesforceAnnouncement")]
            ExSalesforceAnnouncement,
            [Description("EXCEEDED 100000 DISTINCT IDS")]
            [EnumMember(Value = "ExSalesforceAttachment")]
            ExSalesforceAttachment,
            [Description("CONTENTDOCUMENTLINK REQUIRES A FILTER BY A SINGLE ID ON CONTENTDOCUMENTID OR LINKEDENTITYID USING THE EQUALS OPERATOR")]
            [EnumMember(Value = "ExSalesforceContentDocumentLink")]
            ExSalesforceContentDocumentLink,
            [Description("YOUR ORGANIZATION DOESN'T HAVE PERMISSION TO ACCESS THE DATA.COM API")]
            [EnumMember(Value = "ExSalesforceDatacloudContact")]
            ExSalesforceDatacloudContact,
            [Description("DATACLOUD D&B COMPANY IS NOT FILTERABLE WITHOUT A CRITERIA")]
            [EnumMember(Value = "ExSalesforceDatacloudDandBCompany")]
            ExSalesforceDatacloudDandBCompany,
            [Description("DIRECTLY QUERYING FEEDCOMMENT IS ONLY SUPPORTED FOR ADMIN USERS")]
            [EnumMember(Value = "ExSalesforceFeedComment")]
            ExSalesforceFeedComment,
            [Description("FEEDITEM REQUIRES A FILTER BY ID")]
            [EnumMember(Value = "ExSalesforceFeedItem")]
            ExSalesforceFeedItem,
            [Description("USER DOESN'T HAVE ACCESS TO FORECASTING OBJECTS")]
            [EnumMember(Value = "ExSalesforceForecastingQuota")]
            ExSalesforceForecastingQuota,
            [Description("WHEN QUERYING THE VOTE OBJECT, YOU MUST FILTER USING THE FOLLOWING SYNTAX")]
            [EnumMember(Value = "ExSalesforceVote")]
            ExSalesforceVote,
            [Description("CAN SELECT ONLY RECORDID.*.MAXACCESSLEVEL")]
            [EnumMember(Value = "ExSalesforceUserRecordAccess")]
            ExSalesforceUserRecordAccess,
            [Description("BOTH A LIST AND AN ITEMID MUST BE SPECIFIED TO RETURN ATTACHMENTS")]
            [EnumMember(Value = "ExSharePointAttachments")]
            ExSharePointAttachments,
            [Description("A LIBRARY MUST BE SPECIFIED WHEN SELECTING AVAILABLE FILE VERSIONS FROM FILEVERSIONS")]
            [EnumMember(Value = "ExSharePointFileVersions")]
            ExSharePointFileVersions,
            [Description("THE ATTRIBUTE LIST IS REQUIRED BY GETVALIDTERMS.RSD")]
            [EnumMember(Value = "ExSharePointGetValidTerms")]
            ExSharePointGetValidTerms,
            [Description("OBJECTTYPE VALUE MUST BE ONE OF WEB.*.LIST")]
            [EnumMember(Value = "ExSharePointPermissions")]
            ExSharePointPermissions,
            [Description("CANNOT COMPLETE THIS ACTION")]
            [EnumMember(Value = "ExSharePointTimeCard")]
            ExSharePointTimeCard,
            [Description("PLEASE SPECIFY THE LIST YOU ARE REQUESTING VIEWS FOR IN YOUR QUERY")]
            [EnumMember(Value = "ExSharePointViews")]
            ExSharePointViews,
            [Description("INVALID JSON MARKUP")]
            [EnumMember(Value = "ExInvalidJSON")]
            ExInvalidJSON,
            [Description("RESPONSE FROM GOOGLE ADWORDS IS NULL OR EMPTY")]
            [EnumMember(Value = "ExGoogleAdWordsNull")]
            ExGoogleAdWordsNull,
            [Description("UNABLE TO LOAD DATA")]
            [EnumMember(Value = "ExUnableToLoadData")]
            ExUnableToLoadData,
            [Description("EITHER AN UNKNOWN TYPE OR IS NOT SUPPORTED")]
            [EnumMember(Value = "ExInformixUnknownType")]
            ExInformixUnknownType,
            [Description("-9272")]
            [EnumMember(Value = "ExInformix9272")]
            ExInformix9272,
            [Description("YOU DO NOT HAVE THE JIRA ADMINISTRATOR PERMISSION REQUIRED TO GET AUDITING RECORDS")]
            [EnumMember(Value = "ExJiraAudit")]
            ExJiraAudit1,
            [Description("ERROR.*.HY000.*.YOU DO NOT HAVE THE JIRA ADMINISTRATOR PERMISSION REQUIRED TO GET AUDITING RECORDS")]
            [EnumMember(Value = "ExJiraAudit")]
            ExJiraAudit2,
            [Description("YOU NEED TO BE A JIRA ADMINISTRATOR")]
            [EnumMember(Value = "ExJiraAdministrator")]
            ExJiraAdministrator1,
            [Description("ERROR.*.HY000.*.YOU NEED TO BE A JIRA ADMINISTRATOR TO PERFORM THIS OPERATION.")]
            [EnumMember(Value = "ExJiraAdministrator")]
            ExJiraAdministrator2,
            [Description("MARKETING AUTOMATION.*.FEATURE IS NOT ENABLED")]
            [EnumMember(Value = "ExNetSuiteMarketingAutomation")]
            ExNetSuiteMarketingAutomation,
            [Description("SUBSCRIPTION CATEGORIES.*.FEATURE IS NOT ENABLED")]
            [EnumMember(Value = "ExNetSuiteSubscription")]
            ExNetSuiteSubscription,
            [Description("FORBIDDEN")]
            [EnumMember(Value = "ExForbidden")]
            ExForbidden,
            [Description("YOU DO NOT HAVE ACCESS TO THIS PAGE.*.CONTACT THE ACCOUNT OWNER OF THIS HELP DESK")]
            [EnumMember(Value = "ExZendeskNoAccess")]
            ExZendeskNoAccess,
            [Description(".*.THE ''MARKETING AUTOMATION'' FEATURE IS NOT ENABLED IN YOUR NETSUITE ACCOUNT.")]
            [EnumMember(Value = "ExNetSuiteMaketingAutomation")]
            ExNetSuiteMaketingAutomation,
            [Description(".*.THE ''SUBSCRIPTION CATEGORIES'' FEATURE IS NOT ENABLED IN YOUR NETSUITE ACCOUNT.")]
            [EnumMember(Value = "ExNetSuiteSubscriptionCategories")]
            ExNetSuiteSubscriptionCategories,
            [Description(".*.BOTH A SEARCHTYPE AND A SAVEDSEARCHID MUST BE SPECIFIED TO EXECUTE A SAVED SEARCH.")]
            [EnumMember(Value = "ExNetSuiteSpecifySavedSearch")]
            ExNetSuiteSpecifySavedSearch,
            [Description("AT LEAST ISSUEKEY, OR ISSUEID SHOULD BE SPECIFIED IN THIS SQL STATEMENT.")]
            [EnumMember(Value = "ExJiraSpecifyIssueKey")]
            ExJiraSpecifyIssueKey,
            [Description("UNKNOWN SQL TYPE")]
            [EnumMember(Value = "ExUnknownSQLType")]
            ExUnknownSQLType,
            [Description("DOES NOT HAVE PRIVILEGE TO PERFORM OPERATION")]
            [EnumMember(Value = "ExDB2NoPrivilege")]
            ExDB2NoPrivilege,
            [Description("SQLCODE -1092")]
            [EnumMember(Value = "ExSQLCode1092")]
            ExSQLCode1092,
            [Description("ERROR OCCURRED EXECUTING FUNCTION OR PROCEDURE")]
            [EnumMember(Value = "ExFunctionExecution")]
            ExFunctionExecution,
            [Description("CONNECTION ATTEMPT FAILED.*.COULD NOT TRANSLATE HOST")]
            [EnumMember(Value = "ExUnknownHost")]
            ExUnknownHost
        }
        private static T GetAttributeOfType<T>(this Enum enumVal) where T : System.Attribute
        {
            var type = enumVal.GetType();
            var memInfo = type.GetMember(enumVal.ToString());
            var attributes = memInfo[0].GetCustomAttributes(typeof(T), false);
            return (attributes.Length > 0) ? (T)attributes[0] : null;
        }

        public static string GetEnumDescription(string field)
        {
            StringBuilder stringBuilder = new StringBuilder();
            String fieldName = "";
            String description = "";
            foreach (Model.FilterOperator filterOperatorEnum in Enum.GetValues(typeof(Model.FilterOperator)))
            {
                fieldName = Enum.GetName(typeof(Model.FilterOperator), filterOperatorEnum);
                if (fieldName == field)
                    description = (filterOperatorEnum.GetAttributeOfType<DescriptionAttribute>().Description);
            }
            return Resources.ResourceManager.GetString(description);
        }
    }
}
