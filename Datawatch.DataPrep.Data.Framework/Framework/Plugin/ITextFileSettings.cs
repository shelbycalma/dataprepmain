﻿using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface ITextFileSettings 
    {
        ParserSettings ParserSettings { get; }
        int DataDiscoveryRowCount { get; set; }
        NumericDataHelper NumericDataHelper { get; }
        int SkipRows { get; set; }
    }
}
