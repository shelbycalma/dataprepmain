﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    /// <summary>
    /// Contains information about a specific plugin loading failure.
    /// </summary>
    public class PluginLoadingFailureInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PluginLoadingFailureInfo"/> class.
        /// </summary>
        protected internal PluginLoadingFailureInfo()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginLoadingFailureInfo"/> class.
        /// </summary>
        /// <param name="plugin">The plugin instance.</param>
        /// <param name="message">The error message.</param>
        /// <param name="assemblyPath">The plugin assembly path.</param>
        /// <exception cref="System.ArgumentNullException">plugin</exception>
        public PluginLoadingFailureInfo(
            IDataPlugin plugin, string message, string assemblyPath)
        {
            if (plugin == null)
                throw new ArgumentNullException("plugin");

            this.PluginId = plugin.Id;
            this.PluginTitle = plugin.Title;
            this.PluginDataType = plugin.DataPluginType;
            this.IsObsolete = plugin.IsObsolete;
            this.Message = message;
            this.AssemblyPath = assemblyPath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginLoadingFailureInfo"/> class.
        /// </summary>
        /// <param name="attribute">The instnace of <see cref="PluginDescriptionAttribute"/>.</param>
        /// <param name="message">The error message.</param>
        /// <param name="assemblyPath">The plugin assembly path.</param>
        /// <exception cref="System.ArgumentNullException">attribute</exception>
        public PluginLoadingFailureInfo(
            PluginDescriptionAttribute attribute, 
            string message, 
            string assemblyPath)
        {
            if (attribute == null)
                throw new ArgumentNullException("attribute");

            this.PluginId = attribute.PluginId;
            this.PluginTitle = attribute.Name;
            this.PluginDataType = attribute.Type;
            this.IsObsolete = attribute.IsObsolete;
            this.Message = message;
            this.AssemblyPath = assemblyPath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginLoadingFailureInfo"/> class.
        /// </summary>
        /// <param name="pluginId">The plugin identifier.</param>
        /// <param name="pluginTitle">The plugin title.</param>
        /// <param name="pluginDataType">Type of the plugin data.</param>
        /// <param name="isObsolete">if set to <c>true</c>, plugin is soft deprecated.</param>
        /// <param name="message">The error message.</param>
        /// <param name="assemblyPath">The plugin assembly path.</param>
        public PluginLoadingFailureInfo(
            string pluginId,
            string pluginTitle,
            string pluginDataType,
            bool isObsolete,
            string message, 
            string assemblyPath)
        {
            this.PluginId = pluginId;
            this.PluginTitle = pluginTitle;
            this.PluginDataType = pluginDataType;
            this.IsObsolete = isObsolete;
            this.Message = message;
            this.AssemblyPath = assemblyPath;
        }

        /// <summary>
        /// Gets the plugin identifier.
        /// </summary>
        /// <value>
        /// The plugin identifier.
        /// </value>
        public string PluginId { get; protected internal set; }

        /// <summary>
        /// Gets the plugin title.
        /// </summary>
        /// <value>
        /// The plugin title.
        /// </value>
        public string PluginTitle { get; protected internal set; }

        /// <summary>
        /// Gets the data type of the plugin (for data plugins only).
        /// </summary>
        /// <value>
        /// The data type of the plugin.
        /// </value>
        public string PluginDataType { get; protected internal set; }

        /// <summary>
        /// Gets a value indicating whether the failed to load plugin is obsolete.
        /// </summary>
        /// <value>
        /// <c>true</c> if the failed to load plugin is obsolete; otherwise, <c>false</c>.
        /// </value>
        public bool IsObsolete { get; protected internal set; }

        /// <summary>
        /// Gets the plugin assembly path.
        /// </summary>
        /// <value>
        /// The plugin assembly path.
        /// </value>
        public string AssemblyPath { get; protected internal set; }

        /// <summary>
        /// Gets the message possibly describing the reasons of failure.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message { get; protected internal set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format(
                "Id = '{0}', Title = '{1}', Type = '{2}', IsObsolete = {3},{4}" +
                "Message = '{5}',{4}Path = '{6}'",
                this.PluginId ?? string.Empty,
                this.PluginTitle ?? string.Empty,
                this.PluginDataType ?? string.Empty,
                this.IsObsolete.ToString(),
                Environment.NewLine,
                this.Message ?? string.Empty,
                this.AssemblyPath ?? string.Empty);
        }

        /// <summary>
        /// Gets the plugin description attribute of the failed plugin.
        /// </summary>
        /// <returns><see cref="PluginDescriptionAttribute"/> instance for the failed to load plugin. </returns>
        public PluginDescriptionAttribute GetPluginDescription()
        {
            return new PluginDescriptionAttribute(
                this.PluginTitle,
                this.PluginDataType,
                this.PluginId,
                this.IsObsolete);
        }
    }
}
