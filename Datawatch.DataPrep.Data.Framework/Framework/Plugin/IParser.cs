using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IParser
    {
        // Return a row as a dictionary, where the <key, value> pair is the 
        // <ColumnName, rowValueForThatColumn>
        Dictionary<string, object> Parse(string message);
    }
}
