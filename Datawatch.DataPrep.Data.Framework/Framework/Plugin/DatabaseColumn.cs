﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Properties;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public class DatabaseColumn : PropertyBagViewModel
    {

        public DatabaseColumn()
            : base(new PropertyBag())
        {
        }

        public DatabaseColumn(PropertyBag bag)
            : base(bag)
        {
        }

        public DatabaseColumn(DatabaseColumn column)
            : base(new PropertyBag())
        {
            if (column == null)
            {
                throw Exceptions.ArgumentNull("column");
            }
            ColumnName = column.ColumnName;
			ColumnLabel = column.ColumnLabel;
            ColumnType = column.ColumnType;
            AppliedParameterName = column.AppliedParameterName;
            AggregateType = column.AggregateType;
            ColumnTypeName = column.ColumnTypeName;
            Selected = column.Selected;
            FilterValue = column.FilterValue;
            FilterOperator = column.FilterOperator;
            UserDefined = column.UserDefined;
        }

		public string ColumnLabel
		{
			get { return GetInternal("ColumnLabel"); }
			set { SetInternal("ColumnLabel", value); }
		}

		public string ColumnName
        {
            get { return GetInternal("ColumnName"); }
            set { SetInternal("ColumnName", value); }
        }

        public FilterOperator FilterOperator
        {
            get { return GetInternalEnum("FilterOperator", FilterOperator.None); }
            set { SetInternalEnum("FilterOperator", value); }
        }

        public string FilterValue
        {
            get { return GetInternal("FilterValue"); }
            set { SetInternal("FilterValue", value); }
        }

        public ColumnType ColumnType
        {
            get { return GetInternalEnum("ColumnType", ColumnType.Text); }
            set { SetInternalEnum("ColumnType", value); }
        }

        public string AppliedParameterName
        {
            get { return GetInternal("AppliedParameterName"); }
            set { SetInternal("AppliedParameterName", value); }
        }

        // TODO should serialize as its text value.
        [JsonIgnore]
        public AggregateType AggregateType
        {
            get
            {
                return GetInternalEnum("AggregateType",
                    AggregateType.None);
            }
            set { SetInternalEnum("AggregateType", value); }
        }

        public string ColumnTypeName
        {
            get { return GetInternal("ColumnTypeName"); }
            set { SetInternal("ColumnTypeName", value); }
        }

        public bool Selected
        {
            get
            {
                string value = GetInternal("Selected",
                   false.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("Selected", value.ToString()); }
        }

        public bool UserDefined
        {
            get
            {
                string value = GetInternal("UserDefined",
                   false.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("UserDefined", value.ToString()); }
        }

        public bool AllowPredicate
        {
            get
            {
                return !UserDefined;
            }
        }

        public virtual AggregateType[] AggregateTypes
        {
            get
            {
                if (ColumnType == ColumnType.Numeric)
                {
                    return new AggregateType[]
                    {
                        AggregateType.Sum,
                        AggregateType.Count,
                        AggregateType.Min,
                        AggregateType.Max,
                        AggregateType.None
                    };
                }
                else if (ColumnType == ColumnType.Time)
                {
                    return new AggregateType[]
                    {
                        AggregateType.Count,
                        AggregateType.Min,
                        AggregateType.Max,
                        AggregateType.None
                    };
                }
                else
                {
                    return new AggregateType[]
                    {
                        AggregateType.None
                    };
                }

            }
        }

        public virtual List<KeyValuePair<Model.FilterOperator, string>> FilterOperators
        {
            get
            {
                var kvPairList = new List<KeyValuePair<Model.FilterOperator, string>>();
                if (ColumnType == ColumnType.Numeric || ColumnType == ColumnType.Time)
                {
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.None, ErrorMessages.GetEnumDescription("None")));
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.EqualTo, ErrorMessages.GetEnumDescription("EqualTo")));
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.NotEqualTo, ErrorMessages.GetEnumDescription("NotEqualTo")));
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.GreaterThan, ErrorMessages.GetEnumDescription("GreaterThan")));
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.GreaterThanEqual, ErrorMessages.GetEnumDescription("GreaterThanEqual")));
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.LessThan, ErrorMessages.GetEnumDescription("LessThan")));
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.LessThanEqual, ErrorMessages.GetEnumDescription("LessThanEqual")));
                }
                else if (ColumnType == ColumnType.Text)
                {
                    kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.None, ErrorMessages.GetEnumDescription("None")));
                    kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.EqualTo, ErrorMessages.GetEnumDescription("EqualTo")));
                    kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.NotEqualTo, ErrorMessages.GetEnumDescription("NotEqualTo")));
                    kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.Contains, ErrorMessages.GetEnumDescription("Contains")));
                    kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.StartsWith, ErrorMessages.GetEnumDescription("StartsWith")));
                    kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.EndsWith, ErrorMessages.GetEnumDescription("EndsWith")));
                }
                else
                {
                   kvPairList.Add(new KeyValuePair<Model.FilterOperator, string>(Model.FilterOperator.None, ErrorMessages.GetEnumDescription("None")));
                }
                return kvPairList;
            }
        }

        public PropertyBag GetPropertyBag()
        {
            return propertyBag;
        }
    }
}
