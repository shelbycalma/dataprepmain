﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public class DescendingComparer : IComparer<object>
    {
        private readonly IComparer<object> comparer;

        public DescendingComparer(IComparer<object> comparer)
        {
            this.comparer = comparer;
        }

        public int Compare(object x, object y)
        {
            return -comparer.Compare(x, y);
        }
    }
}
