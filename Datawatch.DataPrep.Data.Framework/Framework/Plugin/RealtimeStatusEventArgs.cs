﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public class RealtimeStatusEventArgs : RealtimeDataPluginEventArgs
    {
        public readonly StreamingStatus Status;

        public RealtimeStatusEventArgs(ITable table, StreamingStatus status)
            : base(table)
        {
            this.Status = status;
        }
    }
}
