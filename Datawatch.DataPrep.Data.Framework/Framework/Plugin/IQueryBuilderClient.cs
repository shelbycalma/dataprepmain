﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IQueryBuilderClient
    {
        IEnumerable<DatabaseColumn> Columns { get; }
    }
}
