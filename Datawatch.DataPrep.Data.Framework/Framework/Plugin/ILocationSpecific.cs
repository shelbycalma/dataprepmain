﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface ILocationSpecific : IDataPlugin
    {
        DataLocation GetEnabledLocation(PropertyBag settings);
    }
}
