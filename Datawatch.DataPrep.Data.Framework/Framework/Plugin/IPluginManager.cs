﻿using System;
using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    /// <summary>
    /// Defines a set of properties needed to handle plug-in requests. 
    /// </summary>
    public interface IPluginManager : IDisposable
    {
        string[] EnabledDataPlugins { get; set; }

        string[] SkipDataPlugins { get; set; }

        string[] SkipDataTypes { get; set; }

        bool RemoveFromConnectToData { get; set; }

        bool BlockAtDesignTimeOnly { get; set; }

        /// <summary>
        /// Load all plugin assemblies and instantiate all plugins.
        /// </summary>
        void LoadPlugins();

        /// <summary>
        /// Initializes data plugins.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="globalSettings"></param>
        void InitializeDataPlugins(Window owner, PropertyBag globalSettings);

        /// <summary>
        /// Gets the active list of 
        /// <see cref="IPluginUI">data plug-in UIs</see>.
        /// </summary>
        IEnumerable<IPluginUI<IDataPlugin>> PluginUIs { get; }

        /// <summary>
        /// Gets the active list of 
        /// <see cref="IDataPlugin">data plug-ins</see>.
        /// </summary>
        IEnumerable<IDataPlugin> DataPlugins { get; }

        /// <summary>
        /// Gets the <see cref="IPluginUI"/> for a specific plug-in id.
        /// </summary>
        /// <returns>An <see cref="IPluginUI"/> instance or <code>null</code>.</returns>
        IPluginUI<IDataPlugin> GetPluginUI(string id);

        /// <summary>
        /// Gets the <see cref="IDataPlugin"/> for a specific plug-in id.
        /// </summary>
        /// <returns>An <see cref="IDataPlugin"/> instance or <code>null</code>.</returns>
        IDataPlugin GetDataPlugin(string id);

        /// <summary>
        /// Gets the <see cref="IParserPlugin"/> for a specific plug-in id.
        /// </summary>
        /// <returns>An <see cref="IParserPlugin"/> instance or <code>null</code>.</returns>
        IParserPlugin GetParserPlugin(string id);

        /// <summary>
        /// Gets the active list of 
        /// <see cref="IParserPlugin">dashboard part plug-ins</see>.
        /// </summary>
        IEnumerable<IParserPlugin> ParserPlugins { get; }

        /// <summary>
        /// Gets the instance of plugin error reporting services.
        /// </summary>
        /// <value>
        /// The plugin error reporting service instance.
        /// </value>
        IPluginErrorReportingService ErrorReporter { get; }

        /// <summary>
        /// Gets the list of plugin loading failures descriptions.
        /// </summary>
        /// <value>
        /// The information on failed to load plugins.
        /// </value>
        IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList { get; }

        /// <summary>
        /// Gets or sets a <see cref="PropertyBag"/> containing all plug-in 
        /// settings.
        /// </summary>
        PropertyBag PluginSettings { get; set; }

        /// <summary>
        /// Initializes the plugins UIs.
        /// </summary>
        /// <param name="owner"></param>
        void InitializePluginUIs(Window owner);
    }
}
