﻿using System.Collections.Generic;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IPluginUI<out T> where T : IDataPlugin
    {
        T Plugin { get; }

        string Id { get; }

        ImageSource Image { get; }

        /// <summary>
        /// Returns an element used to configure a data connection described by
        /// a settings <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="bag">A <see cref="PropertyBag"/> containing connection settings.</param>
        /// <param name="parameters">An enumerable of parameter values.</param>
        /// <returns>A configuration element.</returns>
        object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters);

        /// <summary>
        /// Returns a <see cref="PropertyBag"/> containing settings for a connection,
        /// given a configuration element.
        /// </summary>
        /// <param name="obj">The configuration element instance.</param>
        /// <returns>A <see cref="PropertyBag"/> containing connection settings.</returns>
        PropertyBag GetSetting(object obj);


        /// <summary>
        /// Establishes a new connection.
        /// </summary>
        /// <param name="parameters">An enumerable of parameter values.</param>
        /// <returns>Plugin-specific settings for the connection made by the plugin.</returns>
        PropertyBag DoConnect(IEnumerable<ParameterValue> parameters);

        /// <summary>
        /// Gets the title for the connection established through a
        /// call to <see cref="DoConnect"/>.
        /// </summary>
        /// <param name="settings">The settings for a connection the
        /// plugin returned from <see cref="DoConnect"/>.</param>
        /// <returns>A user-friendly string that describes the
        /// connection.</returns>
        string GetTitle(PropertyBag settings);

        /// <summary>
        /// Gets a value indicating whether this <see cref="IPlugin"/> instance
        /// is licensed.
        /// </summary>
        bool IsLicensed { get; }
    }
}
