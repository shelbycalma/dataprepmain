﻿namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IDataPluginConfigElement
    {
        bool IsOk { get; }
        void OnOk();
    }
}
