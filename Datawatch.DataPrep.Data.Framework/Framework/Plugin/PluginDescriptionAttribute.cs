﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    [AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, AllowMultiple = true)]
    public class PluginDescriptionAttribute : Attribute
    {
        private readonly string name;
        private readonly string type;
        private readonly string pluginId;
        private readonly bool obsolete;

        //TODO: rearrange arguments as (id, name, type, isObsolete) and update all plugins
        // as the majority of them are without id.

        public PluginDescriptionAttribute(string name, string type)
            : this(name, type, null)
        {
        }

        public PluginDescriptionAttribute(string name, string type, string pluginId)
            : this(name, type, pluginId, false)
        {
        }
        public PluginDescriptionAttribute(string name, string type, string pluginId, bool obsolete)
        {
            this.pluginId = pluginId;
            this.name = name;
            this.type = type;
            this.obsolete = obsolete;
        }

        public string PluginId
        {
            get { return pluginId; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Type
        {
            get { return type; }
        }

        public bool IsObsolete
        {
            get { return obsolete; }
        }

        public override string ToString()
        {
            return string.Format(
                "Name = {0}, Id = {1}, Type = {2}, IsObsolete = {3}",
                this.Name ?? "None",
                this.PluginId ?? "None",
                this.Type ?? "None",
                this.IsObsolete.ToString());
        }
    }
}