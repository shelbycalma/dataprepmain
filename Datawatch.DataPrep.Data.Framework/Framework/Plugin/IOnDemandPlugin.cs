﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IOnDemandPlugin : IDataPlugin
    {
        bool IsOnDemand(PropertyBag settings);

        ITable GetOnDemandData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount);
    }
}
