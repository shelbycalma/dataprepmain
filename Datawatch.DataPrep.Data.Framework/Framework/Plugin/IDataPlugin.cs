using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    /// <summary>
    /// Defines a set of methods and properties required of data loader plug-ins.
    /// </summary>
    public interface IDataPlugin : IPlugin 
    {
        /// <summary>
        /// Initializes the plug-in on startup.
        /// </summary>
        /// <param name="isHosted">Indicates if a plugin is hosted by a window.</param>
        /// <param name="pluginManager"><see cref="IPluginManager" />.</param>
        /// <param name="settings">The plug-in settings <see cref="PropertyBag" />.</param>
        /// <param name="globalSettings">The plug-in global settings <see cref="PropertyBag" />.</param>
        void Initialize(bool isHosted, IPluginManager pluginManager, PropertyBag settings, 
            PropertyBag globalSettings);

        /// <summary>
        /// Retrieves all un filtered data given settings and parameters. Row fileteration logic is not applied here.
        /// </summary>
        /// <param name="workbookDir">the current workbook directory.</param>
        /// <param name="dataDir">the current data directory.</param>
        /// <param name="settings">the settings <see cref="PropertyBag"/>.</param>
        /// <param name="parameters">a <see cref="Dictionary{T,T}"/> containing parameters.</param>
        /// <returns>An <see cref="ITable"/> instance.</returns>
        ITable GetAllData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters);

        /// <summary>
        /// Retrieves the first n number of rows of the data set specified by the 
        /// settings and parameters. Row fileteration logic is applied here.
        /// </summary>
        /// <param name="workbookDir">the current workbook directory.</param>
        /// <param name="dataDir">the current data directory.</param>
        /// <param name="settings">the settings <see cref="PropertyBag"/>.</param>
        /// <param name="parameters">a <see cref="Dictionary{T,T}"/> 
        ///   containing parameters.</param>
        /// <param name="rowcount">The number of rows to be returned.</param>
        /// <returns>An <see cref="ITable"/> instance.</returns>
        ITable GetData(string workbookDir, string dataDir, PropertyBag settings,
            IEnumerable<ParameterValue> parameters, int rowcount);
        
        /// <summary>
        /// Returns a list of paths to the physical data files for a specific
        /// connection.
        /// </summary>
        /// <param name="settings">A connection settings <see cref="PropertyBag"/>.</param>
        /// <returns>An array of file paths.</returns>
        string[] GetDataFiles(PropertyBag settings);

        /// <summary>
        /// Gets a value categorizing the data plug-in into, for example,
        /// File, Database and Streaming.
        /// </summary>
        string DataPluginType { get; }

        void UpdateFileLocation(PropertyBag settings, string oldPath, 
            string newPath, string workbookPath);

        IPluginManager PluginManager { get; }
    }
}
