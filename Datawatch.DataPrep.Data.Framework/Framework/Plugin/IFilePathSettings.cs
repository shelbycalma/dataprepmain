﻿namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IFilePathSettings : IWebPathSettings
    {
        PathType FilePathType { get; set; }
        string FilePath { get; set; }
        bool IsFilePathParameterized { get; }
        string Text { get; set; }
    }
}
