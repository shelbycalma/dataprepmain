﻿using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface ITextConnectorSettings
    {
        ParserSettings ParserSettings { get; }
    }
}
