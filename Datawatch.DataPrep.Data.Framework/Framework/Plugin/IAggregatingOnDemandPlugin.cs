﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IAggregatingOnDemandPlugin : IOnDemandPlugin
    {
        bool IncludesAggregateData(PropertyBag settings);

        string GetPartitionValue(PropertyBag settings);
    }
}
