﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IFileOpenDataPluginUI<out T> : IPluginUI<T> where T : IDataPlugin
    {
        string[] FileExtensions { get; }
        
        string FileFilter { get; }

        bool CanOpen(string filePath);

        PropertyBag DoOpenFile(string filePaths);

        PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap);
    }
}
