﻿using System.ComponentModel;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public enum StreamingStatus
    {
        [Description("UiStreamingStatusStopped")]
        Stopped,
        
        [Description("UiStreamingStatusWaiting")]
        Waiting,
        
        [Description("UiStreamingStatusStreaming")]
        Streaming,
        
        [Description("UiStreamingStatusPaused")]
        Paused
    }
}
