﻿namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public enum DataLocation
    {
        ClientSide = 0,
        ServerSide = 2,
        Any = 3
    }
}
