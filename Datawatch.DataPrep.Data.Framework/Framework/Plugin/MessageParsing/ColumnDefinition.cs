using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing
{
    public abstract class ColumnDefinition : PropertyBagViewModel
    {
        public event EventHandler<ValidateNameArgs> ValidateName;
        private DateParser dateParser;

        protected ColumnDefinition(PropertyBag bag) : base(bag)
        {
            dateParser = new DateParser(DateFormat);
        }

        // Make a basic validation if the ColumnDefinition is defined correctly. 
        public abstract bool IsValid { get; }

        public PropertyBag PropertyBag
        {
            get { return propertyBag; }
        }

        public ColumnType Type
        {
            get
            {
                // Old workbooks for the AMPS connector can have stored the 
                // type, not as "Numeric" or "Text", but instead as 0 or 1, the 
                // position in the enum. So to solve this situation we read up 
                // the value and if it can be parsed as a number then it is an 
                // old workbook and is a position in the MessageType enum.
                String columnType = GetInternal("Type", "Text");
                ColumnType type;
                if (Enum.TryParse(columnType, out type))
                {
                    return type;
                }
                int index;
                if (Int32.TryParse(columnType, out index))
                {
                    // Ok, it could be parsed as a number, then it's the old style.
                    return (ColumnType)Enum.GetValues(typeof(ColumnType)).
                        GetValue(index);
                }
                // Check if the old DateTime is used instead of Time in the workbook.
                if ("DateTime".Equals(columnType))
                {
                    return ColumnType.Time;
                }
                // Fallback to ColumnType.Text if parsing fails.
                return ColumnType.Text;
            }
            set { 
                SetInternal("Type", value.ToString());
                OnPropertyChanged("IsTime");
            }
        }

        public ColumnType[] Types
        {
            get { return (ColumnType[]) Enum.GetValues(typeof (ColumnType)); }
        }

        public string Name
        {
            get { return GetInternal("Name"); }
            set
            {
                OnValidateName(value);
                SetInternal("Name", value);
            }
        }

        public string DateFormat
        {
            get { return GetInternal("DateFormat"); }
            set
            {
                SetInternal("DateFormat", value);
                dateParser.DateFormat = value;
            }
        }

        public bool IsTime
        {
            get
            {
                return Type == ColumnType.Time;
            }
        }

        public bool IsEnabled
        {
            get
            {
                return GetInternalBool("IsEnabled", 
                    GetInternalBool("Enabled", true));
            }
            set { SetInternalBool("IsEnabled", value); }
        }
        
        public DateParser DateParser
        {
            get
            {
                return dateParser;
            }
        }

        public string FilterText
        {
            get
            {
                return GetInternal("FilterText");
            }
            set
            {
                SetInternal("FilterText", value);
            }
        }

        private void OnValidateName(string newName)
        {
            ValidateNameArgs args = new ValidateNameArgs(newName);
            if (ValidateName != null)
            {
                ValidateName(this, args);
            }
            if (!args.IsValid)
            {
                throw new Exception(string.Format(
                    Properties.Resources.ExColumnAlreadyExists, newName));
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!(obj is ColumnDefinition)) return false;
            ColumnDefinition columnDefinition = (ColumnDefinition) obj;

            return string.Equals(Name, columnDefinition.Name) 
                && Type == columnDefinition.Type 
                && IsEnabled == columnDefinition.IsEnabled
                && string.Equals(DateFormat, columnDefinition.DateFormat)
                && string.Equals(FilterText, columnDefinition.FilterText);
        }

        public override int GetHashCode()
        {
            int code = Type.GetHashCode();

            code += code*23 + IsEnabled.GetHashCode();

            if (!string.IsNullOrEmpty(Name))
            {
                code += code*23 + Name.GetHashCode();
            }
            if (!string.IsNullOrEmpty(DateFormat))
            {
                code += code * 23 + DateFormat.GetHashCode();
            }
            if (!string.IsNullOrEmpty(FilterText))
            {
                code += code * 23 + FilterText.GetHashCode();
            }
            return code;
        }
    }
}
