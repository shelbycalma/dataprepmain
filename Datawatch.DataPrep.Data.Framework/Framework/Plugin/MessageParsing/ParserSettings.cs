using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing
{
    public abstract class ParserSettings : PropertyBagViewModel
    {
        private IEnumerable<ColumnDefinition> columns;
        private Dictionary<string, ColumnDefinition> columnLookup;
        private bool disposed;
        private ICommand addColumn;
        private IEnumerable<ParameterValue> parameters;

        public abstract string GetDescription();

        protected ParserSettings(PropertyBag bag) : base(bag)
        {
            if (bag == null) throw Exceptions.ArgumentNull("bag");
            if (bag.SubGroups["Columns"] == null)
            {
                bag.SubGroups["Columns"] = new PropertyBag();
            }
        }

        public virtual IEnumerable<ParameterValue> Parameters
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
            }
        }

        public virtual bool ShowDescription
        {
            get
            {
                return !string.IsNullOrEmpty(Description);
            }
        }

        public virtual void InitiateNew(bool isMessageQueueSetting)
        {
        }

        public virtual void InitiateOld(bool isMessageQueueSetting)
        {
        }

        public virtual string Description
        {
            get
             {
                string description = GetDescription();

                return (description != null) ? description : string.Empty;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    DisposeColumnDefinitions();
                }
                disposed = true;
            }
            base.Dispose(disposing);
        }

        private void DisposeColumnDefinitions()
        {
            if (columns == null)
            {
                return;
            }
            foreach (ColumnDefinition columnDefinition in columns)
            {
                columnDefinition.PropertyChanged -= 
                    columnDefinition_PropertyChanged;
                columnDefinition.ValidateName -=
                    columnDefinition_ValidateName;
                columnDefinition.Dispose();
            }
            columns = null;
            columnLookup = null;
        }

        public abstract ColumnDefinition CreateColumnDefinition(PropertyBag bag);
        public abstract IParser CreateParser();

        /*
         * Columnn Definition is created lazily when needed for the first time
         * and are used in also in databindings in MessageParserSettingsTemplates.
         */
        public IEnumerable<ColumnDefinition> Columns
        {
            get
            {
                if (columns != null) return columns;

                List<ColumnDefinition> list = new List<ColumnDefinition>();
                Dictionary<string, ColumnDefinition> tempColumnLookup = 
                    new Dictionary<string, ColumnDefinition>();

                foreach (PropertyBag bag in 
                    propertyBag.SubGroups["Columns"].SubGroups)
                {
                    ColumnDefinition columnDefinition = 
                        CreateColumnDefinition(bag);
                    columnDefinition.PropertyChanged += 
                        columnDefinition_PropertyChanged;
                    columnDefinition.ValidateName += 
                        columnDefinition_ValidateName;
                    list.Add(columnDefinition);
                    tempColumnLookup.Add(columnDefinition.Name, columnDefinition);
                }
                columns = list;
                columnLookup = tempColumnLookup;
                return list;
            }
        }

        public int ColumnCount { 
            get
            {
                return propertyBag.SubGroups["Columns"].SubGroups.Count;
            }
        }

        private void columnDefinition_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Name")
            {
                ColumnDefinition columnDefinition = (ColumnDefinition) sender;

                columnLookup.Remove(columnDefinition.PropertyBag.Name);

                columnLookup.Add(columnDefinition.Name, columnDefinition);

                columnDefinition.PropertyBag.Name = columnDefinition.Name;
            }
        }

        private void columnDefinition_ValidateName(object sender, ValidateNameArgs e)
        {
            ColumnDefinition columnDefinition = (ColumnDefinition)sender;
            PropertyBag foundBag =
                propertyBag.SubGroups["Columns"].SubGroups[e.Name];
            if (foundBag != null && foundBag != columnDefinition.PropertyBag)
            {
                e.SetInvalid();
            }
        }

        public virtual void AddColumnDefinitions(IEnumerable<ColumnDefinition> columnDefinitions)
        {
            foreach (ColumnDefinition columnDefinition in columnDefinitions)
            {
                propertyBag.SubGroups["Columns"].
                    SubGroups[columnDefinition.Name] =
                    columnDefinition.PropertyBag;
            }
            DisposeColumnDefinitions();
            OnPropertyChanged("Columns");
        }

        public void AddColumnDefinition(ColumnDefinition col)
        {
            DisposeColumnDefinitions();
            propertyBag.SubGroups["Columns"].SubGroups[col.Name] = col.PropertyBag;
            OnPropertyChanged("Columns");
        }

        public void RemoveColumnDefinition(ColumnDefinition col)
        {
            DisposeColumnDefinitions();
            PropertyBag columnsBag = propertyBag.SubGroups["Columns"];
            if (columnsBag.SubGroups.ContainsKey(col.Name))
            {
                columnsBag.SubGroups.Remove(col.Name);                
            } 
            else
            {
                // Not stored according to new format where sub group name should equal ColumnDefinition.Name, try to use sub PropertyBag instance.
                columnsBag.SubGroups.Remove(col.PropertyBag);                                
            }
            OnPropertyChanged("Columns");
        }

        public void ClearColumnDefinitions()
        {
            foreach(ColumnDefinition colDef in Columns)
            {
                propertyBag.SubGroups["Columns"].SubGroups.Remove(colDef.PropertyBag);
            }
            DisposeColumnDefinitions();
            OnPropertyChanged("Columns");
        }

        public void RemoveColumnDefinition(int index)
        {
            if (columns == null)
            {
                return;
            }
            ColumnDefinition column = ((List<ColumnDefinition>)columns)[index];
            RemoveColumnDefinition(column);
        }

        public bool HasColumn(string name)
        {
            return propertyBag.SubGroups["Columns"].SubGroups.ContainsKey(name);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as ParserSettings);
        }

        public bool Equals(ParserSettings other)
        {
            if (other == null) return false;
            if (ReferenceEquals(this, other)) return true;

            IEnumerator<ColumnDefinition> otherColumns = 
                other.Columns.GetEnumerator();
            IEnumerator<ColumnDefinition> columns = Columns.GetEnumerator();

            while (otherColumns.MoveNext())
            {
                if (!columns.MoveNext())
                {
                    return false;
                }
                if (!Equals(otherColumns.Current, columns.Current))
                {
                    return false;
                }
            }
            if (columns.MoveNext())
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            int code = 17;

            foreach (ColumnDefinition columnDefinition in Columns)
            {
                code += 23*code + columnDefinition.GetHashCode();
            }

            return code;
        }

        public ICommand AddColumn
        {
            get
            {
                if (addColumn == null)
                {
                    addColumn = new DelegateCommand(DoAddColumn);
                }
                return addColumn;
            }
        }

        private void DoAddColumn()
        {
            ColumnDefinition newColumn =
                CreateColumnDefinition(new PropertyBag());
            int index = 1;
            string name = string.Format(Properties.Resources.UiColumnFormatString, index);
            while (HasColumn(name))
            {
                index++;
                name = string.Format(Properties.Resources.UiColumnFormatString, index);
            }
            newColumn.Name = name;
            AddColumnDefinition(newColumn);
        }

        public ColumnDefinition GetColumn(string name)
        {
            if (columnLookup == null)
            {
                Dictionary<string, ColumnDefinition> tempColumnLookup = 
                    new Dictionary<string, ColumnDefinition>();
                foreach (ColumnDefinition column in Columns)
                {
                    tempColumnLookup.Add(column.Name, column);
                }
                columnLookup = tempColumnLookup;
            }
            return columnLookup[name];
        }
    }
}
