using System;

namespace Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing
{
    public class ValidateNameArgs : EventArgs
    {
        public readonly string Name;
        private bool valid;

        public ValidateNameArgs(string name)
        {
            Name = name;
            valid = true;
        }

        public bool IsValid
        {
            get { return valid; }
        }

        public void SetInvalid()
        {
            valid = false;
        }
    }
}
