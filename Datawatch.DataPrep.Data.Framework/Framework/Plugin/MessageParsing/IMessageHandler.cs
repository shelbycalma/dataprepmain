﻿namespace Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing
{
    public interface IMessageHandler
    {
        void MessageReceived(string message);
    }
}
