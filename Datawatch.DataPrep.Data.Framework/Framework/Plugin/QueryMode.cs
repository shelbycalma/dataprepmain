﻿
namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public enum QueryMode
    {
        Table,
        Query,
        Report
    }
}
