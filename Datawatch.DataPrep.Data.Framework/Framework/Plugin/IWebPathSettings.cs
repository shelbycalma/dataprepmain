﻿using System.Net;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IWebPathSettings
    {
        string WebPath { get; set; }
        string WebUserID { get; set; }
        string WebPassword { get; set; }
        string RequestBody { get; set; }
        int RequestTimeout { get; set; }
        WebHeaderCollection WebHeaderCollection { get; }
    }
}
