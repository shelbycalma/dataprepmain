﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    /// <summary>
    /// Defines methods required of data loader plug-ins exposing a data reader.
    /// </summary>
    public interface IDataReaderPlugin : IDataPlugin
    {
        /// <summary>
        /// Returns a data reader as defined by the settings for the row by row 
        /// access to the underlying unfiltered data.
        /// </summary>
        /// <param name="workbookDir">The current workbook directory.</param>
        /// <param name="dataDir">The current data directory.</param>
        /// <param name="settings">The connection settings.</param>
        /// <param name="parameters">Query parameters.</param>
        /// <param name="maxRowCount">The maximum number of rows to
        /// return, or -1 to load all rows.</param>
        IDataReaderWithSchema GetDataReader(
            string workbookDir, string dataDir,
            PropertyBag settings,
            IEnumerable<ParameterValue> parameters,
            int maxRowCount);
    }
}
