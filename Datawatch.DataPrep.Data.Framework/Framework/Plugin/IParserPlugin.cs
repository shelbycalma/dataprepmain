﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IParserPlugin : IPlugin
    {
        ParserSettings CreateParserSettings(PropertyBag bag);
        Type GetParserType();
    }
}
