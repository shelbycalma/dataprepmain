﻿namespace Datawatch.DataPrep.Data.Framework.Plugin
{
    public interface IMessageQueueConnectorSettings : ITextConnectorSettings 
    {
        bool CanGenerateColumns();
        void DoGenerateColumns();
        bool CanTestConnection();
        void DoTestConnection();
        bool ShowFilter { get; }
    }
}
