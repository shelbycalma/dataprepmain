﻿using System;
using System.Diagnostics;
using System.IO;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Datawatch.DataPrep.Data.Framework
{
    //TODO factor this out. This should be a logmanager wrapper
    public static class Log
    {
        private static ILogger singleton;

        private static string timeFormat;
        private static string logFilePath;
        private static LogLevel logFileLogLevel = LogLevel.Error;
        private static object lockObject = new object();

        static Log()
        {
            timeFormat = "yyyy-MM-dd HH\\:mm\\:ss.fff";
        }

        public static void Error(string format, params object[] arguments)
        {
            Error(true, format, arguments);
        }

        public static void Error(bool condition,
            string format, params object[] arguments)
        {
            if (condition)
            {
                Singleton.Error(format, arguments);
            }
        }

        public static void Exception(Exception exception)
        {
            Singleton.Error(exception.ToString());
        }

        public static void Exception(string exStr)
        {
            Singleton.Error(exStr);
        }

        //public static void ExportFile(string destFileName)
        //{
        //    if (logFilePath == null || !File.Exists(logFilePath))
        //    {
        //        return;
        //    }
        //    File.Copy(logFilePath, destFileName, true);
        //}

        public static void Flush()
        {
            FlushInternal();
        }

        private static void FlushInternal()
        {
            FlushTrace();

            // Here we would flush our custom logging (the file log).
            LogManager.Flush();
        }

        [Conditional("TRACE")]
        private static void FlushTrace()
        {
            Trace.Flush();
        }

        public static void Info(string format, params object[] arguments)
        {
            Info(true, format, arguments);
        }

        public static void Info(bool condition,
            string format, params object[] arguments)
        {
            if (condition)
            {
                Singleton.Info(format, arguments);
            }
        }

        public static string LogFilePath
        {
            get { return logFilePath; }
        }

        public static LogLevel LogFileLogLevel
        {
            get { return logFileLogLevel; }
        }

        public static void LogUnconditional(string format,
            params object[] arguments)
        {
            Singleton.Info(format, arguments);
        }

        public static void Warning(string format, params object[] arguments)
        {
            Warning(true, format, arguments);
        }

        public static void Warning(bool condition,
            string format, params object[] arguments)
        {
            if (condition)
            {
                Singleton.Warn(format, arguments);
            }
        }

        public static ILogger Singleton
        {
            get
            {
                if (Log.singleton == null)
                {
                    ConfigureLog(null, LogLevel.Warning);
                    Log.singleton = LogManager.GetLogger(typeof(Log).ToString());
                }
                return singleton;
            }
        }
        
        public static void ConfigureLog(string filePath, LogLevel level, bool addConsole = false)
        {
            NLog.LogLevel outLevel = NLog.LogLevel.Warn;
            switch (level)
            {
                case LogLevel.Error:
                    outLevel = NLog.LogLevel.Error;
                    break;
                case LogLevel.Exception:
                    outLevel = NLog.LogLevel.Fatal;
                    break;
                case LogLevel.Info:
                    outLevel = NLog.LogLevel.Debug;
                    break;
                case LogLevel.Warning:
                    outLevel = NLog.LogLevel.Warn;
                    break;
            }
            logFileLogLevel = level;
            logFilePath = filePath;
            ConfigureLog(filePath, outLevel, addConsole);
        }

        //Called once per app
        //TODO maybe move most of the stuff to files??
        private static void ConfigureLog(string filePath,
            NLog.LogLevel level, bool addConsole)
        {
            lock (lockObject)
            {
                var config = new LoggingConfiguration();
                if (filePath != null)
                {
                    var fileTarget = new FileTarget();
                    fileTarget.Layout = @"${date:format=" + timeFormat + "} ${level} ${logger} ${message}";
                    fileTarget.FileName = filePath;

                    var rule = new LoggingRule("*", level, fileTarget);
                    config.LoggingRules.Add(rule);
                }
#if DEBUG
                var debugTarget = new DebuggerTarget();
                var debugRule = new LoggingRule("*", NLog.LogLevel.Debug, debugTarget);
                config.LoggingRules.Add(debugRule);
#endif
                if (addConsole)
                {
                    var consoleTarget = new ColoredConsoleTarget();
                    consoleTarget.UseDefaultRowHighlightingRules = true;
                    var consoleRule = new LoggingRule("*", NLog.LogLevel.Debug, consoleTarget);
                    config.LoggingRules.Add(consoleRule);
                }
                LogManager.Configuration = config;
                Log.singleton = LogManager.GetLogger(typeof(Log).ToString());
            }
        }
    }
}
