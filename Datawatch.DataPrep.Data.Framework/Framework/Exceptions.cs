﻿using System;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Properties;

namespace Datawatch.DataPrep.Data.Framework
{
    public class Exceptions
    {
        private static readonly CultureInfo culture = CultureInfo.CurrentCulture;

        public static Exception ArgumentNull(string parameter)
        {
            return CreateArgumentNull(parameter,
                Format(Resources.ExArgumentNull, parameter));
        }

        public static Exception StorageCapacitiesDifferent()
        {
            return CreateArgument(
                Format(Resources.ExStorageCapacitiesDifferent));
        }

        public static Exception TableColumnUnknownTypeForRead(
            string name, Type type)
        {
            return CreateArgument(Format(
                Resources.ExTableColumnUnknownTypeForRead, name, type.Name));
        }

        public static Exception TableColumnIsNotSeries(string name)
        {
            return CreateInvalidOperation(Format(
                Resources.ExTableColumnIsNotSeries, name));
        }

        public static Exception TableColumnIsSeries(string name)
        {
            return CreateInvalidOperation(Format(
                Resources.ExTableColumnIsSeries, name));
        }

        public static Exception NumericIntervalEmptyEndpoint(string name)
        {
            return CreateArgument(Format(
                Resources.ExNumericIntervalEmptyEndpoint, name));
        }

        public static Exception TimeIntervalEmptyEndpoints()
        {
            return CreateArgument(Format(
                Resources.ExTimeIntervalEmptyEndpoints));
        }

        public static Exception TimeIntervalReversedEndpoints()
        {
            return CreateArgument(Format(
                Resources.ExTimeIntervalReversedEndpoints));
        }

        public static Exception TimeIntervalReservedEndpoints()
        {
            return CreateArgument(Format(
                Resources.ExTimeIntervalReservedEndpoints));
        }

        public static Exception AliasAlreadyInUse(string alias)
        {
            return CreateNotSupported(
                Format(Resources.ExAliasAlreadyInUse, alias));
        }

        public static Exception FunctionNotSupported(FunctionType function)
        {
            return CreateNotSupported(
                Format(Resources.ExFunctionNotSupported, function));
        }

        #region protected methods
        protected static string Format(string format, params object[] arguments)
        {
            return string.Format(culture, format, arguments);
        }

        protected static InvalidOperationException CreateInvalidOperation(string message)
        {
            return (InvalidOperationException)LogException(
                new InvalidOperationException(message));
        }

        protected static ArgumentException CreateArgument(string message)
        {
            return (ArgumentException)LogException(
                new ArgumentException(message));
        }

        protected static ArgumentException CreateArgument(
            string parameter, string message)
        {
            return (ArgumentException)LogException(
                new ArgumentException(message, parameter));
        }

        protected static ArgumentNullException CreateArgumentNull(string parameter, string message)
        {
            return (ArgumentNullException)LogException(
                new ArgumentNullException(parameter, message));
        }

        protected static Exception CreateUnspecifiedInternal(string message)
        {
            return LogException(new Exception(message));
        }

        protected static NotSupportedException CreateNotSupported(string message)
        {
            return (NotSupportedException)LogException(
                new NotSupportedException(message));
        }

        protected static Exception LogException(Exception exception)
        {
            Log.Exception(exception);
            return exception;
        }


        protected static NotSupportedException
            CreateNotSupported(string message, Exception inner)
        {
            return (NotSupportedException)LogException(
                new NotSupportedException(message, inner));
        }
        #endregion
    }
}
