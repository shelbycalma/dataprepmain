﻿using System.Windows.Media;

namespace Datawatch.DataPrep.Data.Framework
{
    public interface IResourceLoader
    {
        string GetResourceString(string key);
        ImageSource GetImage(string path);
    }
}
