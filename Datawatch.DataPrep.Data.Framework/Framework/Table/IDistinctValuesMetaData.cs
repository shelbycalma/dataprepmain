﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by <see cref="IColumnMetaData"/> classes that can return
    /// distinct values (and their counts) from the associated column.
    /// </summary>
    /// <typeparam name="T">The type of the values (and column).</typeparam>
    /// <remarks>
    /// <para>If this interface is available on a column's meta data it should
    /// be more efficient to use it to get distinct values than actually
    /// scanning the column values and counting.</para>
    /// <para>This interface also has the advantage that you can read the
    /// values one by one, and doesn't force you to read the entire column
    /// (and table) into memory.</para>
    /// </remarks>
    public interface IDistinctValuesMetaData<out T>
    {
        /// <summary>
        /// Gets the column's distinct values and their counts.
        /// </summary>
        /// <param name="excludeNulls">True to leave out nulls from the
        /// result.</param>
        /// <param name="maxRowCount">The maximum number of distinct values to
        /// return, or -1 to retrieve all distinct values.</param>
        /// <returns>A set of distinct values with counts.</returns>
        /// <remarks>
        /// <para>This method is case sensitive, and returns the values in the
        /// order they occur in the column.</para>
        /// </remarks>
        IDistinctValues<T> GetDistinctValues(
            bool excludeNulls, int maxRowCount);
    }
}
