using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Provides information on an <see cref="IColumn"/>.
    /// </summary>
    /// <remarks>
    /// <para>More specialized versions of this interface are available, e.g.
    /// <see cref="INumericColumnMetaData"/>. Multiple interfaces can be
    /// implemented on the same object.</para>
    /// <para>All data source meta data is optional information. A table
    /// implementation may provide no, very little, or full information. Do
    /// not write code that depends on meta data being available, unless you
    /// know it is.</para>
    /// </remarks>
    public interface IColumnMetaData
    {
        /// <summary>
        /// Gets the actual data type of the column.
        /// </summary>
        /// <remarks>
        /// <para>The object that implements <see cref="IColumn"/> might store
        /// its data as some other type than the ones used in the framework.
        /// For example, a column that implements <see cref="INumericColumn"/>
        /// might really store integers or custom decimal values.
        /// This property can be used to specify the "native" type.</para>
        /// <para>Timeseries columns should return the data type of individual
        /// samples in the series.</para>
        /// </remarks>
        Type DataType { get; }

        /// <summary>
        /// Gets a boolean value indicating whether
        /// this column is hidden or not.
        /// </summary>
        /// <remarks>
        /// <para>A hidden column will not be displayed in any 
        /// listing of columns provided by SDK controls.</para>
        /// </remarks>
        bool Hidden { get; }

        /// <summary>
        /// Gets the title of the column.
        /// </summary>
        /// <remarks>
        /// <para>The title can be more descriptive than the column's
        /// <see cref="IColumn.Name"/>, and does not need to be unique. Always
        /// display the title to users rather than the name if there is
        /// one.</para>
        /// </remarks>
        string Title { get; }
    }
}
