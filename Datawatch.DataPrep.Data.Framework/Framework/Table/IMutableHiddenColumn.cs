﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    // TODO: Would love to have this implemented on the column's
    // IColumnMetaData rather than on the column itself.
    public interface IMutableHiddenColumn
    {
        bool Hidden { get; set; }
    }
}
