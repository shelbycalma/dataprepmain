using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// A collection of <see cref="ITime"/> instances 
    /// whose <see cref="DateTime"/> values are reserved.
    /// </summary>
    public class ReservedTime: ITime
    {
        /// <summary>
        /// An <see cref="ITime"/> wrapper for <see cref="DateTime.MinValue"/>.
        /// </summary>
        public static readonly ITime MinValue =
            new ReservedTime(DateTime.MinValue);
        
        /// <summary>
        /// An <see cref="ITime"/> wrapper for <see cref="DateTime.MaxValue"/>.
        /// </summary>
        public static readonly ITime MaxValue =
            new ReservedTime(DateTime.MaxValue);

        private DateTime myTime;

        /// <summary>
        /// Returns true if the supplied <see cref="ITime"/> 
        /// violates one of the reserved times.
        /// </summary>
        /// <param name="time"></param>
        public static bool Contains(ITime time)
        {
            if (time == null) {
                return false;
            }
            return Contains(time.Time);
        }

        /// <summary>
        /// Returns true if the supplied <see cref="DateTime"/> 
        /// violates one of the reserved times.
        /// </summary>
        /// <param name="time"></param>
        public static bool Contains(DateTime time)
        {
            return time == MinValue.Time || time == MaxValue.Time;
        }

        private ReservedTime(DateTime time)
        {
            myTime = time;
        }

        DateTime ITime.Time
        {
            get { return myTime; }
        }

    }
}
