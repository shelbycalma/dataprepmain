﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by tabular data sources to indicate that some or all of
    /// their data comes from other tables.
    /// </summary>
    /// <remarks>
    /// <para>For example, if you implement an "extender" table that takes a
    /// source <see cref="ITable"/> and extends it with calculated columns,
    /// then implement <see cref="IDerivedTable"/> on your extender table and
    /// return the source table from the interface.</para>
    /// <para>This interface supports multiple source tables. This is to
    /// accommodate things like join tables that would be based on two
    /// <see cref="ITable"/>s.</para>
    /// </remarks>
    public interface IDerivedTable : ITable
    {
        /// <summary>
        /// Returns the source <see cref="ITable"/> at the specified index.
        /// </summary>
        /// <param name="index">Index of source table to return.</param>
        /// <returns>A <see cref="ITable"/> that this table gets data
        /// from.</returns>
        ITable GetSourceTable(int index);

        /// <summary>
        /// Returns the number of source <see cref="ITable"/>s that this
        /// table gets data from.
        /// </summary>
        /// <remarks>
        /// <para>This would usually be one or more. A <see cref="ITable"/>
        /// that has no source tables would rarely implement
        /// <see cref="IDerivedTable"/>.</para>
        /// </remarks>
        int SourceTableCount { get; }
    }
}
