﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    public interface INumericBucketColumnMetaData : IBucketColumnMetaData
    {
        INumericColumn NumericColumn { get; }
    }
}
