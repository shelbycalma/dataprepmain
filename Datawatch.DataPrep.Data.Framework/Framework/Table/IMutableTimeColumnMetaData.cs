﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// An extended version of the <see cref="ITimeColumnMetaData"/> interface 
    /// that expose methods to set the Domain and Format properties.
    /// </summary>
    public interface IMutableTimeColumnMetaData 
        : ITimeColumnMetaData, IMutableColumnMetaData
    {
        /// <summary>
        /// The domain (valid range of values) for the column.
        /// </summary>
        /// <remarks>
        /// <para>An implementation should return null to indicate that it
        /// cannot provide the information.</para>
        /// <para>This property is part of the definition of the column and is
        /// independent of the actual data that the table contains. The
        /// property can have a value even if there is no data in the table, but
        /// any data that is in table must not validate the property.</para>
        /// </remarks>
        new TimeValueInterval Domain { get; set; }

        /// <summary>
        /// The string format to use when converting values from the column into
        /// strings for presentation.
        /// </summary>
        /// <remarks>
        /// <para>An implementation should return null to indicate that it
        /// cannot provide the information.</para>
        /// </remarks>
        new string Format { get; set; }
    }
}
