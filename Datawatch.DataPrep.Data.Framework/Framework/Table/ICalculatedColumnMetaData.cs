﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    public interface ICalculatedColumnMetaData : IColumnMetaData
    {
        string Formula { get; }

        bool IsTimeWindowCalculation { get; }
    }
}
