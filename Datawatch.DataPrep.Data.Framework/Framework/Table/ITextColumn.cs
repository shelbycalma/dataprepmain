
namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by columns that have the <see cref="string"/> data type.
    /// </summary>
    public interface ITextColumn : IColumn
    {
        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row to read the value from.</param>
        /// <returns>The cell value as a string.</returns>
        string GetTextValue(IRow row);

        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row index to read the value from.</param>
        /// <returns>The cell value as a string.</returns>
        string GetTextValue(int row);
    }
}
