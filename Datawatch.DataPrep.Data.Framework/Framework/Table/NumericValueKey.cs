﻿using System.Globalization;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// A key for table <see cref="INumericColumn"/> values.
    /// </summary>
    public sealed class NumericValueKey : IValueKey<double>
    {
        /// <summary>
        /// The value that the key represents.
        /// </summary>
        private readonly double value;

        /// <summary>
        /// Creates a new key to wrap a numeric value.
        /// </summary>
        /// <param name="value">The key's numeric value.</param>
        public NumericValueKey(double value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the key's value.
        /// </summary>
        public double Value
        {
            get { return value; }
        }

        /// <summary>
        /// True if the key's value is empty (see
        /// <see cref="NumericValue.IsEmpty(double)"/>).
        /// </summary>
        public bool IsEmpty
        {
            get { return NumericValue.IsEmpty(value); }
        }

        /// <summary>
        /// Checks if this key is equal to another numeric value key.
        /// </summary>
        /// <param name="other">The numeric value key to compare
        /// against.</param>
        /// <returns>True if both keys are equal (both empty, or have the
        /// same value).</returns>
        /// <remarks>
        /// <para>This method uses a robust numeric comparison to protect
        /// against rounding errors. This behavior is different from regular
        /// <see cref="INumericColumn"/> value comparison which uses
        /// <see cref="NumericValue.IsEqual(double, double)"/>, and requires
        /// exact equality.</para>
        /// </remarks>
        public bool Equals(NumericValueKey other)
        {
            if (other == null) {
                return false;
            }
            if (ReferenceEquals(other, this)) {
                return true;
            }
            return NumericValue.IsEqual(value, other.value);
//            if (IsEmpty) {
//                return other.IsEmpty;
//            }
//            if (other.IsEmpty) {
//                return false;
//            }
//            return NumericValue.IsAlmostEqual(value, other.value);
        }

        public bool Equals(IKey other)
        {
            return Equals(other as NumericValueKey);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as NumericValueKey);
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override string ToString()
        {
            if (IsEmpty) {
                return "null";
            }
            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}