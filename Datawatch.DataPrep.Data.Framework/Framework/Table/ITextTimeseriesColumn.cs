namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by time series columns that have the <see cref="string"/>
    /// data type.
    /// </summary>
    /// <remarks>
    /// <para>Note that the interface does not extend <see cref="ITextColumn"/>
    /// - you might expect that because <see cref="INumericTimeseriesColumn"/>
    /// extends <see cref="INumericColumn"/>.</para>
    /// </remarks>
    public interface ITextTimeseriesColumn :
        ITimeseriesColumn
    {
        /// <summary>
        /// Returns the cell value in the specified row and at the specified
        /// sample time.
        /// </summary>
        /// <param name="row">The row to read the value from.</param>
        /// <param name="time">The sample time to read the value from.</param>
        /// <returns>The cell value as a string.</returns>
        string GetTextValue(IRow row, ITime time);

        /// <summary>
        /// Returns the cell value in the specified row and at the specified
        /// sample.
        /// </summary>
        /// <param name="row">The row index to read the value from.</param>
        /// <param name="sample">The sample index to read the value
        /// from.</param>
        /// <returns>The cell value as a string.</returns>
        string GetTextValue(int row, int sample);
    }
}
