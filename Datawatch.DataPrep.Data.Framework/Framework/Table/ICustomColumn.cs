﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by columns that are not of standard type (text, numeric,
    /// or time) but still need to be used by the framework.
    /// </summary>
    /// <remarks>
    /// <para>Since there is very little the framework can do with a column of
    /// unknown type, the usefulness of this interface is limited. However,
    /// some code, e.g. exporting a table as CSV, may fall back on this
    /// interface if none of the known types are supported by a column.</para>
    /// </remarks>
    public interface ICustomColumn : IColumn
    {
        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row to read the value from.</param>
        /// <returns>The cell value as an object (with null to indicate a
        /// missing, empty, or undefined value).</returns>
        object GetCustomValue(IRow row);

        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row index to read the value from.</param>
        /// <returns>The cell value as an object (with null to indicate a
        /// missing, empty, or undefined value).</returns>
        object GetCustomValue(int row);
    }
}
