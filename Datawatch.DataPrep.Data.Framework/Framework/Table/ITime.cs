using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents a sample time in a <see cref="ITimeseriesTable"/>.
    /// </summary>
    /// <remarks>
    /// <para>A sample time is a date and/or time that all time series in the
    /// table are defined on. Having this represented by an interface rather
    /// than a <see cref="DateTime"/> directly allows data sources to rely on
    /// richer implementations or calculations.</para>
    /// </remarks>
    public interface ITime
    {
        /// <summary>
        /// Gets the actual instant in time that the sample time represents.
        /// </summary>
        DateTime Time { get; }
    }
}
