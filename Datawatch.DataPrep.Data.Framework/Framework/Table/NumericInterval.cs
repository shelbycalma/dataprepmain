namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents a numeric (<see cref="double"/>) interval.
    /// </summary>
    /// <remarks>
    /// <para>There is no way to "leave out" one or both of the end points to
    /// specify an open-ended interval. Use
    /// <see cref="double.NegativeInfinity"/> or
    /// <see cref="double.PositiveInfinity"/> in these cases.</para>
    /// </remarks>
    public class NumericInterval
    {
        private readonly double min;

        private readonly double max;


        /// <summary>
        /// Creates a new instance of the <see cref="NumericInterval"/> class.
        /// </summary>
        /// <param name="min">The minimum/lower end point of the
        /// interval.</param>
        /// <param name="max">The maximum/upper end point of the
        /// interval.</param>
        public NumericInterval(double min, double max)
        {
            if (NumericValue.IsEmpty(min)) {
                throw Exceptions.NumericIntervalEmptyEndpoint("min");
            }
            if (NumericValue.IsEmpty(max)) {
                throw Exceptions.NumericIntervalEmptyEndpoint("max");
            }
            this.min = min;
            this.max = max;
        }

        /// <summary>
        /// Checks if a specific <see cref="NumericValue"/> is in the interval.
        /// </summary>
        /// <param name="value">Value to test.</param>
        /// <returns>True if the value is in the interval.</returns>
        /// <remarks>
        /// <para>This method regards the interval as closed, i.e. the test
        /// performed is (min &lt;= value &amp;&amp; value &lt;= max).</para>
        /// <para>This method always returns <c>false</c> for the
        /// <see cref="NumericValue.Empty"/> value.</para>
        /// </remarks>
        public bool Contains(double value)
        {
            if (NumericValue.IsEmpty(value)) {
                return false;
            }
            return min <= value && value <= max;
        }

        /// <summary>
        /// Gets the maximum/upper end point of the interval.
        /// </summary>
        public double Max
        {
            get { return max; }
        }

        /// <summary>
        /// Gets the minimum/lower end point of the interval.
        /// </summary>
        public double Min
        {
            get { return min; }
        }

        /// <summary>
        /// Returns the interval formatted as a string.
        /// </summary>
        /// <returns>A human-readable string representation of the
        /// interval.</returns>
        public override string ToString()
        {
            return string.Format("[{0}, {1}]", min, max);
        }
    }
}
