﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    public interface IRankingColumnMetaData : IBucketColumnMetaData
    {
        INumericColumn RankingColumn { get; }

        SortOrder RankingSortOrder { get; }
    }
}
