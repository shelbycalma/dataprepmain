namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Extends an <see cref="IColumnMetaData"/> with information relevant
    /// for time columns.
    /// </summary>
    public interface ITimeColumnMetaData : IColumnMetaData
    {
        /// <summary>
        /// The domain (valid range of values) for the column.
        /// </summary>
        /// <remarks>
        /// <para>An implementation should return null to indicate that it
        /// cannot provide the information.</para>
        /// <para>This property is part of the definition of the column and is
        /// independent of the actual data that the table contains. The
        /// property can have a value even if there is no data in the table, but
        /// any data that is in table must not validate the property.</para>
        /// </remarks>
        TimeValueInterval Domain { get; }

        /// <summary>
        /// The string format to use when converting values from the column into
        /// strings for presentation.
        /// </summary>
        /// <remarks>
        /// <para>An implementation should return null to indicate that it
        /// cannot provide the information.</para>
        /// </remarks>
        string Format { get; }

        // TODO: Resolution would be great!
    }
}
