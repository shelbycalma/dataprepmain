
namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by columns that have the <see cref="double"/> data type.
    /// </summary>
    public interface INumericColumn : IColumn
    {
        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row to read the value from.</param>
        /// <returns>The cell value as a double.</returns>
        /// <remarks>
        /// <para>A data source will indicate a missing value by returning
        /// <see cref="NumericValue.Empty"/>. Use
        /// <see cref="NumericValue.IsEmpty(double)"/> to test for this.</para>
        /// </remarks>
        double GetNumericValue(IRow row);

        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row index to read the value from.</param>
        /// <returns>The cell value as a double.</returns>
        /// <remarks>
        /// <para>A data source will indicate a missing value by returning
        /// <see cref="NumericValue.Empty"/>. Use
        /// <see cref="NumericValue.IsEmpty(double)"/> to test for this.</para>
        /// </remarks>
        double GetNumericValue(int row);
    }
}
