using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by columns that have the <see cref="DateTime"/> data type.
    /// </summary>
    /// <remarks>
    /// <para>This column type has nothing to do with time series tables or
    /// the <see cref="ITime"/> interface, it is simply a column that stores
    /// date-times.</para>
    /// </remarks>
    public interface ITimeColumn : IColumn
    {
        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row to read the value from.</param>
        /// <returns>The cell value as a date-time.</returns>
        /// <remarks>
        /// <para>A data source will indicate a missing value by returning
        /// <see cref="TimeValue.Empty"/>. Use
        /// <see cref="TimeValue.IsEmpty(DateTime)"/> to test for this.</para>
        /// </remarks>
        DateTime GetTimeValue(IRow row);

        /// <summary>
        /// Returns the cell value in the specified row and this column.
        /// </summary>
        /// <param name="row">Row index to read the value from.</param>
        /// <returns>The cell value as a date-time.</returns>
        /// <remarks>
        /// <para>A data source will indicate a missing value by returning
        /// <see cref="TimeValue.Empty"/>. Use
        /// <see cref="TimeValue.IsEmpty(DateTime)"/> to test for this.</para>
        /// </remarks>
        DateTime GetTimeValue(int row);
    }
}
