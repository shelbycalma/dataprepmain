﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Event data for the <see cref="ITable.Changed"/> event.
    /// </summary>
    /// <remarks>
    /// <para>This class is empty now, but may be extended in the future to
    /// include information on the specific changes that were made to a
    /// <see cref="ITable"/> during the batch.</para>
    /// </remarks>
    public class TableChangedEventArgs : EventArgs
    {
        // Please note, that if you extend this class with more info, like
        // RowsAdded/Removed, ColumnsAdded/Removed, TimesAdded/Removed, and/or
        // ColumnsUpdated, you need to check where the SDK listens for this
        // event and optimize the handling of it.

	    /// <summary>
	    /// Represents an event with no specific data available about what
        /// has changed.
	    /// </summary>
        public new static readonly TableChangedEventArgs
            Empty = new TableChangedEventArgs();

        private TableChangedEventArgs()
        {
        }
    }
}
