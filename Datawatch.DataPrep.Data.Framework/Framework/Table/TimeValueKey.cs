﻿using System;
using System.Globalization;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// A key for table <see cref="ITimeColumn"/> values.
    /// </summary>
    public sealed class TimeValueKey : IValueKey<DateTime>
    {
        /// <summary>
        /// The value that the key represents.
        /// </summary>
        private readonly DateTime value;

        /// <summary>
        /// Creates a new key to wrap a time value.
        /// </summary>
        /// <param name="value">The key's time value.</param>
        public TimeValueKey(DateTime value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the key's value.
        /// </summary>
        public DateTime Value
        {
            get { return value; }
        }

        /// <summary>
        /// True if the key's value is empty (see
        /// <see cref="TimeValue.IsEmpty(DateTime)"/>).
        /// </summary>
        public bool IsEmpty
        {
            get { return TimeValue.IsEmpty(value); }
        }

        /// <summary>
        /// Checks if this key is equal to another time value key.
        /// </summary>
        /// <param name="other">The time value key to compare against.</param>
        /// <returns>True if both keys are equal (both empty, or have same
        /// value).</returns>
        public bool Equals(TimeValueKey other)
        {
            if (other == null) {
                return false;
            }
            if (ReferenceEquals(other, this)) {
                return true;
            }
            if (IsEmpty) {
                return other.IsEmpty;
            }
            if (other.IsEmpty) {
                return false;
            }
            return value.Equals(other.Value);
        }

        public bool Equals(IKey other)
        {
            return Equals(other as TimeValueKey);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TimeValueKey);
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public override string ToString()
        {
            if (IsEmpty) {
                return "null";
            }
            return value.ToString(CultureInfo.InvariantCulture);
        }
    }
}