﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Helper class that lets you read column values as objects, regardless of
    /// the column's type.
    /// </summary>
    /// <remarks>
    /// <para>Avoid this class if possible. It's much faster to access column
    /// values through a typed interface like <see cref="ITextColumn"/>. This
    /// class was added to ease migration, and for a few scenarios where the
    /// type of the column - and the actual value returned - does not
    /// matter.</para>
    /// </remarks>
    public abstract class ColumnReader
    {
        /// <summary>
        /// Creates a column reader for a single column that can be reused for
        /// multiple rows.
        /// </summary>
        /// <param name="column">The column to read from.</param>
        /// <returns>An untyped column reader for the column.</returns>
        public static ColumnReader ForColumn(IColumn column)
        {
            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }

            ITextColumn text = column as ITextColumn;
            if (text != null) {
                return new TextColumnReader(text);
            }
            ITextTimeseriesColumn textSeries = column as ITextTimeseriesColumn;
            if (textSeries != null) {
                return new TextSeriesColumnReader(textSeries);
            }
            INumericColumn numeric = column as INumericColumn;
            if (numeric != null) {
                return new NumericColumnReader(numeric);
            }
            ITimeColumn time = column as ITimeColumn;
            if (time != null) {
                return new TimeColumnReader(time);
            }
            ICustomColumn custom = column as ICustomColumn;
            if (custom != null) {
                return new CustomColumnReader(custom);
            }
            throw Exceptions.TableColumnUnknownTypeForRead(
                column.Name, column.GetType());
        }

        /// <summary>
        /// Creates an array of column readers to match a set of columns.
        /// </summary>
        /// <param name="columns">Columns that you wish to read.</param>
        /// <returns>An array of readers (each one created by the
        /// <see cref="ColumnReader.ForColumn(IColumn)"/> method).</returns>
        public static ColumnReader[] ForColumns(IEnumerable<IColumn> columns)
        {
            if (columns == null) {
                throw Exceptions.ArgumentNull("columns");
            }

            List<ColumnReader> readers = new List<ColumnReader>();
            foreach (IColumn column in columns) {
                readers.Add(ColumnReader.ForColumn(column));
            }
            return readers.ToArray();
        }

        /// <summary>
        /// Creates an array of column readers to match all columns in a table.
        /// </summary>
        /// <param name="table">Table whose columns you wish to read.</param>
        /// <returns>An array of readers (each one created by the
        /// <see cref="ColumnReader.ForColumn(IColumn)"/> method).</returns>
        public static ColumnReader[] ForTable(ITable table)
        {
            if (table == null) {
                throw Exceptions.ArgumentNull("table");
            }

            ColumnReader[] readers = new ColumnReader[table.ColumnCount];
            for (int i = 0; i < table.ColumnCount; i++) {
                readers[i] = ColumnReader.ForColumn(table.GetColumn(i));
            }
            return readers;
        }

        /// <summary>
        /// Reads the row's value and returns it as an object.
        /// </summary>
        /// <param name="row">The row to read.</param>
        /// <returns>The value as an object, or null if the value is missing,
        /// empty, or not defined.</returns>
        public abstract object GetValue(IRow row);

        /// <summary>
        /// Reads the row's value and returns it as an object.
        /// </summary>
        /// <param name="row">The index of the row to read.</param>
        /// <returns>The value as an object, or null if the value is missing,
        /// empty, or not defined.</returns>
        public abstract object GetValue(int row);

        /// <summary>
        /// Reads the row's value in a series column and returns it as an
        /// object.
        /// </summary>
        /// <param name="row">The row to read.</param>
        /// <param name="time">The sample time at which to read.</param>
        /// <returns>The value as an object, or null if the value is missing,
        /// empty, or not defined.</returns>
        /// <remarks>
        /// <para>This method will throw an exception if the reader's column is
        /// not a series column.</para>
        /// </remarks>
        public abstract object GetValue(IRow row, ITime time);

        /// <summary>
        /// Reads the row's value in a series column and returns it as an
        /// object.
        /// </summary>
        /// <param name="row">The index of the row to read.</param>
        /// <param name="sample">The sample index at which to read.</param>
        /// <returns>The value as an object, or null if the value is missing,
        /// empty, or not defined.</returns>
        /// <remarks>
        /// <para>This method will throw an exception if the reader's column is
        /// not a series column.</para>
        /// </remarks>
        public abstract object GetValue(int row, int sample);

        /// <summary>
        /// Returns true if the reader's column is a series.
        /// </summary>
        public abstract bool IsSeries { get; }


        private sealed class CustomColumnReader : ColumnReader
        {
            private readonly ICustomColumn column;

            public CustomColumnReader(ICustomColumn column)
            {
                this.column = column;
            }

            public override object GetValue(IRow row)
            {
                return column.GetCustomValue(row);
            }

            public override object GetValue(int row)
            {
                return column.GetCustomValue(row);
            }

            public override object GetValue(IRow row, ITime time)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override object GetValue(int row, int sample)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }

        private sealed class NumericColumnReader : ColumnReader
        {
            private readonly INumericColumn column;
            private readonly INumericTimeseriesColumn series;

            public NumericColumnReader(INumericColumn column)
            {
                this.column = column;
                series = column as INumericTimeseriesColumn;
            }

            public override object GetValue(IRow row)
            {
                return NumericValue.ToObject(column.GetNumericValue(row));
            }

            public override object GetValue(int row)
            {
                return NumericValue.ToObject(column.GetNumericValue(row));
            }

            public override object GetValue(IRow row, ITime time)
            {
                return NumericValue.ToObject(
                    series.GetNumericValue(row, time));
            }

            public override object GetValue(int row, int sample)
            {
                return NumericValue.ToObject(
                    series.GetNumericValue(row, sample));
            }

            public override bool IsSeries
            {
                get { return series != null; }
            }
        }

        private sealed class TextColumnReader : ColumnReader
        {
            private readonly ITextColumn column;

            public TextColumnReader(ITextColumn column)
            {
                this.column = column;
            }

            public override object GetValue(IRow row)
            {
                return column.GetTextValue(row);
            }

            public override object GetValue(int row)
            {
                return column.GetTextValue(row);
            }

            public override object GetValue(IRow row, ITime time)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override object GetValue(int row, int sample)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }

        private sealed class TextSeriesColumnReader : ColumnReader
        {
            private readonly ITextTimeseriesColumn column;

            public TextSeriesColumnReader(ITextTimeseriesColumn column)
            {
                this.column = column;
            }

            public override object GetValue(IRow row)
            {
                return Exceptions.TableColumnIsSeries(column.Name);
            }

            public override object GetValue(int row)
            {
                return Exceptions.TableColumnIsSeries(column.Name);
            }

            public override object GetValue(IRow row, ITime time)
            {
                return column.GetTextValue(row, time);
            }

            public override object GetValue(int row, int sample)
            {
                return column.GetTextValue(row, sample);
            }

            public override bool IsSeries
            {
                get { return true; }
            }
        }

        private sealed class TimeColumnReader : ColumnReader
        {
            private readonly ITimeColumn column;

            public TimeColumnReader(ITimeColumn column)
            {
                this.column = column;
            }

            public override object GetValue(IRow row)
            {
                return TimeValue.ToObject(column.GetTimeValue(row));
            }

            public override object GetValue(int row)
            {
                return TimeValue.ToObject(column.GetTimeValue(row));
            }

            public override object GetValue(IRow row, ITime time)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override object GetValue(int row, int sample)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }
    }
}
