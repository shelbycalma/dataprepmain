﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    // TODO: Would love to have this implemented on the column's
    // IColumnMetaData rather than on the column itself.
    public interface IMutableFormatString
    {
        string Format { get; set; }
    }
}
