using System.Runtime.InteropServices;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// A structure layout (equivalent to a C/C++ union) which overlaps
    /// a double with a ulong, then exposes pieces of the ulong's bits.
    /// This is useful for doing certain tricks when comparing double values
    /// for near equality.
    /// </summary>
    [StructLayout(LayoutKind.Explicit)]
    internal struct DoubleLayout
    {
        private const ulong signBitMask = 0x8000000000000000L;
        private const int exponentShift = 52;
        private const uint exponentMask = 0x7FF;
        private const ulong mantissaMask = 0xFFFFFFFFFFFFFL;

        [FieldOffset(0)]
        private double v;               // The value as a double.

        [FieldOffset(0)]
        public readonly ulong bits;     // The value as an unsigned long.

        public DoubleLayout(double v)
        {
            bits = 0L;
            this.v = v;
        }

        public bool Negative { get { return (bits & signBitMask) != 0; } }

        public long Mantissa
        {
            get
            {
                return (long)(bits & mantissaMask);
            }
        }

        public int Exponent { get { return (int)((bits >> exponentShift) & exponentMask); } }

        public double Value
        {
            get { return v; }
            set { v = value; }
        }

        public long Bits
        {
            get
            {
                return (long)(bits & ~signBitMask);
            }
        }
    }
}