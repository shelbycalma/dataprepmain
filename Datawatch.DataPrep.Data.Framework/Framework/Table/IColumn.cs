
namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents a column from a tabular data source.
    /// </summary>
    /// <remarks>
    /// <para>Columns can indicate their types by implementing various
    /// additional interfaces (see for example <see cref="ITextColumn"/> and
    /// <see cref="ITimeseriesColumn"/>). A single column can implement any
    /// number of interfaces at the same time.</para>
    /// <para>The <see cref="MetaData"/> property can give additional
    /// information on the column.</para>
    /// </remarks>
    public interface IColumn
    {
        /// <summary>
        /// Gets the column's meta data if any.
        /// </summary>
        /// <remarks>
        /// <para>A table implementation can return null if it does not provide
        /// meta data for the column.</para>
        /// </remarks>
        IColumnMetaData MetaData { get; }

        /// <summary>
        /// Gets the column's name.
        /// </summary>
        /// <remarks>
        /// <para>The name of the column acts as identifier and is unique among
        /// columns from the same table.</para>
        /// <para>Columns may have a more user-friendly name that should be
        /// used for presentation, see
        /// <see cref="IColumnMetaData.Title"/>.</para>
        /// </remarks>
        string Name { get; }

        // Added this property against better judgement to allow code
        // to check for "mixing" of tables where not allowed. If it can be
        // removed later, great! For now, implement it explicitly so you will
        // get errors if the property is later removed from the interface.
        /// <summary>
        /// Gets the table that the column belongs to.
        /// </summary>
        ITable Table { get; }
    }
}
