﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    // NOTE: This is not really a bucket column, but "sort of". At least it
    // very often behaves like one. NOT having this extend bucket would be
    // tricky, as a lot of the code assumes that all user-defined columns
    // are bucket columns.    
    public interface IAutoKeyColumnMetaData : IBucketColumnMetaData
    {
    }
}
