﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// An extended version of the <see cref="IColumnMetaData"/> interface that 
    /// expose methods to set the Hidden and Title properties.
    /// </summary>
    public interface IMutableColumnMetaData : IColumnMetaData
    {
        /// <summary>
        /// Gets a boolean value indicating whether
        /// this column is hidden or not.
        /// </summary>
        /// <remarks>
        /// <para>A hidden column will not be displayed in any 
        /// listing of columns provided by SDK controls.</para>
        /// </remarks>
        new bool Hidden { get; set; }

        /// <summary>
        /// Gets the title of the column.
        /// </summary>
        /// <remarks>
        /// <para>The title can be more descriptive than the column's
        /// <see cref="IColumn.Name"/>, and does not need to be unique. Always
        /// display the title to users rather than the name if there is
        /// one.</para>
        /// </remarks>
        new string Title { get; set; }
    }
}
