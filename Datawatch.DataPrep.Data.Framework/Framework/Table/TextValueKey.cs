﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// A key for table <see cref="ITextColumn"/> values.
    /// </summary>
    public sealed class TextValueKey : IValueKey<string>
    {
        /// <summary>
        /// The value that the key represents.
        /// </summary>
        private readonly string value;

        /// <summary>
        /// Creates a new key to wrap a text value.
        /// </summary>
        /// <param name="value">The key's text value.</param>
        public TextValueKey(string value)
        {
            this.value = value;
        }

        /// <summary>
        /// Gets the key's value.
        /// </summary>
        public string Value
        {
            get { return value; }
        }

        /// <summary>
        /// True if the key's value is empty (null or an empty string).
        /// </summary>
        public bool IsEmpty
        {
            get { return string.IsNullOrEmpty(value); }
        }

        /// <summary>
        /// Checks if this key is equal to another text value key.
        /// </summary>
        /// <param name="other">The text value key to compare against.</param>
        /// <returns>True if both keys are equal (both empty, or have the
        /// same value).</returns>
        public bool Equals(TextValueKey other)
        {
            if (other == null) {
                return false;
            }
            if (ReferenceEquals(other, this)) {
                return true;
            }
            if (IsEmpty) {
                return other.IsEmpty;
            }
            if (other.IsEmpty) {
                return false;
            }
            return value.Equals(other.Value);
        }

        public bool Equals(IKey other)
        {
            return Equals(other as TextValueKey);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as TextValueKey);
        }

        public override int GetHashCode()
        {
            return IsEmpty ? 0 : value.GetHashCode();
        }

        public override string ToString()
        {
            if (IsEmpty) {
                return "null";
            }
            return value;
        }
    }
}