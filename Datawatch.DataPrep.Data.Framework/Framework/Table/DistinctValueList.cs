﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Table
{

    public class DistinctValueList<T> : List<DistinctValue<T>>,
        IDistinctValues<T>
    {
        public DistinctValueList() : base()
        {            
        }

        public DistinctValueList(int capacity) : base(capacity)
        {            
        }

        public IDistinctValue<T> GetDistinctValue(int row)
        {
            if (0 <= row && row < base.Count) {
                return base[row];
            }
            return new DistinctValue<T>(default(T), 0);
        }

        public int RowCount
        {
            get { return base.Count; }
        }
    }
}
