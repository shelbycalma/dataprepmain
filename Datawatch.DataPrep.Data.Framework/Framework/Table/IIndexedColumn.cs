﻿using Datawatch.DataPrep.Data.Framework.Storage;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by <see cref="IColumn"/>s that can map their row values to
    /// integer keys (as a <see cref="KeyIndex"/>).
    /// </summary>
    public interface IIndexedColumn
    {
        /// <summary>
        /// Set if the column can calculate and return indices.
        /// </summary>
        /// <remarks>
        /// <para>Tables may act as facades for other tables, whose columns may
        /// or may not implement <see cref="IIndexedColumn"/>. This flag lets
        /// columns change their interface support at runtime.</para>
        /// <para>This flag only applies to the <see cref="GetIndex(KeyIndex)"/> 
        /// and <see cref="GetIndex(StorageInt, int, KeyIndex)"/> methods,
        /// the <see cref="IsIndexed"/> and <see cref="IsKey"/> properties
        /// should always be valid.</para>
        /// </remarks>
        bool CanGetIndex { get; }

        /// <summary>
        /// Returns a <see cref="KeyIndex"/> for the column, which contains
        /// unique integer identifiers for all row values.
        /// </summary>
        /// <param name="index">An existing key index that can be reused by the
        /// method (if null, a new object will be created).</param>
        /// <returns>A new index, or the <paramref name="index"/> argument
        /// reused and updated.</returns>
        KeyIndex GetIndex(KeyIndex index);

        /// <summary>
        /// Returns a <see cref="KeyIndex"/> for the column, which contains
        /// unique integer identifiers for a particular set of row.
        /// </summary>
        /// <param name="rows">Rows (as indicated by their index, or position
        /// in the table) to include in the index.</param>
        /// <param name="count">Number of entries in the <paramref name="rows"/>
        /// list.</param>
        /// <param name="index">An existing key index that can be reused by the
        /// method (if null, a new object will be created).</param>
        /// <returns>A new index, or the <paramref name="index"/> argument
        /// reused and updated.</returns>
        KeyIndex GetIndex(StorageInt rows, int count, KeyIndex index);

        /// <summary>
        /// This flag is set if the column is currently using an indexed type
        /// of storage.
        /// </summary>
        bool IsIndexed { get; }

        /// <summary>
        /// Indicates that the column should be treated as a primary key
        /// column.
        /// </summary>
        /// <remarks>
        /// <para>A column is free to set this property to true, but still have
        /// duplicate values. In that case, all rows should be treated as
        /// different regardless of their actual value.</para>
        /// </remarks>
        bool IsKey { get; }
    }
}
