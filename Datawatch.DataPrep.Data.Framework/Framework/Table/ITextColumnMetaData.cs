using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Extends an <see cref="IColumnMetaData"/> with information relevant
    /// for text columns.
    /// </summary>
    public interface ITextColumnMetaData : IColumnMetaData
    {
        /// <summary>
        /// A number that describes "how many different values" there are in
        /// the column.
        /// </summary>
        /// <remarks>
        /// <para>This is a number greater than or equal to one (>= 1). Simply
        /// put, it could be the number of rows in a table divided by the number
        /// of disctinct values that occurs in the column. A cardinality of one
        /// (1) means that all rows have distinct values.</para>
        /// <para>An implementation should return zero (0) to indicate that it
        /// cannot provide the information.</para>
        /// <para>This property is part of the definition of the column and is
        /// independent of the actual data that the table contains. The
        /// property can have a value even if there is no data in the table, but
        /// any data that is in table must not validate the property.</para>
        /// </remarks>
        double Cardinality { get; }

        /// <summary>
        /// A comparer that can be used to compare values from this column.
        /// </summary>
        /// <remarks>
        /// <para>An implementation should return <c>null</c> to indicate that
        /// it cannot provide a comparer for the column.</para>
        /// <para>Note that if the column can contain null values, this
        /// comparer must be able to handle that.</para>
        /// <para>The SDK will use this comparer unless it is overridden by
        /// some other ordering mechanism (e.g. an <see
        /// cref="Datawatch.DataPrep.Data.Framework.Framework.Variables.IBreakdownVariable"/>
        /// that implements <see
        /// cref="Datawatch.DataPrep.Data.Framework.Framework.Variables.IOrderingVariableClass"/>).</para>
        /// </remarks>
        IComparer<string> Comparer { get; }

        /// <summary>
        /// All possible values for this column.
        /// </summary>
        /// <remarks>
        /// <para>An implementation should return <c>null</c> to indicate
        /// that it cannot provide the information - returning an empty array
        /// means that there are no values that cells can assume.</para>
        /// <para>The order of the strings in the array is significant in
        /// some cases.</para>
        /// <para>This property is part of the definition of the column and is
        /// independent of the actual data that the table contains. The
        /// property can have a value even if there is no data in the table, but
        /// any data that is in table must not invalidate the property.</para>
        /// </remarks>
        string[] Domain { get; }
    }
}
