using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implements an empty <see cref="ITable"/>.
    /// </summary>
    public sealed class EmptyTable : ITable
    {
        /// <summary>
        /// The singleton instance of this class.
        /// </summary>
        public static readonly EmptyTable Instance = new EmptyTable();

        // To disable warnings about events not being used.
        #pragma warning disable 0067

        /// <summary>
        /// This event will never fire since the table cannot change.
        /// </summary>
        public event EventHandler<TableChangingEventArgs> Changing;

        /// <summary>
        /// This event will never fire since the table cannot change.
        /// </summary>
        public event EventHandler<TableChangedEventArgs> Changed;

        #pragma warning restore 0067


        private EmptyTable()
        {
        }

        /// <summary>
        /// Returns zero (0) for the number of columns.
        /// </summary>
        public int ColumnCount
        {
            get { return 0; }
        }

        /// <summary>
        /// It is invalid to call this method because there will never be any
        /// columns in the table so all indices are invalid.
        /// </summary>
        /// <param name="index">Index of column to retrieve.</param>
        /// <returns>Does not return anything.</returns>
        public IColumn GetColumn(int index)
        {
            throw new InvalidOperationException();
        }

        /// <summary>
        /// This method always returns <c>null</c> for the empty table (there
        /// are no columns in it).
        /// </summary>
        /// <param name="name">The name of the column to retrieve.</param>
        /// <returns>Always <c>null</c>.</returns>
        public IColumn GetColumn(string name)
        {
            if (name == null) {
                throw Exceptions.ArgumentNull("name");
            }
            return null;
        }

        /// <summary>
        /// It is invalid to call this method because there will never be any
        /// rows in the table so all indices are invalid.
        /// </summary>
        /// <param name="index">Index of row to retrieve.</param>
        /// <returns>Does not return anything.</returns>
        public IRow GetRow(int index)
        {
            return null;
        }

        /// <summary>
        /// This will always be false since the table cannot change.
        /// </summary>
        public bool IsUpdating
        {
            get { return false; }
        }

        /// <summary>
        /// Returns zero (0) for the number of rows.
        /// </summary>
        public int RowCount
        {
            get { return 0; }
        }
    }
}
