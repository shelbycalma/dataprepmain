namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by time series columns that have the <see cref="double"/>
    /// data type.
    /// </summary>
    public interface INumericTimeseriesColumn :
        INumericColumn, ITimeseriesColumn
    {
        /// <summary>
        /// Returns the cell value in the specified row and at the specified
        /// sample time.
        /// </summary>
        /// <param name="row">The row to read the value from.</param>
        /// <param name="time">The sample time to read the value from.</param>
        /// <returns>The cell value as a double.</returns>
        /// <remarks>
        /// <para>A data source will indicate a missing value by returning
        /// <see cref="NumericValue.Empty"/>. Use
        /// <see cref="NumericValue.IsEmpty(double)"/> to test for this.</para>
        /// </remarks>
        double GetNumericValue(IRow row, ITime time);

        /// <summary>
        /// Returns the cell value in the specified row and at the specified
        /// sample.
        /// </summary>
        /// <param name="row">The row index to read the value from.</param>
        /// <param name="sample">The sample index to read the value
        /// from.</param>
        /// <returns>The cell value as a double.</returns>
        /// <remarks>
        /// <para>A data source will indicate a missing value by returning
        /// <see cref="NumericValue.Empty"/>. Use
        /// <see cref="NumericValue.IsEmpty(double)"/> to test for this.</para>
        /// </remarks>
        double GetNumericValue(int row, int sample);
    }
}
