﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    public class DistinctValue<T> : IDistinctValue<T>
    {
        private readonly T value;
        private readonly int count;

        public DistinctValue(T value, int count)
        {
            this.value = value;
            this.count = count;
        }

        public T Value
        {
            get { return value; }
        }

        public int Count
        {
            get { return count; }
        }
    }
}
