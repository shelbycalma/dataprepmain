using System.Diagnostics;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents an interval between two <see cref="ITime">sample times</see>.
    /// </summary>
    public class TimeInterval
    {
        private enum IntervalType
        {
            Undefined = 0,
            All = 1
        }

        /// <summary>
        /// A constant that represents an "undefined" interval.
        /// </summary>
        /// <remarks>
        /// <para>Both end points (<see cref="Min"/> and <see cref="Max"/>) of
        /// this constant are null.</para>
        /// </remarks>
        public static readonly TimeInterval
            Undefined = new TimeInterval(IntervalType.Undefined);

        /// <summary>
        /// A constant that represents all possible time.
        /// </summary>
        /// <remarks>
        /// Min equals <see cref="ReservedTime.MinValue"/> while 
        /// max equals <see cref="ReservedTime.MaxValue"/>.
        /// </remarks>
        public static readonly TimeInterval 
            All = new TimeInterval(IntervalType.All);

        private readonly ITimeseriesTable table;
        private readonly ITime min;
        private readonly ITime max;
        
        private TimeInterval(IntervalType type)
        {
            table = null;

            switch (type)
            {
                case IntervalType.Undefined:
                    min = null;
                    max = null;
                    break;
                case IntervalType.All:
                    min = ReservedTime.MinValue;
                    max = ReservedTime.MaxValue;
                    break;
            }
        }

        /// <summary>
        /// Creates a new instance of the <see cref="TimeInterval"/> class.
        /// </summary>
        /// <param name="table">The table that the end points come from.</param>
        /// <param name="min">The minimum/lower end point of the
        /// interval.</param>
        /// <param name="max">The maximum/upper end point of the
        /// interval.</param>
        /// <remarks>
        /// <para>One of the end points (but not both) may be left out to
        /// indicate an open-ended interval. If both end points are given, their
        /// time instants (see <see cref="ITime.Time"/>) must be in the correct
        /// order.</para>
        /// </remarks>
        public TimeInterval(ITimeseriesTable table, ITime min, ITime max)
        {
            if (table == null) {
                throw Exceptions.ArgumentNull("table");
            }
            if (min == null && max == null) {
                throw Exceptions.TimeIntervalEmptyEndpoints();
            }
            if (ReservedTime.Contains(min) || ReservedTime.Contains(max)) {
                throw Exceptions.TimeIntervalReservedEndpoints();
            }
            if (min != null && max != null) {
                if (min.Time > max.Time) {
                    throw Exceptions.TimeIntervalReversedEndpoints();
                }
            }

            this.table = table;
            this.min = min;
            this.max = max;
        }

        /// <summary>
        /// Checks if a specific <see cref="ITime"/> is in the interval.
        /// </summary>
        /// <param name="value">Sample time to test.</param>
        /// <returns>True if the time is in the interval.</returns>
        /// <remarks>
        /// <para>This method regards the interval as closed, i.e. the test
        /// performed is (min &lt;= value &amp;&amp; value &lt;= max).</para>
        /// <para>Special cases: if <paramref name="value"/> is null the method
        /// returns <c>false</c>, if the interval is
        /// <see cref="TimeInterval.All"/> the method returns <c>true</c> for
        /// any non-null <paramref name="value"/>, if the interval is
        /// <see cref="TimeInterval.Undefined"/> the method always returns
        /// <c>false</c>.</para>
        /// </remarks>
        public bool Contains(ITime value)
        {
            if (value == null) {
                return false;
            }
            if (object.ReferenceEquals(this, TimeInterval.Undefined)) {
                return false;
            }
            if (object.ReferenceEquals(this, TimeInterval.All)) {
                return true;
            }

            Debug.Assert(min != null || max != null);
            
            if (min != null && value.Time < min.Time) {
                return false;
            }
            if (max != null && value.Time > max.Time) {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Returns true if the time interval is equal to the specified object
        /// (it is a time interval and both are the same predefined interval,
        /// or both endpoints are the same).
        /// </summary>
        public override bool Equals(object obj)
        {
            TimeInterval interval = obj as TimeInterval;
            if (interval == null) {
                return false;
            }
            if (ReferenceEquals(this, TimeInterval.Undefined)) {
                return ReferenceEquals(interval, TimeInterval.Undefined);
            }
            if (ReferenceEquals(this, TimeInterval.All)) {
                return ReferenceEquals(interval, TimeInterval.All);
            }
            Debug.Assert(table != null);
            Debug.Assert(min != null || max != null);
            return table == interval.table &&
                min == interval.min && max == interval.max;
        }

        /// <summary>
        /// Tests if two time intervals are equal (both are null, or else
        /// satisfy <see cref="TimeInterval.Equals(object)"/>).
        /// </summary>
        public static bool Equals(TimeInterval x, TimeInterval y)
        {
            if (x == null) {
                return y == null;
            }
            return x.Equals(y);
        }

        /// <summary>
        /// Returns the hashcode that indentifies this <see cref="TimeInterval">interval</see>.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            int code = 0;
            if (table != null) {
                code ^= table.GetHashCode();
            }
            if (min != null) {
                code ^= min.GetHashCode();
            }
            if (max != null) {
                code ^= max.GetHashCode();
            }
            return code;
        }

        /// <summary>
        /// Gets the maximum/upper end point of the interval or null if the
        /// upper end is open.
        /// </summary>
        public ITime Max
        {
            get { return max; }
        }

        /// <summary>
        /// Gets the minimum/lower end point of the interval or null if the
        /// lower end is open.
        /// </summary>
        public ITime Min
        {
            get { return min; }
        }

        /// <summary>
        /// Gets the table that this interval's end points belog to (or
        /// <c>null</c> if it is <see cref="Undefined"/> or <see cref="All"/>).
        /// </summary>
        public ITimeseriesTable Table
        {
            get { return table; }
        }

        /// <summary>
        /// Defines equality between <see cref="TimeInterval">intervals</see>.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static bool operator ==(TimeInterval x, TimeInterval y)
        {
            if (ReferenceEquals(x, null)) {
                return ReferenceEquals(y, null);
            }
            return x.Equals(y);
        }

        /// <summary>
        /// Returns false if x == y, otherwise, returns true.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static bool operator !=(TimeInterval x, TimeInterval y)
        {
            return !(x == y);
        }

    }
}
