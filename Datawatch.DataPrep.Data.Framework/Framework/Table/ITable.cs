using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Defines a tabular data source.
    /// </summary>
    /// <remarks>
    /// <para>More specialized versions of this interface are available, e.g.
    /// <see cref="ITimeseriesTable"/>. This interface describes the
    /// minimum requirements on the data source.</para>
    /// </remarks>
    public interface ITable
    {
        /// <summary>
        /// Fired by a <see cref="ITable"/> to indicate that it has entered
        /// batch update mode.
        /// </summary>
        event EventHandler<TableChangingEventArgs> Changing;

        /// <summary>
        /// Fired by a <see cref="ITable"/> at the end of an update to
        /// indicate that all events have been fired.
        /// </summary>
        event EventHandler<TableChangedEventArgs> Changed;

        /// <summary>
        /// Gets the number of columns in the table.
        /// </summary>
        int ColumnCount { get; }

        /// <summary>
        /// Returns the column at the specified index.
        /// </summary>
        /// <param name="index">Index of column to retrieve.</param>
        /// <returns>A column implementation.</returns>
        IColumn GetColumn(int index);

        /// <summary>
        /// Returns the column with the specified name.
        /// </summary>
        /// <param name="name">The column's name.</param>
        /// <returns>The column if it was found in the table, <c>null</c>
        /// otherwise.</returns>
        IColumn GetColumn(string name);

        /// <summary>
        /// Returns the row at the specified index.
        /// </summary>
        /// <param name="index">Index of row to retrieve.</param>
        /// <returns>A row implementation.</returns>
        IRow GetRow(int index);

        /// <summary>
        /// Used to test if a <see cref="ITable"/> is currently in a
        /// Begin/EndUpdate block (the <see cref="Changing"/> event has been
        /// fired but not yet the <see cref="Changed"/> event).
        /// </summary>
        /// <remarks>
        /// <para>The value of this property in a handler for the
        /// <see cref="Changed"/> event should be <c>true</c>.</para>
        /// </remarks>
        bool IsUpdating { get; }

        /// <summary>
        /// Gets the number of rows in the table.
        /// </summary>
        int RowCount { get; }
    }
}
