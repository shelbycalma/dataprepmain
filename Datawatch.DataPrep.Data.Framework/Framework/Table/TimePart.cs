using System.Runtime.Serialization;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Defines different units in which the time axis can be divided.
    /// </summary>
    [DataContract(Namespace = "http://schemas.panopticon.com/2008/12/Dashboards/Model")]
    public enum TimePart
	{
        /// <summary>
        /// Represents an invalid time unit.
        /// </summary>
        [EnumMember]
        Invalid = -1,

        /// <summary>
        /// Represents division into decades, e.g. 2010, 2020, 2030, ...
        /// </summary>
        [EnumMember]
        Decade = 0,

        /// <summary>
        /// Represents division into years, e.g. 2011, 2012, 2013, ...
        /// </summary>
        [EnumMember]
        Year = 1,

        /// <summary>
        /// Represents division into quarters; Q1, Q2, Q3, and Q4.
        /// </summary>
        [EnumMember]
        Quarter = 2,

        /// <summary>
        /// Represents division into months, e.g. January, February, ...
        /// </summary>
        [EnumMember]
        Month = 3,

        /// <summary>
        /// Represents division based on day-of-week, e.g. Monday, Tuesday, ...
        /// </summary>
        [EnumMember]
        Weekday = 4,

        /// <summary>
        /// Represents division based on day-of-month, e.g. 1, 2, ..., 31.
        /// </summary>
        [EnumMember]
        Day = 5,

        /// <summary>
        /// Represents division into hours, e.g. 0, 1, ..., 23.
        /// </summary>
        [EnumMember]
        Hour = 6,

        /// <summary>
        /// Represents division into minutes, e.g. 0, 1, ..., 59.
        /// </summary>
        [EnumMember]
        Minute = 7,

        /// <summary>
        /// Represents division into seconds, e.g. 0, 1, ..., 59.
        /// </summary>
        [EnumMember]
        Second = 8,

        /// <summary>
        /// Represents division into milliseconds, e.g. 0, 1, ..., 999.
        /// </summary>
        [EnumMember]
        Millisecond = 9
    }
}
