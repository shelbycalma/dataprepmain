﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Event data for the <see cref="ITable.Changing"/> event.
    /// </summary>
    /// <remarks>
    /// <para>This class is empty now, but may be extended in the future to
    /// include information on the specific changes that will be made to
    /// the <see cref="ITable"/> during the batch.</para>
    /// </remarks>
    public class TableChangingEventArgs : EventArgs
    {
	    /// <summary>
	    /// Represents an event with no specific data available about what
        /// will change.
	    /// </summary>
        public new static readonly TableChangingEventArgs
            Empty = new TableChangingEventArgs();

        private TableChangingEventArgs()
        {
        }
    }
}
