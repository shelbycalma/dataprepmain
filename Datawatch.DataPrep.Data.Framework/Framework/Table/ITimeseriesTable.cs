namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by tabular data sources that contain time series.
    /// </summary>
    /// <remarks>
    /// <para>A table that implements this interface can have zero or more
    /// <see cref="ITimeseriesColumn"/>s where each cell is a series of values.
    /// All series in one table are defined at the same sample times which
    /// can be accessed through this interface.</para>
    /// </remarks>
    public interface ITimeseriesTable : ITable
    {
        /// <summary>
        /// Gets the number of sample points for all time series in the table.
        /// </summary>
        int TimeCount { get; }

        /// <summary>
        /// Gets the sample time at the specified index.
        /// </summary>
        /// <param name="index">Index of sample time to retrieve.</param>
        /// <returns>A sample time object.</returns>
        /// <remarks>
        /// <para>The samples returned by this method <em>must</em> be in
        /// increasing order, i.e. the following must always hold:
        /// <c>GetTime(i).Time &lt; GetTime(i + 1).Time</c>.</para>
        /// </remarks>
        ITime GetTime(int index);

        /// <summary>
        /// Provides a convenient place to store a "table scoped" snapshot.
        /// </summary>
        /// <remarks>
        /// <para>This property is for future use. Neither the
        /// <see cref="Hierarchy"/> nor the <see cref="StandaloneState"/>
        /// currently use it.</para>
        /// </remarks>
        ITime SnapshotTime { get; }
    }
}
