﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents a set of distinct values as returned by
    /// <see cref="IDistinctValuesMetaData{T}.GetDistinctValues(bool, int)"/>.
    /// </summary>
    /// <typeparam name="T">The type of the values (and column)</typeparam>
    public interface IDistinctValues<out T>
    {
        /// <summary>
        /// Gets the value (and count) at the specific row.
        /// </summary>
        /// <param name="row">The row index (zero up to
        /// <see cref="RowCount"/>).</param>
        IDistinctValue<T> GetDistinctValue(int row);

        /// <summary>
        /// Gets the number of distinct values available (less or equal to the
        /// number requested in the
        /// <see cref="IDistinctValuesMetaData{T}.GetDistinctValues(bool, int)"/>
        /// call.
        /// </summary>
        int RowCount { get; }
    }
}
