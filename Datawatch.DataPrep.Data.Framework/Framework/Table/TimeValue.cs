using System;
using System.Globalization;

namespace Datawatch.DataPrep.Data.Framework.Table
{
	/// <summary>
	/// This class defines useful constants and centralizes all calculations
	/// with time values to one place.
	/// </summary>
	/// <remarks>
	/// <para>Empty or missing ("n/a") values play an important part in the
	/// data model. This class defines how empty values are stored, how to test
	/// for empty values and how to calculate with empty values.</para>
	/// <note>Use the <see cref="TimeValue.Empty"/> constant whenever you need
    /// to specify an empty value. Likewise, use the
    /// <see cref="TimeValue.IsEmpty"/> method to test for empty values. Do not
    /// use the internal representation, because there is no guarantee that it
    /// will stay the same in the future.</note>
	/// </remarks>
	public static class TimeValue
	{
		/// <summary>
		/// Defines an empty or missing time value.
		/// </summary>
		/// <remarks>
		/// <para>Use this constant whenever you need to explicitly inform the
		/// data model that a time value is missing, as in
		/// <c>item.TimeValues[measure] = TimeValue.Empty</c>.</para>
		/// <para>To test for a missing value, don't use this constant (do not
		/// write <c>item.TimeValues[measure] == TimeValue.Empty</c>) - use the
		/// <see cref="TimeValue.IsEmpty"/> method instead:
        /// <c>TimeValue.IsEmpty(item.TimeValues[measure])</c>.</para>
		/// </remarks>
		public static readonly DateTime Empty = DateTime.MinValue;

        /// <summary>
        /// The <see cref="DateTime.Ticks"/> value for the <see cref="Empty"/>
        /// value, as a constant.
        /// </summary>
        public const long EmptyTicks = 0;

		/// <summary>
		/// The culture to use when formatting and parsing time values to and
		/// from strings.
		/// </summary>
		/// <remarks>
		/// <para>This field will be initialized to the current culture (see
		/// <see cref="CultureInfo.CurrentCulture"/>), but can be modified if
		/// another culture should be used.</para>
		/// </remarks>
		public static CultureInfo Culture = CultureInfo.CurrentCulture;

		/// <summary>
		/// The string that should represent empty time values when formatting.
		/// </summary>
		/// <remarks>
		/// <para>This field is initially "n/a", but can be modified if
		/// necessary.</para>
		/// </remarks>
		public static string EmptyText =
            Properties.Resources.UiTimeValueEmptyText;

		/// <summary>
		/// Fallback culture when parsing time values.
		/// </summary>
		private static readonly CultureInfo EnglishCulture =
			CultureInfo.CreateSpecificCulture("en");


		/// <summary>
		/// Compares two time values and takes empty values into account.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>A negative integer if <paramref name="x"/> is less than
		/// <paramref name="y"/>, a positive integer if <paramref name="x"/> is
		/// greater than <paramref name="y"/>, or zero if they are
		/// equal.</returns>
		/// <remarks>
		/// <para>Empty values are considered to be less than any other values,
		/// which will be reflected in the return value.</para>
		/// <para>Two empty values are considered to be equal, i.e.
		/// <c>Compare(TimeValue.Empty, TimeValue.Empty) == 0</c>.</para>
		/// </remarks>
		public static int Compare(DateTime x, DateTime y)
		{
			if (x == DateTime.MinValue) {
				return (y == DateTime.MinValue ? 0 : -2);
			}
			if (y == DateTime.MinValue) {
				return 2;
			}
			return (x == y ? 0 : (x < y ? -1 : 1));
		}


        /// <summary>
        /// Helper method that converts objects to time values for use in
        /// <see cref="ITable"/> implementations.
        /// </summary>
        /// <param name="value">The value to convert (unbox).</param>
        /// <returns>The unboxed value, with null replaced with
        /// <see cref="TimeValue.Empty"/>.</returns>
        public static DateTime FromObject(object value)
        {
            if (value == null) {
                return TimeValue.Empty;
            }
            if (value is DateTime) {
                return (DateTime) value;
            }
            return Convert.ToDateTime(value);
        }


		/// <summary>
		/// Tests for an empty time value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns><c>true</c> if the value is empty, <c>false</c>
		/// otherwise.</returns>
		public static bool IsEmpty(DateTime x)
		{
			return x == DateTime.MinValue;
		}


		/// <summary>
		/// Tests two time values for equality.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns><c>true</c> if the values are equal, <c>false</c>
		/// otherwise.</returns>
		/// <remarks>
		/// <para>An empty value is only considered to be equal to another
		/// empty value.</para>
		/// </remarks>
		public static bool IsEqual(DateTime x, DateTime y)
		{
			if (x == DateTime.MinValue) return y == DateTime.MinValue;
			if (y == DateTime.MinValue) return false;
			return x == y;
		}


		/// <summary>
		/// Returns the larger of two time values.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>The largest argument.</returns>
		/// <remarks>
		/// <para>See the <see cref="TimeValue.Compare"/> method
		/// for definitions of how empty values are compared to other
		/// values.</para>
		/// </remarks>
		public static DateTime Max(DateTime x, DateTime y)
		{
			if (x == DateTime.MinValue) return y;
			if (y == DateTime.MinValue) return x;
			return x > y ? x : y;
		}


		/// <summary>
		/// Returns the smaller of two time values.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>The smallest argument.</returns>
		/// <remarks>
		/// <para><c>Min(TimeValue.Empty, y) == y</c> (where <c>y</c> is any
		/// value, including empty)</para>
		/// <para><c>Min(x, TimeValue.Empty) == x</c> (where <c>x</c> is any
		/// value, including empty)</para>
		/// <note>Even though the <see cref="TimeValue.Compare"/> method
        /// considers an empty value to be smaller than any other non-empty
		/// value, this method returns the non-empty value if the other value
		/// is empty.</note>
		/// </remarks>
		public static DateTime Min(DateTime x, DateTime y)
		{
			if (x == DateTime.MinValue) return y;
			if (y == DateTime.MinValue) return x;
			return x < y ? x : y;
		}


		/// <summary>
		/// Converts an <see cref="object"/> to a time value.
		/// </summary>
		/// <param name="v">A type that can be converted to a time
		/// value.</param>
		/// <returns>The time value equivalent of the argument.</returns>
		/// <exception cref="FormatException">Thrown if the argument could not
		/// be converted to a time value.</exception>
		/// <remarks>
		/// <para>If the argument is <c>null</c> or the <see cref="string"/>
		/// specified in <see cref="TimeValue.EmptyText"/> this method returns
		/// <see cref="TimeValue.Empty"/>.</para>
		/// <para>String representations of values are converted using the
		/// culture specified in <see cref="TimeValue.Culture"/>.</para>
		/// </remarks>
		public static DateTime Parse(object v)
		{
			if (v == null || (v is string && EmptyText.Equals(v))) {
				return Empty;
			}
			try {
				return Convert.ToDateTime(v, Culture);
			} 
			catch (FormatException) {
			}
			try {
				return Convert.ToDateTime(v, EnglishCulture);
			} 
			catch (FormatException) {
			}
			try {
				return DateTime.Parse(v.ToString(), Culture);
			}
			catch (Exception) {
			}
			try {
				return DateTime.Parse(v.ToString(), EnglishCulture);
			}
			catch (Exception) {
			}
			try {
				return DateTime.Parse(v.ToString(),
					CultureInfo.InvariantCulture);
			}
			catch (Exception) {
			}
			throw new FormatException();
		}


        /// <summary>
        /// Helper method that converts time values to objects for use in
        /// <see cref="ITable"/> implementations.
        /// </summary>
        /// <param name="value">The value to convert (box).</param>
        /// <returns>The boxed value, with <see cref="TimeValue.Empty"/>
        /// replaced with null.</returns>
        public static object ToObject(DateTime value)
        {
            return value == DateTime.MinValue ? null : (object) value;
        }

		/// <summary>
		/// Returns the string representation of a time value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns>The argument converted to a string.</returns>
		/// <remarks>
		/// <para>If the value is empty this method returns the string specified
		/// in <see cref="TimeValue.EmptyText"/>. For non-empty values, this
        /// method uses the culture specified in <see cref="TimeValue.Culture"/>
        /// to get the string representation.</para>
		/// </remarks>
		public static string ToString(DateTime x)
		{
			return x == DateTime.MinValue ? EmptyText : x.ToString(Culture);
		}
	}
}
