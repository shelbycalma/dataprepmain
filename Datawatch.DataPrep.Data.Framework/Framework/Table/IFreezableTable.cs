﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by <see cref="ITable"/>s to indicate that they can be
    /// frozen.
    /// </summary>
    /// <remarks>
    /// <para>A table that has been frozen cannot be modified any more. Some
    /// tables may optimize their internal storage when they are freezed, but
    /// before you can call <see cref="Freeze()"/> on them to do this, you need
    /// to check the <see cref="CanFreeze"/> property.</para>
    /// </remarks>
    public interface IFreezableTable : ITable
    {
        /// <summary>
        /// Indicates whether the table can currently be frozen.
        /// </summary>
        bool CanFreeze { get; }

        /// <summary>
        /// Freezes the table and makes it read-only.
        /// </summary>
        /// <remarks>
        /// <para>If you call this method when <see cref="CanFreeze"/> is
        /// <c>false</c> it will throw an exception. Nothing happens if you
        /// call the method on an already frozen table.</para>
        /// </remarks>
        void Freeze();

        /// <summary>
        /// Indicates whether the table has been frozen.
        /// </summary>
        /// <remarks>
        /// <para>If this flag is set, the table may no longer be
        /// modified.</para>
        /// </remarks>
        bool IsFrozen { get; }
    }
}
