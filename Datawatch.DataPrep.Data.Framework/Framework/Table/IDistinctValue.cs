﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents a single distinct value as returned by
    /// <see cref="IDistinctValues{T}.GetDistinctValue(int)"/>.
    /// </summary>
    /// <typeparam name="T">The type of the value (and column).</typeparam>
    public interface IDistinctValue<out T>
    {
        /// <summary>
        /// Gets the actual value (of the same type as the column).
        /// </summary>
        T Value { get; }

        /// <summary>
        /// Gets the total number of occurrences of this value in the table.
        /// </summary>
        int Count { get; }
    }
}
