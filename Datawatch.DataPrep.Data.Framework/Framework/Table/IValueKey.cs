﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Strongly typed version of <see cref="IKey"/> that allows you to read
    /// the key's value (assumes a single value, so not
    /// <see cref="CompositeKey"/>).
    /// </summary>
    /// <typeparam name="T">The type (text, number, time) that the key
    /// represents.</typeparam>
    public interface IValueKey<out T> : IKey
    {
        /// <summary>
        /// Returns the key's value.
        /// </summary>
        T Value { get; }
    }
}
