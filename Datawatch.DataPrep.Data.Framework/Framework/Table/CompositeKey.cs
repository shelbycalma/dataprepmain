﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// A <see cref="IKey"/> composed of multiple components.
    /// </summary>
    public sealed class CompositeKey : IKey
    {
        /// <summary>
        /// The component keys.
        /// </summary>
        private readonly List<IKey> keys;

        /// <summary>
        /// Creates a new composite key from its parts.
        /// </summary>
        /// <param name="keys">The component keys.</param>
        public CompositeKey(IEnumerable<IKey> keys)
        {
            this.keys = new List<IKey>(keys);
        }

        /// <summary>
        /// Returns true if at least one component key is empty.
        /// </summary>
        public bool IsEmpty
        {
            get {
                foreach (IKey key in keys) {
                    if (key.IsEmpty) {
                        return true;
                    }
                }
                return false;
            }
        }

        /// <summary>
        /// Checks if this key is equal to another composite key.
        /// </summary>
        /// <param name="other">The composite key to compare against.</param>
        /// <returns>True if both keys are equal.</returns>
        public bool Equals(CompositeKey other)
        {
            if (other == null) {
                return false;
            }
            if (ReferenceEquals(other, this)) {
                return true;
            }
            if (keys.Count != other.keys.Count) {
                return false;
            }
            for (int i = 0; i < keys.Count; i++) {
                if (!keys[i].Equals(other.keys[i]))
                    return false;
            }
            return true;
        }

        public bool Equals(IKey other)
        {
            return Equals(other as CompositeKey);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as CompositeKey);
        }

        public override int GetHashCode()
        {
            int hash = 0;
            foreach (IKey key in keys)
                hash = 17 * hash ^ key.GetHashCode();
            return hash;
        }
    }
}