
namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Implemented by columns that contain time series.
    /// </summary>
    public interface ITimeseriesColumn : IColumn
    {
    }
}
