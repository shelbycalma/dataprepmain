﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Base interface for <see cref="IValueKey{T}"/>, implemented by classes
    /// that can be part of a <see cref="CompositeKey"/>.
    /// </summary>
    public interface IKey : IEquatable<IKey>
    {
        /// <summary>
        /// Tests if the key's value is empty with the standard table data
        /// type semantics.
        /// </summary>
        /// <seealso cref="NumericValue.IsEmpty(double)"/>
        /// <seealso cref="TimeValue.IsEmpty(DateTime)"/>
        bool IsEmpty { get; }
    }
}
