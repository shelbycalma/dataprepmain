using System;
using System.Globalization;

namespace Datawatch.DataPrep.Data.Framework.Table
{
	/// <summary>
	/// This class defines useful constants and centralizes all calculations
	/// with values to one place.
	/// </summary>
	/// <remarks>
	/// <para>Empty or missing ("n/a") values play an important part in the
	/// data model. This class defines how empty values are stored, how to test
	/// for empty values and how to calculate with empty values.</para>
	/// <note>Use the <see cref="NumericValue.Empty"/> constant whenever you
    /// need to specify an empty value. Likewise, use the
    /// <see cref="NumericValue.IsEmpty"/> method to test
	/// for empty values. Do not use the internal representation, because there
	/// is no guarantee that it will stay the same in the future.</note>
	/// </remarks>
	public static class NumericValue
	{
		/// <summary>
		/// Defines an empty or missing value.
		/// </summary>
		/// <remarks>
		/// <para>Use this constant whenever you need to explicitly inform the
		/// data model that a value is missing, as in
		/// <c>node.Aggregates[function] = NumericValue.Empty</c>.</para>
		/// <para>To test for a missing value, don't use this constant - use the
		/// <see cref="NumericValue.IsEmpty"/> method instead:
		/// <c>NumericValue.IsEmpty(column.GetNumericValue(row))</c>.</para>
		/// </remarks>
		public const double Empty = double.NaN;

		/// <summary>
		/// The culture to use when formatting and parsing values to and from
		/// strings.
		/// </summary>
		/// <remarks>
		/// <para>This field will be initialized to the current culture (see
		/// <see cref="CultureInfo.CurrentCulture"/>), but can be modified if
		/// another culture should be used.</para>
		/// </remarks>
		public static CultureInfo Culture = CultureInfo.CurrentCulture;

		/// <summary>
		/// The string that should represent empty values when formatting.
		/// </summary>
		/// <remarks>
		/// <para>This field is initially "n/a", but can be modified if
		/// necessary.</para>
		/// </remarks>
		public static string EmptyText =
            Properties.Resources.UiNumericValueEmptyText;

		/// <summary>
		/// Fallback culture when parsing values.
		/// </summary>
		private static readonly CultureInfo EnglishCulture =
			CultureInfo.CreateSpecificCulture("en");


		/// <summary>
		/// Returns the absolute value of a value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns>The absolute value of the argument.</returns>
		/// <remarks>
		/// <para><c>IsEmpty(Abs(NumericValue.Empty)) == true</c></para>
		/// </remarks>
		public static double Abs(double x)
		{
			if (double.IsNaN(x)) return Empty;
			return (x < 0d ? -x : x);
		}


		/// <summary>
		/// Returns the sum of two values.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>The sum of the arguments.</returns>
		/// <remarks>
		/// <para><c>Add(NumericValue.Empty, y) == y</c>
        /// (where <c>y</c> is any value, including empty)</para>
		/// <para><c>Add(x, NumericValue.Empty) == x</c>
        /// (where <c>x</c> is any value, including empty)</para>
		/// </remarks>
		public static double Add(double x, double y)
		{
			if (double.IsNaN(x)) return y;
			if (double.IsNaN(y)) return x;
			return (x + y);
		}


		/// <summary>
		/// Compares two values and takes empty values into account.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>A negative integer if <paramref name="x"/> is less than
		/// <paramref name="y"/>, a positive integer if <paramref name="x"/> is
		/// greater than <paramref name="y"/>, or zero if they are
		/// equal.</returns>
		/// <remarks>
		/// <para>Empty values are considered to be less than any other values,
		/// which will be reflected in the return value, i.e.
		/// <c>Compare(5, 2)</c> will be less than
		/// <c>Compare(5, Value.Empty)</c>.</para>
		/// <para>Two empty values are considered to be equal, i.e.
		/// <c>Compare(NumericValue.Empty, NumericValue.Empty) == 0</c>.</para>
		/// </remarks>
		public static int Compare(double x, double y)
		{
			if (double.IsNaN(x)) {
				return (double.IsNaN(y) ? 0 : -2);
			}
			if (double.IsNaN(y)) {
				return 2;
			}
			return (x == y ? 0 : (x < y ? -1 : 1));
		}


		/// <summary>
		/// Returns the quotient of two values.
		/// </summary>
		/// <param name="x">The divident value (possibly empty).</param>
		/// <param name="y">The divisor value (possibly empty).</param>
		/// <returns>The quotient of the arguments.</returns>
		/// <remarks>
		/// <para><c>IsEmpty(Divide(NumericValue.Empty, y)) == true</c> (where
		/// <c>y</c> is any value, including empty)</para>
		/// <para><c>IsEmpty(Divide(x, NumericValue.Empty)) == true</c> (where
		/// <c>x</c> is any value, including empty)</para>
		/// <para><c>IsEmpty(Divide(x, 0d)) == true</c> (where <c>x</c> is
		/// any value, including empty)</para>
		/// <note>Please observe the behaviour in the last case described
		/// above, as it is not entirely standard.</note>
		/// </remarks>
		public static double Divide(double x, double y)
		{
			if (double.IsNaN(x) || double.IsNaN(y)) return Empty;
			if (y == 0d) return Empty;
			return (x / y);
		}


        /// <summary>
        /// Add and marked obsolete to help detect mistakes where this method
        /// is used when <see cref="IsEqual(double, double)"/> was intended.
        /// </summary>
        /// <param name="x">The first object to compare.</param>
        /// <param name="y">The second object to compare.</param>
        /// <returns>True if the objects are equal.</returns>
        /// <remarks>
        /// <para>This implementation delegates to
        /// <see cref="object.Equals(object, object)"/>, but there is no reason
        /// not to call that method directly instead.</para>
        /// </remarks>
        [Obsolete("Did you mean NumericValue.IsEqual?")]
        public new static bool Equals(object x, object y)
        {
            return object.Equals(x, y);
        }


        /// <summary>
        /// Helper method that converts objects to numeric values for use in
        /// <see cref="ITable"/> implementations.
        /// </summary>
        /// <param name="value">The value to convert (unbox).</param>
        /// <returns>The unboxed value, with null replaced with
        /// <see cref="NumericValue.Empty"/>.</returns>
        public static double FromObject(object value)
        {
            if (value == null) {
                return NumericValue.Empty;
            }
            if (value is double) {
                return (double) value;
            }
            return Convert.ToDouble(value);
        }


        /// <summary>
		/// Tests for an empty value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns><c>true</c> if the value is empty, <c>false</c>
		/// otherwise.</returns>
		public static bool IsEmpty(double x)
		{
			return double.IsNaN(x);
		}


		/// <summary>
		/// Tests two values for equality.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns><c>true</c> if the values are equal, <c>false</c>
		/// otherwise.</returns>
		/// <remarks>
		/// <para>An empty value is only considered to be equal to another
		/// empty value.</para>
		/// </remarks>
		public static bool IsEqual(double x, double y)
		{
			if (double.IsNaN(x)) return double.IsNaN(y);
			if (double.IsNaN(y)) return false;
			return (x == y);
		}

        /// <summary>
        /// Test two values for near equality.
        /// </summary>
        /// <param name="x">A (possibly empty) value.</param>
        /// <param name="y">A (possibly empty) value.</param>
        /// <returns><c>true</c> if the values are nearly equal, <c>false</c>
        /// otherwise.</returns>
        /// <remarks>
        /// <para>This method may be a little safer than IsEqual in that it
        /// considers the values to be equal if their absolute difference is
        /// less than a specific threshold or if their actual bit representations
        /// differ by less than a specific number of ULPs ("Units in the
        /// Last Place). See the following article:
        /// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
        /// </para>
        /// <para>An empty value is only considered to be equal to another
        /// empty value.</para>
        /// </remarks>
        public static bool IsAlmostEqual(double x, double y)
        {
            // Explicitly consider NaN to be equal to NaN.
            if (double.IsNaN(x)) return double.IsNaN(y);
            if (double.IsNaN(y)) return false;

            // Suitable thresholds as determined by empiricle tests.
            const double maxAbsDiff = 1e-14;
            const double maxRelDiff = 1e-14;

            // Explicitly test for inifinities, since the code further down doesn't
            // handle these cases.
            if (Double.IsInfinity(x))
            {
                if (Double.IsInfinity(y))
                {
                    return (Double.IsPositiveInfinity(x) == Double.IsPositiveInfinity(y));
                }
                else
                {
                    return false;
                }
            }

            // Calculate the difference.
            double absDiff = Math.Abs(x - y);

            // Check if the numbers are really close -- needed
            // when comparing numbers near zero.
            if (absDiff <= maxAbsDiff)
            {
                return true;
            }

            // Find the largest
            x = Math.Abs(x);
            y = Math.Abs(y);
            double largest = (y > x) ? y : x;

            return (absDiff <= largest * maxRelDiff);
        }

        /// <summary>
        /// Tests for an infinite value.
        /// </summary>
        /// <param name="x">A (possibly infinite) value.</param>
        /// <returns><c>true</c> if the value is infinite, <c>false</c>
        /// otherwise.</returns>
        public static bool IsInfinite(double x)
        {
            return double.IsInfinity(x);
        }

		/// <summary>
		/// Returns the larger of two values.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>The largest argument.</returns>
		/// <remarks>
		/// <para>See the <see cref="NumericValue.Compare"/> method for
        /// definitions of how empty values are compared to other values.</para>
		/// </remarks>
		public static double Max(double x, double y)
		{
			if (double.IsNaN(x)) return y;
			if (double.IsNaN(y)) return x;
			return (x > y ? x : y);
		}


		/// <summary>
		/// Returns the smaller of two values.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>The smallest argument.</returns>
		/// <remarks>
		/// <para><c>Min(NumericValue.Empty, y) == y</c>
        /// (where <c>y</c> is any value, including empty)</para>
		/// <para><c>Min(x, NumericValue.Empty) == x</c>
        /// (where <c>x</c> is any value, including empty)</para>
		/// <note>Even though the <see cref="NumericValue.Compare"/> method
        /// considers an empty value to be smaller than any other non-empty
        /// value, this method returns the non-empty value if the other value
        /// is empty.</note>
		/// </remarks>
		public static double Min(double x, double y)
		{
			if (double.IsNaN(x)) return y;
			if (double.IsNaN(y)) return x;
			return (x < y ? x : y);
		}


		/// <summary>
		/// Returns the product of two values.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <param name="y">A (possibly empty) value.</param>
		/// <returns>The product of the arguments.</returns>
		/// <remarks>
		/// <para><c>IsEmpty(Multiply(NumericValue.Empty, y)) == true</c> (where
		/// <c>y</c> is any value, including empty)</para>
		/// <para><c>IsEmpty(Multiply(x, NumericValue.Empty)) == true</c> (where
		/// <c>x</c> is any value, including empty)</para>
		/// </remarks>
		public static double Multiply(double x, double y)
		{
			if (double.IsNaN(x) || double.IsNaN(y)) return Empty;
			return (x * y);
		}


		/// <summary>
		/// Converts an <see cref="object"/> to a value.
		/// </summary>
		/// <param name="v">A type that can be converted to a value.</param>
		/// <returns>The value equivalent of the argument.</returns>
		/// <exception cref="FormatException">Thrown if the argument could not
		/// be converted to a value.</exception>
		/// <remarks>
		/// <para>If the argument is <c>null</c> or the <see cref="string"/>
		/// specified in <see cref="NumericValue.EmptyText"/> this method
        /// returns <see cref="NumericValue.Empty"/>.
		/// </para>
		/// <para>String representations of values are converted using the
		/// culture specified in <see cref="NumericValue.Culture"/>.</para>
		/// </remarks>
		public static double Parse(object v)
		{
			if (v == null || (v is string && EmptyText.Equals(v))) {
				return Empty;
			}
			try {
				return Convert.ToDouble(v, Culture);
			} 
			catch (FormatException) {
			}
			try {
				return Convert.ToDouble(v, EnglishCulture);
			} 
			catch (FormatException) {
			}
			try {
				return double.Parse(v.ToString(), Culture);
			}
			catch (Exception) {
			}
			try {
				return double.Parse(v.ToString(), EnglishCulture);
			}
			catch (Exception) {
			}
			try {
				return double.Parse(v.ToString(), CultureInfo.InvariantCulture);
			}
			catch (Exception) {
			}
			throw new FormatException();
		}


		/// <summary>
		/// Returns a value indicating the sign of a value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns><c>-1</c> if <paramref name="x"/> is less than zero,
		/// <c>1</c> if it is greater than zero, <c>0</c>
		/// if it is zero and <c>NumericValue.Empty</c> if it is
        /// empty.</returns>
		public static double Sign(double x)
		{
			if (double.IsNaN(x)) return Empty;
			return (x < 0d ? -1d : 1d);
		}


		/// <summary>
		/// Returns the square of a value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns>The square of the argument (<paramref name="x"/>
		/// multiplied by itself).</returns>
		/// <remarks>
		/// <para><c>IsEmpty(Square(NumericValue.Empty)) == true</c></para>
		/// </remarks>
		public static double Square(double x)
		{
			if (double.IsNaN(x)) return Empty;
			return (x * x);
		}


		/// <summary>
		/// Returns the square root of a value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns>The square root of the argument.</returns>
		/// <remarks>
		/// <para><c>IsEmpty(SquareRoot(NumericValue.Empty)) == true</c></para>
		/// </remarks>
		public static double SquareRoot(double x)
		{
			if (double.IsNaN(x)) return Empty;
			return Math.Sqrt(x);
		}


		/// <summary>
		/// Returns the difference between two values.
		/// </summary>
		/// <param name="x">The minuend value (possibly empty).</param>
		/// <param name="y">The subtrahend value (possibly empty).</param>
		/// <returns>The difference between the arguments.</returns>
		/// <remarks>
		/// <para><c>IsEmpty(Subtract(NumericValue.Empty, y)) == true</c> (where
		/// <c>y</c> is any value, including empty)</para>
		/// <para><c>IsEmpty(Subtract(x, NumericValue.Empty)) == true</c> (where
		/// <c>x</c> is any value, including empty)</para>
		/// </remarks>
		public static double Subtract(double x, double y)
		{
			if (double.IsNaN(x)) return -y;
			return (double.IsNaN(y) ? x : x - y);
		}


        /// <summary>
        /// Helper method that converts numeric values to objects for use in
        /// <see cref="ITable"/> implementations.
        /// </summary>
        /// <param name="value">The value to convert (box).</param>
        /// <returns>The boxed value, with <see cref="NumericValue.Empty"/>
        /// replaced with null.</returns>
        public static object ToObject(double value)
        {
            return double.IsNaN(value) ? null : (object) value;
        }

        
        /// <summary>
		/// Returns the string representation of a value.
		/// </summary>
		/// <param name="x">A (possibly empty) value.</param>
		/// <returns>The argument converted to a string.</returns>
		/// <remarks>
		/// <para>If the value is empty this method returns the string
		/// specified in <see cref="NumericValue.EmptyText"/>. For non-empty
        /// values, this method uses the culture specified in
        /// <see cref="NumericValue.Culture"/> to get
		/// the string representation.</para>
		/// </remarks>
		public static string ToString(double x)
		{
			return (double.IsNaN(x) ?
                NumericValue.EmptyText : x.ToString(Culture));
		}
	}
}
