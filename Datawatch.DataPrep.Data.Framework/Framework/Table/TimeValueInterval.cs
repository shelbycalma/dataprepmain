﻿using System;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    /// <summary>
    /// Represents an interval of time.
    /// </summary>
    /// <remarks>
    /// <para>This class is used with <see cref="ITimeColumn"/>s. For intervals
    /// of <em>sample</em> (<see cref="ITime"/>) times, such as a subset of a
    /// time series, use the <see cref="TimeInterval"/> class.</para>
    /// <para>The <see cref="TimeValue.Empty"/> value cannot be used to specify
    /// an open-ended interval. Use <see cref="DateTime.MinValue"/> or
    /// <see cref="DateTime.MaxValue"/> in these cases.</para>
    /// </remarks>
    public class TimeValueInterval
    {
        private readonly DateTime min;

        private readonly DateTime max;


        /// <summary>
        /// Creates a new <see cref="TimeValueInterval"/> with the specified
        /// end points.
        /// </summary>
        /// <param name="min">The minimum/lower end point of the
        /// interval.</param>
        /// <param name="max">The maximum/upper end point of the
        /// interval.</param>
        public TimeValueInterval(DateTime min, DateTime max)
        {
            this.min = min;
            this.max = max;
        }

        /// <summary>
        /// Checks if a specific <see cref="DateTime"/> is in the interval.
        /// </summary>
        /// <param name="value">Time to test.</param>
        /// <returns>True if the time is in the interval.</returns>
        /// <remarks>
        /// <para>This method regards the interval as closed, i.e. the test
        /// performed is (min &lt;= value &amp;&amp; value &lt;= max).</para>
        /// <para>Note that because of the current implementation of
        /// <see cref="TimeValue.Empty"/> as <see cref="DateTime.MinValue"/>,
        /// the SDK cannot separate the two. For the special case where you have
        /// set the interval's <see cref="Min"/> end point to
        /// <see cref="DateTime.MinValue"/> it will return <c>true</c> for
        /// <see cref="TimeValue.Empty"/> (in all other cases, the behavior is
        /// straightforward).</para>
        /// </remarks>
        public bool Contains(DateTime value)
        {
            return min <= value && value <= max;
        }

        /// <summary>
        /// Gets the maximum/upper end point of the interval.
        /// </summary>
        public DateTime Max
        {
            get { return max; }
        }

        /// <summary>
        /// Gets the minimum/lower end point of the interval.
        /// </summary>
        public DateTime Min
        {
            get { return min; }
        }

        /// <summary>
        /// Returns the interval formatted as a string.
        /// </summary>
        /// <returns>A human-readable string representation of the
        /// interval.</returns>
        public override string ToString()
        {
            return string.Format("[{0}, {1}]", min, max);
        }
    }
}
