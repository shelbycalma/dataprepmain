﻿namespace Datawatch.DataPrep.Data.Framework.Table
{
    public interface IDerivedColumnMetaData
    {
        IColumnMetaData SourceMetaData { get; }
    }
}
