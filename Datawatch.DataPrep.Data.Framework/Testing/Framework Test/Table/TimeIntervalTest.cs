﻿using System;
using System.Collections.Generic;
using Xunit;


namespace Datawatch.DataPrep.Data.Framework.Table
{
    public class TimeIntervalTest
    {
        private MockedTable table;
        private ITime[] times;

        public TimeIntervalTest()
        {
            table = new MockedTable(
                new DateTime(1973, 7, 23),
                new DateTime(1973, 7, 24),
                new DateTime(1973, 7, 25),
                new DateTime(1973, 7, 26),
                new DateTime(1973, 7, 27)
            );
            times = new ITime[table.TimeCount];
            for (int i = 0; i < table.TimeCount; i++) {
                times[i] = table.GetTime(i);
            }
        }

        [Fact]
        public void TimeInterval_EmptyMin()
        {
            // Should be allowed, open-ended.
            new TimeInterval(table, null, times[0]);
        }

        [Fact]
        public void TimeInterval_EmptyMax()
        {
            // Should be allowed, open-ended.
            new TimeInterval(table, null, times[0]);
        }

        [Fact]
        public void TimeInterval_EmptyBoth()
        {
            AssertHelper.ThrowsWithMessage<ArgumentException>(
                () => new TimeInterval(table, null, null),
                "Both end points cannot be null.");
        }

        [Fact]
        public void TimeInterval_ReservedMin()
        {
            AssertHelper.ThrowsWithMessage<ArgumentException>(
                () => new TimeInterval(table, ReservedTime.MinValue, times[0]),
                "One or more endpoint values are reserved.");
        }

        [Fact]
        public void TimeInterval_ReservedMax()
        {
            AssertHelper.ThrowsWithMessage<ArgumentException>(
                () => new TimeInterval(table, times[0], ReservedTime.MaxValue),
                "One or more endpoint values are reserved.");
        }

        [Fact]
        public void TimeInterval_ReversedEndpoints()
        {
            AssertHelper.ThrowsWithMessage<ArgumentException>(
                () => new TimeInterval(table, times[1], times[0]),
                "End points interchanged.");
        }

        [Fact]
        public void TimeInterval_SameEndpoints()
        {
            // Should definitely be allowed.
            new TimeInterval(table, times[0], times[0]);
        }

        [Fact]
        public void TimeInterval_Contains()
        {
            TimeInterval interval =
                new TimeInterval(table, times[1], times[3]);

            Assert.Equal(times[1], interval.Min);
            Assert.Equal(times[3], interval.Max);

            Assert.False(interval.Contains(null));
            Assert.False(interval.Contains(times[0]));
            Assert.True(interval.Contains(times[1]));
            Assert.True(interval.Contains(times[2]));
            Assert.True(interval.Contains(times[3]));
            Assert.False(interval.Contains(times[4]));
        }

        [Fact]
        public void TimeInterval_MinOpenContains()
        {
            TimeInterval interval =
                new TimeInterval(table, null, times[1]);

            Assert.Equal(null, interval.Min);
            Assert.Equal(times[1], interval.Max);

            Assert.False(interval.Contains(null));
            Assert.True(interval.Contains(times[0]));
            Assert.True(interval.Contains(times[1]));
            Assert.False(interval.Contains(times[2]));
        }

        [Fact]
        public void TimeInterval_MaxOpenContains()
        {
            TimeInterval interval =
                new TimeInterval(table, times[1], null);

            Assert.Equal(times[1], interval.Min);
            Assert.Equal(null, interval.Max);

            Assert.False(interval.Contains(null));
            Assert.False(interval.Contains(times[0]));
            Assert.True(interval.Contains(times[1]));
            Assert.True(interval.Contains(times[2]));
        }

        [Fact]
        public void TimeInterval_UndefinedContains()
        {
            TimeInterval interval = TimeInterval.Undefined;

            // Not a requirement, if this changes it's not the end
            // of the world (but be careful so the same assumption
            // is not also made in other places).
            Assert.Equal(null, interval.Min);
            Assert.Equal(null, interval.Max);

            Assert.False(interval.Contains(null));
            Assert.False(interval.Contains(times[0]));
        }

        [Fact]
        public void TimeInterval_AllContains()
        {
            TimeInterval interval = TimeInterval.All;

            // Not a requirement, if this changes it's not the end
            // of the world (but be careful so the same assumption
            // is not also made in other places).
            Assert.Equal(ReservedTime.MinValue, interval.Min);
            Assert.Equal(ReservedTime.MaxValue, interval.Max);

            Assert.False(interval.Contains(null));
            Assert.True(interval.Contains(times[0]));
        }

        [Fact]
        public void TimeInterval_ContainsReserved()
        {
            TimeInterval interval = new TimeInterval(table, times[0], times[1]);

            // NOTE: In the olden days, this used to throw an exception as the
            // reserved time's table (null) is different from the interval's
            // table. Times no longer have a table property, so we can't detect
            // that any more.
            Assert.False(interval.Contains(ReservedTime.MinValue));
        }

        [Fact]
        public void TimeInterval_UndefinedContainsReserved()
        {
            TimeInterval interval = TimeInterval.Undefined;

            Assert.False(interval.Contains(ReservedTime.MinValue));
        }

        [Fact]
        public void TimeInterval_AllContainsReserved()
        {
            TimeInterval interval = TimeInterval.All;

            Assert.True(interval.Contains(ReservedTime.MinValue));
        }

        private class MockedTable : ITimeseriesTable
        {
            #pragma warning disable 0067
            public event EventHandler<TableChangingEventArgs> Changing;
            public event EventHandler<TableChangedEventArgs> Changed;
            #pragma warning restore 0067

            private readonly List<MockedTime> times;
            #pragma warning disable 0649
            private MockedTime snapshot;
            #pragma warning restore 0649

            public MockedTable(params DateTime[] dates)
            {
                times = new List<MockedTime>();
                foreach (DateTime date in dates) {
                    times.Add(new MockedTime(date));
                }
            }

            public ITime GetTime(int index)
            {
                return times[index];
            }

            public int TimeCount
            {
                get { return times.Count; }
            }

            public ITime SnapshotTime
            {
                get { return snapshot; }
            }

            public int ColumnCount { get { return 0; } }
            public IColumn GetColumn(int index) { return null; }
            public IColumn GetColumn(string name) { return null; }
            public IRow GetRow(int index) { return null; }
            public bool IsUpdating { get { return false; } }
            public int RowCount { get { return 0; } }
        }

        private class MockedTime : ITime
        {
            private readonly DateTime time;

            public MockedTime(DateTime time)
            {
                this.time = time;
            }

            public DateTime Time
            {
                get { return time; }
            }
        }
    }
}
