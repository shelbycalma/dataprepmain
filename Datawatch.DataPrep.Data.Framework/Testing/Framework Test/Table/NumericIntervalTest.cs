﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    public class NumericIntervalTest
    {
        [Fact]
        public void NumericInterval_EmptyMin()
        {
            AssertHelper.ThrowsWithMessage<ArgumentException>(
                () => new NumericInterval(NumericValue.Empty, 0.0),
                "The 'min' end point in a numeric interval cannot be empty.");
        }

        [Fact]
        public void NumericInterval_EmptyMax()
        {
            AssertHelper.ThrowsWithMessage<ArgumentException>(
                () => new NumericInterval(0.0, NumericValue.Empty),
                "The 'max' end point in a numeric interval cannot be empty.");
        }

        [Fact]
        public void NumericInterval_ReversedEndpoints()
        {
            // TODO: But really, should this be allowed? Did not have the
            // guts to introduce an exception for this without further
            // investigation.
            new NumericInterval(1.0, 0.0);
        }

        [Fact]
        public void NumericInterval_SameEndpoints()
        {
            // Should definitely be allowed.
            new NumericInterval(0.0, 0.0);
        }

        [Fact]
        public void NumericInterval_Contains()
        {
            NumericInterval interval = new NumericInterval(0.0, 1.0);

            Assert.Equal(0.0, interval.Min, 5);
            Assert.Equal(1.0, interval.Max, 5);

            Assert.False(interval.Contains(NumericValue.Empty));
            Assert.False(interval.Contains(-1.0));
            Assert.True(interval.Contains(0.0));
            Assert.True(interval.Contains(0.5));
            Assert.True(interval.Contains(1.0));
            Assert.False(interval.Contains(2.0));
        }

        [Fact]
        public void NumericInterval_MinOpenContains()
        {
            NumericInterval interval =
                new NumericInterval(double.NegativeInfinity, 1.0);

            Assert.Equal(double.NegativeInfinity, interval.Min, 5);
            Assert.Equal(1.0, interval.Max, 5);

            Assert.False(interval.Contains(NumericValue.Empty));
            Assert.True(interval.Contains(double.NegativeInfinity));
            Assert.True(interval.Contains(0.0));
            Assert.True(interval.Contains(1.0));
            Assert.False(interval.Contains(2.0));
        }

        [Fact]
        public void NumericInterval_MaxOpenContains()
        {
            NumericInterval interval =
                new NumericInterval(0.0, double.PositiveInfinity);

            Assert.Equal(0.0, interval.Min, 5);
            Assert.Equal(double.PositiveInfinity, interval.Max, 5);

            Assert.False(interval.Contains(NumericValue.Empty));
            Assert.False(interval.Contains(-1.0));
            Assert.True(interval.Contains(0.0));
            Assert.True(interval.Contains(1.0));
            Assert.True(interval.Contains(double.PositiveInfinity));
        }
    }
}
