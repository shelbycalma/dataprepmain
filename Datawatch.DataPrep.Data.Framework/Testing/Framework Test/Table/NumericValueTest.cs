﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    public class NumericValueTest
    {
        [Fact]
        public void AssemblyResourcesAreAccessibleTest()
        {
            AssertHelper.Equal(NumericValue.EmptyText, "n/a",
                "Either the resources have been obfuscated, " +
                "or the value changed from \"n/a\" " +
                "(which is ok but update the test).");
        }

        [Fact]
        public void EmptyIsEmpty()
        {
            Assert.True(NumericValue.IsEmpty(NumericValue.Empty),
                "The internal representation of empty numeric values " +
                "has changed and one of IsEmpty and Empty wasn't updated.");
        }

        [Theory]
        [InlineData(double.NaN, double.NaN, true)]
        [InlineData(double.Epsilon, -double.Epsilon, true)]
        [InlineData(double.PositiveInfinity, double.NaN, false)]
        [InlineData(double.NegativeInfinity, double.NaN, false)]
        [InlineData(double.Epsilon, double.NaN, false)]
        [InlineData(double.PositiveInfinity, double.NegativeInfinity, false)]
        [InlineData(double.PositiveInfinity, double.PositiveInfinity, true)]
        [InlineData(double.NegativeInfinity, double.NegativeInfinity, true)]
        [InlineData(double.MinValue, double.MaxValue, false)]
        [InlineData(double.MinValue, -double.MaxValue, true)]
        [InlineData(-double.MinValue, double.MaxValue, true)]
        [InlineData(0.0,
            -1.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(0.0,
            1.0 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1 - 0.1,
            true)]
        [InlineData(1.0,
            0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1.5,
            0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 +
            0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(2.0,
            0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 +
            0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(100.0,
            99.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(12.0,
            11.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(123.0,
            122.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1234.0,
            1233.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(12345.0,
            12344.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(123456.0,
            123455.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1234567.0,
            1234566.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(12345678.0,
            12345677.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(123456789.0,
            123456788.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1234567890.0,
            1234567889.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(12345678901.0,
            12345678900.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(123456789012.0,
            123456789011.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1234567890123.0,
            1234567890122.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(12345678901234.0,
            12345678901233.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(123456789012345.0,
            123456789012344.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1234567890123456.0,
            1234567890123455.0 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e-10,
            1e-10 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e-20, 1e-20 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e-30, 1e-30 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e-40, 1e-40 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e-50, 1e-50 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e10,
            1e10 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e15, 1e15 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e20, 1e20 + -1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1,
            true)]
        [InlineData(1e200, 1e199 * 10.0, true)]
//        [InlineData(1, 1.1, false)]
//        [InlineData(1e-20, 1.1e-20, false)]
//        [InlineData(1, 1/2.0, false)]
//        [InlineData(1e-20, 1e-20/2.0, false)]
        public void TestAlmostEqual(double x, double y, bool shouldBeEqual)
        {
            bool almostEqual = NumericValue.IsAlmostEqual(x, y);
            string errorMsg = String.Format("IsAlmostEqual({0},{1})", x, y);
            if (shouldBeEqual)
            {
                Assert.True(almostEqual, errorMsg);
            }
            else
            {
                Assert.False(almostEqual, errorMsg);
            }
        }

        [Fact]
        public void Superman_III()
        {
            NumericValueKey key1 = new NumericValueKey(1.0E-20);
            NumericValueKey key2 = new NumericValueKey(1.1E-20);
            double absRelDiff = Math.Abs((key1.Value - key2.Value) / key2.Value);
            Assert.True(Math.Abs(0.1 - absRelDiff) >= 1.0E-20); // TODO: check if specified precision is good enough
            Assert.False(key1.Equals(key2),
                "10 % rel diff is not equal nor almost equal.");
        }
    }
}
