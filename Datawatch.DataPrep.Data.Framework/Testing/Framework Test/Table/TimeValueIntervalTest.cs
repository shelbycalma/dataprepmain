﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Table
{
    public class TimeValueIntervalTest
    {
        private static readonly DateTime
            dateOne = new DateTime(1973, 7, 23),
            dateTwo = new DateTime(1973, 7, 24),
            dateThree = new DateTime(1973, 7, 25),
            dateFour = new DateTime(1973, 7, 26),
            dateFive = new DateTime(1973, 7, 27);

        [Fact]
        public void TimeValueInterval_EmptyMin()
        {
            // Must be allowed, Empty == DateTime.MinValue.
            new TimeValueInterval(TimeValue.Empty, dateOne);
        }

        [Fact]
        public void TimeValueInterval_EmptyMax()
        {
            // Must be allowed as long as ReversedEndpoints doesn't fail.
            new TimeValueInterval(dateOne, TimeValue.Empty);
        }

        [Fact]
        public void TimeValueInterval_ReversedEndpoints()
        {
            // TODO: But really, should this be allowed? Did not have the
            // guts to introduce an exception for this without further
            // investigation.
            new TimeValueInterval(dateTwo, dateOne);
        }

        [Fact]
        public void TimeValueInterval_SameEndpoints()
        {
            // Should definitely be allowed.
            new TimeValueInterval(dateOne, dateOne);
        }

        [Fact]
        public void TimeValueInterval_Contains()
        {
            TimeValueInterval interval =
                new TimeValueInterval(dateTwo, dateFour);

            Assert.Equal(dateTwo, interval.Min);
            Assert.Equal(dateFour, interval.Max);

            Assert.False(interval.Contains(TimeValue.Empty));
            Assert.False(interval.Contains(dateOne));
            Assert.True(interval.Contains(dateTwo));
            Assert.True(interval.Contains(dateThree));
            Assert.True(interval.Contains(dateFour));
            Assert.False(interval.Contains(dateFive));
        }

        [Fact]
        public void TimeValueInterval_MinOpenContains()
        {
            TimeValueInterval interval =
                new TimeValueInterval(DateTime.MinValue, dateTwo);

            Assert.Equal(DateTime.MinValue, interval.Min);
            Assert.Equal(dateTwo, interval.Max);

            // See note on Contains for why this is true.
            Assert.True(interval.Contains(TimeValue.Empty));

            Assert.True(interval.Contains(DateTime.MinValue));
            Assert.True(interval.Contains(dateOne));
            Assert.True(interval.Contains(dateTwo));
            Assert.False(interval.Contains(dateThree));
        }

        [Fact]
        public void TimeValueInterval_MaxOpenContains()
        {
            TimeValueInterval interval =
                new TimeValueInterval(dateTwo, DateTime.MaxValue);

            Assert.Equal(dateTwo, interval.Min);
            Assert.Equal(DateTime.MaxValue, interval.Max);

            Assert.False(interval.Contains(TimeValue.Empty));
            Assert.False(interval.Contains(dateOne));
            Assert.True(interval.Contains(dateTwo));
            Assert.True(interval.Contains(dateThree));
            Assert.True(interval.Contains(DateTime.MaxValue));
        }
    }
}
