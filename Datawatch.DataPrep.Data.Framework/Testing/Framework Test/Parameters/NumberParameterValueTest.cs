﻿using System;

using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class NumberParameterValueTest
    {
        // Not using the [TestCase] feature here, want to reuse cases in
        // ParameterValueTest.
        public static readonly NumberParameterValue[] Cases =
            new NumberParameterValue[] {
                new NumberParameterValue(),
                new NumberParameterValue()
                {
                    Value = double.NaN
                },
                new NumberParameterValue()
                {
                    Value = 3.14
                }
        };

        public static readonly string[] ExpectedResults = new string[] {
            "0",
            "NaN",
            "3.14"
        };

        public static readonly string[] ExpectedDebugResults = ExpectedResults;

        [Fact]
        public void ToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                NumberParameterValue value = Cases[i];
                string expected = ExpectedResults[i];
                Assert.Equal(expected, value.ToString(null, null));
            }
        }

        [Fact]
        public void DebugToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                NumberParameterValue value = Cases[i];
                string expected = ExpectedDebugResults[i];
                Assert.Equal(expected, value.ToString());
            }
        }

        [Fact]
        public void CustomFormatToStringTest()
        {
            NumberParameterValue value = new NumberParameterValue();
            value.Value = Math.PI;
            string actual = value.ToString("0.0", null);
            Assert.Equal("3.1", actual);
        }
    }
}
