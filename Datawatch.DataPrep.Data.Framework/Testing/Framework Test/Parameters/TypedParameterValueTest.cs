﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class TypedParameterValueTest
    {
        [Fact]
        public void StringTest()
        {
            var a = new StringParameterValue {Value = "foo"};
            var b = new StringParameterValue {Value = "foo"};
            Assert.Equal(a, b);
        }

        [Fact]
        public void NumberTest()
        {
            var a = new NumberParameterValue {Value = 42};
            var b = new NumberParameterValue {Value = 42};
            Assert.Equal(a, b);
        }

        [Fact]
        public void DateTimeTest()
        {
            var a = new DateTimeParameterValue
            {
                Value = new DateTime(2020, 10, 5)
            };
            var b = new DateTimeParameterValue
            {
                Value = new DateTime(2020, 10, 5)
            };
            Assert.Equal(a, b);
        }

        [Fact]
        public void ArrayTest()
        {
            var a = new ArrayParameterValue();
            a.Add(new StringParameterValue {Value = "foo"});
            var b = new ArrayParameterValue();
            b.Add(new StringParameterValue {Value = "foo"});
            Assert.Equal(a, b);
        }

        [Fact]
        public void RemoveSingleQuoteTest()
        {
            Assert.Equal("Oil", ParameterHelper.RemoveSingleQuotes("'Oil'"));
        }
    }
}
