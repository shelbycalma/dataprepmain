﻿using System;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    // These unit tests are not to validate that ToString() is correct,
    // but to assist in porting to java.
    public class ArrayParameterValueTest
    {
        // Not using the [Theory] feature here, want to reuse cases in
        // ParameterValueTest.
        public static readonly ArrayParameterValue[] Cases =
            new ArrayParameterValue[] {
                new ArrayParameterValue(),
                CreateCase2(),
                CreateCase3(),
                CreateCase4(),
                CreateCase5(),
                CreateCase6(),
                CreateCase7(),
                CreateCase8(),
                CreateCase9(),
                CreateCase10()
        };

        public static readonly string[] ExpectedResults = new string[] {
            ",",
            "paramValue1",
            ",",
            "paramValue1,paramValue2",
            "paramValue2,paramValue1",
            "3.14",
            "3.14,3.15",
            "3.15,3.14",
            "01/23/2012 14:15:16",
            "01/23/2012 14:15:16,01/23/2012 14:15:17"
        };

        public static readonly string[] ExpectedDebugResults = new string[] {
            "ArrayParameterValue[]",
            "ArrayParameterValue[paramValue1]",
            "ArrayParameterValue[]",
            "ArrayParameterValue[paramValue1,paramValue2]",
            "ArrayParameterValue[paramValue2,paramValue1]",
            "ArrayParameterValue[3.14]",
            "ArrayParameterValue[3.14,3.15]",
            "ArrayParameterValue[3.15,3.14]",
            "ArrayParameterValue[01/23/2012 14:15:16]",
            "ArrayParameterValue[01/23/2012 14:15:16,01/23/2012 14:15:17]"
        };

        private static ArrayParameterValue CreateCase2()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            value.Values.Add(new StringParameterValue("paramValue1"));
            return value;
        }

        private static ArrayParameterValue CreateCase3()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            value.Values.Add(new StringParameterValue(null));
            return value;
        }

        private static ArrayParameterValue CreateCase4()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            value.Values.Add(new StringParameterValue("paramValue1"));
            value.Values.Add(new StringParameterValue("paramValue2"));
            return value;
        }

        private static ArrayParameterValue CreateCase5()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            value.Values.Add(new StringParameterValue("paramValue2"));
            value.Values.Add(new StringParameterValue("paramValue1"));
            return value;
        }

        private static ArrayParameterValue CreateCase6()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            NumberParameterValue nv1 = new NumberParameterValue();
            nv1.Value = 3.14;
            value.Values.Add(nv1);
            return value;
        }

        private static ArrayParameterValue CreateCase7()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            NumberParameterValue nv1 = new NumberParameterValue();
            nv1.Value = 3.14;
            value.Values.Add(nv1);
            NumberParameterValue nv2 = new NumberParameterValue();
            nv2.Value = 3.15;
            value.Values.Add(nv2);
            return value;
        }
        
        private static ArrayParameterValue CreateCase8()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            NumberParameterValue nv1 = new NumberParameterValue();
            nv1.Value = 3.14;
            NumberParameterValue nv2 = new NumberParameterValue();
            nv2.Value = 3.15;

            value.Values.Add(nv2);
            value.Values.Add(nv1);
            return value;
        }

        private static ArrayParameterValue CreateCase9()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            DateTimeParameterValue dt1 = new DateTimeParameterValue();
            dt1.Value = new DateTime(2012, 1, 23, 14, 15, 16, 666);
            value.Values.Add(dt1);
            return value;
        }

        private static ArrayParameterValue CreateCase10()
        {
            ArrayParameterValue value = new ArrayParameterValue();
            DateTimeParameterValue dt1 = new DateTimeParameterValue();
            dt1.Value = new DateTime(2012, 1, 23, 14, 15, 16, 666);
            value.Values.Add(dt1);
            DateTimeParameterValue dt2 = new DateTimeParameterValue();
            dt2.Value = new DateTime(2012, 1, 23, 14, 15, 17, 666);
            value.Values.Add(dt2);
            return value;
        }

        [Fact]
        public void ToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                ArrayParameterValue value = Cases[i];
                string expected = ExpectedResults[i];
                Assert.Equal(expected, value.ToString(null, null));
            }
        }

        [Fact]
        public void DebugToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                ArrayParameterValue value = Cases[i];
                string expected = ExpectedDebugResults[i];
                Assert.Equal(expected, value.ToString());
            }
        }

    }
}
