﻿using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class StringParameterValueTest
    {
        // Not using the [TestCase] feature here, want to reuse cases in
        // ParameterValueTest.
        public static readonly StringParameterValue[] Cases =
            new StringParameterValue[] {
                new StringParameterValue(null),
                new StringParameterValue("paramValue"),
                new StringParameterValue("'\"\t\n'")
        };

        public static readonly string[] ExpectedResults = new string[] {
            "",
            "paramValue",
            "'\"\t\n'"
        };

        public static readonly string[] ExpectedDebugResults = ExpectedResults;

        [Fact]
        public void ToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                StringParameterValue value = Cases[i];
                string expected = ExpectedResults[i];
                Assert.Equal(expected, value.ToString(null, null));
            }
        }

        [Fact]
        public void DebugToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                StringParameterValue value = Cases[i];
                string expected = ExpectedDebugResults[i];
                Assert.Equal(expected, value.ToString());
            }
        }
    }
}
