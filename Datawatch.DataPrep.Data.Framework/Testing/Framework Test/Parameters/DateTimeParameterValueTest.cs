﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class DateTimeParameterValueTest
    {
        // Not using the [Theory] feature here, want to reuse cases in
        // ParameterValueTest.
        public static readonly DateTimeParameterValue[] Cases =
            new DateTimeParameterValue[] {
                new DateTimeParameterValue()
                {
                    Value = new DateTime(2012, 1, 23, 14, 15, 16, 666)
                }
        };

        // "MM/dd/yyyy HH:mm:ss" is the format being used.
        // This is the null format or "G" in CultureInfo.InvariantCulture.
        public static readonly string[] ExpectedResults = new string[] {
            "01/23/2012 14:15:16",
        };

        public static readonly string[] ExpectedDebugResults = ExpectedResults;

        [Fact]
        public void ToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                DateTimeParameterValue value = Cases[i];
                string expected = ExpectedResults[i];
                Assert.Equal(expected, value.ToString(null, null));
            }
        }

        [Fact]
        public void DebugToStringTest()
        {
            for (int i = 0; i < Cases.Length; i++)
            {
                DateTimeParameterValue value = Cases[i];
                string expected = ExpectedDebugResults[i];
                Assert.Equal(expected, value.ToString());
            }
        }

        [Fact]
        public void CustomFormatToStringTest()
        {
            DateTimeParameterValue value = new DateTimeParameterValue();
            DateTime dateTime = new DateTime(2012, 1, 23, 14, 15, 16, 666);
            value.Value = dateTime;
            string actual = value.ToString("MM/dd/yyyy HH:mm:ss.fff", null);
            Assert.Equal("01/23/2012 14:15:16.666", actual);
        }
    }
}
