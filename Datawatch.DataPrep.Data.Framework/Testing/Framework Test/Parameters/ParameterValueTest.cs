﻿using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Parameters
{
    public class ParameterValueTest
    {
        [Fact]
        public void NullValueToStringTest()
        {
            TypedParameterValue value = null;
            ParameterValue pv = new ParameterValue("paramName", value);
            Assert.Equal("[paramName=]", pv.ToString());
        }

        [Fact]
        public void StringParam_ToStringTest()
        {
            for (int i = 0; i < StringParameterValueTest.Cases.Length; i++)
            {
                StringParameterValue value = StringParameterValueTest.Cases[i];
                string expectedValueStr = StringParameterValueTest.ExpectedResults[i];
                ParameterValue pv = new ParameterValue("paramName", value);
                string expected = "[paramName=" + expectedValueStr + "]";
                Assert.Equal(expected, pv.ToString());
            }            
        }

        [Fact]
        public void DateTimeParam_ToStringTest()
        {
            for (int i = 0; i < DateTimeParameterValueTest.Cases.Length; i++)
            {
                DateTimeParameterValue value = DateTimeParameterValueTest.Cases[i];
                string expectedValueStr = DateTimeParameterValueTest.ExpectedResults[i];
                ParameterValue pv = new ParameterValue("paramName", value);
                string expected = "[paramName=" + expectedValueStr + "]";
                Assert.Equal(expected, pv.ToString());
            }            
        }

        [Fact]
        public void NumberParam_ToStringTest()
        {
            for (int i = 0; i < NumberParameterValueTest.Cases.Length; i++)
            {
                NumberParameterValue value = NumberParameterValueTest.Cases[i];
                string expectedValueStr = NumberParameterValueTest.ExpectedResults[i];
                ParameterValue pv = new ParameterValue("paramName", value);
                string expected = "[paramName=" + expectedValueStr + "]";
                Assert.Equal(expected, pv.ToString());
            }            
        }

        [Fact]
        public void ArrayParam_ToStringTest()
        {
            for (int i = 0; i < ArrayParameterValueTest.Cases.Length; i++)
            {
                ArrayParameterValue value = ArrayParameterValueTest.Cases[i];
                string expectedValueStr = ArrayParameterValueTest.ExpectedResults[i];
                ParameterValue pv = new ParameterValue("paramName", value);
                string expected = "[paramName=" + expectedValueStr + "]";
                Assert.Equal(expected, pv.ToString());
            }            
        }
    }
}
