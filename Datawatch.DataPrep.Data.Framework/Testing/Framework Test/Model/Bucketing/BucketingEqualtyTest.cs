﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Model.Bucketing
{
    public class BucketingEqualtyTest
    {
        [Fact]
        public void TextBucketingEqualtyTest()
        {
            TextBucketingMappingItem mappingItem1 = new TextBucketingMappingItem
            {
                GroupName = "G1",
                SourceColumnValue = "S1"
            };

            TextBucketingMappingItem mappingItem2 = new TextBucketingMappingItem
            {
                GroupName = "G2",
                SourceColumnValue = "S2"
            };

            TextBucketingMappingItem mappingItem3 = new TextBucketingMappingItem
            {
                GroupName = "G3",
                SourceColumnValue = "S3"
            };

            Assert.False(mappingItem1.Equals(mappingItem2));

            mappingItem2.SourceColumnValue = "S1";
            Assert.False(mappingItem1.Equals(mappingItem2));

            mappingItem2.GroupName = "G1";
            Assert.True(mappingItem1.Equals(mappingItem2));

            mappingItem2.GroupName = "G2";
            mappingItem2.SourceColumnValue = "S2";

            TextBucketing textBucketing1 = new TextBucketing
                {
                    Title = "Title1",
                    SourceColumnName = "Source1",
                };

            TextBucketing textBucketing2 = new TextBucketing
            {
                Title = "Title1",
                SourceColumnName = "Source1",
            };

            Assert.False(textBucketing1.Equals(textBucketing2));

            textBucketing1.Name = textBucketing2.Name;
            Assert.True(textBucketing1.Equals(textBucketing2));

            textBucketing2.Title = "Title2";
            Assert.False(textBucketing1.Equals(textBucketing2));
            textBucketing2.Title = "Title1";

            textBucketing2.SourceColumnName = "Source2";
            Assert.False(textBucketing1.Equals(textBucketing2));
            textBucketing2.SourceColumnName = "Source1";

            textBucketing1.Mapping = new[] { mappingItem1 };
            textBucketing2.Mapping = new[] { mappingItem1 };
            Assert.True(textBucketing1.Equals(textBucketing2));

            textBucketing1.Mapping = new[] { mappingItem1, mappingItem2 };
            textBucketing2.Mapping = new[] { mappingItem1, mappingItem2 };
            Assert.True(textBucketing1.Equals(textBucketing2));

            textBucketing2.Mapping = new[] { mappingItem1, mappingItem2, mappingItem3 };
            Assert.False(textBucketing1.Equals(textBucketing2));
            Assert.False(textBucketing2.Equals(textBucketing1));

            textBucketing1.Mapping = new[] { mappingItem1, mappingItem2 };
            textBucketing2.Mapping = new[] { mappingItem2, mappingItem1 };
            Assert.False(textBucketing1.Equals(textBucketing2));
        }
    }
}
