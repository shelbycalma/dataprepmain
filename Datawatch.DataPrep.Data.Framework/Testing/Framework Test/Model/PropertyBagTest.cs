﻿
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Model
{       
    /// <summary>
    ///This is a test class for PropertyBagTest and is intended
    ///to contain all PropertyBagTest Unit Tests
    ///</summary>
    public class PropertyBagTest
    {
        /// <summary>
        ///A test for PropertyBag Constructor
        ///</summary>
        [Fact]
        public void PropertyBagConstructorTest()
        {
            PropertyBag target = new PropertyBag();
        }

        /// <summary>
        ///A test for Clear
        ///</summary>
        [Fact]
        public void ClearTest()
        {
            PropertyBag target = new PropertyBag();

            target.Values["Test"] = "Test Value";
            target.SubGroups["TestGroup"] = new PropertyBag();
            target.SubGroups["TestGroup"].Values["TestValue"] = "SubValue";

            target.Clear();

            Assert.Empty(target.Values);
            Assert.Empty(target.SubGroups);
        }


        /// <summary>
        ///A test for SubGroups
        ///</summary>
        [Fact]
        public void SubGroupTest()
        {
            PropertyBag target = new PropertyBag();
            PropertyBag expected = new PropertyBag();
            PropertyBag actual;
            string key = "SubKey";
            target.SubGroups[key] = expected;
            actual = target.SubGroups[key];
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SubGroupsNoDuplicatesTest()
        {
            PropertyBag target = new PropertyBag();
            Assert.Equal(0, target.SubGroups.Count);

            target.SubGroups["name1"] = new PropertyBag();
            Assert.Equal(1, target.SubGroups.Count);

            target.SubGroups["name1"] = new PropertyBag();
            Assert.Equal(1, target.SubGroups.Count);

            int count = 0;
            foreach (PropertyBag bag in target.SubGroups) {
                count++;
            }
            Assert.Equal(1, count);
        }

        [Fact(Skip = "Unknown reason")]
        public void SubGroupsNoDuplicates2Test()
        {
            PropertyBag target = new PropertyBag();
            Assert.Equal(0, target.SubGroups.Count);

            PropertyBag tempBag = new PropertyBag();
            tempBag.Name = "name1";
            target.SubGroups.Add(tempBag);
            Assert.Equal(1, target.SubGroups.Count);

            PropertyBag tempBag2 = new PropertyBag();
            tempBag2.Name = "name1";
            target.SubGroups.Add(tempBag2);
            Assert.Equal(1, target.SubGroups.Count);

            int count = 0;
            foreach (PropertyBag bag in target.SubGroups) {
                count++;
            }
            Assert.Equal(1, count);
        }

        /// <summary>
        ///A test for Values
        ///</summary>
        [Fact]
        public void ValueTest()
        {
            PropertyBag target = new PropertyBag(); // TODO: Initialize to an appropriate value
            string key = "Test Key";
            string expected = "Test Value"; 
            string actual;
            target.Values[key] = expected;
            actual = target.Values[key];
            Assert.Equal(expected, actual);
        }
    }
}