﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.Model
{        
    /// <summary>
    ///This is a test class for MutableLookupListTest and is intended
    ///to contain all MutableLookupListTest Unit Tests
    ///</summary>
    public class ObservableLookupCollectionTest
    {
        private class NamedTestClass : INamedObject
        {
            private string name;

            public NamedTestClass(string name)
            {
                this.name = name;
            }

            public string Name
            {
                get { return name; }
                set
                {
                    if (value != name)
                    {
                        string oldName = name;

                        name = value;
                        
                        if (NameChanged != null)
                        {
                            NameChanged(this, 
                                new NamedObjectEventArgs(oldName, name));
                        }
                    }
                }
            }

            public event NamedObjectEventHandler NameChanged;
        }

        [Fact]
        public void AddTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();
            NamedTestClass expected = new NamedTestClass("Test");
            NamedTestClass actual;

            target.Add(expected);
            actual = target[expected.Name];
            Assert.Same(expected, actual);
        }

        [Fact(Skip = "Which methods do we need to override in order to fix this?")]
        public void AddDuplicateNameTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();

            NamedTestClass item = new NamedTestClass("Test");
            target.Add(item);

            NamedTestClass item2 = new NamedTestClass("Test");
            Assert.Throws<ArgumentException>(() => target.Add(item2));
        }

        [Fact]
        public void AddRangeTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();
            NamedTestClass[] expected = {
                                            new NamedTestClass("Item 1"),
                                            new NamedTestClass("Item 2"),
                                            new NamedTestClass("Item 3")
                                        };
            NamedTestClass actual;

            foreach (NamedTestClass ntc in expected)
            {
                target.Add(ntc);
            }

            Assert.Equal(expected.Length, target.Count);

            for (int i = 0; i < expected.Length; i++)
            {
                actual = target[expected[i].Name];
                Assert.Same(expected[i], actual);
            }
        }

        [Fact]
        public void RemoveTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();
            NamedTestClass[] expected = {
                                            new NamedTestClass("Item 1"),
                                            new NamedTestClass("Item 2"),
                                            new NamedTestClass("Item 3")
                                        };

            foreach (NamedTestClass ntc in expected)
            {
                target.Add(ntc);
            }

            target.Remove("Item 2");
            Assert.Null(target["Item 2"]);
            Assert.Equal(expected.Length - 1, target.Count);
            Assert.Same(expected[0], target[0]);
            Assert.Same(expected[2], target[1]);
        }

        [Fact]
        public void OnItemNameChangedTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();
            string oldName = "Item";
            string newName = "New Item Name";
            NamedTestClass expected = new NamedTestClass(oldName);
            NamedTestClass actual;

            target.Add(expected);
            expected.Name = newName;
            Assert.Null(target[oldName]);
            actual = target[newName];
            Assert.Same(expected, actual);
        }

        [Fact(Skip = "Enable this test when the ObservableLookupCollection handles INamedObjects with duplicate names")]
        public void OnItemNameChangedToADuplicateTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();
            NamedTestClass firstItem = new NamedTestClass("Name already taken");
            target.Add(firstItem);

            string oldName = "Item";
            string newName = "Name already taken";
            NamedTestClass expected = new NamedTestClass(oldName);

            target.Add(expected);
            Assert.Equal(2, target.Count);

            Assert.Throws<Exception>(() => expected.Name = newName);
        }

        [Fact]
        public void ClearTest()
        {
            ObservableLookupCollection<NamedTestClass> target = new ObservableLookupCollection<NamedTestClass>();
            NamedTestClass[] expected = {
                                            new NamedTestClass("Item 1"),
                                            new NamedTestClass("Item 2"),
                                            new NamedTestClass("Item 3")
                                        };

            foreach (NamedTestClass ntc in expected)
            {
                target.Add(ntc);
            }
            target.Clear();

            Assert.Empty(target);
            for (int i = 0; i < expected.Length; i++)
            {
                Assert.Null(target[expected[i].Name]);
            }
        } 
    }
}
