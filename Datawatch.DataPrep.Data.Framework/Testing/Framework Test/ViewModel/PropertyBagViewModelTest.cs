﻿using System.Windows.Media;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework.ViewModel
{
    /// <summary>
    /// Fixture written to make sure that the PropertyBagViewModel can convert
    /// minor camel case names to upper camel case names and enum values
    /// to indexed integers.
    /// </summary>
    public class PropertyBagViewModelTest
    {
        private PropertyBag CreateOldBag()
        {
            // this is a legacy property bag with name in minor camel case
            var oldBag = new PropertyBag();
            oldBag.Values["mySetting"] = "my_value";
            return oldBag;
        }

        private PropertyBag CreateNewBag()
        {
            // this bag uses major camel case for its names
            var newBag = new PropertyBag();
            newBag.Values["MySetting"] = "my_value";
            return newBag;
        }

        [Fact]
        public void ShouldNotCoerceNamesByDefault()
        {
            var oldBag = CreateOldBag();
            MockedSettingsClass settingsClass = new MockedSettingsClass(oldBag);
            Assert.Null(settingsClass.MySetting);
        }

        [Fact]
        public void ShouldHandleMajorCamelCaseNamesImplicitly()
        {
            var newBag = CreateNewBag();
            MockedSettingsClass settingsClass = new MockedSettingsClass(newBag);
            Assert.NotNull(settingsClass.MySetting);
        }

        [Fact]
        public void ShouldHandleMinorCamelCaseNamesExplicitly()
        {
            var oldBag = CreateOldBag();
            PropertyBag upgradedBag =
                PropertyBagViewModel.ConvertNamesToMajorCamelCase(oldBag);
            MockedSettingsClass settingsClass =
                new MockedSettingsClass(upgradedBag);
            Assert.NotNull(settingsClass.MySetting);
        }

        [Fact]
        public void TestConvertEnum()
        {
            var oldBag = CreateOldBag();
            oldBag.Values["stretch"] = Stretch.Uniform.ToString();
            PropertyBagViewModel.ConvertEnumToIndexedInteger<Stretch>(oldBag, "stretch");

            // verify that the bag now has coerced value
            Assert.True(oldBag.Values.ContainsKey("Stretch"));
            Assert.Equal("2", oldBag.Values["Stretch"]);

            // verify that the bag can be read by the settings class
            // Uniform is index 2
            MockedSettingsClass settingsClass = new MockedSettingsClass(oldBag);
            Assert.Equal(Stretch.Uniform, settingsClass.Stretch);
        }

        [Fact]
        public void ShouldAcceptNullBag()
        {
            PropertyBag upgradedBag =
                PropertyBagViewModel.ConvertNamesToMajorCamelCase(null);
            Assert.Null(upgradedBag);
        }

        [Fact]
        public void ShouldHandleSubGroupsRecursively()
        {
            var oldBag = CreateOldBag();
            var newBag = CreateNewBag();
            oldBag.SubGroups.Add(new PropertyBag { Name = "bagName" });
            oldBag.SubGroups[0].Values["valueName"] = "my_value";
            newBag = PropertyBagViewModel.ConvertNamesToMajorCamelCase(oldBag);
            Assert.Equal(1, newBag.SubGroups.Count);
            Assert.Equal("BagName", newBag.SubGroups[0].Name);
            Assert.Equal("my_value", newBag.SubGroups[0].Values["ValueName"]);
        }

        public class MockedSettingsClass : PropertyBagViewModel
        {
            public MockedSettingsClass(PropertyBag bag) : base(bag)
            {
            }

            public string MySetting
            {
                get { return GetInternal("MySetting"); }
                set { SetInternal("MySetting", value); }
            }

            public Stretch Stretch
            {
                get { return GetInternalEnum("Stretch", Stretch.None); }
                set { SetInternalEnum("Stretch", value); }
            }
        }

        [Fact]
        public void GetInternalColorWithDefaultTest()
        {
            MockedColorSettings settings = new MockedColorSettings();

            System.Drawing.Color actual = settings.TestColorWithDefault;
            Assert.Equal(settings.DefaultValue, actual);

            System.Drawing.Color expected =
                System.Drawing.Color.FromArgb(1, 2, 3, 4);
            settings.TestColorWithDefault = expected;
            actual = settings.TestColorWithDefault;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetInternalColorTest()
        {
            MockedColorSettings settings = new MockedColorSettings();

            System.Drawing.Color expected =
                System.Drawing.Color.FromArgb(1, 2, 3, 4);
            settings.TestColor = expected;

            System.Drawing.Color actual = settings.TestColor;
            Assert.Equal(expected, actual);
        }

        public class MockedColorSettings : PropertyBagViewModel
        {
            public readonly System.Drawing.Color DefaultValue =
                System.Drawing.Color.FromArgb(4, 3, 2, 1);

            public MockedColorSettings() : base(new PropertyBag())
            {
            }

            public System.Drawing.Color TestColor
            {
                get { return GetInternalColor("TestColor"); }
                set { SetInternalColor("TestColor", value); }
            }

            public System.Drawing.Color TestColorWithDefault
            {
                get { return GetInternalColor("TestColorWithDefault", DefaultValue); }
                set { SetInternalColor("TestColorWithDefault", value); }
            }
        }
    }
}
