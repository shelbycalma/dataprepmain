﻿using System;
using System.Globalization;
using System.Threading;

using Xunit;

using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit.Extensions;

namespace Datawatch.DataPrep.Data.Framework.ViewModel
{
    public class NumericDataHelperTest
    {
        [Fact]
        public void ConstructorTest()
        {
            NumericDataHelper helper = new NumericDataHelper(new PropertyBag());
        }

        [Fact]
        public void ConstuctorNullBagTest()
        {
            Assert.Throws<ArgumentNullException>(
                () =>
                    {
                        NumericDataHelper helper = new NumericDataHelper(null);
                    });
        }

        [Fact]
        public void DecimalSeparatorTest()
        {
            NumericDataHelper helper = new NumericDataHelper(new PropertyBag());

            Assert.Equal(
                CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator[0],
                helper.DecimalSeparator);

            helper.DecimalSeparator = 't';
            Assert.Equal('t', helper.DecimalSeparator);
        }

        [Fact]
        public void TestValuesAndSeparators()
        {
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    "1,2", Char.MinValue, new CultureInfo("en-US")),
                12, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    "1,2", ',', new CultureInfo("en-US")),
                1.2, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    "", ',', null),
                double.NaN, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    null, Char.MinValue, null),
                double.NaN, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    null, ',', null),
                double.NaN, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    "1,2,3", ',', null),
                double.NaN, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    "$1.2", '.', new CultureInfo("en-US")),
                1.2, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                    "3,14E+28", ',', null),
                3.14E28, 5);
            Assert.Equal(
                TestValuesAndSeparatorsLogic(
                "$1.2", '.', new CultureInfo("fr-FR")),
                1.2, 5);
            }

        private double TestValuesAndSeparatorsLogic(string value,
            char decimalSeparator, CultureInfo culture)
        {
            CultureInfo originalCulture = null;
            if(culture != null)
            {
                originalCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture = culture;
            }
            NumericDataHelper helper = new NumericDataHelper(new PropertyBag());
            helper.DecimalSeparator = decimalSeparator;
            double converted = helper.ParseNumber(value);

            if (originalCulture != null)
            {
                Thread.CurrentThread.CurrentCulture = originalCulture;
            }

            return converted;
        }
    }
}
