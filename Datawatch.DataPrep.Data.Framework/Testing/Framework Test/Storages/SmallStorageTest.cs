﻿using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;

// "Storages" namespace just to group tests.
namespace Datawatch.DataPrep.Data.Framework.Storages
{
    public class SmallStorageTest
    {
        private static readonly CultureInfo
            culture = CultureInfo.InvariantCulture;

        /// <summary>
        /// Can change capacity and keep existing values.
        /// </summary>
        [Fact]
        public void ChangeCapacity()
        {
            SmallStorage<string> storage = new SmallStorage<string>(10);
            for (int i = 0; i < 10; i++)
            {
                storage[i] = i.ToString(culture);
            }
            int initial = storage.Capacity;

            // Increase capacity and assert elements are kept.
            storage.Capacity = 15;
            int larger = storage.Capacity;
            Assert.True(larger > initial,
                "Of course, the storage could do this if it wanted, " +
                "but it ruins the point of the test.");
            for (int i = 0; i < 10; i++)
            {
                Assert.True(
                    0 == string.Compare(i.ToString(culture), storage[i], culture, CompareOptions.None),
                    "Growing the storage failed.");
            }
            // (We don't care what's in the new positions).

            // Decrease capacity and assert remaining elements are kept.
            storage.Capacity = 5;
            int smaller = storage.Capacity;
            Assert.True(smaller < larger,
                "Of course, the storage could do this if it wanted, " +
                "but it ruins the point of the test.");
            for (int i = 0; i < 5; i++)
            {
                Assert.True(
                    0 == string.Compare(i.ToString(culture), storage[i], culture, CompareOptions.None),
                    "Shrinking the storage failed.");
            }
        }

        /// <summary>
        /// Can clear all and a range, to default and non-default.
        /// </summary>
        [Fact]
        public void Clear()
        {
            int count = 15;
            SmallStorage<string> storage = new SmallStorage<string>(count);
            
            // First, clear with a non-default value.
            storage.Clear("Olle");
            for (int i = 0; i < count; i++) {
                Assert.True(0 == string.Compare("Olle", storage[i], culture, CompareOptions.None),
                    "Didn't clear with proper non-default.");
            }

            // Then, clear with default.
            storage.Clear(null);
            for (int i = 0; i < count; i++) {
                Assert.True(storage[i] == null, "Didn't clear with default.");
            }

            // Now do the same with the range overload.
            storage.Clear(3, 10, "Pelle");
            for (int i = 0; i < count; i++) {
                if (i >= 3 && (i - 3) < 10) {
                    Assert.True(
                        0 == string.Compare("Pelle", storage[i], culture, CompareOptions.None),
                        "Didn't clear range with non-default.");
                } else {
                    Assert.True(storage[i] == null,
                        "Shouldn't touch elements outside range.");
                }
            }

            // Set all elements to same value, then do default.
            storage.Clear("Pelle");
            storage.Clear(3, 10, null);

            for (int i = 0; i < count; i++) {
                if (i >= 3 && (i - 3) < 10) {
                    Assert.True(storage[i] == null,
                        "Didn't clear range with default.");
                } else {
                    Assert.True(
                        0 == string.Compare("Pelle", storage[i], culture, CompareOptions.None),
                        "Shouldn't touch elements outside range.");
                }
            }
        }

        /// <summary>
        /// Can compact, and reuses the elements in the holes.
        /// </summary>
        [Fact]
        public void Compact()
        {
            SmallStorage<string> storage = new SmallStorage<string>(10);
            for (int i = 0; i < 10; i++) {
                storage[i] = i.ToString(culture);
            }
            
            // Holes at first and last position, and two consecutive.
            int[] holes = { 0, 3, 4, 6, 9, 10 };
            storage.Compact(holes);

            Assert.Equal("1", storage[0]);
            Assert.Equal("2", storage[1]);
            Assert.Equal("5", storage[2]);
            Assert.Equal("7", storage[3]);
            Assert.Equal("8", storage[4]);

            // Additionally, for the SmallStorage we require it to move
            // the hole records to the end of the storage for reuse.
            Assert.True(SmallStorage<string>.ReuseOnCompact);
            Assert.True(storage.Capacity >= 10,
                "If the storage has reduced the capacity it violates " +
                "the reuse requirement.");

            // But we don't require them to be in any particular order.
            HashSet<string> reused = new HashSet<string>();
            reused.Add("0");
            reused.Add("3");
            reused.Add("4");
            reused.Add("6");
            reused.Add("9");

            for (int i = 5; i < 10; i++) {
                string value = storage[i];
                Assert.True(reused.Contains(value));
                reused.Remove(value);
            }
        }
    }
}
