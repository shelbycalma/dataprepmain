﻿using System;
using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;


// "Storages" namespace just to group tests.
namespace Datawatch.DataPrep.Data.Framework.Storages
{
    // TODO: Would REALLY like to test CountingSort too, but it's internal.
    // TODO: Would like to test SetIncrementing too, but it's internal.
    public class StorageIntTest
    {
        /// <summary>
        /// Can change capacity in four ways, and keep existing values.
        /// </summary>
        [Fact]
        public void ChangeCapacity()
        {
            int count = StorageInt.SegmentSize + 5;
            StorageInt storage = new StorageInt(count);
            for (int i = 0; i < count; i++) {
                // Avoid the default 0 value.
                storage[i] = 1 + i;
            }

            // Test #3b: Same number of segments, last larger.
            storage.Capacity = StorageInt.SegmentSize + 10;
            for (int i = 0; i < count; i++) {
                Assert.True(1 + i == storage[i],
                    "Growing storage (#3b) failed.");
            }
            // Fill in new positions.
            int newCount = StorageInt.SegmentSize + 10;
            for (int i = count; i < newCount; i++) {
                storage[i] = 1 + i;
            }
            count = newCount;

            // Test #1: More segments.
            storage.Capacity = 2 * StorageInt.SegmentSize + 5;
            for (int i = 0; i < count; i++) {
                Assert.True(1 + i == storage[i],
                    "Growing storage (#1) failed.");
            }

            // Test #2: Fewer segments.
            count = StorageInt.SegmentSize + 5;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                Assert.True(1 + i == storage[i],
                    "Shrinking storage (#2) failed.");
            }

            // Test #3a: Same number of segments, last smaller.
            count = StorageInt.SegmentSize + 2;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                Assert.True(1 + i == storage[i],
                    "Shrinking storage (#3a) failed.");
            }
        }

        /// <summary>
        /// Can clear all and a range, to default and non-default.
        /// </summary>
        [Fact]
        public void Clear()
        {
            int count = StorageInt.SegmentSize + 50;
            StorageInt storage = new StorageInt(count);
            
            // First, clear with a non-default value.
            storage.Clear(17);
            for (int i = 0; i < count; i++) {
                Assert.True(17 == storage[i],
                    "Didn't clear with proper non-default.");
            }

            // Then, clear with default.
            storage.Clear(0);
            for (int i = 0; i < count; i++) {
                Assert.True(0 == storage[i],
                    "Didn't clear with default.");
            }

            // Now do the same with the range overload.
            // Make it straddle a segment boundary.
            int rangeStart = count - 75;
            int rangeCount = 50;
            storage.Clear(rangeStart, rangeCount, 42);
            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    Assert.True(42 == storage[i],
                        "Didn't clear range with non-default.");
                } else {
                    Assert.True(0 == storage[i],
                        "Shouldn't touch elements outside range.");
                }
            }

            // Set all elements to same value, then do default.
            storage.Clear(4711);
            storage.Clear(rangeStart, rangeCount, 0);

            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    Assert.True(0 == storage[i],
                        "Didn't clear range with default.");
                } else {
                    Assert.True(4711 == storage[i],
                        "Shouldn't touch elements outside range.");
                }
            }
        }

        /// <summary>
        /// Can compact (we don't care about reuse).
        /// </summary>
        [Fact]
        public void Compact()
        {
            StorageInt storage = new StorageInt(10);
            for (int i = 0; i < 10; i++) {
                // Avoid the default 0 value.
                storage[i] = 1 + i;
            }
            
            // Holes at first an last position, and two consecutive.
            int[] holes = { 0, 3, 4, 6, 9, 10 };
            storage.Compact(holes);

            Assert.Equal(2, storage[0]);
            Assert.Equal(3, storage[1]);
            Assert.Equal(6, storage[2]);
            Assert.Equal(8, storage[3]);
            Assert.Equal(9, storage[4]);
        }

        /// <summary>
        /// Can copy to a null or existing storage.
        /// </summary>
        [Fact]
        public void CopyToStorage()
        {
            int count = StorageInt.SegmentSize + 25;
            StorageInt source = new StorageInt(count);
            for (int i = 0; i < count; i++) {
                // Avoid default 0 value.
                source[i] = 1 + i;
            }

            StorageInt target = null;
            target = source.CopyTo(target);
            for (int i = 0; i < count; i++) {
                Assert.True(1 + i == target[i],
                    "Failed to copy to new storage.");
            }
            target = new StorageInt(12);
            StorageInt copy = source.CopyTo(target);
            Assert.True(object.ReferenceEquals(target, copy),
                "Didn't reuse target storage");
            Assert.True(target.Capacity >= source.Capacity,
                "Didn't allocate enough space in target.");
            for (int i = 0; i < count; i++) {
                Assert.True(Math.Abs(1 + i - target[i]) < 1e-5,
                    "Failed to copy to reused storage.");
            }
        }

        /// <summary>
        /// Can copy to a storage through an index map.
        /// </summary>
        [Fact]
        public void CopyToStorageMapped()
        {
            int capacity = StorageInt.SegmentSize + 50;
            StorageInt source = new StorageInt(capacity);
            for (int i = 0; i < capacity; i++) {
                // Avoid default 0 value.
                source[i] = 1 + i;
            }

            int count = capacity / 2;
            StorageInt map = new StorageInt(count);
            for (int i = 0; i < count; i++) {
                // Copy out every other element, starting with last.
                // Note that map[0] is index of last element in source,
                // the value at that index is source[map[0]] = capacity.
                map[i] = capacity - 2 * i - 1;
            }

            StorageInt target = new StorageInt(17);
            StorageInt copy = source.CopyTo(map, count, target);
            Assert.True(object.ReferenceEquals(target, copy),
                "Didn't reuse target storage");
            Assert.True(target.Capacity >= count,
                "Didn't allocate enough space in target.");
            for (int i = 0; i < count; i++) {
                // Note that map has indices, and the value at every index
                // in source is 1 + i, so source[i] = i + 1. That's why the
                // loop above has a "-1" and there's none here.
                int expected = capacity - 2 * i;
                Assert.True(expected == target[i],
                    "Failed to copy through map.");
            }
        }

        /// <summary>
        /// Can build a key index for a set of strings.
        /// </summary>
        [Fact]
        public void GetIndex()
        {
            StoragePointer<string> strings = new StoragePointer<string>(7);            
            strings[0] = "B";
            strings[1] = null;
            strings[2] = "A";
            strings[3] = "B";
            strings[4] = "C";
            strings[5] = null;
            strings[6] = "X";

            KeyIndex reuse = new KeyIndex(3);
            KeyIndex index = StorageInt.GetIndex(strings, 6, reuse);
            Assert.True(object.ReferenceEquals(reuse, index),
                "Failed to reuse the index.");

            Assert.True(6 == index.Count,
                "Should contain six keys.");
            Assert.True(3 == index.MaxKey,
                "Should have seen four distinct values (0-3).");
            Assert.True(index.IsDistinct.HasValue && !index.IsDistinct.Value,
                "Should have set IsDistinct, and there are duplicates.");

            Assert.Equal(0, index.Keys[0]); // "B"
            Assert.Equal(1, index.Keys[1]); // null
            Assert.Equal(2, index.Keys[2]); // "A"
            Assert.Equal(0, index.Keys[3]); // "B" again
            Assert.Equal(3, index.Keys[4]); // "C"
            Assert.Equal(1, index.Keys[5]); // null again
        }

        /// <summary>
        /// Can build a key index for strings through an index map.
        /// </summary>
        [Fact]
        public void GetIndexMapped()
        {
            StoragePointer<string> strings = new StoragePointer<string>(7);            
            strings[0] = "B";
            strings[1] = null;
            strings[2] = "A";
            strings[3] = "B";
            strings[4] = "C";
            strings[5] = null;
            strings[6] = "X";

            StorageInt map = new StorageInt(6);
            map[0] = 5; // null
            map[1] = 6; // "X"
            map[2] = 0; // "B"
            map[3] = 3; // "B" again
            map[4] = 2; // "A"
            map[5] = 5; // null again

            KeyIndex reuse = new KeyIndex(3);
            KeyIndex index = StorageInt.GetIndex(map, strings, 6, reuse);
            Assert.True(object.ReferenceEquals(reuse, index),
                "Failed to reuse the index.");

            Assert.True(6 == index.Count,
                "Should contain six keys.");
            Assert.True(3 == index.MaxKey,
                "Should have seen four distinct values (0-3).");
            Assert.True(index.IsDistinct.HasValue && !index.IsDistinct.Value,
                "Should have set IsDistinct, and there are duplicates.");

            Assert.Equal(0, index.Keys[0]); // null
            Assert.Equal(1, index.Keys[1]); // "X"
            Assert.Equal(2, index.Keys[2]); // "B"
            Assert.Equal(2, index.Keys[3]); // "B" again
            Assert.Equal(3, index.Keys[4]); // "A"
            Assert.Equal(0, index.Keys[5]); // null again
        }

        /// <summary>
        /// Can find the maximum element (and not).
        /// </summary>
        [Fact]
        public void GetMax()
        {
            int count = StorageInt.SegmentSize + 50;
            StorageInt storage = new StorageInt(count);
            for (int i = 0; i < count; i++) {
                if (i == count - 17) {
                    storage[i] = 47;
                } else {
                    storage[i] = (i % 17) + 1;
                }
            }

            int max = storage.GetMax(count);
            Assert.True(47 == max,
                "Didn't find the maximum value.");

            max = storage.GetMax(count - 17);
            Assert.True(17 == max,
                "Looks like it ignored the count parameter.");
        }
    }
}
