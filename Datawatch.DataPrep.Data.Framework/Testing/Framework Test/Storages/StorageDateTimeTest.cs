﻿using System;
using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;


// "Storages" namespace just to group tests.
namespace Datawatch.DataPrep.Data.Framework.Storages
{
    public class StorageDateTimeTest
    {
        /// <summary>
        /// Can change capacity in four ways, and keep existing values.
        /// </summary>
        [Fact]
        public void ChangeCapacity()
        {
            DateTime start = new DateTime(2013, 1, 1);

            int count = StorageDateTime.SegmentSize + 5;
            StorageDateTime storage = new StorageDateTime(count);
            for (int i = 0; i < count; i++)
            {
                storage[i] = start.AddTicks(i);
            }

            // Test #3b: Same number of segments, last larger.
            storage.Capacity = StorageDateTime.SegmentSize + 10;
            for (int i = 0; i < count; i++)
            {
                Assert.True(start.AddTicks(i) == storage[i],
                    "Growing storage (#3b) failed.");
            }
            // Fill in new positions.
            int newCount = StorageDateTime.SegmentSize + 10;
            for (int i = count; i < newCount; i++)
            {
                storage[i] = start.AddTicks(i);
            }
            count = newCount;

            // Test #1: More segments.
            storage.Capacity = 2 * StorageDateTime.SegmentSize + 5;
            for (int i = 0; i < count; i++)
            {
                Assert.True(start.AddTicks(i) == storage[i],
                    "Growing storage (#1) failed.");
            }

            // Test #2: Fewer segments.
            count = StorageDateTime.SegmentSize + 5;
            storage.Capacity = count;
            for (int i = 0; i < count; i++)
            {
                Assert.True(start.AddTicks(i) == storage[i],
                    "Shrinking storage (#2) failed.");
            }

            // Test #3a: Same number of segments, last smaller.
            count = StorageDateTime.SegmentSize + 2;
            storage.Capacity = count;
            for (int i = 0; i < count; i++)
            {
                Assert.True(start.AddTicks(i) == storage[i],
                    "Shrinking storage (#3a) failed.");
            }
        }

        /// <summary>
        /// Can clear all and a range, to default and non-default.
        /// </summary>
        [Fact]
        public void Clear()
        {
            int count = StorageDateTime.SegmentSize + 50;
            StorageDateTime storage = new StorageDateTime(count);
            
            // First, clear with a non-default value.
            storage.Clear(new DateTime(2013, 1, 1));
            for (int i = 0; i < count; i++) {
                Assert.True(new DateTime(2013, 1, 1) == storage[i],
                    "Didn't clear with proper non-default.");
            }

            DateTime empty = new DateTime();

            // Then, clear with default.
            storage.Clear(empty);
            for (int i = 0; i < count; i++) {
                Assert.True(empty == storage[i],
                    "Didn't clear with default.");
            }

            // Now do the same with the range overload.
            // Make it straddle a segment boundary.
            int rangeStart = count - 75;
            int rangeCount = 50;
            storage.Clear(rangeStart, rangeCount, new DateTime(2013, 1, 2));
            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    Assert.True(new DateTime(2013, 1, 2) == storage[i],
                        "Didn't clear range with non-default.");
                } else {
                    Assert.True(empty == storage[i],
                        "Shouldn't touch elements outside range.");
                }
            }

            // Set all elements to same value, then do default.
            storage.Clear(new DateTime(2013, 1, 3));
            storage.Clear(rangeStart, rangeCount, empty);

            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    Assert.True(empty == storage[i],
                        "Didn't clear range with default.");
                } else {
                    Assert.True(new DateTime(2013, 1, 3) == storage[i],
                        "Shouldn't touch elements outside range.");
                }
            }
        }

        /// <summary>
        /// Can compact (we don't care about reuse).
        /// </summary>
        [Fact]
        public void Compact()
        {
            StorageDateTime storage = new StorageDateTime(10);
            for (int i = 0; i < 10; i++) {
                storage[i] = new DateTime(2013, 1, i + 1);
            }
            
            // Holes at first an last position, and two consecutive.
            int[] holes = { 0, 3, 4, 6, 9, 10 };
            storage.Compact(holes);

            Assert.Equal(new DateTime(2013, 1, 1 + 1), storage[0]);
            Assert.Equal(new DateTime(2013, 1, 2 + 1), storage[1]);
            Assert.Equal(new DateTime(2013, 1, 5 + 1), storage[2]);
            Assert.Equal(new DateTime(2013, 1, 7 + 1), storage[3]);
            Assert.Equal(new DateTime(2013, 1, 8 + 1), storage[4]);
        }

        /// <summary>
        /// Can copy to a null or existing storage.
        /// </summary>
        [Fact]
        public void CopyToStorage()
        {
            DateTime start = new DateTime(2013, 1, 1);

            int count = StorageDateTime.SegmentSize + 25;
            StorageDateTime source = new StorageDateTime(count);
            for (int i = 0; i < count; i++) {
                source[i] = start.AddTicks(i);
            }

            StorageDateTime target = null;
            target = source.CopyTo(target);
            for (int i = 0; i < count; i++) {
                Assert.True(start.AddTicks(i) == target[i],
                    "Failed to copy to new storage.");
            }
            target = new StorageDateTime(12);
            StorageDateTime copy = source.CopyTo(target);
            Assert.True(Object.ReferenceEquals(target, copy),
                "Didn't reuse target storage");
            Assert.True(target.Capacity >= source.Capacity,
                "Didn't allocate enough space in target.");
            for (int i = 0; i < count; i++) {
                Assert.True(start.AddTicks(i) == target[i],
                    "Failed to copy to reused storage.");
            }
        }
    }
}
