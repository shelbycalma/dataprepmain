﻿using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;


// "Storages" namespace just to group tests.
namespace Datawatch.DataPrep.Data.Framework.Storages
{
    public class StorageBoolTest
    {
        /// <summary>
        /// Can change capacity in four ways, and keep existing values.
        /// </summary>
        [Fact]
        public void ChangeCapacity()
        {
            // Cheat. We know it packs 32 bools into an Int32.
            int segmentSize = 32 * StorageInt.SegmentSize;

            int count = segmentSize + 5;
            StorageBool storage = new StorageBool(count);
            // Fill with 100100100... pattern.
            for (int i = 0; i < count; i++) {
                storage[i] = (i % 3) == 0;
            }

            // Test #3b: Same number of segments, last larger.
            storage.Capacity = segmentSize + 10;
            for (int i = 0; i < count; i++) {
                Assert.True(((i % 3) == 0) == storage[i],
                    "Growing storage (#3b) failed.");
            }
            // Fill in new positions.
            int newCount = segmentSize + 10;
            for (int i = count; i < newCount; i++) {
                storage[i] = (i % 3) == 0;
            }
            count = newCount;

            // Test #1: More segments.
            storage.Capacity = 2 * segmentSize + 5;
            for (int i = 0; i < count; i++) {
                Assert.True(((i % 3) == 0) == storage[i],
                    "Growing storage (#1) failed.");
            }

            // Test #2: Fewer segments.
            count = segmentSize + 5;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                Assert.True(((i % 3) == 0) == storage[i],
                    "Shrinking storage (#2) failed.");
            }

            // Test #3a: Same number of segments, last smaller.
            count = StorageInt.SegmentSize + 2;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                Assert.True(((i % 3) == 0) == storage[i],
                    "Shrinking storage (#3a) failed.");
            }
        }
    }
}
