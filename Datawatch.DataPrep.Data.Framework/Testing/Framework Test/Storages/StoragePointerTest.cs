﻿using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;


// "Storages" namespace just to group tests.
namespace Datawatch.DataPrep.Data.Framework.Storages
{
    public class StoragePointerTest
    {
        private static readonly CultureInfo
            culture = CultureInfo.InvariantCulture;

        /// <summary>
        /// Can change capacity in four ways, and keep existing values.
        /// </summary>
        [Fact]
        public void ChangeCapacity()
        {
            int count = StoragePointer<string>.SegmentSize + 5;
            StoragePointer<string> storage = new StoragePointer<string>(count);
            for (int i = 0; i < count; i++) {
                storage[i] = i.ToString(culture);
            }

            // Test #3b: Same number of segments, last larger.
            storage.Capacity = StoragePointer<string>.SegmentSize + 10;
            for (int i = 0; i < count; i++) {
                Assert.True(0 == string.Compare(i.ToString(culture), storage[i], culture, CompareOptions.None),
                    "Growing storage (#3b) failed.");
            }
            // Fill in new positions.
            int newCount = StoragePointer<string>.SegmentSize + 10;
            for (int i = count; i < newCount; i++) {
                storage[i] = i.ToString(culture);
            }
            count = newCount;

            // Test #1: More segments.
            storage.Capacity = 2 * StoragePointer<string>.SegmentSize + 5;
            for (int i = 0; i < count; i++) {
                Assert.True(0 == string.Compare(i.ToString(culture), storage[i], culture, CompareOptions.None),
                    "Growing storage (#1) failed.");
            }

            // Test #2: Fewer segments.
            count = StoragePointer<string>.SegmentSize + 5;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                Assert.True(0 == string.Compare(i.ToString(culture), storage[i], culture, CompareOptions.None),
                    "Shrinking storage (#2) failed.");
            }

            // Test #3a: Same number of segments, last smaller.
            count = StoragePointer<string>.SegmentSize + 2;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                Assert.True(0 == string.Compare(i.ToString(culture), storage[i], culture, CompareOptions.None),
                    "Shrinking storage (#3a) failed.");
            }
        }

        /// <summary>
        /// Can clear all and a range, to default and non-default.
        /// </summary>
        [Fact]
        public void Clear()
        {
            int count = StoragePointer<string>.SegmentSize + 50;
            StoragePointer<string> storage = new StoragePointer<string>(count);
            
            // First, fill with a non-default value.
            for (int i = 0; i < count; i++) {
                storage[i] = "Olle";
            }
            storage.Clear();
            for (int i = 0; i < count; i++) {
                Assert.True(storage[i] == null,
                    "Didn't clear with default.");
            }

            // Now do the same with the range overload.
            // Make it straddle a segment boundary.
            for (int i = 0; i < count; i++) {
                storage[i] = "Pelle";
            }
            int rangeStart = count - 75;
            int rangeCount = 50;
            storage.Clear(rangeStart, rangeCount);
            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    Assert.True(storage[i] == null,
                        "Didn't clear range with default.");
                } else {
                    Assert.True(string.Equals("Pelle", storage[i]),
                        "Shouldn't touch elements outside range.");
                }
            }
        }

        /// <summary>
        /// Can compact (we don't care about reuse).
        /// </summary>
        [Fact]
        public void Compact()
        {
            StoragePointer<string> storage = new StoragePointer<string>(10);
            for (int i = 0; i < 10; i++) {
                storage[i] = i.ToString(culture);
            }
            
            // Holes at first an last position, and two consecutive.
            int[] holes = { 0, 3, 4, 6, 9, 10 };
            storage.Compact(holes);

            Assert.Equal("1", storage[0]);
            Assert.Equal("2", storage[1]);
            Assert.Equal("5", storage[2]);
            Assert.Equal("7", storage[3]);
            Assert.Equal("8", storage[4]);
        }

        /// <summary>
        /// Can copy to a null or existing storage.
        /// </summary>
        [Fact]
        public void CopyToStorage()
        {
            int count = StoragePointer<string>.SegmentSize + 25;
            StoragePointer<string> source = new StoragePointer<string>(count);
            for (int i = 0; i < count; i++) {
                source[i] = i.ToString(culture);
            }

            StoragePointer<string> target = null;
            target = source.CopyTo(target);
            for (int i = 0; i < count; i++) {
                Assert.True(0 == string.Compare(i.ToString(culture), target[i], culture, CompareOptions.None),
                    "Failed to copy to new storage.");
            }
            target = new StoragePointer<string>(12);
            StoragePointer<string> copy = source.CopyTo(target);
            Assert.True(object.ReferenceEquals(target, copy),
                "Didn't reuse target storage");
            Assert.True(target.Capacity >= source.Capacity,
                "Didn't allocate enough space in target.");
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(i.ToString(culture), target[i],
                    "Failed to copy to reused storage.");
            }
        }
    }
}
