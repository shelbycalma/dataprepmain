﻿using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;


namespace Datawatch.DataPrep.Data.Framework.Storages
{
    public class StringIndexTest
    {
        /// <summary>
        /// Excercise populating with regular GetKey.
        /// </summary>
        [Fact]
        public void GetKey()
        {
            StringIndex index = new StringIndex(4);            
            Assert.True(0 == index.Count,
                "Should be empty initially");

            Assert.True(0 == index.GetKey("A"),
                "First value should get key 0");
            Assert.True(1 == index.GetKey(null),
                "Null values should be supported");
            Assert.True(0 == index.GetKey("A"),
                "Should find existing key");
            Assert.True(1 == index.GetKey(null),
                "Should find existing null key");
            Assert.True(2 == index.GetKey("a"),
                "Should be case-sensitive");
            
            Assert.True(3 == index.Count,
                "There are three values in there now");
        }

        /// <summary>
        /// Excercise populating with 'TryGetKey'.
        /// </summary>
        [Fact]
        public void TryGetKey()
        {
            StringIndex index = new StringIndex(4);            

            int key;
            Assert.True(index.GetKey("A", out key));
            Assert.True(index.GetKey(null, out key));
            Assert.False(index.GetKey("A", out key));
            Assert.False(index.GetKey(null, out key));
            Assert.True(index.GetKey("a", out key));
            
            Assert.True(3 == index.Count,
                "There are three values in there now");
        }
    }
}
