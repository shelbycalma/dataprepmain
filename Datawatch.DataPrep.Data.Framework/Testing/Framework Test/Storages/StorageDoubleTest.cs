﻿using System;
using Datawatch.DataPrep.Data.Framework.Storage;
using Xunit;


// "Storages" namespace just to group tests.
namespace Datawatch.DataPrep.Data.Framework.Storages
{
    public class StorageDoubleTest
    {
        /// <summary>
        /// Can change capacity in four ways, and keep existing values.
        /// </summary>
        [Fact]
        public void ChangeCapacity()
        {
            int count = StorageDouble.SegmentSize + 5;
            StorageDouble storage = new StorageDouble(count);
            for (int i = 0; i < count; i++) {
                // Avoid the default 0.0 value.
                storage[i] = 1.0 + i;
            }

            // Test #3b: Same number of segments, last larger.
            storage.Capacity = StorageDouble.SegmentSize + 10;
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(1.0 + i, storage[i], 1e-5,
                    "Growing storage (#3b) failed.");
            }
            // Fill in new positions.
            int newCount = StorageDouble.SegmentSize + 10;
            for (int i = count; i < newCount; i++) {
                storage[i] = 1.0 + i;
            }
            count = newCount;

            // Test #1: More segments.
            storage.Capacity = 2 * StorageDouble.SegmentSize + 5;
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(1.0 + i, storage[i], 1e-5,
                    "Growing storage (#1) failed.");
            }

            // Test #2: Fewer segments.
            count = StorageDouble.SegmentSize + 5;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(1.0 + i, storage[i], 1e-5,
                    "Shrinking storage (#2) failed.");
            }

            // Test #3a: Same number of segments, last smaller.
            count = StorageDouble.SegmentSize + 2;
            storage.Capacity = count;
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(1.0 + i, storage[i], 1e-5,
                    "Shrinking storage (#3a) failed.");
            }
        }

        /// <summary>
        /// Can clear all and a range, to default and non-default.
        /// </summary>
        [Fact]
        public void Clear()
        {
            int count = StorageDouble.SegmentSize + 50;
            StorageDouble storage = new StorageDouble(count);
            
            // First, clear with a non-default value.
            storage.Clear(17.0);
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(17.0, storage[i], 1e-5,
                    "Didn't clear with proper non-default.");
            }

            // Then, clear with default.
            storage.Clear(0.0);
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(0.0, storage[i], 1e-5,
                    "Didn't clear with default.");
            }

            // Now do the same with the range overload.
            // Make it straddle a segment boundary.
            int rangeStart = count - 75;
            int rangeCount = 50;
            storage.Clear(rangeStart, rangeCount, 42.0);
            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    AssertHelper.Equal(42.0, storage[i], 1e-5,
                        "Didn't clear range with non-default.");
                } else {
                    AssertHelper.Equal(0.0, storage[i], 1e-5,
                        "Shouldn't touch elements outside range.");
                }
            }

            // Set all elements to same value, then do default.
            storage.Clear(4711.0);
            storage.Clear(rangeStart, rangeCount, 0.0);

            for (int i = 0; i < count; i++) {
                if (i >= rangeStart && (i - rangeStart) < rangeCount) {
                    AssertHelper.Equal(0.0, storage[i], 1e-5,
                        "Didn't clear range with default.");
                } else {
                    AssertHelper.Equal(4711.0, storage[i], 1e-5,
                        "Shouldn't touch elements outside range.");
                }
            }
        }

        /// <summary>
        /// Can compact (we don't care about reuse).
        /// </summary>
        [Fact]
        public void Compact()
        {
            StorageDouble storage = new StorageDouble(10);
            for (int i = 0; i < 10; i++) {
                // Avoid the default 0.0 value.
                storage[i] = 1.0 + i;
            }
            
            // Holes at first and last position, and two consecutive.
            int[] holes = { 0, 3, 4, 6, 9, 10 };
            storage.Compact(holes);

            Assert.Equal(2.0, storage[0], 5);
            Assert.Equal(3.0, storage[1], 5);
            Assert.Equal(6.0, storage[2], 5);
            Assert.Equal(8.0, storage[3], 5);
            Assert.Equal(9.0, storage[4], 5);
        }

        /// <summary>
        /// Can copy to a null or existing storage.
        /// </summary>
        [Fact]
        public void CopyToStorage()
        {
            int count = StorageDouble.SegmentSize + 25;
            StorageDouble source = new StorageDouble(count);
            for (int i = 0; i < count; i++) {
                // Avoid default 0.0 value.
                source[i] = 1.0 + i;
            }

            StorageDouble target = null;
            target = source.CopyTo(target);
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(1.0 + i, target[i], 1e-5,
                    "Failed to copy to new storage.");
            }
            target = new StorageDouble(12);
            StorageDouble copy = source.CopyTo(target);
            Assert.True(object.ReferenceEquals(target, copy),
                "Didn't reuse target storage");
            Assert.True(target.Capacity >= source.Capacity,
                "Didn't allocate enough space in target.");
            for (int i = 0; i < count; i++) {
                AssertHelper.Equal(1.0 + i, target[i], 1e-5,
                    "Failed to copy to reused storage.");
            }
        }
    }
}
