﻿using System;
using System.Globalization;
using Xunit;

namespace Datawatch.DataPrep.Data.Framework
{
    /// <summary>
    /// Created to simplify migration from NUnit to Xunit tests.
    /// </summary>
    public static class AssertHelper
    {
        public static void ThrowsWithMessage<TExc>(Action action, string msg) 
            where TExc : Exception
        {
            var exc = Assert.Throws<TExc>(action);
            Assert.Equal(exc.Message, msg);
        }

        public static void Equal(double d1, double d2, double e, string msg)
        {
            if (Double.IsInfinity(d1) || Double.IsNaN(d1) || 
                Double.IsInfinity(d2) || Double.IsNaN(d2))
            {
                Assert.True((d1 as IEquatable<Double>).Equals(d2));
                return;
            }

            Assert.True(Math.Abs(d1 - d2) < e, msg);
        }

        public static void Equal(string s1, string s2, CultureInfo culture, string msg)
        {
            Assert.True(
                0 == string.Compare(s1, s2, culture, CompareOptions.None), msg);
        }

        public static void Equal(string s1, string s2, string msg)
        {
            Assert.True(string.Equals(s1, s2), msg);
        }
    }
}
