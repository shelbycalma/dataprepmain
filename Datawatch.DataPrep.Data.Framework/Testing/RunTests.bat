@echo off
set config=Release
set bit=x86
IF NOT [%1] == [] (
		set config=%1
	)
IF NOT [%2] == [] (
		set bit=%2
	)


set tests="Framework Test\bin\%config%\Frawework Test.dll"

echo tests

if "%bit%" == "x64" (
	GOTO find_xunit_x64
	) else (
	GOTO find_xunit_x86
	)


:dotest
if defined xcp (
echo "xunit => %xcp%"
) else (
echo xunit not found
)

mkdir "reports_dotnet_%bit%"
%xcp% %tests% -noshadow -html reports_dotnet_%bit%\DotNetTestReport.html -xml reports_dotnet_%bit%\TestResults_xunit.xml -nunit reports_dotnet_%bit%\TestResults.xml
GOTO end


:find_xunit_x86
 for /r "../packages" %%a in ('DIR *.* /B /O:-D') do (
	if "%%~nxa"=="xunit.console.x86.exe" set xcp=%%~dpnxa
	)
 GOTO :dotest

:find_xunit_x64
 for /r "../packages" %%a in ('DIR *.* /B /O:-D') do (
	if "%%~nxa"=="xunit.console.exe" set xcp=%%~dpnxa
	)
 GOTO :dotest

:end