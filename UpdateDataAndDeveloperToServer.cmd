@echo off
echo.
echo Updates Data and Developer solutions to use prerelease versions of Data packages from "develop" branch using Datawatch package source.
echo.

call ph.cmd Update Datawatch -S All -T DD PD -PR .*develop