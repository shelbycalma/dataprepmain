@echo off

echo.
echo Updates Data, Developer.NET and Dashboards.NET solutions to use local debug versions of produced and referenced packages.
echo Local debug versions of packages are built, packed and placed into LocalDebug package source.
echo.

call ph.cmd Update Local -S All
pause