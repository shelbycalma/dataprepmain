﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    /**
     * This Unit Test class will for most of the test, test at the same time the 
     * two methods: 
     * 1. Contains(Dictionary<String, Object>)
     * 2. Contains(Object[])
     * in different scenarios. The same result should be expected for both 
     * methods.
     */
    public class ParameterFilterTest
    {
        private const String CompanyColumn = "Company";
        private const String RegionColumn = "Region";
        private const String MarketColumn = "Market";
        private const String McapColumn = "Marketcap";
        private const String ReportDateColumn = "ReportDate";
    
        private const int CompanyIndex = 0;
        private const int RegionIndex = 1;
        private const int MarketIndex = 2;
        private const int McapIndex = 3;
        private const int ReportDateIndex = 4;

        private const String MobileSystem = "MobileSystem";
        private const String RetailClothing = "Retail-Clothing";
    
        private const String Ericsson = "Ericsson";
        private const String Nokia = "Nokia";
    
        private const String Europe = "Europe";
        private const String Asia = "Asia";
        private const String NorthAmerica = "NorthAmerica";
    
        private const double Mcap75 = 75.0;
        private const double Mcap100 = 100.0;
        private const double Mcap150 = 150.0;
        private const double Mcap1200 = 1200.0;
    
        private readonly DateTime Date2014_01_01 = new DateTime(2014, 01, 01);
        private readonly DateTime Date2014_02_02 = new DateTime(2014, 02, 02);
    
        private static StandaloneTable table;
    
        public ParameterFilterTest() {
            table = new StandaloneTable();
            table.BeginUpdate();
            table.AddColumn(new TextColumn(CompanyColumn));
            table.AddColumn(new TextColumn(RegionColumn));
            table.AddColumn(new TextColumn(MarketColumn));
            table.AddColumn(new NumericColumn(McapColumn));
            table.AddColumn(new TimeColumn(ReportDateColumn));
            table.EndUpdate();
        }
    
        [Fact]
        public void NoParameters() {
            // No filter should be created if no parameters exist. 
            String[] names = {};
            Object[] values = {};
            IEnumerable<ParameterValue> parameters = 
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter = 
                    ParameterFilter.CreateFilter(table, parameters);

            Assert.True(parameterFilter == null);
        }

        [Fact]
        public void NonColumnParameter()
        {
            String[] names = { "NotAColumnName", "DefinitelyNotAColumnName" };
            Object[] values = { new Object[] { Europe, Asia }, Mcap150 };

            IEnumerable<ParameterValue> parameters =
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Assert.True(parameterFilter == null);
        }

        [Fact]
        public void OneSingleValue()
        {
            String[] names = { CompanyColumn };
            Object[] values = { Ericsson };

            IEnumerable<ParameterValue> parameters =
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Object[] rowValues = new Object[]{Ericsson, Europe, MobileSystem, 
                120, new DateTime(2011, 2, 3)};
            Dictionary<String, Object> dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[CompanyIndex] = Nokia;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));
        }

        [Fact]
        public void SeveralSingleValues()
        {
            String[] names = { RegionColumn, MarketColumn };
            Object[] values = { Europe, MobileSystem };

            IEnumerable<ParameterValue> parameters =
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Object[] rowValues = new Object[]{Nokia, Europe, MobileSystem, 300, 
                new DateTime(2013, 5, 8)};
            Dictionary<String, Object> dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = Asia;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));

            rowValues = new Object[]{Ericsson, Europe, MobileSystem, 120, 
                new DateTime(2011, 2, 3)};
            dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[MarketIndex] = RetailClothing;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));

        }

        [Fact]
        public void OneMultiValue()
        {
            // Test that rows that have region that is Europe or Asia is 
            // accepted.
            String[] names = { RegionColumn };
            Object[] values = { new Object[] { Europe, Asia } };

            IEnumerable<ParameterValue> parameters =
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Object[] rowValues = new Object[]{Ericsson, Europe, MobileSystem, 
                120, new DateTime(2011, 2, 3)};
            Dictionary<String, Object> dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = Asia;
            dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = NorthAmerica;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));
        }

        [Fact]
        public void SeveralMultiValues()
        {
            String[] names = { RegionColumn, McapColumn };
            Object[] values = {new Object[]{Europe, Asia}, 
                new Object[]{Mcap100, Mcap150, Mcap1200}};

            IEnumerable<ParameterValue> parameters =
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Object[] rowValues = new Object[]{Ericsson, Europe, MobileSystem, 
                Mcap100, new DateTime(2011, 2, 3)};
            Dictionary<String, Object> dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[McapIndex] = Mcap75;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));

            rowValues[McapIndex] = Mcap1200;
            dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = Asia;
            dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = NorthAmerica;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));

            rowValues[McapIndex] = Mcap75;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));
        }

        [Fact]
        public void MixedMultiAndSingleValue()
        {
            String[] names = { RegionColumn, ReportDateColumn };
            Object[] values = { new Object[] { Europe, Asia }, Date2014_01_01 };

            IEnumerable<ParameterValue> parameters =
                    CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Object[] rowValues = new Object[]{Ericsson, Europe, MobileSystem, 
                Mcap100, Date2014_01_01};
            Dictionary<String, Object> dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = Asia;
            dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = NorthAmerica;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));

            rowValues[RegionIndex] = Asia;
            dict = CreateDict(rowValues);
            Assert.True(parameterFilter.Contains(rowValues));
            Assert.True(parameterFilter.Contains(dict));

            rowValues[ReportDateIndex] = Date2014_02_02;
            dict = CreateDict(rowValues);
            Assert.False(parameterFilter.Contains(rowValues));
            Assert.False(parameterFilter.Contains(dict));
        }


        /***************    HELP METHODS   ******************/

        private Dictionary<String, Object> CreateDict(Object[] objects)
        {
            Dictionary<String, Object> row = new Dictionary<String, Object>();
            for (int i = 0; i < objects.Length; i++)
            {
                String columnName = table.GetColumn(i).Name;
                row[columnName] = objects[i];
            }
            return row;
        }
    }
}
