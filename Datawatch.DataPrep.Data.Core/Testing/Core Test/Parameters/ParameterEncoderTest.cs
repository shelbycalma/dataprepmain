﻿using System;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    public class ParameterEncoderTest
    {
        [Theory]
        [InlineData(new[] { "a" }, "$a", "a_value")]
        [InlineData(new[] { "a", "aa" }, "$a,$aa", "a_value,aa_value")]
        [InlineData(new[] { "a", "aa" }, "{a},{aa}", "a_value,aa_value")]
        [InlineData(new[] { "a", "b" }, "$a$b", "a_valueb_value")]
        [InlineData(new[] { "a" }, "$a$a", "a_valuea_value")]
        [InlineData(new[] { "a" }, "{a}{a}", "a_valuea_value")]
        // Added to validate [16970] 12.0 Release - incorrect parsing of 
        // parameters which are superstrings of valid parameters
        [InlineData(new[] { "a" }, "{ab}", "{ab}")]
        // Added to validate [17105] EXD - Designer crash with 
        // 'System.ArgumentOutOfRangeException' exception when a specific 
        // dashboard is opened in an example workbook.
        [InlineData(new[] { "abc" }, "{abc}some text{abc}", "abc_valuesome textabc_value")]
        [InlineData(new[] { "abc" }, "{de}{abc}some text{abc}{de}", "{de}abc_valuesome textabc_value{de}")]
        public void BasicStringReplacement(string[] names,
            string source, string result)
        {
            ParameterValueCollection parameters =
                new ParameterValueCollection();
            foreach (string name in names)
            {
                parameters.Add(name, name + "_value");
            }
            Assert.Equal(result, new ParameterEncoder
            {
                Parameters = parameters,
                SourceString = source
            }.Encoded());
        }
        // Added to validate [17104] H12-1-0-628 - Failing to apply parameter 
        // correctly in Designer when parameter is repeated.
        [Fact]
        public void ShouldReplaceRepeatedParameter()
        {
            const string source =
                "aj[" +
                "`time`sym;" +
                "select from  quotetickhistory where sym = `{sym}, time.time > 14:30:00, time.time < 22:30:00;" +
                "select from  tradetickhistory where sym = `{sym}, time.time > 14:30:00, time.time < 22:30:00]";
            const string expected =
                "aj[" +
                "`time`sym;" +
                "select from  quotetickhistory where sym = `GOOG.O, time.time > 14:30:00, time.time < 22:30:00;" +
                "select from  tradetickhistory where sym = `GOOG.O, time.time > 14:30:00, time.time < 22:30:00]";
            ParameterValueCollection parameters =
                new ParameterValueCollection { { "sym", "GOOG.O" } };
            Assert.Equal(expected,
                new ParameterEncoder()
                {
                    Parameters = parameters,
                    SourceString = source
                }.Encoded());
        }

        [Theory]
        [InlineData("42")]
        public void FormattedReplacement(string expected)
        {
            const string source = "{a:g}";
            string[] names = new[] { "a" };

            ParameterValueCollection parameters =
                new ParameterValueCollection();
            foreach (string name in names)
            {
                parameters.Add(name, 42.0);
            }
            Assert.Equal(expected, new ParameterEncoder
                {
                    Parameters = parameters,
                    SourceString = source
                }.Encoded());
        }

        [Theory]
        [InlineData("{a:;}", "1;2")]
        [InlineData("{a}", "1,2")]
        public void ArrayFormat(string source, string expected)
        {
            ArrayParameterValue arr = new ArrayParameterValue();
            arr.Add(new NumberParameterValue { Value = 1 });
            arr.Add(new NumberParameterValue { Value = 2 });
            ParameterValueCollection parameters = new ParameterValueCollection
            {
                {"a", arr}
            };
            Assert.Equal(expected, new ParameterEncoder
                {
                    Parameters = parameters,
                    SourceString = source
                }.Encoded());
        }

        [Theory]
        [InlineData("{a}", "val", true, "'val'")]
        [InlineData("{a}", "val", false, "val")]
        [InlineData("$a", "val", true, "'val'")]
        [InlineData("$a", "val", false, "val")]
        [InlineData("{a}", "'val'", true, "'val'")]
        [InlineData("{a}", "'val'", false, "val")]
        [InlineData("$a", "'val'", true, "'val'")]
        [InlineData("$a", "'val'", false, "val")]
        public void ShouldSingleQuoteTextAccordingToFlag(
            string source, string value, bool singleQuotes, string expected)
        {
            ParameterValueCollection parameters = new ParameterValueCollection
            {
                {"a", value}
            };
            ParameterEncoder encoder = new ParameterEncoder
            {
                Parameters = parameters,
                SourceString = source,
                EnforceSingleQuoteEnclosure = singleQuotes,
            };

            Assert.Equal(expected, encoder.Encoded());
        }

        [Theory]
        [InlineData(0.0, true, "0")]
        [InlineData(0.0, false, "0")]
        public void ShouldNeverSingleQuoteNumbers(
            double value, bool singleQuotes, string expected)
        {
            ParameterValueCollection collection = new ParameterValueCollection();
            collection.Add("a", value);

            const string query = "$a";
            ParameterEncoder encoder = new ParameterEncoder
            {
                SourceString = query,
                Parameters = collection,
                EnforceSingleQuoteEnclosure = singleQuotes
            };

            Assert.Equal(expected, encoder.Encoded());
        }

        [Fact]
        public void SingleQuoteHandleArray()
        {
            ArrayParameterValue arr = new ArrayParameterValue();
            arr.Add(new StringParameterValue { Value = "a" });
            arr.Add(new StringParameterValue { Value = "b" });
            ParameterValueCollection parameters = new ParameterValueCollection
            {
                {"arrname", arr}
            };

            ParameterEncoder parameterEncoder = new ParameterEncoder
            {
                Parameters = parameters,
                SourceString = "{arrname:+}",
                EnforceSingleQuoteEnclosure = true
            };

            const string expected = "'a'+'b'";
            string actual = parameterEncoder.Encoded();
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("NULL")]
        public void NullStringHandle(string expected)
        {
            ParameterValueCollection parameters = new ParameterValueCollection
            {
                {"a", new StringParameterValue()}
            };
            ParameterEncoder parameterEncoder = new ParameterEncoder
            {
                Parameters = parameters,
                SourceString = "$a",
                NullOrEmptyString = "NULL"
            };
            Assert.Equal(expected, parameterEncoder.Encoded());
        }

        [Fact]
        public void TestSourceStringNotChangedEmptyCollection()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "(You can't touch this) Yeah, that's how we living and you know";
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection();
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            Assert.Equal(sourceString, actual);
        }

        [Fact]
        public void TestSourceStringNotChangedNullCollection()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "(You can't touch this) Yeah, that's how we living and you know";
            encoder.SourceString = sourceString;
            encoder.Parameters = null;
            string actual = encoder.Encoded();
            Assert.Equal(sourceString, actual);
        }

        [Fact]
        public void TestSourceStringNotChangedEmptyCollectionNoFnutts()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "(You cant touch this) Yeah, thats how we living and you know";
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection();
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            Assert.Equal(sourceString, actual);
        }

        [Fact]
        public void TestSourceStringNotChangedEmptyCollection2()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "(You can't touch $this) Yeah, that's how we living and you know";
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection();
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            Assert.Equal(sourceString, actual);
        }

        [Fact]
        public void TestSourceStringNotChangedMissingValue()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "(You can't touch $this) Yeah, that's how we living and you know";
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection
            {
                {"parameter not in use", "value"}
            };
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            Assert.Equal(sourceString, actual);
        }

        [Fact]
        public void TestFnuttEscaped()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "(You can't touch $this) Yeah, that's how we living and you know";
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection
            {
                {"this", "my' 'ipad"}
            };
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            const string expected = "(You can't touch my' 'ipad) Yeah, that's how we living and you know";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestNullSourceString()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            string sourceString = null;
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection();
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            string expected = null;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestEmptySourceString()
        {
            ParameterEncoder encoder = new ParameterEncoder();
            const string sourceString = "";
            encoder.SourceString = sourceString;
            ParameterValueCollection pvc = new ParameterValueCollection();
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            const string expected = "";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HandleEmptyDate()
        {
            ParameterEncoder encoder =
                new ParameterEncoder { SourceString = "{Snapshot}" };
            ParameterValueCollection pvc = new ParameterValueCollection
                {{ "Snapshot", new DateTimeParameterValue() }};
            encoder.Parameters = pvc;
            string actual = encoder.Encoded();
            // DateTime is a struct and defaults to this value
            const string expected = "0001-01-01T00:00:00Z";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HandleColonInFormatString()
        {
            ParameterValueCollection pvc = new ParameterValueCollection
                {{ "Snapshot", new DateTime(1970, 01, 01, 18, 30, 10) }};
            ParameterEncoder encoder = new ParameterEncoder
            {
                Parameters = pvc,
                SourceString = "{Snapshot:HH:mm}"
            };
            string actual = encoder.Encoded();
            const string expected = "18:30";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ShouldRemoveParametersWithoutValue()
        {
            ParameterValueCollection pvc = new ParameterValueCollection
                { new ParameterValue { Name = "Snapshot" } };
            ParameterEncoder encoder = new ParameterEncoder
            {
                SourceString = "{Snapshot}",
                Parameters = pvc
            };
            string actual = encoder.Encoded();
            const string expected = "";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ShouldHandleCurlyBracketsWhenNoParametersSupplied()
        {
            const string query = "http://{var_a}/foo/{var_b}";
            var collection = new ParameterValueCollection();
            collection.Add("var_b", 0);
            ParameterEncoder encoder = new ParameterEncoder
            {
                SourceString = query,
                Parameters = collection
            };
            string actual = encoder.Encoded();
            Assert.Equal("http://{var_a}/foo/0", actual);
        }

        [Fact] // For vision project [12312]
        public void ShouldHandleMissingClosingCurlyBracket()
        {
            const string query = "{date:dd/MM/yyyy HH:mm";
            var collection = new ParameterValueCollection();
            collection.Add("date", new DateTime(1970, 01, 01, 18, 30, 10));
            ParameterEncoder encoder = new ParameterEncoder
            {
                SourceString = query,
                Parameters = collection
            };
            string actual = encoder.Encoded();
            Assert.Equal(query, actual);
        }

        [Fact]
        public void ShouldBeCaseSensitive()
        {
            const string source = "{model}_{MODEL}";
            ParameterValueCollection collection = new ParameterValueCollection();
            collection.Add("model", "lowercase_model");
            collection.Add("MODEL", "uppercase_model");
            ParameterEncoder encoder = new ParameterEncoder
            {
                SourceString = source,
                Parameters = collection
            };
            string actual = encoder.Encoded();
            Assert.Equal("lowercase_model_uppercase_model", actual);
        }
    }
}
