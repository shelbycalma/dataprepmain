﻿
using Datawatch.DataPrep.Data.Framework.Parameters;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    public class ParameterValueCollectionTest
    {
        [Fact]
        public void ShouldNotEqualDifferentNames()
        {
            ParameterValueCollection a = new ParameterValueCollection();
            a.Add("a_key", "a_value");
            ParameterValueCollection b = new ParameterValueCollection();
            b.Add("b_different_key", "a_value");
            Assert.False(a.Equals(b));
        }

        [Fact]
        public void ShouldNotEqualDifferentValues()
        {
            ParameterValueCollection a = new ParameterValueCollection();
            a.Add("a_key", "a_value");
            ParameterValueCollection b = new ParameterValueCollection();
            b.Add("a_key", "b_different_value");
            Assert.False(a.Equals(b));
        }

        [Fact]
        public void ShouldNotAllowNullKeys1()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc.Add(null, "foo1");
            Assert.Null(pvc[null]);
            Assert.Equal(0, pvc.Count);
        }

        [Fact]
        public void ShouldNotAllowNullKeys2()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc[null] = new ParameterValue
            {
                Name = null,
                TypedValue = new StringParameterValue { Value = "foo" }
            };
            Assert.Null(pvc[null]);
            Assert.Equal(0, pvc.Count);
        }

        [Fact]
        public void ShouldNotAllowEmptyKeys1()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc.Add("", "foo");
            Assert.Null(pvc[""]);
            Assert.Equal(0, pvc.Count);
        }

        [Fact]
        public void ShouldNotAllowEmptyKeys2()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc[""] = new ParameterValue
            {
                Name = "",
                TypedValue = new StringParameterValue { Value = "foo" }
            };
            Assert.Null(pvc[""]);
            Assert.Equal(0, pvc.Count);
        }

        [Fact]
        public void ShouldNotAllowDuplicateKeys1()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc.Add("a", "foo1");
            pvc.Add("a", "foo2");
            Assert.Equal(1, pvc.Count);
            Assert.Equal("foo2",
                ((StringParameterValue)pvc["a"].TypedValue).Value);
        }

        [Fact]
        public void ShouldNotAllowDuplicateKeys2()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc["a"] = new ParameterValue
            {
                TypedValue = new StringParameterValue { Value = "foo1" }
            };
            pvc["a"] = new ParameterValue
            {
                TypedValue = new StringParameterValue { Value = "foo2" }
            };
            Assert.Equal(1, pvc.Count);
            Assert.Equal("foo2",
                ((StringParameterValue)pvc["a"].TypedValue).Value);
        }

        [Fact]
        public void ShouldHandleParameterValueRenamed()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            ParameterValue parameterValue = new ParameterValue
            {
                Name = "a",
                TypedValue = new StringParameterValue {Value = "foo"}
            };
            pvc.Add(parameterValue);
            parameterValue.Name = "b";
            Assert.Equal(1, pvc.Count);
            Assert.Equal("foo",
                ((StringParameterValue)pvc["b"].TypedValue).Value);
            Assert.Null(pvc["a"]);
        }

        [Fact]
        public void ShouldRenameParameterValueInBracketOperator()
        {
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc["foo"] = new ParameterValue
            {
                Name = "bar",
                TypedValue = new StringParameterValue { Value = "value" }
            };
            Assert.Null(pvc["bar"]);
            Assert.Equal("value",
                ((StringParameterValue)pvc["foo"].TypedValue).Value);
        }

        [Fact]
        public void TestHashCode1()
        {
            ParameterValueCollection pvc1 = new ParameterValueCollection();
            pvc1.Add("key", "value");
            ParameterValueCollection pvc2 = new ParameterValueCollection();
            pvc2.Add("key", "value");
            Assert.Equal(pvc1, pvc2);
            Assert.Equal(pvc1.GetHashCode(), pvc2.GetHashCode());
        }

        [Fact]
        public void TestHashCode2()
        {
            ParameterValueCollection pvc1 = new ParameterValueCollection();
            pvc1.Add("key", "value");
            ParameterValueCollection pvc2 = new ParameterValueCollection();
            pvc2["key"] = new ParameterValue
            {
                Name = "key",
                TypedValue = new StringParameterValue {Value = "value"}
            };
            Assert.Equal(pvc1, pvc2);
            Assert.Equal(pvc1.GetHashCode(), pvc2.GetHashCode());
        }
    }
}
