﻿using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Datawatch.DataPrep.Data.Core.Licensing;
using Xunit;

namespace Panopticon.Developer.Licensing
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class TestClass_Valid
    {
    }

    // This is used by TestClass_AlsoValid to check an edge case.
    public class TestClass_ConfusingBaseType
    {
    }

    // This class is interesting since if you pass a type but no instance
    // to LicenseManager, and that type has a non-trivial base class (not
    // object or null), it will check all types in the inheritance chain,
    // and will never return a license in that case.
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class TestClass_AlsoValid : TestClass_ConfusingBaseType
    {
    }

    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class TestClass_Expired
    {
    }

    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class TestClass_NoLicense
    {
    }

    public class LicenseCacheTest : IDisposable
    {
        public LicenseCacheTest()
        {
            Datawatch.DataPrep.Data.Setup.Init();

            // Hard-coded namespace, since this test will fail of you change
            // the namespace anyway (it's in the license file too).
            string resource = "Datawatch.DataPrep.Data.Core.Licensing.TestLicense.xml";
            Assembly assembly = GetType().Assembly;
            string license;
            using (StreamReader reader = new StreamReader(
                assembly.GetManifestResourceStream(resource)))
            {
                license = reader.ReadToEnd();
            }
            Assert.False(string.IsNullOrEmpty(license));
            DatawatchXmlLicenseProvider.LicenseFileContents = license;

            // Toggle cache flag to force provider to flush out any cached
            // license document. The core unit tests have a fixture setup
            // method that enables license caching, so the "standard" license
            // file will almost certainly be cached at this point.
            DatawatchXmlLicenseProvider.CacheLicense = false;
            DatawatchXmlLicenseProvider.CacheLicense = true;
        }


        #region Excercise LicenseCache API with TestClass_Valid

        [Fact]
        public void ValidClass_NoInstance_IsValid()
        {
            Assert.True(LicenseCache.IsValid(
                typeof(TestClass_Valid), null));
        }

        [Fact]
        public void ValidClass_WithInstance_IsValid()
        {
            TestClass_Valid instance = new TestClass_Valid();
            Assert.True(LicenseCache.IsValid(
                typeof(TestClass_Valid), instance));
        }

        [Fact]
        public void ValidClass_NoInstance_Validates()
        {
            LicenseCache.Validate(
                typeof(TestClass_Valid), null);
        }

        [Fact]
        public void ValidClass_WithInstance_Validates()
        {
            TestClass_Valid instance = new TestClass_Valid();
            LicenseCache.Validate(
                typeof(TestClass_Valid), instance);
        }

        [Fact]
        public void ValidClass_NoInstance_ReturnsLicense()
        {
            Assert.NotNull(LicenseCache.GetLicense(
                typeof(TestClass_Valid), null));
        }

        [Fact]
        public void ValidClass_WithInstance_ReturnsLicense()
        {
            TestClass_Valid instance = new TestClass_Valid();
            Assert.NotNull(LicenseCache.GetLicense(
                typeof(TestClass_Valid), instance));
        }

        #endregion


        #region Excercise LicenseCache API with TestClass_AlsoValid

        [Fact]
        public void ComplexClass_NoInstance_IsValid()
        {
            Assert.True(LicenseCache.IsValid(
                typeof(TestClass_AlsoValid), null));
        }

        [Fact]
        public void ComplexClass_WithInstance_IsValid()
        {
            TestClass_AlsoValid instance = new TestClass_AlsoValid();
            Assert.True(LicenseCache.IsValid(
                typeof(TestClass_AlsoValid), instance));
        }

        [Fact]
        public void ComplexClass_NoInstance_Validates()
        {
            LicenseCache.Validate(
                typeof(TestClass_AlsoValid), null);
        }

        [Fact]
        public void ComplexClass_WithInstance_Validates()
        {
            TestClass_AlsoValid instance = new TestClass_AlsoValid();
            LicenseCache.Validate(
                typeof(TestClass_AlsoValid), instance);
        }

        [Fact]
        public void ComplexClass_NoInstance_GetLicenseNotAllowed()
        {
            var exc = Assert.Throws<InvalidOperationException>(() =>
            Assert.NotNull(LicenseCache.GetLicense(
                typeof(TestClass_AlsoValid), null)));
            Assert.Equal(
            "Cannot retrieve the license for TestClass_AlsoValid without " +
            "providing an instance (it has non-trivial base types).",
            exc.Message);
        }

        [Fact]
        public void ComplexClass_WithInstance_ReturnsLicense()
        {
            TestClass_AlsoValid instance = new TestClass_AlsoValid();
            Assert.NotNull(LicenseCache.GetLicense(
                typeof(TestClass_AlsoValid), instance));
        }

        #endregion


        #region Excercise LicenseCache API with TestClass_Expired

        [Fact]
        public void ExpiredClass_NoInstance_IsNotValid()
        {
            Assert.False(LicenseCache.IsValid(
                typeof(TestClass_Expired), null));
        }

        [Fact]
        public void ExpiredClass_WithInstance_IsNotValid()
        {
            TestClass_Expired instance = new TestClass_Expired();
            Assert.False(LicenseCache.IsValid(
                typeof(TestClass_Expired), instance));
        }

        [Fact]
        public void ExpiredClass_NoInstance_DoesNotValidate()
        {
            var exc = Assert.Throws<LicenseExpiredException>(() =>
                LicenseCache.Validate(typeof(TestClass_Expired), null));
            Assert.Equal("The license for this type has expired: " +
                "Panopticon.Developer.Licensing.TestClass_Expired, " +
                "Core Test, Version=1.0.0.0, Culture=neutral, " +
                "PublicKeyToken=null",
                exc.Message);
        }

        [Fact]
        public void ExpiredClass_WithInstance_DoesNotValidate()
        {
            TestClass_Expired instance = new TestClass_Expired();
            var exc = Assert.Throws<LicenseExpiredException>(
                () => LicenseCache.Validate(
                    typeof(TestClass_Expired), instance));

            Assert.Equal("The license for this type has expired: " +
            "Panopticon.Developer.Licensing.TestClass_Expired, " +
            "Core Test, Version=1.0.0.0, Culture=neutral, " +
            "PublicKeyToken=null",
            exc.Message);
        }

        [Fact]
        public void ExpiredClass_NoInstance_ReturnsNullLicense()
        {
            Assert.Null(LicenseCache.GetLicense(
                typeof(TestClass_Expired), null));
        }

        [Fact]
        public void ExpiredClass_WithInstance_ReturnsNullLicense()
        {
            TestClass_Expired instance = new TestClass_Expired();
            Assert.Null(LicenseCache.GetLicense(
                typeof(TestClass_Expired), instance));
        }

        #endregion


        #region Excercise LicenseCache API with TestClass_NoLicense

        [Fact]
        public void NoLicenseClass_NoInstance_IsNotValid()
        {
            Assert.False(LicenseCache.IsValid(
                typeof(TestClass_NoLicense), null));
        }

        [Fact]
        public void NoLicenseClass_WithInstance_IsNotValid()
        {
            TestClass_NoLicense instance = new TestClass_NoLicense();
            Assert.False(LicenseCache.IsValid(
                typeof(TestClass_NoLicense), instance));
        }

        [Fact]
        public void NoLicenseClass_NoInstance_DoesNotValidate()
        {
            var exc = Assert.Throws<LicenseMissingException>(() =>
                LicenseCache.Validate(typeof(TestClass_NoLicense), null));
            Assert.Equal(
                "The type \"Panopticon.Developer.Licensing.TestClass_NoLicense, " +
                "Core Test, Version=1.0.0.0, Culture=neutral, " +
                "PublicKeyToken=null\" for product \"Panopticon Developer .NET\" " +
                "in the license file was not signed with the correct key.",
                exc.Message);
        }

        [Fact]
        public void NoLicenseClass_WithInstance_DoesNotValidate()
        {
            TestClass_NoLicense instance = new TestClass_NoLicense();
            var exc = Assert.Throws<LicenseMissingException>(() =>
                LicenseCache.Validate(typeof(TestClass_NoLicense), instance));
            Assert.Equal(
                "The type \"Panopticon.Developer.Licensing.TestClass_NoLicense, " +
                "Core Test, Version=1.0.0.0, Culture=neutral, " +
                "PublicKeyToken=null\" for product \"Panopticon Developer .NET\" " +
                "in the license file was not signed with the correct key.",
                exc.Message);
        }

        [Fact]
        public void NoLicenseClass_NoInstance_ReturnsNullLicense()
        {
            Assert.Null(LicenseCache.GetLicense(
                typeof(TestClass_NoLicense), null));
        }

        [Fact]
        public void NoLicenseClass_WithInstance_ReturnsNullLicense()
        {
            TestClass_NoLicense instance = new TestClass_NoLicense();
            Assert.Null(LicenseCache.GetLicense(
                typeof(TestClass_NoLicense), instance));
        }

        #endregion

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Clear out our test license so we don't mess up other tests.
                    DatawatchXmlLicenseProvider.LicenseFileContents = null;

                    // Toggle cache flag to force provider to flush out the cached
                    // test license document. If we don't we will cause other tests
                    // to fail as they expect an "allow all" license, which will be
                    // loaded and cached the next time it's needed.
                    DatawatchXmlLicenseProvider.CacheLicense = false;
                    DatawatchXmlLicenseProvider.CacheLicense = true;

                    // And zap the contents of the license cache.
                    LicenseCache.Clear();
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }


        #endregion
    }
}
