﻿using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Query
{
    public class QueryBuilderTest
    {
        [Fact]
        public void PreviewAllColumnQuery()
        {
            string expectedQuery = "SELECT TOP 10 * FROM \"dbo\".\"DimCustomer\"  ";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            OnDemandHelper.BuildOnDemandPreviewQuery(queryBuilder, null, 10);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void PreviewSelectedColumnQuery()
        {
            string expectedQuery = "SELECT TOP 10 \"Col1\",\"Col2\" FROM \"dbo\".\"DimCustomer\"  ";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            OnDemandHelper.BuildOnDemandPreviewQuery(queryBuilder,
                new string[] { "Col1", "Col2" }, 10);

            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);

            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void SchemaQuery()
        {
            string expectedQuery = "SELECT * FROM \"dbo\".\"DimCustomer\"  WHERE 1 = 0 ";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, null);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void BreakdownQuery()
        {
            string expectedQuery = "SELECT \"CustomerAlternateKey\",\"FirstName\""
                +",\"LastName\" FROM \"dbo\".\"DimCustomer\"   "
                +"GROUP BY \"CustomerAlternateKey\",\"FirstName\",\"LastName\"";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable = new SchemaAndTable("dbo", "DimCustomer");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] breakdownColumns = { "CustomerAlternateKey" };
            onDemandParameters.BreakdownColumns = breakdownColumns;
            string[] auxiliaryColumns = { "FirstName", "LastName" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void TextDomainQuery()
        {
            string expectedQuery = "SELECT DISTINCT \"Gender\""
                +" FROM \"dbo\".\"DimCustomer\"  ";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            queryBuilder.Select.AddColumn("Gender");
            queryBuilder.Select.IsDistinct = true;
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void NumericDomainQuery()
        {
            string expectedQuery = "SELECT MIN(\"dbo\".\"DimCustomer\".\"YearlyIncome\")"
                +" AS \"mn\" ,MAX(\"dbo\".\"DimCustomer\".\"YearlyIncome\")"
                +" AS \"mx\"  FROM \"dbo\".\"DimCustomer\"  ";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            queryBuilder.Select.AddAggregate(
                new[] { "YearlyIncome" }, FunctionType.Min, "mn");
            queryBuilder.Select.AddAggregate(
                new[] { "YearlyIncome" }, FunctionType.Max, "mx");
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void AggrigateQuery()
        {
            string expectedQuery = "SELECT \"CustomerAlternateKey\","
                +"SUM(\"dbo\".\"DimCustomer\".\"YearlyIncome\") AS \"a1\" ,"
                +"\"FirstName\",\"LastName\" FROM \"dbo\".\"DimCustomer\"   "
                +"GROUP BY \"CustomerAlternateKey\",\"FirstName\",\"LastName\"";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] breakdownColumns = { "CustomerAlternateKey" };
            onDemandParameters.BreakdownColumns = breakdownColumns;
            string[] auxiliaryColumns = { "FirstName", "LastName" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            OnDemandAggregate[] aggregates = new OnDemandAggregate[1];
            aggregates[0] = new OnDemandAggregate();
            aggregates[0].Alias = "a1";
            string[] aggColumns = { "YearlyIncome" };
            aggregates[0].Columns = aggColumns;
            aggregates[0].FunctionType = FunctionType.Sum;
            onDemandParameters.Aggregates = aggregates;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void PredicateQuery()
        {
            string expectedQuery = "SELECT \"CustomerAlternateKey\",\"FirstName\""
                +",\"LastName\" FROM \"dbo\".\"DimCustomer\"  WHERE \"Gender\" IN ('F')"
                +"  GROUP BY \"CustomerAlternateKey\",\"FirstName\",\"LastName\"";
            QueryBuilder queryBuilder = new QueryBuilder();
            queryBuilder.SchemaAndTable =
                new SchemaAndTable("dbo", "DimCustomer");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] breakdownColumns = { "CustomerAlternateKey" };
            onDemandParameters.BreakdownColumns = breakdownColumns;
            string[] auxiliaryColumns = { "FirstName", "LastName" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            string[] filters = { "F" };
            Predicate predicate = new Comparison(Operator.In,
                                new ColumnParameter("Gender"),
                                new ListParameter(filters));

            queryBuilder.Where.Predicate = predicate;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.SQLServer);
            Assert.Equal(expectedQuery, actualQuery);
        }
    }
}