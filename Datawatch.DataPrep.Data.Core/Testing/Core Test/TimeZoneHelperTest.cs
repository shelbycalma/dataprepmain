﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core
{
    public class TimeZoneHelperTest
    {
        [Fact]
        public void ConstructorTest()
        {
            TimeZoneHelper helper = new TimeZoneHelper(new PropertyBag());
        }

        [Fact]
        public void ConstuctorNullBagTest()
        {
            Assert.Throws<ArgumentNullException>(
                () => new TimeZoneHelper(null));
        }

        [Fact]
        public void SelectedTimeZoneTest()
        {
            TimeZoneHelper helper = new TimeZoneHelper(new PropertyBag());
            Assert.Null(helper.SelectedTimeZone);

            helper.SelectedTimeZone = "";
            Assert.Equal("", helper.SelectedTimeZone);
            Assert.False(helper.TimeZoneSelected);

            helper.SelectedTimeZone = null;
            Assert.Null(helper.SelectedTimeZone);
            Assert.False(helper.TimeZoneSelected);

            helper.SelectedTimeZone = "Central European Standard Time";
            Assert.Equal("Central European Standard Time",
                helper.SelectedTimeZone);
            Assert.True(helper.TimeZoneSelected);
        }

        [Fact]
        public void Equals_Test()
        {
            TimeZoneHelper a = new TimeZoneHelper(new PropertyBag());
            a.SelectedTimeZone = "Central European Standard Time";

            TimeZoneHelper b = new TimeZoneHelper(new PropertyBag());
            b.SelectedTimeZone = "Central European Standard Time";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.SelectedTimeZone = "GMT Standard Time";
            Assert.NotEqual(a, b);
        }
    }
}
