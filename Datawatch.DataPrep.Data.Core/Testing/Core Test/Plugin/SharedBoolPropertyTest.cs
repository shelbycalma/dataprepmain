﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedBoolPropertyTest
    {
        [Fact]
        public void DefaultValueTest()
        {
            PropertyBag bag = new PropertyBag();
            SharedBoolProperty boolProperty = new SharedBoolProperty("somepath");
            bool actual = boolProperty.GetValue(bag);
            Assert.False(actual);
        }
    
        [Fact]
        public void RoundtripTest() {
            PropertyBag bag = new PropertyBag();
            SharedBoolProperty boolProperty = new SharedBoolProperty("somepath");
            boolProperty.SetValue(bag, true);
            bool actual = boolProperty.GetValue(bag);
            Assert.True(actual);
        }
    }
}
