﻿using System;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    // TODO: Add more DataTable sprecific tests.
    public class AdoTableTest : IDisposable
    {
        private DataTable dataTable;
        private AdoTable table;
        private TableSink sink;

        public AdoTableTest()
        {
            dataTable = new DataTable();
            table = new AdoTable(dataTable);
            sink = new TableSink();
            sink.Table = table;
            sink.Enabled = false;
        }

        [Fact]
        public void RowCount()
        {
            Assert.Equal(0, table.RowCount);
            for (int i = 0; i < 42; i++) {
                dataTable.Rows.Add();
            }
            Assert.Equal(42, table.RowCount);
            for (int i = 0; i < 17; i++) {
                dataTable.Rows.Remove(dataTable.Rows[0]);
            }
            Assert.Equal(42 - 17, table.RowCount);
        }

        [Fact]
        public void ColumnCount()
        {
            Assert.Equal(0, table.ColumnCount);
            for (int i = 0; i < 42; i++) {
                dataTable.Columns.Add("C" + i, typeof(double));
            }
            Assert.Equal(42, table.ColumnCount);
            for (int i = 0; i < 17; i++) {
                dataTable.Columns.Remove(dataTable.Columns[0]);
            }
            Assert.Equal(42 - 17, table.ColumnCount);
        }

        [Fact]
        // OPTIMIZE: Optimize so no events if value stays the same?
        public void RealtimeEvents_NumericColumn()
        {
            DataRow dataRow = dataTable.Rows.Add();
            AdoRow row = (AdoRow)table.GetRow(0);

            DataColumn dataColumn =
                dataTable.Columns.Add("Column", typeof(double));
            AdoNumericColumn adoColumn = (AdoNumericColumn)table.GetColumn(0);

            double value = adoColumn.GetNumericValue(row);
            Assert.True(NumericValue.IsEmpty(value),
                "Numeric cell was not initialized to Empty.");

            sink.Enabled = true;

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataRow[dataColumn] = 1.0;
            sink.Done("SetNumericValue should only fire Changed.");

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataRow[dataColumn] = 1.0;
            sink.Done("SetNumericValue should fire even if value is the same.");
        }

        [Fact]
        // OPTIMIZE: Optimize so no events if value stays the same?
        public void RealtimeEvents_TextColumn()
        {
            DataRow dataRow = dataTable.Rows.Add();
            AdoRow row = (AdoRow)table.GetRow(0);

            DataColumn dataColumn =
                dataTable.Columns.Add("Column", typeof(string));
            AdoTextColumn adoColumn = (AdoTextColumn)table.GetColumn(0);

            string value = adoColumn.GetTextValue(row);
            Assert.True(value == null,
                "Text cell was not initialized to null.");

            sink.Enabled = true;

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataRow[dataColumn] = "A";
            sink.Done("SetTextValue should only fire Changed.");

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataRow[dataColumn] = "A";
            sink.Done("SetTextValue should fire even if value is the same.");
        }

        [Fact]
        // OPTIMIZE: Optimize so no events if value stays the same?
        public void RealtimeEvents_TimeColumn()
        {
            DataRow dataRow = dataTable.Rows.Add();
            AdoRow row = (AdoRow)table.GetRow(0);

            DataColumn dataColumn =
                dataTable.Columns.Add("Column", typeof(DateTime));
            AdoTimeColumn adoColumn = (AdoTimeColumn)table.GetColumn(0);

            DateTime value = adoColumn.GetTimeValue(row);
            Assert.True(TimeValue.IsEmpty(value),
                "Time cell was not initialized to Empty.");

            sink.Enabled = true;

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            value = new DateTime(1973, 7, 23);
            dataRow[dataColumn] = value;
            sink.Done("SetTimeValue should only fire Changed.");

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataRow[dataColumn] = value;
            sink.Done("SetTimeValue should fire even if value is the same.");
        }

        [Fact]
        public void EventSequence_AddRow()
        {
            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    Assert.True(0 == table.RowCount,
                        "Row should not have been added yet.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(1, table.RowCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Rows.Add();
        }

        [Fact]
        public void EventSequence_RemoveRow()
        {
            dataTable.Columns.Add("Column", typeof(double));
            dataTable.Rows.Add(17.0);

            AdoNumericColumn column = (AdoNumericColumn) table.GetColumn(0);

            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    Assert.True(1 == table.RowCount,
                        "Row should not have been removed yet.");
                    Assert.True(
                        Math.Abs(17.0 - column.GetNumericValue(table.GetRow(0))) < 1e-5,
                        "Row should still have its value.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(0, table.RowCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Rows.RemoveAt(0);
        }

        [Fact]
        // OPTIMIZE: Can we send Changing before the columns are removed?
        public void EventSequence_ClearRows()
        {
            dataTable.Columns.Add("Column", typeof(double));
            dataTable.Rows.Add(17.0);

            AdoNumericColumn column = (AdoNumericColumn) table.GetColumn(0);

            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    //Assert.Equal(1, table.RowCount,
                    //    "Row should not have been removed yet.");
                    //Assert.Equal(17.0,
                    //    column.GetNumericValue(table.GetRow(0)),
                    //    "Row should still have its value.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(0, table.RowCount);
                    Assert.Equal(1, table.ColumnCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Rows.Clear();
        }

        [Fact]
        // OPTIMIZE: Can we send Changing before the column is added?
        public void EventSequence_AddColumn()
        {
            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    //Assert.Equal(0, table.ColumnCount,
                    //    "Column should not have been added yet.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(1, table.ColumnCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Columns.Add("Column", typeof(double));
        }

        [Fact]
        // OPTIMIZE: Can we send Changing before the column is removed?
        public void EventSequence_RemoveColumn()
        {
            dataTable.Columns.Add("Column", typeof(double));
            dataTable.Rows.Add(17.0);

            AdoNumericColumn column = (AdoNumericColumn) table.GetColumn(0);

            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    //Assert.Equal(1, table.ColumnCount,
                    //    "Column should not have been removed yet.");
                    //Assert.Equal(17.0,
                    //    column.GetNumericValue(table.GetRow(0)),
                    //    "Should still be able to read column values.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(0, table.ColumnCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Columns.RemoveAt(0);
        }

        [Fact]
        // OPTIMIZE: Can we send Changing before the columns are removed?
        public void EventSequence_ClearColumns()
        {
            dataTable.Columns.Add("Column", typeof(double));
            dataTable.Rows.Add(17.0);

            AdoNumericColumn column = (AdoNumericColumn) table.GetColumn(0);

            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    //Assert.Equal(1, table.ColumnCount,
                    //    "Column should not have been removed yet.");
                    //Assert.Equal(17.0,
                    //    column.GetNumericValue(table.GetRow(0)),
                    //    "Should still be able to read column values.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(0, table.ColumnCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Columns.Clear();
        }

        [Fact]
        // OPTIMIZE: Can we send Changing before the rows are removed?
        public void EventSequence_ClearTable()
        {
            dataTable.Columns.Add("Column", typeof(double));
            dataTable.Rows.Add(17.0);

            AdoNumericColumn column = (AdoNumericColumn) table.GetColumn(0);

            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    //Assert.Equal(1, table.ColumnCount,
                    //    "Column should not have been removed yet.");
                    //Assert.Equal(1, table.RowCount,
                    //    "Row should not have been removed yet.");
                    //Assert.Equal(17.0,
                    //    column.GetNumericValue(table.GetRow(0)),
                    //    "Should still be able to read column values.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(1, table.ColumnCount);
                    Assert.Equal(0, table.RowCount);
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Clear();
        }

        [Fact]
        public void EventSequence_SetValue()
        {
            dataTable.Columns.Add("Column", typeof(double));
            dataTable.Rows.Add(17.0);

            AdoNumericColumn column = (AdoNumericColumn) table.GetColumn(0);

            sink.Enabled = true;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    Assert.True(
                        Math.Abs(17.0 - column.GetNumericValue(table.GetRow(0))) < 1e-5,
                        "Row should still have its old value.");
                };
            table.Changed +=
                delegate(object sender, TableChangedEventArgs e) {
                    Assert.Equal(47.0,
                        column.GetNumericValue(table.GetRow(0)));
                };

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            dataTable.Rows[0]["Column"] = 47.0;
        }

        public void Dispose()
        {
            sink.Done();
            sink.Table = null;
            sink = null;
            if (table != null)
            {
                Assert.False(table.IsUpdating,
                    "Table left in batch update mode.");
            }
            table = null;
        }
    }
}
