﻿using System;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    public class AdoTableBatchingTest : IDisposable
    {
        private DataTable dataTable;
        private AdoTable table;
        private TableSink sink;

        public AdoTableBatchingTest()
        {
            dataTable = new DataTable();
            table = new AdoTable(dataTable);
            sink = new TableSink();
            sink.Table = table;
        }

        [Fact]
        public void NestedBeginEndUpdate()
        {
            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);

            table.BeginUpdate();
            table.BeginUpdate();
            table.EndUpdate();
            table.EndUpdate();

            sink.Done();
        }

        [Fact]
        public void UnbalancedBeginEndUpdate()
        {
            Assert.Throws<InvalidOperationException>(() => table.EndUpdate());
        }

        [Fact]
        public void NoEventsOutsideChangingChanged()
        {
            sink.Expect(TableEventType.Changing);
            table.BeginUpdate();
            sink.Done();

            dataTable.Rows.Add();
            sink.Done();
            
            sink.Expect(TableEventType.Changed);
            table.EndUpdate();
        }

        [Fact]
        public void NoLeftoverUnbatchedEvents()
        {
            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataTable.Rows.Add();
            sink.Done();

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            table.BeginUpdate();
            table.EndUpdate();
        }

        [Fact]
        public void RemoveAndAddColumnInBatch()
        {
            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            DataColumn column = dataTable.Columns.Add();

            // No batching.
            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataTable.Columns.Remove(column);

            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            dataTable.Columns.Add(column);
            sink.Done();

            // NoEvents batching.
            sink.Expect(TableEventType.Changing);
            sink.Expect(TableEventType.Changed);
            table.BeginUpdate();
            dataTable.Columns.Remove(column);
            dataTable.Columns.Add(column);
            table.EndUpdate();
            sink.Done();
        }

        [Fact]
        public void UpdatingTrueInChangingEvent()
        {
            sink.Enabled = false;

            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    Assert.True(table.IsUpdating);
                };

            table.BeginUpdate();
            table.EndUpdate();
        }

        [Fact]
        public void UpdatingTrueInChangedEvent()
        {
            sink.Enabled = false;

            table.Changed += delegate(object sender, TableChangedEventArgs e) {
                Assert.True(table.IsUpdating);
            };

            table.BeginUpdate();
            table.EndUpdate();
        }

        public void Dispose()
        {
            sink.Done();
            sink.Table = null;
            sink = null;
            if (table != null)
            {
                Assert.False(table.IsUpdating,
                    "Table left in batch update mode.");
            }
            table = null;
        }

        // The following test probably indicates "good" behavior?
        //[Fact]
        //public void DontLeaveUpdatingWhenChangingFails()
        //{
        //    sink.Enabled = false;

        //    table.Changing += delegate(object sender, EventArgs e) {
        //        throw new Exception();
        //    };

        //    try {
        //        table.BeginUpdate();
        //        Assert.Fail("Should not be caught by table.");
        //    } catch (Exception) { }

        //    Assert.False(table.IsUpdating,
        //        "Should not have been put in updating state.");
        //}

        // The following test probably indicates "good" behavior?
        //[Fact]
        //public void DontLeaveUpdatingWhenChangedFails()
        //{
        //    sink.Enabled = false;

        //    table.Changed += delegate(object sender, BatchingTableEventArgs e) {
        //        throw new Exception();
        //    };

        //    try {
        //        table.BeginUpdate();
        //    } catch (Exception) {
        //        Assert.Fail("Should not have caused exception.");
        //    }

        //    try {
        //        table.EndUpdate();
        //        Assert.Fail("Should not be caught by table.");
        //    } catch (Exception) { }

        //    Assert.False(table.IsUpdating,
        //        "Should not have been put in updating state.");
        //}
    }
}
