﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    internal enum TableEventType
    {
        Changing,
        Changed
    }

    internal class RealtimeEvent
    {
        public readonly TableEventType Type;
        public readonly EventArgs Args;

        public RealtimeEvent(TableEventType type, EventArgs args)
        {
            this.Type = type;
            this.Args = args;
        }

        public override string ToString()
        {
            string s = "<" + this.Type.ToString();
            if (this.Args != null) {
                s += " Args=" + this.Args.ToString();
            }
            s += ">";
            return s;
        }
    }

    /// <summary>
    /// Used to validate that a ITable fires the expected events in the
    /// expected order. Call one of the Expect overloads to add an event to
    /// the expected sequence. They will be popped off the queue as the events
    /// are fired from the table, and the fired events are validated. Call
    /// Done to validate that the queue is empty. Call Reset to clear the
    /// queue. Set Enabled to false to disable validation (the queue will not
    /// be touched).
    /// </summary>
    internal class TableSink
    {
        private ITable table;

        private readonly Queue<RealtimeEvent> expectedEvents;

        private bool enabled;

        public TableSink()
        {
            expectedEvents = new Queue<RealtimeEvent>();
            enabled = true;
        }

        private void Attach()
        {
            if (table != null) {
                table.Changing += table_Changing;
                table.Changed += table_Changed;
            }
        }

        private void CheckEvent(RealtimeEvent expected, RealtimeEvent actual)
        {
            Assert.True(expected.GetType() == actual.GetType(),
                string.Format(
                    "Bad event type (expected {0}, got {1})",
                    expected, actual));

            Assert.True(expected.Type == actual.Type,
                string.Format(
                    "Bad event type (expected {0}, got {1})",
                    expected, actual));

            CheckEventArgs(expected, actual);
        }

        private void CheckEventArgs(RealtimeEvent expected, RealtimeEvent actual)
        {
            if (expected == null) {
                return;
            }
            // TODO: Validate contents.
        }

        private void Detach()
        {
            if (table != null) {
                table.Changing -= table_Changing;
                table.Changed -= table_Changed;
            }
        }

        /// <summary>
        /// Validate that the queue of expected events is empty.
        /// </summary>
        public void Done()
        {
            Done(null);
        }

        /// <summary>
        /// Validate that the queue of expected events is empty.
        /// </summary>
        /// <param name="messageOnFail">Text that will be included in the
        /// fail message if the queue is not empty.</param>
        public void Done(string messageOnFail)
        {
            if (!enabled) {
                return;
            }
            if (expectedEvents.Count == 0) {
                return;
            }
            RealtimeEvent e = expectedEvents.Dequeue();
            int remaining = expectedEvents.Count;
            expectedEvents.Clear();
            string message = string.Format(
                "Event expected but not fired: {0} (and {1} more).",
                e, remaining);
            if (messageOnFail != null) {
                message = string.Format("{0} - {1}", messageOnFail, message);
            }
            Assert.True(false, message);
        }

        /// <summary>
        /// Enable/disable validation (on by default).
        /// </summary>
        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        /// <summary>
        /// Adds a table event to the sequence of expected events.
        /// </summary>
        public void Expect(TableEventType type, EventArgs args)
        {
            Expect(new RealtimeEvent(type, args));
        }

        /// <summary>
        /// Adds a table event to the sequence of expected events.
        /// </summary>
        public void Expect(TableEventType type)
        {
            Expect(type, null);
        }

        private void Expect(RealtimeEvent e)
        {
            if (!enabled) {
                Assert.True(false, 
                    "TableSink.Expect was called, but the sink " +
                    "is not enabled. Sure you're doing this right?");
            }

            expectedEvents.Enqueue(e);
        }

        private void MarkEvent(RealtimeEvent actualEvent)
        {
            if (!enabled) {
                return;
            }
            if (expectedEvents.Count == 0) {
                Assert.True(
                    false,
                    "No events expected but got " + actualEvent + ".");
            }
            RealtimeEvent expectedEvent = expectedEvents.Dequeue();
            CheckEvent(expectedEvent, actualEvent);
        }

        private void MarkTableEvent(TableEventType type, EventArgs args)
        {
            MarkEvent(new RealtimeEvent(type, args));
        }

        /// <summary>
        /// Removes all expected events from the queue.
        /// </summary>
        public void Reset()
        {
            if (!enabled) {
                Assert.True(false,
                    "TableSink.Expect was called, but the sink " +
                    "is not enabled. Sure you're doing this right?");
            }

            expectedEvents.Clear();
        }

        /// <summary>
        /// Table to listen to.
        /// </summary>
        public ITable Table
        {
            get { return table; }
            set {
                Detach();
                table = value;
                Attach();
            }
        }

        private void table_Changed(object sender, EventArgs e)
        {
            MarkTableEvent(TableEventType.Changed, e);
        }

        private void table_Changing(object sender, EventArgs e)
        {
            MarkTableEvent(TableEventType.Changing, e);
        }
    }
}
