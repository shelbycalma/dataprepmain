﻿using System;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class StandaloneTableTest : IDisposable
    {
        private StandaloneTable table;

        public StandaloneTableTest()
        {
            table = new StandaloneTable();
        }


        public void Dispose()
        {
            if (table != null)
            {
                Assert.False(table.IsUpdating,
                    "Table left in batch update mode.");
            }
            table = null;
        }

#if CHECKED // This only works with run-time checks.
        /// <summary>
        /// Can't get a row that doesn't exist.
        /// </summary>
        [Fact]
        public void GetRow_OutOfRange()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => table.GetRow(0));
        }
#endif

#if CHECKED // This only works with run-time checks.
        /// <summary>
        /// Can't get a row after it was removed.
        /// </summary>
        [Fact]
        public void GetRow_OutOfRange_AfterRemove()
        {
            table.BeginUpdate();
            Row row = table.AddRow();
            table.RemoveRow(row);
            table.EndUpdate();

            Assert.Throws<ArgumentOutOfRangeException>(
                () => table.GetRow(0));
        }
#endif

        /// <summary>
        /// Can't get a time that doesn't exist.
        /// </summary>
        [Fact]
        public void GetTime_OutOfRange()
        {
            Assert.Throws<ArgumentOutOfRangeException>(
                () => table.GetTime(0));
        }

        /// <summary>
        /// Can't get a time after it was removed.
        /// </summary>
        [Fact]
        public void GetTime_OutOfRange_AfterRemove()
        {
            table.BeginUpdate();
            Time time = table.AddTime(new DateTime(1973, 07, 23));
            table.RemoveTime(time);
            table.EndUpdate();

            Assert.Throws<ArgumentOutOfRangeException>(
                () => table.GetTime(0));
        }

#if CHECKED // This only works with run-time checks.
        /// <summary>
        /// Can't change a table without calling BeginUpdate."
        /// </summary>
        [Fact]
        public void MustCallBeginEndUpdate_AddRow()
        {
            var exc = Assert.Throws<InvalidOperationException>(
                () => table.AddRow());
            Assert.Equal(
                "Any changes to the table must be made " +
                "in a Begin/EndUpdate block.",
                exc.Message);
        }
#endif

        /// <summary>
        /// Every call to EndUpdate must match a BeginUpdate.
        /// </summary>
        [Fact]
        public void UnbalancedBeginEndUpdate()
        {
            var exc = Assert.Throws<InvalidOperationException>(
                () =>table.EndUpdate());
            Assert.Equal(
                "A call to EndUpdate did not have a " +
                "corresponding call to BeginUpdate.",
                exc.Message);
        }

        /// <summary>
        /// Make sure IsUpdating allows nested calls.
        /// </summary>
        [Fact]
        public void IsUpdatingWithNesting()
        {
            Assert.False(table.IsUpdating);
            table.BeginUpdate();
            try {
                Assert.True(table.IsUpdating);
                table.BeginUpdate();
                try {
                    Assert.True(table.IsUpdating);
                } finally {
                    table.EndUpdate();
                }
                Assert.True(table.IsUpdating);
            } finally {
                table.EndUpdate();
            }
            Assert.False(table.IsUpdating);
        }

        /// <summary>
        /// Changing event handlers should see IsUpdating == true.
        /// </summary>
        [Fact]
        public void IsUpdatingTrueInChangingEvent()
        {
            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    Assert.True(table.IsUpdating,
                        "The semantics says IsUpdating should be true here.");
                };

            table.BeginUpdate();
            table.EndUpdate();
        }

        /// <summary>
        /// Changed event handlers should see IsUpdating == true.
        /// </summary>
        [Fact]
        public void IsUpdatingTrueInChangedEvent()
        {
            table.Changed += delegate(object sender, TableChangedEventArgs e) {
                Assert.True(table.IsUpdating,
                    "The semantics says IsUpdating should be true here.");
            };

            table.BeginUpdate();
            table.AddRow();
            table.EndUpdate();
        }

        // NOTE: Does this depend on if someone else has already seen Changing,
        // would they expect IsUpdating and then Changed? This test is more
        // interesting than the Changed version as we recommend that clients
        // put EndUpdate in a finally block.
        // NOTE: This test should probably have multiple Changing handlers.
        /// <summary>
        /// If a Changing handler fails, the table not be left IsUpdating.
        /// </summary>
        [Fact(Skip = "It would be great if this test passed!")]
        public void DontLeaveUpdatingWhenChangingFails()
        {
            table.Changing +=
                delegate(object sender, TableChangingEventArgs e) {
                    throw new Exception();
                };

            try {
                table.BeginUpdate();
                Assert.True(false, "Should not be caught by table.");
            } catch (Exception) { }

            Assert.False(table.IsUpdating,
                "Should not have been put in updating state.");
        }

        // NOTE: This test should probably have multiple Changed handlers.
        /// <summary>
        /// If a Changed handler fails, the table not be left IsUpdating.
        /// </summary>
        [Fact]
        public void DontLeaveUpdatingWhenChangedFails()
        {
            table.Changed += delegate(object sender, TableChangedEventArgs e) {
                throw new Exception();
            };

            table.BeginUpdate();
            table.AddRow();

            try {
                table.EndUpdate();
                Assert.True(false, "Should not be caught by table.");
            } catch (Exception) { }

            Assert.False(table.IsUpdating,
                "Should not have been put in updating state.");
        }

#if CHECKED // This only works with run-time checks.
        /// <summary>
        /// Can't set a value without calling BeginUpdate.
        /// </summary>
        [Fact]
        public void MustCallBeginEndUpdate_SetValue()
        {
            NumericColumn column = new NumericColumn("Column");

            table.BeginUpdate();
            table.AddColumn(column);
            Row row = table.AddRow();
            table.EndUpdate();

            var exc = Assert.Throws<InvalidOperationException>(
                () => column.SetNumericValue(row, 1.0));
            Assert.Equal(
                "Any changes to the table must be made " +
                "in a Begin/EndUpdate block.",
                exc.Message);
        }
#endif

        /// <summary>
        /// Make sure the table can count its rows.
        /// </summary>
        [Fact]
        public void RowCount()
        {
            Assert.Equal(0, table.RowCount);
            table.BeginUpdate();
            for (int i = 0; i < 42; i++) {
                table.AddRow();
            }
            table.EndUpdate();
            Assert.Equal(42, table.RowCount);
            table.BeginUpdate();
            for (int i = 0; i < 17; i++) {
                table.RemoveRow(table.GetRow(i));
            }
            table.EndUpdate();
            Assert.Equal(42 - 17, table.RowCount);
        }

        /// <summary>
        /// Make sure the table can count its columns.
        /// </summary>
        [Fact]
        public void ColumnCount()
        {
            Assert.Equal(0, table.ColumnCount);
            table.BeginUpdate();
            for (int i = 0; i < 42; i++) {
                table.AddColumn(new NumericColumn("C" + i));
            }
            table.EndUpdate();
            Assert.Equal(42, table.ColumnCount);
            table.BeginUpdate();
            for (int i = 0; i < 17; i++) {
                table.RemoveColumn(table.GetColumn(0));
            }
            table.EndUpdate();
            Assert.Equal(42 - 17, table.ColumnCount);
        }

        /// <summary>
        /// Make sure the table can count its times.
        /// </summary>
        [Fact]
        public void TimeCount()
        {
            Assert.Equal(0, table.TimeCount);
            table.BeginUpdate();
            for (int i = 0; i < 42; i++) {
                table.AddTime(new DateTime(1973, 07, 23).AddDays(i));
            }
            table.EndUpdate();
            Assert.Equal(42, table.TimeCount);
            table.BeginUpdate();
            for (int i = 0; i < 17; i++) {
                table.RemoveTime(table.GetTime(0));
            }
            table.EndUpdate();
            Assert.Equal(42 - 17, table.TimeCount);
        }

        /// <summary>
        /// Nothing strange happens when the final row is removed.
        /// </summary>
        [Fact]
        public void AddRowsThenRemoveThemAll()
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            table.RowCapacity = 1;
            table.AddColumn(new NumericColumn("Numeric"));
            for (int i = 0; i < 50; i++) {
                table.AddRow(new object[] { (double) i });
            }
            for (int i = 0; i < 50; i++) {
                table.RemoveRow(table.GetRow(i));
            }

            table.EndUpdate();
        }

        /// <summary>
        /// Nothing strange happens when the final time is removed.
        /// </summary>
        [Fact]
        public void AddTimesThenRemoveThemAll()
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            table.RowCapacity = 1;
            table.TimeCapacity = 1;
            NumericTimeseriesColumn column =
                new NumericTimeseriesColumn("NumericTimeseries");
            table.AddColumn(column);
            Row row = table.AddRow();
            DateTime date = new DateTime(1973, 7, 23);
            for (int i = 0; i < 50; i++) {
                Time time = table.AddTime(date.AddDays(i));
                column.SetNumericValue(row, time, (double) i);
            }
            while (table.TimeCount > 0) {
                table.RemoveTime(table.GetTime(0));
            }

            table.EndUpdate();
        }

        /// <summary>
        /// Sorts the times if they are added out of order.
        /// </summary>
        [Fact]
        public void SortsTimes()
        {
            DateTime[] times = {
                new DateTime(2017, 01, 01),
                new DateTime(1973, 07, 23),
                new DateTime(2012, 01, 01)
            };

            table.BeginUpdate();
            for (int i = 0; i < times.Length; i++) {
                table.AddTime(times[i]);
            }
            table.EndUpdate();

            for (int i = 0; i < 2; i++) {
                Assert.True(
                    table.GetTime(i).DateTime < table.GetTime(i + 1).DateTime,
                    "Times should come out of the table in order.");
            }
        }

        /// <summary>
        /// Row should default to all n/a if added without values.
        /// </summary>
        [Fact]
        public void AddRow_NoValues()
        {
            table.BeginUpdate();

            TextColumn text = new TextColumn("Text");
            table.AddColumn(text);
            NumericColumn numeric = new NumericColumn("Numeric");
            table.AddColumn(numeric);
            TimeColumn time = new TimeColumn("Time");
            table.AddColumn(time);

            Row row = table.AddRow();

            table.EndUpdate();

            Assert.Null(text.GetTextValue(row));
            Assert.True(NumericValue.IsEmpty(numeric.GetNumericValue(row)));
            Assert.True(TimeValue.IsEmpty(time.GetTimeValue(row)));
        }

        /// <summary>
        /// Row should get all n/a if added with null values.
        /// </summary>
        [Fact]
        public void AddRow_NullValues()
        {
            table.BeginUpdate();

            TextColumn text = new TextColumn("Text");
            table.AddColumn(text);
            NumericColumn numeric = new NumericColumn("Numeric");
            table.AddColumn(numeric);
            TimeColumn time = new TimeColumn("Time");
            table.AddColumn(time);

            Row row = table.AddRow(new object[] { null, null, null });
            
            table.EndUpdate();


            Assert.Null(text.GetTextValue(row));
            Assert.True(
                NumericValue.IsEmpty(numeric.GetNumericValue(row)));
            Assert.True(TimeValue.IsEmpty(time.GetTimeValue(row)));
        }

        /// <summary>
        /// Row should get all n/a if added with boxed n/a:s.
        /// </summary>
        [Fact]
        public void AddRow_MissingValues()
        {
            table.BeginUpdate();

            TextColumn text = new TextColumn("Text");
            table.AddColumn(text);
            NumericColumn numeric = new NumericColumn("Numeric");
            table.AddColumn(numeric);
            TimeColumn time = new TimeColumn("Time");
            table.AddColumn(time);

            Row row = table.AddRow(new object[] {
                null, NumericValue.Empty, TimeValue.Empty
            });

            table.EndUpdate();

            Assert.Null(text.GetTextValue(row));
            Assert.True(
                NumericValue.IsEmpty(numeric.GetNumericValue(row)));
            Assert.True(TimeValue.IsEmpty(time.GetTimeValue(row)));
        }

        [Fact]
        public void TextColumn_MetaDomain_Sort()
        {
            table.BeginUpdate();

            TextColumn column = new TextColumn("Column");
            table.AddColumn(column);

            IMutableTextColumnMetaData metaData = 
                (IMutableTextColumnMetaData) column.MetaData;

            metaData.Domain = new string[] { "C", "A", "B" };

            table.EndUpdate();

            // Should not touch order.
            Assert.Equal(3, metaData.Domain.Length);
            Assert.Equal("C", metaData.Domain[0]);
            Assert.Equal("A", metaData.Domain[1]);
            Assert.Equal("B", metaData.Domain[2]);
        }

#if CHECKED // This only works with run-time checks.
        /// <summary>
        /// Can't set a column's value after it's been removed.
        /// </summary>
        [Fact]
        public void SetValueInRemovedColumn()
        {
            NumericColumn column = new NumericColumn("Numeric");

            table.BeginUpdate();
            table.AddColumn(column);
            Row row = table.AddRow();
            column.SetNumericValue(row, 1.0);
            table.EndUpdate();

            table.BeginUpdate();
            table.RemoveColumn(column);
            table.EndUpdate();

            table.BeginUpdate();
            try
            {
                var exc = Assert.Throws<ArgumentException>(
                    () => column.SetNumericValue(row, 2.0));
                Assert.Equal(
                    "The column 'Numeric' does not belong to the table.",
                    exc.Message);
            } finally {
                table.EndUpdate();
            }
        }
#endif

        /// <summary>
        /// Make sure that time series column rows can be cleared (when 
        /// they are reused for a new row) even when there are no times.
        /// </summary>
        [Fact]
        public void ClearTimeseriesRow()
        {
            // This test is to make sure that the StandaloneTable doesn't get
            // tricked - the same way the Aggregator was - by the non-zero
            // MinCapacity on the SmallStorage. This would result in a null
            // "first time slice" if the table had zero times.

            NumericTimeseriesColumn series = new NumericTimeseriesColumn("C");

            // Add the column, and a single row (no times).
            table.BeginUpdate();
            table.AddColumn(series);
            Row oldRow = table.AddRow();
            table.EndUpdate();

            // Remove the row and then reuse it immediately. This causes the
            // old values if any to be cleared before reuse. Make sure the
            // column doesn't try to access the null time slice.
            table.BeginUpdate();
            table.RemoveRow(oldRow);
            Row newRow = table.AddRow();
            table.EndUpdate();
        }
    }
}
