﻿using System;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class NumericBucketsTextColumnTest : IDisposable
    {
        private StandaloneTable table;
        private NumericColumn source;
        private NumericBucketsTextColumn buckets;

        public NumericBucketsTextColumnTest()
        {
            table = new StandaloneTable();
            table.BeginUpdate();
            table.AddColumn(source = new NumericColumn("Source"));
            table.EndUpdate();
        }


        /// <summary>
        /// If the source value is n/a, the bucket should be too.
        /// </summary>
        [Fact]
        public void InitiallyEmpty()
        {
            table.BeginUpdate();

            NumericInterval[] intervals = {
                new NumericInterval(-1.0, 0.0),
                new NumericInterval(0.0, 1.0)
            };
            string[] labels = { "less", "more" };
            buckets = new NumericBucketsTextColumn(
                "Buckets", source, intervals, labels);
            table.AddColumn(buckets);

            Row row = table.AddRow();
            table.EndUpdate();

            Assert.True(null == buckets.GetTextValue(row),
                "Value should be n/a, bucket should be too.");
        }


        /// <summary>
        /// The bucket should update when the source changes.
        /// </summary>
        [Fact]
        public void ChangesWithSource()
        {
            table.BeginUpdate();

            NumericInterval[] intervals = {
                new NumericInterval(-1.0, 0.0),
                new NumericInterval(0.0, 1.0)
            };
            string[] labels = { "less", "more" };
            buckets = new NumericBucketsTextColumn(
                "Buckets", source, intervals, labels);
            table.AddColumn(buckets);

            Row row = table.AddRow(new object[] { 0.5 });
            table.EndUpdate();

            Assert.True(string.Equals("more", buckets.GetTextValue(row)),
                "Bucket should have been initialized with row.");

            table.BeginUpdate();
            source.SetNumericValue(row, -0.5);
            // NOTE: We don't care what the bucket is until we call EndUpdate.
            table.EndUpdate();

            Assert.True(string.Equals("less", buckets.GetTextValue(row)),
                "When source changes, the bucket should update.");
        }

        /// <summary>
        /// The bucket should go back to n/a when the source does.
        /// </summary>
        [Fact]
        public void BackToEmpty()
        {
            table.BeginUpdate();

            NumericInterval[] intervals = {
                new NumericInterval(-1.0, 0.0),
                new NumericInterval(0.0, 1.0)
            };
            string[] labels = { "less", "more" };
            buckets = new NumericBucketsTextColumn(
                "Buckets", source, intervals, labels);
            table.AddColumn(buckets);

            Row row = table.AddRow(new object[] { 0.5 });
            table.EndUpdate();

            Assert.True(null != buckets.GetTextValue(row),
                "Bucket should have been initialized with row.");

            table.BeginUpdate();
            source.SetNumericValue(row, NumericValue.Empty);
            // NOTE: We don't care what the bucket is until we call EndUpdate.
            table.EndUpdate();

            Assert.True(null == buckets.GetTextValue(row),
                "When source goes n/a, so should bucket.");
        }

        // NOTE: The exact type of exception doesn't really matter here.
        /// <summary>
        /// Whatever happens, you should not get a valid bucket from a
        /// column that had its source removed (both n/a and execption is
        /// perfectly fine).
        /// </summary>
        [Fact]
        public void SourceRemoved()
        {
            table.BeginUpdate();

            NumericInterval[] intervals = {
                new NumericInterval(-1.0, 0.0),
                new NumericInterval(0.0, 1.0)
            };
            string[] labels = { "less", "more" };
            buckets = new NumericBucketsTextColumn(
                "Buckets", source, intervals, labels);
            table.AddColumn(buckets);

            Row row = table.AddRow(new object[] { 0.5 });
            table.EndUpdate();

#if CHECKED // Only throws this when run-time checks are enbled.
            Assert.Throws<ArgumentException>(() =>
#else
            Assert.ThrowsAny<Exception>(() =>
#endif
            {
                table.BeginUpdate();
                table.RemoveColumn(source);
                source = null;
                table.EndUpdate();

                Assert.True(null == buckets.GetTextValue(row),
                    "If this returns a value without exception, it's bad.");
            });
        }

        public void Dispose()
        {
            table.BeginUpdate();
            if (buckets != null)
            {
                table.RemoveColumn(buckets);
                buckets = null;
            }
            if (source != null)
            {
                table.RemoveColumn(source);
                source = null;
            }
            table.EndUpdate();
            table = null;
        }
    }
}