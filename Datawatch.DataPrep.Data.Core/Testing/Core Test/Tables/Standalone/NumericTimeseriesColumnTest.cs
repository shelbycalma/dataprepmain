﻿using System;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class NumericTimeseriesColumnTest
    {
        [Fact]
        public void GetNumericValue_ExplicitImpl()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try {
                NumericTimeseriesColumn column =
                    new NumericTimeseriesColumn("column");
                table.AddColumn(column);

                IRow r = table.AddRow();
                ITime t = table.AddTime(new DateTime(2010, 8, 26));
                table.RemoveTime((Time) t);

                var exc = 
#if !CHECKED 
                    Assert.Throws<IndexOutOfRangeException>(() =>
#else   // Only throws this when run-time checks are enbled.
                    Assert.Throws<InvalidOperationException>(() =>
#endif
                ((INumericTimeseriesColumn)column).GetNumericValue(r, t));
#if CHECKED
                Assert.Equal(
                    "The time has been deleted from the table.", exc.Message);
#endif
            } finally {
                table.EndUpdate();
            }
        }

        [Fact]
        public void GetNumericValue_Normal()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try {
                NumericTimeseriesColumn column =
                    new NumericTimeseriesColumn("column");
                table.AddColumn(column);

                Row r = table.AddRow();
                Time t = table.AddTime(new DateTime(2010, 8, 26));
                table.RemoveTime(t);

                var exc =
#if !CHECKED
                    Assert.Throws<IndexOutOfRangeException>(() =>
#else   // Only throws this when run-time checks are enbled.
                    Assert.Throws<InvalidOperationException>(() =>
#endif
                    column.GetNumericValue(r, t));
#if CHECKED
                Assert.Equal(
                    "The time has been deleted from the table.", exc.Message);
#endif
            } finally {
                table.EndUpdate();
            }
        }
    }
}
