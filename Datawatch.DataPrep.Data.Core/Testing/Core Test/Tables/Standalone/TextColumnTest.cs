﻿using System;
using System.Globalization;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class TextColumnTest : IDisposable
    {
        private static readonly CultureInfo
            culture = CultureInfo.InvariantCulture;

        private StandaloneTable table;
        private TextColumn column;

        public  TextColumnTest()
        {
            table = new StandaloneTable();
            column = new TextColumn("Column");
            table.BeginUpdate();
            table.AddColumn(column);
            table.EndUpdate();
        }

        public void Dispose()
        {
            table.BeginUpdate();
            table.RemoveColumn(column);
            table.EndUpdate();
            column = null;
            table = null;
        }

        private static string NewString(string source)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < source.Length; i++) {
                builder.Append(source[i]);
            }
            return builder.ToString();
        }

        [Fact]
        public void TextColumn_DefaultIsAutoCompression()
        {
            Assert.Equal(CompressionMode.Automatic, column.Compression);
        }

        [Fact]
        public void TextColumn_CanAddRowsWithAutoCompression()
        {
            table.BeginUpdate();
            table.AddRow(new object[] { "A" });
            table.AddRow(new object[] { "B" });
            table.AddRow(new object[] { "C" });
            table.AddRow(new object[] { "D" });
            table.AddRow(new object[] { "E" });
            table.AddRow(new object[] { "A" });
            table.EndUpdate();
        }

        [Fact]
        public void TextColumn_CanAddRowsWithNoCompression()
        {
            table.BeginUpdate();
            column.Compression = CompressionMode.Never;
            table.EndUpdate();

            table.BeginUpdate();
            table.AddRow(new object[] { "A" });
            table.AddRow(new object[] { "B" });
            table.AddRow(new object[] { "C" });
            table.AddRow(new object[] { "D" });
            table.AddRow(new object[] { "E" });
            table.AddRow(new object[] { "A" });
            table.EndUpdate();
        }

        [Fact]
        public void TextColumn_CanAddRowsWithCompression()
        {
            table.BeginUpdate();
            column.Compression = CompressionMode.Always;
            table.EndUpdate();

            table.BeginUpdate();
            table.AddRow(new object[] { "A" });
            table.AddRow(new object[] { "B" });
            table.AddRow(new object[] { "C" });
            table.AddRow(new object[] { "D" });
            table.AddRow(new object[] { "E" });
            table.AddRow(new object[] { "A" });
            table.EndUpdate();
        }

        [Fact]
        public void TextColumn_DoesCompress()
        {
            table.BeginUpdate();
            column.Compression = CompressionMode.Always;
            table.EndUpdate();

            string x = NewString("ABC");
            string y = NewString("ABC");

            Assert.NotSame(x, y);

            table.BeginUpdate();
            table.AddRow(new object[] { x });
            table.AddRow(new object[] { y });
            table.EndUpdate();

            string xx = column.GetTextValue(0);
            string yy = column.GetTextValue(1);

            Assert.Same(xx, yy);
        }

        [Fact]
        public void TextColumn_AutomaticCompressesInitially()
        {
            table.BeginUpdate();
            column.Compression = CompressionMode.Automatic;
            table.EndUpdate();

            string x = NewString("ABC");
            string y = NewString("ABC");

            Assert.NotSame(x, y);

            table.BeginUpdate();
            table.AddRow(new object[] { x });
            table.AddRow(new object[] { y });
            table.EndUpdate();

            string xx = column.GetTextValue(0);
            string yy = column.GetTextValue(1);

            Assert.Same(xx, yy);
        }

        [Fact]
        public void TextColumn_DoesNotCompress()
        {
            table.BeginUpdate();
            column.Compression = CompressionMode.Never;
            table.EndUpdate();

            string x = NewString("ABC");
            string y = NewString("ABC");

            Assert.NotSame(x, y);

            table.BeginUpdate();
            table.AddRow(new object[] { x });
            table.AddRow(new object[] { y });
            table.EndUpdate();

            string xx = column.GetTextValue(0);
            string yy = column.GetTextValue(1);

            Assert.NotSame(xx, yy);
            Assert.Same(x, xx);
            Assert.Same(y, yy);
        }

        [Fact]
        public void TextColumn_SwitchesToUncompressed()
        {
            table.BeginUpdate();
            column.Compression = CompressionMode.Automatic;
            table.EndUpdate();

            table.BeginUpdate();

            for (int i = 0; i < 1000; i++) {
                table.AddRow(new object[] { i.ToString(culture) });
            }

            string x = NewString("ABC");
            string y = NewString("ABC");

            Assert.NotSame(x, y);

            Row rx = table.AddRow(new object[] { x });
            Row ry = table.AddRow(new object[] { y });

            string xx = column.GetTextValue(rx);
            string yy = column.GetTextValue(ry);

            Assert.NotSame(xx, yy);
            Assert.Same(x, xx);
            Assert.Same(y, yy);

            table.EndUpdate();
        }

        /// <summary>
        /// CurrentDomain should correctly count distinct values.
        /// </summary>
        [Fact]
        public void TextColumn_CurrentDomain()
        {
            table.BeginUpdate();

            string[] domain = column.GetCurrentDomain();
            Assert.Equal(0, domain.Length);

            table.AddRow(new object[] { "A" });
            table.AddRow(new object[] { "C" });
            table.AddRow(new object[] { "B" });
            table.AddRow(new object[] { "A" });

            table.EndUpdate();

            domain = column.GetCurrentDomain();
            Assert.Equal(3, domain.Length);
            Assert.Equal("A", domain[0]);
            Assert.Equal("B", domain[1]);
            Assert.Equal("C", domain[2]);
        }

        /// <summary>
        /// CurrentDomain should work fine even with nulls.
        /// </summary>
        [Fact]
        public void TextColumn_CurrentDomain_Null()
        {
            table.BeginUpdate();

            table.AddRow();

            string[] domain = column.GetCurrentDomain();
            Assert.Equal(1, domain.Length);
            Assert.True(null == domain[0],
                "An unitialized row should show up as null.");

            // Check that the default comparer handles null.
            table.AddRow(new object[] { "A" });
            domain = column.GetCurrentDomain();
            Assert.Equal(2, domain.Length);
            // The order here is not important, really.
            Assert.Null(domain[0]);
            Assert.Equal("A", domain[1]);

            table.EndUpdate();
        }

        /// <summary>
        /// MetaDomain should not reorder values.
        /// </summary>
        [Fact]
        public void TextColumn_MetaDomain_Sort()
        {
            table.BeginUpdate();

            IMutableTextColumnMetaData meta =
                (IMutableTextColumnMetaData) column.MetaData;

            meta.Domain = new string[] { "C", "A", "B" };

            table.EndUpdate();

            // Should not touch order.
            Assert.Equal(3, meta.Domain.Length);
            Assert.Equal("C", meta.Domain[0]);
            Assert.Equal("A", meta.Domain[1]);
            Assert.Equal("B", meta.Domain[2]);
        }

        /// <summary>
        /// MetaDomain should allow one null value.
        /// </summary>
        [Fact]
        public void TextColumn_MetaDomain_Null()
        {
            table.BeginUpdate();

            IMutableTextColumnMetaData meta =
                (IMutableTextColumnMetaData) column.MetaData;

            meta.Domain = new string[] { "A", null };

            table.EndUpdate();
        }
    }
}
