﻿using System;
using System.IO;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class SerializerTest
    {
        [Fact]
        public void AllTypes()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            table.AddColumn(new TextColumn("Text"));
            table.AddColumn(new NumericColumn("Numeric"));
            table.AddColumn(new TimeColumn("Time"));
            table.AddRow(new object[] {
                "text", 12345.0, new DateTime(2014, 5, 21)
            });
            table.EndUpdate();

            MemoryStream stream = new MemoryStream();
            Serializer.WriteBinary(stream, table);
            stream.Position = 0;
            StandaloneTable copy = Serializer.ReadBinary(stream);

            Assert.Equal(3, copy.ColumnCount);
            Assert.Equal(1, copy.RowCount);

            Assert.Equal("Text", copy.GetColumn(0).Name);
            Assert.IsType<TextColumn>(copy.GetColumn(0));
            Assert.Equal("Numeric", copy.GetColumn(1).Name);
            Assert.IsType<NumericColumn>(copy.GetColumn(1));
            Assert.Equal("Time", copy.GetColumn(2).Name);
            Assert.IsType<TimeColumn>(copy.GetColumn(2));

            Assert.Equal("text",
                ((TextColumn) copy.GetColumn(0)).GetTextValue(0));
            Assert.Equal(12345.0,
                ((NumericColumn) copy.GetColumn(1)).GetNumericValue(0));
            Assert.Equal(new DateTime(2014, 5, 21),
                ((TimeColumn) copy.GetColumn(2)).GetTimeValue(0));
        }

        [Fact]
        public void NullsInFlatText()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            TextColumn text = new TextColumn("Text");
            text.Compression = CompressionMode.Never;
            table.AddColumn(text);
            table.AddRow();
            table.EndUpdate();

            MemoryStream stream = new MemoryStream();
            Serializer.WriteBinary(stream, table);
            stream.Position = 0;
            StandaloneTable copy = Serializer.ReadBinary(stream);

            Assert.Null(((TextColumn) copy.GetColumn(0)).GetTextValue(0));
        }

        [Fact]
        public void NullsInIndexedText()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            TextColumn text = new TextColumn("Text");
            text.Compression = CompressionMode.Always;
            table.AddColumn(text);
            table.AddRow();
            table.EndUpdate();

            MemoryStream stream = new MemoryStream();
            Serializer.WriteBinary(stream, table);
            stream.Position = 0;
            StandaloneTable copy = Serializer.ReadBinary(stream);

            Assert.Null(((TextColumn) copy.GetColumn(0)).GetTextValue(0));
        }

        [Fact]
        public void GapsInIndexedText()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            TextColumn text = new TextColumn("Text");
            text.Compression = CompressionMode.Always;
            table.AddColumn(text);
            table.AddRow();
            table.AddRow();
            table.AddRow();
            // All three rows are null, should be key 0.
            text.SetTextValue(0, "A");
            text.SetTextValue(1, "B");
            text.SetTextValue(2, "C");
            // Keys should now be 1, 2, 3.
            text.SetTextValue(1, null);
            // "B" no longer used, keys are probably 1, 0, 3.
            table.EndUpdate();

            MemoryStream stream = new MemoryStream();
            Serializer.WriteBinary(stream, table);
            stream.Position = 0;
            StandaloneTable copy = Serializer.ReadBinary(stream);

            // We don't care if copy's column is indexed or not.
            TextColumn copyText = (TextColumn) copy.GetColumn(0);
            Assert.Equal("A", copyText.GetTextValue(0));
            Assert.Null(copyText.GetTextValue(1));
            Assert.Equal("C", copyText.GetTextValue(2));
        }

        [Fact]
        public void SpecialNumericsRoundtrip()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            table.AddColumn(new NumericColumn("Numeric"));
            table.AddRow(new object[] { double.NaN });
            table.AddRow(new object[] { double.NegativeInfinity });
            table.AddRow(new object[] { double.PositiveInfinity });
            table.EndUpdate();

            MemoryStream stream = new MemoryStream();
            Serializer.WriteBinary(stream, table);
            stream.Position = 0;
            StandaloneTable copy = Serializer.ReadBinary(stream);

            NumericColumn column = (NumericColumn) copy.GetColumn("Numeric");

            double v0 = column.GetNumericValue(0);
            Assert.True(double.IsNaN(v0));
            double v1 = column.GetNumericValue(1);
            Assert.True(double.IsNegativeInfinity(v1));
            double v2 = column.GetNumericValue(2);
            Assert.True(double.IsPositiveInfinity(v2));
        }
    }
}
