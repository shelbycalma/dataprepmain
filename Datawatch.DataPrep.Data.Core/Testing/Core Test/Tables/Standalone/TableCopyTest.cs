﻿using System;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class TableCopyTest
    {
        [Fact]
        public void NonTimeseriesSchemaCopyTest()
        {
            MockedNonTimeseriesTable source = new MockedNonTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopySchema(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("Numeric", target.GetColumn(1).Name);
            Assert.IsType<NumericColumn>(target.GetColumn(1));
            Assert.Equal("Time", target.GetColumn(2).Name);
            Assert.IsType<TimeColumn>(target.GetColumn(2));

            Assert.Equal(0, target.TimeCount);
            Assert.Equal(0, target.RowCount);
        }

        [Fact]
        public void NonTimeseriesSchemaCopyThenAppendTest()
        {
            MockedNonTimeseriesTable source = new MockedNonTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopySchema(source, target);
            TableCopy.AppendData(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("Numeric", target.GetColumn(1).Name);
            Assert.IsType<NumericColumn>(target.GetColumn(1));
            Assert.Equal("Time", target.GetColumn(2).Name);
            Assert.IsType<TimeColumn>(target.GetColumn(2));

            Assert.Equal(0, target.TimeCount);

            Assert.Equal(3, target.RowCount);
            for (int r = 0; r < 3; r++)
            {
                TextColumn text = (TextColumn)target.GetColumn(0);
                string textValue = "V" + (r + 1);
                Assert.Equal(textValue, text.GetTextValue(r));
                NumericColumn numeric = (NumericColumn)target.GetColumn(1);
                double numericValue = r + 1.0;
                Assert.Equal(numericValue,
                    numeric.GetNumericValue(r), 5);
                TimeColumn time = (TimeColumn)target.GetColumn(2);
                DateTime timeValue = MakeDate(r + 1);
                Assert.Equal(timeValue, time.GetTimeValue(r));
            }
        }

        [Fact]
        public void NonTimeseriesFullCopyTest()
        {
            MockedNonTimeseriesTable source = new MockedNonTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopyAll(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("Numeric", target.GetColumn(1).Name);
            Assert.IsType<NumericColumn>(target.GetColumn(1));
            Assert.Equal("Time", target.GetColumn(2).Name);
            Assert.IsType<TimeColumn>(target.GetColumn(2));

            Assert.Equal(0, target.TimeCount);

            Assert.Equal(3, target.RowCount);
            for (int r = 0; r < 3; r++)
            {
                TextColumn text = (TextColumn)target.GetColumn(0);
                string textValue = "V" + (r + 1);
                Assert.Equal(textValue, text.GetTextValue(r));
                NumericColumn numeric = (NumericColumn)target.GetColumn(1);
                double numericValue = r + 1.0;
                Assert.Equal(numericValue,
                    numeric.GetNumericValue(r), 5);
                TimeColumn time = (TimeColumn)target.GetColumn(2);
                DateTime timeValue = MakeDate(r + 1);
                Assert.Equal(timeValue, time.GetTimeValue(r));
            }
        }

        [Fact]
        public void NonTimeseriesFullCopyThenAppendTest()
        {
            MockedNonTimeseriesTable source = new MockedNonTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopyAll(source, target);
            TableCopy.AppendData(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("Numeric", target.GetColumn(1).Name);
            Assert.IsType<NumericColumn>(target.GetColumn(1));
            Assert.Equal("Time", target.GetColumn(2).Name);
            Assert.IsType<TimeColumn>(target.GetColumn(2));

            Assert.Equal(0, target.TimeCount);

            Assert.Equal(6, target.RowCount);
            for (int r = 0; r < 6; r++)
            {
                int t = r % 3;
                TextColumn text = (TextColumn)target.GetColumn(0);
                string textValue = "V" + (t + 1);
                Assert.Equal(textValue, text.GetTextValue(r));
                NumericColumn numeric = (NumericColumn)target.GetColumn(1);
                double numericValue = t + 1.0;
                Assert.Equal(numericValue,
                    numeric.GetNumericValue(r), 5);
                TimeColumn time = (TimeColumn)target.GetColumn(2);
                DateTime timeValue = MakeDate(t + 1);
                Assert.Equal(timeValue, time.GetTimeValue(r));
            }
        }

        [Fact]
        public void NonTimeseriesFullCopyFrozenThenAppendTest()
        {
            StandaloneTable source = new StandaloneTable();
            source.BeginUpdate();
            source.AddColumn(new TextColumn("Name"));
            source.AddColumn(new TextColumn("Flat")
            {
                Compression = CompressionMode.Never
            });
            source.AddColumn(new NumericColumn("Number"));
            source.AddRow(new object[] { "A", "a", 1.0 });
            source.CanFreeze = true;
            source.Freeze();
            source.EndUpdate();

            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            // For the Flat column, it only gets interesting if the target
            // column also is flat, so we need to copy schema then enforce.
            TableCopy.CopySchema(source, target);
            ((TextColumn)target.GetColumn(1)).Compression = CompressionMode.Never;
            TableCopy.CopyData(source, target);
            target.EndUpdate();

            Assert.False(target.IsFrozen);

            StandaloneTable more = new StandaloneTable();
            more.BeginUpdate();
            more.AddColumn(new TextColumn("Name"));
            more.AddColumn(new TextColumn("Flat"));
            more.AddColumn(new NumericColumn("Number"));
            more.AddRow(new object[] { "B", "b", 2.0 });
            more.EndUpdate();

            target.BeginUpdate();
            TableCopy.AppendData(more, target);
            target.EndUpdate();

            Assert.Equal(2, target.RowCount);
            TextColumn name = (TextColumn)target.GetColumn(0);
            TextColumn flat = (TextColumn)target.GetColumn(1);
            NumericColumn number = (NumericColumn)target.GetColumn(2);
            Assert.Equal("A", name.GetTextValue(0));
            Assert.Equal("a", flat.GetTextValue(0));
            Assert.Equal(1.0, number.GetNumericValue(0), 5);
            Assert.Equal("B", name.GetTextValue(1));
            Assert.Equal("b", flat.GetTextValue(1));
            Assert.Equal(2.0, number.GetNumericValue(1), 5);
        }

        [Fact]
        public void CopyColumnsToLowerCapacityTest()
        {
            StandaloneTable source = new StandaloneTable();
            source.BeginUpdate();
            source.AddColumn(new NumericColumn("Numeric"));
            source.AddColumn(new TextColumn("Flat")
            {
                Compression = CompressionMode.Never
            });
            source.AddColumn(new TextColumn("Indexed")
            {
                Compression = CompressionMode.Always
            });
            for (int i = 0; i < 1000; i++)
            {
                source.AddRow(new object[] {
                    1.0 + i, "flat " + i, "indexed " + i
                });
            }
            source.EndUpdate();
            // Should have rounded up to 1024 or something, or this
            // test is pointless.
            Assert.True(source.RowCapacity > 1000);

            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            target.AddColumn(new NumericColumn("Numeric"));
            target.AddColumn(new TextColumn("Flat")
            {
                Compression = CompressionMode.Never
            });
            target.AddColumn(new TextColumn("Indexed")
            {
                Compression = CompressionMode.Always
            });
            target.AddRows(1000);
            // Should have set capacity exactly to 1000, or this
            // test is pointless.
            Assert.True(target.RowCapacity < source.RowCapacity);
            TableCopy.CopyData(source.GetColumn(0), target.GetColumn(0));
            TableCopy.CopyData(source.GetColumn(1), target.GetColumn(1));
            TableCopy.CopyData(source.GetColumn(2), target.GetColumn(2));
            target.EndUpdate();
        }

        [Fact]
        public void TimeseriesSchemaCopyTest()
        {
            MockedTimeseriesTable source = new MockedTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopySchema(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("NumericSeries", target.GetColumn(1).Name);
            Assert.IsType<NumericTimeseriesColumn>(target.GetColumn(1));
            Assert.Equal("TextSeries", target.GetColumn(2).Name);
            Assert.IsType<TextTimeseriesColumn>(target.GetColumn(2));

            Assert.Equal(0, target.TimeCount);
            Assert.Equal(0, target.RowCount);
        }

        [Fact]
        public void TimeseriesSchemaCopyThenAppendTest()
        {
            MockedTimeseriesTable source = new MockedTimeseriesTable();
            StandaloneTable target = new StandaloneTable();

            target.BeginUpdate();
            TableCopy.CopySchema(source, target);

            ITimeseriesTable series = source as ITimeseriesTable;
            if (series != null)
            {
                target.ClearTimes();
                for (int s = 0; s < series.TimeCount; s++)
                {
                    target.AddTime(series.GetTime(s).Time);
                }
            }

            TableCopy.AppendData(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("NumericSeries", target.GetColumn(1).Name);
            Assert.IsType<NumericTimeseriesColumn>(target.GetColumn(1));
            Assert.Equal("TextSeries", target.GetColumn(2).Name);
            Assert.IsType<TextTimeseriesColumn>(target.GetColumn(2));

            Assert.Equal(2, target.TimeCount);
            Assert.Equal(MakeDate(1), target.GetTime(0).DateTime);
            Assert.Equal(MakeDate(2), target.GetTime(1).DateTime);

            Assert.Equal(3, target.RowCount);
            for (int r = 0; r < 3; r++)
            {
                TextColumn text = (TextColumn)target.GetColumn(0);
                string textValue = "V" + (r + 1);
                Assert.Equal(textValue, text.GetTextValue(r));
                for (int s = 0; s < 2; s++)
                {
                    NumericTimeseriesColumn numericSeries =
                        (NumericTimeseriesColumn)target.GetColumn(1);
                    double numericSeriesValue = r + 1 + 0.1 * (s + 1);
                    Assert.Equal(numericSeriesValue,
                        numericSeries.GetNumericValue(r, s), 5);
                    TextTimeseriesColumn textSeries =
                        (TextTimeseriesColumn)target.GetColumn(2);
                    string textSeriesValue = "V" + (r + 1) + (s + 1);
                    Assert.Equal(textSeriesValue,
                        textSeries.GetTextValue(r, s));
                }
            }
        }

        [Fact]
        public void TimeseriesFullCopyTest()
        {
            MockedTimeseriesTable source = new MockedTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopyAll(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("NumericSeries", target.GetColumn(1).Name);
            Assert.IsType<NumericTimeseriesColumn>(target.GetColumn(1));
            Assert.Equal("TextSeries", target.GetColumn(2).Name);
            Assert.IsType<TextTimeseriesColumn>(target.GetColumn(2));

            Assert.Equal(2, target.TimeCount);
            Assert.Equal(MakeDate(1), target.GetTime(0).DateTime);
            Assert.Equal(MakeDate(2), target.GetTime(1).DateTime);

            Assert.Equal(3, target.RowCount);
            for (int r = 0; r < 3; r++)
            {
                TextColumn text = (TextColumn)target.GetColumn(0);
                string textValue = "V" + (r + 1);
                Assert.Equal(textValue, text.GetTextValue(r));
                for (int s = 0; s < 2; s++)
                {
                    NumericTimeseriesColumn numericSeries =
                        (NumericTimeseriesColumn)target.GetColumn(1);
                    double numericSeriesValue = r + 1 + 0.1 * (s + 1);
                    Assert.Equal(numericSeriesValue,
                        numericSeries.GetNumericValue(r, s), 5);
                    TextTimeseriesColumn textSeries =
                        (TextTimeseriesColumn)target.GetColumn(2);
                    string textSeriesValue = "V" + (r + 1) + (s + 1);
                    Assert.Equal(textSeriesValue,
                        textSeries.GetTextValue(r, s));
                }
            }
        }

        [Fact]
        public void TimeseriesFullCopyThenAppendTest()
        {
            MockedTimeseriesTable source = new MockedTimeseriesTable();
            StandaloneTable target = new StandaloneTable();
            target.BeginUpdate();
            TableCopy.CopyAll(source, target);
            TableCopy.AppendData(source, target);
            target.EndUpdate();

            Assert.Equal(3, target.ColumnCount);
            Assert.Equal("Text", target.GetColumn(0).Name);
            Assert.IsType<TextColumn>(target.GetColumn(0));
            Assert.Equal("NumericSeries", target.GetColumn(1).Name);
            Assert.IsType<NumericTimeseriesColumn>(target.GetColumn(1));
            Assert.Equal("TextSeries", target.GetColumn(2).Name);
            Assert.IsType<TextTimeseriesColumn>(target.GetColumn(2));

            Assert.Equal(2, target.TimeCount);
            Assert.Equal(MakeDate(1), target.GetTime(0).DateTime);
            Assert.Equal(MakeDate(2), target.GetTime(1).DateTime);

            Assert.Equal(6, target.RowCount);
            for (int r = 0; r < 3; r++)
            {
                int t = r % 3;
                TextColumn text = (TextColumn)target.GetColumn(0);
                string textValue = "V" + (t + 1);
                Assert.Equal(textValue, text.GetTextValue(r));
                for (int s = 0; s < 2; s++)
                {
                    NumericTimeseriesColumn numericSeries =
                        (NumericTimeseriesColumn)target.GetColumn(1);
                    double numericSeriesValue = t + 1 + 0.1 * (s + 1);
                    Assert.Equal(numericSeriesValue,
                        numericSeries.GetNumericValue(r, s), 5);
                    TextTimeseriesColumn textSeries =
                        (TextTimeseriesColumn)target.GetColumn(2);
                    string textSeriesValue = "V" + (t + 1) + (s + 1);
                    Assert.Equal(textSeriesValue,
                        textSeries.GetTextValue(r, s));
                }
            }
        }

        private class MockedNonTimeseriesTable : ITable
        {
            public event EventHandler<TableChangingEventArgs> Changing;
            public event EventHandler<TableChangedEventArgs> Changed;

            protected MockedColumn[] columns;
            protected MockedRow[] rows;

            public MockedNonTimeseriesTable()
            {
                columns = new MockedColumn[] {
                    new MockedTextColumn(this, "Text"),
                    new MockedNumericColumn(this, "Numeric"),
                    new MockedTimeColumn(this, "Time")
                };
                rows = new MockedRow[] {
                    new MockedRow(0), new MockedRow(1), new MockedRow(2)
                };
            }

            public int ColumnCount
            {
                get { return columns.Length; }
            }

            public IColumn GetColumn(int index)
            {
                return columns[index];
            }

            public IColumn GetColumn(string name)
            {
                for (int i = 0; i < columns.Length; i++)
                {
                    if (columns[i].Name.Equals(name))
                    {
                        return columns[i];
                    }
                }
                return null;
            }

            public IRow GetRow(int index)
            {
                return rows[index];
            }

            public bool IsUpdating
            {
                get { return false; }
            }

            public int RowCount
            {
                get { return rows.Length; }
            }
        }

        private class MockedRow : IRow
        {
            private readonly int index;

            public MockedRow(int index)
            {
                this.index = index;
            }

            internal int Index
            {
                get { return index; }
            }
        }

        private abstract class MockedColumn : IColumn
        {
            private readonly MockedNonTimeseriesTable table;
            private readonly string name;
            private IColumnMetaData meta;

            protected MockedColumn(MockedNonTimeseriesTable table, string name)
            {
                this.table = table;
                this.name = name;
            }

            public IColumnMetaData MetaData
            {
                get { return meta; }
                set { meta = value; }
            }

            public string Name
            {
                get { return name; }
            }

            public ITable Table
            {
                get { return table; }
            }
        }

        private class MockedTextColumn : MockedColumn, ITextColumn
        {
            private readonly string[] values = { "V1", "V2", "V3" };

            public MockedTextColumn(MockedNonTimeseriesTable table,
                string name) : base(table, name)
            {
            }

            public string GetTextValue(IRow row)
            {
                return values[((MockedRow)row).Index];
            }

            public string GetTextValue(int row)
            {
                return values[row];
            }
        }

        private class MockedNumericColumn : MockedColumn, INumericColumn
        {
            private readonly double[] values = { 1.0, 2.0, 3.0 };

            public MockedNumericColumn(MockedNonTimeseriesTable table,
                string name) : base(table, name)
            {
            }

            public double GetNumericValue(IRow row)
            {
                return values[((MockedRow)row).Index];
            }

            public double GetNumericValue(int row)
            {
                return values[row];
            }
        }

        private class MockedTimeColumn : MockedColumn, ITimeColumn
        {
            private readonly DateTime[] values = {
                MakeDate(1), MakeDate(2), MakeDate(3)
            };

            public MockedTimeColumn(MockedNonTimeseriesTable table,
                string name) : base(table, name)
            {
            }

            public DateTime GetTimeValue(IRow row)
            {
                return values[((MockedRow)row).Index];
            }

            public DateTime GetTimeValue(int row)
            {
                return values[row];
            }
        }

        private class MockedTimeseriesTable :
            MockedNonTimeseriesTable, ITimeseriesTable
        {
            private MockedTime[] times;

            public MockedTimeseriesTable()
            {
                columns = new MockedColumn[] {
                    new MockedTextColumn(this, "Text"),
                    new MockedNumericSeriesColumn(this, "NumericSeries"),
                    new MockedTextSeriesColumn(this, "TextSeries")
                };
                times = new MockedTime[] {
                    new MockedTime(0, MakeDate(1)),
                    new MockedTime(1, MakeDate(2))
                };
            }

            public int TimeCount
            {
                get { return times.Length; }
            }

            public ITime GetTime(int index)
            {
                return times[index];
            }

            public ITime SnapshotTime
            {
                get { return null; }
            }
        }

        private class MockedTime : ITime
        {
            private readonly int index;
            private readonly DateTime time;

            public MockedTime(int index, DateTime time)
            {
                this.index = index;
                this.time = time;
            }

            internal int Index
            {
                get { return index; }
            }

            public DateTime Time
            {
                get { return time; }
            }
        }

        private class MockedNumericSeriesColumn :
            MockedColumn, INumericTimeseriesColumn
        {
            private readonly double[,] values = {
                { 1.1, 1.2 },
                { 2.1, 2.2 },
                { 3.1, 3.2 }
            };

            public MockedNumericSeriesColumn(MockedTimeseriesTable table,
                string name) : base(table, name)
            {
            }

            public double GetNumericValue(IRow row, ITime time)
            {
                int ri = ((MockedRow)row).Index;
                int si = ((MockedTime)time).Index;
                return values[ri, si];
            }

            public double GetNumericValue(int row, int sample)
            {
                return values[row, sample];
            }

            public double GetNumericValue(IRow row)
            {
                throw new NotImplementedException();
            }

            public double GetNumericValue(int row)
            {
                throw new NotImplementedException();
            }
        }

        private class MockedTextSeriesColumn :
            MockedColumn, ITextTimeseriesColumn
        {
            private readonly string[,] values = {
                { "V11", "V12" },
                { "V21", "V22" },
                { "V31", "V32" }
            };

            public MockedTextSeriesColumn(MockedTimeseriesTable table,
                string name) : base(table, name)
            {
            }

            public string GetTextValue(IRow row, ITime time)
            {
                int ri = ((MockedRow)row).Index;
                int si = ((MockedTime)time).Index;
                return values[ri, si];
            }

            public string GetTextValue(int row, int sample)
            {
                return values[row, sample];
            }
        }

        private static DateTime MakeDate(int number)
        {
            return new DateTime(2014, 1, number);
        }
    }
}
