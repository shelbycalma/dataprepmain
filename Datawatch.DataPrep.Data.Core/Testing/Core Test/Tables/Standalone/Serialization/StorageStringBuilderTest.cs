﻿using System;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    public class StorageStringBuilderTest
    {

        [Fact]
        public void NullParams()
        {
            StorageStringBuilder builder = new StorageStringBuilder();
            try
            {
                builder.BuildIndexedTextColumn(null, null, null);
                Assert.True(false);
            } catch(ArgumentNullException ex)
            {
                // Expected
            }
            try
            {
                builder.BuildIndexedTextColumn(new List<int>(), null, null);
                Assert.True(false);
            }
            catch (ArgumentNullException ex)
            {
                // Expected
            }
            try
            {
                builder.BuildIndexedTextColumn(new List<int>(), new List<string>(), null);
                Assert.True(false);
            }
            catch (ArgumentNullException ex)
            {
                // Expected
            }
            try
            {
                builder.BuildIndexedTextColumn(new List<int>(), new List<string>(), "");
            }
            catch (ArgumentNullException ex)
            {
                Assert.True(false);
            }
        }

        [Fact]
        public void BuildIndexedTextColumnTest()
        {
            StorageStringBuilder builder = new StorageStringBuilder();
            List<int> index = new List<int>();
            index.Add(1);
            index.Add(1);
            index.Add(0);
            index.Add(2);

            List<string> domain = new List<string>();
            domain.Add(null);
            domain.Add("First");
            domain.Add("Second");
            domain.Add("Third");

            TextColumn col = builder.BuildIndexedTextColumn(index, domain, "ColumnName");
            Assert.True(col.IsIndexed);
            Assert.Equal(CompressionMode.Always, col.Compression);
            Assert.Equal(col.GetTextValue(1), "First");
            Assert.Null(col.GetTextValue(2));
        }
    }
}
