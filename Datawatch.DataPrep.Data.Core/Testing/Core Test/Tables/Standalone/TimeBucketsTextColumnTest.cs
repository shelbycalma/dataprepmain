﻿using System;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Table;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class TimeBucketsTextColumnTest : IDisposable
    {
        private StandaloneTable table;
        private TimeColumn source;
        private TimeBucketsTextColumn buckets;

        public TimeBucketsTextColumnTest()
        {
            table = new StandaloneTable();
            table.BeginUpdate();
            table.AddColumn(source = new TimeColumn("Source"));
            table.EndUpdate();
        }

        public void Dispose()
        {
            table.BeginUpdate();
            if (buckets != null) {
                table.RemoveColumn(buckets);
                buckets = null;
            }
            if (source != null) {
                table.RemoveColumn(source);
                source = null;
            }
            table.EndUpdate();
            table = null;
        }

        /// <summary>
        /// If the source value is n/a, the bucket should be too.
        /// </summary>
        [Fact]
        public void InitiallyEmpty()
        {
            table.BeginUpdate();

            buckets = new TimeBucketsTextColumn(
                "Buckets", source, TimePart.Day,
                CultureInfo.InvariantCulture.DateTimeFormat);
            table.AddColumn(buckets);

            Row row = table.AddRow();            
            table.EndUpdate();

            Assert.True(null == buckets.GetTextValue(row),
                "Value should be n/a, bucket should be too.");
        }

        /// <summary>
        /// The bucket should update when the source changes.
        /// </summary>
        [Fact]
        public void ChangesWithSource()
        {
            table.BeginUpdate();

            buckets = new TimeBucketsTextColumn(
                "Buckets", source, TimePart.Day,
                CultureInfo.InvariantCulture.DateTimeFormat);
            table.AddColumn(buckets);

            Row row = table.AddRow(new object[] { new DateTime(1973, 07, 23) });
            table.EndUpdate();

            Assert.True("23" == buckets.GetTextValue(row),
                "Bucket should have been initialized with row.");

            table.BeginUpdate();
            source.SetTimeValue(row, new DateTime(2013, 12, 24));
            // NOTE: We don't care what the bucket is until we call EndUpdate.
            table.EndUpdate();

            Assert.True("24" == buckets.GetTextValue(row),
                "When source changes, the bucket should update.");
        }

        /// <summary>
        /// The bucket should go back to n/a when the source does.
        /// </summary>
        [Fact]
        public void BackToEmpty()
        {
            table.BeginUpdate();

            buckets = new TimeBucketsTextColumn(
                "Buckets", source, TimePart.Day,
                CultureInfo.InvariantCulture.DateTimeFormat);
            table.AddColumn(buckets);

            Row row = table.AddRow(new object[] { new DateTime(1973, 07, 23) });
            table.EndUpdate();

            Assert.True(null != buckets.GetTextValue(row),
                "Bucket should have been initialized with row.");

            table.BeginUpdate();
            source.SetTimeValue(row, TimeValue.Empty);
            // NOTE: We don't care what the bucket is until we call EndUpdate.
            table.EndUpdate();

            Assert.True(null == buckets.GetTextValue(row),
                "When source goes n/a, so should bucket.");
        }

        /// <summary>
        /// Buckets column should properly calculate its domain.
        /// </summary>
        [Fact]
        public void CurrentDomain()
        {
            table.BeginUpdate();

            buckets = new TimeBucketsTextColumn(
                "bucket", source, TimePart.Month,
                CultureInfo.InvariantCulture.DateTimeFormat);
            table.AddColumn(buckets);

            table.EndUpdate();

            Assert.Equal(0, buckets.GetCurrentDomain().Length);

            table.BeginUpdate();
            table.AddRow(new object[] { new DateTime(2010, 5, 1)});
            table.EndUpdate();

            Assert.Equal(1, buckets.GetCurrentDomain().Length);
            Assert.Equal("May", buckets.GetCurrentDomain()[0]);
        }

        /// <summary>
        /// Buckets column properly calculates its domain when cleared.
        /// </summary>
        [Fact]
        public void CurrentDomainAfterClear()
        {
            table.BeginUpdate();

            table.AddRow(new object[] { new DateTime(2010, 5, 1) });
            table.AddRow(new object[] { new DateTime(2010, 6, 1) });
            table.AddRow(new object[] { new DateTime(2010, 7, 1) });

            buckets = new TimeBucketsTextColumn(
                "bucket", source, TimePart.Month,
                CultureInfo.InvariantCulture.DateTimeFormat);
            table.AddColumn(buckets);

            table.EndUpdate();

            Assert.Equal(3, buckets.GetCurrentDomain().Length);
            Assert.Equal("May", buckets.GetCurrentDomain()[0]);
            Assert.Equal("June", buckets.GetCurrentDomain()[1]);
            Assert.Equal("July", buckets.GetCurrentDomain()[2]);

            table.BeginUpdate();
            table.ClearRows();
            table.EndUpdate();

            Assert.Equal(0, buckets.GetCurrentDomain().Length);
        }

        // NOTE: The exact type of exception doesn't really matter here.
        /// <summary>
        /// Whatever happens, you should not get a valid bucket from a
        /// column that had its source removed (both n/a and execption is
        /// perfectly fine).
        /// </summary>
        [Fact]
        public void SourceRemoved()
        {
            table.BeginUpdate();

            buckets = new TimeBucketsTextColumn(
                "Buckets", source, TimePart.Day,
                CultureInfo.InvariantCulture.DateTimeFormat);
            table.AddColumn(buckets);

            Row row = table.AddRow(new object[] { new DateTime(1973, 07, 23) });
            table.EndUpdate();

            string res = null;

#if CHECKED // Only throws this when run-time checks are enbled.
            Assert.Throws<ArgumentException>(
#else
            Assert.ThrowsAny<Exception>(
#endif
                () =>
                {
                    table.BeginUpdate();
                    table.RemoveColumn(source);
                    source = null;
                    table.EndUpdate();

                    res = buckets.GetTextValue(row);
                });
            Assert.True(null == res,
                "If this returns a value without exception, it's bad.");
        }
    }
}