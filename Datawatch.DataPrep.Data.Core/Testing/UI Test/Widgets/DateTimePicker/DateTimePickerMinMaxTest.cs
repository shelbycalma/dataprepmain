﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Core.UI.Widgets;

using Xunit;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    public class DateTimePickerMinMaxTest : IDisposable
    {
        private DateTimeSelectableFieldViewModel year;
        private DateTimeSelectableFieldViewModel month;
        private DateTimeSelectableFieldViewModel day;
        private DateTimeSelectableFieldViewModel hour;
        private DateTimeSelectableFieldViewModel minute;
        private DateTimeSelectableFieldViewModel second;
        private readonly DateTime defaultMin = new DateTime(2008, 1, 2);
        private readonly DateTime defaultMax = new DateTime(2009, 3, 6);

        private void CreateViewModels()
        {
            CreateViewModels(defaultMin, defaultMax);
        }

        private void CreateViewModels(DateTime min, DateTime max)
        {
            CultureInfo culture = CultureInfo.GetCultureInfo("sv-SE");
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel(
                culture,
                null,
                new DateTimeInterval(min, max));
            List<DateTimeFieldViewModel> fields = viewModel.Fields;

            // assume swedish locale
            year = (DateTimeSelectableFieldViewModel)fields[0];
            month = (DateTimeSelectableFieldViewModel)fields[2];
            day = (DateTimeSelectableFieldViewModel)fields[4];
            hour = (DateTimeSelectableFieldViewModel)fields[6];
            minute = (DateTimeSelectableFieldViewModel)fields[8];
            second = (DateTimeSelectableFieldViewModel)fields[10];
        }

        public void Dispose()
        {
            year = null;
            month = null;
            day = null;
            hour = null;
            minute = null;
            second = null;
        }

        [Fact]
        public void YearItemsCount()
        {
            CreateViewModels();
            Assert.Equal(2, year.Items.Count);
            Assert.Equal(2, CountEnabled(year));
        }

        [Fact]
        public void MonthItemsCount()
        {
            CreateViewModels();
            Assert.True(12 == month.Items.Count, "total");
            Assert.True(12 == CountEnabled(month), "enabled");
        }

        [Fact]
        public void MonthItemsCount2()
        {
            DateTime min = new DateTime(2012, 6, 26, 10, 38, 40);
            DateTime max = new DateTime(2012, 6, 26, 10, 38, 50);
            CreateViewModels(min, max);
            Assert.True(12 == month.Items.Count, "total");
            Assert.True(1 == CountEnabled(month), "enabled"); // only june
        }

        [Fact]
        public void DayItemsCount()
        {
            CreateViewModels();

            // the minimum date value is the second of january,
            // so one day should be disabled
            Assert.True(31 == day.Items.Count, "total");
            Assert.True(30 == CountEnabled(day), "enabled");
        }

        [Fact]
        public void HourItemsCount()
        {
            DateTime min = new DateTime(2012, 6, 26, 9, 38, 40);
            DateTime max = new DateTime(2012, 6, 26, 11, 38, 40);
            CreateViewModels(min, max);
            Assert.True(24 == hour.Items.Count, "total");
            Assert.True(3 == CountEnabled(hour), "enabled");
        }

        [Fact]
        public void MinuteItemsCount()
        {
            DateTime min = new DateTime(2012, 6, 26, 10, 36, 40);
            DateTime max = new DateTime(2012, 6, 26, 10, 40, 40);
            CreateViewModels(min, max);
            Assert.True(60 == minute.Items.Count, "total");
            Assert.True(5 == CountEnabled(minute), "enabled");
        }

        [Fact]
        public void SecondItemsCount()
        {
            DateTime min = new DateTime(2012, 6, 26, 10, 38, 37);
            DateTime max = new DateTime(2012, 6, 26, 10, 38, 43);
            CreateViewModels(min, max);
            Assert.True(60 == second.Items.Count, "total");
            Assert.True(7 == CountEnabled(second), "enabled");
        }

        private static int CountEnabled(DateTimeSelectableFieldViewModel field)
        {
            int enabled = 0;
            foreach (DateTimeValueViewModel item in field.Items)
            {
                if (item.IsEnabled)
                {
                    enabled++;
                }
            }
            return enabled;
        }
    }
}
