﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Datawatch.DataPrep.Data.Core.UI;
using Datawatch.DataPrep.Data.Core.UI.Widgets;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    public class DateTimePickerViewModelTest
    {
        [Fact]
        public void PropertyChangedTest()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            TestPropertyChangedListener listener =
                new TestPropertyChangedListener(viewModel);
            viewModel.Year.Selected = viewModel.Year.Items[0];
            Assert.Equal(1, listener.Events.Count);
            Assert.Equal("DateTime", listener.Events[0].PropertyName);
            Assert.Same(viewModel, listener.ViewModel);
        }

        [Fact]
        public void DateTimeRoundTrip()
        {
            DateTime dateTime = new DateTime(2012, 2, 20);
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel(
                CultureInfo.GetCultureInfo("sv-SE"));
            viewModel.DateTime = dateTime;
            Assert.Equal(dateTime, viewModel.DateTime);
        }

        [Fact]
        public void ShouldLocalizeMonthNames()
        {
            DateTime dateTime = new DateTime(2012, 2, 20);
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel(
                CultureInfo.GetCultureInfo("sv-SE"));
            viewModel.DateTime = dateTime;
            DateTimeSelectableFieldViewModel field = viewModel.Month;
            Assert.Equal("feb", field.Selected.Title);
        }

        [Fact]
        public void SelectableDaysShouldRespectMonthAndYear()
        {
            DateTime dateTime = new DateTime(2012, 2, 20);
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel(
                CultureInfo.GetCultureInfo("sv-SE"));
            viewModel.DateTime = dateTime;
            DateTimeSelectableFieldViewModel field = viewModel.Day;
            Assert.Equal(29, field.Items.Count);
            viewModel.DateTime = new DateTime(2011, 2, 20);
            Assert.Equal(28, field.Items.Count);
        }

        [Fact]
        public void ShouldCoerceSelectedDayWhenInvalid()
        {
            DateTime dateTime = new DateTime(2012, 2, 29);
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel(
                CultureInfo.GetCultureInfo("sv-SE"));
            viewModel.DateTime = dateTime;
            DateTimeSelectableFieldViewModel yearField = viewModel.Year;
            yearField.Selected = new DateTimeValueViewModel(2011);
            DateTimeSelectableFieldViewModel dayField = viewModel.Day;
            Assert.Equal(new DateTimeValueViewModel(28), dayField.Selected);
        }

        [Fact]
        public void ShouldFireChangeEventWhenSettingDateTime()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            TestPropertyChangedListener listener =
                new TestPropertyChangedListener(viewModel);
            DateTime dateTime = new DateTime(2012, 12, 12);
            viewModel.DateTime = dateTime;
            Assert.Equal(1, listener.Events.Count);
            Assert.Equal(dateTime, viewModel.DateTime);
        }

        [Fact]
        public void ShouldFireChangeEventWhenSettingEnabledInterval()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            TestPropertyChangedListener listener =
                new TestPropertyChangedListener(viewModel);
            DateTimeInterval interval = CreateStocksTimeSeriesInterval();
            viewModel.EnabledInterval = interval;
            Assert.Equal(1, listener.Events.Count);
            Assert.Equal(interval, viewModel.EnabledInterval);
        }

        [Fact]
        public void ShouldNotAddMillisecondsToDefaultInterval()
        {
            DateTimeInterval interval =
                DateTimePickerViewModel.GetDefaultInterval();
            Assert.Equal(0, interval.Min.Millisecond);
            Assert.Equal(0, interval.Max.Millisecond);
        }

        [Fact]
        public void ShouldInitiallyHaveDefaultInterval()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            Assert.Equal(
                DateTimePickerViewModel.GetDefaultInterval(),
                viewModel.EnabledInterval);
            Assert.Equal(
                DateTimePickerViewModel.GetDefaultInterval().Min,
                viewModel.DateTime);
        }

        [Fact]
        public void ShouldEnableLeftExtreme()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            DateTimeInterval interval =
                DateTimePickerViewModel.GetDefaultInterval();
            viewModel.DateTime = interval.Min;
            Assert.Equal(interval.Min, viewModel.DateTime);
        }

        [Fact]
        public void ShouldEnableRightExtreme()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            DateTimeInterval interval =
                DateTimePickerViewModel.GetDefaultInterval();
            viewModel.DateTime = interval.Max;
            Assert.Equal(interval.Max, viewModel.DateTime);
        }

        [Fact]
        public void ShouldSetMinValueWhenChangingInterval()
        {
            DateTimeInterval interval = CreateStocksTimeSeriesInterval();
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            viewModel.EnabledInterval = interval;
            Assert.Equal(interval.Min, viewModel.DateTime);
        }

        [Fact]
        public void ShouldHandleStocksTimeSeriesInterval()
        {
            DateTimeInterval interval = CreateStocksTimeSeriesInterval();
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            viewModel.EnabledInterval = interval;
            viewModel.DateTime = interval.Min;
            Assert.Equal(interval, viewModel.EnabledInterval);
            Assert.Equal(interval.Min, viewModel.DateTime);
            DateTimeSelectableFieldViewModel dayField = viewModel.Day;
            Assert.False(dayField.Items[0].IsEnabled);
        }

        [Fact]
        public void ShouldPreserveYearSelection()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            DateTimeSelectableFieldViewModel yearField = viewModel.Year;
            yearField.Selected = yearField.Items[2];
            Assert.Equal(yearField.Items[2], yearField.Selected);
        }

        [Fact]
        public void ShouldPreserveSelectionWhenChangingInterval()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            DateTime dateTime = new DateTime(2008, 8, 8);
            viewModel.DateTime = dateTime;
            viewModel.EnabledInterval = CreateStocksTimeSeriesInterval();
            Assert.Equal(dateTime, viewModel.DateTime);
        }

        [Fact]
        public void ShouldPreserveSelectionWhenChangingIntervalIfMax()
        {
            DateTimePickerViewModel viewModel = new DateTimePickerViewModel();
            DateTimeInterval interval = CreateStocksTimeSeriesInterval();
            DateTime dateTime = interval.Max;
            viewModel.DateTime = dateTime;
            viewModel.EnabledInterval = interval;
            Assert.Equal(dateTime, viewModel.DateTime);
        }

        // test every format available in DateTimeFormatComboBox
        [Theory]
        [InlineData("MM/dd/yyyy", 3)]
        [InlineData("yyyy/MM/dd", 3)]
        [InlineData("dd/MMM/yyyy", 3)]
        [InlineData("MMM/yyyy", 2)]
        [InlineData("hh:mm tt", 2)]
        [InlineData("hh:mm:ss tt", 3)]
        [InlineData("hh:mm:ss.fff tt", 3)]
        [InlineData("HH:mm", 2)]
        [InlineData("HH:mm:ss", 3)]
        [InlineData("HH:mm:ss.fff", 3)]
        [InlineData("dd/MM/yyyy HH:mm:ss", 6)]
        [InlineData("MM/dd/yyyy HH:mm:ss", 6)]
        public void ShouldCreateCorrectNumberOfFields(string format, int expected)
        {
            DateTimePickerViewModel viewModel =
                new DateTimePickerViewModel(format);

            // count only selectable fields, not separators
            int count = 0;
            foreach (DateTimeFieldViewModel field in viewModel.Fields)
            {
                if (field is DateTimeSelectableFieldViewModel)
                {
                    count++;
                }
            }

            Assert.Equal(expected, count);
        }

        [Fact]
        public void ShouldHandleInvalidHiddenDay()
        {
            // use a time interval that has to many days for february
            DateTimeInterval interval = new DateTimeInterval(
                new DateTime(2012, 1, 31), new DateTime(2012, 3, 31));

            // use a format that hides the day combo box
            DateTimePickerViewModel viewModel =
                new DateTimePickerViewModel(null, "yyyy/MM", interval);

            // ... and select february
            viewModel.Month.Selected = viewModel.Month.Items[1];
            Assert.Equal(new DateTime(2012, 2, 29), viewModel.DateTime);
        }

        [Theory (Skip = "Ignored")]
        [InlineData("MM/dd/yyyy", "/")]
        [InlineData("yyyy/MM/dd", "/")]
        [InlineData("dd/MMM/yyyy", "/")]
        [InlineData("MMM/yyyy", "/")]
        [InlineData("hh:mm tt", null)]
        [InlineData("hh:mm:ss tt", null)]
        [InlineData("hh:mm:ss.fff tt", null)]
        [InlineData("HH:mm", null)]
        [InlineData("HH:mm:ss", null)]
        [InlineData("HH:mm:ss.fff", null)]
        [InlineData("dd/MM/yyyy HH:mm:ss", "/")]
        [InlineData("MM/dd/yyyy HH:mm:ss", "/")]
        public string ShouldGuessDateSeparator(string format)
        {
            DateTimePickerViewModel viewModel =
                new DateTimePickerViewModel(format);
            return viewModel.DateSeparator.Separator;
        }

        [Theory(Skip = "Ignored")]
        [InlineData("MM/dd/yyyy", null)]
        [InlineData("yyyy/MM/dd", null)]
        [InlineData("dd/MMM/yyyy", null)]
        [InlineData("MMM/yyyy", null)]
        [InlineData("hh:mm tt", ":")]
        [InlineData("hh:mm:ss tt", ":")]
        [InlineData("hh:mm:ss.fff tt", ":")]
        [InlineData("HH:mm", ":")]
        [InlineData("HH:mm:ss", ":")]
        [InlineData("HH:mm:ss.fff", ":")]
        [InlineData("dd/MM/yyyy HH:mm:ss", ":")]
        [InlineData("MM/dd/yyyy HH:mm:ss", ":")]
        public string ShouldGuessTimeSeparator(string format)
        {
            DateTimePickerViewModel viewModel =
                new DateTimePickerViewModel(format);
            return viewModel.TimeSeparator.Separator;
        }

        private static DateTimeInterval CreateStocksTimeSeriesInterval()
        {
            DateTime min = new DateTime(2008, 1, 2);
            DateTime max = new DateTime(2009, 3, 6);
            DateTimeInterval interval = new DateTimeInterval(min, max);
            return interval;
        }

        private class TestPropertyChangedListener
        {
            private readonly List<PropertyChangedEventArgs> events =
                new List<PropertyChangedEventArgs>();

            private readonly ViewModelBase viewModel;

            public TestPropertyChangedListener(ViewModelBase viewModel)
            {
                if (viewModel == null)
                {
                    throw Exceptions.ArgumentNull("viewModel");
                }
                viewModel.PropertyChanged += ViewModel_PropertyChanged;
                this.viewModel = viewModel;
            }

            private void ViewModel_PropertyChanged(
                object sender, PropertyChangedEventArgs e)
            {
                events.Add(e);
            }

            public List<PropertyChangedEventArgs> Events
            {
                get { return events; }
            }

            public ViewModelBase ViewModel
            {
                get { return viewModel; }
            }
        }
    }
}
