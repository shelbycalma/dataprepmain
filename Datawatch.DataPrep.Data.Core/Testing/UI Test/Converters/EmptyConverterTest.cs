﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Data;
using System.ComponentModel;
using System.Collections;
using System.Windows.Controls;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class EmptyConverterTest
    {
        [Fact]
        public void ConvertICollectionView()
        {
            EmptyConverter ec = new EmptyConverter();
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;

            ListCollectionView ic = new ListCollectionView(new List<int>());
            Assert.Equal(true, ec.Convert((object)ic, type, null, culture));
            
            ic = new ListCollectionView( new List<int>() { 0, 1, 2, 3, 4, 5 });
            Assert.Equal(false, ec.Convert((object)ic, type, null, culture));
        }

        [Fact]
        public void ConvertICollection()
        {
            EmptyConverter ec = new EmptyConverter();
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;

            ArrayList al = new ArrayList();
            Assert.Equal(true, ec.Convert((object)al, type, null, culture));

            al.Add(1);
            Assert.Equal(false, ec.Convert((object)al, type, null, culture));
        }

        [Fact]
        public void ConvertString()
        {
            EmptyConverter ec = new EmptyConverter();
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;

            Assert.Equal(true, ec.Convert((object)"", type, null, culture));
            Assert.Equal(false, ec.Convert((object)"foo", type, null, culture));
        }

        [Fact]
        public void ConvertBack()
        {
            EmptyConverter ec = new EmptyConverter();
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;
            var exc = Assert.Throws<System.NotImplementedException>(
                () => ec.ConvertBack((object)0, type, null, culture));
        }
    }
}
