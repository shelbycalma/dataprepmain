﻿using System;
using System.Collections.Generic;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class AndConverterTest
    {
        [Fact]
        public void ConvertSingleFlag()
        {
            AndConverter ac = new AndConverter();
            List<object> values = new List<object>();
            values.Add(true);
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture = 
                System.Globalization.CultureInfo.InvariantCulture;

            Assert.Equal(true, ac.Convert(values.ToArray(), type, null, culture));

            values.Add(true);
            Assert.Equal(true, ac.Convert(values.ToArray(), type, null, culture));

            values.Add(false);
            Assert.Equal(false, ac.Convert(values.ToArray(), type, null, culture));
        }

        [Fact]
        public void ConvertBack()
        {
            AndConverter ac = new AndConverter();
            List<object> values = new List<object>();
            values.Add(true);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;
            var exc = Assert.Throws<System.NotImplementedException>(
                () => ac.ConvertBack(
                    values.ToArray(), new List<Type>().ToArray(), null, culture));
        }
    }
}
