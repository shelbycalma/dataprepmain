﻿using System;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class BoolInverterConverterTest
    {
        [Fact]
        public void Convert()
        {
            BoolInverterConverter bic = new BoolInverterConverter();
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;

            object value = true;
            Assert.Equal(false, bic.Convert(value, type, null, culture));

            value = false;
            Assert.Equal(true, bic.Convert(value, type, null, culture));
        }

        [Fact]
        public void ConvertBack()
        {
            BoolInverterConverter bic = new BoolInverterConverter();
            Type type = typeof(bool);
            System.Globalization.CultureInfo culture =
                System.Globalization.CultureInfo.InvariantCulture;

            object value = true;
            Assert.Equal(false, bic.ConvertBack(value, type, null, culture));

            value = false;
            Assert.Equal(true, bic.ConvertBack(value, type, null, culture));
        }
    }
}
