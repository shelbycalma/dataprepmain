﻿using Datawatch.DataPrep.Data.Core.Licensing;
using Xunit;

namespace Datawatch.DataPrep.Data
{
    class Setup
    {
        static Setup()
        {
            DatawatchXmlLicenseProvider.CacheLicense = true;
        }

        public static void Init() { }
    }
}
