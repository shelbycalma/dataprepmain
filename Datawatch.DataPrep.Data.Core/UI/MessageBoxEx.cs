﻿using System;
using System.Reflection;
using System.Windows;
using System.Windows.Interop;

namespace Datawatch.DataPrep.Data.Core.UI
{
    using ShowCoreFunc = Func<IntPtr, string, string,
            MessageBoxButton, MessageBoxImage, MessageBoxResult,
            MessageBoxOptions, MessageBoxResult>;

    public static class MessageBoxEx
    {
        private static readonly ShowCoreFunc showCoreDelegate;

        static MessageBoxEx()
        {
            MethodInfo showCoreMethod = typeof (MessageBox).GetMethod("ShowCore",
                BindingFlags.Static | BindingFlags.NonPublic);
            showCoreDelegate = (ShowCoreFunc)Delegate
                .CreateDelegate(typeof(ShowCoreFunc), showCoreMethod);
        }

        public static MessageBoxResult Show(
            string messageBoxText)
        {
            return ShowCore(IntPtr.Zero, messageBoxText, String.Empty, MessageBoxButton.OK, MessageBoxImage.None, 0, 0);
        }

        public static MessageBoxResult Show(
            string messageBoxText,
            string caption)
        {
            return ShowCore(IntPtr.Zero, messageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.None, 0, 0);
        }

        public static MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button)
        {
            return ShowCore(IntPtr.Zero, messageBoxText, caption, button, MessageBoxImage.None, 0, 0);
        }

        public static MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon)
        {
            return ShowCore(IntPtr.Zero, messageBoxText, caption, button, icon, 0, 0);
        }

        public static MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult)
        {
            return ShowCore(IntPtr.Zero, messageBoxText, caption, button, icon, defaultResult, 0);
        }

        public static MessageBoxResult Show(
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options)
        {
            return ShowCore(IntPtr.Zero, messageBoxText, caption, button, icon, defaultResult, options);
        }

        public static MessageBoxResult Show(
            Window window,
            string messageBoxText)
        {
            IntPtr owner = GetHandlerOrMainWindowHandler(window);
            return ShowCore(owner, messageBoxText, String.Empty, MessageBoxButton.OK, MessageBoxImage.None, 0, 0);
        }

        public static MessageBoxResult Show(
            Window window,
            string messageBoxText,
            string caption)
        {
            IntPtr owner = GetHandlerOrMainWindowHandler(window);
            return ShowCore(owner, messageBoxText, caption, MessageBoxButton.OK, MessageBoxImage.None, 0, 0);
        }

        public static MessageBoxResult Show(
            Window window,
            string messageBoxText,
            string caption,
            MessageBoxButton button)
        {
            IntPtr owner = GetHandlerOrMainWindowHandler(window);
            return ShowCore(owner, messageBoxText, caption, button, MessageBoxImage.None, 0, 0);
        }

        public static MessageBoxResult Show(
            Window window,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon)
        {
            IntPtr owner = GetHandlerOrMainWindowHandler(window);
            return ShowCore(owner, messageBoxText, caption, button, icon, 0, 0);
        }

        public static MessageBoxResult Show(
            Window window,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult)
        {
            IntPtr owner = GetHandlerOrMainWindowHandler(window);
            return ShowCore(owner, messageBoxText, caption, button, icon, defaultResult, 0);
        }

        public static MessageBoxResult Show(
            Window window,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options)
        {
            IntPtr owner = GetHandlerOrMainWindowHandler(window);
            return ShowCore(owner, messageBoxText, caption, button, icon, defaultResult, options);
        }

        private static MessageBoxResult ShowCore(
            IntPtr owner,
            string messageBoxText,
            string caption,
            MessageBoxButton button,
            MessageBoxImage icon,
            MessageBoxResult defaultResult,
            MessageBoxOptions options)
        {
            return showCoreDelegate(owner, messageBoxText, caption, button, icon, defaultResult, options);
        }

        private static IntPtr GetHandlerOrMainWindowHandler(Window window)
        {
            if (window != null)
            {
                WindowInteropHelper windowInteropHelper =
                    new WindowInteropHelper(window);
                return windowInteropHelper.Handle;
            }
            return Utils.MainWindowHandler;
        }
    }
}