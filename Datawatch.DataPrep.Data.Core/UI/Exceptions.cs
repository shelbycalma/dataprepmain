﻿using System;
using Datawatch.DataPrep.Data.Core.UI.Properties;

namespace Datawatch.DataPrep.Data.Core.UI
{
    public class Exceptions : Core.Exceptions
    {
        public static Exception MediatorRegisterDelegateBadArgumentCount(
            string parameter)
        {
            return CreateArgument(parameter, Format(
                Resources.ExMediatorRegisterDelegateBadArgumentCount));
        }

        public static Exception MediatorRegisterNoStaticDelegates(
            string parameter)
        {
            return CreateArgument(parameter, Format(
                Resources.ExMediatorRegisterNoStaticDelegate));
        }

        public static Exception MediatorRegisterSinkBadArgumentCount(
            string target, string method, int arguments)
        {
            return CreateInvalidOperation(Format(
                Resources.ExMediatorRegisterSinkBadArgumentCount,
                target, method, arguments));
        }
    }
}
