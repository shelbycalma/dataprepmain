﻿using System;
using System.Linq;
using System.Reflection;

namespace Datawatch.DataPrep.Data.Core.UI.Mediator
{
	/// <summary>
	/// Provides loosely-coupled messaging between
	/// various colleagues.  All references to objects
	/// are stored weakly, to prevent memory leaks.
	/// </summary>
	public sealed class Mediator
	{
        private static readonly Mediator instance = new Mediator();

		readonly MessageToActionsMap invocationList = new MessageToActionsMap();

        public static Mediator Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Register a ViewModel to the mediator notifications
        /// This will iterate through all methods of the target passed and will 
        /// register all methods that are decorated with the MediatorMessageSink 
        /// Attribute
        /// </summary>
        /// <param name="target">The ViewModel instance to register</param>
		public void Register(object target)
		{
            if (target == null)
            {
                throw Exceptions.ArgumentNull("target");
            }

			//Inspect the attributes on all methods and check if there are RegisterMediatorMessageAttribute
            foreach (var methodInfo in target.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
            {
                foreach (MediatorMessageSinkAttribute attribute in methodInfo.GetCustomAttributes(typeof(MediatorMessageSinkAttribute), true))
                {
                    if (methodInfo.GetParameters().Length == 1)
                    {
                        invocationList.AddAction(attribute.Message, target, 
                            methodInfo, attribute.ParameterType);
                    }
                    else
                    {                        
                        throw Exceptions.MediatorRegisterSinkBadArgumentCount(
                            target.GetType().Name, methodInfo.Name,
                            methodInfo.GetParameters().Length);
                    }
                }
            }
		}

        /// <summary>
        /// Checks the invocation list to determine if the instance of a 
        /// viewModel already exists within it
        /// </summary>
        /// <param name="target">The viewmodel to check</param>
        /// <returns></returns>
	    public bool IsRegistered(object target)
	    {
	        if (target == null)
	        {
	            throw Datawatch.DataPrep.Data.Framework.Exceptions.ArgumentNull("target");
	        }

            foreach (
                var methodInfo in
                    target.GetType()
                        .GetMethods(BindingFlags.NonPublic | BindingFlags.Public |
                                    BindingFlags.Instance))
            {
                var attributes = methodInfo.GetCustomAttributes(
                    typeof (MediatorMessageSinkAttribute), true);

                if (!attributes.Any()) continue;

                foreach (MediatorMessageSinkAttribute attribute in attributes)
                {
                    var actions = invocationList.GetActions(attribute.Message);
                    if (actions == null || !actions.Any())
                    {
                        continue;
                    }

                    if (actions.Any(a => a.Target.Equals(target)))
                    {
                        return true;
                    }
                }
            }

            return false;
	    }

	    public void Unregister(object target)
        {
            invocationList.RemoveTarget(target);
        }

        /// <summary>
        /// Registers a specific method to the Mediator notifications
        /// </summary>
        /// <param name="message">The message to register to</param>
        /// <param name="callback">The callback function to be called when this 
        /// message is broadcasted</param>
		public void Register(string message, Delegate callback)
		{
            //This method is not supported in Silverlight
#if SILVERLIGHT
            throw new InvalidOperationException("This method is not supported in Silverlight");
#endif

            if (callback.Target == null)
            {
                throw Exceptions.MediatorRegisterNoStaticDelegates("callback");
            }

			ParameterInfo[] parameters = callback.Method.GetParameters();

			// JAS - Changed this logic to allow for 0 or 1 parameter.
            if (parameters != null && parameters.Length > 1)
            {
                throw Exceptions.MediatorRegisterDelegateBadArgumentCount(
                    "callback");
            }

			// JAS - Pass in the parameter type.
			Type parameterType = 
                (parameters == null || parameters.Length == 0) ? null : 
                    parameters[0].ParameterType;
			
            invocationList.AddAction(message, callback.Target, 
                callback.Method, parameterType);
		}

        /// <summary>
        /// Notify all registered parties that a specific message was broadcasted
        /// </summary>
        /// <typeparam name="T">The Type of parameter to be passed</typeparam>
        /// <param name="message">The message to broadcast</param>
        /// <param name="parameter">The parameter to pass together with the 
        /// message</param>
		public void NotifyColleagues<T>(string message, T parameter)
		{
			var actions = invocationList.GetActions(message);

            if (actions != null)
            {
                actions.ForEach(action => action.DynamicInvoke(parameter));
            }
		}

		/// <summary>
        /// Notify all registered parties that a specific message was broadcasted
        /// </summary>
        /// <typeparam name="T">The Type of parameter to be passed</typeparam>
        /// <param name="message">The message to broadcast</param>
        public void NotifyColleagues<T>(string message)
		{
			var actions = invocationList.GetActions(message);

			if (actions != null)
				actions.ForEach(action => action.DynamicInvoke());
		}
	}
}