﻿using System.Windows.Controls;

namespace Datawatch.DataPrep.Data.Core.UI.Plugin
{
    /// <summary>
    /// Interaction logic for QueryBuilderTableControl.xaml
    /// </summary>
    public partial class QueryBuilderTableControl : UserControl
    {
        public QueryBuilderTableControl()
        {
            InitializeComponent();
        }

        private void tablesListBox_SelectionChanged(object sender,
            SelectionChangedEventArgs e)
        {
            ListBox selector = sender as ListBox;
            if (selector == null) return;
            selector.ScrollIntoView(selector.SelectedItem);
        }
    }
}
