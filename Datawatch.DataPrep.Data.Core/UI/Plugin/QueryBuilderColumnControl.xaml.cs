﻿using System.Collections.Specialized;
using System.Data.SqlTypes;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.UI.Plugin
{
    /// <summary>
    /// Interaction logic for QueryBuilderColumnControl.xaml
    /// </summary>
    /// 
    
    public partial class QueryBuilderColumnControl : UserControl
    {
        private bool IsSelectedAllUnchecked = true;
        public QueryBuilderColumnControl()
        {
            InitializeComponent();
            ((INotifyCollectionChanged)ColumnsControl.Items).CollectionChanged +=
                CommonQueryPanel_CollectionChanged;
        }

        private void CommonQueryPanel_CollectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            ScrollViewer sv = (ScrollViewer) ColumnsControl.Template.
                FindName("ColumnsScrollBar", ColumnsControl);
            if (sv == null)
            {
                return;
            }
            sv.ScrollToTop();
			
            //reset SelecttAll checkbox
            if (e.Action == NotifyCollectionChangedAction.Reset)
            {
				bool isEdit = false;
				foreach (DatabaseColumn d in ColumnsControl.ItemsSource)
				{
					if (d.Selected)
					{
						isEdit = true;
						break;
					}
				}
				if (!isEdit)
				{
					bool hasColumns = false;
					foreach (DatabaseColumn d in ColumnsControl.ItemsSource)
					{
						d.Selected = true;
						hasColumns = true;
					}
					if (hasColumns)
					{
						ControlTemplate template = ColumnsControl.Template;
						CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
						c.IsChecked = true;
					}
				}
			}

			if (e.Action == NotifyCollectionChangedAction.Add)
            {
                ControlTemplate template = ColumnsControl.Template;
                CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
                IsSelectedAllUnchecked = false;
                c.IsChecked = false;
                IsSelectedAllUnchecked = true;
            }
        }

        private void Select_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            bool isCheckedAll = false;
            foreach (DatabaseColumn c in ColumnsControl.ItemsSource)
            {
                if (c.Selected)
                {
                    isCheckedAll = true;
                }
                else
                {
                    isCheckedAll = false;
                    break;
                }
            }
            if (isCheckedAll)
            {
                ControlTemplate template = ColumnsControl.Template;
                CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
                c.IsChecked = true;
            }
        }

        private void Select_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            ControlTemplate template = ColumnsControl.Template;
            CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;

            IsSelectedAllUnchecked = false;
            c.IsChecked = false;
            IsSelectedAllUnchecked = true;
        }

        private void SelectAll_Checked(object sender, System.Windows.RoutedEventArgs e)
        {
            foreach (DatabaseColumn c in ColumnsControl.ItemsSource)
            {
                c.Selected = true;
            }
        }

        private void SelectAll_Unchecked(object sender, System.Windows.RoutedEventArgs e)
        {
            if (IsSelectedAllUnchecked)
            {
                foreach (DatabaseColumn c in ColumnsControl.ItemsSource)
                {
                    c.Selected = false;
                }
            }
        }
    }
}
