﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interop;
using System.Windows.Media;

namespace Datawatch.DataPrep.Data.Core.UI
{
    public class Utils
    {
        public static T FindNameFromHeaderTemplate<T>(TabItem tabItem, String name) where T : UIElement
        {
            if (tabItem == null)
            {
                throw Exceptions.ArgumentNull("tabItem");
            }
            if (tabItem.HeaderTemplate == null)
            {
                return null;
            }

            ContentPresenter contentPresenter =
                FindVisualChild<ContentPresenter>(tabItem);

            if (contentPresenter == null)
            {
                return null;
            }
            T element =
                tabItem.HeaderTemplate.FindName(name, contentPresenter) as T;

            return element;
        }

        public static T FindVisualChild<T>(DependencyObject obj)
            where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is T)
                    return (T)child;
                else
                {
                    T childOfChild = FindVisualChild<T>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject obj)
            where T : DependencyObject
        {
            if (obj == null) yield break;
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child is T)
                {
                    yield return (T)child;
                }

                foreach (T childOfChild in FindVisualChildren<T>(child))
                {
                    yield return childOfChild;
                }
            }
        }

        public static T GetFrameworkElementByName<T>(FrameworkElement referenceElement, String name) where T : FrameworkElement
        {
            FrameworkElement child = null;

            for (Int32 i = 0; i < VisualTreeHelper.GetChildrenCount(referenceElement); i++)
            {
                child = VisualTreeHelper.GetChild(referenceElement, i) as FrameworkElement;

                if (child != null && child.Name == name && child.GetType() == typeof(T))
                {
                    break;
                }
                else if (child != null)
                {
                    child = GetFrameworkElementByName<T>(child, name);
                    if (child != null)
                    {
                        return child as T;
                    }
                }
            }
            return child as T;
        }

        public static object GetBoundItemFromPoint(TreeView treeView, Point point)
        {
            DependencyObject element = treeView.InputHitTest(point) as DependencyObject;
            while (element != null && !(element is TreeViewItem)) 
            {
                element = VisualTreeHelper.GetParent(element);
            }
            if (element != null)
            {
                TreeViewItem treeViewItem = (TreeViewItem)element;
                return treeViewItem.DataContext;
            }
            return null;
        }

        public static object GetBoundItemFromPoint(ItemsControl box, Point point)
        {
            UIElement element = box.InputHitTest(point) as UIElement;
            while (element != null)
            {
                if (element == box)
                {
                    return null;
                }
                object item = box.ItemContainerGenerator.ItemFromContainer(element);
                bool itemFound = !object.ReferenceEquals(item, DependencyProperty.UnsetValue);
                if (itemFound)
                {
                    return item;
                }
                else
                {
                    element = VisualTreeHelper.GetParent(element) as UIElement;
                }
            }
            return null;
        }

        public static IntPtr MainWindowHandler { get; set; }

        public static UIElement TopElement { get; set; }

        public static Window GetOwnerWindow(Control control)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(control);

            while (parent != null)
            {
                if (parent is Window)
                {
                    Window window = (Window) parent;
                    if (window.IsVisible)
                    {
                        return (Window) parent;
                    }
                }
                parent = VisualTreeHelper.GetParent(parent);
            }
            int lastWindow = Application.Current.Windows.Count - 1;
            while (lastWindow >= 0)
            {
                Window window = Application.Current.Windows[lastWindow];
                if (window == null || !window.IsVisible)
                {
                    lastWindow--;
                }
                else
                {
                    return window;
                }
            }
            return null;
        }

        public static void SetOwnerWindow(Window window, Window owner, Control control)
        {
            if (window.Owner != null)
                return;

            if (owner != null)
            {
                window.Owner = owner;
            }
            else if (control != null)
            {
                window.Owner = GetOwnerWindow(control);
            }
            else if (MainWindowHandler != IntPtr.Zero)
            {
                WindowInteropHelper windowInteropHelper =
                    new WindowInteropHelper(window);
                windowInteropHelper.Owner = MainWindowHandler;
            }
        }

        public static Popup FindPopupParent(DependencyObject control)
        {
            if (control == null) return null;

            do
            {
                DependencyObject parent = LogicalTreeHelper.GetParent(control);
                if (parent == null)
                {
                    parent = VisualTreeHelper.GetParent(control);
                }
                if (parent is Popup)
                {
                    return (Popup) parent;
                }
                control = parent;
            } while (control != null);

            return null;
        }

        public static T FindParent<T>(DependencyObject depObj) where T : DependencyObject
        {
            while (depObj != null)
            {
                // If the current object is a FrameworkElement which is a child of the
                // Canvas, exit the loop and return it.

                if (depObj is T)
                {
                    return (T)depObj;
                }

                //FrameworkElement elem = depObj as FrameworkElement;
                //if (elem != null && partCanvas.Children.Contains(elem))
                //    break;

                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is Visual)
                {
                    depObj = VisualTreeHelper.GetParent(depObj);
                }
                else
                {
                    depObj = LogicalTreeHelper.GetParent(depObj);
                }
            }
            return null;
        }
        
        public static bool IntersectRectsWithTolerance(
            Rect rect1, Rect rect2, int tolerance = 20)
        {
            if (rect1.IsEmpty || rect2.IsEmpty)
            {
                return false;
            }

            double right = rect1.Right - tolerance;
            double left = rect1.Left + tolerance;
            double bottom = rect1.Bottom - tolerance;
            double top = rect1.Top + tolerance;

            return (rect2.Left <= right) &&
                   (rect2.Right >= left) &&
                   (rect2.Top <= bottom) &&
                   (rect2.Bottom >= top);
        }

        public static TreeViewItem FindTreeViewItem(ItemsControl control, object source)
        {
            TreeViewItem treeViewItem = control.ItemContainerGenerator.ContainerFromItem(source) as TreeViewItem;
            if (treeViewItem != null)
            {
                return treeViewItem;
            }

            for (int i = 0; i < control.Items.Count; i++)
            {
                treeViewItem = control.ItemContainerGenerator.ContainerFromIndex(i) as TreeViewItem;

                if (treeViewItem == null) continue;

                TreeViewItem innerTreeViewItem = FindTreeViewItem(treeViewItem, source);
                if (innerTreeViewItem != null)
                {
                    return innerTreeViewItem;
                }
            }

            return null;
        }
    }
}
