using System.Windows;

namespace Datawatch.DataPrep.Data.Core.UI.DragDrop
{
    public interface IDropTargetAdvisor
    {
		UIElement TargetUI
		{
			get;
			set;
		}
		bool IsValidDataObject(IDataObject obj);
		void OnDropCompleted(IDataObject obj, Point dropPoint);
		UIElement GetVisualFeedback(IDataObject obj);
        Point GetOffsetPoint(IDataObject obj);
        Point AdjustDropPoint(IDataObject obj, Point p);
	}
}
