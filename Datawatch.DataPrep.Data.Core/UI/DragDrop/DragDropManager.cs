using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace Datawatch.DataPrep.Data.Core.UI.DragDrop
{
	public static class DragDropManager
	{
        private static readonly List<UIElement> dropTargets = new List<UIElement>();
        
        private static UIElement draggedElement;
		private static Point dragStartPoint;
        private static Point offsetPoint;
		private static DropPreviewAdorner overlayElement;
        private static AdornerLayer dropTargetAdornerLayer;
	    private static bool isDragging;

	    public static IDropTargetAdvisor AdvisorOnDrop;
		#region Dependency Properties
		public static readonly DependencyProperty DragSourceAdvisorProperty =
			DependencyProperty.RegisterAttached("DragSourceAdvisor", 
            typeof(IDragSourceAdvisor), typeof(DragDropManager),
			new FrameworkPropertyMetadata(
                new PropertyChangedCallback(OnDragSourceAdvisorChanged)));

		public static readonly DependencyProperty DropTargetAdvisorProperty =
			DependencyProperty.RegisterAttached("DropTargetAdvisor", 
            typeof(IDropTargetAdvisor), typeof(DragDropManager),
			new FrameworkPropertyMetadata(
                new PropertyChangedCallback(OnDropTargetAdvisorChanged)));

		public static void SetDragSourceAdvisor(DependencyObject depObj, 
            IDragSourceAdvisor dragSourceAdvisor)
		{
            depObj.SetValue(DragSourceAdvisorProperty, dragSourceAdvisor);
		}

		public static void SetDropTargetAdvisor(DependencyObject depObj, 
            IDropTargetAdvisor dropTargetAdvisor)
		{
			depObj.SetValue(DropTargetAdvisorProperty, dropTargetAdvisor);
		}

		public static IDragSourceAdvisor GetDragSourceAdvisor(DependencyObject depObj)
		{
			return depObj.GetValue(DragSourceAdvisorProperty) as IDragSourceAdvisor;
		}

		public static IDropTargetAdvisor GetDropTargetAdvisor(DependencyObject depObj)
		{
			return depObj.GetValue(DropTargetAdvisorProperty) as IDropTargetAdvisor;
		}

		#endregion

		#region Property Change handlers
		private static void OnDragSourceAdvisorChanged(DependencyObject depObj, 
            DependencyPropertyChangedEventArgs args)
		{
			UIElement sourceElt = depObj as UIElement;
            
            if (sourceElt == null) return;

			if (args.NewValue != null && args.OldValue == null)
			{
				sourceElt.PreviewMouseLeftButtonDown += 
                    DragSource_PreviewMouseLeftButtonDown;
				sourceElt.PreviewMouseMove += 
                    DragSource_PreviewMouseMove;
				sourceElt.PreviewMouseUp += 
                    DragSource_PreviewMouseUp;
			}
			else if (args.NewValue == null && args.OldValue != null)
			{
				sourceElt.PreviewMouseLeftButtonDown -= 
                    DragSource_PreviewMouseLeftButtonDown;
				sourceElt.PreviewMouseMove -= 
                    DragSource_PreviewMouseMove;
				sourceElt.PreviewMouseUp -= 
                    DragSource_PreviewMouseUp;
			}
            // Set the Drag source UI
            IDragSourceAdvisor advisor = args.NewValue as IDragSourceAdvisor;
            if (advisor != null)
            {
                advisor.SourceUI = sourceElt;
            }
		}

        private static void AttachDropTarget(UIElement targetElt)
        {
            targetElt.DragEnter +=
                DropTarget_DragEnter;
            targetElt.DragOver +=
                DropTarget_DragOver;
            targetElt.DragLeave +=
                DropTarget_DragLeave;
            targetElt.Drop +=
                DropTarget_Drop;
            targetElt.AllowDrop = true;
        }

        private static void DetachDropTarget(UIElement targetElt)
        {
            targetElt.DragEnter -= DropTarget_DragEnter;
            targetElt.DragOver -= DropTarget_DragOver;
            targetElt.DragLeave -= DropTarget_DragLeave;
            targetElt.Drop -= DropTarget_Drop;
            targetElt.AllowDrop = false;
        }

		private static void OnDropTargetAdvisorChanged(DependencyObject depObj, 
            DependencyPropertyChangedEventArgs args)
		{
			UIElement targetElt = depObj as UIElement;
			if (args.NewValue != null && args.OldValue == null)
			{
                if (!dropTargets.Contains(targetElt))
                {
                    AttachDropTarget(targetElt);
                    dropTargets.Add(targetElt);
                }
			}
			else if (args.NewValue == null && args.OldValue != null)
			{
                DetachDropTarget(targetElt);
                
                dropTargets.Remove(targetElt);
            }
            // Set the Drag source UI
            IDropTargetAdvisor advisor = args.NewValue as IDropTargetAdvisor;
            if (advisor != null)
            {
                advisor.TargetUI = targetElt;
            }

		}
		#endregion

		/* ____________________________________________________________________
		 *		Drop Target events 
		 * ____________________________________________________________________
		 */
		static void DropTarget_Drop(object sender, DragEventArgs e)
		{
			if (UpdateEffects(sender, e) == false) return;
            if (!(sender is UIElement)) return;

			IDropTargetAdvisor advisor = GetDropTargetAdvisor(sender as DependencyObject);
			Point dropPoint = e.GetPosition(sender as UIElement);
            // Calculate displacement for (Left, Top)
            Point adjustedPoint = 
                new Point
                    {
                        X = dropPoint.X - offsetPoint.X, 
                        Y = dropPoint.Y - offsetPoint.Y
                    };

		    adjustedPoint = advisor.AdjustDropPoint(e.Data, adjustedPoint);
			advisor.OnDropCompleted(e.Data, adjustedPoint);
		    e.Handled = true;

			RemovePreviewAdorner();
            offsetPoint = new Point(0, 0);
		}

		static void DropTarget_DragLeave(object sender, DragEventArgs e)
		{
			if (UpdateEffects(sender, e) == false) return;

            IDropTargetAdvisor advisor = GetDropTargetAdvisor(sender as DependencyObject);
            Point mousePoint = MouseUtilities.GetMousePosition(advisor.TargetUI);

            //giving a tolerance of 2 so that the adorner is removed when the mouse is moved fast.
            //this might still be small...in that case increase the tolerance
            if ((mousePoint.X < 4) || (mousePoint.Y < 4) ||
                (mousePoint.X > ((FrameworkElement)(advisor.TargetUI)).ActualWidth - 4) ||
                (mousePoint.Y > ((FrameworkElement)(advisor.TargetUI)).ActualHeight - 4))
            {
                RemovePreviewAdorner();
            }
			e.Handled = true;
		}

		static void DropTarget_DragOver(object sender, DragEventArgs e)
		{
			if (UpdateEffects(sender, e) == false) return;
			// Update position of the preview Adorner
            IDropTargetAdvisor advisor = GetDropTargetAdvisor(sender as DependencyObject);
            if (advisor != null)
            {
                Point position = e.GetPosition(sender as UIElement);

                Point previewPoint = advisor.AdjustDropPoint(e.Data,
                    new Point(position.X - offsetPoint.X,
                        position.Y - offsetPoint.Y));

                // When running inside the WinForms ElementHost, overlayElement will be null.
                if (overlayElement != null)
                {
                    overlayElement.Left = previewPoint.X;
                    overlayElement.Top = previewPoint.Y;
                }

                e.Handled = true;
            }
		}

		static void DropTarget_DragEnter(object sender, DragEventArgs e)
		{
			if (UpdateEffects(sender, e) == false) return;
            
			// Setup the preview Adorner
			UIElement feedbackUI = 
                GetDropTargetAdvisor(sender as DependencyObject).GetVisualFeedback(
                    e.Data);
            offsetPoint = 
                GetDropTargetAdvisor(sender as DependencyObject).GetOffsetPoint(
                    e.Data);

            IDropTargetAdvisor advisor = 
                GetDropTargetAdvisor(sender as DependencyObject);

            Point mousePoint = MouseUtilities.GetMousePosition(advisor.TargetUI);
            //Point mousePoint = e.GetPosition(advisor.TargetUI);

            //giving a tolerance of 2 so that the adorner is created when the mouse is moved fast.
            //this might still be small...in that case increase the tolerance
            if ((mousePoint.X < 4) || (mousePoint.Y < 4) ||
                (mousePoint.X > ((FrameworkElement)(advisor.TargetUI)).ActualWidth - 4) ||
                (mousePoint.Y > ((FrameworkElement)(advisor.TargetUI)).ActualHeight - 4) ||
                 (overlayElement == null))
            {
                CreatePreviewAdorner(sender as UIElement, feedbackUI);
            }

            e.Handled = true;
		}

		static bool UpdateEffects(object uiObject, DragEventArgs e)
		{
			IDropTargetAdvisor advisor = 
                GetDropTargetAdvisor(uiObject as DependencyObject);
            AdvisorOnDrop = advisor;
			if (advisor.IsValidDataObject(e.Data) == false) return false;

			if ((e.AllowedEffects & DragDropEffects.Move) == 0 &&
				(e.AllowedEffects & DragDropEffects.Copy) == 0)
			{
				e.Effects = DragDropEffects.None;
				return true;
			}

			if ((e.AllowedEffects & DragDropEffects.Move) != 0 &&
				(e.AllowedEffects & DragDropEffects.Copy) != 0)
			{
				e.Effects = ((e.KeyStates & DragDropKeyStates.ControlKey) != 0) ?
					DragDropEffects.Copy : DragDropEffects.Move;
			}

			return true;
		}

		/* ____________________________________________________________________
		 *		Drag Source events 
		 * ____________________________________________________________________
		 */
		static void DragSource_PreviewMouseLeftButtonDown(object sender, 
            MouseButtonEventArgs e)
		{
            // Make this the new drag source
            IDragSourceAdvisor advisor = GetDragSourceAdvisor(sender as DependencyObject);

            if (advisor.IsDraggable(e.Source as UIElement,
                    e.OriginalSource as UIElement) == false) return;

            draggedElement = e.Source as UIElement;

		    UIElement topContainer = GetTopContainer(draggedElement);
            
            dragStartPoint = e.GetPosition(topContainer);
            offsetPoint = e.GetPosition(draggedElement);
            dragStartPoint = e.GetPosition(topContainer);
		}

		static void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
		{
            IDragSourceAdvisor advisor = GetDragSourceAdvisor(sender as DependencyObject);

            if (e.Source != draggedElement)
            {
                return;
            }

            if (advisor.IsDraggable(e.Source as UIElement,
                e.OriginalSource as UIElement) == false) return;

            if (!isDragging && 
                Mouse.LeftButton == MouseButtonState.Pressed && 
                IsDragGesture(e.GetPosition(GetTopContainer(draggedElement))))
            {
                DragStarted(sender as UIElement);
            }
		}

        static void DragSource_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            draggedElement = null;
        }

		static void DragStarted(UIElement uiElt)
		{
            isDragging = true;            
			IDragSourceAdvisor advisor = GetDragSourceAdvisor(uiElt);
			DataObject data = advisor.GetDataObject(draggedElement, offsetPoint);

            if (data == null)
            {
                isDragging = false;
                return;
            }

			DragDropEffects supportedEffects = advisor.SupportedEffects;

            DragDropEffects effects = DoDragDrop(draggedElement, 
                data, supportedEffects);

            advisor.FinishDrag(draggedElement, effects);

            Popup popup = Utils.FindPopupParent(uiElt);
            
            if (popup != null && !popup.StaysOpen)
            {
                popup.StaysOpen = true;
                popup.StaysOpen = false;
            }

            draggedElement = null;
		    isDragging = false;
		}

        public static DragDropEffects DoDragDrop(DependencyObject dragSource, 
            DataObject data, DragDropEffects supportedEffects) 
        {
            if (dragSource == null) throw Exceptions.ArgumentNull("dragSource");
            if (data == null) throw Exceptions.ArgumentNull("data");

            UIElement topContainer = GetTopContainer(draggedElement);
            AdornerLayer parentAdorner = null;
            DropTargetAdorner dropTargetAdorner = null;
            if (topContainer != null)
            {
                parentAdorner =
                    AdornerLayer.GetAdornerLayer(topContainer);
                dropTargetAdorner =
                    new DropTargetAdorner(topContainer, dropTargets, data);
                parentAdorner.Add(dropTargetAdorner);                
            }

            try
            {
                // Perform DragDrop
                return System.Windows.DragDrop.DoDragDrop(dragSource, data, 
                        supportedEffects);
            }
            finally
            {
                // Clean up
                if (parentAdorner != null)
                {
                    parentAdorner.Remove(dropTargetAdorner);
                    RemovePreviewAdorner();
                }
            }
		}

		static bool IsDragGesture(Point point)
		{
			bool hGesture = Math.Abs(point.X - dragStartPoint.X) > 
                SystemParameters.MinimumHorizontalDragDistance;
			bool vGesture = Math.Abs(point.Y - dragStartPoint.Y) > 
                SystemParameters.MinimumVerticalDragDistance;

			return (hGesture | vGesture);
		}

		/* ____________________________________________________________________
		 *		Utility functions
		 * ____________________________________________________________________
		 */
		static UIElement GetTopContainer(UIElement element)
		{
            // TODO: Replace with UI.Utils.GetOwnerWindow(element) ?
            if (element != null)
            {
                DependencyObject parent = VisualTreeHelper.GetParent(element);

                while (parent != null)
                {
                    if (parent is Window)
                    {
                        return ((Window) parent).Content as UIElement;
                    }
                    parent = VisualTreeHelper.GetParent(parent);
                }
            }
            if (Utils.TopElement != null)
                return Utils.TopElement;
            if (Application.Current.MainWindow != null)
    		    return Application.Current.MainWindow.Content as UIElement;
            return null;
		}

		private static void CreatePreviewAdorner(UIElement adornedElt, 
            UIElement feedbackUI)
		{
			// Clear if there is an existing preview adorner
			RemovePreviewAdorner();

            Popup popup = Utils.FindPopupParent(adornedElt);
		    Visual parent = popup != null ? 
                (Visual) VisualTreeHelper.GetParent(adornedElt) : 
                GetTopContainer(adornedElt);
            if (parent == null) return;
			dropTargetAdornerLayer = AdornerLayer.GetAdornerLayer(parent);
			overlayElement = new DropPreviewAdorner(feedbackUI, adornedElt);
            dropTargetAdornerLayer.Add(overlayElement);
		}

		private static void RemovePreviewAdorner()
		{
			if (overlayElement != null)
			{
                dropTargetAdornerLayer.Remove(overlayElement);
                if (overlayElement != null)
                {
                    overlayElement.Dispose();
                }
				overlayElement = null;
			}
		}

        public class MouseUtilities
        {
            [StructLayout(LayoutKind.Sequential)]
            private struct Win32Point
            {
                public Int32 X;
                public Int32 Y;
            };

            [DllImport("user32.dll")]
            private static extern bool GetCursorPos(ref Win32Point pt);

            [DllImport("user32.dll")]
            private static extern bool ScreenToClient(IntPtr hwnd, 
                ref Win32Point pt);

            public static Point GetMousePosition(Visual relativeTo)
            {
                Win32Point mouse = new Win32Point();
                GetCursorPos(ref mouse);

                System.Windows.Interop.HwndSource presentationSource =
                    (System.Windows.Interop.HwndSource)PresentationSource.
                        FromVisual(relativeTo);

                if (presentationSource == null) return new Point(0, 0);

                ScreenToClient(presentationSource.Handle, ref mouse);

                GeneralTransform transform = relativeTo.TransformToAncestor(
                    presentationSource.RootVisual);

                Point offset = transform.Transform(new Point(0, 0));

                return new Point(mouse.X - offset.X, mouse.Y - offset.Y);
            }
        };

	}
}
