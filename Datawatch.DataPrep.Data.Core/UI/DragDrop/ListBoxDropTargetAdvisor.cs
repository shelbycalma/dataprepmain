﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core.UI.Widgets;

namespace Datawatch.DataPrep.Data.Core.UI.DragDrop
{
    public abstract class ListBoxDropTargetAdvisor<T> : 
        IDropTargetAdvisor where T : class
    {
        public UIElement TargetUI
        {
            get; set;
        }

        public bool IsValidDataObject(IDataObject obj)
        {
            return GetObject(obj) != null;
        }

        private ObservableCollection<T> GetCollection()
        {
            ListBox list = (ListBox)TargetUI;

            if (list.ItemsSource is CompositeCollection)
            {
                CompositeCollection itemCollection =
                    list.ItemsSource as CompositeCollection;
                ListCollectionView lcv =
                    (ListCollectionView)((CollectionContainer)
                        itemCollection[1]).Collection;
                return
                    lcv.SourceCollection as
                        ObservableCollection<T>;
            }
            return (ObservableCollection<T>)list.ItemsSource;
        }

        private int GetDropIndex(ListBox list, Point dropPoint)
        {
            int fixedItemCount = GetFixedItemCount();

            for (int i = fixedItemCount; i < list.Items.Count; i++)
            {
                ListBoxItem item = (ListBoxItem)list.ItemContainerGenerator.
                    ContainerFromIndex(i);

                if(item == null) continue;

                Point topLeft = list.PointFromScreen(
                    item.PointToScreen(new Point(0, 0)));

                /* We need to divide actual height because on
                 * how wpf counts position of the element.
                 * And sometimes it happens that element position (previous one)
                 * can overlap drop position and can be picked up as one
                 * that we need. But we need the next one.
                 * NOTE: Possible drawback is if element is really small
                 * it might be not enough divided height to determine
                 * right element. */
                if (dropPoint.Y < (topLeft.Y + item.ActualHeight / 2))
                {
                    return i - fixedItemCount;
                }
            }
            return list.Items.Count - fixedItemCount;
        }

        private int GetFixedItemCount()
        {
            ListBox list = (ListBox)TargetUI;
            ObservableCollection<T> collection = GetCollection();
            if (collection == null) return 0;
            return list.Items.Count - collection.Count;
        }

        private static T GetObject(IDataObject obj)
        {
            string[] formats = obj.GetFormats();

            return formats.Select(format => 
                obj.GetData(format)).OfType<T>().FirstOrDefault();
        }

        public void OnDropCompleted(IDataObject obj, Point dropPoint)
        {
            ListBox list = (ListBox)TargetUI;
            ObservableCollection<T> variableCollection = GetCollection();

            if (variableCollection == null) return;

            T draggedObject = GetObject(obj);
            dropPoint.Y += 5;
            int dragIndex = variableCollection.IndexOf(draggedObject);
            int dropIndex = GetDropIndex(list, dropPoint);
            if (dropIndex > dragIndex)
            {
                dropIndex--;
            }
            variableCollection.Move(dragIndex, 
                Math.Min(dropIndex, variableCollection.Count-1));

            list.SelectedItem = draggedObject;
        }

        public UIElement GetVisualFeedback(IDataObject obj)
        {
            DropMarker dropMarker =
                new DropMarker
                {
                    Orientation = Orientation.Horizontal,
                    Width = ((ListBox)TargetUI).ActualWidth,
                    Height = 10
                };
            return dropMarker;
        }

        public Point GetOffsetPoint(IDataObject obj)
        {
            return new Point();
        }

        public Point AdjustDropPoint(IDataObject obj, Point p)
        {
            ListBox list = TargetUI as ListBox;
            if (list == null) return p;

            int fixedItemCount = GetFixedItemCount();
            int dropIndex = GetDropIndex(list, p);
            int listIndex = Math.Min(list.Items.Count - 1,
                dropIndex + fixedItemCount);

            ListBoxItem item =
                list.ItemContainerGenerator.ContainerFromIndex(listIndex)
                    as ListBoxItem;

            if (item == null) return p;

            p.Y = list.PointFromScreen(item.PointToScreen(new Point(0, 0))).Y;

            if (dropIndex == list.Items.Count - fixedItemCount)
            {
                p.Y += item.ActualHeight;
            }
            p.X = 0;
            p.Y -= 5;

            return p;
        }
    }
}
