using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace Datawatch.DataPrep.Data.Core.UI.DragDrop
{
    public class DropPreviewAdorner : Adorner, IDisposable
    {
		private readonly ContentPresenter _presenter;
        private double _left;
        private double _top;

        public double Left
        {
            get { return _left; }
            set 
            { 
                _left = value;
                UpdatePosition();
            }
        }

        public double Top
        {
            get { return _top; }
            set 
            {
                _top = value;
                UpdatePosition();
            }
        }

        public DropPreviewAdorner(UIElement feedbackUI, UIElement adornedElt) : 
            base(adornedElt)
        {
			_presenter = new ContentPresenter
			                 {
			                     Content = feedbackUI, 
                                 IsHitTestVisible = false
			                 };
        }

        private void UpdatePosition()
        {
			AdornerLayer layer = Parent as AdornerLayer;
			if (layer != null)
            {
				layer.Update(AdornedElement);
            }
        }

        protected override Size MeasureOverride(Size constraint)
        {
			_presenter.Measure(constraint);
			return _presenter.DesiredSize;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
			_presenter.Arrange(new Rect(finalSize));
            return finalSize;
        }

        protected override Visual GetVisualChild(int index)
        {
			return _presenter;
        }

        protected override int VisualChildrenCount
        {
            get
            {
                return 1;
            }
        }

    	public override GeneralTransform GetDesiredTransform(GeneralTransform transform)
        {
            GeneralTransformGroup result = new GeneralTransformGroup();
            result.Children.Add(new TranslateTransform(Left, Top));
            result.Children.Add(base.GetDesiredTransform(transform));

            return result;
        }

        public void Dispose()
        {
            if (_presenter.Content is IDisposable)
            {
                IDisposable content = (IDisposable) _presenter.Content;
                content.Dispose();
            }
        }
    }
}
