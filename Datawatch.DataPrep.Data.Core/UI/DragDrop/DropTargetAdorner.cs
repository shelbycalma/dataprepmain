﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;

namespace Datawatch.DataPrep.Data.Core.UI.DragDrop
{
    public class DropTargetAdorner : Adorner
    {
        private readonly List<UIElement> dropTargets;
        private readonly IDataObject dataObject;

        public  DropTargetAdorner(UIElement uiElement, 
            List<UIElement> dropTargets, IDataObject data) : base(uiElement)
        {
            this.dropTargets = dropTargets;
            dataObject = data;
            IsHitTestVisible = false;
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            RectangleGeometry bounds = 
                new RectangleGeometry(new Rect(DesiredSize));
            Geometry shade = bounds;

            foreach (UIElement element in dropTargets) 
            {
                IDropTargetAdvisor dropTargetAdvisor = 
                    DragDropManager.GetDropTargetAdvisor(element);
                if (element.IsVisible && 
                    dropTargetAdvisor.IsValidDataObject(dataObject))
                {
                    try
                    {
                        Point p = AdornedElement.PointFromScreen(
                            element.PointToScreen(new Point(0, 0)));
                        Rect r = new Rect(p, element.RenderSize);
                        shade = new CombinedGeometry(
                            GeometryCombineMode.Exclude, 
                            shade, new RectangleGeometry(r));
                    }
                    catch
                    {
                    }
                }
            }

            drawingContext.DrawGeometry(
                new SolidColorBrush(Color.FromArgb(0xc2, 0xef, 0xef, 0xef)), 
                null, shade);
            
            base.OnRender(drawingContext);
        }
    }
}
