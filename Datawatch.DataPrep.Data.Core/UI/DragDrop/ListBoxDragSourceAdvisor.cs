﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace Datawatch.DataPrep.Data.Core.UI.DragDrop
{
    public abstract class ListBoxDragSourceAdvisor<T> : 
        IDragSourceAdvisor where T : class
    {

        public UIElement SourceUI
        {
            get;
            set;
        }

        public DragDropEffects SupportedEffects
        {
            get { return DragDropEffects.Move; }
        }

        public virtual DataObject GetDataObject(UIElement draggedElt, Point offsetPoint)
        {
            ListBox list = SourceUI as ListBox;

            if (list != null)
            {
                ((UIElement)list.ItemContainerGenerator.ContainerFromItem(
                    list.SelectedItem)).Opacity = 0.3;

                return new DataObject(list.SelectedItem);
            }
            return null;
        }

        public void FinishDrag(UIElement draggedElt, DragDropEffects finalEffects)
        {
            ListBox list = SourceUI as ListBox;
            
            if (list == null) return;

            UIElement dragElement = list.ItemContainerGenerator.ContainerFromItem(
                list.SelectedItem) as UIElement;
            if (dragElement != null)
            {
                dragElement.Opacity = 1;
            }
        }

        public bool IsDraggable(UIElement dragElt, UIElement originalSource)
        {
            if (Utils.FindParent<ScrollBar>(originalSource) != null) return false;
            if (Utils.FindParent<TextBox>(originalSource) != null) return false;
            if (Utils.FindParent<GridViewHeaderRowPresenter>(originalSource) != null) return false;

            if (dragElt is ListBox)
            {
                ListBox list = (ListBox) dragElt;
                T item = list.SelectedItem as T;
                return item != null && IsDraggable(item);
            }
            return false;
        }

        protected virtual bool IsDraggable(T item)
        {
            return true;
        }
    }
}
