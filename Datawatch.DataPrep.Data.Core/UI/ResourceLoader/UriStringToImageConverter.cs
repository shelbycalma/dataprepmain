﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.ResourceLoader
{
    internal class UriStringImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ResourceLoader.GetImage((string) parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
