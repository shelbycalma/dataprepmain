﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class TimeZoneConverter : IValueConverter
    {
        private static readonly ResourceManager ResourceManager = new ResourceManager(typeof(Properties.Resources));

        private static readonly Dictionary<string, string> TimeZones = new Dictionary<string, string> {};

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is string)
            {
                if (value.ToString() == string.Empty)
                    return value;

                return GetResourceString(value);
            }

            string[] timeZones = (string[])value;
            List<string> internalizedTimeZones = new List<string>();

            foreach (var zone in timeZones)
            {
                if (zone == string.Empty)
                {
                    internalizedTimeZones.Add(zone);
                    continue;
                }

                internalizedTimeZones.Add(GetResourceString(zone));
            }

            return internalizedTimeZones.ToArray();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value.ToString() == string.Empty)
                return value;

            string selectedTimeZone = (from timeZone in TimeZones where ResourceManager.GetString(timeZone.Value) == value.ToString() select timeZone.Key).FirstOrDefault();

            if (selectedTimeZone == null)
                throw new Exception(ResourceManager.GetString("ExCanNotFindTimeZone"));

            return selectedTimeZone;
        }

        private static string GetResourceString(object value)
        {
            string resourceName;
            string resourceValue = null;

            if (TimeZones.TryGetValue(value.ToString(), out resourceName))
            {
                resourceValue = ResourceManager.GetString(resourceName);
            }

            if (resourceValue == null)
                throw new Exception(ResourceManager.GetString("ExCanNotFindTimeZone"));

            return resourceValue;
        }
    }
}
