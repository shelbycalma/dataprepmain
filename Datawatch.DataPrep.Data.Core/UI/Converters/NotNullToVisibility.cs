﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class NotNullToVisibility : IValueConverter
    {
        private NotNullConverter notEmptyConverter;
        private VisibilityToBoolConverter visibilityToBoolConverter;

        public NotNullToVisibility()
        {
            notEmptyConverter = new NotNullConverter();
            visibilityToBoolConverter = new VisibilityToBoolConverter();
            visibilityToBoolConverter.Inverted = true;
            visibilityToBoolConverter.Not = false;
        }

        public bool Not
        {
            get { return visibilityToBoolConverter.Not; }
            set { visibilityToBoolConverter.Not = value; }
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool first = (bool)notEmptyConverter.Convert(value, typeof(bool),
                parameter, culture);
            object second = visibilityToBoolConverter.Convert(first, targetType, parameter, culture);
            return second;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
