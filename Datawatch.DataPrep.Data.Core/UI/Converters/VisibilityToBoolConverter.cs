﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class VisibilityToBoolConverter : IValueConverter
    {
        private bool inverted = false;
        private bool not = false;
        private Visibility invisibleValue = Visibility.Collapsed;

        public bool Inverted
        {
            get { return inverted; }
            set { inverted = value; }
        }

        public Visibility InvisibleValue
        {
            get { return invisibleValue; }
            set { invisibleValue = value; }
        }

        public bool Not
        {
            get { return not; }
            set { not = value; }
        }

        private object VisibilityToBool(object value)
        {
            if (!(value is Visibility))
                return DependencyProperty.UnsetValue;

            return (((Visibility)value) == Visibility.Visible) ^ Not;
        }

        private object BoolToVisibility(object value)
        {
            if (!(value is bool))
                return DependencyProperty.UnsetValue;

            return ((bool)value ^ Not) ? Visibility.Visible
                : invisibleValue;
        }

        public object Convert(object value, Type targetType,
                object parameter, CultureInfo culture)
        {
            return Inverted ? BoolToVisibility(value)
                : VisibilityToBool(value);
        }


        public object ConvertBack(object value, Type targetType,
                object parameter, CultureInfo culture)
        {
            return Inverted ? VisibilityToBool(value)
                : BoolToVisibility(value);
        }
    }
}
