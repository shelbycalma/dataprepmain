﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class EnumToVisibilityConverter : IValueConverter
    {
        public bool Inverted { get; set; }

        public object Convert(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value.Equals(parameter) ^ Inverted) ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (value.Equals(Visibility.Visible) ^ Inverted) ? parameter : Binding.DoNothing;
        }
    }
}
