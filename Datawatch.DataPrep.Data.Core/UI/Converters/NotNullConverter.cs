﻿using System;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class NotNullConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }

        #endregion
    }
}
