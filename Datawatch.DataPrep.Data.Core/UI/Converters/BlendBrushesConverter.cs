﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class BlendBrushesConverter : IMultiValueConverter
    {
        private byte ratioForeground = 255;

        public byte RatioForeground
        {
            get { return ratioForeground; }
            set { ratioForeground = value; }
        }

        public object Convert(object[] values, Type targetType,
            object parameter, CultureInfo culture)
        {
            if (!(values[0] is SolidColorBrush) ||
                !(values[1] is SolidColorBrush))
            {
                return DependencyProperty.UnsetValue;
            }

            SolidColorBrush fore = (SolidColorBrush) values[0];
            SolidColorBrush back = (SolidColorBrush) values[1];
            double percentFore = ratioForeground / 255.0;
            Color color = BlendBrushes(fore, back, percentFore);

            if (targetType == typeof(Brush))
            {
                SolidColorBrush brush = new SolidColorBrush(color);
                brush.Freeze();
                return brush;
            }
            else if (targetType == typeof(Color))
            {
                return color;
            }

            throw new ArgumentException(string.Format(Properties.Resources.ExTargetTypeNotSupported, targetType));
        }

        public object[] ConvertBack(object value, Type[] targetTypes,
            object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static Color BlendBrushes(
            SolidColorBrush foregroundBrush, SolidColorBrush backgroundBrush,
            double percentForeground)
        {
            Color fore = foregroundBrush != null ?
                foregroundBrush.Color : Colors.Black;
            Color back = backgroundBrush != null ?
                backgroundBrush.Color : Colors.White;
            double percentBackground = 1.0 - percentForeground;

            Color color = new Color();
            color.A = (byte) Math.Round(fore.A * percentForeground + back.A * percentBackground);
            color.R = (byte) Math.Round(fore.R * percentForeground + back.R * percentBackground);
            color.G = (byte) Math.Round(fore.G * percentForeground + back.G * percentBackground);
            color.B = (byte) Math.Round(fore.B * percentForeground + back.B * percentBackground);

            return color;
        }
    }
}
