﻿using System;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class ParameterNameConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool isEditable = (bool)values[1];
            object parameters;
            if (parameter == null)
            {
                parameters = parameter;
            }
            else
            {
                parameters = (parameter as CollectionViewSource).Source;
            }
            if (isEditable)
            {
                return ParameterUtils.FormatParameter(values[0], parameters);
            }
            ParameterValue parameterValue = values[0] as ParameterValue;
            if (parameterValue == null)
            {
                return "";
            }
            return parameterValue.Name;
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
