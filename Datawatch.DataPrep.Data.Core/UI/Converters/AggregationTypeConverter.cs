﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class AggregationTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is AggregateType)) return DependencyProperty.UnsetValue;

            AggregateType aggType = (AggregateType)value;

            if (aggType == AggregateType.None)
            {
                return Properties.Resources.UiAggregationTypeGroupBy;
            }
            else
            {
                return aggType.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
