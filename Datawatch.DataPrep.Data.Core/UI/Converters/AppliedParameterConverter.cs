﻿using System;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class AppliedParameterConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            bool isEditable = (bool)values[2];
            if (isEditable)
            {
                return ParameterUtils.FormatParameter(values[0], values[1]);
            }
            return values[0];
        }

        public object[] ConvertBack(object value, Type[] targetTypes,
            object parameter, System.Globalization.CultureInfo culture)
        {
            ParameterValue parameterValue = value as ParameterValue;
            if (parameterValue != null)
            {
                return new string[] { ((ParameterValue)value).Name.Replace("{", "").Replace("}", "") };
            }
            string stringValue = value as string;
            if (!string.IsNullOrEmpty(stringValue))
            {
                return new string[] { stringValue.Replace("{", "").Replace("}", "") };
            }
            return new string[] { stringValue };
        }
    }
}
