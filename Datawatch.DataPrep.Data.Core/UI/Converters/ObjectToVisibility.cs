﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    // This ValueConverter is used to set Visibility in xaml.
    // Return Visible if object is not null or Collapsed if it is null.
    public class ObjectToVisibility : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
            {
                if (value == null)
                    return Visibility.Collapsed.ToString();
                else if (value.ToString().Length > 0)
                    return Visibility.Visible.ToString();
                else
                    return Visibility.Collapsed.ToString();
        }

        // No need to implement converting back on a one-way binding 
        public object ConvertBack(object value, Type targetType,
                object parameter, System.Globalization.CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }
}
