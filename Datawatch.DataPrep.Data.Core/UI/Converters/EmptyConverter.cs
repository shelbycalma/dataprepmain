﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class EmptyConverter : IValueConverter
    {
        public virtual object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value is ICollectionView)
            {
                ICollectionView collectionView = (ICollectionView)value;
                return collectionView.IsEmpty;
            }
            if (value is ICollection)
            {
                ICollection collection = (ICollection)value;
                return collection.Count == 0;
            }
            if (value is string)
            {
                return ((string)value).Length == 0;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }  
    }
}
