using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class EnumToLocalizedConverter: IValueConverter
    {
        public ResourceManager ResourceManager { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (ResourceManager == null)
            {
                ResourceManager = Datawatch.DataPrep.Data.Core.Properties.Resources.ResourceManager;
            }
            if (value is Enum)
            {
                DescriptionAttribute descriptionAttribute = this.GetDescriptionAttributeValue(value);
                if (descriptionAttribute != null)
                    return ResourceManager.GetString(descriptionAttribute.Description);
            }

            if (value is IEnumerable)
            {
                IList<string> values = new List<string>();
                foreach (Enum enumValue in value as IEnumerable)
                {
                    DescriptionAttribute descriptionAttribute = this.GetDescriptionAttributeValue(enumValue);
                    if (descriptionAttribute != null)
                        values.Add(ResourceManager.GetString(descriptionAttribute.Description));
                }
                return values;
            }
            return null;
        }

        private DescriptionAttribute GetDescriptionAttributeValue(object input)
        {
            DescriptionAttribute descriptionAttribute =
                input.GetType()
                     .GetField(input.ToString())
                     .GetCustomAttributes(typeof(DescriptionAttribute), false)
                     .SingleOrDefault() as DescriptionAttribute;
            return descriptionAttribute;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string)
            {
                FieldInfo field = targetType.GetFields().FirstOrDefault(info =>
                    {
                        var descriptionAttribute = (DescriptionAttribute) (info.GetCustomAttributes(typeof (DescriptionAttribute), false).FirstOrDefault());
                        return descriptionAttribute != null && ResourceManager.GetString(descriptionAttribute.Description) == value.ToString();
                    });
                if (field != null)
                {
                    object target = Activator.CreateInstance(targetType);
                    return field.GetValue(target);
                }
            }
            return null;
        }
    }
}
