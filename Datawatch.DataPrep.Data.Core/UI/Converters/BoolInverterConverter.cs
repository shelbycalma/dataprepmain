﻿using System;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Converters
{
    public class BoolInverterConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                return !((bool)value);
            }
            return null;
        }
        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is bool)
            {
                return !((bool)value);
            }
            return null;
        }
        #endregion
    }  

}
