﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Windows;
using Datawatch.DataPrep.Data.Core.UI.Properties;
using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core.UI.Behaviors
{
    public static class SharedResources
    {
        private static readonly Dictionary<string, WeakReference> sharedDictionaries
            = new Dictionary<string, WeakReference>();

        public static readonly DependencyProperty MergedDictionariesProperty =
            DependencyProperty.RegisterAttached("MergedDictionaries",
                typeof(string), typeof(SharedResources),
                new FrameworkPropertyMetadata(null, OnMergedDictionariesChanged));

        public static string GetMergedDictionaries(DependencyObject d)
        {
            return (string)d.GetValue(MergedDictionariesProperty);
        }

        public static void SetMergedDictionaries(DependencyObject d, string value)
        {
            d.SetValue(MergedDictionariesProperty, value);
        }

        private static void OnMergedDictionariesChanged(DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(e.NewValue as string)) return;

            foreach (string dictionaryName in (e.NewValue as string).Split(';'))
            {
                ResourceDictionary dictionary =
                    GetResourceDictionary(dictionaryName);
                if (dictionary == null) continue;
                if (d is FrameworkElement)
                {
                    ((FrameworkElement)d).Resources
                        .MergedDictionaries.Add(dictionary);
                }
                else if (d is FrameworkContentElement)
                {
                    ((FrameworkContentElement)d).Resources
                        .MergedDictionaries.Add(dictionary);
                }
            }
        }

        private static ResourceDictionary GetResourceDictionary(string dictionaryName)
        {
            ResourceDictionary result = null;

            if (sharedDictionaries.ContainsKey(dictionaryName))
            {
                result = (ResourceDictionary) sharedDictionaries[dictionaryName].Target;
            }

            if (result != null) return result;

            Assembly entryAssembly = Assembly.GetEntryAssembly();

            // First look for the resource dictionary in the application
            if (entryAssembly != null)
            {
                result  = FindResourceDictionary(entryAssembly, dictionaryName);
            }

            // Fallback, try to find the resource dictionary in this assembly.
            if (result == null)
            {
                result = FindResourceDictionary(
                    Assembly.GetExecutingAssembly(), dictionaryName);            
            }

            if (result != null)
            {
                sharedDictionaries[dictionaryName] = new WeakReference(result);
            }
            
            return result;
        }

        private static ResourceDictionary FindResourceDictionary(Assembly assembly, string name)
        {
            if (assembly == null) throw Exceptions.ArgumentNull("assembly");
            if (name == null) return null;

            name = name.ToLower();
            string resourcesName = assembly.GetName().Name + ".g";
            ResourceManager manager = new ResourceManager(resourcesName, assembly);
            ResourceSet resourceSet = 
                manager.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            
            foreach (DictionaryEntry entry in resourceSet)
            {
                string fileName = (string) entry.Key;
                if (!fileName.EndsWith(".baml")) continue;
                string path = fileName.Substring(0, fileName.Length - 5);
                if (path.EndsWith(name))
                {
                    string assemblyName = Path.GetFileNameWithoutExtension(
                        assembly.ManifestModule.Name);

                    return LoadResourceDictionary(new Uri(
                        assemblyName + ";component/" + path + ".xaml",
                        UriKind.Relative));
                }
            }
            return null;
        }

        private static ResourceDictionary LoadResourceDictionary(Uri uri)
        {
            try
            {
                return Application.LoadComponent(uri) as ResourceDictionary;
            }
            catch (IOException)
            {
                Log.Error(Resources.ExResourceDictionaryNotFound, uri);
            }
            return null;
        }
    }
}