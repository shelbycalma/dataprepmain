﻿<ResourceDictionary
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:converters="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Converters">

    <converters:BlendBrushesConverter
        x:Key="BackgroundConverter" RatioForeground="15" />
    <converters:BlendBrushesConverter
        x:Key="NormalConverter" RatioForeground="58" />
    <converters:BlendBrushesConverter
        x:Key="HoverConverter" RatioForeground="82" />
    <converters:BlendBrushesConverter
        x:Key="PressedConverter" RatioForeground="130" />

    <!-- button at the end of the scroll bar -->
    <Style x:Key="ScrollBarLineButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="OverridesDefaultStyle" Value="True"/>
        <Setter Property="Focusable" Value="False"/>
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <Border
                        x:Name="Border"
                        CornerRadius="0"
                        BorderThickness="1"
                        BorderBrush="{Binding Background,
                            RelativeSource={RelativeSource Self}}">
                        <Border.Background>
                            <MultiBinding Converter="{StaticResource BackgroundConverter}">
                                <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                            </MultiBinding>
                        </Border.Background>
                        <Path
                            HorizontalAlignment="Center"
                            VerticalAlignment="Center"
                            Stretch="Uniform"
                            Data="{Binding Path=Content, RelativeSource={RelativeSource TemplatedParent}}"
                            >
                            <Path.Fill>
                                <MultiBinding Converter="{StaticResource PressedConverter}">
                                    <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                    <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                                </MultiBinding>
                            </Path.Fill>
                        </Path>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="Border" Property="Background">
                                <Setter.Value>
                                    <MultiBinding Converter="{StaticResource HoverConverter}">
                                        <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                        <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                                    </MultiBinding>
                                </Setter.Value>
                            </Setter>
                        </Trigger>
                        <Trigger Property="IsPressed" Value="True">
                            <Setter TargetName="Border" Property="Background">
                                <Setter.Value>
                                    <MultiBinding Converter="{StaticResource PressedConverter}">
                                        <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                        <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                                    </MultiBinding>
                                </Setter.Value>
                            </Setter>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <!-- Button to either side of the thumb -->
    <Style x:Key="ScrollBarPageButton" TargetType="{x:Type RepeatButton}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="True" />
        <Setter Property="IsTabStop" Value="False" />
        <Setter Property="Focusable" Value="False" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type RepeatButton}">
                    <Border>
                        <Border.Background>
                            <MultiBinding Converter="{StaticResource BackgroundConverter}">
                                <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                            </MultiBinding>
                        </Border.Background>
                    </Border>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <Style x:Key="ScrollBarThumb" TargetType="{x:Type Thumb}">
        <Setter Property="SnapsToDevicePixels" Value="True" />
        <Setter Property="OverridesDefaultStyle" Value="True" />
        <Setter Property="IsTabStop" Value="False" />
        <Setter Property="Focusable" Value="False" />
        <Setter Property="Template">
            <Setter.Value>
                <ControlTemplate TargetType="{x:Type Thumb}">
                    <Border
                        x:Name="Border"
                        CornerRadius="0"
                        BorderThickness="1"
                        BorderBrush="{Binding Background,
                            RelativeSource={RelativeSource Self}}">
                        <Border.Background>
                            <MultiBinding Converter="{StaticResource NormalConverter}">
                                <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                            </MultiBinding>
                        </Border.Background>
                    </Border>
                    <ControlTemplate.Triggers>
                        <Trigger Property="IsMouseOver" Value="True">
                            <Setter TargetName="Border" Property="Background">
                                <Setter.Value>
                                    <MultiBinding Converter="{StaticResource HoverConverter}">
                                        <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                        <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                                    </MultiBinding>
                                </Setter.Value>
                            </Setter>
                        </Trigger>
                        <Trigger Property="IsDragging" Value="True">
                            <Setter TargetName="Border" Property="Background">
                                <Setter.Value>
                                    <MultiBinding Converter="{StaticResource PressedConverter}">
                                        <Binding Path="Foreground" RelativeSource="{RelativeSource TemplatedParent}" />
                                        <Binding Path="Background" RelativeSource="{RelativeSource TemplatedParent}" />
                                    </MultiBinding>
                                </Setter.Value>
                            </Setter>
                        </Trigger>
                    </ControlTemplate.Triggers>
                </ControlTemplate>
            </Setter.Value>
        </Setter>
    </Style>

    <ControlTemplate x:Key="VerticalScrollBar" TargetType="{x:Type ScrollBar}">
        <Grid >
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="0.00001*"/>
                <RowDefinition Height="Auto"/>
            </Grid.RowDefinitions>
            <Border
                Grid.RowSpan="3"
                Background="{TemplateBinding Background}" />
            <RepeatButton
                Grid.Row="0"
                Style="{StaticResource ScrollBarLineButton}"
                Height="{Binding ActualWidth, RelativeSource={RelativeSource Self}}"
                Background="{TemplateBinding Background}"
                Foreground="{TemplateBinding Foreground}"
                Command="ScrollBar.LineUpCommand"
                Content="M 0 4 L 8 4 L 4 0 Z" />
            <Track
                Name="PART_Track"
                Grid.Row="1"
                IsDirectionReversed="True">
                <Track.DecreaseRepeatButton>
                    <RepeatButton
                        Style="{StaticResource ScrollBarPageButton}"
                        Background="{TemplateBinding Background}"
                        Foreground="{TemplateBinding Foreground}"
                        Command="ScrollBar.PageUpCommand" />
                </Track.DecreaseRepeatButton>
                <Track.Thumb>
                    <Thumb
                        Style="{StaticResource ScrollBarThumb}"
                        Background="{TemplateBinding Background}"
                        Foreground="{TemplateBinding Foreground}">
                    </Thumb>
                </Track.Thumb>
                <Track.IncreaseRepeatButton>
                    <RepeatButton
                        Style="{StaticResource ScrollBarPageButton}"
                        Background="{TemplateBinding Background}"
                        Foreground="{TemplateBinding Foreground}"
                        Command="ScrollBar.PageDownCommand" />
                </Track.IncreaseRepeatButton>
            </Track>
            <RepeatButton
                Grid.Row="3"
                Style="{StaticResource ScrollBarLineButton}"
                Height="{Binding ActualWidth, RelativeSource={RelativeSource Self}}"
                Background="{TemplateBinding Background}"
                Foreground="{TemplateBinding Foreground}"
                Command="ScrollBar.LineDownCommand"
                Content="M 0 0 L 4 4 L 8 0 Z" />
        </Grid>
    </ControlTemplate>

    <ControlTemplate x:Key="HorizontalScrollBar" TargetType="{x:Type ScrollBar}">
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto" />
                <ColumnDefinition Width="0.00001*" />
                <ColumnDefinition Width="Auto" />
            </Grid.ColumnDefinitions>
            <Border
                Grid.ColumnSpan="3"
                Background="{TemplateBinding Background}" />
            <RepeatButton
                Grid.Column="0"
                Style="{StaticResource ScrollBarLineButton}"
                Width="{Binding ActualHeight, RelativeSource={RelativeSource Self}}"
                Background="{TemplateBinding Background}"
                Foreground="{TemplateBinding Foreground}"
                Command="ScrollBar.LineLeftCommand"
                Content="M 4 0 L 4 8 L 0 4 Z" />
            <Track 
                Name="PART_Track"
                Grid.Column="1"
                IsDirectionReversed="False">
                <Track.DecreaseRepeatButton>
                    <RepeatButton 
                        Style="{StaticResource ScrollBarPageButton}"
                        Background="{TemplateBinding Background}"
                        Foreground="{TemplateBinding Foreground}"
                        Command="ScrollBar.PageLeftCommand" />
                </Track.DecreaseRepeatButton>
                <Track.Thumb>
                    <Thumb 
                        Style="{StaticResource ScrollBarThumb}"
                        Background="{TemplateBinding Background}"
                        Foreground="{TemplateBinding Foreground}">
                    </Thumb>
                </Track.Thumb>
                <Track.IncreaseRepeatButton>
                    <RepeatButton
                        Style="{StaticResource ScrollBarPageButton}"
                        Background="{TemplateBinding Background}"
                        Foreground="{TemplateBinding Foreground}"
                        Command="ScrollBar.PageRightCommand" />
                </Track.IncreaseRepeatButton>
            </Track>
            <RepeatButton
                Grid.Column="3"
                Style="{StaticResource ScrollBarLineButton}"
                Width="{Binding ActualHeight, RelativeSource={RelativeSource Self}}"
                Background="{TemplateBinding Background}"
                Foreground="{TemplateBinding Foreground}"
                Command="ScrollBar.LineRightCommand"
                Content="M 0 0 L 4 4 L 0 8 Z" />
        </Grid>
    </ControlTemplate>

    <Style x:Key="ScrollBarStyle" TargetType="ScrollBar">
        <Setter Property="SnapsToDevicePixels" Value="True"/>
        <Setter Property="OverridesDefaultStyle" Value="True"/>
        <Setter Property="Background" Value="{DynamicResource {x:Static SystemColors.ControlBrushKey}}" />
        <Setter Property="Foreground" Value="{DynamicResource {x:Static SystemColors.ControlTextBrushKey}}" />
        <Style.Triggers>
            <Trigger Property="Orientation" Value="Horizontal">
                <Setter Property="Width" Value="Auto"/>
                <Setter Property="Height" Value="7" />
                <Setter Property="Template" Value="{StaticResource HorizontalScrollBar}" />
            </Trigger>
            <Trigger Property="Orientation" Value="Vertical">
                <Setter Property="Width" Value="7"/>
                <Setter Property="Height" Value="Auto" />
                <Setter Property="Template" Value="{StaticResource VerticalScrollBar}" />
            </Trigger>
        </Style.Triggers>
    </Style>

    <Style BasedOn="{StaticResource ScrollBarStyle}" TargetType="ScrollBar"/>
    
</ResourceDictionary>