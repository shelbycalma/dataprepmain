﻿using System;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    /// <summary>
    /// A single selectable unique value in a DateTimeField.
    /// </summary>
    public class DateTimeValueViewModel : ViewModelBase
    {
        private readonly int value;
        private readonly string title;
        private bool isEnabled = true;

        public DateTimeValueViewModel(int value)
            : this(value, CultureInfo.CurrentCulture)
        {
        }

        public DateTimeValueViewModel(int value, IFormatProvider culture)
            : this(value, string.Format(culture, "{0:00}", value))
        {
        }

        public DateTimeValueViewModel(int value, string title)
        {
            this.value = value;
            this.title = title;
        }

        /// <summary>
        /// This value represent the index, day in month or month in year.
        /// The exception is year, which is not index based.
        /// </summary>
        public int Value
        {
            get { return value; }
        }

        /// <summary>
        /// A user friendly title for this value.
        /// Formatted integers with the exception of month which displays
        /// the localized name.
        /// </summary>
        public string Title
        {
            get { return title; }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            internal set
            {
                if (value == isEnabled)
                {
                    return;
                }
                isEnabled = value;
                OnPropertyChanged("IsEnabled");
            }
        }

        /// <summary>
        /// Only compares index/value, localized title shouldn't matter.
        /// </summary>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            return Value == ((DateTimeValueViewModel)obj).Value;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.InvariantCulture, "DateTimeValue:{0}", title);
        }
    }
}
