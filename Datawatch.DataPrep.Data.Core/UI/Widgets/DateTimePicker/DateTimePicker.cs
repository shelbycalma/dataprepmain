﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    public class DateTimePicker : Control, IWeakEventListener
    {
        private static readonly DependencyPropertyKey ViewModelKey =
            DependencyProperty.RegisterReadOnly(
                "ViewModel",
                typeof(DateTimePickerViewModel),
                typeof(DateTimePicker),
                new FrameworkPropertyMetadata(ViewModel_Changed));

        public static readonly DependencyProperty ViewModelProperty = 
            ViewModelKey.DependencyProperty;

        public static readonly DependencyProperty DateTimeProperty =
            DependencyProperty.Register(
                "DateTime",
                typeof(DateTime),
                typeof(DateTimePicker),
                new FrameworkPropertyMetadata(DateTime.MaxValue, DateTime_Changed)
                {
                    BindsTwoWayByDefault = true
                });

        public static readonly DependencyProperty FormatStringProperty =
            DependencyProperty.Register(
                "FormatString",
                typeof(string),
                typeof(DateTimePicker),
                new FrameworkPropertyMetadata(
                    FormatString_Changed, FormatString_Coerce));

        public static readonly DependencyProperty EnabledIntervalProperty =
            DependencyProperty.Register(
                "EnabledInterval",
                typeof(DateTimeInterval),
                typeof(DateTimePicker),
                new FrameworkPropertyMetadata(
                    new DateTimeInterval(DateTime.MinValue, DateTime.MinValue),
                    EnabledInterval_Changed));

        public static readonly DependencyProperty FallbackModeProperty =
            DependencyProperty.Register(
                "FallbackMode",
                typeof(DateTimePickerFallbackMode),
                typeof(DateTimePicker),
                new PropertyMetadata(FallbackMode_Changed));

        static DateTimePicker()
        {
            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(DateTimePicker),
                new FrameworkPropertyMetadata(typeof(DateTimePicker)));
        }

        public DateTimePicker()
        {
            // Note that the view model need to be assigned this way so the
            // property changed callback is made and listener is attached
            ViewModel = new DateTimePickerViewModel();
        }

        public DateTimePickerViewModel ViewModel
        {
            get { return (DateTimePickerViewModel)GetValue(ViewModelProperty); }
            private set { SetValue(ViewModelKey, value); }
        }

        /// <summary>
        /// Gets or sets the date and time shown in the DateTimePicker.
        /// This is a dependency property.
        /// </summary>
        public DateTime DateTime
        {
            get { return (DateTime)GetValue(DateTimeProperty); }
            set { SetValue(DateTimeProperty, value); }
        }

        public string FormatString
        {
            get { return (string)GetValue(FormatStringProperty); }
            set { SetValue(FormatStringProperty, value); }
        }

        public DateTimeInterval EnabledInterval
        {
            get { return (DateTimeInterval)GetValue(EnabledIntervalProperty); }
            set { SetValue(EnabledIntervalProperty, value); }
        }

        public DateTimePickerFallbackMode FallbackMode
        {
            get { return (DateTimePickerFallbackMode)GetValue(FallbackModeProperty); }
            set { SetValue(FallbackModeProperty, value); }
        }

        private static void DateTime_Changed(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            ((DateTimePicker)d).OnDateTimeChanged((DateTime)e.NewValue);
        }

        private static void FormatString_Changed(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            DateTimePicker picker = (DateTimePicker)d;
            picker.ViewModel.FormatString = (string)e.NewValue;
        }

        // This coerces single letter "shortcut format strings" specific to
        // .net, it cant happen in the filter Format property since we still
        // want the single letters to show up in the configurator.
        // I didn't put it in the DateTimePickerViewModel since I want it to
        // look the same in java, where the coercion isn't needed
        // (both formats are serialized and java uses the already coerced one).
        private static object FormatString_Coerce(
            DependencyObject d, object basevalue)
        {
            string format = basevalue as string;
            if (format != null && format.Length == 1)
            {
                format = CreateCustomFormat(format);
            }
            return format;
        }

        private static string CreateCustomFormat(string format)
        {
            DateTimeFormatInfo currentInfo = DateTimeFormatInfo.CurrentInfo;
            DateTimeFormatInfo invariantInfo = DateTimeFormatInfo.InvariantInfo;
            switch (format)
            {
                case "d":
                    return currentInfo.ShortDatePattern;
                case "D":
                    return currentInfo.LongDatePattern;
                case "t":
                    return currentInfo.ShortTimePattern;
                case "T":
                    return currentInfo.LongTimePattern;
                case "f":
                    format = currentInfo.LongDatePattern;
                    return format + " " + currentInfo.ShortTimePattern;
                case "F":
                    return currentInfo.FullDateTimePattern;
                case "g":
                    return currentInfo.ShortDatePattern + " " +
                        currentInfo.ShortTimePattern;
                case "G":
                    return currentInfo.ShortDatePattern + " " +
                        currentInfo.LongTimePattern;
                case "m":
                case "M":
                    return currentInfo.MonthDayPattern;
                case "r":
                case "R":
                    return invariantInfo.RFC1123Pattern;
                case "s":
                    return invariantInfo.RFC1123Pattern;
                case "u":
                    return currentInfo.UniversalSortableDateTimePattern;
                // The case "U" utilizes UTC (will do time zone adjustments).
                //case "U":
                //    return currentInfo.FullDateTimePattern;
                case "y":
                case "Y":
                    return currentInfo.YearMonthPattern;
                default:
                    return null;
            }
        }

        private static void EnabledInterval_Changed(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue == null ||
                (e.OldValue != null && e.OldValue.Equals(e.NewValue)))
            {
                return;
            }
            DateTimePicker picker = (DateTimePicker)d;
            picker.ViewModel.EnabledInterval = (DateTimeInterval)e.NewValue;
        }

        private static void FallbackMode_Changed(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            DateTimePicker picker = (DateTimePicker)d;
            picker.ViewModel.FallbackMode =
                (DateTimePickerFallbackMode)e.NewValue;
        }   

        private void OnDateTimeChanged(DateTime newValue)
        {
            // if the viewModel doesnt have the same value as the dependency
            // property it usually means that the value was changed through
            // a binding in the parent control, so update the viewModel
            // with the current value.
            if (ViewModel != null && !ViewModel.DateTime.Equals(newValue))
            {
                ViewModel.DateTime = newValue;
            }
        }

        private static void ViewModel_Changed(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            DateTimePicker picker = (DateTimePicker)d;
            if (e.OldValue != null)
            {
                PropertyChangedEventManager.RemoveListener(
                    (INotifyPropertyChanged)e.OldValue, picker, string.Empty);
            }
            if (e.NewValue != null)
            {
                PropertyChangedEventManager.AddListener(
                    (INotifyPropertyChanged)e.NewValue, picker, string.Empty);
            }
        }

        #region Implementation of IWeakEventListener
        public bool ReceiveWeakEvent(
            Type managerType, object sender, EventArgs e)
        {
            if (managerType != typeof(PropertyChangedEventManager))
            {
                Debug.Fail("Bad manager type.");
            }
            PropertyChangedEventArgs eventArgs = e as PropertyChangedEventArgs;
            if (eventArgs != null &&
                eventArgs.PropertyName == "DateTime")
            {
                DateTime = ViewModel.DateTime;
            }
            return true;
        }
        #endregion
    }
}
