﻿using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    /// <summary>
    /// A "date time field" represents both one part of the DateTime
    /// and one corresponding component in the gui.
    /// The sole purpose of this class is to act as a root in the
    /// inheritance hierarchy.
    /// </summary>
    public class DateTimeFieldViewModel : ViewModelBase
    {
    }
}
