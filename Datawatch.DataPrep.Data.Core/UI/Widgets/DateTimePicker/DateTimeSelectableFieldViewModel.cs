﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    /// <summary>
    /// A selectable field is represented by a combobox holding the possible
    /// selectable values and the currently selected value. 
    /// </summary>
    public abstract class DateTimeSelectableFieldViewModel : DateTimeFieldViewModel
    {
        private List<DateTimeValueViewModel> items;
        private DateTimeValueViewModel selected;

        protected DateTimeSelectableFieldViewModel()
            : this(new List<DateTimeValueViewModel>())
        {
        }

        protected DateTimeSelectableFieldViewModel(
            List<DateTimeValueViewModel> items)
        {
            if (items == null)
            {
                throw Exceptions.ArgumentNull("items");
            }
            this.items = items;
            selected = items.FirstOrDefault();
        }

        /// <summary>
        /// Sorted unique values that this DateTimeField can represent.
        /// </summary>
        public List<DateTimeValueViewModel> Items
        {
            get { return items; }
            // allow subclasses to change the selectable items
            // (like selectable days change with month)
            protected set
            {
                if (value == null)
                {
                    throw Exceptions.ArgumentNull("value");
                }
                items = value;
                OnPropertyChanged("Items");
            }
        }

        /// <summary>
        /// The current value for this DateTimeField.
        /// </summary>
        public virtual DateTimeValueViewModel Selected
        {
            get { return selected; }
            set
            {
                if (value == selected)
                {
                    return;
                }
                selected = value;
                OnPropertyChanged("Selected");
            }
        }

        /// <summary>
        /// If <see langword="false"/> the combo box should be hidden and
        /// a placeholder value should take its place when handling selection.
        /// </summary>
        public bool Active { get; internal set; }

        internal abstract void SetMinMaxDateTime(
            DateTime min,
            DateTime max,
            DateTime current);

        /// <summary>
        /// If the currently selected value is no longer enabled or was removed
        /// from the list, the last valid item is selected instead.
        /// </summary>
        internal void CoerceSelected()
        {
            DateTimeValueViewModel selectedItem = null;
            // find the selected item
            foreach (DateTimeValueViewModel currentItem in Items)
            {
                if (currentItem.Equals(Selected))
                {
                    selectedItem = currentItem;
                    break;
                }
            }
            if (selectedItem == null || !selectedItem.IsEnabled)
            {
                // select last enabled item
                for (int i = Items.Count - 1; i >= 0; i--)
                {
                    DateTimeValueViewModel item = Items[i];
                    if (item.IsEnabled)
                    {
                        Selected = item;
                        return;
                    }
                }
            }
        }
    }
}
