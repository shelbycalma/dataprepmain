﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    public class DateTimePickerViewModel : ViewModelBase
    {
        private readonly YearViewModel year;
        private readonly MonthViewModel month;
        private readonly DayViewModel day;
        private readonly HourViewModel hour;
        private readonly MinuteViewModel minute;
        private readonly SecondViewModel second;
        private readonly CultureInfo culture;
        private readonly DateTimeSeparatorViewModel dateSeparator;
        private readonly DateTimeSeparatorViewModel timeSeparator;
        private string formatString;
        private DateTimeInterval enabledInterval;
        private bool disposed;
        private bool updating;
        private int coercionDepth;
        private DateTimePickerFallbackMode fallbackMode;

        public DateTimePickerViewModel()
            : this(null, null, null)
        {
        }

        public DateTimePickerViewModel(CultureInfo culture)
            : this(culture, null, null)
        {
        }

        public DateTimePickerViewModel(string formatString)
            : this(null, formatString, null)
        {
        }

        public DateTimePickerViewModel(DateTimeInterval enabledInterval)
            : this(null, null, enabledInterval)
        {
        }

        /// <summary>
        /// Creates a new instance of DateTimePickerViewModel.
        /// </summary>
        /// <param name="culture">
        /// The culture in which localized month names should be displayed.
        /// </param>
        /// <param name="formatString">
        /// The format string that decides what combo boxes will be visible
        /// and in what order they will be shown.
        /// </param>
        /// <param name="enabledInterval">
        /// The range of dates that will be selectable by the date time picker.
        /// </param>
        public DateTimePickerViewModel(
            CultureInfo culture,
            string formatString,
            DateTimeInterval enabledInterval)
        {
            if (culture == null)
            {
                culture = CultureInfo.CurrentCulture;
            }
            if (formatString == null)
            {
                // by default, display both date and time
                DateTimeFormatInfo info = culture.DateTimeFormat;
                formatString =
                    info.ShortDatePattern + " " + info.LongTimePattern;
            }
            if (enabledInterval == null)
            {
                enabledInterval = GetDefaultInterval();
            }

            dateSeparator = new DateTimeSeparatorViewModel(
                culture.DateTimeFormat.DateSeparator);
            timeSeparator = new DateTimeSeparatorViewModel(
                culture.DateTimeFormat.TimeSeparator);

            // create all models, but keep track of them individually since we
            // need to be able to get/set DateTime
            year = new YearViewModel();
            month = new MonthViewModel(culture);
            day = new DayViewModel();
            hour = new HourViewModel();
            minute = new MinuteViewModel();
            second = new SecondViewModel();

            year.PropertyChanged += Field_PropertyChanged;
            month.PropertyChanged += Field_PropertyChanged;
            day.PropertyChanged += Field_PropertyChanged;
            hour.PropertyChanged += Field_PropertyChanged;
            minute.PropertyChanged += Field_PropertyChanged;
            second.PropertyChanged += Field_PropertyChanged;

            this.culture = culture;
            this.enabledInterval = enabledInterval;

            // format string needs to be set through property setter to update
            // active flag
            FormatString = formatString;

            // set initial date time displayed
            UpdateSelection(enabledInterval.Min);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (!disposed)
            {
                if (disposing)
                {
                    year.PropertyChanged -= Field_PropertyChanged;
                    month.PropertyChanged -= Field_PropertyChanged;
                    day.PropertyChanged -= Field_PropertyChanged;
                    hour.PropertyChanged -= Field_PropertyChanged;
                    minute.PropertyChanged -= Field_PropertyChanged;
                    second.PropertyChanged -= Field_PropertyChanged;
                    year.Dispose();
                    month.Dispose();
                    day.Dispose();
                    hour.Dispose();
                    minute.Dispose();
                    second.Dispose();
                    dateSeparator.Dispose();
                    timeSeparator.Dispose();
                }
                disposed = true;
            }
        }

        private void Field_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            if (coercionDepth > 6)
            {
                return;
            }
            if (!updating)
            {
                if (sender == year || sender == month)
                {
                    day.UpdateDays(year.Selected.Value, month.Selected.Value);
                }
                UpdateSelection(DateTime);
                OnPropertyChanged("DateTime");
            }
        }

        public DateTimeInterval EnabledInterval
        {
            get { return enabledInterval; }
            set
            {
                if (value == null)
                {
                    throw Exceptions.ArgumentNull("value");
                }
                DateTime oldSelection = DateTime;
                enabledInterval = value;
                if (value.Max.Equals(oldSelection) ||
                    (oldSelection.CompareTo(value.Min) > 0 &&
                        oldSelection.CompareTo(value.Max) < 0))
                {
                    UpdateSelection(oldSelection);
                }
                else
                {
                    UpdateSelection(value.Min);
                }
                OnPropertyChanged("EnabledInterval");
            }
        }

        /// <summary>
        /// Creates a new DateTime according to the current selection
        /// or populates the models with a new DateTime.
        /// The created DateTime respects the show date/time properties,
        /// hidden fields are set to DateTime.MinValue.
        /// </summary>
        public DateTime DateTime
        {
            get
            {
                // current intervals minvalue is used for inactive fields
                DateTime fallback =
                    fallbackMode == DateTimePickerFallbackMode.MinValue
                        ? enabledInterval.Min : enabledInterval.Max;

                int y = Get(year, fallback.Year);
                int m = Get(month, fallback.Month);
                int d = Get(day, fallback.Day);

                // handle days overflowing
                d = Math.Min(DateTime.DaysInMonth(y, m), d);

                DateTime current = new DateTime(
                    y, m, d,
                    Get(hour, fallback.Hour),
                    Get(minute, fallback.Minute),
                    Get(second, fallback.Second));
                return current;
            }
            set
            {
                UpdateSelection(value);
                OnPropertyChanged("DateTime");
            }
        }

        /// <summary>
        /// Every DateTimeField, sorted according to culture, exposed
        /// for binding in view.
        /// </summary>
        public List<DateTimeFieldViewModel> Fields
        {
            get { return GetSortedFields(); }
        }

        public string FormatString
        {
            get { return formatString; }
            set
            {
                if (value == null)
                {
                    throw Exceptions.ArgumentNull("value");
                }
                if (formatString == value)
                {
                    return;
                }
                formatString = value;

                year.Active = value.Contains("yyyy");
                month.Active = value.Contains("M");
                day.Active = value.Contains("d");
                // TODO hour is always in 24 hour mode and never 12 hour
                hour.Active = value.Contains("H") || value.Contains("h");
                minute.Active = value.Contains("mm");
                second.Active = value.Contains("ss");

                OnPropertyChanged("FormatString");
                OnPropertyChanged("Fields");
            }
        }

        public DateTimePickerFallbackMode FallbackMode
        {
            get { return fallbackMode; }
            set
            {
                if (fallbackMode != value)
                {
                    fallbackMode = value;
                    OnPropertyChanged("FallbackMode");
                }
            }
        }

        public DateTimeSelectableFieldViewModel Year
        {
            get { return year; }
        }

        public DateTimeSelectableFieldViewModel Month
        {
            get { return month; }
        }

        public DateTimeSelectableFieldViewModel Day
        {
            get { return day; }
        }

        public DateTimeSelectableFieldViewModel Hour
        {
            get { return hour; }
        }

        public DateTimeSelectableFieldViewModel Minute
        {
            get { return minute; }
        }

        public DateTimeSelectableFieldViewModel Second
        {
            get { return second; }
        }

        public DateTimeSeparatorViewModel DateSeparator
        {
            get { return dateSeparator; }
        }

        public DateTimeSeparatorViewModel TimeSeparator
        {
            get { return timeSeparator; }
        }

        private void UpdateSelection(DateTime current)
        {
            updating = true;

            // when the current datetime changes, the enabled property
            // on all the values in the fields needs to be updated
            DateTime min = enabledInterval.Min;
            DateTime max = enabledInterval.Max;

            year.SetMinMaxDateTime(min, max, current);
            month.SetMinMaxDateTime(min, max, current);
            day.SetMinMaxDateTime(min, max, current);
            hour.SetMinMaxDateTime(min, max, current);
            minute.SetMinMaxDateTime(min, max, current);
            second.SetMinMaxDateTime(min, max, current);

            year.Selected = new DateTimeValueViewModel(current.Year, culture);
            month.Selected = new MonthValueViewModel(current.Month, culture);
            day.Selected = new DateTimeValueViewModel(current.Day, culture);
            hour.Selected = new DateTimeValueViewModel(current.Hour, culture);
            minute.Selected = new DateTimeValueViewModel(current.Minute, culture);
            second.Selected = new DateTimeValueViewModel(current.Second, culture);

            updating = false;

            coercionDepth++;
            try
            {
                year.CoerceSelected();
                month.CoerceSelected();
                day.CoerceSelected();
                hour.CoerceSelected();
                minute.CoerceSelected();
                second.CoerceSelected();
            }
            finally
            {
                coercionDepth--;
            }
        }

        private List<DateTimeFieldViewModel> GetSortedFields()
        {
            HashSet<char> seen = new HashSet<char>();

            List<DateTimeFieldViewModel> list =
                new List<DateTimeFieldViewModel>();

            foreach (char c in formatString)
            {
                if (!Char.IsLetter(c))
                {
                    // the designer lets the user select a format string in a
                    // culture agnostic way
                    if (c == '/')
                    {
                        list.Add(dateSeparator);
                    }
                    else if (c == ':')
                    {
                        list.Add(timeSeparator);
                    }
                    else
                    {
                        // treat unknown characters as custom separators
                        // TODO should these be ignored instead?
                        list.Add(new DateTimeSeparatorViewModel(c.ToString()));
                    }

                    // TODO maybe formatting an actual date
                    // would be a better approach, but since we dont know how
                    // long the resulting string will be
                    // (as in the case of full month names)
                    // finding the correct separator might be tricky
                }
                if (seen.Contains(c))
                {
                    continue;
                }
                switch (c)
                {
                    case 'y':
                        list.Add(year);
                        seen.Add(c);
                        break;
                    case 'M':
                        list.Add(month);
                        seen.Add(c);
                        break;
                    case 'd':
                        list.Add(day);
                        seen.Add(c);
                        break;
                    case 'H':
                    case 'h':
                        list.Add(hour);
                        seen.Add(c);
                        break;
                    case 'm':
                        list.Add(minute);
                        seen.Add(c);
                        break;
                    case 's':
                        list.Add(second);
                        seen.Add(c);
                        break;
                }
            }
            return list;
        }

        public static DateTimeInterval GetDefaultInterval()
        {
            DateTime now = DateTime.Now;
            DateTime min = now.AddYears(-12);
            DateTime max = now.AddYears(12);
            // explicit recerate to truncate milliseconds
            // (since theyre not exposed by the DateTimePicker)
            min = new DateTime(
                min.Year, min.Month, min.Day, min.Hour, min.Minute, min.Second);
            max = new DateTime(
                max.Year, max.Month, max.Day, max.Hour, max.Minute, max.Second);
            return new DateTimeInterval(min, max);
        }

        [Obsolete]
        public static bool ShouldShowDate(string format)
        {
            return true;
        }

        [Obsolete]
        public static bool ShouldShowTime(string format)
        {
            return true;
        }

        private static int Get(
            DateTimeSelectableFieldViewModel model, int fallback)
        {
            if (model != null && model.Selected != null && model.Active)
            {
                return model.Selected.Value;
            }
            return fallback;
        }

        private static List<DateTimeValueViewModel> CreateIntegerRangeValues(
            int start, int count)
        {
            List<DateTimeValueViewModel> list =
                new List<DateTimeValueViewModel>();
            for (int i = start; i < start + count; i++)
            {
                list.Add(new DateTimeValueViewModel(i));
            }
            return list;
        }

        private static bool Intersects(
            long minA, long maxA, long minB, long maxB)
        {
            bool intersects = !((maxA < minB) || (maxB < minA));
            return intersects;
        }

        #region nested classes
        // These nested classes handles the updating of selectable days when
        // either month or year changes. 
        // Without hooking on any events (we dont want to handle unhooking,
        // as the control shouldnt be disposable).
        private class YearViewModel : DateTimeSelectableFieldViewModel
        {
            internal override void SetMinMaxDateTime(
                DateTime minValue, DateTime maxValue, DateTime current)
            {
                Items = CreateIntegerRangeValues(
                    minValue.Year,
                    maxValue.Year - minValue.Year + 1);

                if ((Selected == null || !Items.Contains(Selected))
                    && Items.Count > 0)
                {
                    Selected = Items[0];
                }
            }
        }

        private class MonthViewModel : DateTimeSelectableFieldViewModel
        {
            public MonthViewModel(CultureInfo culture)
            {
                List<DateTimeValueViewModel> models =
                    new List<DateTimeValueViewModel>(12);
                for (int i = 1; i < 13; i++)
                {
                    // month one-indexed
                    models.Add(new MonthValueViewModel(i, culture));
                }
                Items = models;
            }

            internal override void SetMinMaxDateTime(
                DateTime min, DateTime max, DateTime current)
            {
                int year = current.Year;
                foreach (DateTimeValueViewModel item in Items)
                {
                    int month = item.Value;
                    DateTime monthMin = new DateTime(year, month, 1);
                    DateTime monthMax = new DateTime(
                        year, month, DateTime.DaysInMonth(year, month), 23, 59, 59);
                    item.IsEnabled = Intersects(
                        monthMin.Ticks, monthMax.Ticks, min.Ticks, max.Ticks);
                }
            }
        }

        private class DayViewModel : DateTimeSelectableFieldViewModel
        {
            internal override void SetMinMaxDateTime(
                DateTime min, DateTime max, DateTime current)
            {
                int year = current.Year;
                int month = current.Month;
                UpdateDays(year, month);

                foreach (DateTimeValueViewModel item in Items)
                {
                    int day = item.Value;
                    DateTime dayMin = new DateTime(year, month, day, 0, 0, 0);
                    DateTime dayMax = new DateTime(year, month, day, 23, 59, 59);
                    item.IsEnabled = Intersects(
                        dayMin.Ticks, dayMax.Ticks, min.Ticks, max.Ticks);
                }
            }

            public void UpdateDays(int year, int month)
            {
                int days = DateTime.DaysInMonth(year, month);
                if (days != Items.Count)
                {
                    List<DateTimeValueViewModel> models =
                        CreateIntegerRangeValues(1, days);
                    if (Selected != null && models.Count < Selected.Value)
                    {
                        Selected = models[models.Count - 1];
                    }
                    Items = models;
                }
            }
        }

        private class HourViewModel : DateTimeSelectableFieldViewModel
        {
            public HourViewModel()
                : base(CreateIntegerRangeValues(0, 24))
            {
            }

            internal override void SetMinMaxDateTime(
                DateTime min, DateTime max, DateTime current)
            {
                int year = current.Year;
                int month = current.Month;
                int day = current.Day;

                foreach (DateTimeValueViewModel item in Items)
                {
                    int hour = item.Value;
                    DateTime hourMin = new DateTime(year, month, day, hour, 0, 0);
                    DateTime hourMax = new DateTime(year, month, day, hour, 59, 59);
                    item.IsEnabled = Intersects(
                        hourMin.Ticks, hourMax.Ticks, min.Ticks, max.Ticks);
                }
            }
        }

        private class MinuteViewModel : DateTimeSelectableFieldViewModel
        {
            public MinuteViewModel()
                : base(CreateIntegerRangeValues(0, 60))
            {
            }

            internal override void SetMinMaxDateTime(
                DateTime min, DateTime max, DateTime current)
            {
                int year = current.Year;
                int month = current.Month;
                int day = current.Day;
                int hour = current.Hour;

                foreach (DateTimeValueViewModel item in Items)
                {
                    int minute = item.Value;
                    DateTime minuteMin = new DateTime(
                        year, month, day, hour, minute, 0);
                    DateTime minuteMax = new DateTime(
                        year, month, day, hour, minute, 59);
                    item.IsEnabled = Intersects(
                        minuteMin.Ticks, minuteMax.Ticks, min.Ticks, max.Ticks);
                }
            }
        }

        private class SecondViewModel : DateTimeSelectableFieldViewModel
        {
            public SecondViewModel()
                : base(CreateIntegerRangeValues(0, 60))
            {
            }

            internal override void SetMinMaxDateTime
                (DateTime min, DateTime max, DateTime current)
            {
                int year = current.Year;
                int month = current.Month;
                int day = current.Day;
                int hour = current.Hour;
                int minute = current.Minute;

                foreach (DateTimeValueViewModel item in Items)
                {
                    int second = item.Value;
                    DateTime secondMin = new DateTime(
                        year, month, day, hour, minute, second);
                    DateTime secondMax = new DateTime(
                        year, month, day, hour, minute, second);
                    item.IsEnabled = Intersects(
                        secondMin.Ticks, secondMax.Ticks, min.Ticks, max.Ticks);
                }
            }
        }
        #endregion
    }
}
