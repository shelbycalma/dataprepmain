﻿using System;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    internal class MonthValueViewModel : DateTimeValueViewModel
    {
        public MonthValueViewModel(int month, IFormatProvider culture)
            : base(month, ToLocalizedMonthName(month, culture))
        {
        }

        internal static string ToLocalizedMonthName(
            int monthIndex,
            IFormatProvider culture)
        {
            DateTime dateTime = new DateTime(
                DateTime.MinValue.Year,
                monthIndex,
                DateTime.MinValue.Day);
            string name = dateTime.ToString("MMM", culture);
            return name;
        }
    }
}
