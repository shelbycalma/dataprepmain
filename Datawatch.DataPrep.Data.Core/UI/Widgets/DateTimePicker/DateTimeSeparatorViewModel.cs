﻿namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    /// <summary>
    /// The separator is a DateTimeField, but a non-interactive one.
    /// It just displays a label/textblock.
    /// </summary>
    public class DateTimeSeparatorViewModel : DateTimeFieldViewModel
    {
        public DateTimeSeparatorViewModel(string separator)
        {
            Separator = separator;
        }

        public string Separator { get; private set; }
    }
}
