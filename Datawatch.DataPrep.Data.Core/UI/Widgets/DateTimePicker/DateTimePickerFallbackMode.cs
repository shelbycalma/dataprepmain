﻿namespace Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker
{
    /// <summary>
    /// This enum specifies how the <see cref="DateTimePicker"/> should handle
    /// the hidden fields, if they should be populated with the minimum valid
    /// value or the maximum.
    /// </summary>
    public enum DateTimePickerFallbackMode
    {
        /// <summary>
        /// Uses enabled interval minimum.
        /// </summary>
        MinValue,

        /// <summary>
        /// Uses enabled interval maximum.
        /// </summary>
        MaxValue
    }
}
