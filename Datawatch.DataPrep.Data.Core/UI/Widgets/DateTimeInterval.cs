using System;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets
{
    [Serializable]
    public class DateTimeInterval
    {
        public DateTimeInterval(DateTime min, DateTime max)
        {
            // no support for milliseconds
            if (min.Millisecond != 0)
            {
                min = new DateTime(
                    min.Year, min.Month, min.Day,
                    min.Hour, min.Minute, min.Second, 0);
            }
            if (max.Millisecond != 0)
            {
                max = new DateTime(
                    max.Year, max.Month, max.Day,
                    max.Hour, max.Minute, max.Second, 0);
                try
                {
                    max = max.AddSeconds(1);
                }
                catch (ArgumentOutOfRangeException)
                {
                    // Okey, tried to create time larger than MaxValue.
                    // Fix for issue 15335
                    max = DateTime.MaxValue;
                }
            }

            Min = min;
            Max = max;
        }

        public DateTime Min { get; private set; }
        public DateTime Max { get; private set; }

        public override string ToString()
        {
            return string.Format("[{0} -> {1}]", Min, Max);
        }

        public override bool Equals(object obj)
        {
            DateTimeInterval other = obj as DateTimeInterval;
            if (ReferenceEquals(null, other))
            {
                return false;
            }
            if (ReferenceEquals(this, other))
            {
                return true;
            }
            return Min == other.Min && Max == other.Max;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Min.GetHashCode() * 397) ^ Max.GetHashCode();
            }
        }
    }
}
