﻿using System.Windows;
using System.Windows.Controls;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets
{
    /// <summary>
    /// Interaction logic for DecimalSeparatorComboBox.xaml
    /// </summary>
    public partial class DecimalSeparatorComboBox : UserControl
    {
        public DecimalSeparatorComboBox()
        {
            InitializeComponent();
        }

        public int LabelMinWidth
        {
            get { return (int)GetValue(MinWidthProperty); }
            set { SetValue(MinWidthProperty, value); }
        }

        public static readonly DependencyProperty LabelMinWidthProperty =
            DependencyProperty.Register(
                "LabelMinWidth", typeof(int), typeof(DecimalSeparatorComboBox), new PropertyMetadata(0));
    }
}
