﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets
{
    /// <summary>
    /// Interaction logic for DateTimeFormatComboBox.xaml
    /// </summary>
    public partial class DateTimeFormatComboBox : UserControl
    {
        public static DependencyProperty SelectedFormatProperty =
            DependencyProperty.Register(
                "SelectedFormat",
                typeof(string),
                typeof(DateTimeFormatComboBox),
                new FrameworkPropertyMetadata()
                {
                    BindsTwoWayByDefault = true,
                    DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged,
                    CoerceValueCallback = SelectedFormatCoerce
                });

        private CompositeCollection dateFormatItems;

        public DateTimeFormatComboBox()
        {
            dateFormatItems = new CompositeCollection();
            foreach (string format in DateTimeFormatHelper.DateTimeFormats)
            {
                dateFormatItems.Add(format);
            }
            InitializeComponent();
        }

        public string SelectedFormat
        {
            get { return (string)GetValue(SelectedFormatProperty); }
            set { SetValue(SelectedFormatProperty, value); }
        }

        private static object SelectedFormatCoerce(DependencyObject obj, object o)
        {
            string s = o as string;
            if (string.IsNullOrEmpty(s))
                return FormatHelper.GetDefaultTimeFormat();
            return s;
        }

        public CompositeCollection FormatSelections
        {
            get { return dateFormatItems; }
        }
    }
}