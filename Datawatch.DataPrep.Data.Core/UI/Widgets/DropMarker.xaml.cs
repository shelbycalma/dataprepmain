﻿using System.Windows;
using System.Windows.Controls;

namespace Datawatch.DataPrep.Data.Core.UI.Widgets
{
    /// <summary>
    /// Interaction logic for DropMarker.xaml
    /// </summary>
    public partial class DropMarker : UserControl
    {
        public static readonly DependencyProperty OrientationProperty =
            DependencyProperty.Register("Orientation",
                typeof(Orientation), typeof(DropMarker),
                new FrameworkPropertyMetadata(Orientation.Vertical,
                    FrameworkPropertyMetadataOptions.AffectsMeasure,
                    null, null));


        public DropMarker()
        {
            InitializeComponent();
            DataContext = this;
        }


        public Orientation Orientation
        {
            get
            {
                return (Orientation)GetValue(OrientationProperty);
            }
            set
            {
                SetValue(OrientationProperty, value);
            }
        }
    }
}
