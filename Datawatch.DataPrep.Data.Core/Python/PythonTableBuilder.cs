﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Python.Properties;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Python
{
    internal static class PythonTableBuilder
    {
        private static readonly HashSet<string> numericTypes = new HashSet<string>(new[] { "Int32", "Int64", "Double", "Decimal" });

        private static readonly string[] iso8601Formats =
        {
            "yyyy-MM-ddTHH:mm:ss.ffffff",
            "yyyy-MM-ddTHH:mm:ss",
            "yyyy-MM-dd"
        };

        public static IList<Hashtable> ConverToPythonTable(ITable table)
        {
            IList<Hashtable> result = new List<Hashtable>();
            string[] columnNames = new string[table.ColumnCount];
            for (int i = 0; i < table.ColumnCount; i++) {
                columnNames[i] = table.GetColumn(i).Name;
            }
            ColumnReader[] readers = ColumnReader.ForTable(table);

            for (int row = 0; row < table.RowCount; row++)
            {
                Hashtable hashTable = new Hashtable();
                for (var columnIndex = 0; columnIndex < table.ColumnCount; columnIndex++)
                {
                    object value = readers[columnIndex].GetValue(row);
                    hashTable.Add(columnNames[columnIndex], value);
                }

                result.Add(hashTable);
            }

            return result;
        }

        public static RowLimitTable ConvertFromPythonTable(
            object pythonTableObject, 
            Dictionary<string, ColumnType> initialColumns, 
            int rowCount, 
            Exception error)
        {
            RowLimitTable table = new RowLimitTable();
            ConvertFromPythonTable(pythonTableObject, initialColumns, rowCount, 
                error, table);
            return table;
        }

        public static void ConvertFromPythonTable(
            object pythonTableObject, 
            Dictionary<string, ColumnType> initialColumns, 
            int rowCount, Exception error, RowLimitTable table)
        {
            IList<Dictionary<string, object>> pythonTable = 
                ValidatePythonTable(pythonTableObject);

            table.Error = error;

            table.BeginUpdate();
            try
            {
                CreateColumns(table, pythonTable, initialColumns);

                int rowNum = 0;
                foreach (Dictionary<string, object> row in pythonTable)
                {
                    object[] values = new object[table.ColumnCount];

                    for (int i = 0; i < table.ColumnCount; i++)
                    {
                        Column column = table.GetColumn(i);
                        string columnName = column.Name;
                        if (!row.ContainsKey(columnName)) continue;
                        object value = row[columnName];

                        if (!(column is TextColumn) && value is string)
                        {
                            string s = (string) value;
                            if ( s.Length == 0)
                            {
                                value = null;
                            }
                            else if (column is TimeColumn)
                            {
                                object dateValue = TryParseDate(s);
                                if (dateValue != null)
                                {
                                    value = dateValue;
                                }
                            }
                        }

                        values[i] = value;
                    }

                    table.AddRow(values);

                    rowNum++;
                    if (rowNum == rowCount) break;
                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        private static IList<Dictionary<string, object>> ValidatePythonTable(object pythonTable)
        {
            if (!(pythonTable is IList))
                throw new ArgumentException(Resources.ExPythonWrongResultTableFormat);

            IList<Dictionary<string, object>> result = new List<Dictionary<string, object>>();

            foreach (object row in pythonTable as IList)
            {
                if (!(row is Hashtable))
                    throw new ArgumentException(Resources.ExPythonWrongResultTableFormat);

                Hashtable hashtable = row as Hashtable;

                if (hashtable.Count == 0)
                    throw new ArgumentException(Resources.ExPythonEmptyRow);

                Dictionary<string, object> dict = new Dictionary<string, object>();

                foreach (object key in hashtable.Keys)
                {
                    if(!(key is string))
                        throw new ArgumentException(Resources.ExPythonInvalidDictKey);
                    dict.Add(key as string, hashtable[key]);
                }

                result.Add(dict);
            }

            return result;
        }

        private static void CreateColumns(StandaloneTable table, 
            IList<Dictionary<string, object>> pythonTable, 
            Dictionary<string, ColumnType> initialColumns)
        {
            IEnumerable<KeyValuePair<string, ColumnType>> columns = 
                GetColumnTypes(pythonTable, initialColumns);

            foreach (KeyValuePair<string, ColumnType> column in columns)
            {
                if (table.GetColumn(column.Key) != null)
                {
                    continue;
                }
                switch (column.Value)
                {
                    case ColumnType.Time:
                        table.AddColumn(new TimeColumn(column.Key));
                        break;
                    case ColumnType.Numeric:
                        table.AddColumn(new NumericColumn(column.Key));
                        break;
                    case ColumnType.Text:
                        table.AddColumn(new TextColumn(column.Key));
                        break;
                }
            }
        }

        private static IEnumerable<KeyValuePair<string, ColumnType>> GetColumnTypes(
            IList<Dictionary<string, object>> pythonTable, 
            Dictionary<string, ColumnType> initialColumns)
        {
            // If empty result return input table columns.
            if (pythonTable.Count == 0)
            {
                return initialColumns;
            }

            IDictionary<string, ColumnType?> pythonTableColumnTypes = new Dictionary<string, ColumnType?>();
            
            //Check values for the first row
            Dictionary<string, object> row = pythonTable[0];
            foreach (string key in row.Keys)
            {
                ColumnType? columnType = GetValueType(row[key]);
                if (!pythonTableColumnTypes.ContainsKey(key))
                {
                    pythonTableColumnTypes.Add(key, columnType);
                }
                else
                {
                    if (columnType == null)
                        continue;

                    if (pythonTableColumnTypes[key] == null)
                    {
                        pythonTableColumnTypes[key] = columnType;
                        continue;
                    }

                    if (pythonTableColumnTypes[key] != columnType)
                    {
                        pythonTableColumnTypes[key] = ColumnType.Text;
                    }
                }
            }

            return GetOrderedColumns(pythonTableColumnTypes, initialColumns);
        }

        private static IEnumerable<KeyValuePair<string, ColumnType>> GetOrderedColumns(
            IDictionary<string, ColumnType?> pythonTableColumnTypes, 
            Dictionary<string, ColumnType> initialColumns)
        {
            IList<KeyValuePair<string, ColumnType>> result = new List<KeyValuePair<string, ColumnType>>();
            foreach (var initialColumn in initialColumns.Keys)
            {
                if (pythonTableColumnTypes.ContainsKey(initialColumn))
                {
                    result.Add(new KeyValuePair<string, ColumnType>(
                        initialColumn, 
                        pythonTableColumnTypes[initialColumn] ?? ColumnType.Text));
                }
            }



            HashSet<string> newColumns = new HashSet<string>(pythonTableColumnTypes.Keys);
            newColumns.ExceptWith(initialColumns.Keys);

            foreach (var newColumn in newColumns)
            {
                result.Add(new KeyValuePair<string, ColumnType>(newColumn, pythonTableColumnTypes[newColumn] ?? ColumnType.Text));
            }

            return result;
        }

        private static ColumnType? GetValueType(object value)
        {
            if (value == null)
                return null;

            if (value is DateTime)
                return ColumnType.Time;

            if(numericTypes.Contains(value.GetType().Name))
                return ColumnType.Numeric;

            if (value is string)
            {
                if (string.IsNullOrEmpty((string)value))
                {
                    return null;
                }
                if(TryParseDate((string)value) != null)
                { 
                    return ColumnType.Time;
                }
                return ColumnType.Text;
            }
                
            throw new ArgumentException(string.Format(Resources.ExPythonUnsupportedType, value.GetType().Name));
        }

        private static object TryParseDate(string value )
        {
            foreach (string format in iso8601Formats)
            {
                DateTime dateTime;
                if (DateTime.TryParseExact(value,
                    format, CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out dateTime))
                {
                    return dateTime;
                }
            }
            return null;
        }
    }
}
