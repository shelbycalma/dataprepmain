﻿using System;
using System.Diagnostics;
using System.Management;
using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core.Python
{
    public class PythonHelper
    {
        private static string startNamedServer = "start_Python_connectivity.bat";

        private static Process pythonNamedServerProcess = null;
        public static void StartConnectivity()
        {
            try
            {
                pythonNamedServerProcess = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = startNamedServer,
                        Arguments = "w",
                        RedirectStandardOutput = true,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };
                pythonNamedServerProcess.OutputDataReceived +=
                    Proc_OutputDataReceived;
                pythonNamedServerProcess.Start();
                pythonNamedServerProcess.BeginOutputReadLine();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }

        public static void StopConnectivity()
        {
            try
            {
                if (pythonNamedServerProcess != null)
                {
                    KillProcessAndChildren(pythonNamedServerProcess.Id);
                }
            }
            catch (Exception ex)
            {
                Log.Error(string.Format(
                    Properties.Resources.LogPythonConnectivityFailed, ex.Message));
            }
        }

        private static void Proc_OutputDataReceived(
            object sender, DataReceivedEventArgs e)
        {
            Log.Info(Properties.Resources.LogPythonConnectivityIdentifier
                + e.Data);
        }

        private static void KillProcessAndChildren(int pid)
        {
            using (var searcher = new ManagementObjectSearcher
                ("Select * From Win32_Process Where ParentProcessID=" + pid))
            {
                var moc = searcher.Get();
                foreach (ManagementObject mo in moc)
                {
                    KillProcessAndChildren(Convert.ToInt32(mo["ProcessID"]));
                }
                try
                {
                    var proc = Process.GetProcessById(pid);
                    proc.Kill();
                }
                catch (Exception)
                {
                    // Process already exited.
                }
            }
        }

    }
}
