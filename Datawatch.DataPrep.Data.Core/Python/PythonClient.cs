﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Python.Properties;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Razorvine.Pyro;
using System.Security;

namespace Datawatch.DataPrep.Data.Core.Python
{
    public class PythonClient
    {
        private readonly string host;
        private readonly int port;
        private readonly string script;
        private readonly SecureString password;

        private const string RemoteObjectName = "Server";
        private const string RemoteEndPointMethod = "execute";
        private const string LineSeparator = "\n";
        
        public PythonClient(PythonSettings settings, IEnumerable<ParameterValue> parameters)
        {
            host = ApplyParameters(settings.Host, parameters, false);
            string portString = ApplyParameters(settings.Port, parameters, false);
            if (!int.TryParse(portString, out port))
            {
                throw new ArgumentException(Resources.ExPythonInvalidPort);
            }

            string parameterName = ApplyParameters(settings.ParameterName, parameters, false);
            script = ApplyParameters(settings.Script, parameters, settings.EncloseParametersInQuotes);

            if (!string.IsNullOrWhiteSpace(script))
            {
                script = ReplaceLineEndings(script);
                script = CreateScriptWrapperMethod(script, parameterName, PythonSettings.DefaultParameterName);
            }
            password = SecurityUtils.ToSecureString(ApplyParameters(SecurityUtils.ToInsecureString(settings.Password), parameters, false));

            //Config.RECIEVE_TIMEOUT = settings.Timeout * 1000;
            Config.SERIALIZER = Config.SerializerType.serpent;
        }

        public RowLimitTable ExcuteScript(ITable table)
        {
            return ExcuteScript(table, 0);
        }

        public RowLimitTable ExcuteScript(int rowCount)
        {
            return ExcuteScript(null, rowCount);
        }

        public RowLimitTable ExcuteScript(ITable table, int rowCount)
        {
            if (table == null)
            {
                table = new RowLimitTable();
            }

            Log.Info(Resources.LogPythonRequestStarted, host, port, script);

            DateTime startTime = DateTime.Now;
            byte[] hmacKey = null;
            if (!string.IsNullOrWhiteSpace(SecurityUtils.ToInsecureString(password)))
            {
                hmacKey = Encoding.UTF8.GetBytes(SecurityUtils.ToInsecureString(password));
            }           

            using (NameServerProxy ns = NameServerProxy.locateNS(host, port, hmacKey))
            using (PyroProxy remoteobject = new PyroProxy(ns.lookup(RemoteObjectName)))
            {
                object response = remoteobject.call(RemoteEndPointMethod, 
                    PythonTableBuilder.ConverToPythonTable(table), script);

                IErrorProducer errorProducer = TableUtils.FindErrorProducerInPipeline(table);
                Exception error = errorProducer != null ? errorProducer.Error : null;
                table = PythonTableBuilder.ConvertFromPythonTable(response, 
                    GetColumns(table), rowCount, error);
            }

            Log.Info(Resources.LogPythonRequestEnded, (DateTime.Now - startTime).TotalSeconds, table.ColumnCount, table.RowCount);

            return (RowLimitTable)table;
        }

        private static Dictionary<string, ColumnType> GetColumns(ITable table)
        {
            Dictionary<string, ColumnType> columns = new Dictionary<string, ColumnType>();
            for (int i = 0; i < table.ColumnCount; i++)
            {
                IColumn col = table.GetColumn(i);
                columns.Add(col.Name, GetColumnType(col));
            }
            return columns;
        }

        private static ColumnType GetColumnType(IColumn col)
        {
            if (col is INumericColumn)
            {
                return ColumnType.Numeric;
            }
            if (col is ITimeColumn)
            {
                return ColumnType.Time;
            }
            return ColumnType.Text;
        }

        public void ExcuteScript(ITable table, bool realTime, int rowCount, 
            RowLimitTable resultTable)
        {
            if (table == null)
            {
                table = new RowLimitTable();
            }

            Log.Info(Resources.LogPythonRequestStarted, host, port, script);

            DateTime startTime = DateTime.Now;
            byte[] hmacKey = null;
            if (!string.IsNullOrWhiteSpace(SecurityUtils.ToInsecureString(password)))
            {
                hmacKey = Encoding.UTF8.GetBytes(SecurityUtils.ToInsecureString(password));
            }

            using (NameServerProxy ns = NameServerProxy.locateNS(host, port, hmacKey))
            using (PyroProxy remoteobject = new PyroProxy(ns.lookup(RemoteObjectName)))
            {
                try
                {
                    object response = remoteobject.call(RemoteEndPointMethod,
                        PythonTableBuilder.ConverToPythonTable(table), script);

                    IErrorProducer errorProducer =
                        TableUtils.FindErrorProducerInPipeline(table);
                    Exception error = errorProducer != null
                        ? errorProducer.Error
                        : null;
                    PythonTableBuilder.ConvertFromPythonTable(response,
                        GetColumns(table), rowCount, error,
                        resultTable);
                }
                catch (Exception e)
                {
                    resultTable.Error = e;
                    if (!realTime)
                    {
                        throw e;
                    }
                }
            }

            Log.Info(Resources.LogPythonRequestEnded, 
                (DateTime.Now - startTime).TotalSeconds, table.ColumnCount, 
                table.RowCount);
        }

        public void TestConnection()
        {
            byte[] hmacKey = null;
            if (!string.IsNullOrWhiteSpace(SecurityUtils.ToInsecureString(password)))
            {
                hmacKey = Encoding.UTF8.GetBytes(SecurityUtils.ToInsecureString(password));
            }

            using (NameServerProxy ns = NameServerProxy.locateNS(host, port, hmacKey))
            using (new PyroProxy(ns.lookup(RemoteObjectName))) { }
        }
        
        private static string CreateScriptWrapperMethod(string script, string parameterName, string defaultParameterName)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("def __wrappermethod({0}):", parameterName).Append(LineSeparator);
            sb.AppendFormat("\t{0}", script.Replace(LineSeparator, LineSeparator + "\t")).Append(LineSeparator);
            sb.AppendFormat("\treturn {0}", parameterName).Append(LineSeparator);
            sb.AppendFormat("result = __wrappermethod({0})", defaultParameterName).Append(LineSeparator);
            sb.AppendFormat("if result is None:").Append(LineSeparator);
            sb.AppendFormat("\tresult={0}", defaultParameterName).Append(LineSeparator);
            sb.AppendFormat("{0}=result", defaultParameterName).Append(LineSeparator);
            return sb.ToString();
        }

        private static string ApplyParameters(string source, IEnumerable<ParameterValue> parameters, bool encloseParameterInQuote)
        {
            if (source == null)
                return null;

            if (parameters == null || !parameters.Any())
                return source;

            return new ParameterEncoder
            {
                SourceString = source,
                Parameters = parameters,
                NullOrEmptyString = "NULL",
                EnforceSingleQuoteEnclosure = encloseParameterInQuote,
            }.Encoded();
        }

        private static string ReplaceLineEndings(string input)
        {
            if (input.Contains("\r\n"))
            {
                input = input.Replace("\r\n", LineSeparator);
            }
            if (input.Contains("\r"))
            {
                input = input.Replace("\r", LineSeparator);
            }

            return input;
        }
    }
}
