﻿using System.Security;

namespace Datawatch.DataPrep.Data.Core.Python
{
    public class PythonSettings
    {
        public bool IsEnabled { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public SecureString Password { get; set; }
        public string Script { get; set; }
        public bool EncloseParametersInQuotes { get; set; }
        public int Timeout { get; set; }
        public string ParameterName { get; set; }

        public const string DefaultParameterName = "table";
    }
}
