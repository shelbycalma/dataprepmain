﻿using System;
using System.Globalization;

namespace Datawatch.DataPrep.Data.Core
{
    public class FormatHelper
    {
        /// <summary>
        /// Get the default format string to use when formatting time values.
        /// </summary>
        /// <returns>The default format.</returns>
        /// <remarks>
        /// <para>This string should be used to format <see cref="DateTime"/>
        /// values (from time columns or time series ticks, c.f. text and
        /// numeric) when presenting them in the UI if a format hasn't been
        /// set explicitly by the user.</para>
        /// </remarks>
        public static string GetDefaultTimeFormat()
        {
            DateTimeFormatInfo info = DateTimeFormatInfo.CurrentInfo;
            if (info == null)
            {
                info = DateTimeFormatInfo.InvariantInfo;
            }
            return info.ShortDatePattern;
        }
    }
}
