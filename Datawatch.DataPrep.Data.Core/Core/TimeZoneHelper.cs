﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core
{
    public class TimeZoneHelper : PropertyBagViewModel
    {
        public TimeZoneHelper(PropertyBag bag)
            :base(bag)
        {
            if (bag == null)
            {
                throw Exceptions.ArgumentNull("bag");
            }
        }

        public string SelectedTimeZone
        {
            get { return GetInternal("SelectedTimeZone"); }
            set { SetInternal("SelectedTimeZone", value); }
        }

        public static DateTime ConvertFromUTC(DateTime sourceDate, string timeZone)
        {
            if (string.IsNullOrEmpty(timeZone) ||
                sourceDate.Kind == DateTimeKind.Local ||
                sourceDate == DateTime.MinValue ||
                sourceDate == DateTime.MaxValue)
            {
                return sourceDate;
            }

            TimeZoneInfo timeZoneInfo =
                TimeZoneInfo.FindSystemTimeZoneById(timeZone);

            if (timeZoneInfo == null)
            {
                return sourceDate;
            }

            DateTime adjustedDateTime =
                TimeZoneInfo.ConvertTimeFromUtc(sourceDate,timeZoneInfo);

            return adjustedDateTime;
        }

        public static DateTime ConvertToUTC(DateTime sourceDate, string timeZone)
        {
            if (string.IsNullOrEmpty(timeZone) ||
                sourceDate.Kind == DateTimeKind.Utc ||
                sourceDate == DateTime.MinValue ||
                sourceDate == DateTime.MaxValue)
            {
                return sourceDate;
            }
            
            TimeZoneInfo timeZoneInfo =
                TimeZoneInfo.FindSystemTimeZoneById(timeZone);

            if (timeZoneInfo == null)
            {
                return sourceDate;
            }
            else if (timeZoneInfo.IsInvalidTime(sourceDate) ||
                (sourceDate.Kind == DateTimeKind.Local &&
                timeZoneInfo != TimeZoneInfo.Local))                
            {
                return sourceDate;
            }

            DateTime adjustedDateTime =
                TimeZoneInfo.ConvertTimeToUtc(sourceDate, timeZoneInfo);

            return adjustedDateTime;
        }

        public DateTime ConvertFromUTC(DateTime sourceDate)
        {
            string timeZone = SelectedTimeZone;
            if(string.IsNullOrEmpty(timeZone))
            {
                return sourceDate;
            }
            return ConvertFromUTC(sourceDate, timeZone);
        }

        public DateTime ConvertToUTC(DateTime sourceDate)
        {
            string timeZone = SelectedTimeZone;
            if (string.IsNullOrEmpty(timeZone))
            {
                return sourceDate;
            }

            return ConvertToUTC(sourceDate, timeZone);
        }

        public bool TimeZoneSelected
        {
            get { return !string.IsNullOrEmpty(SelectedTimeZone); }
        }

        public PropertyBag GetPropertyBag()
        {
            return propertyBag;
        }

        public static string[] TimeZones
        {
            get
            {
                #region TimeZones

                List<string> tzs = new List<string>();

                tzs.Add("");
                foreach (TimeZoneInfo z in TimeZoneInfo.GetSystemTimeZones())
                {
                    tzs.Add(z.Id);
                }
                
                return tzs.ToArray();              

                #endregion
            }
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            TimeZoneHelper ot = (TimeZoneHelper)other;
            return Equals(ot.SelectedTimeZone, SelectedTimeZone);
        }

        public override int GetHashCode()
        {
            if (!string.IsNullOrEmpty(SelectedTimeZone))
            {
                return SelectedTimeZone.GetHashCode();
            }
            return 0;
        }
    }
}
