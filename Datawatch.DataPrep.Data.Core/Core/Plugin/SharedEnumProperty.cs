﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedEnumProperty<T> : SharedProperty
    {
        private readonly SharedStringProperty stringProperty;

        public SharedEnumProperty(string path) : base(path)
        {
            stringProperty = new SharedStringProperty(path);
        }

        public bool IsSet(PropertyBag bag)
        {
            return stringProperty.IsSet(bag);
        }

        public T GetValue(PropertyBag bag)
        {
            string s = stringProperty.GetString(bag);
            T foundEnum = (T)Enum.Parse(typeof(T), s);
            return foundEnum;
        }

        public T GetValue(PropertyBag bag, T defaultValue)
        {
            return IsSet(bag) ? GetValue(bag) : defaultValue;
        }

        public void SetValue(PropertyBag bag, T value)
        {
            stringProperty.SetString(bag, value.ToString());
        }
    }
}
