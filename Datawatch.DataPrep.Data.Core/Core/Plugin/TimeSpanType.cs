﻿namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public enum TimeSpanType
    {
        DateTime,
        Date,
        Time,
        None
    }
}
