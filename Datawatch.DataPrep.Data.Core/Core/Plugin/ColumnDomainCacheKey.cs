﻿using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class ColumnDomainCacheKey
    {
        private readonly string connectionString;
        private readonly SchemaAndTable schemaAndTable;
        private readonly string sourceQuery;
        private readonly string column;
        private readonly string predicate;

        public ColumnDomainCacheKey(string connectionString,
            SchemaAndTable schemaAndTable, string sourceQuery, string column,
            string predicate)
        {
            this.connectionString = connectionString;
            this.schemaAndTable = schemaAndTable;
            this.sourceQuery = sourceQuery;
            this.column = column;
            this.predicate = predicate;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(ColumnDomainCacheKey)) return false;
            return Equals((ColumnDomainCacheKey)obj);
        }

        public bool Equals(ColumnDomainCacheKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return
                Equals(other.connectionString, connectionString) &&
                Equals(other.schemaAndTable, schemaAndTable) &&
                Equals(other.sourceQuery, sourceQuery) &&
                Equals(other.column, column) &&
                Equals(other.predicate, predicate);
        }

        public override int GetHashCode()
        {
            int result = 17;
            if (connectionString != null)
            {
                result = result * 23 + connectionString.GetHashCode();
            }
            if (schemaAndTable != null)
            {
                result = result * 23 + schemaAndTable.GetHashCode();
            }
            if (sourceQuery != null)
            {
                result = result * 23 + sourceQuery.GetHashCode();
            }
            if (column != null)
            {
                result = result * 23 + column.GetHashCode();
            }
            if (predicate != null)
            {
                result = result * 23 + predicate.GetHashCode();
            }
            return result;
        }
    }
}
