﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Core.Reports;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class QuerySettings<T> : PropertyBagViewModel, ISchemaColumnsProvider<T> where T : DatabaseColumn
    {
        private const string selectedColumnsKey = "SelectedColumns";
        private const string filteredColumnsKey = "FilteredColumns";
        private const string predicateColumnsKey = "PredicateColumns";
        private readonly ObservableCollection<DatabaseColumn> filteredColumns =
            new ObservableCollection<DatabaseColumn>();
        private readonly ObservableCollection<DatabaseColumn> selectedColumns =
            new ObservableCollection<DatabaseColumn>();
        private readonly List<DatabaseColumn> predicateColumns = new List<DatabaseColumn>();
        private bool isQueryMode;
        private readonly TimeZoneHelper timeZoneHelper;
        protected bool defaultIsOnDemand = true;
        private bool applyRowFilteration = true;
        private PluginHostSettingsViewModel pluginHostSettings;
        private SchemaAndTable selectedTable;
        private SchemaColumnsSettings<T> schemaColumnSettings;
        private string selectedReportGroup;
        private List<string> reportGroups;
        private Report selectedReport;
        private List<Report> reportList;
        private string selectedReportID = "";
        private string selectedReportName = "";

        public QuerySettings(PropertyBag bag) : base(bag)
        {
            PropertyBag selectedColumnsBag = bag.SubGroups[selectedColumnsKey];
            PropertyBag filteredColumnsBag = bag.SubGroups[filteredColumnsKey];

            if (selectedColumnsBag != null &&
                selectedColumnsBag.SubGroups.Count > 0)
            {
                PopulateSelectedColumns(bag,
                    selectedColumnsBag.SubGroups.Count);
            }

            if (filteredColumnsBag != null &&
                filteredColumnsBag.SubGroups.Count > 0)
                {
                    PopulateFilteredColumns(bag,
                    filteredColumnsBag.SubGroups.Count);
            }

            PopulatePredicateColumns(bag);

            //isQueryMode = QueryMode == QueryMode.Query;
            IsReportSupported = false;

            PropertyBag timeZoneHelperBag =
               bag.SubGroups["TimeZone"];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups["TimeZone"] = timeZoneHelperBag;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);
        }

        public bool AggregationRequired
        {
            get
            {
                string value = GetInternal("AggregationRequired",
                false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("AggregationRequired", value.ToString());
                OnPropertyChanged("IsOnDemandEnabled");
            }
        }

        public bool ApplyFilter
        {
            get
            {
                string value = GetInternal("ApplyFilter", false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("ApplyFilter", value.ToString());
            }
        }

        public bool AutoParameterize
        {
            get
            {
                string value = GetInternal("AutoParameterize",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("AutoParameterize", value.ToString());
            }
        }

        public bool IsOnDemand
        {
            get
            {
                string value = GetInternal("IsOnDemand",
                    defaultIsOnDemand.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("IsOnDemand", value.ToString());
                if (value)
                {
                    AggregationRequired = false;
                }
                OnPropertyChanged("IsOnDemandEnabled");
            }
        }

        public bool IsOnDemandEnabled
        {
            get
            {
                //if (IsQueryMode)
                if (QueryMode == QueryMode.Table)
                {
                    return !AggregationRequired;
                }
                return true;
            }
        }

        public bool IsReportSupported
        {
            get
            {
                string value = GetInternal("IsReportSupported",
                    defaultIsOnDemand.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("IsReportSupported", value.ToString());
                OnPropertyChanged("IsReportSupported");
            }
        }
        
        public List<DatabaseColumn> PredicateColumns
        {
            get { return predicateColumns; }
        }

        public List<string> ReportGroups
        {
            get
            {   return reportGroups; }
            set
            {
                reportGroups = value;
                OnPropertyChanged("ReportGroups");
            }
        }
        public List<Report> ReportList
        {
            get { return reportList; }
            set
            {
                reportList = value;
                OnPropertyChanged("ReportList");
            }
        }

        public Report SelectedReport
        {
            get
            {
                return selectedReport;
            }
            set
            {
                selectedReport = value;
                OnPropertyChanged("SelectedReport");
                OnPropertyChanged("SelectedReportID");
                OnPropertyChanged("SelectedReportName");
            }
        }

        public string SelectedReportID
        {
            get
            {
                return GetInternal("SelectedReportID");
            }
            set
            {
                SetInternal("SelectedReportID", value);
                OnPropertyChanged("SelectedReport");
                OnPropertyChanged("SelectedReportID");
                OnPropertyChanged("SelectedReportName");
            }
        }

        public string ErrorMessage
        {
            get
            {
                return GetInternal("ErrorMessage");
            }
            set
            {
                SetInternal("ErrorMessage", value.Length == 0 ? value : ErrorMessages.GetDisplayMessage(value));
            }
        }

        public string SelectedReportName
        {
            get
            {
                return GetInternal("SelectedReportName");
            }
            set
            {
                SetInternal("SelectedReportName", value);
                OnPropertyChanged("SelectedReport");
                OnPropertyChanged("SelectedReportID");
                OnPropertyChanged("SelectedReportName");
            }
        }

        public string SelectedReportGroup
        {
            get
            {
                return GetInternal("SelectedReportGroup");
            }
            set
            {
                SetInternal("SelectedReportGroup", value);
                selectedReportGroup = value;
                OnPropertyChanged("ReportGroups");
            }
        }

        public QueryMode QueryMode
        {
            get
            {
                // Handle backward compatibility, this property was earlier called
                // RetrievalMode inside CacheRetrievalSettingsBase.
                string retrievalMode = GetInternal("RetrievalMode");
                if (!string.IsNullOrEmpty(retrievalMode))
                {
                    SetInternal("QueryMode", retrievalMode);
                }

                return (QueryMode)
                    Enum.Parse(typeof(QueryMode),
                        GetInternal("QueryMode", "Table"));
            }
            set
            {
                SetInternal("QueryMode", value.ToString());
                OnPropertyChanged("QueryMode");
            }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public string ReportSelectQuery
        {
            get { return GetInternal("ReportSelectQuery"); }
            set { SetInternal("ReportSelectQuery", value); }
        }

        public string ReportWhereQuery
        {
            get { return GetInternal("ReportWhereQuery"); }
            set { SetInternal("ReportWhereQuery", value); }
        }

        public string ReportOrderByQuery
        {
            get { return GetInternal("ReportOrderByQuery"); }
            set { SetInternal("ReportOrderByQuery", value); }
        }

        public string ReportListSelectQuery
        {
            get { return GetInternal("ReportListSelectQuery"); }
            set { SetInternal("ReportListSelectQuery", value); }
        }

        public string ReportListWhereQuery
        {
            get { return GetInternal("ReportListWhereQuery"); }
            set { SetInternal("ReportListWhereQuery", value); }
        }

        public string ReportListOrderByQuery
        {
            get { return GetInternal("ReportListOrderByQuery"); }
            set { SetInternal("ReportListOrderByQuery", value); }
        }

        public string ReportGroupListSelectQuery
        {
            get { return GetInternal("ReportGroupListSelectQuery"); }
            set { SetInternal("ReportGroupListSelectQuery", value); }
        }

        public string ReportGroupListWhereQuery
        {
            get { return GetInternal("ReportGroupListWhereQuery"); }
            set { SetInternal("ReportGroupListWhereQuery", value); }
        }

        public string ReportGroupListOrderByQuery
        {
            get { return GetInternal("ReportGroupListOrderByQuery"); }
            set { SetInternal("ReportGroupListOrderByQuery", value); }
        }

        public bool IsConstrainByDateTime
        {
            get
            {
                string s = GetInternal("IsConstrainByDateTime");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IsConstrainByDateTime", Convert.ToString(value));
            }
        }

        public string SelectedDatetimeColumn
        {
            get { return GetInternal("SelectedDatetimeColumn"); }
            set
            {
                SetInternal("SelectedDatetimeColumn", value);
            }
        }

        public string FromDate
        {
            get { return GetInternal("FromDate"); }
            set
            {
                SetInternal("FromDate", value);
            }
        }

        public string ToDate
        {
            get { return GetInternal("ToDate"); }
            set
            {
                SetInternal("ToDate", value);
            }
        }

        public virtual SchemaAndTable SelectedTable
        {
            get { return selectedTable; }
            set
            {
                selectedTable = value;

                SelectedColumns.Clear();
                FilteredColumns.Clear();
                if (PredicateColumns != null)
                {
                    PredicateColumns.Clear();
                }

                OnPropertyChanged("SelectedTable");
            }
        }

        public ObservableCollection<DatabaseColumn> SelectedColumns
        {
            get { return selectedColumns; }
        }

        public ObservableCollection<DatabaseColumn> FilteredColumns
        {
            get { return filteredColumns; }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return timeZoneHelper; }
        }

        public bool IsQueryMode
        {
            get { return isQueryMode; }
            set
            {
                if (value == isQueryMode) return;
                isQueryMode = value;
                QueryMode = value
                    ? QueryMode.Query : QueryMode.Table;
                OnPropertyChanged("IsQueryMode");
                OnPropertyChanged("IsOnDemandEnabled");
            }
        }

        public SqlDialect SqlDialect
        {
            get
            {
                string name = GetInternal("SqlDialect");
                SqlDialect dialect = SqlDialectFactory.GetDialectByName(name);
                if (dialect != null)
                    return dialect;
                    
                return SqlDialectFactory.AnsiSql;
            }
            set
            {
                SetInternal("SqlDialect", value.Name);
            }
        }

        public bool ApplyRowFilteration
        {
            get { return applyRowFilteration; }
            set { applyRowFilteration = value; }
        }

        public PluginHostSettingsViewModel PluginHostSettings
        {
            get { return pluginHostSettings; }
            set { pluginHostSettings = value; }
        }

        public string LoadTablesCaption
        {
            get { return GetInternal("LoadTablesCaption"); }
            set { SetInternal("LoadTablesCaption", value); }
        }

        public string TablesFilter
        {
            get { return GetInternal("TablesFilter"); }
            set { SetInternal("TablesFilter", value); }
        }

        public string ColumnsFilter
        {
            get { return GetInternal("ColumnsFilter"); }
            set { SetInternal("ColumnsFilter", value); }
        }
        
        public List<string> GetSelectedColumnNames() 
        {
            List<string> selectedColumns = new List<string>();
            foreach (DatabaseColumn column in SelectedColumns)
            {
                selectedColumns.Add(column.ColumnName);
            }
            return selectedColumns;
        }

        public virtual DatabaseColumn CreateColumn(string columnName,
            ColumnType columnType, string appliedParameterName,
            AggregateType aggregateType, FilterOperator filterOperator, string filterValue)
        {
            DatabaseColumn column = new DatabaseColumn();
            column.ColumnName = columnName;
            column.ColumnType = columnType;
            column.AppliedParameterName = appliedParameterName;
            column.AggregateType = aggregateType;
            column.FilterOperator = filterOperator;
            column.FilterValue = filterValue;

            return column;
        }

        public virtual DatabaseColumn CreateColumn(
            string rootPath, PropertyBag bag, int index)
        {
            DatabaseColumnProperties colProperties =
                    new DatabaseColumnProperties(rootPath, index);
            string colName = colProperties.ColumnName.GetString(bag);
            ColumnType colType = colProperties.ColumnType.GetValue(bag);
            string appliedParameterName =
                colProperties.AppliedParameterName.GetString(bag);
            AggregateType aggregateType =
                colProperties.AggregateType.GetValue(bag);
            FilterOperator filterOperator = colProperties.FilterOperator.GetValue(bag);
            string filterValue = colProperties.FilterValue.GetString(bag);

            DatabaseColumn column = CreateColumn(colName, colType,
                appliedParameterName, aggregateType, filterOperator, filterValue);

            return column;
        }

        public virtual void SetSelectedTable(string tableName)
        {
            selectedTable = SchemaAndTable.Parse(tableName);
            SetInternal("Table", tableName);
        }

        public void SetReportGroupList (List<string> reportGroups)
        {
            ReportGroups = reportGroups;
            OnPropertyChanged("ReportGroups");
        }

        public void SetReportList(List<Report> reportList)
        {
            ReportList = reportList;
            OnPropertyChanged("ReportList");
        }

        private void PopulatePredicateColumns(PropertyBag bag)
        {
            PredicateColumns.Clear();

            PropertyBag predicateColumnsBag =
                bag.SubGroups[predicateColumnsKey];

            if (predicateColumnsBag == null ||
                predicateColumnsBag.SubGroups.Count == 0)
            {
                return;
            }

            for (int i = 0; i < predicateColumnsBag.SubGroups.Count; i++)
            {
                DatabaseColumnProperties colProperties =
                    new DatabaseColumnProperties(predicateColumnsKey, i);
                string colName = colProperties.ColumnName.GetString(bag);
                ColumnType colType = colProperties.ColumnType.GetValue(bag);
                string appliedParameterName =
                    colProperties.AppliedParameterName.GetString(bag);
                AggregateType aggregateType =
                    colProperties.AggregateType.GetValue(bag, AggregateType.None);
                FilterOperator filterOperator =
                    colProperties.FilterOperator.GetValue(bag, FilterOperator.None);
                string filterValue = colProperties.FilterValue.GetString(bag);
                //TODO: should use overloaded CreateColumn method instead.
                DatabaseColumn column = CreateColumn(colName, colType,
                    appliedParameterName, aggregateType, filterOperator, filterValue);
                predicateColumns.Add(column);
            }
        }

        private void PopulateSelectedColumns(PropertyBag bag, int count)
        {
            SelectedColumns.Clear();
            for (int i = 0; i < count; i++)
            {
                DatabaseColumnProperties colProperties =
                    new DatabaseColumnProperties(selectedColumnsKey, i);
                string colName = colProperties.ColumnName.GetString(bag);
                ColumnType colType = colProperties.ColumnType.GetValue(bag);
                string appliedParameterName =
                    colProperties.AppliedParameterName.GetString(bag);
                AggregateType aggregateType =
                    colProperties.AggregateType.GetValue(bag);
                FilterOperator filterOperator =
                    colProperties.FilterOperator.GetValue(bag, FilterOperator.None);
                string filterValue = colProperties.FilterValue.GetString(bag);

                //TODO: should use overloaded CreateColumn method instead.
                DatabaseColumn column = CreateColumn(colName, colType,
                    appliedParameterName, aggregateType, filterOperator, filterValue);
                selectedColumns.Add(column);
            }
        }

        private void PopulateFilteredColumns(PropertyBag bag, int count)
        {
            FilteredColumns.Clear();
            for (int i = 0; i < count; i++)
            {
                DatabaseColumnProperties colProperties =
                    new DatabaseColumnProperties(filteredColumnsKey, i);
                string colName = colProperties.ColumnName.GetString(bag);
                ColumnType colType = colProperties.ColumnType.GetValue(bag);
                string appliedParameterName =
                    colProperties.AppliedParameterName.GetString(bag);
                AggregateType aggregateType =
                    colProperties.AggregateType.GetValue(bag);
                FilterOperator filterOperator =
                    colProperties.FilterOperator.GetValue(bag, FilterOperator.None);
                string filterValue = colProperties.FilterValue.GetString(bag);

                //TODO: should use overloaded CreateColumn method instead.
                DatabaseColumn column = CreateColumn(colName, colType,
                    appliedParameterName, aggregateType, filterOperator, filterValue);
                filteredColumns.Add(column);
            }
        }

        public virtual PropertyBag ToPropertyBag()
        {
            // Bag should be cleaned before serializing again.
            propertyBag.SubGroups[selectedColumnsKey] = null;

            if (QueryMode == QueryMode.Table && SelectedColumns != null
                && SelectedColumns.Count > 0)
            {
                int i = 0;
                foreach (DatabaseColumn col in SelectedColumns)
                {
                    ToDatabaseColumnProperties(col, selectedColumnsKey, i++);
                }
            }

            propertyBag.SubGroups[filteredColumnsKey] = null;

            if (QueryMode == QueryMode.Table && FilteredColumns != null
                && FilteredColumns.Count > 0)
            {
                int i = 0;
                foreach (DatabaseColumn col in FilteredColumns)
                {
                    ToDatabaseColumnProperties(col, filteredColumnsKey, i++);
                }
            }

            propertyBag.SubGroups[predicateColumnsKey] = null;

            if (QueryMode == QueryMode.Table && PredicateColumns != null
                && PredicateColumns.Count > 0)
            {
                int i = 0;
                foreach (DatabaseColumn col in PredicateColumns)
                {
                    ToDatabaseColumnProperties(col, predicateColumnsKey, i++);
                }
            }
            
            return propertyBag;
        }
                
        public virtual void ToDatabaseColumnProperties(DatabaseColumn column,
            string rootPath, int index)
        {
            // TODO: should move DatabaseColumnProperties inside DatabaseColumn
            // newly added properties aren't kept-up here.
            // Need to move DatabaseColumnProperties to Framework for that.
            DatabaseColumnProperties colProperties =
                new DatabaseColumnProperties(rootPath, index);
            colProperties.ColumnName.SetString(propertyBag,
                column.ColumnName);
            colProperties.ColumnType.SetValue(propertyBag,
                column.ColumnType);
            colProperties.AppliedParameterName.SetString(propertyBag,
                column.AppliedParameterName);
            colProperties.AggregateType.SetValue(propertyBag,
                column.AggregateType);
            colProperties.FilterOperator.SetValue(propertyBag,
                column.FilterOperator);
            colProperties.FilterValue.SetString(propertyBag,
                column.FilterValue);
        }

        public SchemaColumnsSettings<T> SchemaColumnsSettings
        {
            get
            {
                if (schemaColumnSettings == null)
                {
                    schemaColumnSettings = new SchemaColumnsSettings<T>(ToPropertyBag());
                }
                return schemaColumnSettings;
            }
        }

        public bool IsSchemaRequest { get; set; }
    }
}
