﻿using System;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class RowLimitTable : StandaloneTable, IErrorProducer
    {
        public Exception Error { get; set; }

        public int RowLimit { get; set; }
    }
}
