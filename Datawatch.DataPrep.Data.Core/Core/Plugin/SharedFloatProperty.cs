﻿using System.Xml;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedFloatProperty : SharedProperty
    {
        private readonly SharedStringProperty stringProperty;

        public SharedFloatProperty(string path)
            : base(path)
        {
            stringProperty = new SharedStringProperty(path);
        }

        public double GetValue(PropertyBag bag)
        {
            string s = stringProperty.GetString(bag);
            return XmlConvert.ToSingle(s);
        }

        public bool IsSet(PropertyBag bag)
        {
            return stringProperty.IsSet(bag);
        }

        public void SetValue(PropertyBag bag, float value)
        {
            stringProperty.SetString(bag, XmlConvert.ToString(value));
        }
    }
}
