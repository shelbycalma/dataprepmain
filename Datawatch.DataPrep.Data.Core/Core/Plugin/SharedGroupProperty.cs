﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedGroupProperty  : SharedProperty
    {
        public SharedGroupProperty(string path)
            : base(path)
        {
        }

        public bool IsSet(PropertyBag bag)
        {
            return bag != null && GetGroup(bag) != null;
        }

        public PropertyBag GetGroup(PropertyBag bag)
        {
            if (bag == null)
            {
                throw Exceptions.ArgumentNull("bag");
            }

            string[] pathParts = path.Split(new char[] { '.' });
            for (int i = 0; i < pathParts.Length; i++)
            {
                bag = bag.SubGroups[pathParts[i]];
                if (bag == null)
                {
                    return null;
                }
            }
            return bag;
        }

        public void SetGroup(PropertyBag bag, PropertyBag group)
        {
            if (bag == null)
            {
                throw Exceptions.ArgumentNull("bag");
            }

            string[] pathParts = path.Split(new char[] { '.' });
            int i;
            for (i = 0; i < pathParts.Length - 1; i++)
            {
                if (!bag.SubGroups.ContainsKey(pathParts[i]))
                {
                    bag.SubGroups[pathParts[i]] = new PropertyBag();
                }
                bag = bag.SubGroups[pathParts[i]];
            }
            bag.SubGroups[pathParts[i]] = group;
        }
    }
}
