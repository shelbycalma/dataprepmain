﻿using System;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class HostUriParser
    {
        private readonly int defaultPort;
        private string baseUri;

        public HostUriParser(int defaultPort)
        {
            this.defaultPort = defaultPort;
        }

        public HostUriParser(int defaultPort, string uri)
        {
            this.defaultPort = defaultPort;
            BaseUri = uri;
        }

        public string BaseUri
        {
            get { return baseUri; }
            set
            {
                if (value != baseUri)
                {
                    baseUri = value;
                    Port = ParsePort(baseUri);
                    Host = ParseHost(baseUri);
                }
            }
        }

        public int Port { get; private set; }
        public string Host { get; private set; }

        private int ParsePort(string baseUri)
        {
            if (string.IsNullOrEmpty(baseUri)) return defaultPort;

            // If there is no colon no port is specified
            int index = baseUri.LastIndexOf(':');
            if (index == -1) return defaultPort;

            string portAsString = baseUri.Substring(index + 1);
            try
            {
                int port = int.Parse(portAsString);
                return port;
            }
            catch (FormatException)
            {
                // If there is a format exception we might have
                // tried to parse to much of the url, i.e. BaseUri was
                // tcp://localhost, so just return the default port.
                return defaultPort;
            }
        }

        private string ParseHost(string baseUri)
        {
            if (string.IsNullOrEmpty(baseUri)) return null;

            // If there is no colon no port is specified, entire string is host
            int index = baseUri.LastIndexOf(':');
            if (index == -1) return baseUri;

            string host = baseUri.Substring(0, index);
            return string.IsNullOrEmpty(host) ? null : host;
        }
    }
}
