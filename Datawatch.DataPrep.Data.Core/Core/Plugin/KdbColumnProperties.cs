﻿namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class KdbColumnProperties : DatabaseColumnProperties
    {
        public readonly SharedStringProperty KdbType;

        public KdbColumnProperties(string rootPath, int columnIndex)
            : base(rootPath, columnIndex)
        {
            string propPrefix = rootPath + ".Column_" + columnIndex;
            KdbType = new SharedStringProperty(propPrefix + ".KdbType");
        }
    }
}
