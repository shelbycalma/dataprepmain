﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedStringProperty : SharedProperty
    {
        public SharedStringProperty(string path)
            : base(path)
        {
        }

        public bool IsSet(PropertyBag bag)
        {
            return bag != null && GetString(bag) != null;
        }

        public string GetString(PropertyBag bag, string defaultValue)
        {
            return IsSet(bag) ? GetString(bag) : defaultValue;
        }

        public string GetString(PropertyBag bag)
        {
            if (bag == null)
            {
                throw Exceptions.ArgumentNull("bag");
            } 
            string[] pathParts = path.Split(new char[] { '.' });
            int i;

            for (i = 0; i < pathParts.Length - 1; i++)
            {
                bag = bag.SubGroups[pathParts[i]];
                if (bag == null)
                {
                    return null;
                }
            }
            return bag.Values[pathParts[i]];
        }

        public void SetString(PropertyBag bag, string value)
        {
            if (bag == null)
            {
                throw Exceptions.ArgumentNull("bag");
            } 
            string[] pathParts = path.Split(new char[] { '.' });
            int i;
            for (i = 0; i < pathParts.Length - 1; i++)
            {
                if (!bag.SubGroups.ContainsKey(pathParts[i]))
                {
                    bag.SubGroups[pathParts[i]] = new PropertyBag();
                }
                bag = bag.SubGroups[pathParts[i]];
            }
            bag.Values[pathParts[i]] = value;
        }
    }
}
