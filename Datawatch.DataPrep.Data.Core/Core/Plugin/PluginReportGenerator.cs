﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    // TODO: report generator is currently used for development and QA purposes.
    // In case it becomes available to users, all the hard-coded strings 
    // should be moved to resources.

    /// <summary>
    /// Report generator for plugin manager.
    /// </summary>
    public class PluginReportGenerator
    {
        private PluginManager pluginManager;
        private StringBuilder sb;

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginReportGenerator"/> class.
        /// </summary>
        /// <param name="pm">The pm.</param>
        /// <exception cref="System.ArgumentNullException">pm</exception>
        public PluginReportGenerator(PluginManager pm)
        {
            if (pm == null)
                throw new ArgumentNullException("pm");

            this.pluginManager = pm;
            this.PluginsWithNonUniqueId = new Dictionary<string, List<string>>();
            this.DataPluginsWithNonUniqueTitle = new Dictionary<string, List<string>>();
            this.ParserPluginsWithNonUniqueTitle = new Dictionary<string, List<string>>();
            this.PluginsWithNoId = new List<string>();
            this.PluginsWithNoTitle = new List<string>();
        }

        /// <summary>
        /// Gets the list of plugins with non-unique identifiers.
        /// </summary>
        /// <value>
        /// List of plugins with non-unique identifiers.
        /// </value>
        public Dictionary<string, List<string>> PluginsWithNonUniqueId { get; private set; }

        /// <summary>
        /// Gets the list of plugins with non-unique titles.
        /// </summary>
        /// <value>
        /// List of plugins with non-unique titles.
        /// </value>
        public Dictionary<string, List<string>> DataPluginsWithNonUniqueTitle { get; private set; }

        /// <summary>
        /// Gets the list of plugins with no identifier.
        /// </summary>
        /// <value>
        /// The plugins with no identifier.
        /// </value>
        public List<string> PluginsWithNoId { get; private set; }

        /// <summary>
        /// Gets the list of plugins with no title.
        /// </summary>
        /// <value>
        /// The plugins with no title.
        /// </value>
        public List<string> PluginsWithNoTitle { get; private set; }

        public Dictionary<string, List<string>> ParserPluginsWithNonUniqueTitle { get; private set; }

        /// <summary>
        /// Gets the generated report text.
        /// </summary>
        /// <value>
        /// The report text.
        /// </value>
        public string Report { get; protected set; }

        /// <summary>
        /// Generates a report on plugins processed by plugin manager.
        /// </summary>
        /// <returns>Report text.</returns>
        public virtual string GenerateReport()
        {
            if (this.pluginManager.Plugins.Any() == false &&
                this.pluginManager.NotLoadedPluginList.Any() == false)
            {
                this.Report = "No plugins found.";
                return this.Report;
            }

            this.sb = new StringBuilder();

            this.sb.AppendLine("PLUGIN LOADING REPORT");
            this.AddLoadedPluginsInfo();
            this.AddFailedToLoadPluginsInfo();
            this.AddPluginDiscoveryWarnings();
            this.AddLoaderExceptionsInfo();
            this.AddObsoletePluginsInfo();
            this.AddNonLicensedPluginsInfo();
            this.AddPluginValidationInfo();

            this.Report = this.sb.ToString();
            return this.Report;
        }

        /// <summary>
        /// Adds a block of information on the failed to load plugins.
        /// </summary>
        protected void AddFailedToLoadPluginsInfo()
        {
            var failures = this.pluginManager.NotLoadedPluginList;
            this.AppendCollectionInfo(
                failures != null
                    ? failures.Select(f => f.ToString())
                    : null,
                "FAILED TO LOAD PLUGINS",
                true);
        }

        /// <summary>
        /// Adds a block of information on all loaded plugins.
        /// </summary>
        protected void AddLoadedPluginsInfo()
        {
            var plugins = this.pluginManager.Plugins;
            this.AppendCollectionInfo(
                plugins != null ? plugins.Select(p => this.GetPluginInfo(p))
                : null,
                "LOADED PLUGINS");
        }

        /// <summary>
        /// Adds a block of information on loader exceptions.
        /// </summary>
        protected void AddLoaderExceptionsInfo()
        {
            var exceptions = this.pluginManager.LoaderExceptions;
            this.AppendCollectionInfo(
                exceptions != null
                    ? exceptions.Select(e => this.GetLoaderExceptionInfo(e) + Environment.NewLine)
                    : null,
                "LOADER EXCEPTIONS");
        }

        /// <summary>
        /// Adds a block of information on non-licensed plugins.
        /// </summary>
        protected void AddNonLicensedPluginsInfo()
        {
            var nonLicensed = this.pluginManager.Plugins
                .Where(p => p.IsLicensed == false)
                .Select(p => GetPluginInfo(p));

            this.AppendCollectionInfo(nonLicensed, "NON-LICENSED PLUGINS");
        }

        /// <summary>
        /// Adds a block of information on obsolete plugins.
        /// </summary>
        protected void AddObsoletePluginsInfo()
        {
            var obsolete = this.pluginManager.Plugins
                .Where(p => p.IsObsolete == true)
                .Select(p => GetPluginInfo(p))
                .Concat(
                    this.pluginManager.NotLoadedPluginList
                        .Where(p => p.IsObsolete == true)
                        .Select(p => GetPluginInfo(p)));

            this.AppendCollectionInfo(obsolete, "OBSOLETE PLUGINS");
        }

        /// <summary>
        /// Adds a block of information on plugin discovery warnings.
        /// </summary>
        protected void AddPluginDiscoveryWarnings()
        {
            var warnings = this.pluginManager.PluginDiscoveryWarnings;
            this.AppendCollectionInfo(
                warnings != null
                    ? warnings.Select(w => w.ToString())
                    : null,
                "PLUGIN DISCOVERY WARNINGS");
        }

        /// <summary>
        /// Adds a block of information on plugin validation results.
        /// </summary>
        protected void AddPluginValidationInfo()
        {
            this.ValidatePlugins();

            this.AppendCollectionInfo(this.PluginsWithNoId, "PLUGINS WITHOUT ID");
            this.AppendCollectionInfo(this.PluginsWithNoTitle, "PLUGINS WITHOUT TITLE");
            this.AppendCollectionInfo(this.PluginsWithNonUniqueId, "PLUGINS WITH NON-UNIQUE ID");
            this.AppendCollectionInfo(this.DataPluginsWithNonUniqueTitle, "DATA PLUGINS WITH NON-UNIQUE TITLE");
            this.AppendCollectionInfo(this.ParserPluginsWithNonUniqueTitle, "PARSER PLUGINS WITH NON-UNIQUE TITLE");
        }

        /// <summary>
        /// Appends information from the specified collection to report using the specified header.
        /// </summary>
        /// <param name="collection">The collection with information.</param>
        /// <param name="header">The block header text.</param>
        /// <param name="multilineEntries">if set to <c>true</c>, collection 
        /// entries are to be delimited with an additional blank line.</param>
        protected void AppendCollectionInfo(IEnumerable<string> collection, string header, bool multilineEntries = false)
        {
            this.AppendNewBlock(header,
                collection != null ? collection.Count().ToString() : null);

            if (collection != null && collection.Any() == true)
            {
                foreach (var item in collection)
                {
                    this.sb.AppendLine(item);
                    if (multilineEntries == true)
                        this.sb.AppendLine();
                }
            }
            else
            {
                this.sb.AppendLine("None");
            }
        }

        /// <summary>
        /// Appends information from the specified dictionary collection to 
        /// report using the specified header.
        /// </summary>
        /// <param name="dic">The dictionary with information.</param>
        /// <param name="header">The header text.</param>
        protected void AppendCollectionInfo(IDictionary<string, List<string>> dic, string header)
        {
            string count = dic != null
                ? dic.SelectMany(x => x.Value).Count().ToString()
                : null;
            this.AppendNewBlock(header, count);
            if (dic.Any() == true)
            {
                foreach (var item in dic)
                {
                    this.sb.AppendLine(item.Key);
                    item.Value.ForEach(s => sb.AppendLine("\t" + s));
                    sb.AppendLine();
                }
            }
            else
            {
                sb.AppendLine("None");
            }
        }

        /// <summary>
        /// Appends a new block to report.
        /// </summary>
        /// <param name="header">The block header text.</param>
        /// <param name="count">The count of items in the block.</param>
        protected void AppendNewBlock(string header, string count = null)
        {
            this.sb.AppendLine();
            this.sb.AppendLine();
            if (string.IsNullOrWhiteSpace(count) == true)
            {
                this.sb.AppendLine(header + ":");
            }
            else
            {
                this.sb.AppendFormat("{0} ({1}):{2}",
                    header,
                    count,
                    Environment.NewLine);
            }

            this.sb.AppendLine();
        }

        /// <summary>
        /// Gets a display information for the specified plugin.
        /// </summary>
        /// <param name="p">Plugin instance.</param>
        /// <returns>A string describing the specified plugin instance.</returns>
        protected string GetPluginInfo(IPlugin p)
        {
            return
                string.Format("Id = {0}, Title = {1}, Type = {2}, IsObsolete = {3}, IsLicensed = {4}",
                    string.IsNullOrWhiteSpace(p.Id) == false ? p.Id : "None",
                    string.IsNullOrWhiteSpace(p.Title) == false ? p.Title : "None",
                    p is IDataPlugin
                        ? string.IsNullOrWhiteSpace(((IDataPlugin)p).DataPluginType) == false
                            ? ((IDataPlugin)p).DataPluginType : "None"
                        : "n/a",
                    p.IsObsolete.ToString(),
                    p.IsLicensed.ToString());
        }

        /// <summary>
        /// Gets a display information for the specified failed to load plugin.
        /// </summary>
        /// <param name="failure">Plugin loading failure information.</param>
        /// <returns>A string describing the specified plugin instance.</returns>
        protected string GetPluginInfo(PluginLoadingFailureInfo failure)
        {
            return
                string.Format(
                    "Id = {0}, Title = {1}, Type = {2}, IsObsolete = {3}",
                    string.IsNullOrWhiteSpace(failure.PluginId) == false
                        ? failure.PluginId : "None",
                    string.IsNullOrWhiteSpace(failure.PluginTitle) == false
                        ? failure.PluginTitle : "None",
                    string.IsNullOrWhiteSpace(failure.PluginDataType) == false
                        ? failure.PluginDataType : "None",
                    failure.IsObsolete.ToString());
        }

        /// <summary>
        /// Validates the plugin information.
        /// </summary>
        /// <param name="id">Plugin identifier.</param>
        /// <param name="title">Plugin title.</param>
        /// <param name="info">Plugin information.</param>
        protected void ValidatePlugins()
        {
            var dataById = new Dictionary<string, string>();
            var parserById = new Dictionary<string, string>();
            var dataByTitle = new Dictionary<string, string>();
            var parserByTitle = new Dictionary<string, string>();

            Dictionary<string, string> byId = null;
            Dictionary<string, string> byTitle = null;
            Dictionary<string, List<string>> nonUniqueTitle = null;

            foreach (var p in this.pluginManager.Plugins)
            {
                var id = p.Id;
                var title = p.Title;
                var info = GetPluginInfo(p);

                if (p is IDataPlugin)
                {
                    byId = dataById;
                    byTitle = dataByTitle;
                    nonUniqueTitle = DataPluginsWithNonUniqueTitle;
                }
                else if (p is IParserPlugin)
                {
                    byId = parserById;
                    byTitle = parserByTitle;
                    nonUniqueTitle = ParserPluginsWithNonUniqueTitle;
                }
                else
                {
                    byId = null;
                    byTitle = null;
                    nonUniqueTitle = null;
                }

                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    if (byId != null)
                    {
                        if (byId.ContainsKey(id) == false)
                        {
                            byId.Add(id, info);
                        }
                        else
                        {
                            if (this.PluginsWithNonUniqueId.ContainsKey(id) == false)
                            {
                                this.PluginsWithNonUniqueId.Add(id, new List<string>());
                                this.PluginsWithNonUniqueId[id].Add(byId[id]);
                            }

                            this.PluginsWithNonUniqueId[id].Add(info);
                        }
                    }
                }
                else
                {
                    this.PluginsWithNoId.Add(info);
                }

                if (string.IsNullOrWhiteSpace(title) == false)
                {
                    // We are checking uniqueness of title only for active plugins
                    // as obsolete plugins are not getting shown in UI.
                    if (byTitle != null && p.IsObsolete == false)
                    {
                        if (byTitle.ContainsKey(title) == false)
                        {
                            byTitle.Add(title, info);
                        }
                        else
                        {
                            if (nonUniqueTitle.ContainsKey(title) == false)
                            {
                                nonUniqueTitle.Add(title, new List<string>());
                                nonUniqueTitle[title].Add(byTitle[title]);
                            }

                            nonUniqueTitle[title].Add(info);
                        }
                    }
                }
                else
                {
                    this.PluginsWithNoTitle.Add(info);
                }
            }

            foreach (var p in this.pluginManager.NotLoadedPluginList)
            {
                var id = p.PluginId;
                var title = p.PluginTitle;
                var info = GetPluginInfo(p);

                if (p.PluginDataType != null)
                {
                    byId = dataById;
                    byTitle = dataByTitle;
                    nonUniqueTitle = DataPluginsWithNonUniqueTitle;
                }
                else
                {
                    byId = null;
                    byTitle = null;
                    nonUniqueTitle = null;
                }

                if (string.IsNullOrWhiteSpace(id) == false)
                {
                    if (byId != null)
                    {
                        if (byId.ContainsKey(id) == false)
                        {
                            byId.Add(id, info);
                        }
                        else
                        {
                            if (this.PluginsWithNonUniqueId.ContainsKey(id) == false)
                            {
                                this.PluginsWithNonUniqueId.Add(id, new List<string>());
                                this.PluginsWithNonUniqueId[id].Add(byId[id]);
                            }

                            this.PluginsWithNonUniqueId[id].Add(info);
                        }
                    }
                }
                else
                {
                    this.PluginsWithNoId.Add(info);
                }

                if (string.IsNullOrWhiteSpace(title) == false)
                {
                    if (byTitle != null && p.IsObsolete == false)
                    {
                        if (byTitle.ContainsKey(title) == false)
                        {
                            byTitle.Add(title, info);
                        }
                        else
                        {
                            if (nonUniqueTitle.ContainsKey(title) == false)
                            {
                                nonUniqueTitle.Add(title, new List<string>());
                                nonUniqueTitle[title].Add(byTitle[title]);
                            }

                            nonUniqueTitle[title].Add(info);
                        }
                    }
                }
                else
                {
                    this.PluginsWithNoTitle.Add(info);
                }
            }
        }

        private string GetLoaderExceptionInfo(Exception e)
        {
            var info = e.Message;
            var rtle = e as System.Reflection.ReflectionTypeLoadException;
            if (rtle == null)
            {
                if (e.InnerException != null)
                    info = info + Environment.NewLine + "\t" + GetLoaderExceptionInfo(e.InnerException);
            }
            else
            {
                info = "Unable to load one or more of the requested types: ";
                foreach (var item in rtle.LoaderExceptions)
                {
                    info = info + Environment.NewLine + "\t" + GetLoaderExceptionInfo(item);
                }
            }

            return info;
        }
    }
}
