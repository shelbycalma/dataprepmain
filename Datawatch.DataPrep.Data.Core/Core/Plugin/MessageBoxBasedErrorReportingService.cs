﻿using System.Windows;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    /// <summary>
    /// MessageBox-based implementation on plugin error reporting service.
    /// </summary>
    /// <seealso cref="Datawatch.DataPrep.Data.Framework.Plugin.IPluginErrorReportingService" />
    public class MessageBoxBasedErrorReportingService : IPluginErrorReportingService
    {
        /// <summary>
        /// Reports an error with the specified error category.
        /// </summary>
        /// <param name="errorCategory">The error category.</param>
        /// <param name="errorMessage">The error message.</param>
        public void Report(string errorCategory, string errorMessage)
        {
            MessageBox.Show(errorMessage ?? string.Empty, errorCategory, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// Reports a formatted error with the specified error category.
        /// </summary>
        /// <param name="errorCategory">The error category.</param>
        /// <param name="errorMessageFormat">The error message format.</param>
        /// <param name="errorMessageArgs">The error message arguments.</param>
        public void Report(string errorCategory, string errorMessageFormat, params object[] errorMessageArgs)
        {
            this.Report(errorCategory, string.Format(errorMessageFormat, errorMessageArgs));
        }
    }
}