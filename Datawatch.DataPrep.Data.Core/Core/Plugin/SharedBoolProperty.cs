﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedBoolProperty : SharedProperty
    {
        private readonly SharedStringProperty stringProperty;

        public SharedBoolProperty(string path)
            : base(path)
        {
            stringProperty = new SharedStringProperty(path);
        }

        public bool GetValue(PropertyBag bag)
        {
            string s = stringProperty.GetString(bag);
            return Convert.ToBoolean(s);
        }

        public bool IsSet(PropertyBag bag)
        {
            return stringProperty.IsSet(bag);
        }

        public void SetValue(PropertyBag bag, bool value)
        {
            stringProperty.SetString(bag, value.ToString());
        }
    }
}
