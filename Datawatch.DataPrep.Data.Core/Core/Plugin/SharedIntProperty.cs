﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedIntProperty : SharedProperty
    {
        private readonly SharedStringProperty stringProperty;

        public SharedIntProperty(string path)
            : base(path)
        {
            stringProperty = new SharedStringProperty(path);
        }

        public int GetValue(PropertyBag bag)
        {
            string s = stringProperty.GetString(bag);
            return Convert.ToInt32(s);
        }

        public bool IsSet(PropertyBag bag)
        {
            return stringProperty.IsSet(bag);
        }

        public void SetValue(PropertyBag bag, int value)
        {
            stringProperty.SetString(bag, value.ToString());
        }
    }
}
