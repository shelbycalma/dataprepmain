﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class PluginManager : IPluginManager, INotifyPropertyChanged
    {
        /// <summary>
        /// List of exceptions caught during the plugin loading sequence.
        /// </summary>
        protected readonly List<Exception> loaderExceptions = new List<Exception>();

        /// <summary>
        /// Instance of a service for reporting errors occurred within plugins.
        /// </summary>
        protected IPluginErrorReportingService errorReporter;

        /// <summary>
        /// List of all assemblies loaded during the plugin loading sequence. 
        /// The list is mainly used to avoid repeated processing of assemblies.
        /// </summary>
        protected Dictionary<string, AssemblyName> loadedAssemblies = new Dictionary<string, AssemblyName>();

        /// <summary>
        /// Assemblies loaded to current appdomain. Used find referenced assemblies
        /// not under base plugin directory for loading by path.
        /// </summary>
        protected Dictionary<string, Assembly> domainAssemblies;

        /// <summary>
        /// List of loaded plugin UI tuples that map plugin UI to plugin type.
        /// </summary>
        protected List<PluginUITypeTuple> loadedPluginUITypes = new List<PluginUITypeTuple>();

        /// <summary>
        /// List of names of plugin assemblies discovered within plugin base directory.
        /// </summary>
        protected Dictionary<string, AssemblyName> pluginAssemblyNames;

        /// <summary>
        /// Info on plugin discovery failures.
        /// </summary>
        protected List<PluginDiscoveryWarning> pluginDiscoveryWarnings;

        /// <summary>
        /// List of all loaded plugins.
        /// </summary>
        protected List<IPlugin> pluginList = new List<IPlugin>();

        /// <summary>
        /// Info on plugin loading failures.
        /// </summary>
        protected IDictionary<string, PluginLoadingFailureInfo> pluginLoadFailures = new Dictionary<string, PluginLoadingFailureInfo>();

        protected PropertyBag pluginSettings;

        /// <summary>
        /// List of names of plugin UI assemblies discovered within plugin base directory.
        /// </summary>
        protected Dictionary<string, AssemblyName> pluginUIAssemblyNames;

        /// <summary>
        /// List of all loaded plugin UIs.
        /// </summary>
        protected List<IPluginUI<IDataPlugin>> pluginUIList = new List<IPluginUI<IDataPlugin>>();

        private static Type pluginsMetadataAttributeType;
        private readonly Dictionary<string, IDataPlugin> dataPlugins =
            new Dictionary<string, IDataPlugin>();

        private readonly Dictionary<string, IParserPlugin> parserPlugins =
            new Dictionary<string, IParserPlugin>();

        private readonly IDictionary<string, string> pluginTypes = new Dictionary<string, string>
        {
            { DataPluginTypes.File, Resources.UiDataPluginTypeFile},
            { DataPluginTypes.Streaming, Resources.UiDataPluginTypeStreaming},
            { DataPluginTypes.Database, Resources.UiDataPluginTypeDatabase}
        };

        private string[] enabledDataPlugins;

        /// <summary>
        /// A hashset containing the names of loaded plugin assemblies.
        /// </summary>
        private HashSet<string> loadedPluginAssemblyNames;

        private string pluginDirectory;

        /// <summary>
        /// Info on discovered plugin assemblies required for further plugin loading.
        /// </summary>
        private IDictionary<string, string> pluginToUIAssemblyMap;

        /// <summary>
        /// Dictionary of names of all assemblies located in a directory of 
        /// a plugin being loaded. Key is a full name of the assembly. 
        /// Used for loading all the assemblies referenced by a specific plugin.
        /// </summary>
        private Dictionary<string, AssemblyName> referenceAssemblies;

        /// <summary>
        /// Dictionary of names of all assemblies located in a directory of 
        /// a plugin being loaded. Key is a simple name of the assembly.
        /// Used for loading all the assemblies referenced by a specific plugin.
        /// </summary>
        private Dictionary<string, AssemblyName> referenceAssembliesByName;

        private Assembly reflectionOnlyFrameworkAssembly;
        private string[] skipDataPlugins;

        private string[] skipDataTypes;

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManager" /> class.
        /// </summary>
        /// <param name="dir">Plugin base directory.</param>
        /// <param name="errorReporter">Plugin error reporter service instance. 
        /// If <c>null</c> value is passed, <see cref="MessageBoxBasedErrorReportingService"/> will be used.</param> 
        public PluginManager(string dir, IPluginErrorReportingService errorReporter = null)
            : this(dir, null, errorReporter)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PluginManager"/> class.
        /// </summary>
        /// <param name="dir">Plugin base directory.</param>
        /// <param name="settings">The plugin settings.</param>
        /// <param name="errorReporter">Plugin error reporter service instance. 
        /// If <c>null</c> value is passed, <see cref="MessageBoxBasedErrorReportingService"/> will be used.</param> 
        public PluginManager(string dir, PropertyBag settings, IPluginErrorReportingService errorReporter = null)
        {
            pluginDirectory = dir;
            pluginSettings = settings ?? new PropertyBag();
            this.errorReporter = errorReporter ?? new MessageBoxBasedErrorReportingService();
        }

        public virtual bool BlockAtDesignTimeOnly { get; set; }

        public virtual IEnumerable<IDataPlugin> DataPlugins
        {
            get
            {
                if (Utils.IsNullOrEmpty(SkipDataTypes) &&
                    Utils.IsNullOrEmpty(SkipDataPlugins) &&
                    Utils.IsNullOrEmpty(EnabledDataPlugins))
                {
                    return dataPlugins.Values.ToList();
                }
                IEnumerable<IDataPlugin> plugins = dataPlugins.Values;
                return plugins
                    .Where(e => !SkipDataPluginAndType(e))
                    .ToList();
            }
        }

        /// <summary>
        /// Gets the instance of plugin error reporting services.
        /// </summary>
        /// <value>
        /// The plugin error reporting service instance.
        /// </value>
        public IPluginErrorReportingService ErrorReporter { get { return this.errorReporter; } }

        public virtual string[] EnabledDataPlugins
        {
            get { return enabledDataPlugins; }
            set
            {
                if (value != null)
                {
                    enabledDataPlugins = value
                        .Select(e => e.ToUpper())
                        .ToArray();
                }
                else
                {
                    enabledDataPlugins = new string[0];
                }
            }
        }

        /// <summary>
        /// Gets the list of plugin loading failures descriptions.
        /// </summary>
        /// <value>
        /// The information on failed to load plugins.
        /// </value>
        public IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList
        {
            get
            {
                IEnumerable<PluginLoadingFailureInfo> notLoadedPlugins =
                    this.pluginLoadFailures.Values;

                if (Utils.IsNullOrEmpty(SkipDataTypes) &&
                    Utils.IsNullOrEmpty(SkipDataPlugins) &&
                    Utils.IsNullOrEmpty(EnabledDataPlugins))
                {
                    return notLoadedPlugins;
                }

                IEnumerable<PluginLoadingFailureInfo> disabledDataPlugins;
                if (!RemoveFromConnectToData)
                {
                    disabledDataPlugins = dataPlugins.Values
                        .Where(e => SkipDataPluginAndType(e))
                        .Select(e => new PluginLoadingFailureInfo(e, "Plugin is disabled", null));
                }
                else
                {
                    notLoadedPlugins = notLoadedPlugins
                        .Where(e => !SkipDataPluginAndType(e.PluginDataType, e.PluginId));
                    disabledDataPlugins = Enumerable
                        .Empty<PluginLoadingFailureInfo>();
                }

                return notLoadedPlugins
                    .Concat(disabledDataPlugins)
                    .ToList();
            }
        }

        /// <summary>
        /// Gets the exceptions thrown during the plugins loading sequence.
        /// </summary>
        /// <value>
        /// The plugin loading sequence exceptions.
        /// </value>
        public IEnumerable<Exception> LoaderExceptions
        {
            get { return loaderExceptions; }
        }

        public virtual IEnumerable<IParserPlugin> ParserPlugins
        {
            get { return new List<IParserPlugin>(parserPlugins.Values); }
        }

        /// <summary>
        /// Gets or sets a string representing the current directory used
        /// when loading plug-ins.
        /// </summary>
        public virtual string PluginDirectory
        {
            get { return pluginDirectory; }
            set
            {
                if (value == null)
                {
                    throw Exceptions.ArgumentNull("value");
                }
                pluginDirectory = value;
                Log.Warning(!Directory.Exists(pluginDirectory),
                    Resources.LogPluginManagerInvalidDirectory,
                    pluginDirectory);
            }
        }

        /// <summary>
        /// Gets warnings generated during the plugin discovery sequence.
        /// </summary>
        /// <value>
        /// The plugin discovery warnings.
        /// </value>
        public IEnumerable<PluginDiscoveryWarning> PluginDiscoveryWarnings
        {
            get
            {
                return this.pluginDiscoveryWarnings;
            }
        }

        public IEnumerable<IPlugin> Plugins
        {
            get { return pluginList; }
        }

        public PropertyBag PluginSettings
        {
            get { return pluginSettings; }
            set
            {
                pluginSettings = value;
                if (pluginSettings == null)
                {
                    pluginSettings = new PropertyBag();
                }
            }
        }

        public IEnumerable<IPluginUI<IDataPlugin>> PluginUIs
        {
            get { return pluginUIList; }
        }

        public virtual bool RemoveFromConnectToData { get; set; }

        public virtual string[] SkipDataPlugins
        {
            get { return skipDataPlugins; }
            set
            {
                if (value != null)
                {
                    skipDataPlugins = value
                        .Select(e => e.ToUpper())
                        .ToArray();
                }
                else
                {
                    skipDataPlugins = new string[0];
                }
            }
        }

        public virtual string[] SkipDataTypes
        {
            get { return skipDataTypes; }
            set
            {
                if (value != null)
                {
                    skipDataTypes = value
                        .Select(e => e.ToUpper())
                        .ToArray();
                }
                else
                {
                    skipDataTypes = new string[0];
                }
            }
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        public virtual void Dispose()
        {
            ClearPluginCollections();
        }

        /// <summary>
        /// Generates a report on the latest plugin loading operation.
        /// </summary>
        /// <returns>A string with report text.</returns>
        public virtual string GeneratePluginLoadingReport()
        {
            var reportGenerator = new PluginReportGenerator(this);
            return reportGenerator.GenerateReport();
        }

        public virtual IDataPlugin GetDataPlugin(string id)
        {
            if (id == null)
            {
                throw Exceptions.ArgumentNull("id");
            }
            IDataPlugin plugin;
            if (!dataPlugins.TryGetValue(id, out plugin))
            {
                throw Exceptions.DataPluginNotAvailable(id);
            }

            if (BlockAtDesignTimeOnly) return plugin;
            if (!SkipDataPluginAndType(plugin)) return plugin;

            throw Exceptions.DataPluginNotAvailable(id);
        }

        public virtual IParserPlugin GetParserPlugin(string id)
        {
            if (id == null)
            {
                throw Exceptions.ArgumentNull("id");
            }
            IParserPlugin plugin;
            parserPlugins.TryGetValue(id, out plugin);
            return plugin;
        }

        public IPluginUI<IDataPlugin> GetPluginUI(string id)
        {
            if (id == null)
            {
                throw Exceptions.ArgumentNull("id");
            }
            IPluginUI<IDataPlugin> pluginUI = pluginUIList.FirstOrDefault(p => p.Id == id);

            if (pluginUI == null)
            {
                throw Exceptions.DataPluginUINotAvailable(id);
            }

            if (BlockAtDesignTimeOnly) return pluginUI;
            if (!SkipDataPluginAndType(pluginUI.Plugin)) return pluginUI;

            throw Exceptions.DataPluginNotAvailable(id);
        }
        /// <summary>
        /// Initializes all data plug-ins for use with a specific <see cref="Window"/>.
        /// </summary>
        /// <param name="owner">A <see cref="Window"/> instance.</param>
        /// <param name="globalSettings">A <see cref="PropertyBag"/> globalSettings instance.</param>
        public virtual void InitializeDataPlugins(Window owner, PropertyBag globalSettings)
        {
            Log.Info(Resources.LogPluginManagerInitializing);

            // This method populates the dictionaries, so make
            // sure they are empty when we start.
            ClearPluginDictionaries();

            // Any plugins that fail to initialize or has
            // a duplicate id will end up here.
            List<IPlugin> pluginsToRemove = new List<IPlugin>();

            // Initialize each data plugin and add to dictionaries.
            foreach (IPlugin plugin in pluginList)
            {
                try
                {
                    // Try to add first, so if it has a duplicate
                    // key we don't even call initialize.
                    AddPluginToDictionaries(plugin);

                    PropertyBag settings = pluginSettings != null ?
                        pluginSettings.SubGroups[plugin.Id] : null;
                    if (settings == null)
                    {
                        settings = new PropertyBag();
                        pluginSettings.SubGroups[plugin.Id] = settings;
                    }

                    if (plugin.IsLicensed)
                    {
                        if (plugin is IDataPlugin)
                        {
                            ((IDataPlugin)plugin).Initialize(owner != null, this, settings, globalSettings);
                        }
                    }
                }
                catch (Exception ex)
                {
                    loaderExceptions.Add(ex);
                    Log.Warning(Resources.LogPluginManagerInitialize,
                        plugin.Id, ex.Message);
                    pluginsToRemove.Add(plugin);
                    // TODO: Report error.
                }
            }

            // Remove plugins with errors.
            foreach (IPlugin plugin in pluginsToRemove)
            {
                pluginList.Remove(plugin);
                // TODO: remove from all the other lists as well?
            }

            OnPropertyChanged("DataPlugins");
            OnPropertyChanged("ParserPlugins");

            Log.Info(Resources.LogPluginManagerInitialized, pluginList.Count);
        }

        public void InitializePluginUIs(Window owner)
        {
            foreach (PluginUITypeTuple pluginUITypeTuple in loadedPluginUITypes)
            {
                try
                {
                    IEnumerable<IDataPlugin> plugins = dataPlugins
                        .Select(p => p.Value)
                        .Where(p => pluginUITypeTuple.PluginType == p.GetType());

                    if (plugins.Count() > 1)
                    {
                        Log.Warning(Resources.LogPluginManagerFoundMultiplePluginsForPluginUI, pluginUITypeTuple.PluginUIType.FullName);
                    }
                    else if (!plugins.Any())
                    {
                        Log.Warning(Resources.LogPluginManagerFailedToLoadPluginUINotFound, pluginUITypeTuple.PluginUIType.FullName);
                        continue;
                    }

                    IDataPlugin plugin = plugins.First();
                    if (SkipDataPluginAndType(plugin)) continue;

                    IPluginUI<IDataPlugin> pluginUI =
                        (IPluginUI<IDataPlugin>)Activator.CreateInstance(pluginUITypeTuple.PluginUIType, plugin, owner);

                    pluginUIList.Add(pluginUI);
                }
                catch (Exception ex)
                {
                    loaderExceptions.Add(ex);
                    Log.Warning(Resources.LogPluginManagerFailedToLoadPluginUI, pluginUITypeTuple.PluginUIType.FullName, ex.Message);
                }
            }
        }

        /// <summary>
        /// Load all plugin assemblies and instantiate all plugins.
        /// </summary>
        public virtual void LoadPlugins()
        {
            Log.Info(Resources.LogPluginManagerLoading, pluginDirectory);

            this.loaderExceptions.Clear();
            this.loadedPluginAssemblyNames = new HashSet<string>();
            this.ClearPluginCollections();

            this.DiscoverPluginAssemblies(true);

            try
            {
                AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;

                foreach (AssemblyName asmName in pluginAssemblyNames.Values)
                {
                    AssemblyName uiAsmName = null;
                    if (this.pluginToUIAssemblyMap.ContainsKey(asmName.FullName) == true)
                    {
                        var key = this.pluginToUIAssemblyMap[asmName.FullName];
                        uiAsmName = this.pluginUIAssemblyNames[key];
                    }

                    var pluginFolder = GetPluginFolder(asmName);
                    UpdateReferences(pluginFolder);

                    bool pluginLoaded = false;
                    if (this.CheckIfPluginAssemblyLoaded(asmName) == false)
                    {
                        pluginLoaded = TryLoadPluginsFromAssembly(asmName) != null;
                    }

                    if (uiAsmName != null &&
                        pluginLoaded == true &&
                        this.CheckIfPluginAssemblyLoaded(uiAsmName) == false)
                    {
                        TryLoadPluginUIsFromAssembly(uiAsmName);
                    }
                }
            }
            finally
            {
                AppDomain.CurrentDomain.AssemblyResolve -= OnAssemblyResolve;
                this.loadedPluginAssemblyNames.Clear();
                this.loadedPluginAssemblyNames = null;
                this.referenceAssemblies = null;
                this.referenceAssembliesByName = null;
                this.domainAssemblies = null;
            }

            Log.Info(Resources.LogPluginManagerLoaded, pluginList.Count);
            Log.Warning(pluginList.Count == 1,
                Resources.LogPluginManagerNoneFound);
        }

        // TODO: Any reason why a plugin can't be more than one type? If it's
        // okay to implement more than one type, remove the return statements.
        // TODO: Behavior changed here: used to overwrite existing plugins
        // that had the same id, now throws an exception.
        protected virtual void AddPluginToDictionaries(IPlugin plugin)
        {
            IDataPlugin data = plugin as IDataPlugin;
            if (data != null)
            {
                dataPlugins.Add(plugin.Id, data);
                return;
            }

            IParserPlugin parser = plugin as IParserPlugin;
            if (parser != null)
            {
                parserPlugins.Add(plugin.Id, parser);
                return;
            }
        }

        /// <summary>
        /// Checks if plugin assembly has already been loaded.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly to check.</param>
        /// <returns>If the specified assembly has already been loaded and 
        /// scanned for plugins, return <c>true</c>; otherwise, <c>false</c>.</returns>
        protected virtual bool CheckIfPluginAssemblyLoaded(AssemblyName assemblyName)
        {
            return this.loadedPluginAssemblyNames.Contains(assemblyName.FullName);
        }

        protected virtual void ClearPluginCollections()
        {
            ClearPluginDictionaries();
            foreach (IPlugin plugin in pluginList)
            {
                try
                {
                    plugin.Dispose();
                }
                catch (Exception ex)
                {
                    Log.Warning(
                        Resources.LogPluginManagerDispose,
                        plugin.Id, ex.Message);
                }
            }
            pluginList.Clear();

            OnPropertyChanged("DataPlugins");
            OnPropertyChanged("ParserPlugins");
        }

        protected virtual void ClearPluginDictionaries()
        {
            dataPlugins.Clear();
            parserPlugins.Clear();
        }

        /// <summary>
        /// Creates the assembly load error message for the specified exception.
        /// </summary>
        /// <param name="ex">The exception instance.</param>
        /// <returns>A string with error message.</returns>
        protected string CreateAssemblyLoadErrorMessage(Exception ex)
        {
            string message = null;

            if (ex is FileNotFoundException)
            {
                message = string.Format(
                    Resources.ExFileNotFound, ((FileNotFoundException)ex).FileName);
            }
            else if (ex is FileLoadException)
            {
                var flex = (FileLoadException)ex;
                message = string.Format(
                    Resources.ExFileLoadException,
                    flex.FileName,
                    ExceptionUtils.GetCascadedMessage(flex));
            }
            else if (ex is BadImageFormatException)
            {
                message = string.Format(
                    Resources.ExBadImageFormat,
                    ((BadImageFormatException)ex).FileName);
            }
            else
            {
                message = ExceptionUtils.GetCascadedMessage(ex);
            }

            return message;
        }

        /// <summary>
        /// Discovers plugin assemblies within plugin directory.
        /// </summary>
        /// <returns>A dictionary with info on discovered assemblies.</returns>
        protected virtual void DiscoverPluginAssemblies(bool includeUI = false)
        {
            this.pluginAssemblyNames = new Dictionary<string, AssemblyName>();
            if (includeUI == true)
            {
                this.pluginUIAssemblyNames = new Dictionary<string, AssemblyName>();
                this.pluginToUIAssemblyMap = new Dictionary<string, string>();
            }

            this.pluginDiscoveryWarnings = new List<PluginDiscoveryWarning>();
            var pluginDirectoryInfo = new DirectoryInfo(this.pluginDirectory);

            // Load plugins from plugin directory
            if (Directory.Exists(this.pluginDirectory))
            {
                foreach (DirectoryInfo pluginDir in
                    pluginDirectoryInfo.GetDirectories())
                {
                    var path = Path.Combine(pluginDir.FullName, pluginDir + ".dll");
                    var uiPath = Path.Combine(pluginDir.FullName, pluginDir + ".UI.dll");

                    // If plugin UI assembly is present but logic assembly is not accessible, this is possibly an error.
                    if (this.CheckPluginAssemblyFiles(path, uiPath) == false)
                        continue;

                    var asmName = this.GetAssemblyNameByPath(path);
                    if (asmName == null) continue;

                    if (this.CheckIfPluginAssemblyRegistered(asmName) == true)
                        continue;

                    this.pluginAssemblyNames.Add(asmName.FullName, asmName);

                    if (includeUI == false || File.Exists(uiPath) == false)
                        continue;

                    var uiAsmName = this.GetAssemblyNameByPath(uiPath);

                    // Failed to get UI assembly name. Reasons: not a data 
                    // plugin, UI assembly is missing or has invalid binary format.
                    // In either case we load the plugin as it could be 
                    // referenced by other plugins.
                    if (uiAsmName == null) continue;

                    if (this.CheckIfPluginAssemblyRegistered(uiAsmName, true) == false)
                        this.pluginUIAssemblyNames.Add(uiAsmName.FullName, uiAsmName);
                    this.pluginToUIAssemblyMap.Add(asmName.FullName, uiAsmName.FullName);
                }
            }

            // Load non-data plugins from root folder
            foreach (FileInfo pluginAssembly in
                pluginDirectoryInfo.GetFiles("*.dll"))
            {
                var asmName = this.GetAssemblyNameByPath(pluginAssembly.FullName);
                if (this.CheckIfPluginAssemblyRegistered(asmName) == true)
                    continue;

                this.pluginAssemblyNames.Add(asmName.FullName, asmName);
            }
        }

        protected string GetPluginFolder(AssemblyName assemblyName)
        {
            return Path.Combine(pluginDirectory, assemblyName.Name);
        }

        /// <summary>
        /// Handles the plugin assembly load error by creating and logging an error message
        /// and registering a <see cref="PluginLoadingFailureInfo"/> instance.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="ex">The exception to process.</param>
        /// <param name="isUIAssembly">if set to <c>true</c>, the failed 
        /// to load assembly is plugin UI assembly.</param>
        protected void HandlePluginAssemblyLoadError(AssemblyName assemblyName, Exception ex, bool isUIAssembly = false)
        {
            string message = null;
            bool createFailureInfo = false;

            loaderExceptions.Add(ex);

            if (ex is ReflectionTypeLoadException)
            {
                message =
                    ExceptionUtils.GetReflectionTypeLoadExceptionFullMessage(
                        (ReflectionTypeLoadException)ex);
                createFailureInfo = true;
            }
            else if (ex is FileNotFoundException)
            {
                var fileName = ((FileNotFoundException)ex).FileName;
                message = string.Format(Resources.ExFileNotFound, fileName);
                createFailureInfo = assemblyName.FullName != fileName;
            }
            else if (ex is FileLoadException)
            {
                var flex = (FileLoadException)ex;
                message = string.Format(
                    Resources.ExFileLoadException,
                    flex.FileName,
                    ExceptionUtils.GetCascadedMessage(flex));
                createFailureInfo = assemblyName.FullName != flex.FileName;
            }
            else if (ex is BadImageFormatException)
            {
                message = string.Format(
                    Resources.ExBadImageFormat,
                    ((BadImageFormatException)ex).FileName);
                createFailureInfo = true;
            }
            else
            {
                message = ExceptionUtils.GetCascadedMessage(ex);
            }

            Log.Warning(Resources.LogPluginManagerFileLoad,
                assemblyName.CodeBase,
                message);

            if (createFailureInfo == true && isUIAssembly == false)
            {
                this.RegisterLoadFailureForAssembly(assemblyName, message);
            }
        }

        /// <summary>
        /// Handles the plugin dependencies loading error.
        /// </summary>
        /// <param name="assemblyName">Name of the dependency assembly.</param>
        /// <param name="plugins">A list of loaded plugins possibly 
        /// depending on the failed to load assembly.</param>
        /// <param name="ex">The exception to process.</param>
        protected void HandlePluginDependenciesLoadError(AssemblyName assemblyName, IEnumerable<IPlugin> plugins, Exception ex)
        {
            string message = this.CreateAssemblyLoadErrorMessage(ex);

            if (plugins != null)
                this.RegisterLoadFailureForPlugins(plugins, message, assemblyName.CodeBase);

            Log.Warning(
                Resources.LogPluginManagerFileLoad,
                assemblyName.CodeBase,
                message);
        }

        /// <summary>
        /// Handles the plugin instantiating error.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <param name="type">The plugin type.</param>
        /// <param name="ex">The exception to process.</param>
        protected void HandlePluginInstantiatingError(AssemblyName assemblyName, Type type, Exception ex)
        {
            this.loaderExceptions.Add(ex);
            string message = ExceptionUtils.GetCascadedMessage(ex);
            string assemblyPath = assemblyName.CodeBase;
            try
            {
                foreach (PluginDescriptionAttribute attribute in type.GetCustomAttributes(typeof(PluginDescriptionAttribute), false))
                {
                    this.RegisterLoadFailureForPlugin(attribute, message, assemblyPath);
                }
            }
            catch (Exception exc)
            {
                Log.Warning(
                    Resources.LogPluginManagerLoadPluginAttribute,
                    type.AssemblyQualifiedName, exc.Message);
            }
        }

        /// <summary>
        /// Handles the plugin UI dependencies loading error.
        /// </summary>
        /// <param name="assemblyName">Name of the dependency assembly.</param>
        /// <param name="pluginUITypeTuples">A list of loaded pluginUI tuples 
        /// possibly depending on the failed to load assembly.</param>
        /// <param name="ex">The exception to process.</param>
        protected void HandlePluginUIDependenciesLoadError(AssemblyName assemblyName, IEnumerable<PluginUITypeTuple> pluginUITypeTuples, Exception ex)
        {
            string message = this.CreateAssemblyLoadErrorMessage(ex);

            //TODO: Add plugin UI loading error registration
            if (pluginUITypeTuples != null)
                //this.RegisterLoadFailureForPlugins(pluginUITypeTuples, message, assemblyName.CodeBase);

                Log.Warning(
                    Resources.LogFailedToLoadPluginUITypes,
                    assemblyName.CodeBase,
                    message);
        }

        protected void LoadAssemblyReferences(Assembly assembly, bool calledByResolveHandler = false)
        {
            if (loadedAssemblies == null)
                loadedAssemblies = new Dictionary<string, AssemblyName>();

            foreach (var referencedAssembly in assembly.GetReferencedAssemblies())
            {
                AssemblyName asmName = null;
                loadedAssemblies.TryGetValue(referencedAssembly.Name, out asmName);

                if (asmName != null &&
                    asmName.Version >= referencedAssembly.Version)
                    continue;

                Assembly loadedRefAssembly = null;
                if (calledByResolveHandler == false)
                {
                    loadedRefAssembly = Assembly.Load(referencedAssembly);
                }
                else
                {
                    // Calling assembly loading methods that can raise 
                    // AssemblyResolve event within a handler of this event
                    // should be avoided as it can cause stack overflow.
                    // So we try to load by path.

                    string path = referencedAssembly.CodeBase;
                    if (path != null)
                    {
                        loadedRefAssembly = Assembly.LoadFrom(path);
                    }
                    else
                    {
                        loadedRefAssembly = TryResolveAssembly(referencedAssembly.FullName);
                    }

                    if (loadedRefAssembly == null)
                    {
                        // Assembly not under plugin base directory?
                        if (this.domainAssemblies == null)
                            this.domainAssemblies = AppDomain.CurrentDomain.GetAssemblies()
                                .ToDictionary(k => k.FullName, v => v);

                        if (this.domainAssemblies.TryGetValue(
                            referencedAssembly.FullName, out loadedRefAssembly) == false)
                        {
                            throw new FileNotFoundException(
                                string.Format(Resources.ExFileNotFound, referencedAssembly.FullName),
                                referencedAssembly.FullName);
                        }
                    }
                }

                loadedAssemblies[referencedAssembly.Name] = referencedAssembly;
                LoadAssemblyReferences(loadedRefAssembly);
            }
        }

        /// <summary>
        /// Loads a .NET assembly from the specified assembly name, locates all
        /// types that implement IPlugin, instantiates them, and adds them to
        /// the pluginList member.
        /// </summary>
        /// <param name="assemblyName">Assembly name that contains the assembly.</param>
        /// <param name="calledByResolveHandler">if set to <c>true</c>, the method called within AssemblyResolve event handler.</param>
        /// <returns>Loaded assembly.</returns>
        protected virtual Assembly LoadPluginsFromAssembly(AssemblyName assemblyName, bool calledByResolveHandler = false)
        {
            Log.Info(Resources.LogPluginManagerLoadingFile, assemblyName.Name);
            Assembly assembly = null;
            Type pluginType = typeof(IPlugin);
            List<IPlugin> loadedPlugins = null;

            try
            {
                assembly = calledByResolveHandler == false
                    ? Assembly.Load(assemblyName)
                    : Assembly.LoadFrom(assemblyName.CodeBase);

                loadedPlugins = new List<IPlugin>();
                foreach (Type type in assembly.GetTypes())
                {
                    try
                    {
                        if (pluginType.IsAssignableFrom(type) && !type.IsAbstract)
                        {
                            //20170619 Special handling to disable checking for Teradata,dBase,GreenPlum and SparkSQL
                            //Remove or modify this if stmt if one of these plugins will be implemented
                            if (!type.FullName.Contains("Teradata.Plugin") && !type.FullName.Contains("SparkSql.Plugin") &&
                            !type.FullName.Contains("Greenplum.Plugin") && !type.FullName.Contains("dBase.Plugin"))
                            {
                                IPlugin plugin = (IPlugin) Activator.CreateInstance(type);
                                loadedPlugins.Add(plugin);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        this.HandlePluginInstantiatingError(assemblyName, type, ex);

                        Log.Warning(Resources.LogPluginManagerCreateType,
                            type.AssemblyQualifiedName, ex.Message);
                        if (ex.InnerException != null)
                        {
                            Log.Warning(Resources.LogInnerException,
                                ex.InnerException.Message);
                            Log.Warning(Resources.LogInnerException,
                                ex.InnerException.StackTrace);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.HandlePluginAssemblyLoadError(assemblyName, ex);
                return null;
            }

            // Load all references only for Plugin assemblies
            if (loadedPlugins.Count > 0)
            {
                try
                {
                    LoadAssemblyReferences(assembly, calledByResolveHandler);

                    foreach (var loadedPlugin in loadedPlugins)
                    {
                        pluginList.Add(loadedPlugin);
                    }

                    loadedAssemblies[assemblyName.Name] = assemblyName;
                    this.loadedPluginAssemblyNames.Add(assemblyName.FullName);

                    return assembly;
                }
                catch (Exception ex)
                {
                    if (ex is FileNotFoundException == false &&
                        ex is FileLoadException == false &&
                        ex is BadImageFormatException == false)
                        throw;

                    this.HandlePluginDependenciesLoadError(assemblyName, loadedPlugins, ex);

                    return null;
                }
            }

            return assembly;
        }

        protected virtual Assembly LoadPluginUIsFromAssembly(AssemblyName assemblyName, bool calledByResolveHandler = false)
        {
            Log.Info(Resources.LogPluginManagerLoadingFile, assemblyName.Name);
            Type pluginUIType = typeof(IPluginUI<IDataPlugin>);
            Assembly assembly = null;
            List<PluginUITypeTuple> loadedPluginUITypesFromAssembly = null;
            try
            {
                assembly = calledByResolveHandler == false
                    ? Assembly.Load(assemblyName)
                    : Assembly.LoadFrom(assemblyName.CodeBase);
                loadedPluginUITypesFromAssembly = new List<PluginUITypeTuple>();

                foreach (Type type in assembly.GetTypes())
                {
                    try
                    {
                        if (pluginUIType.IsAssignableFrom(type) && !type.IsAbstract)
                        {
                            //20170619 Special handling to disable checking for Teradata,dBase,GreenPlum and SparkSQL
                            //Remove or modify this if stmt if one of these plugins will be implemented
                            if (!type.FullName.Contains("Teradata.UI.PluginUI") && !type.FullName.Contains("SparkSql.UI.PluginUI") &&
                                !type.FullName.Contains("GreenPlum.UI.PluginUI") && !type.FullName.Contains("dBase.UI.PluginUI"))
                            {
                                ConstructorInfo ci = type.GetConstructors().Single();
                                Type pluginType = ci.GetParameters().First().ParameterType;
                                loadedPluginUITypesFromAssembly.Add(new PluginUITypeTuple(type, pluginType));
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        loaderExceptions.Add(ex);
                        Log.Warning(Resources.LogPluginManagerFailedToLoadPluginUI, pluginUIType.FullName, ex.Message);
                        if (ex.InnerException != null)
                        {
                            Log.Warning(Resources.LogInnerException,
                                ex.InnerException.Message);
                            Log.Warning(Resources.LogInnerException,
                                ex.InnerException.StackTrace);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.HandlePluginAssemblyLoadError(assemblyName, ex, true);
                return null;
            }

            if (loadedPluginUITypesFromAssembly.Count > 0)
            {
                try
                {
                    LoadAssemblyReferences(assembly, calledByResolveHandler);

                    foreach (var loadedPluginUIType in loadedPluginUITypesFromAssembly)
                    {
                        loadedPluginUITypes.Add(loadedPluginUIType);
                    }

                    loadedAssemblies[assemblyName.Name] = assemblyName;
                    this.loadedPluginAssemblyNames.Add(assemblyName.FullName);

                    return assembly;
                }
                catch (Exception ex)
                {
                    if (ex is FileNotFoundException == false &&
                        ex is FileLoadException == false &&
                        ex is BadImageFormatException == false)
                        throw;

                    this.HandlePluginUIDependenciesLoadError(assemblyName, null, ex);

                    return null;
                }
            }
            else
            {
                Log.Warning(
                    Resources.LogPluginManagerUIFileLoad,
                    assemblyName.CodeBase);
            }

            return assembly;
        }

        /// <summary>
        /// Called when assembly loader fails to resolve assembly.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="ResolveEventArgs"/> instance containing the event data.</param>
        /// <returns>A resolved assembly or null, if assembly can't be resolved by the handler.</returns>
        /// <remarks>Using Assembly.Load within this handler may lead to stack overflow!</remarks>
        protected Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            return this.TryResolveAssembly(args.Name);
        }

        protected Assembly TryResolveAssembly(string name)
        {
            Assembly resolvedAssembly = null;
            AssemblyName assemblyName;
            AssemblyName an = new AssemblyName(name);

            // Calling assembly loading methods that can raise 
            // AssemblyResolve event within a handler of this event
            // should be avoided as it can cause stack overflow.
            // So we try to load by path.
            if (referenceAssemblies.TryGetValue(an.FullName, out assemblyName))
            {
                return Assembly.LoadFrom(assemblyName.CodeBase);
            }

            if (referenceAssembliesByName.TryGetValue(an.Name, out assemblyName))
            {
                if (an.Version < assemblyName.Version)
                    return Assembly.LoadFrom(assemblyName.CodeBase);
            }

            if (pluginAssemblyNames.TryGetValue(name, out assemblyName))
                resolvedAssembly = this.TryResolveAssembly(assemblyName, false);

            if (resolvedAssembly == null &&
                pluginUIAssemblyNames.TryGetValue(name, out assemblyName))
                resolvedAssembly = this.TryResolveAssembly(assemblyName, true);

            return resolvedAssembly;
        }

        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Registers the load failure for assembly by retrieving metadata for all
        /// plugins withing the assembly and generating the
        /// <see cref="PluginLoadingFailureInfo"/> instance for each.
        /// </summary>
        /// <param name="assemblyName">Name of the plugin assembly.</param>
        /// <param name="message">The error message to use.</param>
        protected void RegisterLoadFailureForAssembly(AssemblyName assemblyName, string message)
        {
            try
            {
                // TODO: Handle AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve
                var assembly = Assembly.ReflectionOnlyLoadFrom(assemblyName.CodeBase);
                if (reflectionOnlyFrameworkAssembly == null)
                    LoadPluginAttributeMetadata();

                // INFO: if assembly code uses custom attributes defined in an assembly other than 
                // the loaded ones, we won't be able to retrieve the plugin info
                // as GetCustomAttributesData() will fail with ReflectionTypeLoadException.
                foreach (CustomAttributeData customAttributeData in assembly.GetCustomAttributesData())
                {
                    if (customAttributeData.Constructor.DeclaringType == pluginsMetadataAttributeType)
                    {
                        IList<CustomAttributeTypedArgument> ctorArgs =
                            customAttributeData.ConstructorArguments;
                        var name = (string)ctorArgs[0].Value;
                        if (this.pluginLoadFailures.ContainsKey(name) == true)
                            continue;

                        var type = (string)ctorArgs[1].Value;
                        if (pluginTypes.ContainsKey(type))
                        {
                            type = pluginTypes[type];
                        }

                        string id = null;
                        bool isObsolete = false;

                        if (ctorArgs.Count > 2)
                            id = (string)ctorArgs[2].Value;

                        if (ctorArgs.Count > 3)
                            isObsolete = (bool)ctorArgs[3].Value;

                        var failure = new PluginLoadingFailureInfo(
                            id, name, type, isObsolete, message, assemblyName.CodeBase);

                        this.pluginLoadFailures.Add(name, failure);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(
                    Resources.LogPluginManagerReflectedFileLoad,
                    assemblyName.FullName, ex.Message);
            }
        }

        /// <summary>
        /// Registers the plugin loading failure using plugin metadata, error message and assembly path.
        /// </summary>
        /// <param name="attribute">The <see cref="PluginDescriptionAttribute"/> attribute instance with plugin metadata.</param>
        /// <param name="message">The error message.</param>
        /// <param name="assemblyPath">The assembly path.</param>
        protected void RegisterLoadFailureForPlugin(PluginDescriptionAttribute attribute, string message, string assemblyPath)
        {
            var pluginTitle = attribute.Name;
            if (this.pluginLoadFailures.ContainsKey(pluginTitle) == true)
                return;

            var pluginType = attribute.Type;
            if (pluginTypes.ContainsKey(pluginType))
            {
                pluginType = pluginTypes[pluginType];
            }

            var failure = new PluginLoadingFailureInfo(
                attribute.PluginId, pluginTitle, pluginType, attribute.IsObsolete,
                message, assemblyPath);

            this.pluginLoadFailures.Add(pluginTitle, failure);
        }

        /// <summary>
        /// Registers the loading failure for a collection of plugins residing in a single assembly.
        /// </summary>
        /// <param name="loadedPlugins">The collection of plugins from a single assembly.</param>
        /// <param name="message">The error message.</param>
        /// <param name="assemblyPath">The assembly path.</param>
        protected void RegisterLoadFailureForPlugins(IEnumerable<IPlugin> loadedPlugins, string message, string assemblyPath)
        {
            foreach (var plugin in loadedPlugins)
            {
                IDataPlugin dataPlugin = plugin as IDataPlugin;
                if (dataPlugin == null ||
                    this.pluginLoadFailures.ContainsKey(dataPlugin.Title))
                    continue;

                var failure = new PluginLoadingFailureInfo(dataPlugin, message, assemblyPath);
                this.pluginLoadFailures.Add(dataPlugin.Title, failure);
            }
        }

        protected Assembly TryLoadPluginsFromAssembly(AssemblyName assemblyName, bool calledByResolveHandler = false)
        {
            try
            {
                return LoadPluginsFromAssembly(assemblyName, calledByResolveHandler);
            }
            catch (Exception ex)
            {
                loaderExceptions.Add(ex);
                Log.Warning(
                    Resources.LogPluginManagerFileLoad,
                    assemblyName.CodeBase,
                    ExceptionUtils.GetCascadedMessage(ex));
            }

            return null;
        }

        /// <summary>
        /// Tries to load plugin UI from an assembly corresponding to the 
        /// specified <see cref="AssemblyName"/> instance.
        /// </summary>
        /// <param name="assemblyName">Name of the assembly.</param>
        /// <returns>An instance of loaded assembly, if successful; otherwise, <c>null</c>.</returns>
        protected Assembly TryLoadPluginUIsFromAssembly(AssemblyName assemblyName, bool calledByResolveHandler = false)
        {
            try
            {
                return LoadPluginUIsFromAssembly(assemblyName, calledByResolveHandler);
            }
            catch (Exception ex)
            {
                loaderExceptions.Add(ex);
                Log.Warning(
                    Resources.LogPluginManagerFailedToLoadPluginUIAssembly,
                    assemblyName.CodeBase,
                    ExceptionUtils.GetCascadedMessage(ex));
            }

            return null;
        }

        [System.Diagnostics.DebuggerNonUserCode]
        protected void UpdateReferences(string pluginFolder)
        {
            referenceAssemblies = new Dictionary<string, AssemblyName>();
            referenceAssembliesByName = new Dictionary<string, AssemblyName>();
            if (pluginFolder != null && Directory.Exists(pluginFolder))
            {
                foreach (var file in Directory.GetFiles(pluginFolder, "*.dll"))
                {
                    try
                    {
                        var assemblyName = AssemblyName.GetAssemblyName(file);
                        referenceAssemblies.Add(assemblyName.FullName,
                            assemblyName);
                        referenceAssembliesByName.Add(assemblyName.Name,
                            assemblyName);
                    }
                    catch (BadImageFormatException)
                    {
                        // Library is not .NET Assembly
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// Checks if the specified plugin assembly has been discovered and registered.
        /// </summary>
        /// <param name="assemblyName"><see cref="AssemblyName"/> of the plugin assembly to check.</param>
        /// <param name="isUIAssembly">if set to <c>true</c>, the specified assembly is a plugin UI assembly.</param>
        /// <returns>Returns <c>true</c>, if the specified plugin assembly has 
        /// already been discovered and registered.</returns>
        private bool CheckIfPluginAssemblyRegistered(AssemblyName assemblyName, bool isUIAssembly = false)
        {
            var dic = isUIAssembly == false ? this.pluginAssemblyNames : this.pluginUIAssemblyNames;
            if (dic.ContainsKey(assemblyName.FullName) == false)
                return false;

            this.pluginDiscoveryWarnings.Add(
                new PluginDiscoveryWarning(
                    assemblyName.CodeBase,
                    string.Format(
                        Resources.LogPluginManagerPluginAssemblyAlreadyRegistered,
                        assemblyName.FullName)));

            return true;
        }

        /// <summary>
        /// Checks the plugin assembly files for existense.
        /// </summary>
        /// <param name="path">The expected plugin assembly path.</param>
        /// <param name="uiPath">The expected plugin UI assembly path.</param>
        /// <returns>
        /// A value of <c>true</c>, if required files are present and discovery 
        /// must proceed to acquiring assembly names. Otherwise, <c>false</c>.
        /// </returns>
        private bool CheckPluginAssemblyFiles(string path, string uiPath)
        {
            if (File.Exists(path) == false)
            {
                // if no plugin assemblies found, just skip the directory.
                if (File.Exists(uiPath) == false)
                    return false;

                this.pluginDiscoveryWarnings.Add(
                    new PluginDiscoveryWarning(
                        path,
                        string.Format(
                            Resources.LogPluginManagerPluginAssemblyMissing,
                            path)));

                return false;
            }

            return true;
        }

        /// <summary>
        /// Gets the <see cref="AssemblyName"/> for the assembly with the specified path.
        /// </summary>
        /// <param name="path">The path to an assembly file.</param>
        /// <returns>
        /// An instance of <see cref="AssemblyName"/>, if the specified 
        /// assembly has a valid supported format. Otherwise, returns <c>null</c>.
        /// </returns>
        private AssemblyName GetAssemblyNameByPath(string path)
        {
            try
            {
                return AssemblyName.GetAssemblyName(path);
            }
            catch (BadImageFormatException)
            {
                // DLL was built for another platform or is not a .Net assembly.
                this.pluginDiscoveryWarnings.Add(
                    new PluginDiscoveryWarning(
                        path,
                        string.Format(Resources.ExBadImageFormat, path)));
            }

            return null;
        }

        private void LoadPluginAttributeMetadata()
        {
            var attributeType = typeof(PluginDescriptionAttribute);
            reflectionOnlyFrameworkAssembly = Assembly.ReflectionOnlyLoadFrom(
                attributeType.Assembly.CodeBase);
            pluginsMetadataAttributeType = reflectionOnlyFrameworkAssembly.GetType(attributeType.FullName);
            if (pluginsMetadataAttributeType == null)
                throw new TypeLoadException("Failed to load PluginDescriptionAttribute type from reflection-only context.");
        }

        private bool SkipDataPluginAndType(IDataPlugin dataPlugin)
        {
            if (Utils.IsNullOrEmpty(SkipDataTypes) &&
                Utils.IsNullOrEmpty(SkipDataPlugins) &&
                Utils.IsNullOrEmpty(EnabledDataPlugins))
            {
                return false;
            }

            return SkipDataPluginAndType(dataPlugin.DataPluginType, dataPlugin.Id);
        }

        private bool SkipDataPluginAndType(string pluginDataType, string pluginId)
        {
            if (!Utils.IsNullOrEmpty(SkipDataTypes) &&
                SkipDataTypes.Contains(pluginDataType.ToUpper()))
            {
                return true;
            }

            if (!Utils.IsNullOrEmpty(SkipDataPlugins) &&
                !string.IsNullOrEmpty(pluginId) &&
                SkipDataPlugins.Contains(pluginId.ToUpper()))
            {
                return true;
            }

            if (!Utils.IsNullOrEmpty(EnabledDataPlugins) &&
                !string.IsNullOrEmpty(pluginId) &&
                !EnabledDataPlugins.Contains(pluginId.ToUpper()))
            {
                return true;
            }
            return false;
        }

        private Assembly TryResolveAssembly(AssemblyName assemblyName, bool isUIAssembly = false)
        {
            Dictionary<string, AssemblyName> currentReferenceAssemblies =
                referenceAssemblies;
            Dictionary<string, AssemblyName> currentReferenceAssembliesByName =
                referenceAssembliesByName;

            try
            {
                var pluginFolder = GetPluginFolder(assemblyName);
                UpdateReferences(pluginFolder);
                return isUIAssembly == false
                    ? TryLoadPluginsFromAssembly(assemblyName, true)
                    : this.TryLoadPluginUIsFromAssembly(assemblyName, true);
            }
            finally
            {
                referenceAssemblies = currentReferenceAssemblies;
                referenceAssembliesByName = currentReferenceAssembliesByName;
            }
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        protected class PluginUITypeTuple
        {
            public PluginUITypeTuple(Type pluginUIType, Type pluginType)
            {
                PluginUIType = pluginUIType;
                PluginType = pluginType;
            }

            public Type PluginType { get; set; }
            public Type PluginUIType { get; set; }
        }
    }
}
