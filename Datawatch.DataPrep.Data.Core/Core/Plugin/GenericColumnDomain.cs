﻿namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class GenericColumnDomain<T> : ColumnDomain
    {
        public readonly T Domain;

        public GenericColumnDomain(T t)
        {
            Domain = t;
        }
    }
}
