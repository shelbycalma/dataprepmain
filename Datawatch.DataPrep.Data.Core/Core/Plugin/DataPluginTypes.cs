﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public static class DataPluginTypes
    {
        public const string File = "File";
        public const string Database = "Database";
        public const string Streaming = "Streaming";

        private static Dictionary<string, string> resourceMap =
            new Dictionary<string, string>
            {
                { File, Properties.Resources.UiDataPluginTypeFile },
                { Database, Properties.Resources.UiDataPluginTypeDatabase },
                { Streaming, Properties.Resources.UiDataPluginTypeStreaming }
            };

        public static string GetLocalized(string type)
        {
            if (resourceMap.ContainsKey(type) == true)
                return resourceMap[type];

            throw Exceptions.UnknownDataPluginType(type);
        }
    }
}