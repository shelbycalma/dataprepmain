﻿namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public abstract class SharedProperty
    {
        protected readonly string path;

        public SharedProperty(string path)
        {
            this.path = path;
        }
    }
}
