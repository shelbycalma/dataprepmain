﻿using System.Xml;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SharedDoubleProperty : SharedProperty
    {
        private readonly SharedStringProperty stringProperty;

        public SharedDoubleProperty(string path)
            : base(path)
        {
            stringProperty = new SharedStringProperty(path);
        }

        public double GetValue(PropertyBag bag)
        {
            string s = stringProperty.GetString(bag);
            return XmlConvert.ToDouble(s);
        }

        public double GetValue(PropertyBag bag, double defaultValue)
        {
            return IsSet(bag) ? GetValue(bag) : defaultValue;
        }

        public bool IsSet(PropertyBag bag)
        {
            return stringProperty.IsSet(bag);
        }

        public void SetValue(PropertyBag bag, double value)
        {
            stringProperty.SetString(bag, XmlConvert.ToString(value));
        }
    }
}
