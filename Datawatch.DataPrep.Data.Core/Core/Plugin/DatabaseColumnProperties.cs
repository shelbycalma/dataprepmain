﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class DatabaseColumnProperties
    {
        public readonly SharedStringProperty ColumnName;
        public readonly SharedEnumProperty<ColumnType> ColumnType;
        public readonly SharedStringProperty AppliedParameterName;
        public readonly SharedEnumProperty<AggregateType> AggregateType;
        public readonly SharedEnumProperty<FilterOperator> FilterOperator;
        public readonly SharedStringProperty FilterValue;

        public DatabaseColumnProperties(string rootPath, int columnIndex)
        {
            string propPrefix = rootPath + ".Column_" + columnIndex;

            ColumnName = new SharedStringProperty(propPrefix + ".ColumnName");
            ColumnType = new SharedEnumProperty<ColumnType>(propPrefix +
                ".ColumnType");
            AppliedParameterName = new SharedStringProperty(propPrefix +
                ".AppliedParameterName");
            AggregateType = new SharedEnumProperty<AggregateType>(propPrefix +
                ".AggregateType");
            FilterOperator = new SharedEnumProperty<FilterOperator>(propPrefix +
                ".FilterOperator");
            FilterValue = new SharedStringProperty(propPrefix + ".FilterValue");
        }        
    }
}
