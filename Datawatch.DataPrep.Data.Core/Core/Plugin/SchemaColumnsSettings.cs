﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Newtonsoft.Json;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class SchemaColumnsSettings<T> : PropertyBagViewModel where T : DatabaseColumn
    {
        protected List<T> schemaColumns = new List<T>();
        JsonSerializerSettings settings = new JsonSerializerSettings();

        public SchemaColumnsSettings(PropertyBag bag) : base(bag)
        {
            if (SchemaColumnsAsJsonString == null)
            {
                SchemaColumnsAsJsonString = "";
            }

            settings.NullValueHandling = NullValueHandling.Ignore;

        }

        private string SchemaColumnsAsJsonString
        {
            get { return GetInternal("SchemaColumnsAsJsonString"); }
            set { SetInternal("SchemaColumnsAsJsonString", value); }
        }

        public virtual List<T> SchemaColumns
        {
            get
            {
                if (schemaColumns.Count == 0)
                {
                    if (string.IsNullOrEmpty(SchemaColumnsAsJsonString))
                    {
                        return schemaColumns;
                    }
                    schemaColumns =
                        JsonConvert.DeserializeObject<List<T>>(
                            SchemaColumnsAsJsonString);
                }
                return schemaColumns;
            }
        }

        public void ClearSchemaColumns()
        {
            SchemaColumns.Clear();
            SchemaColumnsAsJsonString = null;
        }

        public void SerializeSchemaColumns()
        {
            SchemaColumnsAsJsonString = JsonConvert.SerializeObject(SchemaColumns, settings);
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is SchemaColumnsSettings<T>))
            {
                return false;
            }
            
            SchemaColumnsSettings<T> cs = (SchemaColumnsSettings<T>)obj;
            
            if (this.SchemaColumnsAsJsonString != cs.SchemaColumnsAsJsonString)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            if (!string.IsNullOrEmpty(SchemaColumnsAsJsonString))
            {
                hashCode ^= SchemaColumnsAsJsonString.GetHashCode();
            }                               
            
            return hashCode;
        }
    }
}
