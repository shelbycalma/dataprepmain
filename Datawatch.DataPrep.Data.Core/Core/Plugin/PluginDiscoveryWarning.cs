﻿namespace Datawatch.DataPrep.Data.Core.Plugin
{
    /// <summary>
    /// Contains info on a possible error detected during plugin discovery sequence.
    /// </summary>
    public class PluginDiscoveryWarning
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PluginDiscoveryWarning"/> class.
        /// </summary>
        /// <param name="path">The plugin path.</param>
        /// <param name="message">The error message.</param>
        public PluginDiscoveryWarning(string path, string message)
        {
            this.Path = path;
            this.Message = message;
        }

        /// <summary>
        /// Gets the plugin path.
        /// </summary>
        /// <value>
        /// The plugin path.
        /// </value>
        public string Path { get; protected set; }

        /// <summary>
        /// Gets the error message text.
        /// </summary>
        /// <value>
        /// The error message.
        /// </value>
        public string Message { get; protected set; }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Path = '{0}', Message = '{1}'.",
                this.Path ?? string.Empty,
                this.Message ?? string.Empty);
        }
    }
}
