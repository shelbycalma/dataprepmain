﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class ParameterTable : RowLimitTable
    {
        public IEnumerable<ParameterValue> Parameters;

        public ParameterTable(IEnumerable<ParameterValue> parameters)
        {
            Parameters = parameters;
        }
    }
}
