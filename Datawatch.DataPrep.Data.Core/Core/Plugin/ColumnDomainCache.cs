﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public class ColumnDomainCache
    {
        private Dictionary<ColumnDomainCacheKey, ColumnDomain> cache =
            new Dictionary<ColumnDomainCacheKey, ColumnDomain>();

        public ColumnDomain this[ColumnDomainCacheKey key]
        {
            get { return cache.ContainsKey(key) ? cache[key] : null; }
            set { cache[key] = value; }
        }
    }
}
