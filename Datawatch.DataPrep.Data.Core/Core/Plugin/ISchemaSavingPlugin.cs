﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public interface ISchemaSavingPlugin : IDataPlugin
    {
        void SaveSchema(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters);
    }
}
