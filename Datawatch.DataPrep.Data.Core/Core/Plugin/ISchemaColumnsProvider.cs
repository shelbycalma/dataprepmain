﻿using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    public interface ISchemaColumnsProvider<T> where T : DatabaseColumn
    {
        SchemaColumnsSettings<T> SchemaColumnsSettings { get; }
    }
}
