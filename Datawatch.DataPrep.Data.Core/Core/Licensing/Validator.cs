// Original author: Klas Mellbourn, Capenta AB, e-mail klas@mellbourn.net, phone +46 708 80 00 00
// Some of the code inspired by http://msdn.microsoft.com/msdnmag/issues/04/11/xmlsignatures/default.aspx

using System;
using System.Diagnostics;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Schema;

namespace Datawatch.DataPrep.Data.Core.Licensing
{
    #region Exceptions

    /// <summary>
    /// Thrown when a license is invalid.
    /// </summary>
    public class InvalidLicenseException : Exception
    {
        /// <summary>
        /// Creates a new instance of <see cref="InvalidLicenseException"/>.
        /// </summary>
        /// <param name="message">reason that license is invalid</param>
        public InvalidLicenseException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="InvalidLicenseException"/>.
        /// </summary>
        /// <param name="message">the reason that license is invalid.</param>
        /// <param name="innerException">Exception that is the cause of the current exception.</param>
        public InvalidLicenseException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }

    /// <summary>
    /// Thrown when an license is missing.
    /// </summary>
    public class LicenseMissingException : InvalidLicenseException
    {
        /// <summary>
        /// Creates a new instance of <see cref="LicenseMissingException"/>
        /// </summary>
        /// <param name="message">description of how the license is missing</param>
        public LicenseMissingException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="LicenseMissingException"/>
        /// </summary>
        /// <param name="message">description of how the license is missing.</param>
        /// <param name="innerException">Exception that is the cause of the current exception.</param>
        public LicenseMissingException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }

    /// <summary>
    /// Thrown when an license has expired.
    /// </summary>
    public class LicenseExpiredException : InvalidLicenseException
    {
        /// <summary>
        /// Creates a new instance of <see cref="LicenseExpiredException"/>.
        /// </summary>
        /// <param name="message">description of expiration.</param>
        public LicenseExpiredException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="LicenseExpiredException"/>.
        /// </summary>
        /// <param name="message">description of expiration</param>
        /// <param name="innerException">Exception that is the cause of the current exception</param>
        public LicenseExpiredException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }

    /// <summary>
    /// Thrown when an XML signature of a license file is invalid for some reason
    /// </summary>
    public class SignatureValidationException : InvalidLicenseException
    {
        /// <summary>
        /// Creates a new instance of <see cref="SignatureValidationException"/>.
        /// </summary>
        /// <param name="message">reason that signature is invalid</param>
        public SignatureValidationException(string message)
            : base(message)
        {
        }

        /// <summary>
        /// Creates a new instance of <see cref="SignatureValidationException"/>.
        /// </summary>
        /// <param name="message">reason that signature is invalid</param>
        /// <param name="innerException">Exception that is the cause of the current exception</param>
        public SignatureValidationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

    }
    #endregion

    /// <summary>
    /// Validates XML signatures for licensing purposes.
    /// </summary>
    public static class Validator
    {
        #region constants
        /// <summary>
        /// Company name that must be embedded in the certificate for it to be regarded as valid
        /// </summary>
        private const string CompanyName = "Panopticon";

        /// <summary>
        /// xmlns namespace prefix used in the code to point to XMLDSIG schema
        /// </summary>
        private const string dsigNamespace = "dsig";

        /// <summary>
        /// xpath to signature
        /// </summary>
        private const string sigXPath = "//" + dsigNamespace + ":Signature";

        /// <summary>
        /// Default namespace for datawatch license XML files
        /// </summary>
        public const string defaultNamespace = "http://panopticon.com/PanopticonLicense/2007/11";

        /// <summary>
        /// The latest version of the Datawatch License schema
        /// </summary>
        public readonly static Version currentSchemaVersion = new Version(1, 1);
        
        /// <summary>
        /// name of the schema file
        /// </summary>
        private const string schemaUri = "PanopticonLicense2007-11.xsd";
        
        /// <summary>
        /// xmlns namespace prefix used in the code to point to the DatawatchLicense schema
        /// </summary>
        private const string panNamespace = "plic";

        /// <summary>
        /// Product name string for Datawatch Developer.NET that should appear in a properly formatted License file
        /// </summary>
        public const string PanopticonDeveloperDotNETProductName = "Panopticon Developer .NET";
        #endregion

        #region validation methods
        /// <summary>
        /// Handles validation errors when checking the license document
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static private void SchemaValidationHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    throw new InvalidLicenseException(string.Format(Properties.Resources.ExLicenseNotConformSchema, e.Message));
                case XmlSeverityType.Warning:
                    Debug.WriteLine("Schema Validation Warning: {0}", e.Message);
                    break;
            }
        }

        /// <summary>
        /// Check the signature profile, making sure that the signature
        /// is properly formed.
        /// This method makes sure that:
        ///   1) There is just one Signature element
        ///   2) There are is only one reference, to the envelope
        /// </summary>
        /// <param name="doc">the document with the signature to be verified</param>
        /// <param name="nsm">namespace of the signature</param>
        /// <returns></returns>
        private static bool CheckSignatureProfile(XmlDocument doc, XmlNamespaceManager nsm)
        {
            XmlNodeList sigList = doc.SelectNodes(sigXPath, nsm);
            if (sigList.Count != 1)
                return false;

            const string refXPath = sigXPath + "/" + dsigNamespace + ":SignedInfo/" + dsigNamespace + ":Reference";
            XmlNodeList refList = doc.SelectNodes(refXPath, nsm);
            if (refList.Count != 1)
                return false;

            const string transXPath = refXPath + "/" + dsigNamespace + ":Transforms/" + dsigNamespace + ":Transform/@Algorithm";
            XmlNodeList transList = doc.SelectNodes(transXPath, nsm);
            if (transList.Count != 1)
                return false;

            if (transList[0].InnerText != SignedXml.XmlDsigEnvelopedSignatureTransformUrl)
                return false;

            return true;
        }

        /// <summary>
        /// Verifies that a xml structure is valid according to schema and has a valid signature embedded
        /// </summary>
        /// <param name="xmlLicense">XML License structure to verify</param>
        /// <param name="cert">The certificate to check signature with</param>
        /// <returns>true if signature is valid</returns>
        public static bool Validate(XmlDocument xmlLicense, X509Certificate2 cert)
        {
            if (xmlLicense == null)
                throw new ArgumentNullException("xmlLicense");
            if (cert == null)
                throw new ArgumentNullException("cert");

            Debug.Assert(cert.PrivateKey == null, "You should validate using a standard certificate without a private key");

            // validate the schema
            // The following expects the schema file to have Build Action set to "Content" and "Copy if newer"
            //xmlLicense.Schemas.Add(defaultNamespace, schemaUri);

            // The following is an alternative schema validation that uses a schema that is an embedded resource
            // if you wish to use it, set the schemafile Build Action to "Embedded Resource" and "Do not copy"
            Type type = typeof(Validator);
            XmlReader xmlSchemaReader =
                XmlReader.Create(type.Assembly.GetManifestResourceStream(
                    type.Namespace + "." + schemaUri));
            if (xmlLicense.Schemas.Count == 0) {
                xmlLicense.Schemas.Add(
                    XmlSchema.Read(xmlSchemaReader, SchemaValidationHandler));
            }

            xmlLicense.Validate(SchemaValidationHandler);

            // Find the signature element in the document.
            XmlNamespaceManager nsm = new XmlNamespaceManager(new NameTable());
            nsm.AddNamespace(dsigNamespace, SignedXml.XmlDsigNamespaceUrl);
            XmlElement signatureElement = (XmlElement)xmlLicense.SelectSingleNode(
                sigXPath, nsm);

            // check Schema Version
            nsm.AddNamespace(panNamespace, defaultNamespace);
            string xpath = "/" + panNamespace + ":PanopticonLicense/@SchemaVersion";
            XmlNodeList licenseNodes =
                xmlLicense.SelectNodes(xpath, nsm);
            if (licenseNodes == null || licenseNodes.Count == 0)
                throw new InvalidLicenseException(Properties.Resources.ExUnknownSchemaVersion);
            Version licenseSchemaVersion = new Version(licenseNodes.Item(0).InnerText);
            // in the future, this might have to handle versions in a more complex way, since
            // old schema versions might not be able to be handled the same way
            if (currentSchemaVersion.CompareTo(licenseSchemaVersion) < 0)
                throw new InvalidLicenseException(Properties.Resources.ExLicenseSchemaIsAFuture);

            // check signature structure
            if (!CheckSignatureProfile(xmlLicense, nsm))
                return false;

            // Set up the signature for verification.
            SignedXml xmlSignature = new SignedXml(xmlLicense);
            try
            {
                xmlSignature.LoadXml(signatureElement);
            }
            catch (FormatException fe)
            {
                throw new SignatureValidationException(Properties.Resources.ExInvalidLicense, fe);
            }

            // Verify the signature, using the public key extracted
            // from the KeyInfo element.
            bool embeddedKeyUsedForSigning = xmlSignature.CheckSignature();
            if (!embeddedKeyUsedForSigning)
                return false;

            // Verify that company name is in the certificate
            if (!Regex.IsMatch(cert.Subject, CompanyName))
                return false;

            // verify that the correct certificate has been used 
            // (setting second param to false does not seem to work yet)
            return xmlSignature.CheckSignature(cert, true);
        }

        /// <summary>
        /// Verifies that the xml structure has a valid signature, that it contains 
        /// a particular product and a particular type node and returns the xml fragment that 
        /// corresponds to that node or a fallbackvalue for that product
        /// </summary>
        /// <param name="product">the name of the product that the type is within</param>
        /// <param name="licensedType">the type that is checked for licensing</param>
        /// <param name="xmlLicense">the xml structure presumably containing the license</param>
        /// <returns>xml fragment with license information</returns>
        /// <exception cref="Panopticon.Developer.Licensing.InvalidLicenseException">
        /// thrown if the license is somehow invalid. Not thrown if the node is missing! If the node is missing, 
        /// the fallback value is returned</exception>
        public static XmlNode Validate(string product, Type licensedType, XmlDocument xmlLicense)
        {
            if (String.IsNullOrEmpty(product))
                throw new ArgumentException("product");
            if (licensedType == null)
                throw new ArgumentNullException("licensedType");
            if (xmlLicense == null)
                throw new ArgumentNullException("xmlLicense");

            // signature is valid, check the type inside the xml structure
            XmlNamespaceManager nsm = new XmlNamespaceManager(new NameTable());
            nsm.AddNamespace(panNamespace, defaultNamespace);

            XmlNodeList licenseNodes =
                xmlLicense.SelectNodes("//" + panNamespace + ":Product[@Name = \"" + product
                + "\"]//" + panNamespace + ":TypeLicense[starts-with(\"" + licensedType.AssemblyQualifiedName + "\", @Type)]", nsm);

            XmlNode licenseNode;
            if (licenseNodes == null || licenseNodes.Count == 0)
            {
                // no exact license found, search for fallback
                XmlNodeList fallbackNodes =
                    xmlLicense.SelectNodes("//" + panNamespace + ":Product[@Name = \"" + product
                    + "\"]/" + panNamespace + ":Fallback", nsm);
                if (fallbackNodes == null || fallbackNodes.Count == 0)
                {
                    throw new LicenseMissingException(String.Format(Properties.Resources.ExTypeForProductNotFound, licensedType.AssemblyQualifiedName, product));
                }
                else
                {
                    licenseNode = fallbackNodes[0];
                }
            }
            else
            {
                licenseNode = licenseNodes[0];
            }

            // check public key token
            if (licenseNode.Attributes[PublicKeyTokenAttributeName] != null)
            {
                byte[] typePublicKeyToken = licensedType.Assembly.GetName().GetPublicKeyToken();
                StringBuilder typePublicKeyTokenStringBuilder = new StringBuilder(typePublicKeyToken.Length);
                foreach (byte b in typePublicKeyToken)
                {
                    typePublicKeyTokenStringBuilder.Append(b.ToString("x02"));
                }
                string licensedPublicKeyTokenString = licenseNode.Attributes[PublicKeyTokenAttributeName].Value;
                if (licensedPublicKeyTokenString != typePublicKeyTokenStringBuilder.ToString())
                {
                    throw new LicenseMissingException(String.Format(Properties.Resources.ExTypeWasNotSigned, licensedType.AssemblyQualifiedName, product));
                }
            }

            // check assembly Version number
            if (licenseNode.Attributes[VersionAttributeName] != null)
            {
                Version typeVersion = licensedType.Assembly.GetName().Version;
                string licensedVersionString = licenseNode.Attributes[VersionAttributeName].Value;
                if (!typeVersion.ToString().StartsWith(licensedVersionString))
                {
                    throw new LicenseMissingException(String.Format(Properties.Resources.ExLicenseIsNotCorrectVersion, licensedType.AssemblyQualifiedName, product));
                }
            }

            if (licenseNode.Attributes[ExpiryDateAttributeName] != null // note: no attributes to TypeLicense are compulsory
                && DateTime.Parse(licenseNode.Attributes[ExpiryDateAttributeName].Value,
                CultureInfo.InvariantCulture)
                < DateTime.Now)
            {
                throw new LicenseExpiredException(string.Format(Properties.Resources.ExLicenseHasExpired, licensedType.AssemblyQualifiedName));
            }

            return licenseNode;
        }
        #endregion

        #region extraction methods
        /// <summary>
        /// Name of the ExpiryDate attribute in a type license
        /// </summary>
        public static string ExpiryDateAttributeName = "ExpiryDate";
        /// <summary>
        /// Name of the PublicKeyToken attribute in a type license
        /// </summary>
        public static string PublicKeyTokenAttributeName = "PublicKeyToken";
        /// <summary>
        /// Name of the Version attribute in a type license
        /// </summary>
        public static string VersionAttributeName = "Version";

        /// <summary>
        /// Extracts the ExpiryDate attribute value from a TypeLicense XML Node
        /// </summary>
        /// <param name="licenseTypeFragment">TypeLicense XML fragment</param>
        /// <returns>boolean value of ExpiryDate attribute, or null if not present</returns>
        public static DateTime? GetExpiryDateValue(XmlNode licenseTypeFragment)
        {
            if (licenseTypeFragment.Attributes.Count == 0)
                return null;
            XmlAttribute expiryDateAttribute = licenseTypeFragment.Attributes[ExpiryDateAttributeName];
            if (expiryDateAttribute == null)
                return null;
            return Convert.ToDateTime(expiryDateAttribute.Value);
        }

        /// <summary>
        /// Name of the Evaluation attribute in a type license
        /// </summary>
        public static string EvaluationAttributeName = "Evaluation";

        /// <summary>
        /// Extracts the Evaluation attribute value from a TypeLicense XML Node
        /// </summary>
        /// <param name="licenseTypeFragment">TypeLicense XML fragment</param>
        /// <returns>boolean value of Evaluation attribute, or null if not present</returns>
        public static bool? GetEvaluationValue(XmlNode licenseTypeFragment)
        {
            if (licenseTypeFragment.Attributes.Count == 0)
                return null;
            XmlAttribute evaluationAttribute = licenseTypeFragment.Attributes[EvaluationAttributeName];
            if (evaluationAttribute == null)
                return null;
            return Convert.ToBoolean(evaluationAttribute.Value);
        }

        /// <summary>
        /// Name of the OEM attribute in a type license
        /// </summary>
        public static string OemAttributeName = "Oem";

        /// <summary>
        /// Extracts the Oem attribute value from a TypeLicense XML Node
        /// </summary>
        /// <param name="licenseTypeFragment">TypeLicense XML fragment</param>
        /// <returns>boolean value of Oem attribute, or null if not present</returns>
        public static bool? GetOemValue(XmlNode licenseTypeFragment)
        {
            if (licenseTypeFragment.Attributes.Count == 0)
                return null;
            XmlAttribute oemAttribute = licenseTypeFragment.Attributes[OemAttributeName];
            if (oemAttribute == null)
                return null;
            return Convert.ToBoolean(oemAttribute.Value);
        }
        #endregion
    }
}
