using System;
using System.ComponentModel;
using System.Xml;

namespace Datawatch.DataPrep.Data.Core.Licensing
{
    /// <summary>
    /// Subclass of <see cref="License"/> representing a Datawatch License.
    /// </summary>
    public sealed class DatawatchXmlLicense : License
    {
        private readonly XmlDocument license;
        private readonly DateTime? expires;
        private readonly bool evaluation;
        private readonly bool oem;
        
        private bool disposed;

        internal DatawatchXmlLicense(XmlDocument license,
            DateTime? expires, bool evaluation, bool oem)
        {
            this.license = license;
            this.expires = expires;
            this.evaluation = evaluation;
            this.oem = oem;
        }

        //TODO: What is this
        /// <summary>
        /// 
        /// </summary>
        ~DatawatchXmlLicense()
        {
            Dispose(false);
        }

        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed) {
                if (disposing) {
                    // Managed
                }
                // Unmanaged
                disposed = true;
            }
        }

        /// <summary>
        /// Get a value indicating if this <see cref="DatawatchXmlLicense"/> is an evaluation license.
        /// </summary>
        public bool Evaluation
        {
            get { return evaluation; }
        }

        /// <summary>
        /// Gets the <see cref="DateTime">expiration date</see>.
        /// </summary>
        public DateTime? ExpiryDate
        {
            get { return expires; }
        }

        /// <summary>
        /// Gets the <see cref="String">license key</see>.
        /// </summary>
        public override string LicenseKey
        {
            get { return license.Value; }
        }

        /// <summary>
        /// Is <see cref="Boolean">true</see> if the customer is OEM.
        /// </summary>
        public bool Oem
        {
            get { return oem; }
        }
    }
}
