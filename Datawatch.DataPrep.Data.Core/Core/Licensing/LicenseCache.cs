﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

namespace Datawatch.DataPrep.Data.Core.Licensing
{
    /// <summary>
    /// Helper class that caches licenses for performance.
    /// </summary>
    /// <remarks>
    /// <para>This cache sits on top of any caching that the
    /// <see cref="DatawatchXmlLicenseProvider"/> already performs (it caches
    /// the results of license queries against that class).</para>
    /// </remarks>
    public class LicenseCache
    {
        /// <summary>
        /// Used to synchronize creation of the singleton instance (all other
        /// synchronization is done against the singleton's
        /// <see cref="records"/> instance).
        /// </summary>
        private static readonly object singletonLock = new object();

        /// <summary>
        /// The only instance of this class.
        /// </summary>
        private static LicenseCache singleton;

        /// <summary>
        /// Cached results of license validations, per type.
        /// </summary>
        private readonly Dictionary<Type, LicenseRecord> records;
  

        /// <summary>
        /// Private constructor to ensure singleton.
        /// </summary>
        protected LicenseCache()
        {
            records = new Dictionary<Type, LicenseRecord>();
        }

        /// <summary>
        /// Clears the current contents of the cache.
        /// </summary>
        /// <remarks>
        /// <para>The <see cref="DatawatchXmlLicenseProvider"/> clears this
        /// cache automatically. The only situation where you would need to do
        /// this manually is if you are <em>not</em> caching the license file
        /// in the provider (the
        /// <see cref="DatawatchXmlLicenseProvider.CacheLicense"/> is set to
        /// <c>false</c>) and you dynamically add, remove, or replace license
        /// files.</para>
        /// </remarks>
        public static void Clear()
        {
            LicenseCache.Instance.ClearInternal();
        }

        private void ClearInternal()
        {
            lock (records) {
                records.Clear();
            }
        }

        /// <summary>
        /// Retrieves the license for a particular object or type.
        /// </summary>
        /// <param name="type">The type of the object to test.</param>
        /// <param name="instance">The instance of the type to test (may
        /// be null).</param>
        /// <returns>The license, or null if no valid license could be found
        /// for the object or type.</returns>
        /// <remarks>
        /// <para>This method does not throw license validation exceptions, but
        /// will throw an exception in the special case when
        /// <paramref name="instance"/> is <c>null</c> and the
        /// <paramref name="type"/> has a non-trivial base class (because then
        /// the type may have multiple licenses).</para>
       /// </remarks>
        public static DatawatchXmlLicense
            GetLicense(Type type, object instance)
        {
            if (type == null) {
                throw Exceptions.ArgumentNull("type");
            }
            if (instance == null &&
                type.BaseType != typeof(object) &&
                type.BaseType != null) {
                throw Exceptions.LicenseAmbiguity(type);
            }
            return LicenseCache.Instance.GetLicenseInternal(type, instance);
        }

        private DatawatchXmlLicense
            GetLicenseInternal(Type type, object instance)
        {
            DatawatchXmlLicense license;
            lock (records) {
                LicenseRecord record;
                if (!records.TryGetValue(type, out record)) {
                    // First time we saw type, create record.
                    record = new LicenseRecord();
                    records.Add(type, record);
                }
                // Have we tried to get the license before?
                if (!record.IsLicenseRetrieved) {
                    // Whatever the result, we have checked it.
                    // Just ignore the validated flag.
                    record.IsValidated = true;
                    record.IsLicenseRetrieved = true;
                    try {
                        // Let LicenseManager do its voodoo.
                        License generic =
                            LicenseManager.Validate(type, instance);
                        if (generic == null) {
                            // This should only happen for ambiguous base types
                            // which we check for in GetLicense, but then again
                            // LicenseManager has its quirks.
                            throw Exceptions.LicenseNotFound(
                                type, instance);
                        }
                        // Got here, so the only thing left to test is that
                        // the license has the correct type (with the cast).
                        record.License = (DatawatchXmlLicense) generic;
                        // If it had, then it's also valid.
                        record.IsValid = true;
                    }
                    catch (Exception ex) {
                        // Any validation exceptions (not found, invalid,
                        // expired...) plus the null test above, and cast
                        // if license was wrong type. Unfortunately we catch
                        // all other exceptions too, but it can't be helped
                        // since the license exceptions don't have a
                        // common base class.
                        record.Exception = ex;
                    }
                }
                // This will be null if invalid.
                license = record.License;
            }
            return license;
        }

        private static LicenseCache Instance
        {
            get {
                if (singleton == null) {
                    lock (singletonLock) {
                        if (singleton == null) {
                            singleton = new LicenseCache();
                        }
                    }
                }
                return singleton;
            }
        }

        /// <summary>
        /// Checks if a particular object or type has a valid license.
        /// </summary>
        /// <param name="type">The type of object to test.</param>
        /// <param name="instance">The instance of the type to test (may
        /// be null).</param>
        /// <returns><c>True</c> if a valid license was found.</returns>
        /// <remarks>
        /// <para>This method does not throw license exceptions.</para>
        /// </remarks>
        public static bool IsValid(Type type, object instance)
        {
            if (type == null) {
                throw Exceptions.ArgumentNull("type");
            }
            return LicenseCache.Instance.IsValidInternal(type, instance);
        }

        private bool IsValidInternal(Type type, object instance)
        {
            bool valid = false;
            lock (records) {
                LicenseRecord record;
                if (!records.TryGetValue(type, out record)) {
                    // First time we saw type, create record.
                    record = new LicenseRecord();
                    records.Add(type, record);
                }
                // Have we validated before?
                if (!record.IsValidated) {
                    // Cannot have license if not validated.
                    Debug.Assert(!record.IsLicenseRetrieved);
                    // Whatever the result we will have validated.
                    record.IsValidated = true;
                    // LicenseManager.IsValid doesn't throw exceptions. But
                    // as a bonus it will give us the license in some cases.
                    License generic = null;
                    record.IsValid =
                        LicenseManager.IsValid(type, instance, out generic);
                    // If valid, and we got an ok license, we'll store it
                    // in the record so we don't have to do it again when
                    // someone asks for it.
                    if (generic != null && generic is DatawatchXmlLicense) {
                        record.License = (DatawatchXmlLicense) generic;
                        record.IsLicenseRetrieved = true;
                    }
                }
                valid = record.IsValid;
            }
            return valid;
        }

        /// <summary>
        /// Validates that a particular object or type has a valid license.
        /// </summary>
        /// <param name="type">The type of the object to test.</param>
        /// <param name="instance">The instance of the type to test (may
        /// be null).</param>
        /// <remarks>
        /// <para>This method throws an exception if a license could not be
        /// found or the found license is invalid.</para>
        /// </remarks>
        public static void Validate(Type type, object instance)
        {
            if (type == null) {
                throw Exceptions.ArgumentNull("type");
            }
            LicenseCache.Instance.ValidateInternal(type, instance);
        }

        private void ValidateInternal(Type type, object instance)
        {
            // We could have implemented this method as "if (!IsValid) throw"
            // but LicenseManager.Validate is the only method that lets the
            // license provider throw exceptions (and they may have info that
            // we want). Also, we can't use GetLicense either, because it will
            // fail in some circumstances that we want to allow here.

            lock (records) {
                LicenseRecord record;
                if (!records.TryGetValue(type, out record)) {
                    // First time we saw type, create record.
                    record = new LicenseRecord();
                    records.Add(type, record);
                }
                if (record.IsValidated && record.IsValid) {
                    // Everything is ok.
                    return;
                }
                // If it isn't valid and we've tried to retrieve the
                // license, then we already have the exception.
                if (!record.IsLicenseRetrieved) {
                    // Not validated yet, or validated and failed but we haven't
                    // tried to retrieve the license (so we don't know why it
                    // was invalid).
                    // Regardless of what, we will have validated.
                    record.IsValidated = true;
                    try {
                        // As a bonus, if validation succeeds, we get the
                        // license, so store it if we do.
                        License generic =
                            LicenseManager.Validate(type, instance);
                        // If we got here, then the license is valid.
                        record.IsValid = true;
                        // If we got an ok license, then save it. If we didn't
                        // we don't do anything (wait until it's asked for).
                        if (generic != null &&
                            generic is DatawatchXmlLicense) {
                            record.License = (DatawatchXmlLicense) generic;
                            record.IsLicenseRetrieved = true;
                        }
                    }
                    catch (Exception ex) {
                        // Any validation exceptions (not found, invalid,
                        // expired...). Unfortunately we catch all other
                        // exceptions too, but it can't be helped since the
                        // license exceptions don't have a common base class.
                        record.Exception = ex;
                    }
                }
                if (record.Exception != null) {
                    throw record.Exception;
                }
            }
        }


        /// <summary>
        /// The result of license validation for a particular type.
        /// </summary>
        private class LicenseRecord
        {
            /// <summary>
            /// The type was validated ok by the <see cref="LicenseManager"/>.
            /// </summary>
            public bool IsValid { get; set; }

            /// <summary>
            /// Validation has been performed for the type (and the
            /// <see cref="IsValid"/> property holds the result).
            /// </summary>
            /// <remarks>
            /// <para>A type can be validated without retrieving its
            /// license, so <see cref="IsValidated"/> can be <c>true</c> when
            /// <see cref="IsLicenseRetrieved"/> is <c>false</c>.</para>
            /// </remarks>
            public bool IsValidated { get; set; }

            /// <summary>
            /// The license returned by the <see cref="LicenseManager"/> for
            /// the type, if any.
            /// </summary>
            public DatawatchXmlLicense License { get; set; }

            /// <summary>
            /// The license has been retrieved (and the <see cref="License"/>
            /// property holds the result).
            /// </summary>
            /// <remarks>
            /// <para>If validation failed, e.g. no license was found, the
            /// <see cref="License"/> property will be null and the
            /// <see cref="Exception"/> property tells why.</para>
            /// <para>If this property is <c>true</c> then
            /// <see cref="IsValidated"/> will also be <c>true</c>.</para>
            /// </remarks>
            public bool IsLicenseRetrieved { get; set; }

            /// <summary>
            /// Exception thrown by validation for the type, if any.
            /// </summary>
            public Exception Exception { get; set; }
        }
    }
}
