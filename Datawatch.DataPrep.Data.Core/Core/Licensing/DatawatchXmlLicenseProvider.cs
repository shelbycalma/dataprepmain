using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Licensing
{
    /// <summary>
    /// <see cref="LicenseProvider"/> subclass capable of validating
    /// Datawatch XML Licenses.
    /// </summary>
    /// <remarks>
    /// <para>Normally, licenses for the SDK components that require them are
    /// validated on demand (at some point during the component's
    /// initialization). Each time this happens, the
    /// <see cref="DatawatchXmlLicenseProvider"/> attempts to locate the
    /// license file in a number of default locations (in this order):
    /// <ul>
    ///   <li>In the same directory as the assembly that contains the SDK
    ///   component being validated, and with the same name as that assembly
    ///   but with a &quot;.DatawatchLicense.xml&quot; extension.</li>
    ///   <li>For the ASP .NET WebForms controls, the current AppDomain's base
    ///   directory.</li>
    ///   <li>For WebForms, the bin folder in the base directory.</li>
    ///   <li>A file with the same name and location as the
    ///   <see cref="Assembly.GetEntryAssembly()"/>'s
    ///   <see cref="Assembly.ManifestModule"/> but with a
    ///   &quot;.DatawatchLicense.xml&quot; extension.</li>
    ///   <li>A file with the name &quot;DatawatchLicense.xml&quot; in the
    ///   manifest module's directory.</li>
    /// </ul>
    /// This procedure enables you to have multiple license files, e.g. one
    /// application-wide license combined with specific licenses for individual
    /// visualizations.</para>
    /// <para>The license file is not cached by default once it has been
    /// located. This is so the multiple license file scheme will work without
    /// surprises, but also so you can add and remove licenses at run
    /// time. To turn on license file caching, set the static
    /// <see cref="CacheLicense"/> property to <c>true</c>. If you do this,
    /// you can only use one single license file.</para>
    /// <para>There are two ways to override the normal license file search.
    /// The first is to explicitly give the location of the license file using
    /// the <see cref="LicenseFilePath"/> property. The second is to manually
    /// load the file (e.g. from a resource or server) and set the entire
    /// contents of it using the <see cref="LicenseFileContents"/>
    /// property.</para>
    /// <para>For performance reasons, the output of this class is also cached
    /// by the <see cref="LicenseCache"/>. Components in the SDK validate their
    /// licenses through that cache, and in rare circumstances you may need to
    /// manipulate it directly (see <see cref="LicenseCache.Clear()"/>).</para>
    /// </remarks>
    public sealed class DatawatchXmlLicenseProvider : LicenseProvider
    {
        /// <summary>
        /// Name of the resource (in the Core assembly) where the validation
        /// certificate is stored.
        /// </summary>
        private const string CertificateResource =
            "Datawatch.DataPrep.Data.Core.Licensing.PanopticonLicenseCertificate.cer";

        /// <summary>
        /// Name of this product as it appears in the license files.
        /// </summary>
        private const string ProductName = "Panopticon Developer .NET";

        /// <summary>
        /// Certificate contains the public key corresponding to the private
        /// key used internally at Datawatch for code signing.
        /// </summary>
        private readonly X509Certificate2 certificate;

        /// <summary>
        /// Should the license be cached (in the
        /// <see cref="shouldCacheLicenseXml"/> field)?
        /// </summary>
        private static bool shouldCacheLicenseXml;

        /// <summary>
        /// The license file contents if provided by the user
        /// (<see cref="LicenseFileContents"/>).
        /// </summary>
        private static string licenseXmlContents;

        /// <summary>
        /// The license file path if provided by the user
        /// (<see cref="LicenseFilePath"/>).
        /// </summary>
        private static string licenseXmlPath;

        /// <summary>
        /// Cached copy of license once validated (if
        /// <see cref="shouldCacheLicenseXml"/> is true).
        /// </summary>
        private static XmlDocument cachedLicenseXml;
        
        
        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="DatawatchXmlLicenseProvider"/> class.
        /// </summary>
        public DatawatchXmlLicenseProvider()
        {
            certificate = LoadCertificate();
        }


        /// <summary>
        /// Indicates whether the <see cref="DatawatchXmlLicenseProvider"/>
        /// should cache the license once it has been located - the default is
        /// not to cache.
        /// </summary>
        /// <remarks>
        /// <para>If you use this feature, make sure that you only have one
        /// single license file (see comments on this class). To clear the
        /// cached license, set this property to <c>false</c>.</para>
        /// </remarks>
        public static bool CacheLicense
        {
            get { return shouldCacheLicenseXml; }
            set { 
                shouldCacheLicenseXml = value;
                if (!shouldCacheLicenseXml) {
                    cachedLicenseXml = null;
                }
                LicenseCache.Clear();
            }
        }


        /// <summary>
        /// Gets a <see cref="License">license</see>.
        /// </summary>
        /// <param name="context"><see cref="LicenseContext"/></param>
        /// <param name="type"><see cref="Type"/></param>
        /// <param name="instance"></param>
        /// <param name="allowExceptions"></param>
        /// <returns><see cref="License"/></returns>
        public override License GetLicense(LicenseContext context,
            Type type, object instance, bool allowExceptions)
        {
            XmlDocument xml = null;

            if (shouldCacheLicenseXml) {
                xml = cachedLicenseXml;
            } else if (cachedLicenseXml != null) {
                cachedLicenseXml = null;
            }

            if (xml == null) {
                try {
                    xml = LocateLicenseXml(type);
                } catch (Exception ex) {
                    if (allowExceptions) {
                        throw Exceptions.LicenseUnexpected(
                            type, instance, ex);
                    }
                    return null;
                }
            }
            if (xml == null) {
                if (allowExceptions) {
                    throw Exceptions.LicenseNotFound(type, instance);
                }
                return null;
            }
            
            return ValidateLicenseXml(type, allowExceptions, xml);
        }

        
        /// <summary>
        /// This property can be used to override the default license search
        /// by providing the contents of the license file directly.
        /// </summary>
        /// <remarks>
        /// <para>This feature can be useful if you want to embed the license
        /// as a resource in your application, or want to &quot;hide&quot; it
        /// from end users in some other way. If this property is set, the
        /// value is required to be the contents of a valid Datawatch XML
        /// license file. The default value is <c>null</c>, which means that
        /// the <see cref="DatawatchXmlLicenseProvider"/> will try to locate
        /// the file in the standard manner (see comments on this class).</para>
        /// </remarks>
        /// <seealso cref="LicenseFilePath"/>
        public static string LicenseFileContents
        {
            get { return licenseXmlContents; }
            set {
                licenseXmlContents = value;
                LicenseCache.Clear();
            }
        }


        /// <summary>
        /// This property can be used to override the default license search
        /// by providing the path to the license file directly.
        /// </summary>
        /// <remarks>
        /// <para>This feature can be useful if for some reason you can't have
        /// the license file in any of the standard locations (e.g. if you're
        /// installing the binaries to Program Files on a Vista system but want
        /// the license to be in the user's application data folder).</para>
        /// <para>If this property is set, the value must be the full path to
        /// the Datawatch XML license file (including the file name itself).
        /// The default value is <c>null</c>, which means that the
        /// <see cref="DatawatchXmlLicenseProvider"/> will try to locate
        /// the file in the standard manner (see comments on this class).</para>
        /// </remarks>
        /// <seealso cref="LicenseFileContents"/>
        public static string LicenseFilePath
        {
            get { return licenseXmlPath; }
            set {
                licenseXmlPath = value;
                LicenseCache.Clear();
            }
        }


        private X509Certificate2 LoadCertificate()
        {
            Assembly assembly = GetType().Assembly;
            Stream resource =
                assembly.GetManifestResourceStream(CertificateResource);
            MemoryStream memory = new MemoryStream();
            byte[] buffer = new byte[1024];
            int read;
            while ((read = resource.Read(buffer, 0, buffer.Length)) > 0) {
                memory.Write(buffer, 0, read);
            }
            return new X509Certificate2(memory.ToArray());
        }


        private XmlDocument LoadLicenseFile(Module module, bool useFileName)
        {
            string name = module.FullyQualifiedName;
            string file;
            if (useFileName) {
                file = Path.Combine(Path.GetDirectoryName(name),
                    Path.GetFileNameWithoutExtension(name) +
                        ".DatawatchLicense.xml");
            } else {
                file = Path.Combine(Path.GetDirectoryName(name),
                    "DatawatchLicense.xml");
            }
            return LoadLicenseFile(file);
        }


        private XmlDocument LoadLicenseFile(string file)
        {
            if (!File.Exists(file)) {
                return null;
            }
            XmlDocument xml = new XmlDocument();
            xml.PreserveWhitespace = true;
            xml.Load(file);
            return xml;
        }


        private XmlDocument LocateLicenseXml(Type type)
        {
            XmlDocument xml;

            if (licenseXmlContents != null) {
                xml = new XmlDocument();
                xml.PreserveWhitespace = true;
                xml.LoadXml(licenseXmlContents);
                return xml;
            }

            if (licenseXmlPath != null) {
                return LoadLicenseFile(licenseXmlPath);
            }
            
            if ((xml = LoadLicenseFile(type.Module, true)) != null) {
                return xml;
            }

            if (Assembly.GetEntryAssembly() == null) {
                // If GetEntryAssembly is null you are probably running a
                // unit test. Try to find license beside type assembly.
                if ((xml = LoadLicenseFile(type.Module, false)) != null) {
                    return xml;
                }
            }

            if ((xml = LoadLicenseFile(
                Assembly.GetEntryAssembly().ManifestModule, true)) != null) {
                return xml;
            }
            if ((xml = LoadLicenseFile(
                Assembly.GetEntryAssembly().ManifestModule, false)) != null) {
                return xml;
            }
            if ((xml = LoadLicenseFile(type.Module, false)) != null) {
                return xml;
            }

            // Whoops.
            return xml;
        }


        private DatawatchXmlLicense ValidateLicenseXml(
            Type type, bool allowExceptions, XmlDocument xml)
        {
            DatawatchXmlLicense license = null;
            try {
                if ((shouldCacheLicenseXml && xml != cachedLicenseXml) || !shouldCacheLicenseXml)
                {
                    if (!Validator.Validate(xml, certificate)) 
                    {
                        throw new SignatureValidationException(Resources.ExInvalidSignature);
                    }
                    if (shouldCacheLicenseXml)
                    {
                        cachedLicenseXml = xml;
                    }
                }

                XmlNode node =
                    Validator.Validate(ProductName, type, xml);
                DateTime? expires = Validator.GetExpiryDateValue(node);
                bool? evaluation = Validator.GetEvaluationValue(node);
                bool? oem = Validator.GetOemValue(node);
                license = new DatawatchXmlLicense(xml, expires,
                    evaluation.HasValue ? evaluation.Value : false,
                    oem.HasValue ? oem.Value : false);
            } catch (Exception ex) {
                if (allowExceptions) {
                    throw ex;
                }
                return null;
            }
            return license;
        }
    }
}
