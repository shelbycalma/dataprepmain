﻿using System;
using System.ComponentModel;
using System.Data;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core
{
    public class Exceptions : Framework.Exceptions
    {
        public static Exception InvalidColumnDefinition()
        {
            return CreateInvalidOperation(Resources.ExInvalidColumnDefinition);
        }

		public static Exception InvalidQuery()
		{
			return CreateInvalidOperation(Resources.ExInvalidQuery);
		}

		public static Exception BadColumnType(Type actual)
        {
            return CreateArgument(
                Format(Resources.ExBadColumnType, actual.Name));
        }

        public static Exception LicenseAmbiguity(Type type)
        {
            // This is NOT a license validation exception.
            return CreateInvalidOperation(
                Format(Resources.ExLicenseAmbiguity, type.Name));
        }

        public static Exception DataPluginNotAvailable(string id)
        {
            return CreateArgument(Format(Resources.ExDataPluginNotAvailable, id));
        }

        public static Exception DataPluginUINotAvailable(string id)
        {
            return CreateArgument(Format(Resources.ExDataPluginNotAvailable, id));
        }

        /// <summary>
        /// This method is internal to the Datawatch Developer SDK.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static Exception LicenseNotFound(Type type, object instance)
        {
            return CreateLicense(type, instance,
                Format(Resources.ExLicenseNotFound,
                    type.FullName, type.Assembly.FullName));
        }

        public static Exception LicenseUnexpected(
            Type type, object instance, Exception inner)
        {
            return CreateLicense(type, instance,
                Format(Resources.ExLicenseUnexpected,
                    type.Name, inner.GetType().FullName, inner.Message));
        }

        public static Exception ValueConverterCantConvertBack(Type type)
        {
            return new NotImplementedException(Format(
                Resources.ExValueConverterCantConvertBack, type.Name));
        }

        public static Exception ColumnTypeNotSupported(DataColumn column)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExColumnTypeNotSupported,
                column.ColumnName, column.DataType.Name));
        }

        public static Exception TableColumnNotInTable(string columnName)
        {
            return CreateArgument(string.Format(
                Resources.ExTableColumnNotInTable, columnName));
        }

        public static Exception TableColumnWrongType(Type type)
        {
            return CreateArgument(string.Format(
                Resources.ExTableColumnWrongType, type.Name));
        }

        public static Exception TableRowIndexInvalid(
            string paramName, int index, int rowCount)
        {
            return new ArgumentOutOfRangeException(paramName, index, string.Format(
                Resources.ExTableRowIndexInvalid, index, rowCount));
        }

        public static Exception TableRowInOtherTable()
        {
            return CreateArgument(Resources.ExTableRowInOtherTable);
        }

        public static Exception TableRowWrongType(Type type)
        {
            return CreateArgument(string.Format(
                Resources.ExTableRowWrongType, type.Name));
        }

        public static Exception TableUnlockedTooManyTimes()
        {
            return CreateInvalidOperation(
                Resources.ExTableUnlockedTooManyTimes);
        }

        public static Exception WrapperMetaDataBadPropertyName(string property)
        {
            return CreateArgument(Format(
                Resources.ExWrapperMetaDataBadPropertyName, property));
        }

        public static Exception WrapperMetaDataBadOwnerType(
            string columnName, Type actualType)
        {
            return CreateArgument(Format(
                Resources.ExWrapperMetaDataBadOwnerType,
                columnName, actualType.Name));
        }

        public static Exception WrapperMetaDataBadSourceType(
            string columnName, Type columnType,
            Type expectedSourceType, Type actualSourceType)
        {
            return CreateArgument(Format(
                Resources.ExWrapperMetaDataBadSourceType,
                columnName, columnType,
                expectedSourceType.Name, actualSourceType.Name));
        }

        public static Exception TableCannotWrapColumn(
            Type tableType, string columnName, Type columnType)
        {
            return CreateArgument(Format(
                Resources.ExTableCannotWrapColumn,
                tableType.Name, columnName, columnType.Name));
        }

        public static Exception DerivedTableBadSourceIndex(int index,
            int count)
        {
            return CreateIndexOutOfRangeException(
                Format(Resources.ExDerivedTableBadSourceIndex, index, count));
        }

        public static Exception BadSourceTableCount(int expected, int actual)
        {
            return CreateArgument(
                Format(Resources.ExBadSourceTableCount, expected, actual));
        }

        public static Exception IncorrectSettingValue(string name)
        {
            return CreateArgument(
                Format(Resources.ExIncorrectSettingValue, name));
        }

        public static Exception UnknownDataPluginType(string type)
        {
            return CreateArgument(
                Format(Resources.ExUnknownDataPluginType, type));
        }

        protected static IndexOutOfRangeException
            CreateIndexOutOfRangeException(string message)
        {
            return (IndexOutOfRangeException)LogException(
                new IndexOutOfRangeException(message));
        }

        private static Exception CreateLicense(
            Type type, object instance, string message)
        {
            return new LicenseException(type, instance, message);
        }
    }
}
