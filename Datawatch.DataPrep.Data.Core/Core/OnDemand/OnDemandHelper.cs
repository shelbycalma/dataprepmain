﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.OnDemand
{
    public class OnDemandHelper
    {
        private const string AggregatePrefix = "a";

        public static void BuildOnDemandQuery(QueryBuilder queryBuilder,
            OnDemandParameters onDemandParameters)
        {
            if (queryBuilder == null)
                throw Exceptions.ArgumentNull(
"queryBuilder");

            if (onDemandParameters == null || onDemandParameters.IsEmpty)
            {
                queryBuilder.Where.Predicate = new Comparison(Operator.Equals,
                    new ValueParameter(1), new ValueParameter(0));
            }
            else
            {
                int depth = onDemandParameters.Depth;

                if (onDemandParameters.BreakdownColumns != null)
                {
                    for (int i = 0; i < depth; i++)
                    {
                        string col = onDemandParameters.BreakdownColumns[i];
                        queryBuilder.Select.AddColumn(col);
                        queryBuilder.GroupBy.AddColumn(col);
                    }
                }

                if (onDemandParameters.Aggregates != null &&
                    onDemandParameters.Aggregates.Length > 0)
                {
                    foreach (OnDemandAggregate aggregate in
                        onDemandParameters.Aggregates)
                    {
                        queryBuilder.Select.AddAggregate(
                            aggregate.Columns,
                            aggregate.FunctionType,
                            aggregate.Alias != null ?
                            aggregate.Alias : aggregate.Key);
                    }
                }

                if (onDemandParameters.AuxiliaryColumns != null &&
                    onDemandParameters.AuxiliaryColumns.Length > 0)
                {
                    foreach (string column in
                        onDemandParameters.AuxiliaryColumns)
                    {
                        queryBuilder.Select.AddColumn(column);
                        queryBuilder.GroupBy.AddColumn(column);
                    }
                }

                if (onDemandParameters.Bucketings != null &&
                    onDemandParameters.Bucketings.Length > 0)
                {
                    foreach (Bucketing bucketing in onDemandParameters.Bucketings)
                    {
                        if (!(bucketing is TimeBucketing)) continue;
                        TimeBucketing timeBucketing = (TimeBucketing)bucketing;
                        queryBuilder.Select.AddTimePart(
                            timeBucketing.SourceColumnName,
                            timeBucketing.TimePart,
                            timeBucketing.Name);
                        queryBuilder.GroupBy.AddTimePart(
                            timeBucketing.SourceColumnName,
                            timeBucketing.TimePart,
                            timeBucketing.Name);
                    }
                }

                Predicate drillPredicate = null;

                if (onDemandParameters.DrillPath != null &&
                    onDemandParameters.DrillPath.Length > 0 &&
                    onDemandParameters.BreakdownColumns != null &&
                    onDemandParameters.DrillPath.Length <=
                        onDemandParameters.BreakdownColumns.Length)
                {
                    for (int i = 0; i < onDemandParameters.DrillPath.Length; i++)
                    {
                        drillPredicate = new AndOperator(drillPredicate,
                            new Comparison(Operator.Equals,
                                new ColumnParameter(
                                    onDemandParameters.BreakdownColumns[i]),
                                new StringParameter(
                                    onDemandParameters.DrillPath[i])));
                    }

                }

                Predicate predicate = drillPredicate;

                if (onDemandParameters.Predicates != null &&
                    onDemandParameters.Predicates.Length > 0)
                {
                    foreach (Predicate p in onDemandParameters.Predicates)
                    {
                        predicate = new AndOperator(predicate, p);
                    }
                }

                queryBuilder.Where.Predicate =
                    new AndOperator(queryBuilder.Where.Predicate, predicate);
            }
        }

        public static void BuildOnDemandPreviewQuery(QueryBuilder queryBuilder,
            IEnumerable<string> selectedColumns, int rowCount)
        {
            if (queryBuilder == null)
                throw Exceptions.ArgumentNull(
"queryBuilder");

            queryBuilder.Limit = rowCount;
            if (selectedColumns == null) return;

            foreach (string column in selectedColumns)
            {
                queryBuilder.Select.AddColumn(column,
                    null);
            }
        }

        //public static OnDemandParameters CreateOnDemandParameters(
        //    IOnDemandConsumer consumer, Predicate[] predicates)
        //{
        //    OnDemandParameters parameters = null;

        //    if (consumer is IStateProducerPart)
        //    {
        //        IStateProducerPart stateProducerPart =
        //            (IStateProducerPart)consumer;
        //        if (stateProducerPart.State != null)
        //        {
        //            parameters = CreateOnDemandParameters(stateProducerPart);
        //        }
        //    }
        //    if (parameters == null)
        //    {
        //        parameters = new OnDemandParameters()
        //        {
        //            ConsumerId = consumer.Id
        //        };
        //    }

        //    OnDemandAggregate[] aggregates = consumer.OnDemandAggregates;

        //    if (aggregates != null)
        //    {
        //        parameters.Aggregates =
        //            CombineAggregateArrays(parameters.Aggregates,
        //                aggregates);
        //    }
        //    string[] auxColumns = consumer.OnDemandColumns;
        //    if (auxColumns != null && auxColumns.Length > 0)
        //    {
        //        parameters.AuxiliaryColumns =
        //            CombineColumnArrays(parameters.AuxiliaryColumns,
        //            auxColumns);
        //    }
        //    parameters.Domains = consumer.OnDemandDomains;

        //    Model.Bucketing[] bucketings = consumer.OnDemandBucketings;
        //    if (bucketings != null && bucketings.Length > 0)
        //    {
        //        parameters.Bucketings =
        //            CombineArrays(parameters.Bucketings, bucketings);
        //    }

        //    parameters.Predicates = predicates;

        //    // Create a short alias for each aggregate to avoid running into 
        //    // identifier lenght issues in certain database implementations.
        //    if (parameters.Aggregates != null)
        //    {
        //        int index = 1;
        //        foreach (OnDemandAggregate aggregate in parameters.Aggregates)
        //        {
        //            aggregate.Alias = AggregatePrefix + index++;
        //        }
        //    }

        //    return parameters;
        //}

        //private static OnDemandParameters CreateOnDemandParameters(
        //    IStateProducerPart stateProducer)
        //{
        //    IVisualizationState state = stateProducer.State;
        //    OnDemandParameters parameters =
        //        new OnDemandParameters()
        //        {
        //            ConsumerId = stateProducer.Id,
        //            Aggregates = GetAggregates(state),
        //            DrillPath = GetDrillPath(state),
        //            VisibleDepth = GetNumericDepth(state),
        //        };
        //    AddBreakdownColumns(parameters, state);
        //    AddAuxiliaryColumns(parameters, state);

        //    return parameters;
        //}

        //private static void AddAuxiliaryColumns(
        //    OnDemandParameters parameters, IVisualizationState state)
        //{
        //    ISet<string> auxColumns =
        //        parameters.AuxiliaryColumns != null ?
        //        new HashSet<string>(parameters.AuxiliaryColumns) :
        //        new HashSet<string>();

        //    ISet<Model.Bucketing> bucketings =
        //        parameters.Bucketings != null ?
        //        new HashSet<Model.Bucketing>(parameters.Bucketings) :
        //        new HashSet<Model.Bucketing>();

        //    IVariableCollection variables = state.Variables;
        //    HashSet<IFormatterVariableClass> formatterSet =
        //        new HashSet<IFormatterVariableClass>();
        //    IVariableType[] variableTypes = variables.GetVariableTypes();
        //    for (int i = 0; i < variableTypes.Length; i++)
        //    {
        //        IVariableType variableType = variableTypes[i];
        //        String[] aliases = variables.GetAliases(variableType);
        //        for (int j = 0; j < aliases.Length; j++)
        //        {
        //            VariableSlot slot = new VariableSlot(
        //                variableType, aliases[j]);
        //            IVariable variable = variables.GetVariable(slot);
        //            AddFormatterVariables(variable, formatterSet);
        //        }
        //    }

        //    foreach (IFormatInfoVariableClass formatter in formatterSet)
        //    {
        //        if (formatter is IColumnBasedVariable &&
        //            (formatter is ITextAggregatedVariableClass ||
        //            formatter is ITimeAggregatedVariableClass ||
        //            formatter is TextTimeseriesAggregatedVariable))
        //        {
        //            IColumnBasedVariable columns = (IColumnBasedVariable)formatter;

        //            foreach (IColumn column in columns.Columns)
        //            {
        //                IColumnMetaData metaData = TableUtils.GetAuxMetaData(column);
        //                if (metaData == null)
        //                {
        //                    auxColumns.Add(column.Name);
        //                    continue;
        //                }
        //                if (metaData is ITextBucketColumnMetaData)
        //                {
        //                    auxColumns.Add(((ITextBucketColumnMetaData)metaData)
        //                        .TextColumn.Name);
        //                    continue;
        //                }
        //                if (metaData is ITimeBucketColumnMetaData)
        //                {
        //                    ITimeBucketColumnMetaData timeBucketMeta =
        //                        (ITimeBucketColumnMetaData)metaData;
        //                    bucketings.Add(new TimeBucketing(
        //                        column.Name, timeBucketMeta.Title, timeBucketMeta.TimeColumn.Name,
        //                            timeBucketMeta.TimePart));
        //                    continue;
        //                }
        //                if (metaData is ICalculatedColumnMetaData &&
        //                    metaData is ITextColumnMetaData)
        //                {
        //                    ICalculatedColumnMetaData calculatedTextMetaData =
        //                        (ICalculatedColumnMetaData)metaData;
        //                    IEnumerable<IColumn> referredColumns = CalcUtils.GetReferredColumnsRecursive(
        //                        column.Table, calculatedTextMetaData.Formula, column);
        //                    foreach (IColumn referredColumn in referredColumns)
        //                    {
        //                        auxColumns.Add(referredColumn.Name);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    parameters.AuxiliaryColumns =
        //        auxColumns.Count > 0 ? auxColumns.ToArray() : null;
        //    parameters.Bucketings =
        //        bucketings.Count > 0 ? bucketings.ToArray() : null;
        //}

        //private static OnDemandAggregate[] GetAuxAggregates(IColumn col)
        //{
        //    IColumnMetaData colMeta = TableUtils.GetAuxMetaData(col);

        //    ICalculatedColumnMetaData calcColMeta =
        //        colMeta as ICalculatedColumnMetaData;
        //    if (calcColMeta != null)
        //    {
        //        List<OnDemandAggregate> list = new List<OnDemandAggregate>();

        //        string formula = calcColMeta.Formula;
        //        ITable table = col.Table;
        //        for (int i = 0; i < table.ColumnCount; i++)
        //        {
        //            IColumn c = table.GetColumn(i);
        //            if (formula.Contains("[" + c.Name + "]"))
        //            {
        //                OnDemandAggregate columnInfo =
        //                    new OnDemandAggregate((INumericColumn)c);
        //                list.Add(columnInfo);
        //            }
        //        }

        //        return list.ToArray();
        //    }

        //    IBucketColumnMetaData bucketColMeta =
        //        colMeta as IBucketColumnMetaData;
        //    if (bucketColMeta != null &&
        //        bucketColMeta.AuxColumn is INumericColumn)
        //    {
        //        return new[]
        //        {
        //            new OnDemandAggregate((INumericColumn)bucketColMeta.AuxColumn)
        //        };
        //    }

        //    //TODO: Add check for IRankingColumnMetaData

        //    return null;
        //}

        //private static void AddBreakdownColumns(OnDemandParameters parameters,
        //    IVisualizationState state)
        //{
        //    IBreakdownVariable breakdown = CommonVariables.GetBreakdown(state);
        //    if (breakdown == null || breakdown.ColumnCount == 0)
        //    {
        //        parameters.BreakdownColumns = null;
        //        return;
        //    }
        //    ISet<Model.Bucketing> bucketings =
        //        new HashSet<Model.Bucketing>();
        //    if (parameters.Bucketings != null)
        //    {
        //        bucketings.UnionWith(parameters.Bucketings);
        //    }

        //    List<string> list = new List<string>();
        //    int visibleDepth = GetNumericDepth(state);
        //    for (int i = 0; i < breakdown.ColumnCount; i++)
        //    {
        //        ITextColumn column = breakdown.GetColumn(i);
        //        IColumnMetaData metaData = TableUtils.GetAuxMetaData(column);
        //        if (metaData == null)
        //        {
        //            list.Add(column.Name);
        //            continue;
        //        }

        //        if (visibleDepth == -1 || i < visibleDepth)
        //        {
        //            if (metaData is ITextBucketColumnMetaData)
        //            {
        //                list.Add(((ITextBucketColumnMetaData)metaData)
        //                    .TextColumn.Name);
        //                continue;
        //            }
        //            if (metaData is ITimeBucketColumnMetaData)
        //            {
        //                ITimeBucketColumnMetaData timeBucketMeta =
        //                    (ITimeBucketColumnMetaData)metaData;
        //                bucketings.Add(new TimeBucketing(
        //                    column.Name, timeBucketMeta.Title,
        //                        timeBucketMeta.TimeColumn.Name,
        //                            timeBucketMeta.TimePart));
        //                continue;
        //            }
        //            if (metaData is ICalculatedColumnMetaData &&
        //                metaData is ITextColumnMetaData)
        //            {
        //                ICalculatedColumnMetaData calculatedTextMetaData =
        //                    (ICalculatedColumnMetaData)metaData;
        //                IEnumerable<IColumn> referredColumns = CalcUtils.GetReferredColumnsRecursive(
        //                    column.Table, calculatedTextMetaData.Formula, column);
        //                foreach (IColumn referredColumn in referredColumns)
        //                {
        //                    list.Add(referredColumn.Name);
        //                }
        //            }
        //        }
        //    }
        //    parameters.BreakdownColumns =
        //        list.Count > 0 ? list.ToArray() : null;
        //    parameters.Bucketings =
        //        bucketings.Count > 0 ? bucketings.ToArray() : null;
        //}

        //// TODO: Is this TableUtils.IsUserDefinedColumn?
        //private static bool IsValidColumn(IColumn col)
        //{
        //    if (col == null)
        //    {
        //        return false;
        //    }
        //    return TableUtils.GetAuxMetaData(col) == null;
        //}

        //private static string[] GetDrillPath(IVisualizationState state)
        //{
        //    NodeCursor root = state.Hierarchy.GetCursor(state.RootNode, true);
        //    List<string> list = new List<string>();

        //    while (!root.IsRoot)
        //    {
        //        if (IsValidColumn(root.Column))
        //        {
        //            list.Add(root.Key);
        //        }
        //        root.MoveToParent();
        //    }

        //    if (list.Count > 0)
        //    {
        //        list.Reverse();

        //        return list.ToArray();
        //    }

        //    return null;
        //}

        //public static int GetNumericDepth(IVisualizationState state)
        //{
        //    VisibleDepth visibleDepth = state.VisibleDepth;
        //    if (visibleDepth.Mode == VisibleDepthMode.All)
        //    {
        //        return -1;
        //    }
        //    if (visibleDepth.Mode == VisibleDepthMode.Root)
        //    {
        //        return 0;
        //    }
        //    IBreakdownVariable breakdown = CommonVariables.GetBreakdown(state);
        //    for (int i = 0; i < breakdown.ColumnCount; i++)
        //    {
        //        if (breakdown.GetColumn(i) == visibleDepth.Column)
        //        {
        //            return i + 1;
        //        }
        //    }
        //    return -1;
        //}

        public static OnDemandParameters GetPreviewParameters()
        {
            return new OnDemandPreviewParameters();
        }

        //private static OnDemandAggregate[] GetAggregates(
        //    IVisualizationState state)
        //{
        //    Hierarchy hierarchy = state.Hierarchy;
        //    HashSet<OnDemandAggregate> aggregates =
        //        new HashSet<OnDemandAggregate>();

        //    for (int i = 0; i < hierarchy.AggregateCount; i++)
        //    {
        //        AddFunctionAggregates(hierarchy.Table,
        //            hierarchy.GetAggregate(i).Function, aggregates);
        //    }
        //    if (aggregates.Count > 0)
        //    {
        //        return aggregates.ToArray();
        //    }
        //    return null;
        //}

        //public static OnDemandAggregate[] GetAggregates(ITable table, Function f)
        //{
        //    HashSet<OnDemandAggregate> aggregates =
        //        new HashSet<OnDemandAggregate>();
        //    AddFunctionAggregates(table, f, aggregates);
        //    return aggregates.ToArray();
        //}

        //private static void AddFunctionAggregates(ITable table, Function f,
        //    HashSet<OnDemandAggregate> aggregates)
        //{
        //    if (f is ExternalAggregateFunction)
        //    {
        //        ExternalAggregateFunction external =
        //            (ExternalAggregateFunction)f;
        //        aggregates.Add(new OnDemandAggregate(FunctionType.External,
        //            GetColumns(table, new[] { external.ColumnName }), f.TextKey));
        //    }
        //    if (f.Columns.Length > 0 &&
        //        Enum.IsDefined(typeof(FunctionType), f.GetType().Name))
        //    {
        //        if (RefersToCalculatedColumn(table, f.Columns))
        //        {
        //            for (int i = 0; i < f.Columns.Length; i++)
        //            {
        //                INumericColumn col =
        //                    GetNumericColumn(table, f.Columns[i]);
        //                if (IsValidColumn(col)) continue;
        //                foreach (OnDemandAggregate agg in GetAuxAggregates(col))
        //                {
        //                    aggregates.Add(agg);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            aggregates.Add(new OnDemandAggregate(
        //                (FunctionType)Enum.Parse(
        //                    typeof(FunctionType), f.GetType().Name),
        //                GetColumns(table, f.Columns), f.TextKey));
        //        }
        //    }
        //    if (f.Functions.Length > 0)
        //    {
        //        foreach (Function child in f.Functions)
        //        {
        //            if (child != f)
        //            {
        //                AddFunctionAggregates(table, child, aggregates);
        //            }
        //        }
        //    }
        //}

        //private static bool RefersToCalculatedColumn(
        //    ITable table, string[] columns)
        //{
        //    for (int i = 0; i < columns.Length; i++)
        //    {
        //        IColumn column = table.GetColumn(columns[i]);
        //        if (!IsValidColumn(column))
        //        {
        //            return true;
        //        }
        //    }
        //    return false;
        //}

        //private static IEnumerable<INumericColumn> GetColumns(
        //    ITable table, string[] columns)
        //{
        //    List<INumericColumn> list = new List<INumericColumn>();
        //    for (int i = 0; i < columns.Length; i++)
        //    {
        //        INumericColumn column = GetNumericColumn(table, columns[i]);
        //        // TODO: Correct to bail out if column not found?
        //        if (column == null || !IsValidColumn(column))
        //        {
        //            return null;
        //        }
        //        list.Add(column);
        //    }
        //    return list;
        //}

        private static INumericColumn GetNumericColumn(
            ITable table, string name)
        {
            for (int i = 0; i < table.ColumnCount; i++)
            {
                IColumn column = table.GetColumn(i);
                if (column.Name.Equals(name))
                {
                    return column as INumericColumn;
                }
            }
            return null;
        }

        public static T[] CombineArrays<T>(T[] a, T[] b)
        {
            HashSet<T> set = new HashSet<T>();
            if (a == null) return b;
            if (b == null) return a;
            set.UnionWith(a);
            set.UnionWith(b);
            return set.ToArray();
        }

        public static OnDemandAggregate[] CombineAggregateArrays(
            OnDemandAggregate[] a, OnDemandAggregate[] b)
        {
            HashSet<OnDemandAggregate> set =
                new HashSet<OnDemandAggregate>();

            if (a == null) return b;
            if (b == null) return a;

            set.UnionWith(a);
            set.UnionWith(b);

            return set.ToArray();
        }

        public static string[] CombineColumnArrays(string[] a, string[] b)
        {
            if (a == null) return b;
            if (b == null) return a;

            HashSet<string> set = new HashSet<string>();

            set.UnionWith(a);
            set.UnionWith(b);

            return set.ToArray();
        }

        public static bool IsColumnAggregate(string columnName, OnDemandParameters onDemandParameters)
        {
            if (onDemandParameters != null &&
                onDemandParameters.Aggregates != null)
            {
                foreach (OnDemandAggregate onDemandAggregate
                    in onDemandParameters.Aggregates)
                {
                    if (onDemandAggregate.Alias == columnName)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
