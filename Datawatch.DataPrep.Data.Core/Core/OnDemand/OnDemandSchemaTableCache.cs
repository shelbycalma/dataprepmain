﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.OnDemand
{
    public class OnDemandSchemaTableCache
    {
        private Dictionary<OnDemandSchemaTableCacheKey, StandaloneTable> cache =
            new Dictionary<OnDemandSchemaTableCacheKey, StandaloneTable>();

        public StandaloneTable this[OnDemandSchemaTableCacheKey key]
        {
            get 
            { 
                if(!cache.ContainsKey(key)) return null;
                StandaloneTable table = cache[key];
                StandaloneTable schema = new StandaloneTable();
                if (table is ParameterTable)
                {
                    schema = new ParameterTable(
                        ((ParameterTable)table).Parameters);
                }
                 
                schema.BeginUpdate();
                try
                {
                    for (int i = 0; i < table.ColumnCount; i++)
                    {
                        IColumn col = table.GetColumn(i);
                        Column schemaColumn = new TextColumn(col.Name);
                        if (col is NumericColumn)
                        {
                            schemaColumn = new NumericColumn(col.Name);
                            
                        }
                        else if (col is TimeColumn)
                        {
                            schemaColumn = new TimeColumn(col.Name);
                        }
                        
                        schemaColumn.MetaData = col.MetaData;

                        if (schemaColumn.MetaData is IMutableColumnMetaData)
                        {
                            ((IMutableColumnMetaData)
                                schemaColumn.MetaData).Hidden = false;
                        }

                        schema.AddColumn(schemaColumn);
                    }
                }
                finally
                {
                    schema.EndUpdate();
                }
                return schema;
            }
            set { cache[key] = value; }
        }
    }
}
