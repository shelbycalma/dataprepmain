﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.OnDemand
{
    internal class OnDemandPreviewParameters : OnDemandParameters,
        IOnDemandPreviewParameters
    {
    }
}
