﻿namespace Datawatch.DataPrep.Data.Core.OnDemand
{
    public class OnDemandSchemaTableCacheKey
    {
        private readonly string schemaQuery;
        private readonly string connectionString;
        private readonly string[] selectedColumns;

        public OnDemandSchemaTableCacheKey(string schemaQuery,
            string connectionString, string[] selectedColumns)
        {
            this.schemaQuery = schemaQuery;
            this.connectionString = connectionString;
            this.selectedColumns = selectedColumns;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(OnDemandSchemaTableCacheKey))
            {
                return false;
            }
            return Equals((OnDemandSchemaTableCacheKey)obj);
        }

        private bool Equals(OnDemandSchemaTableCacheKey other)
        {
            return
                Equals(other.schemaQuery, schemaQuery) &&
                Equals(other.connectionString, connectionString) &&
                Utils.IsEqual<string>(other.selectedColumns, selectedColumns);
        }

        public override int GetHashCode()
        {
            int result = 17;
            if (schemaQuery != null)
            {
                result = result * 23 + schemaQuery.GetHashCode();
            }
            if (connectionString != null)
            {
                result = result * 23 + connectionString.GetHashCode();
            }
            if (selectedColumns != null)
            {
                foreach (string column in selectedColumns)
                {
                    result ^= column.GetHashCode();
                }
            }
            return result;
        }
    }
}
