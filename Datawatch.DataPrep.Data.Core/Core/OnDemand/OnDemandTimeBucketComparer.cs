﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.OnDemand
{
    public class OnDemandTimeBucketComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            int ix = Parse(x);
            int iy = Parse(y);

            return ix.CompareTo(iy);
        }

        private static int Parse(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return int.MinValue;
            }
            int i;
            if (int.TryParse(s, out i))
            {
                return i;
            }
            return int.MinValue;
        }
    }
}
