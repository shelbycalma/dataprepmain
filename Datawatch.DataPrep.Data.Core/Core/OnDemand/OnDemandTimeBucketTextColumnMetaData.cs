﻿using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.OnDemand
{
    public class OnDemandTimeBucketTextColumnMetaData :
        TimeBucketTextColumnMetaData, IOnDemandMetaData
    {
        public OnDemandTimeBucketTextColumnMetaData(ITextColumn column,
            ITimeColumn timeColumn, TimePart timePart, string title) :
            base(column, timeColumn, timePart)
        {
            Title = title;
            Comparer = new OnDemandTimeBucketComparer();
        }
    }
}
