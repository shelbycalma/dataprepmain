﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace Datawatch.DataPrep.Data.Core
{
	public class RegistryUtils
	{
		private static bool CreateMonarchRegistryKey()
		{
			try
			{
				RegistryKey key = Registry.CurrentUser.OpenSubKey("Software", true);

				key.CreateSubKey("Datawatch");

				key = key.OpenSubKey("Datawatch", true);

				key.CreateSubKey("Monarch");

				return true;
			}
			catch (Exception e)
			{
				return false;
			}
		}

		private static RegistryKey GetMonarchRegistryKey(string subkeyName, bool writeAccess = true)
		{
			var applicationPath = "Software\\Datawatch\\Monarch";
			var applicationKey = Registry.CurrentUser.OpenSubKey(applicationPath, writeAccess);
			if (applicationKey != null)
			{
				if (!String.IsNullOrEmpty(subkeyName))
				{
					var subkey = applicationKey.OpenSubKey(subkeyName, writeAccess);

					if (subkey != null)
						return subkey;

					subkey = applicationKey.CreateSubKey(subkeyName);
					return subkey;
				}
				else
				{
					return applicationKey;
				}
			}
			else //create the registry key if it doesnt exist
			{
				if (CreateMonarchRegistryKey())
				{
					applicationKey = Registry.CurrentUser.OpenSubKey(applicationPath, writeAccess);
					if (!String.IsNullOrEmpty(subkeyName))
					{
						var subkey = applicationKey.OpenSubKey(subkeyName, writeAccess);

						if (subkey == null)
						{
							subkey = applicationKey.CreateSubKey(subkeyName);
						}

						return subkey;
					}
					else
					{
						return applicationKey;
					}
				}
			}

			return null;
		}
		public static bool GetMonarchRegistryValue(string subkeyName, string settingName, out int value)
		{
			value = 0;
			try
			{
				var key = GetMonarchRegistryKey(subkeyName);
				if (key != null)
				{
					var settingValue = key.GetValue(settingName);
					if (!(settingValue is int))
						return false;
					value = (int)settingValue;
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				return false;
			}
		}
	}
}
