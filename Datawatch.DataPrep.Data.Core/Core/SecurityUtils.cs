﻿using System;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Text;

namespace Datawatch.DataPrep.Data.Core
{
    public abstract class SecurityUtils
    {
        private static byte[] entropy = System.Text.Encoding.Unicode.GetBytes("Salt Is Not A Password");

        public static string EncryptString(System.Security.SecureString input)
        {
            return EncryptString(input, false);
        }

        public static SecureString DecryptString(string encryptedData)
        {
            return DecryptString(encryptedData, false);
        }

        public static string EncryptString(System.Security.SecureString input, bool localMachineScope)
        {
            DataProtectionScope scope = localMachineScope ?
                DataProtectionScope.LocalMachine : DataProtectionScope.CurrentUser;

            byte[] encryptedData = System.Security.Cryptography.ProtectedData.Protect(
                System.Text.Encoding.Unicode.GetBytes(ToInsecureString(input)),
                entropy, scope);
            return Convert.ToBase64String(encryptedData);
        }

        public static SecureString DecryptString(string encryptedData, bool localMachineScope)
        {
            DataProtectionScope scope = localMachineScope ?
                DataProtectionScope.LocalMachine : DataProtectionScope.CurrentUser;

            if (encryptedData == null)
            {
                return new SecureString();
            }

            try
            {
                byte[] decryptedData = System.Security.Cryptography.ProtectedData.Unprotect(
                    Convert.FromBase64String(encryptedData),
                    entropy, scope);
                return ToSecureString(System.Text.Encoding.Unicode.GetString(decryptedData));
            }
            catch
            {
                return new SecureString();
            }
        }

        public static SecureString ToSecureString(string input)
        {
            SecureString secure = new SecureString();
            foreach (char c in input)
            {
                secure.AppendChar(c);
            }
            secure.MakeReadOnly();
            return secure;
        }

        public static string ToInsecureString(SecureString input)
        {
            string returnValue = string.Empty;
            IntPtr ptr = System.Runtime.InteropServices.Marshal.SecureStringToBSTR(input);
            try
            {
                returnValue = System.Runtime.InteropServices.Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ZeroFreeBSTR(ptr);
            }
            return returnValue;
        }

        public static String SALTEncrypt(string plainText, string password, int keySize, int blockSize, int saltIterations)
        {

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 4, 1, 20, 1, 23, 1, 20, 8 };

            var key = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(password), saltBytes, saltIterations);
            byte[] Key = key.GetBytes(keySize / 8);
            byte[] IV = key.GetBytes(blockSize / 8);


            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = AesCryptoServiceProvider.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return Convert.ToBase64String(encrypted);

        }

        public static string SALTDecrypt(String cipherPlainText, string password, int keySize, int blockSize, int saltIterations)
        {

            // Set your salt here, change it to meet your flavor:
            // The salt bytes must be at least 8 bytes.
            byte[] saltBytes = new byte[] { 4, 1, 20, 1, 23, 1, 20, 8 };

            var key = new Rfc2898DeriveBytes(Encoding.UTF8.GetBytes(password), saltBytes, saltIterations);
            byte[] Key = key.GetBytes(keySize / 8);
            byte[] IV = key.GetBytes(blockSize / 8);

            byte[] cipherText = Convert.FromBase64String(cipherPlainText);

            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = AesCryptoServiceProvider.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }
    }
}
