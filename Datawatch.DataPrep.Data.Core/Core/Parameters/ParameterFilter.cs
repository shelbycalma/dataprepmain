﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    /// <summary>
    /// Use this class to to filter out those rows that the parameters allow to be 
    /// added into the table. This class will also work if one parameter have several
    /// values (a MultiValueContainer will be created). 
    /// 
    /// DESIGNER EXAMPLE:
    /// So if a parameter have been defined in the following way:
    ///    Name = Company
    ///    Value = 'Ericsson'
    /// Then only the row should be added into the table if the column "Company" have 
    /// value "Ericsson".
    /// 
    /// USAGE OF THIS CLASS: 
    /// 1. Create the ParameterFilter with the create() method. That method will 
    /// 2. Before adding a new incoming row into the table check if the parameters 
    ///    allow it by calling contains(Dictionary(String, Object)) or by calling 
    ///    contains(Object[]) if you only have a array of parsed Objects, being String,
    ///    Date or double.
    ///  </summary>
    public class ParameterFilter
    {
        private readonly ValueContainer[] valueContainers;
    
        private ParameterFilter(ITable table, IEnumerable<ParameterValue> parameters) 
        {
            ParameterValueCollection parameterLookup =
                    new ParameterValueCollection(parameters);
            List<ValueContainer> containers = new List<ValueContainer>();
            for (int i = 0; i < table.ColumnCount; i++) 
            {
                String columnName = table.GetColumn(i).Name;
                ParameterValue parameterValue = parameterLookup[columnName];
                if (parameterValue != null && parameterValue.TypedValue != null) 
                {
                    // There is a valid parameter that have the same as this column
                    // so then this parameter will be used to filter out valid rows
                    // that should be added into the table.
                    TypedParameterValue typedValue =
                            parameterValue.TypedValue;
                    if (typedValue is ArrayParameterValue)
                    {
                        containers.Add(new MultiValueContainer(typedValue, columnName, i));
                    } 
                    else 
                    {
                        containers.Add(new SingleValueContainer(typedValue, columnName, i));
                    }
                }
            }
            valueContainers = containers.ToArray();
        }

        private ParameterFilter(IEnumerable<ColumnDefinition> columns,
            IEnumerable<ParameterValue> parameters)
        {
            // TODO: should refactor overloaded constructors to re-use code.
            ParameterValueCollection parameterLookup =
                    new ParameterValueCollection(parameters);
            List<ValueContainer> containers = new List<ValueContainer>();
            int i = 0;
            foreach (ColumnDefinition columnDefinition in columns)
            {

                String columnName = columnDefinition.Name;
                string filterText = columnDefinition.FilterText;
                ParameterValue parameterValue = parameterLookup[filterText];
                if (parameterValue != null && parameterValue.TypedValue != null)
                {
                    // There is a valid parameter that have the same as this column
                    // so then this parameter will be used to filter out valid rows
                    // that should be added into the table.
                    TypedParameterValue typedValue =
                            parameterValue.TypedValue;
                    if (typedValue is ArrayParameterValue)
                    {
                        containers.Add(new MultiValueContainer(typedValue, columnName, i));
                    }
                    else
                    {
                        containers.Add(new SingleValueContainer(typedValue, columnName, i));
                    }
                }

                i++;
            }
            valueContainers = containers.ToArray();
        }

        /// <summary>
        /// Check if parameters allow the row/dictionary to be added to the table or 
        /// not.
        /// </summary>
        /// <param name="dictionary"> key = columnName, value = rowValue</param>
        /// <returns> Return false if there is a parameter with same name as the 
        /// columnName but don't have the rowValue defined in the parameter. 
        /// Otherwise true (row will be allowed by the parameters to be added).
        /// </returns>
        public bool Contains(Dictionary<String, Object> dictionary) 
        {
            foreach (ValueContainer container in valueContainers) 
            {
                String columnName = container.ColumnName;
                object value = dictionary.ContainsKey(columnName)
                    ? dictionary[columnName]
                    : string.Empty;
                if (!container.ContainsValue(value))
                {
                    return false;
                }
            }
            return true;
        }
    
        /// <summary>
        /// Check if parameters allow the row to be added to the table or not.
        /// </summary>
        /// <param name="values"> The values for one row should match the order of the 
        /// columns in a table. The values should be like String, Date, ...</param>
        /// <returns> Return false if there is a parameter with same name as the 
        /// columnName but don't have the rowValue defined in the parameter. 
        /// Otherwise true (row will be allowed by the parameters to be added).
        /// </returns>
        public bool Contains(Object[] values) 
        {
            foreach (ValueContainer container in valueContainers) 
            {
                int index = container.Index;
                if (!container.ContainsValue(values[index]))
                {
                    return false;
                }
            }
            return true;
        }
    
        /// <summary>
        /// Creates a filter that can be used to to check whether the parameters 
        /// allow a row to be added or not. This check is done by calling 
        /// Contains(Dictionary (String, Object)) or Contains(Object[]) on the
        /// created filter.
        /// </summary>
        /// <param name="table"> The table that the filter will be applied on.
        /// </param>
        /// <param name="parameters"> The parameters that are defined.</param>
        /// <returns> Returns a filter to be used to filter out rows. If null is 
        /// returned then there is no parameters to check against. </returns>
        public static ParameterFilter CreateFilter(ITable table,
                IEnumerable<ParameterValue> parameters) 
        {
            if (table == null) 
            {
                throw Exceptions.ArgumentNull("table");
            }
            if (parameters == null) 
            {
                return null;
            }
        
            ParameterFilter filter = new ParameterFilter(table, parameters);
            if (filter.valueContainers.Length > 0) 
            {
                // We only return a filter when there is something to check against.
                return filter;
            }
            return null;
        }


        /// <summary>
        /// Creates a filter that can be used to to check whether the parameters 
        /// allow a row to be added or not. This check is done by calling 
        /// Contains(Dictionary (String, Object)) or Contains(Object[]) on the
        /// created filter.
        /// </summary>
        /// <param name="columns"> Array of ColumnDefinition to check against.
        /// </param>
        /// <param name="parameters"> The parameters that are defined.</param>
        /// <returns> Returns a filter to be used to filter out rows. If null is 
        /// returned then there is no parameters to check against. </returns>
        public static ParameterFilter CreateFilter(
            IEnumerable<ColumnDefinition> columns,
            IEnumerable<ParameterValue> parameters)
        {
            if (columns == null)
            {
                throw Exceptions.ArgumentNull("columns");
            }
            if (parameters == null)
            {
                return null;
            }

            ParameterFilter filter = new ParameterFilter(columns, parameters);
            if (filter.valueContainers.Length > 0)
            {
                // We only return a filter when there is something to check against.
                return filter;
            }
            return null;
        }

        private abstract class ValueContainer 
        {
            private readonly String columnName;
            private readonly int index;
        
            public ValueContainer(String columnName, int index) 
            {
                this.columnName = columnName;
                this.index = index;
            }
        
            public abstract bool ContainsValue(Object obj);
        
            protected static Object SingleValue(TypedParameterValue typedValue) 
            {
                if (typedValue is StringParameterValue) 
                {
                    StringParameterValue stringValue =
                            (StringParameterValue) typedValue;
                    return stringValue.Value;
                }
                if (typedValue is DateTimeParameterValue) 
                {
                    DateTimeParameterValue dateValue =
                            (DateTimeParameterValue) typedValue;
                    return dateValue.Value;
                }
                if (typedValue is NumberParameterValue) 
                {
                    return ((NumberParameterValue) typedValue).Value;
                }
                throw new Exception(string.Format("{0}{1}", Resources.ExTypeParameterValueNotSupported, typedValue));
            }
        
            public String ColumnName 
            {
                get { return columnName; }
            }
        
            public int Index 
            {
                get { return index; }
            }
        }
        
        private class SingleValueContainer : ValueContainer 
        {
        
            private readonly Object value;
        
            public SingleValueContainer(TypedParameterValue typedValue,
                    String columnName, int index)
                : base(columnName, index)
            {
                value = SingleValue(typedValue);
            }
        
            
            public override bool ContainsValue(Object obj) 
            {
                if (value == obj) return true;
                return value != null && value.Equals(obj);
            }
        
        }
    
        private class MultiValueContainer : ValueContainer 
        {
        
            private readonly HashSet<Object> values;
        
            public MultiValueContainer(TypedParameterValue typedValue, 
                    String columnName, int index) 
                : base(columnName, index) 
            {
                values = new HashSet<Object>();
                PopulateValues((ArrayParameterValue) typedValue);
            }
        
            public override bool ContainsValue(Object obj) 
            {
                return values.Contains(obj);
            }
        
            private void PopulateValues(ArrayParameterValue arrayValue) 
            {
                if (arrayValue.Values == null) 
                    return;
            
                List<TypedParameterValue> typedValues = arrayValue.Values;
                foreach (TypedParameterValue typedValue in typedValues) 
                {
                    if (typedValue is ArrayParameterValue) 
                    {
                        PopulateValues((ArrayParameterValue) typedValue);
                    } 
                    else 
                    {
                        Object singleValue = SingleValue(typedValue);
                        values.Add(singleValue);
                    }
                }
            }
        
        }
    }
}
