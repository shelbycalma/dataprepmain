﻿using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    public class ParameterUtils
    {
        public static string FormatParameter(object value, object parameterValues)
        {
            if (value == null) return null;

            ParameterValue parameterValue = value as ParameterValue;

            if (parameterValue != null)
            {
                return parameterValue.Name;
            }

            string appliedParameterName = value as string;

            if (string.IsNullOrEmpty(appliedParameterName))
            {
                return appliedParameterName;
            }

            ParameterValueCollection parameterCollection =
                new ParameterValueCollection();

            ParameterValue[] parameters = parameterValues as ParameterValue[];

            if (parameters == null)
            {
                return appliedParameterName.Replace("{", "").Replace("}", "");
            }
            parameterCollection.AddRange(parameters);

            if (parameterCollection.ContainsKey(appliedParameterName))
            {
                return appliedParameterName;
            }
            return appliedParameterName;
        }
    }
}