﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    public class ParameterValueCollection : ObservableLookupCollection<ParameterValue>
    {
        public ParameterValueCollection()
        {
        }
        
        public ParameterValueCollection(IEnumerable<ParameterValue> values)
        {
            AddRange(values);
        }

        public new ParameterValue this[string name]
        {
            get
            {
                if (string.IsNullOrEmpty(name))
                {
                    return null;
                }
                return base[name];
            }
            set { Add(name, value.TypedValue); }
        }

        public void Add(string name, TypedParameterValue value)
        {
            // do not allow null or empty keys
            if (string.IsNullOrEmpty(name))
            {
                return;
            }
            // if a parameter value with the same name already exists,
            // remove it and overwrite, do not allow duplicate names
            ParameterValue parameter = base[name];
            if (parameter != null)
            {
                Remove(parameter);
            }
            Add(new ParameterValue
            {
                Name = name,
                TypedValue = value
            });
        }

        public void Add(string name, string value)
        {
            Add(name, new StringParameterValue {Value = value});
        }

        public void Add(string name, double value)
        {
            Add(name, new NumberParameterValue {Value = value});
        }

        public void Add(string name, DateTime value)
        {
            Add(name, new DateTimeParameterValue {Value = value});
        }

        public void AddRange(IEnumerable<ParameterValue> parameterValues)
        {
            if (parameterValues == null)
            {
                return;
            }
            foreach (ParameterValue value in parameterValues)
            {
                Add(value.Name, value.TypedValue);
            }
        }
        
        public override bool Equals(object obj)
        {
            if (obj == this)
            {
                return true;
            }
            IEnumerable<ParameterValue> values =
                obj as IEnumerable<ParameterValue>;
            if (values == null)
            {
                return false;
            }
            return Utils.IsEqual(this.ToArray(), values.ToArray());
        }

        public override int GetHashCode()
        {
            int hashCode = 0;
            foreach (ParameterValue parameterValue in this)
            {
                hashCode ^= parameterValue.GetHashCode();
            }
            return hashCode;
        }

        public override string ToString()
        {
            List<string> list = new List<string>();
            foreach (ParameterValue entry in this)
            {
                list.Add(entry.Name + "=" + entry.TypedValue);
            }
            return "{" + string.Join(", ", list) + "}";
        }

        /// <summary>
        /// Tests if a TypedParameterValue is considered null.
        /// StringParameters thats null or empty are null.
        /// NumberParameters are never null.
        /// ArrayParameters are null if empty (Count == 0).
        /// </summary>
        /// <param name="parameterValue">Typed value to test.</param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(ParameterValue parameterValue)
        {
            if (parameterValue == null)
            {
                return true;
            }
            if (parameterValue.TypedValue is StringParameterValue)
            {
                return string.IsNullOrEmpty(
                    ((StringParameterValue)parameterValue.TypedValue).Value);
            }
            if (parameterValue.TypedValue is NumberParameterValue)
            {
                return false; // double cant be null, what about NaN?
            }
            if (parameterValue.TypedValue is DateTimeParameterValue)
            {
                return false;
            }
            if (parameterValue.TypedValue is ArrayParameterValue)
            {
                ArrayParameterValue arrayParameterValue =
                    (ArrayParameterValue)parameterValue.TypedValue;
                return arrayParameterValue.Values == null
                    || arrayParameterValue.Values.Count == 0;
            }
            return true;
        }
    }
}
