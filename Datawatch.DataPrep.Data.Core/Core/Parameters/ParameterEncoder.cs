﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Parameters
{
    public class ParameterEncoder : IParameterEncoder
    {
        // TODO ObservableParameterEncoder has been created as a
        // stateful viewmodel for this class. This class should
        // be made less stateful and less mutable.

        // TODO create factory for creating encoders?
        // wrapping properties with function parameters

        public string DefaultArraySeparator { get; set; }
        public string DefaultDateTimeFormat { get; set; }
        public string DefaultNumberFormat { get; set; }
        public IEnumerable<ParameterValue> Parameters { get; set; }

        // TODO replace format provider with functions like in java,
        // making the classes more similiar
        protected ParameterFormatProvider FormatProvider { get; set; }

        // TODO rename this to FormatString
        public string SourceString { get; set; }

        /// <summary>
        /// In the past, values were either always or never enclosed with single quotes.
        /// This was decided at data connector level.
        /// Database connector exposes a boolean setting to the user.
        /// OneTick connector has extra quoting logic.
        /// Neither should be required.
        /// 
        /// The logic has now been expanded and the property made Obsolete.
        /// For text, enclose with single quotes if and only if this property is true.
        /// For numbers, never enclose with single quotes.
        /// </summary>
        [Obsolete("Single quotes logic has been expanded, check property documentation.")]
        public bool EnforceSingleQuoteEnclosure
        {
            get { return FormatProvider.EnforceSingleQuoteEnclosure; }
            set { FormatProvider.EnforceSingleQuoteEnclosure = value; }
        }

        public bool EnforceSingleQuoteHandling
        {
            get { return FormatProvider.EnforceSingleQuoteHandling; }
            set { FormatProvider.EnforceSingleQuoteHandling = value; }
        }

        public string NullOrEmptyString
        {
            get { return FormatProvider.NullOrEmptyString; }
            set { FormatProvider.NullOrEmptyString = value; }
        }

        private readonly Dictionary<string, string> arraySeparators =
            new Dictionary<string, string>();

        /// <summary>
        /// If a ArrayParameterValue is a to be formatted, the values needs to
        /// be separated. A IValueSeparatorProvider with the same name as the
        /// parameter provides the separator. If not found, the
        /// DefaultArraySeparator is used.
        /// </summary>
        // TODO i would like this to be IEnumerable
        public IValueSeparatorProvider[] ArrayValueSeparatorProviders
        {
            set
            {
                arraySeparators.Clear();
                if (value == null) return;
                foreach (IValueSeparatorProvider provider in value)
                {
                    arraySeparators[provider.Name] = provider.ValueSeparator;
                }
            }
        }

        public ParameterEncoder()
        {
            DefaultDateTimeFormat = "yyyy-MM-ddTHH:mm:ssZ"; // iso8601
            DefaultNumberFormat = "g";
            DefaultArraySeparator = ",";
            FormatProvider = new ParameterFormatProvider();
        }

        public virtual string Encoded()
        {
            if (Parameters == null || string.IsNullOrEmpty(SourceString))
            {
                return SourceString;
            }

            // parameters needs to be handled in descending name length
            // otherwise $Trader would break the parameter 
            // named $TraderID. See bug #7630 for details. 
            SortedDictionary<string, TypedParameterValue> parameters =
                new SortedDictionary<string, TypedParameterValue>(
                    new NameLengthComparer());
            foreach (ParameterValue parameterValue in Parameters)
            {
                try
                {
                    // if the parameter doesnt have a value,
                    // replace it with a string parameter instead,
                    // with an empty string as value
                    parameters.Add(parameterValue.Name,
                        parameterValue.TypedValue
                        ?? new StringParameterValue());
                }
                catch (ArgumentException)
                {
                    // ignore duplicate keys (although it shouldnt be possible)
                }
            }
            // if no parameters are found, abort formatting
            if (parameters.Count == 0)
            {
                return SourceString;
            }

            // build format string
            string formatString = CoerceOldFormatKeys(SourceString, parameters);
            formatString = CreateParameterEntries(formatString, parameters);
            return formatString;
        }

        private static string CoerceOldFormatKeys(string formatString, 
            SortedDictionary<string, TypedParameterValue> parameters)
        {
            foreach (string key in parameters.Keys)
            {
                string oldKey = "$" + key;
                if (!formatString.Contains(oldKey))
                {
                    continue; // parameter not used in source string
                }
                // coerce old style
                while (formatString.Contains(oldKey))
                {
                    formatString = formatString.Replace(oldKey, "{" + key + "}");
                }
            }
            return formatString;
        }

        private string CreateParameterEntries(string formatString, 
            SortedDictionary<string, TypedParameterValue> parameters)
        {
            foreach (string key in parameters.Keys)
            {
                // Regex to match both {key} and {key:format}
                Regex regex = new Regex(CreateParameterSearchPattern(key));

                // if the parameter is not used in the formatString continue
                if (!regex.IsMatch(formatString)) continue;

                TypedParameterValue value = parameters[key];

                int indexOffset = 0;
                // Replace all matches with an encoding entry
                foreach (Match match in regex.Matches(formatString))
                {
                    string format = null;
                    // If :format exists Groups.Count will be == 2.
                    if (match.Groups.Count == 2 && match.Groups[1].Value.Length > 1)
                    {
                        format = match.Groups[1].Value.Substring(1);
                    }
                    string entry = CreateEncodingEntry(
                        value, key, format);
                    formatString = formatString.Remove(
                        match.Index + indexOffset, match.Length);
                    formatString = formatString.Insert(
                        match.Index + indexOffset, entry);
                    indexOffset += entry.Length - match.Length;
                }
  }
            return formatString;
        }

        public static string RenameParameters(string formatString,
            Dictionary<string, string> renames)
        {
            foreach (KeyValuePair<string, string> rename in renames)
            {
                string originalName = rename.Key;
                string newName = rename.Value;
                // Regex to match both {key} and {key:format}
                Regex regex = new Regex(CreateParameterSearchPattern(originalName));

                // if the parameter is not used in the formatString continue
                if (!regex.IsMatch(formatString)) continue;


                int indexOffset = 0;
                // Replace all matches with an encoding entry
                foreach (Match match in regex.Matches(formatString))
                {
                    string format = null;
                    // If :format exists Groups.Count will be == 2.
                    if (match.Groups.Count == 2 && match.Groups[1].Value.Length > 1)
                    {
                        format = match.Groups[1].Value.Substring(1);
                    }
                    string entry = format != null
                        ? "{" + newName + ":" + format + "}"
                        : "{" + newName + "}";
                    formatString = formatString.Remove(match.Index + indexOffset, match.Length);
                    formatString = formatString.Insert(match.Index + indexOffset, entry);
                    indexOffset += entry.Length - match.Length;
                }
            }
            return formatString;
        }

        private static string CreateParameterSearchPattern(string key)
        {
            return @"\{" + Regex.Escape(key) + @"(:[^\}]*)?\}";
        }

        private void LogEncodingError(string message)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat(
                Properties.Resources.LogUnableToEncodeParameter, message);
            builder.AppendFormat(
                Properties.Resources.LogUsingFormatString, SourceString);

            // build list of actual values
            builder.AppendFormat(Properties.Resources.LogUsingValues, Parameters.Count());
            foreach (ParameterValue pv in Parameters)
            {
                builder.AppendFormat("\n\t\t{0}={1}", pv.Name, pv.TypedValue);
            }

            Log.Error("{0}", builder);
        }

        private string CreateEncodingEntry(TypedParameterValue parameter,
            string name,string format)
        {
            if (parameter is StringParameterValue)
            {
                return string.Format(FormatProvider, "{0}", parameter);
            }
            if (string.IsNullOrEmpty(format))
            {
                format = GetDefaultFormat(name, parameter);
            }
            Debug.Assert(!string.IsNullOrEmpty(format));
            string formatString = "{0:" + format + "}";
            return string.Format(FormatProvider, formatString, parameter);
        }

        private string GetDefaultFormat(string name, TypedParameterValue arg)
        {
            if (arg is DateTimeParameterValue)
                return DefaultDateTimeFormat;
            if (arg is NumberParameterValue)
                return DefaultNumberFormat;
            if (arg is ArrayParameterValue)
            {
                return arraySeparators.ContainsKey(name)
                    ? arraySeparators[name] : DefaultArraySeparator;
            }
            return null; // strings dont have format strings
        }

        private class NameLengthComparer : IComparer<string>
        {
            public int Compare(string x, string y)
            {
                return y.Length == x.Length
                    ? x.CompareTo(y)
                    : y.Length - x.Length;
            }
        }

        public override string ToString()
        {
            return SourceString;
        }

        public string EncodedValue(ParameterValue parameterValue)
        {
            if (parameterValue == null) return "";
            return EncodedValue(parameterValue.TypedValue);
        }

        public string EncodedValue(TypedParameterValue typedParameterValue)
        {
            if (typedParameterValue == null) return "";
            return string.Format(FormatProvider, "{0}", typedParameterValue);
        }
    }
}
