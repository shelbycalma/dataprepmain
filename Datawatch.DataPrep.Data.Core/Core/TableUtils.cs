﻿using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core
{
    public class TableUtils
    {
        public static IErrorProducer FindErrorProducerInPipeline(ITable table)
        {
            if (table == null)
            {
                throw Exceptions.ArgumentNull("table");
            }
            return FindErrorProducer(table);
        }

        private static IErrorProducer FindErrorProducer(ITable table)
        {
            IErrorProducer errorProducer = table as IErrorProducer;
            if (errorProducer != null)
            {
                return errorProducer;
            }
            if (!(table is IDerivedTable))
            {
                return null;
            }

            IDerivedTable derived =
                (IDerivedTable)table;

            for (int i = 0; i < derived.SourceTableCount &&
                errorProducer == null; i++)
            {
                errorProducer = FindErrorProducer(derived.GetSourceTable(i));
            }
            return errorProducer;
        }
    }
}
