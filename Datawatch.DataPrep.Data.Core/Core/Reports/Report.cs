﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Reports
{
    public class Report
    {
        public string ReportID
        {
            get;
            set;
        }

        public string ReportName
        {
            get;
            set;
        }
    }
}
