﻿using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core
{
    public static class DateTimeFormatHelper
    {
        public static string[] DateTimeFormats = new string[]
        {
            FormatHelper.GetDefaultTimeFormat(),
            "dd/MMM/yyyy",
            "MMM/yyyy",
            "hh:mm tt",
            "hh:mm:ss tt",
            "hh:mm:ss.fff tt",
            "HH:mm",
            "HH:mm:ss",
            "HH:mm:ss.fff",
            "dd/MM/yyyy HH:mm:ss",
            "MM/dd/yyyy HH:mm:ss",
            "M/d/yyyy h:mm:ss tt",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'T'HH:mm:ss.fff'Z'",
            "yyyy-MM-dd'T'HH:mm:ssK",
            "yyyy-MM-dd'T'HH:mm:ss'Z'",
            "yyyyMMdd'T'HHmmss'Z'",
            "yyyy-MM-dd hh:mm:ss tt",
            DateParser.Posix
        };
    }
}
