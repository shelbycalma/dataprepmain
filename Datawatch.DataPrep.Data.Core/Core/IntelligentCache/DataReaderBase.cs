﻿using System;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.IntelligentCache
{
    public abstract class DataReaderBase: IDataReaderWithSchema
    {
        private ITable schemaTable;
        private DataTable adoSchemaTable;
        protected object[] values;
        
        protected DataTable AdoSchemaTable
        {
            set { adoSchemaTable = value; }
            get {
                if (adoSchemaTable == null && schemaTable != null) {
                    adoSchemaTable = CreateAdoSchemaFromITable(schemaTable);
                }
                return adoSchemaTable;
            }
        }

        public ITable SchemaTable
        {
            get { return schemaTable; }
            protected set {
                schemaTable = value;
                adoSchemaTable = null;
            }
        }

        protected static DataTable CreateAdoSchemaFromITable(ITable table)
        {
            // NOTE: The data prep code that loads data into LocalDB will use
            // this method to get the schema from the IDataReader (and not the
            // ITable SchemaTable property). It needs to conform to what it
            // calls TableSchemaType.Simple, so if you override this method to
            // provide your own schema (e.g. you're a database plugin), you
            // need to make sure the below properties are filled in.
            DataTable schemaTable = new DataTable();
            DataColumn[] schemaColumns = {
                new DataColumn("ColumnName", typeof(string)),
                new DataColumn("ColumnOrdinal", typeof(int)),
                new DataColumn("DbType", typeof(int)),
                new DataColumn("Size", typeof(int)),
                new DataColumn("IsLong", typeof(bool))
            };
            schemaTable.Columns.AddRange(schemaColumns);
            for (int i = 0; i < table.ColumnCount; i++) {
                IColumn column = table.GetColumn(i);
                DataRow schemaRow = schemaTable.NewRow();
                schemaRow["ColumnName"] = column.MetaData.Title;
                schemaRow["ColumnOrdinal"] = i;
                schemaRow["DbType"] = GetAdoSchemaColumnType(column);
                schemaRow["Size"] = 0;
                schemaRow["IsLong"] = column is ITextColumn;
                schemaTable.Rows.Add(schemaRow);
            }
            return schemaTable;
        }

        protected static DbType GetAdoSchemaColumnType(IColumn column)
        {
            if (column is ITextColumn) {
                return DbType.String;
            }
            if (column is INumericColumn) {
                return DbType.Double;
            }
            if (column is ITimeColumn) {
                return DbType.DateTime;
            }
            throw new NotSupportedException("Cannot represent column type " +
                column.GetType().Name + " in ADO schema.");
        }

        public int FieldCount
        {
            get
            {
                if (schemaTable == null) return -1;
                return schemaTable.ColumnCount;
            }
        }

        public object GetValue(int i)
        {            
            return values[i];
        }

        public int GetValues(object[] values)
        {
            int count = Math.Min(values.Length, this.values.Length);
            Array.Copy(this.values, values, count);
            return count;
        }

        public abstract bool Read();

        public abstract void Close();        

        /// <summary>
        /// Depth of the nested column (nesting is not supported, so depth is
        /// always 0).
        /// </summary>
        public virtual int Depth
        {
            get { return 0; }
        }

        public virtual string GetName(int i)
        {
            throw new NotImplementedException();
        }

        public virtual int GetOrdinal(string name)
        {
            throw new NotImplementedException();
        }

        public virtual DataTable GetSchemaTable()
        {
            return this.AdoSchemaTable;
        }

        public virtual bool IsClosed
        {
            get { throw new NotImplementedException(); }
        }

        public virtual bool NextResult()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This interface will not allow writing, so we will return -1 always.
        /// </summary>
        public virtual int RecordsAffected
        {
            get { throw new NotImplementedException(); }
        }

        public virtual void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        public virtual long GetBytes(int i, long fieldOffset, byte[] buffer,
            int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public virtual char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        public virtual long GetChars(int i, long fieldoffset, char[] buffer,
            int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        public virtual IDataReader GetData(int i)
        {
            throw new NotImplementedException();
        }

        public virtual string GetDataTypeName(int i)
        {
            throw new NotImplementedException();
        }

        public virtual DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public virtual decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        public virtual double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        public virtual Type GetFieldType(int i)
        {
            throw new NotImplementedException();
        }

        public virtual float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        public virtual Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        public virtual short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        public virtual int GetInt32(int i)
        {
            throw new NotImplementedException();
        }

        public virtual long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        public virtual string GetString(int i)
        {
            throw new NotImplementedException();
        }

        public virtual bool IsDBNull(int i)
        {
            throw new NotImplementedException();
        }

        public virtual object this[string name]
        {
            get { throw new NotImplementedException(); }
        }

        public virtual object this[int i]
        {
            get { throw new NotImplementedException(); }
        }
    }
}
