﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Query
{

    /// <summary>
    /// SQL dialect factory. Provides various vendor-specific SQL dialects.
    /// </summary>
    /// <seealso cref="Datawatch.DataPrep.Data.Framework.Query.SqlDialectFactoryBase" />
    public class SqlDialectFactory : SqlDialectFactoryBase
    {
        protected volatile static SqlDialect[] dialectsArray;

        #region Dialect names

        private const string AccessExcelDialectName = "Access/Excel";
        private const string MySqlDialectName = "MySQL";
        private const string OracleDialectName = "Oracle";
        private const string SqlServerDialectName = "SQL Server";
        private const string SybaseAnywhereDialectName = "Sybase IQ/ASA";
        private const string SybaseAseDialectName = "Sybase ASE";
        private const string NetezzaDialectName = "Netezza";
        private const string VerticaDialectName = "Vertica";
        private const string SQLiteDialectName = "SQLite";
        private const string HadoopHiveDialectName = "HadoopHive";
        private const string KxQDialectName = "KxQ";
        private const string DB2DialectName = "DB2";
        private const string PostgreSqlDialectName = "PostgreSQL";
        private const string ImpalaDialectName = "Impala";
        private const string RedshiftDialectName = "Redshift";
        private const string InformixDialectName = "Informix";
        private const string TeradataDialectName = "Teradata";
        private const string DBaseDialectName = "dBase";
        private const string SparkSqlDialectName = "SparkSql";

        #endregion Dialect names

        #region Date parts

        private static readonly IDictionary<TimePart, string> accessDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "'yyyy'" },
                { TimePart.Month, "'m'" },
                { TimePart.Day, "'d'" },
                { TimePart.Hour, "'h'" },
                { TimePart.Minute, "'n'" },
                { TimePart.Second, "'s'" },
                { TimePart.Quarter, "'q'" }
            };

        private static readonly IDictionary<TimePart, string> db2DateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "'yyyy'" },
                { TimePart.Month, "'mm'" },
                { TimePart.Day, "'dd'" },
                { TimePart.Hour, "'hh'" },
                { TimePart.Minute, "'mi'" },
                { TimePart.Second, "'ss'" },
                { TimePart.Quarter, "'q'" }
            };

        private static readonly IDictionary<TimePart, string> sqlServerDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "yyyy" },
                { TimePart.Month, "m" },
                { TimePart.Day, "d" },
                { TimePart.Hour, "hh" },
                { TimePart.Minute, "n" },
                { TimePart.Second, "s" },
                { TimePart.Quarter, "q" }
            };

        private static readonly IDictionary<TimePart, string> sybaseDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "yy" },
                { TimePart.Month, "mm" },
                { TimePart.Day, "dd" },
                { TimePart.Hour, "hh" },
                { TimePart.Minute, "mi" },
                { TimePart.Second, "s" },
                { TimePart.Quarter, "q" }
            };

        private static readonly IDictionary<TimePart, string> sqliteDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "%Y" },
                { TimePart.Month, "%m" },
                { TimePart.Day, "%d" },
                { TimePart.Hour, "%H" },
                { TimePart.Minute, "%M" },
                { TimePart.Second, "%S" }
            };

        private static readonly IDictionary<TimePart, string> hadoopHiveDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "YEAR" },
                { TimePart.Month, "MONTH" },
                { TimePart.Day, "DAYOFMONTH" },
                { TimePart.Hour, "HOUR" },
                { TimePart.Minute, "MINUTE" },
                { TimePart.Second, "SECOND" }
            };

        private static readonly IDictionary<TimePart, string> qDateParts =
            new Dictionary<TimePart, string>()
            {
                { TimePart.Year, "year" },
                { TimePart.Month, "mm" },
                { TimePart.Day, "dd" },
                { TimePart.Hour, "hh" },
                { TimePart.Minute, "mm$`time" },
                { TimePart.Second, "ss" }
            };

        private static readonly IDictionary<TimePart, string> impalaDateParts =
            hadoopHiveDateParts;

        private static readonly IDictionary<TimePart, string> sparkSqlDateParts =
            hadoopHiveDateParts;

        #endregion Date parts

        #region Query parts

        private static readonly IDictionary<QueryPart, string> db2QueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Top, "" },
                { QueryPart.Limit, " FETCH FIRST {0} ROWS ONLY " }
            };

        private static readonly IDictionary<QueryPart, string> oracleQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Top, "" },
                { QueryPart.Limit, "" },
                { QueryPart.PredicateLimit," ROWNUM <= {0} " }
            };

        private static readonly IDictionary<QueryPart, string> informixLimitQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Top, "FIRST {0}" },
                { QueryPart.Limit, "" }
            };

        private static readonly IDictionary<QueryPart, string> dBaseLimitQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Top, "" },
                { QueryPart.Limit, "" }
            };

        private static readonly IDictionary<QueryPart, string> qQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Select, "select " },
                { QueryPart.From, " from " },
                { QueryPart.GroupBy, " by " },
                { QueryPart.Where, " where " },
                { QueryPart.Distinct, "distinct " },
                { QueryPart.Top, " [{0}] " },
                { QueryPart.Limit, "i < {0} " },
                { QueryPart.Alias, "{1}: {0}" },
                { QueryPart.AllColumns, "" },
                { QueryPart.List, "`$({0})" },
                { QueryPart.ListItemSeparator, ";" },
                { QueryPart.EmptyList, "" },
                { QueryPart.LikeSymbol, " like " },
                { QueryPart.InSymbol, " in " },
                { QueryPart.NullSymbol,"\"\"" },
                { QueryPart.AndSymbol,"&" },
                { QueryPart.OrSymbol,"|" },
                { QueryPart.NotSymbol,"not " },
                { QueryPart.ValueQuoteLeft,"\"" },
                { QueryPart.ValueQuoteRight,"\"" },
                { QueryPart.ValueWrap,"{0}" },
                { QueryPart.ColumnWrap,"{1}" },
                { QueryPart.EqualsValueWrap,"`${0}" },
                { QueryPart.GroupColumnAlias,"{1}: {0}" },
                { QueryPart.PredicateSeparator,"," },
                { QueryPart.EqualsSymbol," = " },
                { QueryPart.EqualsNullSymbol," = " }
            };

        private static readonly IDictionary<QueryPart, string> hadoopHiveQueryParts =
            new Dictionary<QueryPart, string>()
            {
                { QueryPart.Select, "SELECT " },
                { QueryPart.From, " FROM " },
                { QueryPart.GroupBy, " GROUP BY " },
                { QueryPart.Where, " WHERE " },
                { QueryPart.Distinct, "DISTINCT " },
                { QueryPart.Top, "" },
                { QueryPart.Limit, "LIMIT {0} " },
                { QueryPart.Alias, "{0} AS {1} " },
                { QueryPart.AllColumns, "*" },
                { QueryPart.List, "({0})" },
                { QueryPart.ListItemSeparator, "," },
                { QueryPart.EmptyList, "###foo###" },
                { QueryPart.LikeSymbol, " LIKE " },
                { QueryPart.InSymbol, " IN " },
                { QueryPart.NullSymbol," NULL " },
                { QueryPart.AndSymbol,"AND" },
                { QueryPart.OrSymbol,"OR" },
                { QueryPart.ValueQuoteLeft,"'" },
                { QueryPart.ValueQuoteRight,"'" },
                { QueryPart.ValueWrap,"{0}" },
                { QueryPart.ColumnWrap,"{1}" },
                { QueryPart.EqualsValueWrap,"{0}" },
                { QueryPart.GroupColumnAlias,"{0}" },
                { QueryPart.EqualsSymbol," = " },
                { QueryPart.EqualsNullSymbol," IS " },
                { QueryPart.PredicateLimit,"" }
            };

        private static readonly IDictionary<QueryPart, string> impalaQueryParts = hadoopHiveQueryParts;

        private static readonly IDictionary<QueryPart, string> sparkSqlQueryParts = hadoopHiveQueryParts;

        #endregion Query parts

        #region Function Types

        private static readonly IDictionary<FunctionType, string> qFunctionType =
            new Dictionary<FunctionType, string>()
            {
                { FunctionType.AbsSum, "sum(abs({0}))" },
                { FunctionType.Sum, "sum({0})" },
                { FunctionType.Max, "max({0})" },
                { FunctionType.Min, "min({0})" },
                { FunctionType.SumOfProducts, "sum({0}*{1})" },
                { FunctionType.SumOfSquares, "sum({0}*{0})" },
                { FunctionType.First, "first({0})" },
                { FunctionType.Last, "last({0})" },
                { FunctionType.Count, "count({0})" },
                { FunctionType.Samples, "sum(?[null {0};0;1])" },
                { FunctionType.PositiveSum, "sum(?[{0}>0;{0};0])" },
                { FunctionType.NegativeSum, " sum(?[{0}<0;{0};0])" },
                { FunctionType.ConditionalSum, "sum(?[{0}<>0;{0};0])" },
                { FunctionType.SumOfQuotients, " sum(?[{0}<>0;1%{0};0])" },
                { FunctionType.SumOfWeightedQuotients, " sum(?[{0}<>0;{1}%{0};0])" },
                { FunctionType.NonZeroSamples, "sum(?[(abs {0})>0;1;0])" },
                { FunctionType.Mean, "avg({0})" },
            };

        #endregion Function Types

        #region Dialects

        public static SqlDialect AccessExcel
        {
            get { return dialects[AccessExcelDialectName]; }
        }

        public static SqlDialect MySQL
        {
            get { return dialects[MySqlDialectName]; }
        }

        public static SqlDialect Oracle
        {
            get { return dialects[OracleDialectName]; }
        }

        public static SqlDialect SQLServer
        {
            get { return dialects[SqlServerDialectName]; }
        }

        public static SqlDialect SybaseAnywhere
        {
            get { return dialects[SybaseAnywhereDialectName]; }
        }

        public static SqlDialect SybaseASE
        {
            get { return dialects[SybaseAseDialectName]; }
        }

        public static SqlDialect Netezza
        {
            get { return dialects[NetezzaDialectName]; }
        }

        public static SqlDialect Vertica
        {
            get { return dialects[VerticaDialectName]; }
        }

        public static SqlDialect SQLite
        {
            get { return dialects[SQLiteDialectName]; }
        }

        public static SqlDialect HadoopHive
        {
            get { return dialects[HadoopHiveDialectName]; }
        }

        public static SqlDialect KxQ
        {
            get { return dialects[KxQDialectName]; }
        }

        public static SqlDialect DB2
        {
            get { return dialects[DB2DialectName]; }
        }

        public static SqlDialect PostgreSQL
        {
            get { return dialects[PostgreSqlDialectName]; }
        }

        public static SqlDialect Impala
        {
            get { return dialects[ImpalaDialectName]; }
        }

        public static SqlDialect Redshift
        {
            get { return dialects[RedshiftDialectName]; }
        }

        public static SqlDialect Informix
        {
            get { return dialects[InformixDialectName]; }
        }

        public static SqlDialect Teradata
        {
            get { return dialects[TeradataDialectName]; }
        }

        public static SqlDialect dBase
        {
            get { return dialects[DBaseDialectName]; }
        }

        public static SqlDialect SparkSql
        {
            get { return dialects[SparkSqlDialectName]; }
        }

        #endregion Dialects

        static SqlDialectFactory()
        {
            InitializeDialects();
        }

        protected SqlDialectFactory()
        {
        }

        private static void InitializeDialects()
        {
            dialects.Add(
                AccessExcelDialectName,
                new SqlDialect(AccessExcelDialectName, "[", "]", "IIF({0},{1},{2})", "({0}) as source_table", "#{0}#", "iif(isnull({1}), null, Cstr(DatePart({0}, {1})))", accessDateParts, AnsiDateTimeConcat, "yyyy-MM-dd HH:mm:ss", sqlQueryParts));

            dialects.Add(
                MySqlDialectName,
                new SqlDialect(MySqlDialectName, "`", "`", "IF({0},{1},{2})", "({0}) as source_table", "STR_TO_DATE('{0}', '%Y-%m-%d %k:%i:%s')", AnsiDatePartFunction, ansiDateParts, "ADDTIME({0}, {1})", DefaultDateTimeFormat, sqlQueryParts, ansiLimitQueryParts));

            dialects.Add(
                OracleDialectName,
                new SqlDialect(OracleDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) {1}", "TO_DATE('{0}','YYYY-MM-DD HH24:MI:SS')", AnsiDatePartFunction, ansiDateParts, "(TO_DATE({0} || {1},'YYYY-MM-DD HH24:MI:SS'))", "yyyy-MM-dd HH:mm:ss", sqlQueryParts, oracleQueryParts));

            dialects.Add(
                SqlServerDialectName,
                new SqlDialect(SqlServerDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "CONVERT(DATETIME, '{0}', 120)", "CAST(DATEPART({0}, cast({1} as datetime)) AS VARCHAR)", sqlServerDateParts, "(CAST({0} AS DATETIME) + CAST({1} AS DATETIME))", DefaultDateTimeFormat, sqlQueryParts));

            dialects.Add(
                SybaseAnywhereDialectName,
                new SqlDialect(SybaseAnywhereDialectName, "\"", "\"", "IF {0} THEN {1} ELSE {2} ENDIF", "({0}) as {1}", "DATETIME('{0}')", "CAST(DATEPART({0}, {1}) AS VARCHAR(10))", sybaseDateParts, "(DATETIME({0}) + DATETIME({1}))", DefaultDateTimeFormat, sqlQueryParts));

            dialects.Add(
                SybaseAseDialectName,
                new SqlDialect(SybaseAseDialectName, "[", "]", "IIF({0},{1},{2})", "({0}) as source_table", "'{0}'", "CAST(DATEPART({0}, {1}) AS CHAR)", ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts));

            dialects.Add(
                NetezzaDialectName,
                new SqlDialect(NetezzaDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "TO_TIMESTAMP('{0}','YYYY-MM-DD HH:MI:SS')", AnsiDatePartFunction, ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts, ansiLimitQueryParts));

            dialects.Add(
                VerticaDialectName,
                new SqlDialect(VerticaDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "TO_TIMESTAMP('{0}','YYYY-MM-DD HH:MI:SS')", AnsiDatePartFunction, ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts, ansiLimitQueryParts));

            dialects.Add(
                SQLiteDialectName,
                new SqlDialect(SQLiteDialectName, "`", "`", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as source_table", "DATETIME('{0}')", "strftime('{0}', {1})", sqliteDateParts, "(DATETIME({0}) + DATETIME({1}))", DefaultDateTimeFormat, sqlQueryParts, ansiLimitQueryParts));

            dialects.Add(
                HadoopHiveDialectName,
                new SqlDialect(HadoopHiveDialectName, "`", "`", "if({0}, {1}, {2})", "({0}) {1}", "cast('{0}' as timestamp)", "cast({0}({1}) as string)", hadoopHiveDateParts, "(cast({0} as timestamp) + cast({1} as timestamp))", DefaultDateTimeFormat, hadoopHiveQueryParts, null));

            dialects.Add(
                KxQDialectName,
                new SqlDialect(KxQDialectName, "", "", "?[{0}; {1}; {2}]", "({0})", "(\"Z\" $19 # \"{0}\")", "(`$string  `{0}${1})", qDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, qQueryParts, null, qFunctionType, SqlDialectFormattingOptions.LowerCaseNumbers | SqlDialectFormattingOptions.DoubleQuoteEscaping));

            dialects.Add(
                DB2DialectName,
                new SqlDialect(DB2DialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "TIMESTAMP_FORMAT('{0}', 'YYYY-MM-DD HH24:MI:SS')", "VARCHAR_FORMAT(CAST({1} As TimeStamp), {0})", db2DateParts, "(CAST({0} AS TIMESTAMP) + CAST({1} AS TIMESTAMP))", DefaultDateTimeFormat, sqlQueryParts, db2QueryParts));

            //copied from Netezza sqldialect
            dialects.Add(
                PostgreSqlDialectName,
                new SqlDialect(PostgreSqlDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "TO_TIMESTAMP('{0}','YYYY-MM-DD HH:MI:SS')", AnsiDatePartFunction, ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts, ansiLimitQueryParts));

            //similar to hadoop
            dialects.Add(
                ImpalaDialectName,
                new SqlDialect(ImpalaDialectName, "", "", "if({0}, {1}, {2})", "({0}) {1}", "cast('{0}' as timestamp)", "cast({0}({1}) as string)", impalaDateParts, "(cast({0} as timestamp) + cast({1} as timestamp))", DefaultDateTimeFormat, impalaQueryParts, null));

            //sql dialect for redshift should be the same as for PGSQL 8
            dialects.Add(
                RedshiftDialectName,
                new SqlDialect(RedshiftDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "CAST('{0}' AS TIMESTAMP)", AnsiDatePartFunction, ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts, ansiLimitQueryParts));

            dialects.Add(
                InformixDialectName,
                new SqlDialect(InformixDialectName, "", "", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) {1}", "TO_DATE('{0}','%A %B %d, %Y %R')", "TO_CHAR(CAST({1} As TimeStamp), {0})", ansiDateParts, "(TO_DATE({0} || {1},'%A %B %d, %Y %R'))", "%A %B %d, %Y %R", sqlQueryParts, informixLimitQueryParts));

            dialects.Add(
                TeradataDialectName,
                new SqlDialect(TeradataDialectName, "\"", "\"", "CASE WHEN {0} THEN {1} ELSE {2} END", "({0}) as {1}", "cast('{0}' as timestamp)", AnsiDatePartFunction, ansiDateParts, AnsiDateTimeConcat, DefaultDateTimeFormat, sqlQueryParts));

            dialects.Add(
                DBaseDialectName,
                new SqlDialect(DBaseDialectName, "`", "`", "IIF ({0}, {1}, {2})", "({0}) {1}", "CTOD(\"{0}\")", "LTRIM(STR({0}({1})))", ansiDateParts, AnsiDateTimeConcat, "MM/dd/yy", sqlQueryParts, dBaseLimitQueryParts));

            //copied from hadoophive
            dialects.Add(
                SparkSqlDialectName,
                new SqlDialect(SparkSqlDialectName, "`", "`", "if({0}, {1}, {2})", "({0}) {1}", "cast('{0}' as timestamp)", "cast({0}({1}) as string)", hadoopHiveDateParts, "(cast({0} as timestamp) + cast({1} as timestamp))", DefaultDateTimeFormat, hadoopHiveQueryParts));
        }

        /// <summary>
        /// Gets the dialect by the specified connection string.
        /// </summary>
        /// <param name="connectionString">The connection string.</param>
        /// <returns>SqlDialect instance corresponding to the specified connection string.</returns>
        public static SqlDialect GetDialectByConnectionString(string connectionString)
        {
            var strBuilder = new System.Data.Common.DbConnectionStringBuilder();
            strBuilder.ConnectionString = connectionString;
            string provider = "";
            string extendedProperties = "";

            if (strBuilder.ContainsKey("Provider"))
            {
                provider = (string)strBuilder["Provider"];
                provider = provider.ToLower();
            }
            if (strBuilder.ContainsKey("Extended Properties"))
            {
                extendedProperties = (string)strBuilder["Extended Properties"];
                extendedProperties = extendedProperties.ToLower();
            }

            if (provider.StartsWith("sqloledb"))
            {
                return SQLServer;
            }
            if (provider.StartsWith("microsoft.ace.oledb.12.0") ||
                provider.StartsWith("microsoft.jet.oledb.4.0"))
            {
                return AccessExcel;
            }
            if (provider.StartsWith("msdaora") ||
                provider.StartsWith("oraoledb.oracle"))
            {
                return Oracle;
            }
            if (extendedProperties.Contains("driver={mysql odbc 5.1 driver}"))
            {
                return MySQL;
            }

            return AnsiSql;
        }

        /// <summary>
        /// Gets SqlDialect instance by its name.
        /// </summary>
        /// <param name="dialectName">Name of the dialect to obtain.</param>
        /// <returns>SqlDialect instance corresponding to the specified dialect name. 
        /// Returns <c>null</c>, if dialect is not found or dialect name is empty, </returns>
        public static SqlDialect GetDialectByName(string dialectName)
        {
            if (string.IsNullOrWhiteSpace(dialectName) == true)
                return null;

            SqlDialect dialect = null;
            if (dialects.TryGetValue(dialectName, out dialect) == true)
                return dialect;

            return null;
        }

        /// <summary>
        /// Gets the array of all available dialects.
        /// </summary>
        /// <value>
        /// An array of all SqlDialects.
        /// </value>
        public static SqlDialect[] Dialects
        {
            get
            {
                if (dialectsArray == null)
                    dialectsArray = dialects.Values.ToArray();
                return dialectsArray;
            }
        }
    }
}
