﻿using System.Text;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Query
{
    public class QueryBuilder
    {
        private string query;
        private int limit = -1;
        private SchemaAndTable schemaAndTable;
        private readonly SelectClause selectClause = new SelectClause();
        private readonly WhereClause whereClause = new WhereClause();
        private readonly GroupByClause groupByClause = new GroupByClause();

        public GroupByClause GroupBy
        {
            get { return groupByClause; }
        }

        public SchemaAndTable SchemaAndTable
        {
            get { return schemaAndTable; }
            set { schemaAndTable = value; }
        }

        public SelectClause Select
        {
            get { return selectClause; }
        }

        public string SourceQuery
        {
            get { return query; }
            set { query = value; }
        }

        public int Limit 
        {
            get { return limit; }
            set { limit = value; }
        }

        virtual public string ToString(SqlDialect dialect)
        {
            StringBuilder sb = new StringBuilder();
            
            SchemaAndTable activeSAT;
            if (query != null)
            {
                activeSAT = new SchemaAndTable(null, "source_table");
            }
            else
            {
                activeSAT = schemaAndTable;
            }

            selectClause.Top = limit;

            sb.Append(selectClause.ToString(activeSAT, dialect));
            sb.Append(" FROM ");
            if (query != null)
            {
                sb.AppendFormat(dialect.SourceQuery, query, 
                    QueryHelper.QuoteColumn(activeSAT.Table, dialect)); // "({0}) as source_table"
            }
            else
            {   
                if (activeSAT != null)
                {
                    if (activeSAT.Schema != null)
                    {
                        sb.Append(QueryHelper.QuoteColumn(activeSAT.Schema, dialect));
                        sb.Append(".");
                    }
                    sb.Append(QueryHelper.QuoteColumn(activeSAT.Table, dialect));
                }
            }
            whereClause.Limit = limit;
            sb.Append(" ");
            sb.Append(whereClause.ToString(dialect));
            sb.Append(" ");
            sb.Append(groupByClause.ToString(dialect));
            if (limit != -1)
            {
                sb.AppendFormat(dialect.GetQueryPart(QueryPart.Limit), limit);
            }
            return sb.ToString();
        }

        public WhereClause Where
        {
            get { return whereClause; }
        }
    }
}
