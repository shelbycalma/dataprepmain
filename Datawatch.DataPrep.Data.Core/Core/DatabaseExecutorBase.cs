﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core
{
    public abstract class DatabaseExecutorBase
    {
        /// <summary>
        /// The id (-1) to assign to the next request.
        /// </summary>
        /// <remarks>
        /// <para>Each call to Get(OnDemand)Data gets assigned a running id,
        /// so that the user can connect the log messages. For now, these ids
        /// are local to the plugin (can't identify requests across
        /// plugins).</para>
        /// </remarks>
        private int nextRequestId;

        /// <summary>
        /// Used by GetNumeric/Time/TextDomainValues to speed up repeated queries.
        /// </summary>
        private readonly ColumnDomainCache columnDomainCache;
        private readonly OnDemandSchemaTableCache schemaTableCache;
        public abstract DbConnection GetConnection(string connectionString);

        public abstract DbDataAdapter GetAdapter(string query, string connectionString);

        protected virtual Predicate<object[]> CreateFilter(StandaloneTable table,
            IEnumerable<ParameterValue> parameters, bool inMemoryFiltering,
            QueryMode queryMode)
        {
            return null;
        }

        protected abstract Dictionary<string, string> RenameTimeBucketingAliases(
            OnDemandParameters onDemandParameters);

        public DatabaseExecutorBase()
        {
            columnDomainCache = new ColumnDomainCache();
            schemaTableCache = new OnDemandSchemaTableCache(); 
        }

        private void AddPredicateColumnsToQuery(QueryBuilder queryBuilder,
            DatabaseQuerySettings querySettings,
            ParameterValueCollection escapedParameters)
        {
            Predicate parameterPredicate =
                GetConstrainPredicate(querySettings, queryBuilder,
                escapedParameters);


            if (!(querySettings.AutoParameterize && escapedParameters != null &&
                        querySettings.PredicateColumns != null &&
                        querySettings.PredicateColumns.Count > 0))
            {
                if (parameterPredicate != null)
                {
                    queryBuilder.Where.Predicate = parameterPredicate;
                }
                return;
            }

            foreach (DatabaseColumn column in querySettings.PredicateColumns)
            {
                string paramName = column.AppliedParameterName;
                if (string.IsNullOrEmpty(paramName)) continue;

                if (!escapedParameters.ContainsKey(paramName))
                {
                    continue;
                }

                string parameterValue;
                //Do not process special params like TimeWindowStart
                //these parameters are replaced using ApplySpecialParameters
                if (DataUtils.IsSpecialParameter(paramName))
                {
                    parameterValue = "'{" + paramName + "}'";
                }
                else
                {
                    parameterValue = "{" + paramName + "}";

                    //single quotes already handled, so we can tell encoder
                    //not to do anything on that.
                    parameterValue =
                        DataUtils.ApplyParameters(parameterValue,
                        escapedParameters, true);
                }
                parameterPredicate = new AndOperator(parameterPredicate,
                    new Comparison(Operator.In,
                    new ColumnParameter(column.ColumnName),
                    new ValueParameter(string.Format(
                         "({0})", parameterValue))));

            }
            queryBuilder.Where.Predicate = parameterPredicate;
        }

        private void AddSelectedColumnsToQuery(QueryBuilder queryBuilder, DatabaseQuerySettings querySettings)
        {
            if (querySettings.SelectedColumns == null)
            {
                return;
            }
            if (querySettings.AggregationRequired)
            {
                foreach (DatabaseColumn column in querySettings.SelectedColumns)
                {
                    if (!column.AggregateType.Equals(AggregateType.None))
                    {
                        queryBuilder.Select.AddAggregate(new[] {
                                    column.ColumnName },
                            DataUtils.ToFunctionType(column.AggregateType),
                            column.AggregateType + "_" +
                            column.ColumnName);
                    }
                    else
                    {
                        queryBuilder.Select.AddColumn(column.ColumnName, null);
                        queryBuilder.GroupBy.AddColumn(column.ColumnName);
                    }
                }
            }
            else
            {
                foreach (DatabaseColumn column in querySettings.SelectedColumns)
                {
                    queryBuilder.Select.AddColumn(column.ColumnName, null);
                }
            }
        }

        private void AddFilteredColumnsToQuery(QueryBuilder queryBuilder, DatabaseQuerySettings querySettings)
        {
            if (querySettings.FilteredColumns == null)
            {
                return;
            }

            if (querySettings.ApplyFilter)
            {
                foreach (DatabaseColumn column in querySettings.FilteredColumns)
                {
                    if (!column.FilterOperator.Equals(FilterOperator.None) && !String.IsNullOrEmpty(column.FilterValue))
                    {
                        if (queryBuilder.Where.Predicate == null)
                        {
                            queryBuilder.Where.Predicate = new Comparison(GetOperator(column.FilterOperator),
                                new ColumnParameter(column.ColumnName), new ValueParameter(string.Format(GetOperandFormat(column.FilterOperator, column.ColumnType), column.FilterValue)));
                        }
                        else
                        {
                            queryBuilder.Where.Predicate = new AndOperator(queryBuilder.Where.Predicate, new Comparison(GetOperator(column.FilterOperator),
                                new ColumnParameter(column.ColumnName), new ValueParameter(string.Format(GetOperandFormat(column.FilterOperator, column.ColumnType), column.FilterValue))));
                        }
                    }
                }
            }
        }

        private string GetOperandFormat(FilterOperator colOperator, ColumnType colType)
        {
            string strFormat = "{0}";
            string f = "";

            switch (colOperator)
            {
                case FilterOperator.EqualTo:
                case FilterOperator.GreaterThan:
                case FilterOperator.GreaterThanEqual:
                case FilterOperator.LessThan:
                case FilterOperator.LessThanEqual:
                case FilterOperator.NotEqualTo:
                    f = strFormat;
                    break;
                case FilterOperator.StartsWith:
                    f = strFormat + '%';
                    break;
                case FilterOperator.Contains:
                    f = '%' + strFormat + '%';
                    break;
                case FilterOperator.EndsWith:
                    f = '%' + strFormat;
                    break;
                default:
                    f = strFormat;
                    break;
            }
            if (colType == ColumnType.Text || colType == ColumnType.Time)
            {
                f = "'" + f + "'";
            }
            return f;
        }
        private Operator GetOperator(FilterOperator colOperator)
        {
            switch (colOperator)
            {
                case FilterOperator.EqualTo:
                    return Operator.Equals;
                case FilterOperator.GreaterThan:
                    return Operator.GreaterThan;
                case FilterOperator.GreaterThanEqual:
                    return Operator.GreaterThanOrEqualsTo;
                case FilterOperator.LessThan:
                    return Operator.LessThan;
                case FilterOperator.LessThanEqual:
                    return Operator.LessThanOrEqualsTo;
                case FilterOperator.NotEqualTo:
                    return Operator.NotEqualTo;
                case FilterOperator.Contains:
                    return Operator.Like;
                case FilterOperator.StartsWith:
                    return Operator.Like;
                case FilterOperator.EndsWith:
                    return Operator.Like;
                default:
                    return Operator.Equals;
            }
        }

        /// <summary>
        /// Builds the "base" query for on-demand. This is used both to
        /// build the query that retrieves the schema and the one for the
        /// actual execution.
        /// </summary>
        private QueryBuilder BuildOnDemandRawQuery(
            DatabaseQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters)
        {
            QueryBuilder queryBuilder = new QueryBuilder();

            if (querySettings.QueryMode == QueryMode.Table)
            {
                queryBuilder.SchemaAndTable =
                    querySettings.GetSchemaAndTable();

                if (parameters != null)
                {
                    AddPredicateColumnsToQuery(queryBuilder, querySettings,
                        DatabaseUtils.EscapeParameters(parameters));
                }
            }
            else
            {
                // Custom SQL, just apply parameters.
                queryBuilder.SourceQuery =
                    DataUtils.ApplyParameters(querySettings.Query,
                    parameters, querySettings.EncloseParameterInQuote);
            }

            return queryBuilder;
        }


        /// <summary>
        /// Builds the query used to retrieve the schema for an on-demand
        /// result. This will include all columns in the result, even if not
        /// all of them are returned by the actual on-demand query. It will
        /// NOT include the columns that hold aggregate values.
        /// </summary>
        private string BuildOnDemandSchemaQuery(QueryBuilder queryBuilder,
            DatabaseQuerySettings querySettings)
        {
            QueryBuilder schemaQueryBuilder = new QueryBuilder();

            if (!string.IsNullOrEmpty(queryBuilder.SourceQuery))
            {
                schemaQueryBuilder.SourceQuery =
                    queryBuilder.SourceQuery;
            }
            else
            {
                schemaQueryBuilder.SchemaAndTable = queryBuilder.SchemaAndTable;
                if (querySettings.SelectedColumns != null)
                {
                    foreach (DatabaseColumn column in querySettings.SelectedColumns)
                    {
                        schemaQueryBuilder.Select.AddColumn(column.ColumnName,
                            null);
                    }
                }
            }

            schemaQueryBuilder.Where.Predicate = new Comparison(Operator.Equals,
                new ValueParameter(1), new ValueParameter(0));

            return schemaQueryBuilder.ToString(querySettings.SqlDialect);
        }

        /// <summary>
        /// Builds the query for a non-on-demand (regular) request.
        /// </summary>
        protected string BuildQuery(DatabaseQuerySettings querySettings,
            int rowcount,
            ParameterValueCollection escapedParameters)
        {
            string query;
            if ((querySettings.QueryMode == QueryMode.Query) || (querySettings.QueryMode == QueryMode.Report))
            {
                query = querySettings.Query;
            }
            else
            {

                query = GenerateQuery(querySettings, escapedParameters, rowcount);
            }

            return query;
        }

        /// <summary>
        /// Returns an array of cell value converters that convert ADO types
        /// to SDK types as required. The array will have one entry per field
        /// in the reader, and the entry will be null if the values in that
        /// field do not need conversion.
        /// </summary>
        public Func<object, object>[] CreateConverters(DbDataReader reader,
            DatabaseQuerySettings querySettings)
        {
            if (querySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }
            int fields = reader.FieldCount;
            Func<object, object>[] converters =
                new Func<object, object>[fields];

            // Check each field type.
            for (int i = 0; i < fields; i++)
            {
                if (reader.GetFieldType(i) == typeof(TimeSpan))
                {
                    // TimeSpan -> DateTime converter.
                    converters[i] =
                        delegate(object value)
                        {
                            if (value == null)
                            {
                                return null;
                            }
                            // Get the base time from the settings, because we
                            // create a delegate, a copy of the value is made.
                            TimeSpan span = (TimeSpan)value;
                            DateTime time = querySettings.TimeSpanBaseTime + span;
                            return time;
                        };
                }

                if (reader.GetFieldType(i) == typeof(DateTime) &&
                    querySettings.TimeZoneHelper.TimeZoneSelected)
                {
                    // DateTime to TimezoneOffset converter.
                    converters[i] =
                        delegate(object value)
                        {
                            if (value == null)
                            {
                                return null;
                            }
                            DateTime dt = (DateTime)value;
                            dt = querySettings.TimeZoneHelper.ConvertFromUTC(dt);
                            return dt;
                        };
                }

                if (reader.GetFieldType(i) == typeof(Guid))
                {
                    // Guid -> String converter.
                    converters[i] =
                        delegate(object value)
                        {
                            if (value == null)
                            {
                                return null;
                            }
                            Guid guid = (Guid)value;
                            return guid.ToString("D");
                        };
                }
                if (reader.GetFieldType(i) == typeof(byte[]))
                {
                    // Byte[] to double converter.
                    converters[i] =
                        delegate (object value)
                        {
                            if (value == null)
                            {
                                return null;
                            }
                            byte[] bytes = (byte[])value;
                            return BitConverter.ToString(bytes).Replace(
                                "-", string.Empty);

                        };
                }
            }

            return converters;
        }

        /// <summary>
        /// Runs an on-demand query, and returns the result in a
        /// StandaloneTable. The table is initialized with columns to match
        /// the DataTable that is passed in. Note that the query may contain
        /// aggregates, with names like "A_Sum(Mcap(USD);)". These are not in
        /// the passed-in table, instead they are added automatically to the
        /// end of the column list by this method. There will also be columns
        /// in the table that is passed in that are not returned by the query.
        /// </summary>
        private StandaloneTable ExecuteOnDemandQuery(
            DatabaseQuerySettings querySettings,
            string connectionString,
            StandaloneTable table, int requestId, string query,
            IEnumerable<ParameterValue> parameters, int rowcount,
            Dictionary<string, string> columnRenames)
        {
            // This method is very similar to ExecuteQuery, only differences
            // are documented here.

            Log.Info(Resources.LogExecutingOnDemandQuery, requestId, query);
            DateTime start = DateTime.Now;
            // Fields returned by query. Declared here so we can log it
            // at the end of the method.
            int fields;

            using (DbDataAdapter adapter =
                GetAdapter(query, connectionString))
            {
                if (adapter.SelectCommand.Connection.State != ConnectionState.Open)
                {
                    adapter.SelectCommand.Connection.Open();
                }

                using (DbDataReader reader =
                    adapter.SelectCommand.ExecuteReader(
                        CommandBehavior.CloseConnection |
                        CommandBehavior.SingleResult))
                {
                    fields = reader.FieldCount;
                    // Used to hold values from the reader.
                    object[] sourceValues = new object[fields];

                    // Now adjust the table to include columns for the
                    // aggregates.
                    DatabaseUtils.UpdateStandaloneTable(table, reader, columnRenames);
                    // Once that's done, allocate array for row values.
                    object[] targetValues = new object[table.ColumnCount];

                    // Converters operate on the sourceValues array.
                    Func<object, object>[] converters =
                        CreateConverters(reader, querySettings);

                    // This filter operates on the targetValues array.
                    Predicate<object[]> filter =
                        CreateFilter(table, parameters,
                        querySettings.InMemoryFiltering, querySettings.QueryMode);

                    // Get an index mapping from sourceValues to
                    // targetValues. This is so we can quickly copy the
                    // values from the reader into an array for
                    // table.Rows.Add.
                    int[] columnMap =
                        DatabaseUtils.CreateColumnMap(reader, table,
                            columnRenames);

                    table.BeginUpdate();
                    while ((rowcount == -1 || table.RowCount < rowcount) &&
                        reader.Read())
                    {
                        try
                        {
                            //Sybase may get through reader.read() but would 
                            //throw an exception with blank message when there 
                            //is really no rows in case of grouping statements
                            reader.GetValues(sourceValues);
                        }
                        catch (Exception ex)
                        {
                            if (ex.GetType() == typeof(System.FormatException) &&
                                (querySettings.SqlDialect == SqlDialectFactory.SybaseASE ||
                                querySettings.SqlDialect == SqlDialectFactory.SybaseAnywhere))
                            {
                                Log.Info(Resources.LogSybaseEndOfFile,
                                    requestId, table.RowCount);
                                break;
                            }

                            throw new Exception(
                                string.Format(Properties.Resources.
                                ExReadingFailedAtRow,
                                (table.RowCount + 1) + ". " + ex.Message, ex));
                        }

                        DatabaseUtils.ConvertNulls(sourceValues);
                        for (int i = 0; i < fields; i++)
                        {
                            // Convert on the sourceValues.
                            if (converters[i] != null)
                            {
                                sourceValues[i] =
                                    converters[i](sourceValues[i]);
                            }
                            // Now copy to targetValues. We do not need to
                            // clear it first, since sourceValues is "clean"
                            // and positions in targetValues that are not in
                            // the columnMap will remain nulled.
                            targetValues[columnMap[i]] = sourceValues[i];
                        }
                        if (filter == null || filter(sourceValues))
                        {
                            table.AddRow(targetValues);
                        }
                    }
                    table.EndUpdate();
                }
            }
            // Exceptions are not caught here, calling method should catch.

            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            // Use the fields variable here, not table.ColumnCount,
            // since we want to log how much work was done by the query.
            Log.Info(Resources.LogQueryResultSizeAndTime,
                requestId, table.RowCount, fields, seconds);

            return table;
        }

        /// <summary>
        /// Runs a regular (non-on-demand) query, and returns the result.
        /// </summary>
        private ITable ExecuteQuery(DatabaseQuerySettings querySettings,
            int requestId, string connectionString, string query, int rowcount,
            IEnumerable<ParameterValue> parameters)
        {
            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingQuery, requestId, query);
            DateTime start = DateTime.Now;

            StandaloneTable table = null;

            using (DbDataAdapter adapter =
                GetAdapter(query, connectionString))
            {
                adapter.SelectCommand.CommandTimeout = 180;

                // Have to manually open the connection as the adapter will
                // only do that when you use Fill or GetData.
                if (adapter.SelectCommand.Connection.State != ConnectionState.Open)
                {
                    adapter.SelectCommand.Connection.Open();
                }

                // Get the reader so we can process row by row.
                using (DbDataReader reader =
                    adapter.SelectCommand.ExecuteReader(
                        CommandBehavior.CloseConnection |
                        CommandBehavior.SingleResult))
                {
                    // Create table and initialize schema from result.
                    if (querySettings.SchemaColumnsSettings.SchemaColumns.Count > 0)
                    {
                        table = DatabaseUtils.CreateTableFromSavedSchema(querySettings);
                    }
                    else
                    {
                        table = DatabaseUtils.CreateStandaloneTable(reader, querySettings);
                    }
                    if (querySettings.IsSchemaRequest)
                    {
                        return table;
                    }

                    // Object array will hold cell values.
                    int fields = reader.FieldCount;
                    object[] values = new object[fields];

                    int targetFields = table.ColumnCount;
                    object[] targetValues = new object[targetFields];

                    int[] columnMap =
                        DatabaseUtils.CreateColumnMap(reader, table,
                            new Dictionary<string, string>());
                    // Each column may have a value converter associated,
                    // typically ADO TimeSpan columns need to be converted
                    // into DateTimes for SDK Time columns, but most are
                    // converted "naturally" (and for those the converter
                    // will be set to null).
                    Func<object, object>[] converters =
                        CreateConverters(reader, querySettings);

                    // In-memory filtering (which is not). This predicate can
                    // be null (all rows will be included) or should return
                    // true for the ones that should be included. Filtering
                    // happens as the rows are processed.
                    Predicate<object[]> filter = null;
                    if (querySettings.ApplyRowFilteration)
                    {
                        filter = CreateFilter(table, parameters,
                        querySettings.InMemoryFiltering, querySettings.QueryMode);
                    }
                    // Begin/EndUpdate mostly to minimize work done by table,
                    // of course there are no listeners at this point.
                    table.BeginUpdate();
                    while ((rowcount == -1 || table.RowCount < rowcount) &&
                        reader.Read())
                    {
                        // Pop row values into our array. This call will
                        // overwrite any previous values, so we don't have
                        // to reset them first.
                        try
                        {
                            //Sybase may get through reader.read() but would 
                            //throw an exception with blank message when there 
                            //is really no rows in case of grouping statements
                            reader.GetValues(values);
                        }
                        catch (Exception ex)
                        {
                            if (ex.GetType() == typeof(System.FormatException) &&
                               (querySettings.SqlDialect == SqlDialectFactory.SybaseASE ||
                               querySettings.SqlDialect == SqlDialectFactory.SybaseAnywhere))
                            {
                                Log.Info(Resources.LogSybaseEndOfFile,
                                    requestId, table.RowCount);
                                break;
                            }

                            throw new Exception(
                                string.Format(Properties.Resources.
                                ExReadingFailedAtRow,
                                (table.RowCount + 1) + ". " + ex.Message, ex));
                        }

                        // Convert DBNulls to regular nulls. Makes life
                        // easier for the converters and filters below, and
                        // prevents AddRow(object[]) from blowing up.
                        DatabaseUtils.ConvertNulls(values);
                        // Check for converters and let them have a go before
                        // we do anything with the values.
                        for (int i = 0; i < fields; i++)
                        {
                            if (converters[i] != null)
                            {
                                values[i] = converters[i](values[i]);
                            }
                            if (columnMap[i] != -1)
                            {
                                targetValues[columnMap[i]] = values[i];
                            }
                        }
                        // Then check if the row is filtered out.
                        if (filter == null || filter(targetValues))
                        {
                            // This call makes a "copy" of the array, so we
                            // don't have to clone it.
                            table.AddRow(targetValues);
                        }
                    }

                    // We're creating a new StandaloneTable on each request,
                    // so allow EX to freeze the table we return.
                    table.CanFreeze = true;

                    table.EndUpdate();
                }
            }
            // Exceptions are not caught here, calling method should catch.

            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Resources.LogQueryResultSizeAndTime,
                requestId, table.RowCount, table.ColumnCount, seconds);

            return table;
        }

        public string GenerateQuery(DatabaseQuerySettings querySettings,
            ParameterValueCollection escapedParameters, int rowCount = -1)
        {
            if (querySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }
            QueryBuilder queryBuilder = new QueryBuilder();

            if (!string.IsNullOrEmpty(querySettings.Table))
            {
                queryBuilder.SchemaAndTable =
                    querySettings.GetSchemaAndTable();
            }

            queryBuilder.Limit = rowCount;

            AddSelectedColumnsToQuery(queryBuilder, querySettings);
            AddFilteredColumnsToQuery(queryBuilder, querySettings);

            if (querySettings.ApplyRowFilteration)
            {
                AddPredicateColumnsToQuery(queryBuilder, querySettings,
                    escapedParameters);
            }
            return queryBuilder.ToString(querySettings.SqlDialect);
        }

        private Predicate GetConstrainPredicate(
            DatabaseQuerySettings querySettings, QueryBuilder queryBuilder,
            ParameterValueCollection escapedParameters)
        {
        
            Predicate predicate = null;

            if (!querySettings.IsConstrainByDateTime)
            {
                return predicate;
            }

            bool dateTimeSet = false;
            bool dateSet = false;
            bool timeSet = false;
            bool isFromDateRequired = false;
            bool isToDateRequired = false;

            string dateTimeColumn = querySettings.SelectedDatetimeColumn;
            string dateColumn = querySettings.SelectedDateColumn;
            string timeColumn = querySettings.SelectedTimeColumn;

            string fromDate = querySettings.FromDate;
            string toDate = querySettings.ToDate;
            SqlDialect dialect = querySettings.SqlDialect;
            TimeZoneHelper timeZoneHelper = querySettings.TimeZoneHelper;

            if (!string.IsNullOrEmpty(dateTimeColumn))
            {
                dateTimeSet = true;
            }
            if (!string.IsNullOrEmpty(dateColumn))
            {
                dateSet = true;
            }
            if (!string.IsNullOrEmpty(timeColumn))
            {
                timeSet = true;
            }

            if (!dateTimeSet && !dateSet)
            {
                return predicate;
            }

            //for datetime values escape parameters singlequotes are not required
            //hence enforcing to remove them 
            fromDate = DataUtils.ApplyParameters(fromDate, escapedParameters,
                        false, true);
            fromDate = DataUtils.ConvertToUTC(fromDate,
                timeZoneHelper, dialect.DateTimeFormat);

            if (!(string.IsNullOrEmpty(fromDate)))
            {
                isFromDateRequired = true;
                fromDate = string.Format(dialect.DateTimeConvert, fromDate);
            }

            toDate = DataUtils.ApplyParameters(toDate, escapedParameters,
                    false, true);
            toDate = DataUtils.ConvertToUTC(toDate,
                timeZoneHelper, dialect.DateTimeFormat);

            if (!(string.IsNullOrEmpty(toDate)))
            {
                isToDateRequired = true;
                toDate = string.Format(dialect.DateTimeConvert, toDate);
            }

            if (!isFromDateRequired && !isToDateRequired)
            {
                return predicate;
            }

            SqlParameter leftParameter = null;

            if (dateTimeSet)
            {
                leftParameter = new ColumnParameter(dateTimeColumn);
            }
            else if (dateSet && timeSet)
            {
                leftParameter = new ValueParameter(dialect.ConcatDateAndTime(
                        dateColumn, timeColumn));
            }
            else if (dateSet)
            {
                leftParameter = new ColumnParameter(dateColumn);
            }
            else if (timeSet)
            {
                leftParameter = new ColumnParameter(timeColumn);
            }

            if (isFromDateRequired)
            {
                predicate = new AndOperator(predicate,
                    new Comparison(Operator.GreaterThanOrEqualsTo,
                        leftParameter,
                        new ValueParameter(fromDate)));
            }
            if (isToDateRequired)
            {
                predicate = new AndOperator(predicate,
                    new Comparison(Operator.LessThanOrEqualsTo,
                        leftParameter,
                        new ValueParameter(toDate)));
            }

            return predicate;
        }

        /// <summary>
        /// Returns available columns using a schema query.
        /// </summary>
        /// <param name="querySettings"></param>
        /// <param name="parameters"></param>
        /// <param name="connectionString">Needs be be pre-parameterized/corrected</param>
        /// <returns></returns>
        public IEnumerable<DatabaseColumn> GetColumns(
            DatabaseQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            string connectionString)
        {
            List<DatabaseColumn> columns = new List<DatabaseColumn>();

			ParameterValueCollection lookup = new ParameterValueCollection();
			lookup.AddRange(parameters);

			QueryBuilder schemaQueryBuilder = new QueryBuilder();

			schemaQueryBuilder.SchemaAndTable = querySettings.SelectedTable;

			schemaQueryBuilder.Where.Predicate = new Comparison(Operator.Equals,
				new ValueParameter(1), new ValueParameter(0));

			string query = schemaQueryBuilder.ToString(querySettings.SqlDialect);

			Log.Info(Resources.LogRetrievingColumnsQuery, query);

            using (DbDataAdapter adapter =
                GetAdapter(query, connectionString))
            {
                adapter.SelectCommand.CommandTimeout = querySettings.QueryTimeout;

                // Have to manually open the connection as the adapter will
                // only do that when you use Fill or GetData.
                adapter.SelectCommand.Connection.Open();

                // Get the reader so we can process row by row.
                using (DbDataReader reader =
                    adapter.SelectCommand.ExecuteReader(
                        CommandBehavior.CloseConnection |
                        CommandBehavior.SingleResult))
                {
                    StringBuilder columnsBuilder = new StringBuilder();

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        string name = reader.GetName(i);
                        Type type = reader.GetFieldType(i);

                        DatabaseColumn col = new DatabaseColumn(new PropertyBag());
                        col.ColumnName = name;
                        // Allow fallback while geting column type.
                        col.ColumnType = DatabaseUtils.GetColumnType(type, false);
						if (col.ColumnType == ColumnType.Other)
						{
							Log.Info(Resources.LogUnsupportedColumnType, type.Name, col.ColumnName);
							continue;
						}

                        col.ColumnTypeName = reader.GetDataTypeName(i);

                        columnsBuilder.Append(name + ":" + col.ColumnTypeName +
                            ":" + type + ",");
                        // TODO: Predicate/Select column should be moved to 
                        // QueryBuilderViewModel.
                        bool loadDefaultParameterName = true;
                        if (querySettings.PredicateColumns != null &&
                            querySettings.PredicateColumns.Count > 0)
                        {
                            foreach (DatabaseColumn predicateCol in
                                querySettings.PredicateColumns)
                            {
                                string appliedParameter =
                                    predicateCol.AppliedParameterName;

                                if (predicateCol.ColumnName.Equals(name) &&
                                    (lookup.ContainsKey(appliedParameter) ||
                                    (appliedParameter != null)))
                                {
                                    col.AppliedParameterName = appliedParameter;
                                    loadDefaultParameterName = false;
                                    break;
                                }
                            }
                        }
                        if (loadDefaultParameterName &&
                                lookup.ContainsKey(name))
                        {
                            col.AppliedParameterName = lookup[name].Name;
                        }
                        columns.Add(col);
                    }
                    if (columnsBuilder.Length > 0)
                    {
                        columnsBuilder.Length--;
                    }
                    Log.Info(Resources.LogColumnsRetrieved,
                        columns.Count, columnsBuilder.ToString());
                }

            }
            return columns;
        }

		private static bool IsValidQuery(string query)
		{
			if (query.Contains(";"))
				return false;
			if (query.Contains("--"))
				return false;
			if (!(query.ToUpper().StartsWith("SELECT") || query.ToUpper().StartsWith("EXEC")))
				return false;

			return true;
		}

        /// <summary>
        /// Returns data for a regular (non-on-demand) connection. This public
        /// method is used by the plugin to implement IDataPlugin.GetData.
        /// </summary>
        public ITable GetData(DatabaseQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters, int rowcount)
        {

            if (querySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }
            // Get an ID to identify this particular request in log.
            int requestId = Interlocked.Increment(ref nextRequestId);

            ParameterValueCollection escapedParameters = null;
            if (parameters != null)
            {
                escapedParameters =
                DatabaseUtils.EscapeParameters(parameters);
            }
            // First, build the query.
            string query =
                BuildQuery(querySettings, rowcount, escapedParameters);

			if (!IsValidQuery(query))
			{
				throw Exceptions.InvalidQuery();
			}

            //Then replace special parameters
            query = DataUtils.ApplySpecialParameters(query, parameters,
                querySettings.TimeZoneHelper, querySettings.EncloseParameterInQuote,
                null);

            escapedParameters = DatabaseUtils.ConvertDateTimeParametersToUTC(
                escapedParameters,
                querySettings.TimeZoneHelper);

            // Then replace parameters with values.
            //EnforceSingleQuoteHandling should always get default value i.e. true here,
            //so that old workbooks would keep working as it used to work.
            query = DataUtils.ApplyParameters(query, escapedParameters,
                querySettings.EncloseParameterInQuote);

            string connectionString =
                DataUtils.ApplyParameters(querySettings.ConnectionString,
                                      parameters, false);

            // Log fixed connection string.
            Log.Info(Resources.LogConnectionString, requestId, CleanConnectionString(connectionString));

            // Run the query and get the result as an ITable. This call
            // also handles in-memory filtering.
            ITable table = null;
            try
            {
                table = ExecuteQuery(querySettings, requestId,
                    connectionString, query, rowcount, parameters);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                if (querySettings.ConnectionString.Contains("Salesforce"))
                {
                    if (ex.Message.Contains("Object reference not set to an instance of an object."))
                        throw new Exception(Properties.Resources.SalesforceConnectionObjectReferenceError);
                    else if (ex.Message.Contains("ERROR [HY000] HTTP protocol error. 400 Bad Request."))
                        throw new Exception(Properties.Resources.SalesforceConnectionProtocolError);
                    else
                        throw new Exception(ErrorMessages.GetDisplayMessage(ex.Message));
                }
                else
                {
                        throw new Exception(ErrorMessages.GetDisplayMessage(ex.Message));
                }
            }

            return table;
        }

		private string CleanConnectionString(string connstr)
		{
			string newStr = "";

			string[] conn = connstr.Split(';');
			foreach (string s in conn)
			{
				if (!(s.ToUpper().StartsWith("PASSWORD") || s.ToUpper().StartsWith("PWD") || s.ToUpper().StartsWith("RTK") || s.ToUpper().StartsWith("APITOKEN")))
				{
					newStr += (newStr.Length > 0 ? ";" : String.Empty) + s;
				}
			}
			return newStr;
		}

        //private NumericInterval GetNumericDomain(string connectionString,
        //    int requestId, QueryBuilder queryBuilder, string column,
        //    SqlDialect sqlDialect)
        //{
        //    NumericInterval domainValues = null;
        //    // Build a unique cache key for this column. The connection
        //    // string, schema + table or SQL, and column name are used.
        //    // Since only the hash codes are used, collisions are possible
        //    // but highly unlikely.
        //    ColumnDomainCacheKey cacheKey =
        //        new ColumnDomainCacheKey(connectionString,
        //            queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
        //            column, queryBuilder.Where.Predicate.ToString());

        //    // Check for domain in cache.
        //    ColumnDomain columnDomain = columnDomainCache[cacheKey];

        //    if (columnDomain is GenericColumnDomain<NumericInterval>)
        //    {
        //        return 
        //            ((GenericColumnDomain<NumericInterval>)columnDomain).Domain;
        //    }

        //    string query = DatabaseUtils.GetQueryForNumericDomains(queryBuilder,
        //        column, sqlDialect);

        //    try
        //    {
        //        using (DbConnection connection =
        //            GetConnection(connectionString))
        //        {
        //            if (connection.State == ConnectionState.Closed)
        //            {
        //                connection.Open();
        //            }
        //            domainValues = DatabaseUtils.GetNumericDomain(connection,
        //                query, column, sqlDialect, requestId);   
        //        }

        //        // Store it in the cache.
        //        columnDomainCache[cacheKey] =
        //            new GenericColumnDomain<NumericInterval>(domainValues);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(Resources.LogDomainQueryFailed,
        //            requestId, column, ex);
        //        // We catch the error, and let the method finish. Because
        //        // the array is set to null, and the way the cache is tested
        //        // above, the query will still get executed every time.
        //        domainValues = null;
        //    }

        //    return domainValues;
        //}

        /// <summary>
        /// Returns data for an on-demand connection. This public method is
        /// used by the plugin to implement IOnDemandPlugin.GetOnDemandData.
        /// </summary>
        public ITable GetOnDemandData(DatabaseQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount,
            bool useSchemaTableCache = true)
        {
            if (querySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }
            // TODO: try-catch with error reporting.

            // Get an ID to identify this particular request in log.
            int requestId = Interlocked.Increment(ref nextRequestId);

            // This is a little bit more involved than the regular GetData,
            // since we need a separate step to get the schema, and some
            // extra stuff at the end to return meta data.

            // Build the basic query (does not include aggregates, group bys,
            // or wheres).
            QueryBuilder queryBuilder = BuildOnDemandRawQuery(querySettings,
                parameters);
            string connectionString =
                DataUtils.ApplyParameters(querySettings.ConnectionString,
                                      parameters, false);
            // Use that to build the schema query.
            string schemaQuery =  BuildOnDemandSchemaQuery(queryBuilder,
                querySettings);

            OnDemandSchemaTableCacheKey schemaTableCacheKey =
                new OnDemandSchemaTableCacheKey(schemaQuery,
                    connectionString,
                    querySettings.GetSelectedColumnNames().ToArray());

            StandaloneTable schema = null;

            if (useSchemaTableCache)
            {
                schema = schemaTableCache[schemaTableCacheKey];
            }
            // Log fixed connection string.
            Log.Info(Resources.LogConnectionString, requestId, CleanConnectionString(connectionString));

            if (schema == null)
            {
                DataTable dataTable;
                if (querySettings.SchemaColumnsSettings.SchemaColumns.Count > 0)
                {
                    schema = DatabaseUtils.CreateTableFromSavedSchema(querySettings);
                }
                else
                {
                    // Get the schema as a DataTable. All columns in the basic query
                    // are represented, even if they will not be returned by the
                    // actual on-demand query. Aggregate columns are not, they will
                    // be added when the query runs.
                    dataTable =
                        GetSchema(requestId, connectionString, schemaQuery);
                    // Create table from schema to ensure that we have all columns
                    // in the right order, even the ones not returned by the query.
                    schema = DatabaseUtils.CreateStandaloneTable(dataTable, querySettings);
                    schemaTableCache[schemaTableCacheKey] = schema;
                }
            }
            if(querySettings.IsSchemaRequest)
            {
                return schema;
            }

            // Rename time bucketings to t1, t2, etc. to avoid problems in 
            // databases not fully supporting double quote column/alias wrapping.
            Dictionary<string, string> columnMap =
                RenameTimeBucketingAliases(onDemandParameters);

            // Modify the query so only the on-demand data is returned: add
            // group by and where clauses, add aggregates, only select the
            // relevant columns.
            if(onDemandParameters is IOnDemandPreviewParameters)
            {
                OnDemandHelper.BuildOnDemandPreviewQuery(queryBuilder,
                    querySettings.GetSelectedColumnNames(), rowcount);
            }
            else
            {
                if (onDemandParameters != null && onDemandParameters.Predicates != null)
                {
                    foreach (Predicate predicate in onDemandParameters.Predicates)
                    {
                        Utils.UpdateTimeValueInPredicate(predicate,
                            querySettings, schema);
                    }
                }
                OnDemandHelper.BuildOnDemandQuery(queryBuilder,
                    onDemandParameters);
                queryBuilder.Limit = rowcount;
            }

            StandaloneTable table;

            // Should always use schema if rowcount is zero,
            // Some databases like MS Access doesn't like queries like SELECT TOP 0 *
            if (rowcount == 0 || 
                (!(onDemandParameters is IOnDemandPreviewParameters) &&
                queryBuilder.Select.IsEmpty))
            {
                table = schema;
            }
            else
            {
                string query = queryBuilder.ToString(querySettings.SqlDialect);

                
                // Run the query and get the result as a StandaloneTable.
                table = ExecuteOnDemandQuery(querySettings, connectionString, schema,
                requestId, query, parameters,
                rowcount, columnMap);
            }

            DatabaseUtils.SetOnDemandTimeBucketMetaData(table,
                onDemandParameters, columnMap);

            // For text columns that are used in filters, get their domains
            // and add them to the table's meta data.
            //SetOnDemandColumnDomains(querySettings, requestId, table,
            //    onDemandParameters, connectionString, queryBuilder, rowcount);


            // We're creating a new StandaloneTable on each request, so allow
            // EX to freeze the table we return.
            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        /// <summary>
        /// Returns the schema for a query result in the form of a DataTable.
        /// </summary>
        private DataTable GetSchema(int requestId, string connectionString,
            string query)
        {
            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingSchemaQuery, requestId, query);
            DateTime start = DateTime.Now;

            DataTable table = new DataTable();

            using (DbDataAdapter adapter = GetAdapter(query, connectionString))
            {
                adapter.FillSchema(table, SchemaType.Source);
            }
            // Exceptions are not caught here, calling method should catch.

            // Done, log out how long it took.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Resources.LogQueryResultTime, requestId, seconds);

            // Clear out constraints from the schema. When this DataTable
            // is used to hold on-demand results, columns may be empty
            // on rows that represent aggregates. So we need to allow that
            // in this table, even if the database table does not.
            table.PrimaryKey = null;
            foreach (DataColumn column in table.Columns)
            {
                column.AllowDBNull = true;
            }

            return table;
        }

        /// <summary>
        /// Used with on-demand connections to get the domains (distinct
        /// values) for text columns that are not populated by the by the
        /// query (they are aggregated away). This is used by text filters.
        /// </summary>
        //private string[] GetTextDomainValues(int requestId,
        //    string connectionString, QueryBuilder queryBuilder, string column,
        //    int rowcount, SqlDialect sqlDialect)
        //{
        //    // Build a unique cache key for this column. The connection
        //    // string, schema + table or SQL, and column name are used.
        //    // Since only the hash codes are used, collisions are possible
        //    // but highly unlikely.
        //    ColumnDomainCacheKey cacheKey =
        //        new ColumnDomainCacheKey(connectionString,
        //            queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
        //                column, queryBuilder.Where.Predicate.ToString());

        //    // Check for domain in cache.
        //    ColumnDomain columnDomain = columnDomainCache[cacheKey];

        //    if (columnDomain is GenericColumnDomain<string[]>)
        //    {
        //        return ((GenericColumnDomain<string[]>)columnDomain).Domain;
        //    }

        //    string[] domainValues = null;

        //    try
        //    {
        //        string query =
        //            DatabaseUtils.GetQueryForTextDomains(queryBuilder, column,
        //            sqlDialect, rowcount);

        //        using (DbConnection connection = GetConnection(connectionString))
        //        {
        //            if (connection.State == ConnectionState.Closed)
        //            {
        //                connection.Open();
        //            }
        //            domainValues = DatabaseUtils.GetTextDomainValues(connection,
        //                query, column, rowcount, requestId);
        //        }

        //        // Store it in the cache.
        //        columnDomainCache[cacheKey] =
        //            new GenericColumnDomain<string[]>(domainValues);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(Resources.LogDomainQueryFailed,
        //            requestId, column, ex);
        //        // We catch the error, and let the method finish. Because
        //        // the array is set to null, and the way the cache is tested
        //        // above, the query will still get executed every time.
        //        domainValues = null;
        //    }

        //    return domainValues;
        //}

        //private string[] GetTimeBucketDomainValues(int requestId,
        //    string connectionString, QueryBuilder queryBuilder, string column,
        //    string timeColumn, TimePart timePart,
        //    int rowcount, SqlDialect sqlDialect)
        //{
        //    // Build a unique cache key for this column. The connection
        //    // string, schema + table or SQL, and column name are used.
        //    // Since only the hash codes are used, collisions are possible
        //    // but highly unlikely.
        //    ColumnDomainCacheKey cacheKey =
        //        new ColumnDomainCacheKey(connectionString,
        //            queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
        //                column, queryBuilder.Where.Predicate.ToString());

        //    // Check for domain in cache.
        //    ColumnDomain columnDomain = columnDomainCache[cacheKey];

        //    if (columnDomain is GenericColumnDomain<string[]>)
        //    {
        //        return ((GenericColumnDomain<string[]>)columnDomain).Domain;
        //    }

        //    string query = DatabaseUtils.GetQueryForTimeBucketDomains(queryBuilder,
        //        column, timeColumn, timePart, sqlDialect, rowcount);

        //    string[] domainValues;

        //    try
        //    {
        //        // Log the query and time the execution.
        //        Log.Info(Resources.LogExecutingDomainQuery, requestId, query);
        //        DateTime start = DateTime.Now;

        //        using (DbConnection connection = GetConnection(connectionString))
        //        {
        //            if (connection.State == ConnectionState.Closed)
        //            {
        //                connection.Open();
        //            }

        //            domainValues =
        //                DatabaseUtils.GetTimeBucketDomainValues(connection,
        //                    query, column, timeColumn, timePart,
        //                    sqlDialect, requestId, rowcount);
        //        }

        //        // Done, log out how long it took and how much we got.
        //        DateTime end = DateTime.Now;
        //        double seconds = (end - start).TotalSeconds;
        //        Log.Info(Resources.LogQueryResultSizeAndTime, requestId,
        //            domainValues.Length, 1, seconds);

        //        //Array.Sort(domainValues, new OnDemandTimeBucketComparer());

        //        // Store it in the cache.
        //        columnDomainCache[cacheKey] =
        //            new GenericColumnDomain<string[]>(domainValues);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Error(Resources.LogDomainQueryFailed,
        //            requestId, column, ex);
        //        // We catch the error, and let the method finish. Because
        //        // the array is set to null, and the way the cache is tested
        //        // above, the query will still get executed every time.
        //        domainValues = null;
        //    }

        //    return domainValues;
        //}

        private TimeValueInterval GetTimeDomain(
            DatabaseQuerySettings querySettings, int requestId,
            string connectionString, QueryBuilder queryBuilder, string column)
        {
            string columnName = column;
            if (querySettings.TimeZoneHelper != null &&
                !string.IsNullOrEmpty(
                querySettings.TimeZoneHelper.SelectedTimeZone))
            {
                columnName += "_" + querySettings.TimeZoneHelper.SelectedTimeZone;
            }
            TimeValueInterval domainValues = null;
            // Build a unique cache key for this column. The connection
            // string, schema + table or SQL, and column name are used.
            // Since only the hash codes are used, collisions are possible
            // but highly unlikely.
            ColumnDomainCacheKey cacheKey =
                new ColumnDomainCacheKey(connectionString,
                    queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
                        columnName, queryBuilder.Where.Predicate.ToString());

            // Check for domain in cache.
            ColumnDomain columnDomain = columnDomainCache[cacheKey];

            if (columnDomain is GenericColumnDomain<TimeValueInterval>)
            {
                return ((GenericColumnDomain<TimeValueInterval>)
                    columnDomain).Domain;
            }

            string query =
                DatabaseUtils.GetQueryForTimeDomains(queryBuilder, column,
                querySettings.SqlDialect);

            try
            {
                using (DbConnection connection = GetConnection(connectionString))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                    }

                    domainValues =
                        GetTimeDomainValues(querySettings, connection, query,
                        column, requestId);
                }

                // Store it in the cache.
                columnDomainCache[cacheKey] =
                    new GenericColumnDomain<TimeValueInterval>(domainValues);
            }
            catch (Exception ex)
            {
                Log.Error(Resources.LogDomainQueryFailed,
                    requestId, column, ex);
                // We catch the error, and let the method finish. Because
                // the array is set to null, and the way the cache is tested
                // above, the query will still get executed every time.
                domainValues = null;
            }

            return domainValues;
        }

        private TimeValueInterval GetTimeDomainValues(
            DatabaseQuerySettings querySettings, DbConnection connection,
            string query, string column, int requestId)
        {
            TimeValueInterval domainValues = null;

            try
            {
                // Log the query and time the execution.
                Log.Info(Properties.Resources.LogExecutingDomainQuery, requestId,
                    query);
                DateTime start = DateTime.Now;

                DateTime min;
                DateTime max;

                using (DbCommand command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    using (DbDataReader reader = command.ExecuteReader())
                    {
                        int fields = reader.FieldCount;
                        object[] values = new object[fields];

                        Func<object, object>[] converters =
                            CreateConverters(reader, querySettings);

                        if (reader.Read())
                        {
                            reader.GetValues(values);
                        }

                        // Done, log out how long it took and how much we got.
                        DateTime end = DateTime.Now;
                        double seconds = (end - start).TotalSeconds;
                        Log.Info(Resources.LogQueryResultSizeAndTime, requestId,
                            1, fields, seconds);

                        for (int i = 0; i < fields; i++)
                        {
                            if (converters[i] != null)
                            {
                                values[i] = converters[i](values[i]);
                            }
                        }
                        min = (DateTime)values[0];
                        max = (DateTime)values[1];

                        domainValues = new TimeValueInterval(min, max);
                    }
                }

            }
            catch (Exception ex)
            {
                Log.Error(Properties.Resources.LogDomainQueryFailed, requestId,
                    column, ex);
                // We catch the error, and let the method finish. Because
                // the array is set to null, and the way the cache is tested
                // above, the query will still get executed every time.
                domainValues = null;
            }

            return domainValues;
        }

        /// <summary>
        /// Retrieves domains for relevant text columns: if a text column is
        /// used in a filter, the filter needs to know the distinct values,
        /// so we communicate those through the columns' meta data.
        /// </summary>
    //    private void SetOnDemandColumnDomains(
    //        DatabaseQuerySettings querySettings, int requestId,
    //        StandaloneTable table, OnDemandParameters onDemandParameters,
    //        string connectionString, QueryBuilder queryBuilder,
    //        int rowcount)
    //    {
    //        // Were domains requested in the on-demand parameters?
    //        if (onDemandParameters == null ||
    //            onDemandParameters.Domains == null)
    //        {
    //            return;
    //        }

    //        table.BeginUpdate();
    //        try
    //        {
    //            foreach (string domain in onDemandParameters.Domains)
    //            {
    //                Column column = table.GetColumn(domain);
    //                if (column is TextColumn)
    //                {
    //                    // Get the text column (skip if not found).
    //                    TextColumn textColumn = (TextColumn)column;
    //                    string[] domainValues;
    //                    if (column.MetaData is ITimeBucketColumnMetaData)
    //                    {
    //                        ITimeBucketColumnMetaData meta =
    //                            (ITimeBucketColumnMetaData)column.MetaData;
    //                        domainValues =
    //                            GetTimeBucketDomainValues(requestId,
    //                            connectionString, queryBuilder, domain,
    //                            meta.TimeColumn.Name, meta.TimePart, rowcount,
    //                            querySettings.SqlDialect);
    //                    }
    //                    else
    //                    {
    //                        domainValues =
    //                            GetTextDomainValues(requestId,
    //                            connectionString, queryBuilder, domain, rowcount,
    //                            querySettings.SqlDialect);
    //                    }
    //                    // Set the domain in the column's meta data.
    //                    ((IMutableTextColumnMetaData)textColumn.MetaData).Domain
    //                        = domainValues;
    //                }
    //                else if (column is NumericColumn)
    //                {
    //                    NumericColumn numericColumn = (NumericColumn)column;
    //                    ((IMutableNumericColumnMetaData)numericColumn.MetaData).
    //                        Domain = GetNumericDomain(connectionString,
    //                        requestId, queryBuilder, domain,
    //                        querySettings.SqlDialect);
    //                }
    //                else if (column is TimeColumn)
    //                {
    //                    TimeColumn timeColumn = (TimeColumn)column;
    //                    ((IMutableTimeColumnMetaData)timeColumn.MetaData).
    //                        Domain = GetTimeDomain(querySettings, requestId,
    //                            connectionString, queryBuilder, domain);
    //                }
    //            }
    //        }
    //        finally
    //        {
    //            table.EndUpdate();
    //        }
    //    }
    }
}
