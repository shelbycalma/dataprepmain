﻿using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    internal sealed class IndexedTextStorage : StorageBase<string>
    {
        private const double EfficientRatio = 0.7;

        private readonly StorageInt keys;
        private readonly StoragePointer<string> values;
        private StringIndex index;

        /// <summary>
        /// Remembers the StringIndex count if the index has been deleted
        /// (this is needed if we ever need to rebuild the index).
        /// </summary>
        private int deletedIndexCount;

        public IndexedTextStorage(int capacity)
        {
            // TODO: Rebase so 0 is -1?
            keys = new StorageInt(capacity);
            values = new StoragePointer<string>(100);
            index = new StringIndex(100);
            int nullKey = index.GetKey(null);
            Debug.Assert(nullKey == 0);
        }

        internal IndexedTextStorage(StorageInt keys,
            StoragePointer<string> values, StringIndex index)
        {
            this.keys = keys;
            this.values = values;
            this.index = index;
        }

        public override int Capacity
        {
            get { return keys.Capacity; }
            set {
                int oldCapacity = keys.Capacity;
                keys.Capacity = value;
                if (value > oldCapacity) {
                    keys.Clear(oldCapacity, value - oldCapacity, 0);
                }
            }
        }

        public void Clear(int record)
        {
            keys[record] = 0;
        }

        public void Clear(int start, int count)
        {
            keys.Clear(start, count, 0);
        }

        public override void Compact(int[] holes)
        {
            keys.Compact(holes);
        }

        // TODO: Can probably be faster.
        public static IndexedTextStorage Compress(
            StoragePointer<string> flat, int records)
        {
            int capacity = flat.Capacity;
            IndexedTextStorage indexed = new IndexedTextStorage(capacity);
            for (int i = 0; i < records; i++) {
                indexed[i] = flat[i];
            }
            return indexed;
        }

        /// <summary>
        /// Copies the contents of this storage into another indexed text
        /// storage.
        /// </summary>
        /// <param name="target">The storage to populate with the contents
        /// of this one.</param>
        /// <param name="minCapacity">The minimum capacity to set the target
        /// storage to (needed if the source storage has smaller capacity than
        /// the target's table's row capacity).</param>
        /// <remarks>
        /// <para>This clears any existing contents in the
        /// <paramref name="target"/> storage before copying.</para>
        /// </remarks>
        internal void CopyTo(IndexedTextStorage target, int minCapacity)
        {
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }

            keys.CopyTo(target.keys);
            if (minCapacity > target.keys.Capacity) {
                // CopyTo will always set the target's capacity to the same
                // as the source. We don't ever want it to become less that
                // the target table's row capacity.
                // TODO: This is potentially expensive. It would be better to
                // add version of CopyTo that only grows the capacity if
                // needed, but never shrinks it.
                target.keys.Capacity = minCapacity;
            }
            values.CopyTo(target.values);

            // Allow for combinations of deleted indices.
            if (index != null) {
                if (target.index == null) {
                    target.index = new StringIndex(index.Count);
                }
                index.CopyTo(target.index);
            } else {
                target.RebuildIndex(deletedIndexCount);
            }
        }

        public void DeleteIndex()
        {
            int distinct = index.Count;
            values.Capacity = distinct;
            deletedIndexCount = distinct;
            index = null;
        }

        public KeyIndex GetIndex(int count, KeyIndex index)
        {
            if (index == null) {
                index = new KeyIndex(count);
            }

            index.Keys = keys.CopyTo(index.Keys);
            
            index.Count = count;
            index.MaxKey = index.Keys.GetMax(count);
            if (index.MaxKey + 1 < count) {
                index.IsDistinct = false;
            } else {
                index.IsDistinct = null;
            }
            return index;
        }

        public KeyIndex GetIndex(StorageInt records, int count, KeyIndex index)
        {
            if (index == null) {
                index = new KeyIndex(count);
            }

            index.Keys = keys.CopyTo(records, count, index.Keys);

            index.Count = count;
            index.MaxKey = index.Keys.GetMax(count);
            if (index.MaxKey + 1 < count) {
                index.IsDistinct = false;
            } else {
                index.IsDistinct = null;
            }
            return index;
        }

        public bool IsEfficient(int records)
        {
            return index.Count <= EfficientRatio * records;
        }

        private void RebuildIndex(int count)
        {
            index = new StringIndex(values, count);
        }

        public override string this[int record]
        {
            get { return values[keys[record]]; }
            set {
                int key;
                if (index.GetKey(value, out key)) {
                    if (key >= values.Capacity) {
                        values.Capacity *= 2;
                    }
                    values[key] = value;
                }
                keys[record] = key;
            }
        }

        // TODO: Can definitely be faster, e.g. bail after a set number
        // of rows if not benefitting from indexing.
        public static IndexedTextStorage TryCompress(
            StoragePointer<string> flat, int records)
        {
            IndexedTextStorage indexed = Compress(flat, records);
            if (indexed.IsEfficient(records)) {
                return indexed;
            }
            return null;
        }

        public StoragePointer<string> Unpack(int records)
        {
            int capacity = keys.Capacity;
            StoragePointer<string> flat = new StoragePointer<string>(capacity);
            for (int i = 0; i < records; i++) {
                flat[i] = values[keys[i]];
            }
            return flat;
        }
    }
}
