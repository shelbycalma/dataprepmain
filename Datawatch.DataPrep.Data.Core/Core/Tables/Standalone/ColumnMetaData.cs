﻿using System;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// The default abstract base class for column meta data implementations.
    /// </summary>
    public abstract class ColumnMetaData : IMutableColumnMetaData
    {
        private readonly IColumn column;
        private string title;
        private bool hidden = false;

        protected ColumnMetaData(IColumn column)
        {
            if (column == null) throw Exceptions.ArgumentNull("column");
            this.column = column;
        }

        public abstract Type DataType { get; }

        /// <summary>
        /// Gets a boolean value indicating whether 
        /// this column is hidden or not.
        /// <remarks>
        /// A hidden column will not be displayed in any 
        /// listing of columns provided by SDK controls.
        /// </remarks>
        /// </summary>
        public bool Hidden
        {
            get { return hidden; }

            set { hidden = value; }
        }

        /// <summary>
        /// Gets or sets the column's title.
        /// </summary>
        /// <remarks>
        /// <para>If this property is set to null it will return the column's
        /// name when read.</para>
        /// </remarks>
        public string Title
        {
            get { return title != null ? title : column.Name; }

            set
            {
                title = value;
            }
        }
    }
}
