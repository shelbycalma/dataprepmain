﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A default meta data implementation for time columns extending 
    /// <see cref="ColumnMetaData"/> and implementing the 
    /// <see cref="IMutableTimeColumnMetaData"/> interface.
    /// </summary>
    public class TimeColumnMetaData : ColumnMetaData,
        IMutableTimeColumnMetaData,
        IDistinctValuesMetaData<DateTime>
    {
        private readonly ITimeColumn timeColumn;

        private TimeValueInterval domain;
        private string format;

        public TimeColumnMetaData(IColumn column) : base(column)
        {
            timeColumn = column as ITimeColumn;
        }

        public override Type DataType
        {
            get { return typeof (DateTime); }
        }

        /// <summary>
        /// Gets or sets the time domain as a 
        /// <see cref="TimeValueInterval"/> for this column.
        /// </summary>
        public TimeValueInterval Domain
        {
            get { return domain; }
            set { domain = value; }
        }

        /// <summary>
        /// Gets the <see cref="TimeColumn"/> format string.
        /// </summary>
        public string Format
        {
            get { return format; }
            set { format = value; }
        }

        public IDistinctValues<DateTime> GetDistinctValues(
            bool excludeNulls, int maxRowCount)
        {
            if (timeColumn == null || maxRowCount == 0) {
                return new DistinctValueList<DateTime>();
            }

            bool rowLimited = maxRowCount >= 0;
            int initialCapacity = rowLimited ? maxRowCount : 1000;
            List<IValueKey<DateTime>> valueList =
                new List<IValueKey<DateTime>>(initialCapacity);
            Dictionary<IValueKey<DateTime>, int> valueCounts =
                new Dictionary<IValueKey<DateTime>, int>(initialCapacity);

            int distinctRows = 0;
            ITable table = timeColumn.Table;
            for (int row = 0; row < table.RowCount; row++)
            {
                IValueKey<DateTime> key = new TimeValueKey(
                    timeColumn.GetTimeValue(row));

                if (excludeNulls && key.IsEmpty) {
                    continue;
                }

                int count;
                if (valueCounts.TryGetValue(key, out count)) {
                    valueCounts[key] = count + 1;
                }
                else {
                    valueCounts[key] = 1;
                    if (rowLimited && distinctRows < maxRowCount) {
                        valueList.Add(key);
                        distinctRows++;
                    }
                }
            }

            DistinctValueList<DateTime> distinctValueList =
                new DistinctValueList<DateTime>(valueList.Count);
            foreach (IValueKey<DateTime> key in valueList) {
                DateTime value = key.Value;
                int count = valueCounts[key];
                distinctValueList.Add(
                    new DistinctValue<DateTime>(value, count));
            }
            return distinctValueList;
        }
    }
}
