﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A default meta data implementation for text columns extending 
    /// <see cref="ColumnMetaData"/> and implementing the 
    /// <see cref="IMutableTextColumnMetaData"/> interface.
    /// </summary>
    public class TextColumnMetaData : ColumnMetaData,
        IMutableTextColumnMetaData,
        IDistinctValuesMetaData<string>
    {
        private readonly ITextColumn textColumn;

        private double cardinality;
        private TextDomain metaDomain;
        private IComparer<string> comparer;

        public TextColumnMetaData(IColumn column) : base(column)
        {
            textColumn = column as ITextColumn;

            comparer = StringComparer.CurrentCultureIgnoreCase;
        }

        public override Type DataType
        {
            get { return typeof (string); }
        }

        /// <summary>
        /// Gets or set the cardinality of this <see cref="TextColumnMetaData"/>.
        /// </summary>
        public double Cardinality
        {
            get { return cardinality; }

            set { cardinality = value; }
        }

        /// <summary>
        /// Gets or sets the comparer of this <see cref="TextColumnMetaData"/>.
        /// </summary>
        public IComparer<string> Comparer
        {
            get { return comparer; }

            set{ comparer = value;}
        }

        /// <summary>
        /// Gets or sets the domain of this <see cref="TextColumnMetaData"/>.
        /// </summary>
        public string[] Domain
        {
            get
            {
                if (metaDomain != null)
                {
                    return metaDomain.Values;
                }
                return null;
            }

            set
            {
                if (value != null)
                {
                    metaDomain = new TextDomain(value);
                }
                else
                {
                    metaDomain = null;
                }
            }
        }

        public IDistinctValues<string> GetDistinctValues(
            bool excludeNulls, int maxRowCount)
        {
            if (textColumn == null || maxRowCount == 0) {
                return new DistinctValueList<string>();
            }

            bool rowLimited = maxRowCount >= 0;
            int initialCapacity = rowLimited ? maxRowCount : 1000;
            List<IValueKey<string>> valueList =
                new List<IValueKey<string>>(initialCapacity);
            Dictionary<IValueKey<string>, int> valueCounts =
                new Dictionary<IValueKey<string>, int>(initialCapacity);

            int distinctRows = 0;
            ITable table = textColumn.Table;
            for (int row = 0; row < table.RowCount; row++)
            {
                IValueKey<string> key = new TextValueKey(
                    textColumn.GetTextValue(row));

                if (excludeNulls && key.IsEmpty) {
                    continue;
                }

                int count;
                if (valueCounts.TryGetValue(key, out count)) {
                    valueCounts[key] = count + 1;
                }
                else {
                    valueCounts[key] = 1;
                    if (rowLimited && distinctRows < maxRowCount) {
                        valueList.Add(key);
                        distinctRows++;
                    }
                }
            }

            DistinctValueList<string> distinctValueList =
                new DistinctValueList<string>(valueList.Count);
            foreach (IValueKey<string> key in valueList) {
                string value = key.Value;
                int count = valueCounts[key];
                distinctValueList.Add(new DistinctValue<string>(value, count));
            }
            return distinctValueList;
        }
    }
}
