using System;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A class representing time.
    /// </summary>
    [DebuggerDisplay("{record}:{time}")]
    public class Time : ITime
    {
        private readonly DateTime time;
        internal int record;

        internal Time(DateTime time, int record)
        {
            this.time = time;
            this.record = record;
        }

        /// <summary>
        /// The <see cref="DateTime"/> value of this <see cref="Time"/>.
        /// </summary>
        public DateTime DateTime
        {
            get { return time; }
        }

        /// <summary>
        /// This property is <c>true</c> if the <see cref="Time"/> has been
        /// removed from the <see cref="StandaloneTable"/>.
        /// </summary>
        /// <remarks>
        /// <para>It is an error to use a <see cref="Time"/> after you have
        /// removed it.</para>
        /// </remarks>
        public bool IsRemoved
        {
            get { return record < 0; }
        }

        DateTime ITime.Time
        {
            get { return time; }
        }
    }
}
