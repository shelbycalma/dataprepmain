﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A default meta data implementation for numeric columns extending 
    /// <see cref="ColumnMetaData"/> and implementing the 
    /// <see cref="IMutableNumericColumnMetaData"/> interface.
    /// </summary>
    public class NumericColumnMetaData : ColumnMetaData,
        IMutableNumericColumnMetaData,
        IDistinctValuesMetaData<double>
    {
        private readonly INumericColumn numericColumn;

        private NumericInterval domain;
        private string format;
        private double mean = NumericValue.Empty;
        private double metaStandardDeviation = NumericValue.Empty;

        public NumericColumnMetaData(IColumn column) : base(column)
        {
            numericColumn = column as INumericColumn;
        }

        /// <summary>
        /// Gets the the <see cref="Type">data type</see> of values
        /// stored in this <see cref="NumericColumn"/>.
        /// </summary>
        public override Type DataType
        {
            get { return typeof(double); }
        }

        /// <summary>
        /// Gets or sets the mean of this <see cref="NumericColumnMetaData"/>.
        /// </summary>
        public double Mean
        {
            get { return mean; }
            set { mean = value; }
        }

        /// <summary>
        /// Gets or sets the domain of this <see cref="NumericColumnMetaData"/>.
        /// </summary>
        public NumericInterval Domain
        {
            get { return domain; }
            set { domain = value; }
        }

        /// <summary>
        /// Gets the format of this <see cref="NumericColumnMetaData"/>.
        /// </summary>
        public string Format
        {
            get { return format; }
            set { format = value; }
        }

        /// <summary>
        /// Gets or sets the standard deviation of this <see cref="NumericColumnMetaData"/>.
        /// </summary>
        public double StandardDeviation
        {
            get { return metaStandardDeviation; }
            set { metaStandardDeviation = value; }
        }

        public IDistinctValues<double> GetDistinctValues(
            bool excludeNulls, int maxRowCount)
        {
            if (numericColumn == null || maxRowCount == 0) {
                return new DistinctValueList<double>();
            }

            bool rowLimited = maxRowCount >= 0;
            int initialCapacity = rowLimited ? maxRowCount : 1000;
            List<IValueKey<double>> valueList =
                new List<IValueKey<double>>(initialCapacity);
            Dictionary<IValueKey<double>, int> valueCounts =
                new Dictionary<IValueKey<double>, int>(initialCapacity);

            int distinctRows = 0;
            ITable table = numericColumn.Table;
            for (int row = 0; row < table.RowCount; row++)
            {
                IValueKey<double> key = new NumericValueKey(
                    numericColumn.GetNumericValue(row));

                if (excludeNulls && key.IsEmpty) {
                    continue;
                }

                int count;
                if (valueCounts.TryGetValue(key, out count)) {
                    valueCounts[key] = count + 1;
                }
                else {
                    valueCounts[key] = 1;
                    if (rowLimited && distinctRows < maxRowCount) {
                        valueList.Add(key);
                        distinctRows++;
                    }
                }
            }

            DistinctValueList<double> distinctValueList =
                new DistinctValueList<double>(valueList.Count);
            foreach (IValueKey<double> key in valueList) {
                double value = key.Value;
                int count = valueCounts[key];
                distinctValueList.Add(new DistinctValue<double>(value, count));
            }
            return distinctValueList;
        }
    }
}
