using System;
using System.Collections;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A collection of <see cref="Column">columns</see>.
    /// </summary>
    /// <remarks>
    /// <para>You cannot create instances of this class externally. It is used
    /// by the <see cref="StandaloneTable"/> in its
    /// <see cref="StandaloneTable.Columns"/> property.</para>
    /// </remarks>
    public class ColumnCollection : IEnumerable<Column>
    {
        private readonly StandaloneTable table;

        internal ColumnCollection(StandaloneTable table)
        {
            this.table = table;
        }

        /// <summary>
        /// Adds a <see cref="Column"/> to the collection.
        /// </summary>
        /// <param name="column">The <see cref="Column"/> to add.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.AddColumn(Column)")]
        public void Add(Column column)
        {
            table.AddColumn(column);
        }

        /// <summary>
        /// Checks if a column with the specified name exists.
        /// </summary>
        /// <param name="name">Name of the column.</param>
        /// <returns>True if there is a column with the name in the
        /// collection.</returns>
        [Obsolete("Replaced by StandaloneTable.ContainsColumn(string)")]
        public bool Contains(string name)
        {
            return table.ContainsColumn(name);
        }

        /// <summary>
        /// Gets the number of columns currently in the collection.
        /// </summary>
        [Obsolete("Replaced by StandaloneTable.ColumnCount")]
        public int Count
        {
            get { return table.ColumnCount; }
        }

        /// <summary>
        /// Returns the generic <see cref="ColumnCollection"/> enumerator.
        /// </summary>
        /// <returns>Generic enumerator.</returns>
        [Obsolete("Use StandaloneTable.ColumnCount with " +
            "StandaloneTable.GetColumn(int) instead.")]
        public IEnumerator<Column> GetEnumerator()
        {
            return table.LegacyGetColumnEnumerator();
        }

        /// <summary>
        /// Returns the <see cref="ColumnCollection"/> enumerator.
        /// </summary>
        /// <returns>Enumerator</returns>
        [Obsolete("Use StandaloneTable.ColumnCount with " +
            "StandaloneTable.GetColumn(int) instead.")]
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes a <see cref="Column"/> from the collection.
        /// </summary>
        /// <param name="column">The <see cref="Column"/> to remove.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.RemoveColumn(Column)")]
        public void Remove(Column column)
        {
            table.RemoveColumn(column);
        }

        /// <summary>
        /// Gets the column at the specified index.
        /// </summary>
        /// <param name="index">Index of column to retrieve.</param>
        /// <returns>The column at the index.</returns>
        [Obsolete("Replaced by StandaloneTable.GetColumn(int)")]
        public Column this[int index]
        {
            get { return table.GetColumn(index); }
        }

        /// <summary>
        /// Returns a <see cref="Column"/> given its name.
        /// </summary>
        /// <param name="name">Name of the <see cref="Column"/> to retrieve.</param>
        /// <returns>The <see cref="Column"/> with the specified name.</returns>
        [Obsolete("Replaced by StandaloneTable.GetColumn(name)")]
        public Column this[string name]
        {
            get { return table.GetColumn(name); }
        }
    }
}
