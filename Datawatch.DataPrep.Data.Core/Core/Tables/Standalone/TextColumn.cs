using System;
using System.Collections.Generic;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A <see cref="StandaloneTable"/> column of type <see cref="string"/>.
    /// </summary>
    // TODO: Faster to keep two references to the storage, cast as
    // flat and indexed?
    public sealed class TextColumn : Column,
        ITextColumn,
        IIndexedColumn
    {
        private CompressionMode compression;
        private StorageBase<string> storage;
        private bool isKey;
        
        /// <summary>
        /// Creates a new column with the specified name.
        /// </summary>
        /// <param name="name">The name (identifier) of the column.</param>
        public TextColumn(string name) : base(name)
        {
            compression = CompressionMode.Automatic;
            MetaData = new TextColumnMetaData(this);
        }

        internal TextColumn(string name, IndexedTextStorage storage) :
            base(name)
        {
            compression = CompressionMode.Always;
            this.storage = storage;
            MetaData = new TextColumnMetaData(this);
        }

        bool IIndexedColumn.CanGetIndex
        {
            get { return true; }
        }

        // TODO: We COULD care about IsKey in here, but it's kind of cool to
        // be able to set IsKey even if there are duplicate values.
        internal override void CheckStorage(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);

            StoragePointer<string> flat;
            if (compression == CompressionMode.Always) {
                flat = storage as StoragePointer<string>;
                if (flat != null) {
                    storage = IndexedTextStorage.Compress(flat, rows);
                }
                return;
            }
            IndexedTextStorage indexed;
            if (compression == CompressionMode.Never) {
                indexed = storage as IndexedTextStorage;
                if (indexed != null) {
                    storage = indexed.Unpack(rows);
                }
                return;
            }
            indexed = storage as IndexedTextStorage;
            if (indexed != null) {
                if (!indexed.IsEfficient(rows)) {
                    storage = indexed.Unpack(rows);
                }
                return;
            }
            flat = (StoragePointer<string>) storage;
            indexed = IndexedTextStorage.TryCompress(flat, rows);
            if (indexed != null) {
                storage = indexed;
            }
        }

        protected internal override void ClearRow(int row)
        {
            IndexedTextStorage indexed = storage as IndexedTextStorage;
            if (indexed != null) {
                indexed.Clear(row);
            } else {
                ((StoragePointer<string>) storage)[row] = null;
            }
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= storage.Capacity);

            IndexedTextStorage indexed = storage as IndexedTextStorage;
            if (indexed != null) {
                indexed.Clear(0, rows);
            } else {
                ((StoragePointer<string>) storage).Clear(0, rows);
            }
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= storage.Capacity);

            storage.Capacity = rows;
        }

        /// <summary>
        /// Copies the contents of this column into another column.
        /// </summary>
        /// <param name="target">The column to copy to (from the same table or
        /// from a table with the same row count).</param>
        /// <remarks>
        /// <para>This forces the target column to have the same storage mode
        /// (compression) as this column.</para>
        /// </remarks>
        internal void CopyTo(TextColumn target)
        {
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            Debug.Assert(target != this);
            Debug.Assert(table != null);
            Debug.Assert(target.table != null);
            Debug.Assert(target.table.IsUpdating);

            target.compression = compression;
            target.isKey = isKey;

            int minCapacity = target.RowCapacity;
            Debug.Assert(minCapacity >= table.RowCount);

            IndexedTextStorage sourceIndexed =
                storage as IndexedTextStorage;
            if (sourceIndexed != null) {
                IndexedTextStorage targetIndexed =
                    target.storage as IndexedTextStorage;
                if (targetIndexed == null) {
                    target.storage = targetIndexed =
                        new IndexedTextStorage(minCapacity);
                }
                sourceIndexed.CopyTo(targetIndexed, minCapacity);
            } else {
                StoragePointer<string> sourceFlat =
                    (StoragePointer<string>) storage;
                StoragePointer<string> targetFlat =
                    target.storage as StoragePointer<string>;
                if (targetFlat == null) {
                    target.storage = targetFlat =
                        new StoragePointer<string>(minCapacity);
                }
                sourceFlat.CopyTo(targetFlat);
                if (minCapacity > targetFlat.Capacity) {
                    // TODO: See comment in IndexedTextStorage.CopyTo.
                    targetFlat.Capacity = minCapacity;
                }
            }
        }

        internal override void DefragmentRows(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(holes[holes.Length - 1] <= storage.Capacity);

            storage.Compact(holes);
        }

        /// <summary>
        /// Gets or sets the compression strategy for the column.
        /// </summary>
        public CompressionMode Compression
        {
            get { return compression; }
            
            [ModifiesTable] set {
                Checks.CheckTableNullOrUpdating(this);                
                if (value != compression) {
                    compression = value;
                    if (table != null) {
                        CheckStorage(table.RowCount);
                    }
                }
            }
        }
        
        protected internal override void Freeze()
        {
            IndexedTextStorage indexed = storage as IndexedTextStorage;
            if (indexed != null) {
                indexed.DeleteIndex();
            }
        }

        /// <summary>
        /// Returns the domain of the <see cref="TextColumn"/>.
        /// </summary>
        /// <returns>The domain of the column in the form 
        /// of an array of strings.</returns>
        /// <remarks>
        /// <para>This is the actual domain, based on the current data in the
        /// table. In contrast, the <see cref="TextColumnMetaData.Domain"/>
        /// property gets or sets the column's possible values.</para>
        /// <para>If the table has no rows, an empty array is returned. If the
        /// <see cref="TextColumnMetaData.Comparer"/> property is not null, the
        /// values returned will be sorted using it. If there are null values
        /// in the column, a null entry will be included in the returned
        /// result.</para>
        /// <para>This method will go through all the rows in the table and
        /// calculate the domain based the values that are currently in there,
        /// so it is a slow operation and should be avoided if possible.</para>
        /// </remarks>
        public string[] GetCurrentDomain()
        {
            HashSet<string> domain = new HashSet<string>();
            for (int row = 0; row < table.RowCount; row++)
            {
                domain.Add(storage[row]);
            }
            int count = domain.Count;
            string[] array = new string[count];
            domain.CopyTo(array, 0);
            if (MetaData is ITextColumnMetaData)
            {
                IComparer<string> comparer = 
                    ((ITextColumnMetaData) MetaData).Comparer;
                // Null automatically last in array.
                if (comparer != null)
                {
                    Array.Sort<string>(array, comparer);
                }
            }
            return array;
        }

        KeyIndex IIndexedColumn.GetIndex(KeyIndex index)
        {
            Debug.Assert(table != null);

            IndexedTextStorage indexed = storage as IndexedTextStorage;
            if (indexed != null) {
                return indexed.GetIndex(table.RowCount, index);
            } else {
                StoragePointer<string> flat = (StoragePointer<string>) storage;
                return StorageInt.GetIndex(flat, table.RowCount, index);
            }
        }

        KeyIndex IIndexedColumn.GetIndex(
            StorageInt rows, int count, KeyIndex index)
        {
            Debug.Assert(table != null);

            IndexedTextStorage indexed = storage as IndexedTextStorage;
            if (indexed != null) {
                return indexed.GetIndex(rows, count, index);
            } else {
                StoragePointer<string> flat = (StoragePointer<string>) storage;
                return StorageInt.GetIndex(rows, flat, count, index);
            }
        }

        /// <summary>
        /// Returns the <see cref="string">string</see> value of this 
        /// <see cref="TextColumn"/> 
        /// for a specific <see cref="Row"/>.
        /// </summary>
        /// <param name="row"><see cref="Row"/> to get the 
        /// value for.</param>
        /// <returns><see cref="string">String</see> value.</returns>
        public string GetTextValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);

            return storage[row.record];
        }

        string ITextColumn.GetTextValue(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);

            return storage[((Row) row).record];
        }

        public string GetTextValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);

            return storage[row];
        }

        public bool IsIndexed
        {
            get { return storage is IndexedTextStorage; }
        }

        public bool IsKey
        {
            get { return isKey; }
            
            [ModifiesTable] set {
                Checks.CheckTableNullOrUpdating(this);

                isKey = value;
            }
        }

        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);

            if (storage != null) {
                // Already initialized through internal constructor.
                return;
            }
            if (compression == CompressionMode.Never) {
                storage = new StoragePointer<string>(table.RowCapacity);
            } else {
                storage = new IndexedTextStorage(table.RowCapacity);
            }
        }
        
        /// <summary>
        /// Clears this <see cref="TextColumn"/>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
            storage = null;
        }

        protected internal override int RowCapacity
        {
            get {
                Debug.Assert(table != null);
                return storage.Capacity;
            }
            set {
                Debug.Assert(table != null);
                int oldCapacity = storage.Capacity;
                storage.Capacity = value;
                StoragePointer<string> flat = storage as StoragePointer<string>;
                if (flat != null && value > oldCapacity) {
                    flat.Clear(oldCapacity, value - oldCapacity);
                }
            }
        }

        /// <summary>
        /// Sets the text value of this column for a particular 
        /// <see cref="Row"/>.
        /// </summary>
        /// <param name="row">Row to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetTextValue(Row row, string value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckNotFrozen(table);
            Checks.CheckRow(table, row);

            storage[row.record] = value;
        }

        /// <summary>
        /// Sets the text value of this column for a particular row.
        /// </summary>
        /// <param name="row">Row index to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetTextValue(int row, string value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckNotFrozen(table);
            Checks.CheckRowIndex(table, row);

            storage[row] = value;
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            Checks.CheckNotFrozen(table);
           
            string text = value != null ? value.ToString() : null;
            
            storage[row] = text;
        }
    }
}
