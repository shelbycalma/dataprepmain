using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A <see cref="StandaloneTable"/> column of type <see cref="string"/>
    /// based on a <see cref="TimeColumn"/>, where
    /// <see cref="TimePart">time parts</see> corresponds to
    /// text values (so called buckets). Bucketing is also known as binning or
    /// discretization.
    /// </summary>
    public class TimeBucketsTextColumn : Column, ITextColumn
    {
        private readonly TimeColumn column;
        private readonly TimePart timePart;

        private readonly DateTimeFormatInfo formatInfo;

        private readonly Dictionary<string, PartValueInfo> infoLookup =
            new Dictionary<string, PartValueInfo>();
        private int nonMissing;

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeBucketsTextColumn"/>
        /// class.
        /// </summary>
        /// <param name="name">the name of the column</param>
        /// <param name="column">the source time column</param>
        /// <param name="timePart">the time part defining the buckets</param>
        public TimeBucketsTextColumn(string name, TimeColumn column,
            TimePart timePart)
            : this(name, column, timePart, null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TimeBucketsTextColumn"/>
        /// class.
        /// </summary>
        /// <param name="name">the name of the column</param>
        /// <param name="column">the source time column</param>
        /// <param name="timePart">the time part defining the buckets</param>
        /// <param name="formatInfo">The date format info to
        /// use when formatting the text values (can be
        /// null).</param>
        public TimeBucketsTextColumn(string name, TimeColumn column,
            TimePart timePart, DateTimeFormatInfo formatInfo)
            : base(name)
        {
            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }

            this.column = column;
            this.timePart = timePart;
            this.formatInfo = formatInfo != null ?
                formatInfo : DateTimeFormatInfo.CurrentInfo;

            MetaData = 
                new TextColumnMetaData(this)
                {
                    Comparer = new TimeBucketComparer(infoLookup)           
                };
        }

        protected internal override void ClearRow(int row)
        {
            Debug.Assert(table != null);
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
        }

        /// <summary>
        /// Returns the domain of the column in the form 
        /// of an array of strings.
        /// </summary>
        /// <returns>The domain of the column in the form 
        /// of an array of strings.</returns>
        public string[] GetCurrentDomain()
        {
            int missing = table.RowCount - nonMissing > 0 ? 1 : 0;
            string[] array = new string[infoLookup.Count + missing];
            infoLookup.Keys.CopyTo(array, 0);

            if (MetaData is ITextColumnMetaData)
            {
                IComparer<string> comparer = 
                    ((ITextColumnMetaData) MetaData).Comparer;
                if (comparer != null)
                {
                    Array.Sort(array, comparer);                    
                }
            }

            return array;
        }

        private int GetPartValue(DateTime value)
        {
            switch (timePart) {
                case TimePart.Decade:
				    return 10 * (value.Year / 10);
			    case TimePart.Year:
				    return value.Year;
			    case TimePart.Quarter:
				    return ((value.Month - 1) / 3) + 1;
			    case TimePart.Month:
				    return value.Month;
			    case TimePart.Weekday:
				    return (int) value.DayOfWeek;
                case TimePart.Day:
                    return value.Day;
                case TimePart.Hour:
                    return value.Hour;
                case TimePart.Minute:
                    return value.Minute;
                case TimePart.Second:
                    return value.Second;
                case TimePart.Millisecond:
                    return value.Millisecond;
			    default:
				    throw new NotImplementedException();
            }
        }

        private string GetTextValue(DateTime value)
        {
            if (TimeValue.IsEmpty(value)) {
                return null;
            }
            int partValue = GetPartValue(value);
            switch (timePart) {
			    case TimePart.Quarter:
				    return "Q" + partValue;
			    case TimePart.Month:
				    return formatInfo.GetMonthName(partValue);
			    case TimePart.Weekday:
				    return formatInfo.GetDayName((DayOfWeek) partValue);
			    default:
				    return partValue.ToString();
            }
        }

        /// <summary>
        /// Returns the value of the <see cref="TimeBucketsTextColumn"/> 
        /// for a particular <see cref="Row"/>.
        /// </summary>
        /// <param name="row"><see cref="Row"/> to return the value for.</param>
        /// <returns>String value.</returns>
        public string GetTextValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);

            DateTime value = column.GetTimeValue(row);

            return GetTextValue(value);
        }

        string ITextColumn.GetTextValue(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);

            DateTime value = column.GetTimeValue((Row) row);
            
            return GetTextValue(value);
        }

        public string GetTextValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);

            DateTime value = column.GetTimeValue(row);

            return GetTextValue(value);
        }

        /// <summary>
        /// Gets the time column on which the column is based.
        /// </summary>
        public TimeColumn TimeColumn
        {
            get { return column; }
        }

        /// <summary>
        /// Gets the time part defining the buckets.
        /// </summary>
        public TimePart TimePart
        {
            get { return timePart; }
        }

        private bool UpdateInfoLookup(string textBefore, Row row)
        {
            string textAfter = GetTextValue(row);
            if (textAfter == textBefore) {
                return false;
            }
            if (textBefore != null) {
                if (textAfter == null) {
                    nonMissing--;
                }
                PartValueInfo infoBefore = infoLookup[textBefore];
                if (infoBefore.Count > 1) {
                    infoBefore.Count--;
                } else {
                    infoLookup.Remove(textBefore);
                }
            }
            if (textAfter != null) {
                if (textBefore == null) {
                    nonMissing++;
                }
                if (infoLookup.ContainsKey(textAfter)) {
                    infoLookup[textAfter].Count++;
                } else {
                    DateTime valueAfter = column.GetTimeValue(row);
                    infoLookup.Add(textAfter,
                        new PartValueInfo(GetPartValue(valueAfter)));
                }
            }
            return true;
        }

        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
            PopulateInfoLookup();
        }

        /// <summary>
        /// Clears this <see cref="TimeBucketsTextColumn"/>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
            nonMissing = 0;
            infoLookup.Clear();
        }

        private void PopulateInfoLookup()
        {
            nonMissing = 0;
            infoLookup.Clear();
            int rows = table.RowCount;
            for (int row = 0; row < rows; row++) {
                string textValue = GetTextValue(row);
                if (textValue == null) {
                    continue;
                }
                nonMissing++;
                if (!infoLookup.ContainsKey(textValue)) {
                    int partValue = GetPartValue(column.GetTimeValue(row));
                    infoLookup.Add(textValue, new PartValueInfo(partValue));
                } else {
                    infoLookup[textValue].Count++;
                }
            }
        }

        /// <summary>
        /// Overridden to update internal lookup tables at end of update.
        /// </summary>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        protected internal override void Recalculate()
        {
            PopulateInfoLookup();
        }

        protected internal override int RowCapacity
        {
            get { return 0; }
            set { }
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            throw new NotSupportedException();
        }


        private class TimeBucketComparer : IComparer<string>
        {
            private readonly Dictionary<string, PartValueInfo> infoLookup;

            public TimeBucketComparer(
                Dictionary<string, PartValueInfo> infoLookup)
            {
                this.infoLookup = infoLookup;
            }

            public int Compare(string x, string y)
            {
                int xIndex = x != null ? infoLookup[x].PartValue  : int.MinValue;
                int yIndex = y != null ? infoLookup[y].PartValue  : int.MinValue;

                if (xIndex < yIndex) {
                    return -1;
                }
                if (xIndex > yIndex) {
                    return 1;
                }
                return 0;
            }
        }

        private class PartValueInfo
        {
            public readonly int PartValue;
            public int Count;

            public PartValueInfo(int partValue) {
                this.PartValue = partValue;
                this.Count = 1;
            }
        }
    }
}
