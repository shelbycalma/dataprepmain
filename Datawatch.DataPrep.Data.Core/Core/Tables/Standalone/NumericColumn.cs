using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A <see cref="StandaloneTable"/> column of type <see cref="double"/>.
    /// </summary>
    // TODO: Would seal this but EX's ServerCalculatedColumn subclasses...
    public class NumericColumn : Column, INumericColumn
    {
        private StorageDouble storage;

        /// <summary>
        /// Creates a new column with the specified name.
        /// </summary>
        /// <param name="name">The name (identifier) of the column.</param>
        public NumericColumn(string name) : base(name)
        {
            MetaData = new NumericColumnMetaData(this);
        }

        protected internal override void ClearRow(int row)
        {
            Debug.Assert(table != null);

            storage[row] = NumericValue.Empty;
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= storage.Capacity);

            storage.Clear(0, rows, NumericValue.Empty);
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= storage.Capacity);

            storage.Capacity = rows;
        }

        /// <summary>
        /// Copies the contents of this column into another column.
        /// </summary>
        /// <param name="target">The column to copy to (from the same table or
        /// from a table with the same row count).</param>
        internal void CopyTo(NumericColumn target)
        {
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            Debug.Assert(target != this);
            Debug.Assert(table != null);
            Debug.Assert(target.table != null);
            Debug.Assert(target.table.IsUpdating);

            int minCapacity = target.RowCapacity;
            Debug.Assert(minCapacity >= table.RowCount);

            storage.CopyTo(target.storage);
            if (minCapacity > target.storage.Capacity) {
                // TODO: See comment in IndexedTextStorage.CopyTo.
                target.storage.Capacity = minCapacity;
            }
        }

        internal override void DefragmentRows(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(holes[holes.Length - 1] <= storage.Capacity);

            storage.Compact(holes);
        }

        /// <summary>
        /// Returns the <see cref="double">numeric value</see> of this 
        /// <see cref="NumericColumn"/> 
        /// for a specific <see cref="Row"/>.
        /// </summary>
        /// <param name="row"><see cref="Row"/> to get the 
        /// value for.</param>
        /// <returns><see cref="double">Numeric value.</see></returns>
        public double GetNumericValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);

            return storage[row.record];
        }

        double INumericColumn.GetNumericValue(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);

            return storage[((Row) row).record];
        }

        public double GetNumericValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);

            return storage[row];
        }

        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);

            storage = new StorageDouble(table.RowCapacity);
            storage.Clear(NumericValue.Empty);
        }

        /// <summary>
        /// Clears this <see cref="NumericColumn"/> when it is removed from its
        /// <see cref="ITable">table</see>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
            storage = null;
        }

        protected internal override int RowCapacity
        {
            get {
                Debug.Assert(table != null);
                return storage.Capacity;
            }
            set {
                Debug.Assert(table != null);
                int oldCapacity = storage.Capacity;
                storage.Capacity = value;
                if (value > oldCapacity) {
                    storage.Clear(oldCapacity, value - oldCapacity,
                        NumericValue.Empty);
                }
            }
        }

        /// <summary>
        /// Sets the numeric value of this column for a particular
        /// <see cref="Row"/>.
        /// </summary>
        /// <param name="row">Row to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetNumericValue(Row row, double value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);

            storage[row.record] = value;
        }

        /// <summary>
        /// Sets the numeric value of this column for a particular
        /// <see cref="Row"/>.
        /// </summary>
        /// <param name="row">Row index to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetNumericValue(int row, double value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);

            storage[row] = value;
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            double numeric = NumericValue.FromObject(value);

            storage[row] = numeric;
        }
    }
}
