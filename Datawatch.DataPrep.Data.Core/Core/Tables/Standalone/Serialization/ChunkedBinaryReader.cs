﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    /// <summary>
    /// Stream reader that can read (and skip) chunks with a size preamble.
    /// </summary>
    internal class ChunkedBinaryReader : BinaryReader
    {
        /// <summary>
        /// Open chunks that still haven't been closed.
        /// </summary>
        private readonly Stack<BinaryChunkInfo> chunks;

        /// <summary>
        /// Creates a new reader on the specified stream.
        /// </summary>
        /// <param name="input">The stream to read from (must support
        /// seeking).</param>
        /// <param name="encoding">The encoding that was used to write
        /// strings.</param>
        public ChunkedBinaryReader(Stream input, Encoding encoding) :
            base(input, encoding)
        {
            if (!input.CanRead) {
                throw Exceptions.SerializerStreamCannotRead();
            }
            if (!input.CanSeek) {
                throw Exceptions.SerializerStreamCannotSeek();
            }
            chunks = new Stack<BinaryChunkInfo>();
        }

        /// <summary>
        /// Start reading a new chunk (call <see cref="EndChunk()"/> to
        /// close it).
        /// </summary>
        /// <remarks>
        /// <para>This call reads the chunk's size preamble from the stream,
        /// and ensures that the stream is positioned just after the chunk
        /// when it is closed.</para>
        /// </remarks>
        public void BeginChunk()
        {
            BinaryChunkInfo chunk = new BinaryChunkInfo();
            // Save the current position (where header starts).
            chunk.HeaderPosition = this.BaseStream.Seek(0, SeekOrigin.Current);
            // Read the chunk size.
            chunk.ByteCount = ReadInt32();
            if (chunk.ByteCount < 0) {
                throw Exceptions.SerializerBadChunkSize(chunk.ByteCount);
            }
            // Save the start position of the chunk data itself.
            chunk.StartPosition = this.BaseStream.Seek(0, SeekOrigin.Current);
            // Calculate the end position (first byte after chunk).
            chunk.EndPosition = chunk.StartPosition + chunk.ByteCount;
            chunks.Push(chunk);
        }

        /// <summary>
        /// Closes the reader and underlying stream (an exception will be
        /// thrown if there are still open chunks).
        /// </summary>
        public override void Close()
        {
            if (chunks.Count > 0) {
                throw Exceptions.SerializerOpenChunksOnClose();
            }
            base.Close();
        }

        /// <summary>
        /// Checks for open chunks, then disposes the underlying stream.
        /// </summary>
        /// <param name="disposing">True to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (chunks.Count > 0) {
                    // Ok to throw, not called from finalizer.
                    throw Exceptions.SerializerOpenChunksOnDispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Closes the most recently opened chunk and moves to the first
        /// position after it in the stream.
        /// </summary>
        public void EndChunk()
        {
            if (chunks.Count == 0) {
                throw Exceptions.SerializerNoOpenChunks();
            }
            BinaryChunkInfo chunk = chunks.Pop();
            // Record current position (to calculate seek offset).
            long position = this.BaseStream.Seek(0, SeekOrigin.Current);
            int offset = (int) (chunk.EndPosition - position);
            if (offset < 0) {
                throw Exceptions.SerializerReadPastChunkEnd();
            }
            // Only seek if not already at end.
            if (offset > 0) {
                this.BaseStream.Seek(offset, SeekOrigin.Current);
            }
        }
    }
}
