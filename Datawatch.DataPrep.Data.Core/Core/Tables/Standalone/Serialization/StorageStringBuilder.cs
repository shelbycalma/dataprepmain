﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Storage;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    public class StorageStringBuilder
    {

        public TextColumn BuildIndexedTextColumn(List<int> items, List<string> domain, string columnName)
        {
            // Create column directly from storage.
            IndexedTextStorage storage = CreateIndexedStringStorage(items, domain);
            return new TextColumn(columnName, storage);
        }

        /// <summary>
        /// Create a StorageString with the provided items and domain.
        /// The items is a list with values pointing into the index of the items in the domain.
        ///
        /// The highest value in the items list must not exceed the number of items in the domain list.
        ///
        /// </summary>
        /// <param name="items"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        internal IndexedTextStorage CreateIndexedStringStorage(List<int> items, List<string> domain)
        {
            if(items == null)
            {
                throw new ArgumentNullException("items");
            }
            if(domain == null)
            {
                throw new ArgumentNullException("domain");

            }
            StorageInt keys = CreateStorageKeys(items);

            int maxKey = items.Count > 0 ? keys.GetMax(items.Count) : -1;

            // Validate that we have a valid domain
            // The domain will allways include at least one null object at position 0 thus the +1
            if (domain.Count < maxKey + 1)
            {
                throw new ArgumentException("Bad indexed text, domain missing values");
            }

            // We'll only read as many as we need (enough to look up the
            // largest key we saw above).
            int distinct = maxKey + 1;

            StoragePointer<string> values = new StoragePointer<string>(distinct);
            StringIndex index = new StringIndex(distinct);

            // The domain might be larger than the number of distinct values in the items list, ignore the rest.
            for (int i = 0; i < distinct; i++)
            {
                string value = domain[i];
                values[i] = value;
                index.GetKey(value);
            }
        
            return new IndexedTextStorage(keys, values, index);
        }

        /// <summary>
        /// CreateStorageKeys will create a StorageInt with specified size and then populate it with the items stored in the
        /// storage parameter.
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A new StorageKey instance with values from the items parameter</returns>
        private StorageInt CreateStorageKeys(List<int> items)
        {
            // Allocate the storage.
            StorageInt keys = new StorageInt(items.Count);
            for (int i = 0; i < items.Count; ++i)
            {
                keys[i] = items[i];
            }
            return keys;
        }
    }
}
