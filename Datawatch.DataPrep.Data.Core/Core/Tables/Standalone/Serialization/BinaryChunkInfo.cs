﻿namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    /// <summary>
    /// Information about an opened chunk for <see cref="ChunkedBinaryWriter"/>
    /// and <see cref="ChunkedBinaryReader"/>.
    /// </summary>
    struct BinaryChunkInfo
    {
        /// <summary>
        /// At which position in the stream is the chunk header?
        /// </summary>
        public long HeaderPosition;

        /// <summary>
        /// At which position in the stream does the chunk contents start?
        /// </summary>
        public long StartPosition;

        /// <summary>
        /// At which position in the stream does the chunk end (position of
        /// first byte after chunk)?
        /// </summary>
        public long EndPosition;

        /// <summary>
        /// How many bytes are the chunk contents in the stream?
        /// </summary>
        public int ByteCount;
    }
}
