﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    /// <summary>
    /// Stream writer that can write chunks with a size preamble.
    /// </summary>
    internal class ChunkedBinaryWriter : BinaryWriter
    {
        /// <summary>
        /// Open chunks that still haven't had their preamble written.
        /// </summary>
        private readonly Stack<BinaryChunkInfo> chunks; 

        /// <summary>
        /// Creates a new writer on the specified stream.
        /// </summary>
        /// <param name="output">The stream to write to (must support
        /// seeking).</param>
        /// <param name="encoding">The encoding to use for strings.</param>
        public ChunkedBinaryWriter(Stream output, Encoding encoding) :
            base(output, encoding)
        {
            if (!output.CanWrite) {
                throw Exceptions.SerializerStreamCannotWrite();
            }
            if (!output.CanSeek) {
                throw Exceptions.SerializerStreamCannotSeek();
            }
            chunks = new Stack<BinaryChunkInfo>();
        }

        /// <summary>
        /// Start a new chunk (call <see cref="EndChunk()"/> to commit it).
        /// </summary>
        /// <remarks>
        /// <para>This call allocates space in the stream for the chunk
        /// preamble, when the chunk is committed the final size will be
        /// written to the stream.</para>
        /// <para>Chunks can be nested to arbitrary depth.</para>
        /// </remarks>
        public void BeginChunk()
        {
            BinaryChunkInfo chunk = new BinaryChunkInfo();
            // Save the current position (where to write preamble).
            chunk.HeaderPosition = Seek(0, SeekOrigin.Current);
            // Write dummy size to reserve space.
            Write(0);
            // Save the start position of the chunk data itself.
            chunk.StartPosition = Seek(0, SeekOrigin.Current);
            chunks.Push(chunk);
        }

        /// <summary>
        /// Closes the writer and underlying stream (an exception will be
        /// thrown if there are still open chunks).
        /// </summary>
        public override void Close()
        {
            if (chunks.Count > 0) {
                throw Exceptions.SerializerOpenChunksOnClose();
            }
            base.Close();
        }

        /// <summary>
        /// Checks for open chunks, then disposes the underlying stream.
        /// </summary>
        /// <param name="disposing">True to release both managed and unmanaged
        /// resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing) {
                if (chunks.Count > 0) {
                    // Ok to throw, not called from finalizer.
                    throw Exceptions.SerializerOpenChunksOnDispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Commits the most recently opened chunk (writes its preamble).
        /// </summary>
        /// <returns>The total size in bytes of the chunk - not including the
        /// preamble.</returns>
        public int EndChunk()
        {
            if (chunks.Count == 0) {
                throw Exceptions.SerializerNoOpenChunks();
            }
            BinaryChunkInfo chunk = chunks.Pop();
            // Record the chunk end position (next byte after chunk).
            chunk.EndPosition = Seek(0, SeekOrigin.Current);
            // Total chunk size in bytes (excluding header).
            chunk.ByteCount = (int) (chunk.EndPosition - chunk.StartPosition);
            if (chunk.ByteCount < 0) {
                throw Exceptions.SerializerNegativeSizeChunk();
            }
            // How far to seek to header?
            int offset = (int) (chunk.EndPosition - chunk.HeaderPosition);
            Seek(-offset, SeekOrigin.Current);
            // Write size to header.
            Write(chunk.ByteCount);
            // Go back to first position past chunk.
            Seek(chunk.ByteCount, SeekOrigin.Current);
            return chunk.ByteCount;
        }
    }
}
