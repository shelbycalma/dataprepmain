﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    /// <summary>
    /// Writes any <see cref="ITable"/> to a stream in a binary format that
    /// can later be read by <see cref="BinaryTableReader"/> into a
    /// <see cref="StandaloneTable"/>.
    /// </summary>
    internal class BinaryTableWriter : IDisposable
    {
        /// <summary>
        /// Sanity-check header.
        /// </summary>
        internal const string Magic = "DWCH-ST";

        /// <summary>
        /// Current serialization format version.
        /// </summary>
        private const byte Version = 1;

        /// <summary>
        /// Special token used to represent the null string.
        /// </summary>
        /// <remarks>
        /// <para>The <see cref="BinaryWriter"/> and <see cref="BinaryReader"/>
        /// classes don't support null strings, so we write this token instead,
        /// and hope it won't occur as an actual text value.</para>
        /// </remarks>
        private const string NullString = "\0\0";

        private ChunkedBinaryWriter writer;

        private ITable table;
        private int rows;

        private bool disposed;

        /// <summary>
        /// Creates a new table writer on the specified stream.
        /// </summary>
        /// <param name="output">Stream to write to (must support
        /// seeking).</param>
        public BinaryTableWriter(Stream output)
        {
            if (output == null) {
                throw new ArgumentNullException("output");
            }
            writer = new ChunkedBinaryWriter(output, Encoding.UTF8);
        }

        /// <summary>
        /// Writes the specified table to the stream.
        /// </summary>
        /// <param name="table">The table to write.</param>
        public void Write(ITable table)
        {
            if (table == null) {
                throw new ArgumentNullException("table");
            }
            // Store table and row count locally so we don't
            // have to pass them around on every call.
            this.table = table;
            this.rows = table.RowCount;

            WriteTable();
            writer.Flush();
            
            // Clear local references just in case.
            this.table = null;
            this.rows = 0;
        }

        /// <summary>
        /// Writes the full table to the writer.
        /// </summary>
        private void WriteTable()
        {
            ITimeseriesTable series = table as ITimeseriesTable;
            if (series != null && series.TimeCount > 0) {
                throw new NotSupportedException("Time series tables");
            }
            // Write out header.
            writer.Write(Magic);
            writer.Write(Version);
            // Rows and columns.
            writer.Write(table.RowCount);
            writer.Write(table.ColumnCount);
            // Write each column in order.
            for (int i = 0; i < table.ColumnCount; i++) {
                IColumn column = table.GetColumn(i);
                if (column is ITimeseriesColumn) {
                    throw new NotSupportedException("Time series columns");
                }
                // The name first.
                writer.Write(column.Name);
                // TODO: If we want to try to store some column meta data, we
                // should do that here, before the contents.

                // Then the rest in a chunk so we can read just a few rows
                // and then skip past the rest.
                writer.BeginChunk();
                WriteColumn(column);
                int size = writer.EndChunk();
                Trace.WriteLine(string.Format(
                    "Column #{0} \"{1}\": {2} bytes", i, column.Name, size));
            }
        }

        /// <summary>
        /// Writes one column to the writer (excluding name).
        /// </summary>
        private void WriteColumn(IColumn column)
        {
            // Figure out type, and write that first:
            // 'T' = text, 'N' = numeric, 'D' = time.
            ITextColumn text = column as ITextColumn;
            if (text != null) {
                writer.Write('T');
                WriteText(text);
                return;
            }
            INumericColumn numeric = column as INumericColumn;
            if (numeric != null) {
                writer.Write('N');
                WriteNumeric(numeric);
                return;
            }
            ITimeColumn time = column as ITimeColumn;
            if (time != null) {
                writer.Write('D');
                WriteTime(time);
                return;
            }
            throw Exceptions.SerializerUnknownColumnType(
                column.Name, column.GetType());
        }

        /// <summary>
        /// Writes a text column to the writer (excluding name).
        /// </summary>
        private void WriteText(ITextColumn column)
        {
            // Figure out format, and write that first:
            // 'F' = flat, 'I' = indexed.
            IIndexedColumn indexed = column as IIndexedColumn;
            if (indexed != null &&
                indexed.IsIndexed && indexed.CanGetIndex) {
                writer.Write('I');
                WriteTextIndexed(column);
                return;
            }
            writer.Write('F');
            WriteTextFlat(column);
        }

        /// <summary>
        /// Helper method to test for the <see cref="NullString"/> token.
        /// </summary>
        /// <param name="value">Value read from stream.</param>
        /// <returns>True if it's the special token that represents
        /// null.</returns>
        internal static bool IsNullString(string value)
        {
            return value.Length == 2 && value[0] == '\0' && value[1] == '\0';
        }

        /// <summary>
        /// Writes a text column to the writer in a flat format (excluding
        /// name).
        /// </summary>
        private void WriteTextFlat(ITextColumn column)
        {
            // Flat format, all strings unique (or close).
            for (int i = 0; i < rows; i++) {
                string value = column.GetTextValue(i);
                // Replace null with special token.
                writer.Write(value ?? NullString);
            }
        }

        /// <summary>
        /// Writes a text column to the writer in an indexed format (excluding
        /// name).
        /// </summary>
        private void WriteTextIndexed(ITextColumn column)
        {
            IIndexedColumn indexed = column as IIndexedColumn;
            Debug.Assert(indexed != null && indexed.CanGetIndex,
                "Cannot read as indexed column. Test before calling here.");

            // Read out the raw index from the column.
            KeyIndex index = new KeyIndex(rows);
            indexed.GetIndex(index);

            // Write one chunk for the keys - this is simple, as we just write
            // the key for each row. Write as a chunk so we can read just the
            // first few rows later if we want to.
            writer.BeginChunk();
            WriteKeyStorage(index.Keys, rows, index.MaxKey);
            writer.EndChunk();

            // Create a map from key value to row index (if there are no rows
            // with that key value set entry to -1, if there are multiple then
            // set it to the first row in the table).
            int distinct = index.MaxKey + 1;
            StorageInt reverse = new StorageInt(distinct);
            reverse.Clear(-1);
            for (int i = rows - 1; i >= 0; i--) {
                reverse[index.Keys[i]] = i;
            }

            // Now write out the key values using the map. We don't have to
            // use a chunk for this as the entire column is in a chunk, so we
            // can read as many values as we need and then just leave it.

            // TODO: Add direct serialization of StringIndex's dictionary.

            // First write the number of values.
            writer.Write(distinct);
            for (int i = 0; i < distinct; i++) {
                // Look up the "representative" row for this key from the map.
                int row = reverse[i];
                // TODO: Validate that values don't start with '\0' as that
                // will probably mess up the deserialization.
                // If there isn't a row (no rows use that key value any more)
                // or the row value is null, just write the null token.
                string value = row < 0 ? NullString :
                    (column.GetTextValue(row) ?? NullString);
                writer.Write(value);
            }
        }

        /// <summary>
        /// Writes the key storage from an indexed text column to the writer.
        /// </summary>
        /// <param name="storage">The key storage from the index returned by
        /// <see cref="IIndexedColumn.GetIndex(KeyIndex)"/>.</param>
        /// <param name="count">The number of entries (row count).</param>
        /// <param name="max">The largest key in the storage.</param>
        private void WriteKeyStorage(StorageInt storage, int count, int max)
        {
            // Determine how many bytes are needed for each key:
            // 'b' = byte (one), 's' = short (two), 'i' = int (four).
            if (max < 0x100) {
                // Each key fits in a byte.
                writer.Write('b');
                for (int i = 0; i < rows; i++) {
                    int key = storage[i];
                    // Bytes are unsigned, so no problem here.
                    writer.Write((byte) key);
                }
            }
            else if (max < 0x10000) {
                // Each key fits in two bytes.
                writer.Write('s');
                for (int i = 0; i < rows; i++) {
                    int key = storage[i];
                    // Need to use ushort to use all 16 bits.
                    writer.Write((ushort) key);
                }
            }
            else {
                // Need full four bytes.
                writer.Write('i');
                for (int i = 0; i < rows; i++) {
                    int key = storage[i];
                    writer.Write(key);
                }
            }
        }

        /// <summary>
        /// Writes a numeric column to the writer (excluding name).
        /// </summary>
        private void WriteNumeric(INumericColumn column)
        {
            // TODO: Can we efficiently check if all values are integer, and
            // if so use something tighter than doubles to serialize?
            // TODO: Can we efficiently check if there are only a few distinct
            // values, and/or they have a small spread, so we can use an index
            // technique or base + delta to serialize?
            
            // There is currently only one format: 'D' = double.
            writer.Write('D');
            for (int i = 0; i < rows; i++) {
                double value = column.GetNumericValue(i);
                // TODO: +/-inf, nan, roundtrips through Java?
                writer.Write(value);
            }
        }

        /// <summary>
        /// Writes a time column to the writer (excluding name).
        /// </summary>
        private void WriteTime(ITimeColumn column)
        {
            // TODO: Can we efficiently check if all dates have a small spread
            // around a base date and use base + delta for smaller storage?

            // There is currently only one format: 'T' = ticks.
            writer.Write('T');
            for (int i = 0; i < rows; i++) {
                DateTime value = column.GetTimeValue(i);
                writer.Write(value.Ticks);
            }
        }

        /// <summary>
        /// Closes the writer and underlying stream.
        /// </summary>
        public void Close()
        {
            Dispose(true);
        }

        /// <summary>
        /// Clears all buffers for the writer and causes any buffered data to
        /// be written to the underlying stream.
        /// </summary>
        public void Flush()
        {
            writer.Flush();
        }

        /// <summary>
        /// Releases all resources used by the current instance of the writer
        /// and closes the underlying stream.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed) {
                if (disposing) {
                    writer.Close();
                    writer = null;
                    table = null;
                    rows = -1;
                }
                disposed = true;
            }
        }
    }
}
