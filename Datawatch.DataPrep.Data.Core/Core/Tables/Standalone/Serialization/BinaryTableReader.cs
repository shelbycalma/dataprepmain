﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Storage;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization
{
    /// <summary>
    /// Reads a table from a stream that was previously written with
    /// <see cref="BinaryTableWriter"/> into a <see cref="StandaloneTable"/>.
    /// </summary>
    internal class BinaryTableReader : IDisposable
    {
        private const byte MinSupportedVersion = 1;
        private const byte MaxSupportedVersion = 1;

        private ChunkedBinaryReader reader;
        
        private StandaloneTable table;
        private int rows;

        private bool disposed;

        /// <summary>
        /// Creates a new table reader on the specified stream.
        /// </summary>
        /// <param name="input">Stream to read from (must support
        /// seeking).</param>
        public BinaryTableReader(Stream input)
        {
            if (input == null) {
                throw new ArgumentNullException("input");
            }
            reader = new ChunkedBinaryReader(input, Encoding.UTF8);
        }

        /// <summary>
        /// Reads the entire table from the stream (c.f.
        /// <see cref="Read(int)"/>).
        /// </summary>
        /// <returns>The table as a <see cref="StandaloneTable"/>.</returns>
        public StandaloneTable Read()
        {
            return Read(-1);
        }

        /// <summary>
        /// Reads a maximum number of rows from the stream.
        /// </summary>
        /// <param name="maxRows">The maximum number of rows to read (zero to
        /// only read schema, -1 to read all rows).</param>
        /// <returns>The table as a <see cref="StandaloneTable"/>.</returns>
        public StandaloneTable Read(int maxRows)
        {
            // Table will be read to this field.
            table = null;
            ReadTable(maxRows);
            // Get the result and clear the field.
            StandaloneTable result = table;
            table = null;
            return result;
        }

        /// <summary>
        /// Reads the table from the reader into the <see cref="table"/> field.
        /// </summary>
        /// <param name="maxRows">The maximum number of rows.</param>
        private void ReadTable(int maxRows)
        {
            // TODO: Overload where you can choose which columns to load?

            // Validate sanity-check header.
            if (!BinaryTableWriter.Magic.Equals(reader.ReadString())) {
                throw Exceptions.SerializerBadMagic();
            }
            // Check that we can read this version.
            byte version = reader.ReadByte();
            if (version < MinSupportedVersion ||
                version > MaxSupportedVersion) {
                throw Exceptions.SerializerUnsupportedVersion(
                    version, MinSupportedVersion, MaxSupportedVersion);
            }
            // Total rows in the table.
            rows = reader.ReadInt32();
            if (rows < 0 ) {
                throw Exceptions.SerializerBadRowCount(rows);
            }
            if (maxRows >= 0) {
                // User wants to cap, so do it.
                rows = Math.Min(rows, maxRows);
            }
            table = new StandaloneTable(rows);
            table.BeginUpdate();
            // Preload table with empty rows.
            for (int i = 0; i < rows; i++) {
                table.AddRow();
            }
            // Read each column in order.
            int columns = reader.ReadInt32();
            if (columns < 0) {
                throw Exceptions.SerializerBadColumnCount(columns);
            }
            for (int i = 0; i < columns; i++) {
                // Always name first.
                string name = reader.ReadString();
                // TODO: If we add meta data, read that here.
                // Wrapped in chunk, so we can skip past it if we only
                // read a few rows.
                reader.BeginChunk();
                ReadColumn(name);
                reader.EndChunk();
            }
            table.EndUpdate();
        }

        /// <summary>
        /// Reads one column from the reader.
        /// </summary>
        /// <param name="name">The name, already read.</param>
        private void ReadColumn(string name)
        {
            // Figure out type:
            // 'T' = text, 'N' = number, 'D' = time.
            char type = reader.ReadChar();
            switch (type) {
                case 'T':
                    ReadText(name);
                    break;
                case 'N':
                    ReadNumeric(name);
                    break;
                case 'D':
                    ReadTime(name);
                    break;
                default:
                    throw Exceptions.SerializerBadColumnType(name, type);
            }
        }

        /// <summary>
        /// Reads a text column from the reader.
        /// </summary>
        /// <param name="name">The name, already read.</param>
        private void ReadText(string name)
        {
            // Figure out format:
            // 'F' = flat, 'I' = indexed.
            char format = reader.ReadChar();
            switch (format) {
                case 'F':
                    ReadTextFlat(name);
                    break;                
                case 'I':
                    ReadTextIndexed(name);
                    break;                
                default:
                    throw Exceptions.SerializerBadTextFormat(name, format);
            }
        }

        /// <summary>
        /// Reads a flat format text column from the reader.
        /// </summary>
        /// <param name="name">The name, already read.</param>
        private void ReadTextFlat(string name)
        {
            // Flat format, all strings unique (or close).
            TextColumn column = new TextColumn(name);
            // Force column to not check if it should switch to indexed.
            column.Compression = CompressionMode.Never;
            table.AddColumn(column);
            for (int i = 0; i < rows; i++) {
                string value = reader.ReadString();
                // Replace special token with null.
                if (!BinaryTableWriter.IsNullString(value)) {
                    column.SetTextValue(i, value);
                }
            }
        }

        /// <summary>
        /// Reads an indexed format text column from the reader.
        /// </summary>
        /// <param name="name">The name, already read.</param>
        private void ReadTextIndexed(string name)
        {
            // The index keys are stored in a chunk, so we can read just the
            // first few rows and skip the rest.
            reader.BeginChunk();
            // Allocate the storage.
            StorageInt keys = new StorageInt(rows);
            ReadKeyStorage(keys, rows);
            // Find the max key read (-1 if no rows, empty table or just
            // the table schema was requested).
            int maxKey = rows > 0 ? keys.GetMax(rows) : -1;
            reader.EndChunk();

            // Now read the key values. First validate the count.
            int distinct = reader.ReadInt32();
            if (distinct < maxKey + 1) {
                // Not enough values, not ok.
                throw Exceptions.SerializerBadIndexedText(name);
            }
            // We'll only read as many as we need (enough to look up the
            // largest key we saw above).
            distinct = maxKey + 1;
            StoragePointer<string> values =
                new StoragePointer<string>(distinct);
            StringIndex index = new StringIndex(distinct);
            // Count the nulls. The real null always has key #0, the other
            // nulls are the results of unused values when the column was
            // written (this will happen if you delete the last row that had
            // a particular value).
            int nulls = 0;
            for (int i = 0; i < distinct; i++) {
                string value = reader.ReadString();
                // Replace the special token with null...
                if (BinaryTableWriter.IsNullString(value)) {
                    if (nulls == 0) {
                        // ...but only if it's the first.
                        value = null;
                    } else {
                        // ...for the rest, invent a value that will not be
                        // used (normally) and add that, so that the rest of
                        // the values get the proper keys.
                        value = '\0' + nulls.ToString(
                            CultureInfo.InvariantCulture);
                    }
                    nulls++;
                }
                // Store the new value and update the map.
                values[i] = value;
                index.GetKey(value);
            }
            // Create column directly from storage.
            IndexedTextStorage storage =
                new IndexedTextStorage(keys, values, index);
            TextColumn column = new TextColumn(name, storage);
            table.AddColumn(column);
        }

        /// <summary>
        /// Reads the key storage for an indexed text column from the reader.
        /// </summary>
        /// <param name="storage">The storage in which to put the keys.</param>
        /// <param name="count">Number of entries to read (row count).</param>
        private void ReadKeyStorage(StorageInt storage, int count)
        {
            // Determine how many bytes are used for each key:
            // 'b' = byte (one), 's' = short (two), 'i' = int (four).
            char type = reader.ReadChar();
            switch (type) {
                case 'b':
                    // Each key is stored in one byte.
                    for (int i = 0; i < count; i++) {
                        storage[i] = reader.ReadByte();
                    }
                    break;
                case 's':
                    // Each key is stored in two bytes.
                    for (int i = 0; i < count; i++) {
                        storage[i] = reader.ReadUInt16();
                    }
                    break;
                case 'i':
                    // Each key is stored in full four bytes.
                    for (int i = 0; i < count; i++) {
                        storage[i] = reader.ReadInt32();
                    }
                    break;
                default:
                    throw Exceptions.SerializerBadKeyStorageFormat(type);
            }
        }

        /// <summary>
        /// Reads a numeric column from the reader.
        /// </summary>
        /// <param name="name">Name, already read.</param>
        private void ReadNumeric(string name)
        {
            NumericColumn column = new NumericColumn(name);
            table.AddColumn(column);
            
            // There is currently only one format: 'D' = double.
            char format = reader.ReadChar();
            switch (format) {
                case 'D':
                    // Double format, all values stored as doubles.
                    for (int i = 0; i < rows; i++) {
                        // TODO: +/-inf, nan, roundtrips through Java?
                        double value = reader.ReadDouble();
                        column.SetNumericValue(i, value);
                    }
                    break;
                default:
                    throw Exceptions.SerializerBadNumericFormat(name, format);
            }
        }

        /// <summary>
        /// Reads a time column from the reader.
        /// </summary>
        /// <param name="name">Name, already read.</param>
        private void ReadTime(string name)
        {
            TimeColumn column = new TimeColumn(name);
            table.AddColumn(column);

            // There is currently only one format: 'T' = ticks.
            char format = reader.ReadChar();
            switch (format) {
                case 'T':
                    // DateTime format, all values stored as ticks.
                    for (int i = 0; i < rows; i++) {
                        DateTime value = new DateTime(reader.ReadInt64());
                        column.SetTimeValue(i, value);
                    }
                    break;
                default:
                    throw Exceptions.SerializerBadTimeFormat(name, format);
            }
        }

        /// <summary>
        /// Closes the reader and underlying stream.
        /// </summary>
        public void Close()
        {
            Dispose(true);
        }

        /// <summary>
        /// Releases all resources used by the current instance of the reader
        /// and closes the underlying stream.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed) {
                if (disposing) {
                    reader.Close();
                    reader = null;
                    table = null;
                    rows = -1;
                }
                disposed = true;
            }
        }
    }
}
