using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// The base type for columns in a <see cref="StandaloneTable"/>.
    /// </summary>
    public abstract class Column : IColumn
    {
        private readonly string name;
        internal StandaloneTable table;
        private IColumnMetaData metaData;

        /// <summary>
        /// Creates a new column with the specified name.
        /// </summary>
        /// <param name="name">The name (identifier) of the column.</param>
        protected Column(string name)
        {
            if (name == null) {
                throw Exceptions.ArgumentNull("name");
            }
            this.name = name;
        }

        /// <summary>
        /// Check whether the supplied <see cref="IRow"/> is not null, 
        /// belongs to the same table as the <see cref="Column">column
        /// </see>, is of the type <see cref="Row"/>, and has not been removed
        /// from the <see cref="StandaloneTable"/>.
        /// </summary>
        /// <param name="row"><see cref="IRow"/> to check.</param>
        /// <remarks>
        /// <para>Throws an exception if any of the above is false.</para>
        /// </remarks>
        /// <seealso cref="CheckRow(Row)"/>
        [Conditional("CHECKED")]
        protected void CheckIRow(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);
        }

        /// <summary>
        /// Checks whether the supplied <see cref="ITime"/> is not null,
        /// belongs to the same table as the
        /// <see cref="NumericTimeseriesColumn"/>, is of the type
        /// <see cref="Time"/>, and has not been removed from the
        /// <see cref="StandaloneTable"/>.
        /// </summary>
        /// <param name="time"><see cref="ITime"/> to check.</param>
        [Conditional("CHECKED")]
        protected void CheckITime(ITime time)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckITime(table, time);
        }

        /// <summary>
        /// Check whether the supplied <see cref="Row"/> is not null, belongs
        /// to the same <see cref="StandaloneTable"/> as the
        /// <see cref="Column"/>, and has not been removed.
        /// </summary>
        /// <param name="row">Row to check.</param>
        /// <remarks>
        /// <para>Throws an exception if any of the above is false.</para>
        /// </remarks>
        /// <seealso cref="CheckIRow(IRow)"/>
        [Conditional("CHECKED")]
        protected void CheckRow(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);
        }

        /// <summary>
        /// Check whether the supplied row index is non-negative and less than
        /// the table's <see cref="StandaloneTable.RowCount"/>.
        /// </summary>
        /// <param name="row">Row index to check.</param>
        [Conditional("CHECKED")]
        protected void CheckRowIndex(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);
        }

        /// <summary>
        /// Check whether the supplied sample index is non-negative and less
        /// than the table's <see cref="StandaloneTable.TimeCount"/>.
        /// </summary>
        /// <param name="sample">Sample index to check.</param>
        [Conditional("CHECKED")]
        protected void CheckSampleIndex(int sample)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckSampleIndex(table, sample);
        }

        /// <summary>
        /// Check whether the supplied <see cref="Time"/> is not null, belongs
        /// to the same <see cref="StandaloneTable"/> as the
        /// <see cref="Column"/>, and has not been removed.
        /// </summary>
        /// <param name="time">Time to check.</param>
        /// <remarks>
        /// <para>Throws an exception if any of the above is false.</para>
        /// </remarks>
        /// <seealso cref="CheckITime(ITime)"/>
        [Conditional("CHECKED")]
        protected void CheckTime(Time time)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckTime(table, time);
        }

        internal virtual void CheckStorage(int rows)
        {
        }

        [ModifiesTable]
        protected internal abstract void ClearRow(int row);

        internal virtual void ClearRows(int rows)
        {
        }

        // TODO: This is NOT == index, but should be!
        internal virtual void ClearTime(int record)
        {
        }

        internal virtual void ClearTimes(int times)
        {
        }

        [ModifiesTable]
        protected internal abstract void CompactRows(int rows);

        [ModifiesTable]
        protected internal virtual void CompactTimes(int times)
        {
        }

        /// <summary>
        /// Called when rows have been removed (at the indices indicated by the
        /// <paramref name="holes"/> array). The column should move down the
        /// remaining records to fill the holes.
        /// </summary>
        /// <param name="holes">Indices of rows that have been removed, with an
        /// extra entry at the end pointing just past the last remaining row or
        /// hole.</param>
        /// <remarks>
        /// <para>If you override this method you do not need to call the base
        /// class implementation, and you <em>must</em> not change the contents
        /// of the <paramref name="holes"/> array.</para>
        /// </remarks>
        internal virtual void DefragmentRows(int[] holes)
        {
        }

        /// <summary>
        /// Called when times have been removed (at the indices indicated by
        /// the <paramref name="holes"/> array). The column should move down
        /// the remaining records to fill the holes.
        /// </summary>
        /// <param name="holes">Indices of times that have been removed, with
        /// an extra entry at the end pointing just past the last remaining
        /// time or hole.</param>
        /// <remarks>
        /// <para>If you override this method you do not need to call the base
        /// class implementation, and you <em>must</em> not change the contents
        /// of the <paramref name="holes"/> array.</para>
        /// </remarks>
        internal virtual void DefragmentTimes(int[] holes)
        {
        }

        [ModifiesTable]
        protected internal virtual void Freeze()
        {
        }

        /// <summary>
        /// Gets the <see cref="IColumnMetaData">metadata</see> for this column.
        /// </summary>
        public IColumnMetaData MetaData
        {
            get { return metaData; }
            [ModifiesTable] set
            {
                Checks.CheckTableNullOrUpdating(this);
                metaData = value;
            }
        }

        /// <summary>
        /// Gets the column's name.
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Called when the column has been added to a table (but no events
        /// have been fired from the table yet).
        /// </summary>
        protected internal abstract void OnAddedToTable();

        /// <summary>
        /// Called when the column has been removed from a table (but no events
        /// have been fired from the table yet).
        /// </summary>
        /// <remarks>
        /// <para>Any state that the column maintains that is related to the
        /// table (or the rows in the table) should be cleared in this
        /// method. At exit from this method the column should be ready to be
        /// added to a different table.</para>
        /// </remarks>
        protected internal abstract void OnRemovedFromTable();

        /// <summary>
        /// Gives calculated columns a chance to update any internal state
        /// before the <see cref="StandaloneTable.Changed"/> event fires.
        /// </summary>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// <para>This is part of the <see cref="StandaloneTable"/>'s batching
        /// functionality. It should be overridden by <see cref="Column"/>
        /// subclasses that derive their values from other columns in the table
        /// (e.g. calculated columns and bucket columns).</para>
        /// <para>The method will be called by the table itself just prior to
        /// sending out the Changed event to determine if the column was
        /// affected by other changes to the table, typically value changes in
        /// the determinant column or row additions/removals.</para>
        /// </remarks>
        [ModifiesTable]
        protected internal virtual void Recalculate()
        {
        }

        protected internal abstract int RowCapacity {
            get; [ModifiesTable] set;
        }

        protected internal virtual int TimeCapacity {
            get { return 0; }
            [ModifiesTable] set { }
        }

        /// <summary>
        /// Sets the value for a particular <see cref="Row"/>.
        /// </summary>
        /// <param name="row">Row to set value for.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or undefined.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetValue(Row row, object value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);

            SetValueInternal(row.record, value);            
        }

        /// <summary>
        /// Sets the value for a particular <see cref="Row"/>.
        /// </summary>
        /// <param name="row">Row index to set value for.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or undefined.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetValue(int row, object value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);

            SetValueInternal(row, value);            
        }

        /// <summary>
        /// Sets the value for a particular <see cref="Row"/>.
        /// </summary>
        /// <param name="row">Row index to set value for.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or undefined.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// <para>Note to implementors: you do not need to validate the
        /// <paramref name="row"/> argument to this method, it is valid.</para>
        /// </remarks>
        [ModifiesTable]
        protected internal abstract void SetValueInternal(
            int row, object value);

        /// <summary>
        /// Gets the table that the column belongs to, or null if it doesn't
        /// belong to a table (not added yet or removed).
        /// </summary>
        public StandaloneTable Table
        {
            get { return table; }
            internal set {
                if (value != table) {
                    if (table != null) {
                        OnRemovedFromTable();
                    }
                    table = value;
                    if (table != null) {
                        OnAddedToTable();
                    }
                }
            }
        }

        ITable IColumn.Table
        {
            get { return table; }
        }

        /// <summary>
        /// A String that represents the current Object. 
        /// </summary>
        /// <returns><see cref="string"/></returns>
        public override string ToString()
        {
            return name;
        }
    }
}
