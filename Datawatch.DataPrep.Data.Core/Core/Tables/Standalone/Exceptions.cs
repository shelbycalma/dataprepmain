using System;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
	internal static class Exceptions
	{
		public static Exception ArgumentNull(string name)
		{
            return new ArgumentNullException(name);
		}

		private static Exception CreateArgument(string message)
		{
			return new ArgumentException(message);
		}

        private static Exception CreateArgumentOutOfRange(
            string paramName, object value, string message)
        {
            return new ArgumentOutOfRangeException(paramName, value, message);
        }

        private static Exception CreateFileFormat(string message)
        {
            // Would like to use System.IO.FileFormatException, but for some
            // reason it's only in WindowsBase.dll.
            return new FormatException(message);
        }

		private static Exception CreateInvalidOperation(string message)
		{
			return new InvalidOperationException(message);
		}

        public static Exception SerializerBadChunkSize(int size)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadChunkSize, size));
        }

        public static Exception SerializerBadColumnCount(int columns)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadColumnCount, columns));
        }

        public static Exception SerializerBadColumnType(
            string columnName, char type)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadColumnType,
                columnName, type));
        }

        public static Exception SerializerBadIndexedText(string columnName)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadNumericFormat,
                columnName));
        }

        public static Exception SerializerBadNumericFormat(
            string columnName, char format)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadNumericFormat,
                columnName, format));
        }

        public static Exception SerializerBadTextFormat(
            string columnName, char format)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadTextFormat,
                columnName, format));
        }

        public static Exception SerializerBadTimeFormat(
            string columnName, char format)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadTimeFormat,
                columnName, format));
        }

        public static Exception SerializerBadKeyStorageFormat(char type)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadKeyStorageFormat, type));
        }

        public static Exception SerializerBadMagic()
        {
            return CreateFileFormat(
                Properties.Resources.ExSerializerBadMagic);
        }

        public static Exception SerializerBadRowCount(int rows)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerBadRowCount, rows));
        }

        public static Exception SerializerNegativeSizeChunk()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExSerializerNegativeSizeChunk);
        }

        public static Exception SerializerNoOpenChunks()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExSerializerNoOpenChunks);
        }

        public static Exception SerializerOpenChunksOnClose()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExSerializerOpenChunksOnClose);
        }

        public static Exception SerializerOpenChunksOnDispose()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExSerializerOpenChunksOnDispose);
        }

        public static Exception SerializerReadPastChunkEnd()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExSerializerReadPastChunkEnd);
        }

        public static Exception SerializerStreamCannotRead()
        {
            return CreateArgument(
                Properties.Resources.ExSerializerStreamCannotRead);
        }

        public static Exception SerializerStreamCannotSeek()
        {
            return CreateArgument(
                Properties.Resources.ExSerializerStreamCannotSeek);
        }

        public static Exception SerializerStreamCannotWrite()
        {
            return CreateArgument(
                Properties.Resources.ExSerializerStreamCannotWrite);
        }

        public static Exception SerializerUnknownColumnType(
            string columnName, Type columnType)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExSerializerUnknownColumnType,
                columnName, columnType.Name));
        }

        public static Exception SerializerUnsupportedVersion(
            int version, int minSupported, int maxSupported)
        {
            return CreateFileFormat(string.Format(
                Properties.Resources.ExSerializerUnsupportedVersion,
                version, minSupported, maxSupported));
        }

        public static Exception TableColumnInOtherTable(string columnName)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableColumnInOtherTable, columnName));
        }

        public static Exception TableColumnIsNotSeries(string name)
        {
            return CreateInvalidOperation(string.Format(
                Properties.Resources.ExTableColumnIsNotSeries, name));
        }

        public static Exception TableColumnIsSeries(string name)
        {
            return CreateInvalidOperation(string.Format(
                Properties.Resources.ExTableColumnIsSeries, name));
        }

        public static Exception TableColumnNameExists(string columnName)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableColumnNameExists, columnName));
        }

        public static Exception TableColumnNotInTable(string columnName)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableColumnNotInTable, columnName));
        }

        public static Exception TableColumnUnknownTypeForWrite(
            string name, Type type)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableColumnUnknownTypeForWrite,
                name, type.Name));
        }

        public static Exception TableCopyDifferentColumnCounts(
            int sourceCount, int targetCount)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopyDifferentColumnCounts,
                sourceCount, targetCount));
        }

        public static Exception TableCopyDifferentColumnTypes(
            string name, Type source, Type target)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopyDifferentColumnTypes,
                name, source.Name, target.Name));
        }

        public static Exception TableCopyDifferentRowCounts(
            int sourceCount, int targetCount)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopyDifferentRowCounts,
                sourceCount, targetCount));
        }

        public static Exception TableCopyDifferentSampleCounts(
            int sourceCount, int targetCount)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopyDifferentSampleCounts,
                sourceCount, targetCount));
        }

        public static Exception TableCopyDifferentSamples(
            int index, DateTime source, DateTime target)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopyDifferentSamples,
                index, source, target));
        }

        public static Exception TableCopySameColumn(string name)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopySameColumn, name));
        }

        public static Exception TableCopySameTable()
        {
            return CreateArgument(Properties.Resources.ExTableCopySameTable);
        }

        public static Exception TableCopySourceColumnMissingInTarget(
            string name)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopySourceColumnMissingInTarget,
                name));
        }

        public static Exception TableCopyTargetNotEmpty()
        {
            return CreateArgument(
                Properties.Resources.ExTableCopyTargetNotEmpty);
        }

        public static Exception TableCopyUnknownSourceColumnType(
            string name, Type type)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableCopyUnknownSourceColumnType,
                name, type.Name));
        }

        public static Exception TableFrozen()
        {
            return CreateInvalidOperation(Properties.Resources.ExTableFrozen);
        }

        public static Exception TableMustCallBeginEndUpdate()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExTableMustCallBeginEndUpdate);
        }

        public static Exception TableNoSnapshot()
        {
            return CreateInvalidOperation(Properties.Resources.ExTableNoSnapshot);
        }

        public static Exception TableNotFreezable()
        {
            return CreateInvalidOperation(Properties.Resources.ExTableNotFreezable);
        }

        public static Exception TableRowAlreadyDeleted()
        {
            return CreateArgument(Properties.Resources.ExTableRowAlreadyDeleted);
        }

        public static Exception TableRowCapacityInvalid(int value, int rowCount)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableRowCapacityInvalid, value, rowCount));
        }

        public static Exception TableRowDeleted()
        {
            return CreateInvalidOperation(Properties.Resources.ExTableRowDeleted);
        }

        public static Exception TableRowIndexInvalid(
            string paramName, int index, int rowCount)
        {
            return CreateArgumentOutOfRange(paramName, index, string.Format(
                Properties.Resources.ExTableRowIndexInvalid, index, rowCount));
        }

        public static Exception TableRowInOtherTable()
        {
            return CreateArgument(Properties.Resources.ExTableRowInOtherTable);
        }

        public static Exception TableRowWrongType(Type type)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableRowWrongType, type.Name));
        }

        public static Exception TableSampleIndexInvalid(
            string paramName, int index, int sampleCount)
        {
            return CreateArgumentOutOfRange(paramName, index,
                string.Format(Properties.Resources.ExTableSampleIndexInvalid,
                    index, sampleCount));
        }

        public static Exception TableTimeAlreadyDeleted()
        {
            return CreateArgument(Properties.Resources.ExTableTimeAlreadyDeleted);
        }

        public static Exception TableTimeDeleted()
        {
            return CreateInvalidOperation(Properties.Resources.ExTableTimeDeleted);
        }

        public static Exception TableTimeExists(string time)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableTimeExists, time));
        }

        public static Exception TableTimeIndexInvalid(
            string paramName, int index, int timeCount)
        {
            return CreateArgumentOutOfRange(paramName, index, string.Format(
                Properties.Resources.ExTableTimeIndexInvalid, index, timeCount));
        }

        public static Exception TableTimeInOtherTable()
        {
            return CreateArgument(Properties.Resources.ExTableTimeInOtherTable);
        }

        public static Exception TableTimeWrongType(Type type)
        {
            return CreateArgument(string.Format(
                Properties.Resources.ExTableTimeWrongType, type.Name));
        }

        public static Exception TableUnlockedTooManyTimes()
        {
            return CreateInvalidOperation(
                Properties.Resources.ExTableUnlockedTooManyTimes);
        }
    }
}
