using System;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A <see cref="StandaloneTable"/> column of type <see cref="object"/>.
    /// </summary>
    public sealed class CustomColumn : Column, ICustomColumn
    {
        private StoragePointer<object> storage;
        private readonly Type type;
        
        /// <summary>
        /// Creates a new column with the specified name.
        /// </summary>
        /// <param name="name">The name (identifier) of the column.</param>
        /// <param name="type">The type of the custom data if any - use
        /// typeof(object) if it is completely generic.</param>
        public CustomColumn(string name, Type type) : base(name)
        {
            this.type = type;
        }

        protected internal override void ClearRow(int row)
        {
            storage[row] = null;
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= storage.Capacity);

            storage.Clear(0, rows);
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= storage.Capacity);

            storage.Capacity = rows;
        }

        internal override void DefragmentRows(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(holes[holes.Length - 1] <= storage.Capacity);

            storage.Compact(holes);
        }

        public object GetCustomValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);

            return storage[row.record];
        }

        object ICustomColumn.GetCustomValue(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);

            return storage[((Row) row).record];
        }

        public object GetCustomValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);

            return storage[row];
        }

        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);

            storage = new StoragePointer<object>(table.RowCapacity);
        }

        /// <summary>
        /// Clears the <see cref="CustomColumn"/>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
            storage = null;
        }

        protected internal override int RowCapacity
        {
            get {
                Debug.Assert(table != null);
                return storage.Capacity;
            }
            set {
                Debug.Assert(table != null);
                int oldCapacity = storage.Capacity;
                storage.Capacity = value;
                if (value > oldCapacity) {
                    storage.Clear(oldCapacity, value - oldCapacity);
                }
            }
        }

        [ModifiesTable]
        public void SetCustomValue(Row row, object value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);

            storage[row.record] = value;
        }

        [ModifiesTable]
        public void SetCustomValue(int row, object value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);

            storage[row] = value;
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            storage[row] = value;
        }
    }
}
