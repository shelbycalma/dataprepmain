using System;
using System.Collections;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A collection of <see cref="Time">times</see>.
    /// </summary>
    public class TimeCollection : IEnumerable<Time>
    {
        private readonly StandaloneTable table;

        /// <summary>
        /// Creates an instance of the <see cref="TimeCollection"/> class.
        /// </summary>
        /// <param name="table"><see cref="StandaloneTable">table</see> to
        /// store <see cref="Time">times</see> in.</param>
        internal TimeCollection(StandaloneTable table)
        {
            this.table = table;
        }
        
        /// <summary>
        /// Add a <see cref="Time">time</see> to the collection.
        /// </summary>
        /// <param name="value">Time value.</param>
        /// <returns>A <see cref="Time"/> object.</returns>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.Add(DateTime)")]
        public Time Add(DateTime value)
        {
            return table.AddTime(value);
        }

        /// <summary>
        /// Clears the collection.
        /// </summary>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.ClearTimes()")]
        public void Clear()
        {
            table.ClearTimes();
        }

        /// <summary>
        /// Gets the number of 
        /// <see cref="Time">times</see> held 
        /// in the collection.
        /// </summary>
        [Obsolete("Replaced by StandaloneTable.TimeCount")]
        public int Count
        {
            get { return table.TimeCount; }
        }

        /// <summary>
        /// Returns the generic <see cref="TimeCollection"/> enumerator.
        /// </summary>
        /// <returns>Generic enumerator.</returns>
        [Obsolete("Use StandaloneTable.TimeCount with " +
            "StandaloneTable.GetTime(int) instead.")]
        public IEnumerator<Time> GetEnumerator()
        {
            return table.LegacyGetTimeEnumerator();
        }

        /// <summary>
        /// Returns the <see cref="TimeCollection"/> enumerator.
        /// </summary>
        /// <returns>Enumerator</returns>
        [Obsolete("Use StandaloneTable.TimeCount with " +
            "StandaloneTable.GetTime(int) instead.")]
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes a <see cref="Time"/> from the collection.
        /// </summary>
        /// <param name="time">The <see cref="Time"/> to remove.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.RemoveTime(Time)")]
        public void Remove(Time time)
        {
            table.RemoveTime(time);
        }

        /// <summary>
        /// Returns a <see cref="Time"/> given an index.
        /// </summary>
        /// <param name="index">Index of the <see cref="Time"/> to retrieve.</param>
        /// <returns>The <see cref="Time"/> at the specified index.</returns>
        [Obsolete("Replaced by StandaloneTable.GetTime(int)")]
        public Time this[int index]
        {
            get { return table.GetTime(index); }
        }

        /// <summary>
        /// Returns a <see cref="Time"/> for a given 
        /// <see cref="DateTime"/> if one exists or 
        /// null otherwise.
        /// </summary>
        /// <param name="value"><see cref="DateTime"/> for 
        /// which to retrieve the corresponding 
        /// <see cref="Time"/>.</param>
        /// <returns></returns>
        [Obsolete("Replaced by StandaloneTable.GetTime(DateTime)")]
        public Time this[DateTime value]
        {
            get { return table.GetTime(value); }
        }
    }
}
