﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// Helper class that lets you write column values as objects, regardless
    /// of the column's type.
    /// </summary>
    /// <remarks>
    /// <para>This is a companion class to <see cref="ColumnReader"/>. As with
    /// that class, avoid this is possible. It's much faster to access column
    /// values through a typed interface like <see cref="TextColumn"/>. This
    /// class was added to ease migration, and for a few scenarios where the
    /// type of the column - and the value written - does not matter.</para>
    /// <para>Please note also that in some cases this class will be forced to
    /// make conversions that could be culture-dependent. For example, if you
    /// write to a text column with a numeric value, it will need to convert
    /// the value, and the result might not be what you expect.</para>
    /// </remarks>
    public abstract class ColumnWriter
    {
        /// <summary>
        /// Creates a column writer for a single column that can be reused for
        /// multiple rows.
        /// </summary>
        /// <param name="column">The column to write to.</param>
        /// <returns>An untyped column writer for the column.</returns>
        public static ColumnWriter ForColumn(Column column)
        {
            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }

            TextColumn text = column as TextColumn;
            if (text != null) {
                return new TextColumnWriter(text);
            }
            TextTimeseriesColumn textSeries = column as TextTimeseriesColumn;
            if (textSeries != null) {
                return new TextSeriesColumnWriter(textSeries);
            }
            NumericColumn numeric = column as NumericColumn;
            if (numeric != null) {
                return new NumericColumnWriter(numeric);
            }
            NumericTimeseriesColumn numericSeries =
                column as NumericTimeseriesColumn;
            if (numericSeries != null) {
                return new NumericSeriesColumnWriter(numericSeries);
            }
            TimeColumn time = column as TimeColumn;
            if (time != null) {
                return new TimeColumnWriter(time);
            }
            CustomColumn custom = column as CustomColumn;
            if (custom != null) {
                return new CustomColumnWriter(custom);
            }

            throw Exceptions.TableColumnUnknownTypeForWrite(
                column.Name, column.GetType());
        }

        /// <summary>
        /// Creates an array of column writers to match a set of columns.
        /// </summary>
        /// <param name="columns">Columns that you wish to write to.</param>
        /// <returns>An array of writers (each on created by the
        /// <see cref="ColumnWriter.ForColumn(Column)"/> method).</returns>
        public static ColumnWriter[] ForColumns(IEnumerable<Column> columns)
        {
            if (columns == null) {
                throw Exceptions.ArgumentNull("columns");
            }

            List<ColumnWriter> writers = new List<ColumnWriter>();
            foreach (Column column in columns) {
                writers.Add(ColumnWriter.ForColumn(column));
            }
            return writers.ToArray();
        }

        /// <summary>
        /// Creates an array of column writers to match all columns in a table.
        /// </summary>
        /// <param name="table">Table whose columns you wish to write
        /// to.</param>
        /// <returns>An array of writers (each one created by the
        /// <see cref="ColumnWriter.ForColumn(Column)"/> method).</returns>
        public static ColumnWriter[] ForTable(StandaloneTable table)
        {
            if (table == null) {
                throw Exceptions.ArgumentNull("table");
            }

            ColumnWriter[] writers = new ColumnWriter[table.ColumnCount];
            for (int i = 0; i < table.ColumnCount; i++) {
                writers[i] = ColumnWriter.ForColumn(table.GetColumn(i));
            }
            return writers;
        }

        /// <summary>
        /// Writes the row's value as an object.
        /// </summary>
        /// <param name="row">The row to write to.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or not defined.</param>
        public abstract void SetValue(Row row, object value);

        /// <summary>
        /// Writes the row's value as an object.
        /// </summary>
        /// <param name="row">The index of the row to write to.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or not defined.</param>
        public abstract void SetValue(int row, object value);

        /// <summary>
        /// Writes the row's value in a series column as an object.
        /// </summary>
        /// <param name="row">The row to write to.</param>
        /// <param name="time">The sample time at which to write.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or not defined.</param>
        /// <remarks>
        /// <para>This method will throw an exception if the reader's column is
        /// not a series column.</para>
        /// </remarks>
        public abstract void SetValue(Row row, Time time, object value);

        /// <summary>
        /// Writes the row's value in a series column as an object.
        /// </summary>
        /// <param name="row">The index of the row to write to.</param>
        /// <param name="sample">The sample index at which to write.</param>
        /// <param name="value">The value as an object, or null if the value is
        /// missing, empty, or not defined.</param>
        /// <remarks>
        /// <para>This method will throw an exception if the reader's column is
        /// not a series column.</para>
        /// </remarks>
        public abstract void SetValue(int row, int sample, object value);

        /// <summary>
        /// Returns true if the writer's column is a series.
        /// </summary>
        public abstract bool IsSeries { get; }


        private sealed class CustomColumnWriter : ColumnWriter
        {
            private readonly CustomColumn column;

            public CustomColumnWriter(CustomColumn column)
            {
                this.column = column;
            }

            public override void SetValue(Row row, object value)
            {
                column.SetCustomValue(row, value);
            }

            public override void SetValue(int row, object value)
            {
                column.SetCustomValue(row, value);
            }

            public override void SetValue(Row row, Time time, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override void SetValue(int row, int sample, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }

        private sealed class NumericColumnWriter : ColumnWriter
        {
            private readonly NumericColumn column;

            public NumericColumnWriter(NumericColumn column)
            {
                this.column = column;
            }

            public override void SetValue(Row row, object value)
            {
                column.SetNumericValue(row, NumericValue.FromObject(value));
            }

            public override void SetValue(int row, object value)
            {
                column.SetNumericValue(row, NumericValue.FromObject(value));
            }

            public override void SetValue(Row row, Time time, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override void SetValue(int row, int sample, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }

        private sealed class NumericSeriesColumnWriter : ColumnWriter
        {
            private readonly NumericTimeseriesColumn column;

            public NumericSeriesColumnWriter(NumericTimeseriesColumn column)
            {
                this.column = column;
            }

            public override void SetValue(Row row, object value)
            {
                throw Exceptions.TableColumnIsSeries(column.Name);
            }

            public override void SetValue(int row, object value)
            {
                throw Exceptions.TableColumnIsSeries(column.Name);
            }

            public override void SetValue(Row row, Time time, object value)
            {
                column.SetNumericValue(row, time,
                    NumericValue.FromObject(value));
            }

            public override void SetValue(int row, int sample, object value)
            {
                column.SetNumericValue(row, sample,
                    NumericValue.FromObject(value));
            }

            public override bool IsSeries
            {
                get { return true; }
            }
        }

        private sealed class TextColumnWriter : ColumnWriter
        {
            private readonly TextColumn column;

            public TextColumnWriter(TextColumn column)
            {
                this.column = column;
            }

            public override void SetValue(Row row, object value)
            {
                string text = value as string;
                if (value != null && text == null) {
                    text = Convert.ToString(value);
                }
                column.SetTextValue(row, text);
            }

            public override void SetValue(int row, object value)
            {
                string text = value as string;
                if (value != null && text == null) {
                    text = Convert.ToString(value);
                }
                column.SetTextValue(row, text);
            }

            public override void SetValue(Row row, Time time, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override void SetValue(int row, int sample, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }

        private sealed class TextSeriesColumnWriter : ColumnWriter
        {
            private readonly TextTimeseriesColumn column;

            public TextSeriesColumnWriter(TextTimeseriesColumn column)
            {
                this.column = column;
            }

            public override void SetValue(Row row, object value)
            {
                throw Exceptions.TableColumnIsSeries(column.Name);
            }

            public override void SetValue(int row, object value)
            {
                throw Exceptions.TableColumnIsSeries(column.Name);
            }

            public override void SetValue(Row row, Time time, object value)
            {
                string text = value as string;
                if (value != null && text == null) {
                    text = Convert.ToString(value);
                }
                column.SetTextValue(row, time, text);
            }

            public override void SetValue(int row, int sample, object value)
            {
                string text = value as string;
                if (value != null && text == null) {
                    text = Convert.ToString(value);
                }
                column.SetTextValue(row, sample, text);
            }

            public override bool IsSeries
            {
                get { return true; }
            }
        }

        private sealed class TimeColumnWriter : ColumnWriter
        {
            private readonly TimeColumn column;

            public TimeColumnWriter(TimeColumn column)
            {
                this.column = column;
            }

            public override void SetValue(Row row, object value)
            {
                column.SetTimeValue(row, TimeValue.FromObject(value));
            }

            public override void SetValue(int row, object value)
            {
                column.SetTimeValue(row, TimeValue.FromObject(value));
            }

            public override void SetValue(Row row, Time time, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override void SetValue(int row, int sample, object value)
            {
                throw Exceptions.TableColumnIsNotSeries(column.Name);
            }

            public override bool IsSeries
            {
                get { return false; }
            }
        }
    }
}
