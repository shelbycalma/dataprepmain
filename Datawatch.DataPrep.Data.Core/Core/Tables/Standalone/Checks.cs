﻿using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    internal static class Checks
    {
        [Conditional("CHECKED")]
        public static void CheckColumnForRow(Row row, Column column)
        {
            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }
            if (column.Table == null) {
                throw Exceptions.TableColumnNotInTable(column.Name);
            }
            if (row.IsRemoved) {
                throw Exceptions.TableRowDeleted();
            }
            if (row.record >= column.Table.RowCount ||
                column.Table.GetRow(row.record) != row) {
                throw Exceptions.TableColumnInOtherTable(column.Name);
            }
        }

        [Conditional("CHECKED")]
        public static void CheckIRow(StandaloneTable table, IRow row)
        {
            if (row == null) {
                throw Exceptions.ArgumentNull("row");
            }
            Row standaloneRow = row as Row;
            if (standaloneRow == null) {
                throw Exceptions.TableRowWrongType(row.GetType());
            }
            if (standaloneRow.IsRemoved) {
                throw Exceptions.TableRowDeleted();
            }
            if (standaloneRow.record >= table.RowCount ||
                table.GetRow(standaloneRow.record) != row) {
                throw Exceptions.TableRowInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckIsUpdating(StandaloneTable table)
        {
            if (!table.IsUpdating) {
                throw Exceptions.TableMustCallBeginEndUpdate();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckITime(StandaloneTable table, ITime time)
        {
            if (time == null) {
                throw Exceptions.ArgumentNull("time");
            }
            Time standaloneTime = time as Time;
            if (standaloneTime == null) {
                throw Exceptions.TableTimeWrongType(time.GetType());
            }
            if (standaloneTime.IsRemoved) {
                throw Exceptions.TableTimeDeleted();
            }
            if (table.GetTime(standaloneTime.DateTime) != time) {
                throw Exceptions.TableTimeInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckNotFrozen(StandaloneTable table)
        {
            if (table.frozen) {
                throw Exceptions.TableFrozen();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckRow(StandaloneTable table, Row row)
        {
            if (row == null) {
                throw Exceptions.ArgumentNull("row");
            }
            if (row.IsRemoved) {
                throw Exceptions.TableRowDeleted();
            }
            if (row.record >= table.RowCount ||
                table.GetRow(row.record) != row) {
                throw Exceptions.TableRowInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckRowForTableRemove(
            StandaloneTable table, Row row)
        {
            if (row == null) {
                throw Exceptions.ArgumentNull("row");
            }
            if (row.IsRemoved) {
                throw Exceptions.TableRowAlreadyDeleted();
            }
            if (row.record >= table.RowCount ||
                table.GetRow(row.record) != row) {
                throw Exceptions.TableRowInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckRowIndex(StandaloneTable table, int row)
        {
            if (row < 0 || row >= table.RowCount) {
                throw Exceptions.TableRowIndexInvalid(
                    "row", row, table.RowCount);
            }
        }

        [Conditional("CHECKED")]
        public static void CheckSampleIndex(StandaloneTable table, int sample)
        {
            if (sample < 0 || sample >= table.TimeCount) {
                throw Exceptions.TableSampleIndexInvalid(
                    "sample", sample, table.TimeCount);
            }
        }

        [Conditional("CHECKED")]
        public static void CheckSnapshotSet(StandaloneTable table)
        {
            if (table.snapshot == null) {
                throw Exceptions.TableNoSnapshot();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckTableNotNull(Column column)
        {
            if (column.Table == null) {
                throw Exceptions.TableColumnNotInTable(column.Name);
            }
        }

        [Conditional("CHECKED")]
        public static void CheckTableNullOrUpdating(Column column)
        {
            if (column.Table == null) {
                return;
            }
            if (!column.Table.IsUpdating) {
                throw Exceptions.TableMustCallBeginEndUpdate();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckTime(StandaloneTable table, Time time)
        {
            if (time == null) {
                throw Exceptions.ArgumentNull("time");
            }
            if (time.IsRemoved) {
                throw Exceptions.TableTimeDeleted();
            }
            if (table.GetTime(time.DateTime) != time) {
                throw Exceptions.TableTimeInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckTimeForSnapshot(
            StandaloneTable table, Time time)
        {
            if (time == null) {
                return;
            }
            if (time.record < 0) {
                throw Exceptions.TableTimeDeleted();
            }
            if (table.GetTime(time.DateTime) != time) {
                throw Exceptions.TableTimeInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckTimeForTableRemove(
            StandaloneTable table, Time time)
        {
            if (time == null) {
                throw Exceptions.ArgumentNull("time");
            }
            if (time.record < 0) {
                throw Exceptions.TableTimeAlreadyDeleted();
            }
            if (table.GetTime(time.DateTime) != time) {
                throw Exceptions.TableTimeInOtherTable();
            }
        }

        [Conditional("CHECKED")]
        public static void CheckTimeIndex(StandaloneTable table, int index)
        {
            if (index < 0 || index >= table.TimeCount) {
                throw Exceptions.TableTimeIndexInvalid(
                    "index", index, table.TimeCount);
            }
        }
    }
}
