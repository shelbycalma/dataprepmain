﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A <see cref="StandaloneTable"/> column with constant value.
    /// </summary>
    /// <remarks>
    /// <para>You can use this class to represent read-only columns with a
    /// constant value for all rows. This uses a lot less memory that a regular
    /// column and is also faster.</para>
    /// </remarks>
    public abstract class ConstantColumn : Column, IIndexedColumn
    {
        /// <summary>
        /// Creates a new constant column.
        /// </summary>
        /// <param name="name">The column's name.</param>
        protected ConstantColumn(string name) : base(name)
        {
        }

        bool IIndexedColumn.CanGetIndex
        {
            get { return true; }
        }

        protected internal override void ClearRow(int row)
        {
        }

        protected internal override void CompactRows(int rows)
        {
        }

        KeyIndex IIndexedColumn.GetIndex(KeyIndex index)
        {
            return CreateIndex(table.RowCount, index);
        }

        KeyIndex IIndexedColumn.GetIndex(
            StorageInt rows, int count, KeyIndex index)
        {
            return CreateIndex(count, index);
        }

        private static KeyIndex CreateIndex(int count, KeyIndex index)
        {
            if (index == null) {
                index = new KeyIndex(count);
            } else {
                index.Keys.Capacity = count;
                index.Count = count;
            }
            index.Keys.Clear(0);
            index.MaxKey = 0;
            return index;
        }

        bool IIndexedColumn.IsIndexed
        {
            get { return true; }
        }

        bool IIndexedColumn.IsKey
        {
            get { return false; }
        }

        protected internal override void OnAddedToTable()
        {
        }

        protected internal override void OnRemovedFromTable()
        {
        }

        protected internal override int RowCapacity { get; set; }

        protected internal override void SetValueInternal(
            int row, object value)
        {
            throw new NotSupportedException();
        }


        /// <summary>
        /// A constant text column.
        /// </summary>
        public sealed class Text : ConstantColumn, ITextColumn
        {
            private readonly string value;

            /// <summary>
            /// Creates a new constant text column.
            /// </summary>
            /// <param name="name">The column's name.</param>
            /// <param name="value">The column's constant value (use null to
            /// get an "empty" column.</param>
            public Text(string name, string value) : base(name)
            {
                this.value = value;

                TextColumnMetaData meta = new TextColumnMetaData(this);
                meta.Comparer = new SingleValueComparer<string>(value);
                meta.Domain = new string[] { value };
                meta.Cardinality = 0;
                this.MetaData = meta;
            }

            /// <summary>
            /// Gets the constant value that the column will return
            /// for all rows.
            /// </summary>
            public string ConstantValue
            {
                get { return value; }
            }

            string ITextColumn.GetTextValue(IRow row)
            {
                Checks.CheckTableNotNull(this);
                Checks.CheckIRow(table, row);

                return value;
            }

            /// <summary>
            /// Returns this column's constant value.
            /// </summary>
            /// <param name="row">The row index to read.</param>
            /// <returns>Always the constant value.</returns>
            public string GetTextValue(int row)
            {
                Checks.CheckTableNotNull(this);
                Checks.CheckRowIndex(table, row);

                return value;
            }
        }

        /// <summary>
        /// A constant numeric column.
        /// </summary>
        public sealed class Numeric : ConstantColumn, INumericColumn
        {
            private readonly double value;

            /// <summary>
            /// Creates a new constant numeric column.
            /// </summary>
            /// <param name="name">The column's name.</param>
            /// <param name="value">The column's constant value (use
            /// <see cref="NumericValue.Empty"/> to get
            /// an "empty" column.</param>
            public Numeric(string name, double value) : base(name)
            {
                this.value = value;

                NumericColumnMetaData meta = new NumericColumnMetaData(this);
                meta.Format = null;
                meta.Domain = NumericValue.IsEmpty(value) ?
                    null : new NumericInterval(value, value);
                meta.Mean = value;
                meta.StandardDeviation = NumericValue.IsEmpty(value) ?
                    NumericValue.Empty : 0;
                this.MetaData = meta;
            }

            /// <summary>
            /// Gets the constant value that the column will return
            /// for all rows.
            /// </summary>
            public double ConstantValue
            {
                get { return value; }
            }

            double INumericColumn.GetNumericValue(IRow row)
            {
                Checks.CheckTableNotNull(this);
                Checks.CheckIRow(table, row);

                return value;
            }

            /// <summary>
            /// Returns this column's constant value.
            /// </summary>
            /// <param name="row">The row index to read.</param>
            /// <returns>Always the constant value.</returns>
            public double GetNumericValue(int row)
            {
                Checks.CheckTableNotNull(this);
                Checks.CheckRowIndex(table, row);

                return value;
            }
        }

        /// <summary>
        /// A constant time column.
        /// </summary>
        public sealed class Time : ConstantColumn, ITimeColumn
        {
            private readonly DateTime value;

            /// <summary>
            /// Creates a new constant time column.
            /// </summary>
            /// <param name="name">The column's name.</param>
            /// <param name="value">The column's constant value (use
            /// <see cref="TimeValue.Empty"/> to get an "empty" column.</param>
            public Time(string name, DateTime value) : base(name)
            {
                this.value = value;

                TimeColumnMetaData meta = new TimeColumnMetaData(this);
                meta.Format = null;
                meta.Domain = TimeValue.IsEmpty(value) ?
                    null : new TimeValueInterval(value, value);
                this.MetaData = meta;
            }

            /// <summary>
            /// Gets the constant value that the column will return
            /// for all rows.
            /// </summary>
            public DateTime ConstantValue
            {
                get { return value; }
            }

            DateTime ITimeColumn.GetTimeValue(IRow row)
            {
                Checks.CheckTableNotNull(this);
                Checks.CheckIRow(table, row);

                return value;
            }

            /// <summary>
            /// Returns this column's constant value.
            /// </summary>
            /// <param name="row">The row index to read.</param>
            /// <returns>Always the constant value.</returns>
            public DateTime GetTimeValue(int row)
            {
                Checks.CheckTableNotNull(this);
                Checks.CheckRowIndex(table, row);

                return value;
            }
        }


        private sealed class SingleValueComparer<T> : IComparer<T>
        {
            private readonly T value;

            public SingleValueComparer(T value)
            {
                this.value = value;
            }

            public int Compare(T x, T y)
            {
                Debug.Assert(Equals(x, value));
                Debug.Assert(Equals(y, value));

                return 0;
            }
        }
    }
}
