﻿using System.Collections.Generic;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// Utility class for copying data (and schema) from any source
    /// <see cref="ITable"/> into a <see cref="StandaloneTable"/>.
    /// </summary>
    public static class TableCopy
    {
        private static void CheckSameSamples(
            ITable source, StandaloneTable target)
        {
            ITimeseriesTable series = source as ITimeseriesTable;
            if (series == null) {
                return;
            }
            int samples = series.TimeCount;
            if (samples != target.TimeCount) {
                throw Exceptions.TableCopyDifferentSampleCounts(
                    samples, target.TimeCount);
            }
            for (int s = 0; s < samples; s++) {
                ITime st = series.GetTime(s);
                Time tt = target.GetTime(s);
                if (!st.Time.Equals(tt.DateTime)) {
                    throw Exceptions.TableCopyDifferentSamples(
                        s, st.Time, tt.DateTime);
                }
            }
        }

        private static void CheckSchema(ITable source, StandaloneTable target)
        {
            if (source.ColumnCount != target.ColumnCount) {
                throw Exceptions.TableCopyDifferentColumnCounts(
                    source.ColumnCount, target.ColumnCount);
            }
            for (int c = 0; c < source.ColumnCount; c++) {
                IColumn sourceColumn = source.GetColumn(c);
                Column targetColumn = target.GetColumn(sourceColumn.Name);
                if (targetColumn == null) {
                    throw Exceptions.TableCopySourceColumnMissingInTarget(
                        sourceColumn.Name);
                }
                bool ok = true;
                if (sourceColumn is ITextTimeseriesColumn) {
                    ok = targetColumn is TextTimeseriesColumn;
                } else if (sourceColumn is ITextColumn) {
                    ok = targetColumn is TextColumn;
                } else if (sourceColumn is INumericTimeseriesColumn) {
                    ok = targetColumn is NumericTimeseriesColumn;
                } else if (sourceColumn is INumericColumn) {
                    ok = targetColumn is NumericColumn;
                } else if (sourceColumn is ITimeColumn) {
                    ok = targetColumn is TimeColumn;
                }
                if (!ok) {
                    throw Exceptions.TableCopyDifferentColumnTypes(
                        sourceColumn.Name, sourceColumn.GetType(),
                        targetColumn.GetType());
                }
            }
        }

        /// <summary>
        /// Copies the entire contents (schema and data) of a
        /// <see cref="ITable"/> into an empty <see cref="StandaloneTable"/>.
        /// </summary>
        /// <param name="source">The source table.</param>
        /// <param name="target">The empty target table.</param>
        public static void CopyAll(ITable source, StandaloneTable target)
        {
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            if (ReferenceEquals(source, target)) {
                throw Exceptions.TableCopySameTable();
            }
            if (target.ColumnCount > 0 ||
                target.RowCount > 0 ||
                target.TimeCount > 0) {
                throw Exceptions.TableCopyTargetNotEmpty();
            }

            ISet<string> columns = GetAllColumnNames(source);
            CopySchemaInternal(source, target, columns);
            CopyDataInternal(source, target, columns);
        }

        /// <summary>
        /// Copies the data (rows) from a <see cref="ITable"/> into a
        /// <see cref="StandaloneTable"/> with the exact same schema.
        /// </summary>
        /// <param name="source">The source table.</param>
        /// <param name="target">The target table (must have the exact same
        /// schema as the source table).</param>
        public static void CopyData(ITable source, StandaloneTable target)
        {
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            if (ReferenceEquals(source, target)) {
                throw Exceptions.TableCopySameTable();
            }
            CheckSchema(source, target);

            CopyDataInternal(source, target, GetAllColumnNames(source));
        }

        /// <summary>
        /// Copies the data (rows) from a <see cref="ITable"/>
        /// <see cref="IColumn"/> into a <see cref="StandaloneTable"/>
        /// <see cref="Column"/>.
        /// </summary>
        /// <param name="source">The source column.</param>
        /// <param name="target">The target column.</param>
        /// <remarks>
        /// <para>The source column's table must have the same number of rows
        /// as the target column's table. If the source column is a series,
        /// then additionally the target table must have the same number of
        /// samples as the source table.</para>
        /// </remarks>
        public static void CopyData(IColumn source, Column target)
        {
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            if (ReferenceEquals(source, target)) {
                throw Exceptions.TableCopySameColumn(source.Name);
            }
            if (source.Table.RowCount != target.Table.RowCount) {
                throw Exceptions.TableCopyDifferentRowCounts(
                    source.Table.RowCount, target.Table.RowCount);
            }
            CheckSameSamples(source.Table, target.Table);

            CopyDataInternal(source, target);
        }

        private static void CopyDataInternal(ITable source,
            StandaloneTable target, ISet<string> columns)
        {
            int deltaRows = target.RowCount - source.RowCount;
            if (deltaRows < 0) {
                target.AddRows(-deltaRows);
            } else {
                for (int i = 0, j = target.RowCount - 1;
                    i < deltaRows; i++, j--) {
                    target.RemoveRow(target.GetRow(j));
                }
            }
            Debug.Assert(source.RowCount == target.RowCount);

            ITimeseriesTable series = source as ITimeseriesTable;
            if (series != null) {
                target.ClearTimes();
                for (int s = 0; s < series.TimeCount; s++) {
                    target.AddTime(series.GetTime(s).Time);
                }
                Debug.Assert(series.TimeCount == target.TimeCount);
            }
            
            for (int c = 0; c < source.ColumnCount; c++) {
                string name = source.GetColumn(c).Name;
                if (columns.Contains(name)) {
                    CopyDataInternal(source.GetColumn(c),
                        target.GetColumn(c));
                }
            }
        }

        private static void CopyDataInternal(IColumn source, Column target,
            int offset = 0)
        {
            Column standaloneSource = source as Column;
            // Non-zero offset means append to existing data, which is really
            // hard for indexed text. It's also complicated to implement for
            // any flat storage, so the advantage would be very small.
            if (standaloneSource != null && offset == 0) {
                CopyDataInternalFast(standaloneSource, target);
            } else {
                CopyDataInternalSlow(source, target, offset);
            }
        }

        private static void CopyDataInternalFast(Column source, Column target)
        {
            TextTimeseriesColumn textSeriesSource =
                source as TextTimeseriesColumn;
            if (textSeriesSource != null) {
                CopyDataInternalSlow(textSeriesSource,
                    (TextTimeseriesColumn) target, 0);
                return;
            }
            TextColumn textSource = source as TextColumn;
            if (textSource != null) {
                textSource.CopyTo((TextColumn) target);
                return;
            }
            NumericTimeseriesColumn numericSeriesSource =
                source as NumericTimeseriesColumn;
            if (numericSeriesSource != null) {
                CopyDataInternalSlow(numericSeriesSource,
                    (NumericTimeseriesColumn) target, 0);
                return;
            }
            NumericColumn numericSource = source as NumericColumn;
            if (numericSource != null) {
                numericSource.CopyTo((NumericColumn) target);
                return;
            }
            TimeColumn timeSource = source as TimeColumn;
            if (timeSource != null) {
                timeSource.CopyTo((TimeColumn) target);
                return;
            }
            Debug.Fail("Should not get called for other types.");
        }

        private static void CopyDataInternalSlow(IColumn source, Column target,
            int offset)
        {
            int rows = source.Table.RowCount;
            ITimeseriesTable series = source.Table as ITimeseriesTable;
            int samples = series != null ? series.TimeCount : 0;

            ITextTimeseriesColumn textSeriesSource =
                source as ITextTimeseriesColumn;
            if (textSeriesSource != null) {
                TextTimeseriesColumn textSeriesTarget =
                    (TextTimeseriesColumn) target;
                for (int s = 0; s < samples; s++) {
                    for (int r = 0; r < rows; r++) {
                        textSeriesTarget.SetTextValue(r + offset, s,
                            textSeriesSource.GetTextValue(r, s));
                    }
                }
                return;
            }
            ITextColumn textSource = source as ITextColumn;
            if (textSource != null) {
                TextColumn textTarget = (TextColumn) target;
                for (int r = 0; r < rows; r++) {
                    textTarget.SetTextValue(r + offset,
                        textSource.GetTextValue(r));
                }
                return;
            }
            INumericTimeseriesColumn numericSeriesSource =
                source as INumericTimeseriesColumn;
            if (numericSeriesSource != null) {
                NumericTimeseriesColumn numericSeriesTarget =
                    (NumericTimeseriesColumn) target;
                for (int s = 0; s < samples; s++) {
                    for (int r = 0; r < rows; r++) {
                        numericSeriesTarget.SetNumericValue(r + offset, s,
                            numericSeriesSource.GetNumericValue(r, s));
                    }
                }
                return;
            }
            INumericColumn numericSource = source as INumericColumn;
            if (numericSource != null) {
                NumericColumn numericTarget = (NumericColumn) target;
                for (int r = 0; r < rows; r++) {
                    numericTarget.SetNumericValue(r + offset,
                        numericSource.GetNumericValue(r));
                }
                return;
            }
            ITimeColumn timeSource = source as ITimeColumn;
            if (timeSource != null) {
                TimeColumn timeTarget = (TimeColumn) target;
                for (int r = 0; r < rows; r++) {
                    timeTarget.SetTimeValue(r + offset,
                        timeSource.GetTimeValue(r));
                }
                return;
            }
            Debug.Fail("Should not get called for other types.");
        }

        /// <summary>
        /// Copies the schema from a <see cref="ITable"/> into an empty
        /// <see cref="StandaloneTable"/>.
        /// </summary>
        /// <param name="source">The table to copy from.</param>
        /// <param name="target">An empty standalone table.</param>
        /// <remarks>
        /// <para>This method copies the source columns' meta data by
        /// reference into the target table. This means that if you change the
        /// meta data on the target table, it will also change the source
        /// table. If this is not what you intend, manually clone the meta
        /// data after copy.</para>
        /// </remarks>
        public static void CopySchema(ITable source, StandaloneTable target)
        {
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            if (source == target) {
                throw Exceptions.TableCopySameTable();
            }
            if (target.ColumnCount > 0 ||
                target.RowCount > 0 ||
                target.TimeCount > 0) {
                throw Exceptions.TableCopyTargetNotEmpty();
            }
            CopySchemaInternal(source, target, GetAllColumnNames(source));
        }

        private static void CopySchemaInternal(ITable source,
            StandaloneTable target, ISet<string> columns)
        {
            for (int c = 0; c < source.ColumnCount; c++) {
                IColumn sourceColumn = source.GetColumn(c);
                if (!columns.Contains(sourceColumn.Name)) {
                    continue;
                }
                Column targetColumn;
                if (sourceColumn is ITextTimeseriesColumn) {
                    targetColumn = new TextTimeseriesColumn(sourceColumn.Name);
                } else if (sourceColumn is ITextColumn) {
                    targetColumn = new TextColumn(sourceColumn.Name);
                } else if (sourceColumn is INumericTimeseriesColumn) {
                    targetColumn = new NumericTimeseriesColumn(sourceColumn.Name);
                } else if (sourceColumn is INumericColumn) {
                    targetColumn = new NumericColumn(sourceColumn.Name);
                } else if (sourceColumn is ITimeColumn) {
                    targetColumn = new TimeColumn(sourceColumn.Name);
                } else {
                    throw Exceptions.TableCopyUnknownSourceColumnType(
                        sourceColumn.Name, sourceColumn.GetType());
                }
                targetColumn.MetaData = sourceColumn.MetaData;
                target.AddColumn(targetColumn);
            }
        }

        /// <summary>
        /// Appends the data (rows) from a <see cref="ITable"/> into a
        /// <see cref="StandaloneTable"/> with the exact same schema.
        /// </summary>
        /// <param name="source">The source table.</param>
        /// <param name="target">The target table (must have the exact same
        /// schema as the source table).</param>
        public static void AppendData(ITable source, StandaloneTable target)
        {
            if (source == null)
            {
                throw Exceptions.ArgumentNull("source");
            }
            if (target == null)
            {
                throw Exceptions.ArgumentNull("target");
            }
            if (ReferenceEquals(source, target))
            {
                throw Exceptions.TableCopySameTable();
            }
            CheckSchema(source, target);
            CheckSameSamples(source, target);
            AppendDataInternal(source, target, GetAllColumnNames(source));
        }

        private static void AppendDataInternal(ITable source,
            StandaloneTable target, ISet<string> columns)
        {
            int originalRows = target.RowCount;
            target.AddRows(source.RowCount);
            Debug.Assert(originalRows + source.RowCount == target.RowCount);

            for (int c = 0; c < source.ColumnCount; c++) {
                string name = source.GetColumn(c).Name;
                if (columns.Contains(name)) {
                    CopyDataInternal(source.GetColumn(c),
                        target.GetColumn(c), originalRows);
                }
            }
        }

        /// <summary>
        /// Copies the entire contents (schema and data) for a select set of
        /// column in a <see cref="ITable"/> into an empty
        /// <see cref="StandaloneTable"/>.
        /// </summary>
        /// <param name="source">The source table.</param>
        /// <param name="target">The empty target table.</param>
        /// <param name="columns">The names of the source columns to
        /// copy.</param>
        public static void Project(ITable source,
                StandaloneTable target, string[] columns)
        {
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }
            if (target == null) {
                throw Exceptions.ArgumentNull("target");
            }
            if (source == target) {
                throw Exceptions.TableCopySameTable();
            }
            if (target.ColumnCount > 0 ||
                target.RowCount > 0 ||
                target.TimeCount > 0) {
                throw Exceptions.TableCopyTargetNotEmpty();
            }
            if (columns == null) {
                throw Exceptions.ArgumentNull("columns");
            }
            ISet<string> columnSet = new HashSet<string>();
            foreach (string column in columns) {
                if (column == null) {
                    throw Exceptions.ArgumentNull("columns");
                }
                if (source.GetColumn(column) == null) {
                    throw Exceptions.TableColumnNotInTable(column);
                }
                columnSet.Add(column);
            }

            CopySchemaInternal(source, target, columnSet);
            CopyDataInternal(source, target, columnSet);
        }

        private static ISet<string> GetAllColumnNames(ITable table)
        {
            ISet<string> names = new HashSet<string>();
            for (int c = 0; c < table.ColumnCount; c++) {
                names.Add(table.GetColumn(c).Name);
            }
            return names;
        }
    }
}
