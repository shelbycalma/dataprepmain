﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    public class TimeBucketTextColumnMetaData :
        TextColumnMetaData, ITimeBucketColumnMetaData
    {
        private readonly ITimeColumn timeColumn;
        private readonly TimePart timePart;

        public TimeBucketTextColumnMetaData(ITextColumn column, ITimeColumn timeColumn) : 
            this(column, timeColumn, TimePart.Decade)
        {
        }

        public TimeBucketTextColumnMetaData(ITextColumn column, 
            ITimeColumn timeColumn, TimePart timePart) : base(column)
        {
            this.timeColumn = timeColumn;
            this.timePart = timePart;
        }

        public IColumn AuxColumn
        {
            get { return timeColumn; }
        }

        public ITimeColumn TimeColumn
        {
            get { return timeColumn; }
        }

        public TimePart TimePart
        {
            get { return timePart; }
        }
    }
}
