﻿namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// Defines storage compression strategy for <see cref="TextColumn"/>s.
    /// </summary>
    public enum CompressionMode
    {
        /// <summary>
        /// Automatically switch between uncompressed and compressed
        /// storage based on the data (default).
        /// </summary>
        Automatic,

        /// <summary>
        /// Never use compressed storage for the column (use this for
        /// primary key type columns, or if speed is more important than
        /// memory footprint).
        /// </summary>
        Never,

        /// <summary>
        /// Always use compressed storage for the column (use this if you
        /// know in advance that the column has lots of duplicate data).
        /// </summary>
        Always
    }
}
