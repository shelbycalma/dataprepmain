using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A stand-alone implementation of a tabular data source.
    /// </summary>
    /// <remarks>
    /// <para>This table is a data source of its own - it has no dependencies
    /// on external data sources. Set up the table columns and add rows to
    /// use it.</para>
    /// </remarks>
    public class StandaloneTable :
        ITable,
        ITimeseriesTable,
        IFreezableTable
    {
        /// <summary>
        /// Raised to indicate that the <see cref="StandaloneTable"/> is
        /// about to get modified.
        /// </summary>
        public event EventHandler<TableChangingEventArgs> Changing;

        /// <summary>
        /// Raised to indicate that modifications to the
        /// <see cref="StandaloneTable"/> are complete.
        /// </summary>
        public event EventHandler<TableChangedEventArgs> Changed;

        private readonly List<Column> columns;
        private readonly Dictionary<string, int> columnLookup;
        private readonly ColumnCollection columnsWrapper;

        private readonly StoragePointer<Row> rows;
        private readonly List<Row> freeRows;
        private int nextRowRecord;
        private readonly RowCollection rowsWrapper;

        private int timeCapacity;
        private readonly List<Time> times;
        private readonly Dictionary<DateTime, Time> timeLookup;
        private readonly List<int> freeTimes;
        private int nextTimeRecord;
        internal Time snapshot;
        private readonly TimeCollection timesWrapper;

        // How many times have BeginUpdate been called?
        private int updateLocks;

        // In Begin/EndUpdate block? This flag will be set until Changed is
        // fired, so it's safer to test than the locks counter, which will be
        // decremented to zero at the start of EndUpdate - it will be zero
        // when the changed event is fired.
        private bool updating;

        private int version;

        private bool freezable;
        internal bool frozen;

        // Check storage when row count passes this threshold.
        private const int StorageCheckRowCount = 100;
        // Check storage after this many updates with rows in the table
        // and it hasn't already been checked.
        private const int StorageCheckEndUpdateCount = 10;
        // Has the storage been checked already? We only want to check it
        // once - the assumption is that the characteristics of column data
        // won't change, even if the table is cleared and repopulated.
        private bool storageAlreadyChecked;
        // Count of EndUpdate calls where there were rows in the table
        // but storage wasn't checked because the number of rows never
        // exceeded StorageCheckRowCount.
        private int updatesWithoutStorageCheck;
        

        public StandaloneTable() : this(32, 16)
        {
        }

        public StandaloneTable(int rowCapacity) : this(rowCapacity, 16)
        {
        }

        /// <summary>
        /// Creates a new instance of the standalone table.
        /// </summary>
        /// <exception cref="LicenseException">Thrown if no valid license
        /// could be found.</exception>
        public StandaloneTable(int rowCapacity, int timeCapacity)
        {
            columns = new List<Column>();
            columnLookup = new Dictionary<string, int>();
            columnsWrapper = new ColumnCollection(this);

            rows = new StoragePointer<Row>(rowCapacity);
            freeRows = new List<Row>();
            rowsWrapper = new RowCollection(this);

            this.timeCapacity = timeCapacity;
            times = new List<Time>();
            timeLookup = new Dictionary<DateTime, Time>();
            freeTimes = new List<int>();
            timesWrapper = new TimeCollection(this);

            version = 1;

            freezable = false;
        }

        [ModifiesTable]
        public Column AddColumn(Column column)
        {
            Checks.CheckIsUpdating(this);

            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }
            if (column.Table != null) {
                throw Exceptions.TableColumnInOtherTable(column.Name);
            }
            if (columnLookup.ContainsKey(column.Name)) {
                throw Exceptions.TableColumnNameExists(column.Name);
            }

            columnLookup.Add(column.Name, columns.Count);
            columns.Add(column);
            column.Table = this;

            return column;
        }

        [ModifiesTable]
        public Row AddRow()
        {
            Checks.CheckIsUpdating(this);

            if (nextRowRecord == StorageCheckRowCount) {
                // Someone is adding rows one-by-one, we now have enough rows
                // to check the storage, do it immediately rather than wait
                // for EndUpdate (there may be very many rows by then).
                if (freeRows.Count > 0) {
                    RemoveFreeRows();
                }
                for (int i = 0; i < columns.Count; i++) {
                    columns[i].CheckStorage(StorageCheckRowCount);
                }
                storageAlreadyChecked = true;
            }
            Row row;
            if (freeRows.Count == 0) {
                if (nextRowRecord == rows.Capacity) {
                    IncreaseRowCapacity();
                }
                int record = nextRowRecord++;
                row = new Row(record);
                rows[record] = row;
            } else {
                row = ReclaimFreeRow();
            }
            return row;
        }

        [ModifiesTable]
        public void AddRows(int count)
        {
            Checks.CheckIsUpdating(this);

            if (freeRows.Count > 0) {
                RemoveFreeRows();
            }
            int capacity = nextRowRecord + count;
            if (capacity > rows.Capacity) {
                rows.Capacity = capacity;
                for (int i = 0; i < columns.Count; i++) {
                    columns[i].RowCapacity = capacity;
                }
            }
            for (int i = 0; i < count; i++) {
                int record = nextRowRecord++;
                rows[record] = new Row(record);
            }
        }

        [ModifiesTable]
        public Row AddRow(object[] values)
        {
            Checks.CheckIsUpdating(this);

            if (values == null) {
                throw Exceptions.ArgumentNull("values");
            }

            Row row = AddRow();
            int record = row.record;
            for (int i = 0; i < values.Length; i++) {
                if (i >= columns.Count) {
                    break;
                }
                columns[i].SetValueInternal(record, values[i]);
            }
            return row;
        }

        [ModifiesTable]
        public Time AddTime(DateTime value)
        {
            Checks.CheckIsUpdating(this);

            if (timeLookup.ContainsKey(value)) {
                throw Exceptions.TableTimeExists(value.ToString());
            }
            
            int record;
            if (freeTimes.Count == 0) {
#if DEBUG
                for (int j = 0; j < times.Count; j++) {
                    Debug.Assert(times[j].record < nextTimeRecord);
                }
#endif
                record = nextTimeRecord++;
                if (nextTimeRecord > timeCapacity) {
                    IncreaseTimeCapacity();
                }
            } else {
                record = freeTimes[freeTimes.Count - 1];
                freeTimes.RemoveAt(freeTimes.Count - 1);
                for (int j = 0; j < columns.Count; j++) {
                    columns[j].ClearTime(record);
                }
            }
#if DEBUG
            for (int j = 0; j < times.Count; j++) {
                Debug.Assert(times[j].record != record);
            }
#endif

            Time time = new Time(value, record);
            int i = times.Count;
            while (i > 0 && times[i - 1].DateTime > value) {
                i--;
            }
            times.Insert(i, time);
            timeLookup.Add(value, time);
#if DEBUG
            for (int j = 0; j < times.Count - 1; j++) {
                Debug.Assert(times[j].DateTime < times[j + 1].DateTime);
            }
            int max = -1;
            for (int j = 0; j < times.Count; j++) {
                Debug.Assert(times[j].record >= 0);
                max = Math.Max(max, times[j].record);
            }
            Debug.Assert(nextTimeRecord > max);
#endif

            return time;
        }

        /// <summary>
        /// Puts the <see cref="StandaloneTable"/> in edit mode.
        /// </summary>
        public void BeginUpdate()
        {
            Checks.CheckNotFrozen(this);

            // Is this the first call (not nested)?
            if (Interlocked.Increment(ref updateLocks) == 1) {
                // Flag as updating.
                updating = true;
                // Notify listeners we're entering edit mode. If they look at
                // the version at this point, they'll see the old one.
                OnChanging(TableChangingEventArgs.Empty);
                // Increment version.
                version++;
            }
        }

        /// <summary>
        /// Gets or sets the <see cref="StandaloneTable"/>'s freezable state.
        /// </summary>
        /// <remarks>
        /// <para>The table is freezable by default. You can set this property
        /// to indicate to the table's consumers that the table may not be
        /// frozen, but you cannot change it once the table has been frozen.
        /// You need to call <see cref="BeginUpdate"/> before changing the
        /// property.</para>
        /// </remarks>
        public bool CanFreeze
        {
            get { return freezable; }
            
            [ModifiesTable] set {
                Checks.CheckIsUpdating(this);

                if (value != freezable) {
                    Checks.CheckNotFrozen(this);
                    freezable = value;
                }
            }
        }

        [ModifiesTable]
        public void ClearRows()
        {
            Checks.CheckIsUpdating(this);

            for (int i = 0; i < columns.Count; i++) {
                columns[i].ClearRows(nextRowRecord);
            }
            for (int i = 0; i < nextRowRecord; i++) {
                rows[i].record = -1;
                rows[i] = null;
            }
            freeRows.Clear();
            nextRowRecord = 0;
        }

        [ModifiesTable]
        public void ClearTimes()
        {
            Checks.CheckIsUpdating(this);

            for (int i = 0; i < columns.Count; i++) {
                columns[i].ClearTimes(nextTimeRecord);
            }
            for (int i = 0; i < times.Count; i++) {
                times[i].record = -1;
            }
            times.Clear();
            timeLookup.Clear();
            freeTimes.Clear();
            nextTimeRecord = 0;
        }

        public int ColumnCount
        {
            get { return columns.Count; }
        }

        /// <summary>
        /// Gets the table's columns.
        /// </summary>
        [Obsolete("This is kept for backwards compatibility. There are now " +
            "methods on the StandaloneTable itself that are faster than " +
            "going through this collection.")]
        public ColumnCollection Columns
        {
            get { return columnsWrapper; }
        }

        [ModifiesTable]
        public void Compact()
        {
            Checks.CheckIsUpdating(this);

            if (freeRows.Count > 0) {
                RemoveFreeRows();
            }
            rows.Capacity = nextRowRecord;
            for (int i = 0; i < columns.Count; i++) {
                columns[i].CompactRows(nextRowRecord);
            }

            if (freeTimes.Count > 0) {
                // TODO: Is this correct?
                RemoveFreeTimes();
            }
#if DEBUG
            int max = -1;
            for (int i = 0; i < times.Count; i++) {
                max = Math.Max(max, times[i].record);
            }
            Debug.Assert(max == nextTimeRecord - 1);
#endif
            Debug.Assert(times.Count == nextTimeRecord);
            for (int i = 0; i < columns.Count; i++) {
                columns[i].CompactTimes(nextTimeRecord);
            }
        }

        public bool ContainsColumn(string name)
        {
            if (name == null) {
                throw Exceptions.ArgumentNull("name");
            }
            return columnLookup.ContainsKey(name);
        }

        public bool ContainsTime(DateTime time)
        {
            return timeLookup.ContainsKey(time);
        }

        /// <summary>
        /// Takes the <see cref="StandaloneTable"/> out of edit mode.
        /// </summary>
        public void EndUpdate()
        {
            // Unbalanced Begin/EndUpdate calls.
            if (!updating) {
                throw Exceptions.TableUnlockedTooManyTimes();
            }

            // Was this the final lock?
            if (Interlocked.Decrement(ref updateLocks) <= 0)
            {
                if (freeRows.Count > 0) {
                    RemoveFreeRows();
                }
                if (freeTimes.Count > 0) {
                    RemoveFreeTimes();
                }

                if (!storageAlreadyChecked) {
                    if (nextRowRecord >= StorageCheckRowCount) {
                        for (int i = 0; i < columns.Count; i++) {
                            columns[i].CheckStorage(nextRowRecord);
                        }
                        storageAlreadyChecked = true;
                    }
                    else if (nextRowRecord > 0 &&
                        ++updatesWithoutStorageCheck ==
                            StorageCheckEndUpdateCount) {
                        for (int i = 0; i < columns.Count; i++) {
                            columns[i].CheckStorage(nextRowRecord);
                        }
                        storageAlreadyChecked = true;
                    }
                }

                // Give calculated columns a chance to update any
                // internal state.
                for (int i = 0; i < columns.Count; i++) {
                    columns[i].Recalculate();
                }

                try {
                    OnChanged(TableChangedEventArgs.Empty);
                }
                finally {
                    // Mark as no longer updating.
                    updating = false;
                }
            }
        }

        private void FreeRow(Row row)
        {
            rows[row.record] = null;
            row.record = -row.record - 1;
            // TODO: Implement as heap.
            int i = freeRows.Count;
            freeRows.Add(row);
            while (i > 0 && freeRows[i - 1].record > freeRows[i].record) {
                int t = freeRows[i - 1].record;
                freeRows[i - 1].record = freeRows[i].record;
                freeRows[i].record = t;
                i--;
            }
        }

        /// <summary>
        /// Freezes the <see cref="StandaloneTable"/>. This optimizes the
        /// table's memory use, but makes it read-only.
        /// </summary>
        /// <remarks>
        /// <para>Check the <see cref="CanFreeze"/> property before calling
        /// this method.</para>
        /// <para>This method implicitly calls <see cref="BeginUpdate()"/>,
        /// <see cref="Compact()"/>, and <see cref="EndUpdate()"/>.</para>
        /// </remarks>
        [ModifiesTable]
        public void Freeze()
        {
            if (!freezable) {
                throw Exceptions.TableNotFreezable();
            }
            if (!frozen) {
                BeginUpdate();
                try {
                    Compact();
                    for (int i = 0; i < columns.Count; i++) {
                        columns[i].Freeze();
                    }
                    frozen = true;
                } finally {
                    EndUpdate();
                }
            }
        }

        public Column GetColumn(int index)
        {
            return columns[index];
        }

        IColumn ITable.GetColumn(int index)
        {
            return columns[index];
        }

        public Column GetColumn(string name)
        {
            if (name == null) {
                throw Exceptions.ArgumentNull("name");
            }
            int index;
            if (columnLookup.TryGetValue(name, out index)) {
                return columns[index];
            }
            return null;
        }

        IColumn ITable.GetColumn(string name)
        {
            return GetColumn(name);
        }

        // Funny result when freeRows.Count > 0.
        public Row GetRow(int index)
        {
            Checks.CheckRowIndex(this, index);

            return rows[index];
        }

        // Funny result when freeRows.Count > 0.
        IRow ITable.GetRow(int index)
        {
            Checks.CheckRowIndex(this, index);

            return rows[index];
        }

        public Time GetTime(int index)
        {
            Checks.CheckTimeIndex(this, index);

            return times[index];
        }

        ITime ITimeseriesTable.GetTime(int index)
        {
            Checks.CheckTimeIndex(this, index);

            return times[index];
        }

        public Time GetTime(DateTime time)
        {
            Time t;
            timeLookup.TryGetValue(time, out t);
            return t;
        }

        // TODO: Bigger steps at start.
        // TODO: Account for difference in column storage types - maybe let
        // column decide capacity for themselves?
        private void IncreaseRowCapacity()
        {
            int capacity = rows.Capacity;
            if (capacity > StorageInt.SegmentSize) {
                capacity = StorageInt.SegmentSize *
                    ((capacity / StorageInt.SegmentSize) + 1);
            } else {
                capacity = Math.Max(256, capacity << 1);
            }
            Debug.Assert(capacity > 0);
            Debug.Assert(capacity > rows.Capacity);
            rows.Capacity = capacity;
            for (int i = 0; i < columns.Count; i++) {
                columns[i].RowCapacity = capacity;
            }
        }

        private void IncreaseTimeCapacity()
        {
            int capacity = timeCapacity;
            if (capacity > 256) {
                capacity = 256 * ((capacity / 256) + 1);
            } else {
                capacity = Math.Max(1, capacity << 1);
            }
            Debug.Assert(capacity > 0);
            Debug.Assert(capacity > timeCapacity);
            timeCapacity = capacity;
            for (int i = 0; i < columns.Count; i++) {
                columns[i].TimeCapacity = capacity;
            }
        }

        public bool IsFrozen
        {
            get { return frozen; }
        }

        /// <summary>
        /// Checks if the <see cref="StandaloneTable"/> is in edit mode.
        /// </summary>
        public bool IsUpdating
        {
            get { return updating; }
        }

        /// <summary>
        /// Fires the <see cref="Changing"/> event.
        /// </summary>
        /// <param name="e">Data for the event.</param>
        protected virtual void OnChanging(TableChangingEventArgs e)
        {
            if (this.Changing != null) {
                this.Changing(this, e);
            }
        }

        /// <summary>
        /// Fires the <see cref="Changed"/> event.
        /// </summary>
        /// <param name="e">Data for the event.</param>
        protected virtual void OnChanged(TableChangedEventArgs e)
        {
            if (this.Changed != null) {
                this.Changed(this, e);
            }
        }

        private Row ReclaimFreeRow()
        {
            Row row = freeRows[freeRows.Count - 1];
            freeRows.RemoveAt(freeRows.Count - 1);
            row.record = -(row.record + 1);
            Debug.Assert(rows[row.record] == null);
            rows[row.record] = row;
            for (int i = 0; i < columns.Count; i++) {
                columns[i].ClearRow(row.record);
            }
            return row;
        }

        // OPTIMIZE: That comment.
        [ModifiesTable]
        public void RemoveColumn(Column column)
        {
            Checks.CheckIsUpdating(this);

            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }
            if (column.Table == null) {
                throw Exceptions.TableColumnNotInTable(column.Name);
            }
            if (column.Table != this) {
                throw Exceptions.TableColumnInOtherTable(column.Name);
            }

            // SHOULD CHECK IF THE COLUMN IS REMOVABLE (Flag usedByHierarchy)?
            // Before removing from the index.
            // VISION no. 3398

            int index = columnLookup[column.Name];
            columnLookup.Remove(column.Name);
            columns.RemoveAt(index);
            for (int i = index; i < columns.Count; i++) {
                columnLookup[columns[i].Name] = i;
            }

            column.Table = null;

            // Later on, in OnChanged:
            // THROWS EXCEPTION IF THE COLUMN IS USED IN THE HIERARCHY!
            // columns.index will be affected before this exception => shouldn't
            // column.OnRemovedFromTable() will be called before this exception => shouldn't
        }

        private void RemoveFreeRows()
        {
            // Be nice. Keep row object identity.

            int holeCount = freeRows.Count;

            // Get the hole indices in increasing order (they are in
            // decreasing order in freeRows).
            int[] holes = new int[holeCount + 1];
            for (int i = 0; i < holeCount; i++) {
                holes[i] = -(freeRows[holeCount - 1 - i].record + 1);
            }
            // Set last element to row + hole count.
            holes[holeCount] = nextRowRecord;            
            freeRows.Clear();

            // Decrement record indices on rows.
            for (int i = 0; i < holeCount; i++) {
                // Fore each run between holes, including the run between
                // the last hole and the last row.
                int first = holes[i];
                int last = holes[i + 1];
                // Number of steps to decrement index (the same as
                // the number of holes we have passed).
                int steps = i + 1;
                for (int j = first + 1; j < last; j++) {
                    rows[j].record -= steps;
                }
            }
            
            // Remove holes from the rows storage.
            rows.Compact(holes);

            // Remove holes from column storages.
            for (int k = 0; k < columns.Count; k++) {
                columns[k].DefragmentRows(holes);
            }
            
            // Reset next record.
            nextRowRecord -= holeCount;
            // Set holes at end to null (they should already be, since the
            // StoragePointer defaults to reusing on compact).
            rows.Clear(nextRowRecord, holeCount);
        }

        private void RemoveFreeTimes()
        {
            Debug.Assert(freeTimes.Count > 0);
            Debug.Assert(nextTimeRecord == times.Count + freeTimes.Count);

            int holeCount = freeTimes.Count;

            // TODO: Handle holes > time count.
            int[] holes = new int[holeCount + 1];
            freeTimes.CopyTo(holes, 0);
            Array.Sort(holes, 0, holeCount);
            holes[holeCount] = nextTimeRecord;
            freeTimes.Clear();
            
            // TODO: Yuck.
            for (int i = holeCount - 1; i >= 0; i--) {
                for (int j = 0; j < times.Count; j++) {
                    if (times[j].record > holes[i]) {
                        times[j].record--;
                    }
                }
            }

            for (int k = 0; k < columns.Count; k++) {
                columns[k].DefragmentTimes(holes);
            }
            nextTimeRecord = times.Count;
        }

        [ModifiesTable]
        public void RemoveRow(Row row)
        {
            Checks.CheckIsUpdating(this);
            Checks.CheckRowForTableRemove(this, row);

            Debug.Assert(!freeRows.Contains(row));

            FreeRow(row);
        }

        [ModifiesTable]
        public void RemoveTime(Time time)
        {
            Checks.CheckIsUpdating(this);
            Checks.CheckTimeForTableRemove(this, time);

            Debug.Assert(time.record >= 0);
            Debug.Assert(!freeTimes.Contains(time.record));

            times.Remove(time);
            timeLookup.Remove(time.DateTime);

            freeTimes.Add(time.record);
            time.record = -1;
        }

        public int RowCapacity
        {
            get { return rows.Capacity; }

            [ModifiesTable] set {
                Checks.CheckIsUpdating(this);

                Debug.Assert(freeRows.Count == 0);
                
                if (value < nextRowRecord) {
                    throw Exceptions.TableRowCapacityInvalid(
                        value, nextRowRecord);
                }
                rows.Capacity = value;
                for (int i = 0; i < columns.Count; i++) {
                    columns[i].RowCapacity = value;
                }
            }
        }

        // Funny value when freeRows.Count > 0.
        // NOTE: LegacyRowEnumerator below uses this.
        public int RowCount
        {
            get { return nextRowRecord; }
        }

        /// <summary>
        /// Gets the table's rows.
        /// </summary>
        [Obsolete("This is kept for backwards compatibility. There are now " +
            "methods on the StandaloneTable itself that are faster than " +
            "going through this collection.")]
        public RowCollection Rows
        {
            get { return rowsWrapper; }
        }

        public Time SnapshotTime
        {
            get { return snapshot; }

            [ModifiesTable] set {
                Checks.CheckIsUpdating(this);
                Checks.CheckTimeForSnapshot(this, value);

                Debug.Assert(value == null || times.Contains(value));

                snapshot = value;
            }
        }

        ITime ITimeseriesTable.SnapshotTime
        {
            get { return snapshot; }
        }

        public int TimeCapacity
        {
            get { return timeCapacity; }

            [ModifiesTable] set {
                Checks.CheckIsUpdating(this);
                
                Debug.Assert(freeTimes.Count == 0);
                
                // TODO: Throw TableTimeCapacityInvalid.
                if (value != timeCapacity) {
#if DEBUG
                    int max = -1;
                    for (int i = 0; i < times.Count; i++) {
                        max = Math.Max(max, times[i].record);
                    }
                    Debug.Assert(value > max);
#endif
                    timeCapacity = value;
                    for (int i = 0; i < columns.Count; i++) {
                        columns[i].TimeCapacity = value;
                    }
                }
            }
        }

        public int TimeCount
        {
            get { return times.Count; }
        }

        /// <summary>
        /// Gets the <see cref="Time"/> collection 
        /// for this <see cref="StandaloneTable"/>.
        /// </summary>
        [Obsolete("This is kept for backwards compatibility. There are now " +
            "methods on the StandaloneTable itself that are faster than " +
            "going through this collection.")]
        public TimeCollection Times
        {
            get { return timesWrapper; }
        }

        /// <summary>
        /// Gets the table's version, which is a number that auto increments
        /// each time edits to the table are started with
        /// <see cref="BeginUpdate()"/>.
        /// </summary>
        public int Version
        {
            get { return version; }
        }

        internal IEnumerator<Column> LegacyGetColumnEnumerator()
        {
            return columns.GetEnumerator();
        }

        internal IEnumerator<Row> LegacyGetRowEnumerator()
        {
            return new LegacyRowEnumerator(this);
        }

        internal IEnumerator<Time> LegacyGetTimeEnumerator()
        {
            return times.GetEnumerator();
        }


        private class LegacyRowEnumerator : IEnumerator<Row>
        {
            private readonly StandaloneTable table;
            private int currentIndex;

            public LegacyRowEnumerator(StandaloneTable table)
            {
                this.table = table;
                currentIndex = -1;
            }

            public Row Current
            {
                get {
                    if (currentIndex >= 0) {
                        return table.rows[currentIndex];
                    }
                    return null;
                }
            }

            object IEnumerator.Current
            {
                get { return this.Current; }
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (currentIndex < -1) {
                    return false;
                }
                if (++currentIndex >= table.nextRowRecord) {
                    currentIndex = -2;
                    return false;
                }
                return true;
            }

            public void Reset()
            {
                currentIndex = -1;
            }
        }
    }
}
