using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A row in a <see cref="StandaloneTable"/>.
    /// </summary>
    [DebuggerDisplay("{record}")]
    public class Row : IRow
    {
        internal int record;

        internal Row(int record)
        {
            this.record = record;
        }

        /// <summary>
        /// This property is <c>true</c> if this row has been removed from the
        /// table.
        /// </summary>
        /// <remarks>
        /// <para>You cannot access the cell values - get or set - in a row
        /// that has been removed.</para>
        /// </remarks>
        public bool IsRemoved
        {
            get { return record < 0; }
        }
    }
}
