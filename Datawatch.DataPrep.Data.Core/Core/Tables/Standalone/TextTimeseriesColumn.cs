using System;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A time series <see cref="StandaloneTable"/> column of type
    /// <see cref="string"/>.
    /// </summary>
    public class TextTimeseriesColumn : Column,
        ITextTimeseriesColumn
    {
        private SmallStorage<StoragePointer<string>> storage;
        private int rowCapacity;

        /// <summary>
        /// Creates an instance of the <see cref="TextTimeseriesColumn"/> class.
        /// </summary>
        /// <param name="name">Column name.</param>
        public TextTimeseriesColumn(string name) : base(name)
        {
            MetaData = 
                new TextColumnMetaData(this)
                {
                    Comparer = StringComparer.CurrentCultureIgnoreCase
                };
        }

        protected internal override void ClearRow(int row)
        {
            Debug.Assert(table != null);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i][row] = null;
            }
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= rowCapacity);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i].Clear(0, rows);
            }
        }

        internal override void ClearTime(int record)
        {
            Debug.Assert(table != null);

            storage[record].Clear();
        }

        internal override void ClearTimes(int times)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(times <= storage.Capacity);

            int rows = table.RowCount;
            for (int i = 0; i < times; i++) {
                storage[i].Clear(0, rows);
            }
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= rowCapacity);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i].Capacity = rows;
            }
            rowCapacity = rows;
        }

        protected internal override void CompactTimes(int times)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(times <= storage.Capacity);

            storage.Capacity = times;
        }

        // TODO: What happens here if there are zero times?
        internal override void DefragmentRows(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(holes[holes.Length - 1] <= rowCapacity);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i].Compact(holes);
            }
        }

        internal override void DefragmentTimes(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(holes[holes.Length - 1] <= storage.Capacity);

            storage.Compact(holes);
        }

        // Not required by interface, ITextTimeseriesColumn is not
        // a ITextColumn.
        public string GetTextValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);
            Checks.CheckSnapshotSet(table);

            return storage[table.snapshot.record][row.record];
        }

        // Not required by interface, ITextTimeseriesColumn is not
        // a ITextColumn.
        public string GetTextValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSnapshotSet(table);

            return storage[table.snapshot.record][row];
        }

        /// <summary>
        /// Returns the text value of this column for a specific
        /// <see cref="Row"/> and <see cref="Time"/>.
        /// </summary>
        /// <param name="row">Row to get the value for.</param>
        /// <param name="time">Time to get the value for.</param>
        /// <returns>The value.</returns>
        public string GetTextValue(Row row, Time time)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);
            Checks.CheckTime(table, time);
            
            return storage[time.record][row.record];
        }

        /// <summary>
        /// Returns the text value of this column for a specific
        /// <see cref="IRow"/> and <see cref="ITime"/>.
        /// </summary>
        /// <param name="row">Row to get the value for.</param>
        /// <param name="time">Time to get the value for.</param>
        /// <returns>The value.</returns>
        string ITextTimeseriesColumn.GetTextValue(IRow row, ITime time)        
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);
            Checks.CheckITime(table, time);

            return storage[((Time) time).record][((Row) row).record];
        }

        public string GetTextValue(int row, int sample)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSampleIndex(table, sample);

            // TODO: Make Time.record == index, then remove.
            Time time = table.GetTime(sample);
            return storage[time.record][row];
        }
        
        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);

            rowCapacity = table.RowCapacity;
            int times = table.TimeCapacity;
            storage = new SmallStorage<StoragePointer<string>>(times);
            for (int i = 0; i < times; i++) {
                storage[i] = new StoragePointer<string>(rowCapacity);
            }
        }
        
        /// <summary>
        /// Clears the <see cref="TimeColumn"/>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
            storage = null;
        }

        protected internal override int RowCapacity
        {
            get {
                Debug.Assert(table != null);
                return rowCapacity;
            }
            set {
                Debug.Assert(table != null);
                for (int i = 0; i < storage.Capacity; i++) {
                    storage[i].Capacity = value;
                }
                if (value > rowCapacity) {
                    int added = value - rowCapacity;
                    for (int i = 0; i < storage.Capacity; i++) {
                        storage[i].Clear(rowCapacity, added);
                    }
                }
                rowCapacity = value;
            }
        }

        [ModifiesTable]
        // Not required by interface (see GetTextValue(Row)).
        public void SetTextValue(Row row, string value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);
            Checks.CheckSnapshotSet(table);

            storage[table.snapshot.record][row.record] = value;
        }

        [ModifiesTable]
        // Not required by interface (see GetTextValue(Row)).
        public void SetTextValue(int row, string value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSnapshotSet(table);

            storage[table.snapshot.record][row] = value;
        }

        /// <summary>
        /// Sets the text value of this column for a particular 
        /// <see cref="Row"/> and <see cref="Time"/>.
        /// </summary>
        /// <param name="row">Row to set value for.</param>
        /// <param name="time">Time to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetTextValue(Row row, Time time, string value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);
            Checks.CheckTime(table, time);

            storage[time.record][row.record] = value;
        }

        /// <summary>
        /// Sets the text value of this column for a particular row and sample.
        /// </summary>
        /// <param name="row">Row index to set value for.</param>
        /// <param name="sample">Sample index to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetTextValue(int row, int sample, string value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSampleIndex(table, sample);

            // TODO: Make Time.record == index, then remove!
            Time time = table.GetTime(sample);
            storage[time.record][row] = value;
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            Checks.CheckSnapshotSet(table);

            string text = value != null ? value.ToString() : null;

            storage[table.snapshot.record][row] = text;
        }

        protected internal override int TimeCapacity
        {
            get {
                Debug.Assert(table != null);
                return storage.Capacity;
            }
            set {
                Debug.Assert(table != null);
                int oldCapacity = storage.Capacity;
                storage.Capacity = value;
                for (int i = oldCapacity; i < value; i++) {
                    storage[i] = new StoragePointer<string>(rowCapacity);
                }
            }
        }
    }
}
