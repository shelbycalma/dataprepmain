﻿using System;
using System.IO;
using Datawatch.DataPrep.Data.Core.Tables.Standalone.Serialization;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// Provides serialization for <see cref="StandaloneTable"/>s.
    /// </summary>
    public class Serializer
    {
        /// <summary>
        /// Reads a table that was serialized with
        /// <see cref="WriteBinary(Stream, ITable)"/> from a stream.
        /// </summary>
        /// <param name="input">The stream to read from (must support
        /// seeking).</param>
        /// <returns>The table as a standalone table.</returns>
        public static StandaloneTable ReadBinary(Stream input)
        {
            if (input == null) {
                throw new ArgumentNullException("input");
            }
            return ReadBinary(input, -1);
        }

        /// <summary>
        /// Reads a maximum number of rows of a table that was serialized with
        /// <see cref="WriteBinary(Stream, ITable)"/> from a stream.
        /// </summary>
        /// <param name="input">The stream to read from (must support
        /// seeking).</param>
        /// <param name="maxRows">The maximum number of rows to read (can be
        /// zero to just read the schema).</param>
        /// <returns>The table as a standalone table (with no more than
        /// <paramref name="maxRows"/> rows int it).</returns>
        public static StandaloneTable ReadBinary(Stream input, int maxRows)
        {
            if (input == null) {
                throw new ArgumentNullException("input");
            }
            BinaryTableReader reader = new BinaryTableReader(input);
            return reader.Read(maxRows);
        }

        /// <summary>
        /// Writes a table to a stream in a binary format.
        /// </summary>
        /// <param name="output">The stream to write to (must support
        /// seeking).</param>
        /// <param name="table">The table to serialize (any implementation of
        /// the <see cref="ITable"/> interface).</param>
        public static void WriteBinary(Stream output, ITable table)
        {
            if (output == null) {
                throw new ArgumentNullException("output");
            }
            if (table == null) {
                throw new ArgumentNullException("table");
            }
            BinaryTableWriter writer = new BinaryTableWriter(output);
            writer.Write(table);
            writer.Flush();
        }
    }
}
