﻿using System;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// This attribute marks all methods in the <see cref="StandaloneTable"/>
    /// API that modifies the table.
    /// </summary>
    /// <remarks>
    /// <para>Any method with this attribute must be called in a
    /// <see cref="StandaloneTable.BeginUpdate()"/> /
    /// <see cref="StandaloneTable.EndUpdate()"/> pair, or the table
    /// will throw a runtime exception.</para>
    /// </remarks>
    [AttributeUsage(AttributeTargets.Method,
        AllowMultiple = false, Inherited = true)]
    public class ModifiesTableAttribute : Attribute
    {
    }
}
