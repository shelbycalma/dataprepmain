using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Storage;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A time series <see cref="StandaloneTable"/> column of type
    /// <see cref="double"/>.
    /// </summary>
    public class NumericTimeseriesColumn : Column,
        INumericTimeseriesColumn
    {
        private SmallStorage<StorageDouble> storage;
        private int rowCapacity;

        /// <summary>
        /// Creates an instance of the <see cref="NumericTimeseriesColumn"/> class.
        /// </summary>
        /// <param name="name">Column name.</param>
        public NumericTimeseriesColumn(string name) : base(name)
        {
            MetaData = new NumericColumnMetaData(this);
        }

        protected internal override void ClearRow(int row)
        {
            Debug.Assert(table != null);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i][row] = NumericValue.Empty;
            }
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= rowCapacity);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i].Clear(0, rows, NumericValue.Empty);
            }
        }

        internal override void ClearTime(int record)
        {
            Debug.Assert(table != null);

            storage[record].Clear(NumericValue.Empty);
        }

        internal override void ClearTimes(int times)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(times <= storage.Capacity);

            int rows = table.RowCount;
            for (int i = 0; i < times; i++) {
                storage[i].Clear(0, rows, NumericValue.Empty);
            }
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(rows <= rowCapacity);

            for (int i = 0; i < storage.Capacity; i++) {
                storage[i].Capacity = rows;
            }
            rowCapacity = rows;
        }

        protected internal override void CompactTimes(int times)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(times <= storage.Capacity);

            storage.Capacity = times;
        }

        // TODO: What happens here if there are zero times?
        internal override void DefragmentRows(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(holes[holes.Length - 1] <= rowCapacity);

            // Defragment each time slice.
            for (int i = 0; i < storage.Capacity; i++) {
                storage[i].Compact(holes);
            }
        }

        internal override void DefragmentTimes(int[] holes)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
            Debug.Assert(holes[holes.Length - 1] <= storage.Capacity);

            storage.Compact(holes);
        }

        public double GetNumericValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);
            Checks.CheckSnapshotSet(table);

            return storage[table.snapshot.record][row.record];
        }

        double INumericColumn.GetNumericValue(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);
            Checks.CheckSnapshotSet(table);

            return storage[table.snapshot.record][((Row) row).record];
        }

        public double GetNumericValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSnapshotSet(table);

            return storage[table.snapshot.record][row];
        }

        /// <summary>
        /// Returns the numeric value of this column for a specific
        /// <see cref="Row"/> and <see cref="Time"/>.
        /// </summary>
        /// <param name="row">Row to get the value for.</param>
        /// <param name="time">Time to get the value for.</param>
        /// <returns>The numeric value.</returns>
        public double GetNumericValue(Row row, Time time)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);
            Checks.CheckTime(table, time);

            return storage[time.record][row.record];
        }

        /// <summary>
        /// Returns the numeric value of this column for a specific
        /// <see cref="IRow"/> and <see cref="ITime"/>.
        /// </summary>
        /// <param name="row">Row to get the value for.</param>
        /// <param name="time">Time to get the value for.</param>
        /// <returns>The numeric value.</returns>
        double INumericTimeseriesColumn.GetNumericValue(IRow row, ITime time)        
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);
            Checks.CheckITime(table, time);

            return storage[((Time) time).record][((Row) row).record];
        }

        public double GetNumericValue(int row, int sample)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSampleIndex(table, sample);

            // TODO: Make Time.record == index, then remove.
            Time time = table.GetTime(sample);
            return storage[time.record][row];
        }
        
        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);

            rowCapacity = table.RowCapacity;
            int times = table.TimeCapacity;
            storage = new SmallStorage<StorageDouble>(times);
            for (int i = 0; i < times; i++) {
                storage[i] = new StorageDouble(rowCapacity);
                storage[i].Clear(NumericValue.Empty);
            }
        }
        
        /// <summary>
        /// Clears the <see cref="TimeColumn"/>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
            storage = null;
        }

        protected internal override int RowCapacity
        {
            get {
                Debug.Assert(table != null);
                return rowCapacity;
            }
            set {
                Debug.Assert(table != null);
                for (int i = 0; i < storage.Capacity; i++) {
                    storage[i].Capacity = value;
                }
                if (value > rowCapacity) {
                    int added = value - rowCapacity;
                    for (int i = 0; i < storage.Capacity; i++) {
                        storage[i].Clear(rowCapacity, added,
                            NumericValue.Empty);
                    }
                }
                rowCapacity = value;
            }
        }

        [ModifiesTable]
        public void SetNumericValue(Row row, double value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);
            Checks.CheckSnapshotSet(table);

            storage[table.snapshot.record][row.record] = value;
        }

        [ModifiesTable]
        public void SetNumericValue(int row, double value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSnapshotSet(table);

            storage[table.snapshot.record][row] = value;
        }

        /// <summary>
        /// Sets the numeric value of this column for a particular 
        /// <see cref="Row"/> and <see cref="Time"/>.
        /// </summary>
        /// <param name="row">Row to set value for.</param>
        /// <param name="time">Time to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetNumericValue(Row row, Time time, double value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRow(table, row);
            Checks.CheckTime(table, time);

            storage[time.record][row.record] = value;
        }

        /// <summary>
        /// Sets the numeric value of this column for a particular
        /// row and sample.
        /// </summary>
        /// <param name="row">Row index to set value for.</param>
        /// <param name="sample">Sample index to set value for.</param>
        /// <param name="value">Value to set.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        public void SetNumericValue(int row, int sample, double value)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIsUpdating(table);
            Checks.CheckRowIndex(table, row);
            Checks.CheckSampleIndex(table, sample);

            // TODO: Make Time.record == index, then remove!
            Time time = table.GetTime(sample);
            storage[time.record][row] = value;
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            Checks.CheckSnapshotSet(table);

            double numeric = NumericValue.FromObject(value);

            storage[table.snapshot.record][row] = numeric;
        }

        protected internal override int TimeCapacity
        {
            get {
                Debug.Assert(table != null);
                return storage.Capacity;
            }
            set {
                Debug.Assert(table != null);
                int oldCapacity = storage.Capacity;
                storage.Capacity = value;
                for (int i = oldCapacity; i < value; i++) {
                    storage[i] = new StorageDouble(rowCapacity);
                    storage[i].Clear(NumericValue.Empty);
                }
            }
        }
    }
}
