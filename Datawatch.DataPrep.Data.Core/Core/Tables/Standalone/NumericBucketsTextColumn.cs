using System;
using System.Collections.Generic;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    
    /// <summary>
    /// A <see cref="StandaloneTable"/> column of type <see cref="string"/>
    /// based on a <see cref="NumericColumn"/>, where
    /// <see cref="NumericInterval">numeric intervals</see> corresponds to
    /// text values (so called buckets). Apart from bucketing this is also known
    /// as binning or discretization.
    /// </summary>
    public class NumericBucketsTextColumn : Column,
        ITextColumn
    {
        private readonly NumericColumn column;
        private readonly NumericInterval[] intervals;
        private readonly TextDomain domain;

		/// <summary>
		/// Creates a new instance of the
        /// <see cref="NumericBucketsTextColumn"/>.
        /// </summary>
        /// <param name="name">The name of column.</param>
        /// <param name="column">The numeric column on which the column should
        /// be based.</param>
        /// <param name="intervals">The intervals defining the buckets (in
        /// acending order).</param>
        /// <param name="textValues">The text values of the buckets.</param>
        public NumericBucketsTextColumn(string name, NumericColumn column,
            NumericInterval[] intervals, string[] textValues)
            : base(name)
        {
            if (column == null) {
                throw Exceptions.ArgumentNull("column");
            }
            if (intervals == null) {
                throw Exceptions.ArgumentNull("intervals");
            }
            if (textValues == null) {
                throw Exceptions.ArgumentNull("textValues");
            }

            this.column = column;
            this.intervals = (NumericInterval[]) intervals.Clone();
            domain = new TextDomain(textValues);

		    MetaData =
		        new TextColumnMetaData(this)
		            {
		                Domain = textValues,
		                Comparer = new NumericBucketComparer(
                            new TextDomain(textValues))
		            };
        }

        protected internal override void ClearRow(int row)
        {
            Debug.Assert(table != null);
        }

        internal override void ClearRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
        }

        protected internal override void CompactRows(int rows)
        {
            Debug.Assert(table != null);
            Debug.Assert(table.IsUpdating);
        }

        /// <summary>
        /// Returns the domain of the <see cref="NumericBucketsTextColumn"/>.
        /// </summary>
        /// <returns>The domain of the column in the form 
        /// of an array of strings.</returns>
        /// <remarks>
        /// <para>This method will go through all the rows in the table and
        /// calculate the domain based the values that are currently in there,
        /// so it is a slow operation and should be avoided if possible.</para>
        /// </remarks>
        public string[] GetCurrentDomain()
        {
            HashSet<string> domain = new HashSet<string>();
            for (int row = 0; row < table.RowCount; row++) {
                double value = column.GetNumericValue(row);
                domain.Add(GetTextValue(value));
            }
            string[] array = new string[domain.Count];
            domain.CopyTo(array, 0);
            return array;
        }

        private string GetTextValue(double value)
        {
            for (int i = 0; i < intervals.Length; i++) {
                if (value <= intervals[i].Max &&
                    value >= intervals[i].Min) {
                    return domain[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the value of the <see cref="NumericBucketsTextColumn"/> 
        /// for a particular <see cref="Row"/>.
        /// </summary>
        /// <param name="row"><see cref="Row"/> to return the value for.</param>
        /// <returns>String value.</returns>
        public string GetTextValue(Row row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRow(table, row);

            double value = column.GetNumericValue(row);

            return GetTextValue(value);
        }

        /// <summary>
        /// Returns the value of the <see cref="ITextColumn"/> 
        /// for a particular <see cref="IRow"/>.
        /// </summary>
        /// <param name="row"><see cref="IRow"/> to return the value for.</param>
        /// <returns>String value.</returns>
        string ITextColumn.GetTextValue(IRow row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckIRow(table, row);

            double value = column.GetNumericValue((Row) row);

            return GetTextValue(value);
        }

        public string GetTextValue(int row)
        {
            Checks.CheckTableNotNull(this);
            Checks.CheckRowIndex(table, row);

            double value = column.GetNumericValue(row);

            return GetTextValue(value);
        }

        /// <summary>
        /// Returns the <see cref="NumericColumn"/> on which 
        /// this <see cref="NumericBucketsTextColumn"/> is based.
        /// </summary>
        public NumericColumn NumericColumn
        {
            get { return column; }
        }

        /// <summary>
        /// Returns the numeric intervals defining this 
        /// <see cref="NumericBucketsTextColumn"/>.
        /// </summary>
        public NumericInterval[] NumericIntervals
        {
            get { return (NumericInterval[]) intervals.Clone(); }
        }

        /// <summary>
        /// Internal event handler.
        /// </summary>
        protected internal override void OnAddedToTable()
        {
        }
        
        /// <summary>
        /// Clears this <see cref="NumericBucketsTextColumn"/>.
        /// </summary>
        protected internal override void OnRemovedFromTable()
        {
        }

        protected internal override int RowCapacity
        {
            get { return 0; }
            set { }
        }

        protected internal override void SetValueInternal(int row, object value)
        {
            throw new NotSupportedException();
        }


        private sealed class NumericBucketComparer : IComparer<string>
        {
            private readonly TextDomain domain;

            public NumericBucketComparer(TextDomain domain)
            {
                this.domain = domain;
            }

            public int Compare(string x, string y)
            {
                int xIndex = x != null ? domain.GetIndex(x) : -2;
                int yIndex = y != null ? domain.GetIndex(y) : -2;

                if (xIndex < yIndex) {
                    return -1;
                }
                if (xIndex > yIndex) {
                    return 1;
                }
                return 0;
            }
        }
    }
}
