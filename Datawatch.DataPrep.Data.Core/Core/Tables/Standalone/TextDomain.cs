using System.Collections.Generic;
using System.Diagnostics;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    // TODO: This is less useful now since we're not validating values against
    // the domain anymore. All it does now is help the NumericBucketComparer.

    // Utility class used by text columns to store the meta data domain
    // and validate cell values. This class must allow null values, because
    // values are not validated when rows are added - e.g. with Rows.Add() -
    // so there's no point in handling null here.
    internal class TextDomain
    {
        private readonly string[] values;

        private readonly Dictionary<string, int> lookup;
        private int nullIndex;

        public TextDomain(string[] values)
        {
            Debug.Assert(values != null);

            this.values = (string[]) values.Clone();

            lookup = new Dictionary<string, int>(values.Length);
            nullIndex = -1;

            for (int i = 0; i < values.Length; i++) {
                string value = values[i];
                if (value == null) {
                    nullIndex = i;
                } else if (!lookup.ContainsKey(value)) {
                    lookup.Add(value, i);
                }
            }
        }

        public bool Contains(string value)
        {
            if (value == null) {
                return nullIndex >= 0;
            }
            return lookup.ContainsKey(value);
        }

        public int GetIndex(string value)
        {
            if (value == null) {
                return nullIndex;
            }
            int index;
            if (lookup.TryGetValue(value, out index)) {
                return index;
            }
            return -1;
        }

        public string this[int index]
        {
            get { return values[index]; }
        }

        public string[] Values
        {
            get { return (string[]) values.Clone(); }
        }
    }
}
