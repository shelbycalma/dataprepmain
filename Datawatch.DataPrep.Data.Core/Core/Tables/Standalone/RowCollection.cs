using System;
using System.Collections;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Tables.Standalone
{
    /// <summary>
    /// A collection of <see cref="Row">rows</see>.
    /// </summary>
    /// <remarks>
    /// <para>You cannot create instances of this class externally. It is used
    /// by the <see cref="StandaloneTable"/> in its
    /// <see cref="StandaloneTable.Rows"/> property.</para>
    /// </remarks>
    public class RowCollection : IEnumerable<Row>
    {
        private readonly StandaloneTable table;

        internal RowCollection(StandaloneTable table)
        {
            this.table = table;
        }

        /// <summary>
        /// Creates and adds a new row to the table.
        /// </summary>
        /// <returns>A new, initially empty row.</returns>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.AddRow()")]
        public Row Add()
        {
            return table.AddRow();
        }

        /// <summary>
        /// Creates a row, sets any number of supplied values, and adds it to
        /// the table.
        /// </summary>
        /// <param name="values">Values to initialize the row with.</param>
        /// <returns>The new row.</returns>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// <para>The <paramref name="values"/> array can be any length but the
        /// order of the values must match the order and type of the columns in
        /// the <see cref="StandaloneTable"/>. If fewer values are supplied than
        /// the number of columns, the remaining ones will be left empty. If
        /// more values are supplied than there are columns they will be
        /// ignored.</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.AddRow(object[])")]
        public Row Add(object[] values)
        {
            return table.AddRow(values);
        }

        /// <summary>
        /// Clears this <see cref="RowCollection"/>
        /// </summary>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.ClearRows()")]
        public void Clear()
        {
            table.ClearRows();
        }

        /// <summary>
        /// Gets the number of rows currently in the collection.
        /// </summary>
        [Obsolete("Replaced by StandaloneTable.RowCount")]
        public int Count
        {
            get { return table.RowCount; }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the rows.
        /// </summary>
        /// <returns>A row enumerator.</returns>
        [Obsolete("It's faster to use StandaloneTable.RowCount with " +
            "StandaloneTable.GetRow(int).")]
        public IEnumerator<Row> GetEnumerator()
        {
            return table.LegacyGetRowEnumerator();
        }

        /// <summary>
        /// Returns the <see cref="RowCollection"/> enumerator.
        /// </summary>
        /// <returns>Enumerator</returns>
        [Obsolete("Use StandaloneTable.RowCount with " +
            "StandaloneTable.GetRow(int) instead.")]
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Removes a <see cref="Row"/> from the <see cref="RowCollection"/>.
        /// </summary>
        /// <param name="row"><see cref="Row"/> to remove.</param>
        /// <remarks>
        /// <para>This method modifies the <see cref="StandaloneTable"/>, so you
        /// need to call <see cref="StandaloneTable.BeginUpdate()"/> before you
        /// call it (and then <see cref="StandaloneTable.EndUpdate()"/>
        /// afterwards).</para>
        /// </remarks>
        [ModifiesTable]
        [Obsolete("Replaced by StandaloneTable.RemoveRow(Row)")]
        public void Remove(Row row)
        {
            table.RemoveRow(row);
        }

        /// <summary>
        /// Gets the row at the specified index.
        /// </summary>
        /// <param name="index">Index of row to retrieve.</param>
        /// <returns>The row at the index.</returns>
        [Obsolete("Replaced by StandaloneTable.GetRow(int)")]
        public Row this[int index]
        {
            get { return table.GetRow(index); }
        }
    }
}
