using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    /// <summary>
    /// <see cref="ITable"/> implementation  wrapping an ADO
    /// <see cref="DataTable"/> in order to expose ADO data to
    /// the framework without the need of any data replication.
    /// </summary>
    public class AdoTable :
        ITable,
        IDisposable
    {
        /// <summary>
        /// Raised to indicate that the <see cref="AdoTable"/> has
        /// entered batch mode.
        /// </summary>
        public event EventHandler<TableChangingEventArgs> Changing;

        /// <summary>
        /// Raised to indicate that the <see cref="AdoTable"/>
        /// is exiting batch mode.
        /// </summary>
        public event EventHandler<TableChangedEventArgs> Changed;

        private readonly DataTable source;
        private readonly Dictionary<DataColumn, AdoColumn> columns;
        private readonly Dictionary<DataRow, AdoRow> rows;
        private readonly DataView dataView;

        private DateTime timeSpanBaseTime;

        private readonly AdoBatchingHelper batching;

        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdoTable"/> type, 
        /// passing the <see cref="DataTable"/> to wrap in the source parameter.
        /// </summary>
        /// <param name="source"></param>
        public AdoTable(DataTable source)
        {
            if (source == null)
            {
                throw Exceptions.ArgumentNull("source");
            }
            
            this.source = source;

            timeSpanBaseTime = new DateTime(1900, 1, 1);
            
            columns = new Dictionary<DataColumn, AdoColumn>();
            foreach (DataColumn column in source.Columns)
            {
                columns.Add(column,
                    AdoColumnFactory.CreateWrapper(this, column));
            }

            dataView = new DataView(source);
            dataView.RowStateFilter = DataViewRowState.CurrentRows;

            this.rows = new Dictionary<DataRow, AdoRow>();
            foreach (DataRowView rowView in dataView)
            {
                rows.Add(rowView.Row, new AdoRow(rowView.Row));
            }

            batching = new AdoBatchingHelper(this);

            source.RowChanging += source_RowChanging;
            source.RowChanged += source_RowChanged;
            source.RowDeleting += source_RowDeleting;
            source.RowDeleted += source_RowDeleted;
            source.ColumnChanging += source_ColumnChanging;
            source.ColumnChanged += source_ColumnChanged;
            source.TableCleared += source_TableClearing;
            source.TableCleared += source_TableCleared;
            source.Columns.CollectionChanged += sourceColumns_CollectionChanged;
        }

        /// <summary>
        /// Destructor for the table.
        /// </summary>
        ~AdoTable()
        {
            Dispose(false);
        }

        private void AddRow(DataRow dataRow)
        {
            AdoRow adoRow = new AdoRow(dataRow);
            rows.Add(dataRow, adoRow);
        }

        internal AdoBatchingHelper Batching
        {
            get { return batching; }
        }

        /// <summary>
        /// Maintains performance when updating a large amout of rows in the 
        /// <see cref="DataTable"/> by preventing events being fired until the 
        /// <see cref="EndUpdate"/> method is called.
        /// </summary>
        public void BeginUpdate()
        {
            batching.BeginUpdate();
        }

        /// <summary>
        /// Gets the number of <see cref="AdoColumn">columns</see> in the table.
        /// </summary>
        public int ColumnCount
        {
            get { return source.Columns.Count; }
        }

        /// <summary>
        /// Releases event handlers for garbage collection.
        /// </summary>
         /// <seealso cref="IDisposable"/> 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases event handlers from the source <see cref="DataTable"/>.
        /// </summary>
        /// <param name="disposing">True if the method is called from 
        /// user code, false if not.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    source.RowChanged -= source_RowChanged;
                    source.ColumnChanged -= source_ColumnChanged;
                    source.TableCleared -= source_TableCleared;
                    source.Columns.CollectionChanged -=
                        sourceColumns_CollectionChanged;
                }
                disposed = true;
            }
        }

        /// <summary>
        /// Resumes event fireing of the <see cref="AdoTable"/> after having 
        /// been suspended by the <see cref="BeginUpdate"/> method.
        /// </summary>
        public void EndUpdate()
        {
            batching.EndUpdate();
        }

        /// <summary>
        /// Retrieves the <see cref="AdoColumn"/> at the specified index.
        /// </summary>
        /// <param name="index">A column index.</param>
        /// <returns>An <see cref="AdoColumn"/></returns>
        public AdoColumn GetColumn(int index)
        {
            return columns[source.Columns[index]];
        }

        IColumn ITable.GetColumn(int index)
        {
            return GetColumn(index);
        }

        /// <summary>
        /// Retrieves the <see cref="AdoColumn"/> with the specified name.
        /// </summary>
        /// <param name="name">Name of column to get.</param>
        /// <returns>The column if found, <c>null</c> otherwise.</returns>
        public AdoColumn GetColumn(string name)
        {
            if (name == null) {
                throw Exceptions.ArgumentNull("name");
            }
            DataColumn column = source.Columns[name];
            if (column != null) {
                return columns[column];
            }
            return null;
        }

        IColumn ITable.GetColumn(string name)
        {
            return GetColumn(name);
        }

        internal AdoRow GetRow(DataRow sourceRow)
        {
            AdoRow adoRow = null;
            rows.TryGetValue(sourceRow, out adoRow);
            return adoRow;
        }

        /// <summary>
        /// Retrieves the <see cref="AdoRow"/> at the specified index.
        /// </summary>
        /// <param name="index">A row index.</param>
        /// <returns>An <see cref="AdoRow"/>.</returns>
        public AdoRow GetRow(int index)
        {
            return rows[dataView[index].Row];
        }

        IRow ITable.GetRow(int index)
        {
            return GetRow(index);
        }

        /// <summary>
        /// Checks if the <see cref="AdoTable"/> is in batch update mode.
        /// </summary>
        public bool IsUpdating
        {
            get { return batching.IsUpdating; }
        }

        /// <summary>
        /// Fires the <see cref="Changing"/> event.
        /// </summary>
        /// <param name="e">Data for the event.</param>
        internal virtual void OnChanging(TableChangingEventArgs e)
        {
            if (this.Changing != null)
            {
                this.Changing(this, e);
            }
        }

        /// <summary>
        /// Fires the <see cref="Changed"/> event.
        /// </summary>
        /// <param name="e">Data for the event.</param>
        internal virtual void OnChanged(TableChangedEventArgs e)
        {
            if (this.Changed != null)
            {
                this.Changed(this, e);
            }
        }

        private void RemoveRow(DataRow dataRow)
        {
            AdoRow adoRow = rows[dataRow];
            rows.Remove(dataRow);
            adoRow.SourceRow = null;
        }

        /// <summary>
        /// Gets the number of <see cref="AdoRow">rows</see> in the table.
        /// </summary>
        public int RowCount
        {
            get { return dataView.Count; }
        }

        /// <summary>
        /// Gets the <see cref="DataTable"/> instance wrapped by this 
        /// <see cref="AdoTable"/>.
        /// </summary>
        public DataTable SourceTable
        {
            get { return source; }
        }

        private void source_ColumnChanging(object sender,
            DataColumnChangeEventArgs e)
        {
            if (rows.ContainsKey(e.Row)) {
                batching.BeginUpdate();
            }
        }

        private void source_ColumnChanged(object sender,
            DataColumnChangeEventArgs e)
        {
            if (rows.ContainsKey(e.Row)) {
                batching.EndUpdate();
            }
        }

        private void sourceColumns_CollectionChanged(object sender,
            CollectionChangeEventArgs e)
        {
            DataColumn col = e.Element as DataColumn;
            AdoColumn adoColumn;

            switch (e.Action)
            {
                case CollectionChangeAction.Add:
                    batching.BeginUpdate();
                    adoColumn = AdoColumnFactory.CreateWrapper(this, col);
                    columns.Add(col, adoColumn);
                    batching.EndUpdate();
                break;

                case CollectionChangeAction.Remove:
                    batching.BeginUpdate();
                    adoColumn = columns[col];
                    columns.Remove(col);
                    adoColumn.Table = null;
                    // The following warning is copied from StandaloneTable:
                    // THROWS EXCEPTION IF THE COLUMN IS USED IN THE HIERARCHY!
                    // columns.index will be affected before this exception => shouldn't
                    // column.OnRemovedFromTable() will be called before this exception => shouldn't
                    batching.EndUpdate();
                break;

                case CollectionChangeAction.Refresh:
                    // All elements were removed (and e.Element is null).
                    batching.BeginUpdate();
                    AdoColumn[] adoColumns = new AdoColumn[columns.Count];
                    columns.Values.CopyTo(adoColumns, 0);
                    columns.Clear();
                    foreach (AdoColumn ado in adoColumns) {
                        ado.Table = null;
                    }
                    batching.EndUpdate();
                break;
            }
        }

        void source_RowChanged(object sender, DataRowChangeEventArgs e)
        {
            if (e.Action == DataRowAction.Add) {
                // A row has been added, e.g. table.AddRow().
                AddRow(e.Row);
                this.Batching.EndUpdate();
            }
        }

        void source_RowChanging(object sender, DataRowChangeEventArgs e)
        {
            switch (e.Action)
            {
                case DataRowAction.Rollback:
                    if (e.Row.RowState == DataRowState.Added) {
                        RemoveRow(e.Row);
                    }
                    else if (e.Row.RowState == DataRowState.Deleted) {
                        AddRow(e.Row);
                    }
                    break;

                case DataRowAction.Add:
                    // A row is about to be added, e.g. table.AddRow().
                    this.Batching.BeginUpdate();
                    break;

                case DataRowAction.Delete:
                    // TODO: We're not getting this.
                    //this.Batching.BeginUpdate();
                    break;
            }
        }

        void source_RowDeleting(object sender, DataRowChangeEventArgs e)
        {
            // A row is about to be deleted, e.g. table.Rows.RemoveAt().
            batching.BeginUpdate();
        }

        void source_RowDeleted(object sender, DataRowChangeEventArgs e)
        {
            Debug.Assert(e.Action == DataRowAction.Delete);
            // Row has been deleted, e.g. table.Rows.RemoveAt().
            RemoveRow(e.Row);
            batching.EndUpdate();
        }

        void source_TableCleared(object sender, DataTableClearEventArgs e)
        {
            AdoRow[] adoRows = new AdoRow[rows.Count];
            rows.Values.CopyTo(adoRows, 0);
            rows.Clear();
            foreach (AdoRow adoRow in adoRows) {
                adoRow.SourceRow = null;
            }
            batching.EndUpdate();
        }

        void source_TableClearing(object sender, DataTableClearEventArgs e)
        {
            // All rows are about to be deleted, e.g. table.Rows.Clear(),
            // or table.Clear().
            batching.BeginUpdate();
        }

        /// <summary>
        /// Base date when converting from time spans (default is
        /// January 1 1900).
        /// </summary>
        /// <remarks>
        /// <para>ADO columns that have the <see cref="TimeSpan"/> data type
        /// (e.g. SQL Server TIME columns) will be represented as
        /// <see cref="AdoTimeColumn"/>s by the <see cref="AdoTable"/>. To
        /// convert from TimeSpans to <see cref="DateTime"/>s, it adds them to
        /// this base date.</para>
        /// </remarks>
        public DateTime TimeSpanBaseTime
        {
            get { return timeSpanBaseTime; }
            set { timeSpanBaseTime = value; }
        }
    }
}
