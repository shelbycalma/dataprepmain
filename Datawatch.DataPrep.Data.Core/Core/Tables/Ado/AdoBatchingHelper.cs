﻿using System.Threading;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    internal class AdoBatchingHelper
    {
        private readonly AdoTable table;

        // How many times have BeginUpdate been called?
        private int locks;

        // In Begin/EndUpdate block? This flag will be set until Changed is
        // fired, so it's safer to test than the locks counter, which will be
        // decremented to zero at the start of EndUpdate - it will be zero
        // while the summary events are fired.
        private bool updating;

        public AdoBatchingHelper(AdoTable table)
        {
            this.table = table;
        }

        public void BeginUpdate()
        {
            // Is this the first call (not nested)?
            if (Interlocked.Increment(ref locks) == 1) {
                // Flag as updating.
                updating = true;
                // Notify listeners we're entering batch mode.
                table.OnChanging(TableChangingEventArgs.Empty);
            }
        }

        public void EndUpdate()
        {
            // Unbalanced Begin/EndUpdate calls.
            if (!updating) {
                throw Exceptions.TableUnlockedTooManyTimes();
            }

            // Was this the final lock?
            if (Interlocked.Decrement(ref locks) <= 0)
            {
                table.OnChanged(TableChangedEventArgs.Empty);

                // Mark as no longer updating.
                updating = false;
            }
        }

        public bool IsUpdating
        {
            get { return updating; }
        }
    }
}
