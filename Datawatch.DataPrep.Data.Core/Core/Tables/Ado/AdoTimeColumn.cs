using System;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    /// <summary>
    /// Subclass of the <see cref="AdoColumn"/> type for wrapping 
    /// <see cref="DataColumn"/> instances where the data type is 
    /// <see cref="DateTime"/>.
    /// </summary>
    public class AdoTimeColumn :
        AdoColumn,
        ITimeColumn,
        ITimeColumnMetaData
    {
        private string metaFormat;

        internal AdoTimeColumn(AdoTable table, DataColumn source)
            : base(table, source)
        {
        }

        /// <summary>
        /// Gets the actual data type of the column (DateTime).
        /// </summary>
        public override Type DataType
        {
            get { return typeof(DateTime); }
        }

        /// <summary>
        /// Returns the <see cref="DateTime">time value</see> of this 
        /// <see cref="AdoTimeColumn"/> 
        /// for a specific <see cref="AdoRow"/>.
        /// </summary>
        /// <param name="row"><see cref="AdoRow"/> to get the 
        /// value for.</param>
        /// <returns><see cref="DateTime">Time value.</see></returns>
        public DateTime GetTimeValue(AdoRow row)
        {
            return GetTimeValueInternal(CheckIRow(row));
        }

        DateTime ITimeColumn.GetTimeValue(IRow row)
        {
            return GetTimeValueInternal(CheckIRow(row));
        }

        public DateTime GetTimeValue(int row)
        {
            CheckRowIndex(row);

            // TODO: Do this better.
            return GetTimeValueInternal(this.Table.GetRow(row));
        }

        private DateTime GetTimeValueInternal(AdoRow row)
        {
            DateTime time;
            lock (this.Table.SourceTable) {
                object value = row.SourceRow[this.SourceColumn];
                if (value is DBNull) {
                    time = TimeValue.Empty;
                } else if (value is DateTime) {
                    time = (DateTime) value;
                } else {
                    TimeSpan span = (TimeSpan) value;
                    time = this.Table.TimeSpanBaseTime + span;
                }
            }
            return time;
        }

        TimeValueInterval ITimeColumnMetaData.Domain
        {
            get { return null; }
        }

        string ITimeColumnMetaData.Format
        {
            get { return metaFormat; }
        }

        /// <summary>
        /// Gets the format of this <see cref="AdoTimeColumn"/>.
        /// </summary>
        public string MetaFormat
        {
            get { return metaFormat; }
            set { metaFormat = value; }
        }
    }
}