using System;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    /// <summary>
    /// Base type for <see cref="IColumn"/> implementations wrapping 
    /// <see cref="DataColumn">ADO columns</see>.
    /// </summary>
    public abstract class AdoColumn :
        IColumn,
        IColumnMetaData
    {
        private AdoTable table;
        private readonly DataColumn source;
        private bool hidden;
        private string title;


        internal AdoColumn(AdoTable table, DataColumn source)
        {
            this.table = table;
            this.source = source;
            this.hidden = false;
            this.title = source.Caption;
        }

        internal AdoRow CheckIRow(IRow row)
        {
            if (row == null) {
                throw Exceptions.ArgumentNull("row");
            }
            AdoRow adoRow = row as AdoRow;
            if (adoRow == null) {
                throw Exceptions.TableRowWrongType(row.GetType());
            }
            if (this.Table.GetRow(adoRow.SourceRow) != adoRow) {
                throw Exceptions.TableRowInOtherTable();
            }
            return adoRow;
        }

        internal void CheckRowIndex(int row)
        {
            if (row < 0 || row >= table.RowCount) {
                throw Exceptions.TableRowIndexInvalid(
                    "row", row, table.RowCount);
            }
        }

        /// <summary>
        /// Gets the type of the data stored in the column.
        /// </summary>
        public abstract Type DataType
        {
            get; 
        }

        /// <summary>
        /// Gets a boolean value indicating whether
        /// this column is hidden or not.
        /// </summary>
        /// <remarks>
        /// <para>A hidden column will not be displayed in any 
        /// listing of columns provided by SDK controls.</para>
        /// </remarks>
        public virtual bool Hidden
        {
            get { return hidden; }
            set { hidden = value; }
        }

        /// <summary>
        /// Gets the column's meta data.
        /// </summary>
        public virtual IColumnMetaData MetaData
        {
            get { return this; }
        }

        /// <summary>
        /// Gets the column's name.
        /// </summary>
        /// <remarks>
        /// <para>The name of the column acts as identifier and is unique among
        /// columns from the same table.</para>
        /// <para>Columns may have a more user-friendly name that should be
        /// used for presentation, see
        /// <see cref="IColumnMetaData.Title"/>.</para>
        /// </remarks>
        public string Name
        {
            get { return source.ColumnName; }
        }

        /// <summary>
        /// Gets the wrapped ADO <see cref="DataColumn"/> instance.
        /// </summary>
        public DataColumn SourceColumn
        {
            get { return source; }
        }

        /// <summary>
        /// Gets an instance of the containing <see cref="AdoTable"/>.
        /// </summary>
        public AdoTable Table
        {
            get { return table; }
            internal set { table = value; }
        }

        /// <summary>
        /// Gets the table that the column belongs to.
        /// </summary>
        ITable IColumn.Table
        {
            get { return this.Table; }
        }

        /// <summary>
        /// Gets or sets the column's title.
        /// </summary>
        /// <remarks>
        /// <para>If this property is set to null it will return the column's
        /// name when read.</para>
        /// </remarks>
        public virtual string Title
        {
            get { return title != null ? title : source.ColumnName; }
            set { title = value; }
        }
    }
}
