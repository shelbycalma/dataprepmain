using System;
using System.Data;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    internal static class AdoColumnFactory
    {
        public static AdoColumn CreateWrapper(
            AdoTable table, DataColumn source)
        {
            Type type = source.DataType;
            if (type == typeof(float) ||
                type == typeof(double) || 
                type == typeof(decimal) ||
                type == typeof(byte) || type == typeof(sbyte) ||
                type == typeof(short) || type == typeof(ushort) ||
                type == typeof(int) || type == typeof(uint) ||
                type == typeof(long) || type == typeof(ulong)) {
                return new AdoNumericColumn(table, source);
            }
            if (type == typeof(string) ||
                type == typeof(char) ||
                type == typeof(Boolean) ||
                type == typeof(Guid)) {
                return new AdoTextColumn(table, source);
            }
            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan)) {
                return new AdoTimeColumn(table, source);
            }
            throw Exceptions.ColumnTypeNotSupported(source);
        }
    }
}
