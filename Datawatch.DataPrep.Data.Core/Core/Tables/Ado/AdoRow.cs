using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    /// <summary>
    /// A row in a <see cref="AdoTable"/>.
    /// </summary>
    public class AdoRow : IRow
    {
        private DataRow source;

        internal AdoRow(DataRow source)
        {
            this.source = source;
        }

        /// <summary>
        /// Gets the instance of the wrapped <see cref="DataRow"/>.
        /// </summary>
        public DataRow SourceRow
        {
            get { return source; }
            internal set { source = value; }
        }
    }
}
