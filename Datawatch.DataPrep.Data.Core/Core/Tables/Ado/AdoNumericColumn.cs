using System;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    /// <summary>
    /// Subclass of the <see cref="AdoColumn"/> type for wrapping 
    /// <see cref="DataColumn"/> instances where the data type is 
    /// <see cref="double"/>.
    /// </summary>
    public class AdoNumericColumn :
        AdoColumn,
        INumericColumn,
        INumericColumnMetaData
    {
        private string metaFormat;

        internal AdoNumericColumn(AdoTable table, DataColumn source)
            : base(table, source)
        {
        }

        /// <summary>
        /// Gets the actual data type of the column (double).
        /// </summary>
        public override Type DataType
        {
            get { return typeof(double); }
        }

        /// <summary>
        /// Returns the <see cref="double">numeric value</see> of this 
        /// <see cref="AdoNumericColumn"/> 
        /// for a specific <see cref="AdoRow"/>.
        /// </summary>
        /// <param name="row"><see cref="AdoRow"/> to get the 
        /// value for.</param>
        /// <returns><see cref="double">Numeric value.</see></returns>
        public double GetNumericValue(AdoRow row)
        {
            return GetNumericValueInternal(CheckIRow(row));
        }

        double INumericColumn.GetNumericValue(IRow row)
        {
            return GetNumericValueInternal(CheckIRow(row));
        }

        public double GetNumericValue(int row)
        {
            CheckRowIndex(row);

            // TODO: Do this better.
            return GetNumericValueInternal(this.Table.GetRow(row));
        }

        private double GetNumericValueInternal(AdoRow row)
        {
            double numeric;
            lock (this.Table.SourceTable) {
                object value = row.SourceRow[this.SourceColumn];
                if (value is DBNull) {
                    numeric = NumericValue.Empty;
                } else {
                    numeric = Convert.ToDouble(value);
                }
            }
            return numeric;
        }

        NumericInterval INumericColumnMetaData.Domain
        {
            get { return null; }
        }
        
        string INumericColumnMetaData.Format
        {
            get { return metaFormat; }
        }

        /// <summary>
        /// Gets the format of this <see cref="AdoNumericColumn"/>.
        /// </summary>
        public string MetaFormat
        {
            get { return metaFormat; }
            set { metaFormat = value; }
        }

        double INumericColumnMetaData.Mean
        {
            get { return NumericValue.Empty; }
        }

        double INumericColumnMetaData.StandardDeviation
        {
            get { return NumericValue.Empty; }
        }
    }
}
