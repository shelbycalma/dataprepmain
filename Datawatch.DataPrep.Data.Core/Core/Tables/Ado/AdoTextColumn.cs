using System;
using System.Collections.Generic;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.Ado
{
    /// <summary>
    /// Subclass of the <see cref="AdoColumn"/> type for wrapping 
    /// <see cref="DataColumn"/> instances where the data type is 
    /// <see cref="string"/>.
    /// </summary>
    public class AdoTextColumn :
        AdoColumn,
        ITextColumn,
        ITextColumnMetaData
    {
        private double metaCardinality;
        private string[] metaDomain;
        private IComparer<string> comparer;

        internal AdoTextColumn(AdoTable table, DataColumn source)
            : base(table, source)
        {
        }

        /// <summary>
        /// Returns the comparer for this <see cref="AdoTextColumn"/>.
        /// </summary>
        public IComparer<string> Comparer
        {
            get { return comparer; }
            set { comparer = value; }
        }

        /// <summary>
        /// Gets the actual data type of the column (string).
        /// </summary>
        public override Type DataType
        {
            get { return typeof(string); }
        }

        /// <summary>
        /// Returns the <see cref="string">text value</see> of this 
        /// <see cref="AdoTextColumn"/> 
        /// for a specific <see cref="AdoRow"/>.
        /// </summary>
        /// <param name="row"><see cref="AdoRow"/> to get the 
        /// value for.</param>
        /// <returns><see cref="string">Text value.</see></returns>
        public string GetTextValue(AdoRow row)
        {
            return GetTextValueInternal(CheckIRow(row));
        }
        
        string ITextColumn.GetTextValue(IRow row)
        {
            return GetTextValueInternal(CheckIRow(row));
        }

        public string GetTextValue(int row)
        {
            CheckRowIndex(row);

            // TODO: Do this better.
            return GetTextValueInternal(this.Table.GetRow(row));
        }

        private string GetTextValueInternal(AdoRow row)
        {
            string text;
            lock (this.Table.SourceTable) {
                object value = row.SourceRow[this.SourceColumn];
                if (value is DBNull) {
                    text = null;
                } else {
                    text = value.ToString();
                }
            }
            return text;
        }

        /// <summary>
        /// Gets or sets the cardinality of this <see cref="AdoTextColumn"/>.
        /// </summary>
        public double MetaCardinality
        {
            get { return metaCardinality; }
            set { metaCardinality = value; }
        }

        /// <summary>
        /// Gets or sets the domain of this <see cref="AdoTextColumn"/>.
        /// </summary>
        public string[] MetaDomain
        {
            get {
                return metaDomain;
            }
            set {
                metaDomain = value;
            }
        }

        double ITextColumnMetaData.Cardinality
        {
            get { return MetaCardinality; }
        }

        string[] ITextColumnMetaData.Domain
        {
            get { return MetaDomain; }
        }
    }
}
