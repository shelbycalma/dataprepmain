﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    class AdoFilterRow : IRow
    {
        private AdoFilterTable table;
        private IRow source;

        internal AdoFilterRow(AdoFilterTable table, IRow source)
        {
            this.table = table;
            this.source = source;
        }

        internal IRow Source
        {
            get { return source; }
        }

        public ITable Table
        {
            get { return table; }
        }
    }
}
