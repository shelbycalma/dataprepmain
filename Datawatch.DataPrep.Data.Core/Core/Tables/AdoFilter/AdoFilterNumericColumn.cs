﻿using Datawatch.DataPrep.Data.Core.Tables.Ado;
using Datawatch.DataPrep.Data.Core.Wrapper;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    // Sealed so batching won't fail because of subclasses elsewhere.
    internal sealed class AdoFilterNumericColumn :
        AdoFilterColumn,
        INumericColumn,
        IMutableFormatString
    {
        private readonly INumericColumn numericSource;

        internal AdoFilterNumericColumn(
            AdoFilterTable table, INumericColumn source) :
            base(table, source)
        {
            numericSource = source;

            this.MetaData = new WrapperColumnMetaData.Numeric(this);
            this.MetaData.SourceMetaData = source.MetaData;
        }

        public double GetNumericValue(IRow row)
        {
            IRow sourceRow = ((AdoFilterRow) row).Source;
            return numericSource.GetNumericValue(sourceRow);
        }

        public double GetNumericValue(int row)
        {
            // TODO: Do better.
            AdoFilterRow adoRow = (AdoFilterRow) this.Table.GetRow(row);
            return numericSource.GetNumericValue(adoRow.Source);
        }

        public string Format
        {
            get { return this.MetaData.Format; }
            set {
                this.MetaData.Format = value;
                if (numericSource is AdoNumericColumn) {
                    ((AdoNumericColumn) numericSource).MetaFormat = value;
                }
            }
        }

        public new WrapperColumnMetaData.Numeric MetaData
        {
            get { return (WrapperColumnMetaData.Numeric) base.MetaData; }
            private set { base.MetaData = value; }
        }
    }
}
