﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    internal static class AdoFilterColumnFactory
    {
        public static AdoFilterColumn CreateColumn(
            AdoFilterTable table, IColumn source)
        {
            if (source is INumericColumn) {
                return new AdoFilterNumericColumn(
                    table, (INumericColumn) source);
            }

            if (source is ITextColumn) {
                return new AdoFilterTextColumn(
                    table, (ITextColumn) source);
            }

            if (source is ITimeColumn) {
                return new AdoFilterTimeColumn(
                    table, (ITimeColumn) source);
            }

            throw Exceptions.TableCannotWrapColumn(
                table.GetType(), source.Name, source.GetType());
        }
    }
}
