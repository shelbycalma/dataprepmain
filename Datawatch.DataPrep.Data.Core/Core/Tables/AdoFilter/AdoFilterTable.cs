﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Ado;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    // Sealed so batching won't fail because of subclasses elsewhere.
    public sealed class AdoFilterTable :
        ITable,
        IDerivedTable,
        IDisposable
    {
        public event EventHandler<TableChangingEventArgs> Changing;
        public event EventHandler<TableChangedEventArgs> Changed;
        
        
        private readonly AdoTable source;

        // Current parameter values.
        private readonly ParameterValueCollection parameters;
        
        private readonly int maxRowCount;

        // Wrapper rows.
        private readonly List<AdoFilterRow> rows = new List<AdoFilterRow>();
        // Source row to wrapper lookup.
        private readonly Dictionary<IRow, AdoFilterRow> rowLookup = 
            new Dictionary<IRow,AdoFilterRow>();
        
        // Wrapper columns.
        private readonly List<AdoFilterColumn> columns =
            new List<AdoFilterColumn>();
        // Wrapper column name to wrapper lookup.
        private readonly Dictionary<string, AdoFilterColumn> columnLookup = 
            new Dictionary<string, AdoFilterColumn>();

        // Source columns that are used in parameters.
        private readonly List<IColumn> parameterColumns = new List<IColumn>();
        // Column readers for the parameter source columns.
        private ColumnReader[] parameterColumnReaders;

        private bool disposed;

        
        public AdoFilterTable(AdoTable source, 
            IEnumerable<ParameterValue> parameters, int maxRowCount)
        {
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }

            this.source = source;
            this.maxRowCount = maxRowCount;
            this.parameters = new ParameterValueCollection();
            this.parameters.AddRange(parameters);

            AddColumnWrappers();
            UpdateParameterColumns();

            UpdateRows();
            
            Attach(source);
        }

        ~AdoFilterTable()
        {
            Dispose(false);
        }

        private void Attach(ITable source)
        {
            source.Changing += Source_Changing;
            source.Changed += Source_Changed;
        }

        private void Detach(ITable source)
        {
            source.Changing -= Source_Changing;
            source.Changed -= Source_Changed;
        }

        /// <summary>
        /// Disposes this table and its source table.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Detach(source);
                    columns.Clear();
                    columnLookup.Clear();
                    rows.Clear();
                    rowLookup.Clear();
                    if (source is IDisposable)
                    {
                        ((IDisposable)source).Dispose();
                    }
                }
                disposed = true;
            }
        }

        public int ColumnCount
        {
            get { return columns.Count; }
        }

        public IColumn GetColumn(int index)
        {
            return columns[index];
        }

        public IColumn GetColumn(string name)
        {
            if (name == null) {
                throw Exceptions.ArgumentNull("name");
            }
            AdoFilterColumn column;
            if (columnLookup.TryGetValue(name, out column)) {
                return column;
            }
            return null;
        }

        public IRow GetRow(int index)
        {
            return rows[index];
        }

        private bool IsVisible(IRow row)
        {
            if (parameters == null) return true;
            for (int i = 0; i < parameterColumns.Count; i++) {
                IColumn column = parameterColumns[i];
                ParameterValue parameterValue = parameters[column.Name];
                if (parameterValue == null) continue;
                object readerValue = parameterColumnReaders[i].GetValue(row);
                if (!ParameterHelper.IsEqual(
                    parameterValue.TypedValue, readerValue))
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsUpdating
        {
            get { return source.IsUpdating; }
        }

        private void OnChanging(TableChangingEventArgs e)
        {
            if (this.Changing != null) {
                this.Changing(this, e);
            }
        }

        private void OnChanged(TableChangedEventArgs e)
        {
            if (this.Changed != null) {
                this.Changed(this, e);
            }
        }

        public int RowCount
        {
            get { return rows.Count; }
        }

        private void Source_Changing(object sender, TableChangingEventArgs e)
        {
            OnChanging(TableChangingEventArgs.Empty);
        }

        // OPTIMIZE: Column validation? Previously, the
        // ColumnAdd/Removed would throw exceptions.
        private void Source_Changed(object sender, TableChangedEventArgs e)
        {
            UpdateRows();

            OnChanged(TableChangedEventArgs.Empty);
        }

        private void AddColumnWrappers()
        {
            Debug.Assert(columns.Count == 0);

            for (int i = 0; i < source.ColumnCount; i++) {
                AddColumnWrapper(source.GetColumn(i));
            }
        }

        private AdoFilterColumn AddColumnWrapper(IColumn sourceColumn)
        {
            AdoFilterColumn column =
                AdoFilterColumnFactory.CreateColumn(this, sourceColumn);
            columns.Add(column);
            columnLookup[column.Name] = column;
            return column;
        }

        private void UpdateParameterColumns()
        {
            List<ColumnReader> readers = new List<ColumnReader>();
            foreach (ParameterValue parameterValue in parameters)
            {
                if (columnLookup.ContainsKey(parameterValue.Name))
                {
                    IColumn col = columnLookup[parameterValue.Name].Source;
                    parameterColumns.Add(col);
                    readers.Add(ColumnReader.ForColumn(col));
                }
            }
            parameterColumnReaders = readers.ToArray();
        }

        private void UpdateRows()
        {
            if (rows.Count > 0) {
                rows.Clear();
                rowLookup.Clear();
            }

            for (int i = 0; i < source.RowCount; i++) {
                IRow sourceRow = source.GetRow(i);
                if (IsVisible(sourceRow)) {
                    AddRowWrapper(sourceRow);
                }
                if (maxRowCount >= 0 &&
                    rows.Count == maxRowCount) {
                    break;
                }
            }
        }

        private AdoFilterRow AddRowWrapper(IRow sourceRow)
        {
            AdoFilterRow row = new AdoFilterRow(this, sourceRow);
            rows.Add(row);
            rowLookup.Add(sourceRow, row);
            return row;
        }

        /// <summary>
        /// Gets the AdoFilterRow wrapper for the specified source row, or
        /// null if it doesn't have one (it's filtered out or we can into
        /// maxRowCount).
        /// </summary>
        /// <param name="sourceRow">Row to get wrapper for.</param>
        /// <returns>Wrapper or null if there is none.</returns>
        internal AdoFilterRow GetRowWrapper(IRow sourceRow)
        {
            AdoFilterRow row;
            rowLookup.TryGetValue(sourceRow, out row);
            return row;
        }

        public ITable GetSourceTable(int index)
        {
            if (index != 0)
            {
                throw Exceptions.DerivedTableBadSourceIndex(index, 1);
            }
            return source;
        }

        public int SourceTableCount
        {
            get { return 1; }
        }
    }
}
