﻿using System;
using Datawatch.DataPrep.Data.Core.Tables.Ado;
using Datawatch.DataPrep.Data.Core.Wrapper;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    // Sealed so batching won't fail because of subclasses elsewhere.
    internal sealed class AdoFilterTimeColumn :
        AdoFilterColumn,
        ITimeColumn,
        IMutableFormatString
    {
        private readonly ITimeColumn timeSource;

        internal AdoFilterTimeColumn(
            AdoFilterTable table, ITimeColumn source) :
            base(table, source)
        {
            timeSource = source;

            this.MetaData = new WrapperColumnMetaData.Time(this);
            this.MetaData.SourceMetaData = source.MetaData;
        }

        public DateTime GetTimeValue(IRow row)
        {
            IRow sourceRow = ((AdoFilterRow) row).Source;
            return timeSource.GetTimeValue(sourceRow);
        }

        public DateTime GetTimeValue(int row)
        {
            // TODO: Do better.
            AdoFilterRow adoRow = (AdoFilterRow) this.Table.GetRow(row);
            return timeSource.GetTimeValue(adoRow.Source);
        }

        public string Format
        {
            get { return this.MetaData.Format; }
            set {
                this.MetaData.Format = value;
                if (timeSource is AdoTimeColumn) {
                    ((AdoTimeColumn) timeSource).MetaFormat = value;
                }
            }
        }

        public new WrapperColumnMetaData.Time MetaData
        {
            get { return (WrapperColumnMetaData.Time) base.MetaData; }
            private set { base.MetaData = value; }
        }
    }
}
