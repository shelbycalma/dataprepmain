﻿using Datawatch.DataPrep.Data.Core.Wrapper;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    // Sealed so batching won't fail because of subclasses elsewhere.
    internal sealed class AdoFilterTextColumn :
        AdoFilterColumn,
        ITextColumn
    {
        private readonly ITextColumn textSource;

        internal AdoFilterTextColumn(
            AdoFilterTable table, ITextColumn source) :
            base(table, source)
        {
            textSource = source;

            this.MetaData = new WrapperColumnMetaData.Text(this);
            this.MetaData.SourceMetaData = source.MetaData;
        }

        public string GetTextValue(IRow row)
        {
            IRow sourceRow = ((AdoFilterRow) row).Source;
            return textSource.GetTextValue(sourceRow);
        }

        public string GetTextValue(int row)
        {
            // TODO: Do better.
            AdoFilterRow adoRow = (AdoFilterRow) this.Table.GetRow(row);
            return textSource.GetTextValue(adoRow.Source);
        }

        public new WrapperColumnMetaData.Text MetaData
        {
            get { return (WrapperColumnMetaData.Text) base.MetaData; }
            private set { base.MetaData = value; }
        }
    }
}
