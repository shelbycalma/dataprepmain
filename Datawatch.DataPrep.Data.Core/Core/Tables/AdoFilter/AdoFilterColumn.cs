﻿using Datawatch.DataPrep.Data.Core.Tables.Ado;
using Datawatch.DataPrep.Data.Core.Wrapper;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Tables.AdoFilter
{
    class AdoFilterColumn :
        IColumn,
        IMutableColumnTitle,
        IMutableHiddenColumn
    {
        private readonly AdoFilterTable table;
        private readonly IColumn source;
        private WrapperColumnMetaData meta;

        internal AdoFilterColumn(AdoFilterTable table, IColumn source)
        {
            this.table = table;
            this.source = source;
        }

        public bool Hidden
        {
            get { return meta.Hidden; }
            set {
                meta.Hidden = value;
                if (source is AdoColumn) {
                    ((AdoColumn) source).Hidden = value;
                }
            }
        }

        IColumnMetaData IColumn.MetaData
        {
            get { return this.MetaData; }
        }

        protected WrapperColumnMetaData MetaData
        {
            get { return meta; }
            set { meta = value; }
        }

        public string Name
        {
            get { return source.Name; }
        }

        internal IColumn Source
        {
            get { return source; }
        }

        ITable IColumn.Table
        {
            get { return this.Table; }
        }

        public AdoFilterTable Table
        {
            get { return table; }
        }

        public string Title
        {
            get { return meta.Title; }
            set {
                meta.Title = value;
                if (source is AdoColumn) {
                    ((AdoColumn) source).Title = value;
                }
            }
        }
    }
}
