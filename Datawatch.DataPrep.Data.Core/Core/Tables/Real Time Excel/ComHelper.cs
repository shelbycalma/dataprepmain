﻿using System.Runtime.InteropServices;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class ComHelper
    {
        public static void ReleaseComObject(object obj)
        {
            if (obj != null) {
                try {
                    Marshal.ReleaseComObject(obj);
                    //while (Marshal.ReleaseComObject(obj) > 0) ;
                } catch {
                }
            }
        }
    }
}
