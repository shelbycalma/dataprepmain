﻿using System;
using System.Data;
using Datawatch.DataPrep.Data.Core.Tables.Ado;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{

    /// <summary>
    /// Subclass of <see cref="AdoTable"/> 
    /// providing easy access to streaming data from MS Excel.
    /// </summary>
    public class RealtimeExcelTable : AdoTable
    {
        /// <summary>
        /// Fired when the real time updating status of the table has changed.
        /// </summary>
        public event EventHandler StreamingStatusChanged;

        /// <summary>
        /// Fired before a real time update.
        /// </summary>
        public event EventHandler Updating;

        /// <summary>
        /// Fired after a real time update is completed.
        /// </summary>
        public event EventHandler Updated;

        private string path;
        private string range;
        private WorkbookSink sink;
        private StreamingStatus status = StreamingStatus.Stopped;

        private bool disposed;

        /// <summary>
        /// Initializes a new instance of the RealtimeExcelTable class.
        /// </summary>
        /// <param name="path">A path to an Excel workbook.</param>
        /// <param name="range">A named range or sheet within the workbook.</param>
        public RealtimeExcelTable(string path, string range) : 
            base(RealtimeExcelManager.Instance.GetDataTable(path, range))
        {
            this.path = path;
            this.range = range;
        }

        /// <summary>
        /// Releases event handlers from the source <see cref="DataTable"/>.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    RealtimeExcelManager.Instance.StopUpdates(this);
                    SetSink(null);
                }
                disposed = true;
            }
            base.Dispose(disposing);
        }

        private void Attach(WorkbookSink sink)
        {
            sink.StreamingStatusChanged +=
                new EventHandler(workbookSink_StreamingStatusChanged);
            sink.Updating +=
                new EventHandler<RealtimeExcelEventArgs>(workbookSink_Updating);
            sink.Updated +=
                new EventHandler<RealtimeExcelEventArgs>(workbookSink_Updated);
        }

        private void Detach(WorkbookSink sink)
        {
            sink.StreamingStatusChanged -=
                new EventHandler(workbookSink_StreamingStatusChanged);
            sink.Updating -=
                new EventHandler<RealtimeExcelEventArgs>(workbookSink_Updating);
            sink.Updated -=
                new EventHandler<RealtimeExcelEventArgs>(workbookSink_Updated);
        }

        /// <summary>
        /// Returns a list of named ranges and sheets for a specific Excel workbook.
        /// </summary>
        /// <param name="path">The path to the Excel workbook.</param>
        /// <returns>An array of string containing named ranges and sheets.</returns>
        public static string[] GetRanges(string path)
        {
            return RealtimeExcelManager.Instance.GetRanges(path);
        }

        ///// <summary>
        ///// Returns a value indicating whether the RealtimeExcelTable is currently updating.
        ///// </summary>
        //public bool IsUpdating
        //{
        //    get { return sink != null; }
        //}

        /// <summary>
        /// Returns the Excel workbook path for this RealtimeExcelTable.
        /// </summary>
        public string Path
        {
            get { return path; }
        }

        /// <summary>
        /// Returns the range for this RealtimeExcelTable.
        /// </summary>
        public string Range
        {
            get { return range; }
        }

        private void SetSink(WorkbookSink sink)
        {
            if (this.sink != null)
            {
                Detach(this.sink);
            }
            
            this.sink = sink;
            StreamingStatus = StreamingStatus.Stopped;

            if (sink != null)
            {
                StreamingStatus = sink.StreamingStatus;
                Attach(sink);
            }
        }

        /// <summary>
        /// Starts real-time updates for this RealtimeExcelTable.
        /// </summary>
        public void Start()
        {
            SetSink(RealtimeExcelManager.Instance.StartUpdates(this));
        }

        /// <summary>
        /// Stops real-time updates for this RealtimeExcelTable.
        /// </summary>
        public void Stop()
        {
            SetSink(null);
            RealtimeExcelManager.Instance.StopUpdates(this);
        }


        /// <summary>
        /// Gets a value indicating the real-time updating status of this
        /// RealtimeExcelTable.
        /// </summary>
        public StreamingStatus StreamingStatus
        {
            get { return status; }

            private set
            {
                if (status != value)
                {
                    status = value;
                    if (StreamingStatusChanged != null)
                    {
                        StreamingStatusChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        private void workbookSink_StreamingStatusChanged(object sender, EventArgs e)
        {
            StreamingStatus = sink.StreamingStatus;
        }

        private void workbookSink_Updated(object sender, RealtimeExcelEventArgs e)
        {
            if (Updated != null)
            {
                Updated(this, e);
            }
        }

        private void workbookSink_Updating(object sender, RealtimeExcelEventArgs e)
        {
            if (Updating != null)
            {
                Updating(this, e);
            }
        }
    }
}
