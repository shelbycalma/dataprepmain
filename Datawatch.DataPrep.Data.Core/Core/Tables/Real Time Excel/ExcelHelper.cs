﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using Datawatch.DataPrep.Data.Framework;
using Excel9.Interop;
using DataTable = System.Data.DataTable;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class ExcelHelper
    {
        public static Application GetExcelInstance()
        {
            Application app = null;
            int tryCount = 0;

            do
            {
                try
                {
                    app = (Application)Marshal.GetActiveObject(
                        "Excel.Application");
                }
                catch
                {
                    Thread.Sleep(200);
                }
                tryCount++;
            }
            while ((app == null) && IsExcelRunning() && tryCount < 10);
            return app;
        }

        public static Range GetRange(Workbook book, string name)
        {
            if (name == null || name.Length == 0)
            {
                throw new ArgumentNullException("name");
            }
            if (book == null)
            {
                throw new ArgumentNullException("book");
            }

            Range range;
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            bool comInterrupted = false;

            try
            {
                do
                {
                    range = null;
                    Missing m = Missing.Value;
                    Thread.CurrentThread.CurrentCulture =
                        CultureInfo.CreateSpecificCulture("en-US");

                    try
                    {
                        comInterrupted = false;

                        if (name.StartsWith("'") && name.EndsWith("'"))
                        {
                            name = name.Substring(1, name.Length - 2);
                        }
                        int index = name.IndexOf("$");
                        if ((index != -1) && (name.IndexOf(':') != -1))	// Defined range
                        {
                            string rangeName = string.Format("'{0}'!{1}",
                                name.Substring(0, index), name.Substring(index + 1));

                            range = book.Application.get_Range(rangeName, m);

                            range = book.Application.Intersect(range,
                                range.Worksheet.UsedRange,
                                m, m, m, m, m, m, m, m, m, m, m, m, m,
                                m, m, m, m, m, m, m, m, m, m, m, m, m, m, m);

                            if (range == null)
                            {
                                throw new Exception(string.Format(
                                    Properties.Resources.ExInvalidRange, name));
                            }
                        }
                        else if (name.EndsWith("$"))	// Sheet name
                        {
                            Sheets sheetCollection = null;
                            string sheetName = name.Substring(0, name.Length - 1);
                            sheetCollection = book.Sheets;
                            int sheetCollectionCount = 0;
                            sheetCollectionCount = sheetCollection.Count;
                            for (int i = 1; i <= sheetCollectionCount; i++)
                            {
                                Worksheet sheet = null;
                                sheet = sheetCollection.get_Item(i) as Worksheet;
                                if (sheet != null && (sheet.Name == sheetName))
                                {
                                    range = sheet.UsedRange;
                                    ComHelper.ReleaseComObject(sheet);
                                    sheet = null;
                                }
                            }
                        }
                    }
                    catch (COMException)
                    {
                        try
                        {
                            Thread.Sleep(200);
                        }
                        catch
                        {
                        }
                        comInterrupted = true;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(Properties.Resources.LogRetrievingRangeError, 
                            range, ex.Message);
                    }
                } while (comInterrupted);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
            }

            return range;
        }

        public static string[] GetRanges(Workbook workbook, string path)
        {
            List<string> ranges = new List<string>();
            ArrayList doneWorksheets = new ArrayList();
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;

            do
            {
                Thread.CurrentThread.CurrentCulture =
                    CultureInfo.CreateSpecificCulture("en-US");
                try
                {
                    foreach (Worksheet ws in workbook.Worksheets)
                    {
                        if (!doneWorksheets.Contains(ws))
                        {
                            ranges.Add(ws.Name + "$");
                            doneWorksheets.Add(ws);
                        }
                    }
                    return ranges.ToArray();
                }
                catch (COMException)
                {
                    try
                    {
                        Thread.Sleep(200);
                    }
                    catch (Exception)
                    {
                    }
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCulture;
                }
            } while (true);
        }

        public static Workbook GetWorkbook(Application excel, string path)
        {
            ArrayList doneWorkbooks = new ArrayList();
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;

            do
            {
                Thread.CurrentThread.CurrentCulture =
                    CultureInfo.CreateSpecificCulture("en-US"); try
                {
                    foreach (Workbook wb in excel.Workbooks)
                    {
                        if (!doneWorkbooks.Contains(wb))
                        {
                            if (wb.FullName == path)
                            {
                                return wb;
                            }
                        }
                        doneWorkbooks.Add(wb);
                    }
                    return null;
                }
                catch (COMException)
                {
                    try
                    {
                        Thread.Sleep(200);
                    }
                    catch (Exception)
                    {
                    }
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCulture;
                }
            } while (true);
        }

        public static bool IsExcelRunning()
        {
            try
            {
                Process[] processArray = Process.GetProcesses();

                Thread.SpinWait(5);

                foreach (Process process in processArray)
                {
                    if ((process.ProcessName != null) &&
                        (process.ProcessName.ToLower() == "excel"))
                    {
                        return true;
                    }
                }
            }
            catch
            { }

            return false;
        }

        /// <summary>
        /// Makes sure the Excel file data is synchronized with the table data.
        /// </summary>
        public static void UpdateDataTable(Application excel, DataTable table,
            string path, string range, bool modifyColumns)
        {
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            Workbook book = null;

            try
            {
                book = GetWorkbook(excel, path);

                if (book == null)
                {
                    throw new Exception(Properties.Resources.ExWorkbookNotFound);
                }
                UpdateDataTable(table, book, range, modifyColumns);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
                ComHelper.ReleaseComObject(book);
            }
        }

        public static void UpdateDataTable(DataTable table, Workbook book,
            string range, bool modifyColumns)
        {
            if (book == null) return;

            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            Range r = null;
            try
            {
                r = ExcelHelper.GetRange(book, range);

                if (r != null)
                {
                    DataHelper.UpdateDataTable(table, r, modifyColumns);
                }
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
                if (r != null)
                {
                    ComHelper.ReleaseComObject(r);
                }
                range = null;
            }
        }
    }
}
