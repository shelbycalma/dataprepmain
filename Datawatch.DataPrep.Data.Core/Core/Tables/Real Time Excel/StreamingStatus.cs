﻿namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    /// <summary>
    /// Indicates the real-time streaming status of a 
    /// <see cref="Panopticon.Developer.Tables.RealTimeExcel.RealtimeExcelTable"/>. 
    /// </summary>
    public enum StreamingStatus
    {
        /// <summary>
        /// Indicates that real time updates are not active.
        /// </summary>
        Stopped,

        /// <summary>
        /// Indicates that the table is waiting for an Excl instance to be started, 
        /// or for the target range to be activated in a started Excel instance.
        /// </summary>
        Waiting,

        /// <summary>
        /// Indicates that real time updates are active.
        /// </summary>
        Streaming
    }
}
