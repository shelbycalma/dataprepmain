﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using Datawatch.DataPrep.Data.Framework;
using Excel9.Interop;
using Timer = System.Timers.Timer;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class WorkbookSink : IDisposable
    {
        public event EventHandler StreamingStatusChanged;
        public event EventHandler<RealtimeExcelEventArgs> Updating;
        public event EventHandler<RealtimeExcelEventArgs> Updated;

        private Workbook workbook;
        private RealtimeExcelTable table;
        private System.Data.DataTable dataTable;
        private bool disposed;
        private DateTime updateDateTime;

        private bool isRecalcEnabled;

        private long lastUpdate;
        private int realtimeUpdateLimit = 2000;
        private long realtimeUpdateLimitTicks = 2000*TimeSpan.TicksPerMillisecond;

        private int fileCheckInterval = 1000;
        private int recalcInterval = 1000;

        private Timer recalcTimer;
        private Timer fileCheckTimer;
        private Timer updateTimeoutTimer;
        private StreamingStatus streamingStatus = StreamingStatus.Waiting;

        public WorkbookSink(RealtimeExcelTable table)
        {
            this.table = table;
            this.dataTable = table.SourceTable;

            updateDateTime = DateTime.MinValue;
            
            Attach(workbook);

            recalcTimer = new Timer(recalcInterval);
            recalcTimer.Stop();
            fileCheckTimer = new Timer(fileCheckInterval);
            fileCheckTimer.Stop();
            updateTimeoutTimer = new Timer(realtimeUpdateLimit);
            updateTimeoutTimer.Stop();

            fileCheckTimer.AutoReset = false;
            recalcTimer.AutoReset = false;

            recalcTimer.Elapsed +=
                new ElapsedEventHandler(RecalcTimer_Elapsed);
            fileCheckTimer.Elapsed +=
                new ElapsedEventHandler(FileCheckTimer_Elapsed);
            updateTimeoutTimer.Elapsed +=
                new ElapsedEventHandler(UpdateTimeoutTimer_Elapsed);
        }

        ~WorkbookSink()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    recalcTimer.Elapsed -=
                        new System.Timers.ElapsedEventHandler(RecalcTimer_Elapsed);
                    updateTimeoutTimer.Elapsed -=
                        new ElapsedEventHandler(UpdateTimeoutTimer_Elapsed);
                    fileCheckTimer.Elapsed -=
                        new System.Timers.ElapsedEventHandler(FileCheckTimer_Elapsed);

                    if (workbook != null)
                    {
                        Detach(workbook);
                        ComHelper.ReleaseComObject(workbook);
                        workbook = null;
                    }

                    recalcTimer.Dispose();
                    fileCheckTimer.Dispose();
                    updateTimeoutTimer.Dispose();
                }

                disposed = true;
            }
        }


        private void Attach(Workbook workbook)
        {
            if (workbook != null)
            {
                CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture =
                    CultureInfo.CreateSpecificCulture("en-US");

                bool comInterrupted = true;
                try
                {
                    do
                    {
                        try
                        {
                            workbook.SheetChange +=
                                new WorkbookEvents_SheetChangeEventHandler(
                                Book_SheetChange);
                            comInterrupted = false;
                        }
                        catch (COMException)
                        {
                            try
                            {
                                Thread.Sleep(200);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.Assert(false, ex.Message);
                            throw ex;
                        }
                    } while (comInterrupted);

                    comInterrupted = true;
                    do
                    {
                        try
                        {
                            workbook.SheetCalculate +=
                                new WorkbookEvents_SheetCalculateEventHandler(
                                Book_SheetCalculate);
                            comInterrupted = false;
                        }
                        catch (COMException)
                        {
                            try
                            {
                                Thread.Sleep(200);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        catch (Exception ex)
                        {
                            System.Diagnostics.Debug.Assert(false, ex.Message);
                            throw ex;
                        }
                    } while (comInterrupted);

                    StreamingStatus = StreamingStatus.Streaming;
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCulture;
                }
            }
        }

        private void Book_SheetCalculate(object sheetObject)
        {
            if (IsCorrectSheet(sheetObject as Worksheet,
                workbook, table.Range)) 
            {
                UpdateDataTable();
            }
        }

        private void Book_SheetChange(object sheetObject, Range range)
        {
            if (IsCorrectSheet(sheetObject as Worksheet,
                workbook, table.Range))
            {
                UpdateDataTable();
            }
        }

        private void Detach(Workbook workbook)
        {
            if (workbook != null)
            {
                bool comInterrupted = false;
                do
                {
                    try
                    {
                        comInterrupted = false;
                        workbook.SheetChange -=
                            new WorkbookEvents_SheetChangeEventHandler(
                            Book_SheetChange);
                    }
                    catch (COMException)
                    {
                        try
                        {
                            Thread.Sleep(200);
                        }
                        catch
                        {
                        }
                        comInterrupted = true;
                    }
                    catch (Exception ex)
                    {
                        Debug.Assert(true, "Detach: SheetChange -= Book_SheetChange", ex.Message);
                    }
                } while (comInterrupted);

                do
                {
                    try
                    {
                        comInterrupted = false;
                        workbook.SheetCalculate -=
                            new WorkbookEvents_SheetCalculateEventHandler(
                            Book_SheetCalculate);
                    }
                    catch (COMException)
                    {
                        comInterrupted = true;
                        try
                        {
                            Thread.Sleep(200);
                        }
                        catch (Exception)
                        {
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Assert(true, "Detach: SheetCalculate -= Book_SheetCalculate", ex.Message);
                    }
                } while (comInterrupted);

                StreamingStatus = StreamingStatus.Waiting;
            }
        }



        /// <summary>
        /// Checks whether the excel file has been altered or not and updates
        /// the data table accordingly.
        /// </summary>
        /// <param name="sender">The instance that invoked the event.</param>
        /// <param name="e">The event arguments.</param>
        private void FileCheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                string path = table.Path;

                if (File.Exists(path))
                {
                    DateTime currentDateTime = File.GetLastWriteTime(path);

                    if (updateDateTime.CompareTo(currentDateTime) < 0)
                    {
                        updateDateTime = File.GetLastWriteTime(path);
                        UpdateDataTable();
                    }
                    fileCheckTimer.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(Properties.Resources.LogUpdatingTableError, 
                    ex.Message);
            }
        }


        private static bool IsCorrectSheet(Worksheet sheet, Workbook workbook, 
            string range)
        {
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            try
            {
                if (sheet != null)
                {
                    string name = range;

                    if (name != null && name.EndsWith("$")) // Sheet:
                    {
                        return (sheet.Name + '$' == name);
                    }
                    else if (workbook != null)	// Named range:
                    {
                        Range r = null;
                        Worksheet rangeSheet = null;

                        try
                        {
                            r = ExcelHelper.GetRange(workbook, range);

                            if (r != null)
                            {
                                rangeSheet = r.Worksheet;
                                return (rangeSheet == sheet);
                            }
                            else
                            {
                                return false;
                            }
                        }
                        finally
                        {
                            ComHelper.ReleaseComObject(r);
                            range = null;

                            ComHelper.ReleaseComObject(rangeSheet);
                            rangeSheet = null;
                        }
                    }
                }
            }
            catch
            { }
            finally
            {
                ComHelper.ReleaseComObject(sheet);
                sheet = null;
                Thread.CurrentThread.CurrentCulture = oldCulture;
            }

            return false;
        }

        public bool IsRecalcEnabled
        {
            get { return isRecalcEnabled; }
            set
            {
                if (isRecalcEnabled != value)
                {
                    isRecalcEnabled = value;

                    if (isRecalcEnabled)
                    {
                        if (workbook != null)
                        {
                            recalcTimer.Enabled = true;
                        }
                    }
                    else
                    {
                        recalcTimer.Enabled = false;
                    }
                }
            }
        }

        /// <summary>
        /// Recalculates all the cells in the worksheet and updates the table.
        /// </summary>
        /// <param name="sender">The instance that invoked the event.</param>
        /// <param name="e">The event arguments.</param>
        private void RecalcTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            Range range = null;

            try
            {
                range = ExcelHelper.GetRange(workbook, table.Range);

                if (range != null)
                {
                    range.Calculate();
                }
            }
            catch (Exception ex)
            {
                Log.Error(Properties.Resources.LogRecalculatingRange,
                    ex.Message);
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
                ComHelper.ReleaseComObject(range);
                range = null;
                recalcTimer.Enabled = true;
            }
        }

        private void StartTimers()
        {
            fileCheckTimer.Interval = fileCheckInterval;
            recalcTimer.Interval = recalcInterval;

            fileCheckTimer.Enabled = true;
            recalcTimer.Enabled = isRecalcEnabled;
        }

        private void StopTimers()
        {
            updateTimeoutTimer.Enabled = false;
            fileCheckTimer.Enabled = true;
            recalcTimer.Enabled = false;
        }

        public StreamingStatus StreamingStatus
        {
            get { return streamingStatus; }
            set
            {
                if (streamingStatus != value)
                {
                    streamingStatus = value;
                    if (StreamingStatusChanged != null)
                    {
                        StreamingStatusChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        private void UpdateDataTable()
        {
            if (disposed) return;

            lock (this)
            {
                if (DateTime.Now.Ticks - lastUpdate > realtimeUpdateLimitTicks)
                {
                    try
                    {
                        if (Updating != null)
                        {
                            Updating(this, new RealtimeExcelEventArgs(table));
                        }
                        updateTimeoutTimer.Enabled = false;
                        lastUpdate = DateTime.Now.Ticks;
                        ExcelHelper.UpdateDataTable(dataTable, workbook,
                            table.Range, false);
                    }
                    catch (Exception e)
                    {
                        Log.Error(Properties.Resources.LogUpdatingDataTableError, 
                            e.Message);
                    }
                    finally
                    {
                        if (Updated != null)
                        {
                            Updated(this, new RealtimeExcelEventArgs(table));
                        }
                    }
                }
                else 
                {
                    updateTimeoutTimer.Enabled = true;
                }
            }
        }

        private void UpdateTimeoutTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            updateTimeoutTimer.Enabled = false;
            UpdateDataTable();
        }

        public Workbook Workbook
        {
            get { return workbook; }
            set
            {
                if (workbook != null)
                {
                    Detach(workbook);
                    StopTimers();
                }
                workbook = value;
                if (workbook != null)
                {
                    Attach(workbook);
                    StartTimers();
                }
            }
        }

    }
}
