﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Management;
using System.Runtime.InteropServices;
using System.Threading;
using System.Timers;
using Datawatch.DataPrep.Data.Framework;
using Excel9.Interop;
using Timer = System.Timers.Timer;
using DataTable = System.Data.DataTable;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class RealtimeExcelManager : IDisposable
    {
        public static RealtimeExcelManager Instance = new RealtimeExcelManager();

		private const string NAME_PARAM = "range";	// Sheet name or named range.
		private const string RECALC_ENABLED_PARAM = "recalcenabled";	// Whether recalculations are enabled or not.
		private const string RECALC_INTERVAL_PARAM = "recalcinterval";	// Intervals between recalculations.
		private const string REALTIME_UPDATE_LIMIT_PARAM = "realtimeupdatelimit"; //Limit when executing SetRows

		private const string WATCH_QUERY_FORMAT =
			"SELECT * FROM {0} WITHIN 2 " +
			"WHERE TargetInstance ISA 'Win32_Process' AND " +
			"TargetInstance.Name LIKE 'EXCEL.EXE'";

		private const string PROCESS_START_EVENT = "__InstanceCreationEvent";
		private const string PROCESS_STOP_EVENT = "__InstanceDeletionEvent";

		private const double INSTANCE_CHECK_INTERVAL_MS = 1000;

		private const string COMEXCEPTION_CALLEEMSG = "Call was rejected by callee.";
		private const string COMEXCEPTION_BUSYMSG = "The message filter indicated that the application is busy.";

        private Dictionary<RealtimeExcelTable, WorkbookSink> workbookSinks = 
            new Dictionary<RealtimeExcelTable, WorkbookSink>();

        private ManagementEventWatcher excelStartWatcher;
		private ManagementEventWatcher excelStopWatcher;
		
        private Timer instanceCheckTimer;

        private Application excelApp;
				
        private bool disposed;


		/// <summary>
		/// Empty constructor.
		/// </summary>
		private RealtimeExcelManager()
		{
            instanceCheckTimer = new Timer(INSTANCE_CHECK_INTERVAL_MS);
            instanceCheckTimer.Elapsed +=
                new ElapsedEventHandler(InstanceCheckTimer_Elapsed);

            string startQuery = 
                string.Format(WATCH_QUERY_FORMAT, PROCESS_START_EVENT);
            excelStartWatcher = new ManagementEventWatcher(startQuery);
            excelStartWatcher.EventArrived += 
                new EventArrivedEventHandler(excelStartWatcher_EventArrived);

            string stopQuery = 
                string.Format(WATCH_QUERY_FORMAT, PROCESS_STOP_EVENT);
            excelStopWatcher = new ManagementEventWatcher(stopQuery);
            excelStopWatcher.EventArrived += 
                new EventArrivedEventHandler(excelStopWatcher_EventArrived);

            instanceCheckTimer.AutoReset = false;
        }

        ~RealtimeExcelManager() 
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }

        public void Dispose()
        {
 	        Dispose(true);
        }

        protected void Dispose(bool disposing) 
        {
            if (!disposed) 
            {
                if (disposing) 
                {
                    instanceCheckTimer.Elapsed -=
                        new ElapsedEventHandler(InstanceCheckTimer_Elapsed);

                    instanceCheckTimer.Dispose();

			        excelStartWatcher.EventArrived -= 
				        new EventArrivedEventHandler(excelStartWatcher_EventArrived);

			        excelStartWatcher.Dispose();

			        excelStopWatcher.EventArrived -= 
				        new EventArrivedEventHandler(excelStopWatcher_EventArrived);

			        excelStopWatcher.Dispose();

                    foreach (RealtimeExcelTable table in workbookSinks.Keys) 
                    {
                        WorkbookSink sink = workbookSinks[table];
                        sink.Dispose();
                    }
                    workbookSinks.Clear();

                    SetExcelApplication(null);
                }
                disposed = true;
            }
        }

        private void Attach(Application excel)
        {
            bool comInterrupted = true;
            do
            {
                try
                {
                    excel.WorkbookBeforeClose +=
                        new AppEvents_WorkbookBeforeCloseEventHandler(
                            ExcelApp_BeforeWorkbookClose);
                    comInterrupted = false;
                }
                catch (COMException)
                {
                    try
                    {
                        Thread.Sleep(200);
                    }
                    catch (Exception)
                    {
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(Properties.Resources.LogAttachingExcelError, ex.Message);
                    throw ex;
                }
            } while (comInterrupted);

            comInterrupted = true;
            do
            {
                try
                {
                    excel.WorkbookOpen +=
                        new AppEvents_WorkbookOpenEventHandler(
                            ExcelApp_WorkbookOpen);
                    comInterrupted = false;
                }
                catch (COMException)
                {
                    try
                    {
                        Thread.Sleep(200);
                    }
                    catch (Exception)
                    {
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(Properties.Resources.LogAttachingExcelError, ex.Message);
                    throw ex;
                }
            } while (comInterrupted);
        }

        private void Detach(Application excel)
        {
            bool comInterrupted = false;
            do
            {
                try
                {
                    comInterrupted = false;
                    excel.WorkbookBeforeClose -=
                        new AppEvents_WorkbookBeforeCloseEventHandler(
                            ExcelApp_BeforeWorkbookClose);
                }
                catch (COMException)
                {
                    try
                    {
                        Thread.Sleep(200);
                    }
                    catch (Exception)
                    {
                    }
                    comInterrupted = true;
                }
                catch (Exception ex)
                {
                    Log.Error(Properties.Resources.LogDetachingExcelError, ex.Message);
                }
            } while (comInterrupted);

            do
            {
                try
                {
                    comInterrupted = false;
                    excel.WorkbookOpen -=
                        new AppEvents_WorkbookOpenEventHandler(
                            ExcelApp_WorkbookOpen);
                }
                catch (COMException)
                {
                    try
                    {
                        Thread.Sleep(200);
                    }
                    catch (Exception)
                    {
                    }
                    comInterrupted = true;
                }
                catch (Exception ex)
                {
                    Log.Error(Properties.Resources.LogDetachingExcelError, ex.Message);
                }
            } while (comInterrupted);
        }

        private void ExcelApp_BeforeWorkbookClose(Workbook book, ref bool Cancel)
        {
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            Log.Info(Properties.Resources.LogExcelBeforeWorkbookClose, book.Name);

            try
            {
                foreach (RealtimeExcelTable table in workbookSinks.Keys)
                {
                    if (book.FullName == table.Path)
                    {
                        workbookSinks[table].Workbook = null;
                    }
                }
            }
            catch
            { }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
            }

            ComHelper.ReleaseComObject(book);
        }

        private void ExcelApp_WorkbookOpen(Workbook book)
        {
            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            Log.Info(Properties.Resources.LogExcelWorkbookOpen, book.Name);

            try
            {
                string fullname = book.FullName;
                foreach (RealtimeExcelTable table in workbookSinks.Keys)
                {
                    if (table.Path == fullname)
                    {
                        workbookSinks[table].Workbook = book;
                    }
                }
            }
            catch
            {
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
            }
        }

        private void excelStartWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            Log.Info(Properties.Resources.LogExcelStarted, "");

            if (workbookSinks.Count > 0)
            {
                SetExcelApplication(ExcelHelper.GetExcelInstance());
            }
        }

        private void excelStopWatcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            Log.Info(Properties.Resources.LogExcelStopped, "");

            SetExcelApplication(null);

            if (workbookSinks.Count > 0)
            {
                excelStartWatcher.Start();
            }
        }

		internal string[] GetRanges(string path)
		{
			bool closeBook = false;
			Workbook workbook = null;

			CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
			Thread.CurrentThread.CurrentCulture = 
                CultureInfo.CreateSpecificCulture("en-US"); 

			Application excel = ExcelHelper.GetExcelInstance();

			try 
			{
				if (excel != null) 
				{
					workbook = ExcelHelper.GetWorkbook(excel, path);

					if (workbook == null) 
					{
						workbook = excel.Workbooks.Open(path, 
							false, true, Type.Missing, Type.Missing, Type.Missing, 
							Type.Missing, Type.Missing, Type.Missing,
							false, false, Type.Missing, false);
						closeBook = true;
					}
					if (workbook != null) 
					{	
						return ExcelHelper.GetRanges(workbook, path);
					}
				} 
				else 
				{
                    return OleDbExcelHelper.GetRanges(path);
				}
			} 
			finally 
			{
				Thread.CurrentThread.CurrentCulture = oldCulture;
				if (closeBook && workbook != null) 
				{
					try 
					{
						workbook.Close(false, null, null);
					} 
					catch
					{
					}
                }
                ComHelper.ReleaseComObject(workbook);
                ComHelper.ReleaseComObject(excel);
			}
            return null;
		}

        private void InstanceCheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            bool release = false;

            CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
            Thread.CurrentThread.CurrentCulture =
                CultureInfo.CreateSpecificCulture("en-US");

            try
            {
                bool comInterrupted;
                do
                {
                    try
                    {
                        comInterrupted = false;
                        release = !excelApp.Visible;
                    }
                    catch (COMException)
                    {
                        comInterrupted = true;
                        try
                        {
                            Thread.Sleep(200);
                        }
                        catch (Exception)
                        {
                        }
                    }
                } while (comInterrupted);
            }
            catch
            {
                release = true;
            }
            finally
            {
                Thread.CurrentThread.CurrentCulture = oldCulture;
            }

            if (release)
            {
                Log.Info(Properties.Resources.LogInstanceCheckTimerReleasing, "");
                SetExcelApplication(null);
                if (workbookSinks.Count > 0)
                {
                    excelStartWatcher.Start();
                }
            }
            else
            {
                instanceCheckTimer.Enabled = true;
            }
        }

		internal DataTable GetDataTable(string path, string range)
		{
            Application excel = ExcelHelper.GetExcelInstance();
            DataTable dataTable = null;

            if (excel != null)
            {
                try
                {
                    dataTable = new DataTable();
                    ExcelHelper.UpdateDataTable(excel, dataTable, path, range, true);
                    ComHelper.ReleaseComObject(excel);
                }
                catch
                {
                    dataTable = null;
                }
                finally
                {
                    ComHelper.ReleaseComObject(excel);
                }
            }
            
            if (dataTable == null)
            {
                dataTable = OleDbExcelHelper.LoadDataTable(path, range);
            }
            return dataTable;
		}

        private void SetExcelApplication(Application excel)
        {
            if (excelApp != null)
            {
                instanceCheckTimer.Enabled = false;
                excelStopWatcher.Stop();
                Detach(excelApp);
                ComHelper.ReleaseComObject(excelApp);

                foreach (WorkbookSink sink in workbookSinks.Values)
                {
                    sink.Workbook = null;
                }
                GC.Collect();
            }

            excelApp = excel;

            if (excelApp != null)
            {
                Attach(excelApp);
                excelStartWatcher.Stop();
                excelStopWatcher.Start();
                instanceCheckTimer.Enabled = true;

                foreach (RealtimeExcelTable table in workbookSinks.Keys)
                {
                    WorkbookSink sink = workbookSinks[table];
                    try
                    {
                        sink.Workbook =
                            ExcelHelper.GetWorkbook(excelApp, table.Path);
                    }
                    catch
                    {
                    }
                }
            }
        }

		internal WorkbookSink StartUpdates(RealtimeExcelTable table)
		{
            Log.Info(Properties.Resources.LogStartingUpdates, table.Path, table.Range);
            lock (this)
            {
                WorkbookSink sink = new WorkbookSink(table);

                workbookSinks[table] = sink;

                // If this is the first WorkbookSink, try to retrieve an Excel instance.
                // If Excel is not available, start the excelStartWatcher.
                if (workbookSinks.Count == 1)
                {
                    SetExcelApplication(ExcelHelper.GetExcelInstance());
                    if (excelApp == null)
                    {
                        excelStartWatcher.Start();
                    }
                }
                else if (excelApp != null)
                {
                    try
                    {
                        sink.Workbook =
                            ExcelHelper.GetWorkbook(excelApp, table.Path);
                    }
                    catch
                    {
                    }
                }
                return sink;
            }
		}

		internal void StopUpdates(RealtimeExcelTable table)
		{
            Log.Info(Properties.Resources.LogStoppingUpdates, table.Path, table.Range);

            lock (this)
            {
                if (workbookSinks.ContainsKey(table))
                {
                    WorkbookSink sink = workbookSinks[table];
                    sink.Dispose();
                    workbookSinks.Remove(table);
                }

                if (workbookSinks.Count == 0)
                {
                    SetExcelApplication(null);
                    excelStartWatcher.Stop();
                    excelStopWatcher.Stop();
                }
            }
		}
	}   
}
