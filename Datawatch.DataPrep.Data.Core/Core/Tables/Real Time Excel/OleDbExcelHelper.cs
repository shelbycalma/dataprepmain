﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using DataTable = System.Data.DataTable;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class OleDbExcelHelper
    {
        private static OleDbConnection CreateConnection(string file)
        {
            string extension = Path.GetExtension(file).ToLower();
			OleDbConnectionStringBuilder builder = new OleDbConnectionStringBuilder();
			if (file.IndexOfAny(Path.GetInvalidFileNameChars()) < 0 && !string.IsNullOrEmpty(file))
			{
				// Create the appropiate connection string depending on the version of Excel
				if (extension == ".xls")
				{
					builder.Provider = "Microsoft.Jet.OLEDB.4.0";
					builder.DataSource = file;
					builder["Mode"] = "Read";
					builder["Extended Properties"] = "Excel 8.0;HDR=Yes;IMEX=1;";
				}
				else if (extension == ".xlsx" || extension == ".xlsb")
				{
					builder.Provider = "Microsoft.ACE.OLEDB.12.0";
					builder.DataSource = file;
					builder["Mode"] = "Read";
					builder["Extended Properties"] = "Excel 12.0;HDR=YES;";
				}
				else
				{
					throw new Exception(
						string.Format(Properties.Resources.ExUnknownFileType, extension));
				}
			}

            return new OleDbConnection(builder.ConnectionString);
        }

        public static string[] GetRanges(string file)
        {
            if (!File.Exists(file))
            {
                throw new Exception(string.Format(
                    Properties.Resources.ExFileNotFound, Path.GetFullPath(file)));
            }
            string name = Path.GetFileNameWithoutExtension(file);
            string[] names = null;

            // Open the file and get the sheet names
            using (OleDbConnection connection = CreateConnection(file))
            {
                connection.Open();
                names = GetRanges(connection);
                connection.Close();
            }

            return names;
        }

        // Retrieve the sheet names from an Excel workbook
        private static string[] GetRanges(OleDbConnection connection)
        {
            DataTable tables = connection.GetOleDbSchemaTable(
                OleDbSchemaGuid.Tables,
                new object[] { null, null, null, "TABLE" });
            List<string> names = new List<string>();

            foreach (DataRow row in tables.Rows)
            {
                string name = (string)row["TABLE_NAME"];
                if (!name.EndsWith("'") && (name.IndexOf("$'") != -1))
                {
                    name = name.Replace("$'", "'$");
                }
                names.Add(name);
            }
            return names.ToArray();
        }

        public static void UpdateDataTable(DataTable table, string path, string range)
        {
            DataHelper.UpdateDataTable(table, LoadDataTable(path, range), true);
        }

        public static DataTable LoadDataTable(string path, string range)
        {
            if (!File.Exists(path))
            {
                throw new Exception(string.Format(
                    Properties.Resources.ExFileNotFound, Path.GetFullPath(path)));
            }

            DataTable data = null;

            using (OleDbConnection connection = CreateConnection(path))
            {
                connection.Open();

                string command = string.Format("SELECT * FROM [{0}]", range);
                OleDbDataAdapter adapter =
                    new OleDbDataAdapter(command, connection);
                // Cerate a data table to fill with the Excel data
                data = new DataTable();
                adapter.FillSchema(data, SchemaType.Source);
                adapter.Fill(data);

                connection.Close();
            }
            return data;
        }
    }
}
