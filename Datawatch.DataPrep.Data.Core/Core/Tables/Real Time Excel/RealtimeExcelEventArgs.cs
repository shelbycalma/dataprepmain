﻿using System;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class RealtimeExcelEventArgs : EventArgs
    {
        public readonly RealtimeExcelTable Table;

        public RealtimeExcelEventArgs(RealtimeExcelTable table)
        {
            Table = table;
        }
    }
}
