﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Excel9.Interop;
using DataTable = System.Data.DataTable;

namespace Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel
{
    class DataHelper
    {
        private const int typeCheckRowCount = 8;

        public static object GetCellValue(DataTable tableSource, 
            Array arraySource, int col, int row)
        {
            if (arraySource != null)
            {
                return arraySource.GetValue(row + 1, col + 1);
            }
            else if (tableSource != null)
            {
                return tableSource.Rows[row][col];
            }

            return null;
        }

        public static int GetColumnCount(object source)
        {
            if (source is Array)
            {
                Array cellArray = source as Array;
                return cellArray.GetLength(1);
            }
            else if (source is DataTable)
            {
                DataTable table = source as DataTable;
                return table.Columns.Count;
            }

            return 0;
        }

        public static Type GetColumnType(object source, int col, int rowCount, 
            int typeCheckRows)
        {
            Type type = null;

            if (source is Array)
            {
                Array cellArray = source as Array;
                //int typeCheckRows = mySettings.TypeCheckRows;

                for (int row = 1; (row - 1 < typeCheckRows) && (row < rowCount); row++)
                {
                    type = GetColumnType(type, cellArray.GetValue(row + 1, col + 1));
                }

                if (type == null)
                {
                    type = typeof(string);
                }
            }
            else if (source is DataTable)
            {
                DataTable table = source as DataTable;

                type = table.Columns[col].DataType;
            }

            return type;
        }

        /// <summary>
        /// Returns the type of the column.
        /// </summary>
        /// <param name="oldType">The column type so far.</param>
        /// <param name="cell">The cell that should be a member of the column type.</param>
        /// <returns>The type of the column.</returns>
        private static Type GetColumnType(Type oldType, object cell)
        {
            if ((cell == null) || object.Equals(oldType, typeof(string)))
            {
                return oldType;
            }
            else if (object.Equals(oldType, typeof(DateTime)))
            {
                if (object.Equals(cell.GetType(), typeof(DateTime)))
                {
                    return typeof(DateTime);
                }
                else
                {
                    return typeof(string);
                }
            }

            switch (cell.GetType().Name)
            {
                case "Double":
                case "Int32":
                case "Int64":
                case "Single":
                case "Int16":
                case "Byte":
                case "UInt64":
                case "UInt32":
                case "SByte":
                case "UInt16":
                    return typeof(double);
                case "DateTime":
                    return typeof(DateTime);
                default:
                    return typeof(string);
            }
        }

        public static int GetRowCount(object source)
        {
            if (source is Array)
            {
                Array cellArray = source as Array;
                return cellArray.GetLength(0);
            }
            else if (source is DataTable)
            {
                DataTable table = source as DataTable;
                return table.Rows.Count;
            }

            return 0;
        }

        /// <summary>
        /// Returns a collection of unique column names.
        /// </summary>
        /// <param name="source">The source is either a 2 dimensional array or a table that represents the updated table.</param>
        /// <returns>A collection of unique column names.</returns>
        public static List<string> GetUniqueColumnNames(object source)
        {
            List<string> nameList;

            if (source is DataTable)
            {
                DataTable table = source as DataTable;

                nameList = new List<string>(table.Columns.Count);

                foreach (DataColumn column in table.Columns)
                {
                    nameList.Add(column.ColumnName);
                }

                return nameList;
            }

            Array cellArray = source as Array;
            int colCount = cellArray.GetLength(1);

            nameList = new List<string>(colCount);

            for (int col = 0; col < colCount; col++)
            {
                string name = cellArray.GetValue(1, col + 1) as string;

                if (name != null && name.Length > 1 && name.IndexOf("!") > -1)
                {
                    name = name.Replace("!", "_");
                }

                if ((name == null) || (name.Length == 0))
                {
                    name = "F" + (col + 1);	// Mimics the ExcelPlugin.
                }

                int dupeCount = 0;
                string uniqueName;
                bool isDupe;

                do
                {
                    isDupe = false;

                    uniqueName = name;

                    if (dupeCount > 0)
                    {
                        uniqueName += dupeCount;
                    }

                    for (int prevCol = 0; prevCol < col; prevCol++)
                    {
                        string prevName =
                            cellArray.GetValue(1, prevCol + 1) as string;

                        if ((uniqueName == prevName) ||
                            nameList.Contains(uniqueName))
                        {
                            isDupe = true;
                            dupeCount++;
                            break;
                        }
                    }
                }
                while (isDupe);

                nameList.Add(uniqueName);
            }

            return nameList;
        }

        /// <summary>
        /// Sets the column names and types.
        /// </summary>
        /// <param name="source">The source is either a 2 dimensional array or a table that represents the updated table.</param>
        /// <param name="table">The table which column collection should be altered to suite the cell data.</param>
        /// <param name="modifyColumns"></param>
        /// <returns>The column mapping between the columns in excel and the columns in the table.</returns>
        private static string[] SetColumns(object source, DataTable table, bool modifyColumns)
        {
            int rowCount = DataHelper.GetRowCount(source);
            int colCount = DataHelper.GetColumnCount(source);

            if (rowCount == 0)
            {
                table.Columns.Clear();
                return new string[0];
            }

            List<String> columnNames = DataHelper.GetUniqueColumnNames(source);
            List<DataColumn> columnMapping = new List<DataColumn>(colCount);

            if (modifyColumns)
            {
                for (int col = 0; col < colCount; col++)
                {
                    Type type = DataHelper.GetColumnType(source, col, 
                        rowCount, typeCheckRowCount);
                    columnMapping.Add(new DataColumn(columnNames[col], type));
                }

                // Removes columns that doesn't exist or is of a different type:
                for (int col = 0; col < table.Columns.Count && modifyColumns; )
                {
                    int index = columnNames.IndexOf(table.Columns[col].ColumnName);
                    Type oldType = null;
                    Type newType = null;

                    if (index != -1)
                    {
                        oldType = table.Columns[col].DataType;
                        newType = ((DataColumn)columnMapping[index]).DataType;
                    }

                    if ((index == -1) || !object.Equals(oldType, newType))
                    {
                        table.Columns.RemoveAt(col);
                    }
                    else	// No change:
                    {
                        col++;
                    }
                }
            }

            // Adds missing columns to the table:
            foreach (DataColumn column in columnMapping)
            {
                if (!table.Columns.Contains(column.ColumnName))
                {
                    table.Columns.Add(column);
                }
            }

            return columnNames.ToArray();
        }

        public static void UpdateDataTable(DataTable table, object source,
            bool modifyColumns)
        {
            Range range = source as Range;

            if (range != null)
            {
                CultureInfo oldCulture = Thread.CurrentThread.CurrentCulture;
                Thread.CurrentThread.CurrentCulture =
                    CultureInfo.CreateSpecificCulture("en-US");

                try
                {
                    source = (Array)range.Value;
                }
                finally
                {
                    Thread.CurrentThread.CurrentCulture = oldCulture;
                    ComHelper.ReleaseComObject(range);
                    range = null;
                }
            }

            if (source != null)
            {
                string[] columnMapping = SetColumns(source, table, modifyColumns);
                int rowOffset = source is Array ? 1 : 0;
                int rowCount = DataHelper.GetRowCount(source);
                int colCount = DataHelper.GetColumnCount(source);
                int dataRowCount = source is Array ? rowCount - 1 : rowCount;
                DataRow dataRow;
                DataTable tableSource = source as DataTable;
                Array arraySource = source as Array;

                lock (table)
                {
                    // Removes excessive rows:
                    while (table.Rows.Count > dataRowCount)
                    {
                        table.Rows.RemoveAt(table.Rows.Count - 1);
                    }

                    for (int row = rowOffset; row < rowCount; row++)
                    {
                        if (row - rowOffset < table.Rows.Count)
                        {
                            dataRow = table.Rows[row - rowOffset];
                        }
                        else
                        {
                            dataRow = table.NewRow();
                            table.Rows.Add(dataRow);
                        }

                        for (int col = 0; col < colCount; col++)
                        {
                            object cell = GetCellValue(
                                tableSource, arraySource, col, row);
                            string columnName = columnMapping[col];
                            DataColumn column = table.Columns[columnName];

                            if (column != null)
                            {
                                Type columntype = 
                                    table.Columns[columnName].DataType;

                                if ((cell == null) ||
                                    ((cell.GetType() == typeof(string)) &&
                                    (columntype != typeof(string))))
                                {
                                    cell = DBNull.Value;
                                }
                                else if (columntype == typeof(string))
                                {
                                    cell = cell.ToString();
                                }
                                else if (!columntype.Equals(cell.GetType()))
                                {
                                    cell = DBNull.Value;
                                }

                                try
                                {
                                    if (!object.Equals(cell, dataRow[column]))
                                    {
                                        dataRow[column] = cell;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Debug.Assert(true, ex.Message);
                                }
                            }
                        }                        
                    }
                    table.AcceptChanges();
                }
            }
        }
    }
}
