﻿using System;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core
{
    public static class DataUtils
    {
        private static readonly string TimeWindowStart = "TimeWindowStart";
        private static readonly string TimeWindowEnd = "TimeWindowEnd";
        private static readonly string Snapshot = "Snapshot";
        private static readonly string DefaultDateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

        public static List<string> SpecialParameters = new List<string>()
        {
            TimeWindowStart,
            TimeWindowEnd,
            Snapshot
        };

        public static string ApplySpecialParameters(string sourceString,
            IEnumerable<ParameterValue> parameters, TimeZoneHelper timeZoneHelper,
            bool encloseParameterInQuote, string targetDateFormat)
        {
            if (parameters == null || timeZoneHelper == null ||
                !timeZoneHelper.TimeZoneSelected)
            {
                return sourceString;
            }

            if (string.IsNullOrEmpty(targetDateFormat))
            {
                targetDateFormat = DefaultDateTimeFormat;
            }
            try
            {
                if (parameters == null) return sourceString;

                foreach (string specialParam in SpecialParameters)
                {
                    string oldKey = "$" + specialParam;
                    string newKey = "{" + specialParam;
                    if (!(sourceString.IndexOf(oldKey) != -1
                    || sourceString.IndexOf(newKey) != -1))
                    {
                        continue; // parameter not used in source string
                    }

                    while (sourceString.Contains(oldKey))
                    {
                        sourceString = sourceString.Replace(oldKey, newKey + "}");
                    }

                    int startBrace = sourceString.IndexOf(newKey);
                    int endBrace = sourceString.IndexOf("}", startBrace);
                    newKey = sourceString.Substring(startBrace,
                        endBrace - startBrace + 1);

                    foreach (ParameterValue parameterValue in parameters)
                    {
                        if (string.IsNullOrEmpty(parameterValue.Name) ||
                            parameterValue.TypedValue == null)
                        {
                            continue;
                        }

                        if (!specialParam.Equals(parameterValue.Name))
                        {
                            continue;
                        }
                        string newValue = "";
                        if (parameterValue.TypedValue is DateTimeParameterValue)
                        {
                            DateTime value = ((DateTimeParameterValue)
                                parameterValue.TypedValue).Value;
                            value = timeZoneHelper.ConvertToUTC(value);
                            newValue = value.ToString(targetDateFormat);
                        }
                        else if (parameterValue.TypedValue is StringParameterValue)
                        {
                            String value = ((StringParameterValue)
                                parameterValue.TypedValue).Value;
                            newValue = ConvertToUTC(value, timeZoneHelper,
                                targetDateFormat);
                        }

                        if (!string.IsNullOrEmpty(newValue))
                        {
                            if (encloseParameterInQuote)
                            {
                                newValue = QuoteLiteralParameterValue(newValue);
                            }
                            sourceString = sourceString.Replace(newKey,
                                newValue);
                        }

                        break;
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                Log.Error("Error while replacing special parameters sourcestring:{0}",
                    sourceString);
            }
            return sourceString;
        }

        public static string ConvertToUTC(string sourceString,
            TimeZoneHelper timeZoneHelper, string targetDateFormat)
        {
            if (string.IsNullOrEmpty(sourceString))
            {
                return null;
            }

            try
            {

                //Apply the time zone offset,from required time zone to UTC 
                DateTime adjustedDateTime;

                if (timeZoneHelper != null && timeZoneHelper.TimeZoneSelected)
                {
                    adjustedDateTime = timeZoneHelper.ConvertToUTC(
                        Convert.ToDateTime(sourceString));

                    //convert it to required format and append UTC
                    return adjustedDateTime.ToString(
                        targetDateFormat);
                }
                else if (IsDate(sourceString, out adjustedDateTime))
                {
                    return adjustedDateTime.ToString(
                        targetDateFormat);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

            Log.Error(Properties.Resources.LogFailedToParseDate, sourceString);
            return null;
        }

        public static bool IsDate(string inputDate, out DateTime dt)
        {
            return DateTime.TryParse(inputDate, out dt);
        }

        public static string QuoteLiteralParameterValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            if (value[0] == '$' || value[0] == '{')
            {
                return value;
            }
            if (value[0] == '\'')
            {
                return value;
            }
            return string.Format("'{0}'", value);
        }

        public static bool IsSpecialParameter(string paramName)
        {
            if (string.IsNullOrEmpty(paramName))
            {
                return false;
            }
            if (SpecialParameters.Contains(paramName))
            {
                return true;
            }

            return false;
        }

        public static FunctionType ToFunctionType(
            AggregateType aggregateType)
        {
            switch (aggregateType)
            {
                case AggregateType.Sum:
                    return FunctionType.Sum;
                case AggregateType.Min:
                    return FunctionType.Min;
                case AggregateType.Max:
                    return FunctionType.Max;
                case AggregateType.Count:
                    return FunctionType.Count;
                case AggregateType.First:
                    return FunctionType.First;
                case AggregateType.Last:
                    return FunctionType.Last;
                case AggregateType.Mean:
                    return FunctionType.Mean;
                default:
                    return FunctionType.Sum;
            }
        }

        /// <summary>
        /// Replaces parameters in query with actual values. This overloaded
        /// method should only be used in places where we want to enforce
        /// single quote handling.
        /// </summary>
        public static string ApplyParameters(string query,
            IEnumerable<ParameterValue> parameters, bool encloseParameterInQuote,
            bool enforceSingleQuoteHanding = true)
        {
            // No parameters, do nothing. Testing for count == 0 would
            // be nice but might be expensive with enumerable?
            if (parameters == null)
            {
                return query;
            }

            return new ParameterEncoder
            {
                Parameters = parameters,
                SourceString = query,
                // Propety is obsolete but is still required for correct
                // handling of the quotes.
                EnforceSingleQuoteEnclosure = encloseParameterInQuote,
                EnforceSingleQuoteHandling = enforceSingleQuoteHanding,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }

        public static string ApplyParameters(string source,
            IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(source)) return null;
            if (parameters == null) return source;

            return new ParameterEncoder
            {
                SourceString = source,
                Parameters = parameters,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }

        public static bool CanOpen(string filePath, string[] fileExtensions)
        {
            if (fileExtensions == null) throw Framework.Exceptions.ArgumentNull("fileExtensions");
            if (filePath == null) return false;

            int maxExtensionLength = 0;
            foreach (string extension in fileExtensions)
            {
                maxExtensionLength =
                    Math.Max(maxExtensionLength, extension.Length);
            }
            maxExtensionLength = Math.Min(maxExtensionLength, filePath.Length);

            string filePathEnd =
                filePath.Substring(filePath.Length - maxExtensionLength,
                maxExtensionLength).ToLower();

            foreach (string extension in fileExtensions)
            {
                if (filePathEnd.EndsWith(extension)) return true;
            }
            return false;
        }


        // Return the file path where the file actually is
        public static string FixFilePath(string workbookDir, string dataDir,
            string filePath, IEnumerable<ParameterValue> parameters)
        {
            string path = ApplyParameters(filePath, parameters);
            // handle relative paths
            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(workbookDir, path);
            }

            // handle non-existant paths
            if (!File.Exists(path))
            {
                string fileName = Path.GetFileName(path);
                string testPath = Path.Combine(dataDir, fileName);
                if (File.Exists(testPath))
                {
                    return testPath;
                }
            }
            if (!path.Equals(filePath))
            {
                return path;
            }
            return filePath;
        }

        public static ParameterValueCollection EscapeParameters(
    IEnumerable<ParameterValue> sourceParameters)
        {
            if (sourceParameters == null)
            {
                return null;
            }
            ParameterValueCollection escapedParameters =
                new ParameterValueCollection();
            foreach (ParameterValue parameterValue in sourceParameters)
            {
                if (parameterValue.TypedValue == null)
                {
                    continue;
                }
                string paramName = parameterValue.Name;
                TypedParameterValue typedValue = parameterValue.TypedValue;

                if (typedValue is StringParameterValue)
                {
                    escapedParameters.Add(paramName, EscapeParameterValue(
                            parameterValue.TypedValue));
                }
                else if (typedValue is ArrayParameterValue)
                {
                    ArrayParameterValue newArray = new ArrayParameterValue();

                    TypedParameterValue[] paramArray =
                        ((ArrayParameterValue)typedValue).Values.ToArray();

                    for (int i = 0; i < paramArray.Length; i++)
                    {
                        if ((paramArray[i] is StringParameterValue))
                        {
                            newArray.Add(EscapeParameterValue(paramArray[i]));
                        }
                        else
                        {
                            //just copy the the parametervalue
                            newArray.Add(parameterValue.TypedValue);
                        }

                    }
                    escapedParameters.Add(paramName, newArray);
                }
                else
                {
                    //just copy the the parametervalue
                    escapedParameters.Add(paramName, parameterValue.TypedValue);
                }
            }

            return escapedParameters;
        }

        private static StringParameterValue EscapeParameterValue(
    TypedParameterValue typedValue)
        {
            StringParameterValue stringValue = typedValue as StringParameterValue;
            if (stringValue == null || string.IsNullOrEmpty(stringValue.Value))
            {
                return null;
            }
            string paramValue = stringValue.Value;
            if (paramValue.Contains("'"))
            {
                paramValue = paramValue.Replace("'", "''");
            }
            paramValue = "'" + paramValue + "'";

            return new StringParameterValue(paramValue);
        }
    }
}
