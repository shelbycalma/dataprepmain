﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Text;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Svg
{
    public static class SvgHelper
    {
        private const int MaxSequenceLength = 10;

        public static void AddShapeData(string shapeData,
            IList<PointF> points, IList<byte> types)
        {
            if (shapeData == null) {
                throw Exceptions.ArgumentNull("shapeData");
            }
            if (points == null) {
                throw Exceptions.ArgumentNull("points");
            }
            if (types == null) {
                throw Exceptions.ArgumentNull("types");
            }

            int pipeIndex = shapeData.IndexOf('|');
            string transformData = shapeData.Substring(0, pipeIndex);
            string pathData = shapeData.Substring(
                pipeIndex + 1, shapeData.Length - pipeIndex - 1);
            string[] paths = pathData.Split(' ');
            if (paths.Length == 1 && paths[0].Length == 0) {
                return;
            }
            int index = 0;
            int startPointIndex = points.Count;
            CultureInfo culture = CultureInfo.InvariantCulture;
            float previousX = 0;
            float previousY = 0;
            char previousCommand = 'Z';
            float mX = 0;
            float mY = 0;
            while (index < paths.Length) {
                char command = paths[index][0];
                switch (command) {
                    case 'M':
                        mX = previousX = float.Parse(paths[++index], culture);
                        mY = previousY = float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(0);
                        break;
                    case 'm':
                        mX = previousX += float.Parse(paths[++index], culture);
                        mY = previousY += float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(0);
                        break;
                    case 'Z':
                    case 'z':
                        previousX = mX;
                        previousY = mY;
                        types[types.Count - 1] += 128;
                        break;
                    case 'L':
                        previousX = float.Parse(paths[++index], culture);
                        previousY = float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        break;
                    case 'l':
                        previousX += float.Parse(paths[++index], culture);
                        previousY += float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        break;
                    case 'H':
                        previousX = float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        break;
                    case 'h':
                        previousX += float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        break;
                    case 'V':
                        previousY = float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        break;
                    case 'v':
                        previousY += float.Parse(paths[++index], culture);
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        break;
                    case 'C':
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        for (int i = 0; i < 3; i++) {
                            previousX = float.Parse(paths[++index], culture);
                            previousY = float.Parse(paths[++index], culture);
                            points.Add(new PointF(previousX, previousY));
                            types.Add(3);
                        }
                        break;
                    case 'c': {
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        float x = previousX;
                        float y = previousY;
                        for (int i = 0; i < 3; i++) {
                            previousX = x + float.Parse(paths[++index], culture);
                            previousY = y + float.Parse(paths[++index], culture);
                            points.Add(new PointF(previousX, previousY));
                            types.Add(3);
                        }
                        break;
                    }
                    case 'S':
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        if (previousCommand == 'C' || previousCommand == 'c' ||
                            previousCommand == 'S' || previousCommand == 's') {
                            previousX =
                                2 * previousX - points[points.Count - 3].X;
                            previousY =
                                2 * previousY - points[points.Count - 3].Y;
                            points.Add(new PointF(previousX, previousY));
                        } else {
                            points.Add(new PointF(previousX, previousY));
                        }
                        types.Add(3);
                        for (int i = 0; i < 2; i++) {
                            previousX = float.Parse(paths[++index], culture);
                            previousY = float.Parse(paths[++index], culture);
                            points.Add(new PointF(previousX, previousY));
                            types.Add(3);
                        }
                        break;
                    case 's': {
                        points.Add(new PointF(previousX, previousY));
                        types.Add(1);
                        float x = previousX;
                        float y = previousY;
                        if (previousCommand == 'C' || previousCommand == 'c' ||
                            previousCommand == 'S' || previousCommand == 's') {
                            previousX =
                                2 * previousX - points[points.Count - 3].X;
                            previousY =
                                2 * previousY - points[points.Count - 3].Y;
                            points.Add(new PointF(previousX, previousY));
                        } else {
                            points.Add(new PointF(previousX, previousY));
                        }
                        types.Add(3);
                        for (int i = 0; i < 2; i++) {
                            previousX = x + float.Parse(paths[++index], culture);
                            previousY = y + float.Parse(paths[++index], culture);
                            points.Add(new PointF(previousX, previousY));
                            types.Add(3);
                        }
                        break;
                    }
                    default:
                        throw new Exception(string.Format(Resources.ExUnsupportedCommand, command));
                }
                previousCommand = command;
                index++;
            }
            using (Matrix matrix = new Matrix()) {
                Transform(matrix, transformData, culture);
                float[] elements = matrix.Elements;
                for (int i = startPointIndex; i < points.Count; i++) {
                    PointF point = points[i];
                    points[i] = new PointF(
                        elements[0] * point.X + elements[1] * point.Y +
                            elements[4],
                        elements[2] * point.X + elements[3] * point.Y +
                            elements[5]);
                }
            }
        }

        public static string PreparePathData(string pathData)
        {
            StringBuilder builder = new StringBuilder(pathData.Length);
            bool isDecimal = false;
            bool isExponent = false;
            char previousChar = ' ';
            char previousCommand = 'Z';
            int dividersSinceCommand = 0;
            for (int i = 0; i < pathData.Length; i++) {
                char currentChar = pathData[i];
                if (currentChar > 47 && currentChar < 58) {
                    dividersSinceCommand = TryAddCommand(builder,
                        dividersSinceCommand, previousCommand);
                    builder.Append(currentChar);
                } else {
                    switch (currentChar) {
                        case 'E':
                        case 'e':
                            builder.Append(currentChar);
                            isDecimal = false;
                            isExponent = true;
                            break;
                        case ' ':
                        case ',':
                        case '\t':
                        case '\n':
                        case '\r':
                            currentChar = ' ';
                            if (previousChar != ' ') {
                                dividersSinceCommand++;
                                builder.Append(' ');
                            }
                            isDecimal = false;
                            break;
                        case '+':
                            break;
                        case '-':
                            if (!isExponent && previousChar != ' ') {
                                dividersSinceCommand++;
                                builder.Append(' ');
                            }
                            dividersSinceCommand = TryAddCommand(builder,
                                dividersSinceCommand, previousCommand);
                            builder.Append(currentChar);
                            isDecimal = false;
                            break;
                        case '.':
                            if (isDecimal && !isExponent &&
                                    previousChar != ' ') {
                                dividersSinceCommand++;
                                builder.Append(' ');
                            }
                            dividersSinceCommand = TryAddCommand(builder,
                                dividersSinceCommand, previousCommand);
                            builder.Append(currentChar);
                            isDecimal = true;
                            break;
                        case 'M':
                        case 'm':
                        case 'Z':
                        case 'z':
                        case 'L':
                        case 'l':
                        case 'H':
                        case 'h':
                        case 'V':
                        case 'v':
                        case 'C':
                        case 'c':
                        case 'S':
                        case 's':
                            if (previousChar != ' ') {
                                builder.Append(' ');
                            }
                            builder.Append(currentChar);
                            switch (currentChar) {
                                case 'M':
                                    previousCommand = 'L';
                                    break;
                                case 'm':
                                    previousCommand = 'l';
                                    break;
                                case 'z':
                                    previousCommand = 'Z';
                                    break;
                                default:
                                    previousCommand = currentChar;
                                    break;
                            }
                            isDecimal = false;
                            isExponent = false;
                            builder.Append(' ');
                            currentChar = ' ';
                            dividersSinceCommand = 1;
                            break;
                        default:
                            throw new Exception(string.Format(Resources.ExUnsupportedCharacter, currentChar, i));
                    }
                }
                previousChar = currentChar;
            }
            while (builder.Length > 0 && builder[builder.Length - 1] == ' ') {
                builder.Remove(builder.Length - 1, 1);
            }
            return builder.ToString();
        }

        public static string PrepareTransformData(string transformData)
        {
            char previousChar = ' ';
            StringBuilder builder = new StringBuilder(transformData.Length);
            for (int i = 0; i < transformData.Length; i++) {
                char currentChar = transformData[i];
                switch (currentChar) {
                    case ' ':
                    case ',':
                    case '\t':
                    case '\n':
                    case '\r':
                    case '(':
                    case ')':
                        currentChar = ' ';
                        if (previousChar != ' ') {
                            builder.Append(' ');
                        }
                        break;
                    default:
                        builder.Append(currentChar);
                        break;
                }
                previousChar = currentChar;
            }
            while (builder.Length > 0 &&
                builder[builder.Length - 1] == ' ') {
                builder.Remove(builder.Length - 1, 1);
            }
            return builder.ToString();
        }

        private static void Transform(Matrix matrix, string transformData,
            CultureInfo culture)
        {
            if (transformData != null && transformData.Length > 0) {
                string[] transforms = transformData.Split(new char[] { ' ' });
                int index = 0;
                while (index < transforms.Length) {
                    string transform = transforms[index];
                    switch (transform) {
                        case "matrix":
                            float m11 =
                                float.Parse(transforms[++index], culture);
                            float m21 =
                                float.Parse(transforms[++index], culture);
                            float m12 =
                                float.Parse(transforms[++index], culture);
                            float m22 =
                                float.Parse(transforms[++index], culture);
                            float dx =
                                float.Parse(transforms[++index], culture);
                            float dy =
                                float.Parse(transforms[++index], culture);
                            using (Matrix custom =
                                    new Matrix(m11, m12, m21, m22, dx, dy)) {
                                matrix.Multiply(custom, MatrixOrder.Append);
                            }
                            break;
                        case "translate":
                            matrix.Translate(
                                float.Parse(transforms[++index], culture),
                                float.Parse(transforms[++index], culture));
                            break;
                        case "scale":
                            matrix.Scale(
                                float.Parse(transforms[++index], culture),
                                float.Parse(transforms[++index], culture));
                            break;
                        default:
                            char error = transform != null &&
                                    transform.Length > 0 ?
                                transform[0] : ' ';
                            throw new Exception(string.Format(Resources.ExUnsupportedTransform, transform));

                    }
                    index++;
                }
            }
        }

        private static int TryAddCommand(StringBuilder builder,
            int dividersSinceCommand, char previousCommand)
        {
            switch (dividersSinceCommand) {
                case 3: {
                    switch (previousCommand) {
                        case 'L':
                        case 'l':
                        case 'H':
                        case 'h':
                        case 'V':
                        case 'v':
                            builder.Append(previousCommand);
                            builder.Append(' ');
                            dividersSinceCommand = 1;
                            break;
                    }
                    break;
                }
                case 7: {
                    switch (previousCommand) {
                        case 'C':
                        case 'c':
                            builder.Append(previousCommand);
                            builder.Append(' ');
                            dividersSinceCommand = 1;
                            break;
                    }
                    break;
                }
                case 5: {
                    switch (previousCommand) {
                        case 'S':
                        case 's':
                            builder.Append(previousCommand);
                            builder.Append(' ');
                            dividersSinceCommand = 1;
                            break;
                    }
                    break;
                }
            }
            return dividersSinceCommand;
        }
    }
}
