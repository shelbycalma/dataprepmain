﻿using System;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core
{
    public class Utils : Framework.Utils
    {
        public static string ComputeRelativePath(string mainDirPath, string absoluteFilePath)
        {
            string[] firstPathParts =
                mainDirPath.Trim(Path.DirectorySeparatorChar).Split(
                    Path.DirectorySeparatorChar);

            string[] secondPathParts =
                absoluteFilePath.Trim(Path.DirectorySeparatorChar).Split(
                    Path.DirectorySeparatorChar);

            int sameCounter = 0;
            for (int i = 0; i < Math.Min(firstPathParts.Length, secondPathParts.Length); i++)
            {
                if (!firstPathParts[i].ToLower().Equals(secondPathParts[i].ToLower()))
                {
                    break;
                }
                sameCounter++;
            }

            if (sameCounter == 0)
            {
                return absoluteFilePath;
            }

            string newPath = String.Empty;

            for (int i = sameCounter; i < firstPathParts.Length; i++)
            {
                if (i > sameCounter)
                {
                    newPath += Path.DirectorySeparatorChar;
                }
                newPath += "..";
            }
            if (newPath.Length == 0)
            {
                newPath = ".";
            }
            for (int i = sameCounter; i < secondPathParts.Length; i++)
            {
                newPath += Path.DirectorySeparatorChar;
                newPath += secondPathParts[i];
            }
            return newPath;
        }

        public static string CreateFilterString(string title,
            params string[] extensions)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(title);
            sb.Append(" (");
            bool first = true;
            foreach (string extension in extensions)
            {
                if (!first)
                {
                    sb.Append(", ");
                }
                sb.Append("*." + extension);
                first = false;
            }
            sb.Append(")|");

            first = true;
            foreach (string extension in extensions)
            {
                if (!first)
                {
                    sb.Append(";");
                }
                sb.Append("*.");
                sb.Append(extension);
                first = false;
            }
            return sb.ToString();
        }


        public static PropertyBag GetOrCreateSubGroup(PropertyBag propertyBag,
            string[] path)
        {
            if (propertyBag == null) return new PropertyBag();

            PropertyBag bag = propertyBag;

            if (path != null && path.Length > 0)
            {
                for (int i = 0; i < path.Length; i++)
                {
                    if (!bag.SubGroups.ContainsKey(path[i]))
                    {
                        bag.SubGroups[path[i]] = new PropertyBag();
                    }
                    bag = bag.SubGroups[path[i]];
                }
            }
            return bag;
        }

        public static void UpdateTimeValueInPredicate<T>(Predicate predicate,
            QuerySettings<T> querySettings, StandaloneTable table) where T : DatabaseColumn
        {
            if (predicate is NotOperator)
            {
                NotOperator notOperator = (NotOperator)predicate;
                UpdateTimeValueInPredicate(notOperator.A, querySettings, table);
            }
            else if (predicate is BinaryOperator)
            {
                BinaryOperator binaryOperator = (BinaryOperator)predicate;

                UpdateTimeValueInPredicate(binaryOperator.A, querySettings, table);
                UpdateTimeValueInPredicate(binaryOperator.B, querySettings, table);
            }
            else if (predicate is Comparison)
            {
                string columnName = string.Empty;
                DateTime columnValue = DateTime.MinValue;
                Comparison comparison = (Comparison)predicate;

                if (comparison.Left is ColumnParameter)
                {
                    columnName = ((ColumnParameter)comparison.Left).ColumnName;
                    Column column = table.GetColumn(columnName);
                    if (column is TimeColumn)
                    {
                        SqlParameter parameter = comparison.Right;
                        ValueParameter valueParam = parameter as ValueParameter;
                        if (valueParam == null) return;

                        DateTimeParameterValue dtValueParam = valueParam.Value as
                            DateTimeParameterValue;
                        if (dtValueParam == null) return;

                        ((ValueParameter)comparison.Right).Value =
                                new DateTimeParameterValue(
                                    querySettings.TimeZoneHelper.ConvertToUTC(
                                    dtValueParam.Value));
                    }
                }
            }
        }

        /// <summary>
        /// Gets the recursivly first source table not implementing
        /// <see cref="IDerivedTable"/>.
        /// </summary>
        /// <param name="table">The table for which to find the first
        /// non derived table</param>
        /// <returns>The recursivly first source table not implementing
        /// <see cref="IDerivedTable"/>.</returns>
        /// <remarks>
        /// <para>
        /// If the argument implements <see cref="IDerivedTable"/> then this
        /// method will recursivly fetch the first source table until it
        /// finds a table not implementing <see cref="IDerivedTable"/>.
        /// </para>
        /// <para>
        /// If the argument is not an <see cref="IDerivedTable"/> then it is
        /// simply returned.
        /// </para>
        /// <para>
        /// This method will check that each <see cref="IDerivedTable"/> it
        /// finds only contains exactly one source table.
        /// </para>
        /// </remarks>
        public static ITable GetNonDerivedSource(ITable table)
        {
            while (table is IDerivedTable)
            {
                if (((IDerivedTable)table).SourceTableCount != 1)
                {
                    throw Exceptions.BadSourceTableCount(1,
                        ((IDerivedTable)table).SourceTableCount);
                }
                table = ((IDerivedTable)table).GetSourceTable(0);
            }
            return table;
        }

        public static bool IsNullOrEmpty(Array array)
        {
            return (array == null || array.Length == 0);
        }
}
}
