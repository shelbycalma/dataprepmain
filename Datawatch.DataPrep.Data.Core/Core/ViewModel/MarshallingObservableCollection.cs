using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Threading;

namespace Datawatch.DataPrep.Data.Core.ViewModel
{
    public class MarshallingObservableCollection<T> : ObservableCollection<T>
    {
        public override event NotifyCollectionChangedEventHandler CollectionChanged;

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged == null)
            {
                return;
            }

            Dispatcher dispatcher = GetDispatcher();

            if (dispatcher != null && dispatcher.CheckAccess() == false)
            {
                dispatcher.Invoke(
                    DispatcherPriority.DataBind,
                    (Action)(() => OnCollectionChanged(e)));
                return;
            }
            
            foreach (NotifyCollectionChangedEventHandler handler in
                CollectionChanged.GetInvocationList())
            {
                handler.Invoke(this, e);
            }
        }

        private Dispatcher GetDispatcher()
        {
            if (CollectionChanged == null)
            {
                return null;
            }
            foreach (NotifyCollectionChangedEventHandler handler in
                CollectionChanged.GetInvocationList())
            {
                DispatcherObject target = handler.Target as DispatcherObject;
                if (target != null)
                {
                    return target.Dispatcher;
                }
            }
            return null;
        }
    }
}
