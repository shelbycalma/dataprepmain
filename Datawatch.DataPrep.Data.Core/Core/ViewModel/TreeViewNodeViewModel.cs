﻿using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.ViewModel
{
    public class TreeViewNodeViewModel : ViewModelBase
    {
        private readonly MarshallingObservableCollection<TreeViewNodeViewModel> children =
            new MarshallingObservableCollection<TreeViewNodeViewModel>();

        private TreeViewNodeViewModel parent;
        private bool? isSelected;
        private bool isExpanded;

        public TreeViewNodeViewModel(
            TreeViewNodeViewModel parent = null,
            MarshallingObservableCollection<TreeViewNodeViewModel> children = null)
        {
            this.parent = parent;
            this.children =
                children ?? new MarshallingObservableCollection<TreeViewNodeViewModel>();
        }

        public TreeViewNodeViewModel Parent
        {
            get { return parent; }
            set
            {
                parent = value;
                OnPropertyChanged("Parent");
            }
        }

        public MarshallingObservableCollection<TreeViewNodeViewModel> Children
        {
            get { return children; }
        }

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is selected.
        /// </summary>
        public virtual bool? IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets/sets whether the TreeViewItem 
        /// associated with this object is expanded.
        /// </summary>
        public virtual bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                if (value != isExpanded)
                {
                    isExpanded = value;
                    OnPropertyChanged("IsExpanded");
                }

                // Expand all the way up to the root.
                if (isExpanded && parent != null)
                {
                    parent.IsExpanded = true;
                }
            }
        }

        public void RecursiveExpand()
        {
            IsExpanded = true;
            foreach (TreeViewNodeViewModel child in Children)
            {
                if (child == null)
                {
                    continue;
                }
                child.RecursiveExpand();
            }
        }
    }
}
