﻿using System;
using System.ComponentModel;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.ViewModel
{
    public class QueryBuilderTableViewModel<T> : QueryBuilderViewModel<T> where T : DatabaseColumn
    {
        private readonly IQueryBuilderTableClient client;
        private SchemaAndTable[] tables;

        private ICollectionView tablesCollectionView;
        private ICommand loadTablesCommand;

        public QueryBuilderTableViewModel(IQueryBuilderTableClient client,
            QuerySettings<T> querySettings)
            : base(client, querySettings)
        {
            if (client == null)
            {
                throw Exceptions.ArgumentNull("client");
            }
            if (querySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }

            this.client = client;
        }

        public ICommand LoadTablesCommand
        {
            get
            {
                if (loadTablesCommand == null)
                {
                    loadTablesCommand = new DelegateCommand(LoadTables,
                        CanLoadTables);
                }
                return loadTablesCommand;
            }
        }

        public bool CanLoadTables()
        {
            return client.CanLoadTables;
        }

        public void LoadTables()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                
                LoadTables(true);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        public void LoadTables(bool fireChanged)
        {
            tables = null;
            string val = null;
            int ky = 0;

            try
            {
                tables = client.Tables;
                if (querySettings.SelectedTable == null)
                {
                    querySettings.SelectedTable = tables.FirstOrDefault();
                }
                querySettings.ErrorMessage = String.Empty;
            }
            catch (System.Data.Odbc.OdbcException e)
            {
                Log.Exception(e);
                List<string> err = new List<string>();
                List<string> dErr = new List<string>();
                string errMsg = "";
                for (int i = 0; i < e.Errors.Count; i++)
                      err.Add(e.Errors[i].Message);
                dErr = err.Distinct().ToList();

                for (int i = 0; i < dErr.Count; i++)
                    errMsg = errMsg + dErr[i];

                if (errMsg.Contains("400 Bad Request"))
                {
                    errMsg = Resources.UiQueryBuilderErrorMessageBadRequest;
                }
                else if (errMsg.Contains("The driver requires a user prompt for OAuth authentication"))
                {
                    errMsg = Resources.UiQueryBuilderErrorMessageOAuth;
                }

                querySettings.ErrorMessage = errMsg;
                tables = new SchemaAndTable[0];
            }
            catch (Exception ex)
            {
                string errMsg = "";
                Log.Exception(ex);
				if (ex.Message.Contains("The driver requires a user prompt for OAuth authentication"))
				{
                    errMsg = Resources.UiQueryBuilderErrorMessageOAuth;
				}
				else
				{
                    errMsg = ex.Message;
				}

                querySettings.ErrorMessage = errMsg;
                tables = new SchemaAndTable[0];
            }

            tablesCollectionView = CollectionViewSource.GetDefaultView(tables);
            tablesCollectionView.Filter = tables_Filter;
            if (fireChanged)
            {
                OnPropertyChanged("Tables");
            }
        }

        public SchemaAndTable[] Tables
        {
            get
            {
                if (tables != null)
                {
                    return tables;
                }
                if (querySettings.SelectedTable != null)
                {
                    LoadTables(false);
                }
                return tables;
            }
        }

        private bool tables_Filter(object item)
        {

            SchemaAndTable fullTableName = item as SchemaAndTable;
            if (fullTableName == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(querySettings.TablesFilter))
            {
                return true;
            }
            bool visible = fullTableName.ToString().IndexOf(
                querySettings.TablesFilter, filterComparison) >= 0;
            return visible;
        }

        protected override void QuerySettings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TablesFilter")
            {
                if (tablesCollectionView != null)
                {
                    tablesCollectionView.Refresh();
                }
            }
            base.QuerySettings_PropertyChanged(sender, e);
        }
    }
}
