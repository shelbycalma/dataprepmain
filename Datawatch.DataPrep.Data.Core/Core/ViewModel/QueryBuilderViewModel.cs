﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.ViewModel
{
    public class QueryBuilderViewModel<T> : ViewModelBase where T : DatabaseColumn
    {
        private readonly IQueryBuilderClient client;
        protected readonly QuerySettings<T> querySettings;

        private IEnumerable<DatabaseColumn> availableColumns;

        private ObservableCollection<DatabaseColumn> columns =
            new ObservableCollection<DatabaseColumn>();

        private ICollectionView columnsCollectionView;

        protected static readonly StringComparison filterComparison =
            StringComparison.InvariantCultureIgnoreCase;

        private ICommand deleteColumnsCommand;

        public event EventHandler DoUpdateQuery;

        public QueryBuilderViewModel(IQueryBuilderClient client,
            QuerySettings<T> querySettings)
        {
            if (client == null)
            {
                throw Exceptions.ArgumentNull("client");
            }
            if (querySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }

            this.client = client;

            this.querySettings = querySettings;

            querySettings.PropertyChanged += QuerySettings_PropertyChanged;
        }

        public IEnumerable<DatabaseColumn> AvailableColumns
        {
            get
            {
                if (availableColumns != null)
                {
                    return availableColumns;
                }

                availableColumns = client.Columns;

                if (availableColumns == null)
                {
                    return null;
                }

                OnPropertyChanged("ColumnsToCopy");
                OnPropertyChanged("DatetimeColumns");
                OnPropertyChanged("DateColumns");
                OnPropertyChanged("TimeColumns");
                return availableColumns;
            }
        }

        public IEnumerable<DatabaseColumn> ColumnsToCopy
        {
            get
            {
                if (AvailableColumns == null)
                {
                    return null;
                }
                return AvailableColumns.Where(o =>
                    o.ColumnType != ColumnType.Text);
            }
        }

        public virtual DatabaseColumn CreateColumn(object v)
        {
            return new DatabaseColumn((DatabaseColumn)v);
        }

        public DatabaseColumn NewColumn
        {
            get
            {
                return null;
            }
            set
            {
                DatabaseColumn newCol = CreateColumn(value);
                newCol.UserDefined = true;
                newCol.AppliedParameterName = "";

                newCol.AggregateType =
                    GetDefaultAggregationMethod(newCol.ColumnType);

                newCol.PropertyChanged += AvailableColumns_PropertyChanged;
                columns.Add(newCol);
            }
        }

        public ObservableCollection<DatabaseColumn> Columns
        {
            get
            {
                columns = new ObservableCollection<DatabaseColumn>();

                querySettings.PredicateColumns.Clear();
                List<DatabaseColumn> duplicateColumns = new List<DatabaseColumn>();
                if (AvailableColumns == null)
                {
                    return columns;
                }
                foreach (DatabaseColumn availableColumn in AvailableColumns)
                {
                    if (availableColumn.AppliedParameterName != null)
                    {
                        querySettings.PredicateColumns.Add(availableColumn);
                    }

                    DatabaseColumn column = CreateColumn(availableColumn);
                    foreach (DatabaseColumn selectedCol in
                        querySettings.SelectedColumns)
                    {
                        if (selectedCol.ColumnName == column.ColumnName &&
                            column.Selected)
                        {

                            DatabaseColumn duplicateColumn =
                                        CreateColumn(selectedCol);
                            duplicateColumn.Selected = true;
                            duplicateColumn.UserDefined = true;
                            duplicateColumn.AppliedParameterName = "";
                            duplicateColumn.PropertyChanged +=
                                AvailableColumns_PropertyChanged;
                            duplicateColumns.Add(duplicateColumn);
                        }
                        else if (selectedCol.ColumnName == column.ColumnName &&
                                column.Selected == false)
                        {
                            column.Selected = true;
                            column.AggregateType = selectedCol.AggregateType;
                            column.PropertyChanged +=
                                AvailableColumns_PropertyChanged;
                        }
                    }

                    column.PropertyChanged -= AvailableColumns_PropertyChanged;
                    if (querySettings.ApplyFilter)
                    {
                        foreach (DatabaseColumn filteredCol in querySettings.FilteredColumns)
                        {
                            if (filteredCol.ColumnName == column.ColumnName)
                            {
                                column.FilterOperator = filteredCol.FilterOperator;
                                column.FilterValue = filteredCol.FilterValue;
                            }
                        }
                    }
                    column.PropertyChanged += AvailableColumns_PropertyChanged;

                    column.PropertyChanged -= AvailableColumns_PropertyChanged;
                    if (column.AggregateType == AggregateType.None)
                    {
                        column.AggregateType =
                            GetDefaultAggregationMethod(column.ColumnType);
                    }
                    column.PropertyChanged += AvailableColumns_PropertyChanged;
                    columns.Add(column);
                }
                foreach (DatabaseColumn duplicateCol in duplicateColumns)
                {
                    columns.Add(duplicateCol);
                }

                UpdateQuery();

                columnsCollectionView =
                    CollectionViewSource.GetDefaultView(columns);
                columnsCollectionView.Filter = columns_Filter;

                return columns;
            }
        }

        public IEnumerable<string> DatetimeColumns
        {
            get
            {
                if (AvailableColumns == null) return null;
                List<string> datetimeColumns = new List<string>();
                foreach (DatabaseColumn col in AvailableColumns)
                {
                    if (ParseDataTypeName(col.ColumnTypeName) ==
                        TimeSpanType.DateTime)
                    {
                        datetimeColumns.Add(col.ColumnName);
                    }
                }

                return datetimeColumns;
            }
        }

        public IEnumerable<string> DateColumns
        {
            get
            {
                if (AvailableColumns == null) return null;
                List<string> datetimeColumns = new List<string>();
                foreach (DatabaseColumn col in AvailableColumns)
                {
                    if (ParseDataTypeName(col.ColumnTypeName) ==
                        TimeSpanType.Date)
                    {
                        datetimeColumns.Add(col.ColumnName);
                    }
                }

                return datetimeColumns;
            }
        }

        public IEnumerable<string> TimeColumns
        {
            get
            {
                if (AvailableColumns == null) return null;
                List<string> datetimeColumns = new List<string>();
                foreach (DatabaseColumn col in AvailableColumns)
                {
                    if (ParseDataTypeName(col.ColumnTypeName) ==
                        TimeSpanType.Time)
                    {
                        datetimeColumns.Add(col.ColumnName);
                    }
                }

                return datetimeColumns;
            }
        }

        public void DeleteColumn(DatabaseColumn column)
        {
            columns.Remove(column);
            UpdateQuerySettings();
        }

        public virtual AggregateType GetDefaultAggregationMethod(
            ColumnType type)
        {
            if (type == ColumnType.Numeric)
            {
                return AggregateType.Sum;
            }
            else
            {
                return AggregateType.None;
            }
        }

        public ICommand DeleteColumnsCommand
        {
            get
            {
                if (deleteColumnsCommand == null)
                {
                    deleteColumnsCommand = new DelegateCommand<object>
                        (DeleteColumnExecute, CanDeleteColumn);
                }
                return deleteColumnsCommand;
            }
        }

        public void DeleteColumnExecute(object parameter)
        {
            DatabaseColumn databaseColumn = parameter as DatabaseColumn;
            if (MessageBox.Show(
                string.Format(
                Properties.Resources.ExQBConfirmDeleteColumn,
                    databaseColumn.ColumnName),
                Properties.Resources.UiQueryBuilderErrorMessageBoxTitle,
                MessageBoxButton.OKCancel, MessageBoxImage.Question) ==
                MessageBoxResult.OK)
            {
                DeleteColumn(databaseColumn);
            }
        }

        public bool CanDeleteColumn(object parameter)
        {
            if (parameter is DatabaseColumn)
            {
                return true;
            }
            return false;
        }

        public virtual bool EditableParameters
        {
            get { return false; }
        }

        private TimeSpanType ParseDataTypeName(string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
            {
                return TimeSpanType.None;
            }
            typeName = typeName.ToLower();

            if ((typeName.Contains("date") && typeName.Contains("time")) ||
                typeName.Contains("timestamp"))
            {
                return TimeSpanType.DateTime;
            }
            if (typeName.Contains("date"))
            {
                return TimeSpanType.Date;
            }
            if (typeName.Contains("time"))
            {
                return TimeSpanType.Time;
            }

            return TimeSpanType.None;
        }

        private void AvailableColumns_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            UpdateQuerySettings();
        }

        private void UpdateQuerySettings()
        {
            querySettings.PredicateColumns.Clear();
            querySettings.SelectedColumns.Clear();
            querySettings.FilteredColumns.Clear();
            foreach (DatabaseColumn availableColumn in columns)
            {
                if (availableColumn.AppliedParameterName != null)
                {
                    querySettings.PredicateColumns.Add(availableColumn);
                }
                if (availableColumn.Selected)
                {
                    querySettings.SelectedColumns.Add(availableColumn);
                }
                if (querySettings.ApplyFilter)
                {
                    if (!availableColumn.FilterOperator.Equals(FilterOperator.None) && !String.IsNullOrEmpty(availableColumn.FilterValue))
                    {
                        querySettings.FilteredColumns.Add(availableColumn);
                    }
                }
            }
            UpdateQuery();
        }

        protected virtual void QuerySettings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedTable")
            {
                availableColumns = null;
                OnPropertyChanged("Columns");

            }

            if (e.PropertyName == "ColumnsFilter")
            {
                if (columnsCollectionView != null)
                {
                    columnsCollectionView.Refresh();
                }
            }

            UpdateQuery();
        }

        private void UpdateQuery()
        {
            if (DoUpdateQuery != null)
            {
                DoUpdateQuery(this, EventArgs.Empty);
            }
        }

        private bool columns_Filter(object item)
        {
            DatabaseColumn column = item as DatabaseColumn;
            if (column == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(querySettings.ColumnsFilter))
            {
                return true;
            }
            bool visible = column.ColumnName.IndexOf(
                querySettings.ColumnsFilter, filterComparison) >= 0;
            return visible;
        }
    }
}
