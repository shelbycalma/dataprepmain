﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Wrapper
{
    public static class WrapperColumnMetaDataFactory
    {
        public static WrapperColumnMetaData Create(
            IColumn owner, IColumnMetaData source)
        {
            if (owner == null) {
                throw Exceptions.ArgumentNull("owner");
            }
            if (source == null) {
                throw Exceptions.ArgumentNull("source");
            }
            if (owner is INumericColumn ||
                owner is INumericTimeseriesColumn) {
                // Test for INumericTimeseriesColumn is "verbose",
                // because it extends INumericColumn.
                return CreateNumeric(owner, source);
            }
            if (owner is ITextColumn ||
                owner is ITextTimeseriesColumn) {
                // ITextTimeseriesColumn does not extend ITextColumn.
                return CreateText(owner, source);
            }
            if (owner is ITimeColumn) {
                return CreateTime(owner, source);
            }

            throw Exceptions.WrapperMetaDataBadOwnerType(
                owner.Name, owner.GetType());
        }

        private static WrapperColumnMetaData.Numeric CreateNumeric(
            IColumn owner, IColumnMetaData source)
        {
            INumericColumnMetaData meta =
                source as INumericColumnMetaData;
            if (source != null && meta == null) {
                throw Exceptions.WrapperMetaDataBadSourceType(
                    owner.Name, owner.GetType(),
                    typeof(INumericColumnMetaData), source.GetType());
            }
            if (meta is IRankingColumnMetaData) {
                return new WrapperColumnMetaData.Ranking(owner) {
                    SourceMetaData = meta
                };
            }
            if (meta is ICalculatedColumnMetaData) {
                return new WrapperColumnMetaData.Calculated(owner) {
                    SourceMetaData = meta
                };
            }
            return new WrapperColumnMetaData.Numeric(owner) {
                SourceMetaData = meta
            };
        }

        private static WrapperColumnMetaData.Text CreateText(
            IColumn owner, IColumnMetaData source)
        {
            ITextColumnMetaData meta =
                source as ITextColumnMetaData;
            if (source != null && meta == null) {
                throw Exceptions.WrapperMetaDataBadSourceType(
                    owner.Name, owner.GetType(),
                    typeof(ITextColumnMetaData), source.GetType());
            }
            if (meta is ITimeBucketColumnMetaData) {
                return new WrapperColumnMetaData.TimeBucket(owner) {
                    SourceMetaData = meta
                };
            }
            if (meta is INumericBucketColumnMetaData) {
                return new WrapperColumnMetaData.NumericBucket(owner) {
                    SourceMetaData = meta
                };
            }
            if (meta is IAutoKeyColumnMetaData) {
                return new WrapperColumnMetaData.AutoKeyColumn(owner) {
	            SourceMetaData = meta
                };
            }
            if (meta is ITextBucketColumnMetaData) {
                return new WrapperColumnMetaData.TextBucket(owner) {
                    SourceMetaData = meta
                };
            }
            if (meta is ICalculatedColumnMetaData)
            {
                return new WrapperColumnMetaData.CalculatedText(owner)
                {
                    SourceMetaData = meta
                };
            }
            return new WrapperColumnMetaData.Text(owner) {
                SourceMetaData = meta
            };
        }

        private static WrapperColumnMetaData.Time CreateTime(
            IColumn owner, IColumnMetaData source)
        {
            ITimeColumnMetaData meta =
                source as ITimeColumnMetaData;
            if (source != null && meta == null) {
                throw Exceptions.WrapperMetaDataBadSourceType(
                    owner.Name, owner.GetType(),
                    typeof(ITimeColumnMetaData), source.GetType());
            }
            return new WrapperColumnMetaData.Time(owner) {
                SourceMetaData = meta
            };
        }
    }
}
