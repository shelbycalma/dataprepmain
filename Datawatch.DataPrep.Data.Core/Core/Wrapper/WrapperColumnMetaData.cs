﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Wrapper
{
    public class WrapperColumnMetaData :
        WrapperMetaData,
        IColumnMetaData,
        IMutableHiddenColumn,
        IMutableColumnTitle,
        IDerivedColumnMetaData
    {
        protected const string DataTypeProperty = "DataType";
        protected const string HiddenProperty = "Hidden";
        protected const string TitleProperty = "Title";

        private readonly IColumn owner;

        private IColumnMetaData source;

        internal WrapperColumnMetaData(IColumn owner)
        {
            if (owner == null) {
                throw Exceptions.ArgumentNull("owner");
            }
            this.owner = owner;
        }

        public static WrapperColumnMetaData Create(
            IColumn owner, IColumnMetaData source)
        {
            return WrapperColumnMetaDataFactory.Create(owner, source);
        }

        protected IColumn GetOwnerTableColumn(string name)
        {
            for (int i = 0; i < owner.Table.ColumnCount; i++) {
                IColumn column = owner.Table.GetColumn(i);
                if (column.Name.Equals(name)) {
                    return column;
                }
            }
            return null;
        }

        protected IColumn Owner
        {
            get { return owner; }
        }

        public Type DataType
        {
            get { return GetValue<Type>(DataTypeProperty); }
            set { SetValue<Type>(DataTypeProperty, value); }
        }

        public void ClearDataType()
        {
            ClearValue(DataTypeProperty);
        }

        public bool Hidden
        {
            get { return GetValue<bool>(HiddenProperty); }
            set { SetValue<bool>(HiddenProperty, value); }
        }

        public void ClearHidden()
        {
            ClearValue(HiddenProperty);
        }

        public string Title
        {
            get { return GetValue<string>(TitleProperty); }
            set { SetValue<string>(TitleProperty, value); }
        }

        public void ClearTitle()
        {
            ClearValue(TitleProperty);
        }

        protected override object GetSourceOrDefaultValue(string property)
        {
            if (DataTypeProperty.Equals(property)) {
                return source != null ? source.DataType : null;
            }
            if (HiddenProperty.Equals(property)) {
                return source != null ? source.Hidden : false;
            }
            if (TitleProperty.Equals(property)) {
                return source != null ? source.Title : null;
            }

            return base.GetSourceOrDefaultValue(property);
        }

        public IColumnMetaData SourceMetaData
        {
            get { return source; }
            set { source = value; }
        }


        public class Numeric :
            WrapperColumnMetaData,
            INumericColumnMetaData,
            IMutableFormatString
        {
            protected const string DomainProperty = "Domain";
            protected const string FormatProperty = "Format";
            protected const string MeanProperty = "Mean";
            protected const string StandardDeviationProperty = "StandardDeviation";

            public Numeric(IColumn owner) : base(owner)
            {
                this.DataType = typeof(double);
            }

            public NumericInterval Domain
            {
                get { return GetValue<NumericInterval>(DomainProperty); }
                set { SetValue<NumericInterval>(DomainProperty, value); }
            }

            public void ClearDomain()
            {
                ClearValue(DomainProperty);
            }

            public string Format
            {
                get { return GetValue<string>(FormatProperty); }
                set { SetValue<string>(FormatProperty, value); }
            }

            public void ClearFormat()
            {
                ClearValue(FormatProperty);
            }

            public double Mean
            {
                get { return GetValue<double>(MeanProperty); }
                set { SetValue<double>(MeanProperty, value); }
            }

            public void ClearMean()
            {
                ClearValue(MeanProperty);
            }

            public double StandardDeviation
            {
                get { return GetValue<double>(StandardDeviationProperty); }
                set { SetValue<double>(StandardDeviationProperty, value); }
            }

            public void ClearStandardDeviation()
            {
                ClearValue(StandardDeviationProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                INumericColumnMetaData numeric =
                    source as INumericColumnMetaData;

                if (DomainProperty.Equals(property)) {
                    return numeric != null ? numeric.Domain : null;
                }
                if (FormatProperty.Equals(property)) {
                    return numeric != null ? numeric.Format : null;
                }
                if (MeanProperty.Equals(property)) {
                    return numeric != null ? numeric.Mean : NumericValue.Empty;
                }
                if (StandardDeviationProperty.Equals(property)) {
                    return numeric != null ? numeric.StandardDeviation : NumericValue.Empty;
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }


        public class Text :
            WrapperColumnMetaData,
            ITextColumnMetaData
        {
            protected const string CardinalityProperty = "Cardinality";
            protected const string ComparerProperty = "Comparer";
            protected const string DomainProperty = "Domain";

            public Text(IColumn owner) : base(owner)
            {
                this.DataType = typeof(string);
            }

            public double Cardinality
            {
                get { return GetValue<double>(CardinalityProperty); }
                set { SetValue<double>(CardinalityProperty, value); }
            }

            public void ClearCardinality()
            {
                ClearValue(CardinalityProperty);
            }

            public IComparer<string> Comparer
            {
                get { return GetValue<IComparer<string>>(ComparerProperty); }
                set { SetValue<IComparer<string>>(ComparerProperty, value); }
            }

            public void ClearComparer()
            {
                ClearValue(ComparerProperty);
            }

            public string[] Domain
            {
                get { return GetValue<string[]>(DomainProperty); }
                set { SetValue<string[]>(DomainProperty, value); }
            }

            public void ClearDomain()
            {
                ClearValue(DomainProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                ITextColumnMetaData text = source as ITextColumnMetaData;

                if (CardinalityProperty.Equals(property)) {
                    return text != null ? text.Cardinality : 0.0;
                }
                if (ComparerProperty.Equals(property)) {
                    return text != null ? text.Comparer : null;
                }
                if (DomainProperty.Equals(property)) {
                    return text != null ? text.Domain : null;
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }


        public class Time :
            WrapperColumnMetaData,
            ITimeColumnMetaData,
            IMutableFormatString
        {
            protected const string DomainProperty = "Domain";
            protected const string FormatProperty = "Format";

            public Time(IColumn owner) : base(owner)
            {
                this.DataType = typeof(DateTime);
            }

            public TimeValueInterval Domain
            {
                get { return GetValue<TimeValueInterval>(DomainProperty); }
                set { SetValue<TimeValueInterval>(DomainProperty, value); }
            }

            public void ClearDomain()
            {
                ClearValue(DomainProperty);
            }

            public string Format
            {
                get { return GetValue<string>(FormatProperty); }
                set { SetValue<string>(FormatProperty, value); }
            }

            public void ClearFormat()
            {
                ClearValue(FormatProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                ITimeColumnMetaData time = source as ITimeColumnMetaData;

                if (DomainProperty.Equals(property)) {
                    return time != null ? time.Domain : null;
                }
                if (FormatProperty.Equals(property)) {
                    return time != null ? time.Format : null;
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }
    
        public class Calculated :
            Numeric,
            ICalculatedColumnMetaData
        {
            protected const string FormulaProperty = "Formula";
            protected const string IsTimeWindowCalculationProperty =
                "IsTimeWindowCalculation";

            public Calculated(IColumn owner) : base(owner)
            {
            }

            public string Formula
            {
                get { return GetValue<string>(FormulaProperty); }
                set { SetValue<string>(FormulaProperty, value); }
            }

            public void ClearFormula()
            {
                ClearValue(FormulaProperty);
            }

            public bool IsTimeWindowCalculation
            {
                get { return GetValue<bool>(IsTimeWindowCalculationProperty); }
                set { SetValue<bool>(IsTimeWindowCalculationProperty, value); }
            }

            public void ClearIsTimeWindowCalculation()
            {
                ClearValue(IsTimeWindowCalculationProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                ICalculatedColumnMetaData calculated =
                    this.SourceMetaData as ICalculatedColumnMetaData;

                if (FormulaProperty.Equals(property)) {
                    return calculated != null ? calculated.Formula : null;
                }
                if (IsTimeWindowCalculationProperty.Equals(property)) {
                    return calculated != null ?
                        calculated.IsTimeWindowCalculation :
                        CalcUtils.ContainsTimeWindowFunction(this.Formula);
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }
    
        public class CalculatedText :
            Text,
            ICalculatedColumnMetaData
        {
            protected const string FormulaProperty = "Formula";
            protected const string IsTimeWindowCalculationProperty =
                "IsTimeWindowCalculation";

            public CalculatedText(IColumn owner)
                : base(owner)
            {
            }

            public string Formula
            {
                get { return GetValue<string>(FormulaProperty); }
                set { SetValue<string>(FormulaProperty, value); }
            }

            public void ClearFormula()
            {
                ClearValue(FormulaProperty);
            }

            public bool IsTimeWindowCalculation
            {
                get { return GetValue<bool>(IsTimeWindowCalculationProperty); }
                set { SetValue<bool>(IsTimeWindowCalculationProperty, value); }
            }

            public void ClearIsTimeWindowCalculation()
            {
                ClearValue(IsTimeWindowCalculationProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                ICalculatedColumnMetaData calculated =
                    this.SourceMetaData as ICalculatedColumnMetaData;

                if (FormulaProperty.Equals(property)) {
                    return calculated != null ? calculated.Formula : null;
                }
                if (IsTimeWindowCalculationProperty.Equals(property)) {
                    return calculated != null ?
                        calculated.IsTimeWindowCalculation :
                        CalcUtils.ContainsTimeWindowFunction(this.Formula);
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }

        public class NumericBucket :
            Text,
            INumericBucketColumnMetaData
        {
            protected const string NumericColumnProperty = "NumericColumn";

            public NumericBucket(IColumn owner) : base(owner)
            {
            }

            public IColumn AuxColumn
            {
                get { return this.NumericColumn; }
            }

            public INumericColumn NumericColumn
            {
                get { return GetValue<INumericColumn>(NumericColumnProperty); }
                set { SetValue<INumericColumn>(NumericColumnProperty, value); }
            }

            public void ClearNumericColumn()
            {
                ClearValue(NumericColumnProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                INumericBucketColumnMetaData bucket =
                    source as INumericBucketColumnMetaData;

                if (NumericColumnProperty.Equals(property)) {
                    if (bucket == null) {
                        return null;
                    }
                    return (INumericColumn)
                        GetOwnerTableColumn(bucket.NumericColumn.Name);
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }

        public class TimeBucket :
            Text,
            ITimeBucketColumnMetaData
        {
            protected const string TimeColumnProperty = "TimeColumn";
            protected const string TimePartProperty = "TimePart";

            public TimeBucket(IColumn owner) : base(owner)
            {
            }

            public IColumn AuxColumn
            {
                get { return this.TimeColumn; }
            }

            public ITimeColumn TimeColumn
            {
                get { return GetValue<ITimeColumn>(TimeColumnProperty); }
                set { SetValue<ITimeColumn>(TimeColumnProperty, value); }
            }

            public void ClearTimeColumn()
            {
                ClearValue(TimeColumnProperty);
            }

            public TimePart TimePart
            {
                get { return GetValue<TimePart>(TimePartProperty); }
                set { SetValue<TimePart>(TimePartProperty, value); }
            }

            public void ClearTimePart()
            {
                ClearValue(TimePartProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                ITimeBucketColumnMetaData bucket =
                    source as ITimeBucketColumnMetaData;

                if (TimeColumnProperty.Equals(property)) {
                    if (bucket == null) {
                        return null;
                    }
                    return (ITimeColumn)
                        GetOwnerTableColumn(bucket.TimeColumn.Name);
                }
                if (TimePartProperty.Equals(property)) {
                    return bucket != null ? bucket.TimePart : TimePart.Year;
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }

        public class TextBucket :
            Text,
            ITextBucketColumnMetaData
        {
            protected const string TextColumnProperty = "TextColumn";

            public TextBucket(IColumn owner) : base(owner)
            {
            }

            public IColumn AuxColumn
            {
                get { return this.TextColumn; }
            }

            public ITextColumn TextColumn
            {
                get { return GetValue<ITextColumn>(TextColumnProperty); }
                set { SetValue<ITextColumn>(TextColumnProperty, value); }
            }

            public void ClearTextColumn()
            {
                ClearValue(TextColumnProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                ITextBucketColumnMetaData bucket =
                    source as ITextBucketColumnMetaData;

                if (TextColumnProperty.Equals(property))
                {
                    if (bucket == null)
                    {
                        return null;
                    }
                    return (ITextColumn)
                        GetOwnerTableColumn(bucket.TextColumn.Name);
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }

        public class Ranking :
            Numeric,
            IRankingColumnMetaData
        {
            protected const string RankingColumnProperty = "RankingColumn";
            protected const string RankingSortOrderProperty = "RankingSortOrder";

            public Ranking(IColumn owner) : base(owner)
            {
            }

            public IColumn AuxColumn
            {
                get { return this.RankingColumn; }
            }

            public INumericColumn RankingColumn
            {
                get { return GetValue<INumericColumn>(RankingColumnProperty); }
                set { SetValue<INumericColumn>(RankingColumnProperty, value); }
            }

            public void ClearRankingColumn()
            {
                ClearValue(RankingColumnProperty);
            }

            public SortOrder RankingSortOrder
            {
                get { return GetValue<SortOrder>(RankingSortOrderProperty); }
                set { SetValue<SortOrder>(RankingSortOrderProperty, value); }
            }

            public void ClearRankingSortOrder()
            {
                ClearValue(RankingSortOrderProperty);
            }

            protected override object GetSourceOrDefaultValue(string property)
            {
                IRankingColumnMetaData ranking =
                    source as IRankingColumnMetaData;

                if (RankingColumnProperty.Equals(property)) {
                    if (ranking == null) {
                        return null;
                    }
                    return (INumericColumn)
                        GetOwnerTableColumn(ranking.RankingColumn.Name);
                }
                if (RankingSortOrderProperty.Equals(property)) {
                    return ranking != null ?
                        ranking.RankingSortOrder : SortOrder.Ascending;
                }

                return base.GetSourceOrDefaultValue(property);
            }
        }

        public class AutoKeyColumn :
            Text,
            IAutoKeyColumnMetaData
        {
            public AutoKeyColumn(IColumn owner) : base(owner)
            {
            }

            public IColumn AuxColumn
            {
                get { return null; }
            }
        }
    }
}
