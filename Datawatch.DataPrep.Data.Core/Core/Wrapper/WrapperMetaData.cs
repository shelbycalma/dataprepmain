﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Wrapper
{
    public abstract class WrapperMetaData
    {
        private readonly Dictionary<string, object> values;

        protected WrapperMetaData()
        {
            values = new Dictionary<string, object>();
        }

        protected void ClearValue(string property)
        {
            if (values.ContainsKey(property)) {
                values.Remove(property);
            }
        }

        protected virtual object GetSourceOrDefaultValue(string property)
        {
            throw Exceptions.WrapperMetaDataBadPropertyName(property);
        }

        protected T GetValue<T>(string property)
        {
            if (values.ContainsKey(property)) {
                return (T) values[property];
            }
            return (T) GetSourceOrDefaultValue(property);
        }

        protected void SetValue<T>(string property, T value)
        {
            values[property] = value;
        }
    }
}
