﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers.RserveCLI2
{
    public class SexpArrayChron : SexpArrayDouble
    {
        private const int SecondsInDay = 60*60*24;
        private static readonly DateTime Origin = new DateTime(1970, 1, 1);
        private const string ClassName = "chron";

        public SexpArrayChron(IEnumerable<DateTime> theValue)
            : base(theValue.Select(ChronToRDouble))
        {
            Attributes["class"] = new SexpArrayString(ClassName);
        }

        internal SexpArrayChron(IEnumerable<double> values)
            : base( values )
        {}

        internal new List<DateTime> Value
        {
            get
            {
                return (base.Value.Select(RDoubleToChron).ToList());
            }
        }

        public override object ToNative()
        {
            return Value.ToArray();
        }

        private static double ChronToRDouble(DateTime dateTime)
        {
            return (dateTime.Subtract(Origin).TotalSeconds) / SecondsInDay;
        }

        private static DateTime RDoubleToChron(double rdate)
        {
            return Origin.AddSeconds(rdate * SecondsInDay);
        }
    }
}
