﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers.RserveCLI2
{
    public class SexpArrayPosixCt : SexpArrayDouble
    {
        private static readonly DateTime Origin = new DateTime(1970, 1, 1);
        private const string ClassName = "POSIXct";
        private const string BaseName = "POSIXt";

        public SexpArrayPosixCt(IEnumerable<DateTime> theValue)
            : base(theValue.Select(PosixCtToRDouble))
        {
            Attributes["class"] = new SexpArrayString(new[]{ClassName, BaseName});
        }

        internal SexpArrayPosixCt(IEnumerable<double> values)
            : base(values)
        {
        }

        internal new List<DateTime> Value
        {
            get
            {
                return (base.Value.Select(RDoubleToPosixCt).ToList());
            }
        }

        public override object ToNative()
        {
            return Value.ToArray();
        }

        private static double PosixCtToRDouble(DateTime dateTime)
        {
            return dateTime.Subtract(Origin).TotalSeconds;
        }

        private static DateTime RDoubleToPosixCt(double rdate)
        {
            return Origin.AddSeconds(rdate);
        }
    }
}
