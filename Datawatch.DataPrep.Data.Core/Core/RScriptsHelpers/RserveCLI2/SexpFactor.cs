﻿namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers.RserveCLI2
{
    public class SexpFactor : Sexp
    {
        private readonly int[] payload;
        private readonly string[] levels;

        public SexpFactor(int[] ids, string[] levels)
        {
            this.payload = ids ?? new int[0];
            this.levels = levels ?? new string[0];
	    }

        public override int Count
        {
            get { return payload.Length; }
        }

        public override object ToNative()
        {
            string[] values = new string[payload.Length];
            int i = 0;
            while (i < payload.Length)
            {
                int li = payload[i] - 1;
                values[i] = (li < 0 || li > levels.Length) ? null : levels[li];
                i++;
            }

            return values;
        }
    }
}
