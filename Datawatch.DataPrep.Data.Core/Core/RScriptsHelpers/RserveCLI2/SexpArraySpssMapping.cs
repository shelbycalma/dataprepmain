﻿namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers.RserveCLI2
{
    public class SexpArraySpssMapping : SexpArrayString
    {
        public SexpArraySpssMapping(Sexp expression)
        {
            string[] mapping = expression.Attributes["value.labels"].AsStrings;

            foreach (double value in expression)
            {
                Value.Add(mapping[(int)value - 1]);
            }
        }

        public static bool IsSpssMapping(Sexp expression)
        {
            if (!(expression is SexpArrayDouble) ||
                expression.Attributes == null ||
                !expression.Attributes.ContainsKey("value.labels"))
            {
                return false;
            }

            string[] values = expression.Attributes["value.labels"].AsStrings;

            if (values.Length == 0 ||
                values.Length == 1 && values[0] == "\x01NULL\x01")
            {
                return false;
            }

            foreach (double sourceValue in expression)
            {
                if (sourceValue%1 != 0 ||
                    sourceValue < 1 ||
                    sourceValue > values.Length)
                {
                    return false;
                }
            }
            
            return true;
        }
    }
}
