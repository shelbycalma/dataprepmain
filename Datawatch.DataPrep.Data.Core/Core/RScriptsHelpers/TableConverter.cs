﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.RScriptsHelpers.RserveCLI2;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers
{
    public static class TableConverter
    {
        public static RowLimitTable GetTable(SexpList frame, int rowCountMax, 
            Exception error)
        {
            RowLimitTable table = new RowLimitTable();
            WriteTable(table, frame, rowCountMax, error);
            return table;
        }

        public static void WriteTable(RowLimitTable table, SexpList frame, 
            int rowCountMax, Exception error)
        {
            table.Error = error;

            int rowCount = frame.Values.Max(sexp => sexp.Count);
            if (rowCountMax > 0)
            {
                rowCount = Math.Min(rowCountMax, rowCount);
            }

            Dictionary<string,Column> columns = new Dictionary<string, Column>();

            for (int i = 0; i < table.ColumnCount; i++)
            {
                Column column = table.GetColumn(i);
                columns.Add(column.Name, column);
            }

            try
            {
                table.BeginUpdate();
                for (int i = 0; i < rowCount; i++)
                {
                    table.AddRow();
                }

                for (int i = 0; i < frame.Count; i++)
                {
                    Sexp expression = frame[i];
                    if(frame.Names == null)
                        throw new ArgumentException(Resources.ExRserveInvalidResponse);

                    if (SexpArraySpssMapping.IsSpssMapping(expression))
                    {
                        expression = new SexpArraySpssMapping(expression);
                    }

                    string colName = frame.Names[i];
                    Column column;

                    columns.TryGetValue(colName, out column);

                    if (expression is SexpArrayDate || expression is SexpArrayChron || expression is SexpArrayPosixCt)
                    {
                        if (column == null)
                        {
                            column = new TimeColumn(colName);
                        }
                        PopulateValues(table, column, rowCount, expression);
                    }
                    else if (expression is SexpArrayDouble || expression is SexpArrayBool || expression is SexpArrayInt)
                    {
                        if (column == null)
                        {
                            column = new NumericColumn(colName);
                        }
                        PopulateValues(table, column, rowCount, expression);
                    }
                    else if (expression is SexpArrayString || expression is SexpFactor)
                    {
                        if (column == null)
                        {
                            column = new TextColumn(colName);
                        }
                        PopulateValues(table, column, rowCount, expression);
                    }
                    else
                    {
                        throw new ArgumentException(string.Format(Resources.ExRserveUnsupportedColumnType, expression.GetType().Name));
                    }

                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        private static void PopulateValues(StandaloneTable table, Column column, int rowCount, Sexp expression)
        {
            if (column.Table == null)
            {
                table.AddColumn(column);
            }
            IList<object> values = new List<object>();
            object nativeObject = expression.ToNative();
            if (nativeObject is IEnumerable)
            {
                IEnumerable enumerableObject = nativeObject as IEnumerable;
                foreach (object element in enumerableObject)
                {
                    values.Add(element);
                }

                for (int i = 0; i < rowCount; i++)
                {
                    column.SetValue(table.GetRow(i), values[i]);
                }
            }
        }

        public static SexpList GetFrame(ITable table, DateClass dateClass)
        {
            IList<KeyValuePair<string, object>> columns = new List<KeyValuePair<string, object>>();

            for (int i = 0; i < table.ColumnCount; i++)
            {
                IColumn column = table.GetColumn(i);
                if (column is ITextColumn)
                {
                    ITextColumn textColumn = column as ITextColumn;
                    IList<string> values = new List<string>();
                    for (int j = 0; j < table.RowCount; j++)
                    {
                        string value = textColumn.GetTextValue(table.GetRow(j));
                        values.Add(value);
                    }
                    columns.Add(new KeyValuePair<string, object>(column.Name, Sexp.Make(values)));
                }
                else if (column is INumericColumn)
                {
                    INumericColumn numericColumn = column as INumericColumn;
                    IList<double> values = new List<double>();
                    for (int j = 0; j < table.RowCount; j++)
                    {
                        double value = numericColumn.GetNumericValue(table.GetRow(j));
                        values.Add(value);
                    }
                    columns.Add(new KeyValuePair<string, object>(column.Name, Sexp.Make(values)));
                }
                else if (column is ITimeColumn)
                {
                    ITimeColumn timeColumn = column as ITimeColumn;
                    IList<DateTime> values = new List<DateTime>();
                    for (int j = 0; j < table.RowCount; j++)
                    {
                        DateTime value = timeColumn.GetTimeValue(table.GetRow(j));
                        values.Add(value);
                    }

                    Sexp date;
                    switch (dateClass)
                    {
                        case DateClass.Date:
                            date = new SexpArrayDate(values);
                            break;
                        case DateClass.chron:
                            date = new SexpArrayChron(values);
                            break;
                        case DateClass.POSIXct:
                            date = new SexpArrayPosixCt(values);
                            break;
                        default:
                            throw new NotSupportedException(dateClass.ToString());
                    }
                    
                    columns.Add(new KeyValuePair<string, object>(column.Name, date));
                }
            }

            return Sexp.MakeDataFrame(columns);
        }
    }
}
