﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.RScriptsHelpers.RserveCLI2;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers
{
    public static class ExecutionHelper
    {
        public static RowLimitTable ExecuteTableScript(ITable inputData, RSettings settings,
                                                IEnumerable<ParameterValue> parameters)
        {
            return ExecuteTableScript(inputData, settings, parameters, 0);
        }

        public static RowLimitTable ExecuteTableScript(RSettings settings, IEnumerable<ParameterValue> parameters, int rowCount)
        {
            return ExecuteTableScript(null, settings, parameters, rowCount);
        }

        public static RowLimitTable ExecuteTableScript(ITable inputData, RSettings settings,
                                                IEnumerable<ParameterValue> parameters, int rowCount)
        {
            RowLimitTable resultTable = new RowLimitTable();
            ExecuteTableScript(inputData, settings, parameters, rowCount, 
                resultTable);
            return resultTable;
        }

        public static void ExecuteTableScript(ITable inputData, RSettings settings,
            IEnumerable<ParameterValue> parameters, int rowCount, 
            RowLimitTable resultTable)
        {
            using (RConnection connection = GetConnection(settings, parameters))
            {
                if (inputData != null)
                {
                    string frameName = ApplyParameters(settings.FrameName, parameters);
                    SexpList executionFrame = TableConverter.GetFrame(inputData, settings.DateClass);
                    connection[frameName] = executionFrame;
                }
                
                Sexp result;
                DateTime startTime = DateTime.Now;
                string script = ApplyParameters(settings.ScriptContent, parameters, settings.EncloseParametersInQuotes);
                try
                {
                    Log.Info(Resources.LogExecutingRScript, script);
                    result = RunScripts(connection, script);
                }
                catch (SocketException)
                {
                    Log.Error(Resources.ExcRserveTimeout);
                    throw new TimeoutException(Resources.ExcRserveTimeout);
                }
                catch (Exception e)
                {
                    string message = connection.Eval("geterrmessage()").ToString();
                    Log.Error(message);
                    throw new Exception(string.Format(Resources.ExRThrewAnError, message));
                }

                if (result is SexpList)
                {
                    IErrorProducer errorProducer = null;
                    if (inputData != null)
                    {
                        errorProducer =
                            TableUtils.FindErrorProducerInPipeline(inputData);
                    }
                    Exception error = errorProducer != null ?
                        errorProducer.Error : null;

                    
                    TableConverter.WriteTable(resultTable, result as SexpList, 
                        rowCount, error);

                    Log.Info(Resources.LogDoneExecutingRScript, (DateTime.Now - startTime).TotalSeconds,
                             resultTable.RowCount, resultTable.ColumnCount, script);
                    return;
                }
                
                throw new ArgumentException(Resources.ExRserveInvalidResponse);
            }
        }
        
        private static Sexp RunScripts(RConnection connection, string script)
        {
            if (script.Contains("\r\n"))
            {
                script = script.Replace("\r\n", "\n");
            }
            if (script.Contains("\r"))
            {
                script = script.Replace("\r", "\n");
            }
            script = script.Replace(Environment.NewLine, "\n");

            Sexp result = connection.Eval(script);
            return result;
        }

        private static RConnection GetConnection(RSettings settings, IEnumerable<ParameterValue> parameters)
        {
            try
            {
                string address = ApplyParameters(settings.Address, parameters);
                if (address == "localhost")
                {
                    address = "127.0.0.1";
                }

                int port = int.Parse(ApplyParameters(settings.Port, parameters));
                string username = ApplyParameters(settings.User, parameters);
                string password = ApplyParameters(settings.Password, parameters);

                IPAddress addressIp;
                if (IPAddress.TryParse(address, out addressIp))
                {
                    return new RConnection(addressIp, port, username, password, settings.Timeout * 1000);
                }

                return new RConnection(address, port, username, password, settings.Timeout * 1000);
            }
            catch (Exception)
            {
                Log.Error(Resources.ExcRserveTimeout);
                throw new TimeoutException(Resources.ExcRserveTimeout);
            }
        }

        public static bool TestConnection(RSettings settings, IEnumerable<ParameterValue> parameters)
        {
            try
            {
                using (RConnection connection = GetConnection(settings, parameters))
                {
                    connection.VoidEval("2==2");
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }


        internal static string ApplyParameters(string sourceString, IEnumerable<ParameterValue> parameters,
                                               bool encloseParametersInQuotes = false)
        {
            if (string.IsNullOrEmpty(sourceString)) return null;
            if (parameters == null || !parameters.Any()) return sourceString;

            var result = new ParameterEncoder
            {
                SourceString = sourceString,
                Parameters = parameters,
                NullOrEmptyString = "NULL",
                EnforceSingleQuoteEnclosure = encloseParametersInQuotes,
            }.Encoded();
                
            return result;
        }
    }
}