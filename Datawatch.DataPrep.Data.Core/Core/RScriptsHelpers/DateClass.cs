﻿namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers
{
    public enum DateClass
    {
        Date,
        chron,
        POSIXct
    }
}
