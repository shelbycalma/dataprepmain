﻿using System.Security;

namespace Datawatch.DataPrep.Data.Core.RScriptsHelpers
{
    public class RSettings
    {
        private SecureString _password;
        public string Address { get; set; }
        public string Port { get; set; }
        public string User { get; set; }
        public string Password
        {
            get { return SecurityUtils.ToInsecureString(_password); }
            set { this._password = SecurityUtils.ToSecureString(value);}
        }
        public string ScriptContent { get; set; }
        public bool ScriptsEnabled { get; set; }
        public bool EncloseParametersInQuotes { get; set; }
        public string FrameName { get; set; }
        public int Timeout { get; set; }
        public DateClass DateClass { get; set; }
    }
}
