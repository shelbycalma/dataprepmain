﻿using System.IO;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core
{
    public class SettingsHelper
    {
        public const string GlobalKey = "GlobalSettings";
        public const string PluginKey = "PluginSettings";
        public const string IconsKey = "Icons";
        public const string ColorSchemesKey = "ColorSchemes";
        public const string NumericKey = "Numeric";
        public const string TextKey = "Text";
        public const string EmailKey = "EmailSettings";

        private const string DirectorySettingsKey = "DirectorySettings";
        private const string DefaultWorkbookDirectoryKey = "DefaultWorkbookDirectory";
        private const string DefaultDataDirectoryKey = "DefaultDataDirectory";
        private const string LastWorkbookDirectoryKey = "LastWorkbookDirectory";
        private const string LastDataDirectoryKey = "LocalDataDirectory";
        
        public static void CreateDirectorySettings(PropertyBag globalSettings,
            string defaultWorkbookDirectory, string defaultDataDirectory)
        {
            PropertyBag bag = GetDirectorySettings(globalSettings);

            if (bag.Values.Any())
                return;

            bag.Values[DefaultWorkbookDirectoryKey] = defaultWorkbookDirectory;

            bag.Values[LastWorkbookDirectoryKey] = defaultWorkbookDirectory;
            if (!Path.IsPathRooted(bag.Values[LastWorkbookDirectoryKey]))
            {
                bag.Values[LastWorkbookDirectoryKey] =
                    Path.GetFullPath(bag.Values[LastWorkbookDirectoryKey]);
            }

            bag.Values[DefaultDataDirectoryKey] = defaultDataDirectory;

            bag.Values[LastDataDirectoryKey] = defaultDataDirectory;
            if (!Path.IsPathRooted(bag.Values[LastDataDirectoryKey]))
            {
                bag.Values[LastDataDirectoryKey] =
                    Path.GetFullPath(bag.Values[LastDataDirectoryKey]);
            }
        }

        public static string GetLastDataDirectory(PropertyBag globalSettings)
        {
            PropertyBag bag = GetDirectorySettings(globalSettings);

            return bag.Values[LastDataDirectoryKey];
        }

        public static string GetLastWorkbookDirectory(PropertyBag globalSettings)
        {
            PropertyBag bag = GetDirectorySettings(globalSettings);

            return bag.Values[LastWorkbookDirectoryKey];
        }

        public static string GetDefaultWorkbookDirectory(PropertyBag globalSettings)
        {
            PropertyBag bag = GetDirectorySettings(globalSettings);

            return bag.Values[DefaultWorkbookDirectoryKey];
        }

        private static PropertyBag GetDirectorySettings(PropertyBag globalSettings)
        {
            return Utils.GetOrCreateSubGroup(globalSettings, 
                new[] { DirectorySettingsKey }); 
        }

        public static void SetLastDataDirectory(PropertyBag globalSettings, string newPath)
        {
            PropertyBag bag = GetDirectorySettings(globalSettings);

            bag.Values[LastDataDirectoryKey] = newPath;
        }

        public static void SetLastWorkbookDirectory(PropertyBag globalSettings, string newPath)
        {
            PropertyBag bag = GetDirectorySettings(globalSettings);

            bag.Values[LastWorkbookDirectoryKey] = newPath;
        }
    }
}
