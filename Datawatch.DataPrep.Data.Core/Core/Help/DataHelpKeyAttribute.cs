﻿using System;

namespace Datawatch.DataPrep.Data.Core.Help
{
    /// <summary>
    /// An attribute for assigning a help topic key to Data repo items.
    /// </summary>
    /// <seealso cref="System.Attribute" />
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class DataHelpKeyAttribute : Attribute
    {
        public DataHelpKeyAttribute(string key)
        {
            this.Key = key;
        }

        /// <summary>
        /// Gets the string with Help topic key.
        /// </summary>
        /// <value>
        /// The Help topic key string value.
        /// </value>
        public string Key { get; private set; }
    }
}