﻿using System;
using System.Windows;

namespace Datawatch.DataPrep.Data.Core.Help
{
    public class DataHelpProvider
    {
        public static readonly DependencyProperty HelpKeyProperty =
            DependencyProperty.Register("HelpKey", typeof(string), typeof(DataHelpProvider));

        public static void SetHelpKey(DependencyObject obj, string value)
        {
            obj.SetValue(HelpKeyProperty, value);
        }

        public static string GetHelpKey(DependencyObject obj)
        {
            if (obj != null)
            {
                object element = obj.GetValue(HelpKeyProperty);
                if (element != null)
                    return element.ToString();
            }

            return null;
        }

        public static DataHelpKeyAttribute GetHelpAttribute(object source)
        {
            if (source == null)
            {
                return null;
            }
            return Attribute.GetCustomAttribute(source.GetType(),
                typeof(DataHelpKeyAttribute)) as DataHelpKeyAttribute;
        }
    }
}
