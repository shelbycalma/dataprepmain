﻿using System;
using System.Reflection;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Plugin
{
    /// <summary>
    /// Provides utility methods for working with exceptions.
    /// </summary>
    public static class ExceptionUtils
    {
        /// <summary>
        /// Gets a message created by concatenation of exception message and 
        /// its inner exceptions messages.
        /// </summary>
        /// <param name="ex">The exception to created message from.</param>
        /// <returns>A string with the created message.</returns>
        public static string GetCascadedMessage(Exception ex)
        {
            if (ex.InnerException == null)
                return ex.Message;

            var sb = new StringBuilder();
            sb.Append(ex.Message);
            Exception inner = ex.InnerException;
            while (inner != null)
            {
                string msg = 
                    Environment.NewLine + " " + inner.Message ?? string.Empty;
                if (string.IsNullOrWhiteSpace(msg) == false)
                    sb.Append(msg);
                inner = inner.InnerException;
            }

            return sb.ToString();
        }

        /// <summary>
        /// Gets the message for <see cref="ReflectionTypeLoadException"/> 
        /// including the loader exceptions messages.
        /// </summary>
        /// <param name="ex">The exception instance to create message from.</param>
        /// <returns>A string with the created message.</returns>
        public static string GetReflectionTypeLoadExceptionFullMessage(ReflectionTypeLoadException ex)
        {
            StringBuilder message = new StringBuilder();
            message.AppendFormat(
                "{0} error{1}:\n",
                ex.LoaderExceptions.Length,
                ex.LoaderExceptions.Length > 1 ? "s" : "");
            for (int i = 0; i < ex.LoaderExceptions.Length; i++)
            {
                message.AppendFormat(
                    "[{0}] {1}\n",
                    i + 1,
                    ex.LoaderExceptions[i].Message);
            }

            return message.ToString().TrimEnd();
        }
    }
}