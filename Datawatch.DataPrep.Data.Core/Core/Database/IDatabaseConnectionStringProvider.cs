﻿namespace Datawatch.DataPrep.Data.Core.Database
{
    public interface IDatabaseConnectionStringProvider
    {
        string DatabaseConnectionString { get; }
    }
}
