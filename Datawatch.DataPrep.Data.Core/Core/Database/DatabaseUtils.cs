﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Database
{
    public class DatabaseUtils
    {
        private static readonly Dictionary<string,string> EmptyDictionary = 
            new Dictionary<string, string>();

        /// <summary>
        /// Creates a StandaloneTable with the "same" schema as the DataTable
        /// passed in. ADO types are mapped as good as possible to SDK types.
        /// </summary>
        public static StandaloneTable CreateStandaloneTable(DataTable schema, 
            QuerySettings<DatabaseColumn> querySettings)
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            if (querySettings != null && querySettings.IsSchemaRequest)
            {
                querySettings.SchemaColumnsSettings.ClearSchemaColumns();
            }

            try
            {
                for (int i = 0; i < schema.Columns.Count; i++)
                {
                    string name = schema.Columns[i].ColumnName;
                    Type type = schema.Columns[i].DataType;
                    // All work is done in this factory method.
                    Column column = CreateStandaloneColumn(name, type);
                    if (querySettings != null && querySettings.IsSchemaRequest)
                    {
                        DatabaseColumn dbColumn = CreateDBColumn(column);
                        querySettings.SchemaColumnsSettings.SchemaColumns.Add(dbColumn);
                    }
                    table.AddColumn(column);
                }
            }
            finally
            {
                table.EndUpdate();
            }
            if (querySettings != null && querySettings.IsSchemaRequest)
            {
                querySettings.SchemaColumnsSettings.SerializeSchemaColumns();
            }
            return table;
        }

        /// <summary>
        /// Creates a table based on the saved schema
        /// </summary>
        /// <param name="querySettings"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static StandaloneTable CreateTableFromSavedSchema(QuerySettings<DatabaseColumn> querySettings)
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();
            try
            {
                foreach (DatabaseColumn dbColumn in querySettings.SchemaColumnsSettings.SchemaColumns)
                {
                    Column column = CreateColumn(dbColumn.ColumnName, dbColumn.ColumnType);
                    table.AddColumn(column);
                }
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        /// <summary>
        /// Creates a StandaloneTable with the "same" schema as the result
        /// in the reader. It will have one column for each field in the
        /// reader, mapped as good as possible from ADO to SDK types. Some
        /// ADO column values will still need conversion before they can be
        /// written to the StandaloneTable (e.g. TimeSpan types).
        /// </summary>
        public static StandaloneTable CreateStandaloneTable(
            IDataReader reader, QuerySettings<DatabaseColumn> querySettings)
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            if (querySettings != null && querySettings.IsSchemaRequest)
            {
                querySettings.SchemaColumnsSettings.ClearSchemaColumns();
            }

            try
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string name = reader.GetName(i);
                    Type type = reader.GetFieldType(i);
                    // All work is done in this factory method.
                    Column column = CreateStandaloneColumn(name, type);
                    if (querySettings != null && querySettings.IsSchemaRequest)
                    {
                        DatabaseColumn dbColumn = CreateDBColumn(column);
                        querySettings.SchemaColumnsSettings.SchemaColumns.Add(dbColumn);
                    }
                    table.AddColumn(column);
                }
            }
            finally
            {
                table.EndUpdate();
            }
            if (querySettings != null && querySettings.IsSchemaRequest)
            {
                querySettings.SchemaColumnsSettings.SerializeSchemaColumns();
            }
            return table;
        }

        private static DatabaseColumn CreateDBColumn(Column column)
        {
            DatabaseColumn dbColumn = new DatabaseColumn();
            dbColumn.ColumnName = column.Name;
            if (column is TextColumn)
            {
                dbColumn.ColumnType = ColumnType.Text;
            }
            else if (column is NumericColumn)
            {
                dbColumn.ColumnType = ColumnType.Numeric;
            }
            else if (column is TimeColumn)
            {
                dbColumn.ColumnType = ColumnType.Time;
            }
            return dbColumn;
        }

        public static int[] CreateColumnMap(
            DbDataReader source, StandaloneTable target)
        {
            return CreateColumnMap(source, target, EmptyDictionary);
        }

        /// <summary>
        /// Creates an index map from ordinal positions in the reader to
        /// ordinal positions in the table's column list. This is used to
        /// map values from the reader to the table, something like this:
        /// row[table.Columns[map[i]] = reader[i].
        /// </summary>
        public static int[] CreateColumnMap(
            DbDataReader source, StandaloneTable target, 
            Dictionary<string,string> columnRenames)
        {
            int[] map = new int[source.FieldCount];

            for (int i = 0; i < source.FieldCount; i++)
            {
                string name = source.GetName(i);
                if (columnRenames.ContainsKey(name))
                {
                    name = columnRenames[name];
                }
                // This could be optimized, but hardly worth it.
                int j = IndexOfColumn(target, name);
                map[i] = j;
            }

            return map;
        }
                
        public static Column CreateStandaloneColumn(string name, Type type)
        {
            ColumnType colType = GetColumnType(type, false);
            return CreateColumn(name, colType);
        }

        private static Column CreateColumn(string name, ColumnType columnType)
        {
            if (columnType == ColumnType.Numeric)
            {
                return new NumericColumn(name);
            }
            if (columnType == ColumnType.Time)
            {
                return new TimeColumn(name);
            }
            return new TextColumn(name);
        }

        /// <summary>
        /// Converts any cell values that are DBNull to null.
        /// </summary>
        public static void ConvertNulls(object[] values)
        {
            for (int i = 0; i < values.Length; i++)
            {
                if (values[i] == DBNull.Value)
                {
                    values[i] = null;
                }
            }
        }

        public static ParameterValueCollection EscapeParameters(
            IEnumerable<ParameterValue> sourceParameters)
        {
            ParameterValueCollection escapedParameters =
                new ParameterValueCollection();
            foreach (ParameterValue parameterValue in sourceParameters)
            {
                if (parameterValue.TypedValue == null)
                {
                    continue;
                }
                string paramName = parameterValue.Name;
                TypedParameterValue typedValue = parameterValue.TypedValue;

                if (typedValue is StringParameterValue)
                {
                    escapedParameters.Add(paramName, EscapeParameterValue(
                            parameterValue.TypedValue));
                }
                else if (typedValue is ArrayParameterValue)
                {
                    ArrayParameterValue newArray = new ArrayParameterValue();

                    TypedParameterValue[] paramArray =
                        ((ArrayParameterValue)typedValue).Values.ToArray();

                    for (int i = 0; i < paramArray.Length; i++)
                    {
                        if ((paramArray[i] is StringParameterValue))
                        {
                            newArray.Add(EscapeParameterValue(paramArray[i]));
                        }
                        else if(( paramArray[i] is NumberParameterValue ))
                        {
                            newArray.Add(paramArray[i]);
                        }
                        else
                        {
                            //just copy the the parametervalue
                            newArray.Add(parameterValue.TypedValue);
                        }

                    }
                    escapedParameters.Add(paramName, newArray);
                }
                else
                {
                    //just copy the the parametervalue
                    escapedParameters.Add(paramName, parameterValue.TypedValue);
                }
            }

            return escapedParameters;
        }

        public static StringParameterValue EscapeParameterValue(
            TypedParameterValue typedValue)
        {
            StringParameterValue stringValue = typedValue as StringParameterValue;
            if (stringValue == null || string.IsNullOrEmpty(stringValue.Value))
            {
                return null;
            }
            string paramValue = stringValue.Value;
            if (paramValue.Contains("'"))
            {
                paramValue = paramValue.Replace("'", "''");
            }
            paramValue = "'" + paramValue + "'";

            return new StringParameterValue(paramValue);
        }
                
        /// <summary>
        /// Returns the best SDK column type "fit" for an ADO column type.
        /// </summary>
        public static ColumnType GetColumnType(Type type, bool useTextAsFallback)
        {
            // This code is copied from SDK's AdoTable AdoColumnFactory.
            // TODO: Could possibly get nasty if this code gets out of sync
            // with the one in AdoTable? Especially, if AdoTable is modified
            // to handle new data types, or to handle some type conversion on
            // the fly (as currently with TimeSpans), should change here too.
            if (type == typeof(float) ||
                type == typeof(double) ||
                type == typeof(decimal) ||
                type == typeof(byte) || type == typeof(sbyte) ||
                type == typeof(short) || type == typeof(ushort) ||
                type == typeof(int) || type == typeof(uint) ||
                type == typeof(long) || type == typeof(ulong))
            {
                return ColumnType.Numeric;
            }
            if (type == typeof(string) ||
                type == typeof(char) ||
                type == typeof(Boolean) ||
                type == typeof(Guid) ||
                type == typeof(byte[]))
            {
                return ColumnType.Text;
            }
            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan))
            {
                return ColumnType.Time;
            }

			if (useTextAsFallback)
			{
				return ColumnType.Text;
			}
			else
				return ColumnType.Other;

            //throw Exceptions.BadColumnType(type);
        }

		//public static NumericInterval 
  //          GetNumericDomain(DbConnection connection, string query,
  //          string column, SqlDialect sqlDialect, int requestId)
  //      {
  //          NumericInterval domainValues;

  //          try
  //          {
  //              // Log the query and time the execution.
  //              Log.Info(Properties.Resources.LogExecutingDomainQuery,
  //                  requestId, query);
  //              DateTime start = DateTime.Now;

  //              DataTable table = new DataTable();

  //              using (DbCommand command = connection.CreateCommand())
  //              {
  //                  command.CommandText = CheckQuery(query);
  //                  using (DbDataReader reader = command.ExecuteReader())
  //                  {
  //                      table.Load(reader);
  //                  }
  //              }

  //              // Done, log out how long it took and how much we got.
  //              DateTime end = DateTime.Now;
  //              double seconds = (end - start).TotalSeconds;
  //              Log.Info(Properties.Resources.LogQueryResultSizeAndTime, requestId,
  //                  table.Rows.Count, table.Columns.Count, seconds);

  //              DataRow row = table.Rows[0];
  //              double min = Convert.ToDouble(row["mn"]);
  //              double max = Convert.ToDouble(row["mx"]);

  //              domainValues = new NumericInterval(min, max);
  //          }
  //          catch (Exception ex)
  //          {
  //              Log.Error(Properties.Resources.LogDomainQueryFailed, requestId,
  //                  column, ex);
  //              // We catch the error, and let the method finish. Because
  //              // the array is set to null, and the way the cache is tested
  //              // above, the query will still get executed every time.
  //              domainValues = null;
  //          }

  //          return domainValues;
  //      }

        public static string GetQueryForNumericDomains(QueryBuilder queryBuilder,
            string column, SqlDialect sqlDialect)
        {
            QueryBuilder domainQuery = new QueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }

            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Min, "mn");
            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Max, "mx");

            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;

            string query = domainQuery.ToString(sqlDialect);

            return query;
        }

        public static string GetQueryForTextDomains(QueryBuilder queryBuilder,
            string column, SqlDialect sqlDialect, int rowCount)
        {
            // Not in cache, need to go get it.
            QueryBuilder domainQuery = new QueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }

            domainQuery.Limit = rowCount;

            domainQuery.Select.AddColumn(column);
            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;
            domainQuery.Select.IsDistinct = true;

            string query = domainQuery.ToString(sqlDialect);
            return query;
        }

        public static string GetQueryForTimeBucketDomains(QueryBuilder queryBuilder,
            string column, string timeColumn,
            TimePart timePart, SqlDialect sqlDialect, int rowCount)
        {
            // Not in cache, need to go get it.
            QueryBuilder domainQuery = new QueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }

            domainQuery.Limit = rowCount;

            domainQuery.Select.AddTimePart(timeColumn, timePart, column);
            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;
            domainQuery.Select.IsDistinct = true;

            string query = domainQuery.ToString(sqlDialect);
            return query;
        }

        public static string GetQueryForTimeDomains(QueryBuilder queryBuilder,
            string column, SqlDialect sqlDialect)
        {
            // Not in cache, need to go get it.
            QueryBuilder domainQuery = new QueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }

            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Min, "mn");
            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Max, "mx");

            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;

            string query = domainQuery.ToString(sqlDialect);
            return query;
        }

     //   public static string[] GetTextDomainValues(DbConnection connection,
     //       string query, string column, int rowcount, int requestId)
     //   {
     //       string[] domainValues = null;

     //       try
     //       {
     //           // Log the query and time the execution.
     //           Log.Info(Properties.Resources.LogExecutingDomainQuery, requestId, query);
     //           DateTime start = DateTime.Now;

     //           ISet<string> domain = new HashSet<string>();

     //           using (DbCommand command = connection.CreateCommand())
     //           {
					//command.CommandText = CheckQuery(query);

					//using (DbDataReader reader =
     //                   command.ExecuteReader(CommandBehavior.CloseConnection |
     //                   CommandBehavior.SingleResult))
     //               {
     //                   while ((rowcount == -1 || domain.Count < rowcount) &&
     //                          reader.Read())
     //                   {
     //                       if (reader.IsDBNull(0))
     //                       {
     //                           domain.Add(null);
     //                           continue;
     //                       }
     //                       domain.Add(reader.GetValue(0).ToString());
     //                   }
     //               }
     //           }

     //           // Done, log out how long it took and how much we got.
     //           DateTime end = DateTime.Now;
     //           double seconds = (end - start).TotalSeconds;
     //           Log.Info(Properties.Resources.LogQueryResultSizeAndTime, requestId,
     //               domain.Count, 1, seconds);

     //           domainValues = domain.ToArray();
     //       }
     //       catch (Exception ex)
     //       {
     //           Log.Error(Properties.Resources.LogDomainQueryFailed, requestId,
     //               column, ex);
     //           // We catch the error, and let the method finish. Because
     //           // the array is set to null, and the way the cache is tested
     //           // above, the query will still get executed every time.
     //           domainValues = null;
     //       }
     //       return domainValues;
     //   }

     //   public static string[] GetTimeBucketDomainValues(DbConnection connection,
     //       string query, string column, string timeColumn,
     //           TimePart timePart, SqlDialect sqlDialect,
     //               int requestId, int rowcount)
     //   {
     //       string[] domainValues = null;

     //       try
     //       {
     //           ISet<string> domain = new HashSet<string>();

     //           using (DbCommand command = connection.CreateCommand())
     //           {
					//command.CommandText = CheckQuery(query);

					//using (DbDataReader reader =
     //                   command.ExecuteReader(CommandBehavior.CloseConnection |
     //                   CommandBehavior.SingleResult))
     //               {
     //                   while ((rowcount == -1 || domain.Count < rowcount) &&
     //                          reader.Read())
     //                   {
     //                       if (reader.IsDBNull(0))
     //                       {
     //                           domain.Add(null);
     //                           continue;
     //                       }
     //                       // Datepart query for many dialects including SQL Server
     //                       // is returning padded value, trimming it here is the easiest solution.
     //                       domain.Add(reader.GetString(0).Trim());
     //                   }
     //               }
     //           }
                
     //           domainValues = domain.ToArray();
     //       }
     //       catch (Exception ex)
     //       {
     //           Log.Error(Resources.LogDomainQueryFailed, requestId, column, ex);
     //           // We catch the error, and let the method finish. Because
     //           // the array is set to null, and the way the cache is tested
     //           // above, the query will still get executed every time.
     //           domainValues = null;
     //       }

     //       return domainValues;
     //   }

        /// <summary>
        /// Helper method used by CreateColumnMap. Bizarrely, LINQ does not
        /// include an IndexOf method for enumerables.
        /// </summary>
        public static int IndexOfColumn(StandaloneTable table, string name)
        {
            for (int i = 0; i < table.ColumnCount; i++)
            {
                if (name.Equals(table.GetColumn(i).Name))
                {
                    return i;
                }
            }
            // Since we create the StandaloneTable ourselves and make sure
            // it includes all DbDataReader fields with the call to
            // UpdateStandaloneTable this should never happen.
            return -1;
        }

        public static void SetOnDemandTimeBucketMetaData(StandaloneTable table,
            OnDemandParameters onDemandParameters,
            Dictionary<string, string> columnRenames)
        {
            if (onDemandParameters == null ||
                onDemandParameters.Bucketings == null ||
                onDemandParameters.Bucketings.Length == 0)
            {
                return;
            }

            table.BeginUpdate();
            try
            {
                foreach (Bucketing bucketing in onDemandParameters.Bucketings)
                {
                    if (!(bucketing is TimeBucketing)) continue;
                    TimeBucketing timeBucketing = (TimeBucketing)bucketing;
                    string name = timeBucketing.Name;
                    if (columnRenames != null && columnRenames.ContainsKey(name))
                    {
                        name = columnRenames[name];
                    }
                    IColumn column = table.GetColumn(name);
                    IColumn source = table.GetColumn(timeBucketing.SourceColumnName);
                    if (!(column is ITextColumn && source is ITimeColumn)) continue;
                    TextColumn textColumn = (TextColumn)column;
                    ITimeColumn timeColumn = (ITimeColumn)source;
                    textColumn.MetaData =
                        new OnDemandTimeBucketTextColumnMetaData(
                            textColumn, timeColumn, timeBucketing.TimePart,
                            timeBucketing.Title);
                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        public static void UpdateStandaloneTable(
            StandaloneTable table, IDataReader reader)
        {
            UpdateStandaloneTable(table, reader, EmptyDictionary);
        }


        /// <summary>
        /// Adds columns to the StandaloneTable for every field in the reader
        /// that is not already represented. Columns in the table that are
        /// not in the reader's fields are untouched.
        /// </summary>
        public static void UpdateStandaloneTable(
            StandaloneTable table, IDataReader reader, 
            Dictionary<string, string> columnRenames)
        {
            table.BeginUpdate();

            try
            {
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string name = reader.GetName(i);
                    if (columnRenames.ContainsKey(name))
                    {
                        name = columnRenames[name];
                    }
                    if (!table.ContainsColumn(name))
                    {
                        Type type = reader.GetFieldType(i);
                        // Good old factory method.
                        table.AddColumn(CreateStandaloneColumn(name, type));
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        public static Dictionary<string, string> RenameTimeBucketingAliases(
            OnDemandParameters onDemandParameters)
        {
            Dictionary<string, string> columnMap =
                new Dictionary<string, string>();

            if (onDemandParameters != null &&
                onDemandParameters.Bucketings != null)
            {
                int index = 1;
                foreach (Bucketing bucketing in
                    onDemandParameters.Bucketings)
                {
                    if (!(bucketing is TimeBucketing)) continue;
                    TimeBucketing timeBucketing = (TimeBucketing)bucketing;
                    string newName = "t" + index++;
                    columnMap.Add(newName, timeBucketing.Name);
                    timeBucketing.Name = newName;
                }
            }
            return columnMap;
        }

        public static ParameterValueCollection ConvertDateTimeParametersToUTC(
            ParameterValueCollection parameters, TimeZoneHelper timeZoneHelper)
        {
            if (parameters == null) return null;

            ParameterValueCollection newParameters = new ParameterValueCollection();
            foreach (ParameterValue parameter in parameters)
            {
                // Skip non-date time and special parameters, these will be elsewhere.
                if (!(parameter.TypedValue is DateTimeParameterValue) ||
                    DataUtils.SpecialParameters.Contains(parameter.Name))
                {
                    newParameters.Add(parameter);
                    continue;
                }
                DateTimeParameterValue dateTimeValue = 
                    (DateTimeParameterValue) parameter.TypedValue;
                newParameters.Add(parameter.Name, new DateTimeParameterValue(
                    timeZoneHelper.ConvertToUTC(dateTimeValue.Value)));
            }
            return newParameters;
        }
    }
}
