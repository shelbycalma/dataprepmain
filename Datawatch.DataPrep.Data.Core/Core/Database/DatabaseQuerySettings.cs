﻿using System;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Database
{
    public class DatabaseQuerySettings : QuerySettings<DatabaseColumn>
    {
        private const int DefaultQueryTimeout = 60;
        private readonly IDatabaseConnectionStringProvider connectionStringProvider;

        public DatabaseQuerySettings(PropertyBag bag,
            IDatabaseConnectionStringProvider connectionStringProvider)
            : base(bag)
        {
            if (connectionStringProvider == null)
            {
                throw new ArgumentNullException("connectionStringProvider");
            }
            
            this.connectionStringProvider = connectionStringProvider;
        }

        public string ConnectionString
        {
            get
            {
                return connectionStringProvider.DatabaseConnectionString;
            }
        }

        public virtual bool EncloseParameterInQuote
        {
            get
            {
                string value = GetInternal("EncloseParameterInQuote",
                  true.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("EncloseParameterInQuote", value.ToString()); }
        }

        public virtual bool InMemoryFiltering
        {
            get
            {
                string value = GetInternal("InMemoryFiltering",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("InMemoryFiltering", value.ToString());
            }
        }

        public int QueryTimeout
        {
            get
            {
                return this.GetInternalInt("QueryTimeout", DefaultQueryTimeout);
            }
            set
            {
                if (value < 0)
                    return;

                this.SetInternalInt("QueryTimeout", value);
            }
        }

        public string SelectedDateColumn
        {
            get { return GetInternal("SelectedDateColumn"); }
            set
            {
                SetInternal("SelectedDateColumn", value);
            }
        }

        public virtual SchemaAndTable GetSchemaAndTable()
        {
            if (Table == null) return null;

            return SchemaAndTable.Parse(Table);
        }

        public string SelectedTimeColumn
        {
            get { return GetInternal("SelectedTimeColumn"); }
            set
            {
                SetInternal("SelectedTimeColumn", value);
            }
        }

        public virtual string Table
        {
            get { return SelectedTable != null ? SelectedTable.ToString() : null; }
            set
            {
                if (value != null)
                {
                    SelectedTable = SchemaAndTable.Parse(value);
                }
                else
                {
                    SelectedTable = null;
                }
            }
        }

        public DateTime TimeSpanBaseTime
        {
            get
            {
                string timeSpanBaseTime = GetInternal("TimeSpanBaseTime");
                if (string.IsNullOrEmpty(timeSpanBaseTime))
                {
                    return DateTime.Today;
                }

                return XmlConvert.ToDateTime(timeSpanBaseTime,
                          XmlDateTimeSerializationMode.Local);
            }
            set
            {
                SetInternal("TimeSpanBaseTime", XmlConvert.ToString(value,
                   XmlDateTimeSerializationMode.Local));
            }
        }
    }
}
