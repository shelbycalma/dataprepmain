﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Database
{
    public class DatabaseConnectorQuerySettings : DatabaseQuerySettings
    {
        private SchemaAndTable selectedTable;

        public DatabaseConnectorQuerySettings(PropertyBag bag,
            IDatabaseConnectionStringProvider connectionStringProvider,
            SchemaAndTable selectedTable)
            : base(bag, connectionStringProvider)
        {
            this.selectedTable = selectedTable;
            defaultIsOnDemand = false;
        }
        
        public override SchemaAndTable SelectedTable
        {
            get { return selectedTable; }
            set
            {
                selectedTable = value;

                SelectedColumns.Clear();
                if (PredicateColumns != null)
                {
                    PredicateColumns.Clear();
                }

                OnPropertyChanged("SelectedTable");
            }
        }

    }
}
