﻿namespace Datawatch.DataPrep.Data.Core
{
    public class CalcUtils
    {
        public static bool ContainsTimeWindowFunction(string formula)
        {
            string upperFormula = formula.ToUpper();
            return
                upperFormula.Contains("TIMEWIN") ||
                    upperFormula.Contains("LOOKUP") ||
                        upperFormula.Contains("NPREV");
        }
    }
}
