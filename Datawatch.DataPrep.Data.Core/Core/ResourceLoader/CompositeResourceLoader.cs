﻿using System.Resources;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core.ResourceLoader
{ 
    public class CompositeResourceLoader : IResourceLoader
    {
        private readonly IResourceLoader datawatchResourceLoader;
            
        public CompositeResourceLoader(ResourceManager defaultResourceManager, string defaultNamespace)
        {
            datawatchResourceLoader = new DatawatchResourceLoader(defaultResourceManager, defaultNamespace);
        }

        public static IResourceLoader BrandedResourceLoader { get; set; }

        public string GetResourceString(string key)
        {
            if (BrandedResourceLoader != null)
            {
                string value = BrandedResourceLoader.GetResourceString(key);
                if (value != null) return value;
            }

            return datawatchResourceLoader.GetResourceString(key);
        }

        public ImageSource GetImage(string path)
        {
            if (BrandedResourceLoader != null)
            {
                ImageSource value = BrandedResourceLoader.GetImage(path);
                if (value != null) return value;
            }

            return datawatchResourceLoader.GetImage(path);
        }
    }
}
