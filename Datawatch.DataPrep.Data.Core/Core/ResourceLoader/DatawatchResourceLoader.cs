﻿using System;
using System.Resources;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core.ResourceLoader
{
    internal class DatawatchResourceLoader : IResourceLoader
    {
        private readonly ResourceManager resourceManager;
        private readonly string assemblyName;

        public DatawatchResourceLoader(ResourceManager resourceManager,
            string assemblyName)
        {
            this.resourceManager = resourceManager;
            this.assemblyName = assemblyName;
        }

        public string GetResourceString(string key)
        {
            return resourceManager.GetString(key);
        }

        public ImageSource GetImage(string path)
        {
            string uriString = string.Format("pack://application:,,,/{0};component/{1}", assemblyName, path);
            Uri uri = new Uri(uriString, UriKind.RelativeOrAbsolute);

            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = uri;
            bi.EndInit();

            return bi;
        }
    }
}
