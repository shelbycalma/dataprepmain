@echo off
echo.
echo Updates Data, Developer.NET and Dashboards.NET to use the latest prerelease versions of packages from "develop" branch using Datawatch package source.
echo.

call ph.cmd Update Datawatch -S All -PR .*develop