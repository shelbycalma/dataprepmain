﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;

namespace CRTKM
{
    public class RTK
    {
        SortedList _RTKList = new SortedList();

        private SortedList InitializeRTKInfo()

        {
            SortedList sl = new SortedList();

            //Salesforce
            //"524644434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E0000505A384D553139354A4E36390000"
            //xxx"524644434141535544424141454E5450395448463139313100000000000000000000000000000000323436323655435200005855573247503534304543590000"
            //MySql
            //"444D44434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E000039434B5731415342373059540000"
            //xxx"444D44434141535544424141454E545039544846313931310000000000000000000000000000000032343632365543520000524732304146584A355934300000"
            //Access
            //"444344434141535544424141454E54503954484631393131000000000000000000000000000000003234363236554352000032474E415043344655304A560000"
            //Odata
            //"524444434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E000044474A4E41414A4B34594B310000"
            //xxx"524444434141535544424141454E5450395448463139313100000000000000000000000000000000323436323655435200003948394435475946483737450000"
            //IntAcct
            //"435444434141535544424141454E54503954484631393131000000000000000000000000000000003234363236554352000056434145575259324E345A380000"  
            //Sharepoint
            //"525344434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E000058383958304630473155524B0000"
            //Google AdWords
            //"445A44434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E0000524D505443384D34545A35310000"

            //sl.Add("cdata.odbc.access.dll17.0.0.6556", "tcZuY1fMiycquO1dIpPtljSCG8eZqzM2CGp0bZGad7MYPX6fPLCFnBfo8XXdTF8yyPa6doi9mGs4y+oMaqMtyWFodO9nZjCqzu/Bk6QE6keF2/200sYvJuapq0yPevs9lGVXqEC8aRaC0JkY/GScsb8/f4zi6E50vhPmdLerPSzkIyyuqTtw1LY3iWI8ry1d");
            //sl.Add("cdata.odbc.intacct.dll17.0.0.6556", "x8l4G87M6Xktt+c+7kmaMbwGlPIHR6Ij7xIyH+1SZUXuEiXFs4mNOF/BDnAzWdJ7VDQtiI+yknIo15WECNVO/Mn+mcdQ2TfJ6vrO7U7NPRIvLxJgzN9CQKq4Rlnnv7ApAyezkwvyI9kXzGxhBNEJLoN5K5WvAA0RXyWscet53IXWGNceqrAB3aF2uwhnJNse");
            //sl.Add("cdata.odbc.mysql.dll17.0.0.6542", "Da4ZGw1gfWHyuDyMNqIskJX36S6zeP7adJr0mXFNLZJ0Fj5LvdoFis5ruI6ts9r5JP4JSH/SSS0KIFBGtkfsn9S87/AFxfY4SAihGd+4ZdDzMxs9eG8qEK7RF0UYEuBl/Jqx6ZrDLeJhUdMqJGzcvR3TrDxVOGrOJE7YiwyKSdtm3KgPnE/TdSbr7vz8hSBF");
            //sl.Add("cdata.odbc.odata.dll17.0.0.6556", "9Q9IBqjoNKcBItwoe7hORsD1XgsLG+QToewlWLDt6TQ5xqmdoksmYBM6pwTDEuxL2qNgqbnXctPrsaz5aZ/dwl/68m9yK/s7ESEQoKolE9OkZCpxADP05MFU3bcq03KZlYQCjk82F3WxOrFAdzaiUMZa9czqtwGnGq7IylYSMr6Kcf4gy5bqc8xXTaBkn6an");
            //sl.Add("cdata.odbc.salesforce.dll17.0.0.6451", "XoqlwZf/zByvYbVJ2jCZ0Df3LdInfn8RDBd89Jf91Ot9U90Pp0IqoXu5yNkrc6REJoMB+gcAVX+6sVoHBNpMhALhp1Vv+GfwTxRtrc9UxC+EgAI887gZ8zd7Yx4Hz+PG0UZ0zkFwRCeD763nIbwBSpJQkKtJLrGGEJUUicL81GH/2Vje6IgJtoGUW1DCB+8Z");

            //sl.Add("cdata.odbc.mysql.dll17.0.0.6521", "Da4ZGw1gfWHyuDyMNqIskJX36S6zeP7adJr0mXFNLZJ0Fj5LvdoFis5ruI6ts9r5JP4JSH/SSS0KIFBGtkfsn9S87/AFxfY4SAihGd+4ZdDpoS5OfMm06fH8bUk81zwX7N9c6IpD0JjtShDXs/Ma5fH4rqywoNe/220SEW9T0NWn4+b+mVtb6xKtHzi7eYDo");
            //sl.Add("cdata.odbc.salesforce.dll17.0.0.6556", "XoqlwZf/zByvYbVJ2jCZ0Df3LdInfn8RDBd89Jf91Ot9U90Pp0IqoXu5yNkrc6REJoMB+gcAVX+6sVoHBNpMhALhp1Vv+GfwTxRtrc9UxC86wfFzIncyCThLjSx65qbsMd656grt/QvIv5PqeH27ded1WEnUgWI/WRh9is1Ny/UWLeUgN3oJhX+Zc8rfEUJn");
            //sl.Add("cdata.odbc.sharepoint.dll17.0.0.6556", "Dq1qkceRsnZCp/GCuGkZ6Yz2X1SJtaPTNDtyYlmAgBwaQR6HBx9Hxd2oOWoP8dvQzNWyWk7BN5vHQ2LsApxXM73DeR9FGBbSh5+5dv/pjMmZrf+aHPMdWGwzcUXJKVc3woFJtE83Tq8hcRtZZ787fJ1FmJNotgrA1F+gcKTx98zF9quXWkQhRNUfp23FNFzI");
            //sl.Add("cdata.odbc.googleadwords.dll17.0.0.6556", "Yk2Oun9fAU79qnHcpaSzml9zcNTuHnd/t8IozBgeb7MGQVJR2ikyKHqtuoE9Vt8lWF5KfqeECkjl/puMtYO3RKQ209DMLJ7r7cbiI0dY0U+t3MsslGv5wZ6hIAOoxvY+BF1RpnOBCAXRf1+oKUQ5Esz0hODc9F4ZKwd/agAneO8CkItgMjhXfdhRDac/SFfF");
            //sl.Add("cdata.odbc.eloqua.dll17.0.0.6556", "NLet1R5L64Lvb9b5rNdMnAzRpzq48DI2Lx+Pa2ehaJuokvr2fEp7MUIksj17aocYJ6M5DIYtG89axh2RIT4pxOuewNqPvIjWA8y87UqjCVBJbjQYAJKy7g7BP7MKwFiXyJL6QpDEn3H9jDQqCY9gSjiVRgI8XH+4doPKYVr1GHGAgtDDIt5fTMijcsylEFLz");

            ////test
            //sl.Add("cdata.odbc.intacct.dll17.0.0.6521", "Da4ZGw1gfWHyuDyMNqIskJX36S6zeP7adJr0mXFNLZJ0Fj5LvdoFis5ruI6ts9r5JP4JSH/SSS0KIFBGtkfsn9S87/AFxfY4SAihGd+4ZdDpoS5OfMm06fH8bUk81zwX7N9c6IpD0JjtShDXs/Ma5fH4rqywoNe/220SEW9T0NWn4+b+mVtb6xKtHzi7eYDo");

            //CData version 17.x
            sl.Add("cdata.odbc.access.dll17.0", "tcZuY1fMiycquO1dIpPtljSCG8eZqzM2CGp0bZGad7MYPX6fPLCFnBfo8XXdTF8yyPa6doi9mGs4y+oMaqMtyWFodO9nZjCqzu/Bk6QE6keF2/200sYvJuapq0yPevs9lGVXqEC8aRaC0JkY/GScsb8/f4zi6E50vhPmdLerPSzkIyyuqTtw1LY3iWI8ry1d");
            sl.Add("cdata.odbc.intacct.dll17.0", "x8l4G87M6Xktt+c+7kmaMbwGlPIHR6Ij7xIyH+1SZUXuEiXFs4mNOF/BDnAzWdJ7VDQtiI+yknIo15WECNVO/Mn+mcdQ2TfJ6vrO7U7NPRIvLxJgzN9CQKq4Rlnnv7ApAyezkwvyI9kXzGxhBNEJLoN5K5WvAA0RXyWscet53IXWGNceqrAB3aF2uwhnJNse");
            sl.Add("cdata.odbc.mysql.dll17.0", "Da4ZGw1gfWHyuDyMNqIskJX36S6zeP7adJr0mXFNLZJ0Fj5LvdoFis5ruI6ts9r5JP4JSH/SSS0KIFBGtkfsn9S87/AFxfY4SAihGd+4ZdDzMxs9eG8qEK7RF0UYEuBl/Jqx6ZrDLeJhUdMqJGzcvR3TrDxVOGrOJE7YiwyKSdtm3KgPnE/TdSbr7vz8hSBF");
            sl.Add("cdata.odbc.odata.dll17.0", "9Q9IBqjoNKcBItwoe7hORsD1XgsLG+QToewlWLDt6TQ5xqmdoksmYBM6pwTDEuxL2qNgqbnXctPrsaz5aZ/dwl/68m9yK/s7ESEQoKolE9OkZCpxADP05MFU3bcq03KZlYQCjk82F3WxOrFAdzaiUMZa9czqtwGnGq7IylYSMr6Kcf4gy5bqc8xXTaBkn6an");
            sl.Add("cdata.odbc.salesforce.dll17.0", "XoqlwZf/zByvYbVJ2jCZ0Df3LdInfn8RDBd89Jf91Ot9U90Pp0IqoXu5yNkrc6REJoMB+gcAVX+6sVoHBNpMhALhp1Vv+GfwTxRtrc9UxC+EgAI887gZ8zd7Yx4Hz+PG0UZ0zkFwRCeD763nIbwBSpJQkKtJLrGGEJUUicL81GH/2Vje6IgJtoGUW1DCB+8Z");
            sl.Add("cdata.odbc.sharepoint.dll17.0", "Dq1qkceRsnZCp/GCuGkZ6Yz2X1SJtaPTNDtyYlmAgBwaQR6HBx9Hxd2oOWoP8dvQzNWyWk7BN5vHQ2LsApxXM73DeR9FGBbSh5+5dv/pjMmZrf+aHPMdWGwzcUXJKVc3woFJtE83Tq8hcRtZZ787fJ1FmJNotgrA1F+gcKTx98zF9quXWkQhRNUfp23FNFzI");
            sl.Add("cdata.odbc.googleadwords.dll17.0", "Yk2Oun9fAU79qnHcpaSzml9zcNTuHnd/t8IozBgeb7MGQVJR2ikyKHqtuoE9Vt8lWF5KfqeECkjl/puMtYO3RKQ209DMLJ7r7cbiI0dY0U+t3MsslGv5wZ6hIAOoxvY+BF1RpnOBCAXRf1+oKUQ5Esz0hODc9F4ZKwd/agAneO8CkItgMjhXfdhRDac/SFfF");
            sl.Add("cdata.odbc.eloqua.dll17.0", "NLet1R5L64Lvb9b5rNdMnAzRpzq48DI2Lx+Pa2ehaJuokvr2fEp7MUIksj17aocYJ6M5DIYtG89axh2RIT4pxOuewNqPvIjWA8y87UqjCVBJbjQYAJKy7g7BP7MKwFiXyJL6QpDEn3H9jDQqCY9gSjiVRgI8XH+4doPKYVr1GHGAgtDDIt5fTMijcsylEFLz");
            sl.Add("cdata.odbc.netsuite.dll17.0", "HnpXfuX1nT5LV+9NmYq50XyBdtkOA19HhuU0b4pjzKdIocyZjrpJDYahleMyJY5ZcsGhlT56/o0slUFiXASnm3bfDLxlbocFNM9tBFIi0flLD9FaJGsBMQmW7c3tKA1fGJ5h4kTtJBqmwpc+i5E6j6eCfSoQ+aTvpNNuMZdUwxSgV+KdQ/3rL9O9bIPqkKUj");
            sl.Add("cdata.odbc.hubspot.dll17.0", "zA37b286HN8bD5V0xgXHLEp1vmYG/K6BoprPEM0r8ubXxtambFAuiJUxrQbA8LznrV7+BK46G6+vl6Kf3CzBUOvwFbWr8Mw1dbyA6qLz2OAeJtvwh8TgRNJ8kk4OI+nRqoxwFJCp+Rhr2A3K0PetD4HEjpzzmoeHG9HRqjlfkCKyvz2nuqqdrMomCuDb8tl4");
            sl.Add("cdata.odbc.sugarcrm.dll17.0", "YLHSFtJRP4nYqD+BE63W8L9Pq2rhOhMJlZjbcQf0x61fPkrOLMpriHZEHJcqdxLPssifSFqu8VNiAInjAVbo7nhUD7uE9Sl/Uoltn5ZN42uz1CpQDFW6BsafFm7PVcUunMv3JfxEU93YdOzY+INSrMP6rtTbH/JKBjo70FtBN+3doz8tQispP21IwwNkIrHA");
            sl.Add("cdata.odbc.cassandra.dll17.0", "M9UT8TmQ7xfeegcQF+TJvp4MiDd3usH8PVkVMSDEJdUIuGzIUwjB0Q5yLID4lWFneC0OP+hbUOMerB+i2rKrjW6eF3EGIiLi4qcq0avNnDnwCb/KWpGZfjYH0WKATF0zyL4uIzPHK4EX7UImLUXl/JFDzPUPwEqNVnNy6Rf9mgv9/zhz89mOsBXxANsOuVhy");
            sl.Add("cdata.odbc.googlebigquery.dll17.0", "r3eVTZRLO1wSd5qUcAZdW2MtJKTXSlNmpxpDl/tWtDNGFNEx6ywW/4IQOsivut6rjS+lnXRa61VW/ezw7+Eik/NvB+94b67fOAHeSXiiygqOfyl6OONLy7z7IUOqSAEueIO5rbhCnM9KMCIKdJXtyMIfBhjVcMikj1YGj9oic9NZAw780PDLtyJ7UD9Bd3EN");
            sl.Add("cdata.odbc.marketo.dll17.0", "+mTtObAH2rvotPukqG+mstOWTwW/XkVGqKoHcSvYNk9LQ9eusx4vbSFBYjsPgjWXrUjBjm8vkgur+Y9oZ3WAa4hyjCQYhoK1QMaOn2c5RRshIQJXJYNFVjjJjiBAgUn7NqzhA2tuHL2IZ8zojT7aIq84Z7WICH6hFngFZThFAVZrCrA+HAlj/WaWCgNgZ95j");
            //CData version 18.x
            sl.Add("cdata.odbc.access.dll18.0", "sT1MZQpisbPkKyzUlotFHjqQ4x1RtZPvvD80itJLqurlycOJO9rmG8wqiZjaJ2n8cV1EjkBjPxNVX/eKtpRBdlBXaIr/8vXhWCZVQGHrGuI9JX18Yc2NQoaGienJHX8HbyqWtFqRxXw/E0bELuG5MV83Mh7JrmtEHrhlUKtk1F+kRbpTR1vynwN/6gOSDfdJ");
            sl.Add("cdata.odbc.activedirectory.dll18.0", "dlgTG6Rn0ciX1oWB+EibOCpf7k5yauO1ABGGhK7ZIIVmDOG0mMD68OqXluonwt2zpSPwGYCtIyMal1HCB5QufwDGMp3uvekAtuvsqHNdOQQMEtahHo6jPZrPttsbAS6GIOxYFrcJCX4JJ68uekfKHvM2ZBuR+ATAjZf8ocBFlcPEZ5wvn8+7AERMbKvRjP9s");
            sl.Add("cdata.odbc.box.dll18.0", "rlFFoq8cTqx+vMIc89DEtIcicwM8tLlrm+UW5ZW7s5bKo3f5HM9jrr45iwkgrqejL+SGrAZhVy281WEUiYSGlS7MLFEOvJLCOBR+jszb0ilRou4DzN1hJvILZuFG2o3/5ciXzCFMdZZ7yfbaaEER5gTKBo+QE4oPAoS7/hUGWBxVehMILIspaV2U/ZzeHO0n");
            sl.Add("cdata.odbc.cassandra.dll18.0", "4FfhpLwpZ6dEMzlVq/HbzqFd+SDaUS8LxDTmfm4HtA94wh0vbvd1RiZuyyBhpRpqCJS7EkB9XKxBnh4AU5ONlILgcqYzmI8xgTBImRsHO9YBAw30VGsQc2rD9orMKxyTGRyZ/cDaeIoByyPStCDP5HLFxUOIeDlInRivi9wgc54P6+KSu8TPe52GloPM2yAd");
            sl.Add("cdata.odbc.eloqua.dll18.0", "KjBjmo6P2S0iDlVmNNNaYSaJSMaJhHX7wJxpIhCO1qXTS/o25Cb2yEyGniqyz5CpyN2tLFl9PaBKO6XlauWfJPYP0fSZi/Cz3DI9IANhCj7kB/3bTy6A4kyYY/lxsPV/PXQ4FIKpwBxgLeE7as6B7PY0iQAD/W6rkMlP4Ph9QfUeodnHAyE13jhpFolc8Sx/");
            sl.Add("cdata.odbc.googleadwords.dll18.0", "Z58NxhGH7BNY6jGjcuaL6F7mU5GqKwdKQGi95M6mXAvTD3rEYteEoeA7Fwh4ZfoQu/HGvkd4+LilC53pnu4WDRqkr1fMplBO2SkXeOP50Z+kQVbw+bBZeqcDEXNLK7K6X5wMoHcTxwrJIDm6vKIg+E3dUlBE7A/ywGVjEZ2Z7HO5R3As3dIg+4G3OjdjIvXL");
            sl.Add("cdata.odbc.googlebigquery.dll18.0", "I08bbl3GJXCrhWeK4tJOHMna4I6QRkeklPJ/nq6yl5Zkw5fRaJT9pQD0W38B1x4AQEFoxetdVR3333ifribQMr6oT7azr7KEQZzLlYkxBi/TzJVGAyK1JhpaZsZ7UcK6PfXhe/9sVCBqL+/n8hFPIlAdFfuMYXIvCw3n3odlUJ4N9coYTbYe4yxYrGe4IDP3");
            sl.Add("cdata.odbc.googledrive.dll18.0", "j+FU6RcOKkYcUyzQGNoVotOdhogDL+dpHvwuLn4d2gyqSdGYjiVT6dqrRgi7DM3yovB1+K5pmGPkJApFaT4ukDcUkt9/OZz5llpGjC8iVqSPJO4sqb30eDANzXTM/bWKUfIuQ0tpyhwL+GEWfMcgfgZJkXL+YNsAZkh12EUrY+xFVf8wo4k2KbdxB071K1vW");
            sl.Add("cdata.odbc.googlesheets.dll18.0", "oZHTNkw6rOqqZlBybaSSIspbZUX+nsZ8mEqqi3hhDD1R1YkRCYw//iEbgDIWR3kNwFeQH8hkDN2BkRuRIcAzn1mcYPHLoFguqpASdL9R6SUc8/ASO+HQm7Wa+gG1RWsf1OKHW/xMganbcg5aG7nBgJ5yHCe8MP7P2X5+xNCQ7srC4/oj4LIrHVLDmRse9cNJ");
            sl.Add("cdata.odbc.hubspot.dll18.0", "8JXuseEByMh/7kaZptEV/sLWHkVY/jbfEtL7GqitE0v2pqdv4PRn7kzsEIjjVOohJcd5YGoZ3MumZUgknCiqu7I/EmuoZv9XfkZcl7Mks1lE/3lPW+OAn/cCTOgamdEynZleaR/Gw3YJBjcLaQ5tJz3LV1zv3wKzqHa8NFlkyx+qp6ctaoy7iYo+vLmJA9Rc");
            sl.Add("cdata.odbc.intacct.dll18.0", "3CnSvyPkhIz6W1nL20FIdhd4uYregTZYK/1gG8X/BRDLHk9eLP1PCsw1vSMChJLAd17cTu4FHUpPYcxSXGRocRW3k/Ss1MkD9z4KrC0Np8Gps2Ddw8JD345rY/WA0EE6dNJnuOw/QbwR9OHnGBeZnMIWmQWzjUNUVdIUWKCvOYCQ3upKZwYJdKC2jVCP/k4p");
            sl.Add("cdata.odbc.marketo.dll18.0", "YMKidd18jis6bAAiNo5Go0TZ7IGFxW9gktzQdRWY2GrttZFpBkuIc4XdAisGsX26+T8tBEwfOqgYnXJTtsGO1hbH54JsjxO0qMYzkIAqecwqKZmXluxbARmb1aMnizEVaFqkcyNWOukI5wl9RbvRbQhG2KkdHB0MGQhpGfLCdnfjntns20R9RaIJ26tXjkWI");
            sl.Add("cdata.odbc.mysql.dll18.0", "YLVqdAdBHMS2akiJnGOudzLEq0qg8CS5FXLXGwvyNlwR6ZQzwPleJumTfnAnDyaKFhllsjbMk6wtMnsQmV0nH2c+OBxqXVyGkpAOmP6+4IlFcd8o2y4oeIIK7Oj5QHbHzSBUXG+BZH/OU3qfOcwNC+JovFVWcuzImKFsBP32STUdYS9lzHNZFkcv3+z4BjOr");
            sl.Add("cdata.odbc.netsuite.dll18.0", "CzUr7Mj787vhZI3QpnC2D1nrU3HP26xM8dpJJCycBmShKR4la9iMiCxenxWriiJjB5eeXU/bXqh+XCRzLdyc4QoQihVKrBk3nUHSYY4BTB2T/ef6vsgfb7SVUGmsfXL9uCEh2KOsoIEc+RQ2yzFZpk54KWjbNC/kLDSP/MW/o+sZvT7LWy9dl6d1KelbarB5");
            sl.Add("cdata.odbc.odata.dll18.0", "tT+Dc6wEBCq2YgYP3juGGkfNdv9NJMueMXZ2yOoqoJjqES94aa2DIDsMp4AlkKAsuHWWCcq4k0Vz+TvveTetXSM4KmnxnT1cwGvj0bKm6SELuuhEQl8321xJRHEDQ20aw0pvQQ7yt18m1mnC6BD/GLFsToc2EJkhq2rS2Pe3tW3+d20QIXQiKn8rzNXiDDfR");
            sl.Add("cdata.odbc.oraclesalescloud.dll18.0", "dOiXzpdsMYKPa41NmfeyUIQCJtMM3R5hRiF6Lfc8LuSFpzKmWZNdtqJyrofkCrJ/sixmvuLkTUmitrA3qJg9/LMfkaYsjMXCVK6aSVwE1/OX97DmtyBePETUJ5hy1SMXFeuDSdHPpa8Rft8qGr+d3rpPWtPcmngQXVUZhjNFSW8BFx2LKkwCPlNPdZUQfEmF");
            sl.Add("cdata.odbc.quickbooksonline.dll18.0", "A3JqYLN+DOqUUsJJWY+ZxJOLR3qWK7MzHexgJSKmpgWskvqVeyxY00JiTg5Kk5JOoRQBji1r0JEJqH7Rycd4cZ8rXD3WkVEqum3kYKAV8STVsdVUbupUGlqDjqK8JJrnGZ3kYYxu8Z3ZNEGtVk7KT96EDnk3As9NV0Zdnu7RT33lVmDbotIx/CTDju8vgVb2");
            sl.Add("cdata.odbc.quickbookspos.dll18.0", "JLbACCZ1LEm6KPiLOa3Z4PEbrBKN/SL6ifwTzVeYfoXjblE3lYp2sb/ieD8K37REE07qL5kZTymgdbPiJWGMhoE5tltXbdC1X65iPyEum5sPnS+P8FR+tfOtgi4EqqoynVwRkTxfHvw5IuPcv0Sb3+U6fBjUo7lVihMJ95kCldQjE7TB+ewtCwbo5ch0IJ7G");
            sl.Add("cdata.odbc.salesforce.dll18.0", "+aHDRtt85Uq/IBQkgeZ1GeKvxNtfzsCyXSIhRqCxG6TxWNJPOBVuNbhf7iwxBQfGvwKFC3SxhOJ0D0Lurmy+9wytN1ZmJdi4Bz1SUzrFblMe2hEG9mWhX5ASHF5gviPsoeNVIVO1z0upzoZHGaME803C4P/5YKT21pqxTv1ZC0VGYQOEmfDAEIu5UzAKDeD9");
            sl.Add("cdata.odbc.sharepoint.dll18.0", "xkgMQkeK5eBWkvLA9Nn8Hu4nyy7j/ZWL9hF7rrPHswclL5lQzc5KgJQ9nkYakhoA1Cgw+DL+nRuhArju3dZuEQF+JqU9irXqBqBb9eCwZdJUpSiN5Wt29ajVEKOjgjJCyacwrOmPk+pXA06Odm+A4v9IMtBgLkAYWZyWm9OuMRQoZmY/U+HLSZLeYo953/n2");
            sl.Add("cdata.odbc.sparksql.dll18.0", "EUcJIU2Os8VuSpMgCuWgssTtdxwfdccG5LYdS8Wmxcf9osW53RI4YIOv/GBoJEH74ByRjS7nG3n8sXLJDcvUvZuZHbis2vEYm8w5W+OnKlSsJMjHKA7A8Vpa5wqD8U+iOF316GohCeqPsgG6ZHZZ+qMIWT17hi89nX0RR9f9uwHGVPQThglh9MlDrbPV7+6d");
            sl.Add("cdata.odbc.sugarcrm.dll18.0", "AmFzm18vCosV10MZFQ465PQw9DQVFxgi1Mc8Fh18s6bkNdV3FUEZemB0OIrqBsSsp+D8W2rjQ5ioS7ElcRCKaYGXVdAfAOznb9YDU/tF+T0O+IQ99sQkRkNqgW4ak21N4xARs8miHq5txfJIj+NN/I1mxWN1OVgs67ZgAP1042B0uW9TglX1yqeypNspd34F");
            sl.Add("cdata.odbc.zendesk.dll18.0", "VWZp1gqOLy2lpgSkkbzEBx6RNBLFXG5vGuOJdlJvaOAmb0Qh+oykY8PpAoBs+H4P8f1XhmE9JeBqfsxISA8rMO0u1qer4uRMwT2WqQGxTLQNRMqVrFe8ri96TKARfQtpVbfpreMSxGRKlPs3N25eNgn6czvwzco2Z3GEPYRG8QBzDEV9+VXvzYUSp7FeinX7");
            sl.Add("cdata.odbc.jira.dll18.0", "5ZAypUGNNKo3ExvFK5snztaAzjYlKuKjCHUhFgOMjH0IFdKyD8YyNoMkQo0RbL0WzC887AgUiLTVofJPRA3GVX6mRdvIsg1U8YWYXUP/niMjgmJpMCq3+0S7TFgj3vfWeGkbgytmDmEleL1L1jaCy7sUUnUUEQv2p6o4B0tUe3sgqU+hlk1KkVYybo9PKyCn");

            return sl;
        }

        public RTK()
        {
            _RTKList = InitializeRTKInfo();
        }

        private void Log(Exception ex)
        {
            File.AppendAllText("CaughtExceptions" + DateTime.Now.ToString("yyyy-MM-dd") + ".log", DateTime.Now.ToString("HH:mm:ss") + ": " + ex.Message + "\n" + ex.ToString() + "\n");
        }

        public string GetRTK(string driverName, string DSN, string callerID, bool debugMode)
        {
            const int keySize = 256;
            const int blockSize = 128;
            const int iterations = 1000;
            const int majorVersionIndex = 0;
            const int minorVersionIndex = 1;

            string temp = AES.Decrypt(callerID, "/+OwM6rTxmHyQ43g5qH2qxXjqWd0o5jlPwbnlRXnKW4=", keySize, blockSize, iterations);
            if (temp.Substring(0,5) != "Autho")
            {
                return temp;
            }

            string path = "";
            string result = "";

            try
            {
                string regPath = "SOFTWARE\\ODBC\\ODBC.INI\\" + DSN;

                if (DSN.Length == 0)
                {
                    regPath = "SOFTWARE\\ODBC\\ODBCINST.INI\\" + driverName;
                }
                
                RegistryKey key = Registry.LocalMachine.OpenSubKey(regPath);

                if (key == null && DSN.Length != 0)
                {
                    key = Registry.CurrentUser.OpenSubKey(regPath);
                } 

                if (key != null)
                {
                    Object o = key.GetValue("Driver");
                    if (o != null)
                    {
                        path = o.ToString();   
                    }
                }

                var version = FileVersionInfo.GetVersionInfo(path).FileVersion;

                var productversion = FileVersionInfo.GetVersionInfo(path).ProductVersion;

                string assemblyFileName = Path.GetFileName(path);

                string[] tempVer = version.Split('.');

                //Only check for the major minor version

                string versionMajorMinor = tempVer[majorVersionIndex] + "." + tempVer[minorVersionIndex];


                if (debugMode == true)
                {
                    result = "Driver Name: " + driverName + " -DSN: " + DSN + " -File Version: " + version;
                    result += " -DLL: " + assemblyFileName +  " -Auth: " + temp + " -Product Version: " + productversion + " -RTK: " + _RTKList[assemblyFileName.ToLower() + versionMajorMinor] + " -key: " + assemblyFileName.ToLower() + versionMajorMinor;
                }
                else
                {
                    result = (string) _RTKList[assemblyFileName.ToLower() + versionMajorMinor];
                }
            }

            catch (Exception ex)  
            {
                //log the error
                Log(ex);
            }

            return result;
        }
    }
}
