﻿using System;
using CRTKM;
using System.IO;

namespace testApp
{
    class RTKAgent
    {
        private const int keySize = 256;
        private const int blockSize = 128;
        private const int iterations = 1000; 

        private void Log(Exception ex)
        {
            File.AppendAllText("CaughtExceptions" + DateTime.Now.ToString("yyyy-MM-dd") + ".log", DateTime.Now.ToString("HH:mm:ss") + ": " + ex.Message + "\n" + ex.ToString() + "\n");
        }

        public string getRTK(string driverName, string DSN)
        {
            RTK RTK_Manager = new RTK();

            string RTK = "";
            string result = "";

            String CallerID = AES.Encrypt("Authorized " + DateTime.Now.ToString("h:mm:ss tt"), "/+OwM6rTxmHyQ43g5qH2qxXjqWd0o5jlPwbnlRXnKW4=", keySize, blockSize, iterations);

            string p = "G4GyWIHzzwRXXH84LdVWxQ==";

            //sample calls

            try
            {
                RTK = RTK_Manager.GetRTK(driverName, DSN, CallerID, false);
                result = AES.Decrypt(RTK, "G4GyWIHzzwRXXH84LdVWxQ==", keySize, blockSize, iterations);
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
                Log(ex);
            }

            return result;
        }
    }
}
