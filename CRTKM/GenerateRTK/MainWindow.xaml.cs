﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;
using System.Security.Cryptography;
using CRTKM;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 
      public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
            GetRunTimeLicense();
        }

        //string _connectionString = "Driver={CData ODBC Driver for Salesforce}; User=marvin_briones.dev@datawatch.com;Password=Devops102a;Security Token=vYithuOITFfjZNCpk0xQEtfr";

        //string _connectionString = "Driver={CData ODBC Driver for Cassandra};User=reader;Password=Passw0rd2;Server=54.201.93.230;";

        //string _connectionString = "Driver={Datawatch ODBC Driver for SugarCRM};User=jim;Password=jim;URL=http://sg-datawatch.demo.sugarcrm.com";

        string _connectionString = "DSN=SugarCRM;User=jim;Password=jim";

        //Other sample connection strings:

        //string _connectionString = "Driver={CData ODBC Driver for Salesforce}; User=devops@datawatch.com;Password=DevelopmentOps1;Security Token=LjVeUC7mqLgdDpMDD5pgFb3L";

        //string _connectionString = "Driver={Vertica};Servername=192.168.1.10;UID=dbadmin;PWD=password;Database=VMart";

        //string _connectionString = "Driver={CData ODBC Driver for Postgresql}; User=postgres;Password=dtc3316!";

        //string _connectionString = "Driver={SQL Server}; Server=52.10.62.17; UID=dwch_modeler; PWD=monarch123!; ";

        //string _connectionString = "Server=52.10.62.17; UID=dwch_modeler; PWD=monarch123!; ";

        //string _connectionString = "Driver={CData ODBC Driver for Postgresql}; Database=postgres; User=postgres;Password=dtc3316!";

        public static List<string> GetTables(string connectionString)
        {
            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {
                connection.Open();
                DataTable schema = connection.GetSchema("Tables");
                List<string> TableNames = new List<string>();
                foreach (DataRow row in schema.Rows)
                {
                    TableNames.Add(row[2].ToString());
                }
                connection.Close();
                return TableNames;
            }
        }

        public static string GetTableNames(string connectionString)
        {
            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {
                connection.Open();
                DataTable schema = connection.GetSchema("Tables");
                string TableNames = "";
                foreach (DataRow row in schema.Rows)
                {
                    TableNames = TableNames + (row[2].ToString()) + "\r\n";
                }
                connection.Close();
                return TableNames;
            }
        }

        public static string GetDBNames(string connectionString)
        {
            using (System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(connectionString))
            {
                connection.Open();
                DataTable schema = connection.GetSchema("Databases");
                string DBNames = "";
                foreach (DataRow row in schema.Rows)
                {
                    DBNames = DBNames + (row.Field<String>("database_name")) + "\r\n";
                }
                connection.Close();
                return DBNames;
            }
        }


        private static void ShowColumns(DataTable columnsTable)
        {
            var selectedRows = from info in columnsTable.AsEnumerable()
                               select new
                               {
                                   TableCatalog = info["TABLE_CATALOG"],
                                   TableSchema = info["TABLE_SCHEMA"],
                                   TableName = info["TABLE_NAME"],
                                   ColumnName = info["COLUMN_NAME"],
                                   DataType = info["DATA_TYPE"]
                               };

            //Console.WriteLine("{0,-15}{1,-15}{2,-15}{3,-15}{4,-15}", "TableCatalog", "TABLE_SCHEMA",
            //    "TABLE_NAME", "COLUMN_NAME", "DATA_TYPE");
            foreach (var row in selectedRows)
            {
                MessageBox.Show( row.TableCatalog + " - " + row.TableSchema + " - " + row.TableName + " - " + row.ColumnName + " - " + row.DataType);
            }
        }

        public static string GetFieldNames(string tableName, string connectionString)
        {

            // You can specify the Catalog, Schema, Table Name, Column Name to get the specified column(s).
            // You can use four restrictions for Column, so you should create a 4 members array.
            String[] columnRestrictions = new String[4];

            // For the array, 0-member represents Catalog; 1-member represents Schema; 
            // 2-member represents Table Name; 3-member represents Column Name. 
            // Now we specify the Table_Name and Column_Name of the columns what we want to get schema information.
            columnRestrictions[2] = "Product2";
            columnRestrictions[3] = "Name";

            //DataTable departmentIDSchemaTable = conn.GetSchema("Columns", columnRestrictions);

            using (OdbcConnection connection = new OdbcConnection(connectionString))
            {
                connection.Open();
                DataTable schema = connection.GetSchema("Columns");
                ShowColumns(schema);


                string TableNames = "";
                foreach (DataRow row in schema.Rows)
                {
                    TableNames = TableNames + (row[2].ToString()) + "\r\n";
                }
                connection.Close();
                return TableNames;
            }

        }



        public void DoODBCProc(string SQLCommand, string connectionString)
        {

            OdbcConnection cn;
            OdbcCommand cmd;

            // Set cursor as hourglass
            Mouse.OverrideCursor = Cursors.Wait;


            cn = new OdbcConnection(connectionString);

            try
            {
                cn.Open();

               cmd = new OdbcCommand(SQLCommand, cn);
              
             
               OdbcDataReader myReader = cmd.ExecuteReader();

               String sTemp = "Operation Completed.";

                if (SQLCommand.StartsWith("Select"))
                    {
                        sTemp = "Operation Result: ";
                    }

				DataTable dt = new DataTable();
				dt.Load(myReader);
				dgResults.ItemsSource = dt.DefaultView;
				myReader.Close();
				myReader = null;
                MessageBox.Show(sTemp);
            }
            catch (Exception e)
            {
                MessageBox.Show("An error occurred: " +  e.ToString());
            }
            finally
            {
                cn.Close();
                // Set cursor as default arrow
                Mouse.OverrideCursor = null;
              
            }
        }

        public void GetRunTimeLicense()
        {
            //Initialize Encryption
            byte[] salt1 = new byte[] { 4, 1, 20, 1, 23, 1, 20, 8 };
            Rfc2898DeriveBytes k1 = new Rfc2898DeriveBytes("DWCHdkDM", salt1, 1000);
            string x = Convert.ToBase64String(k1.GetBytes(16));
            //x = "2V0DEFmGPxe/A29oYclGIQ==";
            String p = AES.Encrypt("DWCHdkDM", x, 256, 128, 1000);
            //p="G4GyWIHzzwRXXH84LdVWxQ==";
            String p1 = AES.Encrypt("comAuthentication101", x, 256, 128, 1000);
            //p1="/+OwM6rTxmHyQ43g5qH2qxXjqWd0o5jlPwbnlRXnKW4=";

            string AccessRTK = new System.Data.CData.Access.AccessConnection().RuntimeLicense;
            string Access = AES.Encrypt(AccessRTK, p, 256, 128, 1000);
            Debug.Print("AccessRTK - " + AccessRTK);
            Debug.Print("Access - " + Access);

            string ActiveDirectoryRTK = new System.Data.CData.ActiveDirectory.ActiveDirectoryConnection().RuntimeLicense;
            string ActiveDirectory = AES.Encrypt(ActiveDirectoryRTK, p, 256, 128, 1000);
            Debug.Print("ActiveDirectoryRTK - " + ActiveDirectoryRTK);
            Debug.Print("ActiveDirectory - " + ActiveDirectory);

            string BoxRTK = new System.Data.CData.Box.BoxConnection().RuntimeLicense;
            string Box = AES.Encrypt(BoxRTK, p, 256, 128, 1000);
            Debug.Print("BoxRTK - " + BoxRTK);
            Debug.Print("Box - " + Box);

            string CassandraRTK = new System.Data.CData.Cassandra.CassandraConnection().RuntimeLicense;
            string Cassandra = AES.Encrypt(CassandraRTK, p, 256, 128, 1000);
            Debug.Print("CassandraRTK - " + CassandraRTK);
            Debug.Print("Cassandra - " + Cassandra);

            string EloquaRTK = new System.Data.CData.Eloqua.EloquaConnection().RuntimeLicense;
            string Eloqua = AES.Encrypt(EloquaRTK, p, 256, 128, 1000);
            Debug.Print("EloquaRTK - " + EloquaRTK);
            Debug.Print("Eloqua - " + Eloqua);

            string GoogleAdWordsRTK = new System.Data.CData.GoogleAdWords.GoogleAdWordsConnection().RuntimeLicense;
            string GoogleAdWords = AES.Encrypt(GoogleAdWordsRTK, p, 256, 128, 1000);
            Debug.Print("GoogleAdWordsRTK - " + GoogleAdWordsRTK);
            Debug.Print("GoogleAdWords - " + GoogleAdWords);

            string GoogleBigQueryRTK = new System.Data.CData.GoogleBigQuery.GoogleBigQueryConnection().RuntimeLicense;
            string GoogleBigQuery = AES.Encrypt(GoogleBigQueryRTK, p, 256, 128, 1000);
            Debug.Print("GoogleBigQueryRTK - " + GoogleBigQueryRTK);
            Debug.Print("GoogleBigQuery - " + GoogleBigQuery);

            string GoogleDriveRTK = new System.Data.CData.GoogleDrive.GoogleDriveConnection().RuntimeLicense;
            string GoogleDrive = AES.Encrypt(GoogleDriveRTK, p, 256, 128, 1000);
            Debug.Print("GoogleDriveRTK - " + GoogleDriveRTK);
            Debug.Print("GoogleDrive - " + GoogleDrive);

            string GoogleSpreadsheetsRTK = new System.Data.CData.GoogleSheets.GoogleSheetsConnection().RuntimeLicense;
            string GoogleSpreadsheets = AES.Encrypt(GoogleSpreadsheetsRTK, p, 256, 128, 1000);
            Debug.Print("GoogleSpreadsheetsRTK - " + GoogleSpreadsheetsRTK);
            Debug.Print("GoogleSpreadsheets - " + GoogleSpreadsheets);

            string HubSpotRTK = new System.Data.CData.HubSpot.HubSpotConnection().RuntimeLicense;
            string HubSpot = AES.Encrypt(HubSpotRTK, p, 256, 128, 1000);
            Debug.Print("HubSpotRTK - " + HubSpotRTK);
            Debug.Print("HubSpot - " + HubSpot);

            string IntacctRTK = new System.Data.CData.Intacct.IntacctConnection().RuntimeLicense;
            string Intacct = AES.Encrypt(IntacctRTK, p, 256, 128, 1000);
            Debug.Print("IntacctRTK - " + IntacctRTK);
            Debug.Print("Intacct - " + Intacct);

            string MarketoRTK = new System.Data.CData.Marketo.MarketoConnection().RuntimeLicense;
            string Marketo = AES.Encrypt(MarketoRTK, p, 256, 128, 1000);
            Debug.Print("MarketoRTK - " + MarketoRTK);
            Debug.Print("Marketo - " + Marketo);

            string MySQLRTK = new System.Data.CData.MySQL.MySQLConnection().RuntimeLicense;
            string MySQL = AES.Encrypt(MySQLRTK, p, 256, 128, 1000);
            Debug.Print("MySQLRTK - " + MySQLRTK);
            Debug.Print("MySQL - " + MySQL);

            string NetSuiteRTK = new System.Data.CData.NetSuite.NetSuiteConnection().RuntimeLicense;
            string NetSuite = AES.Encrypt(NetSuiteRTK, p, 256, 128, 1000);
            Debug.Print("NetSuiteRTK - " + NetSuiteRTK);
            Debug.Print("NetSuite - " + NetSuite);

            string OdataRTK = new System.Data.CData.OData.ODataConnection().RuntimeLicense;
            string Odata = AES.Encrypt(OdataRTK, p, 256, 128, 1000);
            Debug.Print("OdataRTK - " + OdataRTK);
            Debug.Print("Odata - " + Odata);

            string OracleSalesCloudRTK = new System.Data.CData.OracleSalesCloud.OracleSalesCloudConnection().RuntimeLicense;
            string OracleSalesCloud = AES.Encrypt(OracleSalesCloudRTK, p, 256, 128, 1000);
            Debug.Print("OracleSalesCloudRTK - " + OracleSalesCloudRTK);
            Debug.Print("OracleSalesCloud - " + OracleSalesCloud);

            string QuickBooksOnlineRTK = new System.Data.CData.QuickBooksOnline.QuickBooksOnlineConnection().RuntimeLicense;
            string QuickBooksOnline = AES.Encrypt(QuickBooksOnlineRTK, p, 256, 128, 1000);
            Debug.Print("QuickBooksOnlineRTK - " + QuickBooksOnlineRTK);
            Debug.Print("QuickBooksOnline - " + QuickBooksOnline);

            string QuickBooksPOSRTK = new System.Data.CData.QuickBooksPOS.QuickBooksPOSConnection().RuntimeLicense;
            string QuickBooksPOS = AES.Encrypt(QuickBooksPOSRTK, p, 256, 128, 1000);
            Debug.Print("QuickBooksPOSRTK - " + QuickBooksPOSRTK);
            Debug.Print("QuickBooksPOS - " + QuickBooksPOS);

            string SalesforceRTK = new System.Data.CData.Salesforce.SalesforceConnection().RuntimeLicense;
            string Salesforce = AES.Encrypt(SalesforceRTK, p, 256, 128, 1000);
            Debug.Print("SalesforceRTK - " + SalesforceRTK);
            Debug.Print("Salesforce - " + Salesforce);

            string SharePointRTK = new System.Data.CData.SharePoint.SharePointConnection().RuntimeLicense;
            string SharePoint = AES.Encrypt(SharePointRTK, p, 256, 128, 1000);
            Debug.Print("SharePointRTK - " + SharePointRTK);
            Debug.Print("SharePoint - " + SharePoint);

            string SparkSQLRTK = new System.Data.CData.SparkSQL.SparkSQLConnection().RuntimeLicense;
            string SparkSQL = AES.Encrypt(SparkSQLRTK, p, 256, 128, 1000);
            Debug.Print("SparkSQLRTK - " + SparkSQLRTK);
            Debug.Print("SparkSQL - " + SparkSQL);

            string SugarCRMRTK = new System.Data.CData.SugarCRM.SugarCRMConnection().RuntimeLicense;
            string SugarCRM = AES.Encrypt(SugarCRMRTK, p, 256, 128, 1000);
            Debug.Print("SugarCRMRTK - " + SugarCRMRTK);
            Debug.Print("SugarCRM - " + SugarCRM);

            string ZendeskRTK = new System.Data.CData.Zendesk.ZendeskConnection().RuntimeLicense;
            string Zendesk = AES.Encrypt(ZendeskRTK, p, 256, 128, 1000);
            Debug.Print("ZendeskRTK - " + ZendeskRTK);
            Debug.Print("Zendesk - " + Zendesk);

            string rtk = MarketoRTK;

            //MessageBox.Show(rtk);

            txtGetRTK.Text = rtk;
		}
        
		private void Button_Click(object sender, RoutedEventArgs e)
        {
            DoODBCProc(txtSelect.Text, _connectionString);
        }

        private void btnInsert_Click(object sender, RoutedEventArgs e)
        {
            DoODBCProc(txtInsert.Text, _connectionString);

        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            DoODBCProc(txtUpdate.Text, _connectionString);

        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            DoODBCProc(txtDelete.Text, _connectionString);

        }

        private void btnGetRTK_Click(object sender, RoutedEventArgs e)
        {
            GetRunTimeLicense();

        }
		
        private void btnGetTables_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(GetTableNames(_connectionString),"Tables:");
        }

 
        private void btnUpdateConnectionString_Click(object sender, RoutedEventArgs e)
        {
            _connectionString = txtUpdateConnectionString.Text;
            MessageBox.Show("Connection string updated.");
        }

        private void btnDoProc_Click(object sender, RoutedEventArgs e)
        {
            DoODBCProc(txtDoProc.Text, _connectionString);
        }
    }

}

