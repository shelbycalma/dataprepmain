﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRTKM;
using System.Security.Cryptography;
using System.IO;

namespace testApp
{
    class RTKAgent
    {
        private const int keySize = 256;
        private const int blockSize = 128;
        private const int iterations = 1000;


        private void GetRunTimeLicense()
        {

            //instructions on getting the RTK
            //Make sure to referencde the ODBCm library for the designated driver you want to get the RTK from
            //the assembly is normally located in C:\Program Files\CData\CData ODBC Driver for Salesforce\lib64
            //call the RunTimeLicense function as shown in the sample below
            //as much as possible save the RTK as a comment in this code so it can be tracked

            string SalesforceRTK = new System.Data.CData.Salesforce.SalesforceConnection().RuntimeLicense;

            string rtk = SalesforceRTK;

            //Salesforce
            //"524644434141535544424141454E5450395448463139313100000000000000000000000000000000323436323655435200005855573247503534304543590000"
            //MySql
            //"444D44434141535544424141454E545039544846313931310000000000000000000000000000000032343632365543520000524732304146584A355934300000"
            //Access
            //"444344434141535544424141454E54503954484631393131000000000000000000000000000000003234363236554352000032474E415043344655304A560000"
            //Odata
            //"524444434141535544424141454E5450395448463139313100000000000000000000000000000000323436323655435200003948394435475946483737450000"
            //IntAcct
            //"435444434141535544424141454E54503954484631393131000000000000000000000000000000003234363236554352000056434145575259324E345A380000"            //

        }

        private void Log(Exception ex)
        {
            File.AppendAllText("CaughtExceptions" + DateTime.Now.ToString("yyyy-MM-dd") + ".log", DateTime.Now.ToString("HH:mm:ss") + ": " + ex.Message + "\n" + ex.ToString() + "\n");
        }

        public string getRTK(string driverName, string DSN)
        {
            RTK RTK_Manager = new RTK();

            string RTK = "";
            string result = "";

            String CallerID = AES.Encrypt("Authorized " + DateTime.Now.ToString("h:mm:ss tt"), "/+OwM6rTxmHyQ43g5qH2qxXjqWd0o5jlPwbnlRXnKW4=", keySize, blockSize, iterations);

            string p = "G4GyWIHzzwRXXH84LdVWxQ==";

            //sample calls

            try
            {
                RTK = RTK_Manager.GetRTK(driverName, DSN, CallerID, false);
                result = AES.Decrypt(RTK, "G4GyWIHzzwRXXH84LdVWxQ==", keySize, blockSize, iterations);
            }
            catch (Exception ex)  //just for demonstration...it's always best to handle specific exceptions
            {
                //react appropriately
                Log(ex);
            }

            return result;
        }
    }
}
