﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CRTKM;
using System.Security.Cryptography;


namespace testApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private void InitialEncryption()
        {
            byte[] salt1 = new byte[] { 4, 1, 20, 1, 23, 1, 20, 8 };


            Rfc2898DeriveBytes k1 = new Rfc2898DeriveBytes("DWCHdkDM", salt1, 1000);

            string x = Convert.ToBase64String(k1.GetBytes(16));

            //x = "2V0DEFmGPxe/A29oYclGIQ==";

            String p = AES.Encrypt("DWCHdkDM", x, 256, 128, 1000);

            //p="G4GyWIHzzwRXXH84LdVWxQ==";


            String p1 = AES.Encrypt("comAuthentication101", x, 256, 128, 1000);

            //p1="/+OwM6rTxmHyQ43g5qH2qxXjqWd0o5jlPwbnlRXnKW4=";

            String s = AES.Encrypt("Test RTK for Intacct 12345", p, 256, 128, 1000);
            s = AES.Encrypt("Test RTK for MySQL 123456", p, 256, 128, 1000);
            s = AES.Encrypt("Test RTK for Salesforce 1234567", p, 256, 128, 1000);

            //Salesforce
            String Salesforce = AES.Encrypt("524644434141535544424141454E5450395448463139313100000000000000000000000000000000323436323655435200005855573247503534304543590000", p, 256, 128, 1000);
            //"XoqlwZf/zByvYbVJ2jCZ0Df3LdInfn8RDBd89Jf91Ot9U90Pp0IqoXu5yNkrc6REJoMB+gcAVX+6sVoHBNpMhALhp1Vv+GfwTxRtrc9UxC+EgAI887gZ8zd7Yx4Hz+PG0UZ0zkFwRCeD763nIbwBSpJQkKtJLrGGEJUUicL81GH/2Vje6IgJtoGUW1DCB+8Z"

            //MySql
            String MySQL = AES.Encrypt("444D44434141535544424141454E545039544846313931310000000000000000000000000000000032343632365543520000524732304146584A355934300000", p, 256, 128, 1000);
            //"Da4ZGw1gfWHyuDyMNqIskJX36S6zeP7adJr0mXFNLZJ0Fj5LvdoFis5ruI6ts9r5JP4JSH/SSS0KIFBGtkfsn9S87/AFxfY4SAihGd+4ZdDzMxs9eG8qEK7RF0UYEuBl/Jqx6ZrDLeJhUdMqJGzcvR3TrDxVOGrOJE7YiwyKSdtm3KgPnE/TdSbr7vz8hSBF"

            //Access
            String Access = AES.Encrypt("444344434141535544424141454E54503954484631393131000000000000000000000000000000003234363236554352000032474E415043344655304A560000", p, 256, 128, 1000);
            //"tcZuY1fMiycquO1dIpPtljSCG8eZqzM2CGp0bZGad7MYPX6fPLCFnBfo8XXdTF8yyPa6doi9mGs4y+oMaqMtyWFodO9nZjCqzu/Bk6QE6keF2/200sYvJuapq0yPevs9lGVXqEC8aRaC0JkY/GScsb8/f4zi6E50vhPmdLerPSzkIyyuqTtw1LY3iWI8ry1d"

            //Odata
            String Odata = AES.Encrypt("524444434141535544424141454E5450395448463139313100000000000000000000000000000000323436323655435200003948394435475946483737450000", p, 256, 128, 1000);
            //"9Q9IBqjoNKcBItwoe7hORsD1XgsLG+QToewlWLDt6TQ5xqmdoksmYBM6pwTDEuxL2qNgqbnXctPrsaz5aZ/dwl/68m9yK/s7ESEQoKolE9M6mOUrRVNBlLw8lNNZv8h5EAcOc+VVlP8+TFxMNfpJuEwOdUR33wqNqHbbN+thdd6oqk+8Cn3Q4BbzynMpnH1L"

            //IntAcct
            String Intacct = AES.Encrypt("435444434141535544424141454E54503954484631393131000000000000000000000000000000003234363236554352000056434145575259324E345A380000", p, 256, 128, 1000);
            //"x8l4G87M6Xktt+c+7kmaMbwGlPIHR6Ij7xIyH+1SZUXuEiXFs4mNOF/BDnAzWdJ7VDQtiI+yknIo15WECNVO/Mn+mcdQ2TfJ6vrO7U7NPRIvLxJgzN9CQKq4Rlnnv7ApAyezkwvyI9kXzGxhBNEJLoN5K5WvAA0RXyWscet53IXWGNceqrAB3aF2uwhnJNse"


        }

        public MainWindow()
        {
            InitializeComponent();
            InitialEncryption();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RTKAgent agent = new RTKAgent();

            MessageBox.Show(agent.getRTK(txtDriverName.Text, txtDSN.Text));

        }
    }
}
