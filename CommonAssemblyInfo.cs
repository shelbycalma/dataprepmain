﻿using System.Reflection;
using System.Runtime.InteropServices;

// Assembly attributes that are common to all assemblies in the solution.
// To use these attributes in a new project, add this file as a link to the
// project, then move it into the Properties folder.

// TODO: If the AssemblyCompany is changed, old settings are lost.
[assembly: AssemblyCompany("Datawatch Corporation")]
[assembly: AssemblyCopyright("Copyright © Datawatch Corporation, 2017. All rights reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.0.837")]
#if !PHLOCAL
[assembly: AssemblyVersion("14.1.1.0")]
[assembly: AssemblyInformationalVersion("14.1.1")]
#else
[assembly: AssemblyVersion("99.9.9")]
[assembly: AssemblyInformationalVersion("99.9.9")]
#endif