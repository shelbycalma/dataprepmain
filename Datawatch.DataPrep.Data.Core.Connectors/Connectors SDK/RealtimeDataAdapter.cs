﻿using System;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class RealtimeDataAdapter<T,C,E>
        where T : ParameterTable
        where C : RealtimeConnectionSettings 
    {
        protected TableUpdater<T,C,E> updater;
        protected T table;
        protected C settings;

        public virtual void Initialize(TableUpdater<T, C, E> updater, 
            T table, C settings)
        {
            this.updater = updater;
            this.table = table;
            this.settings = settings;
        }

        public abstract void Start();

        public abstract void Stop();

        public abstract void UpdateColumns(Row row, E evt);

        public abstract DateTime GetTimestamp(E evt);

        public IPluginErrorReportingService ErrorReporter { get; set; }
    }
}
