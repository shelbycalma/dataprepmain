﻿using System.IO;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    // Use this to avoid new line tookens within quotations and easily read out each line
    class ReadWriteStream
    {
        private readonly MemoryStream ms;
        private readonly StreamWriter sw;
        private readonly StreamReader sr;
        private readonly bool gotoStartAfterWrite;

        public ReadWriteStream(bool gotoStartAfterWrite)
        {
            ms = new MemoryStream();
            sw = new StreamWriter(ms);
            sr = new StreamReader(ms);
            this.gotoStartAfterWrite = gotoStartAfterWrite;
        }

        public void WriteMessage(string message)
        {
            sw.Write(message);
            sw.Flush();
            if (gotoStartAfterWrite)
            {
                ms.Seek(0, SeekOrigin.Begin);
            }
        }

        public string ReadLine()
        {
            return sr.ReadLine();
        }

        public StreamReader StreamReader
        {
            get
            {
                return sr;
            }
        }

        // Call to make all written data accessible to the StreamReader
        public void GotoStart()
        {
            ms.Seek(0, SeekOrigin.Begin);
        }

        public void Close()
        {
            if (sw != null)
            {
                sw.Close();
            }
            if (sr != null)
            {
                sr.Close();
            }
            if (ms != null)
            {
                ms.Close();
            }
        }

    }
}
