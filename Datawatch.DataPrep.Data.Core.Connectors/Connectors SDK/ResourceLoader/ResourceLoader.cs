﻿using System.Reflection;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Core.ResourceLoader;
using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core.Connectors.ResourceLoader
{
    internal static class ResourceLoader
    {
        private static readonly IResourceLoader resourceLoader =
            new CompositeResourceLoader(Properties.Resources.ResourceManager,
                Assembly.GetExecutingAssembly().GetName().Name);

        public static string GetResourceString(string key)
        {
            return resourceLoader.GetResourceString(key);
        }

        public static ImageSource GetImage(string path)
        {
            return resourceLoader.GetImage(path);
        }
    }
}
