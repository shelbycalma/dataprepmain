﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors.Properties;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class MessageQueuePluginBase<T, TC, TE, TR> : RealtimeDataPlugin<T, TC, TE, TR>
        where T : ParameterTable
        where TC : MessageQueueSettingsBase
        where TE : Dictionary<string, object>
        where TR : MessageQueueAdapterBase<T, TC, TE>, new()
    {
        // The id of the data plug-in, this string must not be changed after
        // the plug-in has been used in workbooks or the workbooks created 
        // using the plug-in will loose their data connections.
        private readonly string id;

        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        private readonly string title;

        // TODO: should add an overloaded CreateConnectionSettings method
        // with both Parameters and PropertyBag, it is required in places like PluginUIBase.GetConfigElement
        public abstract TC CreateConnectionSettings(IEnumerable<ParameterValue> parameters);
        public abstract TC CreateConnectionSettings(PropertyBag bag);
        
        protected MessageQueuePluginBase(string id, string title)
        {
            this.id = id;
            this.title = title;
        }

        public override TC CreateSettings(PropertyBag bag)
        {
            TC settings = CreateConnectionSettings(bag);
            if (string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = settings.GenerateTitle(Title);
            }
            return settings;
        }

        protected override ITable CreateTable(TC connectionSettings,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;
            ParameterTable table = new ParameterTable(parameters);
            table.BeginUpdate();
            try
            {
                DataPluginUtils.AddColumns(table, 
                    connectionSettings.ParserSettings.Columns);
            }
            finally
            {
                table.EndUpdate();
            }
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);
            return table;
        }

        public override string Id
        {
            get { return id; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override string Title
        {
            get { return title; }
        }

    }

}
