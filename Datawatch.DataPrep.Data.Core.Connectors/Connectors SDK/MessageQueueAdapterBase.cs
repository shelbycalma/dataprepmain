﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class MessageQueueAdapterBase<T, TC, TE> : 
        RealtimeDataAdapter<T, TC, TE>, ICombineEvents<Dictionary<string, object>>, IMessageHandler
        where T : ParameterTable
        where TC : MessageQueueSettingsBase
        where TE : Dictionary<string, object>
    {
        protected IParser parser;
        protected string idColumn;
        protected string timeIdColumn;
        private bool isTimeIdColumnGenerated;
        
        public override void Start()
        {
            idColumn = settings.IdColumn;
            timeIdColumn = settings.TimeIdColumn;
            isTimeIdColumnGenerated = settings.IsTimeIdColumnGenerated;
            // Verify settings
            if (settings.ParserSettings.ColumnCount == 0)
            {
                throw Exceptions.NoColumns();
            }
            parser = settings.ParserSettings.CreateParser();
        }

        public override void Stop()
        {
            parser = null;
            idColumn = null;
        }

        public override void UpdateColumns(Row row, TE fields)
        {
            DataPluginUtils.UpdateColumns(fields, table, row,
                settings.NumericDataHelper, settings.ParserSettings);
        }

        public override DateTime GetTimestamp(TE fields)
        {
            return DataPluginUtils.GetTimeStamp(fields, isTimeIdColumnGenerated, 
                timeIdColumn, 
                settings.ParserSettings.GetColumn(timeIdColumn).DateParser);
        }

        public void MessageReceived(string message)
        {
            // If we receive a message after Stop is called, bail out.
            if (parser == null) return;
            DataPluginUtils.EnqueueMessage(message, parser, idColumn, updater);
        }

        public virtual bool DoCombineEvents
        {
            get { return settings.IsCombineEvents; }
        }

        public Dictionary<string, object> Combine(
            Dictionary<string, object> event1, 
            Dictionary<string, object> event2)
        {
            foreach (string key in event2.Keys)
            {
                event1[key] = event2[key];
            }
            return event1;
        }
    }
}
