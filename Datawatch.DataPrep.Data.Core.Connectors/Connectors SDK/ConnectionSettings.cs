﻿using System;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    /// <summary>
    /// Maintains settings for a data connection.
    /// </summary>
    /// <remarks>
    /// <para>Internally, this class stores the settings in a
    /// <see cref="PropertyBag"/> that can be serialized by Panopticon. This
    /// class provides easy access to this property bag, and also implements
    /// the <see cref="INotifyPropertyChanged"/> interface.</para>
    /// <para>Note to implementors: all access to the property bag itself is
    /// protected, which means that you need to add public accessors for any
    /// settings you define.</para>
    /// </remarks>
    public class ConnectionSettings : INotifyPropertyChanged
    {
        /// <summary>
        /// Fired when one of the connection properties are changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private PropertyBag propertyBag;

        /// <summary>
        /// Creates a new instance with accompanying <see cref="PropertyBag"/>.
        /// </summary>
        public ConnectionSettings() : this(new PropertyBag())
        {
        }

        /// <summary>
        /// Creates a new instance from an existing <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="bag">A property bag that already contains settings
        /// for the connection.</param>
        public ConnectionSettings(PropertyBag bag) 
        {
            if (bag == null) {
                throw Exceptions.ArgumentNull("bag");
            }

            propertyBag = bag;
        }

        /// <summary>
        /// Gets a string setting from the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to retrieve.</param>
        /// <returns>The value or null if not present.</returns>
        protected string GetInternal(string name)
        {
            return propertyBag.Values[name];
        }

        /// <summary>
        /// Gets an string setting from the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to retrieve.</param>
        /// <param name="defaultValue">Value to return if the setting is not
        /// present.</param>
        /// <returns>The value or default.</returns>
        protected string GetInternal(string name, string defaultValue)
        {
            string value = propertyBag.Values[name];
            return string.IsNullOrEmpty(value) ? defaultValue : value;
        }

        /// <summary>
        /// Gets an integer setting from the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to retrieve.</param>
        /// <param name="defaultValue">Value to return if the setting is not
        /// present or cannot be converted to an int.</param>
        /// <returns>The value or default.</returns>
        protected int GetInternalInt(string name, int defaultValue)
        {
            string s = propertyBag.Values[name];
            if (!string.IsNullOrEmpty(s))
            {
                try
                {
                    return Convert.ToInt32(s);
                }
                catch
                {
                }
            }
            return defaultValue;
        }

        /// <summary>
        /// Sets a string setting in the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to store.</param>
        /// <param name="value">The setting's new value.</param>
        /// <remarks>
        /// <para>This method will fire the <see cref="PropertyChanged"/> event
        /// if the new value is different from any existing value.</para>
        /// </remarks>
        protected void SetInternal(string name, string value)
        {
            if (propertyBag.Values[name] != value)
            {
                propertyBag.Values[name] = value;
                FirePropertyChanged(name);
            }
        }

        /// <summary>
        /// Sets an integer setting in the <see cref="PropertyBag"/>.
        /// </summary>
        /// <param name="name">Name of setting to store.</param>
        /// <param name="value">The setting's new value.</param>
        /// <remarks>
        /// <para>This method will fire the <see cref="PropertyChanged"/> event
        /// if the new value is different from any existing value.</para>
        /// </remarks>
        protected void SetInternalInt(string name, int value)
        {
            int differentDefault = value == 0 ? 1 : 0;
            if (GetInternalInt(name, differentDefault) != value)
            {
                propertyBag.Values[name] = Convert.ToString(value);
                FirePropertyChanged(name);
            }
        }

        protected PropertyBag GetInternalSubGroup(string name)
        {
            return propertyBag.SubGroups[name];
        }

        protected void SetInternalSubGroup(string name, PropertyBag bag)
        {
            propertyBag.SubGroups[name] = bag;
            FirePropertyChanged(name);
        }

        /// <summary>
        /// Fires the PropertyChanged event for the specified property name.
        /// </summary>
        /// <param name="name">A property name.</param>
        protected virtual void FirePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Default property "Title".
        /// </summary>
        /// <remarks>
        /// <para>This is a convenience property to get and set the string
        /// setting with name "Title". It can also be accessed through
        /// <see cref="GetInternal(string)"/> and
        /// <see cref="SetInternal(string, string)"/>.</para>
        /// </remarks>
        public virtual string Title
        {
            get { return GetInternal("Title"); }
            set { SetInternal("Title", value); }
        }

        /// <summary>
        /// Get the <see cref="PropertyBag"/> maintained by this object.
        /// </summary>
        /// <returns>A property bag that can be serialized.</returns>
        public virtual PropertyBag ToPropertyBag()
        {
            return propertyBag;
        }
    }
}
