﻿using System;
using System.Collections.Generic;
using System.Timers;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class TableUpdater<T,C,E> : IDisposable 
        where T : ParameterTable 
        where C : RealtimeConnectionSettings
    {
        internal event EventHandler<CancellableEventArgs> Updating;
        internal event EventHandler Updated;
        internal event EventHandler StatusChanged;

        private StreamingStatus status = StreamingStatus.Stopped;

        private readonly T table;
        private readonly C settings;
        private RealtimeDataAdapter<T,C,E> realtimeDataAdapter;

        private readonly Dictionary<RowLookupKey, Row> rowLookup =
            new Dictionary<RowLookupKey, Row>();

        private readonly EventQueue<E> queue;

        private readonly object flushSync = new object();

        private readonly Timer flushTimer;
        private readonly Timer reconnectTimer;

        private long lastUpdate;

        private readonly int idColumnIndex = -1;
        private readonly int timeIdColumnIndex = -1;

        private DateTime maxTimeIdFromSource = DateTime.MinValue;

        private bool disposed;
        private bool stopped = true;
        private bool useInclusiveTimeWindow = true;

        public TableUpdater(T table, C settings)
        {
            this.settings = settings;
            this.table = table;

            queue = new EventQueue<E>();

            for (int i = 0; i < table.ColumnCount; i++) {
                string name = table.GetColumn(i).Name;
                if (name.Equals(settings.IdColumn)) {
                    idColumnIndex = i;
                }
                else if (name.Equals(settings.TimeIdColumn)) {
                    timeIdColumnIndex = i;
                }
            }

            flushTimer = new Timer(settings.Limit);
            flushTimer.Stop();
            flushTimer.Elapsed += flushTimer_Elapsed;

            reconnectTimer = new Timer(5000);
            reconnectTimer.Stop();
            reconnectTimer.Elapsed += reconnectTimer_Elapsed;

            UpdateRowLookup();
        }

        ~TableUpdater()
        {
            Dispose(false);
            GC.SuppressFinalize(this);
        }

        public void Connected()
        {
            Status = StreamingStatus.Streaming;
        }

        public void ConnectionLost(bool reconnect)
        {
            Status = StreamingStatus.Waiting;
            if (reconnect)
            {
                reconnectTimer.Start();
            }
        }

        public void Dispose()
        {
            Dispose(true);
        }
        
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    reconnectTimer.Stop();
                    Stop();
                }

                disposed = true;
            }
        }

        public void EnqueueDelete(string id, E evt)
        {
            Enqueue(EventType.Delete, id, evt);
        }

        public void EnqueueInsert(string id, E evt)
        {
            Enqueue(EventType.Insert, id, evt);
        }

        public void EnqueueUpdate(string id, E evt)
        {
            Enqueue(EventType.Update, id, evt);
        }

        private void Enqueue(EventType type, string id, E evt)
        {
            // We don't timestamp (if auto) until we flush, so this will be
            // MinValue unless it's the adapter that provides timestamps.
            DateTime time = GetTimestamp(DateTime.MinValue, evt);

            if (status == StreamingStatus.Stopped) {
                return;
            }

            queue.Enqueue(type, id, time, evt);

            if (!flushTimer.Enabled && !settings.ManualFlush) {
                flushTimer.Start();
            }
        }

        private void flushTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            lock (flushSync)
            {
                flushTimer.Stop();
                FlushInternal();
            }
        }

        public void Flush()
        {
            lock (flushSync)
            {
                flushTimer.Stop();
                FlushInternal();
            }
        }

        private void FlushInternal()
        {
            IEnumerator<EventQueueEntry<E>> events = queue.Mark(); 

            lock (flushSync)
            {
                if (!OnUpdating(new CancellableEventArgs())) {
                    queue.DiscardMark();
                    return;
                }

                // Should we auto-generate time stamps on each flush,
                // and is there a valid time column?
                bool autoTime = timeIdColumnIndex >= 0 &&
                    settings.IsTimeIdColumnGenerated;

                DateTime now = DateTime.Now;
                table.BeginUpdate();

                try
                {
                    while (events.MoveNext())
                    {
                        EventQueueEntry<E> entry = events.Current;
                        
                        DateTime timeId = autoTime ? now : entry.Time;
                        RowLookupKey key = new RowLookupKey(entry.Id, timeId);

                        switch (entry.Type)
                        {
                            case EventType.Reset:
                                table.ClearRows();
                                rowLookup.Clear();
                                break;

                            case EventType.Insert:
                                ProcessInsert(key, entry.Event);
                                break;

                            case EventType.Update:
                                ProcessUpdate(key, entry.Event);
                                break;

                            case EventType.Delete:
                                ProcessDelete(entry.Id, entry.Event);
                                break;
                        }
                    }

                    CheckDeleteOldTimeWindow(now);
                    CheckRowLimit();
                }
                finally {
                    queue.CommitMark();
                    table.EndUpdate();
                }

                OnUpdated(EventArgs.Empty);
                lastUpdate = DateTime.Now.Ticks;
            }
        }

        private bool OnUpdating(CancellableEventArgs e)
        {
            if (this.Updating != null) {
                this.Updating(this, e);                    
                if (!e.IsCancelled) {
                    return true;
                }
            }
            return false;
        }

        private void OnUpdated(EventArgs e)
        {
            if (this.Updated != null) {
                this.Updated(this, e);
            }
        }

        private void CheckDeleteOldTimeWindow(DateTime now)
        {
            if (settings.TimeWindowSeconds <= 0 || timeIdColumnIndex < 0) {
                return;
            }

            DateTime minTimeWindow = DateTime.MinValue;
            if (settings.IsTimeIdColumnGenerated) {
                minTimeWindow = now.AddSeconds(
                    -settings.TimeWindowSeconds);
            }
            else if (maxTimeIdFromSource != DateTime.MinValue) {
                minTimeWindow = maxTimeIdFromSource.AddSeconds(
                    -settings.TimeWindowSeconds);
            }
            DeleteOldTimeWindow(minTimeWindow);
        }

        private void CheckRowLimit()
        {
            // As we only support MaxRowCountExceededBehavior.Prevent 
            // in streaming data connectors we can assume that the 
            // table.RowLimit is the "actual rowlimit" minus 1, as it
            // is calculated in DatatableBuilder.GetData.
            int actualLimit = table.RowLimit - 1;
            if (table.RowLimit != -1 && table.RowCount > actualLimit) {
                Log.Error(string.Format(
                    Resources.LogMaximumNumberOfRows, actualLimit));
                table.Error = new Exception(string.Format(
                    Resources.ExMaximumNumberOfRows, actualLimit));
                Stop();
            }
        }

        private void ProcessInsert(RowLookupKey key, E evt)
        {
            Row row;
            if (!rowLookup.TryGetValue(key, out row)) {
                row = AddRow(key);
            }
            realtimeDataAdapter.UpdateColumns(row, evt);
        }

        private void ProcessUpdate(RowLookupKey key, E evt)
        {
            Row row;
            if (!rowLookup.TryGetValue(key, out row)) {
                row = AddRow(key);
            }
            realtimeDataAdapter.UpdateColumns(row, evt);
        }

        private void ProcessDelete(string id, E evt)
        {
            List<RowLookupKey> matching = new List<RowLookupKey>();
            foreach (RowLookupKey key in rowLookup.Keys) {
                if (key.Id == id) {
                    matching.Add(key);
                }
            }
            foreach (RowLookupKey key in matching) {
                Row row = rowLookup[key];
                table.RemoveRow(row);
                rowLookup.Remove(key);
            }
        }

        private void DeleteOldTimeWindow(DateTime minValue)
        {
            List<RowLookupKey> toRemove = new List<RowLookupKey>();
            foreach(RowLookupKey key in rowLookup.Keys)
            {
                if (key.TimeId != DateTime.MinValue &&
                    ((useInclusiveTimeWindow && key.TimeId < minValue) || 
                    key.TimeId <= minValue))
                {
                    toRemove.Add(key);
                }
            }
            foreach(RowLookupKey key in toRemove)
            {
                Row row = rowLookup[key];
                table.RemoveRow(row);
                rowLookup.Remove(key);
            }
        }

        // Returns DateTime.MinValue if there is no time id column, the
        // passed in value if we should generate timestamps, and otherwise
        // asks the adapter.
        private DateTime GetTimestamp(DateTime now, E evt)
        {
            if (timeIdColumnIndex < 0) {
                return DateTime.MinValue;
            }
            if (settings.IsTimeIdColumnGenerated) {
                return now;
            }
            return realtimeDataAdapter.GetTimestamp(evt);
        }


        private Row AddRow(RowLookupKey key)
        {
            object[] values = new object[table.ColumnCount];
            values[idColumnIndex] = key.Id;
            if (timeIdColumnIndex >= 0) {
                values[timeIdColumnIndex] = key.TimeId;
                if (key.TimeId > maxTimeIdFromSource) {
                    maxTimeIdFromSource = key.TimeId;
                }
            }
            Row row = table.AddRow(values);
            rowLookup[key] = row;
            return row;
        }

        private void reconnectTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Stop();

            Status = StreamingStatus.Waiting;

            try
            {
                realtimeDataAdapter.Start();
                Status = StreamingStatus.Streaming;
                reconnectTimer.Stop();
                return;
            }
            catch
            {
            }

            //Log.Error(Properties.Resources.ExReconnectFailed,
            //    settings.Host, settings.UserName, settings.Stream,
            //    connection != null ? connection.Status.getErrorMessage() : "");
        }

        public void Reset()
        {
            // TODO: Should check StreamingStatus?
            queue.Enqueue(EventType.Reset,
                null, DateTime.MinValue, default(E));
                
            if (!flushTimer.Enabled && !settings.ManualFlush) {
                flushTimer.Start();
            }
        }

        public StreamingStatus Status
        {
            get { return status; }
            private set
            {
                if (status != value)
                {
                    status = value;
                    if (StatusChanged != null)
                    {
                        StatusChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        public void Start(RealtimeDataAdapter<T,C,E> r)
        {
            realtimeDataAdapter = r;
            // queue.Combiner = realtimeDataAdapter as ICombineEvents<E>;
            realtimeDataAdapter.Initialize(this, table, settings);
            realtimeDataAdapter.Start();
            Status = StreamingStatus.Streaming;
            stopped = false;
        }

        public virtual void Stop()
        {
            if (!stopped)
            {
                Status = StreamingStatus.Stopped;
                flushTimer.Stop();
                realtimeDataAdapter.Stop();
                stopped = true;
            }
        }

        public StandaloneTable Table
        {
            get { return table; }
        }

        private void UpdateRowLookup()
        {
            if (idColumnIndex < 0) {
                return;
            }
            TextColumn idColumn = table.GetColumn(idColumnIndex) as TextColumn;
            if (idColumn == null) {
                return;
            }
            TimeColumn timeIdColumn = timeIdColumnIndex >= 0 ?
                table.GetColumn(timeIdColumnIndex) as TimeColumn : null;

            for (int row = 0; row < table.RowCount; row++)
            {
                string id = idColumn.GetTextValue(row);
                if (string.IsNullOrEmpty(id)) {
                    continue;
                }
                DateTime timestamp = timeIdColumn != null ?
                    timeIdColumn.GetTimeValue(row) : DateTime.MinValue;
                RowLookupKey key = new RowLookupKey(id, timestamp);
                if (timestamp > maxTimeIdFromSource) {
                    maxTimeIdFromSource = timestamp;
                }
                rowLookup[key] = table.GetRow(row);
            }
        }

        public bool UseInclusiveTimeWindow
        {
            get { return useInclusiveTimeWindow; }
            set { useInclusiveTimeWindow = value; }
        }


        private class RowLookupKey
        {
            public readonly string Id;
            public readonly DateTime TimeId;

            public RowLookupKey(string id, DateTime timeId)
            {
                this.Id = id;
                this.TimeId = timeId;
            }
            public override bool Equals(object obj)
            {
                if (this == obj) return true;
                if (!(obj is RowLookupKey)) return false;
                
                RowLookupKey other = (RowLookupKey) obj;
                return this.Id.Equals(other.Id) &&
                    this.TimeId.Equals(other.TimeId);
            }

            public override int GetHashCode()
            {
                return (Id.GetHashCode()*397) ^ TimeId.GetHashCode();
            }
        }
    }
}
