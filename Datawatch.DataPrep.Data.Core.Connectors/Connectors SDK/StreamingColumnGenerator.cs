﻿using System;
using System.Collections.Generic;
using System.Threading;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class StreamingColumnGenerator : IMessageHandler
    {
        private MessageQueueSettingsBase settings;
        private readonly int maxNumberOfMessagesToAnalyze;
        private readonly int maximumTimeToWaitSecs;
        private int numReceivedMessages = 0;
        private long timeLimit;
        private bool isDone;
        private readonly object lockObject = new object();
        private ColumnGenerator cg;
        private IPluginErrorReportingService errorReporter;

        List<string> receivedMessages = new List<string>();

        public StreamingColumnGenerator(MessageQueueSettingsBase settings,
            IPluginErrorReportingService errorReporter)
            : this(settings, 10, 10, errorReporter)
        {
        }

        //TODO: Should consider moving maximumTimeToWaitSecs to MessageQueueSettingsBase
        public StreamingColumnGenerator(MessageQueueSettingsBase settings,
            int maxNumberOfMessagesToAnalyze, int maximumTimeToWaitSecs,
            IPluginErrorReportingService errorReporter)
        {
            this.settings = settings;
            this.maxNumberOfMessagesToAnalyze = maxNumberOfMessagesToAnalyze;
            this.maximumTimeToWaitSecs = maximumTimeToWaitSecs;
            this.errorReporter = errorReporter;
        }

        // Generate a stream containing incommming rows. Then that stream will be 
        // used to generate the columns.
        public void Generate()
        {
            isDone = false;
            TimeSpan timeout = new TimeSpan(0, 0, 0, maximumTimeToWaitSecs);
            // Wait until timeout or all wanted rows have been collected
            lock (lockObject)
            {
                Monitor.Wait(lockObject, timeout);
            }

            if (numReceivedMessages <= 0)
            {
                this.errorReporter.Report(
                    Properties.Resources.UiInfo,
                    Properties.Resources.UiColumnGeneratorNoRosReceived);
                return;
            }

            // We now got our rows through the callback method MessageReceived(string)
            // and we can now generate the ColumnDefinitions.
            cg = new ColumnGenerator(settings.ParserSettings,
                settings.NumericDataHelper, true, true);
            foreach (string message in receivedMessages)
            {
                cg.Generate(message);
            }
        }

        // Use this method as a callback method to receive rows
        public void MessageReceived(string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }
            lock (lockObject)
            {
                if (isDone)
                {
                    return;
                }

                receivedMessages.Add(message);
                numReceivedMessages++;

                if (numReceivedMessages >= maxNumberOfMessagesToAnalyze)
                {
                    isDone = true;
                    Monitor.PulseAll(lockObject);
                }
            }
        }
    }
}
