﻿using System.Collections.ObjectModel;
using System.Xml;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class RealtimeConnectionSettings : ConnectionSettings
    {
        private string[] idColumnCandidates;
        private string[] timeIdColumnCandidates;

        private ObservableCollection<string> idColumns =
            new ObservableCollection<string>();
        private ObservableCollection<TimeIdColumnViewModel> timeIdColumns =
            new ObservableCollection<TimeIdColumnViewModel>();

        public RealtimeConnectionSettings()
            : base()
        {
            AddFirstTwoTimeId();
        }

        public RealtimeConnectionSettings(PropertyBag bag,
            bool initializeCandidates) : base(bag)
        {
            AddFirstTwoTimeId();
            if (initializeCandidates)
            {
                InitializeCandidates();
            }
        }

        protected void InitializeCandidates()
        {
            if (IdColumn != null)
            {
                IdColumnCandidates = new string[] { IdColumn };
            }
            if (TimeIdColumn != null && !IsTimeIdColumnGenerated)
            {
                TimeIdColumnCandidates = new string[] { TimeIdColumn };
            }
        }

        private void AddFirstTwoTimeId()
        {
            timeIdColumns.Add(new TimeIdColumnViewModel(this, null, false,
                Properties.Resources.UiDataPluginNoTimeId));
            string generatedName = IsTimeIdColumnGenerated ? TimeIdColumn : 
                Properties.Resources.UiDataPluginDefaultGeneratedName;
            timeIdColumns.Add(new TimeIdColumnViewModel(this, generatedName,
                true, Properties.Resources.UiDataPluginDefaultGenerated));
        }

        public virtual string IdColumn
        {
            get { return GetInternal("IdColumn"); }
            set { SetInternal("IdColumn", value); }
        }

        public int Limit
        {
            get 
            {
                return GetInternalInt("Limit", 1000);
            }
            set
            {
                if (value <= 0)
                {
                    throw Exceptions.BadRealtimeLimit();
                }
                SetInternalInt("Limit", value); 
            }
        }

        public virtual string TimeIdColumn
        {
            get { return GetInternal("TimeIdColumn"); }
            set { SetInternal("TimeIdColumn", value); }
        }

        public bool IsTimeIdColumnGenerated
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsTimeIdColumnGenerated", defaultValue));
            }
            set
            {
                SetInternal("IsTimeIdColumnGenerated",
                    XmlConvert.ToString(value));
            }
        }

        public int TimeWindowSeconds
        {
            get { return GetInternalInt("TimeWindowSeconds", 0); }
            set { SetInternalInt("TimeWindowSeconds", value);}
        }

        public string[] IdColumnCandidates
        {
            get { return idColumnCandidates; }
            set
            {
                if (idColumnCandidates != value)
                {
                    idColumnCandidates = value;
                    UpdateIdColumns();
                }
            }
        }

        public string[] TimeIdColumnCandidates
        {
            get { return timeIdColumnCandidates; }
            set
            {
                if (timeIdColumnCandidates != value)
                {
                    timeIdColumnCandidates = value;
                    UpdateTimeIdColumns();
                }
            }
        }

        private void UpdateIdColumns()
        {
            string oldIdColumn = IdColumn;

            idColumns.Clear();
            
            bool found = false;

            if (IdColumnCandidates != null)
            {
                foreach (string s in IdColumnCandidates)
                {
                    found = found || s == oldIdColumn;
                    idColumns.Add(s);
                }
            }

            if (found) {
                IdColumn = oldIdColumn;
            } else if (idColumns.Count > 0) {
                IdColumn = idColumns[0];
            } else {
                IdColumn = null;
            }
        }

        private void UpdateTimeIdColumns()
        {
            while (timeIdColumns.Count > 2) {
                timeIdColumns.RemoveAt(timeIdColumns.Count - 1);
            }

            bool found = false;

            if (TimeIdColumnCandidates != null) {
                foreach (string s in TimeIdColumnCandidates)
                {
                    found = found || s == TimeIdColumn;
                    TimeIdColumnViewModel vm =
                        new TimeIdColumnViewModel(this, s, false, s);
                    timeIdColumns.Add(vm);
                }
            }

            if (!IsTimeIdColumnGenerated &&
                TimeIdColumn != null && TimeIdColumn.Length > 0
                && !found)
            {
                IsTimeIdColumnGenerated = false;
                TimeIdColumn = null;
            }

            FirePropertyChanged("SelectedTimeIdColumnViewModel");
        }

        public ObservableCollection<string> IdColumns
        {
            get { return idColumns; }
        }

        public ObservableCollection<TimeIdColumnViewModel> TimeIdColumns
        {
            get { return timeIdColumns; }
        }

        public virtual TimeIdColumnViewModel SelectedTimeIdColumnViewModel
        {
            get
            {
                if (IsTimeIdColumnGenerated) {
                    return timeIdColumns[1];
                }
                if (TimeIdColumn == null)
                {
                    return timeIdColumns[0];
                }
                for (int i = 2; i < timeIdColumns.Count; i++)
                {
                    if (timeIdColumns[i].Name == TimeIdColumn)
                    {
                        return timeIdColumns[i];
                    }
                }
                return null;
            }
            set
            {
                if (value == null)
                {
                    // Ignore when trying to assign a null value
                    return;
                }

                IsTimeIdColumnGenerated = value.IsGenerated;
                TimeIdColumn = value.Name;
                FirePropertyChanged("SelectedTimeIdColumnViewModel");
            }
        }

        public bool ManualFlush
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("ManualFlush", defaultValue));
            }
            set
            {
                SetInternal("ManualFlush",XmlConvert.ToString(value));
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RealtimeConnectionSettings))
            {
                return false;
            }

            RealtimeConnectionSettings other = (RealtimeConnectionSettings) obj;
            return this.IdColumn == other.IdColumn
                && this.IsTimeIdColumnGenerated == other.IsTimeIdColumnGenerated
                && this.Limit == other.Limit
                && this.TimeIdColumn == other.TimeIdColumn
                && this.TimeWindowSeconds == other.TimeWindowSeconds
                && this.ManualFlush == other.ManualFlush;
        }

        public override int GetHashCode()
        {
            int result = 17;
            const int multipcator = 31;

            result = multipcator * result
                + (IdColumn != null ? IdColumn.GetHashCode() : 0);
            result = multipcator * result
                + IsTimeIdColumnGenerated.GetHashCode();
            result = multipcator * result
                + Limit;
            result = multipcator * result
                + (TimeIdColumn != null ? TimeIdColumn.GetHashCode() : 0);
            result = multipcator * result
                + TimeWindowSeconds;
            result = multipcator * result
                + ManualFlush.GetHashCode();
            return result;
        }

        public class TimeIdColumnViewModel : ViewModelBase
        {
            private RealtimeConnectionSettings parent;
            private string name;
            private bool isGenerated;

            public TimeIdColumnViewModel(RealtimeConnectionSettings parent,
                string name, bool isGenerated, string displayName)
            {
                this.parent = parent;
                this.name = name;
                this.isGenerated = isGenerated;
                this.DisplayName = displayName;
            }

            public string Name
            {
                get { return name; }
                set
                {
                    if (name != value)
                    {
                        name = value;
                        parent.TimeIdColumn = value;
                        OnPropertyChanged("Name");
                    }
                }
            }

            public bool IsGenerated
            {
                get { return isGenerated; }
                set
                {
                    if (isGenerated != value)
                    {
                        isGenerated = value;
                        OnPropertyChanged("IsGenerated");
                    }
                }
            }
        }
    }
}
