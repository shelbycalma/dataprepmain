﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class RealtimeDataPlugin<T,C,E,R> : 
        DataPluginBase<C>, IRealtimeDataPlugin 
        where T : ParameterTable
        where C : RealtimeConnectionSettings
        where R : RealtimeDataAdapter<T,C,E>,new()
    {
        public event EventHandler<RealtimeDataPluginEventArgs> Updating;

        public event EventHandler<RealtimeDataPluginEventArgs> Updated;

        public event EventHandler<RealtimeStatusEventArgs> StreamingStatusChanged;

        // The tableLookup is used to keep track of active ParameterTable instances
        // with respect to the ConnectionSettings and Parameters used to create 
        // them.
        // The tableLookup might go out of sync if there are two datatables 
        // using the same parameters and settings in which case the same key 
        // will be generated.
        private readonly Dictionary<RealtimeDataPluginTableKey<C>, T> tableLookup =
            new Dictionary<RealtimeDataPluginTableKey<C>, T>();

        // The sinkLookup is used to keep track of active real-time sinks.
        private readonly Dictionary<T, TableUpdater<T, C, E>> tableUpdaterLookup =
            new Dictionary<T, TableUpdater<T,C,E>>();

        private bool disposed;

        protected abstract ITable CreateTable(C settings, 
            IEnumerable<ParameterValue> parameters, string dataDirectory);

        private void AttachTableUpdater(TableUpdater<T, C, E> tableUpdater)
        {
            tableUpdater.Updating += tableUpdater_Updating;
            tableUpdater.Updated += tableUpdater_Updated;
            tableUpdater.StatusChanged += tableUpdater_StatusChanged;
        }

        private void DetachTableUpdater(TableUpdater<T, C, E> tableUpdater)
        {
            tableUpdater.Updating -= tableUpdater_Updating;
            tableUpdater.Updated -= tableUpdater_Updated;
            tableUpdater.StatusChanged -= tableUpdater_StatusChanged;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (!disposed)
            {
                if (disposing)
                {
                    //Managed
                    foreach (TableUpdater<T,C,E> sink in tableUpdaterLookup.Values)
                    {
                        DetachTableUpdater(sink);
                        sink.Dispose();
                    }
                    tableUpdaterLookup.Clear();
                    tableLookup.Clear();
                    disposed = true;
                }
            }
        }

        public override string DataPluginType
        {
            get { return Datawatch.DataPrep.Data.Core.Properties.Resources.UiDataPluginTypeStreaming; }
        }
        
        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            // Check that the plug-in has a valid license.
            CheckLicense();

            // Create a ConnectionSettings instance from the settings PropertyBag.
            C settings = CreateSettings(bag);

            // If the tableLookup contains a table for the ConnectionSettings
            // and parameters, return the active table.
            RealtimeDataPluginTableKey<C> tableKey =
                new RealtimeDataPluginTableKey<C>(settings, parameters, rowcount);

            lock (this)
            {
                if (tableLookup.ContainsKey(tableKey))
                {
                    T result = tableLookup[tableKey];

                    IDisposable disposable = settings as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }

                    return result;
                }
            }

            // [DDTV-5969] Synchronized the rest of this method so concurrent
            // calls to this method don't create multiple tables and start
            // updaters for them.
            lock (this) {
                // Check again in case it was put there while we slept.
                if (tableLookup.ContainsKey(tableKey)) {
                    T result = tableLookup[tableKey];
                    IDisposable disposable = settings as IDisposable;
                    if (disposable != null) {
                        disposable.Dispose();
                    }
                    return result;
                }
                // Retrieve an empty table from the plugin implementation.
                ITable table = CreateTable(settings, parameters, dataDir);
                ParameterTable pTable = table as ParameterTable;
                if (pTable != null) {
                    pTable.RowLimit = rowcount;
                }

                // Add the auto generated time id column if needed
                if (settings.IsTimeIdColumnGenerated &&
                    settings.TimeIdColumn != null &&
                    settings.TimeIdColumn.Length > 0 &&
                    table is T) {
                    T tTable = (T) table;
                    tTable.BeginUpdate();
                    try {
                        TimeColumn timeColumn = new TimeColumn(settings.TimeIdColumn);
                        tTable.AddColumn(timeColumn);
                    }
                    finally {
                        tTable.EndUpdate();
                    }
                }

                // If server-side start updates immediately inside
                // synchronization block (this will add the table
                // to tableLookup).
                if (!isHosted) {
                    StartRealtimeUpdates(table, bag);
                }

                // Return the table.
                return table;
            }
        }

        public virtual bool IsRealtime(PropertyBag settings)
        {
            return true;
        }

        public void StartRealtimeUpdates(ITable t, PropertyBag bag)
       
        {
            if (!(t is T)) return;

            T table = (T)t;

            lock (this)
            {
                if (!tableUpdaterLookup.ContainsKey(table))
                {
                    C settings = CreateSettings(bag);
                    
                    TableUpdater<T,C,E> tableUpdater = null;
                    try
                    {
                        tableUpdater = new TableUpdater<T,C,E>(table, settings);
                        AttachTableUpdater(tableUpdater);
                        var adapter = new R() { ErrorReporter = this.ErrorReporter };
                        tableUpdater.Start(adapter);
                        tableUpdaterLookup[table] = tableUpdater;
                        RealtimeDataPluginTableKey<C> tableKey =
                            new RealtimeDataPluginTableKey<C>(settings,
                                table.Parameters, table.RowLimit);
                        tableLookup[tableKey] = table;
                    }
                    catch (Exception)
                    {
                        //Log.Error(Properties.Resources.ExConnect,
                        //    connectionSettings.Host,
                        //    connectionSettings.UserName,
                        //    connectionSettings.Stream,
                        //    e.Message);
                        if (tableUpdater != null)
                        {
                            DetachTableUpdater(tableUpdater);
                            tableUpdater.Dispose();
                        }
                        
                    }
                }
            }
        }

        public void StopRealtimeUpdates(ITable t, PropertyBag bag)
        {
            if (!(t is T)) return;

            lock (this)
            {
                T table = (T)t;

                if (tableUpdaterLookup.ContainsKey(table))
                {
                    TableUpdater<T,C,E> tableUpdater = tableUpdaterLookup[table];
                    C settings = CreateSettings(bag);
                    
                    DetachTableUpdater(tableUpdater);
                    tableUpdater.Dispose();
                    tableUpdaterLookup.Remove(table);
                    RealtimeDataPluginTableKey<C> tableKey =
                        new RealtimeDataPluginTableKey<C>(settings,
                            table.Parameters, table.RowLimit);
                    tableLookup.Remove(tableKey);
                }
            }
        }

        private void tableUpdater_StatusChanged(object sender, EventArgs e)
        {
            TableUpdater<T, C, E> tableUpdater = (TableUpdater<T, C, E>)sender;
            if (StreamingStatusChanged != null)
            {
                StreamingStatusChanged(this,
                    new RealtimeStatusEventArgs(tableUpdater.Table, tableUpdater.Status));
            }
        }

        private void tableUpdater_Updating(object sender, CancellableEventArgs e)
        {
            TableUpdater<T, C, E> tableUpdater = (TableUpdater<T, C, E>)sender;
            if (Updating != null)
            {
                RealtimeDataPluginEventArgs eventArgs = 
                    new RealtimeDataPluginEventArgs(tableUpdater.Table);
                Updating(this, eventArgs);
                e.IsCancelled = eventArgs.IsCancelled;
            }
        }

        private void tableUpdater_Updated(object sender, EventArgs e)
        {
            TableUpdater<T, C, E> tableUpdater = (TableUpdater<T, C, E>)sender;
            if (Updated != null)
            {
                Updated(this, new RealtimeDataPluginEventArgs(tableUpdater.Table));
            }
        }
    }
}
