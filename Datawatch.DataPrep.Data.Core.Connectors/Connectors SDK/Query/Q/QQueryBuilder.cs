﻿using System.Text;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Connectors.Query.Q
{
    public class QQueryBuilder : QueryBuilder
    {
        private bool isSelectAllQuery = false;
        public bool IsSelectAllQuery
        { 
            get { return isSelectAllQuery; }
            set { isSelectAllQuery = value; }
        }
        override public string ToString(SqlDialect dialect)
        {
            StringBuilder sb = new StringBuilder();
            if (Limit > 0)
            {
                sb.AppendFormat("{0} sublist ",Limit);
            }
            
            SchemaAndTable activeSAT;
            if (SourceQuery != null)
            {
                activeSAT = new SchemaAndTable(null, "source_table");
            }
            else
            {
                activeSAT = SchemaAndTable;
            }

            foreach (GroupByClause.GroupItem groupItem in GroupBy.GroupColumns) 
            {
                string columnName = groupItem.ColumnName;
                if (groupItem is GroupByClause.GroupTimePart 
                    && Select.ContainsColumn(groupItem.Alias)) 
                {
                    columnName = groupItem.Alias;
                }
                Select.RemoveColumn(columnName);
            }

            if (Select.IsEmpty && GroupBy.GroupColumns.Count>0 &&
                !IsSelectAllQuery)
            {
                Select.AddColumn("1");
            }
            sb.Append(Select.ToString(activeSAT, dialect));

            sb.Append(GroupBy.ToString(dialect));

            sb.Append(dialect.GetQueryPart(QueryPart.From));
            if (SourceQuery != null)
            {
                sb.AppendFormat(dialect.SourceQuery,
                    SourceQuery, activeSAT.Table);
            }
            else
            {
                if (activeSAT != null)
                {
                    sb.Append(activeSAT.Table);
                }
            }

            CheckPredicates(Where.Predicate);
            sb.Append(Where.ToString(dialect));

            return sb.ToString();
        }

        private static void CheckPredicates(Predicate predicate, int depth = 0) 
        {
            if (predicate is NotOperator)
            {
                NotOperator notOperator = (NotOperator)predicate;
                depth++;
                CheckPredicates(notOperator.A, depth);
            }
            else if (predicate is BinaryOperator) 
            {
                BinaryOperator binaryOperator = (BinaryOperator)predicate;

                if (binaryOperator.A == null && binaryOperator.B != null)
                {
                    depth++;
                }

                if (predicate is AndOperator && depth <= 1)
                {
                    AndOperator andOperator = predicate as AndOperator;
                    andOperator.UsePredicateSeparator = true;
                }
                CheckPredicates(binaryOperator.A, depth);
                CheckPredicates(binaryOperator.B, depth);
            }
            else if (predicate is Comparison)
            {
                Comparison comparison = (Comparison)predicate;
                if (comparison.Right is StringParameter)
                {
                    StringParameter stringParam = 
                        comparison.Right as StringParameter;
                    if (!string.IsNullOrEmpty(stringParam.TextValue))
                    {
                        stringParam.TextValue =
                            stringParam.TextValue.Replace('%', '*').
                            Replace('_', '?');
                    }
                    if (comparison.Operator == Operator.Like
                        && comparison.Left is ColumnParameter)
                    {
                        stringParam.TextValue = string.Format("lower(\"{0}\")", 
                            stringParam.TextValue);
                        stringParam.IsQuoteValue = false;
                        ColumnParameter columnParam = (ColumnParameter)comparison.Left;
                        columnParam.ColumnName = string.Format("lower[{0}]", 
                            columnParam.ColumnName);
                    }
                }
            }
        }
    }
}
