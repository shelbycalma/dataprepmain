﻿using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors.Query.Q
{
    public interface IKdbColumnMetaData : IColumnMetaData
    {
        char KdbType { get; set; }
    }
}
