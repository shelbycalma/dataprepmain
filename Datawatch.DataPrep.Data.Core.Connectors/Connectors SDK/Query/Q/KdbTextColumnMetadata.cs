﻿using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors.Query.Q
{
    public class KdbTextColumnMetaData : TextColumnMetaData, IKdbColumnMetaData
    {
        public KdbTextColumnMetaData(IColumn column): base(column)
        { 
        }

        public char KdbType
        {
            get;
            set;
        }
    }
}
