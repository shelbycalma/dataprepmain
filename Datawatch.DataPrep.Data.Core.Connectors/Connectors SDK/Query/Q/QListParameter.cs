﻿using System.Linq;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Connectors.Query.Q
{
    public class QListParameter : ListParameter
    {
        public char KdbType { get; set; }

        public QListParameter()
        {
        }

        public QListParameter(ListParameter v, char kdbType):base(v.Values)
        {
            KdbType = kdbType;
        }

        public override string ToString(SqlDialect dialect)
        {
            //Convert the boolean text values to Kdb boolean
            if (KdbType == 'b')
            {
                QuoteValue = false;
                for (int i = 0; i < Values.Length; i++)
                {
                    if ("true".Equals(Values[i]))
                    {
                        Values[i] = "1";
                    }
                    else
                    {
                        Values[i] = "0";
                    }
                }
            }

            string ret = base.ToString(dialect);
            if (KdbType == 'c' || KdbType == 'b')
            {
                ret = ret.Replace("`$(", "(");
                //single char[] value needs to be declared enlist
                if (Values.Count() == 1)
                {
                    return "enlist" + ret;
                }
            }
            return ret;
        }
    }
}
