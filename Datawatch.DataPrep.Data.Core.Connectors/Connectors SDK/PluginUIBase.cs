﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class PluginUIBase<T, TC> : IPluginUI<T>
        where T : DataPluginBase<TC>
        where TC : ConnectionSettings
    {
        protected Window owner;
        protected ImageSource image;
        
        protected PluginUIBase(T plugin, Window owner)
        {
            if (plugin == null)
            {
                Framework.Exceptions.ArgumentNull("plugin");
            }

            Plugin = plugin;
            this.owner = owner;
        }

        public T Plugin { get; private set; }

        public string Id { get { return Plugin.Id; } }

        public abstract object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters);

        public abstract PropertyBag GetSetting(object obj);

        public abstract PropertyBag DoConnect(IEnumerable<ParameterValue> parameters);

        public virtual bool IsLicensed
        {
            get { return Plugin.IsLicensed; }
        }

        public ImageSource Image
        {
            get { return image; }
        }

        public virtual string GetTitle(PropertyBag settings)
        {
            return Plugin.CreateSettings(settings).Title;
        }

        protected void CheckLicense()
        {
            if (!this.IsLicensed)
            {
                throw Exceptions.PluginNotLicensed(GetType());
            }
        }
    }
}
