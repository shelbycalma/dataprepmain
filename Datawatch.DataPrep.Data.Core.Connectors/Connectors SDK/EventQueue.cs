﻿using System;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public enum EventType
    {
        Insert, Update, Delete, Reset
    }

    public sealed class EventQueueEntry<TEvent>
    {
        private readonly EventType type;
        private readonly string id;
        private readonly DateTime time;
        private readonly TEvent evt;

        public EventQueueEntry(EventType type,
            string id, DateTime time, TEvent evt)
        {
            this.type = type;
            this.id = id;
            this.time = time;
            this.evt = evt;
        }

        public TEvent Event { get { return evt; } }
        public string Id { get { return id; } }
        public DateTime Time { get { return time; } }
        public EventType Type { get { return type; } }

        public EventQueueEntry<TEvent> Next { get; set; } 
    }

    public sealed class EventQueue<TEvent>
    {
        // First entry in queue (may be marked).
        private EventQueueEntry<TEvent> first;
        // Last entry in queue (may be after marked).
        private EventQueueEntry<TEvent> last;

        // If marked, last marked entry.
        private EventQueueEntry<TEvent> mark;
        // Because mark can be null.
        private bool hasMark;

        private readonly object sync = new object();


        private void Clear()
        {
            lock (sync) {
                if (hasMark && mark != null) {
                    // Only clear entries after the mark.
                    last = mark;
                    last.Next = null;
                } else {
                    first = null;
                    last = null;
                }
            }
        }

        // For reset, all other arguments ignored.
        public void Enqueue(EventType type,
            string id, DateTime time, TEvent e)
        {
            EventQueueEntry<TEvent> entry =
                new EventQueueEntry<TEvent>(type, id, time, e);

            lock (sync) {
                if (entry.Type == EventType.Reset) {
                    Clear();
                }
                if (last != null) {
                    last.Next = entry;
                } else {
                    first = entry;
                }
                last = entry;
            }
        }

        // Marks all entries currently in the queue and returns them.
        // Call CommitMark or DiscardMark when finished.
        public IEnumerator<EventQueueEntry<TEvent>> Mark()
        {
            Enumerator marked;
            lock (sync) {
                if (hasMark) {
                    throw new InvalidOperationException("Already marked.");
                }
                if (last == null) {
                    marked = Enumerator.Empty;
                } else {
                    mark = last;
                    marked = new Enumerator(first, mark);
                }
                hasMark = true;
            }
            return marked;
        }

        public void CommitMark()
        {
            lock (sync) {
                if (!hasMark) {
                    throw new InvalidOperationException("Not marked.");
                }
                if (mark != null) {
                    // Marked when queue was empty.
                    if ((first = mark.Next) == null) {
                        last = null;
                    }
                }
                mark = null;
                hasMark = false;
            }
        }

        public void DiscardMark()
        {
            lock (sync) {
                if (!hasMark) {
                    throw new InvalidOperationException("Not marked.");
                }
                // Check if reset was queued after mark.
                if (mark != null && mark.Next != null &&
                    mark.Next.Type == EventType.Reset) {
                    // Make reset the first event.
                    first = mark.Next;
                }
                mark = null;
                hasMark = false;
            }
        }


        private sealed class Enumerator : IEnumerator<EventQueueEntry<TEvent>>
        {
            // ReSharper disable once StaticFieldInGenericType
            public static readonly Enumerator Empty = new Enumerator();

            private readonly EventQueueEntry<TEvent> first;
            private readonly EventQueueEntry<TEvent> last;
            
            private EventQueueEntry<TEvent> current;

            private Enumerator()
            {
                first = last = null;
            }

            public Enumerator(
                EventQueueEntry<TEvent> first, EventQueueEntry<TEvent> last)
            {
                if (first == null) {
                    throw Exceptions.ArgumentNull("first");
                }
                if (last == null) {
                    throw Exceptions.ArgumentNull("last");
                }
                this.first = first;
                this.last = last;
            }

            public EventQueueEntry<TEvent> Current
            {
                get { return current; }
            }

            public void Dispose()
            {
            }

            object System.Collections.IEnumerator.Current
            {
                get { return current; }
            }

            public bool MoveNext()
            {
                if (current == last) {
                    // Also handles Empty enumerator.
                    return false;
                }
                if (current == null) {
                    current = first;
                    return true;
                }
                current = current.Next;
                return true;
            }

            public void Reset()
            {
                current = null;
            }
        }
    }
}
