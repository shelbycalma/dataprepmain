﻿using System;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class CancellableEventArgs : EventArgs
    {
        public bool IsCancelled { get; set; }
    }
}
