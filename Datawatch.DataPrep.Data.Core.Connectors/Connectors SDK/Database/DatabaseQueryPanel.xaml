﻿<UserControl
    x:Class="Datawatch.DataPrep.Data.Core.Connectors.Database.DatabaseQueryPanel"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml" 
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:Properties="clr-namespace:Datawatch.DataPrep.Data.Core.Connectors.Properties"
    xmlns:Converters="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Converters;assembly=Datawatch.DataPrep.Data.Core.UI"
    xmlns:ui="clr-namespace:Datawatch.DataPrep.Data.Core.Connectors.Database;assembly=Datawatch.DataPrep.Data.Core.Connectors"
    xmlns:coreUI="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Plugin;assembly=Datawatch.DataPrep.Data.Core.UI"
    xmlns:behaviors="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Behaviors;assembly=Datawatch.DataPrep.Data.Core.UI"
    behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme">
    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/Converters/Converters.xaml"/>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/Themes/Generic.xaml"/>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/Widgets/ListViewWithDelete.xaml"/>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/ResourceDictionaries/CommonStyles.xaml"/>
            </ResourceDictionary.MergedDictionaries>

            <Style TargetType="ToolTip">
                <Style.Resources>
                    <Style TargetType="ContentPresenter">
                        <Style.Resources>
                            <Style TargetType="TextBlock">
                                <Setter Property="TextWrapping" Value="Wrap" />
                            </Style>
                        </Style.Resources>
                    </Style>
                </Style.Resources>
                <Setter Property="MaxWidth" Value="400" />
            </Style>
            <Style BasedOn="{StaticResource CommonCheckBoxStyle}" TargetType="{x:Type CheckBox}"/>
            <Style BasedOn="{StaticResource CommonTextBoxStyle}" TargetType="{x:Type TextBox}"/>
            <Style BasedOn="{StaticResource CommonLabelStyle}" TargetType="{x:Type Label}" />
            <Style x:Key="HeaderControl" TargetType="Control">
                <Setter Property="VerticalAlignment" Value="Bottom"/>
                <Setter Property="VerticalContentAlignment" Value="Bottom"/>
                <Setter Property="Margin" Value="5 5 5 0"/>
                <Setter Property="MinHeight" Value="22"/>
            </Style>

            <BitmapImage x:Key="alert-red" UriSource="Images/alert-red.png" />
            <BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
            <Converters:BoolInverterConverter x:Key="Not"/>
            <Converters:TimeZoneConverter x:Key="TimeZoneConverter" />
            <Converters:ObjectToVisibility x:Key="ObjectToVisibility" />
        </ResourceDictionary>
    </UserControl.Resources>

    <UserControl.CommandBindings>
        <CommandBinding
            Command="{x:Static ui:DatabaseQueryPanel.PreviewCommand}"
            CanExecute="Preview_CanExecute"/>
    </UserControl.CommandBindings>

    <Grid>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <!-- error alert -->
            <RowDefinition Height="*"/>
            <!-- columns -->
            <RowDefinition Height="*"/>
            <!-- Query -->
            <RowDefinition Height="Auto"/>
            <!-- TimeZone -->
        </Grid.RowDefinitions>
        <Border Height="38" BorderBrush="Red" BorderThickness="2" Visibility="{Binding QuerySettings.ErrorMessage, Converter={StaticResource ObjectToVisibility}}">
            <Grid Height="34" Grid.Row="0" Visibility="{Binding QuerySettings.ErrorMessage, Converter={StaticResource ObjectToVisibility}}" Background="#035F92">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" />
                    <ColumnDefinition Width="*" />
                </Grid.ColumnDefinitions>
                <Image Grid.Column="0" Source="{StaticResource alert-red}" Margin="5,5,5,5" 
                    Visibility="{Binding QuerySettings.ErrorMessage, Converter={StaticResource ObjectToVisibility}}"
                    ToolTip="{Binding QuerySettings.ErrorMessage}" 
                    VerticalAlignment="Center" HorizontalAlignment="Left" Width="22" />
                <TextBlock Grid.Column="1" Text="{Binding QuerySettings.ErrorMessage}" VerticalAlignment="Center" TextTrimming="WordEllipsis"
                    ToolTip="{Binding QuerySettings.ErrorMessage}" Foreground="White" />
            </Grid>
        </Border>
        <TabControl x:Name="SourceType" SelectionChanged="TabControl_SelectionChanged" Grid.Row="1">
            <TabItem Header="{x:Static Properties:Resources.UiTablesLabel}" x:Name="TableTab">
                <Grid
                        Grid.IsSharedSizeScope="True"
                        IsEnabled="{Binding QuerySettings.IsQueryMode,
                        Converter={StaticResource boolInverterConverter}}">
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <!--<ColumnDefinition Width="5"/>
                        <ColumnDefinition Width="*"/>-->
                        <!--<ColumnDefinition Width="18*"/>-->
                    </Grid.ColumnDefinitions>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="*"/>
                    </Grid.RowDefinitions>
                    <coreUI:QueryBuilderTableControl
                        Grid.Row="0"
                        DataContext="{Binding Settings, 
                        RelativeSource={RelativeSource
                        FindAncestor, AncestorType=UserControl}}"/>
                    <!--<GridSplitter Grid.Column="1" Width="3"
                                      HorizontalAlignment="Center" />-->
                    <coreUI:QueryBuilderColumnControl
                        Grid.Row="1"
                        DataContext="{Binding Settings, 
                        RelativeSource={RelativeSource
                        FindAncestor, AncestorType=UserControl}}"/>
                </Grid>
                <!--</GroupBox>-->

            </TabItem>
            <TabItem Header="{x:Static Properties:Resources.UiQueryLabel}" x:Name="QueryTab">
                <Canvas Name="QueryCanvas" Margin="5 0" Grid.Row="3" MinHeight="80">
                    <TextBox behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme"
                            Width="{Binding ActualWidth, ElementName=QueryCanvas}"
                            Height="{Binding ActualHeight, ElementName=QueryCanvas}"
                            HorizontalAlignment="Stretch"
                            VerticalAlignment="Stretch"
                            AcceptsReturn="True"
                            IsEnabled="True"
                            IsReadOnly="False"
                            TextWrapping="Wrap"
                            VerticalScrollBarVisibility="Auto"
                            Text="{Binding QuerySettings.Query,
                            UpdateSourceTrigger=PropertyChanged}"
                            Style="{DynamicResource QueryTextboxStyle}" />
                </Canvas>
            </TabItem>
            <TabItem Header="{x:Static Properties:Resources.UiReportLabel}" IsSelected="False" Visibility="{Binding QuerySettings.IsReportSupported,
                    Converter={StaticResource BooleanToVisibilityConverter}}" x:Name="ReportTab">
                <Grid>
                    <Grid.RowDefinitions>
                        <RowDefinition Height="Auto"/>
                        <RowDefinition Height="*" />
                        <RowDefinition Height="Auto" />
                    </Grid.RowDefinitions>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="Auto" />
                        <ColumnDefinition Width="5" />
                        <ColumnDefinition Width="*" />
                    </Grid.ColumnDefinitions>
                    <Button Grid.Row="0"
                                Content="{x:Static Properties:Resources.UiLoadReports}" Click="LoadReports_OnClick"/>
                    <ListBox Name="ReportGroupList"
                                 Grid.Column="0"
                                 Height="135"
                                 ScrollViewer.VerticalScrollBarVisibility="Auto"
                                 Grid.Row="1"
                                 SelectionMode="Single"
                                 SelectionChanged="reportGroupListBox_SelectionChanged"
                                 ItemsSource="{Binding Settings.QuerySettings.ReportGroups}"
                                 SelectedItem="{Binding Settings.QuerySettings.SelectedReportGroup}">
                        <ListBox.Resources>
                            <SolidColorBrush
                                    x:Key="{x:Static SystemColors.HighlightBrushKey}"
                                    Color="LightSkyBlue" />
                            <SolidColorBrush
                                    x:Key="{x:Static SystemColors.HighlightTextBrushKey}"
                                    Color="Black" />
                        </ListBox.Resources>
                    </ListBox>
                    <GridSplitter Grid.Column="1" Width="3" Grid.RowSpan="2"
                                      HorizontalAlignment="Center" />
                    <ListBox Name="ReportList"
                                 Height="135"
                                 ScrollViewer.VerticalScrollBarVisibility="Auto"
                                 Grid.Column="2" Grid.Row="1"
                                 SelectionMode="Single"
                                 SelectionChanged="reportListBox_SelectionChanged"
                                 DisplayMemberPath="ReportName"
                                 ItemsSource="{Binding Settings.QuerySettings.ReportList.Report}"
                                 SelectedItem="{Binding Settings.QuerySettings.SelectedReport}">
                        <ListBox.Resources>
                            <SolidColorBrush
                                    x:Key="{x:Static SystemColors.HighlightBrushKey}"
                                    Color="LightSkyBlue" />
                            <SolidColorBrush
                                    x:Key="{x:Static SystemColors.HighlightTextBrushKey}"
                                    Color="Black" />
                        </ListBox.Resources>
                    </ListBox>
                </Grid>
            </TabItem>
        </TabControl>
        <Grid Grid.Row="2" Grid.IsSharedSizeScope="True">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>
            <Button Content="{x:Static Properties:Resources.UiPreviewData}" Grid.Row="0" Width="Auto" HorizontalAlignment="Left" 
                Command="{x:Static ui:DatabaseQueryPanel.PreviewCommand}" Click="PreviewData_OnClick" />
            <DataGrid behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme"
                    Grid.Row="1" Height="100" Width="Auto" MaxHeight="250" MinHeight="150" Name="PreviewGrid"
                    AutoGenerateColumns="True"
                    AutoGeneratingColumn="PreviewGrid_AutoGeneratingColumn"
                    ScrollViewer.CanContentScroll="True"
                    ScrollViewer.HorizontalScrollBarVisibility="Auto"
                    ScrollViewer.VerticalScrollBarVisibility="Auto"
                    CanUserAddRows="false" IsReadOnly="True">
            </DataGrid>
        </Grid>
        <Grid Grid.Row="3">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="0.5*"/>
                <ColumnDefinition Width="0.5*"/>
            </Grid.ColumnDefinitions>
            <Grid.RowDefinitions>
                <RowDefinition/>
                <RowDefinition/>
                <RowDefinition/>
            </Grid.RowDefinitions>
            <Label
                Content="{x:Static Properties:Resources.UiTimezoneLabel}"
                Style="{StaticResource CommonLabelStyle}"
                Margin="5 0 0 0"
                Visibility="{Binding QuerySettings.PluginHostSettings.IsTimezoneSupported,
                    Converter={StaticResource BooleanToVisibilityConverter}}"/>
            <ComboBox behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme"
                Grid.Row="0"
                Grid.Column="1"
                Style="{DynamicResource ComboBoxStyleBase}"
                ItemsSource="{Binding QuerySettings.TimeZoneHelper.TimeZones}"
                SelectedItem="{Binding QuerySettings.TimeZoneHelper.SelectedTimeZone}"
                IsSynchronizedWithCurrentItem="True"
                Visibility="{Binding QuerySettings.PluginHostSettings.IsTimezoneSupported,
                    Converter={StaticResource BooleanToVisibilityConverter}}"/>
            <!--<CheckBox
                Grid.Row="1"                        
                Grid.ColumnSpan="2"
                Style="{StaticResource CommonCheckBoxStyle}"
                Margin="10 5 5 5"
                IsChecked="{Binding QuerySettings.IsOnDemand}"
                Visibility="Hidden"
                IsEnabled="False"
                Content="{x:Static Properties:Resources.UiOnDemand}"/>-->
        </Grid>
    </Grid>
</UserControl>