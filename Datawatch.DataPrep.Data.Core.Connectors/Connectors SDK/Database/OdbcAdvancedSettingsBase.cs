﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public abstract class OdbcAdvancedSettingsBase : PropertyBagViewModel
    {
        private const string AdvancedSettings = "AdvancedSettings";
        IList<PropertyInfo> reflectedProperties;

        public OdbcAdvancedSettingsBase(PropertyBag settings)
            : base(CreateSubGroup(settings))
        {
        }

        private static PropertyBag CreateSubGroup(PropertyBag settings)
        {
            if (settings == null)
            {
                throw Exceptions.ArgumentNull("settings");
            }

            PropertyBag bag = settings.SubGroups[AdvancedSettings];
            if (bag == null)
            {
                bag = new PropertyBag();
                settings.SubGroups[AdvancedSettings] = bag;
            }
            
            return bag;
        }

        public virtual string ConvertToString(object value)
        {
            if (value == null)
            {
                return string.Empty;
            }
            if (value is bool)
            {
                // Most ODBC drivers except bool as 0/1
                return Convert.ToInt16(value).ToString();
            }

            return value.ToString();
        }

        public Dictionary<string, object> PropertiesDictionary
        {
            get
            {
                return this.GetType().GetProperties(
                    BindingFlags.Public | BindingFlags.Instance).Where(
                    p => p.DeclaringType.IsSubclassOf(
                        typeof(OdbcAdvancedSettingsBase))).ToDictionary(
                        p => p.Name, p => p.GetGetMethod().Invoke(this, null));

            }
        }

        public IEnumerable<OdbcProperty> Properties
        {
            get 
            {
                IList<OdbcProperty> properties =
                    new List<OdbcProperty>();
                Type type = this.GetType();

                reflectedProperties = new List<PropertyInfo>(
                    type.GetProperties(BindingFlags.Public |
                    BindingFlags.Instance));

                foreach (PropertyInfo prop in reflectedProperties)
                {
                    // Get rid of properties in OdbcAdvancedSettingsBase
                    // and its parents.
                    if(!prop.DeclaringType.IsSubclassOf(
                        typeof(OdbcAdvancedSettingsBase)))
                    {
                        // Can't break here, GetProperties method does not 
                        // return properties in a particular order.
                        continue;
                    }

                    object value = prop.GetValue(this, null);
                    if (value == null)
                    {
                        value = "";
                    }
                    string displayName = prop.Name;
                    OdbcProperty propValue = null;
                    string[] items = null;
                    foreach (Attribute attr in prop.GetCustomAttributes(false))
                    {
                        OdbcAdvancedSettingsAttribute propAttr =
                            attr as OdbcAdvancedSettingsAttribute;
                        if (propAttr != null)
                        {
                            displayName = propAttr.DisplayName;
                            items = propAttr.Items;
                            break;
                        }
                    }
                    if (items != null)
                    {
                        propValue = new OdbcItemsProperty(prop.Name, value,
                            displayName, items);
                    }
                    else
                    {
                        propValue =
                            new OdbcProperty(prop.Name, value,
                                displayName);
                    }                   
                    propValue.PropertyChanged += propValue_PropertyChanged;
                    properties.Add(propValue);
                }                    
                
                return properties; 
            }
        }

        private void propValue_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            OdbcProperty propValue = sender as OdbcProperty;
            if (propValue == null)
            {
                return;
            }
            PropertyInfo targetProp = FindProperty(propValue.Name);
            if (targetProp == null)
            {
                return;
            }
            try
            {
                targetProp.SetValue(this, propValue.Value, null);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        private PropertyInfo FindProperty(string name)
        {
            foreach (PropertyInfo prop in reflectedProperties)
            {
                if (prop.Name == name)
                {
                    return prop;
                }
            }
            return null;
        }

    }
}
