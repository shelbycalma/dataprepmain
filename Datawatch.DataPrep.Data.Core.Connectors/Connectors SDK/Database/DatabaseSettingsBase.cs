﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Text;
using System.IO;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.ViewModel;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Threading;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
	public abstract class DatabaseSettingsBase : ConnectionSettings,
		IDatabaseConnectionStringProvider, IQueryBuilderTableClient
	{
		private readonly DatabaseConnectorQuerySettings querySettings;
		private IEnumerable<ParameterValue> parameters;
		private ParameterValueCollection escapedParameters;
		IEnumerable<DatabaseColumn> columns;
		protected List<string> databases;
		private DelegateCommand<Window> fetchDatabases;
		protected readonly List<string> restrictedTables = new List<string>();

		private readonly QueryBuilderTableViewModel<DatabaseColumn> queryBuilderViewModel;
		private DatabaseExecutorBase executor;

		public DatabaseSettingsBase(PropertyBag bag,
			IEnumerable<ParameterValue> parameters, SqlDialect sqlDialect,
			DatabaseExecutorBase executor)
			: base(bag)
		{
			this.parameters = parameters;
			this.executor = executor;
			string table = GetInternal("Table");
			SchemaAndTable selectedTable = null;

			if (!string.IsNullOrEmpty(table))
			{
				selectedTable = SchemaAndTable.Parse(table);
			}

			querySettings = new DatabaseConnectorQuerySettings(bag, this,
				selectedTable);
			querySettings.SqlDialect = sqlDialect;

			querySettings.PropertyChanged += querySettings_PropertyChanged;

			querySettings.SelectedColumns.CollectionChanged +=
				new NotifyCollectionChangedEventHandler(
					SelectedColumns_CollectionChanged);
			queryBuilderViewModel =
				new QueryBuilderTableViewModel<DatabaseColumn>(this, QuerySettings);
			queryBuilderViewModel.DoUpdateQuery += QueryBuilderViewModel_DoUpdateQuery;
		}


		public DatabaseExecutorBase Executor
		{
			get { return executor; }

		}

		private void QueryBuilderViewModel_DoUpdateQuery(object sender, EventArgs e)
		{
			UpdateQuery();
		}

		[HandleProcessCorruptedStateExceptions]
		[SecurityCritical]
		public virtual void GetDatabases()
		{
			if (string.IsNullOrEmpty(DatabaseListQuery)) return;

			string connectionString =
				DataUtils.ApplyParameters(GetConnectionString(true),
					parameters, false);

			string databasepattern = "(?<=;Database=).*?(?=;)";
            connectionString = Regex.Replace(connectionString, databasepattern, "");

			try
			{
				using (DbConnection connection =
					Executor.GetConnection(connectionString))
				{
					connection.Open();
					using (DbCommand command = connection.CreateCommand())
					{
						command.CommandType = CommandType.Text;
						command.CommandText = DatabaseListQuery;
						using (DbDataReader reader = command.ExecuteReader())
						{
							databases = new List<string>();
							while (reader.Read())
							{
								databases.Add(reader.GetString(0).Trim());
							}
						}
					}
				}
				FirePropertyChanged("AvailableDatabases");
			}
			catch (AccessViolationException)
			{
                Log.Exception(Properties.Resources.ExUnableToConnectDueToAVE);
                MessageBox.Show(Properties.Resources.ExUnableToConnectDueToAVE, PluginTitle,
				  MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		protected virtual string DatabaseListQuery
		{
			get
			{
				return string.Empty;
			}
		}

		public ICommand FetchDatabases
		{
			get
			{
				if (fetchDatabases == null)
				{
					Window mainWindow = null;
					fetchDatabases = new DelegateCommand<Window>(DoFetchDatabases,
						window => CanFetchDatabases(window) && !fetchDatabaseWorker.IsBusy);
					fetchDatabaseWorker.DoWork += (sender, args) =>
					{
						mainWindow = (Window)args.Argument;
                        querySettings.ErrorMessage = String.Empty;
                        GetDatabases();
					};
					fetchDatabaseWorker.RunWorkerCompleted += (sender, args) =>
					{
						IsLoading = false;
						CommandManager.InvalidateRequerySuggested();
						if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
						{
							Mouse.OverrideCursor = null;
						}
						if (args.Error != null)
						{
                            querySettings.ErrorMessage = args.Error.Message;
						}
						else
						{
							if (string.IsNullOrEmpty(DatabaseName))
							{
								IEnumerable<string> sources = AvailableDatabases;
								if (sources != null)
								{
									DatabaseName = AvailableDatabases.FirstOrDefault();
                                }
                            }
                        }
                    };
				}
				return fetchDatabases;
			}
		}

		private bool CanFetchDatabases(Window window)
		{
			return (!string.IsNullOrEmpty(Host) && !string.IsNullOrEmpty(Port) && !IsLoading);
		}

		private BackgroundWorker fetchDatabaseWorker = new BackgroundWorker();
		public bool IsLoading { get; set; }

		private void DoFetchDatabases(Window window)
		{
			if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
			{
				Mouse.OverrideCursor = Cursors.Wait;
			}
			IsLoading = true;
			fetchDatabaseWorker.RunWorkerAsync(window);
			CommandManager.InvalidateRequerySuggested();
		}

		/// <summary>
		/// Should be overridden by concrete classes which has their own
		/// additional settings other than Host/Port etc, that needs to go inside connectionstring.
		/// This is however only usefull when GetConnectionString method is not overridden.
		/// </summary>
		public virtual string PluginConnectionString
		{
			get
			{
				return "";
			}
		}

		public abstract string DriverName { get; }

		public abstract string PluginTitle { get; }

		public abstract string WindowTitle { get; }

		public abstract bool IsPluginSettingsOk { get; }

		// Should be overridden by concrete when additional connection parameters
		// are required.
		public virtual bool CanLoadTables
		{
			get
			{
				if (string.IsNullOrEmpty(Host) ||
					   string.IsNullOrEmpty(Port) ||
					   string.IsNullOrEmpty(DatabaseName))
				{
					return false;
				}

				return true;
			}
		}

		// If not null, this is used while loading tables and views
		// as a parameter to GetSchema Method.
		// Should be overridden by concrete classes to limit amout of schema returned.
		public virtual string[] SchemaRestrictions
		{
			get
			{
				return null;
			}
		}

		public virtual bool AllowFetchDatabases
		{
			get
			{
				return string.IsNullOrEmpty(DatabaseListQuery);
			}
		}

		public string DatabaseConnectionString
		{
			get
			{
				return GetConnectionString();
			}
		}

		public IEnumerable<string> AvailableDatabases
		{
			get
			{
				return databases;
			}
		}

		[HandleProcessCorruptedStateExceptions]
		[SecurityCritical]
		private void querySettings_PropertyChanged(object sender,
			PropertyChangedEventArgs e)
		{
			try
			{
				if (e.PropertyName == "SelectedTable")
				{
					if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
					{
						Mouse.OverrideCursor = Cursors.Wait;
					}
					UpdateColumns();
                    querySettings.ApplyFilter = false;
                    querySettings.FilteredColumns.Clear();
                    FirePropertyChanged("Columns");
				}
				UpdateQuery();
			}
			catch (AccessViolationException)
			{
                Log.Exception(Properties.Resources.ExUnableToConnectDueToAVE);
				MessageBox.Show(Properties.Resources.ExUnableToConnectDueToAVE, PluginTitle,
				  MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
				if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
				{
					Mouse.OverrideCursor = null;
				}
			}

		}

		public virtual string Host
		{
			get { return GetInternal("Host"); }
			set { SetInternal("Host", value); }
		}

		public virtual string Url
		{
			get { return GetInternal("Url"); }
			set { SetInternal("Url", value); }
		}

		public virtual string Port
		{
			get { return GetInternal("Port"); }
			set
			{
				SetInternal("Port", value);
				FirePropertyChanged("Port");
			}
		}

		public string UserId
		{
			get { return GetInternal("UserId"); }
			set { SetInternal("UserId", value); }
		}

		public string Password
		{
			get { return GetInternal("Password"); }
			set { SetInternal("Password", value); }
		}

		public string OAuth_AccessToken
		{
			get { return GetInternal("OAuth_AccessToken"); }
			set { SetInternal("OAuth_AccessToken", value); }
		}

		public string OAuth_RefreshToken
		{
			get { return GetInternal("OAuth_RefreshToken"); }
			set { SetInternal("OAuth_RefreshToken", value); }
		}

        public string OAuth_TokenExpirySession
        {
            get { return GetInternal("OAuth_TokenExpirySession"); }
            set { SetInternal("OAuth_TokenExpirySession", value); }
        }

        public string OAuth_TokenTimestampSession
        {
            get { return GetInternal("OAuth_TokenTimestampSession"); }
            set { SetInternal("OAuth_TokenTimestampSession", value); }
        }

        public string OAuth_AccessTokenSession
        {
            get { return GetInternal("OAuth_AccessTokenSession"); }
            set { SetInternal("OAuth_AccessTokenSession", value); }
        }

        public string OAuth_RefreshTokenSession
        {
            get { return GetInternal("OAuth_RefreshTokenSession"); }
            set { SetInternal("OAuth_RefreshTokenSession", value); }
        }

		public virtual string DatabaseName
		{
			get { return GetInternal("DatabaseName"); }
			set
			{
				SetInternal("DatabaseName", value);
				CommandManager.InvalidateRequerySuggested();
				if (Columns != null)
				{
					QueryBuilderViewModel.Columns.Clear();
				}
				FirePropertyChanged("Columns");
				QuerySettings.SelectedColumns.Clear();
                QuerySettings.FilteredColumns.Clear();
			}
		}

		public virtual OdbcAdvancedSettingsBase AdvancedSettings
		{
			get { return null; }
		}

		protected virtual string HostKey
		{
			get { return "host"; }
		}

		protected virtual string DatabaseKey
		{
			get { return "db"; }
		}

		private static bool IsValidQuery(string query)
		{
			if (query.Contains(';'))
				return false;
			if (query.Contains("--"))
				return false;
			if (!(query.ToUpper().StartsWith("SELECT") || query.ToUpper().StartsWith("EXEC")))
				return false;

			return true;
		}

		public bool IsOk
		{
			get
			{
				if (!string.IsNullOrEmpty(Host) &&
					   !string.IsNullOrEmpty(Port) &&
					   !string.IsNullOrEmpty(DatabaseName) &&
					   !IsPluginSettingsOk)
				{
					return false;
				}

				if (querySettings == null)
				{
					return false;
				}
                //return querySettings.IsQueryMode
                //	? !string.IsNullOrEmpty(querySettings.Query)
                //	: querySettings.SelectedTable != null && querySettings.SelectedColumns.Count > 0;

				if (querySettings.QueryMode == QueryMode.Query && querySettings.Query != null)
				{
					if (IsValidQuery(QuerySettings.Query))
						return true;
					else
						return false;
				}
                else if (querySettings.QueryMode == QueryMode.Table && querySettings.SelectedTable != null && querySettings.SelectedColumns.Count > 0)
                    return true;
                else 
                if (querySettings.QueryMode == QueryMode.Report && querySettings.SelectedReport != null && querySettings.Query != null)
                    return true;
                else
                    return false;
			}
		}

		public DatabaseQuerySettings QuerySettings
		{
			get { return querySettings; }
		}

		public IEnumerable<ParameterValue> Parameters
		{
			get { return parameters; }
			set { parameters = value; }
		}

		protected List<string> RestrictedSchemas
		{
			get
			{
				List<string> schemas = new List<string>();
				schemas.Add("SYS");
				schemas.Add("INFORMATION_SCHEMA");
				return schemas;
			}
		}

		protected List<string> RestrictedTables
		{
			get
			{
				return restrictedTables;
			}
		}

		public ParameterValueCollection EscapedParameters
		{
			get
			{
				if (parameters == null) return null;

				if (escapedParameters == null)
				{
					escapedParameters =
						DatabaseUtils.EscapeParameters(parameters);
				}
				return escapedParameters;
			}
		}

		public IEnumerable<DatabaseColumn> Columns
		{
			get
			{
				if (columns == null)
				{
					UpdateColumns();
				}
				return columns;
			}
		}

		public void UpdateColumns()
		{
			if (querySettings.SelectedTable != null)
			{
				try
				{
					string connectionString =
						DataUtils.ApplyParameters(
						querySettings.ConnectionString, parameters, false);
					columns = new ObservableCollection<DatabaseColumn>(
						Executor.GetColumns(querySettings,
						parameters, connectionString).ToList<DatabaseColumn>());
				}
				catch (Exception ex)
				{
                    if (ex.Message.Contains("400 Bad Request"))
                    {
                        querySettings.ErrorMessage = Properties.Resources.UiQueryBuilderErrorMessageBadRequest;
                    }
					else
					{
						querySettings.ErrorMessage = ex.Message;
					}
                    Log.Exception(querySettings.ErrorMessage);
                    return;
				}
				QuerySettings.PredicateColumns.Clear();
				foreach (DatabaseColumn availableColumn in columns)
				{
					availableColumn.PropertyChanged +=
						new PropertyChangedEventHandler(
							availableColumns_PropertyChanged);
					if (availableColumn.AppliedParameterName != null)
					{
						QuerySettings.PredicateColumns.Add(availableColumn);
					}
				}
				UpdateQuery();
                querySettings.ErrorMessage = String.Empty;
			}
		}

		private void availableColumns_PropertyChanged(object sender,
			PropertyChangedEventArgs e)
		{
			QuerySettings.PredicateColumns.Clear();
			foreach (DatabaseColumn availableColumn in columns)
			{
				if (availableColumn.
					AppliedParameterName != null)
				{
					QuerySettings.PredicateColumns.Add(availableColumn);
				}
			}
			UpdateQuery();
		}

		public virtual bool IsOndemandSupported
		{
			get
			{
				if (querySettings.PluginHostSettings != null)
				{
					return querySettings.PluginHostSettings.IsOnDemandSupported;
				}
				return true;
			}
		}

		public virtual bool IsFreeformSQLSupported
		{
			get
			{
				if (querySettings.PluginHostSettings != null)
				{
					return querySettings.PluginHostSettings.IsFreeformSQLSupported;
				}
				return true;
			}
		}

		public virtual bool FilterSchema(string schema)
		{
			return RestrictedSchemas.Contains(schema.ToUpper());
		}

		public virtual bool FilterTable(string table)
		{
			return RestrictedTables.Contains(table.ToUpper());
		}

		public virtual string SystemDatabaseName
		{
			get { return string.Empty; }
		}

		public QueryBuilderTableViewModel<DatabaseColumn> QueryBuilderViewModel
		{
			get { return queryBuilderViewModel; }
		}

		public SchemaAndTable[] Tables
		{
			get
			{
				string connectionString = DataUtils.ApplyParameters(
					QuerySettings.ConnectionString, Parameters, false);
				return GetTablesAndViews(connectionString);
			}
		}

		public virtual bool IsCDataDriver { get; private set; }

		public virtual string IsReset { get; set; }

		protected virtual string GetConnectionString()
		{
			return GetConnectionString(false);
		}

		protected virtual string GetConnectionString(bool useSystemDatabase)
		{
			StringBuilder connectionString = new StringBuilder();

			if (IsCDataDriver)
			{
				connectionString.Append(@"Driver={");

				connectionString.Append(DriverName + "}");

				if (!string.IsNullOrEmpty(Url))
				{
					connectionString.Append(";Url=" + Url);
				}

				if (!string.IsNullOrEmpty(Host))
				{
					connectionString.Append(";Server=" + Host);
				}
				if (!string.IsNullOrEmpty(Port))
				{
					connectionString.Append(";Port=" + Port);
				}
				if (!string.IsNullOrEmpty(UserId))
				{
					connectionString.Append(";User=" + UserId);
				}
				if (!string.IsNullOrEmpty(Password))
				{
					connectionString.Append(";Password=" + Password);
				}
				if (useSystemDatabase && !string.IsNullOrEmpty(SystemDatabaseName))
				{
					connectionString.Append(";Database=" + SystemDatabaseName);
				}
				else if (!string.IsNullOrEmpty(DatabaseName))
				{
					connectionString.Append(";Database=" + DatabaseName);
				}
				string RTK = GetRTK(DriverName);
				if (RTK.Length > 0)
				{
					connectionString.Append(";RTK=" + RTK);
                    //querySettings.ErrorMessage = String.Empty;
                }
                else
				{
                    querySettings.ErrorMessage = Properties.Resources.UiErrorNoRTK;
                    //MessageBox.Show(Properties.Resources.UiErrorNoRTK, PluginTitle, MessageBoxButton.OK, MessageBoxImage.Error);
				}
			}
			else
			{
				connectionString.Append(@"Driver={");

				connectionString.Append(DriverName + "}");

				if (!string.IsNullOrEmpty(Host))
				{
					connectionString.Append(";" + HostKey + "=" + Host);
				}
				if (!string.IsNullOrEmpty(Port))
				{
					connectionString.Append(";port=" + Port);
				}
				if (!string.IsNullOrEmpty(UserId))
				{
					connectionString.Append(";uid=" + UserId);
				}
				if (!string.IsNullOrEmpty(Password))
				{
					connectionString.Append(";pwd=" + Password);
				}
				if (useSystemDatabase && !string.IsNullOrEmpty(SystemDatabaseName))
				{
					connectionString.Append(";" + DatabaseKey + "=" + SystemDatabaseName);
				}
				else if (!string.IsNullOrEmpty(DatabaseName))
				{
					connectionString.Append(";" + DatabaseKey + "=" + DatabaseName);
				}
			}

			if (!string.IsNullOrEmpty(PluginConnectionString))
			{
				connectionString.Append(";" + PluginConnectionString);
			}

			if (DriverName.ToUpper().Contains("BOX"))
			{
				if ((OAuth_AccessTokenSession == null && OAuth_RefreshTokenSession == null) || IsReset == "True")
				{
					connectionString.Append(";" + GetOAuthTokenBox(connectionString.ToString()));
                    GetSessionTokens(connectionString.ToString());
				}
				else
				{
                    OAuth_AccessToken = OAuth_AccessTokenSession;
                    OAuth_RefreshToken = OAuth_RefreshTokenSession;
                    connectionString.Append(";Other='OAuthSettingsLocation=memory://box;InitiateOAuth=REFRESH;_persist_oauthexpiresin=" + OAuth_TokenExpirySession + ";_persist_token_timestamp=" + OAuth_TokenTimestampSession + ";OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'");
				}
			}

			if (DriverName.ToUpper().Contains("GOOGLEADWORDS"))
			{
				if ((OAuth_AccessToken == null && OAuth_RefreshToken == null) || IsReset == "True")
				{
					connectionString.Append(";" + GetOAuthTokenGoogleAdwords(connectionString.ToString()));
				}
				else
				{
					connectionString.Append(";Other='SupportEnhancedSQL=True;InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'");
				}
			}

			if (DriverName.ToUpper().Contains("GOOGLEBIGQUERY"))
			{
				if ((OAuth_AccessToken == null && OAuth_RefreshToken == null) || IsReset == "True")
				{
					connectionString.Append(";" + GetOAuthTokenGoogleBigQuery(connectionString.ToString()));
				}
				else
				{
					connectionString.Append(";Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'");
				}
			}

			if (DriverName.ToUpper().Contains("GOOGLESPREADSHEETS"))
			{
				if ((OAuth_AccessToken == null && OAuth_RefreshToken == null) || IsReset == "True")
				{
					connectionString.Append(";" + GetOAuthTokenGoogleSheets(connectionString.ToString()));
				}
				else
				{
					connectionString.Append(";Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'");
				}
			}

			if (DriverName.ToUpper().Contains("GOOGLEDRIVE"))
			{
				if ((OAuth_AccessToken == null && OAuth_RefreshToken == null) || IsReset == "True")
				{
					connectionString.Append(";" + GetOAuthTokenGoogleDrive(connectionString.ToString()));
				}
				else
				{
					connectionString.Append(";Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'");
				}
			}

			if (DriverName.ToUpper().Contains("HUBSPOT"))
			{
				if ((OAuth_AccessToken == null && OAuth_RefreshToken == null) || IsReset == "True")
				{
					connectionString.Append(";" + GetOAuthTokenHubspot(connectionString.ToString()));
				}
				else
				{
					connectionString.Append(";Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'");
				}
			}

			if (AdvancedSettings != null &&
				AdvancedSettings.PropertiesDictionary != null)
			{
				foreach (var property in AdvancedSettings.PropertiesDictionary)
				{
					connectionString.Append(";" + property.Key + "=" +
						AdvancedSettings.ConvertToString(property.Value));
				}
			}

            if (IsReset == "True")
                IsReset = "False";

			return connectionString.ToString();
		}

		private void GetSessionTokens(string connectString)
        {
            using (DbConnection connection = Executor.GetConnection(connectString))
            {
                connection.Open();

                string query = "SELECT Value FROM sys_connection_props WHERE IsSessionProperty=true AND Name='_persist_oauthrefreshtoken'";
                DbCommand cmd = connection.CreateCommand();
                cmd.CommandText = query;
                DbDataReader rdr = cmd.ExecuteReader();
                if (rdr != null)
                {
                    rdr.Read();
                    OAuth_RefreshTokenSession = rdr[0].ToString();
                    rdr.Close();
                }

                query = "SELECT Value FROM sys_connection_props WHERE IsSessionProperty=true AND Name='_persist_oauthaccesstoken'";
                cmd = connection.CreateCommand();
                cmd.CommandText = query;
                rdr = cmd.ExecuteReader();
                if (rdr != null)
                {
                    rdr.Read();
                    OAuth_AccessTokenSession = rdr[0].ToString();
                    rdr.Close();
                }

                query = "SELECT Value FROM sys_connection_props WHERE IsSessionProperty=true AND Name='_persist_oauthexpiresin'";
                cmd = connection.CreateCommand();
                cmd.CommandText = query;
                rdr = cmd.ExecuteReader();
                if (rdr != null)
                {
                    rdr.Read();
                    OAuth_TokenExpirySession = rdr[0].ToString();
                    rdr.Close();
                }

                query = "SELECT Value FROM sys_connection_props WHERE IsSessionProperty=true AND Name='_persist_token_timestamp'";
                cmd = connection.CreateCommand();
                cmd.CommandText = query;
                rdr = cmd.ExecuteReader();
                if (rdr != null)
                {
                    rdr.Read();
                    OAuth_TokenTimestampSession = rdr[0].ToString();
                    rdr.Close();
                }

            }
        }
		
		private string GetOAuthTokenBox(string connectString)
		{
			string expiresIn = "";
			string retval = "";

			try
			{
				connectString += ";Other='PromptMode=prompt;InitiateOAuth=OFF'";
				using (DbConnection connection = Executor.GetConnection(connectString))
				{
					connection.Open();
					string query = "EXECUTE GetOAuthAuthorizationURL";
					DbCommand cmd = connection.CreateCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = query;

					DbDataReader rdr = cmd.ExecuteReader();
					rdr.Read();
					Application.Current.Dispatcher.Invoke((Action)delegate {
						var dialog = new OAuthLoginForm(rdr[0].ToString());
						dialog.Height = 600;
						dialog.Width = 900;
						rdr.Close();
						if (dialog.ShowDialog() == true)
						{
							string authCode = dialog.AuthorizationCode;
							query = "EXECUTE GetOAuthAccessToken Verifier = '" + authCode + "', AuthMode = 'WEB', CallBackUrl = 'http://localhost'";
							cmd = connection.CreateCommand();
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = query;
							rdr = cmd.ExecuteReader();
							if (rdr != null)
							{
								rdr.Read();
								OAuth_AccessToken = rdr[0].ToString();
								OAuth_RefreshToken = rdr[1].ToString();
								expiresIn = rdr[2].ToString();
								rdr.Close();
							}
							connection.Close();
                            retval = "Other='OAuthSettingsLocation=memory://box;InitiateOAuth=GETANDREFRESH;_persist_oauthexpiresin=" + OAuth_TokenExpirySession + ";_persist_token_timestamp=" + OAuth_TokenTimestampSession + ";OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'";
						}
						else
						{
							OAuth_AccessToken = null;
							OAuth_RefreshToken = null;
							retval = String.Empty;
						}
					});

					return retval;
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				return String.Empty;
			}
		}

		private string GetOAuthTokenGoogleBigQuery(string connectString)
		{
			string expiresIn = "";
			string retval = "";

			try
			{
				connectString += ";Other='PromptMode=prompt;InitiateOAuth=OFF'";
				//string connectString = "Driver={Datawatch ODBC Driver for GoogleAdWords};RTK=445A44434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E0000524D505443384D34545A35310000;Other='PromptMode=prompt;InitiateOAuth=OFF';Developer Token=FQroiW_FyKEzW7vFMLYqGA;Client Customer ID=838-223-7823;DEB=1";
				using (DbConnection connection = Executor.GetConnection(connectString))
				{
					connection.Open();
					string query = "EXECUTE GetOAuthAuthorizationURL Scope = 'https://www.googleapis.com/auth/bigquery',ApprovalPrompt = 'AUTO', AccessType = 'OFFLINE';";
					DbCommand cmd = connection.CreateCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = query;

					DbDataReader rdr = cmd.ExecuteReader();
					rdr.Read();
					Application.Current.Dispatcher.Invoke((Action)delegate {
						var dialog = new OAuthLoginForm(rdr[0].ToString());
						dialog.Height = 600;
						dialog.Width = 900;
						rdr.Close();
						if (dialog.ShowDialog() == true)
						{
							string authCode = dialog.AuthorizationCode;
							query = "EXECUTE GetOAuthAccessToken Verifier = '" + authCode + "', AuthMode = 'WEB',Scope = 'https://www.googleapis.com/auth/adwords',ApprovalPrompt = 'AUTO', AccessType = 'OFFLINE';";
							cmd = connection.CreateCommand();
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = query;
							rdr = cmd.ExecuteReader();
							if (rdr != null)
							{
								rdr.Read();
								OAuth_AccessToken = rdr[0].ToString();
								OAuth_RefreshToken = rdr[1].ToString();
								expiresIn = rdr[2].ToString();
								rdr.Close();
							}
							connection.Close();
							retval = "Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'";
						}
						else
						{
							OAuth_AccessToken = null;
							OAuth_RefreshToken = null;
							retval = String.Empty;
						}
					});

					return retval;
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				return String.Empty;
			}
		}


		private string GetOAuthTokenGoogleAdwords(string connectString)
		{
			string expiresIn = "";
			string retval = "";

			try
			{
				connectString += ";Other='PromptMode=prompt;InitiateOAuth=OFF'";
				//string connectString = "Driver={Datawatch ODBC Driver for GoogleAdWords};RTK=445A44434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E0000524D505443384D34545A35310000;Other='PromptMode=prompt;InitiateOAuth=OFF';Developer Token=FQroiW_FyKEzW7vFMLYqGA;Client Customer ID=838-223-7823;DEB=1";
				using (DbConnection connection = Executor.GetConnection(connectString))
				{
					connection.Open();
					string query = "EXECUTE GetOAuthAuthorizationURL AuthMode = 'WEB',Scope = 'https://www.googleapis.com/auth/adwords',ApprovalPrompt = 'AUTO', AccessType = 'OFFLINE';";
					DbCommand cmd = connection.CreateCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = query;

					DbDataReader rdr = cmd.ExecuteReader();
					rdr.Read();
					Application.Current.Dispatcher.Invoke((Action)delegate {
						var dialog = new OAuthLoginForm(rdr[0].ToString());
						dialog.Height = 600;
						dialog.Width = 900;
						rdr.Close();
						if (dialog.ShowDialog() == true)
						{
							string authCode = dialog.AuthorizationCode;
							query = "EXECUTE GetOAuthAccessToken Verifier = '" + authCode + "', AuthMode = 'WEB',Scope = 'https://www.googleapis.com/auth/adwords',ApprovalPrompt = 'AUTO', AccessType = 'OFFLINE';";
							cmd = connection.CreateCommand();
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = query;
							rdr = cmd.ExecuteReader();
							if (rdr != null)
							{
								rdr.Read();
								OAuth_AccessToken = rdr[0].ToString();
								OAuth_RefreshToken = rdr[1].ToString();
								expiresIn = rdr[2].ToString();
								rdr.Close();
							}
							connection.Close();
							retval = "Other='SupportEnhancedSQL=True;InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'";
						}
						else
						{
							OAuth_AccessToken = null;
							OAuth_RefreshToken = null;
							retval = String.Empty;
						}
					});

					return retval;
				}
			}
			catch (Exception ex)
			{
                Log.Exception(ex);
                return String.Empty;
			}
		}

		private string GetOAuthTokenGoogleDrive(string connectString)
		{
			string expiresIn = "";
			string retval = "";

			try
			{
				connectString += ";Other='PromptMode=prompt;InitiateOAuth=OFF'";
				using (DbConnection connection = Executor.GetConnection(connectString))
				{
					connection.Open();
					string query = "EXECUTE GetOAuthAuthorizationURL";
					DbCommand cmd = connection.CreateCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = query;

					DbDataReader rdr = cmd.ExecuteReader();
					rdr.Read();
					Application.Current.Dispatcher.Invoke((Action)delegate {
						var dialog = new OAuthLoginForm(rdr[0].ToString());
						dialog.Height = 600;
						dialog.Width = 900;
						rdr.Close();
						if (dialog.ShowDialog() == true)
						{
							string authCode = dialog.AuthorizationCode;
							query = "EXECUTE GetOAuthAccessToken Verifier = '" + authCode + "'";
							cmd = connection.CreateCommand();
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = query;
							rdr = cmd.ExecuteReader();
							if (rdr != null)
							{
								rdr.Read();
								OAuth_AccessToken = rdr[0].ToString();
								OAuth_RefreshToken = rdr[1].ToString();
								expiresIn = rdr[2].ToString();
								rdr.Close();
							}
							connection.Close();
							retval = "Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'";
						}
						else
						{
							OAuth_AccessToken = null;
							OAuth_RefreshToken = null;
							retval = String.Empty;
						}
					});

					return retval;
				}
			}
			catch (Exception ex)
			{
                Log.Exception(ex);
                return String.Empty;
			}
		}

		private string GetOAuthTokenGoogleSheets(string connectString)
		{
			string expiresIn = "";
			string retval = "";

			try
			{
				connectString += ";Other='PromptMode=prompt;InitiateOAuth=OFF'";
				//string connectString = "Driver={Datawatch ODBC Driver for GoogleAdWords};RTK=445A44434141535544424141454E545039544846313931310000000000000000000000000000000055464B58524D324E0000524D505443384D34545A35310000;Other='PromptMode=prompt;InitiateOAuth=OFF';Developer Token=FQroiW_FyKEzW7vFMLYqGA;Client Customer ID=838-223-7823;DEB=1";
				using (DbConnection connection = Executor.GetConnection(connectString))
				{
					connection.Open();
					string query = "EXECUTE GetOAuthAuthorizationURL";
					DbCommand cmd = connection.CreateCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = query;

					DbDataReader rdr = cmd.ExecuteReader();
					rdr.Read();
					Application.Current.Dispatcher.Invoke((Action)delegate {
						var dialog = new OAuthLoginForm(rdr[0].ToString());
						dialog.Height = 600;
						dialog.Width = 900;
						rdr.Close();
						if (dialog.ShowDialog() == true)
						{
							string authCode = dialog.AuthorizationCode;
							query = "EXECUTE GetOAuthAccessToken Verifier = '" + authCode + "'";
							cmd = connection.CreateCommand();
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = query;
							rdr = cmd.ExecuteReader();
							if (rdr != null)
							{
								rdr.Read();
								OAuth_AccessToken = rdr[0].ToString();
								OAuth_RefreshToken = rdr[1].ToString();
								expiresIn = rdr[2].ToString();
								rdr.Close();
							}
							connection.Close();
							retval = "Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'";
						}
						else
						{
							OAuth_AccessToken = null;
							OAuth_RefreshToken = null;
							retval = String.Empty;
						}
					});

					return retval;
				}
			}
			catch (Exception ex)
			{
                Log.Exception(ex);
                return String.Empty;
			}
		}

		private string GetOAuthTokenHubspot(string connectString)
		{
			string expiresIn = "";
			string retval = "";

			try
			{
				connectString += ";Other='PromptMode=prompt;InitiateOAuth=OFF'";
				using (DbConnection connection = Executor.GetConnection(connectString))
				{
					connection.Open();
					string query = "EXECUTE GetOAuthAuthorizationURL CallbackUrl = 'https://localhost',RequiredScopes = 'contacts';";
					DbCommand cmd = connection.CreateCommand();
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.CommandText = query;

					DbDataReader rdr = cmd.ExecuteReader();
					rdr.Read();
					Application.Current.Dispatcher.Invoke((Action)delegate {
						var dialog = new OAuthLoginForm(rdr[0].ToString() + "&response_type=code");

						dialog.Height = 600;
						dialog.Width = 900;
						rdr.Close();
						if (dialog.ShowDialog() == true)
						{
							string authCode = dialog.AuthorizationCode;
							query = "EXECUTE GetOAuthAccessToken Verifier = '" + authCode + "', AuthMode = 'WEB',RequiredScopes = 'Contacts', CallbackUrl='https://localhost';";
							cmd = connection.CreateCommand();
							cmd.CommandType = CommandType.StoredProcedure;
							cmd.CommandText = query;
							rdr = cmd.ExecuteReader();
							if (rdr != null)
							{
								rdr.Read();
								OAuth_AccessToken = rdr[0].ToString();
								OAuth_RefreshToken = rdr[1].ToString();
								expiresIn = rdr[2].ToString();
								rdr.Close();
							}
							connection.Close();
							retval = "Other='InitiateOAuth=REFRESH;OAuth Access Token=" + OAuth_AccessToken + ";OAuth Refresh Token=" + OAuth_RefreshToken + "'";
						}
						else
						{
							OAuth_AccessToken = null;
							OAuth_RefreshToken = null;
							retval = String.Empty;
						}
					});

					return retval;
				}
			}
			catch (Exception ex)
			{
                Log.Exception(ex);
                return String.Empty;
			}
		}

		private string Encode(string s)
		{
			s.Replace(":", "%3A").Replace("/", "%2F").Replace("&", "%3F").Replace("=", "%3D");
			return s;
		}
			private string GetRTK(string DriverName)
		{
			const int keySize = 256;
			const int blockSize = 128;
			const int iterations = 1000;

			string RTK = "";
			string result = "";

			try
			{
				Assembly CRTKM = Assembly.LoadFile(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonProgramFiles), @"Datawatch\CData" , @"CRTKM.dll"));
				var typeRTK = CRTKM.GetType("CRTKM.RTK");
				dynamic c = Activator.CreateInstance(typeRTK);

				var typeAES = CRTKM.GetType("CRTKM.AES");
				dynamic d = Activator.CreateInstance(typeAES);

				String CallerID = (String)typeAES.InvokeMember("Encrypt", BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public, null, d, new object[] { "Authorized " + DateTime.Now.ToString("h:mm:ss tt"), "/+OwM6rTxmHyQ43g5qH2qxXjqWd0o5jlPwbnlRXnKW4=", keySize, blockSize, iterations });
				RTK = c.GetRTK(DriverName, String.Empty, CallerID, false);
				if (RTK.Length > 0)
					result = (String)typeAES.InvokeMember("Decrypt", BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.Public, null, d, new object[] { RTK, "G4GyWIHzzwRXXH84LdVWxQ==", keySize, blockSize, iterations });
				else
					result = String.Empty;
			}
			catch (Exception ex) 
			{
                Log.Exception(ex);
            }
            return result;
		}

		public static bool GetIsOnDemand(PropertyBag bag)
        {
            bool isOnDemand = false;
            string value = bag.Values["IsOnDemand"];
            if (!string.IsNullOrEmpty(value))
            {
                isOnDemand = bool.Parse(value);
            }
            return isOnDemand;
        }

        private void ResetErrorMsg()
        {
            querySettings.ErrorMessage = string.Empty;
        }

        public virtual SchemaAndTable[] GetTablesAndViews(string connectionString)
        {
            List<SchemaAndTable> list = new List<SchemaAndTable>();
            var tables = new DataTable();
            var views = new DataTable();

            if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
            {
                Mouse.OverrideCursor = Cursors.Wait;
                Application.Current.Dispatcher.Invoke(new Action(ResetErrorMsg), DispatcherPriority.Background);
            }

            using (DbConnection connection = Executor.GetConnection(connectionString))
            {
                connection.Open();
                if (SchemaRestrictions != null)
                {
                    tables = connection.GetSchema("Tables", SchemaRestrictions);
                    views = connection.GetSchema("Views", SchemaRestrictions);
                }
                else
                {
                    tables = connection.GetSchema("Tables");
                    views = connection.GetSchema("Views");
                }

                connection.Close();
            }
            list.AddRange(GetSchemaAndTable(tables));
            list.AddRange(GetSchemaAndTable(views));

            if (DriverName.ToUpper().Contains("BOX"))
            {
                if (String.IsNullOrEmpty(OAuth_AccessTokenSession) && String.IsNullOrEmpty(OAuth_RefreshTokenSession) && String.IsNullOrEmpty(OAuth_TokenExpirySession) && String.IsNullOrEmpty(OAuth_TokenTimestampSession))
                {
                    GetSessionTokens(connectionString);
                }
            }

            list.Sort();
            return list.ToArray();
        }

        public virtual List<SchemaAndTable> GetSchemaAndTable(DataTable result)
        {
            List<SchemaAndTable> list = new List<SchemaAndTable>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                DataRow dataRow = result.Rows[i];
                string table = dataRow["TABLE_NAME"].ToString();
                string schema = dataRow["TABLE_SCHEM"].ToString();

                if (FilterSchema(schema) || FilterTable(table))
                {
                    continue;
                }
                if (schema != null && schema.Length == 0)
                {
                    schema = null;
                }

                if (IsCDataDriver)
                    schema = null;

                list.Add(new SchemaAndTable(schema, table));
            }

            return list;
        }

        private void SelectedColumns_CollectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            if (e == null)
            {
                return;
            }
            if (e.OldItems != null)
            {
                foreach (INotifyPropertyChanged item in e.OldItems)
                {
                    item.PropertyChanged -= SelectedColumn_PropertyChanged;
                }
            }
            if (e.NewItems != null)
            {
                foreach (INotifyPropertyChanged item in e.NewItems)
                {
                    item.PropertyChanged += SelectedColumn_PropertyChanged;
                }
            }
           
            UpdateQuery();
        }

        public void SelectAllColumns()
        {
            QuerySettings.SelectedColumns.Clear();

            foreach (DatabaseColumn column in columns)
            {
                QuerySettings.SelectedColumns.Add(new DatabaseColumn(column));
            }
        }

        private void SelectedColumn_PropertyChanged(object sender,
           PropertyChangedEventArgs e)
        {
            UpdateQuery();
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag bag = base.ToPropertyBag();

            bag.Merge(querySettings.ToPropertyBag());
            //save selected table into bag.
            SetInternal("Table", querySettings.Table);

            return bag;
        }

        private void UpdateQuery()
        {
            if (querySettings.QueryMode == QueryMode.Table)
            {                
                if (!string.IsNullOrEmpty(querySettings.Table) && querySettings.SelectedColumns.Count > 0)
                {
                    querySettings.Query = Executor.GenerateQuery(querySettings,
                        EscapedParameters);
                }
            }
        }
    }
}
