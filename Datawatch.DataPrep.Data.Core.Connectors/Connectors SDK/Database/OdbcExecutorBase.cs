﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using System.Data.SqlClient;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public class OdbcExecutorBase : DatabaseExecutorBase
    {
        private static HashSet<string> unlockedDrivers = new HashSet<string>();
        // These queries contains prefix and passwords created in Progress partlink site.
        // D9 prefix is used in both 64 and 32 bit branding.
        private const string unlockQuery32Bit = "{IVD9.LIC,1Dr67H_e6YgV!8u#675lV9S2}";
        private const string unlockQuery64Bit = "{DDD9.LIC,1Dr67H_e6YgV!8u#675lV9S2}";

        public override DbDataAdapter GetAdapter(string queryString,
            string connectionString)
        {
            OdbcDataAdapter adapter =
                new OdbcDataAdapter(queryString, connectionString);
            DoUnlock(adapter.SelectCommand.Connection);
            return adapter;
        }

        private OdbcConnectionStringBuilder CheckConnectionString(string connectionString)
        {
            try
            {
                System.Data.Odbc.OdbcConnectionStringBuilder dbBuilder =
                    new OdbcConnectionStringBuilder();
                dbBuilder.ConnectionString = connectionString;
                string dbvalue = "";
                foreach (string key in dbBuilder.Keys)
                    dbvalue = key + "=" + dbBuilder[key].ToString();
                return dbBuilder;
            }
            catch (System.Collections.Generic.KeyNotFoundException ex)
            {
                return null;
            }
            catch (System.FormatException ex)
            {
                return null;
            }
            catch (System.Exception e)
            {
                return null;
            }
        }

        public override DbConnection GetConnection(string connectionString)
        {
            System.Data.Odbc.OdbcConnectionStringBuilder dbBuilderString =
                       new OdbcConnectionStringBuilder();
            dbBuilderString = CheckConnectionString(connectionString);
            string conn = dbBuilderString.ConnectionString;
            OdbcConnection connection = new OdbcConnection(dbBuilderString.ToString());
            DoUnlock(connection);
            return connection;
        }

        protected override Dictionary<string, string> RenameTimeBucketingAliases(
            OnDemandParameters onDemandParameters)
        {
            return DatabaseUtils.RenameTimeBucketingAliases(onDemandParameters);
        }

        private void DoUnlock(OdbcConnection connection)
        {
            DbConnectionStringBuilder csb = new DbConnectionStringBuilder();
            csb.ConnectionString = connection.ConnectionString;
            string driver = null;
            if (csb.ContainsKey("driver"))
            {
                driver = csb["driver"].ToString();
            }
            if (!string.IsNullOrEmpty(driver))
            {
                if (unlockedDrivers.Contains(driver)) return;
            }

            OdbcCommand cmd =
                new OdbcCommand(Environment.Is64BitProcess ?
                    unlockQuery64Bit : unlockQuery32Bit,
                    connection);
            bool doCloseConnection = false;

            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                    doCloseConnection = true;
                }
                //first thing to do is to execuete the unlock
                cmd.ExecuteNonQuery();
            }
            catch (OdbcException exception)
            {
                // TODO: This error handling needs to be optimised.
                // It is so far based on suggestion from Progress
                // And should be fixed when they correct some API behaviour.
                for (int i = 0; i < exception.Errors.Count; i++)
                {
                    if (exception.Errors[i].NativeError.Equals(6082) &&
                        exception.Errors[i].Message.Contains(
                        "successfully unlocked"))
                    {
                        //Eureka
                    }
                }
            }
            if (doCloseConnection)
            {
                connection.Close();
            }

            unlockedDrivers.Add(driver);
        }
    }
}
