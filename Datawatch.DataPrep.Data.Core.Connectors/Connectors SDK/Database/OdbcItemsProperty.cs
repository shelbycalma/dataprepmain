﻿namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public class OdbcItemsProperty : OdbcProperty
    {

        private string[] items;

        public OdbcItemsProperty(string name, object value, string[] items)
            :this(name, value, null, items)
        {            
        }

        public OdbcItemsProperty(string name, object value, string displayName,
             string[] items)
            :base(name, value, displayName)
        {
            this.items = items;
        }

        public string[] Items
        {
            get { return items; }
        }
    }
}
