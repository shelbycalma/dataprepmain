﻿using System;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{ 
    [AttributeUsage(AttributeTargets.Property)]
    public class OdbcAdvancedSettingsAttribute : Attribute
    {
        private readonly string displayName;
        private readonly string[] items;

        public OdbcAdvancedSettingsAttribute(string displayName)
            :this(displayName, null)
        {
        }

        public OdbcAdvancedSettingsAttribute(string displayName,
            string[] items)
        {
            this.displayName = displayName;
            this.items = items;
        }

        public string DisplayName
        {
            get { return displayName; }
        }

        public string[] Items
        {
            get { return items; }
        }
    }
}
