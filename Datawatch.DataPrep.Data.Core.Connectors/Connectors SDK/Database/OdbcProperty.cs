﻿using System;
using System.ComponentModel;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public class OdbcProperty : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        private object value;
        private string displayName;

        public OdbcProperty(string name, object value)            
            :this(name, value, null)
        {
        }

        public OdbcProperty(string name, object value, string displayName)
        {
            this.name = name;
            this.value = value;
            this.displayName = displayName;
        }

        public string Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    string oldName = name;
                    name = value;
                    FirePropertyChanged("Name");
                }
            }
        }

        public object Value
        {
            get { return value; }
            set
            {
                if (this.value != value)
                {
                    this.value = value;
                    FirePropertyChanged("Value");
                }
            }
        }

        public string DisplayName
        {
            get { return displayName; }
            set
            {
                if (this.displayName != value)
                {
                    this.displayName = value;
                    FirePropertyChanged("DisplayName");
                }
            }
        }

        public Type ValueType
        {
            get
            {
                return value == null ? null : value.GetType();
            }
        }
        protected void FirePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }


        public override int GetHashCode()
        {
            int hashCode = 17;
            if (!string.IsNullOrEmpty(name))
            {
                hashCode = hashCode * 23 + name.GetHashCode();
            }
            if (value != null)
            {
                hashCode = hashCode * 23 + value.GetHashCode();
            }
            if (!string.IsNullOrEmpty(displayName))
            {
                hashCode = hashCode * 23 + displayName.GetHashCode();
            }
            return hashCode;
        }

        public override bool Equals(object obj)
        {
            if (base.Equals(obj)) return true;
            if (!(obj is OdbcProperty)) return false;

            OdbcProperty pv = (OdbcProperty)obj;

            return Utils.IsEqual(name, pv.Name)
                && Utils.IsEqual(value, pv.Value)
                && Utils.IsEqual(displayName, pv.DisplayName);
        }

    }
}
