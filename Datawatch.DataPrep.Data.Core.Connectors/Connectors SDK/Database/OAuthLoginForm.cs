﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
//using Datawatch.DataPrep.UX.Controls;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
//using WebBrowser = System.Windows.Controls.WebBrowser;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
	/// <summary>
	/// Interaction logic for OAuthLogin.xaml
	/// </summary>
	public partial class OAuthLoginForm : Window
    {
        public string AuthorizationCode { get; set; }
		private string Url { get; set; }
		public System.Windows.Forms.WebBrowser WebBrowser { get; private set; }

		public OAuthLoginForm(string url)
        {
			//InitializeComponent();
			Url = url;
            Loaded += OAuthLogin_Loaded;
            //Closed += OAuthLogin_Closed;
            //BrowserView.AuthorizationCodeReceived = OnAuthorizationCodeReceived;
            //BrowserView.DocumentComplete = DocumentComplete;
        }

        private void OnAuthorizationCodeReceived(string code)
        {
            AuthorizationCode = code;
            DialogResult = !string.IsNullOrWhiteSpace(code);
            Close();
        }

        void OAuthLogin_Loaded(object sender, RoutedEventArgs e)
        {
			WebBrowser = new System.Windows.Forms.WebBrowser();
            WebBrowser.ScriptErrorsSuppressed = true;
			WebBrowser.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(webBrowser1_DocumentCompleted);
			WebBrowser.Navigate(Url);
			Grid grid = new Grid();
			var stackPanel = new StackPanel { Orientation = System.Windows.Controls.Orientation.Vertical };
			var formsHost = new WindowsFormsHost { Child = WebBrowser, Height = 550, Width = 850 };
			stackPanel.Children.Add(formsHost);
			grid.Children.Add(stackPanel);
			Content = grid;
		}

		private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
		{
            // Use this way to decide if the whole page document has been completely downloaded.
            if (e.Url == WebBrowser.Document.Url)
            {
                if (WebBrowser.ReadyState == WebBrowserReadyState.Complete)
                {
                    if (WebBrowser.DocumentTitle.StartsWith("Success"))
                    {
                        WebBrowser.Visible = false;
                        string codeStr = "code=";
                        string s = WebBrowser.DocumentTitle;
                        string code = s.Substring(s.IndexOf(codeStr) + codeStr.Length);
                        OnAuthorizationCodeReceived(code);
                    }
                }
            }
            else if (e.Url.Query.StartsWith("?code"))
            {
                if (WebBrowser.ReadyState == WebBrowserReadyState.Complete)
                {
                    WebBrowser.Visible = false;
                    string codeStr = "?code=";
                    string s = e.Url.Query;
                    string code = s.Substring(s.IndexOf(codeStr) + codeStr.Length);
                    OnAuthorizationCodeReceived(code);
                }
            }
            else if (e.Url.Query.StartsWith("?error=access_denied"))
            {
                WebBrowser.Visible = false;
                OnAuthorizationCodeReceived(null);
            }

        }

	}
}
