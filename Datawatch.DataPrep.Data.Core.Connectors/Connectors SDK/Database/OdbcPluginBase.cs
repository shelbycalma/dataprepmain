﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public abstract class OdbcPluginBase<C> :
        DataPluginBase<C>, IOnDemandPlugin
        where C : DatabaseSettingsBase
    {

        private bool disposed = true;

        public OdbcPluginBase()
        {           
            if (!OdbcDriversHelper.IsDriverInstalled(DriverName))
            {
                throw new Exception(Properties.Resources.ExODBCDriverNotInstalled);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposed)
            {
                if (disposing)
                {

                }
                disposed = true;
            }
        }

        public abstract C GetSettings(PropertyBag properties,
            IEnumerable<ParameterValue> parameters);

        public abstract string DriverName { get; }

        public override C CreateSettings(
            PropertyBag properties)
        {
            C settings = GetSettings(properties, null);
			if (settings.QuerySettings.QueryMode == QueryMode.Table &&
				settings.QuerySettings.SelectedTable != null)
			{
				settings.Title =
					settings.QuerySettings.SelectedTable.ToString();
			}
			else if (settings.QuerySettings.QueryMode == QueryMode.Report &&
				settings.QuerySettings.SelectedReportName != null)
			{
				settings.Title =
					settings.QuerySettings.SelectedReportName.ToString();
			}
			else if (settings.QuerySettings.QueryMode == QueryMode.Query &&
				settings.QuerySettings.Query != null)
			{
				string[] queryArr = Regex.Replace(settings.QuerySettings.Query, @"\s+", " ").Split(' ');
				if (queryArr.Length >= 4)
				{
					for (int i = 0; i < queryArr.Length; i++)
					{
						if (queryArr[i].ToUpper() == "FROM")
						{
							string q = queryArr[i + 1];
							if (q.StartsWith("\"") && !q.EndsWith("\""))
							{
								int j = i + 2;
								while (j < queryArr.Length)
								{
									q += " " + queryArr[j];
									if (queryArr[j].EndsWith("\""))
									{
										settings.Title = q;
										break;
									}
									else
									{
										j++;
									}
								}
							}
							else
							{
								settings.Title = queryArr[i + 1];
								break;
							}
						}
					}
				}
			}
			else
			{
				settings.Title = settings.DatabaseName;
			}

            if (string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = settings.PluginTitle;
            }
            return settings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            CheckLicense();

            C connectionSettings = GetSettings(properties,
                parameters);
            if (connectionSettings.QuerySettings != null)
            {
                connectionSettings.QuerySettings.ApplyRowFilteration =
                    applyRowFilteration;
            }
            return connectionSettings.Executor.GetData(
                connectionSettings.QuerySettings, parameters, rowcount);
        }

        public ITable GetOnDemandData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            CheckLicense();

            C connectionSettings = GetSettings(settings,
                parameters);

            return connectionSettings.Executor.GetOnDemandData(
                connectionSettings.QuerySettings, parameters,
                onDemandParameters, rowcount);
        }

        public virtual bool IsOnDemand(PropertyBag settings)
        {
            return DatabaseSettingsBase.GetIsOnDemand(settings);
        }
    }
}