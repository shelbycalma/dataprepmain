﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public abstract class OdbcPluginUIBase<T, C> : PluginUIBase<T, C>
        where C : DatabaseSettingsBase
        where T : OdbcPluginBase<C>
    {
        public OdbcPluginUIBase(T plugin, Window owner) : base(plugin, owner)
        {
        }

        public abstract object GetConfigPanel(C settings,
            IEnumerable<ParameterValue> parameters);

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            C settings = Plugin.GetSettings(bag, parameters);
            settings.Parameters = parameters;
            settings.QuerySettings.PluginHostSettings =
                new PluginHostSettingsViewModel(Plugin.GlobalSettings);

            return GetConfigPanel(settings, parameters);
        }

        public override PropertyBag GetSetting(object element)
        {
            return ToPropertyBag(element);
        }

        public abstract PropertyBag ToPropertyBag(object element);

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            C settings =Plugin.GetSettings(new PropertyBag(),
                parameters);
            settings.QuerySettings.PluginHostSettings =
                new PluginHostSettingsViewModel(Plugin.GlobalSettings);

            object configPanel = GetConfigPanel(settings, parameters);
            Window window =
                CreateConnectionWindow(settings, configPanel);


            return DataPluginUtils.ShowDialog(owner, window, settings);
        }

        public virtual Window CreateConnectionWindow(C settings, object configPanel)
        {
            OdbcConnectionWindow window =
                new OdbcConnectionWindow(settings, configPanel);
            return window;
        }
    }
}
