﻿using System.Windows;
using System.Windows.Controls;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public partial class DatabaseConfigPanel : UserControl
    {
        private DatabaseSettingsBase settings;
        
        public DatabaseConfigPanel()
            : this(null)
        {
        }

        public DatabaseConfigPanel(DatabaseSettingsBase settings)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }
        
        public DatabaseSettingsBase Settings
        {
            get { return settings; }
        }

        private void UserControl_DataContextChanged(object sender, 
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as DatabaseSettingsBase;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
