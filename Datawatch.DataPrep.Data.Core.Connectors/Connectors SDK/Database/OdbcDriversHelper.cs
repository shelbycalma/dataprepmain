﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public static class OdbcDriversHelper
    {
        private static readonly List<string> drivers = GetOdbcDriverNames();

        private static List<string> GetOdbcDriverNames()
        {
            List<string> odbcDriverNames = null;
            using (RegistryKey localMachineHive = Registry.LocalMachine)
            using (RegistryKey odbcDriversKey = localMachineHive
                .OpenSubKey(@"SOFTWARE\ODBC\ODBCINST.INI\ODBC Drivers"))
            {
                if (odbcDriversKey != null)
                {
                    odbcDriverNames = odbcDriversKey.GetValueNames()
                        .Select(e => e.ToUpper())
                        .ToList();
                }
            }

            return odbcDriverNames;
        }

        static OdbcDriversHelper()
        {
        }

        public static bool IsDriverInstalled(string driverName)
        {
            return drivers != null && drivers.Contains(driverName.ToUpper());
        }
    }
}