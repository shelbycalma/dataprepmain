﻿using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public partial class OdbcConnectionWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(OdbcConnectionWindow));

        private DatabaseSettingsBase settings;
        private FrameworkElement pluginConfigElement;

        public OdbcConnectionWindow(DatabaseSettingsBase settings,
            object pluginConfigElement)            
        {
            DataContext = this;
            this.settings = settings;
            this.pluginConfigElement = (FrameworkElement)pluginConfigElement;
            DataHelpKeyAttribute attribute = DataHelpProvider
                .GetHelpAttribute(this.pluginConfigElement.DataContext);
            if (attribute != null)
            {
                DataHelpProvider.SetHelpKey(this, attribute.Key);
            }

            InitializeComponent();
        }
        
        public DatabaseSettingsBase Settings
        {
            get { return settings; }
        }

        public FrameworkElement PluginConfigElement
        {
            get { return pluginConfigElement; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (PluginConfigElement is IDataPluginConfigElement)
            {
                e.CanExecute = ((IDataPluginConfigElement)PluginConfigElement).IsOk;
                return;
            }
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (PluginConfigElement is IDataPluginConfigElement)
            {
                ((IDataPluginConfigElement)PluginConfigElement).OnOk();
            }
            DialogResult = true;
        }
    }
}
