﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.Odbc;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Microsoft.Win32;
using System.Security;
using System.Runtime.ExceptionServices;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core.Reports;
using System.Collections.Generic;
using System.Linq;
using System.Data.Common;
using System.Windows.Threading;

namespace Datawatch.DataPrep.Data.Core.Connectors.Database
{
    public partial class DatabaseQueryPanel : UserControl, INotifyPropertyChanged
    {
		public static RoutedCommand PreviewCommand = new RoutedCommand("Preview", typeof(DatabaseQueryPanel));
        public static RoutedCommand LoadReportsCommand = new RoutedCommand("LoadReports", typeof(DatabaseQueryPanel));

        public event PropertyChangedEventHandler PropertyChanged;
		private DatabaseExecutorBase executor = new OdbcExecutorBase();

		public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(DatabaseSettingsBase), typeof(DatabaseQueryPanel),
                new PropertyMetadata(new PropertyChangedCallback(
                    Settings_Changed)));

        public DatabaseQueryPanel(DatabaseSettingsBase settings)
            : this()
        {
            Settings = settings;
            DataContext = this;
        }

        public DatabaseQueryPanel()
        {
            InitializeComponent();
        }

        public DatabaseSettingsBase Settings
        {
            get { return (DatabaseSettingsBase)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((DatabaseQueryPanel)obj).OnSettingsChanged(
                (DatabaseSettingsBase)args.OldValue,
                (DatabaseSettingsBase)args.NewValue);
        }

		public DatabaseExecutorBase Executor
		{
			get { return executor; }

		}

		private void OnSettingsChanged(DatabaseSettingsBase oldSettings,
            DatabaseSettingsBase newSettings)
        {

            if (oldSettings != null)
            {
                newSettings.PropertyChanged -=
                    new PropertyChangedEventHandler(
                        Settings_PropertyChanged);
            }

            FireChanged("Tables");
            FireChanged("Columns");

            if (newSettings != null)
            {
                newSettings.PropertyChanged +=
                    new PropertyChangedEventHandler(
                        Settings_PropertyChanged);

                string loadCaption = Properties.Resources.UiLoadTablesLabel;
                switch (Settings.PluginTitle.Replace("CData", "").Trim().ToUpper())
                {
                    case "GOOGLE SHEETS":
                        loadCaption = Properties.Resources.UiLoadTablesGoogleSheetsLabel;
                        break;
                    case "GOOGLE ADWORDS":
                    case "GOOGLE BIGQUERY":
                    case "HUBSPOT":
                    case "GOOGLE DRIVE":
                        loadCaption = Properties.Resources.UiLoadTablesOthersLabel;
                        break;
                }
                Settings.QuerySettings.LoadTablesCaption = loadCaption;

				if (newSettings.QuerySettings.QueryMode == QueryMode.Report)
				{
					populateReportGroupList();
					string selectedReportID = newSettings.QuerySettings.SelectedReportID;
					PopulateReportList(newSettings.QuerySettings.SelectedReportGroup);
					ReportGroupList.SelectedValue = newSettings.QuerySettings.SelectedReportGroup;
					ReportList.SelectedValue = newSettings.QuerySettings.ReportList.FirstOrDefault(x => x.ReportID == selectedReportID);
				}

				QueryMode selectedTab = newSettings.QuerySettings.QueryMode;
				string queryStmt = newSettings.QuerySettings.Query;
				if (selectedTab == QueryMode.Table)
					TableTab.IsSelected = true;
				else if (selectedTab == QueryMode.Query)
				{
					QueryTab.IsSelected = true;
					newSettings.QuerySettings.Query = queryStmt;
				}
				else if (selectedTab == QueryMode.Report)
				{
					ReportTab.IsSelected = true;
					newSettings.QuerySettings.Query = queryStmt;
				}

				//else if (newSettings.QuerySettings.QueryMode == QueryMode.Query)
				//{
				//	QueryMode selectedTab = newSettings.QuerySettings.QueryMode;
				//	string queryStmt = newSettings.QuerySettings.Query;
				//	if (selectedTab == QueryMode.Table)
				//		TableTab.IsSelected = true;
				//	else if (selectedTab == QueryMode.Query)
				//	{
				//		QueryTab.IsSelected = true;
				//		newSettings.QuerySettings.Query = queryStmt;
				//	}
				//}
				//else if (newSettings.QuerySettings.QueryMode == QueryMode.Table)
				//{
				//	QueryMode selectedTab = newSettings.QuerySettings.QueryMode;
				//	string queryStmt = newSettings.QuerySettings.Query;
				//	if (selectedTab == QueryMode.Table)
				//		TableTab.IsSelected = true;
				//	else if (selectedTab == QueryMode.Query)
				//	{
				//		QueryTab.IsSelected = true;
				//		newSettings.QuerySettings.Query = queryStmt;
				//	}
				//}
			}
		}

        private void Settings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "QueryMode")
            {
                if (Settings.IsOk)
                {
                    Settings.QueryBuilderViewModel.LoadTables();
                }
            }
            if (e.PropertyName == "DatabaseName")
            {
                //tables = new ObservableCollection<SchemaAndTable>();
                FireChanged("Tables");
            }
        }

        protected void FireChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Settings != null)
            {
                if (TableTab.IsSelected)
                    Settings.QuerySettings.QueryMode = QueryMode.Table;
                else if (QueryTab.IsSelected)
                    Settings.QuerySettings.QueryMode = QueryMode.Query;
                else if (ReportTab.IsSelected)
                    Settings.QuerySettings.QueryMode = QueryMode.Report;
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
		private void PreviewData_OnClick(object sender, RoutedEventArgs e)
        {
			try
			{
                Settings.QuerySettings.ErrorMessage = String.Empty;
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new Action(delegate { this.UpdateLayout(); }));
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                if (Settings.QuerySettings.SelectedColumns.Count == 0 && Settings.QuerySettings.QueryMode == QueryMode.Table)
                {
					MessageBox.Show(Properties.Resources.UiErrorNoColumnsSelected, Settings.Title);
				}
				else
				{
					int previewRowLimit;
					RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

					if (previewRowLimit == 0)
					{
						previewRowLimit = 10;
					}

					DataTable dt = new DataTable();
					DbConnection connect = Executor.GetConnection(Settings.QuerySettings.ConnectionString);
					connect.Open();

                    //added this since GetConnection call might reset the mouse cursor to null
					if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
					{
						Mouse.OverrideCursor = Cursors.Wait;
					}

					string query = Regex.Replace(Settings.QuerySettings.Query, @"\s+", " ");
					if (Settings.QuerySettings.QueryMode == QueryMode.Table || query.ToUpper().StartsWith("SELECT "))
					{
						switch (Settings.PluginTitle.Replace("CData", "").Trim().ToUpper())
						{
							case "SYBASE IQ":
							case "AMAZON REDSHIFT":
							case "SQL SERVER":
							case "ODATA":
							case "SALESFORCE":
							case "SHAREPOINT":
							case "ACCESS":
							case "CASSANDRA":
							case "SUGARCRM":
                            case "ZENDESK":
                                if ((!query.ToUpper().Contains("SELECT TOP")) && (!query.ToUpper().Contains("EXECUTE ")))
									query =  "SELECT TOP " + previewRowLimit.ToString() + " " + query.Substring(6);
								break;
							case "ORACLE":
								if (!query.ToUpper().Contains("ROWNUM"))
									query = "SELECT * FROM (" + query + ") WHERE ROWNUM <= " + previewRowLimit.ToString();
								break;
							case "ACTIVE DIRECTORY":
							case "BOX CLOUD STORAGE":
							case "CLOUDERA IMPALA":
							case "POSTGRESQL":
							case "MYSQL":
							case "NETSUITE":
							case "HUBSPOT":
							case "GOOGLE ADWORDS":
							case "GOOGLE BIGQUERY":
							case "GOOGLE DRIVE":
							case "GOOGLE SHEETS":
							case "ORACLE SALES CLOUD":
							case "VERTICA":
							case "HADOOP HIVE":
							case "AMAZON EMR HIVE":
							case "CLOUDERA CDH HIVE":
							case "HORTONWORKS HIVE":
							case "IBM BIGINSIGHTS":
							case "MAPR HIVE":
							case "PIVOTAL HD HIVE":
							case "SPARK SQL":
							case "JIRA":
                                if (!query.ToUpper().Contains(" LIMIT "))
									query = query + " LIMIT " + previewRowLimit.ToString();
								break;
							case "INFORMIX":
								if (!query.ToUpper().Contains("SELECT FIRST"))
									query = "SELECT FIRST " + previewRowLimit.ToString() + " " + query.Substring(6);
								break;
							case "DB2":
								if (!query.ToUpper().Contains("FETCH FIRST"))
									query = query + " FETCH FIRST " + previewRowLimit.ToString() + " ROWS ONLY";
								break;
						}
					}
					
					DbCommand cmd = connect.CreateCommand();
					cmd.CommandType = CommandType.Text;
					cmd.CommandText = query;

					DbDataReader dr = cmd.ExecuteReader();
					DataTable dtlimit = new DataTable();

					using (DataSet ds = new DataSet() { EnforceConstraints = false })
					{
						ds.Tables.Add(dt);
						dt.Load(dr);
						ds.Tables.Remove(dt);
					}

					connect.Close();
					dt = ConvertByteToStringRows(dt);
					if (dt.Rows.Count > 0)
						dtlimit = dt.AsEnumerable().Take(previewRowLimit).CopyToDataTable();
					else
						dtlimit = dt;
					PreviewGrid.ItemsSource = dtlimit.AsDataView();
                }
            }
			catch (Exception ex)
			{
                string errorMsg = "";
                errorMsg = ex.Message;

                if (Settings.PluginTitle.Replace("CData", "").Trim().ToUpper() == "SALESFORCE")
                {
                    if (errorMsg == "Object reference not set to an instance of an object.")
                        errorMsg = Properties.Resources.SalesforceConnectionObjectReferenceError;
                    if (errorMsg == "ERROR [HY000] HTTP protocol error. 400 Bad Request.")
                        errorMsg = Properties.Resources.SalesforceConnectionProtocolError;
                }
                else if (Settings.PluginTitle.Replace("CData", "").Trim().ToUpper() == "BOX CLOUD STORAGE")
                {
                    if (errorMsg.StartsWith("ERROR [HY000] When querying this table you have to specify"))
                        errorMsg = errorMsg + " " + Properties.Resources.BoxConnectionSpecifyFilter;
                }
                else if (Settings.PluginTitle.Replace("CData", "").Trim().ToUpper() == "JIRA")
                {
                    if (errorMsg.StartsWith("ERROR [HY000] At least"))
                        errorMsg = errorMsg + " " + Properties.Resources.BoxConnectionSpecifyFilter;
                    else if (errorMsg.StartsWith("ERROR [HY000] HTTP protocol error"))
                        errorMsg = Properties.Resources.JiraConnectionNoPermissions;
                }

                Log.Exception(ex);
                Settings.QuerySettings.ErrorMessage = errorMsg;
                //MessageBox.Show(errorMsg, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
        }

		private DataTable ConvertByteToStringRows(DataTable dt)
		{
			List<DataColumn> colCollRem = new List<DataColumn>();
			List<DataColumn> colCollAdd = new List<DataColumn>();
			foreach (DataColumn col in dt.Columns)
				if (col.DataType == typeof(byte[]))
					colCollRem.Add(col);

			// Remove old add new.
			foreach (DataColumn col in colCollRem)
			{
				int tmpOrd = col.Ordinal;
				string colName = String.Format("{0}(Hex)", col.ColumnName);
				DataColumn tmpCol = new DataColumn(colName, typeof(String));
				dt.Columns.Add(tmpCol);
				colCollAdd.Add(tmpCol);
				foreach (DataRow row in dt.Rows)
				{
					if (row[col].ToString().Length > 0)
						row[tmpCol] = ByteArrayToHexString((byte[])row[col]);
					else
						row[tmpCol] = row[col];
				}
				dt.Columns.Remove(col);
				string colNameNew = colName.Replace("(Hex)", String.Empty);
				dt.Columns[colName].ColumnName = colNameNew;
				dt.Columns[colNameNew].SetOrdinal(tmpOrd);
			}
			return dt;
		}


        private void LoadReports_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                    populateReportGroupList();
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private void populateReportGroupList()
        {
            DataTable dt = new DataTable();
			DbConnection connect = Executor.GetConnection(Settings.QuerySettings.ConnectionString);
			connect.Open();

            string query = Settings.QuerySettings.ReportGroupListSelectQuery + Settings.QuerySettings.ReportGroupListWhereQuery + Settings.QuerySettings.ReportGroupListOrderByQuery;
            List<string> reportGroup = new List<string>();

			DbCommand cmd = connect.CreateCommand();
			cmd.CommandType = CommandType.StoredProcedure;
			cmd.CommandText = query;

			DbDataReader dr = cmd.ExecuteReader();

			using (DataSet ds = new DataSet() { EnforceConstraints = false })
            {
                ds.Tables.Add(dt);
                dt.Load(dr);
                ds.Tables.Remove(dt);
            }

            foreach (DataRow row in dt.Rows)
            {
                if (!reportGroup.Contains(row[0].ToString()))
                    reportGroup.Add(row[0].ToString());
                //Settings.QuerySettings.ReportGroups.Add(row.Field<string>(0));
            }
            Settings.QuerySettings.SetReportGroupList(reportGroup);
            ReportGroupList.ItemsSource = Settings.QuerySettings.ReportGroups;

            connect.Close();
            dt = ConvertByteToStringRows(dt);
        }

        private void reportGroupListBox_SelectionChanged(object sender,
            SelectionChangedEventArgs e)
        {
            ListBox selector = sender as ListBox;
            if (selector == null) return;
            selector.ScrollIntoView(selector.SelectedItem);
            PopulateReportList(selector.SelectedItem.ToString());
            Settings.QuerySettings.SelectedReportGroup = selector.SelectedItem.ToString();
            Settings.QuerySettings.SelectedReportID = "";
        }

        private void reportListBox_SelectionChanged(object sender,
                SelectionChangedEventArgs e)
        {
            ListBox selector = sender as ListBox;
            if (selector == null) return;
            selector.ScrollIntoView(selector.SelectedItem);
            if (selector.SelectedItem == null) return;
            Report selectedReport = (Report)selector.SelectedItem;

            Settings.QuerySettings.SelectedReport = selectedReport;
            Settings.QuerySettings.SelectedReportID = selectedReport.ReportID;
            Settings.QuerySettings.SelectedReportName = selectedReport.ReportName;
            Settings.QuerySettings.Query = Settings.QuerySettings.ReportSelectQuery + Settings.QuerySettings.ReportWhereQuery + selectedReport.ReportID + "' " + Settings.QuerySettings.ReportOrderByQuery;
        }

        private void PopulateReportList(string selectedItem)
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;

                    DataTable dt = new DataTable();
                    DbConnection connect = Executor.GetConnection(Settings.QuerySettings.ConnectionString);
                    connect.Open();

                    string query = Settings.QuerySettings.ReportListSelectQuery +  Settings.QuerySettings.ReportListWhereQuery + selectedItem.Replace("'","''") + Settings.QuerySettings.ReportListOrderByQuery;
                    List<Report> reportList = new List<Report>();

                    DbCommand cmd = connect.CreateCommand();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = query;

                    DbDataReader dr = cmd.ExecuteReader();

                    using (DataSet ds = new DataSet() { EnforceConstraints = false })
                    {
                        ds.Tables.Add(dt);
                        dt.Load(dr);
                        ds.Tables.Remove(dt);
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        Report reportItem = new Report();
                        reportItem.ReportName = row[0].ToString();
                        reportItem.ReportID = row[1].ToString();
                        reportList.Add(reportItem);
                    }
                    Settings.QuerySettings.SetReportList(reportList);
                    ReportList.ItemsSource = Settings.QuerySettings.ReportList;

                    connect.Close();
                    dt = ConvertByteToStringRows(dt);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

		private static string ByteArrayToHexString(byte[] p)
		{
			byte b;
			char[] c = new char[p.Length * 2 + 2];
//			c[0] = '0'; c[1] = 'x';
			for (int y = 0, x = 0; y < p.Length; ++y, ++x)
			{
				b = ((byte)(p[y] >> 4));
				c[x] = (char)(b > 9 ? b + 0x37 : b + 0x30);
				b = ((byte)(p[y] & 0xF));
				c[++x] = (char)(b > 9 ? b + 0x37 : b + 0x30);
			}
			return new string(c);
		}

		private void Preview_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = Settings != null && Settings.IsOk;
		}

        private Report getSelectedReport(List<Report> RList, string reportID)
        {
            Report selectedReport = new Report();
            foreach (Report r in RList)
            {
                if (r.ReportID.ToString() == reportID.ToString())
                    selectedReport = r;
            }
            return (selectedReport);
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }

    }
}

