﻿using System;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class ChunkedStringHelper
    {
        private const int TextChunkSize = 4000;

        private readonly PropertyBag bag;
        private bool textInitialized;
        private string text;

        public ChunkedStringHelper(PropertyBag bag)
        {
            if (bag == null) {
                throw Exceptions.ArgumentNull("bag");
            }
            this.bag = bag;
        }

        private void InitializeText()
        {
            int capacity = bag.Values.Count * TextChunkSize;
            StringBuilder result = new StringBuilder(capacity);
            for (int i = 0; i < bag.Values.Count; i++)
            {
                string chunk = bag.Values["" + i];
                result.Append(chunk);
            }
            text = result.ToString();

            textInitialized = true;
        }

        private void StoreText()
        {
            bag.Clear();

            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            int i = 0;
            int pos = 0;
            while (pos < text.Length)
            {
                int size = Math.Min(text.Length - pos, TextChunkSize);
                string chunk = text.Substring(pos, size);

                bag.Values["" + i] = chunk;

                pos += size;
                i++;
            }
        }

        public string Text
        {
            get
            {
                if (!textInitialized)
                {
                    InitializeText();
                }
                
                return text;
            }
            set
            {
                if (textInitialized && text == value)
                {
                    return;
                }

                text = value;
                textInitialized = true;
                StoreText();
            }
        }

        public PropertyBag ToPropertyBag()
        {
            return bag;
        }
    }
}
