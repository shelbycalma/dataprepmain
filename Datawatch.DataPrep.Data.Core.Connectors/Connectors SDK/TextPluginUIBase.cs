﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class TextPluginUIBase<TC, T> : PluginUIBase<T, TC>, IFileOpenDataPluginUI<T>
        where TC : TextSettingsBase
        where T : TextPluginBase<TC>
    {
        protected TextPluginUIBase(T plugin, Window owner, string imageUri)
            : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            TC con = Plugin.CreateConnectionSettings(parameters);
            Window win = CreateConnectionWindow(con);

            return DataPluginUtils.ShowDialog(owner, win, con);
        }

        public abstract Window CreateConnectionWindow(TC settings);

        public virtual PropertyBag DoOpenFile(string filePath)
        {
            CheckLicense();

            TC con = Plugin.CreateConnectionSettings(new ParameterValue[0]);
            con.FilePath = filePath;
            con.FilePathType = PathType.File;
            Window win = CreateConnectionWindow(con);

            return DataPluginUtils.ShowDialog(owner, win, con);
        }

        public virtual PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new NotImplementedException();
        }

        public abstract string[] FileExtensions { get; }
        public abstract string FileFilter { get; }

        public bool CanOpen(string filePath)
        {
            return DataUtils.CanOpen(filePath, FileExtensions);
        }
    }
}
