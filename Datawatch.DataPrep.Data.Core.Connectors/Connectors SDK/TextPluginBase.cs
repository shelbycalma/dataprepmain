﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class TextPluginBase<C> : DataPluginBase<C>
        where C : TextSettingsBase
    {
        // The id of the data plug-in, this string must not be changed after
        // the plug-in has been used in workbooks or the workbooks created 
        // using the plug-in will loose their data connections.
        private readonly string id;

        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        private readonly string title;

        public abstract C CreateConnectionSettings(IEnumerable<ParameterValue> parameters);
        public abstract C CreateConnectionSettings(PropertyBag bag);

        protected TextPluginBase(string id, string title)
        {
            this.id = id;
            this.title = title;
        }

        public override C CreateSettings(PropertyBag bag)
        {
            C settings = CreateConnectionSettings(bag);
            if (string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = string.Format(Properties.Resources.UiSettingsTitleFormat, Title);
            }
            return settings;
        }

        public override string DataPluginType
        {
            get { return Core.Properties.Resources.UiDataPluginTypeFile; }
        }

        public override string Id
        {
            get { return id; }
        }

        public override string Title
        {
            get { return title; }
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            return DataPluginUtils.GetDataFiles(CreateConnectionSettings(settings));
        }

        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();

            DataPluginUtils.UpdateFileLocation(CreateConnectionSettings(settings),
                oldPath, newPath, workbookPath);
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }
    }
}
