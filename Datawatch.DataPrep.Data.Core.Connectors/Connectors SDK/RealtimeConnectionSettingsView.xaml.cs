﻿using System.Windows;
using System.Windows.Controls;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    /// <summary>
    /// Interaction logic for RealtimeConnectionSettingsView.xaml
    /// </summary>
    public partial class RealtimeConnectionSettingsView : UserControl
    {
        public static readonly DependencyProperty IdColumnVisibilityProperty =
            DependencyProperty.Register(
                "IdColumnVisibility",
                typeof(Visibility),
                typeof(RealtimeConnectionSettingsView));
        
        public RealtimeConnectionSettingsView()
        {
            InitializeComponent();
        }

        public Visibility IdColumnVisibility
        {
            get { return (Visibility)GetValue(IdColumnVisibilityProperty); }
            set { SetValue(IdColumnVisibilityProperty, value); }
        }

        public int LabelMinWidth
        {
            get { return (int)GetValue(MinWidthProperty); }
            set { SetValue(MinWidthProperty, value); }
        }

        public static readonly DependencyProperty LabelMinWidthProperty =
            DependencyProperty.Register(
                "LabelMinWidth", typeof(int), typeof(RealtimeConnectionSettingsView), new PropertyMetadata(0));
    }
}
