﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Microsoft.Win32;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class DataPluginUtils
    {
        private static readonly string UserAgent = "Datawatch Application";

        /// <summary>
        /// Static plugins can call this method when adding a new Row to the 
        /// table. Can be used when the values in the row is stored
        /// in the dictionary as {key=columnName, value=newValue>.
        /// NOTE: You need to surround the call to this method with a
        /// Begin/EndUpdate pair on the StandaloneTable. For performance
        /// reasons this is not done inside this method.
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="table"></param>
        /// <param name="numericDataHelper"></param>
        /// <param name="parameterFilter"></param>
        public static void AddRow(Dictionary<string, object> fields,
            StandaloneTable table, NumericDataHelper numericDataHelper,
            ParameterFilter parameterFilter, ParserSettings parserSettings)
        {
            Row row = table.AddRow();

            UpdateColumns(fields, table, row, numericDataHelper, parserSettings);
            
            if (parameterFilter != null && !parameterFilter.Contains(fields))
            {
                // Row should not have been included in the table
                table.RemoveRow(row);
            }
        }

        /// <summary>
        /// Realtime plugins can call this method when updating the table. 
        /// Realtime tables isn't using the parameters to filter out rows. 
        /// This is done through the plugin GUI, like setting Symbols in 
        /// OneTick or Predicate in Streambase.
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="table"></param>
        /// <param name="numericDataHelper"></param>
        /// <param name="row"></param>
        public static void UpdateColumns(Dictionary<string, object> fields,
                StandaloneTable table, Row row, NumericDataHelper numericDataHelper, 
            ParserSettings parserSettings)
        {
            string format;
            for (int i = 0; i < table.ColumnCount; i++)
            {
                Column column = table.GetColumn(i);
                string columnName = column.Name;

                object value;
                if (!fields.TryGetValue(columnName, out value))
                {
                    continue;
                }

                if (value == null)
                {
                    // Don't try to parse and add null values to the table
                    continue;
                }
                if (column is NumericColumn)
                {
                    double newDouble = ObjectToDouble(value, numericDataHelper);
                    if (!Double.IsNaN(newDouble))
                    {
                        ((NumericColumn)column).SetNumericValue(row, newDouble);
                    }
                }
                else if (column is TimeColumn)
                {
                    DateParser dateParser = parserSettings.GetColumn(columnName).DateParser;
                    ((TimeColumn)column).SetTimeValue(row, ObjectToDateTime(value, dateParser));
                }
                else if (column is TextColumn)
                {
                    string newString = ObjectToString(value);
                    if (!string.IsNullOrEmpty(newString))
                    {
                        ((TextColumn)column).SetTextValue(row, newString);
                    }
                }
            }
        }

        public static double ObjectToDouble(object value, 
            NumericDataHelper numericDataHelper)
        {
            if (value is double)
            {
                // Allready a double. So no need to parse it.
                return (double) value;
            }
            try
            {
                if (value is string)
                {
                    return numericDataHelper.ParseNumber((string)value);
                }
                return Convert.ToDouble(value);
            }
            catch
            {
                return double.NaN;
            }            
        }

        public static string ObjectToString(object value)
        {
            if (value is string)
            {
                // Allready a double. So no need to parse it.
                return (string)value;
            }
            try
            {
                return Convert.ToString(value);
            }
            catch
            {
                // Don't update the row with this invalid value. Remove 
                // it from the Dictionary.
                Log.Error(string.Format(Properties.Resources.LogInvalidDoubleValue,
                    value));
                return null;
            }
        }

        public static void ParseStringData(StandaloneTable table,
            string[] row, int numRead, object[] data, NumericDataHelper numericDataHelper, 
            ParserSettings parserSettings)
        {
            if (table == null) throw Exceptions.ArgumentNull("table");
            if (row == null) throw Exceptions.ArgumentNull("row");
            if (data == null) throw Exceptions.ArgumentNull("data");

            int count = Math.Min(table.ColumnCount, numRead);
            for (int i = 0; i < count; i++)
            {
                Column column = table.GetColumn(i);

                if (column is TextColumn)
                {
                    string t = row[i];
                    if (!string.IsNullOrEmpty(t)) {
                        data[i] = t;
                    } else {
                        data[i] = null;
                    }
                }
                else if (column is NumericColumn)
                {
                    double d = ObjectToDouble(row[i], numericDataHelper);
                    if (Double.IsNaN(d)) {
                        data[i] = null;
                    } else {
                        data[i] = d;
                    }
                }
                else if (column is TimeColumn)
                {
                    DateParser dateParser = parserSettings.GetColumn(column.Name).DateParser;
                    DateTime t = ObjectToDateTime(row[i], dateParser);
                    if (TimeValue.IsEmpty(t)) {
                        data[i] = null;
                    } else {
                        data[i] = t;
                    }
                }
                else
                {
                    data[i] = row[i];
                }
            }
            Array.Clear(data, count, data.Length-count);
        }

        public static void ParseObjectArrayData(StandaloneTable table,
           Dictionary<string, object> row, object[] data,
           NumericDataHelper numericDataHelper, ParserSettings parserSettings)
        {
            if (table == null) throw Exceptions.ArgumentNull("table");
            if (row == null) throw Exceptions.ArgumentNull("row");
            if (data == null) throw Exceptions.ArgumentNull("data");

            int count = table.ColumnCount;
            for (int i = 0; i < count; i++)
            {
                Column column = table.GetColumn(i);
                string columnName = column.Name;

                object value;
                if (!row.TryGetValue(columnName, out value))
                {
                    continue;
                }

                if (value == null)
                {
                    // Don't try to parse and add null values to the table
                    continue;
                }

                data[i] = null;

                if (column is NumericColumn)
                {
                    double d = ObjectToDouble(value, numericDataHelper);
                    if (!Double.IsNaN(d))
                    {
                        data[i] = d;
                    }
                }
                else if (column is TimeColumn)
                {
                    DateParser dateParser = parserSettings
                        .GetColumn(column.Name).DateParser;
                    DateTime t = ObjectToDateTime(value, dateParser);
                    if (!TimeValue.IsEmpty(t))
                    {
                        data[i] = t;
                    }
                }
                else 
                {
                    string t = ObjectToString(value);
                    if (!string.IsNullOrEmpty(t))
                    {
                        data[i] = t;
                    }
                }
            }
        }

        /// <summary>
        /// Returns a parsed DateTime. Will return DateTime.MinValue if it was 
        /// not parsable.
        /// </summary>
        /// <param name="dateObject">object is expected to be a string.</param>
        /// <returns></returns>
        public static DateTime ObjectToDateTime(object dateObject, DateParser dateParser)
        {
            if (dateObject is DateTime) {
                // Allready a DateTime. So no need to parse it.
                return (DateTime)dateObject;
            }
            if (!(dateObject is string))
            {
                return DateTime.MinValue;
            }

            return dateParser.Parse((string) dateObject);
        }

        /// <summary>
        /// Show FileDialog where you can pick a file from computer.
        /// The name of choosen file is returned.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="oldFileName"></param>
        /// <param name="fileFilter"></param>
        /// <param name="globalSettings"></param>
        /// <returns></returns>
        public static string BrowseForFile(Window owner, 
            string oldFileName, string fileFilter, PropertyBag globalSettings)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = oldFileName;
            dlg.Filter = fileFilter;
            string dir = SettingsHelper.GetLastDataDirectory(globalSettings);
            if (dir != null && Directory.Exists(dir))
            {
                dlg.InitialDirectory = dir;
            }

            if (dlg.ShowDialog(owner) == true)
            {
                string fileName = dlg.FileName;

                SettingsHelper.SetLastDataDirectory(globalSettings, 
                    Path.GetDirectoryName(fileName));

                return fileName;
            }
            return null;
        }

        // Returns a stream that can be used to read from. 
        public static StreamReader GetStream(IFilePathSettings settings,
            IEnumerable<ParameterValue> parameters, string filePath = null)
        {
            StreamReader streamReader;

            if (settings.FilePathType == PathType.File)
            {
                string parameterizedFilePath = DataUtils.ApplyParameters(filePath, parameters);
                Log.Info(Properties.Resources.LogFilePath, parameterizedFilePath);
                streamReader = new StreamReader(parameterizedFilePath, Encoding.Default);
            }
            else if (settings.FilePathType == PathType.WebURL)
            {
                string URL = DataUtils.ApplyParameters(settings.WebPath, parameters);
//                int timeout = settings.RequestTimeout * 1000;
                string userId = DataUtils.ApplyParameters(settings.WebUserID, parameters);
                string pwd = DataUtils.ApplyParameters(settings.WebPassword, parameters);

                string postData = null;
                if (!string.IsNullOrEmpty(settings.RequestBody))
                {
                    postData = DataUtils.ApplyParameters(settings.RequestBody,
                        parameters);
                }
                HttpWebRequest httpRequest = GetHttpWebRequestInstance(URL, settings.RequestTimeout, 
                    userId, pwd, postData, settings.WebHeaderCollection);

                HttpWebResponse webResponse =
                    (HttpWebResponse)httpRequest.GetResponse();

                //create stream object from webresponse
                streamReader = new StreamReader(webResponse.GetResponseStream(), 
                    Encoding.UTF8);

            }
            else if (settings.FilePathType == PathType.Text)
            {
                string text = DataUtils.ApplyParameters(settings.Text, parameters);
                byte[] bytes = Encoding.UTF8.GetBytes(text);
                MemoryStream memoryStream = new MemoryStream(bytes);
                streamReader = new StreamReader(memoryStream, Encoding.UTF8);
            }
            else
            {
                throw new Exception(Properties.Resources.ExFilePath);
            }

            return streamReader;
        }

        public static HttpWebRequest GetHttpWebRequestInstance(string URL, int requestTimeout,
            string userId, string pwd, string postData)
        {
            return GetHttpWebRequestInstance(URL, requestTimeout, userId, pwd, postData, null);
        }

        public static HttpWebRequest GetHttpWebRequestInstance(string URL, int requestTimeout, 
            string userId, string pwd, string postData, WebHeaderCollection headers)
        {
            Log.Info(Properties.Resources.LogWebPath, URL, postData);

            HttpWebRequest httpRequest =
                (HttpWebRequest)WebRequest.Create(URL);

            if (headers != null && headers.Count > 0)
            {
                httpRequest.Headers = headers;
            }

            httpRequest.UserAgent = UserAgent;
            //set the timeout
            httpRequest.Timeout = requestTimeout * 1000;
            //set cache policy to none
            HttpRequestCachePolicy noCachePolicy =
                new HttpRequestCachePolicy(
                    HttpRequestCacheLevel.NoCacheNoStore);
            httpRequest.CachePolicy = noCachePolicy;

            //get URI credential
            httpRequest.Credentials = GetAppropriateCredential(userId, pwd);

            //if the request body is not emty do a post request
            if (postData != null)
            {
                httpRequest.Method = "POST";

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                httpRequest.ContentType = "application/x-www-form-urlencoded";
                httpRequest.ContentLength = byteArray.Length;
                // Get the request stream.
                Stream dataStream = httpRequest.GetRequestStream();
                // Write the data to the request stream.
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.
                dataStream.Close();
            }
            return httpRequest;
        }

        public static NetworkCredential GetAppropriateCredential(
            string webUserId, string webPassword)
        {
            //apply credentials if userid/password not emty
            if (!string.IsNullOrEmpty(webUserId) &&
                !string.IsNullOrEmpty(webPassword))
            {

                //check if user has entered domain name like domain\username
                string[] userDomain = null;

                if (webUserId.Contains(@"\") &&
                    webUserId.Length > 2)
                {

                    userDomain = webUserId.Split('\\');
                }

                if (userDomain != null && userDomain.Length > 0)
                {
                    return new NetworkCredential(userDomain[1], webPassword, 
                        userDomain[0]);
                }

                return new NetworkCredential(webUserId, webPassword);
            }
            return null;
        }

        public static string[] GetDataFiles(IFilePathSettings settings)
        {
            // Do not return file path when path contains parameters
            if (settings.FilePathType == PathType.File &&
                settings.FilePath.Length > 0 &&
                (!settings.IsFilePathParameterized))
            {
                return new string[] { settings.FilePath };
            }
            return null;
        }

        public static void UpdateFileLocation(IFilePathSettings settings,
            string oldPath, string newPath, string workbookPath) 
        {
            if (settings.FilePathType == PathType.File &&
                settings.FilePath.Length > 0 &&
                Path.GetFileName(settings.FilePath) ==
                Path.GetFileName(oldPath))
            {
                settings.FilePath =
                    Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }

        /**
         * Add all columns (that are defined in columnDefintions) to the table.
         * <params name="table"> The table to add columns on</params>
         * <params name="columnDefinitions"> Describes the columns to add. Each
         * element contain name and type. </params> 
         */
        public static void AddColumns(StandaloneTable table,
                IEnumerable<ColumnDefinition> columnDefinitions) {

            foreach (ColumnDefinition column in columnDefinitions) {
                if (!column.IsEnabled) continue;

                if (!column.IsValid)
                {
                    throw Exceptions.InvalidColumnDefinition();
                }

                switch (column.Type) {
                    case ColumnType.Numeric:
                        table.AddColumn(new NumericColumn(column.Name));
                        break;
                    case ColumnType.Text:
                        table.AddColumn(new TextColumn(column.Name));
                        break;
                    case ColumnType.Time:
                        table.AddColumn(new TimeColumn(column.Name));
                        break;
                }
            }
        }

        /// <summary>
        /// Add columns to the table from the schema column list.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="schemaColumns"></param>
        public static void AddColumns<T>(StandaloneTable table, SchemaColumnsSettings<T> schemaColumnSettings) where T : DatabaseColumn
        {
            if (schemaColumnSettings.SchemaColumns != null && !schemaColumnSettings.SchemaColumns.Any())
            {
                return;
            }
            foreach (DatabaseColumn column in schemaColumnSettings.SchemaColumns)
            {
                if (table.ContainsColumn(column.ColumnName))
                {
                    continue;
                }
                AddColumn(table, column.ColumnName, column.ColumnType);
            }
        }

        /// <summary>
        /// Adds a column to table of type columnType with name columnName. 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="columnName"></param>
        /// <param name="columnType"></param>
        /// <returns></returns>
        public static Column AddColumn(StandaloneTable table, string columnName, ColumnType columnType)
        {
            Column column = null;
            switch (columnType)
            {
                case ColumnType.Numeric:
                    {
                        column = new NumericColumn(columnName);
                        break;
                    }
                case ColumnType.Text:
                    {
                        column = new TextColumn(columnName);
                        break;
                    }
                case ColumnType.Time:
                    column = new TimeColumn(columnName);
                    break;
            }
            table.AddColumn(column);
            return column;
        }

        /**
         * Method that will show the Connection Settings GUI for the plugin and 
         * when window is closed pressing OK a PropertyBag with all settings is
         * returned.
         * <params name="window"> Owner window</params>
         * <params name="window"> Window that show be presented.</params>
         * <params name="settings"> A ConnectionSettings of the plugins. </params>
         * <returns> If click on OK in the connection window, this method will 
         * return a PropertyBag with all the settings done in the connection 
         * plugin GUI. Otherwise null will be returned.</returns>
         */
        public static PropertyBag ShowDialog(Window owner, Window window, 
            ConnectionSettings settings)
        {
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                PropertyBag bag = settings.ToPropertyBag();

                return bag;
            }

            return null;            
        }

        /// <summary>
        /// Generates a timestamp for realtime plugins. Should be called from
        /// the plugins adapter method GetTimeStamp() when it gets a 
        /// <see cref="Dictionary{TKey, TValue}"/> as a parameter.
        /// </summary>
        /// <param name="fields">Dictionary of key=columnName value=valueAsString</param>
        /// <param name="isTimeIdColumnGenerated"></param>
        /// <param name="timeIdColumn"></param>
        /// <returns>A timestamp</returns>
        public static DateTime GetTimeStamp(Dictionary<string, object> fields,
            bool isTimeIdColumnGenerated, string timeIdColumn, DateParser dateParser)
        {
            if (isTimeIdColumnGenerated ||
                string.IsNullOrEmpty(timeIdColumn))
            {
                return DateTime.MinValue;
            }
            object timeObject;
            if (!fields.TryGetValue(timeIdColumn, out timeObject) || 
                timeObject == null)
            {
                return DateTime.MinValue;
            }
            DateTime timeDate = ObjectToDateTime(timeObject, dateParser);
            return timeDate;
        }

        /// <summary>
        /// This method is used by realtime adapters. Call this method from 
        /// the method that receive the message when you want to parse it into
        /// a <see cref="Dictionary{TKey, TValue}"/> and enqueue it to the 
        /// TableUpdater.
        /// </summary>
        /// <typeparam name="T">Table type</typeparam>
        /// <typeparam name="C">RealtimeConnectionSettings</typeparam>
        /// <typeparam name="E">A Dictionary</typeparam>
        /// <param name="message">The text message to be parsed and enueue.</param>
        /// <param name="parser">The parser that should do the parsing job.</param>
        /// <param name="idColumn">The idcolumn</param>
        /// <param name="updater">The TableUpdater where we put this parsed message.</param>
        public static void EnqueueMessage<T, C, E>(string message, 
            IParser parser, string idColumn, TableUpdater<T, C, E> updater)
            where T : ParameterTable
            where C : RealtimeConnectionSettings
            where E : Dictionary<string, object>
        {
            Dictionary<string, object> fields = null;
            try
            {
                fields = parser.Parse(message);
            } catch
            {
                // Solves issue 16325.
                // If wrong message type is sent to the parser it throws Exception. 
                // Catch it to avoid for example the QpidListener -> Listener crash 
                // internally because MessageTransfer() calls this method, and then
                // the session is dead and hangs at session.Close().
                return;
            }

            EnqueueMessage(fields, idColumn, updater);
        }

        public static void EnqueueMessage<T, C, E>(Dictionary<string, object> fields, 
                string idColumn, TableUpdater<T, C, E> updater)
            where T : ParameterTable
            where C : RealtimeConnectionSettings
            where E : Dictionary<string, object>
        {
            object id;
            if (!fields.TryGetValue(idColumn, out id) || id == null)
            {
                return;
            }
            string idString = id.ToString();
            if (string.IsNullOrEmpty(idString))
            {
                return;
            }
            updater.EnqueueUpdate(idString, (E)fields);
        }
    }
}
