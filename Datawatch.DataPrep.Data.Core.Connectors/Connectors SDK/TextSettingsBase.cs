﻿using System;
using System.Collections.Generic;
using System.Net;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class TextSettingsBase : ConnectionSettings, IFilePathSettings,
        IDisposable, IMessageQueueConnectorSettings
    {
        private const string NumericDataHelperKey = "NumericDataHelper";
        private readonly IPluginManager pluginManager;
        private readonly NumericDataHelper numericDataHelper;
        private bool disposed;
        protected readonly PropertyBag parserSettingsBag;
        protected ParserSettings parserSettings;

        public abstract PathType FilePathType { get; set; }
        protected abstract string DefaultParserPluginId { get; }
        private readonly IEnumerable<ParameterValue> parameters;
        private readonly ChunkedStringHelper textHelper;
        protected IPluginErrorReportingService errorReporter;
        
        public TextSettingsBase(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public TextSettingsBase(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag)
        {
            if (pluginManager != null)
            {
                this.pluginManager = pluginManager;
                this.errorReporter = pluginManager.ErrorReporter;
            }
            this.parameters = parameters;
            parserSettingsBag =
                bag.SubGroups["ParserSettings"];
            if (parserSettingsBag == null)
            {
                parserSettingsBag = new PropertyBag();
                bag.SubGroups["ParserSettings"] = parserSettingsBag;
            }

            PropertyBag numericDataHelperBag =
                bag.SubGroups[NumericDataHelperKey];
            if (numericDataHelperBag == null)
            {
                numericDataHelperBag = new PropertyBag();
                bag.SubGroups[NumericDataHelperKey] = numericDataHelperBag;
            }
            numericDataHelper = new NumericDataHelper(numericDataHelperBag);

            PropertyBag textSubGroup = GetInternalSubGroup("Text");
            if (textSubGroup == null)
            {
                textSubGroup = new PropertyBag();
            }
            textHelper = new ChunkedStringHelper(textSubGroup);
        }

        ~TextSettingsBase()
        {
            Dispose(false);
        }

        public string FilePath
        {
            get
            {
                return GetInternal("FilePath");

            }
            set
            {
                SetInternal("FilePath", value);
            }
        }

        public bool IsFilePathParameterized
        {
            get
            {
                if (this.parameters == null)
                {
                    return false;
                }

                foreach (ParameterValue parameter in this.parameters)
                {
                    if (this.FilePath.Contains("{" + parameter.Name + "}") ||
                        this.FilePath.Contains("$" + parameter.Name))
                    {
                        return true;
                    }
                }
                return false;
            }
        }


        public virtual string WebPath
        {
            get
            {
                return GetInternal("WebPath", "http://");
            }
            set
            {
                SetInternal("WebPath", value);
            }
        }

        public string WebUserID
        {
            get
            {
                return GetInternal("WebUserID");
            }

            set
            {
                SetInternal("WebUserID", value);
            }
        }

        public string WebPassword
        {
            get
            {
                return GetInternal("WebPassword");
            }

            set
            {
                SetInternal("WebPassword", value);
            }
        }

        public string RequestBody
        {
            get
            {
                return GetInternal("RequestBody");
            }

            set
            {
                SetInternal("RequestBody", value);
            }
        }

        public int RequestTimeout
        {
            get
            {
                return GetInternalInt("RequestTimeout", 10);
            }
            set
            {
                SetInternalInt("RequestTimeout", value);
            }
        }

        public int[] RequestTimeouts
        {
            get
            {
                return new int[]{
                    10,
                    20,
                    30,
                    60
                };
            }
        }

        public string Text
        {
            get
            {
                return textHelper.Text;
            }
            set
            {
                textHelper.Text = value;
                SetInternalSubGroup("Text", textHelper.ToPropertyBag());
            } 
        }

        public virtual IParserPlugin ParserPlugin
        {
            get
            {

                IParserPlugin parserPlugin = pluginManager.GetParserPlugin(DefaultParserPluginId);
                return parserPlugin;
            }
            set
            {
            }
        }

        public virtual IEnumerable<IParserPlugin> ParserPlugins
        {
            get
            {
                return null;
            }
        }

        public virtual bool ShowParserSelector
        {
            get { return false; }
        }

        public virtual bool ShowTestConnectorButton
        {
            get { return false; }
        }

        public virtual bool ShowGenerateColumnButton
        {
            get { return false; }
        }

        public virtual bool ShowAddColumnButton
        {
            get { return true; }
        }

        public virtual ParserSettings ParserSettings
        {
            get
            {
                if (parserSettings != null) return parserSettings;

                IParserPlugin parserPlugin = ParserPlugin;
                if (parserPlugin == null)
                {
                    return EmptyParserSettings.Instance;
                }
                parserSettings =
                    parserPlugin.CreateParserSettings(parserSettingsBag);
                return parserSettings;
            }
        }

        public virtual bool ShowFilter
        {
            get
            {
                return true;
            }
        }

        public virtual bool CanGenerateColumns()
        {
            return false;
        }

        public virtual void DoGenerateColumns()
        {
        }

        public virtual bool CanTestConnection()
        {
            return false;
        }

        public virtual void DoTestConnection()
        {
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public NumericDataHelper NumericDataHelper
        {
            get
            {
                return numericDataHelper;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (parserSettings != null)
                    {
                        parserSettings.Dispose();
                    }
                }
                disposed = true;
            }
        }

        public virtual bool IsOk
        {
            get
            {
                return (!string.IsNullOrEmpty(FilePath) ||
                    !string.IsNullOrEmpty(WebPath)) &&
                    IsParserSettingsOk;
            }
        }

        public bool IsPathOk()
        {
            if (FilePathType == PathType.File &&
                FilePath != null)
            {
                return FilePath.Length > 0;
            }
            if (FilePathType == PathType.WebURL &&
                WebPath != null)
            {
                if (!IsHeadersOk)
                {
                    return false;
                }
                //webpath should be larger then default http://
                return WebPath.Length > 7;
            }
            if (FilePathType == PathType.Text &&
                Text != null)
            {
                return Text.Length > 0;
            }

            return false;
        }

        public bool IsParserSettingsOk
        {
            get
            {
                int cnt = 0;
                foreach (ColumnDefinition colDef in ParserSettings.Columns)
                {
                    if (!colDef.IsValid)
                    {
                        return false;
                    }
                    cnt++;
                }
                return cnt != 0;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TextSettingsBase)) return false;

            if (!base.Equals(obj)) return false;

            TextSettingsBase cs = (TextSettingsBase)obj;

            if (!FilePathType.Equals(cs.FilePathType) ||
                !string.Equals(FilePath, cs.FilePath) ||
                !string.Equals(WebPath, cs.WebPath) ||
                !string.Equals(WebUserID, cs.WebUserID) ||
                !string.Equals(WebPassword, cs.WebPassword) ||
                !string.Equals(RequestBody, cs.RequestBody) ||
                !string.Equals(NumericDataHelper, cs.NumericDataHelper) ||
                !string.Equals(Text, cs.Text))
            {
                return false;
            }

            return ParserSettings.Equals(cs.ParserSettings);
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            hashCode ^= FilePathType.Equals(PathType.File) ? 0 : 1;

            object[] fields = { FilePath, WebPath, WebUserID, WebPassword, 
                                  RequestBody, NumericDataHelper };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            hashCode ^= ParserSettings.GetHashCode();

            return hashCode;
        }

        public string HttpHeaders
        {
            get
            {
                return GetInternal("HttpHeaders");
            }

            set
            {
                SetInternal("HttpHeaders", value);
            }
        }

        public bool IsHeadersOk
        {
            get
            {
                return WebHeaderCollection != null;
            }
        }

        // Return null if any header is invalid. 
        public WebHeaderCollection WebHeaderCollection
        {
            get
            {
                string headers =
                    DataUtils.ApplyParameters(HttpHeaders, parameters); ;
                WebHeaderCollection headerCollection = new WebHeaderCollection();
                if (string.IsNullOrEmpty(headers))
                {
                    return headerCollection;
                }
                string[] headerPairs = headers.Split(',');
                HashSet<string> takenNames = new HashSet<string>();
                for (int i = 0; i < headerPairs.Length; i++)
                {
                    string headerPair = headerPairs[i];
                    if (string.IsNullOrEmpty(headerPair))
                    {
                        // Bad header
                        return null;
                    }
                    string[] nameValue = headerPair.Split('=');
                    if (nameValue.Length != 2)
                    {
                        // Bad header
                        return null;
                    }
                    string name = nameValue[0].Trim();
                    if (string.IsNullOrEmpty(name) || name.Contains(" "))
                    {
                        // Bad header
                        return null;
                    }
                    // Check so not two headers have same name.
                    if (takenNames.Contains(name))
                    {
                        // headers can not have same name
                        return null;
                    }
                    string value = nameValue[1];
                    takenNames.Add(name);
                    headerCollection.Add(name, value);
                }
                return headerCollection;
            }
        }
    }
}
