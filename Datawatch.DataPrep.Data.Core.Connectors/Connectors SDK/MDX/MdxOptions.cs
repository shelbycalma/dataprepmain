﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class MdxOptions
    {
        public bool NonEmpty { get; set; }
        public AxisItemNode NullSuppression { get; set; }
    }
}
