﻿using System;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class ComparisonMemberNode : ExpressionNode
    {
        private readonly MemberNode node;
        private readonly string parameter;
        private readonly ComparisonOperator comparisonOperator;

        private readonly string format;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="parameter"></param>
        /// <param name="comparisonOperator">Defines the comparison operator</param>
        public ComparisonMemberNode(MemberNode node, string parameter, 
            ComparisonOperator comparisonOperator = ComparisonOperator.Equal, bool wrapParameterInQuotes = true)
        {
            this.node = node;
            this.parameter = parameter;
            this.comparisonOperator = comparisonOperator;
            format = wrapParameterInQuotes ? "cstr({0}) {1} \"{2}\"" : "{0} {1} {2}";
        }

        private string GetComparisonToken()
        {
            switch (comparisonOperator)
            {
                case ComparisonOperator.Equal:
                    return TokensHelper.OPERATOR_EQUALS;
                case ComparisonOperator.NotEqual:
                    return TokensHelper.OPERATOR_NOT_EQUALS;
                case ComparisonOperator.GreaterOrEqual:
                    return TokensHelper.OPERATOR_GREATER_OR_EQUAL;
                case ComparisonOperator.LessOrEqual:
                    return TokensHelper.OPERATOR_LESS_OR_EQUAL;
            }
            return null;
        }

        public override string ToExpression()
        {
            if (!(node is EquatableMemberNode))
            {
                throw new NotSupportedException();
            }

            string nodeName = this.node.FullName;
            string token = ((EquatableMemberNode)this.node).GetEqualtyNodeExpressionToken();
            if (!string.IsNullOrEmpty(token))
            {
                nodeName = $"{this.node.FullName}.{token}";
            }
            return string.Format(format, nodeName,
                GetComparisonToken(), 
                this.parameter);
        }
    }
}