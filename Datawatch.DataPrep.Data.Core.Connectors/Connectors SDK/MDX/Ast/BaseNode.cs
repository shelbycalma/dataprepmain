﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public abstract class BaseNode
    {
        public abstract string ToExpression();
    }
}