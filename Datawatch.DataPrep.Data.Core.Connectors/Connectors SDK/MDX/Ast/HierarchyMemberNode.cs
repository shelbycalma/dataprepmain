﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class HierarchyMemberNode : EquatableMemberNode
    {
        public HierarchyMemberNode(string fullName) : base(fullName)
        {
        }
        
        public override string ToExpression()
        {
            return string.Format("{0}.{1}", this.FullName, TokensHelper.MEMBERS);
        }

        public override string GetEqualtyNodeExpressionToken()
        {
            return string.Format("{0}.{1}",TokensHelper.CURRENT_MEMBER, TokensHelper.MEMBER_VALUE);
        }
    }
}