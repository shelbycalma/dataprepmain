﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public enum BinaryOperator
    {
        OrOperator,
        AndOperator
    }
}