﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class CubeNode : FromNode
    {
        public CubeNode(string cubeName)
        {
            CubeName = cubeName;
        }

        public string CubeName { get; private set; }

        public override string ToExpression()
        {
            return string.Format("[{0}]", CubeName);
        }
    }
}