﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class LevelMemberNode : EquatableMemberNode
    {
        public LevelMemberNode(string fullName) : base(fullName)
        {
        }

        public override string ToExpression()
        {
            return string.Format("{0}.{1}", this.FullName, TokensHelper.MEMBERS);
        }

        public override string GetEqualtyNodeExpressionToken()
        {
            return string.Format("{0}.{1}.{2}",TokensHelper.HIERARCHY, TokensHelper.CURRENT_MEMBER, TokensHelper.MEMBER_VALUE);
        }
    }
}