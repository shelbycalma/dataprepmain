﻿using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class HierarchizeFunctionNode : FunctionAxisItemNode
    {
        private readonly AxisItemNode node;

        public HierarchizeFunctionNode(AxisItemNode node)
        {
            this.node = node;
        }

        public AxisItemNode Node
        {
            get { return this.node; }
        }

        public override string ToExpression()
        {
            var expressionBuilder = new StringBuilder();
            expressionBuilder.Append(TokensHelper.FUNCTION_HIERARCHIZE);
            expressionBuilder.Append(TokensHelper.BRACKET_OPEN);
            expressionBuilder.Append(this.node.ToExpression());
            expressionBuilder.Append(TokensHelper.BRACKET_CLOSE);
            return expressionBuilder.ToString();
        }
    }
}