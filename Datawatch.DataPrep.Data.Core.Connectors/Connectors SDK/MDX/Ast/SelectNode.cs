﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class SelectNode : ExpressionNode
    {
        private IList<AxisNode> axises;
        private SlicerAxisNode slicerAxis;
        private FromNode fromNode;

        public SelectNode(IEnumerable<AxisNode> axises, SlicerAxisNode slicerAxis, FromNode fromNode)
        {
            // TODO: Check right indexes, and order by AxisIndex
            this.axises = axises.ToList();
            this.slicerAxis = slicerAxis;
            this.fromNode = fromNode;
        }

        public SelectNode() : this(new List<AxisNode>(0), null, null)
        {
        }

        public IList<AxisNode> Axises
        {
            get { return this.axises; }
            set { this.axises = value; }
        }

        public FromNode FromNode
        {
            get { return this.fromNode; }
            set { this.fromNode = value; }
        }

        public SlicerAxisNode Slicer
        {
            get { return this.slicerAxis; }
            set { this.slicerAxis = value; }
        }

        public override string ToExpression()
        {
            var expressionBuilder = new StringBuilder();

            expressionBuilder.Append(TokensHelper.SELECT);
            expressionBuilder.Append(TokensHelper.SPACE);

            var axisExpressions = this.axises.Select(axis => axis.ToExpression().Trim())
                          .Where(expression => !string.IsNullOrWhiteSpace(expression))
                          .ToList();

            expressionBuilder.Append(string.Join(string.Format(",{0}", Environment.NewLine), axisExpressions));
            expressionBuilder.AppendLine(TokensHelper.SPACE);

            expressionBuilder.Append(TokensHelper.FROM);
            expressionBuilder.Append(TokensHelper.SPACE);
            expressionBuilder.Append(this.fromNode.ToExpression());
            expressionBuilder.AppendLine(TokensHelper.SPACE);

            if (slicerAxis != null)
            {
                expressionBuilder.Append(TokensHelper.WHERE);
                expressionBuilder.Append(TokensHelper.SPACE);
                expressionBuilder.Append(this.slicerAxis.ToExpression());
                expressionBuilder.AppendLine();
            }

            return expressionBuilder.ToString();
        }
    }
}