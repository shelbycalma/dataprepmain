﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public enum ComparisonOperator
    {
        Equal,
        NotEqual,
        GreaterOrEqual,
        LessOrEqual
    }
}