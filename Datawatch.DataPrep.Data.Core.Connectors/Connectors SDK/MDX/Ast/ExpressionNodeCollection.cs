﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class ExpressionNodeCollection
    {
        public ExpressionNodeCollection(IEnumerable<ExpressionNode> expressionNodes, BinaryOperator binaryOperator = BinaryOperator.AndOperator)
        {
            ExpressionNodes = expressionNodes;
            BinaryOperator = binaryOperator;
        }
        public IEnumerable<ExpressionNode> ExpressionNodes { get; set; }
        public BinaryOperator BinaryOperator { get; set; }
    }
}