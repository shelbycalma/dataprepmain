﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class TokensHelper
    {
        public const string DIMENSION_PROPERTIES = "DIMENSION PROPERTIES";
        public const string OPERATOR_EQUALS = "=";
        public const string OPERATOR_NOT_EQUALS = "<>";
        public const string OPERATOR_OR = "OR";
        public const string OPERATOR_AND = "AND";
        public const string OPERATOR_GREATER_OR_EQUAL = ">=";
        public const string OPERATOR_LESS_OR_EQUAL = "<=";
        public const string COMMA = ",";
        public const string COMMA_SPACE = ", ";
        public const string FUNCTION_UNION = "Union";
        public const string FUNCTION_HIERARCHIZE = "Hierarchize";
        public const string FUNCTION_CROSS_JOIN = "Crossjoin";
        public const string FUNCTION_FILTER = "Filter";
        public const string WHERE = "WHERE";
        public const string SELECT = "SELECT";
        public const string FROM = "FROM";
        public const string NON_EMPTY = "NON EMPTY";
        public const string SPACE = " ";
        public const string ON = "ON";
        public const string BRACE_OPEN = "{";
        public const string BRACE_CLOSE = "}";
        public const string BRACKET_OPEN = "(";
        public const string BRACKET_CLOSE = ")";
        public const string MEMBERS = "Members";
        public const string CURRENT_MEMBER = "CurrentMember";
        public const string MEMBER_VALUE = "MemberValue";
        public const string HIERARCHY = "Hierarchy";
        public const string AXIS_COLUMNS = "COLUMNS";
        public const string AXIS_ROWS = "ROWS";
        public const string AXIS_PAGES = "PAGES";
        public const string AXIS_SECTIONS = "SECTIONS";
        public const string AXIS_CHAPTERS = "CHAPTERS";
        public const string AXIS_WHERE = "WHERE";
        public const string WITH = "WITH";
        public const string MEMBER = "MEMBER";
        public const string AS = "AS";
        public const string IF_THEN_ELSE = "IIF({0},{1},{2})";
        public const string IS_EMPTY = "ISEMPTY({0})";
        public const string MEMBERS_EXPRESSION = "{0}.MEMBERS";
        public const string PARENT = "PARENT";

        public static readonly Dictionary<FunctionType, string>
        
        SimpleFunctionTypeToQuery = new Dictionary<FunctionType, string>
        {
            {FunctionType.External, "{0}"},
            {FunctionType.Count, "COUNT({0})"},
            //Sum is calculated automatically 
            //There is no need to specify it explicitly
            {FunctionType.Sum, "{0}"},
            {FunctionType.Min, "MIN({0})"},
            {FunctionType.Max, "MAX({0})"},
            {FunctionType.AbsSum, "ABS({0})"},
            {FunctionType.First, "HEAD({0}, 1)"},
            {FunctionType.Last, "TAIL({0}, 1)"},
            {FunctionType.SumOfSquares, "{0}*{0}"},
        };
    }
}