﻿

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class HierarchyWithMeasureFilterMemberNode : EquatableMemberNode
    {
        public HierarchyWithMeasureFilterMemberNode(string fullName) : base(fullName)
        {
        }

        public override string ToExpression()
        {
            return string.Format("{0}.{1}", this.FullName, TokensHelper.MEMBERS);
        }

        public override string GetEqualtyNodeExpressionToken()
        {
            return "";
        }
    }
}