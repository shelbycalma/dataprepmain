﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class DimensionMemberNode : MemberNode
    {
        public DimensionMemberNode(string fullName) : base(fullName)
        {
        }

        public override string ToExpression()
        {
            return string.Format("{0}.{1}", this.FullName, TokensHelper.MEMBERS);
        }
    }
}