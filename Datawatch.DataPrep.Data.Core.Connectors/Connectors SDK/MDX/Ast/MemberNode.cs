﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class MemberNode : AxisItemNode
    {
        private readonly string fullName;

        public MemberNode(string fullName)
        {
            this.fullName = fullName;
        }

        public string FullName
        {
            get { return this.fullName; }
        }

        public override string ToExpression()
        {
            return this.fullName;
        }
    }
}