﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class SetNode : AxisItemNode
    {
        private readonly IList<TupleNode> tuples;

        public SetNode(IEnumerable<TupleNode> tuples)
        {
            this.tuples = tuples.ToList();
        }

        public override string ToExpression()
        {
            var builder = new StringBuilder();
            builder.Append(TokensHelper.BRACE_OPEN);
            builder.AppendLine();

            builder.Append(string.Join(string.Format(",{0}", Environment.NewLine), this.tuples.Select(n => n.ToExpression())));

            builder.AppendLine();
            builder.Append(TokensHelper.BRACE_CLOSE);
            return builder.ToString();
        }
    }
}