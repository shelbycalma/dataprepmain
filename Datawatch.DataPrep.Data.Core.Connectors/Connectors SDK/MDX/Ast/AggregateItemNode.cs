﻿using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class AggregateItemNode : MemberNode
    {
        private string[] columns;
        private string alias;

        public AggregateItemNode(FunctionType functionType,
            string alias, string[] columns) : base(alias)
        {
            this.FunctionType = functionType;
            this.alias = alias;
            this.columns = columns;
        }

        public FunctionType FunctionType { get; private set; }

        public string ToString(MdxOptions options)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(TokensHelper.MEMBER);
            builder.Append(TokensHelper.SPACE);
            builder.Append(alias);
            builder.Append(TokensHelper.SPACE);
            builder.Append(TokensHelper.AS);
            builder.Append(TokensHelper.SPACE);
            builder.Append(GetFunctionString(options));
            return builder.ToString();
        }

        public override string ToExpression()
        {
            return this.ToString(null);
        }

        private string GetFunctionString(MdxOptions options)
        {
            //Process simple function types
            //They always use only first column, and have no conditions
            if (TokensHelper.SimpleFunctionTypeToQuery.ContainsKey(FunctionType)
)
            {
                return string.Format(
                    TokensHelper.SimpleFunctionTypeToQuery[FunctionType],
                    new object[] { columns[0] });
            }
            switch (FunctionType)
            {
                case FunctionType.SumOfProducts:
                    return string.Format("{0}*{1}", columns[0], columns[1]);
                case FunctionType.NegativeSum:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format("{0} < 0", columns[0]),
                        columns[0], "0");
                case FunctionType.PositiveSum:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format("{0} > 0", columns[0]), columns[0], "0");
                case FunctionType.Samples:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format(TokensHelper.IS_EMPTY, columns[0]),
                        "0", "1");
                case FunctionType.ConditionalSum:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format("{0}<>0", columns[0]), columns[0], 0);
                case FunctionType.SumOfQuotients:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format("{0}<>0", columns[0]),
                        string.Format("1/{0}", columns[0]), "0");
                case FunctionType.SumOfWeightedQuotients:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format("{0}<>0", columns[1]),
                        string.Format("{0}/{1}", columns[0],
                            columns[1]), "0");
                case FunctionType.NonZeroSamples:
                    return string.Format(TokensHelper.IF_THEN_ELSE,
                        string.Format("{0}<>0", columns[0]),
                        "1", "0");
            }
            return null;
        }
    }
}
