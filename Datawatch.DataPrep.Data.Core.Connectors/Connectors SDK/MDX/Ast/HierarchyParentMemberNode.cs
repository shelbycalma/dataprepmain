﻿using System;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class HierarchyParentMemberNode: HierarchyMemberNode
    {
        protected int ParentLevel;
        public HierarchyParentMemberNode(string fullName, int parentLevel = 1) : base(fullName)
        {
            if(parentLevel < 1)
            {
                throw new ArgumentException(string.Format(
                    Properties.Resources.ExInvalidArgumentType, "parentLevel", parentLevel));
            }
            this.ParentLevel = parentLevel;
        }

        private string GenerateParent()
        {
            StringBuilder builder = new StringBuilder();
            for(int i = 0; i<ParentLevel; i++)
            {
                builder.Append(TokensHelper.PARENT);
                builder.Append(".");
            }
            return builder.ToString();
        }

        public override string GetEqualtyNodeExpressionToken()
        {
            return string.Format("{0}.{1}{2}", TokensHelper.CURRENT_MEMBER, GenerateParent(), TokensHelper.MEMBER_VALUE);
        }
    }
}