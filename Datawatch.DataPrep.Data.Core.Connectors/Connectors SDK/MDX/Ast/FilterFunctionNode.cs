﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class FilterFunctionNode : FunctionAxisItemNode
    {
        private readonly BaseNode setNode;
        private readonly IList<ExpressionNodeCollection> expressions;

        public FilterFunctionNode(BaseNode setNode, IList<ExpressionNodeCollection> expressions)
        {
            // TODO: Check that baseNode can be only expression that returns set
            this.setNode = setNode;
            this.expressions = expressions.ToList();
        }

        public IList<ExpressionNodeCollection> Expressions => expressions;

        public BaseNode Node => setNode;

        public override string ToExpression()
        {
            if (expressions.Count == 0 || expressions.All(e => !e.ExpressionNodes.Any())) return string.Empty;

            var expressionBuilder = new StringBuilder();
            expressionBuilder.Append(TokensHelper.SPACE);
            expressionBuilder.Append(TokensHelper.FUNCTION_FILTER);
            expressionBuilder.Append(TokensHelper.BRACKET_OPEN);

            expressionBuilder.Append(this.Node.ToExpression());
            expressionBuilder.Append(TokensHelper.COMMA);

            string separationFormat = TokensHelper.SPACE +
                                      "{0}" + TokensHelper.SPACE;

            var separateCollections = new List<string>();

            foreach (var expressionNodeCollection in Expressions)
            {
                var separator = string.Format(separationFormat,
                    expressionNodeCollection.BinaryOperator == BinaryOperator.AndOperator
                        ? TokensHelper.OPERATOR_AND
                        : TokensHelper.OPERATOR_OR);
                var regularExpressions = string.Join(separator,
                    expressionNodeCollection.ExpressionNodes.Select(e => e.ToExpression()));
                if (!string.IsNullOrEmpty(regularExpressions))
                {
                    var resultString = $"({regularExpressions})";
                    separateCollections.Add(resultString);
                }
            }

            var resultExpression = "";
            if (separateCollections.Count > 0)
            {
                resultExpression = string.Join(TokensHelper.OPERATOR_AND + TokensHelper.SPACE, separateCollections);
            }
            expressionBuilder.Append(resultExpression);

            expressionBuilder.Append(TokensHelper.BRACKET_CLOSE);

            return expressionBuilder.ToString();
        }
    }
}