﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class AxisNode : BaseNode, IComparable
    {
        private static readonly string[] DefaultAxisNames =
            {
                TokensHelper.AXIS_COLUMNS,
                TokensHelper.AXIS_ROWS,
                TokensHelper.AXIS_PAGES,
                TokensHelper.AXIS_SECTIONS,
                TokensHelper.AXIS_CHAPTERS,
                TokensHelper.AXIS_WHERE,
            };

        private readonly int axisIndex;
        protected readonly bool nonEmpty;
        protected readonly AxisItemNode node;
        protected readonly IList<string> dimensionProperties = new List<string>();

        public AxisNode(int axisIndex, bool nonEmpty, AxisItemNode node)
        {
            this.axisIndex = axisIndex;
            this.nonEmpty = nonEmpty;
            this.node = node;
        }

        public int AxisIndex
        {
            get { return this.axisIndex; }
        }

        public bool NonEmpty
        {
            get { return this.nonEmpty; }
        }

        public AxisItemNode Node
        {
            get { return this.node; }
        }

        public IList<string> DimensionProperties
        {
            get { return dimensionProperties; }
        }

        public override string ToExpression()
        {
            var builder = new StringBuilder();

            // comparison with "one" means that
            // nonempty should be applied for rows axis only
            if (nonEmpty && axisIndex == 1)
            {
                builder.Append(TokensHelper.NON_EMPTY);
                builder.Append(TokensHelper.SPACE);
                builder.Append(TokensHelper.BRACKET_OPEN);
                builder.Append(this.node.ToExpression());
                builder.Append(TokensHelper.BRACKET_CLOSE);
            }
            else
            {
                builder.Append(this.node.ToExpression());
            }

            if (dimensionProperties.Count > 0)
            {
                builder.Append(TokensHelper.SPACE);
                builder.Append(TokensHelper.DIMENSION_PROPERTIES);
                builder.Append(TokensHelper.SPACE);
                builder.Append(string.Join(",", dimensionProperties));
            }

            builder.Append(TokensHelper.SPACE);
            builder.Append(TokensHelper.ON);
            builder.Append(TokensHelper.SPACE);
            builder.Append(GetAxisName(axisIndex));

            return builder.ToString();
        }

        protected static string GetAxisName(int axisIndex)
        {
            return axisIndex >= DefaultAxisNames.Length
                       ? string.Format("Axis({0})", axisIndex)
                       : DefaultAxisNames[axisIndex];
        }

        public int CompareTo(object obj)
        {
            AxisNode other = obj as AxisNode;
            if (other == null)
            {
                return 1;
            }

            return axisIndex.CompareTo(other.axisIndex);
        }
    }
}