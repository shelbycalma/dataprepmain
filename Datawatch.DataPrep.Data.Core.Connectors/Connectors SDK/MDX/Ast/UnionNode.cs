﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class UnionNode : AxisItemNode
    {
        private readonly IList<AxisItemNode> nodes;

        public UnionNode(IEnumerable<AxisItemNode> nodes)
        {
            this.nodes = nodes.ToList();
        }

        public override string ToExpression()
        {
            var expressionBuilder = new StringBuilder();
            expressionBuilder.Append(TokensHelper.SPACE);
            expressionBuilder.Append(TokensHelper.FUNCTION_UNION);
            expressionBuilder.Append(TokensHelper.BRACKET_OPEN);
            expressionBuilder.AppendLine();

            expressionBuilder.Append(string.Join(TokensHelper.COMMA + Environment.NewLine,
                                                         this.nodes.Select(e => e.ToExpression())));

            expressionBuilder.AppendLine();
            expressionBuilder.Append(TokensHelper.BRACKET_CLOSE);
            return expressionBuilder.ToString();
        }
    }
}