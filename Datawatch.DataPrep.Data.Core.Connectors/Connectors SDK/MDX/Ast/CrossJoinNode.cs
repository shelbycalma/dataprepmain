﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class CrossJoinNode : FunctionAxisItemNode
    {
        private readonly IList<AxisItemNode> nodes;

        public CrossJoinNode(IEnumerable<AxisItemNode> nodes)
        {
            this.nodes = nodes.ToList();
        }

        public IList<AxisItemNode> Nodes
        {
            get { return nodes; }
        }

        public override string ToExpression()
        {
            if (Nodes.Count > 1)
            {
                var builder = new StringBuilder();

                builder.Append(TokensHelper.FUNCTION_CROSS_JOIN);
                builder.Append(TokensHelper.BRACKET_OPEN);
                builder.Append(string.Join(", ", this.Nodes.Select(e => e.ToExpression())));
                builder.Append(TokensHelper.BRACKET_CLOSE);

                return builder.ToString();
            }

            else if (Nodes.Count == 1)
            {
                return this.Nodes.First().ToExpression();
            }
            return string.Empty;
        }
    }
}