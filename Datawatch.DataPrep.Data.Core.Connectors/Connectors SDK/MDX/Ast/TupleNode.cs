﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class TupleNode : AxisItemNode
    {
        private readonly IList<AxisItemNode> members;

        public TupleNode(IEnumerable<AxisItemNode> members)
        {
            this.members = members.ToList();
        }

        public override string ToExpression()
        {
            if (this.members.Count == 1)
                return this.members[0].ToExpression();

            var builder = new StringBuilder();
            builder.Append(TokensHelper.BRACKET_OPEN);
            builder.AppendLine();

            builder.Append(string.Join(string.Format(",{0}", Environment.NewLine), this.members.Select(m => m.ToExpression())));

            builder.AppendLine();
            builder.Append(TokensHelper.BRACKET_CLOSE);
            return builder.ToString();
        }
    }
}