﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public abstract class EquatableMemberNode: MemberNode
    {
        public abstract string GetEqualtyNodeExpressionToken();

        public EquatableMemberNode(string fullName) : base(fullName)
        {
        }
    }
}
