﻿using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast
{
    public class SlicerAxisNode : AxisNode
    {
        public SlicerAxisNode(bool nonEmpty, AxisItemNode node) : base(-1, nonEmpty, node)
        {
        }

        public override string ToExpression()
        {
            var builder = new StringBuilder();

            // todo: i'm not really sure that we need nonempty in slicer
            //if (nonEmpty)
            //{
            //    builder.Append(TokensHelper.NON_EMPTY);
            //    builder.Append(TokensHelper.SPACE);
            //}

            builder.Append(this.node.ToExpression());

            return builder.ToString();
        }
    }
}