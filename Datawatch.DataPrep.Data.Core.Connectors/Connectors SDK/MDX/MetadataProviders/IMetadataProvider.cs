﻿using System.Collections.Generic;
using System.Data;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders
{
    public interface IMetadataProvider
    {
        IDbConnection Connection { get; }
        Cube GetCubeMetaData(string cubeName);
        IList<MeasureGroup> GetMeasureGroups(string cubeName);
        IList<string> GetAllCubes();
        IList<Catalog> GetAllCatalogs();
        void SetCatalogName(string catalogName);
    }
}
