﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public abstract class AbstractQueryBuilder : SelectNode
    {
        protected readonly List<AxisItemNode> columnMeasures = new List<AxisItemNode>();
        protected readonly List<AxisItemNode> columnDimensions = new List<AxisItemNode>();
        protected readonly List<AxisItemNode> rows = new List<AxisItemNode>();
        protected readonly List<AxisItemNode> slicer = new List<AxisItemNode>();
        protected readonly List<ComparisonMemberNode> rowsParameters = new List<ComparisonMemberNode>();
        protected readonly List<ComparisonMemberNode> slicerParameters = new List<ComparisonMemberNode>();
        protected readonly List<FilterFunctionNode> filters = new List<FilterFunctionNode>();
        protected readonly List<ExpressionNode> numericFilters = new List<ExpressionNode>();
        protected readonly List<AggregateItemNode> aggregates = new List<AggregateItemNode>();
        protected readonly IMdxSettings settings;


        public AbstractQueryBuilder(IMdxSettings settings)
        {
            this.settings = settings;
        }

        public virtual void AddColumn(MetadataElementBase measure)
        {
            this.columnMeasures.Add(new MemberNode(measure.UniqueName));
        }

        public void AddColumnDimension(MetadataElementBase dimension)
        {
            this.columnDimensions.Add(new DimensionMemberNode(dimension.UniqueName));
        }

        public virtual void RemoveColumn(MetadataElementBase measure)
        {
            throw new NotImplementedException();
        }

        public virtual void AddRow(MetadataElementBase metadataElement)
        {
            this.rows.Add(GetMemberNode(metadataElement));
        }

        public virtual void AddRowParameter(MetadataElementBase metadataElement, string paremeterName)
        {
            this.rowsParameters.Add(new ComparisonMemberNode(GetMemberNode(metadataElement), paremeterName, ComparisonOperator.Equal));
        }

        public virtual void RemoveRow(MetadataElementBase dimension)
        {
            throw new NotImplementedException();
        }

        public virtual void AddSlicer(MetadataElementBase element)
        {
            this.slicer.Add(GetMemberNode(element));
        }

        public virtual void AddSlicerParameter(MetadataElementBase dimension, string paremeterName)
        {
            this.slicerParameters.Add(new ComparisonMemberNode(GetMemberNode(dimension), paremeterName, ComparisonOperator.Equal));
        }

        public virtual void RemoveSlicer(MetadataElementBase element)
        {
            throw new NotImplementedException();
        }

        public void AddFilter(MetadataElementBase metadataElement,
            ExpressionNodeCollection expressionNodeCollection)
        {
            var memberNode = GetMemberNode(metadataElement);
            AddFilter(memberNode, expressionNodeCollection);
        }

        public void AddFilter(MemberNode memberNode, 
            ExpressionNodeCollection expressionNodeCollection)
        {
            var node = this.filters.FirstOrDefault(f => f.Node.ToExpression() == memberNode.ToExpression());
            if (node == null)
            {
                node = new FilterFunctionNode(
                    memberNode, new List<ExpressionNodeCollection>());
                this.filters.Add(node);
            }
            node.Expressions.Add(expressionNodeCollection);
        }

        public void AddNumericFilter(IEnumerable<ExpressionNode> expressionNodes)
        {
            if (expressionNodes != null)
            {
                numericFilters.AddRange(expressionNodes);
            }
        }

        public virtual void AddAggregate(AggregateItemNode aggregateItemNode)
        {
            this.aggregates.Add(aggregateItemNode);
            this.columnMeasures.Add(new MemberNode(aggregateItemNode.FullName));
        }

        protected virtual MemberNode GetMemberNode(MetadataElementBase metadataElement)
        {
            string uniqueName = metadataElement.UniqueName;
            if (metadataElement is Dimension)
                return new DimensionMemberNode(uniqueName);
            if (metadataElement is Hierarchy)
                return new HierarchyMemberNode(uniqueName);
            if (metadataElement is Level)
                return new LevelMemberNode(uniqueName);

            throw new Exception(string.Format(Properties.Resources.ExUnknownTypeForRow, metadataElement.GetType().Name));
        }

        public SelectNode BuildExpression()
        {
            var axises = new List<AxisNode>();

            if (columnMeasures.Count > 0 || columnDimensions.Count > 0)
            {
                var columnsNode = GenerateAxisItemNode(this.columnMeasures, this.columnDimensions);
                var columnsAxisNode = new AxisNode(0, settings.NullSuppression, columnsNode);
                axises.Add(columnsAxisNode);
            }
            if (rows.Count > 0)
            {
                var rowsNode = GenerateAxisItemNode(null, rows);
                if (this.rowsParameters.Count > 0)
                    rowsNode = new FilterFunctionNode(rowsNode, new List<ExpressionNodeCollection>
                                        {
                                            new ExpressionNodeCollection(this.rowsParameters)
                                        });
                var rowsAxisNode = new AxisNode(1, settings.NullSuppression, rowsNode);
                axises.Add(rowsAxisNode);
            }
            SlicerAxisNode slicerAxisNode = null;
            if (slicer.Count > 0)
            {
                var slicerNode = GenerateAxisItemNode(null, slicer);
                if (this.slicerParameters.Count > 0)
                    slicerNode = new FilterFunctionNode(slicerNode, new List<ExpressionNodeCollection> {new ExpressionNodeCollection(this.slicerParameters)});
                slicerAxisNode = new SlicerAxisNode(settings.NullSuppression, slicerNode);
            }

            var cubeNode = new CubeNode(this.settings.CubeName);

            return new SelectNode(axises, slicerAxisNode, cubeNode);
        }

        private static AxisItemNode GenerateAxisItemNode(IList<AxisItemNode> measures, IList<AxisItemNode> dimensions)
        {
            if (measures != null && measures.Count > 0 && dimensions != null && dimensions.Count > 0)
                return GenerateComplexAxisNode(measures, dimensions);
            if (measures != null && measures.Count > 0)
                return GenerateMeasuresSet(measures);
            return GenerateHierarchizeNode(dimensions);
        }

        private static AxisItemNode GenerateComplexAxisNode(IEnumerable<AxisItemNode> measures, IEnumerable<AxisItemNode> dimensions)
        {
            var crossJoins = measures.Select(m => new CrossJoinNode(new[] { m }.Concat(dimensions))).ToList();

            if (crossJoins.Count == 1) return crossJoins[0];
            return new HierarchizeFunctionNode(new UnionNode(crossJoins));
        }

        private static SetNode GenerateMeasuresSet(IEnumerable<AxisItemNode> measures)
        {
            return new SetNode(measures.Select(m => new TupleNode(new[] { m })));
        }

        private static AxisItemNode GenerateHierarchizeNode(IEnumerable<AxisItemNode> dimensions)
        {
            return new HierarchizeFunctionNode(new TupleNode(dimensions.Select(d => new TupleNode(new[] { d }))));
        }
    }
}