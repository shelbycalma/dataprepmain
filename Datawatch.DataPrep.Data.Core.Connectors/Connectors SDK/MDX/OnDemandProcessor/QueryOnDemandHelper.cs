﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using ComparisonOperator = Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast.ComparisonOperator;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.OnDemandProcessor
{
    public class QueryOnDemandHelper
    {
        public IList<IColumnModel> GetIncludedColumns(
            OnDemandParameters onDemandParameters)
        {
            if (onDemandParameters == null)
            {
                throw Exceptions.ArgumentNull("onDemandParameters");
            }

            List<IColumnModel> resultPredicates
                 = new List<IColumnModel>();
            if (onDemandParameters.Predicates != null
                && onDemandParameters.Predicates.Length > 0)
            {

                foreach (Predicate predicate in onDemandParameters.Predicates)
                {
                    ParsePredicate(predicate, resultPredicates);
                }
            }

            return resultPredicates;
        }

        public IList<IColumnModel> GetOnDemandExclusivePredicates(
            OnDemandParameters onDemandParameters)
        {
            if (onDemandParameters == null)
            {
                throw Exceptions.ArgumentNull("onDemandParameters");
            }

            List<IColumnModel> resultPredicates
                = new List<IColumnModel>();

            if (onDemandParameters.Predicates != null
                && onDemandParameters.Predicates.Length > 0)
            {
                foreach (var pred in onDemandParameters.Predicates)
                {
                    if (pred is NotOperator)
                    {
                        ParseNotPredicate((NotOperator)pred, resultPredicates);
                    }
                }
            }

            return resultPredicates;
        }

        public void ParsePredicate(Predicate predicate,
            IList<IColumnModel> parsedPredicates)
        {
            if (predicate is Comparison)
            {
                ParseTextComparisonPredicate((Comparison)predicate,
                    parsedPredicates);
            }
            else if (predicate is OrOperator)
            {
                ParseOrOperator((OrOperator)predicate,
                    parsedPredicates);
            }
            else if (predicate is AndOperator)
            {
                ParseAndOperator((AndOperator)predicate,
                    parsedPredicates);
            }
        }

        public void ParseTextComparisonPredicate(
            Comparison comparison, IList<IColumnModel> parsedPredicates)
        {
            string column = MDXUtils.GetHierarchyName(((ColumnParameter)comparison.Left).ColumnName);
            List<string> elements = new List<string>();
            if (comparison.Right is ListParameter)
            {
                elements.AddRange(
                    ((ListParameter)comparison.Right).Values);
            }
            else if (comparison.Right is StringParameter)
            {
                // todo this is a short term solution, the escaping that happens
                // in the tostring method could mess up some element names.
                var val =
                    comparison.Right.ToString(SqlDialectFactory.MySQL).Trim('\'').TrimEnd('%');
                elements.Add(val);
            }

            AddValues(parsedPredicates, column, elements);
        }

        public void ParseOrOperator(OrOperator orOperator,
            IList<IColumnModel> parsedPredicates)
        {
            ParsePredicate(orOperator.A, parsedPredicates);
            ParsePredicate(orOperator.B, parsedPredicates);
        }

        public void ParseAndOperator(AndOperator andOperator,
            IList<IColumnModel> parsedPredicates)
        {
            bool leftIsValue = andOperator.A is Comparison &&
                               ((Comparison)andOperator.A).Right
                               is ValueParameter;
            bool rightIsValue = andOperator.B is Comparison &&
                                ((Comparison)andOperator.B).Right
                                is ValueParameter;

            if (leftIsValue || rightIsValue)
            {
                List<string> valList = new List<string>();
                valList.Add(string.Empty);
                valList.Add(string.Empty);

                if (leftIsValue)
                {
                    ParseValuePredicate((Comparison)andOperator.A,
                        ComparisonOperator.GreaterOrEqual, valList,
                        parsedPredicates);

                }
                if (rightIsValue)
                {
                    ParseValuePredicate((Comparison)andOperator.B,
                        ComparisonOperator.LessOrEqual, valList,
                        parsedPredicates);
                }
            }
            else
            {
                ParsePredicate(andOperator.A, parsedPredicates);
                ParsePredicate(andOperator.B, parsedPredicates);
            }
        }

        public void ParseValuePredicate(Comparison numericPred,
            ComparisonOperator sign, IList<string> valueList,
            IList<IColumnModel> parsedPredicates)
        {
            string key = MDXUtils.GetHierarchyName(((ColumnParameter)numericPred.Left).ColumnName);
            ValueParameter valueParam = (ValueParameter)numericPred.Right;
            var value = valueParam.Value as NumberParameterValue;
            if (value != null)
            {
                AddNumericValue(parsedPredicates, key, value.Value, sign);
            }
        }

        private void ParseNotPredicate(NotOperator pred,
            IList<IColumnModel> notPredicates)
        {
            if (pred.A is OrOperator)
            {
                ParseNotOrPredicate((OrOperator)pred.A, notPredicates);
            }
        }

        private void ParseNotOrPredicate(OrOperator orOperator,
            IList<IColumnModel> notPredicates)
        {
            if (orOperator.A == null && orOperator.B is OrOperator)
            {
                ParseNotOrPredicate((OrOperator)orOperator.B, notPredicates);
            }
            else if (orOperator.A == null && orOperator.B is AndOperator)
            {
                ParseNotAndPredicate((AndOperator)orOperator.B, notPredicates);
            }
        }

        private void ParseNotAndPredicate(AndOperator andOperator,
            IList<IColumnModel> notPredicates)
        {
            if (andOperator.A == null && andOperator.B is Comparison)
            {
                ParseNotPredicateValue((Comparison)andOperator.B,
                    notPredicates);
            }
            else if (andOperator.A is Comparison && andOperator.B is Comparison)
            {
                ParseNotPredicateValue((Comparison)andOperator.A,
                    notPredicates);
                ParseNotPredicateValue((Comparison)andOperator.B,
                    notPredicates);
            }
            else if (andOperator.A == null && andOperator.B is OrOperator)
            {
                ParseNotOrPredicate((OrOperator)andOperator.B, notPredicates);
            }
            else if (andOperator.A == null && andOperator.B is AndOperator)
            {
                ParseNotAndPredicate((AndOperator)andOperator.B, notPredicates);
            }
            else if (andOperator.A is Comparison && andOperator.B is OrOperator)
            {
                ParseNotPredicateValue((Comparison)andOperator.A,
                    notPredicates);

                ParseNotOrPredicate((OrOperator)andOperator.B,
                    notPredicates);
            }
            else if (andOperator.A is Comparison && andOperator.B is AndOperator)
            {
                ParseNotPredicateValue((Comparison)andOperator.A,
                     notPredicates);

                ParseNotAndPredicate((AndOperator)andOperator.B,
                    notPredicates);
            }

        }

        private void ParseNotPredicateValue(Comparison comparison,
            IList<IColumnModel> notPredicates)
        {
            string key = MDXUtils.GetHierarchyName(((ColumnParameter)comparison.Left).ColumnName);

            if (comparison.Right is StringParameter)
            {
                string value =
                    ((StringParameter)comparison.Right).ToString(SqlDialectFactory.MySQL)
                        .TrimEnd('\'')
                        .TrimStart('\'');
                AddValues(notPredicates, key, new List<string> { value });
            }
            else if (comparison.Right is ListParameter)
            {
                string[] values = ((ListParameter)comparison.Right).Values;
                //TODO WHY it is like this??? Research
                AddValues(notPredicates, key, values);
            }
        }

        private void AddValues(IList<IColumnModel> columnValuesModel, string key, IList<string> value)
        {
            var column = new ColumnValuesModel(key, new List<string>());
            columnValuesModel.Add(column);
            foreach (var val in value)
            {
                column.ColumnValues.Add(val);
            }
        }

        private void AddNumericValue(IList<IColumnModel> columnValuesModel, string key,
            double value, ComparisonOperator comparisonOperator)
        {
            var column = new ColumnRangeModel(key, value, comparisonOperator);
            columnValuesModel.Add(column);
        }
    }
}