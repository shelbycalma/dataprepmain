﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.OnDemandProcessor
{
    public class ColumnRangeModel : IColumnModel
    {
        public ColumnRangeModel(string columnName, double value, ComparisonOperator comparisonOperator)
        {
            ColumnName = columnName;
            Value = value;
            ComparisonOperator = comparisonOperator;
        }

        public string ColumnName { get; private set; }

        public double Value { get; private set; }

        public ComparisonOperator ComparisonOperator { get; private set; }
    }
}