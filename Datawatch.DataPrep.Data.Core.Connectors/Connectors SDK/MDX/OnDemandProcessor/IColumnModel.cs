﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.OnDemandProcessor
{
    public interface IColumnModel
    {
        string ColumnName { get; }
    }
}