﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.OnDemandProcessor
{
    public class ColumnValuesModel : IGrouping<string, string>, IColumnModel
    {
        public ColumnValuesModel(string columnName, IList<string> columnValues)
        {
            ColumnName = columnName;
            ColumnValues = columnValues;
        }

        public IEnumerator<string> GetEnumerator()
        {
            return ColumnValues.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public string Key
        {
            get { return ColumnName; }
            set { ColumnName = value; }
        }

        public string ColumnName { get; private set; }
        public IList<string> ColumnValues { get; private set; }
    }
}
