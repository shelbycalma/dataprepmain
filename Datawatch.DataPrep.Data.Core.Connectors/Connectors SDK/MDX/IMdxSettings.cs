﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public interface IMdxSettings
    {
        string CubeName { get; set; }

        bool NullSuppression { get; set; }
    }
}
