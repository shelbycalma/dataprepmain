namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public interface IHierarchy
    {
        Level GetLevelByFullName(string levelUniqueName);
        Level GetLevel(string levelName);
    }
}