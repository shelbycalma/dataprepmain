﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class Hierarchy : MetadataElementBase, IHierarchy
    {
        public Hierarchy()
        {
            LevelInfos = new List<Level>();
        }

        public string ParentCubeId { get; set; }

        public string DefaultMember { get; set; }
        public string DisplayFolder { get; set; }
        public HierarchyOrigin HierarchyOrigin { get; set; }
        public string ParentDimensionId { get; set; }
        public string AllMemberUniqueName { get; set; }
        public IList<Level> LevelInfos { get; set; }

        public Level GetLevelByFullName(string levelUniqueName)
        {
            return LevelInfos != null
                       ? LevelInfos.FirstOrDefault(
                           propInfo => propInfo.UniqueName.Equals(levelUniqueName, StringComparison.OrdinalIgnoreCase))
                       : null;
        }

        public Level GetLevel(string levelName)
        {
            var levelUniqueName = string.Format("{0}.{1}", UniqueName, levelName);
            return GetLevelByFullName(levelUniqueName);
        }
    }
}