﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class Measure : MetadataElementBase
    {
        private const string MEASUREGROUP_NAME = "MEASUREGROUP_NAME";

        public string ParentCubeId { get; set; }
        public string DisplayFolder { get; set; }
        public string Expression { get; set; }

        public string MeasureGroup
        {
            get
            {
                PropertyInfo pi = GetPropertyInfo(MEASUREGROUP_NAME);
                return pi != null && pi.Value != null
                           ? pi.Value.ToString()
                           : null;
            }
        }
    }
}