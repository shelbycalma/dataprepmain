﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public enum HierarchyOrigin
    {
        UserHierarchy = 1,
        AttributeHierarchy = 2,
        ParentChildHierarchy = 3,
    }
}