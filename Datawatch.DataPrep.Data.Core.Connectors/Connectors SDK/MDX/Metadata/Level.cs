﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class Level : MetadataElementBase
    {
        public Level()
        {
            LevelPropertyInfos = new List<LevelProperty>();
        }

        public string ParentCubeId { get; set; }
        public string ParentDimensionId { get; set; }
        public string ParentHirerachyId { get; set; }
        public int LevelNumber { get; set; }
        public LevelType LevelType { get; set; }
        public long MemberCount { get; set; }
        public IList<LevelProperty> LevelPropertyInfos { get; set; }

        public LevelProperty GetLevelPropertyInfoByName(string propName)
        {
            return LevelPropertyInfos != null
                       ? LevelPropertyInfos.FirstOrDefault(propInfo => propInfo.Name.Equals(propName, StringComparison.OrdinalIgnoreCase) )
                       : null;
        }
    }
}