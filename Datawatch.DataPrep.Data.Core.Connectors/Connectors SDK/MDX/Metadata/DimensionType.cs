﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public enum DimensionType
    {
        Accounts = 6,
        BillOfMaterials = 0x10,
        Channel = 13,
        Currency = 11,
        Customers = 7,
        Geography = 0x11,
        Measure = 2,
        Organization = 15,
        Other = 3,
        Products = 8,
        Promotion = 14,
        Quantitative = 5,
        Rates = 12,
        Scenario = 9,
        Time = 1,
        Unknown = 0,
        Utility = 10
    }
}