﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class LevelProperty : MetadataElementBase
    {
        public string ParentLevelId { get; set; }
        public int PropertyType { get; set; }
    }
}