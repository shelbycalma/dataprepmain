﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class MetadataElementBase : PropertiesBase
    {
        public MetadataElementBase()
        {
            Caption = string.Empty;
            Description = string.Empty;
            Name = string.Empty;
        }

        public string Caption { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }

        public string UniqueName { get; set; }

        protected bool Equals(MetadataElementBase other)
        {
            return string.Equals(this.UniqueName, other.UniqueName);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((MetadataElementBase)obj);
        }

        public override int GetHashCode()
        {
            return (this.UniqueName != null ? this.UniqueName.GetHashCode() : 0);
        }
    }
}