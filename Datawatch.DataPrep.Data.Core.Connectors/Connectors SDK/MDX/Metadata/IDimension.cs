﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public interface IDimension
    {
        Hierarchy GetHierarchyByFullName(string hierarchyFullName);
        Hierarchy GetHierarchy(string hierarchyName);
    }
}