﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public interface ICube
    {
        Dimension GetDimenision(string dimensionName);
        Measure GetMeasure(string measureName);
    }
}