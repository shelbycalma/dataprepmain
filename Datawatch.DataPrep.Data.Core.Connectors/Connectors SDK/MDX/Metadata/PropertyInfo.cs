﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class PropertyInfo
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}