﻿using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class Dimension : MetadataElementBase, IDimension
    {
        public Dimension()
        {
            UniqueName = string.Empty;
            DimensionType = DimensionType.Unknown;
            HierarchyInfos = new List<Hierarchy>();
        }

        public DimensionType DimensionType { get; set; }
        public string ParentCubeId { get; set; }
        public IList<Hierarchy> HierarchyInfos { get; set; }

        public Hierarchy GetHierarchyByFullName(string hierarchyFullName)
        {
            return !string.IsNullOrWhiteSpace(hierarchyFullName)
                       ? HierarchyInfos.FirstOrDefault(hierarchy => hierarchy.UniqueName == hierarchyFullName)
                       : null;
        }

        public Hierarchy GetHierarchy(string hierarchyName)
        {
            string hierarchyNameFullName = string.Format("{0}.{1}", this.UniqueName, hierarchyName);
            return !string.IsNullOrWhiteSpace(hierarchyName)
                       ? HierarchyInfos.FirstOrDefault(hierarchy => hierarchy.UniqueName == hierarchyNameFullName)
                       : null;
        }
    }
}