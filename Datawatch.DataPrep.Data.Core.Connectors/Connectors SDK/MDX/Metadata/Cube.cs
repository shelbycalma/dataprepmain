﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class Cube : MetadataElementBase, ICube
    {
        public Cube()
        {
            Dimensions = new List<Dimension>();
            Measures = new List<Measure>();
        }

        public DateTime LastProcessed { get; set; }
        public DateTime LastUpdated { get; set; }

        public CubeType Type { get; set; }

        public IList<MeasureGroup> MeasureGroups { get; set; }
        public IList<Dimension> Dimensions { get; set; }

        public IList<Measure> Measures { get; set; }

        public Dimension GetDimenision(string dimensionName)
        {
            return !string.IsNullOrWhiteSpace(dimensionName)
                       ? Dimensions.FirstOrDefault(dimension => dimension.UniqueName == dimensionName)
                       : null;
        }

        public Measure GetMeasure(string measureName)
        {
            return !string.IsNullOrWhiteSpace(measureName)
                       ? Measures.FirstOrDefault(measure => measure.UniqueName == measureName)
                       : null;
        }
    }
}