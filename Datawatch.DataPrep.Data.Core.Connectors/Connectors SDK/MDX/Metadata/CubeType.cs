﻿namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public enum CubeType
    {
        Unknown,
        Cube,
        Dimension
    }
}