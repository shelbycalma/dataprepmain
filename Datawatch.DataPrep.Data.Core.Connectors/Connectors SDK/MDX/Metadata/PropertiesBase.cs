﻿using System.Collections.Generic;
using System.Linq;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class PropertiesBase
    {
        public const string DIMENSION_CAPTION = "DIMENSION_CAPTION";
        public const string HIERARCHY_CAPTION = "HIERARCHY_CAPTION";
        public const string LEVEL_CAPTION = "LEVEL_CAPTION";
        public const string CUBE_CAPTION = "CUBE_CAPTION";

        public PropertiesBase()
        {
            PropertyInfos = new List<PropertyInfo>();
            CustomPropertyInfos = new List<PropertyInfo>();
        }

        public IList<PropertyInfo> PropertyInfos { get; private set; }
        public IList<PropertyInfo> CustomPropertyInfos { get; private set; }

        public PropertyInfo GetPropertyInfo(string propertyName)
        {
            return PropertyInfos.FirstOrDefault(prInf => prInf.Name == propertyName);
        }

        public object GetPropertyValue(string propertyName)
        {
            PropertyInfo resultPropInfo = GetPropertyInfo(propertyName);
            return resultPropInfo != null ? resultPropInfo.Value : null;
        }

        public PropertyInfo GetCustomPropertyInfo(string propertyName)
        {
            return CustomPropertyInfos.FirstOrDefault(prInf => prInf.Name == propertyName);
        }

        public object GetCustomPropertyValue(string propertyName)
        {
            PropertyInfo resultPropInfo = GetCustomPropertyInfo(propertyName);
            return resultPropInfo != null ? resultPropInfo.Value : null;
        }
    }
}