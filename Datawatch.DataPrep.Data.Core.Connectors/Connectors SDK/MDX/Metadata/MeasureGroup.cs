﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata
{
    public class MeasureGroup : MetadataElementBase
    {
        public MeasureGroup()
        {
            Measures = new List<string>();
            Dimensions = new List<string>();
        }

        public string CatalogName { get; set; }
        public string CubeName { get; set; }
        public IList<string> Measures { get; set; }
        public IList<string> Dimensions { get; set; }
    }
}