﻿using System.Linq;
using System.Text;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class MDXUtils
    {
        private const char Separator = '/';
        private const char Space = ' ';
        public static string GetHierarchyName(string hierarchyLevelName)
        {
            if (hierarchyLevelName.Contains(Separator))
            {
                var splitted = hierarchyLevelName.Split(Separator);
                if (splitted.Length == 2)
                {
                    return splitted[0];
                }
            }
            return hierarchyLevelName;
        }

        public static double GetNumericValue(object value)
        {
            double result = double.NaN;
            if (value == null)
            {
                return result;
            }
            if (value is double)
            {
                return (double)value;
            }
            //Simply casting to double in both cases doesn't work
            //The value provided seems to have some kind of "special" type
            //Didn't investigate too deeply, but this works
            if (value is int)
            {
                return (int)value;
            }
            double.TryParse(value.ToString(), out result);
            return result;
        }

        //The "special case" is when hierarchy is not top level

        public static string GetSpecialCaseHierarchyName(string hierarchyName)
        {
            if (!hierarchyName.Contains(Space))
            {
                return hierarchyName;
            }

            string[] splitted = hierarchyName.Split(Space);
            if (splitted.Length != 2)
            {
                return hierarchyName;
            }
            StringBuilder realNameBuilder = new StringBuilder();
            realNameBuilder.Append(splitted[0]);
            return realNameBuilder.ToString();
        }

        //TODO research, cause this seems like an application issue
        public static string GetLevelName(string hierarchyLevelName)
        {
            if (hierarchyLevelName.Contains(Separator))
            {
                var splitted = hierarchyLevelName.Split(Separator);
                if (splitted.Length == 2)
                {
                    return splitted[1];
                }
            }
            return hierarchyLevelName;
        }

        public static string CreateHierarchyLevelName(string hierarchy, string level)
        {
            return hierarchy + Separator + level;
        }
    }
}
