﻿using System;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public interface IQueryResult
    {
        string[] Names { get; }
        int RowCount { get; }
        int ColumnCount { get; }
        object GetValue(int column, int row);
        Type GetFieldType(int ordinal);
    }
}
