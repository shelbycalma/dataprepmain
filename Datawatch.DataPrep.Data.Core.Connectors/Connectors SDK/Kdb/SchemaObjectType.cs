﻿namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public enum SchemaObjectType
    {
        Table,
        Columns,
        Views
    }
}
