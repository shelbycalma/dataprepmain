﻿using System;
using System.Collections.Generic;
using System.Text;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public static class KdbUtil
    {
        public const string DateTimeFormat = "yyyy'.'MM'.'dd'T'HH':'mm':'ss";
        public static readonly string TimeWindowStart = "TimeWindowStart";
        public static readonly string TimeWindowEnd = "TimeWindowEnd";
        public static readonly string Snapshot = "Snapshot";
        public const string StaticDateColumnName = "TODAY";
        private const string TimeZoneHelperKey = "TimeZoneHelper";
        private const string StaticDateColumnValue = ".z.d";

        private static string QueryMemoryCheck = ".Q.w[]";
        public const string deferredSyncQueryParameter = "{Query}";

        public static string DefaultDeferredSyncQuery =
            "{@[neg .z.w;@[value;x;`$\"" +
            Properties.Resources.UiKDBfailedToRunQuery +
            "\"];`$\"" +
            Properties.Resources.UiKDBfailedToPostback +
            "\"]}[\"" +
            deferredSyncQueryParameter + "\"]";

        public static readonly string[] SpecialParameters = new[]
        {            
            TimeWindowStart,
            TimeWindowEnd,
            Snapshot
        };

        public static bool IsSpecialParameter(string paramName)
        {
            if (string.IsNullOrEmpty(paramName))
            {
                return false;
            }
            foreach (string specialParam in SpecialParameters)
            {
                if (specialParam == paramName)
                    return true;
            }
            return false;
        }

        public static string ApplyDeferredSyncQuery(bool isDeferredSyncQuery,
            string deferredSyncQuery, string query)
        {
            if (isDeferredSyncQuery)
            {
                query = query.Replace("\"", "\\\"").
                    Replace(System.Environment.NewLine, " ");
                query = deferredSyncQuery.Replace(
                    KdbUtil.deferredSyncQueryParameter, query);
            }
            return query;
        }

        public static string ApplySpecialParameters(string sourceString,
            IEnumerable<ParameterValue> parameters, TimeZoneHelper timeZoneHelper)
        {
            if (!timeZoneHelper.TimeZoneSelected)
            {
                return sourceString;
            }

            try
            {
                if (parameters == null) return sourceString;

                foreach (string specialParam in SpecialParameters)
                {
                    string oldKey = "$" + specialParam;
                    string newKey = "{" + specialParam + "}";
                    if (!(sourceString.IndexOf(oldKey) != -1
                    || sourceString.IndexOf(newKey) != -1))
                    {
                        continue; // parameter not used in source string
                    }

                    while (sourceString.Contains(oldKey))
                    {
                        sourceString = sourceString.Replace(oldKey, newKey);
                    }

                    foreach (ParameterValue parameterValue in parameters)
                    {
                        if (string.IsNullOrEmpty(parameterValue.Name) ||
                            parameterValue.TypedValue == null)
                        {
                            continue;
                        }

                        if (specialParam.Equals(parameterValue.Name) &&
                            parameterValue.TypedValue is DateTimeParameterValue)
                        {
                            DateTime value = ((DateTimeParameterValue)
                                parameterValue.TypedValue).Value;
                            value = timeZoneHelper.ConvertToUTC(value);
                            string strValue = value.ToString(DateTimeFormat);
                            sourceString = sourceString.Replace(
                                    newKey, strValue);
                            break;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                Log.Error(Resources.LogReplacingParameterError,
                    sourceString);
            }
            return sourceString;
        }

        public static string ApplyArrayParameters(string sourceString,
            IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(sourceString)) return null;
            if (parameters == null) return sourceString;

            StringBuilder paramValue = new StringBuilder(string.Empty);

            foreach (ParameterValue parameterValue in parameters)
            {
                if (parameterValue.Name.Equals(sourceString) &&
                    parameterValue.TypedValue is ArrayParameterValue)
                {
                    TypedParameterValue[] paramArray =
                    ((ArrayParameterValue)parameterValue.TypedValue).Values.ToArray();

                    for (int i = 0; i < paramArray.Length; i++)
                    {
                        if ((paramArray[i] is StringParameterValue))
                        {
                            paramValue.Append("\"" + paramArray[i] + "\"");
                        }
                        else
                        {
                            paramValue.Append(paramArray[i].ToString());
                        }
                        paramValue.Append(";");
                    }
                }
            }
            paramValue.Length--;
            return paramValue.ToString();
        }

        public static string ApplyParameters(string sourceString,
            IEnumerable<ParameterValue> parameters, QueryMode queryMode)
        {
            if (queryMode == QueryMode.Query)
            {
                return KdbUtil.ApplyParameters(sourceString, parameters, null);
            }
            return KdbUtil.ApplyParameters(sourceString, parameters);
        }

        public static string ApplyParameters(string sourceString,
            IEnumerable<ParameterValue> parameters, string arraySeparator = ";")
        {
            if (string.IsNullOrEmpty(sourceString)) return null;
            if (parameters == null) return sourceString;

            try
            {

                ParameterEncoder paramEncoder = new ParameterEncoder
                {
                    SourceString = sourceString,
                    DefaultDateTimeFormat = DateTimeFormat,
                    Parameters = parameters
                };

                if (!string.IsNullOrEmpty(arraySeparator))
                {
                    paramEncoder.DefaultArraySeparator = arraySeparator;
                }
                return paramEncoder.Encoded();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                Log.Error(Resources.LogReplacingParametersSourceString,
                    sourceString);
            }
            return sourceString;
        }

        public static string ConvertToUTC(string sourceString,
            TimeZoneHelper timeZoneHelper,
            out bool isSourceStringValidDate)
        {

            //Apply the time zone offset,from required time zone to UTC 
            string adjustedString = sourceString;
            DateTime adjustedDateTime;
            isSourceStringValidDate = false;
            if(!IsDate(sourceString, out adjustedDateTime))
            {
                isSourceStringValidDate = false;
                return sourceString;
            }

            if(timeZoneHelper.TimeZoneSelected)
            {
                adjustedDateTime =
                    timeZoneHelper.ConvertToUTC(adjustedDateTime);                    
            }
            //convert it to required format and append UTC
            adjustedString = adjustedDateTime.ToString(DateTimeFormat);
            isSourceStringValidDate = true;

            return adjustedString;
        }

        public static IQueryResult DoQuery(string query,
            c cInstance, int requestId = -1, bool deferredSync = false,
            bool isQoD = false)
        {
            string logMessage = string.Format(
                Resources.LogPluginExecutingKdbQuery, query);
            if (isQoD)
            {
                logMessage = string.Format(
                    Resources.LogPluginExecutingKdbQoDQuery, query);
            }

            if (requestId > 0)
            {
                logMessage = string.Format(
                    Resources.LogKDBRequestNumber, requestId, logMessage);
            }
            Log.Info(logMessage);
            DateTime start = DateTime.Now;

            object result;

            if (!deferredSync)
            {
                result = cInstance.k(query);
            }
            else
            {
                cInstance.ks(query);
                result = cInstance.k();
            }

            IQueryResult queryResult = GetQueryResult(result);

            SingleValueQueryResult errorResult =
                queryResult as SingleValueQueryResult;

            if (errorResult != null)
            {
                object resultValue = errorResult.GetValue(0, 0);
                if(resultValue.GetType() == typeof(string))
                {
                    if (resultValue.ToString().Contains(
                        Properties.Resources.UiKDBfailedToPostback) ||
                        resultValue.ToString().Contains(
                        Properties.Resources.UiKDBfailedToRunQuery))
                    {
                        throw new Exception(resultValue.ToString());
                    }
                }
            }

            logMessage = string.Format(
                Resources.LogPluginQueryCompleted, queryResult.RowCount, 
                queryResult.ColumnCount, (DateTime.Now - start).TotalSeconds);
            if (requestId > 0)
            {
                logMessage = string.Format(
                    Resources.LogKDBRequestNumber, requestId, logMessage);
            }
            Log.Info(logMessage);

            return queryResult;
        }

        public static IQueryResult GetQueryResult(object result)
        {
            IQueryResult queryResult;
            if (SingleValueQueryResult.IsSingle(result))
            {
                queryResult = new SingleValueQueryResult(result);
            }
            else if (ListQueryResult.IsList(result))
            {
                queryResult = new ListQueryResult(result);
            }
            else if (DictQueryResult.IsDict(result))
            {
                queryResult = new DictQueryResult(result);
            }
            else if (MetaQueryResult.IsMeta(result))
            {
                queryResult = new MetaQueryResult(result);
            }
            else
            {
                queryResult = new FlipQueryResult(result);
            }
            return queryResult;
        }

        public static void ExecuteNonQuery(string query, c cInstance)
        {
            cInstance.k(query);
        }

        public static void ExecuteInsert(string tablename, 
            object[] values, c cInstance)
        {
            cInstance.k("insert", tablename, values);
        }

        public static List<string> FilterColumnsList(
            IEnumerable<DatabaseColumn> allColumns, char[] columnTypes)
        {
            List<string> filteredColumns = new List<string>();

            foreach (char columnType in columnTypes)
            {
                foreach (KdbColumn col in allColumns)
                {
                    if (col.KdbType == columnType)
                    {
                        filteredColumns.Add(col.ColumnName);
                    }
                }
            }
            return filteredColumns;
        }

        public static KdbTuple GetTupleFromArrayResult(ArrayQueryResult result,
            int rowIndex)
        {
            KdbTuple tuple = new KdbTuple();
            string[] names = result.Names;
            for (int j = 0; j < result.ColumnCount; j++)
            {
                KdbValue espValue = new KdbValue();

                object value = result.GetValue(j, rowIndex);
                espValue.Value = value;

                tuple.Values.Add(names[j], espValue);
            }
            return tuple;
        }

        public static KdbTuple GetTupleFromQueryResult(IQueryResult result,
            int rowIndex)
        {
            KdbTuple tuple = new KdbTuple();
            string[] names = result.Names;
            for (int j = 0; j < result.ColumnCount; j++)
            {
                KdbValue espValue = new KdbValue();

                object value = result.GetValue(j, rowIndex);
                espValue.Value = value;

                tuple.Values.Add(names[j], espValue);
            }
            return tuple;
        }

        public static void CloseConnection(c cInstance)
        {
            if (cInstance != null)
            {
                try
                {
                    cInstance.Close();
                }
                catch
                {
                }
                cInstance = null;
            }
        }

        public static string ConcatenateValues(object value)
        {
            string result = string.Empty;
            if (value.GetType() == typeof(string[]))
            {
                string[] res = (string[])value;
                for (int i = 0; i < res.Length; i++)
                {
                    result += res[i] + ".";
                }
            }
            return result.TrimEnd('.');
        }

        public static DateTime ConvertFromTimeSpan(object source,
            TimeZoneHelper timeZoneHelper)
        {
            if (source is TimeSpan)
            {
                if ((Nullable<TimeSpan>)source != (Nullable<TimeSpan>)null)
                {
                    TimeSpan t = (TimeSpan)source;
                    return GetDateFromTimespan(timeZoneHelper, t);
                }

            }
            return TimeValue.Empty;
        }

        public static DateTime ConvertFromKTimeSpan(object source,
            TimeZoneHelper timeZoneHelper)
        {
            if (source is c.KTimespan)
            {
                if (source != null)
                {
                    c.KTimespan t = (c.KTimespan)source;
                    return GetDateFromTimespan(timeZoneHelper, t.t);
                }
            }
            return TimeValue.Empty;
        }

        private static DateTime GetDateFromTimespan(
            TimeZoneHelper timeZoneHelper, TimeSpan t)
        {
            DateTime dt = TimeValue.Empty;
            dt = DateTime.SpecifyKind(DateTime.Today.AddTicks(t.Ticks),
                DateTimeKind.Unspecified);
            //apply timezone offset
            if (timeZoneHelper != null && timeZoneHelper.TimeZoneSelected)
            {
                dt = timeZoneHelper.ConvertFromUTC(dt);
            }
            return dt;
        }

        static bool IsDate(string inputDate, out DateTime dt)
        {
            return DateTime.TryParse(inputDate, out dt);
        }

        public static TimeZoneHelper GetTimeZoneHelper(PropertyBag bag)
        {
            //TODO: This should be shifted to TimeZoneHelper,
            //as this may be required in other connector using old timezone implementation.
            TimeZoneHelper timeZoneHelper;

            PropertyBag timeZoneHelperBag =
               bag.SubGroups[TimeZoneHelperKey];
            bool isOldWorkbook = false;
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups[TimeZoneHelperKey] = timeZoneHelperBag;
                isOldWorkbook = true;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);

            //For backward compatibility
            if (isOldWorkbook)
            {
                string timeZone = bag.Values["TimeZone"];
                if (!string.IsNullOrEmpty(timeZone) && !timeZone.Equals("null"))
                {
                    timeZoneHelper.SelectedTimeZone = timeZone;
                }
            }

            return timeZoneHelper;
        }

        public static string CheckValueWrap(string value)
        {
            StringBuilder strb = new StringBuilder();
            strb.Append(value.Replace("\"", "\\\"")
                .Replace(System.Environment.NewLine, string.Empty));
            return strb.ToString();
        }

        public static string AppendPeriodAggregation(AggregationPeriodType periodType,
           string periodText, string dateTimeColumnName, string periodColName,
           bool isCompositeDateTimeColumnSelected = false,
           string dateColumnName = null, string timeColumnName = null)
        {
            StringBuilder sbQuery = new StringBuilder();
            sbQuery.Append(periodColName);
            sbQuery.Append(" : ");

            if (periodType == AggregationPeriodType.second ||
                periodType == AggregationPeriodType.minute)
            {
                //if aggregation period type is Second or Minute 
                //then Encase the period group by section including the date in brackets,
                //and shift the column alias “bar” to the left of the date element
                //constructing bar : (LAST_TRADE_TIME.date + 1 xbar LAST_TRADE_TIME.second)
                sbQuery.Append(" (");
                if (string.IsNullOrEmpty(dateColumnName))
                {
                    sbQuery.Append(dateTimeColumnName);
                    sbQuery.Append(".");
                    sbQuery.Append("date");
                }
                else
                {
                    sbQuery.Append(GetDateColumn(dateColumnName));
                }

                sbQuery.Append(" + ");
                sbQuery.Append(periodText);
                sbQuery.Append(" xbar ");

                if (string.IsNullOrEmpty(timeColumnName))
                {
                    sbQuery.Append(dateTimeColumnName);
                }
                else
                {
                    sbQuery.Append(timeColumnName);
                }
                sbQuery.Append(".");
                sbQuery.Append(periodType.ToString());
                sbQuery.Append(")");
            }
            else if (periodType == AggregationPeriodType.hh)
            {
                int hhInterval;
                //if aggregation period type is hour 
                //then same as second/minute except we need to convert period text to minute.
                //constructing bar: (LAST_TRADE_TIME.date + (60 xbar LAST_TRADE_TIME.minute))
                sbQuery.Append(" (");
                if (string.IsNullOrEmpty(dateColumnName))
                {
                    sbQuery.Append(dateTimeColumnName);
                    sbQuery.Append(".");
                    sbQuery.Append("date");
                }
                else
                {
                    sbQuery.Append(GetDateColumn(dateColumnName));
                }

                sbQuery.Append(" + (");

                if (int.TryParse(periodText, out hhInterval))
                {
                    hhInterval *= 60;
                    sbQuery.Append(hhInterval);
                }
                else
                {
                    sbQuery.Append(periodText);
                }

                sbQuery.Append(" xbar ");

                if (string.IsNullOrEmpty(timeColumnName))
                {
                    sbQuery.Append(dateTimeColumnName);
                }
                else
                {
                    sbQuery.Append(timeColumnName);
                }
                sbQuery.Append(".");
                sbQuery.Append(AggregationPeriodType.minute.ToString());
                sbQuery.Append("))");
            }
            else if (periodType == AggregationPeriodType.date ||
                periodType == AggregationPeriodType.week ||
                periodType == AggregationPeriodType.month)
            {
                if (periodType == AggregationPeriodType.month)
                {
                    sbQuery.Append("\"d\"$");
                }

                //constructing bar:1 xbar LAST_TRADE_TIME.date
                sbQuery.Append(" (");
                sbQuery.Append(periodText);
                sbQuery.Append(" xbar ");
                if (string.IsNullOrEmpty(dateColumnName))
                {
                    sbQuery.Append(dateTimeColumnName);
                    sbQuery.Append(".");
                    sbQuery.Append(periodType.ToString());
                }
                else if (isCompositeDateTimeColumnSelected)
                {
                    sbQuery.Append(GetDateColumn(dateColumnName));
                }
                sbQuery.Append(")");
            }
            return sbQuery.ToString();
        }

        public static void LogMemoryCheck(c cInstance, bool doLog)
        {
            if (!doLog) return;

            IQueryResult result = KdbUtil.DoQuery(QueryMemoryCheck, 
                cInstance);
            if (result == null) return;
            StringBuilder logLine = new StringBuilder();
            logLine.Append(Resources.LogKDBMemoryCheck);
            for (int i = 0; i < result.RowCount; i++)
            {
                logLine.AppendFormat(" {0} - {1};", result.GetValue(0, i),
                    result.GetValue(1, i));
            }
            Log.Info(logLine.ToString());
        }

        public static string GetCacheKey(IKdbConnectionSettings settings,
            IEnumerable<ParameterValue> parameters = null)
        {
            string host = KdbUtil.ApplyParameters(settings.Host, parameters);
            string port = KdbUtil.ApplyParameters(settings.Port, parameters);
            string username = KdbUtil.ApplyParameters(
                    settings.UserName, parameters);

            return string.Format("Kx QoD {0},{1},{2}",
                host, port, username);
        }

        public static c GetConnection(IKdbConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            string host = KdbUtil.ApplyParameters(settings.Host, parameters);
            string port = KdbUtil.ApplyParameters(settings.Port, parameters);
            int portAsInt;

            if (!int.TryParse(port, out portAsInt))
            {
                throw new Exception(string.Format(
                            Properties.Resources.ExInvalidPort, port));
            }

            string userCredential = string.Empty;

            string username = KdbUtil.ApplyParameters(
                    settings.UserName, parameters);

            if (!string.IsNullOrEmpty(settings.UserName))
            {   
                string password = settings.Password;

                if (settings.IsPasswordEncrypted)
                {
                    password = SecurityUtils.ToInsecureString(
                        SecurityUtils.DecryptString(password));
                }
                password = KdbUtil.ApplyParameters(
                    password, parameters);
                userCredential = username + ":" + password;
            }
            Log.Info(Properties.Resources.LogKdbConnection,
                host, port, username);
            c cInstance = new c(
                    host,
                    portAsInt,
                    userCredential);
            cInstance.ReceiveTimeout = settings.Timeout * 1000;

            return cInstance;
        }

        public static string GetDateConstraint(TimeZoneHelper timeZoneHelper,
            IEnumerable<ParameterValue> parameters, string dateString,
            string datecolumn = null, string timecolumn = null)
        {
            bool isValidFromDate;
            dateString = ApplyParameters(dateString, parameters);
            //no date string parsing in case of only time column selected
            if (!string.IsNullOrEmpty(timecolumn) &&
                string.IsNullOrEmpty(datecolumn))
            {
                return dateString;
            }
            //Convert from and to dates to UTC
            dateString = ConvertToUTC(dateString, timeZoneHelper,
                out isValidFromDate);
            return dateString;
        }

        public static AggregateType GetDefaultAggregationMethod(
            ColumnType type)
        {
            if (type == ColumnType.Numeric)
            {
                return AggregateType.Sum;
            }
            else
            {
                return AggregateType.None;
            }
        }

        public static string GetKeyByValue(
            Dictionary<string, string> columnMap,
            string odDomain)
        {
            string domain = odDomain;
            foreach (string key in columnMap.Keys)
            {
                if (columnMap[key] == domain)
                {
                    domain = key;
                }
            }
            return domain;
        }

        private static bool IsArrayParameter(
            IEnumerable<ParameterValue> parameters, string paramName)
        {
            foreach (ParameterValue paramValue in parameters)
            {
                if (paramValue.Name.Equals(paramName) &&
                    paramValue.TypedValue is ArrayParameterValue)
                {
                    return true;
                }
            }
            return false;
        }

        private static bool IsParameterExists(
            IEnumerable<ParameterValue> parameters, string paramName)
        {
            if (parameters == null) return false;

            foreach (ParameterValue paramValue in parameters)
            {
                if (paramValue.Name.Equals(paramName))
                {
                    return true;
                }
            }
            return false;
        }

        public static Predicate GetParameterizedPredicate(
            KdbQuerySettings kdbQuerySettings,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> columnMap)
        {
            Predicate parameterPredicate = null;
            if (kdbQuerySettings.AutoParameterize &&
                kdbQuerySettings.PredicateColumns != null &&
                kdbQuerySettings.PredicateColumns.Count > 0)
            {
                string valueParam = "";
                string parameterValue = "";
                foreach (DatabaseColumn column in kdbQuerySettings.PredicateColumns)
                {
                    string paramName = column.AppliedParameterName;
                    if (string.IsNullOrEmpty(paramName)) continue;

                    if (!IsParameterExists(parameters, paramName))
                    {
                        valueParam = string.Format(
                                     "`$(\"{0}\")", paramName);
                        parameterValue = column.AppliedParameterName;
                    }
                    else
                    {
                        if (!KdbUtil.IsSpecialParameter(paramName))
                        {
                            if (IsArrayParameter(parameters, paramName))
                            {
                                parameterValue =
                                    KdbUtil.ApplyArrayParameters(paramName,
                                    parameters);

                                valueParam = string.Format(
                                         "`$({0})", parameterValue);
                            }
                            else
                            {
                                parameterValue = "{" + paramName + "}";

                                parameterValue =
                                    KdbUtil.ApplyParameters(parameterValue,
                                    parameters);

                                valueParam = string.Format(
                                         "`$(\"{0}\")", parameterValue);
                            }
                        }
                    }
                    string columnName = column.ColumnName;
                    if (columnMap != null)
                    {
                        columnName = GetKeyByValue(columnMap,
                            columnName);
                    }

                    switch (column.ColumnType)
                    {
                        case ColumnType.Numeric:
                            {
                                valueParam = "( " + parameterValue + " )";
                                break;
                            }
                        case ColumnType.Time:
                            {
                                valueParam = KdbUtil.GetDateConstraint(
                                    kdbQuerySettings.TimeZoneHelper,
                                    parameters, parameterValue);
                                break;
                            }
                    }
                    AndOperator paramPredicate = new AndOperator(parameterPredicate,
                        new Comparison(Operator.In,
                        new ColumnParameter(columnName),
                        new ValueParameter(valueParam)));
                    paramPredicate.UsePredicateSeparator = true;
                    parameterPredicate = paramPredicate;
                }
            }
            return parameterPredicate;
        }

        public static string GetPridicateString(Predicate predicate)
        {
            if (predicate != null)
            {
                return predicate.ToString(SqlDialectFactory.KxQ);
            }

            return null;
        }

        public static string GetDateColumn(string dateColumn)
        {
            if (!dateColumn.Equals(KdbUtil.StaticDateColumnName))
            {
                return dateColumn;
            }
            else
            {
                return StaticDateColumnValue;
            }
        }

        public static void ValidateDeferredSyncQuery(bool isDeferredSyncQuery,
            string deferredSyncQuery)
        {
            if (isDeferredSyncQuery)
            {
                if (string.IsNullOrEmpty(deferredSyncQuery))
                {
                    throw new Exception(
                        Properties.Resources.ExEmptyDeferredSyncQuery);
                }
                if (deferredSyncQuery.IndexOf(
                    KdbUtil.deferredSyncQueryParameter) == -1)
                {
                    throw new Exception(
                        Properties.Resources.ExInvalidDeferredSyncQuery);
                }
            }
        }

    }
}
