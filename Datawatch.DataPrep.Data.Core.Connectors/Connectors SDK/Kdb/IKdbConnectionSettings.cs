﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public interface IKdbConnectionSettings
    {
        string Host { get; set; }
        string Port { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        int Timeout { get; set; }
        bool IsPasswordEncrypted { get; }
        c GetConnection(IEnumerable<ParameterValue> parameters);
    }
}
