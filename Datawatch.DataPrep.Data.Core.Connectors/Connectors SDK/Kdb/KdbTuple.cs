﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class KdbTuple
    {
        public Dictionary<string, KdbValue> Values { get; set; }
        public KdbTuple()
        {
            Values = new Dictionary<string, KdbValue>();
        }
    }
}
