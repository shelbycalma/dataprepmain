﻿using System;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public abstract class QueryResult : IQueryResult
    {
        public string[] Names { get; protected set; }
        public int RowCount { get; protected set; }
        public int ColumnCount { get; protected set; }

        public abstract object GetValue(int column, int row);

        public virtual Type GetFieldType(int ordinal)
        {
            object columnData = GetValue(ordinal, 0);
            if (columnData == null)
            {
                throw new Exception(
                    Resources.ExInvalidQueryResult);
            }
            return columnData.GetType();
        }
    }
}
