﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Connectors.Query.Q;
using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public abstract class KdbExecutorBase
    {
        private readonly ColumnDomainCache columnDomainCache;
        private readonly OnDemandSchemaTableCache schemaTableCache;
        protected const string schemaQuery = "meta[{0}]";

        private int nextRequestId;

        public KdbExecutorBase()
        {
            columnDomainCache = new ColumnDomainCache();
            schemaTableCache = new OnDemandSchemaTableCache();
        }

        public abstract void AddPredicateColumnsToQuery(
            KdbQuerySettings settings,
            QQueryBuilder queryBuilder,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> columnMap);

        protected string ApplyDeferredSyncQuery(KdbQuerySettings querySettings,
            string query)
        {
            //TODO: should remove the method wrap.
            return KdbUtil.ApplyDeferredSyncQuery(
                querySettings.IsDeferredSyncQuery,
                querySettings.DeferredSyncQuery, query);
        }

        public QQueryBuilder BuildOnDemandRawQuery(
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> columnMap,
            string selectedTableName)
        {
            QQueryBuilder queryBuilder = new QQueryBuilder();

            if (querySettings.QueryMode == QueryMode.Table)
            {
                queryBuilder.SchemaAndTable =
                        SchemaAndTable.Parse(selectedTableName);
                AddPredicateColumnsToQuery(querySettings, queryBuilder,
                    parameters, columnMap);
            }
            else
            {
                queryBuilder.SourceQuery =
                    KdbUtil.ApplyParameters(querySettings.Query,
                    parameters, querySettings.QueryMode);
            }

            return queryBuilder;
        }

        protected static string BuildOnDemandSchemaQuery(QQueryBuilder queryBuilder)
        {
            string metaQuery = "";

            if (!string.IsNullOrEmpty(queryBuilder.SourceQuery))
            {
                metaQuery = queryBuilder.SourceQuery;
            }
            else
            {

                metaQuery = queryBuilder.SchemaAndTable.ToString();
            }

            return string.Format(schemaQuery, metaQuery);
        }

        protected string CheckPassToFunctionQuery(string query,
            bool isPassToFunction, string functionName)
        {
            if (isPassToFunction)
            {
                StringBuilder strb = new StringBuilder(functionName);
                strb.Append(" \"");
                strb.Append(query.Replace("\"", "\\\"")
                    .Replace(System.Environment.NewLine, string.Empty));
                strb.Append("\"");
                return strb.ToString();
            }
            return query;
        }

        private static string GetKeyByValue(
            Dictionary<string, string> columnMap,
            string odDomain)
        {
            string domain = odDomain;
            foreach (string key in columnMap.Keys)
            {
                if (columnMap[key] == domain)
                {
                    domain = key;
                }
            }
            return domain;
        }

        private static string GetCacheKey(IKdbConnectionSettings settings)
        {
            return string.Format("Kx QoD {0},{1},{2}",
                settings.Host, settings.Port, settings.UserName);
        }

        public ITable GetOnDemandData(
            IKdbConnectionSettings connectionSettings,
            KdbQuerySettings querySettings,
            Dictionary<string, string> schemaColumnMap,
            IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters,
            string selectedTableName, int rowCount,
            bool useSchemaTableCache = true)
        {
            if (connectionSettings == null)
            {
                throw new ArgumentNullException("connectionSettings");
            }
            if (querySettings == null)
            {
                throw new ArgumentNullException("querySettings");
            }
            if (querySettings.QueryMode == QueryMode.Table &&
                string.IsNullOrEmpty(selectedTableName))
            {
                throw new ArgumentNullException("selectedTableName");
            }
            // TODO: schemaColumnMap should be comming from an abstract method.
            // this would allow queries to be run only from within this class.
            // and few log for queries e.g. ColumnMapping can have requestId.

            // Get an ID to identify this particular request in log.
            int requestId = Interlocked.Increment(ref nextRequestId);

            string strConnSettings = string.Format("{0}:{1};{2}",
                connectionSettings.Host, connectionSettings.Port, 
                connectionSettings.UserName);

            QQueryBuilder queryBuilder =
                BuildOnDemandRawQuery(querySettings, parameters,
                schemaColumnMap, selectedTableName);

            string schemaQuery = BuildOnDemandSchemaQuery(queryBuilder);
            schemaQuery = CheckPassToFunctionQuery(schemaQuery,
                querySettings.PassToFunction, querySettings.FunctionName);
            schemaQuery = ApplyDeferredSyncQuery(querySettings, schemaQuery);

            List<string> selectedColumns = 
                querySettings.GetSelectedColumnNames();

            OnDemandSchemaTableCacheKey schemaTableCacheKey =
                new OnDemandSchemaTableCacheKey(schemaQuery,
                    strConnSettings, selectedColumns.ToArray());

            c cInstance = null;
            IQueryResult query = null;
            ParameterTable dataTable = null;

            if (useSchemaTableCache)
            {
                dataTable =
                    (ParameterTable)schemaTableCache[schemaTableCacheKey];
            }

            if (dataTable == null)
            {
                if (useSchemaTableCache == false && 
                    querySettings.SchemaColumnsSettings.SchemaColumns.Count > 0)
                {
                    dataTable = TableBuilder.CreateTableFromSavedSchema(
                        querySettings.SchemaColumnsSettings, parameters);
                }
                else
                {
                    Log.Info(Resources.LogExecutingSchemaQuery, requestId,
                        schemaQuery);
                    cInstance = connectionSettings.GetConnection(parameters);
                    try
                    {
                        // Get the schema as a DataTable. 
                        query = KdbUtil.DoQuery(KdbUtil.ApplyParameters(
                            schemaQuery, parameters, querySettings.QueryMode),
                            cInstance, requestId,
                            querySettings.IsDeferredSyncQuery, true);
                    }
                    finally
                    {
                        KdbUtil.CloseConnection(cInstance);
                    }

                    dataTable = TableBuilder.CreateTableSchema(
                        query, useSchemaTableCache ? null :
                        querySettings, parameters, selectedColumns,
                        schemaColumnMap);
                }
                schemaTableCache[schemaTableCacheKey] = dataTable;
            }

            if(querySettings.IsSchemaRequest)
            {
                return dataTable;
            }

            if (onDemandParameters != null &&
                onDemandParameters.Bucketings != null)
            {
                int index = 1;
                foreach (Bucketing bucketing in
                    onDemandParameters.Bucketings)
                {
                    if (!(bucketing is TimeBucketing)) continue;
                    TimeBucketing timeBucketing = (TimeBucketing)bucketing;
                    string newName = "t" + index++;
                    schemaColumnMap.Add(newName, timeBucketing.Name);
                    timeBucketing.Name = newName;
                }
            }

            UpdateColumnName(onDemandParameters, schemaColumnMap);

            if (onDemandParameters is IOnDemandPreviewParameters)
            {
                OnDemandHelper.BuildOnDemandPreviewQuery(queryBuilder,
                    UpdateOnDemandColumns(selectedColumns.ToArray(), schemaColumnMap),
                    rowCount);
            }
            else
            {
                if (onDemandParameters != null && onDemandParameters.Predicates != null)
                {
                    foreach (Predicate predicate in onDemandParameters.Predicates)
                    {
                        Utils.UpdateTimeValueInPredicate(predicate,
                            querySettings, dataTable);
                    }
                }

                OnDemandHelper.BuildOnDemandQuery(
                    queryBuilder, onDemandParameters);
            }

            UpdatePredicate(queryBuilder.Where.Predicate, dataTable, schemaColumnMap);
            
            ParameterTable table;
            if (!(onDemandParameters is IOnDemandPreviewParameters) &&
                queryBuilder.Select.IsEmpty)
            {
                table = dataTable;
            }
            else
            {
                queryBuilder.Limit = rowCount;

                string strQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
                strQuery = CheckPassToFunctionQuery(strQuery,
                querySettings.PassToFunction, querySettings.FunctionName);
                strQuery = ApplyDeferredSyncQuery(querySettings, strQuery);

                cInstance = connectionSettings.GetConnection(parameters);
                try
                {
                    query = KdbUtil.DoQuery(
                        KdbUtil.ApplyParameters(strQuery, parameters, querySettings.QueryMode),
                        cInstance, requestId, querySettings.IsDeferredSyncQuery, true);
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }
                table = TableBuilder.CreateOnDemandTable(query, rowCount,
                    querySettings.TimeZoneHelper, parameters,
                    schemaColumnMap, dataTable);
            }

            SetOnDemandTimeBucketMetaData(table, onDemandParameters,
                schemaColumnMap);

            SetOnDemandColumnDomains(table, connectionSettings, querySettings,
                parameters, onDemandParameters, queryBuilder, rowCount,
                requestId, schemaColumnMap);

            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        protected static void SetOnDemandTimeBucketMetaData(StandaloneTable table,
            OnDemandParameters onDemandParameters,
            Dictionary<string, string> columnMap = null)
        {
            if (onDemandParameters == null ||
                onDemandParameters.Bucketings == null ||
                onDemandParameters.Bucketings.Length == 0)
            {
                return;
            }

            table.BeginUpdate();
            try
            {
                foreach (Bucketing bucketing in onDemandParameters.Bucketings)
                {
                    if (!(bucketing is TimeBucketing)) continue;
                    TimeBucketing timeBucketing = (TimeBucketing)bucketing;
                    string columnName = timeBucketing.Name;
                    if (columnMap != null && columnMap.ContainsKey(columnName))
                    {
                        columnName = columnMap[columnName];
                    }
                    IColumn column = table.GetColumn(columnName);

                    string srcColumnName = timeBucketing.SourceColumnName;
                    if (columnMap != null && columnMap.ContainsKey(srcColumnName))
                    {
                        srcColumnName = columnMap[srcColumnName];
                    }
                    IColumn source = table.GetColumn(srcColumnName);
                    if (!(column is ITextColumn && source is ITimeColumn)) continue;
                    TextColumn textColumn = (TextColumn)column;
                    ITimeColumn timeColumn = (ITimeColumn)source;
                    textColumn.MetaData =
                        new OnDemandTimeBucketTextColumnMetaData(
                            textColumn, timeColumn, timeBucketing.TimePart, timeBucketing.Title);
                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        private void SetOnDemandColumnDomains(StandaloneTable table,
            IKdbConnectionSettings settings,
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters,
            QueryBuilder queryBuilder,
            int rowcount, int requestId,
            Dictionary<string, string> columnMap = null)
        {
            if (onDemandParameters == null ||
                onDemandParameters.Domains == null)
            {
                return;
            }

            table.BeginUpdate();
            try
            {
                foreach (string domain in onDemandParameters.Domains)
                {
                    Column column = table.GetColumn(domain);

                    if (columnMap != null && columnMap.Count > 0
                        && columnMap.ContainsKey(domain))
                    {
                        column = table.GetColumn(columnMap[domain]);
                    }
                    if (column == null) continue;

                    if (column is TextColumn)
                    {
                        // Get the text column (skip if not found).
                        TextColumn textColumn = (TextColumn)column;
                        string[] domainValues;
                        if (column.MetaData is ITimeBucketColumnMetaData)
                        {
                            ITimeBucketColumnMetaData meta =
                                (ITimeBucketColumnMetaData)column.MetaData;
                            domainValues =
                                GetTimeBucketDomainValues(
                                    settings, querySettings, parameters,
                                    queryBuilder, domain, meta.TimeColumn.Name,
                                    meta.TimePart, rowcount, requestId);
                        }
                        else
                        {
                            domainValues =
                                GetTextDomainValues(settings, querySettings,
                                parameters, queryBuilder, domain, rowcount,
                                requestId);
                        }
                        ((IMutableTextColumnMetaData)textColumn.MetaData).Domain
                            = domainValues;
                    }
                    else if (column is NumericColumn)
                    {
                        NumericColumn numericColumn = (NumericColumn)column;
                        ((IMutableNumericColumnMetaData)numericColumn.MetaData).
                            Domain = GetNumericDomain(settings, querySettings,
                            parameters, queryBuilder, domain, requestId);
                    }
                    else if (column is TimeColumn)
                    {
                        TimeColumn timeColumn = (TimeColumn)column;
                        ((IMutableTimeColumnMetaData)timeColumn.MetaData).
                            Domain = GetTimeDomain(settings, querySettings,
                            parameters, queryBuilder, domain, requestId);
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        private string[] GetTextDomainValues(
            IKdbConnectionSettings connSettings,
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            QueryBuilder queryBuilder, string column,
            int rowcount, int requestId)
        {
            ColumnDomainCacheKey cacheKey =
                new ColumnDomainCacheKey(KdbUtil.GetCacheKey(connSettings, parameters),
                    queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
                    column, queryBuilder.Where.Predicate.ToString());

            ColumnDomain columnDomain = columnDomainCache[cacheKey];

            if (columnDomain is GenericColumnDomain<string[]>)
            {
                return ((GenericColumnDomain<string[]>)columnDomain).Domain;
            }

            QQueryBuilder domainQuery = new QQueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }
            domainQuery.Limit = rowcount;
            domainQuery.Select.AddColumn(column);
            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;
            domainQuery.Select.IsDistinct = true;

            string query = domainQuery.ToString(querySettings.SqlDialect);

            query = KdbUtil.ApplyParameters(query, parameters, querySettings.QueryMode);
            query = CheckPassToFunctionQuery(query, querySettings.PassToFunction,
                querySettings.FunctionName);
            query = ApplyDeferredSyncQuery(querySettings, query);
            string[] domainValues;

            try
            {
                Log.Info(Resources.LogKdbRetrievingDomainValues, requestId);

                ISet<string> domain = new HashSet<string>();
                c cInstance = connSettings.GetConnection(parameters);
                IQueryResult queryResult = null;
                try
                {
                    queryResult = KdbUtil.DoQuery(query, cInstance, requestId,
                        querySettings.IsDeferredSyncQuery);
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }


                for (int i = 0; i < queryResult.RowCount; i++)
                {
                    object value = queryResult.GetValue(0, i);
                    string strValue = null;
                    if (value != null)
                    {
                        strValue = value.ToString();
                    }
                    if (value is char[])
                    {
                        strValue = new string((char[])value);
                    }
                    if (value is bool)
                    {
                        strValue = XmlConvert.ToString(Convert.ToBoolean(value));
                    }
                    domain.Add(strValue);
                    if (rowcount != -1 && domain.Count == rowcount)
                    {
                        break;
                    }
                }

                domainValues = domain.ToArray();

                columnDomainCache[cacheKey] =
                    new GenericColumnDomain<string[]>(domainValues);
            }
            catch (Exception ex)
            {
                Log.Error(Resources.LogDomainQueryFailed, requestId, column, ex);
                domainValues = null;
            }

            return domainValues;
        }

        private string[] GetTimeBucketDomainValues(
            IKdbConnectionSettings connSettings,
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            QueryBuilder queryBuilder, string column,
            string timeColumn, TimePart timePart,
            int rowcount, int requestId)
        {
            ColumnDomainCacheKey cacheKey =
                new ColumnDomainCacheKey(GetCacheKey(connSettings),
                    queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
                    column, queryBuilder.Where.Predicate.ToString());

            ColumnDomain columnDomain = columnDomainCache[cacheKey];

            if (columnDomain is GenericColumnDomain<string[]>)
            {
                return ((GenericColumnDomain<string[]>)columnDomain).Domain;
            }

            QQueryBuilder domainQuery = new QQueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }
            domainQuery.Limit = rowcount;
            domainQuery.Select.AddTimePart(timeColumn, timePart, "t");
            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;
            domainQuery.Select.IsDistinct = true;

            string query = domainQuery.ToString(querySettings.SqlDialect);
            query = KdbUtil.ApplyParameters(query, parameters, querySettings.QueryMode);
            query = CheckPassToFunctionQuery(query, querySettings.PassToFunction,
                querySettings.FunctionName);
            query = ApplyDeferredSyncQuery(querySettings, query);
            string[] domainValues;

            try
            {
                Log.Info(Resources.LogKdbRetrievingDomainValues, requestId);
                c cInstance = connSettings.GetConnection(parameters);
                IQueryResult queryResult = null;
                try
                {
                    queryResult = KdbUtil.DoQuery(query, cInstance, requestId);
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }

                ISet<string> domain = new HashSet<string>();
                for (int i = 0; i < queryResult.RowCount; i++)
                {
                    string value = Convert.ToString(queryResult.GetValue(0, i));
                    domain.Add(value);
                    if (rowcount != -1 && domain.Count == rowcount)
                    {
                        break;
                    }
                }

                domainValues = domain.ToArray();

                columnDomainCache[cacheKey] =
                    new GenericColumnDomain<string[]>(domainValues);
            }
            catch (Exception ex)
            {
                Log.Error(Resources.LogDomainQueryFailed, requestId, column, ex);
                domainValues = null;
            }

            return domainValues;
        }

        private TimeValueInterval GetTimeDomain(
            IKdbConnectionSettings connSettings,
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            QueryBuilder queryBuilder, string column,
            int requestId)
        {
            string columnName = column;
            if(querySettings.TimeZoneHelper != null &&
                !string.IsNullOrEmpty(
                querySettings.TimeZoneHelper.SelectedTimeZone))
            {
                columnName += "_" + querySettings.TimeZoneHelper.SelectedTimeZone;
            }
            TimeValueInterval domainValues;
            ColumnDomainCacheKey cacheKey =
                new ColumnDomainCacheKey(GetCacheKey(connSettings),
                    queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
                    columnName, queryBuilder.Where.Predicate.ToString());

            ColumnDomain columnDomain = columnDomainCache[cacheKey];

            if (columnDomain is GenericColumnDomain<TimeValueInterval>)
            {
                return ((GenericColumnDomain<TimeValueInterval>)
                    columnDomain).Domain;
            }

            QQueryBuilder domainQuery = new QQueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }

            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Min, "mn");
            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Max, "mx");

            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;

            string query = domainQuery.ToString(querySettings.SqlDialect);

            query = KdbUtil.ApplyParameters(query, parameters, querySettings.QueryMode);
            query = CheckPassToFunctionQuery(query, querySettings.PassToFunction,
                querySettings.FunctionName);
            query = ApplyDeferredSyncQuery(querySettings, query);
            try
            {
                Log.Info(Resources.LogKdbRetrievingDomainValues, requestId);
                c cInstance = connSettings.GetConnection(parameters); ;
                IQueryResult queryResult = null;
                try
                {
                    queryResult = KdbUtil.DoQuery(query, cInstance, requestId);
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }
                KdbTuple touple = KdbUtil.GetTupleFromQueryResult(queryResult, 0);

                DateTime min = DateTime.Parse(touple.Values["mn"].Value.ToString());
                DateTime max = DateTime.Parse(touple.Values["mx"].Value.ToString());

                min = querySettings.TimeZoneHelper.ConvertFromUTC(min);
                max = querySettings.TimeZoneHelper.ConvertFromUTC(max);
                domainValues = new TimeValueInterval(min, max);

                columnDomainCache[cacheKey] =
                    new GenericColumnDomain<TimeValueInterval>(domainValues);
            }
            catch (Exception ex)
            {
                Log.Error(Resources.LogDomainQueryFailed, requestId, column, ex);
                domainValues = null;
            }

            return domainValues;
        }

        private NumericInterval GetNumericDomain(
            IKdbConnectionSettings connSettings,
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            QueryBuilder queryBuilder, string column,
            int requestId)
        {
            NumericInterval domainValues;
            ColumnDomainCacheKey cacheKey =
                new ColumnDomainCacheKey(GetCacheKey(connSettings),
                    queryBuilder.SchemaAndTable, queryBuilder.SourceQuery,
                        column, queryBuilder.Where.Predicate.ToString());

            ColumnDomain columnDomain = columnDomainCache[cacheKey];

            if (columnDomain is GenericColumnDomain<NumericInterval>)
            {
                return ((GenericColumnDomain<NumericInterval>)
                    columnDomain).Domain;
            }

            QQueryBuilder domainQuery = new QQueryBuilder();

            if (queryBuilder.SchemaAndTable != null)
            {
                domainQuery.SchemaAndTable = queryBuilder.SchemaAndTable;
            }
            else
            {
                domainQuery.SourceQuery = queryBuilder.SourceQuery;
            }

            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Min, "mn");
            domainQuery.Select.AddAggregate(
                new[] { column }, FunctionType.Max, "mx");

            domainQuery.Where.Predicate = queryBuilder.Where.Predicate;

            string query = domainQuery.ToString(querySettings.SqlDialect);

            query = KdbUtil.ApplyParameters(query, parameters, querySettings.QueryMode);
            query = CheckPassToFunctionQuery(query, querySettings.PassToFunction,
                querySettings.FunctionName);
            query = ApplyDeferredSyncQuery(querySettings, query);
            try
            {
                Log.Info(Resources.LogKdbRetrievingDomainValues, requestId);
                c cInstance = connSettings.GetConnection(parameters);
                IQueryResult queryResult = null;
                try
                {
                    queryResult = KdbUtil.DoQuery(query, cInstance, requestId);
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }
                KdbTuple tuple = KdbUtil.GetTupleFromQueryResult(queryResult, 0);
                
                double min = Convert.ToDouble(tuple.Values["mn"].Value);
                double max = Convert.ToDouble(tuple.Values["mx"].Value);

                if (!Double.IsInfinity(min) && !Double.IsInfinity(max))
                {
                    domainValues = new NumericInterval(min, max);
                    columnDomainCache[cacheKey] =
                    new GenericColumnDomain<NumericInterval>(domainValues);
                }
                else
                {
                    domainValues = null;
                }
                
            }
            catch (Exception ex)
            {
                Log.Error(Resources.LogDomainQueryFailed, requestId, column, ex);
                domainValues = null;
            }

            return domainValues;
        }

        private string[] UpdateOnDemandColumns(string[] columns,
            Dictionary<string, string> columnMap)
        {
            if (columns != null)
            {
                List<string> tableColumns = new List<string>();
                foreach (string col in columns)
                {
                    tableColumns.Add(GetKeyByValue(columnMap, col));
                }
                return tableColumns.ToArray<string>();
            }
            return null;
        }

        private void UpdateColumnName(OnDemandParameters onDemandParameters,
            Dictionary<string, string> columnMap)
        {
            if (onDemandParameters == null || onDemandParameters.IsEmpty ||
                columnMap == null) return;

            onDemandParameters.BreakdownColumns =
                UpdateOnDemandColumns(onDemandParameters.BreakdownColumns,
                columnMap);

            onDemandParameters.AuxiliaryColumns =
                UpdateOnDemandColumns(onDemandParameters.AuxiliaryColumns,
                columnMap);

            if (onDemandParameters.Aggregates != null)
            {
                foreach (OnDemandAggregate agreegate
                    in onDemandParameters.Aggregates)
                {
                    agreegate.Columns =
                        UpdateOnDemandColumns(agreegate.Columns, columnMap);
                }
            }

            if (onDemandParameters.Bucketings != null)
            {
                foreach (Bucketing bucketing in
                    onDemandParameters.Bucketings)
                {
                    bucketing.SourceColumnName = GetKeyByValue(columnMap,
                        bucketing.SourceColumnName);
                }
            }
            onDemandParameters.DrillPath =
                UpdateOnDemandColumns(onDemandParameters.DrillPath, columnMap);

            onDemandParameters.Domains =
                UpdateOnDemandColumns(onDemandParameters.Domains, columnMap);

            if (onDemandParameters.Predicates != null)
            {
                foreach (Predicate predicate in onDemandParameters.Predicates)
                {
                    UpdatePredicateColumns(predicate, columnMap);
                }
            }
        }

        private void UpdatePredicate(Predicate predicate, ITable schema,
            Dictionary<string, string> columnMap)
        {
            if (schema == null) return;

            if (predicate is NotOperator)
            {
                NotOperator notOperator = (NotOperator)predicate;
                UpdatePredicate(notOperator.A, schema, columnMap);
            }
            else if (predicate is BinaryOperator)
            {
                BinaryOperator binaryOperator = (BinaryOperator)predicate;
                UpdatePredicate(binaryOperator.A, schema, columnMap);
                UpdatePredicate(binaryOperator.B, schema, columnMap);
            }
            else if (predicate is Comparison)
            {
                Comparison comparison = (Comparison)predicate;
                if (comparison.Right is ListParameter &&
                    comparison.Left is ColumnParameter)
                {
                    string columnName = ((ColumnParameter)comparison.Left).ColumnName;
                    if (columnMap != null && columnMap.ContainsKey(columnName))
                    {
                        columnName = columnMap[columnName];
                    }
                    IColumn column = schema.GetColumn(columnName);
                    if (column.MetaData is IKdbColumnMetaData)
                    {
                        comparison.Right = new QListParameter(
                            (ListParameter)comparison.Right,
                            ((IKdbColumnMetaData)column.MetaData).KdbType);
                    }

                }
            }

        }

        private static void UpdatePredicateColumns(Predicate predicate,
            Dictionary<string, string> columnMap)
        {
            if (predicate is NotOperator)
            {
                NotOperator notOperator = (NotOperator)predicate;
                UpdatePredicateColumns(notOperator.A, columnMap);
            }
            else if (predicate is BinaryOperator)
            {
                BinaryOperator binaryOperator = (BinaryOperator)predicate;
                UpdatePredicateColumns(binaryOperator.A, columnMap);
                UpdatePredicateColumns(binaryOperator.B, columnMap);
            }
            else if (predicate is Comparison)
            {
                Comparison comparison = (Comparison)predicate;
                if (comparison.Left is ColumnParameter)
                {
                    ColumnParameter columnParam =
                        (ColumnParameter)comparison.Left;

                    columnParam.ColumnName = GetKeyByValue(columnMap,
                        columnParam.ColumnName);
                }
                if (comparison.Left is TimePartParameter)
                {
                    TimePartParameter timeParam =
                        (TimePartParameter)comparison.Left;

                    timeParam.Column = GetKeyByValue(columnMap,
                        timeParam.Column);
                }
                if (comparison.Right is ColumnParameter)
                {
                    ColumnParameter columnParam =
                        (ColumnParameter)comparison.Right;

                    columnParam.ColumnName = GetKeyByValue(columnMap,
                        columnParam.ColumnName);
                }
            }
        }
        
        protected void ValidateDeferredSyncQuery(KdbQuerySettings querySettings)
        {
            //TODO: should remove the method wrap.
            KdbUtil.ValidateDeferredSyncQuery(querySettings.IsDeferredSyncQuery,
                querySettings.DeferredSyncQuery);
        }

    }
}
