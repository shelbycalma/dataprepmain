﻿using System;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class DictQueryResult : QueryResult
    {
        private readonly ListQueryResult keys;
        private readonly ListQueryResult values;

        public DictQueryResult(object result)
        {
            c.Dict dict = (c.Dict)result;
            keys = new ListQueryResult(dict.x);
            values = new ListQueryResult(dict.y);
            Names = new[] {"Key", "Value"};
            RowCount = Math.Max(keys.RowCount, values.RowCount);
            ColumnCount = 2;
        }

        public static bool IsDict(object result)
        {
            if (!(result is c.Dict))
            {
                return false;
            }
            c.Dict dict = (c.Dict)result;
            bool keysValid = ListQueryResult.IsList(dict.x);
            bool valuesValid = ListQueryResult.IsList(dict.y);
            return keysValid && valuesValid;
        }

        public override object GetValue(int column, int row)
        {
            switch (column)
            {
            case 0:
                return keys.GetValue(0, row);
            case 1:
                return values.GetValue(0, row);
            default:
                throw new IndexOutOfRangeException(
                    string.Format(Resources.ExBadColumnIndex, column));
            }
        }
    }
}
