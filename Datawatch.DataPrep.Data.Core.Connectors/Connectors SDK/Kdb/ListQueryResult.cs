﻿using System;
using System.Collections;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class ListQueryResult : QueryResult
    {
        private readonly IList values;

        public ListQueryResult(object result)
        {
            values = (IList)result;
            ColumnCount = 1;
            RowCount = values.Count;
            Names = new[] {"Values"};
        }

        public static bool IsList(object result)
        {
            if (!(result is IList))
            {
                return false;
            }
            HashSet<Type> types = new HashSet<Type>();
            foreach (object o in (IList)result)
            {
                types.Add(o.GetType());
            }
            if (types.Count != 1) // cant handle more than one type
            {
                return false;
            }
            foreach (Type type in types)
            {
                if (!TableBuilder.IsSingleValueType(type))
                {
                    return false;
                }
            }
            return true;
        }

        public override object GetValue(int column, int row)
        {
            if (column != 0)
            {
                throw new IndexOutOfRangeException(
                    string.Format(Resources.ExBadColumnIndex, column));
            }
            return values[row];
        }
    }
}
