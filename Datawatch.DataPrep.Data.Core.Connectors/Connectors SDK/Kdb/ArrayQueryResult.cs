﻿using System;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class ArrayQueryResult : QueryResult
    {
        private readonly c.Flip a;
        private c.Dict dictionary;
        private bool bIsDictionary = false;

        public string OpCode { get; private set; }
        public string TableName { get; private set; }
                
        public ArrayQueryResult(object queryResult)
        {
            
            object[] objs = (object[])queryResult;
            TableName = "";
            ColumnCount = 0;
            RowCount = 0;

            if (isValidTickUpdateFormat(queryResult))
            {
                if (objs[0] is char[])
                {
                    OpCode = new string((char[])objs[0]);
                }
                else
                {
                    OpCode = (string)objs[0];
                }

                TableName = (string)objs[1];

                if (objs[2] is c.Dict)
                {
                    // dictionary being returned
                    dictionary = (c.Dict)objs[2];
                    c.Flip tempCFlipKC = (c.Flip)dictionary.x;
                    c.Flip tempCFlipVD = (c.Flip)dictionary.y;
                    string[] keyColumns = (tempCFlipKC).x;
                    string[] valueColumns = (tempCFlipVD).x;
                    object[] keyData = (tempCFlipKC).y;
                    object[] valueData = (tempCFlipVD).y;

                    bIsDictionary = true;

                    string[] n = new string[keyColumns.Length + valueColumns.Length];
                    for (int i = 0; i < keyColumns.Length; i++)
                    {
                        n[i] = keyColumns[i];
                    }
                    for (int i = 0; i < valueColumns.Length; i++)
                    {
                        n[i + keyColumns.Length] = valueColumns[i];
                    }
                    Names = n;

                    ColumnCount = Names.Length;
                    RowCount = -1;
                    if (valueData != null && valueData.Length > 0 &&
                        valueData[0] != null)
                    {
                        object[] vdl = (object[])valueData[0];
                        RowCount = vdl.Length;
                    }
                    return;
                }
                else
                {

                    // table being returned
                    a = objs[2] as c.Flip;
                }

            }
            else if (isValidSubscriptionResponseFormat(queryResult))
            {
                TableName = (string)objs[0];

                if (objs[1] is c.Dict)
                {
                    objs[1] = c.td(objs[1]);
                }

                a = objs[1] as c.Flip;
            }
            else
            {
                return;
            }
            //get column names from flip.
            Names = a.x;
            ColumnCount = Names.Length;
            RowCount = ((Array)a.y[0]).Length;
        }

        public override object GetValue(int column, int row)
        {
            // in case of dict, access either a or b depending on index
            if (bIsDictionary)
            {

                string[] keyColumns = ((c.Flip)dictionary.x).x;
                string[] valueColumns = ((c.Flip)dictionary.y).x;
                object[] keyData = ((c.Flip)dictionary.x).y;
                object[] valueData = ((c.Flip)dictionary.y).y;

                if (column < keyColumns.Length)
                {
                    return c.at(keyData[column], row);
                }
                else if (column < keyColumns.Length + valueColumns.Length)
                {
                    return c.at(valueData[column - keyColumns.Length], row);
                }
            }
            else if (column < a.y.Length)
            {
                return c.at(a.y[column], row);
            }
            
            return null;
        }

        private static bool isValidSubscriptionResponseFormat(object result)
        {
            if (!(result is object[]))
            {
                return false;
            }
            object[] objs = (object[])result;
            if (!((objs.Length == 2)
                && (objs[0] is string)
                && ((objs[1] is c.Flip) || (objs[1] is c.Dict))))
            {
                return false;
            }

            return true;

        }

        private static bool isValidTickUpdateFormat(object result)
        {
            if (!(result is object[]))
            {
                return false;
            }
            object[] objs = (object[])result;
            if (!((objs.Length == 3)
                && (objs[0] is char[] || (objs[0] is string))
                && (objs[1] is string)
                && ((objs[2] is c.Flip) || (objs[2] is c.Dict))))
            {
                return false;
            }

            return true;
        }


        public static IQueryResult GetSchemaQueryResult(object queryResult)
        {
            //In case of functional subscription we get meta with query result
            if (!ArrayQueryResult.isValidSubscriptionResponseFormat(
                queryResult))
            {
                throw new Exception(Resources.ExInvalidQueryResult);
            }

            object[] serviceQueryResult = queryResult as object[];
            return KdbUtil.GetQueryResult(serviceQueryResult[1]);
        }
    }
}
