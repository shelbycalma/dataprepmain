﻿namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class SingleValueQueryResult : QueryResult
    {
        private readonly object value;

        public SingleValueQueryResult(object result)
        {
            value = result;
            Names = new[] {"Value"};
            RowCount = 1;
            ColumnCount = 1;
        }

        public static bool IsSingle(object result)
        {
            if (TableBuilder.IsSingleValueType(result.GetType()))
            {
                return true;
            }

            return false;
        }

        public override object GetValue(int column, int row)
        {
            return value;
        }
    }
}
