﻿using System;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class KdbQuerySettings : QuerySettings<KdbColumn>
    {
        public KdbQuerySettings(PropertyBag bag)
            : base(bag)
        {
            defaultIsOnDemand = false;
        }
        
        public bool IsSeries
        {
            get
            {
                string s = GetInternal("IsSeries");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IsSeries", Convert.ToString(value));
            }
        }

        public string SelectedSymbolColumnAggregate
        {
            get { return GetInternal("SelectedSymbolColumnAggregate"); }
            set { SetInternal("SelectedSymbolColumnAggregate", value); }
        }

        public bool IsPeriod
        {
            get
            {
                string s = GetInternal("IsPeriod");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IsPeriod", Convert.ToString(value));
            }
        }

        public string PeriodText
        {
            get { return GetInternal("PeriodText"); }
            set { SetInternal("PeriodText", value); }
        }

        public AggregationPeriodType AggregationPeriodType
        {
            get
            {
                return (AggregationPeriodType)
                   Enum.Parse(typeof(AggregationPeriodType),
                       GetInternal("AggregationPeriodType", "second"));
            }
            set
            {
                SetInternal("AggregationPeriodType", value.ToString());
            }
        }


        public bool PassToFunction
        {
            get
            {
                string value = GetInternal("PassToFunction",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("PassToFunction", value.ToString());
            }
        }

        public string FunctionName
        {
            get
            {
                return GetInternal("FunctionName", "value");
            }
            set
            {
                SetInternal("FunctionName", value);
            }
        }

        public string SelectedDateColumn
        {
            get { return GetInternal("SelectedDateColumn"); }
            set
            {
                SetInternal("SelectedDateColumn", value);
            }
        }

        public string SelectedTimeColumn
        {
            get { return GetInternal("SelectedTimeColumn"); }
            set
            {
                SetInternal("SelectedTimeColumn", value);
            }
        }

        public bool IsDeferredSyncQuery
        {
            get
            {
                string value = GetInternal("IsDeferredSyncQuery",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("IsDeferredSyncQuery", value.ToString());
            }
        }

        public string DeferredSyncQuery
        {
            get { return GetInternal("DeferredSyncQuery",
                KdbUtil.DefaultDeferredSyncQuery); }
            set { SetInternal("DeferredSyncQuery", value); }
        }

        public override SchemaAndTable SelectedTable
        {
            get
            {
                string s = GetInternal("SelectedTable");
                if (!string.IsNullOrEmpty(s))
                {
                    return SchemaAndTable.Parse(s);
                }
                return null;
            }
            set
            {
                SelectedColumns.Clear();
                if (PredicateColumns != null)
                {
                    PredicateColumns.Clear();
                }
                SetInternal("SelectedTable", value != null ? value.Table : null);
                OnPropertyChanged("SelectedTable");
            }
        }

        public AggregationPeriodType[] AggregationPeriodTypes
        {
            get
            {
                return new AggregationPeriodType[] 
                    { 
                        AggregationPeriodType.second, 
                        AggregationPeriodType.minute,
                        AggregationPeriodType.hh,
                        AggregationPeriodType.date,
                        AggregationPeriodType.week,
                        AggregationPeriodType.month
                    };
            }
        }

        public new SqlDialect SqlDialect
        {
            get { return SqlDialectFactory.KxQ; }
        }
        
        public override DatabaseColumn CreateColumn(string columnName,
            ColumnType columnType, string appliedParameterName,
            AggregateType aggregateType, FilterOperator filterOperator, string filterValue)
        {
            KdbColumn column = new KdbColumn();
            column.ColumnName = columnName;
            column.ColumnType = columnType;
            column.AppliedParameterName = appliedParameterName;
            column.AggregateType = aggregateType;
            column.FilterOperator = filterOperator;
            column.FilterValue = filterValue;

            return column;
        }
        
        public override DatabaseColumn CreateColumn(
            string rootPath, PropertyBag bag, int index)
        {
            KdbColumnProperties kdbColumnProperties = new KdbColumnProperties(
                rootPath, index);
            
                       
            KdbColumn column = (KdbColumn) base.CreateColumn(rootPath, bag, index);
            string kdbTypeString = kdbColumnProperties.KdbType.GetString(bag);
            if(!string.IsNullOrEmpty(kdbTypeString))
            {
                column.KdbType = kdbTypeString[0];
            }
            return column;
        }

        public override void ToDatabaseColumnProperties(DatabaseColumn column,
            string rootPath, int index)
        {
            KdbColumn kdbColumn = column as KdbColumn;
            if (kdbColumn == null)
            {
                base.ToDatabaseColumnProperties(column, rootPath, index);
                return;
            }

            base.ToDatabaseColumnProperties(column, rootPath, index);
            KdbColumnProperties colProperties =
                    new KdbColumnProperties(rootPath, index);
            colProperties.KdbType.SetString(propertyBag,
                kdbColumn.KdbType.ToString());
        }
    }
}
