﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Properties;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class FlipQueryResult : QueryResult
    {
        private readonly c.Flip a;
        private readonly c.Flip b;

        public FlipQueryResult(object queryResult)
        {
            if (queryResult is c.Flip)
            {
                a = queryResult as c.Flip;
                Names = a.x;
            }
            else if (queryResult is c.Dict)
            {                
                var dict = queryResult as c.Dict;
                a = (c.Flip)dict.x;
                b = (c.Flip)dict.y;
                List<string> columnNames = new List<string>();
                columnNames.AddRange(a.x);
                columnNames.AddRange(b.x);
                Names = columnNames.ToArray();
            }
            else
            {
                throw new Exception(Resources.ExInvalidQueryResult);
            }

            ColumnCount = Names.Length;
            RowCount = ((Array)a.y[0]).Length;
        }

        public override object GetValue(int column, int row)
        {
            // in case of dict, access either a or b depending on index
            if (column < a.y.Length)
            {
                return c.at(a.y[column], row);
            }
            return c.at(b.y[column - a.y.Length], row);
        }

        public override Type GetFieldType(int ordinal)
        {
            //This method is overriden because, for FlipQueryResult
            //we can try to get table schema even when query returns no rows,
            //its not possible using Implementation in QueryResult

            Type columnType = typeof(object);
            // in case of dict, access either a or b depending on index
            if (ordinal < a.y.Length)
            {
                columnType = a.y[ordinal].GetType();
            }
            else if (b != null)
            {
                columnType = b.y[ordinal - a.y.Length].GetType();
            }
            //Type for all the columns are array type, actual array type columns
            //returned as object[]. So GetElementType is required for all other columns

            if (columnType == typeof(object[]))
            {
                if (ordinal < a.y.Length)
                {
                    object[] array = (object[])a.y[ordinal];
                    if (array.Length <= 0)
                    {
                        //this happen when rowcount is zero for the query.
                        return typeof(object);
                    }
                    return array[0].GetType();
                }
                else
                {
                    object[] array = (object[])b.y[ordinal - a.y.Length];
                    if (array.Length <= 0)
                    {
                        //this happen when rowcount is zero for the query.
                        return typeof(object);
                    }
                    return array[0].GetType();
                }
            }
            else
            {
                return columnType.GetElementType();
            }
        }
    }
}
