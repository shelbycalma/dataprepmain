﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Newtonsoft.Json;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class KdbColumn : DatabaseColumn
    {
        public KdbColumn()
            : base(new PropertyBag())
        {
        }

        public KdbColumn(PropertyBag bag)
            : base(bag)
        {
        }

        public KdbColumn(KdbColumn kdbColumn)
            :base(kdbColumn)
        {
            KdbType = kdbColumn.KdbType;
        }

        [JsonIgnore]
        public override AggregateType[] AggregateTypes
        {
            get
            {
                if (ColumnType == ColumnType.Numeric)
                {
                    return new AggregateType[]
                    {
                        AggregateType.Last,
                        AggregateType.First,
                        AggregateType.Max,
                        AggregateType.Min,
                        AggregateType.Sum,
                        AggregateType.Count,
                        AggregateType.Mean,
                        AggregateType.None
                    };
                }
                else if (ColumnType == ColumnType.Time)
                {
                    return new AggregateType[]
                    {
                        AggregateType.Last,
                        AggregateType.First,
                        AggregateType.Max,
                        AggregateType.Min,
                        AggregateType.Count,
                        AggregateType.None
                    };
                }
                else
                {
                    return new AggregateType[]
                    {
                        AggregateType.Last,
                        AggregateType.First,
                        AggregateType.Count,
                        AggregateType.None
                    };
                }

            }
        }

        public char? KdbType
        {
            get
            {
                string s = GetInternal("KdbType");
                if (!string.IsNullOrEmpty(s))
                {
                    return s[0];
                }
                return null;
            }
            set { SetInternal("KdbType", value.ToString()); }
        }
    }
}
