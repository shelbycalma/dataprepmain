﻿using System;
using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class MetaQueryResult: QueryResult
    {
        private readonly List<KdbTypeMapping> dataTypeLookup =
            new List<KdbTypeMapping>();

        public MetaQueryResult(object result)
        {
            c.Dict dict = (c.Dict)result;
            c.Flip a = (c.Flip)dict.x;
            c.Flip b = (c.Flip)dict.y;

            Names = (string[]) a.y[0];
            RowCount = 0;
            ColumnCount = Names.Length;

            foreach(char kdbType in (char[])b.y[0])
            {
                dataTypeLookup.Add(new KdbTypeMapping(kdbType,
                    ConvertType(kdbType)));
            }
        }

        public Type ConvertType(char KdbType)
        {
            switch (KdbType)
            {
                case 's':
                    return typeof(string);
                case 'S':
                    return typeof(string[]);
                case 'g':
                    return typeof(Guid);
                case 'G':
                    return typeof(Guid[]);
                case 'c':
                    return typeof(char);
                case 'C':
                    return typeof(char[]);
                case 'b':
                    return typeof(bool);
                case 'B':
                    return typeof(bool[]);
                case 't':
                    return typeof(TimeSpan);
                case 'T':
                    return typeof(TimeSpan[]);
                case 'i':
                    return typeof(Int32);
                case 'I':
                    return typeof(Int32[]);
                case 'j':
                    return typeof(Int64);
                case 'J':
                    return typeof(Int64[]);
                case 'f':
                    return typeof(Double);
                case 'F':
                    return typeof(Double[]);
                case 'h':
                    return typeof(Int16);
                case 'H':
                    return typeof(Int16[]);
                case 'e':
                    return typeof(Single);
                case 'E':
                    return typeof(Single[]);
                case 'x':
                    return typeof(byte);
                case 'X':
                    return typeof(byte[]);
                case 'm':
                    return typeof(c.Month);
                case 'u':
                    return typeof(c.Minute);
                case 'v':
                    return typeof(c.Second);
                case 'z':
                    return typeof(DateTime);
                case 'Z':
                    return typeof(DateTime[]);
                case 'd':
                    return typeof(c.Date);
                case 'D':
                    return typeof(c.Date[]);
                case 'n':
                    return typeof(c.KTimespan);
                case 'N':
                    return typeof(c.KTimespan[]);
                case 'p':
                    return typeof(DateTime);
                case 'P':
                    return typeof(DateTime[]);
                default:
                    return typeof(string);
            }
        }

        public static bool IsMeta(object result)
        {
            if (!(result is c.Dict))
            {
                return false;
            }
            c.Dict dict = (c.Dict)result;
            if (!(dict.x is c.Flip) || !(dict.y is c.Flip))
            {
                return false;
            }

            c.Flip a = (c.Flip)dict.x;
            c.Flip b = (c.Flip)dict.y;

            // A meta query return a table keyed by column name, with columns: 
            //c - column name
            //t - data type
            //f - foreign key (enumeration)
            //a - attribute 

            if ((a.x.Length > 0 && a.x[0] == "c") && (a.y.Length  > 0) && 
                (a.y[0] is string[])
                && (b.x.Length == 3 && b.x[0] == "t" && b.x[1] == "f" &&
                b.x[2] == "a"))
            {
                return true;
            }
            return false;
        }

        public override object GetValue(int column, int row)
        {
            return null;
        }

        public override Type GetFieldType(int ordinal)
        {
            // This method is overriden because, for MetaQueryResult
            // We need to convert KDB types.
            return dataTypeLookup[ordinal].MappedType;
        }

        public char GetKdbType(int ordinal)
        {
            return dataTypeLookup[ordinal].KdbType;
        }
    }
}