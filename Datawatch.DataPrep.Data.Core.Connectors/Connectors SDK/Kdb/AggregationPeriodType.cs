﻿namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public enum AggregationPeriodType
    {
        second,
        minute,
        hh,
        date,
        week,
        month
    }
}
