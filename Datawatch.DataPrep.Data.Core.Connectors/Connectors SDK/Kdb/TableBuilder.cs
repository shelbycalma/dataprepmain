﻿using System;
using System.Collections.Generic;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Connectors.Properties;
using Datawatch.DataPrep.Data.Core.Connectors.Query.Q;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class TableBuilder
    {
        public static ParameterTable CreateTableSchema(IQueryResult result,
            IEnumerable<ParameterValue> parameters, List<string> selectedColumns,
            Dictionary<string, string> columnMap = null,
            bool isColumnsExplicit = false)
        {
            return CreateTableSchema(result, null, false, parameters,
                selectedColumns, columnMap, isColumnsExplicit);
        }

        public static ParameterTable CreateTableSchema(IQueryResult result,
            KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters, List<string> selectedColumns,
            Dictionary<string, string> columnMap = null,
            bool isColumnsExplicit = false)
        {
            return CreateTableSchema(result,
                querySettings.SchemaColumnsSettings,
                querySettings.IsSchemaRequest, parameters,
                selectedColumns, columnMap, isColumnsExplicit);
        }

        public static ParameterTable CreateTableSchema(IQueryResult result,
            SchemaColumnsSettings<KdbColumn> schemaColumnsSettings,
            bool isSchemaRequest, IEnumerable<ParameterValue> parameters,
            List<string> selectedColumns,
            Dictionary<string, string> columnMap = null,
            bool isColumnsExplicit = false)
        {
            /*Column Mappings
             KDB Type    .Net Type        Column Type
                s           String           TextColumn
                S           String[]         TextColumn
                g           Guid             TextColumn
                G           Guid[]           TextColumn
                c           Char             TextColumn
                C           Char[]           TextColumn
                b           Boolean          TextColumn
                B           Boolean[]        TextColumn
                T           TimeSpan[]       TextColumn
                i           Int32            NumericColumn
                I           Int32[]          NumericColumn
                j           Int64            NumericColumn
                J           Int64[]          NumericColumn
                f           Double           NumericColumn
                F           Double[]         NumericColumn
                h           Int16            NumericColumn
                H           Int16[]          NumericColumn
                e           Single           NumericColumn
                E           Single[]         NumericColumn
                x           Byte             NumericColumn
                X           Byte[]           NumericColumn
                m           kx.c.Month       NumericColumn
                u           kx.c.Minute      NumericColumn
                v           kx.c.Second      NumericColumn 
                z           DateTime         TimeColumn
                Z           DateTime[]       TimeColumn
                d           Date             TimeColumn
                D           Date[]           TimeColumn
                t           TimeSpan         TimeColumn
                n           kx.c.KTimespan   TimeColumn
                N           kx.c.KTimespan[] TimeColumn
                p           DateTime         TimeColumn
                P           DateTime[]       TimeColumn
            */
            if (isSchemaRequest && schemaColumnsSettings != null)
            {
                schemaColumnsSettings.ClearSchemaColumns();
            }
            ParameterTable table = new ParameterTable(parameters);
            table.BeginUpdate();
            try
            {
                string[] names = result.Names;

                for (int i = 0; i < names.Length; i++)
                {
                    string columnName = names[i];
                    if (columnMap != null && columnMap.ContainsKey(columnName))
                    {
                        columnName = columnMap[columnName];
                    }
                    else if (isColumnsExplicit)
                    {
                        continue;
                    }
                    if (selectedColumns != null && selectedColumns.Count > 0 &&
                        !selectedColumns.Contains(columnName))
                    {
                        continue;
                    }
                    Type type = result.GetFieldType(i);
                    ColumnType columnType = ToColumnType(type);
                    Column column = CreateColumn(columnType, columnName,
                        GetKdbType(type));
                    if (column == null) continue;
                    if(isSchemaRequest && schemaColumnsSettings != null)
                    {
                        AddSchemaColumn(schemaColumnsSettings, column);
                    }
                    table.AddColumn(column);
                }
            }
            finally {
                table.EndUpdate();
            }
            if (isSchemaRequest && schemaColumnsSettings != null)
            {
                schemaColumnsSettings.SerializeSchemaColumns();
            }
            return table;
        }

        private static void AddSchemaColumn(SchemaColumnsSettings<KdbColumn> schemaColumnsSettings,
            Column column)
        {
            KdbColumn kdbColumn = new KdbColumn();
            kdbColumn.ColumnName = column.Name;
            if (column is TextColumn)
            {
                kdbColumn.ColumnType = ColumnType.Text;
                KdbTextColumnMetaData meta =
                    column.MetaData as KdbTextColumnMetaData;
                kdbColumn.KdbType = meta.KdbType;
            }
            else if (column is NumericColumn)
            {
                kdbColumn.ColumnType = ColumnType.Numeric;
            }
            else if (column is TimeColumn)
            {
                kdbColumn.ColumnType = ColumnType.Time;
            }

            schemaColumnsSettings.SchemaColumns.Add(kdbColumn);
        }

        public static ParameterTable CreateTableFromSavedSchema(
            SchemaColumnsSettings<KdbColumn> schemaColumnsSettings, 
            IEnumerable<ParameterValue> parameters)
        {
            ParameterTable table = new ParameterTable(parameters);
            if(schemaColumnsSettings == null ||schemaColumnsSettings.SchemaColumns.Count == 0)
            {
                return table;   
            }
            table.BeginUpdate();
            try
            {                
                foreach (KdbColumn col in schemaColumnsSettings.SchemaColumns)
                {                    
                    Column column = CreateColumn(col.ColumnType, col.ColumnName,
                        col.KdbType);
                    
                    if (column == null) continue;
                    table.AddColumn(column);
                }
            }
            finally
            {
                table.EndUpdate();
            }

            Log.Info(Resources.LogCreatedTableFromSavedSchema,
                table.ColumnCount);
            return table;
        }

        private static Column CreateColumn(ColumnType columnType, 
            string columnName, char? kdbType)
        {
            if (columnType == ColumnType.Text)
            {
                Column textColumn = new TextColumn(columnName);
                if (kdbType != null)
                { 
                    textColumn.MetaData =
                        new KdbTextColumnMetaData(textColumn)
                        { KdbType = kdbType.Value };
                }
                return textColumn;
            }
            else if (columnType == ColumnType.Numeric)
            {
                return new NumericColumn(columnName);
            }
            else if (columnType == ColumnType.Time)
            {
                return new TimeColumn(columnName);
            }
            return null;
        }

        private static char GetKdbType(Type type)
        {
            char kdbType = 's';
            if (type == typeof(char) ||
                type == typeof(char[]))
            {
                kdbType = 'c';
            }
            else if (type == typeof(bool))
            {
                kdbType = 'b';
            }

            return kdbType;
        }

        public static ParameterTable CreateTable(IQueryResult result,
            int maxRows, TimeZoneHelper timezoneHelper,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> columnMap = null,
            ParameterTable table = null)
        {
            if (table == null)
            {
                table = CreateTableSchema(result, parameters, null, columnMap);
            }
            AddRowsToColumn(result, timezoneHelper, columnMap,
                table, maxRows);
            return table;
        }

        public static ParameterTable CreateTable(IQueryResult result,
            int maxRows, KdbQuerySettings querySettings,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> columnMap = null,
            ParameterTable table = null)
        {
            //Datatypes (from kx docs)

            //t| size literal      q        sql       java      .net     xmlschema 
            //---------------------------------------------------------------------
            //b  1    0b           boolean            Boolean   boolean  boolean   
            //x  1    0x0          byte               Byte      byte     byte      
            //h  2    0h           short    smallint  Short     int16    short     
            //i  4    0            int      int       Integer   int32    int       
            //j  8    0j           long     bigint    Long      int64    long      
            //e  4    0e           real     real      Float     single   single    
            //f  8    0.0          float    float     Double    double   double    
            //c  1    " "          char               Character char               
            //s  .    `            symbol   varchar   String    string   string    
            //m  4    2000.01m     month                                           
            //d  4    2000.01.01   date     date      Date               date      
            //z  8    dateTtime    datetime timestamp Timestamp DateTime dateTime  
            //u  4    00:00        minute                                          
            //v  4    00:00:00     second                                          
            //t  4    00:00:00.000 time     time      Time      TimeSpan time      
            //*  4    `s$`         enum

            if (table == null)
            {
                //TODO: Check if null check is requried here for querySettings 
                // in that case there would be failures inside  CreateTableSchema.
                if ((querySettings != null &&
                    querySettings.SchemaColumnsSettings.SchemaColumns.Count > 0))
                {
                    table = CreateTableFromSavedSchema(
                        querySettings.SchemaColumnsSettings, parameters);
                }
                else
                {
                    table = CreateTableSchema(result, querySettings, 
                        parameters, null, columnMap);
                }
            }

            if(querySettings.IsSchemaRequest)
            {
                return table;
            }

            AddRowsToColumn(result, querySettings.TimeZoneHelper, columnMap,
                table, maxRows);
            return table;
        }

        private static void AddRowsToColumn(IQueryResult result,
            TimeZoneHelper timezoneHelper, Dictionary<string, string> columnMap,
            ParameterTable table, int maxRows)
        {
            table.BeginUpdate();
            try
            {
                string[] names = result.Names;

                for (int i = 0; i < result.RowCount; i++)
                {
                    if (maxRows > -1 && table.RowCount >= maxRows) break;

                    Row r = table.AddRow();
                    for (int j = 0; j < result.ColumnCount; j++)
                    {
                        object value = result.GetValue(j, i);
                        string colName = names[j];
                        if (columnMap != null && columnMap.ContainsKey(colName))
                        {
                            colName = columnMap[colName];
                        }
                        SetColumnValue(table, r, value, colName, timezoneHelper);
                    }

                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        public static ParameterTable CreateOnDemandTable(IQueryResult result,
            int maxRows, TimeZoneHelper timeZoneHelper,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> renamedColMap,
            ITable tableSchema = null)
        {
            // TODO: should actually remove this method, and use CreateTable,
            // which which does the same job with restricted/saved schema.
            ParameterTable table;
            if (tableSchema == null)
            {
                // TODO Check, should never arrive here.
                table = (ParameterTable)CreateTableSchema(
                    result, null, parameters, null, renamedColMap);
            }
            else
            {
                table = (ParameterTable)tableSchema;
            }
            // Adjust the table to include columns for the aggregates.
            UpdateParameterTable(table, result, renamedColMap);

            object[] sourceValues = new object[result.ColumnCount];
            object[] targetValues = new object[table.ColumnCount];

            int[] columnMap = CreateColumnMap(table, result, renamedColMap);

            table.BeginUpdate();
            try
            {
                string[] names = result.Names;
                for (int i = 0; i < result.RowCount; i++)
                {
                    if (maxRows > -1 && table.RowCount >= maxRows) break;
                    for (int j = 0; j < result.ColumnCount; j++)
                    {
                        Column column = table.GetColumn(columnMap[j]);
                        object value = GetColumnValue(column,
                            result.GetValue(j, i), timeZoneHelper);
                        // Check if value is correct based on column type.
                        // i.e. source is may change and get off from saved schema.
                        value = ValidateColumnValue(column, value);
                        sourceValues[j] = value;
                    }
                    for (int ctr = 0; ctr < result.ColumnCount; ctr++)
                    {
                        if (columnMap[ctr] < 0) continue;
                        targetValues[columnMap[ctr]] = sourceValues[ctr];
                    }
                    table.AddRow(targetValues);
                }
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        private static object ValidateColumnValue(Column column, object value)
        {
            if(value == null)
            {
                return value;
            }
            if (column is NumericColumn && value.GetType() != typeof(double))
            {
                value = Double.NaN;
            }
            else if (column is TimeColumn && value.GetType() != typeof(DateTime))
            {
                value = null;
            }
            else if (column is TextColumn && value.GetType() != typeof(string))
            {
                value = null;
            }

            return value;
        }

        public static ColumnType ToColumnType(Type type)
        {
            if (type == typeof(char) ||
                type == typeof(char[]) ||
                type == typeof(string) ||
                type == typeof(bool) ||
                type == typeof(Guid) ||
                type == typeof(string[]) ||
                type == typeof(bool[]) ||
                type == typeof(Guid[]))
            {
                return ColumnType.Text;
            }
            else if (type == typeof(byte) ||
                type == typeof(Int16) ||
                type == typeof(Int32) ||
                type == typeof(Int64) ||
                type == typeof(Double) ||
                type == typeof(Single) ||
                type == typeof(byte[]) ||
                type == typeof(Int16[]) ||
                type == typeof(Int32[]) ||
                type == typeof(Int64[]) ||
                type == typeof(Double[]) ||
                type == typeof(Single[]) ||
                type == typeof(c.Month) ||
                type == typeof(c.Minute) ||
                type == typeof(c.Second))
            {
                return ColumnType.Numeric;
            }
            else if (type == typeof(DateTime) ||
                type == typeof(c.Date) ||
                type == typeof(TimeSpan) ||
                type == typeof(TimeSpan[]) ||
                type == typeof(DateTime[]) ||
                type == typeof(c.Date[]) ||
                type == typeof(c.KTimespan) ||
                type == typeof(c.KTimespan[]))
            {
                return ColumnType.Time;
            }
            return ColumnType.Text;
        }

        private static void UpdateParameterTable(
            ParameterTable table, IQueryResult queryResult,
            Dictionary<string, string> renamedColMap)
        {
            table.BeginUpdate();
            try
            {
                for (int i = 0; i < queryResult.Names.Length; i++)
                {
                    string name = queryResult.Names[i];
                    if (renamedColMap != null 
                        && renamedColMap.ContainsKey(name))
                    {
                            name = renamedColMap[name];
                    }
                    if (!table.ContainsColumn(name))
                    {

                        Type type = queryResult.GetFieldType(i);
                        ColumnType columnType = ToColumnType(type);
                        Column column = CreateColumn(columnType, name,
                            GetKdbType(type));
                        if (column == null) continue;
                        table.AddColumn(column);
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }
        }

        public static int[] CreateColumnMap(
            ParameterTable target, IQueryResult source,
            Dictionary<string, string> renamedColMap)
        {            
            int[] map = new int[source.ColumnCount];

            if (renamedColMap == null) return map;

            for (int i = 0; i < source.ColumnCount; i++)
            {
                string name = source.Names[i];
                if (renamedColMap.ContainsKey(name))
                {
                    name = renamedColMap[name];
                }
                int j = DatabaseUtils.IndexOfColumn(target, name);
                map[i] = j;
            }
            return map;
        }

        public static bool IsSingleValueType(Type type)
        {
            if (type == typeof(bool) ||
                type == typeof(byte) ||
                type == typeof(Int16) ||
                type == typeof(Int32) ||
                type == typeof(Int64) ||
                type == typeof(Double) ||
                type == typeof(Single) ||
                type == typeof(char) ||
                type == typeof(string) ||
                type == typeof(DateTime) ||
                type == typeof(TimeSpan) ||
                type == typeof(c.KTimespan) ||
                type == typeof(c.Date))
            {
                return true;
            }

            return false;
        }
        
        public static void SetColumnValue(StandaloneTable table, Row r,
            object value, string columnName, TimeZoneHelper timeZoneHelper)
        {
            if (value == null) return;
            Column column = table.GetColumn(columnName);
            if (column == null) return;
            // TODO: To make this faster, keep arrays of the columns already
            // cast to the most specific type (e.g. INumericColumn[]).

            object parsedValue = GetColumnValue(column, value, timeZoneHelper);

            if (parsedValue == null) return;

            NumericColumn typedCol = column as NumericColumn;
            
            if (parsedValue is Double && typedCol != null)
            {
                typedCol.SetNumericValue(r, (Double)parsedValue);
                return;
            }

            TimeColumn timeCol = column as TimeColumn;

            if (parsedValue is DateTime && timeCol != null)
            {
                timeCol.SetTimeValue(r, (DateTime)parsedValue);
                return;
            }

            TextColumn textCol = column as TextColumn;

            if (textCol != null)
            {
                textCol.SetTextValue(r, parsedValue.ToString());
                return;
            }            
        }

        public static object GetColumnValue(Column column, 
            object value, TimeZoneHelper timeZoneHelper)
        {
            if (value == null)
            {
                if (column is NumericColumn)
                    return Double.NaN;

                return null;
            }

            try
            {
                Type type = value.GetType();

                if (type == typeof(string) ||
                    type == typeof(char) ||
                    type == typeof(Guid))
                {
                    return value.ToString();
                }
                else if (type == typeof(bool))
                {
                    return XmlConvert.ToString(Convert.ToBoolean(value));
                }
                else if (type == typeof(byte) ||
                    type == typeof(Int16) ||
                    type == typeof(Int32) ||
                    type == typeof(Int64) ||
                    type == typeof(Double) ||
                    type == typeof(Single))
                {
                    return Convert.ToDouble(value);
                }
                else if (type == typeof(c.Month))
                {
                    return Convert.ToDouble(((c.Month)value).i);
                }
                else if (type == typeof(c.Minute))
                {
                    return Convert.ToDouble(((c.Minute)value).i);
                }
                else if (type == typeof(c.Second))
                {
                    return Convert.ToDouble(((c.Second)value).i);
                }
                else if (type == typeof(DateTime))
                {
                    return timeZoneHelper.ConvertFromUTC((DateTime)value);
                }
                else if (type == typeof(c.Date))
                {
                    return timeZoneHelper.ConvertFromUTC(((c.Date)value).DateTime());
                }
                else if (type == typeof(TimeSpan))
                {
                    return KdbUtil.ConvertFromTimeSpan(value, timeZoneHelper);
                }
                else if (type == typeof(c.KTimespan))
                {
                    return KdbUtil.ConvertFromKTimeSpan(value, timeZoneHelper);
                }
                else if (type == typeof(c.KTimespan[]))
                {
                    return KdbUtil.ConvertFromKTimeSpan(
                        ((c.KTimespan[])value)[0], timeZoneHelper);
                }
                else if (type == typeof(string[]))
                {
                    return KdbUtil.ConcatenateValues(value);
                }
                else if (type == typeof(char[]))
                {
                    return new string((char[])value);
                }
                else if (type == typeof(DateTime[]))
                {
                    return timeZoneHelper.ConvertFromUTC(((DateTime[])value)[0]);
                }
                else if (type == typeof(TimeSpan[]))
                {
                    return ((TimeSpan[])value)[0].ToString();
                }
                else if (type == typeof(bool[]))
                {
                    return ((bool[])value)[0].ToString();
                }
                else if (type == typeof(Guid[]))
                {
                    return ((Guid[])value)[0].ToString();
                }
                else if (type == typeof(byte[]))
                {
                    return Convert.ToDouble(((byte[])value)[0]);
                }
                else if (type == typeof(Int16[]))
                {
                    return Convert.ToDouble(((Int16[])value)[0]);
                }
                else if (type == typeof(Int32[]))
                {
                    return Convert.ToDouble(((Int32[])value)[0]);
                }
                else if (type == typeof(Int64[]))
                {
                    return  Convert.ToDouble(((Int64[])value)[0]);
                }
                else if (type == typeof(Double[]))
                {
                    return Convert.ToDouble(((double[])value)[0]);
                }
                else if (type == typeof(Single[]))
                {
                    return Convert.ToDouble(((Single[])value)[0]);
                }
                else if (type == typeof(c.Date[]))
                {
                    return timeZoneHelper.ConvertFromUTC(
                        Convert.ToDateTime((((c.Date[])value)[0]).DateTime()));
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            return null;
        }   
    }
}
