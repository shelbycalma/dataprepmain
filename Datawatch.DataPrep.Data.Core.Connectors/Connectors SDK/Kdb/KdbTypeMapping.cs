﻿using System;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public class KdbTypeMapping
    {
        private readonly char kdbType;
        private readonly Type mappedType;

        public KdbTypeMapping(char kdbType, Type mappedType)
        {
            this.kdbType = kdbType;
            this.mappedType = mappedType;
        }

        public char KdbType
        {
            get { return kdbType; }
        }

        public Type MappedType
        {
            get { return mappedType; }
        }
    }
}
