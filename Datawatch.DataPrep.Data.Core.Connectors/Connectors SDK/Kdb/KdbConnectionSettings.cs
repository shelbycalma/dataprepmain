﻿using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Datawatch.DataPrep.Data.Core.Connectors.Kdb
{
    public abstract class KdbConnectionSettings : ConnectionSettings
    {
        private TimeZoneHelper timeZoneHelper;
        
        public KdbConnectionSettings() : this(new PropertyBag())
        {
        }

        public KdbConnectionSettings(PropertyBag properties)
            : base(properties)
        {
            timeZoneHelper = KdbUtil.GetTimeZoneHelper(properties);
        }

        public string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value); }
        }

        public int Port
        {
            get { return GetInternalInt("Port", 5001); }
            set { SetInternalInt("Port", value); }
        }

        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string UserAuthString
        {
            get 
            {
                string userId = "";
                if (!string.IsNullOrEmpty(UserName))
                {
                    userId = UserName + ":" + Password;
                }
                return userId;
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return timeZoneHelper;
            }
        }

        public SqlDialect SqlDialect
        {
            get
            {
                return SqlDialectFactory.KxQ;
            }
        }

        public abstract ICommand TestConnection { get; }

        
    }
}
