﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    internal class RealtimeDataPluginTableKey<C>
        where C : RealtimeConnectionSettings
    {
        private readonly C settings;
        private readonly IEnumerable<ParameterValue> parameters;
        private readonly int rowlimit;

        public RealtimeDataPluginTableKey(C settings,
            IEnumerable<ParameterValue> parameters, int rowlimit)
        {
            if (settings == null)
            {
                throw Exceptions.ArgumentNull("settings");
            }

            this.settings = settings;
            this.parameters = parameters;
            this.rowlimit = rowlimit;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RealtimeDataPluginTableKey<C>))
            {
                return false;
            }
            RealtimeDataPluginTableKey<C> other =
                (RealtimeDataPluginTableKey<C>) obj;
            if (!settings.Equals(other.settings))
            {
                return false;
            }
            if (rowlimit != other.rowlimit)
            {
                return false;
            }
            
            if (parameters == null)
            {
                return other.parameters == null;
            }
            if (other.parameters == null)
            {
                return false;
            }
            List<ParameterValue> list1 = new List<ParameterValue>(parameters);
            List<ParameterValue> list2 = new List<ParameterValue>(other.parameters);
            
            return Utils.SetEquals(list1.ToArray(), list2.ToArray());
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + settings.GetHashCode() + rowlimit;

            if (parameters == null)
            {
                return hash;
            }

            foreach (ParameterValue parameterValue in parameters) {
                hash ^= parameterValue.GetHashCode();
            }

            return hash;
        }
    }
}
