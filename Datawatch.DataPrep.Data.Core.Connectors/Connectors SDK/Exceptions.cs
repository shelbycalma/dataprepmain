﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors.Properties;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class Exceptions : Core.Exceptions
    {
        public static Exception BadRealtimeLimit()
        {
            return CreateArgument(Resources.ExInvalidRealtimeLimit);
        }

        public static Exception PluginNotLicensed(Type type)
        {
            return CreateUnspecifiedInternal(
                Format(Resources.ExPluginNotLicensed, type.ToString()));
        }

        public static Exception PluginNotLicensed(Type type, string component)
        {
            // This guy is to simulate the old part plugin error. It didn't
            // give the type for the part plugin, just the component.
            return CreateUnspecifiedInternal(
                Format(Resources.ExPluginNotLicensed, component));
        }


        public static Exception PluginDependenciesAreNotFound(string pluginId, Exception exception)
        {
            return CreateArgument(
                Format(Resources.ExPluginDependenciesAreNotFound,
                pluginId,
                exception.Message));
        }

        public static Exception DataAdaptorWasNull()
        {
            return CreateArgument(Resources.ExDataAdaptorWasNull);
        }

        internal static Exception NoColumns()
        {
            return CreateInvalidOperation(Resources.ExNoColumns);
        }
    }
}
