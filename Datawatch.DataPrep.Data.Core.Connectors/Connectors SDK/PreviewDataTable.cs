﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
	public class PreviewDataTable
	{
		private static DbType ColumnDbTypeConverter(IColumn column)
		{
			if (column is NumericColumn)
				return DbType.Double;
			if (column is TimeColumn)
				return DbType.DateTime;
			//if (column is TextColumn)
			// the default will be string.
			return DbType.String;

		}

		private static object GetColumnValue(IColumn column, Row row)
		{
			if (column is TextColumn)
			{
				var text = GetTextColumnValue((column as TextColumn), row);
				return text != null ? (object)text : DBNull.Value;
			}
			if (column is TimeColumn)
			{
				var dateTime = GetTimeValue((column as TimeColumn), row);
				//return DatabaseUtility.InBounds(dateTime) ? (object)dateTime : DBNull.Value;
				return (object)dateTime;
			}
			if (column is NumericColumn)
			{
				var dblVal = GetNumericValue((column as NumericColumn), row);
				//return MathUtility.IsFinite(dblVal) ? (object)dblVal : DBNull.Value;
				return (object)dblVal;
			}
			return DBNull.Value;
		}

		private static String GetTextColumnValue(TextColumn column, Row row)
		{
			return column.GetTextValue(row);
		}

		/// <summary>
		/// Get the DateTime value from a row, column.
		/// </summary>
		/// <param name="column">The column to get from.</param>
		/// <param name="row">The row to get from.</param>
		/// <returns>The DateTime from the row, column.</returns>
		private static DateTime GetTimeValue(TimeColumn column, Row row)
		{
			return column.GetTimeValue(row);
		}

		/// <summary>
		/// Get the Double value from a row, column.
		/// </summary>
		/// <param name="column">The column to get from.</param>
		/// <param name="row">The row to get from.</param>
		/// <returns>The Double from the row, column.</returns>
		private static Double GetNumericValue(NumericColumn column, Row row)
		{
			return column.GetNumericValue(row);
		}

		public object GetValue(ITable table, Row row, int i)
		{
			return GetColumnValue(table.GetColumn(i), row);
		}

		public static DataTable GetPreviewTable(ITable table)
		{
			/**
             **********************ITable SchemaTable ***********************
             * | ColumnName | ColumnOrdinal | DbType | Size | IsLong |
             */
			var dataTable = new DataTable();
			var iColumns = Enumerable.Range(0, (table.ColumnCount)).Select(table.GetColumn);

			foreach (var iColumn in iColumns)
			{
				dataTable.Columns.Add(iColumn.Name);
			}

			for (int j = 0; j < table.RowCount; j++)
			{
				Row row = table.GetRow(j) as Row;
				var newrow = dataTable.NewRow();
				foreach (var iColumn in iColumns)
				{
					newrow[iColumn.Name] = GetColumnValue(iColumn, row);
				}
				dataTable.Rows.Add(newrow);
			}

			return dataTable;
		}
	}
}
