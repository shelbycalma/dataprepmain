﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Xml;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class MessageQueueSettingsBase : RealtimeConnectionSettings,
        IDisposable, IMessageQueueConnectorSettings,
        IParameterizedMessageQueueSettings
    {
        private const string NumericDataHelperKey = "NumericDataHelper";
        private readonly NumericDataHelper numericDataHelper;
        private bool disposed;
        protected readonly PropertyBag parserSettingsBag;
        protected ParserSettings parserSettings;
        protected IPluginManager pluginManager;
        protected IEnumerable<IParserPlugin> parserPlugins;
        protected IPluginErrorReportingService errorReporter;

        protected abstract string DefaultParserPluginId { get; }
        private bool newSetting;

        private IEnumerable<ParameterValue> parameters;

        public MessageQueueSettingsBase(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag())
        {
            newSetting = true;
        }

        public MessageQueueSettingsBase(IPluginManager pluginManager, PropertyBag bag)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public MessageQueueSettingsBase(IPluginManager pluginManager,
            PropertyBag bag, IEnumerable<ParameterValue> parameters)
            : base(bag, false)
        {
            this.pluginManager = pluginManager;
            this.errorReporter = pluginManager.ErrorReporter;
            this.parameters = parameters;

            //remove duplicate ParserSettings,old workbooks may contain multiple
            //ParserSettings.
            if (bag.SubGroups.Count > 1)
            {
                PropertyBag latestParserBag = bag.SubGroups["ParserSettings"];
                for (int i = bag.SubGroups.Count; i-- > 0; )
                {
                    if (bag.SubGroups[i].Name.Equals("ParserSettings"))
                    {
                        bag.SubGroups.RemoveAt(i);
                    }
                }

                bag.SubGroups["ParserSettings"] = latestParserBag;
            }

            // If the Columns SubGroup is directly beneath the PropertyBag root
            // this is an old style work book (saved before ActiveMQ data plugin
            // was added) and need to be handled as a special case.
            if (bag.SubGroups.ContainsKey("Columns"))
            {
                // Move the Columns SubGroup into a newly created ParserSetting
                // SubGroup to make the XPathMessageParserSettings parse it as
                // expected.
                PropertyBag parserBag = new PropertyBag();
                parserBag.Name = "ParserSettings";

                parserBag.SubGroups.Add(bag.SubGroups["Columns"]);
                parserBag.Values["IdColumn"] = bag.Values["IdColumn"];
                bag.SubGroups["ParserSettings"] = parserBag;
                bag.SubGroups.Remove("Columns");
                bag.Values.Remove("IdColumn");
            }

            parserSettingsBag = bag.SubGroups["ParserSettings"];

            if (parserSettingsBag == null)
            {
                parserSettingsBag = new PropertyBag();
                bag.SubGroups["ParserSettings"] = parserSettingsBag;
            }

            parserSettingsBag.PropertyChanged += 
                parserSettingsBag_PropertyChanged;

            PropertyBag numericDataHelperBag = bag.SubGroups[NumericDataHelperKey];
            if (numericDataHelperBag == null)
            {
                numericDataHelperBag = new PropertyBag();
                bag.SubGroups[NumericDataHelperKey] = numericDataHelperBag;
                numericDataHelper = new NumericDataHelper(numericDataHelperBag);
                numericDataHelper.DecimalSeparator = 
                    Convert.ToChar(CultureInfo.InvariantCulture.NumberFormat.
                        NumberDecimalSeparator);
            }
            else
            {
                numericDataHelper = new NumericDataHelper(numericDataHelperBag);
            }

            UpdateIdCandidates();
        }

        ~MessageQueueSettingsBase()
        {
            Dispose(false);
        }

        public virtual bool ShowFilter
        {
            get
            {
                return true;
            }
        }

        public virtual bool CanGenerateColumns()
        {
            return false;
        }

        public virtual void DoGenerateColumns()
        {
        }

        public virtual bool CanTestConnection()
        {
            return false;
        }

        public virtual void DoTestConnection()
        {
        }

        /// <summary>
        /// Uri to the broker server.
        /// </summary>
        public string Broker
        {
            get { return GetInternal("Broker"); }
            set { SetInternal("Broker", value); }
        }

        public string ParserPluginId
        {
            get { return GetInternal("ParserPluginId", DefaultParserPluginId); }
        }

        public virtual IParserPlugin ParserPlugin
        {
            get
            {
                string id = ParserPluginId;
                IParserPlugin parserPlugin = pluginManager.GetParserPlugin(id);
                return parserPlugin;
            }
            set
            {
                if (value == null)
                {
                    PropertyBag bag = ToPropertyBag();
                    bag.SubGroups.Remove("ParserPluginId");
                }
                else
                {
                    IParserPlugin oldParserPlugin = ParserPlugin;
                    SetInternal("ParserPluginId", value.Id);
                    if (oldParserPlugin.GetParserType() == value.GetParserType())
                    {
                        return;
                    }
                }
                if (parserSettings != null)
                {
                    parserSettings.Dispose();
                    parserSettings = null;
                }
                FirePropertyChanged("ParserSettings");
                FirePropertyChanged("Description");
                FirePropertyChanged("ShowDescription");
            }
        }

        public virtual IEnumerable<IParserPlugin> ParserPlugins
        {
            get
            {
                if (parserPlugins == null)
                {
                    parserPlugins = pluginManager.ParserPlugins;
                }
                return parserPlugins;
            }
        }

        public virtual bool ShowParserSelector
        {
            get
            {
                IEnumerable<IParserPlugin> pluginsIterator = ParserPlugins;
                int cnt = 0;
                foreach (IParserPlugin plugin in pluginsIterator)
                {
                    cnt++;
                }
                return cnt > 1;
                //return MessageTypes.Length > 1;
            }
        }

        public virtual bool ShowTestConnectorButton
        {
            get { return false; }
        }

        public virtual bool ShowGenerateColumnButton
        {
            get { return false; }
        }

        public virtual bool ShowAddColumnButton
        {
            get { return true; }
        }

        public virtual ParserSettings ParserSettings
        {
            get
            {
                if (parserSettings != null) return parserSettings;

                IParserPlugin parserPlugin = ParserPlugin;
                if (parserPlugin == null)
                {
                    return EmptyParserSettings.Instance;
                }
                parserSettings = 
                    parserPlugin.CreateParserSettings(parserSettingsBag);
                if (newSetting)
                {
                    parserSettings.InitiateNew(true);
                    newSetting = false;
                } else
                {
                    parserSettings.InitiateOld(true);
                }
                return parserSettings;
            }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public virtual string Topic
        {
            get { return GetInternal("Topic"); }
            set { SetInternal("Topic", value); }
        }

        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        public virtual void UpdateIdCandidates()
        {
            UpdateIdCandidates(null, null);
        }

        public virtual void UpdateIdCandidates(string extraIdCandidate, 
            string extraTimeIdCandidate)
        {
            List<string> idCandidates = new List<string>();
            List<string> timeIdCandidates = new List<string>();

            if (extraIdCandidate != null)
            {
                idCandidates.Add(extraIdCandidate);
            }
            if (extraTimeIdCandidate != null)
            {
                timeIdCandidates.Add(extraTimeIdCandidate);
            }
            foreach (ColumnDefinition columnDefinition in
                ParserSettings.Columns)
            {
                if(!columnDefinition.IsEnabled)
                {
                    continue;
                }
                if (columnDefinition.Type == ColumnType.Text)
                {
                    idCandidates.Add(columnDefinition.Name);
                }
                else if (columnDefinition.Type == ColumnType.Time)
                {
                    timeIdCandidates.Add(columnDefinition.Name);
                }
            }

            IdColumnCandidates = idCandidates.Count > 0 ?
                idCandidates.ToArray() : null;

            TimeIdColumnCandidates = timeIdCandidates.Count > 0 ?
                timeIdCandidates.ToArray() : null;
        }

        public virtual bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(Broker) &&
                    !string.IsNullOrEmpty(Topic) &&
                    !string.IsNullOrEmpty(IdColumn) &&
                    IsParserSettingsOk;
            }
        }

        public bool IsParserSettingsOk
        {
            get
            {
                if (string.IsNullOrEmpty(IdColumn))
                {
                    return false;
                }

                foreach (ColumnDefinition colDef in ParserSettings.Columns)
                {
                    if (!colDef.IsValid)
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        public NumericDataHelper NumericDataHelper
        {
            get { return numericDataHelper; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MessageQueueSettingsBase)) return false;

            if (!base.Equals(obj)) return false;

            MessageQueueSettingsBase cs = (MessageQueueSettingsBase)obj;

            if (!string.Equals(Broker, cs.Broker) ||
                !string.Equals(UserName, cs.UserName) ||
                !string.Equals(Password, cs.Password) ||
                !string.Equals(Topic, cs.Topic) ||
                IsCombineEvents != cs.IsCombineEvents ||
                !string.Equals(GetId(ParserPlugin), GetId(cs.ParserPlugin)) ||
                !NumericDataHelper.Equals(cs.NumericDataHelper))
            {
                return false;
            }

            if (ParserSettings == null)
            {
                return cs.ParserSettings == null;
            }

            return ParserSettings.Equals(cs.ParserSettings);
        }

        private string GetId(IParserPlugin plugin)
        {
            return plugin != null ? plugin.Id : "";
        }
        // For old workbooks. The IdColumn was not stored in ConnectionSettings, 
        // but instead in the ParserSettings, so we first try to read the 
        // IdColumn from the ConnectionSettings, if it's not there, it's 
        // probably an old workbook so then we read it up from the ParserSettings. 
        // But when we save the idColumn, we save it always in the 
        // ConnectionSettings.
        public override string IdColumn
        {
            get
            {
                string idColumn = base.IdColumn;
                if (string.IsNullOrEmpty(idColumn))
                {
                    // Dig out the IdColumn from the ParserSettings
                    idColumn = parserSettingsBag.Values["IdColumn"]; 
                }
                return idColumn;
            }
            set 
            { 
                base.IdColumn = value;
                if (!string.IsNullOrEmpty(parserSettingsBag.Values["IdColumn"]))
                {
                    // Remove old storage. IdColumn should only be stored 
                    // under ConnectionSettings, not under the ParsingSettings.
                    parserSettingsBag.Values["IdColumn"] = null;
                }
            }

        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields = { Broker, Topic, UserName, Password, 
                                  IsCombineEvents, GetId(ParserPlugin), NumericDataHelper };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            if (ParserSettings != null)
            {
                hashCode ^= ParserSettings.GetHashCode();
            }

            return hashCode;
        }

        public string GenerateTitle(string prefix)
        {
            // documentation
            string title = string.Join("_", new[]
                {
                    prefix == null ? "" : prefix.Trim(),
                    Broker == null ? "" : Broker.Trim(),
                    Topic == null ? "" : Topic.Trim()
                }, 0, 3);
            title = title.Trim(new[] { '_' });
            title = title.Replace(" ", "_");
            title = Regex.Replace(title, @"[^\w-]+", "");
            title = title.Replace("_", " ");
            return title;
        }

        public bool IsCombineEvents
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsCombineEvents", defaultValue));
            }
            set
            {
                SetInternal("IsCombineEvents",
                    XmlConvert.ToString(value));
            }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get
            {
                return parameters;
            }
            set
            {
                parameters = value;
            }
        }

        private void parserSettingsBag_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName.StartsWith("SubGroup[Columns]"))
            {
                UpdateIdCandidates();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    parserSettingsBag.PropertyChanged -=
                        parserSettingsBag_PropertyChanged;

                    parserSettings.Dispose();
                }
                disposed = true;
            }
        }
    }
}
