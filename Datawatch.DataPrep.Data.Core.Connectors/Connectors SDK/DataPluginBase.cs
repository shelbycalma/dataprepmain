﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class DataPluginBase<C> : IDataPlugin, IDisposable where C : ConnectionSettings
    {
        private bool disposed;

        protected bool isHosted;
        protected PropertyBag settings;
        protected PropertyBag globalSettings;
        protected IPluginManager pluginManager;
        protected IPluginErrorReportingService errorReporter;

        public abstract C CreateSettings(PropertyBag bag);

        public virtual ITable GetAllData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters, -1,
                false);
        }

        public virtual ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            int rowcount)
        {
            return GetData(workbookDir, dataDir, settings, parameters, rowcount,
                true);
        }

        public abstract ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration);


        public virtual string[] GetDataFiles(PropertyBag settings)
        {
            return null;
        }

        public virtual string DataPluginType
        {
            get { return Core.Properties.Resources.UiDataPluginTypeDatabase; }
        }

        public IPluginManager PluginManager { get {return pluginManager;} }

        public virtual void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            
        }

        /// <summary>
        /// Utility method that throws an exception if <see cref="IsLicensed"/>
        /// return <c>false</c>.
        /// </summary>
        protected void CheckLicense()
        {
            if (!this.IsLicensed) {
                throw Exceptions.PluginNotLicensed(GetType());
            }
        }

        #region IPlugin Members

        public virtual void Initialize(bool isHosted, IPluginManager pluginManager,
            PropertyBag settings, PropertyBag globalSettings)
        {
            this.isHosted = isHosted;
            this.settings = settings;
            this.globalSettings = globalSettings ?? new PropertyBag();
            this.pluginManager = pluginManager;
            this.errorReporter = pluginManager.ErrorReporter;
        }

        /// <summary>
        /// Gets the instance of plugin error reporting services.
        /// </summary>
        /// <value>
        /// The plugin error reporting service instance.
        /// </value>
        public IPluginErrorReportingService ErrorReporter { get { return this.errorReporter; } }

        public abstract string Id
        {
            get;
        }

        public abstract string Title
        {
            get;
        }

        public virtual bool IsObsolete
        {
            get { return false; }
        }

        public PropertyBag Settings
        {
            get { return settings; }
        }

        public PropertyBag GlobalSettings
        {
            get { return globalSettings; }
        }

        public virtual bool IsLicensed
        {
            get { return true; }
        }
        #endregion

        ~DataPluginBase()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Managed.
                }
                // Unmanaged.
                disposed = true;
            }
        }
    }
}
