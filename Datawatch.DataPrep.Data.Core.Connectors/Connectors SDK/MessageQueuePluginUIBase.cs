﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public abstract class MessageQueuePluginUIBase<TT, TC, TE, TR, T> : PluginUIBase<T, TC>
        where TT : ParameterTable
        where TC : MessageQueueSettingsBase
        where TE : Dictionary<string, object>
        where TR : MessageQueueAdapterBase<TT, TC, TE>, new()
        where T : MessageQueuePluginBase<TT, TC, TE, TR>
    {
        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        private readonly string title;

        protected MessageQueuePluginUIBase(T plugin, Window owner, string imageUri)
            : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }

            this.title = title;
        }

        public abstract Window CreateConnectionWindow(TC settings);

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            TC con = Plugin.CreateConnectionSettings(parameters);
            Window win = CreateConnectionWindow(con);

            return DataPluginUtils.ShowDialog(owner, win, con);
        }
    }
}
