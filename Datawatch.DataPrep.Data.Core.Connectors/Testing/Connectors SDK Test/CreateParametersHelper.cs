﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    /**
     * Unit Test Helper Class.
     * Use this class to create a Iterable<ParameterValue> out of arrays of names  
     * and values. The array of values can be of type string, Double, Date or a new 
     * array. In case of a new array, then this is will create a multi value 
     * parameter. That array must only have values have to be string, Double or Date.
     * 
     * Example: 
     * List<object> list = new List<object>();
     * Iterable<ParameterValue> pvList;
     * pvList = ParameterValuehelper.create(new string[]{"Industry", "Region"}, 
     *              new object[]{"MobileSystem", new object[]{"Europe","Asia"}});
     * 
     */
    class CreateParametersHelper
    {
        /// <summary>
        /// Use this method to create an IEnumerable<ParameterValue/> list based 
        /// on a list of names and values. names[i] and values[i] is a pair. 
        /// </summary>
        /// <param name="names"> List of parameter names. </param>
        /// <param name="values"> List of values. Should come in as string, 
        /// Double or Date.</param>
        /// <returns> A Iterable<ParameterValue/> is returned. </returns>
        public static IEnumerable<ParameterValue> CreateParameters(string[] names, 
                object[] values) 
        {
        
            //ParameterValueCollection pvc = new ParameterValueCollection();
            List<ParameterValue> parameters = new List<ParameterValue>();
            if (names.Length != values.Length) 
            {
                // Different length. Each name must have a value and a type!
                throw new Exception(
                        "CreateParametersHelper->createParameters() (UNIT TEST): "+
                        " Parameters names and values got different length");
            }
        
            for (int i = 0; i < names.Length; i++) 
            {
                ParameterValue pv;
                if (values[i] is object[]) 
                {
                    pv = CreateArrayParameterValue(names[i], (object[])values[i]);
                } else 
                {
                    TypedParameterValue spv = MakeParameterValue(names[i], values[i]);
                    pv = new ParameterValue(names[i], spv);
                }
                parameters.Add(pv);
            }
            return parameters;
        }
    
        public static TypedParameterValue MakeParameterValue(string name, 
                object obj)
        {

            if (obj is string)
            {
                return new StringParameterValue() {Value = (string) obj};
            }
            if (obj is Double)
            {
                return new NumberParameterValue() {Value = (Double) obj};
            } 
            if (obj is DateTime) 
            {
                return new DateTimeParameterValue() { Value = (DateTime)obj };
            } 
            // Should never come here!
            throw new Exception(
                    "CreateParametersHelper->createParameters() (UNIT TEST): "+
                    " Bad type used in UNIT Test. Type " + 
                    obj.GetType().Name+" not alowed!");
        }

        public static ParameterValue CreateArrayParameterValue(string name, 
                object[] objects) 
        {
            ParameterValue pv = new ParameterValue();
            ArrayParameterValue array = new ArrayParameterValue();
            foreach (object obj in objects) 
            {
                TypedParameterValue tpv = MakeParameterValue(name, obj);
                array.Add(tpv);
            }
            pv.TypedValue = array;
            pv.Name = name;
            return pv;
        }
    }
}
