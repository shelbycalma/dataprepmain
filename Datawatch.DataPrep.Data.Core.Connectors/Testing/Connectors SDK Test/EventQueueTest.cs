﻿using System;
using System.Collections.Generic;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class EventQueueTest
    {
        private static string[] ToArray(IEnumerator<EventQueueEntry<string>> mark) {
            List<string> list = new List<string>();
            while (mark.MoveNext()) {
                EventQueueEntry<string> next = mark.Current;
                list.Add(next.Event);
            }
        
            return list.ToArray();
        }

        [Fact]
        public void TestCommitWithoutMark()
        {
            EventQueue<string> queue = new EventQueue<string>();
            Assert.Throws<InvalidOperationException>(
                () => queue.CommitMark());
        }

        [Fact]
        public void TestDiscardWithoutMark()
        {
            EventQueue<string> queue = new EventQueue<string>();
            Assert.Throws<InvalidOperationException>(
                () => queue.DiscardMark());
        }

        [Fact]
        public void TestCommitMarkEmpty() {
            EventQueue<string> queue = new EventQueue<string>();
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added yet, should be zero events.");

            queue.CommitMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added after the first Mark() call.");
        }
    
        [Fact]
        public void TestCommitMarkOneEvent() {
            EventQueue<string> queue = new EventQueue<string>();
            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the first Mark() call.");

            queue.CommitMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added after the first Mark() call.");
        }

        [Fact]
        public void TestDiscardMarkEmpty() {
            EventQueue<string> queue = new EventQueue<string>();
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added yet, should be zero events.");

            queue.DiscardMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added after the first Mark() call.");
        }
    
        [Fact]
        public void TestDiscardMarkOneEvent() {
            EventQueue<string> queue = new EventQueue<string>();
            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the first Mark() call.");

            queue.DiscardMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "Should still have one event after the DiscardMark() call.");
        }

        [Fact]
        public void TestCommitMarkEmptyAndAddOneMore() {
            EventQueue<string> queue = new EventQueue<string>();
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added yet, should be zero events.");

            queue.Enqueue(EventType.Insert, "id1", DateTime.Now, "event1");
            
            queue.CommitMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the second Mark() call.");
        }

        [Fact]
        public void TestDiscardMarkEmptyAndAddOneMore() {
            EventQueue<string> queue = new EventQueue<string>();
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added yet, should be zero events.");

            queue.Enqueue(EventType.Insert, "id1", DateTime.Now, "event1");
            
            queue.DiscardMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the second Mark() call.");
        }

        [Fact]
        public void TestCommitMarkEmptyAddOneThenReset() {
            EventQueue<string> queue = new EventQueue<string>();
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added yet, should be zero events.");

            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            queue.Enqueue(EventType.Reset, null, DateTime.MinValue, "reset");
            
            queue.CommitMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "Reset event was added before the second Mark() call.");
            Assert.True("reset" == events[0],
                "Should only have kept the reset event.");
        }

        [Fact]
        public void TestDiscardMarkEmptyAddOneThenReset() {
            EventQueue<string> queue = new EventQueue<string>();
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(0 == events.Length,
                "No events added yet, should be zero events.");

            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            queue.Enqueue(EventType.Reset, null, DateTime.MinValue, "reset");
            
            queue.DiscardMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "Reset event was added before the second Mark() call.");
            Assert.True("reset" == events[0],
                "Should only have kept the reset event.");
        }

        [Fact]
        public void TestCommitMarkOneEventAndAddOneMore() {
            EventQueue<string> queue = new EventQueue<string>();
            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the first Mark() call.");

            queue.Enqueue(EventType.Insert, "id1", DateTime.Now, "event1");
            
            queue.CommitMark();
        
            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the second Mark() call.");
        }

        [Fact]
        public void TestDiscardMarkOneEventAndAddOneMore() {
            EventQueue<string> queue = new EventQueue<string>();
            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the first Mark() call.");

            queue.Enqueue(EventType.Insert, "id1", DateTime.Now, "event1");
            
            queue.DiscardMark();

            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(2 == events.Length,
                "One event was added before the DiscardMark() call.");
        }

        [Fact]
        public void TestDiscardWithPendingReset() {
            EventQueue<string> queue = new EventQueue<string>();
            queue.Enqueue(EventType.Insert, "id0", DateTime.Now, "event0");
            IEnumerator<EventQueueEntry<string>> mark = queue.Mark();
            string[] events = ToArray(mark);
            Assert.True(1 == events.Length,
                "One event was added before the first Mark() call.");

            queue.Enqueue(EventType.Reset, null, DateTime.MinValue, "reset");

            queue.DiscardMark();

            mark = queue.Mark();
            events = ToArray(mark);
            Assert.True(1 == events.Length,
                "Reset should remove all events before it.");
            Assert.True("reset" == events[0],
                "Should only have kept the reset event.");
        }
    }
}
