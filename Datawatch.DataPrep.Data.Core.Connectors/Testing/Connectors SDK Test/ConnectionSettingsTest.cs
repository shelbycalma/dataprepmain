﻿using System;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class ConnectionSettingsTest
    {
        private int propertyChangedCounter;

        private void settings_TitlePropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            Assert.Equal("Title", e.PropertyName);
            propertyChangedCounter++;
        }

        private void settings_NamePropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            Assert.Equal("Name", e.PropertyName);
            propertyChangedCounter++;
        }

        [Fact]
        public void ConnectionSettings_DefaultConstructor()
        {
            ConnectionSettings settings = new ConnectionSettings();
            
            PropertyBag bag = settings.ToPropertyBag();
            Assert.NotNull(bag);
            Assert.Empty(bag.Values);
            Assert.Empty(bag.SubGroups);
        }

        [Fact]
        public void ConnectionSettings_BagConstructor()
        {
            PropertyBag bag = new PropertyBag();
            ConnectionSettings settings = new ConnectionSettings(bag);
        }

        [Fact]
        public void ConnectionSettings_NullBagConstructor()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                ConnectionSettings settings = new ConnectionSettings(null);

                // Should not even get here.
                string title = settings.Title;
            });
        }

        [Fact]
        public void ConnectionSettings_Title()
        {
            ConnectionSettings settings = new ConnectionSettings();
            Assert.Null(settings.Title);

            settings.Title = "X";
            Assert.Equal("X", settings.Title);

            PropertyBag bag = settings.ToPropertyBag();
            Assert.Equal("X", bag.Values["Title"]);
        }

        [Fact]
        public void ConnectionSettings_SameBag()
        {
            PropertyBag bagOne = new PropertyBag();
            
            ConnectionSettings settings = new ConnectionSettings(bagOne);

            PropertyBag bagTwo = settings.ToPropertyBag();
            Assert.Same(bagOne, bagTwo);

            settings.Title = "X";
            PropertyBag bagThree = settings.ToPropertyBag();
            Assert.Same(bagOne, bagThree);
        }

        [Fact]
        public void ConnectionSettings_NullSettingIsRemoved()
        {
            ConnectionSettings settings = new ConnectionSettings();
            settings.Title = "X";

            PropertyBag bag = settings.ToPropertyBag();
            Assert.Equal("X", bag.Values["Title"]);

            settings.Title = null;
            Assert.Empty(bag.Values);
        }

        [Fact]
        public void ConnectionSettings_PropertyChangedFired()        
        {
            ConnectionSettings settings = new ConnectionSettings();
            settings.PropertyChanged += settings_TitlePropertyChanged;
            propertyChangedCounter = 0;

            settings.Title = "X";
            Assert.Equal(1, propertyChangedCounter);

            settings.Title = "X";
            Assert.Equal(1, propertyChangedCounter);

            settings.Title = "Y";
            Assert.Equal(2, propertyChangedCounter);
        }

        [Fact]
        public void ConnectionSettings_GetNullName()
        {
            var exc = Assert.Throws<ArgumentNullException>(() =>
            {
                MockedSettings settings = new MockedSettings();
                string value = settings.Get(null);
            });
        }

        [Fact]
        public void ConnectionSettings_SetNullName()
        {
            var exc = Assert.Throws<ArgumentNullException>(() =>
            {
                MockedSettings settings = new MockedSettings();
                settings.Set(null, "X");
            });
        }

        [Fact]
        public void ConnectionSettings_GetIntNullName()
        {
            var exc = Assert.Throws<ArgumentNullException>(() =>
            {
                MockedSettings settings = new MockedSettings();
                int value = settings.GetInt(null, 0);
            });
        }

        [Fact]
        public void ConnectionSettings_SetIntNullName()
        {
            var exc = Assert.Throws<ArgumentNullException>(() =>
            {
                MockedSettings settings = new MockedSettings();
                settings.SetInt(null, 0);
            });
        }

        [Fact]
        public void ConnectionSettings_IntRoundtrip()
        {
            MockedSettings settings = new MockedSettings();

            settings.SetInt("Name", 17);
            Assert.Equal(17, settings.GetInt("Name", -1));

            PropertyBag bag = settings.ToPropertyBag();
            Assert.Equal(Convert.ToString(17), bag.Values["Name"]);

            bag.Values["Name"] = "Bangalla";
            
            Assert.Equal(-1, settings.GetInt("Name", -1));
        }

        [Fact]
        public void ConnectionSettings_IntDefaults()
        {
            MockedSettings settings = new MockedSettings();
            
            Assert.Equal(47, settings.GetInt("Name", 47));
            Assert.Equal(0, settings.GetInt("Name", 0));
            Assert.Equal(1, settings.GetInt("Name", 1));

            settings.SetInt("Name", 1);
            Assert.Equal(1, settings.GetInt("Name", -1));
            
            settings.SetInt("Name", 0);
            Assert.Equal(0, settings.GetInt("Name", -1));
        }

        [Fact]
        public void ConnectionSettings_IntPropertyChangedZeroFirst()
        {
            MockedSettings settings = new MockedSettings();
            settings.PropertyChanged += settings_NamePropertyChanged;
            propertyChangedCounter = 0;

            settings.SetInt("Name", 0);
            Assert.Equal(1, propertyChangedCounter);

            settings.SetInt("Name", 0);
            Assert.Equal(1, propertyChangedCounter);

            settings.SetInt("Name", 1);
            Assert.Equal(2, propertyChangedCounter);
        }

        [Fact]
        public void ConnectionSettings_IntPropertyChangedOneFirst()
        {
            MockedSettings settings = new MockedSettings();
            settings.PropertyChanged += settings_NamePropertyChanged;
            propertyChangedCounter = 0;

            settings.SetInt("Name", 1);
            Assert.Equal(1, propertyChangedCounter);

            settings.SetInt("Name", 1);
            Assert.Equal(1, propertyChangedCounter);

            settings.SetInt("Name", 0);
            Assert.Equal(2, propertyChangedCounter);
        }

        private class MockedSettings : ConnectionSettings
        {
            public string Get(string name)
            {
                return GetInternal(name);
            }

            public void Set(string name, string value)
            {
                SetInternal(name, value);
            }

            public int GetInt(string name, int defaultValue)
            {
                return GetInternalInt(name, defaultValue);
            }

            public void SetInt(string name, int value)
            {
                SetInternalInt(name, value);
            }
        }
    }
}
