﻿using System;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class ChunkedStringHelperTest
    {
        [Fact]
        public void ConstructorEmptyBagTest()
        {
            ChunkedStringHelper helper = new ChunkedStringHelper(
                new PropertyBag());
        }

        [Fact]
        public void ConstructorNoBagTest()
        {
            var exc = Assert.Throws<ArgumentNullException>(() =>
                new ChunkedStringHelper(null));
        }

        [Fact]
        public void DefaultValueTest()
        {
            ChunkedStringHelper helper = new ChunkedStringHelper(
                new PropertyBag());
            Assert.True(string.IsNullOrEmpty(helper.Text));
        }

        [Fact]
        public void ShortTextTest()
        {
            ChunkedStringHelper helper = new ChunkedStringHelper(
                new PropertyBag());
            helper.Text = "short text";
            Assert.Equal("short text", helper.Text);
        }

        [Fact]
        public void ShortTextRoundtripTest()
        {
            PropertyBag bag = new PropertyBag();
            ChunkedStringHelper helper = new ChunkedStringHelper(bag);
            helper.Text = "short text";

            helper = new ChunkedStringHelper(bag);
            Assert.Equal("short text", helper.Text);
        }

        //[Fact]
        //public void ShortTextCanSerializeWorkbookTest()
        //{
        //    // piggyback on the workbook style PropertyBag
        //    Workbook workbook = new Workbook();
        //    ChunkedStringHelper helper = new ChunkedStringHelper(
        //        workbook.Style);
        //    helper.Text = "short text";

        //    byte[] buffer;
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        WorkbookSerializer.Save(ms, workbook);
        //        ms.Close();
        //        buffer = ms.ToArray();
        //    }
        //    using (MemoryStream ms = new MemoryStream(buffer))
        //    {
        //        workbook = WorkbookSerializer.Load(ms);
        //    }
        //    helper = new ChunkedStringHelper(workbook.Style);
        //    Assert.Equal("short text", helper.Text);
        //}

        [Fact]
        public void LongTextTest()
        {
            ChunkedStringHelper helper = new ChunkedStringHelper(
                new PropertyBag());
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 10000; i++)
            {
                sb.Append("substring");
            }
            helper.Text = sb.ToString();
            Assert.Equal(sb.ToString(), helper.Text);
        }

        [Fact]
        public void LongTextRoundtripTest()
        {
            PropertyBag bag = new PropertyBag();
            ChunkedStringHelper helper = new ChunkedStringHelper(bag);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 10000; i++)
            {
                sb.Append("substring");
            }
            helper.Text = sb.ToString();

            helper = new ChunkedStringHelper(bag);
            Assert.Equal(sb.ToString(), helper.Text);
        }

        //[Fact]
        //public void LongTextCanSerializeWorkbookTest()
        //{
        //    // Dont want to get a SerializationException in this test

        //    // piggyback on the workbook style PropertyBag
        //    Workbook workbook = new Workbook();
        //    ChunkedStringHelper helper = new ChunkedStringHelper(
        //        workbook.Style);
        //    StringBuilder sb = new StringBuilder();
        //    for (int i = 0; i < 10000; i++)
        //    {
        //        sb.Append("substring");
        //    }
        //    helper.Text = sb.ToString();

        //    byte[] buffer;
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        WorkbookSerializer.Save(ms, workbook);
        //        ms.Close();
        //        buffer = ms.ToArray();
        //    }
        //    using (MemoryStream ms = new MemoryStream(buffer))
        //    {
        //        workbook = WorkbookSerializer.Load(ms);
        //    }
        //    helper = new ChunkedStringHelper(workbook.Style);
        //    Assert.Equal(sb.ToString(), helper.Text);
        //}

        [Fact]
        public void WriteReadTest()
        {
            ChunkedStringHelper helper = new ChunkedStringHelper(
                new PropertyBag());
            helper.Text = "sometext";
            Assert.Equal("sometext", helper.Text);
        }

        [Fact]
        public void ReadWriteReadTest()
        {
            ChunkedStringHelper helper = new ChunkedStringHelper(
                new PropertyBag());
            Assert.True(string.IsNullOrEmpty(helper.Text));
            helper.Text = "sometext";
            Assert.Equal("sometext", helper.Text);
        }

        [Fact]
        public void LongTextThenWriteShorterTest()
        {
            PropertyBag bag = new PropertyBag();
            ChunkedStringHelper helper = new ChunkedStringHelper(bag);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 10000; i++)
            {
                sb.Append("longer");
            }
            helper.Text = sb.ToString();

            sb.Clear();
            for (int i = 0; i < 5000; i++)
            {
                sb.Append("shorter");
            }
            helper.Text = sb.ToString();

            helper = new ChunkedStringHelper(bag);
            Assert.Equal(sb.ToString(), helper.Text);
        }
    }
}
