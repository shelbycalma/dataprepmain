﻿using System;
using System.IO;
using System.Reflection;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class MessageQueueSettingsBaseTest
    {
        private IPluginManager pluginManager;

        public MessageQueueSettingsBaseTest()
        {
            pluginManager =
                new PluginManager(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
            pluginManager.LoadPlugins();
        }


        [Fact]
        public void Equals_Blank_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager, new PropertyBag());
            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager, new PropertyBag());

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_IdColumn_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.IdColumn = "id";

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.IdColumn = "id";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IdColumn = "otherid";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsTimeIdColumnGenerated_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.IsTimeIdColumnGenerated = true;

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.IsTimeIdColumnGenerated = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsTimeIdColumnGenerated = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Limit_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.Limit = 4211;

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.Limit = 4211;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Limit = 123;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_TimeIdColumn_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.TimeIdColumn = "timeid";

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.TimeIdColumn = "timeid";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeIdColumn = "othertimeid";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_TimeWindowSeconds_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.TimeWindowSeconds = 7;

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Broker_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.Broker = "broker";

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.Broker = "broker";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Broker = "otherbroker";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Password_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.Password = "passwd";

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.Password = "passwd";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "otherpasswd";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Topic_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.Topic = "topic";

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.Topic = "topic";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Topic = "othertopic";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Username_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            a.UserName = "username";

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Columns_Test()
        {
            MessageQueueSettingsBase a = new MyMessageQueueSettingsBase(pluginManager);
            MyColumnDefinition colDef = new MyColumnDefinition(new PropertyBag());
            colDef.Name = "name1";
            a.ParserSettings.AddColumnDefinition(colDef);

            MessageQueueSettingsBase b = new MyMessageQueueSettingsBase(pluginManager);
            colDef = new MyColumnDefinition(new PropertyBag());
            colDef.Name = "name1";
            b.ParserSettings.AddColumnDefinition(colDef);

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            colDef = new MyColumnDefinition(new PropertyBag());
            colDef.Name = "name2";
            b.ParserSettings.AddColumnDefinition(colDef);
            Assert.NotEqual(a, b);

            b.ParserSettings.RemoveColumnDefinition(1);
            Assert.Equal(a, b);
            b.ParserSettings.RemoveColumnDefinition(0);
            Assert.NotEqual(a, b);
        }
    }

    class MyColumnDefinition : ColumnDefinition
    {

        public MyColumnDefinition(PropertyBag bag)
            : base(bag)
        {
        }

        public override bool IsValid
        {
            get { return !string.IsNullOrEmpty(Name); }
        }
    }

    class MyMessageQueueSettingsBase : MessageQueueSettingsBase
    {
        private IParserPlugin parserPlugin;

        public MyMessageQueueSettingsBase(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public MyMessageQueueSettingsBase(IPluginManager pluginManager, PropertyBag bag)
            : base(pluginManager, bag)
        {
        }

        protected override string DefaultParserPluginId
        {
            get { return "My"; }
        }

        public override ParserSettings ParserSettings
        {
            get
            {
                if (parserSettings != null) return parserSettings;

                parserPlugin =
                    new MyParserPlugin();
                parserSettings =
                    parserPlugin.CreateParserSettings(parserSettingsBag);
                return parserSettings;
            }
        }

    }

    class MyParserPlugin : ParserPluginBase, IParserPlugin
    {

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new MyParserSettings(bag);
        }

        public IParser CreateParser(ParserSettings settings)
        {
            return null;
        }

        public string GetDescription()
        {
            throw new NotImplementedException();
        }

        public Type GetParserType()
        {
            throw new NotImplementedException();
        }

        public override string Id
        {
            get { return "My"; }
        }

        public override string Title
        {
            get { return "My"; }
        }
    }

    class MyParserSettings : ParserSettings
    {

        public MyParserSettings(PropertyBag bag)
            : base(bag)
        {
        }

        public override string GetDescription()
        {
            return "";
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new MyColumnDefinition(bag);
        }

        public override IParser CreateParser()
        {
            throw new System.NotImplementedException();
        }

    }
}
