﻿
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class RealtimeConnectionSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings();
            RealtimeConnectionSettings b = new RealtimeConnectionSettings();

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings(
                new PropertyBag(), true);
            RealtimeConnectionSettings b = new RealtimeConnectionSettings(
                new PropertyBag(), true);

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_Blank3_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings(
                new PropertyBag(), false);
            RealtimeConnectionSettings b = new RealtimeConnectionSettings(
                new PropertyBag(), false);

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_IdColumn_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings();
            a.IdColumn = "id";
            
            RealtimeConnectionSettings b = new RealtimeConnectionSettings();
            b.IdColumn = "id";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IdColumn = "otherid";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsTimeIdColumnGenerated_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings();
            a.IsTimeIdColumnGenerated = true;
            
            RealtimeConnectionSettings b = new RealtimeConnectionSettings();
            b.IsTimeIdColumnGenerated = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsTimeIdColumnGenerated = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Limit_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings();
            a.Limit = 4211;
            
            RealtimeConnectionSettings b = new RealtimeConnectionSettings();
            b.Limit = 4211;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Limit = 123;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_TimeIdColumn_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings();
            a.TimeIdColumn = "timeid";
            
            RealtimeConnectionSettings b = new RealtimeConnectionSettings();
            b.TimeIdColumn = "timeid";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeIdColumn = "othertimeid";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_TimeWindowSeconds_Test()
        {
            RealtimeConnectionSettings a = new RealtimeConnectionSettings();
            a.TimeWindowSeconds = 7;
            
            RealtimeConnectionSettings b = new RealtimeConnectionSettings();
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }
    }
}
