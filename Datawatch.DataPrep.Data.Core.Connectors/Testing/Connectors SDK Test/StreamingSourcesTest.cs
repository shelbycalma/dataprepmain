﻿using System;
using System.Collections.Generic;
using System.Windows;
using Xunit;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class StreamingSourcesTest
    {
        [Fact]
        public void StreamingSources_Single_Simple()
        {
            StandaloneTable source = new StandaloneTable();
            source.BeginUpdate();
            source.AddColumn(new TextColumn("Name"));
            source.AddColumn(new NumericColumn("Value"));
            source.AddRow(new object[] { "A", 1.0 });
            source.AddRow(new object[] { "B", 2.0 });
            source.EndUpdate();

            source.BeginUpdate();
            source.GetColumn("Value").SetValue(source.GetRow(0), 1.1);
            source.GetColumn("Value").SetValue(source.GetRow(1), 2.1);
            source.EndUpdate();
        }


        private class MockedPlugin : IRealtimeDataPlugin
        {
            public event EventHandler<RealtimeDataPluginEventArgs> Updating;

            public event EventHandler<RealtimeDataPluginEventArgs> Updated;

            public event EventHandler<RealtimeStatusEventArgs> StreamingStatusChanged;

            public void Dispose()
            {
            }

            public ITable GetAllData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters)
            {
                throw new NotImplementedException();
            }

            public ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount)
            {
                throw new NotImplementedException();
            }

            public string[] GetDataFiles(PropertyBag settings)
            {
                throw new NotImplementedException();
            }

            public string Id
            {
                get { return "{DCC4195F-1F40-4e6f-B25E-7DA25C72B92E}"; }
            }

            public void Initialize(bool isHosted, IPluginManager pluginManager, 
                PropertyBag settings, PropertyBag globalSettings)
            {                
            }

            public bool IsLicensed
            {
                get { return true; }
            }

            public bool IsObsolete
            {
                get { throw new NotImplementedException(); }
            }

            public bool IsRealtime(PropertyBag settings)
            {
                throw new NotImplementedException();
            }

            public PropertyBag Settings
            {
                get { throw new NotImplementedException(); }
            }

            public void StartRealtimeUpdates(ITable table, PropertyBag settings)
            {
                throw new NotImplementedException();
            }

            public void StopRealtimeUpdates(ITable table, PropertyBag settings)
            {
                throw new NotImplementedException();
            }
            
            public string Title
            {
                get { throw new NotImplementedException(); }
            }

            public string DataPluginType
            {
                get
                {
                    throw new NotImplementedException();
                }
            }

            public IPluginManager PluginManager { get; private set; }

            public void UpdateFileLocation(PropertyBag settings, string oldPath, string newPath, string workbookPath)
            {
                throw new NotImplementedException();
            }
        }
    }
}
