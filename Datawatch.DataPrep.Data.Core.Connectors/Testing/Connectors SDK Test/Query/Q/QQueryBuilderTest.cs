﻿using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.Query.Q
{
    public class QQueryBuilderTest
    {
        [Fact]
        public void SchemaQuery()
        {
            string expectedQuery = "select  from customer where 1 = 0";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = 
                new SchemaAndTable("", "customer");
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, null);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void BreakdownQuery()
        {
            string expectedQuery = "select 1" +
                " by CustomerAlternateKey,FirstName,LastName from customer";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = new SchemaAndTable("", "customer");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] breakdownColumns = { "CustomerAlternateKey" };
            onDemandParameters.BreakdownColumns = breakdownColumns;
            string[] auxiliaryColumns = { "FirstName", "LastName" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void TextDomainQuery()
        {
            string expectedQuery = "select distinct Gender from customer";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = 
                new SchemaAndTable(null, "customer");
            queryBuilder.Select.AddColumn("Gender");
            queryBuilder.Select.IsDistinct = true;
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void NumericDomainQuery()
        {
            string expectedQuery = "select a1: min(YearlyIncome),"+
                "a2: max(YearlyIncome) from customer";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = 
                new SchemaAndTable(null, "customer");
            queryBuilder.Select.AddAggregate(
                new[] { "YearlyIncome" }, FunctionType.Min, "a1");
            queryBuilder.Select.AddAggregate(
                new[] { "YearlyIncome" }, FunctionType.Max, "a2");
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void AggrigateQuery()
        {
            string expectedQuery = "select " +
                "A1: sum(YearlyIncome) by "+
                "CustomerAlternateKey,FirstName,LastName from customer";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = 
                new SchemaAndTable(null, "customer");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] breakdownColumns = { "CustomerAlternateKey" };
            onDemandParameters.BreakdownColumns = breakdownColumns;
            string[] auxiliaryColumns = { "FirstName", "LastName" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            OnDemandAggregate[] aggregates = new OnDemandAggregate[1];
            aggregates[0] = new OnDemandAggregate();
            aggregates[0].Alias = "A1";
            string[] aggColumns = { "YearlyIncome" };
            aggregates[0].Columns = aggColumns;
            aggregates[0].FunctionType = FunctionType.Sum;
            onDemandParameters.Aggregates = aggregates;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void PredicateQuery()
        {
            string expectedQuery = "select 1 " +
                "by CustomerAlternateKey,FirstName from customer "+
                "where Gender in `$(\"M\")";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = 
                new SchemaAndTable(null, "customer");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] breakdownColumns = { "CustomerAlternateKey" };
            onDemandParameters.BreakdownColumns = breakdownColumns;
            string[] auxiliaryColumns = { "FirstName" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            string[] filters = { "M" };
            Predicate predicate = new Comparison(Operator.In,
                                new ColumnParameter("Gender"),
                                new ListParameter(filters));

            queryBuilder.Where.Predicate = predicate;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }

        [Fact]
        public void PredicateQuery2()
        {
            string expectedQuery = "select 1" +
                " by Supersector,Symbol from stocksstatic" +
                " where (Industry in `$(\"Financials\";\"Oil & Gas\"))"+
                " , (Supersector = `$\"Banks\")";
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.SchemaAndTable = 
                new SchemaAndTable(null, "stocksstatic");
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            string[] auxiliaryColumns = { "Supersector", "Symbol" };
            onDemandParameters.AuxiliaryColumns = auxiliaryColumns;
            string[] filters = { "Financials", "Oil & Gas" };
            Predicate predicate = new Comparison(Operator.In,
                                new ColumnParameter("Industry"),
                                new ListParameter(filters));
            predicate = new AndOperator(predicate,
                            new Comparison(Operator.Equals,
                                new ColumnParameter("Supersector"),
                                new StringParameter("Banks")));
            queryBuilder.Where.Predicate = predicate;
            OnDemandHelper.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            string actualQuery = queryBuilder.ToString(SqlDialectFactory.KxQ);
            Assert.Equal(expectedQuery, actualQuery);
        }
    }
}