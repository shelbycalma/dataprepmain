﻿using System.Security.Cryptography.X509Certificates;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class HierarchizeFunctionNodeTest
    {
        [Fact]
        public void TestHierarchizeExpression()
        {
            var axisNode = new DimensionMemberNode("T");
            var node = new HierarchizeFunctionNode(axisNode);

            Assert.Equal("Hierarchize(T.Members)", node.ToExpression());
        }
    }
}