﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class UnionNodeTest
    {
        public static IEnumerable<object[]> NodeData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                new object[] { Enumerable.Empty<AxisItemNode>(), " Union(\r\n\r\n)" },
                new object[] { new [] {new MemberNode("One")}, " Union(\r\nOne\r\n)" },
                new object[] { new [] {new MemberNode("One"), new MemberNode("Two") }, " Union(\r\nOne,\r\nTwo\r\n)" },
                new object[] { new [] {new MemberNode("One"), new MemberNode("Two"), new MemberNode("Three") }, " Union(\r\nOne,\r\nTwo,\r\nThree\r\n)" },
            };
            }
        }

        [Theory]
        [MemberData("NodeData")]
        public void TestUnionNodeExpression(IEnumerable<AxisItemNode> memberNodes, string expected)
        {
            var unionNode = new UnionNode(memberNodes);
            Assert.Equal(expected, unionNode.ToExpression());   
        }
    }
}