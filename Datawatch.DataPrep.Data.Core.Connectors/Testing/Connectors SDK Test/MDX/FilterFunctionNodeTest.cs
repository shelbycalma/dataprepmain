﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class FilterFunctionNodeTest
    {
        private const string name = "T";
        private static readonly MemberNode memberNode = new MemberNode(name);
        private static readonly HierarchyMemberNode hierarcyMemberNode = new HierarchyMemberNode("H");

        public static IEnumerable<object[]> NodeData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                    new object[]
                    {
                        new[] {new ComparisonMemberNode(hierarcyMemberNode, "One")},
                        BinaryOperator.AndOperator,
                        " Filter(T,(cstr(H.CurrentMember.MemberValue) = \"One\"))"
                    },
                    new object[]
                    {
                        new[]
                        {new ComparisonMemberNode(hierarcyMemberNode, "One"), new ComparisonMemberNode(hierarcyMemberNode, "Two")},
                        BinaryOperator.AndOperator,
                        " Filter(T,(cstr(H.CurrentMember.MemberValue) = \"One\" AND cstr(H.CurrentMember.MemberValue) = \"Two\"))"
                    },
                    new object[]
                    {
                        new[]
                        {
                            new ComparisonMemberNode(hierarcyMemberNode, "One"), new ComparisonMemberNode(hierarcyMemberNode, "Two"),
                            new ComparisonMemberNode(hierarcyMemberNode, "Three")
                        },
                        BinaryOperator.AndOperator,
                        " Filter(T,(cstr(H.CurrentMember.MemberValue) = \"One\" AND cstr(H.CurrentMember.MemberValue) = \"Two\" AND cstr(H.CurrentMember.MemberValue) = \"Three\"))"
                    },
                    new object[]
                    {
                        new[] {new ComparisonMemberNode(hierarcyMemberNode, "One")},
                        BinaryOperator.OrOperator,
                        " Filter(T,(cstr(H.CurrentMember.MemberValue) = \"One\"))"
                    },
                    new object[]
                    {
                        new[]
                        {new ComparisonMemberNode(hierarcyMemberNode, "One"), new ComparisonMemberNode(hierarcyMemberNode, "Two")},
                        BinaryOperator.OrOperator,
                        " Filter(T,(cstr(H.CurrentMember.MemberValue) = \"One\" OR cstr(H.CurrentMember.MemberValue) = \"Two\"))"
                    },
                    new object[]
                    {
                        new[]
                        {
                            new ComparisonMemberNode(hierarcyMemberNode, "One"), new ComparisonMemberNode(hierarcyMemberNode, "Two"),
                            new ComparisonMemberNode(hierarcyMemberNode, "Three")
                        },
                        BinaryOperator.OrOperator,
                        " Filter(T,(cstr(H.CurrentMember.MemberValue) = \"One\" OR cstr(H.CurrentMember.MemberValue) = \"Two\" OR cstr(H.CurrentMember.MemberValue) = \"Three\"))"
                    },
                };
            }
        }

        [Theory]
        [MemberData("NodeData")]
        public void TestFilterExpression(IEnumerable<ExpressionNode> expression, BinaryOperator binaryOperator,
            string exprectedResult)
        {
            var filterFunctionNode = new FilterFunctionNode(memberNode,
                new List<ExpressionNodeCollection> {new ExpressionNodeCollection(expression, binaryOperator)});
            Assert.Equal(exprectedResult, filterFunctionNode.ToExpression());
        }

        [Fact]
        public void TestFilterExpressionCombined()
        {
            IEnumerable<ExpressionNode> expressions = new List<ExpressionNode>
            {
                new ComparisonMemberNode(hierarcyMemberNode, "One"),
                new ComparisonMemberNode(new HierarchyWithMeasureFilterMemberNode("Sum"), "2.0", ComparisonOperator.GreaterOrEqual )
            };
            IEnumerable<ExpressionNode> expressions2 = new List<ExpressionNode>
            {
                new ComparisonMemberNode(hierarcyMemberNode, "One"),
                new ComparisonMemberNode(new HierarchyWithMeasureFilterMemberNode("OtherSum"), "1.0", ComparisonOperator.LessOrEqual )
            };

            var filterFunctionNode = new FilterFunctionNode(hierarcyMemberNode, new List<ExpressionNodeCollection>()
            {
                new ExpressionNodeCollection(expressions, BinaryOperator.OrOperator),
                new ExpressionNodeCollection(expressions2)
            });

            Assert.Equal(" Filter(H.Members,(cstr(H.CurrentMember.MemberValue) = \"One\" OR cstr(Sum) >= \"2.0\")AND (cstr(H.CurrentMember.MemberValue) = \"One\" AND cstr(OtherSum) <= \"1.0\"))", 
                filterFunctionNode.ToExpression());
        }
    }
}