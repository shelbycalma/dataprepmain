﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class HierarchyMemberNodeTest
    {
        private const string name = "T";

        [Fact]
        public void TestHierarchyMemberNodeExpression()
        {
            var hierarchyMemberNode = new HierarchyMemberNode(name);
            Assert.Equal("T.Members", hierarchyMemberNode.ToExpression());
        }
    }
}