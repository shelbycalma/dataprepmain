﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;
using Xunit.Extensions;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class CrossJoinNodeTest
    {
        public static IEnumerable<object[]> NodeData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                new object[] { Enumerable.Empty<AxisItemNode>(), "" },
                new object[] { new [] {new MemberNode("One")}, "One" },
                new object[] { new [] {new MemberNode("One"), new MemberNode("Two") }, "Crossjoin(One, Two)" },
                new object[] { new [] {new MemberNode("One"), new MemberNode("Two"), new MemberNode("Three") }, "Crossjoin(One, Two, Three)" },
            };
            }
        }

        [Theory]
        [MemberData("NodeData")]
        public void TestCrossJoinNodeGeneration(IEnumerable<AxisItemNode> nodes, string expected)
        {
            var node = new CrossJoinNode(nodes);
            Assert.Equal(expected, node.ToExpression());
        }
    }
}
