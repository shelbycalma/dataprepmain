﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class MemberNodeTest
    {
        [Fact]
        public void TestMemberNodeExpression()
        {
            var memberNode = new MemberNode("T");
            Assert.Equal("T", memberNode.ToExpression());
        }
    }
}