﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class HierarchyParentMemberNodeTest
    {
        private const string name = "T";

        [Fact]
        public void TestHierarchyMemberNodeExpression()
        {
            var hierarchyMemberNode = new HierarchyParentMemberNode(name);
            Assert.Equal("T.Members", hierarchyMemberNode.ToExpression());
        }

        [Theory]
        [InlineData(1, "CurrentMember.PARENT.MemberValue")]
        [InlineData(2, "CurrentMember.PARENT.PARENT.MemberValue")]
        [InlineData(3, "CurrentMember.PARENT.PARENT.PARENT.MemberValue")]
        [InlineData(4, "CurrentMember.PARENT.PARENT.PARENT.PARENT.MemberValue")]
        public void TestGetEqualtyNodeExpressionToken(int level, string expected)
        {
            var hierarchyMemberNode = new HierarchyParentMemberNode(name, level);
            Assert.Equal(expected, hierarchyMemberNode.GetEqualtyNodeExpressionToken());
        }

        [Fact]
        public void TestGetEqualtyNodeExpressionTokenFails()
        {
            Assert.Throws<ArgumentException>(() => new HierarchyParentMemberNode(name, 0));
        }
    }
}