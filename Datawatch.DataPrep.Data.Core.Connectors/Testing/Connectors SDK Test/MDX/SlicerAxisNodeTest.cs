﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class SlicerAxisNodeTest
    {
        private const string nodeName = "T";

        [Theory]
        [InlineData(false, "T")]
        [InlineData(true, "T")]
        public void TestSlicerAxis(bool nonEmpty, string expected)
        {
            var slicer = new SlicerAxisNode(nonEmpty, new MemberNode(nodeName));
            Assert.Equal(expected, slicer.ToExpression());
        }

    }
}