﻿using System.Collections.Generic;
using System.Linq.Expressions;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class AxisNodeTest
    {
        private const string fullName = "T";

        [Theory]
        [InlineData(0, false, "T ON COLUMNS")]
        [InlineData(1, false, "T ON ROWS")]
        [InlineData(2, false, "T ON PAGES")]
        [InlineData(3, false, "T ON SECTIONS")]
        [InlineData(4, false, "T ON CHAPTERS")]
        [InlineData(5, false, "T ON WHERE")]
        [InlineData(6, false, "T ON Axis(6)")]
        [InlineData(7, false, "T ON Axis(7)")]
        [InlineData(0, true, "T ON COLUMNS")]
        [InlineData(1, true, "NON EMPTY (T) ON ROWS")]
        public void TestAxisNodeGeneration(int axisIndex, bool nonEmpty, string expected)
        {
            var axisNode = new AxisNode(axisIndex, nonEmpty, new MemberNode(fullName));
            Assert.Equal(expected, axisNode.ToExpression());
        }

        [Theory]
        [InlineData(0, new[] {"T1"}, "T DIMENSION PROPERTIES T1 ON COLUMNS")]
        [InlineData(1, new[] { "T1", "T2" }, "T DIMENSION PROPERTIES T1,T2 ON ROWS")]
        public void TestAxisNodeGenerationWithDimensionProperties(int axisIndex, IEnumerable<string> dimensionProperties, string expected)
        {
            var axisNode = new AxisNode(axisIndex, false, new MemberNode(fullName));
            foreach (var dimensionProperty in dimensionProperties)
            {
                axisNode.DimensionProperties.Add(dimensionProperty);
            }
            
            Assert.Equal(expected, axisNode.ToExpression());
        }

        public static IEnumerable<object[]> ComparationAxisNodesData
        {
            get
            {
                return new[]
                {
                    new object[] {new AxisNode(0, false, null), new AxisNode(1, false, null), -1 },
                    new object[] {new AxisNode(1, false, null), new AxisNode(0, false, null), 1 },
                    new object[] {new AxisNode(1, false, null), new AxisNode(1, false, null), 0 },
                    new object[] {new AxisNode(1, false, null), null, 1 },
                };
            }
        }
        [Theory]
        [MemberData("ComparationAxisNodesData")]
        public void TestCompareTo(AxisNode first, AxisNode other, int expected)
        {
            Assert.Equal(expected, first.CompareTo(other));
        }
    }
}