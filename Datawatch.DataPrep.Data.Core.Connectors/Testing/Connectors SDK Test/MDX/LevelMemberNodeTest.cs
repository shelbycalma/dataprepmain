﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class LevelMemberNodeTest
    {
        private const string name = "T";

        [Fact]
        public void TestLevelMemberNodeExpression()
        {
            var node = new LevelMemberNode(name);
            Assert.Equal("T.Members", node.ToExpression());
        }
    }
}