﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class TupleNodeTest
    {
        public static IEnumerable<object[]> NodeData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                new object[] { Enumerable.Empty<AxisItemNode>(), "(\r\n\r\n)" },
                new object[] { new [] {new MemberNode("One")}, "One" },
                new object[] { new [] {new MemberNode("One"), new MemberNode("Two") }, "(\r\nOne,\r\nTwo\r\n)" },
                new object[] { new [] {new MemberNode("One"), new MemberNode("Two"), new MemberNode("Three") }, "(\r\nOne,\r\nTwo,\r\nThree\r\n)" },
            };
            }
        }

        [Theory]
        [MemberData("NodeData")]
        public void TupleNodeExpressionTest(IEnumerable<AxisItemNode> items, string expectedResult)
        {
            var node = new TupleNode(items);
            Assert.Equal(expectedResult, node.ToExpression());
        }

    }
}