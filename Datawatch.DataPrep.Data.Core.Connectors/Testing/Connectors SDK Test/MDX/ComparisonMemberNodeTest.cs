﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class ComparisonMemberNodeTest
    {
        private const string name = "T";
        private const string parameter = "p";

        [Fact]
        public void TestEqualMemberNameNotSupported()
        {
            var equalMemberNode = new ComparisonMemberNode(new MemberNode(name), parameter, ComparisonOperator.Equal);

            Assert.Throws<NotSupportedException>(() => equalMemberNode.ToExpression());
        }

        [Fact]
        public void TestEqualMemberNameSupported()
        {
            var memberNode = new HierarchyMemberNode(name);
            var equalMemberNode = new ComparisonMemberNode(memberNode, parameter, ComparisonOperator.Equal);
            Assert.Equal("cstr(T.CurrentMember.MemberValue) = \"p\"", equalMemberNode.ToExpression());
        }

        [Fact]
        public void TestGreaterOrEqual()
        {
            var memberNode = new HierarchyWithMeasureFilterMemberNode(name);
            string numberParameter = "2";
            var comparisonMemberNode = new ComparisonMemberNode(memberNode, numberParameter, ComparisonOperator.GreaterOrEqual, false);
            Assert.Equal("T >= 2", comparisonMemberNode.ToExpression());
        }

        [Fact]
        public void TestLessOrEqual()
        {
            var memberNode = new HierarchyWithMeasureFilterMemberNode(name);
            string numerParameter = "1";
            var comparisonMemberNode = new ComparisonMemberNode(memberNode, numerParameter, ComparisonOperator.LessOrEqual, false);
            Assert.Equal("T <= 1", comparisonMemberNode.ToExpression());
        }

        [Fact]
        public void TestEqualMemberNotEqual()
        {
            var memberNode = new HierarchyMemberNode(name);
            var equalMemberNode = new ComparisonMemberNode(memberNode, parameter, ComparisonOperator.NotEqual);
            Assert.Equal("cstr(T.CurrentMember.MemberValue) <> \"p\"", equalMemberNode.ToExpression());
        }
    }
}