﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class AggregateItemNodeTest
    {
        private const string _alias = "t";
        private readonly string[] _columns = new[]
        {
            "Column1",
            "Column2"
        };

        [Theory]
        [InlineData(FunctionType.SumOfProducts, "MEMBER t AS Column1*Column2")]
        [InlineData(FunctionType.NegativeSum, "MEMBER t AS IIF(Column1 < 0,Column1,0)")]
        [InlineData(FunctionType.PositiveSum, "MEMBER t AS IIF(Column1 > 0,Column1,0)")]
        [InlineData(FunctionType.Samples, "MEMBER t AS IIF(ISEMPTY(Column1),0,1)")]
        [InlineData(FunctionType.ConditionalSum, "MEMBER t AS IIF(Column1<>0,Column1,0)")]
        [InlineData(FunctionType.SumOfQuotients, "MEMBER t AS IIF(Column1<>0,1/Column1,0)")]
        [InlineData(FunctionType.SumOfWeightedQuotients, "MEMBER t AS IIF(Column2<>0,Column1/Column2,0)")]
        [InlineData(FunctionType.NonZeroSamples, "MEMBER t AS IIF(Column1<>0,1,0)")]
        
        [InlineData(FunctionType.External, "MEMBER t AS Column1")]
        [InlineData(FunctionType.Count, "MEMBER t AS COUNT(Column1)")]
        [InlineData(FunctionType.Sum, "MEMBER t AS Column1")]
        [InlineData(FunctionType.Min, "MEMBER t AS MIN(Column1)")]
        [InlineData(FunctionType.Max, "MEMBER t AS MAX(Column1)")]
        [InlineData(FunctionType.AbsSum, "MEMBER t AS ABS(Column1)")]
        [InlineData(FunctionType.First, "MEMBER t AS HEAD(Column1, 1)")]
        [InlineData(FunctionType.Last, "MEMBER t AS TAIL(Column1, 1)")]
        [InlineData(FunctionType.SumOfSquares, "MEMBER t AS Column1*Column1")]
        public void TestToString(FunctionType functionType, string exprectedResult)
        {
            var aggregateItemNode = new AggregateItemNode(functionType, _alias, _columns);
            Assert.Equal(exprectedResult, aggregateItemNode.ToExpression());
        }
    }
}
