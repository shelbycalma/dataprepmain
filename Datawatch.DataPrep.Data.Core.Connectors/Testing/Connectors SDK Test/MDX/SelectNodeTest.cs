﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class SelectNodeTest
    {
        private const string name = "T";
        private const string cubeName = "F";

        public static IEnumerable<object[]> NodeData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                    new object[]
                    {
                        Enumerable.Empty<AxisNode>(), new SlicerAxisNode(false, new MemberNode(name)),
                        new CubeNode(cubeName), "SELECT  \r\nFROM [F] \r\nWHERE T\r\n"
                    },
                    new object[]
                    {
                        new[] {new AxisNode(0, false, new MemberNode("One"))}, new SlicerAxisNode(false, new MemberNode(name)),
                        new CubeNode(cubeName), "SELECT One ON COLUMNS \r\nFROM [F] \r\nWHERE T\r\n"
                    },
                    new object[]
                    {
                        new[] { new AxisNode(0, false, new MemberNode("One")), new AxisNode(1, false, new MemberNode("Two"))},
                        new SlicerAxisNode(false, new MemberNode(name)), new CubeNode(cubeName), "SELECT One ON COLUMNS,\r\nTwo ON ROWS \r\nFROM [F] \r\nWHERE T\r\n"
                    }
                };
            }
        }

        [Theory]
        [MemberData("NodeData")]
        public void TestSelectNodeExpression(IEnumerable<AxisNode> axises, 
            SlicerAxisNode slicer, FromNode from, string expectedResult)
        {
            var selectNode = new SelectNode(axises, slicer, from);
            Assert.Equal(expectedResult, selectNode.ToExpression());
        }
    }
}