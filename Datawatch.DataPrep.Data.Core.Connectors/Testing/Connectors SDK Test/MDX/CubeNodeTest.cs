﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class CubeNodeTest
    {
        [Fact]
        public void TestCubeNode()
        {
            var cubeNode = new CubeNode("TestName");
            Assert.Equal("[TestName]", cubeNode.ToExpression());
        } 
    }
}