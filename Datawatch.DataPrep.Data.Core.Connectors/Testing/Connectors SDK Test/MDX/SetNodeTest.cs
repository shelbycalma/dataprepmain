﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class SetNodeTest
    {
        public static IEnumerable<object[]> NodeData
        {
            get
            {
                // Or this could read from a file. :)
                return new[]
                {
                    new object[]
                    {
                        Enumerable.Empty<TupleNode>(), "{\r\n\r\n}"
                    },
                    new object[]
                    {
                        new[] {new TupleNode(new[] {new MemberNode("One")})},
                        "{\r\nOne\r\n}"
                    },
                    new object[]
                    {
                        new[] {new TupleNode(new[] {new MemberNode("One"), new MemberNode("Two")})},
                        "{\r\n(\r\nOne,\r\nTwo\r\n)\r\n}"
                    },
                    new object[]
                    {
                        new[]
                        {
                            new TupleNode(new[] {new MemberNode("One"), new MemberNode("Two")}),
                            new TupleNode(new[] {new MemberNode("Three")})
                        },
                        "{\r\n(\r\nOne,\r\nTwo\r\n),\r\nThree\r\n}"
                    },
                    new object[]
                    {
                        new[]
                        {
                            new TupleNode(new[] {new MemberNode("One"), new MemberNode("Two")}),
                            new TupleNode(new[] {new MemberNode("Three"), new MemberNode("Four")})
                        },
                        "{\r\n(\r\nOne,\r\nTwo\r\n),\r\n(\r\nThree,\r\nFour\r\n)\r\n}"
                    },
                };
            }
        }

        [Theory]
        [MemberData("NodeData")]
        public void TestSetNodeExpression(IEnumerable<TupleNode> tupleNodes, string exprectedResult)
        {
            var setNode = new SetNode(tupleNodes);
            Assert.Equal(exprectedResult, setNode.ToExpression());    
        }
    }
}