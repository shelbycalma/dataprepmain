﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors.MDX
{
    public class DimensionMemberNodeTest
    {
        private const string name = "Node";

        [Fact]
        public void TestDimensionMemberNode()
        {
            var node = new DimensionMemberNode(name);
            Assert.Equal("Node.Members", node.ToExpression());
        }
    }
}