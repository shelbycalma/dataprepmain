﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class ColumnGenerationTest
    {

        [Fact]
        public void TextColumnGeneration()
        {
            string[] lines = new[]
            {
                "Empty,NotEmpty,Numeric,Date,Mixed",
                ",A,1,7/4/2016,1.0",
                ",B,2,8/4/2016,2.0",
                ",C,3,9/4/2016,B",
            };

            TextFileParserSettings settings = 
                new TextFileParserSettings(new PropertyBag());
            TextFileParser textFileParser = new TextFileParser(settings);

            ColumnGenerator columnGenerator = 
                new ColumnGenerator(settings, 
                    new NumericDataHelper(new PropertyBag()));

            foreach (string line in lines)
            {
                columnGenerator.GenerateAndObtainValues(line);
            }

            Assert.Equal(settings.ColumnCount, 5);
            TextFileColumnDefinition cd = 
                (TextFileColumnDefinition) settings.GetColumn("Empty");
            Assert.NotNull(cd);
            Assert.Equal(cd.Type, ColumnType.Text);

            cd = (TextFileColumnDefinition) settings.GetColumn("NotEmpty");
            Assert.NotNull(cd);
            Assert.Equal(cd.Type, ColumnType.Text);

            cd = (TextFileColumnDefinition) settings.GetColumn("Numeric");
            Assert.NotNull(cd);
            Assert.Equal(cd.Type, ColumnType.Numeric);

            cd = (TextFileColumnDefinition) settings.GetColumn("Date");
            Assert.NotNull(cd);
            Assert.Equal(cd.Type, ColumnType.Time);

            cd = (TextFileColumnDefinition)settings.GetColumn("Mixed");
            Assert.NotNull(cd);
            Assert.Equal(cd.Type, ColumnType.Text);
        }
    }
}