﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class XPathParserTest
    {
        private const string GeneralColumn = "GeneralColumn";
        private const string DefaultCompanyColumn = "DefaultCompany";
        private const string FirstBigCompanyColumn = "FirstBigCompany";
        private const string FirstSmallCompanyColumn = "FirstSmallCompany";
        private const string DefaultMarketcapColumn = "DefaultMarketcap";
        private const string TotalMarketcapColumn = "TotalMarketcap";
        private const string FirstSmallMarketcapColumn = "FirstSmallMarketcap";
        private const string TotalColumn = "TotalCost";
        private const string NetColumn = "NetCost";
        private const string TaxColumn = "TaxCost";
        private const string FunctionColumn = "Function";

        private const string Up = "up";
        private const string Arla = "Arla";
        private const string XmlCharacterDataValue = "public int inc(int i) {return ++i;}";
        private const string Ericsson = "Ericsson";
        private const string Fingerprint = "Fingerprint";
        private const string Boliden = "Boliden";

        private const string FoodXml =
            @"<products>" +
                "<price company= \"" + Arla + "\" net=\"3.26\" tax=\"1.34\" movement=\"" + Up + "\"/>" +
                "<price company= \"Axa\" net=\"5.5\" tax=\"3.5\" movement=\"down\"/>" +
             "</products>";
        private const string ConcatXml =
            @"<products>" +
                "<price company= \"Arla\" ceo=\"Karl Andersson\" net=\"3.26\" tax=\"1.34\" movement=\"up\"/>" +
             "</products>";
        private const string CdataXml =
           @"<code>" +
               "<func0>" +
                   "<![CDATA[ public void notThisMethod0() {} ]]>" +
               "</func0>" +
               "<func1>" +
                   "<![CDATA[" + XmlCharacterDataValue + "]]>" +
               "</func1>" +
               "<func2>" +
                   "<![CDATA[ public void notThisMethod1() {} ]]>" +
               "</func2>" +
            "</code>";
        private const string CompanySmallXml = @"<industry><company>" + Ericsson +
            "</company></industry>";
        private const string NamespaceXml =
            @"<industry xmlns:big=""http:\/\/bigcompanies-com"" xmlns:small=""http:\/\/smallcompanies-com"">" +
                "<big:company big:name=\"" + Ericsson + "\" big:mcap=\"200\" />" +
                "<small:company small:name=\"" + Fingerprint + "\" small:mcap=\"3.7\" />" +
                "<big:company name=\"ABB\" big:mcap=\"80\" />" +
                "<small:company small:name=\"Bore\" small:mcap=\"0.5\" />" +
             "</industry>";
        private const string DefaultNamespaceXml =
            @"<industry " +
                "xmlns:big=\"http://bigcompanies.com\" xmlns=\"http://defaultnamespace.com\" xmlns:small=\"http://smallcompanies.com\">" +
                "<big:company big:name=\"" + Ericsson + "\" big:mcap=\"200\" />" +
                "<company name=\"" + Boliden + "\" mcap=\"300\" />" +
                "<small:company small:name=\"Fingerprint\" small:mcap=\"5\" />" +
                "<big:company name=\"ABB\" big:mcap=\"80\" />" +
                "<small:company small:name=\"Bore\" small:mcap=\"0.5\" />" +
             "</industry>";
        private const string BadFormatXml =
            @"<products>" +
                "<price company bad = \"Arla\" ceo=\"Karl Andersson\" net=\"3.26\" tax=\"1.34\" movement=\"up\"/>" +
             "</products>";


        private const double PrecisionDelta = 0.00001;
        private const int Precision = 5;

        private XPathParser parser;

        [Fact]
        public void XmlAttribute()
        {
            List<XPathColumnDefinition> xPathDefinitions = new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(GeneralColumn, "products/price/@tax", "Numeric"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(FoodXml);

            Assert.Equal(1.34, XmlConvert.ToDouble((string)dict[GeneralColumn]), Precision);
        }

        [Fact]
        public void XmlAttribute_Sum_Basic()
        {
            List<XPathColumnDefinition> xPathDefinitions = new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(GeneralColumn, "sum(products/price/@tax)", "Numeric"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(FoodXml);

            Assert.Equal(4.84, (Double)dict[GeneralColumn], Precision);
        }

        [Fact]
        public void XmlAttribute_Sum_Pipe()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(GeneralColumn,
                "sum(products/price/@net|products/price/@tax)", "Numeric"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(FoodXml);

            Assert.Equal(13.6, (Double)dict[GeneralColumn], Precision);
        }

        [Fact]
        public void XmlAttribute_Concat()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(GeneralColumn,
                "concat(products/price/@ceo, string(\" at \"), products/price/@company)",
                "Text"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(ConcatXml);

            Assert.Equal("Karl Andersson at Arla", dict[GeneralColumn]);
        }

        [Fact]
        public void XmlAttribute_Sum_SeveralElements()
        {
            List<XPathColumnDefinition> xPathDefinitions = new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(TotalColumn,
                "sum(products/price/@net|products/price/@tax)", "Numeric"));
            xPathDefinitions.Add(CreateXPathDefinition(NetColumn,
                "sum(products/price/@net)", "Numeric"));
            xPathDefinitions.Add(CreateXPathDefinition(TaxColumn,
                "sum(products/price/@tax)", "Numeric"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(FoodXml);

            Assert.Equal(13.6, (Double)dict[TotalColumn], Precision);
            Assert.Equal(8.76, (Double)dict[NetColumn], Precision);
            Assert.Equal(4.84, (Double)dict[TaxColumn], Precision);
        }

        [Fact]
        public void XmlCharacterData()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(GeneralColumn,
                "code/func1/text()", "Text"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(CdataXml);

            Assert.Equal(XmlCharacterDataValue, dict[GeneralColumn]);
        }

        [Fact]
        public void XmlElement()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(FunctionColumn,
                "industry/company", "Text"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(CompanySmallXml);

            Assert.Equal(Ericsson, dict[FunctionColumn]);
        }

        [Fact]
        public void Namespace()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(FirstBigCompanyColumn,
                "industry/big:company/@big:name", "Text"));
            xPathDefinitions.Add(CreateXPathDefinition(FirstSmallCompanyColumn,
                "industry/small:company/@small:name", "Text"));
            xPathDefinitions.Add(CreateXPathDefinition(FirstSmallMarketcapColumn,
                "industry/small:company/@small:mcap", "Numeric"));
            xPathDefinitions.Add(CreateXPathDefinition(TotalMarketcapColumn,
                "sum(industry/big:company/@big:mcap|industry/small:company/@small:mcap)",
                "Numeric"));

            CreateParser(xPathDefinitions);

            // Validate incomming xmlMessage. :)
            Dictionary<string, object> dict = parser.Parse(NamespaceXml);

            Assert.Equal(Ericsson, dict[FirstBigCompanyColumn]);
            Assert.Equal(Fingerprint, dict[FirstSmallCompanyColumn]);
            Assert.Equal(3.7, XmlConvert.ToDouble(
                (string)dict[FirstSmallMarketcapColumn]), Precision);
            Assert.Equal(284.2, (Double)dict[TotalMarketcapColumn], Precision);
        }

        [Fact]
        public void DefaultNamespace()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(FirstBigCompanyColumn,
                "default:industry/big:company/@big:name", "Text"));
            xPathDefinitions.Add(CreateXPathDefinition(DefaultCompanyColumn,
                "default:industry/default:company/@name", "Text"));
            xPathDefinitions.Add(CreateXPathDefinition(DefaultMarketcapColumn,
                "sum(default:industry/default:company/@mcap)", "Numeric"));
            xPathDefinitions.Add(CreateXPathDefinition(TotalMarketcapColumn,
                "sum(default:industry/big:company/@big:mcap|default:industry/default:company/@mcap)",
                "Numeric"));

            CreateParser(xPathDefinitions);

            Dictionary<string, object> dict = parser.Parse(DefaultNamespaceXml);

            Assert.Equal(Ericsson, dict[FirstBigCompanyColumn]);
            Assert.Equal(Boliden, dict[DefaultCompanyColumn]);
            Assert.Equal(300, (Double)dict[DefaultMarketcapColumn],
                Precision);
            Assert.Equal(580, (Double)dict[TotalMarketcapColumn],
                Precision);
        }

        [Fact]
        public void BadAttributeInXPathExpression()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(FirstBigCompanyColumn,
                "default:industry/big:company/@big:name1", "Text"));

            CreateParser(xPathDefinitions);

            Dictionary<string, object> dict = parser.Parse(DefaultNamespaceXml);
            Assert.Equal(0, dict.Count);
        }

        [Fact]
        public void BadNamespaceInXPathExpression()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();

            xPathDefinitions.Add(CreateXPathDefinition(FirstBigCompanyColumn,
                "default:industry/big:company/@big1:name", "Text"));

            CreateParser(xPathDefinitions);

            var exc = Assert.Throws<XPathException>(() =>
            {
                Dictionary<string, object> dict = parser.Parse(DefaultNamespaceXml);
                Assert.Equal(Ericsson, dict[FirstBigCompanyColumn]);
            });
        }

        [Fact]
        public void BadFormattedXml()
        {
            List<XPathColumnDefinition> xPathDefinitions =
                new List<XPathColumnDefinition>();
            xPathDefinitions.Add(CreateXPathDefinition(GeneralColumn,
                            "products/price/@company", "Text"));

            CreateParser(xPathDefinitions);
            var exc = Assert.Throws<XmlException>(() =>
            {
                Dictionary<string, object> dict = parser.Parse(BadFormatXml);
            });
        }

        /*************************        HELP METHODS        ************************/

        private static XPathColumnDefinition CreateXPathDefinition(
            string generalColumn, string xPath, string type)
        {
            XPathColumnDefinition xpd = new XPathColumnDefinition(new PropertyBag());
            xpd.Name = generalColumn;
            xpd.XPath = xPath;
            xpd.Type = (ColumnType)Enum.Parse(typeof(ColumnType), type);
            return xpd;
        }

        private void CreateParser(IEnumerable<XPathColumnDefinition> xPathColumns)
        {
            XPathParserSettings settings =
                new XPathParserSettings(new PropertyBag());
            foreach (XPathColumnDefinition xPathColumn in xPathColumns)
            {
                settings.AddColumnDefinition(xPathColumn);
            }
            parser = new XPathParser(settings);
        }
    }
}
