﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Newtonsoft.Json.Linq;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class JsonParserTest
    {
        private static List<ParameterValue> parameters;
        private JsonParserSettings settings;

        private const string NameColumn = "NameColumn";
        private const string AgeColumn = "AgeColumn";
        private const string HomeColumn = "HomeColumn";
        private const string MobileColumn = "MobileColumn";

        private const string Sweden = "Sweden";
        private const string Stockholm = "Stockholm";
        private const string Name = "Name";
        private const string Age = "Age";
        private const string Employees = "Employees";
        private const string Phone = "Phone";
        private const string Home = "Home";
        private const string Mobile = "Mobile";
        private const string Number = "Number";
        private const string Ename = "Ename";

        private const string Kalle = "Kalle";
        private const string KalleAge = "55.1";

        private const string Eva = "Eva";
        private const string EvaAge = "33.2";
        private const string EvaHome = "06-666";
        private const string EvaMobile = "070-666";

        private const string Pelle = "Pelle";

        private static readonly string NameSimpleJSON = 
             "{"+
                "\""+Sweden+"\": {"+
                    "\""+Stockholm+"\": ["+
                        "{\"" + Name + "\": \"" + Kalle + "\", \"" + Age + "\":\""+KalleAge+"\"}," +
                        "{\"" + Name + "\": \"" + Eva + "\",   \"" + Age + "\":\""+EvaAge+"\"}" +
                    "]"+
                 "}"+
            "}";

        private static readonly string NameAdvanceJSON = 
             "{"+
                "\""+Sweden+"\": ["+
                    "{\""+"Gothenburg"+"\": null},"+
                    "{\""+Stockholm+"\": "+
                        "["+
                            "{\"Name\": \""+Kalle+"\",   \"Age\":\""+KalleAge+"\"},"+
                            "null,"+
                            "{\"Name\": \""+Eva+"\",     \"Age\":\""+EvaAge+"\"},"+
                            "{\"Name\": \""+"Pelle"+"\", \"Age\":\""+"77.0"+"\"}"+
                        "]"+
                    "}"+
                "]"+
             "}";

        private static readonly string EmployeesJSON = 
            "{"+
                Employees+" :"+ 
                "["+
                    "{Name : \""+Kalle+"\", "+Phone+" : [ {type: \""+Home+"\", "+Number+" : \"05-555\"},       {type: \""+Mobile+"\", "+Number+" : \"070-555\"} ] },"+
                    "{Name : \""+Eva+"\",   "+Phone+" : [ {type: \""+Home+"\", "+Number+" : \""+EvaHome+"\"}, {type: \""+Mobile+"\", "+Number+" : \""+EvaMobile+"\"} ] },"+
                    "{Name : \""+Pelle+"\",     "+Phone+" : [ {type: \""+Home+"\", "+Number+" : \"07-777\"},       {type: \""+Mobile+"\", "+Number+" : \"070-777\"} ] }"+
                "]"+
            "}";

        private static readonly string ArrayDirectItemsJSON =
            "[" +
              "{ \"" + Name + "\":\"" + Kalle + "\", \"" + Age + "\": \"" + KalleAge + "\" }," +
              "{ \"" + Name + "\":\"" + Eva + "\",   \"" + Age + "\": \"" + EvaAge + "\" }" +
            "]";

        private static readonly string NestedArraysJSON =
                "{" +
                Employees + " :" +
                "[" +
                "[" +
                "{" + Name + " : \"Kalle\", \"" + Ename + "\" : \"Oskarsson\", " + Phone +
                " : [[ {type: \"Home\", Number : \"05-555\"}, {type: \"Mobile\", Number : \"070-555\"} ]] }," +
                            "{"+Name+" : \"Eva\", \""+Ename+"\" : \"Johansson\", "+Phone+" : [[ {type: \"Home\", Number : \""+EvaHome+"\"}, {type: \"Mobile\", Number : \""+EvaMobile+"\"} ]] }"+
                        "]"+
                    "]"+
            "}";
    
        public JsonParserTest() {
            //table = new StandaloneTable();
            settings = new JsonParserSettings(new PropertyBag());

            parameters = new List<ParameterValue>();
        }
    
        [Fact]
        public void RecordPath_noPath() {
            string recordPath = "";

            JsonParser inhouseParser = new JsonParser(settings);
            JToken jRows = inhouseParser.FindRecords(ArrayDirectItemsJSON, recordPath);

            Assert.NotNull(jRows);
        }
    
        [Fact]
        public void RecordPath_simpleCorrectPath() {
            string recordPath = Sweden+"."+Stockholm;

            JsonParser inhouseParser = new JsonParser(settings);
            JToken jRows = inhouseParser.FindRecords(NameSimpleJSON, recordPath);

            Assert.NotNull(jRows);
        }

        [Fact]
        public void RecordPath_simpleWrongPath()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = Sweden + "." + "wrongPathElement";

            JToken jRows =
                inhouseParser.FindRecords(NameSimpleJSON, recordPath);

            Assert.Null(jRows);
        }
        
        [Fact]
        public void RecordPath_arraySimpleCorrectPath()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = "";

            JToken jRows =
                inhouseParser.FindRecords(ArrayDirectItemsJSON, recordPath);

            Assert.NotNull(jRows);
        }

        [Fact]

        public void RecordPath_arraySimpleNegativeIndexPath()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = "[-1]";
            var exc = Assert.Throws<ArgumentOutOfRangeException>(() =>
            {
                JToken jRows =
                    inhouseParser.FindRecords(ArrayDirectItemsJSON, recordPath);

                Assert.Null(jRows);
            });
        }
        
        [Fact]
        public void RecordPath_arraySimpleToLargeIndexPath()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = "[2]";

            JToken jRows =
                inhouseParser.FindRecords(ArrayDirectItemsJSON, recordPath);

            Assert.Null(jRows);
        }
        
        [Fact]
        public void RecordPathSimple_indexWhenNoArray()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = "[0]." + Sweden + "." + Stockholm;

            JToken jRows =
                inhouseParser.FindRecords(NameSimpleJSON, recordPath);
            Assert.Null(jRows);
        }

        [Fact]
        public void RecordPathSimple_correctArrayIndexMiddle()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = Employees + "[0][0]." + Phone;

            JToken jRows =
                inhouseParser.FindRecords(NestedArraysJSON, recordPath);

            Assert.NotNull(jRows);
        }

        [Fact]
        public void RecordPathSimple_negativeIndexMiddle()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = Sweden + "." + Stockholm + "[-1]." + Name;
            Assert.Throws<ArgumentOutOfRangeException>(() =>
                {
                    JToken jRows =
                        inhouseParser.FindRecords(NameSimpleJSON, recordPath);
                    Assert.Null(jRows);
                });
        }

        [Fact]
        public void RecordPathSimple_toLargeIndexMiddle()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = Sweden + "." + Stockholm + "[100]." + Name;

            JToken jRows =
                inhouseParser.FindRecords(NameSimpleJSON, recordPath);

            Assert.Null(jRows);
        }

        [Fact]
        public void RecordPathSimple_nestedArrays()
        {
            JsonParser inhouseParser = new JsonParser(settings);
            string recordPath = Employees + "[0]";

            JToken jRows =
                inhouseParser.FindRecords(NestedArraysJSON, recordPath);

            Assert.NotNull(jRows);
        }

        [Fact]
        public void SimpleParsing()
        {
            AddColumn(NameColumn, Name, ColumnType.Text);
            AddColumn(AgeColumn, Age, ColumnType.Numeric);
            JsonParser inhouseParser = new JsonParser(settings);

            string recordPath = Sweden + "." + Stockholm;
            JToken jRows = inhouseParser.FindRecords(NameSimpleJSON, recordPath);

            Assert.NotNull(jRows);

            List<Dictionary<string, object>> list =
                new List<Dictionary<string, object>>();
            foreach (JObject jobject in jRows)
            {
                list.Add(inhouseParser.Parse(jobject));
            }

            Assert.Equal(Kalle, list[0][NameColumn]);
            Assert.Equal(KalleAge, list[0][AgeColumn]);

            Assert.Equal(Eva, list[1][NameColumn]);
            Assert.Equal(EvaAge, list[1][AgeColumn]);
        }

        [Fact]
        public void AdvanceParsingRecordPath()
        {
            AddColumn(NameColumn, Name, ColumnType.Text);
            AddColumn(AgeColumn, Age, ColumnType.Numeric);

            JsonParser inhouseParser = new JsonParser(settings);

            string recordPath = Sweden+"[1]."+Stockholm;
            JToken jRows =
                inhouseParser.FindRecords(NameAdvanceJSON, recordPath);

            Assert.NotNull(jRows);

            List<Dictionary<string, object>> list =
                new List<Dictionary<string, object>>();
            foreach (JToken jToken in jRows)
            {
                if (jToken.HasValues)
                {
                    list.Add(inhouseParser.Parse((JObject) jToken));
                }
            }

            Assert.Equal(Kalle, list[0][NameColumn]);
            Assert.Equal(KalleAge, list[0][AgeColumn]);

            Assert.Equal(Eva, list[1][NameColumn]);
            Assert.Equal(EvaAge, list[1][AgeColumn]);
        }

        [Fact]
        public void AdvanceParsingColumnDepth()
        {
            AddColumn(NameColumn, Name, ColumnType.Text);
            AddColumn(HomeColumn, Phone+"[0]."+Number, ColumnType.Text);
            AddColumn(MobileColumn, Phone+"[1]."+Number, ColumnType.Text);

            JsonParser inhouseParser = new JsonParser(settings);

            string recordPath = Employees;
            JToken jRows =
                inhouseParser.FindRecords(EmployeesJSON, recordPath);

            Assert.NotNull(jRows);

            List<Dictionary<string, object>> list =
                new List<Dictionary<string, object>>();
            foreach (JToken jToken in jRows)
            {
                if (jToken.HasValues)
                {
                    list.Add(inhouseParser.Parse((JObject) jToken));
                }
            }

            Assert.Equal(Kalle, list[0][NameColumn]);
            Assert.Equal(Eva, list[1][NameColumn]);
            Assert.Equal(Pelle, list[2][NameColumn]);

            Assert.Equal(EvaHome, list[1][HomeColumn]);
            Assert.Equal(EvaMobile, list[1][MobileColumn]);
        }
        
        [Fact]
        public void AdvanceParsingNestledArrays()
        {
            AddColumn(NameColumn, Name, ColumnType.Text);
            AddColumn(HomeColumn, Phone+"[0][0]."+Number, ColumnType.Text);
            AddColumn(MobileColumn, Phone+"[0][1]."+Number, ColumnType.Text);

            JsonParser inhouseParser = new JsonParser(settings);

            string recordPath = Employees+"[0]";
            JToken jRows =
                inhouseParser.FindRecords(NestedArraysJSON, recordPath);

            Assert.NotNull(jRows);

            List<Dictionary<string, object>> list =
                new List<Dictionary<string, object>>();
            foreach (JToken jToken in jRows)
            {
                if (jToken.HasValues)
                {
                    list.Add(inhouseParser.Parse((JObject) jToken));
                }
            }

            Assert.Equal(Kalle, list[0][NameColumn]);
            Assert.Equal(Eva, list[1][NameColumn]);

            Assert.Equal(EvaHome, list[1][HomeColumn]);
            Assert.Equal(EvaMobile, list[1][MobileColumn]);
        }

        [Fact]
        public void ParseArrayDirect()
        {
            AddColumn(NameColumn, Name, ColumnType.Text);
            AddColumn(AgeColumn, Age, ColumnType.Numeric);

            JsonParser inhouseParser = new JsonParser(settings);

            string recordPath = "";
            JToken jRows =
                inhouseParser.FindRecords(ArrayDirectItemsJSON, recordPath);

            Assert.NotNull(jRows);

            List<Dictionary<string, object>> list =
                new List<Dictionary<string, object>>();
            foreach (JToken jToken in jRows)
            {
                if (jToken.HasValues)
                {
                    list.Add(inhouseParser.Parse((JObject) jToken));
                }
            }

            Assert.Equal(Kalle, list[0][NameColumn]);
            Assert.Equal(Eva, list[1][NameColumn]);

            Assert.Equal(KalleAge, list[0][AgeColumn]);
            Assert.Equal(EvaAge, list[1][AgeColumn]);
        }

        readonly long unixEpochTicks = 
            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;

        [Fact]
        public void UtcAspNetDateTime() 
        {
            AddColumn("LastUpdated", "LastUpdated", ColumnType.Time);
            const string dateTimeData = 
                "[{\"LastUpdated\":\"\\/Date(1408469674106)\\/\"}]";
            JsonParser inhouseParser = new JsonParser(settings);
            const string recordPath = "";
            JToken jRows = 
                    inhouseParser.FindRecords(dateTimeData, recordPath);
            Assert.NotNull(jRows);
            Dictionary<string,object> map = 
                inhouseParser.Parse((JObject) jRows[0]);
            DateTime expected = 
                new DateTime(1408469674106*10000L + unixEpochTicks);
            Assert.Equal(expected, map["LastUpdated"]);
        }

        [Fact]
        public void LocalAspNetDateTime()
        {
            AddColumn("LastUpdated", "LastUpdated", ColumnType.Time);
            const string dateTimeData = 
                "[{\"LastUpdated\":\"\\/Date(1408469674106-0500)\\/\"}]";
            JsonParser inhouseParser = new JsonParser(settings);
            const string recordPath = "";
            JToken jRows =
                    inhouseParser.FindRecords(dateTimeData, recordPath);
            Assert.NotNull(jRows);
            Dictionary<string, object> map = 
                inhouseParser.Parse((JObject)jRows[0]);
            DateTime expected = 
                new DateTime(1408469674106*10000L + unixEpochTicks).
                    ToLocalTime();
            Assert.Equal(expected, map["LastUpdated"]);
        }

        [Fact]
        public void PathNameStartingWithDot() {
            AddColumn("Name", ".Name", ColumnType.Text);
            const string testJson = "[{\"Name\":\"" + Kalle + "\"}]";
            JsonParser inhouseParser = new JsonParser(settings);
            JToken jRows =
                     inhouseParser.FindRecords(testJson, "");
            Assert.NotNull(jRows);
            Assert.True(jRows.HasValues);
            Dictionary<string, object> map =
                inhouseParser.Parse((JObject)jRows[0]);
            Assert.Equal(Kalle, map["Name"]);
        }

        [Fact]
        public void FirstArrayInsideObjectTest()
        {
            AddColumn("name", "name", ColumnType.Text);
            const string testJson = "{\"homeTeam\":[{\"Name\":\"name0\"},{\"Name\":\"name1\"}],\"awayTeam\":[{\"Name\":\"name2\"},{\"Name\":\"name3\"}]}";
            JsonParser inhouseParser = new JsonParser(settings);
            JToken jRows = inhouseParser.FindRecords(testJson, "homeTeam");
            Assert.NotNull(jRows);
            Assert.True(jRows.HasValues);
        }

        [Fact]
        public void SecondArrayInsideObjectTest()
        {
            AddColumn("name", "name", ColumnType.Text);
            const string testJson = "{\"homeTeam\":[{\"Name\":\"name0\"},{\"Name\":\"name1\"}],\"awayTeam\":[{\"Name\":\"name2\"},{\"Name\":\"name3\"}]}";
            JsonParser inhouseParser = new JsonParser(settings);
            JToken jRows = inhouseParser.FindRecords(testJson, "awayTeam");
            Assert.NotNull(jRows);
            Assert.True(jRows.HasValues);
        }
    

        ///*********************        HELP METHODS        *******************/

        private void AddColumn(string columnName, string path, ColumnType type) {

            JsonColumnDefinition newColumn = (JsonColumnDefinition)settings.
                    CreateColumnDefinition(new PropertyBag());

            newColumn.Name = columnName;
            newColumn.JsonPath = path;
            newColumn.Type = type;

            settings.AddColumnDefinition(newColumn);
        }
    }
}
