﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class XPathColumnDefinitionTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            XPathColumnDefinition a = new XPathColumnDefinition(new PropertyBag());
            XPathColumnDefinition b = new XPathColumnDefinition(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            XPathColumnDefinition template = new XPathColumnDefinition(new PropertyBag());
            template.Type = ColumnType.Text;
            XPathColumnDefinition a = new XPathColumnDefinition(
                template.PropertyBag);
            XPathColumnDefinition b = new XPathColumnDefinition(
                template.PropertyBag);

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_XPath_Test()
        {
            XPathColumnDefinition a = new XPathColumnDefinition(new PropertyBag());
            a.XPath = "xpath";
            XPathColumnDefinition b = new XPathColumnDefinition(new PropertyBag());
            b.XPath = "xpath";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.XPath = "otherxpath";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClassDifferent_Test()
        {
            XPathColumnDefinition a = new XPathColumnDefinition(new PropertyBag());
            a.XPath = "xpath";
            XPathColumnDefinition b = new XPathColumnDefinition(new PropertyBag());
            b.XPath = "xpath";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Name = "bname";
            Assert.NotEqual(a, b);
        }
    }
}
