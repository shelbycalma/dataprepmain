﻿using System;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class TextAnalyserTest
    {
        private TextFileParserSettings parserSettings;
        private const char CommaColumnDelimiter = ',';
        private const string PipeColumnDelimiter = "|";
        private const string CustomDelimiter = "@";
        private const char DoubleQuote = '"';
        private const char SingleQuote = '\'';

        public TextAnalyserTest()
        {
            PropertyBag parserSettingsBag = new PropertyBag();

            parserSettingsBag.Values["ColumnDelimiter"] = PipeColumnDelimiter;
            parserSettingsBag.Values["CustomColumnDelimiter"] = "";
            parserSettingsBag.Values["TextQualifier"] = '\''+"";

            parserSettings = new TextFileParserSettings(parserSettingsBag);
        }

        [Fact]
        public void NormalCase()
        {
            // Test analysing string "a|b|c".
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "a", "b", "c" }, analyser);

        }

        [Fact]
        public void MissingLastValue()
        {
            // Test analysing string "a|b|".
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "a", "b", "" }, analyser);
        }

        [Fact]
        public void MissingFirstValue()
        {
            // Test analysing string "|b|c".
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "", "b", "c" }, analyser);
        }

        [Fact]
        public void OnlyMissingValues()
        {
            // Test analysing string "||".
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "", "", "" }, analyser);
        }

        [Fact]
        public void EmptyString()
        {
            // Test analysing string "".
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "" }, analyser);
        }

        [Fact]
        public void CommaColumnDelimiterTest()
        {
            // Test analysing string "a,,c".
            parserSettings.ColumnDelimiter = CommaColumnDelimiter;
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "a", "", "c" }, analyser);
        }

        [Fact]
        public void OtherCustomizedColumnDelimiterTest()
        {
            // Test analysing string "@b@c".
            parserSettings.ColumnDelimiter = DropDownDataProvider.OtherColumnDelimiterValue;
            parserSettings.CustomColumnDelimiter = CustomDelimiter;
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "", "b", "c" }, analyser);
        }

        [Fact]
        public void TextQualifierDoubleQuotes()
        {
            // Test analysing string '"a"|1|"c"'.
            parserSettings.TextQualifier = DoubleQuote;
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "a", "1", "c" }, analyser);
        }

        [Fact]
        public void TextQualifierSingleQuotes()
        {
            // Test analysing string "'a'|1|'c'".
            parserSettings.TextQualifier = SingleQuote;
            TextAnalyser analyser = new TextAnalyser(parserSettings);

            TestAnalyse(new string[] { "a", "1", "c" }, analyser);
        }


        /*************************    HELP METHOD   **************************/

        private void TestAnalyse(string[] columnvalues, TextAnalyser analyser)
        {
            string lineToAnalyse = makeCsvLine(columnvalues);

            // ANALYSE THE LINE
            analyser.InitAnalyseOfNewRow(lineToAnalyse);
            string analyserResult;
            // Do the analyse
            for (int i = 0; i < columnvalues.Length - 1; i++)
            {
                analyserResult = analyser.GetNextItem();
                Assert.Equal(columnvalues[i], analyserResult);
                Assert.False(analyser.IsEol());
            }
            // Now analyse the last expected value! We expect an EOL.
            analyserResult = analyser.GetNextItem();
            Assert.Equal(columnvalues[columnvalues.Length - 1], analyserResult);
            Assert.True(analyser.IsEol());
        }

        // CREATE CSV LINE (that we pretend to rad up)
        private String makeCsvLine(string[] columnvalues)
        {
            string columnDelimiter =
                parserSettings.ColumnDelimiter == DropDownDataProvider.OtherColumnDelimiterValue
                    ? parserSettings.CustomColumnDelimiter
                    : parserSettings.ColumnDelimiter + "";

            // CREATE CSV LINE (that we pretend to rad up)
            StringBuilder sb = new StringBuilder();
            // Make an easy to add text qualifier to the pretended read string
            string textQualifier =
                parserSettings.TextQualifier == DropDownDataProvider.NoneTextQualifierValue
                    ? ""
                    : parserSettings.TextQualifier + "";
            try
            {
                Convert.ToDouble(columnvalues[0]);
                sb.Append(columnvalues[0]);
            }
            catch (Exception)
            {
                // String, so add textQualifier if set to have it
                sb.Append(textQualifier + columnvalues[0] + textQualifier);
            }
            for (int i = 1; i < columnvalues.Length; i++)
            {
                sb.Append(columnDelimiter);
                try
                {
                    Convert.ToDouble(columnvalues[i]);
                    sb.Append(columnvalues[i]);
                }
                catch (Exception)
                {
                    // String, so add textQualifier if set to have it
                    sb.Append(textQualifier + columnvalues[i] + textQualifier);
                }
            }
            return sb.ToString();
        }

    }
}
