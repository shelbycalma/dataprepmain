﻿using Datawatch.DataPrep.Data.Core.Plugin;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class TableUpdaterTest
    {
        [Fact]
        public void DisposeUnstarted()
        {
            TableUpdater<ParameterTable, RealtimeConnectionSettings, object> 
                tableUpdater = new TableUpdater<ParameterTable, 
                    RealtimeConnectionSettings, object>(new ParameterTable(null), 
                        new RealtimeConnectionSettings());

            tableUpdater.Dispose();
        }
    }
}
