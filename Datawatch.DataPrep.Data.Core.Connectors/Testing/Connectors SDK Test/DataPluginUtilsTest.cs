﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Xunit;

namespace Datawatch.DataPrep.Data.Core.Connectors
{
    public class DataPluginUtilsTest
    {
        private const double Double_1_1 = 1.1;
        private const byte Byte_12 = 12;
        private const short Short_13 = 13;
        private const int Int_14 = 14;
        private const long Long_15 = 15;
        private const float Float_1_6 = 1.6f;
        private const decimal Decimal_1_7 = (decimal)1.7;
        private const sbyte Sbyte_12 = -12;
        private const ushort Ushort_18 = 18;
        private const uint Uint_19 = 19;
        private const ulong Ulong_20 = 20;
        private const string StringABC = "abc";
        private const bool BoolTrue = true;
        
        private DateTime DateTimeNow;
        private const string DateFormatT = "yyyy-MM-dd'T'HH:mm:ss";
        private const string DateFormat = "yyyy/MM/dd";//"yyyy-MM-dd HH:mm:ss";

        private const string CompanyColumn = "CompanyColumn";
        private const string MarketCapColumn = "MarketCapColumn";
        private const string ReportDateColumn = "ReportDateColumn";

        private const string Ericsson = "Ericsson";
        private const double EricssonValue = 150.4;
        private readonly DateTime EricssonDate = new DateTime(2014, 1, 11);

        private const string Abb = "ABB";

        private const string Nokia = "Nokia";
        private const double NokiaValue = 400.1;
        private readonly DateTime NokiaDate = new DateTime(2014, 3, 3);

        List<Dictionary<string, object>> inRows;

        private static StandaloneTable table;
        private static NumericDataHelper numericDataHelper;
        private static IEnumerable<ParameterValue> parameters;
        private static MyParserSettings parserSettings;
        private DateParser dateParser;

        public DataPluginUtilsTest()
        {
            DateTimeNow = DateTime.Now;
            // Remove the milliseconds that will be removed when doing DateTimeNow.ToString().
            DateTimeNow = DateTimeNow.AddTicks(-(DateTimeNow.Ticks%TimeSpan.TicksPerSecond));

            table = new StandaloneTable();

            parserSettings = new MyParserSettings(new PropertyBag());
            MyColumnDefinition colDef;

            table.BeginUpdate();

            table.AddColumn(new TextColumn(CompanyColumn));
            colDef = new MyColumnDefinition(new PropertyBag());
            colDef.Type = ColumnType.Text;
            colDef.Name = CompanyColumn;
            parserSettings.AddColumnDefinition(colDef);

            table.AddColumn(new NumericColumn(MarketCapColumn));
            colDef = new MyColumnDefinition(new PropertyBag());
            colDef.Type = ColumnType.Numeric;
            colDef.Name = MarketCapColumn;
            parserSettings.AddColumnDefinition(colDef);

            table.AddColumn(new TimeColumn(ReportDateColumn));
            colDef = new MyColumnDefinition(new PropertyBag());
            colDef.Type = ColumnType.Time;
            colDef.Name = ReportDateColumn;
            parserSettings.AddColumnDefinition(colDef);

            table.EndUpdate();

            numericDataHelper = new NumericDataHelper(new PropertyBag());


            parameters = new List<ParameterValue>();

            dateParser = new DateParser(null);
        }

        [Fact]
        public void ObjectToDouble_ValidDouble()
        {
            // DOUBLE
            double value = DataPluginUtils.ObjectToDouble(Double_1_1, numericDataHelper);
            Assert.Equal(Double_1_1, value);

            // STRING
            string newString = Double_1_1.ToString(CultureInfo.CurrentCulture);
            value = DataPluginUtils.ObjectToDouble(newString, numericDataHelper);
            Assert.Equal(Double_1_1, value);

            // BYTE
            value = DataPluginUtils.ObjectToDouble(Byte_12, numericDataHelper);
            Assert.Equal(Byte_12, value);

            // SHORT
            value = DataPluginUtils.ObjectToDouble(Short_13, numericDataHelper);
            Assert.Equal(Short_13, value);

            // INT
            value = DataPluginUtils.ObjectToDouble(Int_14, numericDataHelper);
            Assert.Equal(Int_14, value);

            // LONG
            value = DataPluginUtils.ObjectToDouble(Long_15, numericDataHelper);
            Assert.Equal(Long_15, value);

            // FLOAT
            value = DataPluginUtils.ObjectToDouble(Float_1_6, numericDataHelper);
            Assert.Equal(Float_1_6, value);

            // DECIMAL
            value = DataPluginUtils.ObjectToDouble(Decimal_1_7, numericDataHelper);
            Assert.Equal(Convert.ToDouble(Decimal_1_7), value, 5);

            // SBYTE
            value = DataPluginUtils.ObjectToDouble(Sbyte_12, numericDataHelper);
            Assert.Equal(Sbyte_12, value);

            // USHORT
            value = DataPluginUtils.ObjectToDouble(Ushort_18, numericDataHelper);
            Assert.Equal(Ushort_18, value);

            // UINT
            value = DataPluginUtils.ObjectToDouble(Uint_19, numericDataHelper);
            Assert.Equal(Uint_19, value);

            // ULONG
            value = DataPluginUtils.ObjectToDouble(Ulong_20, numericDataHelper);
            Assert.Equal(Ulong_20, value);
        }

        [Fact]
        public void ObjectToDouble_InvalidDouble()
        {
            // NON DIGITS
            double value = DataPluginUtils.ObjectToDouble(StringABC, numericDataHelper);
            Assert.Equal(Double.NaN, value);
        }

        [Fact]
        public void ObjectToDateTime_ValidDateTime()
        {
            // DATE TIME
            DateTime date = DataPluginUtils.ObjectToDateTime(DateTimeNow, null);
            Assert.Equal(DateTimeNow, date);

            // STRING
            string newString = DateTimeNow.ToString(CultureInfo.CurrentCulture);
            date = DataPluginUtils.ObjectToDateTime(newString, dateParser);
            Assert.Equal(DateTimeNow, date);

            // STRING - SPECIFIED FORMAT IN COLUMN DEFINITION
            dateParser.DateFormat = DateFormatT;
            string formattedDate = DateTimeNow.ToString(DateFormatT);
            date = DataPluginUtils.ObjectToDateTime(formattedDate, dateParser);
            Assert.Equal(DateTimeNow, date);

        }

        [Fact]
        public void ObjectToDateTime_ValidDateTime_UnixEpoch_PositiveNumber()
        {
            dateParser.DateFormat = DateParser.Posix;
            int secondsAfterUnixEpochDate = 59;
            DateTime expectedDateTime = new DateTime(1970, 1, 1, 0, 0, secondsAfterUnixEpochDate, DateTimeKind.Utc);
            string unixEpochDate = secondsAfterUnixEpochDate + "";
            DateTime date = DataPluginUtils.ObjectToDateTime(unixEpochDate, dateParser);
            Assert.Equal(expectedDateTime, date);
        }

        [Fact]
        public void ObjectToDateTime_ValidDateTime_UnixEpoch_NegativeNumber()
        {
            dateParser.DateFormat = DateParser.Posix;
            int secondsBeforeUnixEpochDate = -59;
            DateTime expectedDateTime = new DateTime(1969, 12, 31, 23, 59, 60 + secondsBeforeUnixEpochDate, DateTimeKind.Utc);
            string unixEpochDate = secondsBeforeUnixEpochDate + "";
            DateTime date = DataPluginUtils.ObjectToDateTime(unixEpochDate, dateParser);
            Assert.Equal(expectedDateTime, date);
        }

        [Fact]
        public void ObjectToDateTime_InvalidDateTime()
        {
            // When Invalid DateTime => DateTime.MinValue should be returned

            // DOUBLE
            DateTime date = DataPluginUtils.ObjectToDateTime(Double_1_1, null);
            Assert.Equal(DateTime.MinValue, date);

            // STRING
            string newString = StringABC.ToString(CultureInfo.CurrentCulture);
            date = DataPluginUtils.ObjectToDateTime(newString, dateParser);
            Assert.Equal(DateTime.MinValue, date);
        }

        [Fact]
        public void ObjectToDateTime_InvalidDateFormat()
        {
            string formatedDate = DateTime.Now.ToString(DateFormatT, CultureInfo.CurrentCulture);
            dateParser.DateFormat = DateFormat;
            DateTime date = DataPluginUtils.ObjectToDateTime(formatedDate, dateParser);
            // The failed parsing of the DateTime string should result in a DateTime.MinValue.
            Assert.Equal(DateTime.MinValue, date);
        }

        [Fact]
        public void ObjectToString_ValidString()
        {
            // STRING
            string value = DataPluginUtils.ObjectToString(StringABC);
            Assert.Equal(StringABC, value);

            // DOUBLE
            value = DataPluginUtils.ObjectToString(Double_1_1);
            Assert.Equal(Double_1_1.ToString(CultureInfo.CurrentCulture),
                value);

            // BYTE
            value = DataPluginUtils.ObjectToString(Byte_12);
            Assert.Equal(Byte_12.ToString(CultureInfo.CurrentCulture), value);

            // SHORT
            value = DataPluginUtils.ObjectToString(Short_13);
            Assert.Equal(Short_13.ToString(CultureInfo.CurrentCulture),
                value);

            // INT
            value = DataPluginUtils.ObjectToString(Int_14);
            Assert.Equal(Int_14.ToString(CultureInfo.CurrentCulture),
                value);

            // LONG
            value = DataPluginUtils.ObjectToString(Long_15);
            Assert.Equal(Long_15.ToString(CultureInfo.CurrentCulture),
                value);

            // FLOAT
            value = DataPluginUtils.ObjectToString(Float_1_6);
            Assert.Equal(Float_1_6.ToString(CultureInfo.CurrentCulture),
                value);

            // DECIMAL
            value = DataPluginUtils.ObjectToString(Decimal_1_7);
            Assert.Equal(Decimal_1_7.ToString(CultureInfo.CurrentCulture),
                value);

            // SBYTE
            value = DataPluginUtils.ObjectToString(Sbyte_12);
            Assert.Equal(Sbyte_12.ToString(CultureInfo.CurrentCulture),
                value);

            // USHORT
            value = DataPluginUtils.ObjectToString(Ushort_18);
            Assert.Equal(Ushort_18.ToString(CultureInfo.CurrentCulture),
                value);

            // UINT
            value = DataPluginUtils.ObjectToString(Uint_19);
            Assert.Equal(Uint_19.ToString(CultureInfo.CurrentCulture),
                value);

            // ULONG
            value = DataPluginUtils.ObjectToString(Ulong_20);
            Assert.Equal(Ulong_20.ToString(CultureInfo.CurrentCulture),
                value);

            // BOOL
            value = DataPluginUtils.ObjectToString(BoolTrue);
            Assert.Equal(BoolTrue.ToString(CultureInfo.CurrentCulture),
                value);
        }

        [Fact]
        public void AddRow_CheckAllRowWhereAdded()
        {
            InitRowsDictionaries();

            string[] names = new string[] { CompanyColumn };
            object[] values = new object[] { new string[] { Ericsson, Abb, Nokia } };
            parameters = CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            AddAllRows(parameterFilter);
            Assert.Equal(inRows.Count, table.RowCount);
        }

        [Fact]
        public void AddRow_AddRowWithParameterValue()
        {
            InitRowsDictionaries();

            string[] names = new string[] { CompanyColumn };
            object[] values = new object[] { Ericsson };
            parameters = CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            Assert.Equal(0, table.RowCount);

            table.BeginUpdate();
            DataPluginUtils.AddRow(inRows[0], table, numericDataHelper,
                parameterFilter, parserSettings);
            table.EndUpdate();

            // Only the Erisson row should have been added
            Assert.Equal(1, table.RowCount);

            Assert.Equal(Ericsson,
                ((TextColumn) table.GetColumn(CompanyColumn))
                    .GetTextValue(table.GetRow(0)));
            Assert.Equal(EricssonValue, 
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));
            Assert.Equal(EricssonDate, 
                ((TimeColumn) table.GetColumn(ReportDateColumn))
                    .GetTimeValue(table.GetRow(0)));
        }

        [Fact]
        public void AddRow_TryAddRowNotWithParameterValue()
        {
            InitRowsDictionaries();

            string[] names = new string[] { CompanyColumn };
            object[] values = new object[] { Ericsson };
            parameters = CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            table.BeginUpdate();
            DataPluginUtils.AddRow(inRows[1], table, numericDataHelper,
                parameterFilter, parserSettings);
            table.EndUpdate();

            Assert.Equal(0, table.RowCount);
        }

        [Fact]
        public void AddRow_OnlyRowsWithParameterValueAdded()
        {
            InitRowsDictionaries();

            string[] names = new string[] { CompanyColumn };
            object[] values = new object[] { Nokia };
            parameters = CreateParametersHelper.CreateParameters(names, values);
            ParameterFilter parameterFilter =
                    ParameterFilter.CreateFilter(table, parameters);

            AddAllRows(parameterFilter);

            Assert.Equal(1, table.RowCount);

            Assert.Equal(Nokia,
                ((TextColumn) table.GetColumn(CompanyColumn))
                    .GetTextValue(table.GetRow(0)));
            Assert.Equal(NokiaValue,
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));
            Assert.Equal(NokiaDate,
                ((TimeColumn) table.GetColumn(ReportDateColumn))
                    .GetTimeValue(table.GetRow(0)));
        }

        [Fact]
        public void UpdateColumns_CheckValues()
        {
            InitRowsDictionaries();

            Row row = CreateEmptyRow();

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(Ericsson,
                ((TextColumn) table.GetColumn(CompanyColumn))
                    .GetTextValue(table.GetRow(0)));
            Assert.Equal(EricssonValue, 
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));
            Assert.Equal(EricssonDate, 
                ((TimeColumn) table.GetColumn(ReportDateColumn))
                    .GetTimeValue(table.GetRow(0)));
        }

        [Fact]
        public void UpdateColumns_TryAddNullTextValue()
        {
            InitRowsDictionaries();

            Row row = CreateEmptyRow();

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(Ericsson,
                ((TextColumn) table.GetColumn(CompanyColumn))
                    .GetTextValue(table.GetRow(0)));

            InitRowsDictionaries();
            inRows[0][CompanyColumn] = null;

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(Ericsson,
                ((TextColumn) table.GetColumn(CompanyColumn))
                    .GetTextValue(table.GetRow(0)));
        }

        [Fact]
        public void UpdateColumns_TryAddNullNumericValue()
        {
            InitRowsDictionaries();

            Row row = CreateEmptyRow();

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(EricssonValue, 
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));

            InitRowsDictionaries();
            inRows[0][MarketCapColumn] = null;

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(EricssonValue, 
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));
        }

        [Fact]
        public void UpdateColumns_TryAddBadDateTimeValue()
        {
            InitRowsDictionaries();

            Row row = CreateEmptyRow();

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(EricssonValue,
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));

            InitRowsDictionaries();
            inRows[0][ReportDateColumn] = "abcd";

            table.BeginUpdate();
            DataPluginUtils.UpdateColumns(inRows[0], table, row, numericDataHelper, parserSettings);
            table.EndUpdate();

            Assert.Equal(EricssonValue,
                ((NumericColumn) table.GetColumn(MarketCapColumn))
                    .GetNumericValue(table.GetRow(0)));
        }

        private void AddAllRows(ParameterFilter parameterFilter)
        {
            table.BeginUpdate();
            foreach (Dictionary<string, object> inRow in inRows)
            {
                DataPluginUtils.AddRow(inRow, table, numericDataHelper,
                    parameterFilter, parserSettings);
            }
            table.EndUpdate();
        }

        private Row CreateEmptyRow()
        {
            table.BeginUpdate();
            Row row = table.AddRow();
            table.EndUpdate();
            return row;
        }

        private void InitRowsDictionaries()
        {
            inRows = new List<Dictionary<string, object>>();

            for (int i = 0; i < 3; i++)
            {
                inRows.Add(new Dictionary<string, object>());
            }

            inRows[0][CompanyColumn] = Ericsson;
            inRows[0][MarketCapColumn] = "" + EricssonValue;
            inRows[0][ReportDateColumn] = "" + EricssonDate;

            inRows[1][CompanyColumn] = Abb;
            inRows[1][MarketCapColumn] = "60.8";
            inRows[1][ReportDateColumn] = "2014-02-22";

            inRows[2][CompanyColumn] = Nokia;
            inRows[2][MarketCapColumn] = "" + NokiaValue;
            inRows[2][ReportDateColumn] = "" + NokiaDate;
        }

        class MyParserSettings : ParserSettings
        {

            public MyParserSettings(PropertyBag bag)
                : base(bag)
            {
            }

            public override string GetDescription()
            {
                return "";
            }

            public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
            {
                return new MyColumnDefinition(bag);
            }

            public override IParser CreateParser()
            {
                throw new System.NotImplementedException();
            }

        }

        class MyColumnDefinition : ColumnDefinition
        {
            public MyColumnDefinition(PropertyBag bag) : base(bag)
            {
            }

            public override bool IsValid
            {
                get { return true; }
            }
        }
    }
}
