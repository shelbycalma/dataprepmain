using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class FixParserSettings : ParserSettings
    {
        public FixParserSettings(PropertyBag bag) : base(bag)
        {
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new FixColumnDefinition(bag);
        }

        public override IParser CreateParser()
        {
            return new FixParser(this);
        }

        public override string GetDescription()
        {
            return "";
        }
    }
}
