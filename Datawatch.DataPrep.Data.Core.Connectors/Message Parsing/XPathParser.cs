﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using Datawatch.DataPrep.Data.Core.MessageParsing.Properties;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class XPathParser : ParserBase, ISchemaDiscovery
    {
        private readonly XPathParserSettings settings;
        private IEnumerator<string> columnNames;
        private IEnumerator<XPathExpression> expressions;
        private NamespaceResolver namespaceResolver;
        //private Predicate<object[]> filter = null;

        public XPathParser(XPathParserSettings settings)
        {
            this.settings = settings;
        }

        private void InitColumnLists()
        {
            // Create the columnNames and expressions based on the namespace
            // we now have.
            List<string> columnNameList = new List<string>();
            List<XPathExpression> expressionList = new List<XPathExpression>();
            foreach (ColumnDefinition columnDefinition in settings.Columns)
            {

                if (!columnDefinition.IsEnabled) continue;

                expressionList.Add(XPathExpression.Compile(
                    ((XPathColumnDefinition)columnDefinition).XPath,
                    namespaceResolver));
                columnNameList.Add(columnDefinition.Name);
            }
            columnNames = columnNameList.GetEnumerator();
            expressions = expressionList.GetEnumerator();
        }

        public XmlNodeList GetRecords(XmlDocument reusableXmlDoc, string xmlString, string recordPath)
        {
            reusableXmlDoc.LoadXml(xmlString);

            return GetRecords(reusableXmlDoc, recordPath);
        }

        public XmlNodeList GetRecords(StreamReader st, string recordPath)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(st);

            return GetRecords(xmlDoc, recordPath);
        }

        public XmlNodeList GetRecords(XmlDocument xmlDoc, string recordPath)
        {
            if (namespaceResolver == null)
            {
                namespaceResolver = new NamespaceResolver(xmlDoc);
            }
            XmlNodeList xmlRows;
            try
            {
                xmlRows = xmlDoc.SelectNodes(recordPath, namespaceResolver);
            }
            catch (Exception e)
            {
                throw new Exception(string.Format(
                        Resources.ExErrorInvalidXPath, "Record XPath"));
            }
            return xmlRows;
        }

        // Call this when we have one single xml document containing all the rows 
        // we are looking for. Before calling this method, call GetRecords() to 
        // get all records (as a XmlNodeList) that exist at the RecordPath. Then 
        // for each Node, call this method. This so we don't have to convert the 
        // Node to a String and then call parse(String) that will convert back 
        // again to a xml object which isn't effective in this case.
        public Dictionary<string, object> Parse(XmlNode xmlNode)
        {
            Dictionary<string, object> dictionary = new Dictionary<string, object>();

            if (xmlNode == null) return dictionary;

            if (xmlNode is XmlDocument && namespaceResolver == null)
            {
                // Set namespaceresolver before precompile expressions
                namespaceResolver = new NamespaceResolver((XmlDocument)xmlNode);
            }

            if (columnNames == null || expressions == null)
            {
                InitColumnLists();
            }

            XPathNavigator navigator = xmlNode.CreateNavigator();

            expressions.Reset();
            columnNames.Reset();
            List<object> listValues = new List<object>();

            while (expressions.MoveNext() && columnNames.MoveNext())
            {
                if (expressions.Current == null || columnNames.Current == null)
                    break;

                object obj;
                try
                {
                    obj = navigator.Evaluate(expressions.Current);
                }
                catch (Exception ex)
                {
                    Log.Error(Resources.LogXPathEvaluationFailed, columnNames.Current,
                        ex.Message);
                    throw;
                }
                if (obj == null)
                {
                    listValues.Add(new object());
                    continue;
                }

                if (obj is XPathNodeIterator)
                {
                    XPathNodeIterator it = (XPathNodeIterator)obj;
                    if (it.MoveNext() && it.Current is IHasXmlNode)
                    {
                        XmlNode node = ((IHasXmlNode)it.Current).GetNode();
                        if (node != null)
                        {
                            // Handle attribute values
                            if (node is XmlAttribute)
                            {
                                obj = node.Value;
                            }
                            //Handle text() and element
                            else if (node is XmlCharacterData)
                            {
                                obj = node.Value;
                            }
                            else if (node is XmlElement)
                            {
                                obj = node.InnerText;
                            }
                        }
                    }
                    else
                    {
                        obj = null;
                    }
                }

                if (obj == null)
                {
                    listValues.Add(new object());
                    continue;
                }
                listValues.Add(obj);
                dictionary[columnNames.Current] = obj;
            }
            object[] values = listValues.ToArray();

            return GetFilteredRecord(values, dictionary,
                settings.Parameters, settings.Columns);
        }

        // Call this when one incomming xml document is one row, typically in a 
        // realtime plugin, getting one xml document/row at a time as a String. 
        // Call this method with that xml document (as a String) and it will be 
        // parsed into a dictionary <columnName, value>.
        public Dictionary<string, object> Parse(string message)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(message);
            return Parse(doc);
        }

        public void DiscoverDocumentSchema(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(new StringReader(message));

            //recursively find all the xml nodes
            Dictionary<string, Attribute> nodesAndPaths =
                GetXmlNodesAndPaths(xDoc.ChildNodes, xDoc.DocumentElement.Name, isStreaming);

            foreach (KeyValuePair<string, Attribute> nodeAndPath
                in nodesAndPaths)
            {
                try
                {
                    string columnName = nodeAndPath.Key.Replace("@", "").Replace('/', '_');
                    string value = nodeAndPath.Value.value;
                    XPathColumnDefinition colDef =
                        (XPathColumnDefinition)columnGenerator.UpdateColumnDefinitionList(
                            columnName, value);
                    colDef.XPath = String.Join("/", nodeAndPath.Value.path);
                }
                catch (Exception e)
                {
                    Log.Error(Properties.Resources.LogColumnDefinitionError);
                    Log.Exception(e);
                }
            }
        }

        public string[] DiscoverDocumentSchemaAndValues(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            throw new NotImplementedException();
        }

        private Dictionary<string, Attribute> GetXmlNodesAndPaths(
            XmlNodeList xNodeList, string rootName, bool isStreaming, List<string> path = null)
        {
            Dictionary<string, Attribute> retDict =
                new Dictionary<string, Attribute>();

            foreach (XmlNode xNode in xNodeList)
            {
                List<string> branchPath;
                if (path == null)
                {
                    branchPath = new List<string>();
                    if (isStreaming)
                    {
                        branchPath.Add(rootName);
                    }
                }
                else
                {
                    branchPath = new List<string>(path);
                }

                if (xNode.Name != rootName)
                    branchPath.Add(xNode.Name);

                Dictionary<string, Attribute> childDict =
                    GetXmlNodesAndPaths(xNode.ChildNodes, rootName, isStreaming, branchPath);

                foreach (KeyValuePair<string, Attribute> kvp in childDict)
                {
                    if (!retDict.ContainsKey(kvp.Key))
                    {
                        retDict.Add(kvp.Key, kvp.Value);
                    }
                }

                if (!retDict.ContainsKey(xNode.Name)
                    && !xNode.Name.StartsWith("#")
                    && !xNode.Name.Equals("xml")
                    && !xNode.Name.Equals(rootName))
                {
                    Attribute attr = new Attribute();
                    attr.path = branchPath;
                    if (xNode.Value != null)
                    {
                        attr.value = xNode.Value;
                    }
                    else
                    {
                        attr.value = xNode.InnerText;
                    }
                    retDict.Add(String.Join("/", branchPath), attr);
                }
                if (xNode.Attributes != null)
                {
                    foreach (XmlAttribute attribute in xNode.Attributes)
                    {
                        List<string> attributePath = new List<string>(branchPath);
                        attributePath.Add("@" + attribute.Name);
                        Attribute attr = new Attribute();
                        attr.path = attributePath;
                        attr.value = attribute.Value;
                        retDict.Add(String.Join("/", attributePath), attr);
                    }
                }
            }

            return retDict;
        }

        class Attribute
        {
            internal List<string> path;
            internal string value;
        }
    }
}
