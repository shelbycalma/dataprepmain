﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class TextFilePlugin : ParserPluginBase, IParserPlugin
    {
        private readonly string Name = "Text";

        public Type GetParserType()
        {
            return typeof(TextFileParser);
        }

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new TextFileParserSettings(bag);
        }

        public override string Id
        {
            get { return Name; }
        }

        public override string Title
        {
            get { return Name; }
        }
    }
}
