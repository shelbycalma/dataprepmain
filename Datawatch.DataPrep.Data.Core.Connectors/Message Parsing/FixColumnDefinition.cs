using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class FixColumnDefinition : ColumnDefinition
    {
        public FixColumnDefinition(PropertyBag bag) : base(bag)
        {
        }

        public string FixTag
        {
            get
            {
                string tag = GetInternal("FixTag");
                if (!string.IsNullOrEmpty(tag))
                {
                    return tag;
                }
                return GetInternal("FixName");
            }
            set { SetInternal("FixTag", value);}
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is FixColumnDefinition)) return false;
            FixColumnDefinition fixColumnDefinition = (FixColumnDefinition)obj;

            return string.Equals(FixTag, fixColumnDefinition.FixTag);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (!string.IsNullOrEmpty(FixTag))
            {
                code += code * 23 + FixTag.GetHashCode();
            }
            return code;
        }

        // Check so column name and path have been entered.
        public override bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Name) 
                    && !string.IsNullOrEmpty(FixTag);
            }
        }

    }
}
