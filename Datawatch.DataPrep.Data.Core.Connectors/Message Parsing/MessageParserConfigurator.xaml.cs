﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using ColumnDefinition = Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing.ColumnDefinition;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    /// <summary>
    /// Interaction logic for MessageTypeConfigurator.xaml
    /// </summary>
    public partial class MessageParserConfigurator : UserControl
    {
        private ICommand testConnection;
        private ICommand generateColumns;

        public MessageParserConfigurator()
        {
            InitializeComponent();
        }

        private void Delete_CanExecute(object sender,
            CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter is ColumnDefinition;
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show(
                Properties.Resources.UiAskRemoveColumn,
                ResourceLoader.ResourceLoader.GetResourceString("UiApplicationTitle"),
                MessageBoxButton.OKCancel, 
                MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                ((IMessageQueueConnectorSettings) DataContext).ParserSettings.RemoveColumnDefinition(
                    (ColumnDefinition) e.Parameter);
            }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get
            {
                IParameterizedMessageQueueSettings settings =
                    DataContext as IParameterizedMessageQueueSettings;
                if (settings == null) return null;

                return settings.Parameters;
            }
        }

        public bool ShowFilter
        {
            get
            {

                IMessageQueueConnectorSettings settings =
                    DataContext as IMessageQueueConnectorSettings;
                if (settings == null) return false;

                return ((IMessageQueueConnectorSettings)DataContext).ShowFilter;
            }
        }

        public ICommand GenerateColumns
        {
            get
            {
                if (generateColumns == null)
                {
                    generateColumns = new DelegateCommand(
                        DoGenerateColumns, CanGenerateColumns);
                }
                return generateColumns;
            }
        }

        private bool CanGenerateColumns()
        {
            if (DataContext == null)
            {
                return false;
            }
            return ((IMessageQueueConnectorSettings)DataContext).CanGenerateColumns();
        }

        private void DoGenerateColumns()
        {
            try
            {
                ((IMessageQueueConnectorSettings)DataContext).DoGenerateColumns();
            }
            catch (System.Exception e)
            {
                MessageBox.Show(
                   e.Message, Core.ResourceLoader.ResourceLoader.GetResourceString("UiApplicationTitle"),
                   MessageBoxButton.OK,
                   MessageBoxImage.Warning);
            }
        }

        public ICommand TestConnection
        {
            get
            {
                if (testConnection == null)
                {
                    testConnection = new DelegateCommand(DoTestConnection,
                        CanTestConnection);
                }
                return testConnection;
            }
        }

        private bool CanTestConnection()
        {
            if (DataContext == null)
            {
                return false;
            }
            return ((IMessageQueueConnectorSettings) DataContext).CanTestConnection();
        }

        private void DoTestConnection()
        {
            ((IMessageQueueConnectorSettings)DataContext).DoTestConnection();
        }

    }
}
