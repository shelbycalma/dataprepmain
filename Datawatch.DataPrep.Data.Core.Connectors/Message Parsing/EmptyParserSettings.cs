﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class EmptyParserSettings : ParserSettings
    {
        public static readonly EmptyParserSettings Instance = new EmptyParserSettings();

        private EmptyParserSettings() : base(new PropertyBag())
        {
        }

        public override string GetDescription()
        {
            return "";
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            throw new System.NotImplementedException();
        }

        public override IParser CreateParser()
        {
            throw new System.NotImplementedException();
        }

    }
}
