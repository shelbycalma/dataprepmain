namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public static class DropDownDataProvider
    {
        public const char OtherColumnDelimiterValue = 'O';
        public const char NoneTextQualifierValue = 'N';

        public static char[] ColumnDelimiters = new char[]
            {
                ',',
                ';',
                '|',
                ' ',
                '\t',
                OtherColumnDelimiterValue//Other
            };

        public static char[] TextQualifiers = new char[]
            {
                NoneTextQualifierValue,
                '"',
                '\''
            };
       
       
        public static int[] DataTypeDiscoveryRows = new int[]
        {
            1,
            10,
            50
        };

        public static int[] TimeoutSeconds = new int[]
        {
            10,
            20,
            30,
            60
        };
    }
}
