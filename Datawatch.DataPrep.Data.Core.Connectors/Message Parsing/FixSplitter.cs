using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class FixSplitter
    {
        public static Dictionary<string,string> Split(string fixdata)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            int startIndex1 = 0;

            // Skip initial field separators
            while (startIndex1 < fixdata.Length && fixdata[startIndex1] == '\x0001')
            {
                startIndex1++;
            }           
            while (true)
            {
                int startIndex2 = fixdata.IndexOf('=', startIndex1);
                if (startIndex2 == -1) break;

                string fixtag = fixdata.Substring(startIndex1, startIndex2 - startIndex1);
                int num2 = fixdata.IndexOf('\x0001', startIndex2);
                string str =
                    num2 == -1 ?
                    fixdata.Substring(startIndex2 + 1) :
                    fixdata.Substring(startIndex2 + 1, num2 - (startIndex2 + 1));
                if (str.Length > 0)
                {
                    dictionary[fixtag] = str;                    
                }
                if (num2 == -1)
                {
                    break;
                }
                // Skip field separators
                while (num2 < fixdata.Length && fixdata[num2] == '\x0001')
                {
                    num2++;
                }
                if (num2 > fixdata.Length - 4)
                {
                    break;
                }
                startIndex1 = num2;
            }
            return dictionary;
        }                 
    }
}
