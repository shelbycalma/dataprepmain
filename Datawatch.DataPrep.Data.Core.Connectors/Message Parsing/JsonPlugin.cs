using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class JsonPlugin : ParserPluginBase, IParserPlugin
    {
        private const string Name = "Json";

        public Type GetParserType()
        {
            return typeof(JsonParser);
        }

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new JsonParserSettings(bag);
        }

        public override string Id
        {
            get { return Name; }
        }

        public override string Title
        {
            get { return Name; }
        }
    }
}