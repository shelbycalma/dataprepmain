﻿using System.Collections.Generic;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class DynamicTextAnalyser
    {
        private readonly TextAnalyser analyser;

        /// <summary>
        /// Use this during the data discovery phase when we always want to extract 
        /// all column values. When you only need to extract the first # number of 
        /// column values, then use StaticTextAnalyser.
        /// </summary>
        /// <param name="analyser"></param>
        public DynamicTextAnalyser(TextAnalyser analyser)
        {
            this.analyser = analyser;
        }

        public string[] GetColumnValues(string line)
        {
            if (line == null) return null;

            List<string> row = new List<string>();

            analyser.InitAnalyseOfNewRow(line);
            string cell = analyser.GetNextItem();

            if (cell.Length == 0 && analyser.IsEol())
            {
                // Empty row
                return null;
            }

            row.Add(cell);

            while (!analyser.IsEol())
            {
                cell = analyser.GetNextItem();

                row.Add(cell);
            }
            return row.ToArray();
        }
    }
}
