using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class TextFileParser : ParserBase, ISchemaDiscovery
    {
        private readonly List<string> columnNames;
        private readonly int[] columnIndexes;
        // We use maxIndex to make parsing much faster when only the first 
        // columns are used in the message. We know how many column values 
        // we need to parse before we can throw the rest away.
        private StaticTextAnalyser analyser;
        private DynamicTextAnalyser columnDiscoveryAnalyser;
        private readonly int maxIndex;
        private readonly bool isFirstRowHeading;
        private bool initCheckForNewLineDone = false;
        private TextFileParserSettings settings;
        private int discoveredRows = 0;

        private ParameterFilter parameterFilter = null;
        public TextFileParser(TextFileParserSettings settings)
        {
            columnNames = new List<string>();
            List<int> colIndexes = new List<int>();
            this.settings = settings;
            foreach (TextFileColumnDefinition columnDefinition in settings.Columns)
            {
                if (!columnDefinition.IsEnabled) continue;

                columnNames.Add(columnDefinition.Name);
                colIndexes.Add(columnDefinition.Index);
            }
            columnIndexes = colIndexes.ToArray();
            maxIndex = settings.MaxIndex;
            isFirstRowHeading = settings.IsFirstRowHeading;
        }

        public void DiscoverDocumentSchema(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            DiscoverDocumentSchemaAndValues(message, columnGenerator, isStreaming);
        }

        public string[] DiscoverDocumentSchemaAndValues(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            if (string.IsNullOrEmpty(message))
            {
                return null;
            }
            if (columnDiscoveryAnalyser == null)
            {
                columnDiscoveryAnalyser = new DynamicTextAnalyser(new TextAnalyser(settings));
            }
            if (settings.IsFirstRowHeading)
            {
                if (isStreaming)
                {
                    // For each message we had a header row 
                    StringReader stringReader = new StringReader(message);
                    string headerLine = stringReader.ReadLine();
                    message = stringReader.ReadToEnd();
                    stringReader.Close();
                    stringReader.Dispose();
                    if (columnNames.Count == 0)
                    {
                        ExtractHeaders(headerLine);
                    }
                }
                else if (columnNames.Count == 0)
                {
                    ExtractHeaders(message);
                    return null;
                }
            }
            string[] columnValues = columnDiscoveryAnalyser.GetColumnValues(message);

            if (columnValues == null ||
                columnValues.Length == 1 && string.IsNullOrEmpty(columnValues[0]))
            {
                // Empty row, skip it!
                return null;
            }
            for (int i = 0; i < columnValues.Length; i++)
            {
                if (discoveredRows == 0)
                {
                    //remove non-breaking space
                    //this is to fix VP# 10258.
                    //TODO: Improve! should we support this character?
                    columnValues[i] = columnValues[i].Replace("\u00A0", string.Empty);
                }
                string columnValue = columnValues[i];
                if (i >= columnNames.Count)
                {
                    // We need to add an additional header
                    AddGeneratedHeader(columnNames);
                }
                // Fix for [DDTV-5578], do not continue on empty value, 
                // add as text.
                //if (string.IsNullOrEmpty(columnValue))
                //{
                //    continue;
                //}
                string columnName = columnNames[i];
                TextFileColumnDefinition cd = (TextFileColumnDefinition)columnGenerator.UpdateColumnDefinitionList(
                    columnName, columnValue);
                cd.Index = i;
                if (discoveredRows > 0 && ColumnType.Numeric.Equals(cd.Type))
                {
                    //remove non-breaking space
                    //this is to fix VP# 10258.
                    //TODO: Improve! should we support this character?
                    columnValues[i] = columnValue.Replace("\u00A0", string.Empty);
                }
            }
            discoveredRows++;
            return columnValues;
        }

        private void ExtractHeaders(string headerRow)
        {
            // Remember the column names
            string[] foundNames = columnDiscoveryAnalyser.GetColumnValues(headerRow);
            columnNames.Clear();
            foreach (string columnName in foundNames)
            {
                if (string.IsNullOrEmpty(columnName) || columnNames.Contains(columnName))
                {
                    // Invalid column name => get a new name
                    AddGeneratedHeader(columnNames);
                }
                else
                {
                    columnNames.Add(columnName);
                }
            }

        }

        public bool IsValidRow(string[] row)
        {
            return !(row.Length == 1 && string.IsNullOrEmpty(row[0]));
        }

        public void SetLineAnalyser(StaticTextAnalyser analyser)
        {
            this.analyser = analyser;
        }

        public Dictionary<string, object> ParseInternal(string message,
            Dictionary<string, object> dict, string[] row)
        {
            int numRead = analyser.GetColumnValues(message, row);

            if (row == null) return dict;
            for (int i = 0; i < columnNames.Count; i++)
            {
                int index = columnIndexes[i];

                if (index >= numRead) continue;

                if (!string.IsNullOrEmpty(row[index]))
                {
                    dict[columnNames[i]] = row[index];
                }
            }
            return GetFilteredRecord(row, dict, settings.Parameters,
                settings.Columns);
        }

        /// <summary>
        /// Call this when parsing a whole file and can reuse the Dictionary to 
        /// speed things up.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="dict"></param>
        /// <returns></returns>
        public Dictionary<string, object> Parse(string message, Dictionary<string, object> dict,
            string[] row)
        {
            dict.Clear();
            return ParseInternal(message, dict, row);
        }

        /**
         * Call this when obtaining one record at a time running streaming data
         * with for example ActiveMQ. These message have not yet been analyzed in 
         * regard to decimal separator, column delimiter and text qualifier, so we 
         * have to do it here.
         * Call AddLineAnalyser() before calling this method.
         */
        public Dictionary<string, object> Parse(string message)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string[] row = new string[maxIndex + 1];
            if (!initCheckForNewLineDone)
            {
                analyser.PassNewLine = isFirstRowHeading;
                initCheckForNewLineDone = true;
            }
            return ParseInternal(message, dict, row);
        }

        private void AddGeneratedHeader(List<string> columnNames)
        {
            int c = 1;
            while (true)
            {
                string h = "Field" + c++;
                if (!columnNames.Contains(h))
                {
                    columnNames.Add(h);
                    return;
                }
            }
        }
    }
}