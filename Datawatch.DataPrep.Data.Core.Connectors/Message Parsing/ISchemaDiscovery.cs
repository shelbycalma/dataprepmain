﻿using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public interface ISchemaDiscovery : IParser
    {
        void DiscoverDocumentSchema(string message, ColumnGenerator columnGenerator,
            bool isStreaming);
        string[] DiscoverDocumentSchemaAndValues(string message, ColumnGenerator columnGenerator,
            bool isStreaming);
    }
}