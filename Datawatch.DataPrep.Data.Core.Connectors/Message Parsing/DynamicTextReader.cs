﻿using System.IO;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    /// <summary>
    /// Use this class in the data discovery phase when we need to extract 
    /// all column values that are read up by the stream. If only need to 
    /// read the first # number of column values, then use StaticTextReader.
    /// </summary>
    public class DynamicTextReader 
    {
        private readonly string[] EmptyArray = new string[] { };
        private readonly StreamReader stream;
        private readonly DynamicTextAnalyser analyser;

        public DynamicTextReader(StreamReader stream, TextAnalyser textAnalyser) {
            this.stream = stream;
            this.analyser = new DynamicTextAnalyser(textAnalyser);
        }

        public string[] ReadNextLine()
        {
            string line = stream.ReadLine();

            if (line == null) return null;

            if (line.Length == 0) return EmptyArray;
            
            return analyser.GetColumnValues(line);
        }
    }
}
