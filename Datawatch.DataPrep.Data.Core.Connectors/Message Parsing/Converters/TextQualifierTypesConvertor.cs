using System;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core;

namespace Datawatch.DataPrep.Data.Core.MessageParsing.Converters
{
    class TextQualifierTypesConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is char))
                value = DropDownDataProvider.NoneTextQualifierValue;

            char columnDelimiter = (char)value;

            switch (columnDelimiter)
            {
                case DropDownDataProvider.NoneTextQualifierValue:
                    return Properties.Resources.UiTextQualifierNone;
                case '"':
                    return Properties.Resources.UiTextQualifierDoubleQuotes;
                case '\'':
                    return Properties.Resources.UiTextQualifierSingleQuote;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
