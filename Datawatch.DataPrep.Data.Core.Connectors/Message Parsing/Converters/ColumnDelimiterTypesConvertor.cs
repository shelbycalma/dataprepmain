using System;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core;

namespace Datawatch.DataPrep.Data.Core.MessageParsing.Converters
{
    class ColumnDelimiterTypesConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value == null || !(value is char))
                value = ',';
            
            char columnDelimiter =(char) value;

            switch (columnDelimiter)
            {
                case ',':
                    return Properties.Resources.UiColumnDelimeterComma;
                case ';':
                    return Properties.Resources.UiColumnDelimeterSemicolon;
                case '|':
                    return Properties.Resources.UiColumnDelimeterPipe;
                case ' ':
                    return Properties.Resources.UiColumnDelimeterSpace;
                case '\t':
                    return Properties.Resources.UiColumnDelimeterTab;
                case DropDownDataProvider.OtherColumnDelimiterValue:
                    return Properties.Resources.UiColumnDelimeterOther;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
