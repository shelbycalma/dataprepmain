﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core.MessageParsing.Properties;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Datawatch.DataPrep.Data.Core.MessageParsing.Converters
{
    [ValueConversion(typeof(ColumnType), typeof(string))]
    public class ColumnTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is ColumnType)) 
                return null;

            ColumnType columnType = (ColumnType)value;

            if (columnType == ColumnType.Text)
            {
                return Resources.UiColumnTypeText;
            }
            if (columnType == ColumnType.Time)
            {
                return Resources.UiColumnTypeTime;
            }
            if (columnType == ColumnType.Numeric)
            {
                return Resources.UiColumnTypeNumeric;
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (Resources.UiColumnTypeText.Equals(value))
            {
                return ColumnType.Text;
            }
            if (Resources.UiColumnTypeTime.Equals(value))
            {
                return ColumnType.Time;
            }
            if (Resources.UiColumnTypeNumeric.Equals(value))
            {
                return ColumnType.Numeric;
            }
            return DependencyProperty.UnsetValue;
        }
    }
}
