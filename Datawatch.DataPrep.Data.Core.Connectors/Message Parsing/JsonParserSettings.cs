using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class JsonParserSettings: ParserSettings
    {
        public JsonParserSettings(PropertyBag bag) : base(bag)
        {
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new JsonColumnDefinition(bag);
        }

        public override IParser CreateParser()
        {
            return new JsonParser(this);
        }

        public override string GetDescription()
        {
            return "";
        }
    }
}
