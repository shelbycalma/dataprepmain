using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class JsonColumnDefinition : ColumnDefinition
    {
        private const string JsonPathProperty = "JsonPath";

        public JsonColumnDefinition(PropertyBag bag) : base(bag)
        {
        }

        public string JsonPath
        {
            get { return GetInternal(JsonPathProperty); }
            set { SetInternal(JsonPathProperty, value); }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is JsonColumnDefinition)) return false;

            JsonColumnDefinition xpathColumnDefinition =
                (JsonColumnDefinition)obj;

            return string.Equals(JsonPath, xpathColumnDefinition.JsonPath);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (!string.IsNullOrEmpty(JsonPath))
            {
                code += code * 23 + JsonPath.GetHashCode();
            }
            return code;
        }

        // Check so column name and path have been entered.
        public override bool IsValid
        {
            get {
                return !string.IsNullOrEmpty(Name) 
                    && !string.IsNullOrEmpty(JsonPath);
            }
        }

    }
}
