﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public abstract class ParserPluginBase : IPlugin
    {
        protected PropertyBag settings;
        private bool disposed;

        ~ParserPluginBase()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    // Managed.
                }
                // Unmanaged.
                disposed = true;
            }
        }

        public abstract string Id
        {
            get;
        }

        public virtual bool IsLicensed
        {
            get { return true; }
        }

        public virtual bool IsObsolete
        {
            get { return false; }
        }

        public PropertyBag Settings
        {
            get { return settings; }
        }

        public abstract string Title
        {
            get;
        }
    }
}
