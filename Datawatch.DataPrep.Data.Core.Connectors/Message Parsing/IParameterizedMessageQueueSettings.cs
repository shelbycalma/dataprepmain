﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public interface IParameterizedMessageQueueSettings
    {
        IEnumerable<ParameterValue> Parameters { get; }
    }
}
