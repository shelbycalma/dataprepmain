﻿using System;
using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class TextFileParserSettings : ParserSettings
    {
        private int defaultColumnIndex;

        public TextFileParserSettings(PropertyBag bag) : base(bag)
        {
            defaultColumnIndex = MaxIndex + 1;
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            TextFileColumnDefinition col = new TextFileColumnDefinition(bag);
            if (col.Index < 0)
            {
                col.Index = defaultColumnIndex;
                defaultColumnIndex++;
            }
            return col;
        }

        public override IParser CreateParser()
        {
            TextFileParser parser = new TextFileParser(this);
            parser.SetLineAnalyser(
                new StaticTextAnalyser(new TextAnalyser(this)));
            return parser;
        }

        public override void InitiateNew(bool isMessageQueueSetting)
        {
            if (isMessageQueueSetting)
            {
                IsFirstRowHeading = false;
            }
        }

        public override void InitiateOld(bool isMessageQueueSetting)
        {
            if (isMessageQueueSetting)
            {
                string isFirstRowHeading = GetInternal("IsFirstRowHeading");
                if (isFirstRowHeading == null)
                {
                    // Value not present => old workbook
                    IsFirstRowHeading = false;
                }
            }
        }
        
        public char ColumnDelimiter
        {
            get
            {
                return GetInternal("ColumnDelimiter", ",").ToCharArray()[0];
            }

            set
            {
                SetInternal("ColumnDelimiter", value.ToString());
                OnPropertyChanged("IsCustomColumnDelimiter");
            }
        }

        public char[] ColumnDelimiters
        {
            get
            {
                return DropDownDataProvider.ColumnDelimiters;
            }
        }

        public Visibility IsCustomColumnDelimiter
        {
            get
            {
                if (ColumnDelimiter == DropDownDataProvider.OtherColumnDelimiterValue)
                {
                    return Visibility.Visible;
                }
                return Visibility.Collapsed;
            }
        }

        public string CustomColumnDelimiter
        {
            get
            {
                return GetInternal("CustomColumnDelimiter", "");
            }

            set
            {
                SetInternal("CustomColumnDelimiter", value);
            }
        }

        public char TextQualifier
        {
            get
            {
                return GetInternal("TextQualifier", "N").ToCharArray()[0];
            }

            set
            {
                SetInternal("TextQualifier", value.ToString());
            }
        }

        public char[] TextQualifiers
        {
            get
            {
                return DropDownDataProvider.TextQualifiers;
            }
        }

        public bool IsFirstRowHeading
        {
            get
            {
                string firstRowHeading = GetInternal("IsFirstRowHeading",
                    Convert.ToString(true));
                return Convert.ToBoolean(firstRowHeading);
            }

            set
            {
                SetInternal("IsFirstRowHeading", Convert.ToString(value));
            }
        }

        public override string GetDescription()
        {
            return Properties.Resources.UiParserPluginDescriptionTextFile;
        }

        public int MaxIndex
        {
            get
            {
                int maxSoFar = -1;
                foreach (ColumnDefinition definition in Columns)
                {
                    TextFileColumnDefinition textDefinition = (TextFileColumnDefinition) definition;
                    if (textDefinition.Index > maxSoFar && textDefinition.IsEnabled)
                    {
                        maxSoFar = textDefinition.Index;
                    }
                }
                return maxSoFar;
            }
        }

        public override void AddColumnDefinitions(IEnumerable<ColumnDefinition> columnDefinitions)
        {
            base.AddColumnDefinitions(columnDefinitions);
            int maxIndex = MaxIndex;
            defaultColumnIndex = maxIndex + 1;
        }
    }
}
