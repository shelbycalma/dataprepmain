﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class JsonParser : ParserBase, ISchemaDiscovery
    {
        private readonly IList<string> columnNameList = new List<string>();
        private readonly IList<string> jsonPathList = new List<string>();
        private readonly IList<bool> isTimeColumn = new List<bool>();
        private readonly JsonParserSettings settings;
        public const string Key = "key";
        private int keyIndex = -1;

        public JsonParser(JsonParserSettings settings)
        {
            this.settings = settings;

            int cnt = 0;
            foreach (JsonColumnDefinition columnDefinition in settings.Columns)
            {
                if (!columnDefinition.IsEnabled) continue;

                String jsonPath = ((JsonColumnDefinition)columnDefinition).JsonPath;
                if (Key.Equals(jsonPath))
                {
                    keyIndex = cnt;
                }
                jsonPathList.Add(columnDefinition.JsonPath);
                columnNameList.Add(columnDefinition.Name);
                isTimeColumn.Add(
                    columnDefinition.Type == ColumnType.Time);
            }
        }

        // message = 1 row
        public Dictionary<string, object> Parse(JToken message)
        {
            Dictionary<string, object> dictionary = new Dictionary<string,
                object>();

            if (message == null) return dictionary;

            object[] values = new object[jsonPathList.Count];

            for (int i = 0; i < jsonPathList.Count; i++)
            {
                if (jsonPathList[i] == null || columnNameList[i] == null)
                {
                    break;
                }

                object value = null;
                try
                {
                    JToken token;
                    if (message is JProperty && message.First != null)
                    {
                        token = message.First.SelectToken(jsonPathList[i]);
                    }
                    else
                    {
                        token = message.SelectToken(jsonPathList[i]);
                    }
                    if (token == null)
                    {
                        value = null;
                    }
                    else if (!isTimeColumn[i])
                    {
                        value = (string)token;
                    }
                    else
                    {
                        try
                        {
                            DateTime? dateTime = (DateTime?)token;
                            if (dateTime != null)
                            {
                                value = dateTime.Value;
                            }
                        }
                        catch (System.FormatException ex)
                        {
                            DateTime dateTime;
                            if (DateTime.TryParse(token.ToString(), out dateTime))
                            {
                                value = dateTime;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                    throw new Exception(string.Format(
                        Properties.Resources.ExErrorInvalidJsonPath,
                        columnNameList[i]));
                }
                if (i == keyIndex && message is JProperty)
                {
                    // We are not parsing through an array of rows but instead 
                    // an object list
                    dictionary[columnNameList[i]] = ((JProperty)message).Name;
                    continue;
                }
                values[i] = value;
                dictionary[columnNameList[i]] = value;
            }
            return GetFilteredRecord(values, dictionary, settings.Parameters,
                settings.Columns);
        }

        public JToken FindRecords(string data, string recordsPath)
        {
            using (StringReader stringReader = new StringReader(data))
            {
                return FindRecords(stringReader, recordsPath);
            }
        }

        public JToken FindRecords(Stream inputStream, string recordsPath)
        {
            using (StreamReader streamReader = new StreamReader(inputStream))
            {
                return FindRecords(streamReader, recordsPath);
            }
        }

        public JToken FindRecords(TextReader textReader, string recordPath)
        {
            using (JsonReader reader = new JsonTextReader(textReader))
            {
                // JsonReader implements IDisposable, so do a using block
                // to make sure it's disposed.
                JToken data = JToken.Load(reader);

                //ensure json is in correct format
                string jString = data.ToString();
                jString = jString.Trim();

                // TODO: Why was this code added, doesn't work when I test it?
                //if (!jString.StartsWith("["))
                //    jString = jString.PadLeft(jString.Length + 1, '[');
                //if (!jString.EndsWith("]"))
                //    jString = jString.PadRight(jString.Length + 1, ']');

                data = JToken.Parse(jString);

                JToken jToken = data.SelectToken(recordPath ?? "");
                if (jToken is JArray || jToken is JObject)
                {
                    return jToken;
                }
            }
            return null;
        }

        // message = 1 row
        public Dictionary<string, object> Parse(string message)
        {
            return Parse(JToken.Parse(message));
        }

        public void DiscoverDocumentSchema(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            DiscoverDocumentSchema(message, columnGenerator, null);
        }

        public string[] DiscoverDocumentSchemaAndValues(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            throw new NotImplementedException();
        }

        public void DiscoverDocumentSchema(string message, ColumnGenerator columnGenerator,
            string rootpath)
        {
            if (rootpath == null) rootpath = "";
            foreach (string subMessage in ParseJsonObjectsFromMessage(message))
            {
                JToken parsedMessage = JToken.Parse(subMessage);

                foreach (JToken currToken in parsedMessage)
                {
                    string path = null;
                    JTokenType type;
                    JArray array = null;
                    String value = null;

                    JProperty currProp = currToken as JProperty;

                    if ((currProp == null
                        || currProp.Value.Type == JTokenType.Null)
                        && !(currToken is JValue || currToken is JArray))
                    {
                        break;
                    }
                    else //if (currToken is JValue)
                    {
                        path = currProp == null ? currToken.Path : currProp.Path;
                        type = currProp == null ? currToken.Type : currProp.Value.Type;

                        if (currProp == null)
                        {
                            // Handling nested array here
                            if (currToken is JArray)
                            {
                                array = (JArray)currToken;
                            }
                        }
                        else if (currProp.Value.Type == JTokenType.Array)
                        {
                            array = new JArray(currProp.Value);
                        }
                        else
                        {
                            value = currProp.Value.ToString();
                        }
                    }

                    try
                    {

                        string jsonPath = rootpath + "." + path;

                        // Strip the leading period
                        string name = jsonPath.Substring(1);
                        // Fix for [DDTV-3727] JSON connector does not treat multi word field names correctly
                        //name = name.Replace("['", "").Replace("']", "");
                        name = new Regex(@"\['|'\]").Replace(name, "");
                        // Replace periods, blank space and brackets with underscore
                        name = new Regex(@"[.\s\[\]]+").Replace(name, "_");

                        switch (type)
                        {
                            case JTokenType.Array:
                                DiscoverArraySchema(array, columnGenerator, jsonPath);
                                break;
                            case JTokenType.Date:
                                //create date column
                                // TODO: Find a way to obtain the original string value to find out valid date format
                                UpdateColumn(name, ColumnType.Time, jsonPath);
                                break;
                            case JTokenType.Integer:
                            case JTokenType.Float:
                                //create numeric column
                                UpdateColumn(name, ColumnType.Numeric, jsonPath);
                                break;
                            case JTokenType.String:
                                JsonColumnDefinition cd =
                                    (JsonColumnDefinition)columnGenerator.UpdateColumnDefinitionList(name, value);
                                cd.JsonPath = jsonPath;
                                break;
                            case JTokenType.Boolean:
                                //create text column
                                UpdateColumn(name, ColumnType.Text, jsonPath);
                                break;
                            case JTokenType.Object:
                                DiscoverDocumentSchema(currProp.Value.ToString(), columnGenerator,
                                    jsonPath);
                                break;
                            default:
                                UpdateColumn(name, ColumnType.Text, jsonPath);
                                break;
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error(Properties.Resources.LogColumnDefinitionError);
                    }
                }
            }
        }

        private void UpdateColumn(string columnName, ColumnType columnType, string jsonPath)
        {
            JsonColumnDefinition cd = null;
            foreach (JsonColumnDefinition c in settings.Columns)
            {
                if (c.Name.Equals(columnName))
                {
                    cd = c;
                    break;
                }
            }
            if (cd != null)
            {
                if (cd.Type != columnType)
                {
                    cd.Type = ColumnType.Text;
                }
                return;
            }
            cd = (JsonColumnDefinition)settings.CreateColumnDefinition(new PropertyBag());
            cd.Name = columnName;
            cd.Type = columnType;
            cd.JsonPath = jsonPath;
            settings.AddColumnDefinition(cd);
        }

        //Handles the "Top Level Array" of documents use case, e.g. when the 
        //messagerecieved contains multiple Json objects.
        private IEnumerable<string> ParseJsonObjectsFromMessage(string message)
        {
            message = message.Trim(new[] { ' ' });

            Stack<int> openBraceLocations = new Stack<int>();
            List<string> jsonStrList = new List<string>();

            for (int i = 0; i < message.Length; i++)
            {
                if (message[i] == '{' || message[i] == '[')
                {
                    openBraceLocations.Push(i);
                }
                else if (message[i] == '}' || message[i] == ']')
                {
                    int startIndex = openBraceLocations.Pop();
                    if (openBraceLocations.Count == 0)
                    {
                        int endLength = ((i + 1) - startIndex);

                        string jsonString = message.Substring(startIndex,
                            endLength);

                        jsonStrList.Add(jsonString);
                    }
                }
            }

            return jsonStrList;
        }

        private void DiscoverArraySchema(JArray parsedArr, ColumnGenerator columnGenerator,
            string rootpath = null)
        {
            if (rootpath == null) rootpath = "";

            int arrIndex = 0;
            foreach (JToken currArrToken in parsedArr.Values())
            {
                bool isValue = false;
                ColumnType columnType = ColumnType.Text;

                string jsonPath = rootpath + "[" + arrIndex + "]";
                string name = rootpath + "[" + arrIndex + "]";

                switch (currArrToken.Type)
                {
                    case JTokenType.Array:
                        DiscoverArraySchema(new JArray(currArrToken), columnGenerator,
                            rootpath + "[" + arrIndex + "]");
                        break;
                    case JTokenType.Object:
                        DiscoverDocumentSchema(currArrToken.ToString(), columnGenerator,
                            rootpath + "[" + arrIndex + "]");
                        break;
                    case JTokenType.String:
                        JProperty currProp = currArrToken as JProperty;
                        string value = string.Empty;
                        if (currProp != null)
                        {
                            value = currProp.Value.ToString();
                        }
                        JsonColumnDefinition cd =
                            (JsonColumnDefinition)columnGenerator.UpdateColumnDefinitionList(
                            name, value);
                        cd.JsonPath = jsonPath;
                        break;
                    case JTokenType.Boolean:
                        isValue = true;
                        columnType = ColumnType.Text;
                        break;

                    case JTokenType.Integer:
                    case JTokenType.Float:
                        isValue = true;
                        columnType = ColumnType.Numeric;
                        break;

                    case JTokenType.Date:
                        isValue = true;
                        columnType = ColumnType.Time;
                        break;
                }

                if (isValue)
                {
                    UpdateColumn(name, columnType, jsonPath);
                }

                arrIndex++;
            }
        }

    }
}
