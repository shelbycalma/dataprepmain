using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class XPathParserSettings : ParserSettings
    {
        public XPathParserSettings(PropertyBag bag) : base(bag)
        {
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new XPathColumnDefinition(bag);
        }

        public override IParser CreateParser()
        {
            return new XPathParser(this);
        }

        public override string GetDescription()
        {
            return Properties.Resources.UiParserPluginDescription;
        }

    }
}
