﻿using System.IO;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    /// <summary>
    /// Use this class when need to extract column values out of the first 
    /// # of column found in the string.
    /// </summary>
    public class StaticTextReader
    {
        private readonly StreamReader stream;
        private readonly StaticTextAnalyser analyser;

        public StaticTextReader(StreamReader stream, TextAnalyser textAnalyser)
        {
            this.stream = stream;
            this.analyser = new StaticTextAnalyser(textAnalyser);
        }

        public bool IsEof()
        {
            return stream.EndOfStream;
        }

        public int ReadNextLine(string[] row)
        {
            string line = stream.ReadLine();

            if (line == null || line.Length==0) return 0;

            return analyser.GetColumnValues(line, row);
        }
    }
}
