namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class StaticTextAnalyser
    {
        private readonly TextAnalyser analyser;
        private bool passNewLine;

        /// <summary>
        /// Use this class when not in DataDiscovery mode and already discovered the rows. 
        /// If in DataDiscovery mode, then use the DynamicTextAnalyser.
        /// </summary>
        /// <param name="analyser"></param>
        public StaticTextAnalyser(TextAnalyser analyser)
        {
            this.analyser = analyser;
        }

        // Fill the row with read items and return the number of items read
        // If no column values was found, then 0 will be returned.
        public int GetColumnValues(string message, object[] row)
        {
            int currRowPos = 0;

            analyser.InitAnalyseOfNewRow(message);
            if (passNewLine)
            {
                // The message is expected to consist of two rows. 
                // First row is a header row and the second row is the actual data.
                // Skip the first row, the "header row".
                for (int i = 0; i < message.Length; i++)
                {
                    char c = message[i];
                    if (c == '\n')
                    {
                        analyser.CharPointer = i + 1;
                        break;
                    }
                    if (c == '\r')
                    {
                        if (i + 1 < message.Length && message[i + 1] == '\n')
                        {
                            analyser.CharPointer = i + 2;
                            break;
                        }
                        analyser.CharPointer = i + 1;
                        break;
                    }
                }
            }
            string cell = analyser.GetNextItem();

            if (cell.Length == 0 && analyser.IsEol())
            {
                // Empty row
                return 0;
            }

            row[currRowPos++] = cell;

            while (currRowPos < row.Length && !analyser.IsEol())
            {
                cell = analyser.GetNextItem();

                row[currRowPos++] = cell;
            }
            return currRowPos;
        }

        /// <summary>
        /// Set this to true if we want the strip away the first row in the message.
        /// Will sometimes occure for streaming plugins where each string message arriving 
        /// containing a two row (1st row is header row and second is the value row).
        /// </summary>
        public bool PassNewLine
        {
            set { passNewLine = value; }
        }
    }
}
