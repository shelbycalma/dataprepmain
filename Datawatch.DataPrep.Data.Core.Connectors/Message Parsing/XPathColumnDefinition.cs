using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class XPathColumnDefinition : ColumnDefinition
    {
        public XPathColumnDefinition(PropertyBag bag) : base(bag)
        {
        }

        public string XPath
        {
            get { return GetInternal("XPath"); }
            set { SetInternal("XPath", value);}
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is XPathColumnDefinition)) return false;

            XPathColumnDefinition xpathColumnDefinition = 
                (XPathColumnDefinition)obj;

            return string.Equals(XPath, xpathColumnDefinition.XPath);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (!string.IsNullOrEmpty(XPath))
            {
                code += code * 23 + XPath.GetHashCode();
            }
            return code;
        }

        // Check so column name and path have been entered.
        public override bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Name) 
                    && !string.IsNullOrEmpty(XPath);
            }
        }

    }
}
