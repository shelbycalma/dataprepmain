using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class XPathPlugin : ParserPluginBase, IParserPlugin
    {
        private const string Name = "Xml";

        public Type GetParserType()
        {
            return typeof(XPathParser);
        }

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new XPathParserSettings(bag);
        }

        public override string Id
        {
            get { return Name; }
        }

        public override string Title
        {
            get { return Name; }
        }

    }
}
