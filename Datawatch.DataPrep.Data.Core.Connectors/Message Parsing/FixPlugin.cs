using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class FixPlugin : ParserPluginBase, IParserPlugin
    {
        private const string Name = "Fix";

        public Type GetParserType()
        {
            return typeof(FixParser);
        }

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new FixParserSettings(bag);
        }

        public override string Id
        {
            get { return Name; }
        }

        public override string Title
        {
            get { return Name; }
        }
    }
}
