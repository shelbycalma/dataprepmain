﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.XPath;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    // Class made simular to Java side to take care of namespaces in XML
    sealed class NamespaceResolver : XmlNamespaceManager
    {
        public NamespaceResolver(XmlDocument xmlDoc)
            : base(xmlDoc.NameTable)
        {
            XmlElement xmlElem = xmlDoc.DocumentElement;

            if (xmlElem == null) return;

            //Initilize the namespace manager
            XPathDocument xpathDoc = new XPathDocument(
                new XmlNodeReader(xmlElem));
            XPathNavigator nav = xpathDoc.CreateNavigator();
            nav.MoveToFollowing(XPathNodeType.Element);
            IDictionary<string, string> dictAllNamespaces =
                nav.GetNamespacesInScope(XmlNamespaceScope.All);

            foreach (KeyValuePair<string, string> xns in dictAllNamespaces)
            {
                if (xns.Key == String.Empty)
                {
                    AddNamespace("default", xns.Value);
                }
                else
                {
                    AddNamespace(xns.Key, xns.Value);
                }
            }
        }
    }
}
