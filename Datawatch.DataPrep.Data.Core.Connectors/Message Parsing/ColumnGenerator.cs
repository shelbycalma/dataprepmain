﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Properties;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class ColumnGenerator
    {
        private readonly ParserSettings parserSettings;
        private readonly ISchemaDiscovery schemaDiscoverySettings;
        private readonly NumericDataHelper numericDataHelper;
        private readonly Dictionary<string, string[]> oldWorkingDateFormats;
        private readonly IParser parser;
        private bool isStreaming;
        private bool suggestDateFormat;
        public static string[] DateTimeFormats = new string[]
            {
                FormatHelper.GetDefaultTimeFormat(),
                "yyyy-MM-dd",
                "dd/MMM/yyyy",
                "MMM/yyyy",
                "hh:mm tt",
                "hh:mm:ss tt",
                "hh:mm:ss.fff tt",
                "HH:mm",
                "HH:mm:ss",
                "HH:mm:ss.fff",
                "dd/MM/yyyy HH:mm:ss",
                "MM/dd/yyyy HH:mm:ss",
                "M/d/yyyy h:mm:ss tt",
                "yyyy-MM-dd HH:mm:ss",
                "yyyy-MM-dd'T'HH:mm:ss",
                "yyyy-MM-dd'T'HH:mm:ss.fff'Z'",
                "yyyy-MM-dd'T'HH:mm:ssK",
                "yyyy-MM-dd'T'HH:mm:ss'Z'",
                "yyyyMMdd'T'HHmmss'Z'",
                "yyyy-MM-dd hh:mm:ss tt",
                DateParser.Posix
            };

        public ColumnGenerator(ParserSettings parSettings,
            NumericDataHelper numericDataHelper)
            : this(parSettings, numericDataHelper, false, true)
        {
        }

        public ColumnGenerator(ParserSettings parSettings,
            NumericDataHelper numericDataHelper,
            bool isStreaming, bool suggestDateFormat)
        {
            parserSettings = parSettings;
            this.numericDataHelper = numericDataHelper;
            this.isStreaming = isStreaming;
            this.suggestDateFormat = suggestDateFormat;
            parserSettings.ClearColumnDefinitions();
            parser = parserSettings.CreateParser();
            if (!(parser is ISchemaDiscovery))
            {
                throw new Exception(
                    Resources.ExSchemaDiscoveryUnsupported);
            }
            schemaDiscoverySettings = parser as ISchemaDiscovery;
            oldWorkingDateFormats = new Dictionary<string, string[]>();
        }

        public void Generate(string message)
        {
            schemaDiscoverySettings.DiscoverDocumentSchema(message, this,
                isStreaming);
        }

        public string[] GenerateAndObtainValues(string message)
        {
            return schemaDiscoverySettings.DiscoverDocumentSchemaAndValues(message, this,
                isStreaming);
        }

        // Returns the current effected ColumnDefinition
        public ColumnDefinition UpdateColumnDefinitionList(
            string columnName, string columnValue)
        {
            if (suggestDateFormat)
            {
                return UpdateListDateFormat(columnName, columnValue);
            }
            return UpdateListSimple(columnName, columnValue);
        }

        // Returns the current effected ColumnDefinition
        public ColumnDefinition UpdateListDateFormat(
            string columnName, string columnValue)
        {
            ColumnDefinition column = GetColumnDefinition(columnName);
            if (column != null && string.IsNullOrEmpty(columnValue))
            {
                // If value is empty and the column was already analysed 
                // before => keep the old type
                return column;
            }
            if (column != null && column.Type == ColumnType.Text)
            {
                // Already added as a Text column definition => 
                // that wont be changed => We are done!
                return column;
            }
            // Evaluate type
            ColumnType columnType = ColumnType.Text;
            string dateFormat = null;
            string[] dateFormatsWorking = null;

            oldWorkingDateFormats.TryGetValue(columnName,
                out dateFormatsWorking);
            if (dateFormatsWorking == null)
            {
                // Not rekognized as a Time column yet => Set a full list 
                // to check
                dateFormatsWorking = DateTimeFormats;
            }

            // Check what type the column value is
            if (numericDataHelper.IsNumber(columnValue))
            {
                columnType = ColumnType.Numeric;
            }
            else
            {
                dateFormatsWorking = TryParseWithFormats(dateFormatsWorking,
                    columnValue);
                if (dateFormatsWorking.Length > 0)
                {
                    columnType = ColumnType.Time;
                    dateFormat = dateFormatsWorking[0];
                }
            }

            if (column == null)
            {
                // Column has not been added yet
                column = parserSettings.CreateColumnDefinition(new PropertyBag());
                column.Name = columnName;
                column.Type = columnType;
                if (columnType == ColumnType.Time)
                {
                    column.DateFormat = dateFormat;
                    if (oldWorkingDateFormats.ContainsKey(columnName))
                    {
                        oldWorkingDateFormats.Remove(columnName);
                    }
                    oldWorkingDateFormats.Add(columnName, dateFormatsWorking);
                }
                parserSettings.AddColumnDefinition(column);
                return column;
            }
            // Check the already added column type against the type the current 
            // column value have
            if (column.Type == ColumnType.Numeric && column.Type == columnType)
            {
                return column;
            }
            if (column.Type == ColumnType.Time &&
                column.Type == columnType)
            {
                column.DateFormat = dateFormat;
                if (oldWorkingDateFormats.ContainsKey(columnName))
                {
                    oldWorkingDateFormats.Remove(columnName);
                }
                oldWorkingDateFormats.Add(column.Name, dateFormatsWorking);
                return column;
            }
            column.Type = ColumnType.Text;
            if (oldWorkingDateFormats.ContainsKey(columnName))
            {
                oldWorkingDateFormats.Remove(columnName);
            }
            return column;
        }

        private ColumnDefinition GetColumnDefinition(string columnName)
        {
            // Check if the column have already been added
            foreach (ColumnDefinition cd in parserSettings.Columns)
            {
                if (cd.Name.Equals(columnName))
                {
                    return cd;
                }
            }
            return null;
        }

        // Returns the current effected ColumnDefinition
        public ColumnDefinition UpdateListSimple(string columnName,
            string columnValue)
        {
            ColumnDefinition column = GetColumnDefinition(columnName);
            if (column != null && column.Type == ColumnType.Text)
            {
                // Already added as a Text column definition => 
                // that wont be changed => We are done!
                return column;
            }

            // Evaluate type
            ColumnType columnType = ColumnType.Text;

            // Check what type the column value is
            if (numericDataHelper.IsNumber(columnValue))
            {
                columnType = ColumnType.Numeric;
            }
            else
            {
                DateTime dt;
                if (DateTime.TryParse(columnValue, out dt))
                {
                    columnType = ColumnType.Time;
                }
            }

            if (column == null)
            {
                // Column has not been added yet
                column = parserSettings.CreateColumnDefinition(new PropertyBag());
                column.Name = columnName;
                column.Type = columnType;
                parserSettings.AddColumnDefinition(column);
                return column;
            }
            // Check the already added column type against the type the current 
            // column value have
            if (column.Type == ColumnType.Numeric && column.Type == columnType)
            {
                return column;
            }
            if (column.Type == ColumnType.Time && column.Type == columnType)
            {
                return column;
            }
            column.Type = ColumnType.Text;
            return column;
        }

        public static string[] TryParseWithFormats(string[] formats, string value)
        {
            List<string> successfulFormats = new List<string>();
            foreach (string format in formats)
            {
                if (DateParser.Posix.Equals(format)) continue;

                DateTime dateTime;
                if (DateTime.TryParseExact(value, format,
                    CultureInfo.InvariantCulture, DateTimeStyles.None,
                    out dateTime))
                {
                    successfulFormats.Add(format);
                }
            }
            return successfulFormats.ToArray();
        }

        public IParser Parser
        {
            get
            {
                return parser;
            }
        }
    }
}
