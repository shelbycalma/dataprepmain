﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class TextFileColumnDefinition : ColumnDefinition
    {
        private const string IndexProperty = "Index";

        public TextFileColumnDefinition(PropertyBag bag) : base(bag)
        {
        }

        public int Index
        {
            get { return GetInternalInt(IndexProperty, -1); }
            set { SetInternalInt(IndexProperty, value); }
        }

        public override bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Name)
                  && Index >= 0;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TextFileColumnDefinition)) return false;

            if (Index != ((TextFileColumnDefinition)obj).Index)
            {
                return false;
            }

            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            hashCode ^= Index.GetHashCode();

            return hashCode;
        }
    }
}
