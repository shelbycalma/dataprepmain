using System.Text;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class TextAnalyser
    {
        private readonly char columnDelimiter;
        private readonly char textQualifier;
        private readonly bool isQuoted;
        private string row;
        private int charPointer;
        StringBuilder item = new StringBuilder();
        private bool eol;

        public TextAnalyser(TextFileParserSettings parserSettings)
        {
            columnDelimiter = GetColumnDelimiter(parserSettings);
            textQualifier = parserSettings.TextQualifier;
            isQuoted = parserSettings.TextQualifier !=
                DropDownDataProvider.NoneTextQualifierValue;
        }

        private char GetColumnDelimiter(TextFileParserSettings parserSettings)
        {
            char retValue;
            if (parserSettings.ColumnDelimiter ==
                DropDownDataProvider.OtherColumnDelimiterValue)
            {
                retValue = parserSettings.CustomColumnDelimiter.ToCharArray()[0];
            }
            else
            {
                retValue = parserSettings.ColumnDelimiter;
            }

            return retValue;
        }

        /// <summary>
        /// Call always this method to start analysing a new row. Then call
        /// GetNextItem to extract one column value at a time.
        /// </summary>
        /// <param name="line"></param>
        public void InitAnalyseOfNewRow(string line)
        {
            row = line;
            charPointer = 0;
            eol = false;
        }

        public int CharPointer
        {
            set { charPointer = value; }
        }

        ///// <summary>
        ///// Method should return the follwoing:
        ///// string.Empty
        ///// </summary>
        ///// <returns></returns>
        //public string GetFirstItem()
        //{
        //    if (charPointer >= row.Length)
        //    {
        //        return null;
        //    }

        //    item.Length = 0;

        //    while (true)
        //    {
        //        char c = row[charPointer++];

                
        //    }
        //}

        public bool IsEol()
        {
            return eol;
        }

        /// <summary>
        /// Returns the following in the following situations. Pipe shows what character 
        /// that is in turn to read for the row string.
        /// CASE 1:
        /// a,b,c
        ///     |       => set eol=true, return "c"
        /// CASE 2:
        /// a,b, 
        ///     |       => set eol=true, return string.Empty
        /// CASE 3:
        /// ,b,c
        /// |           => return string.Empty
        /// CASE 4:
        /// ,
        /// |           => return string.Empty
        /// CASE 5:
        /// "" (empty string)
        /// |           => return string.Empty
        /// CASE 6:
        /// a,b,c
        /// |           => return "a"
        /// </summary>
        /// <returns></returns>
        public string GetNextItem()
        {
            //   outCharPointer = inCharPointer;
            if (charPointer >= row.Length)
            {
                eol = true;
                return string.Empty;
            }

            bool quoted = false;
            bool predata = true;
            bool postdata = false;

            item.Length = 0;

            while (true)
            {
                char c = row[charPointer++];//GetNextChar(true);
                if (charPointer >= row.Length)
                {
                    if (c != columnDelimiter)
                    {
                        if (c != textQualifier || !quoted)
                        {
                            item.Append(c);
                        }
                        eol = true;
                    }
                    return item.Length > 0 ? item.ToString() : string.Empty;
                }

                if ((postdata || !quoted) && c == columnDelimiter)
                {
                    return item.ToString();
                }

                if ((predata || postdata || !quoted) && (c == '\n' || c == '\r'))
                {
                    if (c == '\r' && row[charPointer] == '\n')
                    {
                        charPointer++;
                    }

                    return item.ToString();
                }

                if (predata && c == ' ')
                {
                    continue;
                }

                //check if TextQualifier is not none
                if (isQuoted)
                {
                    if (predata && c == textQualifier)
                    {
                        quoted = true;
                        predata = false;
                        continue;
                    }
                }

                if (predata)
                {
                    predata = false;
                    item.Append(c);
                    continue;
                }

                if (c == textQualifier && quoted)
                {
                    if (row[charPointer] == textQualifier)
                    {
                        item.Append(row[charPointer++]);
                    }
                    else
                    {
                        postdata = true;
                    }
                    continue;
                }

                item.Append(c);
            }
        }

    }
}
