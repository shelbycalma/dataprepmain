using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class FixParser : ISchemaDiscovery
    {
        private readonly FixParserSettings settings;
        private readonly Dictionary<string, HashSet<string>> columnNameLookup =
            new Dictionary<string, HashSet<string>>();

        public FixParser(FixParserSettings parserSettings)
        {
            settings = parserSettings;

            foreach (FixColumnDefinition fixColumnDefinition in settings.Columns)
            {
                if (!fixColumnDefinition.IsEnabled) continue;

                HashSet<string> names;
                if (columnNameLookup.ContainsKey(fixColumnDefinition.FixTag))
                {
                    names = columnNameLookup[fixColumnDefinition.FixTag];
                }
                else
                {
                    names = new HashSet<string>();
                    columnNameLookup[fixColumnDefinition.FixTag] = names;
                }
                names.Add(fixColumnDefinition.Name);
            }
        }

        public Dictionary<string, object> Parse(string message)
        {
            return ParseFixData(message);
        }

        private Dictionary<string, object> ParseFixData(string fixdata)
        {
            Dictionary<string, object> dictionary =
                new Dictionary<string, object>();
            Dictionary<string, string> fix = FixSplitter.Split(fixdata);
            foreach (string fixtag in fix.Keys)
            {
                if (!columnNameLookup.ContainsKey(fixtag)) continue;
                string str = fix[fixtag];
                foreach (string name in columnNameLookup[fixtag])
                {
                    dictionary[name] = str;
                }
            }
            return dictionary;
        }

        public void DiscoverDocumentSchema(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            Dictionary<string, string> fix = FixSplitter.Split(message);

            foreach (string columnName in fix.Keys)
            {
                string value = fix[columnName];

                try
                {
                    FixColumnDefinition colDef =
                        (FixColumnDefinition)columnGenerator.UpdateColumnDefinitionList(
                        columnName, value);

                    colDef.FixTag = columnName;
                }
                catch (Exception e)
                {
                    Log.Error(Properties.Resources.LogColumnDefinitionError);
                }
            }
        }

        public string[] DiscoverDocumentSchemaAndValues(string message, ColumnGenerator columnGenerator,
            bool isStreaming)
        {
            throw new NotImplementedException();
        }
    }
}
