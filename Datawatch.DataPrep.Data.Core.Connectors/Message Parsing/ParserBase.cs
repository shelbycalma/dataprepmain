﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Datawatch.DataPrep.Data.Core.MessageParsing
{
    public class ParserBase
    {
        protected bool filterCreated = false;
        private ParameterFilter parameterFilter = null;

        protected Dictionary<string, object> GetFilteredRecord(object[] values,
            Dictionary<string, object> dict,
            IEnumerable<ParameterValue> parameters, 
            IEnumerable<ColumnDefinition> columns)
        {
            if (filterCreated == false)
            {
                //TODO: check if this needs to be created on every message received.
                parameterFilter = ParameterFilter.CreateFilter(columns, parameters);
                filterCreated = true;
            }
            if (parameterFilter != null && !parameterFilter.Contains(values))
            {
                return new Dictionary<string, object>();
            }
            return dict;
        }        
    }
}
