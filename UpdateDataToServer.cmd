@echo off
echo.
echo Updates Data solution to use prerelease version of Framework package from "develop" branch using Datawatch package source.
echo.

call ph.cmd Update Datawatch -P ddf -T DD -PR .*develop