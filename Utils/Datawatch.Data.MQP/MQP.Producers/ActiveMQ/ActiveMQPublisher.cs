﻿using System;
using Apache.NMS.ActiveMQ;
using Apache.NMS;
using Datawatch.Data.MQP.ProducerBase;

namespace Datawatch.Data.MQP.Producers.ActiveMQ
{
    public class ActiveMQPublisher : Publisher
    {
        public readonly static string Name = "ActiveMQ";
        private const string DefaultServerCommand = "C:\\activeMQ_NEW\\apache-activemq-5.4.3\\bin\\activemq.bat";
        private const string DefaultServerParmaters = "";
        private string serverCommand;
        private string serverParamaters;
        private string host = "tcp://localhost";
        private string port = "61616";
        public string topic = "pano";
        private IConnection connection;
        private IConnectionFactory connectionFactory;
        private ISession session;

        public override string GetName()
        {
            return Name;
        }

        public string Topic
        {
            get { return topic; }
            set { topic = value; }
        }

        public override string Host
        {
            get { return host; }
            set { host = value; }
        }

        public override string Port
        {
            get { return port; }
            set { port = value; }
        }

        public override bool StartConnection()
        {
            try
            {
                connectionFactory = new ConnectionFactory(host + ":" + port);
                connection = connectionFactory.CreateConnection();
                connection.Start();
                session = connection.CreateSession();
            } catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public override void StopConnection()
        {
            if (session != null)
            {
                try
                {
                    session.Close();
                    session.Dispose();
                    connection.Stop();
                    connection.Dispose();
                }
                catch (Exception)
                {
                }
            }
        }

        public override string Send(string message, string field)
        {
            string sentAsTopic = Topic + "." + field.Trim();
            var publisher = new TopicPublisher(session, sentAsTopic);
            publisher.SendMessage(@message);
            publisher.Dispose();
            return sentAsTopic;
        }

        public override string GetInformation()
        {
            return "If topic name \"" + "pano" + "\". Then use something like \"" + Topic + ".>\" in the workbook.";
        }

        public override string GetDefaultServerCommand()
        {
            return DefaultServerCommand;
        }

        public override string GetDefaultServerParameters()
        {
            return DefaultServerParmaters;
        }

        public override string ServerCommand
        {
            get
            {
                if (serverCommand == null)
                {
                    serverCommand = DefaultServerCommand;
                }
                return serverCommand;
            }
            set
            {
                serverCommand = value;
            }
        }

        public override string ServerParameters
        {
            get
            {
                if (serverParamaters == null)
                {
                    serverParamaters = DefaultServerParmaters;
                }
                return serverParamaters;
            }
            set { serverParamaters = value; }
        }

    }
}
