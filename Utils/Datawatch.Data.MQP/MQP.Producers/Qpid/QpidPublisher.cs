﻿using System;
using System.Text;
using org.apache.qpid.client;
using Datawatch.Data.MQP.ProducerBase;

namespace Datawatch.Data.MQP.Producers.Qpid
{
    public class QpidPublisher : Publisher
    {
        private string DefaultServerCommand = "C:\\Program Files\\Apache\\qpidc-0.6\\bin\\qpidd.exe";
        private const string DefaultServerParameters = "--auth no";
        private string serverCommand;
        private string serverParameters;
        private string host = "localhost";
        private string port = "5672";
        private string exchange = "amq.topic";
        private string routingKey = "routing_key";
        private string queue = "message_queue";
        private Client client = new Client();
        private IMessage qpidMessage;
        private IClientSession session;

        public override string GetName()
        {
            return "Qpid";
        }

        public override string Host
        {
            get { return host; }
            set { host = value; }
        }

        public override string Port
        {
            get { return port; }
            set { port = value; }
        }

        public override bool StartConnection()
        {
            try
            {
                int iPort = Convert.ToInt32(port);
                client.Connect(host, iPort, "", "", "");
                session = client.CreateSession(50000L);
                session.QueueDeclare(queue);
                session.ExchangeBind(queue, exchange, routingKey);
                qpidMessage = new Message();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public override void StopConnection()
        {
            try
            {
                qpidMessage.ClearData();
                session.Sync();
                client.Close();
            }
            catch { }
        }

        public override string Send(string message, string field)
        {
            qpidMessage.ClearData();
            qpidMessage.AppendData(Encoding.UTF8.GetBytes(message));
            session.MessageTransfer(exchange, routingKey, qpidMessage);
            return "";
        }

        public override string GetInformation()
        {
            return "Exchange = \"amq.topic\", RoutingKey = \"routing_key\".";
        }

        public override string GetDefaultServerCommand()
        {
            return DefaultServerCommand;
        }

        public override string ServerCommand
        {
            get
            {
                if (serverCommand == null)
                {
                    serverCommand = DefaultServerCommand;
                }
                return serverCommand;
            }
            set { serverCommand = value; }
        }

        public override string ServerParameters
        {
            get
            {
                if (serverParameters == null)
                {
                    serverParameters = DefaultServerParameters;
                }
                return serverParameters;
            }
            set { serverParameters = value; }
        }

        public override string GetDefaultServerParameters()
        {
            return DefaultServerParameters;
        }
    }
}
