﻿namespace Datawatch.Data.MQP.ProducerBase
{
    public enum DataFormat
    {
        Name,
        RowStart,
        Assign,
        PairSep, // PairSeprator
        VariableApostrophe,
        ValueApostrophe,
        RowEnd,
        HeaderType
    }
}
