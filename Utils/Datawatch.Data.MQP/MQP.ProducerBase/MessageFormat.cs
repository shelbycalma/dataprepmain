﻿using System.Collections.Generic;
using System.IO;

namespace Datawatch.Data.MQP.ProducerBase
{
    public class MessageFormat
    {
        // Column Separators
        public const string Other = "Other";
        public const string Comma = "Comma {,}";
        public const string SemiColon = "Semi Colon {;}";
        public const string Pipe = "Pipe {|}";
        public const string Space = "Space";
        public const string Tab = "Tab {t}";
        // Text Qualifiers
        public const string None = "<none>";
        public const string DoubleQuotes = "Double Quotes {\"\"}";
        public const string SingleQuotes = "Single Quotes {\'\'}";
        private const string UserDefinitions = "userDefinition.txt";
        private const string NumHeader = "num";     // Used when header should be pure numeric
        private const string TextHeader = "txt";    // Used when header is text
        // Message Types
        private const string XPath = "Xml";
        private const string Json = "Json";
        private const string Fix = "Fix";
        public const string Csv = "Text";
        private const char Sep = (char)1;
        private static readonly string[][] messageFormat = new string[][]
                    {
                        new string[] {XPath, "<tuple ",         "=",   " ",    "",   "'",  " />",  TextHeader},
                        new string[] {Json,  "{ \"tuple\" : {", " : ", ", ",   "\"", "\"", " }}",  TextHeader},
                        new string[] {Fix,  "",                 "=",   Sep+"", "",   "",   Sep+"", NumHeader},
                        new string[] {Csv,  null }, 
                    };

        public static string[] GetMessageTypes()
        {
            if (!File.Exists(UserDefinitions))
            {
                File.Create(UserDefinitions);
            }
            StreamReader file = new StreamReader(UserDefinitions);

            List<string> list = new List<string>();
            foreach (string[] format in messageFormat)
            {
                list.Add(format[(int)DataFormat.Name]);
            }
            return list.ToArray();
        }

        public static string[] GetTextQualifiers
        {
            get { return new string[] {None, DoubleQuotes, SingleQuotes}; }
        }

        public static string[] GetColumnSeparators
        {
            get { return new string[] { Comma, SemiColon, Pipe, Space, Tab, Other }; }
        }

        // Find the index for selected messageType
        public static int GetIndex(string publisherName)
        {
            //            string selectedItem = comboMessageType.SelectedItem.ToString();
            for (int i = 0; i < messageFormat.Length; i++)
            {
                if (messageFormat[i][(int)DataFormat.Name].Equals(publisherName))
                {
                    return i;
                }
            }
            return -1;
        }

        public static string Get(int publisherId, DataFormat wantedFormat)
        {
            return messageFormat[publisherId][(int) wantedFormat];
        }

        public static bool UseTextHeader(int publisherIndex)
        {
            return messageFormat[publisherIndex][(int)DataFormat.HeaderType] == TextHeader;
        }
    }
}
