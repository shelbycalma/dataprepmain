﻿using System;
using System.Diagnostics;

namespace Datawatch.Data.MQP.ProducerBase
{
    public abstract class Publisher
    {
        public abstract string GetName();
        public abstract string Host { get; set; }
        public abstract string Port { get; set; }
        public abstract bool StartConnection();
        public abstract void StopConnection();
        public abstract string Send(string message, string extraInfo);
        public abstract string GetInformation();
        public abstract string GetDefaultServerCommand();
        public abstract string GetDefaultServerParameters();
        public abstract string ServerCommand { get; set; }
        public abstract string ServerParameters { get; set; }

        public void StartServer()
        {
            Process p = new Process();
            string command = ServerCommand.Trim();
            string parameters = ServerParameters.Trim();
            
            if (string.IsNullOrEmpty(command)) return;
            
            p.StartInfo.FileName = command;
            if (!string.IsNullOrEmpty(parameters))
            {
                p.StartInfo.Arguments = parameters;
            }
            try
            {
                p.Start();
            } catch (Exception e)
            {
                //MessageBox.Show(Resources.UiCannotStartServer,
                //Resources.UiTitleCannotStartServer,
                //MessageBoxButtons.OK,
                //MessageBoxIcon.Asterisk);
            }
        }
    }
}
