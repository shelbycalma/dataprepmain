﻿namespace MQP
{
    partial class Selector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.publisherMQ_comboBox = new System.Windows.Forms.ComboBox();
            this.labelHost = new System.Windows.Forms.Label();
            this.textBoxHost = new System.Windows.Forms.TextBox();
            this.labelPort = new System.Windows.Forms.Label();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.labelFilePath = new System.Windows.Forms.Label();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.labelMessPerSec = new System.Windows.Forms.Label();
            this.textBoxMessPerSec = new System.Windows.Forms.TextBox();
            this.labelMessageType = new System.Windows.Forms.Label();
            this.comboBoxMessageType = new System.Windows.Forms.ComboBox();
            this.textBoxSentMessages = new System.Windows.Forms.TextBox();
            this.labelMessagesSent = new System.Windows.Forms.Label();
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelInformation = new System.Windows.Forms.Label();
            this.labelInformationText = new System.Windows.Forms.Label();
            this.checkBoxRepeat = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonStartServer = new System.Windows.Forms.Button();
            this.textBoxServerCommand = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxServerParameters = new System.Windows.Forms.TextBox();
            this.topic_Label = new System.Windows.Forms.Label();
            this.topic_textBox = new System.Windows.Forms.TextBox();
            this.keyFieldLabel = new System.Windows.Forms.Label();
            this.keyFieldTextBox = new System.Windows.Forms.TextBox();
            this.AddHeader_checkbox = new System.Windows.Forms.CheckBox();
            this.columnSeparator_label = new System.Windows.Forms.Label();
            this.textQualifier_label = new System.Windows.Forms.Label();
            this.textQualifier_ComboBox = new System.Windows.Forms.ComboBox();
            this.columnSeparator_comboBox = new System.Windows.Forms.ComboBox();
            this.otherSeparator_label = new System.Windows.Forms.Label();
            this.otherSeparator_textBox = new System.Windows.Forms.TextBox();
            this.sentAsTopic_label = new System.Windows.Forms.Label();
            this.sentAsTopic_textBox = new System.Windows.Forms.TextBox();
            this.chkFirstRowHeading = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Publisher";
            // 
            // publisherMQ_comboBox
            // 
            this.publisherMQ_comboBox.FormattingEnabled = true;
            this.publisherMQ_comboBox.Location = new System.Drawing.Point(104, 24);
            this.publisherMQ_comboBox.Name = "publisherMQ_comboBox";
            this.publisherMQ_comboBox.Size = new System.Drawing.Size(121, 21);
            this.publisherMQ_comboBox.TabIndex = 1;
            // 
            // labelHost
            // 
            this.labelHost.AutoSize = true;
            this.labelHost.Location = new System.Drawing.Point(12, 184);
            this.labelHost.Name = "labelHost";
            this.labelHost.Size = new System.Drawing.Size(29, 13);
            this.labelHost.TabIndex = 2;
            this.labelHost.Text = "Host";
            // 
            // textBoxHost
            // 
            this.textBoxHost.Location = new System.Drawing.Point(92, 184);
            this.textBoxHost.Name = "textBoxHost";
            this.textBoxHost.Size = new System.Drawing.Size(86, 20);
            this.textBoxHost.TabIndex = 3;
            // 
            // labelPort
            // 
            this.labelPort.AutoSize = true;
            this.labelPort.Location = new System.Drawing.Point(193, 189);
            this.labelPort.Name = "labelPort";
            this.labelPort.Size = new System.Drawing.Size(26, 13);
            this.labelPort.TabIndex = 4;
            this.labelPort.Text = "Port";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(225, 185);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(75, 20);
            this.textBoxPort.TabIndex = 5;
            // 
            // labelFilePath
            // 
            this.labelFilePath.AutoSize = true;
            this.labelFilePath.Location = new System.Drawing.Point(193, 147);
            this.labelFilePath.Name = "labelFilePath";
            this.labelFilePath.Size = new System.Drawing.Size(48, 13);
            this.labelFilePath.TabIndex = 6;
            this.labelFilePath.Text = "File Path";
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Location = new System.Drawing.Point(247, 143);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.Size = new System.Drawing.Size(263, 20);
            this.textBoxFilePath.TabIndex = 7;
            this.textBoxFilePath.Text = "C:\\aleri_demo\\apps\\UKTrading\\History.csv";
            this.textBoxFilePath.TextChanged += new System.EventHandler(this.textBoxFilePath_TextChanged);
            // 
            // labelMessPerSec
            // 
            this.labelMessPerSec.AutoSize = true;
            this.labelMessPerSec.Location = new System.Drawing.Point(516, 148);
            this.labelMessPerSec.Name = "labelMessPerSec";
            this.labelMessPerSec.Size = new System.Drawing.Size(85, 13);
            this.labelMessPerSec.TabIndex = 8;
            this.labelMessPerSec.Text = "Messages / Sec";
            // 
            // textBoxMessPerSec
            // 
            this.textBoxMessPerSec.Location = new System.Drawing.Point(607, 144);
            this.textBoxMessPerSec.Name = "textBoxMessPerSec";
            this.textBoxMessPerSec.Size = new System.Drawing.Size(89, 20);
            this.textBoxMessPerSec.TabIndex = 9;
            this.textBoxMessPerSec.Text = "10";
            // 
            // labelMessageType
            // 
            this.labelMessageType.AutoSize = true;
            this.labelMessageType.Location = new System.Drawing.Point(12, 146);
            this.labelMessageType.Name = "labelMessageType";
            this.labelMessageType.Size = new System.Drawing.Size(74, 13);
            this.labelMessageType.TabIndex = 10;
            this.labelMessageType.Text = "MessageType";
            // 
            // comboBoxMessageType
            // 
            this.comboBoxMessageType.FormattingEnabled = true;
            this.comboBoxMessageType.Location = new System.Drawing.Point(92, 143);
            this.comboBoxMessageType.Name = "comboBoxMessageType";
            this.comboBoxMessageType.Size = new System.Drawing.Size(86, 21);
            this.comboBoxMessageType.TabIndex = 11;
            // 
            // textBoxSentMessages
            // 
            this.textBoxSentMessages.Location = new System.Drawing.Point(144, 366);
            this.textBoxSentMessages.Name = "textBoxSentMessages";
            this.textBoxSentMessages.Size = new System.Drawing.Size(552, 20);
            this.textBoxSentMessages.TabIndex = 12;
            // 
            // labelMessagesSent
            // 
            this.labelMessagesSent.AutoSize = true;
            this.labelMessagesSent.Location = new System.Drawing.Point(141, 350);
            this.labelMessagesSent.Name = "labelMessagesSent";
            this.labelMessagesSent.Size = new System.Drawing.Size(76, 13);
            this.labelMessagesSent.TabIndex = 13;
            this.labelMessagesSent.Text = "Message sent:";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(144, 401);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 14;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(486, 401);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 15;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // labelInformation
            // 
            this.labelInformation.AutoSize = true;
            this.labelInformation.Location = new System.Drawing.Point(12, 219);
            this.labelInformation.Name = "labelInformation";
            this.labelInformation.Size = new System.Drawing.Size(62, 13);
            this.labelInformation.TabIndex = 16;
            this.labelInformation.Text = "Information:";
            // 
            // labelInformationText
            // 
            this.labelInformationText.AutoSize = true;
            this.labelInformationText.Location = new System.Drawing.Point(80, 219);
            this.labelInformationText.Name = "labelInformationText";
            this.labelInformationText.Size = new System.Drawing.Size(0, 13);
            this.labelInformationText.TabIndex = 17;
            // 
            // checkBoxRepeat
            // 
            this.checkBoxRepeat.AutoSize = true;
            this.checkBoxRepeat.Location = new System.Drawing.Point(597, 220);
            this.checkBoxRepeat.Name = "checkBoxRepeat";
            this.checkBoxRepeat.Size = new System.Drawing.Size(61, 17);
            this.checkBoxRepeat.TabIndex = 19;
            this.checkBoxRepeat.Text = "Repeat";
            this.checkBoxRepeat.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(542, 248);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(169, 13);
            this.label2.TabIndex = 20;
            this.label2.Text = "(  to restart sending the rows again";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(552, 261);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "   when last have been sent )";
            // 
            // buttonStartServer
            // 
            this.buttonStartServer.Location = new System.Drawing.Point(323, 101);
            this.buttonStartServer.Name = "buttonStartServer";
            this.buttonStartServer.Size = new System.Drawing.Size(75, 23);
            this.buttonStartServer.TabIndex = 22;
            this.buttonStartServer.Text = "Start Server";
            this.buttonStartServer.UseVisualStyleBackColor = true;
            this.buttonStartServer.Click += new System.EventHandler(this.buttonStartServer_Click);
            // 
            // textBoxServerCommand
            // 
            this.textBoxServerCommand.Location = new System.Drawing.Point(106, 62);
            this.textBoxServerCommand.Name = "textBoxServerCommand";
            this.textBoxServerCommand.Size = new System.Drawing.Size(359, 20);
            this.textBoxServerCommand.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "Server Command";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(483, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Params";
            // 
            // textBoxServerParameters
            // 
            this.textBoxServerParameters.Location = new System.Drawing.Point(545, 62);
            this.textBoxServerParameters.Name = "textBoxServerParameters";
            this.textBoxServerParameters.Size = new System.Drawing.Size(141, 20);
            this.textBoxServerParameters.TabIndex = 28;
            // 
            // topic_Label
            // 
            this.topic_Label.AutoSize = true;
            this.topic_Label.Location = new System.Drawing.Point(320, 190);
            this.topic_Label.Name = "topic_Label";
            this.topic_Label.Size = new System.Drawing.Size(34, 13);
            this.topic_Label.TabIndex = 31;
            this.topic_Label.Text = "Topic";
            // 
            // topic_textBox
            // 
            this.topic_textBox.Location = new System.Drawing.Point(373, 185);
            this.topic_textBox.Name = "topic_textBox";
            this.topic_textBox.Size = new System.Drawing.Size(137, 20);
            this.topic_textBox.TabIndex = 32;
            // 
            // keyFieldLabel
            // 
            this.keyFieldLabel.AutoSize = true;
            this.keyFieldLabel.Location = new System.Drawing.Point(541, 189);
            this.keyFieldLabel.Name = "keyFieldLabel";
            this.keyFieldLabel.Size = new System.Drawing.Size(63, 13);
            this.keyFieldLabel.TabIndex = 33;
            this.keyFieldLabel.Text = "Key Column";
            this.keyFieldLabel.Click += new System.EventHandler(this.label6_Click);
            // 
            // keyFieldTextBox
            // 
            this.keyFieldTextBox.Location = new System.Drawing.Point(607, 186);
            this.keyFieldTextBox.Name = "keyFieldTextBox";
            this.keyFieldTextBox.Size = new System.Drawing.Size(89, 20);
            this.keyFieldTextBox.TabIndex = 34;
            this.keyFieldTextBox.Text = "0";
            // 
            // AddHeader_checkbox
            // 
            this.AddHeader_checkbox.AutoSize = true;
            this.AddHeader_checkbox.Location = new System.Drawing.Point(15, 293);
            this.AddHeader_checkbox.Name = "AddHeader_checkbox";
            this.AddHeader_checkbox.Size = new System.Drawing.Size(225, 17);
            this.AddHeader_checkbox.TabIndex = 35;
            this.AddHeader_checkbox.Text = "Add Header Row For Each Sent Message";
            this.AddHeader_checkbox.UseVisualStyleBackColor = true;
            this.AddHeader_checkbox.CheckedChanged += new System.EventHandler(this.AddHeader_checkbox_CheckedChanged);
            // 
            // columnSeparator_label
            // 
            this.columnSeparator_label.AutoSize = true;
            this.columnSeparator_label.Location = new System.Drawing.Point(320, 261);
            this.columnSeparator_label.Name = "columnSeparator_label";
            this.columnSeparator_label.Size = new System.Drawing.Size(91, 13);
            this.columnSeparator_label.TabIndex = 36;
            this.columnSeparator_label.Text = "Column Separator";
            // 
            // textQualifier_label
            // 
            this.textQualifier_label.AutoSize = true;
            this.textQualifier_label.Location = new System.Drawing.Point(12, 261);
            this.textQualifier_label.Name = "textQualifier_label";
            this.textQualifier_label.Size = new System.Drawing.Size(69, 13);
            this.textQualifier_label.TabIndex = 37;
            this.textQualifier_label.Text = "Text Qualifier";
            // 
            // textQualifier_ComboBox
            // 
            this.textQualifier_ComboBox.FormattingEnabled = true;
            this.textQualifier_ComboBox.Location = new System.Drawing.Point(92, 258);
            this.textQualifier_ComboBox.Name = "textQualifier_ComboBox";
            this.textQualifier_ComboBox.Size = new System.Drawing.Size(133, 21);
            this.textQualifier_ComboBox.TabIndex = 38;
            // 
            // columnSeparator_comboBox
            // 
            this.columnSeparator_comboBox.FormattingEnabled = true;
            this.columnSeparator_comboBox.Location = new System.Drawing.Point(417, 258);
            this.columnSeparator_comboBox.Name = "columnSeparator_comboBox";
            this.columnSeparator_comboBox.Size = new System.Drawing.Size(93, 21);
            this.columnSeparator_comboBox.TabIndex = 39;
            // 
            // otherSeparator_label
            // 
            this.otherSeparator_label.AutoSize = true;
            this.otherSeparator_label.Location = new System.Drawing.Point(320, 297);
            this.otherSeparator_label.Name = "otherSeparator_label";
            this.otherSeparator_label.Size = new System.Drawing.Size(82, 13);
            this.otherSeparator_label.TabIndex = 40;
            this.otherSeparator_label.Text = "Other Separator";
            // 
            // otherSeparator_textBox
            // 
            this.otherSeparator_textBox.Location = new System.Drawing.Point(417, 294);
            this.otherSeparator_textBox.MaxLength = 1;
            this.otherSeparator_textBox.Name = "otherSeparator_textBox";
            this.otherSeparator_textBox.Size = new System.Drawing.Size(93, 20);
            this.otherSeparator_textBox.TabIndex = 41;
            // 
            // sentAsTopic_label
            // 
            this.sentAsTopic_label.AutoSize = true;
            this.sentAsTopic_label.Location = new System.Drawing.Point(12, 350);
            this.sentAsTopic_label.Name = "sentAsTopic_label";
            this.sentAsTopic_label.Size = new System.Drawing.Size(84, 13);
            this.sentAsTopic_label.TabIndex = 42;
            this.sentAsTopic_label.Text = "Sent with Topic:";
            // 
            // sentAsTopic_textBox
            // 
            this.sentAsTopic_textBox.Location = new System.Drawing.Point(15, 366);
            this.sentAsTopic_textBox.Name = "sentAsTopic_textBox";
            this.sentAsTopic_textBox.Size = new System.Drawing.Size(110, 20);
            this.sentAsTopic_textBox.TabIndex = 43;
            // 
            // chkFirstRowHeading
            // 
            this.chkFirstRowHeading.AutoSize = true;
            this.chkFirstRowHeading.Location = new System.Drawing.Point(15, 316);
            this.chkFirstRowHeading.Name = "chkFirstRowHeading";
            this.chkFirstRowHeading.Size = new System.Drawing.Size(130, 17);
            this.chkFirstRowHeading.TabIndex = 44;
            this.chkFirstRowHeading.Text = "Is First Row Heading?";
            this.chkFirstRowHeading.UseVisualStyleBackColor = true;
            // 
            // Selector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(723, 438);
            this.Controls.Add(this.chkFirstRowHeading);
            this.Controls.Add(this.sentAsTopic_textBox);
            this.Controls.Add(this.sentAsTopic_label);
            this.Controls.Add(this.otherSeparator_textBox);
            this.Controls.Add(this.otherSeparator_label);
            this.Controls.Add(this.columnSeparator_comboBox);
            this.Controls.Add(this.textQualifier_ComboBox);
            this.Controls.Add(this.textQualifier_label);
            this.Controls.Add(this.columnSeparator_label);
            this.Controls.Add(this.AddHeader_checkbox);
            this.Controls.Add(this.keyFieldTextBox);
            this.Controls.Add(this.keyFieldLabel);
            this.Controls.Add(this.topic_textBox);
            this.Controls.Add(this.topic_Label);
            this.Controls.Add(this.textBoxServerParameters);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxServerCommand);
            this.Controls.Add(this.buttonStartServer);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBoxRepeat);
            this.Controls.Add(this.labelInformationText);
            this.Controls.Add(this.labelInformation);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.labelMessagesSent);
            this.Controls.Add(this.textBoxSentMessages);
            this.Controls.Add(this.comboBoxMessageType);
            this.Controls.Add(this.labelMessageType);
            this.Controls.Add(this.textBoxMessPerSec);
            this.Controls.Add(this.labelMessPerSec);
            this.Controls.Add(this.textBoxFilePath);
            this.Controls.Add(this.labelFilePath);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.labelPort);
            this.Controls.Add(this.textBoxHost);
            this.Controls.Add(this.labelHost);
            this.Controls.Add(this.publisherMQ_comboBox);
            this.Controls.Add(this.label1);
            this.Name = "Selector";
            this.Text = "Message Queue Publisher";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox publisherMQ_comboBox;
        private System.Windows.Forms.Label labelHost;
        private System.Windows.Forms.TextBox textBoxHost;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Label labelFilePath;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.Label labelMessPerSec;
        private System.Windows.Forms.TextBox textBoxMessPerSec;
        private System.Windows.Forms.Label labelMessageType;
        private System.Windows.Forms.ComboBox comboBoxMessageType;
        private System.Windows.Forms.TextBox textBoxSentMessages;
        private System.Windows.Forms.Label labelMessagesSent;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelInformation;
        private System.Windows.Forms.Label labelInformationText;
        private System.Windows.Forms.CheckBox checkBoxRepeat;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonStartServer;
        private System.Windows.Forms.TextBox textBoxServerCommand;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxServerParameters;
        private System.Windows.Forms.Label topic_Label;
        private System.Windows.Forms.TextBox topic_textBox;
        private System.Windows.Forms.Label keyFieldLabel;
        private System.Windows.Forms.TextBox keyFieldTextBox;
        private System.Windows.Forms.CheckBox AddHeader_checkbox;
        private System.Windows.Forms.Label columnSeparator_label;
        private System.Windows.Forms.Label textQualifier_label;
        private System.Windows.Forms.ComboBox textQualifier_ComboBox;
        private System.Windows.Forms.ComboBox columnSeparator_comboBox;
        private System.Windows.Forms.Label otherSeparator_label;
        private System.Windows.Forms.TextBox otherSeparator_textBox;
        private System.Windows.Forms.Label sentAsTopic_label;
        private System.Windows.Forms.TextBox sentAsTopic_textBox;
        private System.Windows.Forms.CheckBox chkFirstRowHeading;
    }
}