﻿using Datawatch.Data.MQP.ProducerBase;
using Datawatch.Data.MQP.Producers.ActiveMQ;
using Datawatch.Data.MQP.Producers.Qpid;
using Datawatch.Data.MQP.Producers.Kafka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MQP;

namespace Datawatch.Data.MQP
{
    static class Program
    {
        private static readonly Publisher[] list = new Publisher[]
                                            {
                                                new ActiveMQPublisher(), 
                                                new QpidPublisher(),
                                                new KafkaPublisher()
                                            };
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Selector(list));
        }
    }
}
