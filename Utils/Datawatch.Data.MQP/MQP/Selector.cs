﻿using Datawatch.Data.MQP.ProducerBase;
using Datawatch.Data.MQP.Producers.ActiveMQ;
using Datawatch.Data.MQP.Producers.Kafka;
using MQP.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MQP
{
    public partial class Selector : Form
    {
        private const string FilePathKey = "filePath";
        private const string CommandKey = "_ServerCommand";
        private const string ParametersKey = "_ServerParameters";
        private const string DefaultFilePath = "C:\\aleri_demo\\apps\\UKTrading\\History.csv";
        private string filePath;
        private const char DataSeparator = '¤';
        private const string DataFile = "data.dat";
        private Dictionary<string, Publisher> lookUp;
        private bool publishing = false;
        private Thread thread;
        //        private string keyFieldString = "3";

        public Selector(Publisher[] list)
        {
            InitializeComponent();

            lookUp = new Dictionary<string, Publisher>();
            foreach (Publisher publisher in list)
            {
                publisherMQ_comboBox.Items.Add(publisher.GetName());
                lookUp.Add(publisher.GetName(), publisher);
            }
            publisherMQ_comboBox.SelectedItem = list[0].GetName();
            if (!File.Exists(DataFile))
            {
                RecreateDataFile();
            }
            if (!ReadConfigData())
            {
                RecreateDataFile();
                ReadConfigData();
                MessageBox.Show(Resources.UiCorruptDataFileText,
                Resources.UiCorruptDataFileTitle,
                MessageBoxButtons.OK,
                MessageBoxIcon.Asterisk);
            }
            InitInfo(list[0]);
            foreach (string messageType in MessageFormat.GetMessageTypes())
            {
                comboBoxMessageType.Items.Add(messageType);
            }
            comboBoxMessageType.SelectedItem = MessageFormat.GetMessageTypes()[0];
            labelInformationText.Text = GetSelectedPublisher().GetInformation();
            //            keyFieldTextBox.Text = keyFieldString;
            this.CenterToScreen();
            this.publisherMQ_comboBox.SelectedIndexChanged +=
                comboBoxMQSystem_SelectedIndexChanged;
            this.textBoxHost.TextChanged += textBoxHost_TextChanged;
            this.keyFieldTextBox.TextChanged += keyField_TextChanged;
            this.topic_textBox.TextChanged += topic_TextChanged;
            this.textBoxPort.TextChanged += textBoxPort_TextChanged;
            this.textBoxFilePath.TextChanged += textBoxFilePath_TextChanged;
            this.textBoxServerCommand.TextChanged 
                += textBoxServerCommand_TextChanged;
            this.textBoxServerParameters.TextChanged 
                += textBoxServerParameters_TextChanged;
            this.comboBoxMessageType.SelectedIndexChanged 
                += comboBoxMessageType_SelectedIndexChanged;
            this.FormClosing += new System.Windows.Forms.
                FormClosingEventHandler(this.MainForm_FormClosing);
            foreach (string textQualifier in MessageFormat.GetTextQualifiers)
            {
                textQualifier_ComboBox.Items.Add(textQualifier);
            }
            foreach (string columnSeparators in MessageFormat.GetColumnSeparators)
            {
                columnSeparator_comboBox.Items.Add(columnSeparators);
            }
            columnSeparator_comboBox.SelectedIndexChanged 
                += columnSeparator_comboBox_SelectedIndexChanged;
            AddHeader_checkbox.Checked = false;
            textQualifier_ComboBox.SelectedItem = MessageFormat.GetTextQualifiers[0];
            columnSeparator_comboBox.SelectedItem = MessageFormat.GetColumnSeparators[0];
            changedMessageTypeUpdateItemVisibilities();
        }

        private void InitInfo(Publisher publisher)
        {
            textBoxServerCommand.Text = publisher.ServerCommand;
            textBoxServerParameters.Text = publisher.ServerParameters;
            textBoxHost.Text = publisher.Host;
            textBoxPort.Text = publisher.Port;

            labelInformationText.Text = GetSelectedPublisher().GetInformation();
            topic_Label.Visible = publisher is ActiveMQPublisher || publisher is KafkaPublisher;
            topic_textBox.Visible = publisher is ActiveMQPublisher || publisher is KafkaPublisher;
            keyFieldLabel.Visible = publisher is ActiveMQPublisher;
            keyFieldTextBox.Visible = publisher is ActiveMQPublisher;
            if (publisher is ActiveMQPublisher)
            {
                topic_textBox.Text = ((ActiveMQPublisher)publisher).Topic;
            }
            if (publisher is KafkaPublisher)
            {
                topic_textBox.Text = ((KafkaPublisher)publisher).Topic;
            }
            sentAsTopic_label.Visible = publisher is ActiveMQPublisher;
            sentAsTopic_textBox.Visible = publisher is ActiveMQPublisher;

            chkFirstRowHeading.Visible = AddHeader_checkbox.Checked;
        }

        private void comboBoxMQSystem_SelectedIndexChanged(object sender,
                                                           System.EventArgs e)
        {
            Publisher selectedPublisher = GetSelectedPublisher();
            InitInfo(selectedPublisher);
        }


        private void changedMessageTypeUpdateItemVisibilities()
        {
            textQualifier_label.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv;
            textQualifier_ComboBox.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv;
            columnSeparator_label.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv;
            columnSeparator_comboBox.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv;
            otherSeparator_label.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv 
                && ((string)columnSeparator_comboBox.SelectedItem) == MessageFormat.Other;
            otherSeparator_textBox.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv 
                && ((string)columnSeparator_comboBox.SelectedItem) == MessageFormat.Other;
            AddHeader_checkbox.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv;
            chkFirstRowHeading.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv 
                && AddHeader_checkbox.Checked;
        }

        private void comboBoxMessageType_SelectedIndexChanged(object sender,
                                                           System.EventArgs e)
        {
            changedMessageTypeUpdateItemVisibilities();
        }

        private void columnSeparator_comboBox_SelectedIndexChanged(object sender,
                                                           System.EventArgs e)
        {
            otherSeparator_label.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv 
                && ((string)columnSeparator_comboBox.SelectedItem) == MessageFormat.Other;
            otherSeparator_textBox.Visible = ((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv
                && ((string)columnSeparator_comboBox.SelectedItem) == MessageFormat.Other;
        }

        private void textBoxHost_TextChanged(object sender, System.EventArgs e)
        {
            Publisher selectedPublisher = GetSelectedPublisher();
            selectedPublisher.Host = textBoxHost.Text;
        }


        private void textBoxServerParameters_TextChanged(object sender, System.EventArgs e)
        {
            Publisher selectedPublisher = GetSelectedPublisher();
            selectedPublisher.ServerParameters = textBoxServerParameters.Text;
        }

        private void textBoxPort_TextChanged(object sender, System.EventArgs e)
        {
            Publisher selectedPublisher = GetSelectedPublisher();
            selectedPublisher.Port = textBoxPort.Text;
        }

        private void keyField_TextChanged(object sender, System.EventArgs e)
        {
            //            keyFieldString = keyFieldTextBox.Text;
        }

        private void topic_TextChanged(object sender, System.EventArgs e)
        {
            if (GetSelectedPublisher() is ActiveMQPublisher)
            {
                ((ActiveMQPublisher)GetSelectedPublisher()).Topic = topic_textBox.Text;
            }
            if (GetSelectedPublisher() is KafkaPublisher)
            {
                ((KafkaPublisher)GetSelectedPublisher()).Topic = topic_textBox.Text;
                labelInformationText.Text = GetSelectedPublisher().GetInformation();
            }
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            string filePath = textBoxFilePath.Text;
            if (!File.Exists(filePath))
            {
                MessageBox.Show(Resources.UiCannotFindFile,
                    Resources.UiDialogTitleCannotFindFile,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                return;
            }
            if (publishing)
            {
                // Already publishing. 
                return;
            }
            Publisher publisher = GetSelectedPublisher();
            string selectedPublisherName = comboBoxMessageType.SelectedItem.ToString();
            Boolean repeat = checkBoxRepeat.Checked;
            Boolean addHeader = false;
            Boolean isFirstRowHeader = false;
            if (((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv)
            {
                addHeader = AddHeader_checkbox.Checked;
                isFirstRowHeader = chkFirstRowHeading.Checked;
            }

            string newColumnSeparator = GetSelectedColumnDelimiter();
            if (newColumnSeparator == null)
            {
                return;
            }
            string newTextQualifier = GetSelectedTextQualifier();

            thread = new Thread(() =>
                PublisherThread(publisher, selectedPublisherName, 
                repeat, addHeader, isFirstRowHeader,
                newColumnSeparator, newTextQualifier));
            thread.Start();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            publishing = false;
        }

        private void buttonStartServer_Click(object sender, EventArgs e)
        {
            Publisher publisher = GetSelectedPublisher();
            publisher.StartServer();
        }

        private Publisher GetSelectedPublisher()
        {
            return lookUp[publisherMQ_comboBox.SelectedItem.ToString()];
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            UpdateDataFile();
        }

        private string GetSelectedColumnDelimiter()
        {
            if (((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv)
            {
                switch ((string)columnSeparator_comboBox.SelectedItem)
                {
                    case MessageFormat.Comma:
                        return ",";
                    case MessageFormat.SemiColon:
                        return ";";
                    case MessageFormat.Pipe:
                        return "|";
                    case MessageFormat.Space:
                        return " ";
                    case MessageFormat.Tab:
                        return "\t";
                    case MessageFormat.Other:
                        if (string.IsNullOrEmpty(otherSeparator_textBox.Text))
                        {
                            MessageBox.Show(Resources.UiInvalidColumnSeparator,
                                            Resources.UiTitleInvalidColumnSeparator,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Asterisk);
                            return null;
                        }
                        return otherSeparator_textBox.Text;
                    default:
                        throw new Exception("Not supported ColumnDelimiter");
                }
            }
            return ",";
        }

        private string GetSelectedTextQualifier()
        {
            if (((string)comboBoxMessageType.SelectedItem) == MessageFormat.Csv)
            {
                switch ((string)textQualifier_ComboBox.SelectedItem)
                {
                    case MessageFormat.None:
                        return "";
                    case MessageFormat.DoubleQuotes:
                        return "\"";
                    case MessageFormat.SingleQuotes:
                        return "\'";
                    default:
                        throw new Exception("Not supported Text Qualifier");
                }
            }
            return "";
        }

        private void PublisherThread(Publisher publisher, 
            string selectedMessageType, Boolean repeat, 
            Boolean addHeader, Boolean isFirstRowHeader,
            string newColumnSeparator, string newTextQualifier)
        {
            bool exit = false;

            if (!publisher.StartConnection())
            {
                MessageBox.Show(Resources.UiCannotConnectToServer,
                Resources.UiTitleFailedToConnect,
                MessageBoxButtons.OK,
                MessageBoxIcon.Asterisk);
                thread.Abort();
                return;
            }

            publishing = true;

            double MessagesPerSecond = Convert.ToDouble(textBoxMessPerSec.Text);
            int threadDelay = Convert.ToInt32(1000 / MessagesPerSecond);

            string filePath = textBoxFilePath.Text;
            int keyFieldNumber = Convert.ToInt32(keyFieldTextBox.Text);
            bool isCsv = selectedMessageType.Equals(MessageFormat.Csv);
            int id = MessageFormat.GetIndex(selectedMessageType);
            bool useTextHeader = false;
            if (!isCsv)
            {
                useTextHeader = MessageFormat.UseTextHeader(id);
            }

            string headerRow = "";
            // While the publishing flag has been set to true and there are rows to retrieve so exit is set to false.
            string message;
            string cvsLine;
            string[] cvsDelim = { "," };


            string[] cvsFields;
            string[] cvsLineDelim = { "\r\n" };
            StringBuilder sb = new StringBuilder();
            string keyValue;
            string sentAsTopic;
            while (!exit && (publishing == true))
            {
                try
                {
                    lock (this)
                    {
                        // Read the file and display it line by line.
                        StreamReader file = new StreamReader(filePath);

                        string[] cvsLines;
                        string allfile = file.ReadToEnd();
                        cvsLines = allfile.Split(cvsLineDelim, StringSplitOptions.None);

                        if (addHeader)
                        {
                            headerRow = string.Empty;
                            string[] headers = cvsLines[0].Split(',');
                            if (isFirstRowHeader)
                            {
                                for (int i = 0; i < headers.Length; i++)
                                    headerRow += (headers[i] + ",");
                            }
                            else
                            {
                                for (int i = 0; i < headers.Length; i++)
                                    headerRow += ("f" + i + ",");
                            }
                            headerRow = headerRow.Remove(headerRow.Length - 1) + "\r\n";
                        }

                        for (int dataCounter = isFirstRowHeader ? 1 : 0; dataCounter < cvsLines.Length; dataCounter++)
                        {
                            if (publishing == false)
                            {
                                break;
                            }
                            if (dataCounter == cvsLines.Length - 1)
                            {
                                // Reached the end of the file
                                if (repeat == true)
                                {
                                    //Reset the counter and read the file again
                                    dataCounter = isFirstRowHeader ? 1 : 0; ;
                                }
                            }

                            cvsLine = cvsLines[dataCounter];

                            if (publishing)
                            {
                                message = "";
                                keyValue = "";
                                if (cvsLine.Contains(","))
                                {
                                    cvsFields = cvsLine.Split(cvsDelim, StringSplitOptions.None);

                                    if (publisher.GetName() != ActiveMQPublisher.Name)
                                    {
                                        keyValue = "";
                                    }
                                    else
                                    {
                                        if (keyFieldNumber < 0 || keyFieldNumber >= cvsFields.Length)
                                        {
                                            MessageBox.Show(Resources.UiInvalidKeyField,
                                            Resources.UiTitleInvalidKeyField,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Asterisk);
                                            publishing = false;
                                            exit = true;
                                            break;
                                        }
                                        keyValue = cvsFields[keyFieldNumber];
                                    }

                                    if (isCsv)
                                    {
                                        sb.Length = 0;
                                        if (string.IsNullOrEmpty(cvsLine)) continue;
                                        for (int f = 0; f < cvsFields.Length; f++)
                                        {
                                            if (f != 0)
                                            {
                                                sb.Append(newColumnSeparator);
                                                if (newColumnSeparator != " ")
                                                {
                                                    sb.Append(" ");
                                                }
                                            }
                                            sb.Append(newTextQualifier).
                                               Append(cvsFields[f].Trim()).
                                               Append(newTextQualifier);
                                        }
                                        string csvMessage = sb.ToString();

                                        string sentMessage = headerRow + csvMessage;
                                        sentAsTopic = publisher.Send(sentMessage,
                                            keyValue);
                                        showWhatsBeenSent(dataCounter, sentMessage, sentAsTopic);

                                        Thread.Sleep(threadDelay);
                                        continue;
                                    }
                                    // Add start row tokens
                                    message = MessageFormat.Get(id, DataFormat.RowStart);//"<tuple ";

                                    for (int c = 0; c < cvsFields.Length; c++)
                                    {
                                        // Add pair separator
                                        if (c != 0)
                                        {
                                            message += MessageFormat.Get(id, DataFormat.PairSep);
                                        }
                                        // Add value pair
                                        message += // Add the variable
                                                    MessageFormat.Get(id, DataFormat.VariableApostrophe) +
                                                    (useTextHeader ? "f" + c : "" + c) +
                                                    MessageFormat.Get(id, DataFormat.VariableApostrophe) +
                                            // Add the assignment token
                                                    MessageFormat.Get(id, DataFormat.Assign) +
                                            // Add the value
                                                    MessageFormat.Get(id, DataFormat.ValueApostrophe) +
                                                    cvsFields[c] +
                                                    MessageFormat.Get(id, DataFormat.ValueApostrophe);
                                    }
                                    // Add end row tokens
                                    message += MessageFormat.Get(id, DataFormat.RowEnd); //"/>";
                                }

                                if (string.IsNullOrEmpty(message)) continue;

                                sentAsTopic = publisher.Send(headerRow + message, keyValue);

                                showWhatsBeenSent(dataCounter, message, sentAsTopic);

                                Thread.Sleep(threadDelay);
                            }
                        }

                        exit = true;
                        publishing = false;
                    }
                }
                catch (Exception e)
                {
                    lock (this)
                    {
                        publisher.StopConnection();
                        if (e.Message.Equals("Index was outside the bounds of the array."))
                        {
                            MessageBox.Show(Resources.UiInvalidKeyField,
                                Resources.UiTitleInvalidKeyField,
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Asterisk);
                        }
                    }
                }
            }
            publishing = false;
            publisher.StopConnection();
            thread.Abort();
        }

        private void showWhatsBeenSent(int index, string message, string topic)
        {
            // Now write the output string of rows to the screen.
            MethodInvoker action = delegate
            { textBoxSentMessages.Text = index + ") " + message; };
            textBoxSentMessages.BeginInvoke(action);
            action = delegate
            { sentAsTopic_textBox.Text = topic; };
            sentAsTopic_textBox.BeginInvoke(action);
        }

        private void textBoxFilePath_TextChanged(object sender, EventArgs e)
        {
            FilePath = textBoxFilePath.Text;
        }

        private void textBoxServerCommand_TextChanged(object sender, EventArgs e)
        {
            Publisher publisher = GetSelectedPublisher();
            publisher.ServerCommand = textBoxServerCommand.Text;
        }

        private bool ReadConfigData()
        {
            StreamReader file = null;
            try
            {
                file = new StreamReader(DataFile);
                while (!file.EndOfStream)
                {
                    string line = file.ReadLine();

                    if (string.IsNullOrEmpty(line)) continue;

                    string[] pair = line.Split(DataSeparator);
                    if (string.IsNullOrEmpty(pair[0]))
                    {
                        // Something is wrong with the data file, so reset it completely
                        file.Close();
                        return false;
                    }
                    if (pair[0] == FilePathKey)
                    {
                        this.textBoxFilePath.Text = pair[1];
                        continue;
                    }
                    int index = pair[0].LastIndexOf("_");

                    if (index < 0)
                    {
                        // Something is wrong with the data file, so reset it completely
                        file.Close();
                        return false;
                    }

                    string publisherName = pair[0].Substring(0, index);
                    string dataType = pair[0].Substring(index);
                    Publisher publisher = lookUp[publisherName];
                    if (CommandKey.Equals(dataType))
                    {
                        publisher.ServerCommand = pair[1].Trim();
                        continue;
                    }
                    if (ParametersKey.Equals(dataType))
                    {
                        publisher.ServerParameters = pair[1].Trim();
                        continue;
                    }
                    // Something is wrong with the data file, so reset it completely
                    file.Close();
                    return false;
                }
            }
            finally
            {
                if (file != null)
                {
                    file.Close();
                }
            }
            return true;
        }

        private void RecreateDataFile()
        {
            if (File.Exists(DataFile))
            {
                File.Delete(DataFile);
            }

            StreamWriter file = null;
            try
            {
                file = new StreamWriter(DataFile);
                string filePathLine = FilePathKey + DataSeparator + DefaultFilePath;
                file.WriteLine(filePathLine);
                foreach (string key in publisherMQ_comboBox.Items)
                {
                    Publisher publisher = lookUp[key];
                    string command = publisher.GetDefaultServerCommand();
                    string commandLine = key + CommandKey + DataSeparator + command;
                    file.WriteLine(commandLine);
                    string parameters = publisher.GetDefaultServerParameters();
                    string parametersLine = key + ParametersKey + DataSeparator + parameters;
                    file.WriteLine(parametersLine);
                }
            }
            finally
            {
                if (file != null)
                {
                    file.Close();
                }
            }
        }

        private void UpdateDataFile()
        {
            if (File.Exists(DataFile))
            {
                File.Delete(DataFile);
            }
            StreamWriter file = null;
            try
            {
                file = new StreamWriter(DataFile);
                string filePathLine = FilePathKey + DataSeparator + FilePath;
                file.WriteLine(filePathLine);
                foreach (string key in publisherMQ_comboBox.Items)
                {
                    Publisher publisher = lookUp[key];
                    // Write Server Command
                    string command = publisher.ServerCommand;
                    string commandLine = key + CommandKey + DataSeparator + command;
                    file.WriteLine(commandLine);
                    // Write Ser Parameters
                    string parameters = publisher.ServerParameters;
                    string parameterLine = key + ParametersKey + DataSeparator + parameters;
                    file.WriteLine(parameterLine);
                }
            }
            finally
            {
                if (file != null)
                {
                    file.Close();
                }
            }
        }

        private string FilePath
        {
            get
            {
                if (filePath == null)
                {
                    filePath = DefaultFilePath;
                }
                return filePath;
            }
            set { filePath = value; }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void AddHeader_checkbox_CheckedChanged(
            object sender, EventArgs e)
        {
            chkFirstRowHeading.Visible = AddHeader_checkbox.Checked;
        }
    }
}
