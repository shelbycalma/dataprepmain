﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KafkaNet;
using KafkaNet.Common;
using KafkaNet.Model;
using KafkaNet.Protocol;
using Datawatch.Data.MQP.ProducerBase;

namespace Datawatch.Data.MQP.Producers.Kafka
{
    public class KafkaPublisher : Publisher
    {
        private string DefaultServerCommand = "C:\\kafka\\bin\\windows\\kafka-producer-run.bat";
        private const string DefaultServerParameters = "";
        private string serverCommand;
        private string serverParameters;
        private string topicName = "test";
        private string host = "localhost";
        private string port = "9092";
        private KafkaOptions options = null;
        private Producer producer = null;

        public string Topic
        {
            get { return topicName; }
            set { topicName = value; }
        }

        public override string GetName()
        {
            return "Kafka";
        }

        public override string Host
        {
            get { return host; }
            set { host = value; }
        }

        public override string Port
        {
            get { return port; }
            set { port = value; }
        }

        public override bool StartConnection()
        {
            try
            {
                string url = string.Concat("http://", Host, ":", Port);
                //create an options file that sets up driver preferences
                options = new KafkaOptions(new Uri(url), new Uri(url))
                {
                    Log = new ConsoleLog()
                };

                //create a producer to send messages with
                producer = new KafkaNet.Producer(new BrokerRouter(options))
                {
                    BatchSize = 100,
                    BatchDelayTime = TimeSpan.FromMilliseconds(2000)
                };
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public override void StopConnection()
        {
            options = null;
            producer = null;
        }

        public override string Send(string message, string extraInfo)
        {
            if (string.IsNullOrEmpty(message))
            {
                //send a random batch of messages
                SendRandomBatch(producer, Topic, 200);
            }
            else
            {
                producer.SendMessageAsync(Topic, new[] { new Message(message) });
            }
            return string.Empty;
        }

        public override string GetInformation()
        {
            return "topic name : " + Topic + " is used for publish messages";
        }

        public override string GetDefaultServerCommand()
        {
            return DefaultServerCommand;
        }

        public override string GetDefaultServerParameters()
        {
            return DefaultServerParameters;

        }

        public override string ServerCommand
        {
            get
            {
                if (serverCommand == null)
                {
                    serverCommand = DefaultServerCommand;
                }
                return serverCommand;
            }
            set { serverCommand = value; }
        }

        public override string ServerParameters
        {
            get
            {
                if (serverParameters == null)
                {
                    serverParameters = DefaultServerParameters;
                }
                return serverParameters;
            }
            set { serverParameters = value; }
        }

        #region "Private Methods"
        private static async void SendRandomBatch(KafkaNet.Producer producer, string _topic, int count)
        {
            //send multiple messages
            var sendTask = producer.SendMessageAsync(_topic, Enumerable.Range(0, count).Select(x => new Message(x.ToString())));

            Console.WriteLine("Posted #{0} messages.  Buffered:{1} AsyncCount:{2}", count, producer.BufferCount, producer.AsyncCount);

            var response = await sendTask;

            Console.WriteLine("Completed send of batch: {0}. Buffered:{1} AsyncCount:{2}", count, producer.BufferCount, producer.AsyncCount);
            foreach (var result in response.OrderBy(x => x.PartitionId))
            {
                Console.WriteLine("Topic:{0} PartitionId:{1} Offset:{2}", result.Topic, result.PartitionId, result.Offset);
            }
        }
        #endregion
    }
}
