﻿using System;
using Datawatch.Data.Utils.PackageHelper;
using Xunit;

namespace PackageHelper.Tests
{
    public class FileToolsTests
    {
        [Fact]
        public void NormalizePathTest()
        {
            string result = FileTools.NormalizePath("..\\..\\Dir1\\asd.zxc");
            Assert.Equal("..\\..\\Dir1\\asd.zxc", result);
            result = FileTools.NormalizePath("..\\..\\Dir1\\\\Dir 2\\");
            Assert.Equal("..\\..\\Dir1\\Dir 2\\", result);
            result = FileTools.NormalizePath("..\\..\\Dir1\\\\Dir 2\\", true);
            Assert.Equal("\"..\\..\\Dir1\\Dir 2\\\"", result);
            result = FileTools.NormalizePath("..\\..\\Dir1\\\"\\Dir 2\\\"\\\"Dir 3\"", true);
            Assert.Equal("\"..\\..\\Dir1\\Dir 2\\Dir 3\"", result);
            result = FileTools.NormalizePath("..\\..\\Dir1\\..\\asd.zxc");
            Assert.Equal("..\\..\\asd.zxc", result);
            result = FileTools.NormalizePath("C:\\Dir1\\..\\Dir2\\asd.zxc");
            Assert.Equal("C:\\Dir2\\asd.zxc", result);
            result = FileTools.NormalizePath("C:\\Dir 1\\..\\Dir 2\\asd.zxc", true);
            Assert.Equal("\"C:\\Dir 2\\asd.zxc\"", result);
            result = FileTools.NormalizePath(".\\Dir1\\..\\Dir2\\asd.zxc");
            Assert.Equal(".\\Dir2\\asd.zxc", result);
            Assert.Equal("C:\\A\\a.bc", FileTools.NormalizePath("C:/.\\A\\B\\../.\\..\\A\\.\\a.bc"));
            Assert.Equal("C:/A/a.bc", FileTools.NormalizePath("C:/.\\A\\B\\../.\\..\\A\\.\\a.bc", true, '/'));
            Assert.Throws<ArgumentException>(() => FileTools.NormalizePath("C:\\..\\Dir1\\asd.zxc"));
            Assert.Throws<ArgumentException>(() => FileTools.NormalizePath(null));
        }

        [Fact]
        public void CombinePathTest()
        {
            string result = FileTools.CombinePath("C:\\Dir1\\Dir2\\", "..\\RDir1\\");
            Assert.Equal("C:\\Dir1\\RDir1\\", result);

            result = FileTools.CombinePath("\\Dir1\\Dir2\\", "..\\RDir1\\", true);
            Assert.Equal("\\Dir1\\RDir1\\", result);

            result = FileTools.CombinePath("..\\Dir1\\Dir2\\", "..\\RDir1\\");
            Assert.Equal("..\\Dir1\\RDir1\\", result);

            result = FileTools.CombinePath(".\\Dir1\\Dir2\\", "..\\RDir1\\");
            Assert.Equal(".\\Dir1\\RDir1\\", result);

            result = FileTools.CombinePath(".\\Dir1\\Dir2\\", null);
            Assert.Equal(".\\Dir1\\Dir2\\", result);

            result = FileTools.CombinePath(".\\Dir 1\\Dir2\\", "Dir3\\..\\..\\Dir4", true);
            Assert.Equal("\".\\Dir 1\\Dir4\"", result);

            var path = @"C:\Dir1\Dir2";
            var r1 = System.IO.Path.GetDirectoryName(path);
            var r2 = System.IO.Path.GetDirectoryName(path + "\\");
            
            Assert.Throws<ArgumentException>(() => FileTools.CombinePath("", "Dir1"));
            Assert.Throws<ArgumentOutOfRangeException>(() => FileTools.CombinePath("C:\\Dir1\\Dir2\\", "..\\Dir3", true, true));
        }

        [Fact]
        public void GetRelativePathTest()
        {
            string result = FileTools.GetRelativePath("C:\\Dir1", "C:\\Dir1\\Dir2");
            Assert.Equal("Dir2\\", result);

            result = FileTools.GetRelativePath("C:\\Dir 1\\Dir2\\", "C:\\Dir3");
            Assert.Equal("..\\..\\Dir3\\", result);

            result = FileTools.GetRelativePath("\\Dir 1\\Dir2\\", "\\Dir 3");
            Assert.Equal("..\\..\\Dir 3\\", result);

            result = FileTools.GetRelativePath("\\Dir 1\\Dir2\\", "\\");
            Assert.Equal("..\\..\\", result);

            Assert.Throws<ArgumentException>(() => FileTools.GetRelativePath("..\\Dir 1\\Dir2\\", "Dir3\\abc.txt"));
            Assert.Throws<ArgumentException>(() => FileTools.GetRelativePath("\\", "D:\\Dir3"));
            Assert.Throws<ArgumentException>(() => FileTools.GetRelativePath("C:\\Dir 1\\Dir2\\", "D:\\Dir3"));
            Assert.Throws<ArgumentException>(() => FileTools.GetRelativePath("\\Dir 1\\Dir2\\", "D:\\Dir3"));
            Assert.Throws<ArgumentException>(() => FileTools.GetRelativePath("C:\\Dir 1\\Dir2\\", "\\Dir3"));
        }
    }
}
