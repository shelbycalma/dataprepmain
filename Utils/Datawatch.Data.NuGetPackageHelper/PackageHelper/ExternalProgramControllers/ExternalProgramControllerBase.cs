﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    abstract class ExternalProgramControllerBase
    {
        protected string configPath;
        protected string exePath;
        protected ILogger logger;

        public ExternalProgramControllerBase(ILogger logger, VerbosityLevel verbLevel = VerbosityLevel.Normal)
        {
            this.logger = logger;
            this.Verbosity = verbLevel;
        }

        public VerbosityLevel Verbosity { get; protected set; }

        protected virtual int ExecuteCommand(string commandArgs)
        {
            this.logger.Log(LogLevel.Debug, 
                this.exePath + " " + commandArgs ?? string.Empty);

            var startInfo = new ProcessStartInfo(this.exePath);
            startInfo.Arguments = commandArgs;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;

            var p = Process.Start(startInfo);
            while (p.StandardOutput.EndOfStream == false)
            {
                // TODO: optimize for large amounts of text
                this.logger.Log(LogLevel.Info, p.StandardOutput.ReadToEnd());
            }

            while (p.StandardError.EndOfStream == false)
            {
                this.logger.Log(LogLevel.Error, p.StandardError.ReadToEnd());
            }

            p.WaitForExit();

            return p.ExitCode;
        }
        protected virtual int ExecuteCommand(string commandArgs, out string response)
        {
            this.logger.Log(LogLevel.Debug,
                this.exePath + " " + commandArgs ?? string.Empty);

            var startInfo = new ProcessStartInfo(this.exePath);
            startInfo.Arguments = commandArgs;
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;

            var sb = new StringBuilder(4096);
            var p = Process.Start(startInfo);
            while (p.StandardOutput.EndOfStream == false)
            {
                var data = p.StandardOutput.ReadToEnd();
                this.logger.Log(LogLevel.Info, data);
                sb.Append(data);
            }

            while (p.StandardError.EndOfStream == false)
            {
                this.logger.Log(LogLevel.Error, p.StandardError.ReadToEnd());
            }

            response = sb.ToString();
            p.WaitForExit();

            return p.ExitCode;
        }
    }
}