﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class NuGetController : ExternalProgramControllerBase
    {
        public const string SolutionPackageCacheDir = "packages";

        public NuGetController(string nugetPath, string nugetConfigPath, VerbosityLevel verbLevel, ILogger logger)
            : base(logger, verbLevel)
        {
            nugetPath = FileTools.CreatePathRelativeToExecAssembly(nugetPath);
            nugetConfigPath = FileTools.CreatePathRelativeToExecAssembly(nugetConfigPath);
            MethodArgsHelper.CheckFilePath(nugetPath, "NuGet");
            MethodArgsHelper.CheckFilePath(nugetConfigPath, "NuGet configuration");

            this.exePath = nugetPath;
            this.configPath = FileTools.EscapePath(nugetConfigPath);
        }

        public List<NuGetPackageInfo> List(string pkgSrc, string pkgMask)
        {
            string cmd = string.Format(
                "list {0} -Source {1} -ConfigFile {2} -Verbosity {3}",
                string.IsNullOrWhiteSpace(pkgMask) == false ? pkgMask : string.Empty,
                pkgSrc,
                FileTools.EscapePath(this.configPath),
                this.GetNuGetVerbosity());

            string response = null;
            ExecuteNugetCommand(cmd, out response);
            if (string.IsNullOrWhiteSpace(response) == true ||
                response.Contains("No packages found") == true)
                return null;

            return this.ParseListResponse(response);
        }

        public void Pack(string nuspecPath, string outDir, string overrideVersion)
        {
            string cmd = string.Format(
                "pack {0} -OutputDirectory {1} -Verbosity {2}{3}",
                FileTools.EscapePath(nuspecPath),
                FileTools.EscapePath(outDir),
                GetNuGetVerbosity(),
                string.IsNullOrWhiteSpace(overrideVersion) == false
                    ? " -Version " + overrideVersion
                    : string.Empty);

            ExecuteNugetCommand(cmd);
        }

        public void Update(string slnPath, string pkgId, string pkgSrc, bool allowPrerelease, string versionPattern)
        {
            string cmd = string.Format(
                "update {0} -Id {1} -Source {2} -ConfigFile {3} -Verbosity {4}",
                FileTools.EscapePath(slnPath), 
                pkgId, 
                pkgSrc, 
                FileTools.EscapePath(this.configPath), 
                GetNuGetVerbosity());

            if (allowPrerelease == true)
                cmd += " -Prerelease";

            if (versionPattern != null)
            {
                string version = this.RetrievePrereleasePackageVersion(
                    pkgSrc, pkgId, versionPattern);
                if (version == null)
                    throw new PackageHelperException(
                        string.Format(
                            "Failed to find a package \"{0}\" version matching pattern \"{1}\" using source \"{2}\".",
                            pkgId, versionPattern, pkgSrc));
                cmd += " -Version " + version;
            }

            ExecuteNugetCommand(cmd);
        }

        private string RetrievePrereleasePackageVersion(
            string pkgSrc, string pkgId, string versionPattern)
        {
            string cmd = string.Format(
                "list {0} -Source {1} -ConfigFile {2} -Prerelease -AllVersions -Verbosity {3} -NonInteractive",
                pkgId,
                pkgSrc,
                FileTools.EscapePath(this.configPath),
                this.GetNuGetVerbosity());

            string response = null;
            ExecuteNugetCommand(cmd, out response);
            if (string.IsNullOrWhiteSpace(response) == true ||
                response.Contains("No packages found") == true)
                return null;

            var lines = response.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            if (lines.IsEmpty() == true)
                return null;

            var pkgLineParts = lines
                .Select(x => x.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries))
                .First(x => x != null && x.Length > 1 &&
                    x[0].Trim() == pkgId &&
                    Regex.IsMatch(x[1], versionPattern) == true);

            return (pkgLineParts != null)
                ? pkgLineParts[1]
                : null;
        }

        public void Restore(string slnPath)
        {
            string cmd = string.Format(
                "restore {0} -ConfigFile {1} -Verbosity {2} -NonInteractive",
                FileTools.EscapePath(slnPath), 
                FileTools.EscapePath(this.configPath), GetNuGetVerbosity());
            ExecuteNugetCommand(cmd);
        }

        private void ExecuteNugetCommand(string cmd)
        {
            int exitCode = this.ExecuteCommand(cmd);
            if (exitCode != 0)
                throw new NuGetOperationException(
                    "Command execution ended with an error:" +
                    exitCode.ToString() + ". CommandLine: " + cmd);
        }

        private void ExecuteNugetCommand(string cmd, out string response)
        {
            int exitCode = this.ExecuteCommand(cmd, out response);
            if (exitCode != 0)
                throw new NuGetOperationException(
                    "Command execution ended with an error:" +
                    exitCode.ToString() + ". CommandLine: " + cmd);
        }

        private string GetNuGetVerbosity()
        {
            switch (this.Verbosity)
            {
                case VerbosityLevel.Quiet:
                    return "quiet";

                case VerbosityLevel.Minimal:
                case VerbosityLevel.Normal:
                default:
                    return "normal";

                case VerbosityLevel.Detailed:
                case VerbosityLevel.Diagnostic:
                    return "detailed";
            }
        }

        private List<NuGetPackageInfo> ParseListResponse(string response)
        {
            var lines = response.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            if (lines.IsEmpty() == true)
                return null;

            var pi = new List<NuGetPackageInfo>();
            foreach (var item in lines)
            {
                var parts = item.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.IsEmpty() == false)
                {
                    pi.Add(new NuGetPackageInfo()
                    {
                        Id = parts[0].Trim(),
                        Version = parts[1].Trim()
                    });
                }
            }

            return pi;
        }
    }

    class NuGetPackageInfo
    {
        public string Id { get; set; }
        public string Version { get; set; }
    }
}