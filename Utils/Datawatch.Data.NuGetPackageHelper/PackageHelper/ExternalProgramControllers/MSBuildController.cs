﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class MSBuildController : ExternalProgramControllerBase
    {
        private static readonly string[] MSBuildPaths = {
                @"C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe",         // VS2015
                @"C:\Program Files\MSBuild\14.0\Bin\MSBuild.exe",               // VS2015
                @"C:\Program Files (x86)\MSBuild\12.0\Bin\MSBuild.exe",         // VS2013
                @"C:\Program Files\MSBuild\12.0\Bin\MSBuild.exe",               // VS2013
                @"C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"    // VS2012 and VS2010
            };

        private const string BuildConfigPropertyName = "Configuration";
        private const string PlatformPropertyName = "Platform";
        private const string DefineConstantsPropertyName = "DefineConstants";
        private const string PropertyTemplate = " /p:{0}=\"{1}\"";
        private const string MaxCpuCountOption = " /maxcpucount:";
        private const string VerbosityOption = " /verbosity:";
        private const string RebuildTarget = " /t:Rebuild";


        public MSBuildController(string path, VerbosityLevel verbLevel, ILogger logger)
            : base(logger, verbLevel)
        {
            if (path.ToUpper() == "AUTO")
            {
                path = DiscoverMSBuildExecutable();
                if (path == null)
                    throw new PackageHelperException("Failed to discover MSBuild executable file.");
            }

            path = FileTools.CreatePathRelativeToExecAssembly(path);
            MethodArgsHelper.CheckFilePath(path, "MSBuild");
            this.exePath = FileTools.EscapePath(path);
        }

        public static string DiscoverMSBuildExecutable()
        {
            foreach (var path in MSBuildPaths)
            {
                if (File.Exists(path) == true)
                    return path;
            }

            return null;
        }

        public void Build(string slnPath, string platform, string config, IEnumerable<string> defineConstants)
        {
            var sb = new StringBuilder();
            sb.Append(FileTools.EscapePath(slnPath));
            sb.Append(RebuildTarget);

            if (string.IsNullOrWhiteSpace(platform) == false)
                sb.AppendFormat(PropertyTemplate, PlatformPropertyName, platform);

            if (string.IsNullOrWhiteSpace(config) == false)
                sb.AppendFormat(PropertyTemplate, BuildConfigPropertyName, config);

            sb.Append(VerbosityOption + GetMSBuildVerbosity());
            sb.Append(MaxCpuCountOption + GetCpuCount());

            if (defineConstants.IsEmpty() == false)
            {
                var constantsSequence = string.Join(" ", defineConstants);
                sb.AppendFormat(PropertyTemplate, DefineConstantsPropertyName, constantsSequence.Trim());
            }

            var cmdArgs = sb.ToString().Trim();
            int exitCode = this.ExecuteCommand(cmdArgs);
            if (exitCode != 0)
                throw new MSBuildOperationException(
                    "Command execution ended with an error:" + exitCode.ToString() + ". CommandLine: " + cmdArgs);
        }

        private string GetCpuCount()
        {
            return (Properties.Settings.Default.CpuCoresToUse <= Environment.ProcessorCount
                ? Properties.Settings.Default.CpuCoresToUse
                : Environment.ProcessorCount).ToString();
        }

        private string GetMSBuildVerbosity()
        {
            switch (this.Verbosity)
            {
                case VerbosityLevel.Quiet:
                    return "q";

                case VerbosityLevel.Minimal:
                    return "m";

                case VerbosityLevel.Normal:
                default:
                    return "n";

                case VerbosityLevel.Detailed:
                    return "d";

                case VerbosityLevel.Diagnostic:
                    return "diag";
            }
        }
    }
}