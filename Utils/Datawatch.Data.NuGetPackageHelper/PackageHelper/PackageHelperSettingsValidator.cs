﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{
    class PackageHelperSettingsValidator
    {
        private ValidationResultBuilder vrb;

        public ValidationResult ValidateSettings()
        {
            this.vrb = new ValidationResultBuilder();

            this.ValidateNuGetSettings();
            this.ValidateMSBuildSettings();
            this.ValidateCpuCount();

            return this.vrb.BuildResult();
        }

        private void ValidateNuGetSettings()
        {
            string path = Settings.Default.NuGetPath;
            this.ValidateFilePath(path, "NuGet command-line tool");
            path = Settings.Default.NuGetConfigPath;
            this.ValidateFilePath(path, "NuGet command-line tool configuration");

            int intValue = Settings.Default.NuGetVerbosity;
            if (Enum.IsDefined(typeof(VerbosityLevel), intValue) == false)
                this.vrb.AddError(
                    "Invalid NuGet verbosity value is specified - " +
                    intValue.ToString() +
                    " (expected range: 0-4).");
        }

        private void ValidateMSBuildSettings()
        {
            string path = Settings.Default.MSBuildPath;
            if (path.ToUpper() != "AUTO")
                this.ValidateFilePath(path, "MSBuild tool");

            int intValue = Settings.Default.MSBuildVerbosity;
            if (Enum.IsDefined(typeof(VerbosityLevel), intValue) == false)
                this.vrb.AddError(
                    "Invalid MSBuild verbosity value is specified - " +
                    intValue.ToString() +
                    " (expected range: 0-4).");
        }

        private void ValidateCpuCount()
        {
            int cpuCount = Settings.Default.CpuCoresToUse;
            if (cpuCount <= 0 || cpuCount > 8)
                this.vrb.AddError(
                    "Invalid number of cpu cores has been specified - " +
                    cpuCount.ToString() + ". Expected range: 1-8.");

            if (cpuCount > Environment.ProcessorCount)
            {
                this.vrb.AddWarning(
                    "The specified number of CPU cores to use ({0}) " +
                    "exceeds the number of CPU cores on this machine ({1}). " +
                    "The number of existing CPU cores will be used instead.",
                    cpuCount, Environment.ProcessorCount);
            }
        }

        private void ValidateFilePath(string path, string fileDesc)
        {
            if (string.IsNullOrWhiteSpace(path) == false)
            {
                var chkPath = FileTools.CreatePathRelativeToExecAssembly(path);
                if (FileTools.VerifyFilePath(chkPath) == false)
                    this.vrb.AddError(
                        "Path to {0} is invalid - \"{1}\".", fileDesc, path);
            }
            else
            {
                this.vrb.AddError("Path to " + fileDesc + " is not specified.");
            }
        }
    }
}
