﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{

    [Serializable]
    public class PackageHelperException : Exception
    {
        public PackageHelperException() { }
        public PackageHelperException(string message) : base(message) { }
        public PackageHelperException(string message, Exception inner) : base(message, inner) { }
        protected PackageHelperException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    [Serializable]
    public class UnknownCommandException : PackageHelperException
    {
        public UnknownCommandException() { }
        public UnknownCommandException(string message) : base(message) { }
        public UnknownCommandException(string message, Exception inner) : base(message, inner) { }
        protected UnknownCommandException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
    

    [Serializable]
    public class CommandParsingException : PackageHelperException
    {
        public CommandParsingException() { }
        public CommandParsingException(string message) : base(message) { }
        public CommandParsingException(string message, Exception inner) : base(message, inner) { }
        protected CommandParsingException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class SerializationException : PackageHelperException
    {
        public SerializationException() { }
        public SerializationException(string message) : base(message) { }
        public SerializationException(string message, Exception inner) : base(message, inner) { }
        protected SerializationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class NuGetOperationException : PackageHelperException
    {
        public NuGetOperationException() { }
        public NuGetOperationException(string message) : base(message) { }
        public NuGetOperationException(string message, Exception inner) : base(message, inner) { }
        protected NuGetOperationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class MSBuildOperationException : PackageHelperException
    {
        public MSBuildOperationException() { }
        public MSBuildOperationException(string message) : base(message) { }
        public MSBuildOperationException(string message, Exception inner) : base(message, inner) { }
        protected MSBuildOperationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class CommandExecutionException : PackageHelperException
    {
        public CommandExecutionException() { }
        public CommandExecutionException(string message) : base(message) { }
        public CommandExecutionException(string message, Exception inner) : base(message, inner) { }
        protected CommandExecutionException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }


    [Serializable]
    public class InvalidConfigurationException : PackageHelperException
    {
        public InvalidConfigurationException() { }
        public InvalidConfigurationException(string message) : base(message) { }
        public InvalidConfigurationException(string message, Exception inner) : base(message, inner) { }
        protected InvalidConfigurationException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
