﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class Program
    {
        static int Main(string[] args)
        {
            //Console.ReadKey();
            ILogger logger = new PackageHelperLogger();
            
            logger.Log(LogLevel.Debug, string.Empty);
            logger.Log(LogLevel.Debug, "---------- PACKAGE HELPER STARTED ----------");
            logger.Log(LogLevel.Debug, "Command: " + string.Join(" ", args));

            try
            {
                Console.WriteLine(GetProgramDescription());
                logger.Log(LogLevel.Debug, "Validating PackageHelper settings...");
                var validator = new PackageHelperSettingsValidator();
                var result = validator.ValidateSettings();
                if (result.HasWarnings)
                {
                    logger.Log(LogLevel.Warn, "Settings validation warnings: ");
                    logger.Log(LogLevel.Warn, result.Warnings);
                }

                if (result.HasErrors)
                {
                    logger.Log(LogLevel.Error, "Errors in PackageHelper settings: ");
                    logger.Log(LogLevel.Error, result.Errors);
                    return (int)PackageHelperReturnCodes.InvalidSettings;
                }

                logger.Log(LogLevel.Debug, "PackageHelper settings validation found no errors.");

                var helper = new PackageHelper(logger);
                if (args.Length == 0)
                {
                    Console.WriteLine(helper.GetHelpText());
                    return (int)PackageHelperReturnCodes.NoErrors; ;
                }

                helper.ProcessCommand(args);
                logger.Log(LogLevel.Debug, "---------- SUCCESS ----------");

                return (int)PackageHelperReturnCodes.NoErrors;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Fatal, 
                    "Command processing aborted due to an error: " + e.ToString());

                var fc = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Execution failed due to an error: " + e.Message);
                Console.ForegroundColor = fc;

                logger.Log(LogLevel.Debug, "---------- EXECUTION FAILED ----------");

                return (int)PackageHelperReturnCodes.ExecutionError;       
            }
        }

        enum PackageHelperReturnCodes
        {
            NoErrors = 0,
            ExecutionError = 1,
            InvalidSettings = 2
        }

        public static string GetProgramDescription()
        {
            var info = FileVersionInfo.GetVersionInfo(
                Assembly.GetEntryAssembly().Location);

            var sb = new StringBuilder();
            sb.AppendLine("Package Helper v" + info.ProductVersion);

            var description = Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyDescriptionAttribute>();
            if (description != null &&
                string.IsNullOrWhiteSpace(description.Description) == false)
                sb.AppendLine(description.Description);

            sb.AppendLine(info.LegalCopyright.Replace("©", "(c)"));
            sb.AppendLine();

            return sb.ToString();
        }

    }
}
