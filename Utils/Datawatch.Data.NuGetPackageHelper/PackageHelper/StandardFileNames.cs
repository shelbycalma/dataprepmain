﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class StandardFileNames
    {
        public const string NuGetProjectConfig = "packages.config";
        public const string RepositoryConfigurationFilename = "ph.config";
    }

    static class StandardFileExtensions
    {
        public const string NuGetSpecsExtension = "nuspec";
        public const string NuGetSpecsFileMask = "*.nuspec";
        public const string VSSolutionExtension = "sln";
        public const string VSSolutionFileMask = "*.sln";
        public const string BackupExtension = "bak";
        public const string BackupFileMask = "*.bak";
    }

    static class StandardFolders
    {
        public const string DefaultNuspecFolder = ".DebugNuspec";
        public const string DefaultPackageSourceFolder = ".DebugPackages";
    }

    static class StandardSubst
    {
        public const string Null = "<null>";
        public const string Default = "<default>";
    }
}