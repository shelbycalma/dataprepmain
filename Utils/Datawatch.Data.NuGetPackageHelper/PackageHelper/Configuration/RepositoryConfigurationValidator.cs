﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    class RepositoryConfigurationValidator
    {
        private string baseFolder;
        private ValidationResultBuilder vrb;
        private ILogger logger;


        public RepositoryConfigurationValidator(
            string baseFolder, ILogger logger)
        {
            this.baseFolder = baseFolder ?? Directory.GetCurrentDirectory();
            this.logger = logger;
            this.vrb = new ValidationResultBuilder();
        }

        public ValidationResult Validate(RepositoryConfiguration localConfig)
        {
            if (localConfig == null)
                throw new ArgumentNullException(
                    "config", "No configuration instance provided for validation.");

            vrb.Reset();

            if (string.IsNullOrWhiteSpace(localConfig.Id) == true)
            {
                if (localConfig.ExternalRepositories.IsEmpty() == false)
                {
                    this.vrb.AddError("Current repository Id must be present, if configuration references other repositories.");
                }
                else
                {
                    this.vrb.AddWarning("It is recommended to set current repository Id.");
                }
            }

            this.ValidateRepositoryReferences(localConfig);
            this.ValidateConfigData(localConfig, this.baseFolder);

            foreach (var repo in localConfig.ExternalRepositories)
            {
                var repoBaseFolder = FileTools.CombinePath(
                    this.baseFolder,
                    Path.GetDirectoryName(repo.ConfigPath));
                this.ValidateConfigData(repo.Configuration, repoBaseFolder);
            }

            return vrb.BuildResult();
        }

        public ValidationResult ValidateGlobalConfig(GlobalConfiguration config)
        {
            this.vrb.Reset();

            // Check package aliases for uniqueness
            var aliases = new HashSet<string>();
            foreach (var p in config.Packages.Values)
            {
                string pid = p.Id;
                string alias = p.Alias;
                if (string.IsNullOrWhiteSpace(alias) == false)
                {
                    if (aliases.Contains(alias) == false)
                    {
                        aliases.Add(alias);
                    }
                    else
                    {
                        this.vrb.AddError(
                            "Non-unique package alias \"{0}\", package Id = \"{1}\", repository Id = \"{2}\".{3}",
                            alias, p.Id, p.RepositoryId, Environment.NewLine);
                    }
                }
            }

            // Check solution aliases for uniqueness
            aliases.Clear();
            foreach (var s in config.Solutions.Values)
            {
                var alias = s.Alias;
                if (string.IsNullOrWhiteSpace(alias) == false)
                {
                    if (aliases.Contains(alias) == false)
                    {
                        aliases.Add(alias);
                    }
                    else
                    {
                        this.vrb.AddError(
                            "Non-unique solution alias \"{0}\", solution Id = \"{1}\", repository Id = \"{2}\".{3}",
                            alias, s.Id, s.RepositoryId, Environment.NewLine);
                    }
                }
            }

            // Check if all package references were replaced with full package info
            var invalidPkgRefs = config.Packages.Values.Where(x =>
                string.IsNullOrWhiteSpace(x.Path) == true ||
                string.IsNullOrWhiteSpace(x.SolutionId) == true);
            if (invalidPkgRefs.IsEmpty() == false)
                vrb.AddError(
                    "Main configuration has unresolved references to packages " +
                    "from other repositories: " +
                    Environment.NewLine +
                    string.Join(Environment.NewLine, invalidPkgRefs
                        .Select(x => x.ToString())));

            return vrb.BuildResult();
        }

        private void ValidateConfigData(RepositoryConfiguration config, string repoBaseFolder)
        {
            this.ValidateFolders(config, repoBaseFolder);
            this.ValidatePackages(config, repoBaseFolder);
            this.ValidateSolutions(config, repoBaseFolder);
        }

        private void ValidateCrossConfigData(RepositoryConfiguration localConfig)
        {
            if (localConfig.ExternalRepositories.IsEmpty() == true)
                return;

            var repoIds = new HashSet<string>();
            repoIds.Add(localConfig.Id);
            var pkgIds = new HashSet<string>(localConfig.Packages.Select(x => x.Id));
            var slnIds = new HashSet<string>(localConfig.Solutions.Select(x => x.Id));
            var pkgAliases = new HashSet<string>(localConfig.Packages
                .Where(x => string.IsNullOrWhiteSpace(x.Alias) == false)
                .Select(x => x.Alias));
            var slnAliases = new HashSet<string>(localConfig.Solutions
                .Where(x => string.IsNullOrWhiteSpace(x.Alias) == false)
                .Select(x => x.Alias));

            foreach (var repo in localConfig.ExternalRepositories)
            {
                string id = repo.Configuration.Id;

                var pids = repo.Configuration.Packages.Select(x => x.Id);
                foreach (var pid in pids)
                {
                    if (pkgIds.Contains(pid) == false)
                    {
                        pkgIds.Add(pid);
                    }
                    else
                    {
                        this.vrb.AddError(
                            "Non-unique package Id \"{0}\", external configuration with path \"{1}\".",
                            pid, repo.ConfigPath);
                    }
                }

                var sids = repo.Configuration.Solutions.Select(x => x.Id);
                foreach (var sid in sids)
                {
                    if (slnIds.Contains(sid) == false)
                    {
                        slnIds.Add(sid);
                    }
                    else
                    {
                        this.vrb.AddError(
                            "Non-unique solution Id \"{0}\", external configuration with path \"{1}\".",
                            sid, repo.ConfigPath);
                    }
                }

                var pas = repo.Configuration.Packages
                    .Where(x => string.IsNullOrWhiteSpace(x.Alias) == false)
                    .Select(x => x.Alias);
                foreach (var alias in pas)
                {
                    if (pkgAliases.Contains(alias) == false)
                    {
                        pkgAliases.Add(alias);
                    }
                    else
                    {
                        this.vrb.AddError(
                            "Non-unique package alias \"{0}\", external configuration with path \"{1}\".",
                            alias, repo.ConfigPath);
                    }
                }

                var sas = repo.Configuration.Solutions
                    .Where(x => string.IsNullOrWhiteSpace(x.Alias) == false)
                    .Select(x => x.Alias);
                foreach (var alias in sas)
                {
                    if (slnAliases.Contains(alias) == false)
                    {
                        slnAliases.Add(alias);
                    }
                    else
                    {
                        this.vrb.AddError(
                            "Non-unique solution alias \"{0}\", external configuration with path \"{1}\".",
                            alias, repo.ConfigPath);
                    }
                }
            }
        }

        private void ValidateRepositoryReferences(RepositoryConfiguration config)
        {
            if (config.ExternalRepositories.IsEmpty() == false)
            {
                var ids = new HashSet<string>();
                var cfgs = new HashSet<string>();
                var srcs = new HashSet<string>();

                for (int i = 0; i < config.ExternalRepositories.Length; i++)
                {
                    var repoRef = config.ExternalRepositories[i];
                    string msgTpl = "Repository reference with index \"" + i.ToString() + "\" ";

                    // Is repo id valid?
                    if (string.IsNullOrWhiteSpace(repoRef.Id) == false)
                    {
                        if (ids.Contains(repoRef.Id) == false)
                        {
                            ids.Add(repoRef.Id);
                        }
                        else
                        {
                            vrb.AddError(msgTpl + "has non-unique Id.");
                        }
                    }
                    else
                    {
                        vrb.AddError(msgTpl + "has no Id specified.");
                    }

                    // Is package source id valid (without NuGet config checking)
                    if (string.IsNullOrWhiteSpace(repoRef.PackageSourceId) == false)
                    {
                        if (srcs.Contains(repoRef.PackageSourceId) == false)
                        {
                            srcs.Add(repoRef.PackageSourceId);
                        }
                        else
                        {
                            vrb.AddError(msgTpl + "has non-unique package source Id.");
                            
                        }
                    }
                    else
                    {
                        vrb.AddError(msgTpl + "has no package source Id specified.");
                    }

                    // Is config path valid?
                    if (FileTools.VerifyFilePath(repoRef.ConfigPath) == true)
                    {
                        if (cfgs.Contains(repoRef.ConfigPath) == false)
                        {
                            cfgs.Add(repoRef.ConfigPath);
                        }
                        else
                        {
                            vrb.AddError(msgTpl + "shares the same configuration file path with other references.");
                        }
                    }
                    else
                    {
                        vrb.AddError(msgTpl + "has invalid configuration file path '" +
                            repoRef.ConfigPath ?? StandardSubst.Null + "'.");
                    }
                }
            }
        }

        private void ValidateFolders(RepositoryConfiguration config, string repoBaseFolder)
        {
            var folder = FileTools.CombinePath(repoBaseFolder, config.PackageSourcePath);
            if (string.IsNullOrWhiteSpace(folder) == false)
            {
                if (this.VerifyFolderExists(folder) == true)
                    this.logger.Log(LogLevel.Warn,
                        "Local package source folder has been restored.");
            }
            else
            {
                vrb.AddError("Path to local package source is not specified.");
            }

            folder = FileTools.CombinePath(repoBaseFolder, config.NuspecFolderPath);
            if (string.IsNullOrWhiteSpace(folder) == false)
            {
                if (Directory.Exists(folder) == false)
                    vrb.AddError("Failed to find a folder with nuspec files using path \"" +
                        folder + "\".");
                //if (this.VerifyFolderExists(folder) == true)
                //    this.logger.Log(LogLevel.Warn,
                //        "Folder for debug nuspec files has been restored.");
            }
            else
            {
                vrb.AddError("Path to a debug nuspec folder is not specified.");
            }
        }

        private bool VerifyFolderExists(string path)
        {
            if (Path.IsPathRooted(path) == false)
                path = Path.Combine(this.baseFolder, path);

            try
            {
                if (Directory.Exists(path) == false)
                {
                    Directory.CreateDirectory(path);
                    return true;
                }
            }
            catch (Exception e)
            {
                this.logger.Log(LogLevel.Error,
                    "Failed to create folder \"{0}\".{1}Error: {2}.",
                    path, Environment.NewLine, e.Message);
            }

            return false;
        }

        private bool CheckFilePath(string relPath)
        {
            var path = Path.IsPathRooted(relPath) == false
                ? Path.Combine(this.baseFolder, relPath)
                : relPath;

            return FileTools.VerifyFilePath(path);
        }

        private void ValidatePackages(RepositoryConfiguration config, string repoBaseFolder)
        {
            if (config.Packages == null)
            {
                vrb.AddError("Configuration contains no information on packages.");
                return;
            }

            HashSet<string> solutions = config.Solutions != null && config.Solutions.Any()
                ? new HashSet<string>(config.Solutions.Select(x => x.Id))
                : null;

            HashSet<string> aliases = new HashSet<string>();

            for (int i = 0; i < config.Packages.Length; i++)
            {
                string msgTpl = "Package with index " + i.ToString();
                var pkg = config.Packages[i];
                if (string.IsNullOrWhiteSpace(pkg.Id) == true)
                    vrb.AddError(msgTpl + " has invalid Id");

                if (pkg.RepositoryId == config.Id || pkg.RepositoryId == null)
                {
                    var path = FileTools.CombinePath(repoBaseFolder, pkg.Path);
                    if (this.CheckFilePath(path) == false)
                        vrb.AddError(msgTpl + " has invalid path to nuspec file.");

                    if (string.IsNullOrWhiteSpace(pkg.SolutionId) == true)
                    {
                        vrb.AddError(msgTpl + " has invalid solution Id value.");
                    }
                    else if (solutions != null && solutions.Contains(pkg.SolutionId) == false)
                        vrb.AddError(msgTpl + " references unknown solution - \"" + pkg.SolutionId + "\".");

                    if (string.IsNullOrWhiteSpace(pkg.Alias) == false &&
                        aliases.Add(pkg.Alias) == false)
                        vrb.AddError(msgTpl + " has non-unique alias - \"" + pkg.Alias + "\".");

                    BuildPlatforms platforms;
                    if (BuildPlatformsTools.TryParsePlatforms(pkg.RequiredPlatforms, out platforms) == false)
                    {
                        vrb.AddError("Invalid platform set specified - \"" +
                            pkg.RequiredPlatforms ?? StandardSubst.Null + "\".");
                    }
                }
                else
                {
                    if (config.ExternalRepositories.Any(x => x.Id == pkg.RepositoryId) == false)
                        vrb.AddError(msgTpl + " references unknown external repository - \"" + pkg.RepositoryId + "\".");
                }
            }
        }

        private void ValidateSolutions(RepositoryConfiguration config, string repoBaseFolder)
        {
            if (config.Solutions == null)
            {
                vrb.AddError("Configuration contains no information on solutions.");
                return;
            }

            HashSet<string> packages = config.Packages != null && config.Packages.Any()
                ? new HashSet<string>(config.Packages.Select(x => x.Id))
                : null;

            HashSet<string> aliases = new HashSet<string>();

            for (int i = 0; i < config.Solutions.Length; i++)
            {
                string msgTpl = "Solution with index " + i.ToString();
                var sln = config.Solutions[i];

                if (string.IsNullOrWhiteSpace(sln.Id) == true)
                    vrb.AddError(msgTpl + " has invalid Id");

                var path = FileTools.CombinePath(repoBaseFolder, sln.Path);
                if (this.CheckFilePath(path) == false)
                    vrb.AddError(msgTpl + " has invalid path to sln file.");

                if ((sln.Dependencies == null || sln.Dependencies.Any() == false) &&
                    sln.ProducedPackages == null || sln.ProducedPackages.Any() == false)
                {
                    vrb.AddWarning(msgTpl +
                        " has no dependencies and produces no packages. It is possibly useless for PackageHelper operations.");
                }

                if (sln.DefaultBuildInfo == null)
                {
                    vrb.AddWarning(msgTpl + " has no default build info. It is recommended to specify it.");
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(sln.DefaultBuildInfo.Configuration) == true)
                        vrb.AddError(msgTpl + " has invalid defaultConfiguration value in buildInfo.");

                    if (string.IsNullOrWhiteSpace(sln.DefaultBuildInfo.Platform) == true)
                        vrb.AddError(msgTpl + " has invalid defaultPlatform value in buildInfo.");
                }

                if (packages != null)
                {
                    if (sln.Dependencies != null)
                        foreach (var item in sln.Dependencies.Where(x => packages.Contains(x) == false))
                            vrb.AddError(msgTpl + " depends on unknown package - \"" + item + "\".");

                    if (sln.ProducedPackages != null)
                        foreach (var item in sln.ProducedPackages.Where(x => packages.Contains(x) == false))
                            vrb.AddError(msgTpl + " produces unknown package - \"" + item + "\".");

                    if (string.IsNullOrWhiteSpace(sln.Alias) == false && aliases.Add(sln.Alias) == false)
                        vrb.AddError(msgTpl + " has non-unique alias - \"" + sln.Alias + "\".");
                }
            }
        }
    }
}