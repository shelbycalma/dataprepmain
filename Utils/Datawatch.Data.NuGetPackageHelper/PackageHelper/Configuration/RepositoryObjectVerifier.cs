﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    class RepositoryObjectVerifier : IRepositoryObjectVerifier
    {
        private GlobalConfiguration config;

        public RepositoryObjectVerifier(GlobalConfiguration config)
        {
            if (config == null)
                throw new ArgumentNullException("config");

            this.config = config;
        }

        public GlobalConfiguration Configuration { get { return this.config; } }

        public bool IsReferencedPackage(string packageName)
        {
            return this.Configuration.Solutions.Values.Any(
                x => x.Dependencies != null && x.Dependencies.Any(
                    c => string.CompareOrdinal(packageName, c) == 0) == true);
        }

        public bool IsProducedPackage(string packageName)
        {
            return this.Configuration.Solutions.Values.Any(
                x => x.ProducedPackages.Any(
                    c => string.CompareOrdinal(packageName, c) == 0) == true);
        }

        public bool IsProducedAndReferencedPackage(string packageName)
        {
            return 
                this.Configuration.Solutions.Values.Any(
                    x => x.ProducedPackages.Any(
                        c => string.CompareOrdinal(packageName, c) == 0) == true) &&
                this.Configuration.Solutions.Values.Any(
                    x => x.Dependencies.Any(
                        c => string.CompareOrdinal(packageName, c) == 0) == true);
        }

        public bool IsPackage(string packageName)
        {
            return this.Configuration.Packages.ContainsKey(packageName);
        }

        public bool AreProducedByTheSameSolution(string[] packages)
        {
            if (packages.Length == 1)
                return true;

            return packages
                .Select(x => this.Configuration.Packages.Values
                                .Where(p => string.CompareOrdinal(p.Id, x) == 0)
                                .Select(p => p.SolutionId)
                                .First())
                .Distinct()
                .Count() == 1;
        }

        public bool IsSolution(string solutionName)
        {
            return this.Configuration.Solutions.ContainsKey(solutionName);
        }

        public bool IsSolutionAlias(string name)
        {
            return this.Configuration.Solutions.Values.Any(
                x => string.CompareOrdinal(x.Alias, name) == 0);
        }

        public bool IsSolutionAlias(string name, out string slnId)
        {
            var sln = this.Configuration.Solutions.Values
                .FirstOrDefault(x => string.CompareOrdinal(x.Alias, name) == 0);
            slnId = sln != null ? sln.Id : null;
            return sln != null;
        }

        public bool IsPackageAlias(string name)
        {
            return this.Configuration.Packages.Values.Any(
                x => string.CompareOrdinal(x.Alias, name) == 0);
        }

        public bool IsPackageAlias(string name, out string pkgId)
        {
            var pkg = this.Configuration.Packages.Values
                .FirstOrDefault(x => string.CompareOrdinal(x.Alias, name) == 0);
            pkgId = pkg != null ? pkg.Id : null;
            return pkg != null;
        }
    }
}