﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    class RepositoryAnalyzer
    {
        private HashSet<string> foldersToIgnore;
        private ILogger logger;

        /// <summary>
        /// Identifiers of packages built within solution with given id.
        /// </summary>
        private Dictionary<string, string[]> solutionPackages;
        private Dictionary<string, string[]> solutionOnPackageDependencies;
        private Dictionary<string, string[]> solutionOnSolutionDependencies;
        private Dictionary<string, SolutionInfo> solutionsMapById;
        private Dictionary<string, PackageInfo> packagesMapById;

        public RepositoryAnalyzer(ILogger logger)
        {
            this.logger = logger;
        }

        public IEnumerable<SolutionInfo> Solutions { get { return this.solutionsMapById.Values; } }
        public IEnumerable<PackageInfo> Packages { get { return this.packagesMapById.Values; } }

        public void RunInitialAnalysis(string basePath, IEnumerable<string> skipList)
        {
            this.foldersToIgnore = skipList != null
                ? new HashSet<string>(
                    skipList.Select(x => FileTools.CombinePath(basePath, x)))
                : null;

            var solutions = this.DetectSolutions(basePath);
            if (solutions == null || solutions.Any() == false)
                throw new PackageHelperException("No solutions found.");

            var packages = DetectPackages(basePath, solutions);
            if (packages == null || packages.Any() == false)
                throw new PackageHelperException("No package nuspec files found.");

            PreBuildDataStructures(false, solutions, packages);
            GetDependenciesForSolutions();
            DetermineBuildSequence();
        }

        public void RunAnalysis(IEnumerable<SolutionInfo> solutions, IEnumerable<PackageInfo> packages)
        {
            PreBuildDataStructures(true, solutions, packages);
            DetermineBuildSequence();
        }

        private void DetermineBuildSequence()
        {
            var slns = new HashSet<string>(this.solutionOnSolutionDependencies.Keys);
            var processed = new HashSet<string>();

            int buildStep = 0;
            while (slns.Any() == true)
            {
                var stepSolutions = slns
                    .Where(x => this.solutionOnSolutionDependencies[x] == null || 
                            this.solutionOnSolutionDependencies[x].All(c => processed.Contains(c)))
                    .ToArray();
                if (stepSolutions == null || stepSolutions.Any() == false || buildStep > this.solutionsMapById.Count)
                    throw new PackageHelperException("Unexpected solution data state.");

                foreach (var item in stepSolutions)
                {
                    this.solutionsMapById[item].BuildStep = buildStep;
                    processed.Add(item);
                    slns.Remove(item);
                }

                buildStep++;
            }
        }

        private List<SolutionInfo> DetectSolutions(string basePath)
        {
            var files = Directory.GetFiles(
                basePath, StandardFileExtensions.VSSolutionFileMask, SearchOption.AllDirectories);
            if (files.Any() == false)
                return null;

            var slns = new List<SolutionInfo>();
            foreach (var item in files)
            {
                slns.Add(new SolutionInfo()
                {
                    Id = Path.GetFileNameWithoutExtension(item),
                    Path = item.Remove(0, basePath.Length + 1),
                    DefaultBuildInfo = BuildOptionsGenerator.GetDefaultBuildInfo()
                });
            }

            return slns;
        }

        private List<PackageInfo> DetectPackages(string basePath, IEnumerable<SolutionInfo> solutions)
        {
            var packages = new List<PackageInfo>();

            foreach (var item in solutions)
            {
                var path = Path.Combine(basePath, Path.GetDirectoryName(item.Path));
                var slnPkgs = this.DetectSolutionPackages(path, item.Id);
                if (slnPkgs == null)
                    continue;
                packages.AddRange(slnPkgs);
                //item.ProducedPackages = slnPkgs.Select(x => x.Id).ToArray();
            }

            // Trying to correct package-solution relations for the case when all nuspecs 
            // are placed to a common folder and package name matches solution name.
            foreach (var s in solutions)
            {
                string slnToken = s.Id.ToUpper();
                foreach (var p in packages)
                {
                    if (p.Id.ToUpper() == slnToken)
                        p.SolutionId = s.Id;
                }
            }

            foreach (var s in solutions)
            {
                s.ProducedPackages = packages
                    .Where(p => p.SolutionId == s.Id)
                    .Select(x => x.Id).ToArray();
            }

            return packages;
        }

        private List<PackageInfo> DetectSolutionPackages(string slnBasePath, string slnId)
        {
            IEnumerable<string> files = Directory.GetFiles(
                slnBasePath, StandardFileExtensions.NuGetSpecsFileMask, SearchOption.AllDirectories);

            if (foldersToIgnore != null)
                files = files.Where(f =>
                    this.foldersToIgnore
                        .Any(d => f.StartsWith(d, StringComparison.CurrentCulture) == true) == false );

            if (files.Any() == false)
                return null;

            var slnPkgs = new List<PackageInfo>();
            foreach (var filePath in files)
            {
                string id = GetPackageId(filePath);
                if (id == null)
                {
                    logger.Log(LogLevel.Debug, "No package id found in file " + filePath + ". Skipping ...");
                    continue;
                }

                var pkgInfo = new PackageInfo()
                {
                    Id = id,
                    Path = filePath,
                    SolutionId = slnId
                };

                slnPkgs.Add(pkgInfo);
            }

            return slnPkgs;
        }

        private string GetPackageId(string nuspecPath)
        {
            if (string.IsNullOrWhiteSpace(nuspecPath) == true)
                throw new ArgumentException("Invalid path to nuspec file.");

            var doc = XDocument.Load(nuspecPath);

            // Search ignoring namespaces
            var p = doc.GetElementByLocalName("package");
            if (p == null)
                return null;
            var m = p.GetElementByLocalName("metadata");
            if (m == null)
                return null;

            return m.GetElementByLocalName("id").Value;
        }

        private void PreBuildDataStructures(
            bool allData, IEnumerable<SolutionInfo> solutions, IEnumerable<PackageInfo> packages)
        {
            this.solutionPackages = packages.GroupBy(x => x.SolutionId, x => x.Id)
                .ToDictionary(x => x.Key, x => x.ToArray());
            this.solutionsMapById = solutions.Where(x => this.solutionPackages.ContainsKey(x.Id))
                .ToDictionary(x => x.Id, x => x);
            this.packagesMapById = packages.ToDictionary(x => x.Id, x => x);

            if (allData == true)
            {
                this.solutionOnPackageDependencies =
                    solutions.ToDictionary(x => x.Id, x => x.Dependencies);
                this.solutionOnSolutionDependencies =
                    solutions.ToDictionary(x => x.Id,
                        x => (x.Dependencies != null)
                            ? x.Dependencies
                                .Select(p => this.packagesMapById[p].SolutionId)
                                .Distinct()
                                .ToArray()
                            : new string[0]);
            }
        }

        private void GetDependenciesForSolutions()
        {
            this.solutionOnPackageDependencies = new Dictionary<string, string[]>();
            this.solutionOnSolutionDependencies = new Dictionary<string, string[]>();

            foreach (var sln in this.solutionsMapById)
            {
                var pkgs = GetSolutionDependenciesOnPackages(sln.Value.Path);
                if (pkgs == null || pkgs.Any() == false)
                {
                    this.solutionOnPackageDependencies.Add(sln.Key, null);
                    this.solutionOnSolutionDependencies.Add(sln.Key, null);
                    continue;
                }

                sln.Value.Dependencies = pkgs;               

                this.solutionOnPackageDependencies.Add(sln.Key, pkgs);
                var slns = pkgs.Where(x => this.packagesMapById.ContainsKey(x))
                    .Select(x => this.packagesMapById[x].SolutionId)
                    .Distinct()
                    .ToArray();
                this.solutionOnSolutionDependencies.Add(sln.Key, slns);
            }
        }

        private string[] GetSolutionDependenciesOnPackages(string slnPath)
        {
            var projects = ExtractProjectPathesFromSolution(slnPath);

            // We assume that if there are no C# projects, there are no packages
            if (projects == null || projects.Any() == false)
            {
                this.logger.Log(LogLevel.Info, "Skipping solution " + slnPath + " as it has no C# projects");
                return null;
            }

            string slnBasePath = Path.GetDirectoryName(slnPath);
            var slnPkgs = new HashSet<string>();

            foreach (var prj in projects)
            {
                var path = Path.Combine(slnBasePath,
                    Path.GetDirectoryName(prj),
                    StandardFileNames.NuGetProjectConfig);

                if (File.Exists(path) == false)
                {
                    this.logger.Log(
                        LogLevel.Info, "File \"" + path + "\" does not exist or is inaccessible.");
                    continue;
                }

                try
                {
                    ExtractProjectDependencies(path, slnPkgs);
                }
                catch (Exception e)
                {
                    this.logger.Log(LogLevel.Warn, "Failed to getg NuGet dependencies from " + path);
                    this.logger.Log(LogLevel.Error, "Error: " + e.ToString());
                }
            }

            return slnPkgs.ToArray();
        }

        private string[] ExtractProjectPathesFromSolution(string slnPath)
        {
            if (string.IsNullOrEmpty(slnPath) == true)
                throw new ArgumentException("Invalid path to solution.");

            if (File.Exists(slnPath) == false)
                throw new ArgumentException("Solution file does not exist or is inaccessible.");

            string prjRegex = @"Project\(""\{[A-F0-9-]+\}""\)\s*=\s*"".*"",\s*""(.*csproj)"",";
            using (var fs = File.OpenRead(slnPath))
            using (var reader = new StreamReader(fs))
            {
                var text = reader.ReadToEnd();
                var matches = Regex.Matches(text, prjRegex);
                if (matches.Count == 0)
                    return null;

               string[] projects = matches.Cast<Match>()
                    .Where(x => x.Groups.Count == 2)
                    .Select(x => x.Groups[1].Value)
                    .ToArray();

                return projects;
            }
        }

        private void ExtractProjectDependencies(string path, HashSet<string> slnPkgs)
        {
            var doc = XDocument.Load(path);
            var root = doc.GetElementByLocalName("packages");
            if (root == null)
                return;

            var pkgs = root.GetElementsByLocalName("package")
                .Select(x => x.Attribute("id").Value)
                .Where(x => this.packagesMapById.ContainsKey(x) == true &&
                            slnPkgs.Contains(x) == false);

            if (pkgs != null && pkgs.Any() == true)
                foreach (var item in pkgs)
                {
                    slnPkgs.Add(item);
                }
        }
    }
}