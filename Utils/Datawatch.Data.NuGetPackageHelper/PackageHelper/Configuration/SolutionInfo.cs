﻿using System.Xml.Serialization;

namespace Datawatch.Data.Utils.PackageHelper
{
    [XmlRoot(ElementName = "solution", Namespace = "", IsNullable = false)]
    public class SolutionInfo
    {
        private string alias;
        private int buildStep;
        private string[] dependencies;
        private string id;
        private string path;
        private string[] producedPackages;

        [XmlAttribute("id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        [XmlAttribute("alias")]
        public string Alias
        {
            get { return alias; }
            set { alias = value; }
        }

        [XmlAttribute("path")]
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        [XmlAttribute("buildStep")]
        public int BuildStep
        {
            get { return buildStep; }
            set { buildStep = value; }
        }

        private BuildInfo defaultBuildInfo;

        [XmlElement("buildInfo")]
        public BuildInfo DefaultBuildInfo
        {
            get { return defaultBuildInfo; }
            set { defaultBuildInfo = value; }
        }
        
        [XmlArray("dependencies")]
        [XmlArrayItem("packageRef")]
        public string[] Dependencies
        {
            get { return dependencies; }
            set { dependencies = value; }
        }

        [XmlArray("producedPackages")]
        [XmlArrayItem("packageRef")]
        public string[] ProducedPackages
        {
            get { return producedPackages; }
            set { producedPackages = value; }
        }

        [XmlIgnore]
        public string RepositoryId { get; set; }

        public override string ToString()
        {
            return string.Format("Id = {0}, Alias = {1} Path = {2}, BuildStep = {3}", this.Id, this.Alias ?? "<NONE>", this.Path, this.BuildStep);
        }
    }
}