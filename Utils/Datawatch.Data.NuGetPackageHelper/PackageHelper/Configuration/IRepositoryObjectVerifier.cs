﻿namespace Datawatch.Data.Utils.PackageHelper
{
    interface IRepositoryObjectVerifier
    {
        bool IsReferencedPackage(string packageName);
        bool IsProducedPackage(string packageName);
        bool IsPackage(string packageName);
        bool IsSolution(string solutionName);
        bool AreProducedByTheSameSolution(string[] packages);
        bool IsSolutionAlias(string name);
        bool IsSolutionAlias(string name, out string slnId);
        bool IsPackageAlias(string name);
        bool IsPackageAlias(string name, out string pkgId);

    }
}