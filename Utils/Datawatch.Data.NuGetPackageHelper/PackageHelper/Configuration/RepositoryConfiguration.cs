﻿using System.Xml.Serialization;

namespace Datawatch.Data.Utils.PackageHelper
{
    [XmlRoot(ElementName = "repository", Namespace = "", IsNullable = false)]
    public class RepositoryConfiguration
    {
        private string nuspecFolderPath;
        private PackageInfo[] packages;
        private string packageSourcePath;
        private SolutionInfo[] solutions;
        private string id;
        private RepositoryReference[] externalRepositories;

        [XmlAttribute("id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        [XmlElement("nuspecFolderPath")]
        public string NuspecFolderPath
        {
            get { return nuspecFolderPath; }
            set { nuspecFolderPath = value; }
        }

        [XmlElement("packageSourcePath")]
        public string PackageSourcePath
        {
            get { return packageSourcePath; }
            set { packageSourcePath = value; }
        }

        [XmlArray("solutions")]
        [XmlArrayItem("solution")]
        public SolutionInfo[] Solutions
        {
            get { return solutions; }
            set { solutions = value; }
        }

        [XmlArray("packages")]
        [XmlArrayItem("package")]
        public PackageInfo[] Packages
        {
            get { return packages; }
            set { packages = value; }
        }

        [XmlArray("externalRepositories")]
        [XmlArrayItem("repositoryRef")]
        public RepositoryReference[] ExternalRepositories
        {
            get { return externalRepositories; }
            set { externalRepositories = value; }
        }

        [XmlIgnore]
        public string BasePath { get; set; }

        [XmlIgnore]
        public string PackageSourceId { get; set; }
    }
}
