﻿using System.Xml.Serialization;

namespace Datawatch.Data.Utils.PackageHelper
{
    [XmlRoot(ElementName = "buildInfo")]
    public class BuildInfo
    {
        private string configuration;
        private bool ignoreOtherPlatforms;
        private string platform;

        [XmlAttribute("defaultConfig")]
        public string Configuration
        {
            get { return configuration; }
            set { configuration = value; }
        }

        [XmlAttribute("ignoreOtherPlatforms")]
        public bool IgnoreOtherPlatforms
        {
            get { return ignoreOtherPlatforms; }
            set { ignoreOtherPlatforms = value; }
        }

        [XmlAttribute("defaultPlatform")]
        public string Platform
        {
            get { return platform; }
            set { platform = value; }
        }

        public BuildPlatforms GetDefaultBuildPlatform()
        {
            return BuildPlatformsTools.ParsePlatforms(this.Platform);
        }
    }
}