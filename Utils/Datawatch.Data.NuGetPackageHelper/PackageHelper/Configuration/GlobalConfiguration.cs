﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class GlobalConfiguration
    {
        private Dictionary<string, PackageInfo> packages;

        private Dictionary<string, PackageSource> packageSources;

        private Dictionary<string, SolutionInfo> solutions;

        private Dictionary<string, RepositoryConfiguration> repositoryConfigurations;

        public GlobalConfiguration(RepositoryConfiguration localConfig)
        {
            this.LocalRepositoryConfiguration = localConfig;
            this.repositoryConfigurations = new Dictionary<string, RepositoryConfiguration>();
            this.repositoryConfigurations.Add(localConfig.Id, localConfig);
            this.solutions = new Dictionary<string, SolutionInfo>();
            this.packages = new Dictionary<string, PackageInfo>();
            this.packageSources = new Dictionary<string, PackageSource>();
        }
        public RepositoryConfiguration LocalRepositoryConfiguration { get; private set; }

        public IReadOnlyDictionary<string, PackageInfo> Packages
        {
            get
            {
                return packages;
            }
        }

        public IReadOnlyDictionary<string, PackageSource> PackageSources
        {
            get
            {
                return packageSources;
            }
        }
        
        public IReadOnlyDictionary<string, RepositoryConfiguration> RepositoryConfigurations
        {
            get
            {
                return repositoryConfigurations;
            }
        }

        public IReadOnlyDictionary<string, SolutionInfo> Solutions
        {
            get
            {
                return solutions;
            }
        }

        public void AddConfiguration(RepositoryConfiguration config)
        {
            if (this.repositoryConfigurations.ContainsKey(config.Id) == false)
            {
                this.repositoryConfigurations.Add(config.Id, config);
            }
            else
            {
                throw new ArgumentException(
                    string.Format(
                        "Non-unique external configuration Id \"{0}\", path \"{1}\".{2}",
                        config.Id, config.BasePath, Environment.NewLine));
            }
        }

        public void AddPackage(PackageInfo pkg)
        {
            if (this.packages.ContainsKey(pkg.Id) == false)
            {
                this.packages.Add(pkg.Id, pkg);
                return;
            }

            if (pkg.IsExternal == true)
            {
                // if pkg is just a package reference, ignore it
                return;
            }
            else if (this.packages[pkg.Id].IsExternal == false)
            {
                // if pkg and registered packages are not references to external package,
                // package uniqueness is violated.
                throw new InvalidConfigurationException(
                    "Package Id \"" + pkg.Id + "\" is not unique.");
            }

            // Replacing package references
            this.packages[pkg.Id] = pkg;
        }

        public void AddPackageSource(PackageSource pkgSrc)
        {
            if (this.PackageSources.ContainsKey(pkgSrc.RepositoryId) == false)
            {
                this.packageSources.Add(pkgSrc.RepositoryId, pkgSrc);
                return;
            }

            throw new InvalidConfigurationException(
                "Package source Id \"" + pkgSrc.Id + "\" is not unique.");
        }

        public void AddSolution(SolutionInfo sln)
        {
            if (this.solutions.ContainsKey(sln.Id) == false)
            {
                this.solutions.Add(sln.Id, sln);
                return;
            }

            throw new InvalidConfigurationException(
                "Solution Id \"" + sln.Id + "\" is not unique.");
        }
    }
}
