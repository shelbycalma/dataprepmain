﻿using System.Xml.Serialization;

namespace Datawatch.Data.Utils.PackageHelper
{
    [XmlRoot(ElementName = "externalRepository", IsNullable = true, Namespace = "")]
    public class RepositoryReference
    {
        private string configPath;
        private string packageSourceId;
        private string id;

        [XmlAttribute("id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }


        [XmlAttribute("configPath")]
        public string ConfigPath
        {
            get { return configPath; }
            set { configPath = value; }
        }

        [XmlAttribute("packageSourceId")]
        public string PackageSourceId
        {
            get { return packageSourceId; }
            set { packageSourceId = value; }
        }

        [XmlIgnore]
        public RepositoryConfiguration Configuration { get; set; }
    }
}