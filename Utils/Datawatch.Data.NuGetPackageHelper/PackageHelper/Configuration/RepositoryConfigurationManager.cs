﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    class RepositoryConfigurationManager
    {
        private ILogger logger;
        private RepositoryAnalyzer analyzer;
        private RepositoryConfigurationValidator validator;

        public GlobalConfiguration Configuration { get; private set; }

        public string WorkingFolder { get; set; }

        public RepositoryConfigurationManager(ILogger logger)
        {
            this.logger = logger;
            this.ConfigurationPath =
                Path.Combine(Directory.GetCurrentDirectory(),
                    StandardFileNames.RepositoryConfigurationFilename);

            this.WorkingFolder = Directory.GetCurrentDirectory();
        }

        public string ConfigurationPath { get; private set; }

        public GlobalConfiguration LoadConfiguration(string configPath = null)
        {
            this.logger.Log(LogLevel.Info, "Loading configuration...");

            string cfgPath = null;
            string offset = string.Empty;
            if (string.IsNullOrWhiteSpace(configPath) == true)
            {
                cfgPath = this.ConfigurationPath;
            }
            else
            {
                offset = Path.GetDirectoryName(configPath);
                this.WorkingFolder = FileTools.CombinePath(this.WorkingFolder, offset);
                cfgPath = FileTools.CombinePath(Directory.GetCurrentDirectory(), configPath);
            }

            try
            {
                if (File.Exists(cfgPath) == false)
                {
                    this.logger.Log(LogLevel.Warn,
                        "Failed to find configuration file \"" + cfgPath + "\"");
                    return null;
                }

                RepositoryConfiguration config = null;
                using (var fs = File.OpenRead(cfgPath))
                using (var reader = new StreamReader(fs))
                {
                    var data = reader.ReadToEnd();
                    config = Serializer.Deserialize<RepositoryConfiguration>(data);
                }

                config.BasePath = this.WorkingFolder;
                config.PackageSourceId = config.Id + "Debug";

                this.logger.Log(LogLevel.Info,
                    "Configuration has been successfully loaded from \"" +
                    Path.GetFullPath(cfgPath) + "\".");

                var globalConfig = new GlobalConfiguration(config);

                foreach (var repoRef in config.ExternalRepositories)
                {
                    repoRef.ConfigPath = FileTools.CombinePath(this.WorkingFolder, repoRef.ConfigPath);
                    config = this.LoadExternalRepositoryConfiguration(repoRef.ConfigPath);

                    if (config != null)
                    {
                        config.BasePath = Path.GetDirectoryName(repoRef.ConfigPath);
                        config.PackageSourceId = repoRef.PackageSourceId;
                        repoRef.Configuration = config;
                        globalConfig.AddConfiguration(config);
                    }
                }

                this.validator = new RepositoryConfigurationValidator(
                    this.WorkingFolder, this.logger);

                this.ValidateConfiguration(globalConfig.LocalRepositoryConfiguration);
                this.InitializeGlobalConfiguration(globalConfig);
                this.ValidateGlobalConfiguration(globalConfig);
                this.Configuration = globalConfig;
                return globalConfig;
            }
            catch (Exception e)
            {
                this.logger.Log(LogLevel.Fatal,
                    "Failed to load configuration from \"" + cfgPath +
                    "\". Error: " + e.Message);
                throw;
            }
        }

        private void ValidateConfiguration(RepositoryConfiguration config)
        {
            var result = this.validator.Validate(config);
            if (string.IsNullOrEmpty(result.Errors) == false)
            {
                this.logger.Log(LogLevel.Fatal,
                    "Repository configuration has failed validation. Errors: " +
                    Environment.NewLine + result.Errors);
            }

            if (string.IsNullOrWhiteSpace(result.Warnings) == false)
            {
                this.logger.Log(LogLevel.Warn,
                    "Repository configuration has validation warnings: " +
                    Environment.NewLine + result.Warnings);
            }

            if (result.CanWork == false)
                throw new InvalidConfigurationException(
                    "The specified repository configuration is invalid: " +
                    Environment.NewLine + result.Errors +
                    Environment.NewLine + result.Warnings);
        }

        private void ValidateGlobalConfiguration(GlobalConfiguration config)
        {
            var result = this.validator.ValidateGlobalConfig(config);
            if (string.IsNullOrEmpty(result.Errors) == false)
            {
                this.logger.Log(LogLevel.Fatal,
                    "Global configuration has failed validation. Errors: " +
                    Environment.NewLine + result.Errors);
            }

            if (string.IsNullOrWhiteSpace(result.Warnings) == false)
            {
                this.logger.Log(LogLevel.Warn,
                    "Global configuration has validation warnings: " +
                    Environment.NewLine + result.Warnings);
            }

            if (result.CanWork == false)
                throw new InvalidConfigurationException(
                    "Global configuration is invalid: " +
                    Environment.NewLine + result.Errors +
                    Environment.NewLine + result.Warnings);
        }

        private void InitializeGlobalConfiguration(GlobalConfiguration gcfg)
        {
            if (gcfg.LocalRepositoryConfiguration == null ||
                gcfg.RepositoryConfigurations.IsEmpty() == true)
                throw new InvalidOperationException("Failed to initialize global configuration - repository configurations are missing.");


            foreach (var cfg in gcfg.RepositoryConfigurations.Values)
            {
                var rid = cfg.Id;
                var isLocalConfig = cfg.Id == gcfg.LocalRepositoryConfiguration.Id;
                var repoDir = cfg.BasePath;
                // var repoDir = isLocalConfig == false 
                // ? cfg.BasePath : this.WorkingDirectory;

                var pkgSrc = new PackageSource()
                {
                    Id = cfg.PackageSourceId,
                    Path = FileTools.CombinePath(repoDir, cfg.PackageSourcePath),
                    RepositoryId = rid
                };

                gcfg.AddPackageSource(pkgSrc);

                foreach (var sln in cfg.Solutions)
                {
                    sln.RepositoryId = rid;
                    if (isLocalConfig == false)
                        sln.Path = FileTools.CombinePath(repoDir, sln.Path);

                    gcfg.AddSolution(sln);
                }

                //var repoPkgs = cfg.Packages.Where(p => p.IsExternal == false);
                foreach (var pkg in cfg.Packages)
                {
                    pkg.RepositoryId = rid;
                    if (pkg.IsExternal == false)
                        pkg.Path = FileTools.CombinePath(repoDir, pkg.Path);
                    gcfg.AddPackage(pkg);
                }
            }

            this.analyzer = new RepositoryAnalyzer(this.logger);
            analyzer.RunAnalysis(gcfg.Solutions.Values, gcfg.Packages.Values);

        }

        protected RepositoryConfiguration LoadExternalRepositoryConfiguration(string configPath)
        {
            this.logger.Log(LogLevel.Info,
                "Loading external repository configuration from \"{0}\"...",
                configPath);

            try
            {
                if (File.Exists(configPath) == false)
                {
                    throw new PackageHelperException(
                    //this.logger.Log(LogLevel.Warn,
                        "Failed to find external repository configuration file \"" + configPath + "\"");
                }

                RepositoryConfiguration config = null;

                using (var fs = File.OpenRead(configPath))
                using (var reader = new StreamReader(fs))
                {
                    var data = reader.ReadToEnd();
                    config = Serializer.Deserialize<RepositoryConfiguration>(data);
                }

                this.logger.Log(LogLevel.Info,
                    "External repository configuration has been successfully loaded from \"" +
                    configPath + "\".");

                return config;
            }
            catch (Exception e)
            {
                this.logger.Log(LogLevel.Fatal,
                    "Failed to load external repository configuration from \"" +
                    configPath + "\". Error: " + e.Message);
                throw;
            }
        }

        public void SaveConfiguration(RepositoryConfiguration config)
        {
            this.logger.Log(LogLevel.Info, "Saving configuration...");

            var path = this.ConfigurationPath;
            try
            {
                if (FileTools.BackupFileIfExists(path) == true)
                    this.logger.Log(LogLevel.Info,
                        "A backup of the previous configuration has been created.");

                using (var fs = File.Create(path))
                using (var writer = new StreamWriter(fs))
                {
                    writer.Write(Serializer.Serialize<RepositoryConfiguration>(config));
                    writer.Flush();
                }

                this.logger.Log(LogLevel.Info, "Configuration has been saved to \"" + path + "\"");
            }
            catch (Exception e)
            {
                this.logger.Log(LogLevel.Fatal,
                    "Failed to save configuration to \"" + path + "\". : " + e.Message);
                throw;
            }
        }

        public RepositoryConfiguration CreateConfigForRepository(string nuspecDir, string pkgDir, string repoId = null)
        {
            string path = this.WorkingFolder;
            this.logger.Log(LogLevel.Info, "Base directory: " + path);

            try
            {
                var cfg = new RepositoryConfiguration();
                cfg.Id = string.IsNullOrEmpty(repoId) == true
                    ? path.Split(FileTools.PathSeparators, StringSplitOptions.RemoveEmptyEntries)
                        .Last()
                        .Replace(" ", string.Empty)
                    : repoId;
    
                cfg.NuspecFolderPath = nuspecDir;
                cfg.PackageSourcePath = pkgDir;

                if (this.analyzer == null)
                    this.analyzer = new RepositoryAnalyzer(this.logger);

                this.analyzer.RunInitialAnalysis(path, new string[] { nuspecDir, pkgDir });
                cfg.Solutions = this.analyzer.Solutions.ToArray();
                cfg.Packages = this.analyzer.Packages.ToArray();

                return cfg;
            }
            catch (Exception e)
            {
                this.logger.Log(LogLevel.Warn, "Failed to create configuration.");
                var msg = e is PackageHelperException ? e.Message : e.ToString();
                this.logger.Log(LogLevel.Error, "Error: " + msg);
                return null;
            }
        }
    }
}
