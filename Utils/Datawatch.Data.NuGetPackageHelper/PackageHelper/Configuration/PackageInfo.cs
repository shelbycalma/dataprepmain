﻿using System;
using System.Xml.Serialization;

namespace Datawatch.Data.Utils.PackageHelper
{
    [XmlRoot(ElementName = "package", Namespace = "", IsNullable = false)]
    public class PackageInfo
    {
        private string alias;
        private string id;
        private string path;
        private string repositoryId;
        private string requiredPlatforms;
        private string solutionId;

        [XmlAttribute("alias")]
        public string Alias
        {
            get { return alias; }
            set { alias = value; }
        }

        [XmlAttribute("id")]
        public string Id
        {
            get { return id; }
            set { id = value; }
        }

        [XmlAttribute("path")]
        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        [XmlAttribute("solutionId")]
        public string SolutionId
        {
            get { return solutionId; }
            set { solutionId = value; }
        }

        [XmlAttribute("repositoryId")]
        public string RepositoryId
        {
            get { return repositoryId; }
            set { repositoryId = value; }
        }

        [XmlAttribute("requiredPlatforms")]
        public string RequiredPlatforms
        {
            get { return requiredPlatforms; }
            set { requiredPlatforms = value; }
        }

        public BuildPlatforms GetRequiredPlatforms()
        {
            return BuildPlatformsTools.ParsePlatforms(this.requiredPlatforms);
        }

        public bool IsExternal
        {
            get
            {
                return
                    string.IsNullOrEmpty(this.Path) == true &&
                    string.IsNullOrEmpty(this.SolutionId) == true &&
                    string.IsNullOrEmpty(this.Id) == false &&
                    string.IsNullOrEmpty(this.RepositoryId) == false;
            }
        }

        public override string ToString()
        {
            return string.Format("Id = {0}, Path = {1}, SolutionId = {2}, RepositoryId = {3}, RequiredPlatforms = {4}", 
                this.Id ?? StandardSubst.Null, 
                this.Path ?? StandardSubst.Null, 
                this.SolutionId ?? StandardSubst.Null, 
                this.RepositoryId ?? StandardSubst.Null,
                this.RequiredPlatforms ?? StandardSubst.Default);
        }
    }
}