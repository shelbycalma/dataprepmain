﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class PackPackageCepItem : CommandExecutionPlanItem
    {
        public PackPackageCepItem(string nuspecPath, string outDir, string ovrVer = null)
        {
            this.NuspecPath = nuspecPath;
            this.OutDirPath = outDir;
            this.OverrideVersion = ovrVer;
        }

        public string NuspecPath { get; private set; }

        public string OutDirPath { get; private set; }

        public string OverrideVersion { get; private set; }

        public override string ToString()
        {
            return string.Format(
                "Pack NuspecPath = \"{0}\", OutDir = \"{1}\", OverrideVersion = \"{2}\"",
                this.NuspecPath ?? StandardSubst.Null,
                this.OutDirPath ?? StandardSubst.Null,
                this.OverrideVersion ?? StandardSubst.Null);
        }

    }
}