﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class CommandExecutionPlanItem
    {
        public int Index { get; set; }

        public virtual string ToStringIndexed()
        {
            return this.Index.ToString() + ". " + this.ToString();
        }
    }
}