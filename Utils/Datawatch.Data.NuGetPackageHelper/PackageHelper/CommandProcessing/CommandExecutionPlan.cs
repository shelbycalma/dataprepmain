﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class CommandExecutionPlan
    {
        private int index;

        private Queue<CommandExecutionPlanItem> plan;
        public CommandExecutionPlan()
        {
            this.plan = new Queue<CommandExecutionPlanItem>();
        }

        public void AddItem(CommandExecutionPlanItem item)
        {
            if (item == null)
                throw new ArgumentNullException("item", 
                    "Command execution item must be non-null in order to execute.");

            item.Index = this.index++;
            this.plan.Enqueue(item);
        }

        public CommandExecutionPlanItem TakeItem()
        {
            if (this.plan.Count > 0)
                return this.plan.Dequeue();

            return null;
        }

        public int Count { get { return this.plan.Count; } }

        public string GetDump()
        {
            var sb = new StringBuilder();
            sb.AppendLine();
            sb.AppendLine(
                "----- EXECUTION PLAN DUMP (" + this.plan.Count.ToString() + " item(s)) ----- ");
            foreach (var item in this.plan)
            {
                sb.AppendLine(item.ToString());
            }

            sb.AppendLine("----- EXECUTION PLAN DUMP END -----");

            return sb.ToString();
        }

        public override string ToString()
        {
            return "Item count: " + this.plan.Count.ToString();
        }
    }
}
