﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class BuildCepItem : CommandExecutionPlanItem
    {
        public BuildCepItem(
            string slnPath, string platform = null, string config = null, IEnumerable<string> constants = null)
        {
            this.SolutionPath = slnPath;
            this.Platform = platform;
            this.Configuration = config;
            this.Constants = constants;
        }

        public string Platform { get; private set; }

        public string SolutionPath { get; private set; }

        public string Configuration { get; private set; }

        public IEnumerable<string> Constants { get; private set; }

        public override string ToString()
        {
            return string.Format(
                "Build SolutionPath = \"{0}\", Platform = \"{1}\", Configuration = \"{2}\", Constants = \"{3}\"",
                this.SolutionPath ?? StandardSubst.Null,
                this.Platform ?? StandardSubst.Null,
                this.Configuration ?? StandardSubst.Null,
                this.Constants != null ? string.Join(" ", this.Constants) : string.Empty);
        }
    }
}