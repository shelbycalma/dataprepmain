﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class UpdatePackageCepItem : CommandExecutionPlanItem
    {
        public UpdatePackageCepItem(string slnPath, string pkg, string pkgSrc)
        {
            this.SolutionPath = slnPath;
            this.Package = pkg;
            this.PackageSource = pkgSrc;
            this.UsesLocalPackagesSource = true;
        }

        public UpdatePackageCepItem(
            string slnPath, string pkg, string pkgSrc, bool usePrerelease, string versionPattern)
        {
            this.SolutionPath = slnPath;
            this.Package = pkg;
            this.PackageSource = pkgSrc;
            this.UsePrerelease = usePrerelease;
            this.PrereleaseVersionPattern = versionPattern;
        }

        public string SolutionPath { get; private set; }

        public string Package { get; private set; }

        public string PackageSource { get; private set; }

        public bool UsePrerelease { get; private set; }

        public string PrereleaseVersionPattern { get; private set; }

        public bool UsesLocalPackagesSource { get; private set; }

        public override string ToString()
        {
            return
                string.Format(
                    "Update SolutionPath = \"{0}\", Package = \"{1}\", PackageSource =\"{2}\", Prerelease = \"{3}\"",
                    this.SolutionPath ?? StandardSubst.Null,
                    this.Package ?? StandardSubst.Null,
                    this.PackageSource ?? StandardSubst.Null,
                    this.UsePrerelease.ToString());
        }
    }
}