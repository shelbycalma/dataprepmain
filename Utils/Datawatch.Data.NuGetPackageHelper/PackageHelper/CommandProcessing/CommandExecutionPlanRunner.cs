﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{
    class CommandExecutionPlanRunner
    {
        private ILogger logger;
        private MSBuildController msbuild;
        private NuGetController nuget;
        private int processedItemCount;
        private int totalItemCount;

        public CommandExecutionPlanRunner(ILogger logger)
        {
            this.logger = logger;
            InitializeTools();
        }

        public void ExecutePlan(CommandExecutionPlan plan)
        {
            if (plan == null)
                throw new ArgumentNullException("plan", "Execution plan instance cannot be null.");

            this.logger.Log(LogLevel.Info, plan.GetDump());

            this.logger.Log(LogLevel.Info,
                "Plan execution has started (" + plan.Count + " item(s) to process).");

            var summary = new StringBuilder();
            summary.AppendLine("Plan execution summary:");

            this.totalItemCount = plan.Count;
            var itemTimer = new Stopwatch();
            var planTimer = Stopwatch.StartNew();
            CommandExecutionPlanItem item = null;

            try
            {
                for (processedItemCount = 0; processedItemCount < totalItemCount; processedItemCount++)
                {
                    itemTimer.Restart();

                    item = plan.TakeItem();
                    this.logger.Log(LogLevel.Debug,
                        "Processing item #{0}: {1}",
                        item.Index.ToString(), item.ToString());

                    if (item is UpdatePackageCepItem)
                    {
                        this.RunUpdatePackageItem((UpdatePackageCepItem)item);
                    }
                    else if (item is BuildCepItem)
                    {
                        this.RunBuildItem((BuildCepItem)item);
                    }
                    else if (item is PackPackageCepItem)
                    {
                        this.RunPackItem((PackPackageCepItem)item);
                    }

                    itemTimer.Stop();
                    summary.AppendFormat(
                        "{3}. SUCCESS - {0} - {1}{2}",
                        itemTimer.Elapsed.ToString("c"),
                        item.ToString(),
                        Environment.NewLine,
                        (processedItemCount + 1).ToString());

                    this.logger.Log(LogLevel.Debug,
                        "Item #" + item.Index.ToString() + " has been processed");
                }
            }
            catch (Exception e)
            {
                itemTimer.Stop();
                summary.AppendFormat(
                    "{3}. FAILURE - {0} - {1}{2}",
                    itemTimer.Elapsed.ToString("c"),
                    item.ToString(),
                    Environment.NewLine,
                    (processedItemCount + 1).ToString());

                string msg = string.Format(
                    "plan execution failed on step {0}/{1}.",
                    (this.processedItemCount + 1).ToString(),
                    this.totalItemCount.ToString());

                throw new CommandExecutionException(msg, e);
            }
            finally
            {
                planTimer.Stop();
                summary.AppendLine();
                summary.AppendLine("Plan execution time - " +
                    planTimer.Elapsed.ToString("c"));

                this.logger.Log(LogLevel.Info, summary.ToString());
            }

            this.logger.Log(LogLevel.Debug,
                "Plan execution has finished. A set of {0} out of {1} items has been processed.",
                this.processedItemCount.ToString(),
                this.totalItemCount.ToString());
        }

        private void InitializeTools()
        {
            var verbosity = (VerbosityLevel)Settings.Default.NuGetVerbosity;
            this.nuget = new NuGetController(
                Settings.Default.NuGetPath,
                Settings.Default.NuGetConfigPath,
                verbosity,
                this.logger);

            verbosity = (VerbosityLevel)Settings.Default.MSBuildVerbosity;
            this.msbuild =
                new MSBuildController(Settings.Default.MSBuildPath, verbosity, this.logger);
        }

        private void RemoveExistingPackages(string package, string slnPath)
        {
            string slnDir = Path.GetDirectoryName(slnPath);
            string pkgDir = Path.Combine(slnDir,
                NuGetController.SolutionPackageCacheDir,
                package + "." + Settings.Default.DebugPackageVersion);

            try
            {
                if (Directory.Exists(pkgDir) == true)
                {
                    Directory.Delete(pkgDir, true);
                    this.logger.Log(LogLevel.Debug,
                        "Package cache successfully deleted from \"" + pkgDir + "\".");
                }
            }
            catch (Exception e)
            {
                this.logger.Log(LogLevel.Error,
                    "Failed to remove solution package cache for package \"" + package + "\": " + e.Message);
            }
        }

        private void RunBuildItem(BuildCepItem item)
        {
            this.nuget.Restore(item.SolutionPath);
            this.msbuild.Build(item.SolutionPath, item.Platform, item.Configuration, item.Constants);
        }

        private void RunPackItem(PackPackageCepItem item)
        {
            this.nuget.Pack(item.NuspecPath, item.OutDirPath, item.OverrideVersion);
        }

        private void RunUpdatePackageItem(UpdatePackageCepItem item)
        {
            if (item.UsesLocalPackagesSource == true)
                RemoveExistingPackages(item.Package, item.SolutionPath);

            this.nuget.Restore(item.SolutionPath);
            this.nuget.Update(
                item.SolutionPath, 
                item.Package, 
                item.PackageSource,
                item.UsePrerelease,
                item.PrereleaseVersionPattern);
        }
    }
}