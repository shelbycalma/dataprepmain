﻿using System;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{
    internal class CommandDataValidator
    {
        private IRepositoryObjectVerifier objectVerifier;
        private ValidationResultBuilder vrb = new ValidationResultBuilder();

        public ValidationResult Validate(HelperCommand cmd, IRepositoryObjectVerifier verifier)
        {
            this.objectVerifier = verifier;

            this.vrb.Reset();

            if (cmd is ShowHelpCommand)
                return this.ValidateShowHelpCommand((ShowHelpCommand)cmd);

            if (cmd is InitCommand)
                return this.ValidateInitCommand((InitCommand)cmd);

            if (cmd is UpdateCommand)
                return this.ValidateUpdateCommand((UpdateCommand)cmd);

            if (cmd is PackCommand)
                return this.ValidatePackCommand((PackCommand)cmd);

            return null;
        }

        private ValidationResult ValidateInitCommand(InitCommand cmd)
        {
            if (FileTools.IsValidPath(cmd.NuspecPath) == false)
                vrb.AddError("Invalid path to nuspec files - \"" +
                    cmd.NuspecPath ?? StandardSubst.Null + "\".");

            if (FileTools.IsValidPath(cmd.PackageSourcePath) == false)
                vrb.AddError("Invalid path to nuspec files - \"" +
                    cmd.NuspecPath ?? StandardSubst.Null + "\".");

            if (string.IsNullOrWhiteSpace(cmd.RepositoryId) == true)
                vrb.AddError("Repository Id is not specified or has invalid value.");

            return vrb.BuildResult();
        }

        private ValidationResult ValidatePackCommand(PackCommand cmd)
        {
            // Solutions and packages options are mutually exclusive
            if (cmd.Solution != null && cmd.Packages != null)
            {
                this.vrb.AddError("You may specify either packages or a solution but not both.");
                return this.vrb.BuildResult();
            }

            // Packages must be produced by repository solutions.
            // By default only referenced packages can be packed,
            // to pack unreferenced packages -AllowUnreferenced option 
            // should be present
            if (cmd.Packages != null)
            {
                for (int i = 0; i < cmd.Packages.Length; i++)
                {
                    cmd.Packages[i] = this.PackageAliasToName(cmd.Packages[i]);
                    var item = cmd.Packages[i];
                    if (this.objectVerifier.IsProducedPackage(item) == false)
                    {
                        vrb.AddError("Unknown package " + item + ".");
                    }
                    else if (cmd.AllowUnreferenced == false &&
                            this.objectVerifier.IsReferencedPackage(item) == false)
                        vrb.AddError("Unreferenced packages are not allowed.");
                }

                // For now, we pack only packages produced in the same solution.
                if (vrb.ErrorCount == 0 && 
                    this.objectVerifier.AreProducedByTheSameSolution(cmd.Packages) == false)
                    vrb.AddError("Specified packages should be produced by the same solution.");
            }

            // Solutions must exist in repository config
            if (cmd.Solution != null)
            {
                cmd.Solution = this.VerifySolutionName(cmd.Solution);
            }

            // Build config / platform must be specified together with build option
            if ((cmd.BuildConfiguraton != null || cmd.BuildPlatform != null) &&
                cmd.IsBuildRequired == false)
                vrb.AddError("Build configuration and/or platform must be specified together with build option");


            return vrb.BuildResult();
        }

        private ValidationResult ValidateShowHelpCommand(ShowHelpCommand cmd)
        {
            return new ValidationResult(null, null);
        }

        private ValidationResult ValidateUpdateCommand(UpdateCommand cmd)
        {
            // Solutions and packages options are mutually exclusive
            if (cmd.Solutions != null && cmd.Packages != null)
            {
                this.vrb.AddError("You may specify either packages or solutions but not both.");
                return vrb.BuildResult();
            }

            // Packages must be produced by repository solutions
            if (cmd.Packages != null)
                for (int i = 0, cmdPackagesLength = cmd.Packages.Length; i < cmdPackagesLength; i++)
                {
                    cmd.Packages[i] = this.PackageAliasToName(cmd.Packages[i]);
                    string item = cmd.Packages[i];
                    if (this.objectVerifier.IsProducedPackage(item) == false)
                        vrb.AddError("Unknown package " + item + ".");
                }

            // Solutions must exist in repository config
            if (cmd.Solutions != null)
                for (int i = 0; i < cmd.Solutions.Length; i++)
                    cmd.Solutions[i] = this.VerifySolutionName(cmd.Solutions[i]);

            // Target solutions must exist in repository config
            if (cmd.TargetSolutions != null)
            {
                for (int i = 0; i < cmd.TargetSolutions.Length; i++)
                    cmd.TargetSolutions[i] = this.VerifySolutionName(cmd.TargetSolutions[i]);
            }

            // External package source should be specified in NuGet config file
            if (cmd.UseLocalPackageSources == false)
            {
                string result = this.VerifyPackageSource(cmd.PackageSource);
                if (result != null)
                    vrb.AddError(result);
            }

            // Prerelease package version pattern should be a valid regex
            if (cmd.UsePrereleasePackages == true && cmd.PrereleaseVersionPattern != null)
            {
                if (RegexTools.IsValidRegexPattern(cmd.PrereleaseVersionPattern) == false)
                    this.vrb.AddError(
                        "Invalid prerelease package version pattern - \"" +
                        cmd.PrereleaseVersionPattern +
                        "\".");
            }

            return vrb.BuildResult();
        }

        private string VerifyPackageSource(string pkgSrcName)
        {
            if (string.IsNullOrWhiteSpace(pkgSrcName) == true)
                return "Package source name is empty.";

            var nugetCfgPath = FileTools.CreatePathRelativeToExecAssembly(Settings.Default.NuGetConfigPath);
            if (FileTools.VerifyFilePath(nugetCfgPath) == false)
                throw new PackageHelperException(
                    "NuGet configuration file path in program settings is invalid.");
            try
            {
                var doc = XDocument.Load(nugetCfgPath);
                var cfg = doc.GetElementByLocalName("configuration");
                var pkgs = cfg.GetElementByLocalName("packageSources");

                var pkgSrcNames = pkgs.GetElementsByLocalName("add")
                                    .Select(x => x.Attribute("key"))
                                    .Where(x => x != null)
                                    .Select(x => x.Value)
                                    .ToDictionary(x => x.ToUpper());

                if (pkgSrcNames.ContainsKey(pkgSrcName.ToUpper()) == true)
                    return null;

                return "Unknown package source name \"" + pkgSrcName + "\"";
            }
            catch (NullReferenceException e)
            {
                throw new PackageHelperException(
                    "Failed to parse NuGet configuration file: " + nugetCfgPath, e);
            }
            catch (Exception e)
            {
                throw new PackageHelperException("Failed to validate package source name.", e);
            }
        }

        private string VerifySolutionName(string name)
        {
            string slnId = null;
            string item = this.objectVerifier.IsSolutionAlias(name, out slnId) == true
                && string.IsNullOrWhiteSpace(slnId) == false
                ? slnId
                : name;

            if (this.objectVerifier.IsSolution(item) == false)
                vrb.AddError("Unknown solution \"" + item + "\".");

            return item;
        }

        private string PackageAliasToName(string name)
        {
            string pkgId = null;
            string item = this.objectVerifier.IsPackageAlias(name, out pkgId) == true
                && string.IsNullOrWhiteSpace(pkgId) == false
                ? pkgId
                : name;

            return item;
        }
    }
}