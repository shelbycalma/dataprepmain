﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{
    class CommandLineParser
    {
        private const string RepoIdPattern = "^[A-Za-z][A-Za-z0-9_-]*$";
        private const string CommandParseErrorTemplate = "Failed to parse {0} command: {1}";
        private const char OptionPrefix = '-';

        private HelperCommand command;
        private string[] helpTokens = { "/?", "/h", "-?", "-h", "help", "--help" };
        private ILogger logger;
        private List<string> unrecognizedEntries;

        public CommandLineParser(ILogger logger)
        {
            this.logger = logger;
        }

        public HelperCommand Parse(string[] args)
        {
            if (args == null || args.Length == 0)
                throw new ArgumentException("Invalid command line.");

            if (unrecognizedEntries == null)
                unrecognizedEntries = new List<string>();

            unrecognizedEntries.Clear();

            var s = args[0].ToUpper();

            try
            {
                if (IsHelpToken(s) || IsMatch(CommandToken.ShowHelp, s))
                    return new ShowHelpCommand(CommandToken.ShowHelp, this.logger);

                if (IsMatch(CommandToken.Init, s))
                    return ParseInitCommand(args);

                if (IsMatch(CommandToken.Update, s))
                    return ParseUpdateCommand(args);

                if (IsMatch(CommandToken.Pack, s))
                    return ParsePackCommand(args);
            }
            finally
            {
                // Unrecognized entries are show-stoppers as user's intents are not clear.
                if (unrecognizedEntries.Count != 0)
                    ReportCommandParsingError(
                        this.command.Token,
                        "unrecognized entries - " + string.Join(" ", unrecognizedEntries));
            }

            throw new UnknownCommandException("Failed to recognize command: \"" + (args[0] ?? StandardSubst.Null) + "\"");
        }

        private bool ConvertToShowHelpCommandIfRequired(string[] args, string token)
        {
            bool isRequired = args.Length >= 2 && IsHelpToken(args[1]);
            if (isRequired)
            {
                this.command = new ShowHelpCommand(CommandToken.ShowHelp, this.logger)
                {
                    TargetCmdToken = token
                };
            }

            return isRequired;
        }

        private bool IsHelpToken(string str)
        {
            var s = str.ToUpper();
            return this.helpTokens.Any(t => IsMatch(t, s)) == true;
        }

        private bool IsMatch(string token, string s)
        {
            return string.CompareOrdinal(s, token.ToUpper()) == 0;
        }

        private HelperCommand ParseInitCommand(string[] args)
        {
            if (ConvertToShowHelpCommandIfRequired(args, CommandToken.Init))
                return this.command;

            var cmd = new InitCommand(CommandToken.Init, this.logger);
            if (args.Length == 1)
            {
                ReportCommandParsingError(cmd.Token,
                    "repository id was not specified.");
            }

            this.command = cmd;

            string repoId = args[1];
            if (Regex.IsMatch(repoId, RepoIdPattern) == true)
            {
                cmd.RepositoryId = repoId;
            }
            else
            {
                ReportCommandParsingError(cmd.Token,
                    "invalid repository Id value - \"" + repoId + "\"");
            }

            for (int i = 2; i < args.Length; i++)
            {
                string key = args[i];
                string mkey = key.ToUpper();
                if (IsMatch(CommandOptionToken.NuspecDir, mkey) ||
                    IsMatch(CommandOptionToken.NuspecDirShort, mkey))
                {
                    if (++i == args.Length)
                        ReportCommandParsingError(cmd.Token, 
                            "found no value for command option \"" + key + "\"");

                    cmd.NuspecPath = args[i];
                    continue;
                }

                if (IsMatch(CommandOptionToken.PackageSourceDir, mkey) ||
                    IsMatch(CommandOptionToken.PackageSourceDirShort, mkey))
                {
                    if (++i == args.Length)
                        ReportCommandParsingError(cmd.Token,
                            "found no value for command option \"" + key + "\"");

                    cmd.PackageSourcePath = args[i];
                    continue;
                }

                if (IsMatch(CommandOptionToken.CreateConfigOnly, mkey) ||
                    IsMatch(CommandOptionToken.CreateConfigOnlyShort, mkey))
                {
                    cmd.CreateConfigOnly = true;
                    continue;
                }

                unrecognizedEntries.Add(key);
            }

            return cmd;
        }

        private HelperCommand ParsePackCommand(string[] args)
        {
            if (ConvertToShowHelpCommandIfRequired(args, CommandToken.Pack))
                return this.command;

            var cmd = new PackCommand(CommandToken.Pack, this.logger);
            this.command = cmd;

            if (args.Length == 1)
            {
                ReportCommandParsingError(cmd.Token,
                    "no packages specified.");
            }

            // Parsing command options
            for (int i = 1; i < args.Length; i++)
            {
                string key = args[i];
                string ukey = key.ToUpper();

                if (IsMatch(CommandOptionToken.Packages, ukey) ||
                    IsMatch(CommandOptionToken.PackagesShort, ukey))
                {
                    if (cmd.Packages != null)
                        ReportCommandParsingError(cmd.Token, "multiple package sets specified.");

                    var pkgs = this.RetrieveMultipleOptionValues(i + 1, args);
                    if (pkgs.Count == 0)
                        ReportCommandParsingError(cmd.Token, "no packages specified.");

                    cmd.Packages = pkgs.ToArray();
                    this.SetPackageSelection(cmd, SolutionPackageSelection.Custom);
                    i += pkgs.Count;
                    continue;
                }

                if (IsMatch(CommandOptionToken.Solution, ukey) ||
                    IsMatch(CommandOptionToken.SolutionShort, ukey))
                {
                    if (cmd.Solution != null)
                        ReportCommandParsingError(cmd.Token, "multiple solution options specified.");

                    var slns = this.RetrieveMultipleOptionValues(i + 1, args);
                    if (slns.Count == 0)
                        ReportCommandParsingError(cmd.Token, "no solutions specified.");

                    cmd.Solution = slns.First();
                    this.SetPackageSelection(cmd,
                        SolutionPackageSelection.CustomBySolutions);
                    i += slns.Count;
                    continue;
                }

                if (IsMatch(CommandOptionToken.AllowUnreferenced, ukey) ||
                    IsMatch(CommandOptionToken.AllowUnreferencedShort, ukey))
                {
                    cmd.AllowUnreferenced = true;
                    continue;
                }

                if (IsMatch(CommandOptionToken.BuildRequired, ukey) ||
                    IsMatch(CommandOptionToken.BuildRequiredShort, ukey))
                {
                    cmd.IsBuildRequired = true;
                    continue;
                }

                if (IsMatch(CommandOptionToken.ExecutionPlanOnly, ukey) ||
                    IsMatch(CommandOptionToken.ExecutionPlanOnlyShort, ukey))
                {
                    cmd.CreateExecutionPlanOnly = true;
                    continue;
                }

                if (this.TryGetConfigFilePath(cmd, ukey, args, i) == true)
                {
                    i++;
                    continue;
                }

                if (this.TryGetBuildConfiguration(cmd, ukey, args, i) == true)
                {
                    i++;
                    continue;
                }

                if (this.TryGetBuildPlatform(cmd, ukey, args, i) == true)
                {
                    i++;
                    continue;
                }

                unrecognizedEntries.Add(key);
            }

            return cmd;
        }

        private HelperCommand ParseUpdateCommand(string[] args)
        {
            if (ConvertToShowHelpCommandIfRequired(args, CommandToken.Update))
                return this.command;

            var cmd = new UpdateCommand(CommandToken.Update, this.logger);
            this.command = cmd;

            if (args.Length == 1)
                ReportCommandParsingError(cmd.Token, "no package source was specified.");

            // Parsing command argument - local or remote package source
            string key = args[1];
            string ukey = args[1].ToUpper();
            if (IsMatch(CommandArgumentToken.Local, ukey))
            {
                cmd.UseLocalPackageSources = true;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(key) == true ||
                    key[0] == OptionPrefix)
                    ReportCommandParsingError(
                        cmd.Token, "no command argument was specified.");

                cmd.PackageSource = key;
            }

            // Parsing command options
            for (int i = 2; i < args.Length; i++)
            {
                key = args[i];
                ukey = key.ToUpper();

                if (IsMatch(CommandOptionToken.Packages, ukey) ||
                    IsMatch(CommandOptionToken.PackagesShort, ukey))
                {
                    if (cmd.Packages != null)
                        ReportCommandParsingError(cmd.Token, "multiple package sets specified.");

                    var pkgs = this.RetrieveMultipleOptionValues(i + 1, args);
                    if (pkgs.Count == 0)
                        ReportCommandParsingError(cmd.Token, "no packages specified.");

                    if (pkgs.Count == 1 && 
                        IsMatch(CommandSpecialValueToken.All, pkgs[0].ToUpper()))
                    {
                        this.SetPackageSelection(cmd,
                            SolutionPackageSelection.AllProducedAndReferenced);
                        i++;
                        continue;
                    }

                    cmd.Packages = pkgs.ToArray();
                    this.SetPackageSelection(cmd, SolutionPackageSelection.Custom);
                    i += pkgs.Count;
                    continue;
                }

                if (IsMatch(CommandOptionToken.Solutions, ukey) ||
                    IsMatch(CommandOptionToken.SolutionsShort, ukey))
                {
                    if (cmd.Solutions != null)
                        ReportCommandParsingError(cmd.Token, "multiple solution sets specified.");

                    var slns = this.RetrieveMultipleOptionValues(i + 1, args);
                    if (slns.Count == 0)
                        ReportCommandParsingError(cmd.Token, "no solutions specified.");

                    if (slns.Count == 1 &&
                        IsMatch(CommandSpecialValueToken.All, slns[0].ToUpper()))
                    {
                        this.SetPackageSelection(cmd,
                        SolutionPackageSelection.ProducedByAllSolutionsAndReferenced);
                        i++;
                        continue;
                    }

                    cmd.Solutions = slns.ToArray();
                    this.SetPackageSelection(cmd,
                        SolutionPackageSelection.CustomBySolutions);
                    i += slns.Count;
                    continue;
                }

                if (IsMatch(CommandOptionToken.TargetSolutions, ukey) ||
                    IsMatch(CommandOptionToken.TargetSolutionsShort, ukey))
                {
                    if (cmd.TargetSolutions != null)
                        ReportCommandParsingError(cmd.Token, "multiple target solution sets specified.");

                    var slns = this.RetrieveMultipleOptionValues(i + 1, args);
                    if (slns.Count == 0)
                        ReportCommandParsingError(cmd.Token, "no target solutions specified.");

                    if (slns.Count == 1 && 
                        IsMatch(CommandSpecialValueToken.All, slns[0].ToUpper()))
                    {
                        cmd.TargetSolutionSelection = SolutionSelection.AllReferencing;
                        i++;
                        continue;
                    }

                    cmd.TargetSolutions = slns.ToArray();
                    cmd.TargetSolutionSelection = SolutionSelection.Custom;
                    i += slns.Count;
                    continue;
                }

                if (IsMatch(CommandOptionToken.ExecutionPlanOnly, ukey) ||
                    IsMatch(CommandOptionToken.ExecutionPlanOnlyShort, ukey))
                {
                    cmd.CreateExecutionPlanOnly = true;
                    continue;
                }

                if (this.TryGetConfigFilePath(cmd, ukey, args, i) == true)
                {
                    i++; 
                    continue;
                }

                if (this.TryGetBuildConfiguration(cmd, ukey, args, i) == true)
                {
                    i++;
                    continue;
                }

                if (this.TryGetBuildPlatform(cmd, ukey, args, i) == true)
                {
                    i++;
                    continue;
                }

                if (this.TryGetPrereleaseVersion(cmd, ukey, args, i) == true)
                {
                    i += cmd.PrereleaseVersionPattern != null ? 1 : 0;
                    continue;
                }

                unrecognizedEntries.Add(key);
            }

            if (cmd.PackageSelection == SolutionPackageSelection.None)
                cmd.PackageSelection = SolutionPackageSelection.AllProducedAndReferenced;

            return cmd;
        }

        private bool TryGetPrereleaseVersion(
            PackageBasedHelperCommand cmd, string ukey, string[] args, int index)
        {
            if (IsMatch(CommandOptionToken.UsePrereleasePackages, ukey) ||
                IsMatch(CommandOptionToken.UsePrereleasePackagesShort, ukey))
            {
                cmd.UsePrereleasePackages = true;
                if (index < args.Length - 1)
                {
                    var values = this.RetrieveMultipleOptionValues(index + 1, args);
                    if (values.Count == 0)
                        return true;

                    if (values.Count > 1)
                        ReportCommandParsingError(cmd.Token,
                            "invalid prerelease package version pattern specified.");

                    if (cmd.PrereleaseVersionPattern != null)
                        ReportCommandParsingError(cmd.Token,
                            "multiple prerelease version patterns specified.");

                    cmd.PrereleaseVersionPattern = values.First();
                }

                return true;
            }

            return false;
        }

        private bool TryGetConfigFilePath(
            PackageBasedHelperCommand cmd, string ukey, string[] args, int index)
        {
            if (IsMatch(CommandOptionToken.ConfigFile, ukey) ||
                IsMatch(CommandOptionToken.ConfigFileShort, ukey))
            {
                if (cmd.ConfigFilePath != null)
                    ReportCommandParsingError(cmd.Token, 
                        "multiple repository configuration options specified.");

                var paths = this.RetrieveMultipleOptionValues(index + 1, args);
                if (paths.Count == 0)
                    ReportCommandParsingError(cmd.Token, 
                        "no repository config file path specified.");

                if (paths.Count > 1)
                    ReportCommandParsingError(cmd.Token, 
                        "invalid repository config file path specified.");

                cmd.ConfigFilePath = paths.First();
                return true;
            }

            return false;
        }

        private bool TryGetBuildConfiguration(
            PackageBasedHelperCommand cmd, string ukey, string[] args, int index)
        {
            if (IsMatch(CommandOptionToken.BuildConfig, ukey) ||
                IsMatch(CommandOptionToken.BuildConfigShort, ukey))
            {
                if (cmd.BuildConfiguraton != null)
                    ReportCommandParsingError(cmd.Token, 
                        "multiple build configuration options specified.");

                var cfgs = this.RetrieveMultipleOptionValues(index + 1, args);
                if (cfgs.Count == 0)
                    ReportCommandParsingError(cmd.Token, 
                        "no build configuration specified.");

                if (cfgs.Count > 1)
                    ReportCommandParsingError(cmd.Token, 
                        "invalid build configuration specified.");

                cmd.BuildConfiguraton = cfgs.First();
                return true;
            }

            return false;
        }

        private bool TryGetBuildPlatform(
            PackageBasedHelperCommand cmd, string ukey, string[] args, int index)
        {
            if (IsMatch(CommandOptionToken.BuildPlatform, ukey) ||
                IsMatch(CommandOptionToken.BuildPlatformShort, ukey))
            {
                if (cmd.BuildPlatform != null)
                    ReportCommandParsingError(cmd.Token,
                        "multiple build platform options specified.");

                var cfgs = this.RetrieveMultipleOptionValues(index + 1, args);
                if (cfgs.Count == 0)
                    ReportCommandParsingError(cmd.Token,
                        "no build platform specified.");

                if (cfgs.Count > 1)
                    ReportCommandParsingError(cmd.Token,
                        "invalid build platform specified.");

                cmd.BuildPlatform = cfgs.First();
                return true;
            }

            return false;
        }

        private void ReportCommandParsingError(string cmdToken, string description)
        {
            throw new CommandParsingException(
                string.Format(CommandParseErrorTemplate, cmdToken, description));
        }

        private List<string> RetrieveMultipleOptionValues(int startIndex, string[] args)
        {
            var values = new List<string>();
            for (int i = startIndex; i < args.Length; i++)
            {
                if (args[i][0] == '-')
                    break;

                values.Add(args[i]);
            }

            return values;
        }

        private void SetPackageSelection(PackageBasedHelperCommand cmd, SolutionPackageSelection selection)
        {
            if (cmd.PackageSelection != SolutionPackageSelection.None)
                ReportCommandParsingError(cmd.Token,
                    "You may specify either packages or solutions but not both.");

            cmd.PackageSelection = selection;
        }
    }
}
