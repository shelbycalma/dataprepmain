﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class CommandToken
    {
        public const string ShowHelp = "Help";
        public const string Init = "Init";
        public const string Pack = "Pack";
        public const string Update = "Update";
    }

    static class CommandArgumentToken
    {
        public const string Local = "Local";
    }

    static class CommandOptionToken
    {
        public const string RepositoryId = "-RepoId";
        public const string RepositoryIdShort = "-R";
        public const string NuspecDir = "-NuspecDir";
        public const string NuspecDirShort = "-N";
        public const string PackageSourceDir = "-PackageSrcDir";
        public const string PackageSourceDirShort = "-P";
        public const string CreateConfigOnly = "-ConfigOnly";
        public const string CreateConfigOnlyShort = "-C";
        public const string ConfigFile = "-ConfigFile";
        public const string ConfigFileShort = "-C";
        public const string Packages = "-Packages";
        public const string PackagesShort = "-P";
        public const string Solutions = "-Solutions";
        public const string SolutionsShort = "-S";
        public const string Solution = "-Solution";
        public const string SolutionShort = "-S";
        public const string TargetSolutions = "-TargetSolutions";
        public const string TargetSolutionsShort = "-T";
        public const string AllowUnreferenced = "-AllowUnreferenced";
        public const string AllowUnreferencedShort = "-A";
        public const string UsePrereleasePackages = "-Prerelease";
        public const string UsePrereleasePackagesShort = "-PR";
        public const string BuildRequired = "-Build";
        public const string BuildRequiredShort = "-B";
        public const string BuildConfig = "-BuildConfig";
        public const string BuildConfigShort = "-BC";
        public const string BuildPlatform = "-BuildPlatform";
        public const string BuildPlatformShort = "-BP";
        public const string ExecutionPlanOnly = "-PlanOnly";
        public const string ExecutionPlanOnlyShort = "-PO";
    }

    static class CommandSpecialValueToken
    {
        public const string All = "All";
        public const string AllProduced = "AllProduced";
    }
}
