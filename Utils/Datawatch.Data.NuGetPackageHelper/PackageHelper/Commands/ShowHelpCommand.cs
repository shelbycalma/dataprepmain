﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Datawatch.Data.Utils.PackageHelper
{
    class ShowHelpCommand : HelperCommand
    {
        public ShowHelpCommand(string token, ILogger logger)
            : base(token, logger)
        {
            this.CanExecuteWithoutConfig = true;
        }

        public string TargetCmdToken { get; set; }

        public override void Execute(RepositoryConfigurationManager rcm)
        {
            var hcr = new HelperCommandRegistry();
            if (string.IsNullOrEmpty(this.TargetCmdToken) == true)
            {
                Console.WriteLine(hcr.GenerateCommandReference());
            }
            else
            {
                var exeName = System.IO.Path.GetFileName(Assembly.GetEntryAssembly().Location);

                var ci = hcr.GetCommandInfo(this.TargetCmdToken);
                Console.WriteLine(ci.ToString(exeName, this.TargetCmdToken));
            }
        }
    }
}