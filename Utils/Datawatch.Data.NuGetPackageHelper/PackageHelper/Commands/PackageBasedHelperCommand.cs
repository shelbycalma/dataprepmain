﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;
using System.IO;

namespace Datawatch.Data.Utils.PackageHelper
{
    abstract class PackageBasedHelperCommand : HelperCommand
    {
        protected GlobalConfiguration config;
        protected readonly string[] BuildConstants = new string[] { "PHLOCAL" };
        public PackageBasedHelperCommand(string token, ILogger logger)
            : base(token, logger)
        {
        }

        public string ConfigFilePath { get; set; }

        public string BuildConfiguraton { get; set; }

        public string BuildPlatform { get; set; }

        public string[] Packages { get; set; }

        public bool UsePrereleasePackages { get; set; }

        public string PrereleaseVersionPattern { get; set; }

        public bool CreateExecutionPlanOnly { get; set; }

        public SolutionPackageSelection PackageSelection { get; set; }
        protected virtual string GetSolutionPath(string workingFolder, string slnId)
        {
            string slnPath = this.config.Solutions[slnId].Path;
            return Path.Combine(workingFolder, slnPath);
        }

        protected void GenerateBuildItems(CommandExecutionPlan plan, string slnId, string[] pkgIds, string workingFolder)
        {
            var buildInfo = this.config.Solutions[slnId]
                .DefaultBuildInfo;

            BuildPlatforms platforms = BuildPlatforms.Default;
            if (buildInfo.IgnoreOtherPlatforms == false)
                foreach (var pid in pkgIds)
                {
                    var pkg = this.config.Packages[pid];
                    platforms |= pkg.GetRequiredPlatforms();
                }

            var buildItems = new List<BuildCepItem>();

            var slnPath = GetSolutionPath(workingFolder, slnId);
            var buildConfig = BuildOptionsGenerator.GetConfiguration(
                buildInfo, this.BuildConfiguraton);

            if (buildInfo.IgnoreOtherPlatforms == true || platforms == BuildPlatforms.Default)
            {
                var buildPlatform = BuildOptionsGenerator.GetPlatform(buildInfo, this.BuildPlatform);
                var item = new BuildCepItem(slnPath, buildPlatform, buildConfig, BuildConstants);
                buildItems.Add(item);
            }
            else
            {
                var availablePlatforms = Enum.GetValues(typeof(BuildPlatforms))
                    .OfType<BuildPlatforms>()
                    .Where(x => x != BuildPlatforms.Default);

                foreach (var availablePlatform in availablePlatforms)
                {
                    if ((platforms & availablePlatform) != 0)
                        buildItems.Add(
                            new BuildCepItem(
                                slnPath, availablePlatform.GetName(), buildConfig, BuildConstants));
                }
            }

            foreach (var item in buildItems)
            {
                plan.AddItem(item);
            }
        }
    }
}