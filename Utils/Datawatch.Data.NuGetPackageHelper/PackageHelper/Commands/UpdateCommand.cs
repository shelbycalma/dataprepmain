﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;
using System.IO;

namespace Datawatch.Data.Utils.PackageHelper
{
    class UpdateCommand : PackageBasedHelperCommand
    {
        public UpdateCommand(string token, ILogger logger)
            : base(token, logger)
        {
            this.CanExecuteWithoutConfig = false;
            this.TargetSolutionSelection =
                SolutionSelection.AllReferencing;
        }

        public string PackageSource { get; set; }

        public string[] Solutions { get; set; }

        public string[] TargetSolutions { get; set; }

        public SolutionSelection TargetSolutionSelection { get; set; }

        public bool UseLocalPackageSources { get; set; }

        public override void Execute(RepositoryConfigurationManager rcm)
        {
            this.config = rcm.Configuration;
            var execPlan = this.UseLocalPackageSources == true
                ? CreateExecutionPlanForLocal(rcm.WorkingFolder)
                : CreateExecutionPlanForServer(rcm.WorkingFolder);

            if (this.CreateExecutionPlanOnly == true)
            {
                this.logger.Log(LogLevel.Info, execPlan.GetDump());
                return;
            }

            var runner = new CommandExecutionPlanRunner(this.logger);

            try
            {
                runner.ExecutePlan(execPlan);
            }
            catch (CommandExecutionException e)
            {
                this.logger.Log(LogLevel.Error, "Update command failed");
                throw;
            }
        }

        private CommandExecutionPlan CreateExecutionPlanForLocal(string workingFolder)
        {
            var plan = new CommandExecutionPlan();
            var items = DetermineItemsToProcessLocal();
           
            var slnFilter = this.TargetSolutionSelection == SolutionSelection.AllReferencing
                ? null
                : new HashSet<string>(this.TargetSolutions);

            foreach (var item in items)
            {
                // Build the package producing solution
                this.GenerateBuildItems(plan, item.Key, item.Value, workingFolder);

                foreach (var pid in item.Value)
                {
                    // Pack package
                    var pkg = this.config.Packages[pid];
                    var nuspecPath = FileTools.CombinePath(workingFolder, pkg.Path);
                    var pkgSrc = this.config.PackageSources[pkg.RepositoryId];
                    var outDir = FileTools.CombinePath(workingFolder, pkgSrc.Path);

                    var pack = new PackPackageCepItem(nuspecPath, outDir);
                    plan.AddItem(pack);

                    // Update package for all the dependent solutions
                    var depSlns = this.config.Solutions.Values
                        .OrderBy(x => x.BuildStep)
                        .Where(x => x.Dependencies != null &&
                                    x.Dependencies.Any(p => p == pid) &&
                                    (slnFilter == null || slnFilter.Contains(x.Id)))
                        .Select(x => x.Path);

                    if (depSlns.Any() == false)
                        continue;

                    foreach (var sln in depSlns)
                    {
                        var path = FileTools.CombinePath(workingFolder, sln);
                        var update = new UpdatePackageCepItem(path, pid, pkgSrc.Id);
                        plan.AddItem(update);
                    }
                }
            }

            return plan;
        }

        private CommandExecutionPlan CreateExecutionPlanForServer(string workingFolder)
        {
            var plan = new CommandExecutionPlan();
            var packages = this.DetermineItemsToProcessServer();

            var slnFilter = this.TargetSolutionSelection == SolutionSelection.AllReferencing
                ? null
                : new HashSet<string>(this.TargetSolutions);

            foreach (var pkg in packages)
            {
                // Update package for all the dependent solutions
                var depSlns = this.config.Solutions.Values
                    .OrderBy(x => x.BuildStep)
                    .Where(x => x.Dependencies != null &&
                        x.Dependencies.Any(p => p == pkg) &&
                        (slnFilter == null || slnFilter.Contains(x.Id)))
                    .Select(x => x.Path);

                if (depSlns.Any() == false)
                    continue;

                foreach (var sln in depSlns)
                {
                    var slnPath = FileTools.CombinePath(workingFolder, sln);
                    var update = new UpdatePackageCepItem(
                        slnPath, pkg, this.PackageSource, this.UsePrereleasePackages, this.PrereleaseVersionPattern);
                    plan.AddItem(update);
                }
            }

            return plan;
        }

        private Dictionary<string, string[]> DetermineItemsToProcessLocal()
        {
            var referencedPackages = new HashSet<string>(
                this.config.Solutions.Values
                .Where(x => x.Dependencies != null)
                .SelectMany(x => x.Dependencies));

            //if ((this.Packages == null || this.Packages.IsEmpty()) &&
            //    (this.Solutions == null || this.Solutions.IsEmpty()))
            if (this.PackageSelection == SolutionPackageSelection.AllProducedAndReferenced ||
                this.PackageSelection ==
                SolutionPackageSelection.ProducedByAllSolutionsAndReferenced)
            {
                // All solutions and their produced packages which are referenced
                // within repository
                return this.config.Solutions.Values
                    .OrderBy(x => x.BuildStep)
                    .Select(s => new
                    {
                        SlnId = s.Id,
                        Pkgs = s.ProducedPackages
                            .Where(p => referencedPackages.Contains(p)).ToArray()
                    })
                    .Where(x => x.Pkgs != null && x.Pkgs.Length > 0)
                    .ToDictionary(x => x.SlnId, x => x.Pkgs);

            }

            // Process just the specified package set
            if (this.PackageSelection == SolutionPackageSelection.Custom)
            {
                return this.config.Solutions.Values
                    .OrderBy(x => x.BuildStep)
                    .Select(s => new
                    {
                        SlnId = s.Id,
                        Pkgs = s.ProducedPackages
                            .Where(p => this.Packages.Any(x => x == p)).ToArray()
                    })
                    .Where(x => x.Pkgs != null && x.Pkgs.Length > 0)
                    .ToDictionary(x => x.SlnId, x => x.Pkgs);
            }

            // Only referenced within the repository packages produced by the
            // specified solutions are to be processed
            return this.config.Solutions.Values
                .OrderBy(x => x.BuildStep)
                .Where(x => this.Solutions.Any(c => c == x.Id))
                .ToDictionary(x => x.Id, x => x.ProducedPackages);
        }

        private List<string> DetermineItemsToProcessServer()
        {
            var referencedPackages = new HashSet<string>(
                this.config.Solutions.Values
                .Where(x => x.Dependencies != null)
                .OrderBy(x => x.BuildStep)
                .SelectMany(x => x.Dependencies));

            var allowedPackages = this.config.Solutions.Values
                    .OrderBy(x => x.BuildStep)
                    .SelectMany(x => x.ProducedPackages)
                    .Where(x => referencedPackages.Contains(x))
                    .ToList();

            if ((this.Packages == null || this.Packages.IsEmpty()) &&
                (this.Solutions == null || this.Solutions.IsEmpty()))
            {
                // All produced packages which are referenced within repository
                // are to be processed
                return allowedPackages;
            }

            // Process just the specified package set
            if (this.Solutions == null)
            {
                return allowedPackages
                    .Intersect(this.Packages)
                    .ToList();
            }

            // Only referenced within the repository packages produced by the
            // specified solutions are to be processed
            return this.config.Solutions.Values
                .OrderBy(x => x.BuildStep)
                .Where(x => this.Solutions.Any(c => c == x.Id))
                .SelectMany(x => x.ProducedPackages)
                .Where(x => referencedPackages.Contains(x))
                .ToList();
        }
    }
}