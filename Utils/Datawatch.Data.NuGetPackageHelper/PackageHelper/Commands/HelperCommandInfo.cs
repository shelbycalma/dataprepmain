using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    class HelperCommandInfo
    {
        public string UsageExample { get; internal set; }

        public string Description { get; internal set; }

        public string Summary { get; internal set; }

        public string ToString(string exeName, string cmdKey)
        {
            return string.Format(
                "{0} - {1}{2}{2}\t {3} {4}{2}{2} {5}{2}{2}",
                    cmdKey,
                    this.Summary,
                    Environment.NewLine,
                    exeName,
                    string.Format(this.UsageExample, cmdKey),
                    string.Format(this.Description, cmdKey));
        }
    }
}