﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;
using System.IO;

namespace Datawatch.Data.Utils.PackageHelper
{
    enum SolutionSelection
    {
        None = 0,
        Custom,
        AllReferencing,
        AllProducing,
        All
    }
}