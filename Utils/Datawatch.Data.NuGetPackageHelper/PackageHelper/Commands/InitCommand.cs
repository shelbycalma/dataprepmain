﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{

    class InitCommand : HelperCommand
    {
        public InitCommand(string token, ILogger logger)
            : base(token, logger)
        {
            this.CanExecuteWithoutConfig = true;
            this.NuspecPath = StandardFolders.DefaultNuspecFolder;
            this.PackageSourcePath = StandardFolders.DefaultPackageSourceFolder;
        }

        public string RepositoryId { get; set; }

        public string NuspecPath { get; set; }

        public string PackageSourcePath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether command execution should 
        /// only analyze repository and create configuration. Package-related 
        /// activity should be skipped.
        /// </summary>
        /// <value>
        /// <c>true</c> if only creating of configuration is required; otherwise, <c>false</c>.
        /// </value>
        public bool CreateConfigOnly { get; set; }

        public override void Execute(RepositoryConfigurationManager rcm)
        {
            var cfg = rcm.CreateConfigForRepository(
                this.NuspecPath, this.PackageSourcePath, this.RepositoryId);
            if (cfg == null)
                throw new PackageHelperException("repository configuration was not created.");

            this.ProcessNuspecs(cfg);
            rcm.SaveConfiguration(cfg);
        }

        private void ProcessNuspecs(RepositoryConfiguration cfg)
        {
            var currentDir = Directory.GetCurrentDirectory();

            string nuspecDir = Path.Combine(currentDir, this.NuspecPath);

            if (this.CreateConfigOnly == false)
            {
                if (Directory.Exists(nuspecDir) == false)
                    Directory.CreateDirectory(nuspecDir);

                string packageSourceDir = Path.Combine(currentDir, this.PackageSourcePath);
                if (Directory.Exists(packageSourceDir) == false)
                    Directory.CreateDirectory(packageSourceDir);
            }

            string version = Settings.Default.DebugPackageVersion;
            foreach (var item in cfg.Packages)
            {
                string targetPath = Path.Combine(nuspecDir, Path.GetFileName(item.Path));
                if (this.CreateConfigOnly == false)
                    this.UpdatePackageVersion(item.Path, targetPath, version);
                if (targetPath.IndexOf(currentDir, 0, StringComparison.CurrentCulture) != -1)
                    targetPath = targetPath.Substring(currentDir.Length + 1);
                item.Path = targetPath;
            }
        }

        private void UpdatePackageVersion(string nuspecPath, string targetPath, string version)
        {
            var doc = XDocument.Load(nuspecPath);
            bool done = false;
            try
            {
                var p = doc.GetElementByLocalName("package");
                if (p == null) 
                    return;

                var m = p.GetElementByLocalName("metadata");
                if (m == null)
                    return;

                var v = m.GetElementByLocalName("version");
                if (v == null)
                    return;

                v.Value = version;
                doc.Save(targetPath);
                done = true;
            }
            catch (Exception e)
            {
                done = true;
                throw new PackageHelperException("Failed to update package spec for " + nuspecPath, e);
            }
            finally
            {
                if (done == false)
                    throw new PackageHelperException("Failed to update package spec for " + nuspecPath);
            }
        }

        public override string ToString()
        {
            return string.Format("{0} -r {1} -n {2} -p {3}",
                this.Token,
                this.RepositoryId ?? StandardSubst.Null,
                this.NuspecPath ?? StandardSubst.Null,
                this.PackageSourcePath ?? StandardSubst.Null);
        }
    }
}
