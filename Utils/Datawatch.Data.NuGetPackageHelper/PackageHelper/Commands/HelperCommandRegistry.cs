using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Datawatch.Data.Utils.PackageHelper
{
    class HelperCommandRegistry
    {
        private Dictionary<string, HelperCommandInfo> info =
            new Dictionary<string, HelperCommandInfo>()
            {
                {
                    CommandToken.Init, new HelperCommandInfo()
                    {
                        UsageExample = " {0} <RepositoryId> [options]",
                        Description =
                                    "\t Options: " + Environment.NewLine + Environment.NewLine +
                                    "\t -NuspecDir or -N "  + Environment.NewLine +
                                    "\t Specifies a path to a folder with debug versions of nuspec files. Default is \".DebugNuspec\"."  + Environment.NewLine + Environment.NewLine +
                                    "\t -PackageSrcDir or -P" + Environment.NewLine +
                                    "\t Specifies a folder for local package source. Default is \".DebugPackages\"." + Environment.NewLine + 
                                    "\t It is recommended to add the contents of this folder to ignore list of the version control system" + Environment.NewLine +
                                    "\t used on a project." + Environment.NewLine + Environment.NewLine +
                                    "\t -ConfigOnly or -C" + Environment.NewLine +
                                    "\t Tells PH just to analyze repository and create configuration file and skip all other operations." + Environment.NewLine +
                                    "\t Used to avoid overwriting the manually updated nuspec files." + Environment.NewLine + Environment.NewLine +
                                    "\t Examples: " + Environment.NewLine +
                                    "\t PackageHelper.exe Init RepA" + Environment.NewLine +
                                    "\t PackageHelper.exe Init RepA -NuspecDir .debugNuspec" + Environment.NewLine +
                                    "\t PackageHelper.exe Init RepA -ConfigOnly",

                        Summary =
                        "  Creates initial data structures required for operations. " +
                        "The created structures are NOT final and require manual verification and updating."

                    }
                },
                {
                    CommandToken.Pack, new HelperCommandInfo()
                    {
                        UsageExample = " {0} (-P[ackages] <PkgA> [<PkgB> ...])|(-S[olution] <SlnA>) [options]",
                        Description = 
                                    "\t Options: " + Environment.NewLine + Environment.NewLine +
                                    "\t -AllowUnreferenced or -A" + Environment.NewLine +
                                    "\t Allows to pack unreferenced packages produced within repository." + Environment.NewLine + Environment.NewLine +
                                    "\t -Build or -B" + Environment.NewLine +
                                    "\t Sets a request to build the solution before creating packages." + Environment.NewLine + Environment.NewLine +
                                    "\t -BuildConfiguration or -BC" + Environment.NewLine +
                                    "\t Sets the build configuration name to use for building the solution. If not specified, the configuration name set in the " + Environment.NewLine +
                                    "\t solution buildInfo is taken from the repository configuration. Must be used together with -Build option. Default value" + Environment.NewLine +
                                    "\t is \"Debug\"." + Environment.NewLine + Environment.NewLine +
                                    "\t -BuildPlatform or -BP" + Environment.NewLine +
                                    "\t Sets the build platform name to use for building the solution. If not specified, the platform name set in the" + Environment.NewLine +
                                    "\t solution buildInfo is taken from the repository configuration. If buildInfo's option ignoreOtherPlatforms is set to true," + Environment.NewLine +
                                    "\t any value specified in the command line will be overriden by platform value from repository configuration." + Environment.NewLine +
                                    "\t Must be used together with -Build option. Default value is \"Any CPU\"." + Environment.NewLine + Environment.NewLine +
                                    "\t -ConfigFile or -C <PathToRepoConfigFile>" + Environment.NewLine +
                                    "\t Specifies a path to repository configuration file to use. Option is typically used for issuing PH commands outside of" + Environment.NewLine +
                                    "\t the repository root folder. " + Environment.NewLine + Environment.NewLine +
                                    "\t -PlanOnly or -PO" + Environment.NewLine +
                                    "\t Makes PackageHelper show execution plan and skip the plan execution." + Environment.NewLine + Environment.NewLine +
                                    "\t Examples:" + Environment.NewLine + 
                                    "\t Pack local debug package a1: " + Environment.NewLine +
                                    "\t PackageHelper.exe Pack -P a1" + Environment.NewLine + Environment.NewLine +
                                    "\t Pack all solution A packages including the unreferenced ones:" + Environment.NewLine +
                                    "\t PackageHelper.exe Pack -S A -AllowUnreferenced",

                        Summary =
                            "  Packs NuGet packages (produced and referenced within repository) with the specified identifiers or packs all NuGet packages " + Environment.NewLine +
                            "\t for the specified solution. "
                    }
                },
                {
                    CommandToken.Update, new HelperCommandInfo()
                    {
                        UsageExample = " {0} (Local|<PkgSrcKey>) [options]",
                        Description =
                                    "\t Options:" + Environment.NewLine + Environment.NewLine +
                                    "\t -Packages or -P ( All | <PkgA> [<PkgB> ...])" + Environment.NewLine +
                                    "\t Specifies a set of repository packages (produced and referenced) to process. Cannot be used together with -Solutions option. " + Environment.NewLine +
                                    "\t A value of \"All\" requests the processing of all packages produced and  referenced by one or more solutions of the repository." + Environment.NewLine +
                                    "\t If Packages and Solutions options are omitted, the All value is used." + Environment.NewLine + Environment.NewLine +
                                    "\t -Solutions or -S ( All | <SlnA> [<SlnB> ...])" + Environment.NewLine +
                                    "\t Specifies a set of repository solutions a package set produced and referenced by which shall be processed. " + Environment.NewLine +
                                    "\t Value \"All\" means all solutions present in repository configuration. Cannot be used together with -Packages option." + Environment.NewLine + Environment.NewLine +
                                    "\t -TargetSolutions or -T ( All | <SlnX> [<SlnY> ...])" + Environment.NewLine +
                                    "\t Allows to limit the set of solutions that should be updated. A value of \"All\" means all dependent solutions " + Environment.NewLine +
                                    "\t that can be updated. By default, \"All\" value is used." + Environment.NewLine + Environment.NewLine +
                                    "\t -BuildConfiguration or -BC" + Environment.NewLine +
                                    "\t Sets the build configuration name to use for building the solution. If not specified, the configuration name set in the" + Environment.NewLine +
                                    "\t solution buildInfo is taken from the repository configuration. Default value is \"Debug\"." + Environment.NewLine + Environment.NewLine +
                                    "\t -BuildPlatform or -BP" + Environment.NewLine +
                                    "\t Sets the build platform name to use for building the solution. If not specified, the platform name set in the" + Environment.NewLine +
                                    "\t solution buildInfo is taken from the repository configuration. If buildInfo's option ignoreOtherPlatforms is set to true," + Environment.NewLine +
                                    "\t any value specified in the command line will be overriden by platform value from repository configuration." + Environment.NewLine +
                                    "\t Default value is \"Any CPU\"." + Environment.NewLine + Environment.NewLine +
                                    "\t -ConfigFile or -C <PathToRepoConfigFile>" + Environment.NewLine +
                                    "\t Specifies a path to repository configuration file to use. Option is typically used for issuing PH commands outside of" +  Environment.NewLine +
                                    "\t the repository root folder." + Environment.NewLine + Environment.NewLine +
                                    "\t -Prerelease or -PR [versionPattern]" + Environment.NewLine +
                                    "\t Allows to use prerelease NuGet packages. Version pattern provides a way to filter package versions using regular expressions" +  Environment.NewLine +
                                    "\t and pick the highest version that matches the specified pattern." + Environment.NewLine + Environment.NewLine +
                                    "\t -PlanOnly or -PO" + Environment.NewLine +
                                    "\t Makes PackageHelper show execution plan and skip the plan execution." + Environment.NewLine + Environment.NewLine +
                                    "\t Examples:" + Environment.NewLine +
                                    "\t Switch all solutions to use local debug package source:" + Environment.NewLine +
                                    "\t PackageHelper.exe {0} Local -S All" + Environment.NewLine + Environment.NewLine +
                                    "\t Recreate package and propagate it to solutions B and C: " + Environment.NewLine +
                                    "\t PackageHelper.exe {0} Local -P a1 -T B C" + Environment.NewLine + Environment.NewLine +
                                    "\t Update solutions B and C to use package a1 with highest for branch PRJ-123 prerelease version from package source PS1: " + Environment.NewLine +
                                    "\t PackageHelper.exe {0} PS1 -P a1 -T B C -PR .*PRJ-123" ,

                        Summary =
                            "Updates solution dependencies using the specified package source. If local package source is used, PackageHelper builds solutions,"  + Environment.NewLine +
                            "\t packs NuGet packages and updates dependent solutions in a cascaded manner. "
                    }
                }
            };

        public HelperCommandRegistry()
        {
        }

        public string GenerateCommandReference()
        {
            var sb = new StringBuilder(2048);
            sb.AppendLine("Usage: PackageHelper <Command> [Options]");
            sb.AppendLine();
            sb.AppendLine("Possible commands: ");
            sb.AppendLine();

            var exeName = System.IO.Path.GetFileName(Assembly.GetEntryAssembly().Location);
            foreach (var item in this.info)
            {
                sb.AppendLine(item.Key + " - " + item.Value.Summary);
                //sb.Append(item.Value.ToString(exeName, item.Key));
            }

            return sb.ToString();
        }

        public HelperCommandInfo GetCommandInfo(string cmdKey)
        {
            HelperCommandInfo data;
            if (this.info.TryGetValue(cmdKey, out data) == true)
                return data;

            return null;
        }

        public List<string> GetCommandKeys()
        {
            return this.info.Keys.ToList();
        }
    }
}