using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Datawatch.Data.Utils.PackageHelper
{
    class PackCommand : PackageBasedHelperCommand
    {
         public PackCommand(string token, ILogger logger)
            : base(token, logger)
        {
            this.CanExecuteWithoutConfig = false;
        }

        public bool AllowUnreferenced { get; set; }

        public bool IsBuildRequired { get; set; }

        public string Solution { get; set; }

        public override void Execute(RepositoryConfigurationManager rcm)
        {
            this.config = rcm.Configuration;
            var execPlan = CreateExecutionPlan(rcm.WorkingFolder);

            if (this.CreateExecutionPlanOnly == true)
            {
                this.logger.Log(LogLevel.Info, execPlan.GetDump());
                return;
            }

            var runner = new CommandExecutionPlanRunner(this.logger);

            try
            {
                runner.ExecutePlan(execPlan);
            }
            catch (CommandExecutionException)
            {
                this.logger.Log(LogLevel.Error, "Pack command failed.");
                throw;
            }
        }

        private CommandExecutionPlan CreateExecutionPlan(string workingFolder)
        {
            var plan = new CommandExecutionPlan();
            var items = DetermineItemsToProcess();

            // Build the package producing solution
            if (this.IsBuildRequired == true)
            {
                var slnId = GetSolutionId();
                this.GenerateBuildItems(plan, slnId, items, workingFolder);
            }

            foreach (var item in items)
            {
                // Pack package
                var pkg = config.Packages[item];
                string nuspecPath = FileTools.CombinePath(workingFolder, pkg.Path);
                var pkgSrc = this.config.PackageSources[pkg.RepositoryId];
                var outDir = FileTools.CombinePath(workingFolder, pkgSrc.Path);
                var pack = new PackPackageCepItem(nuspecPath, outDir);
                plan.AddItem(pack);
            }

            return plan;
        }

        private string[] DetermineItemsToProcess()
        {
            var referencedPackages = new HashSet<string>(
                this.config.Solutions.Values
                .Where(x => x.Dependencies != null)
                .SelectMany(x => x.Dependencies));

            if (this.PackageSelection == SolutionPackageSelection.Custom)
            {
                return this.Packages;
            }

            if (this.PackageSelection == SolutionPackageSelection.CustomBySolutions)
            {
                if (this.AllowUnreferenced == true)
                    return this.config.Solutions.Values
                        .Where(x => x.Id == this.Solution)
                        .SelectMany(s => s.ProducedPackages)
                        .ToArray();

                return this.config.Solutions.Values
                    .Where(x => x.Id == this.Solution)
                    .SelectMany(s => s.ProducedPackages
                            .Where(p => referencedPackages.Contains(p)))
                    .ToArray();
            }

            throw new CommandExecutionException("Failed to find items to process.");
        }

        private string GetSolutionId()
        {
            if (this.PackageSelection == SolutionPackageSelection.CustomBySolutions)
                return this.Solution;

            return this.config.Packages[this.Packages[0]].SolutionId;
        }
    }
}