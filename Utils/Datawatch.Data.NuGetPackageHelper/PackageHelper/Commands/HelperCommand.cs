﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{
    abstract class HelperCommand
    {
        protected ILogger logger;

        public HelperCommand(string token, ILogger logger)
        {
            this.Token = token;
            this.logger = logger;
        }

        public string Token { get; protected set; }

        public bool CanExecuteWithoutConfig { get; protected set; }
        public abstract void Execute(RepositoryConfigurationManager rcm);
    }
}