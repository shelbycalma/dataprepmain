﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;
using System.IO;

namespace Datawatch.Data.Utils.PackageHelper
{
    enum SolutionPackageSelection
    {
        None = 0,

        /// <summary>
        /// A set of manually specified packages.
        /// </summary>
        Custom,

        /// <summary>
        /// A set of packages produced by a set of manually specified solutions.
        /// </summary>
        CustomBySolutions,

        AllProducedAndReferenced,
        ProducedByAllSolutionsAndReferenced,
        AllProduced,
    }
}