﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Datawatch.Data.Utils.PackageHelper
{
    class BuildOptionsGenerator
    {
        public const string AnyCpuPlatformValue = "\"" + BuildPlatformsTools.AnyCpu + "\"";
        public const string DefaultConfiguration = "Debug";

        public static string DefaultPlatform
        {
            get { return null; }// AnyCpuPlatformValue; }
        }

        public static string GetConfiguration(BuildInfo info, string config)
        {
            if (info != null)
                return string.IsNullOrWhiteSpace(config) == true
                    ? info.Configuration
                    : config;

            return string.IsNullOrWhiteSpace(config) == true
                    ? DefaultConfiguration
                    : config;
        }

        public static BuildInfo GetDefaultBuildInfo()
        {
            return new BuildInfo()
            {
                Configuration = DefaultConfiguration,
                Platform = AnyCpuPlatformValue,
                IgnoreOtherPlatforms = false
            };
        }

        public static string GetPlatform(BuildInfo info, string cmdOptionsPlatform)
        {
            if (info != null)
            {
                if (string.IsNullOrWhiteSpace(cmdOptionsPlatform) == false)
                    return BuildPlatformsTools.IsAnyCpuPlatform(cmdOptionsPlatform) == true ||
                        (BuildPlatformsTools.IsAnyCpuPlatform(info.Platform) == true &&
                            info.IgnoreOtherPlatforms == true)
                        ? BuildPlatformsTools.AnyCpu
                        : cmdOptionsPlatform;

                return BuildPlatformsTools.IsAnyCpuPlatform(info.Platform) == true
                        ? BuildPlatformsTools.AnyCpu
                        : info.Platform;
            }

            return string.IsNullOrWhiteSpace(cmdOptionsPlatform) == true
                    ? DefaultPlatform
                    : BuildPlatformsTools.IsAnyCpuPlatform(cmdOptionsPlatform) == true
                        ? BuildPlatformsTools.AnyCpu
                        : cmdOptionsPlatform;
        }
    }
}
