﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    [Flags]
    public enum BuildPlatforms
    {
        Default = 0,
        AnyCpu = 1,
        x86 = 2,
        x64 = 4
    }

    static class BuildPlatformsTools
    {
        public const string AnyCpu = "Any CPU";
        public const string X86 = "x86";
        public const string X64 = "x64";

        private const string X86U = "X86";
        private const string X64U = "X64";

        public static string GetName(this BuildPlatforms val)
        {
            switch (val)
            {
                case BuildPlatforms.Default:
                    return null;

                case BuildPlatforms.AnyCpu:
                    return AnyCpu;

                case BuildPlatforms.x86:
                    return X86;

                case BuildPlatforms.x64:
                    return X64;

                default:
                    throw new ArgumentException(
                        "Unknown build platform value - \"" + val.ToString() + "\"");
            }
        }

        public static BuildPlatforms ParsePlatforms(string platformsString)
        {
            BuildPlatforms platforms;
            ParsePlatforms(platformsString, true, out platforms);
            return platforms;
        }

        public static bool TryParsePlatforms(string platformsString, out BuildPlatforms platforms)
        {
            return ParsePlatforms(platformsString, false, out platforms);
        }

        private static bool ParsePlatforms(string platformsString, bool throwException, out BuildPlatforms platforms)
        {
           platforms = BuildPlatforms.Default;
            if (string.IsNullOrWhiteSpace(platformsString) == true)
                return true;

            var parts = platformsString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var part in parts)
            {
                var p = part.Trim().ToUpper();
                if (IsAnyCpuPlatform(p) == true)
                {
                    platforms |= BuildPlatforms.AnyCpu;
                    continue;
                }

                if (string.CompareOrdinal(p, X86U) == 0)
                {
                    platforms |= BuildPlatforms.x86;
                    continue;
                }

                if (string.CompareOrdinal(p, X64U) == 0)
                {
                    platforms |= BuildPlatforms.x64;
                    continue;
                }

                if (throwException == true)
                {
                    throw new ArgumentException(
                        "Unknown build platform value - \"" + p + "\"");
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsAnyCpuPlatform(string platform)
        {
            var p = platform.ToUpper();
            return
                string.CompareOrdinal(p, "ANY CPU") == 0 ||
                string.CompareOrdinal(p, "ANYCPU") == 0 ||
                string.CompareOrdinal(p, "\"ANY CPU\"") == 0;
        }
    }
}
