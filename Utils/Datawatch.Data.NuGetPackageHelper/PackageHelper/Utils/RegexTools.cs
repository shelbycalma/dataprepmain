﻿using System;
using System.Text.RegularExpressions;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class RegexTools
    {
        public static bool IsValidRegexPattern(string pattern)
        {
            try
            {
                new Regex(pattern);
                return true;
            }
            catch (ArgumentException)
            {
                return false;
            }
        }
    }
}
