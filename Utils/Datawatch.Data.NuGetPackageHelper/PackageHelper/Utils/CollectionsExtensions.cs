﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;
using System.IO;
using System.Collections;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class CollectionsExtensions
    {
        public static bool IsEmpty<T>(this IEnumerable<T> col)
        {
            return col != null && col.Any() == false;
        }

        //public static bool IsEmpty(this Array array)
        //{
        //    return array != null && array.Length == 0;
        //}
    }
}