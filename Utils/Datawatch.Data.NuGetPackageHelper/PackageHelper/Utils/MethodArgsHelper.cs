﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    public static class MethodArgsHelper
    {
       public static void CheckFilePath(string path, string description)
        {
            if (FileTools.VerifyFilePath(path) == false)
                throw new ArgumentException(
                    string.Format(
                        "Path to {0} is invalid - \"{1}\".", description, path));
        }
    }
}