﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Datawatch.Data.Utils.PackageHelper
{
    enum LogLevel
    {
        Debug,
        Info,
        Warn,
        Error,
        Fatal
    }

    interface ILogger
    {
        void Log(LogLevel level, string message);
        void Log(LogLevel level, string format, params object[] args);
    }

    class PackageHelperLogger : ILogger
    {
        private Logger logger;
        public PackageHelperLogger(string logFile = null)
        {
            if (string.IsNullOrWhiteSpace(logFile) == true)
                logFile = "${basedir}/PackageHelper.log";

            var config = new LoggingConfiguration();
            var target = new FileTarget();
            target.FileName = logFile;
            target.Layout = @"${date:format=yyyy-MM-dd HH\:mm\:ss.fff} ${level} ${message}";
            target.ArchiveAboveSize = 10485760;
            target.MaxArchiveFiles = 0;
            config.AddTarget("file", target);
            config.LoggingRules.Add(new LoggingRule("*", NLog.LogLevel.Debug, target));

            var consoleTarget = new ColoredConsoleTarget();
            consoleTarget.Layout = "${message}";
            
            var rule = new LoggingRule("*", consoleTarget);
            rule.EnableLoggingForLevel(NLog.LogLevel.Info);
            rule.EnableLoggingForLevel(NLog.LogLevel.Warn);
            rule.EnableLoggingForLevel(NLog.LogLevel.Error);
            config.LoggingRules.Add(rule);
            LogManager.Configuration = config;

            this.logger = LogManager.GetLogger("PackageHelper");            
        }

        public void Log(LogLevel level, string message)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    logger.Debug(message);
                    break;
                case LogLevel.Info:
                    logger.Info(message);
                    break;
                case LogLevel.Warn:
                    logger.Warn(message);
                    break;
                case LogLevel.Error:
                    logger.Error(message);
                    break;
                case LogLevel.Fatal:
                    logger.Fatal(message);
                    break;
                default:
                    break;
            }
        }

        public void Log(LogLevel level, string format, params object[] args)
        {
            switch (level)
            {
                case LogLevel.Debug:
                    logger.Debug(format, args);
                    break;
                case LogLevel.Info:
                    logger.Info(format, args);
                    break;
                case LogLevel.Warn:
                    logger.Warn(format, args);
                    break;
                case LogLevel.Error:
                    logger.Error(format, args);
                    break;
                case LogLevel.Fatal:
                    logger.Fatal(format, args);
                    break;
                default:
                    break;
            }
        }
    }
}
