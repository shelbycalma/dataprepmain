﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class Serializer
    {
        public static string Serialize<T>(T obj)
        {
            var serializer = new XmlSerializer(typeof(T));

            using (var stream = new MemoryStream(65536))
            {
                stream.Position = 0;
                var xsn = new XmlSerializerNamespaces();
                xsn.Add(string.Empty, string.Empty);
                serializer.Serialize(stream, obj, xsn);
                return Encoding.UTF8.GetString(stream.GetBuffer(), 0, (int)stream.Length);
            }
        }

        public static T Deserialize<T>(string data)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (var reader = new StringReader(data))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
    }
}
