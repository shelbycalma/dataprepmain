﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class LinqToXMLExtensions
    {
        public static XElement GetElementByLocalName(this XDocument doc, string name)
        {
            return doc.Elements().FirstOrDefault(x => x.Name.LocalName == name);
        }

        public static XElement GetElementByLocalName(this XElement element, string name)
        {
            return element.Elements().FirstOrDefault(x => x.Name.LocalName == name);
        }

        public static IEnumerable<XElement> GetElementsByLocalName(this XElement doc, string name)
        {
            return doc.Elements().Where(x => x.Name.LocalName == name);
        }
    }
}