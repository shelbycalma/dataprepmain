﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Datawatch.Data.Utils.PackageHelper
{
    class ValidationResult
    {
        public ValidationResult(string errors, string warnings)
        {
            this.Errors = errors;
            this.Warnings = warnings;
        }

        public string Errors { get; private set; }
        public string Warnings { get; private set; }

        public bool HasWarnings
        {
            get { return string.IsNullOrWhiteSpace(this.Warnings) == false; }
        }

        public bool HasErrors
        {
            get { return string.IsNullOrWhiteSpace(this.Errors) == false; }
        }

        public bool CanWork
        {
            get
            {
                return string.IsNullOrEmpty(this.Errors);
            }
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            if (this.HasErrors == true)
            {
                sb.AppendLine("Errors:");
                sb.AppendLine(this.Errors);
            }
            else
            {
                sb.AppendLine("No errors.");
            }

            if (this.HasWarnings == true)
            {
                sb.AppendLine("Warnings:");
                sb.AppendLine(this.Warnings);
            }
            else
            {
                sb.AppendLine("No warnings.");
            }

            return sb.ToString();
        }
    }
}