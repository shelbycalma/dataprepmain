﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datawatch.Data.Utils.PackageHelper.Properties;

namespace Datawatch.Data.Utils.PackageHelper
{
    class ValidationResultBuilder
    {
        private int errorCount = 0;
        private int warningCount = 0;
        private StringBuilder wsb = new StringBuilder();
        private StringBuilder esb = new StringBuilder();

        public void AddWarning(string message)
        {
            this.wsb.AppendLine(message);
            this.warningCount++;
        }

        public void AddWarning(string format, params object[] args)
        {
            this.wsb.AppendFormat(format + Environment.NewLine, args);
            this.warningCount++;
        }

        public void AddError(string message)
        {
            this.esb.AppendLine(message);
            this.errorCount++;
        }

        public void AddError(string format, params object[] args)
        {
            this.esb.AppendFormat(format + Environment.NewLine, args);
            this.errorCount++;
        }

        public void Reset()
        {
            this.warningCount = 0;
            this.errorCount = 0;
            this.wsb.Clear();
            this.esb.Clear();
        }

        public int ErrorCount
        {
            get { return this.errorCount; }
        }

        public int WarningCount
        {
            get { return this.warningCount; }
        }

        public ValidationResult BuildResult()
        {
            return new ValidationResult(this.esb.ToString(), this.wsb.ToString());
        }
    }
}