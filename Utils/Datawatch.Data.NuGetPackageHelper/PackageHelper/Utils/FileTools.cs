﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Data.Utils.PackageHelper
{
    static class FileTools
    {
        public const string UpperFolder = "..";
        public const string CurrentFolder = ".";
        public const string NetworkPathRoot = @"\\";
        public static readonly char[] PathSeparators = { '\\', '/' };

        public static bool BackupFileIfExists(string path, string bakPath = null)
        {
            if (File.Exists(path) == false)
                return false;

            if (string.IsNullOrWhiteSpace(bakPath) == true)
                bakPath = path + "." + StandardFileExtensions.BackupExtension;

            if (File.Exists(bakPath) == true)
                File.Delete(bakPath);
            File.Move(path, bakPath);

            return true;
        }

        public static bool VerifyFilePath(string path)
        {
            return string.IsNullOrWhiteSpace(path) == false &&
                IsValidPath(path) &&
                File.Exists(path) == true;
        }

        public static string CreatePathRelativeToExecAssembly(string path)
        {
            if (Path.IsPathRooted(path) == true)
                return path;

            var execPath = Assembly.GetExecutingAssembly().Location;
            return Path.Combine(Path.GetDirectoryName(execPath), path);
        }

        public static bool IsValidPath(string path)
        {
            if (string.IsNullOrEmpty(path) == true)
                return false;

            var invalidFileNameChars = Path.GetInvalidFileNameChars();
            var fileName = Path.GetFileName(path);
            if (string.IsNullOrEmpty(fileName) == false &&
                invalidFileNameChars.Any(x => fileName.IndexOf(x) != -1) == true)
                return false;

            var invalidPathChars = Path.GetInvalidPathChars();
            var dir = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(dir) == false &&
                invalidPathChars.Any(x => dir.IndexOf(x) != -1) == true)
                return false;

            return true;
        }

        public static string EscapePath(string path)
        {
            var p = path.Trim();
            if (p.Contains(' ') == false)
                return p;

            bool needsLeft = p[0] != '"';
            bool needsRight = p[p.Length - 1] != '"';

            return
                (needsLeft ? "\"" : string.Empty) +
                p +
                (needsRight ? "\"" : string.Empty);
        }

        public static string NormalizePath(string path, bool escapePath = false, char separator = '\\')
        {
            if (string.IsNullOrWhiteSpace(path) == true)
                throw new ArgumentException("path");

            if (path.Contains("\""))
                path = path.Replace("\"", string.Empty);

            var parts = new List<String>(path.Split(PathSeparators));
            int i = 0;
            bool isRooted = Path.IsPathRooted(path);
            int dirCount = isRooted == true ? -1 : 0;

            while (i < parts.Count)
            {
                if (parts[i] == UpperFolder)
                {
                    if (dirCount <= 0)
                    {
                        if (isRooted == true)
                            throw new ArgumentException("path");
                        i++;
                        continue;
                    }

                    parts.RemoveAt(i);
                    i--;
                    parts.RemoveAt(i);
                    dirCount--;
                    continue;
                }

                if (parts[i] == CurrentFolder)
                {
                    if (i != 0)
                    {
                        parts.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                    continue;
                }

                if (parts[i] == string.Empty &&
                    i != 0 &&
                    i != parts.Count - 1)
                {
                    parts.RemoveAt(i);
                    continue;
                }

                i++;
                dirCount++;
            }

            string result = string.Join(new string(separator, 1), parts);
            return result.Contains(" ") == false || escapePath == false
                ? result
                : EscapePath(result);
        }

        public static string CombinePath(string basePath, string relativePath, bool escapePath = false, bool guardBaseDir = false)
        {
            if (string.IsNullOrWhiteSpace(basePath) == true)
                throw new ArgumentException("Argument 'basePath' has invalid value.");

            if (string.IsNullOrWhiteSpace(relativePath) == true)
                return basePath;

            string result = NormalizePath(Path.Combine(basePath, relativePath), escapePath);
            if (guardBaseDir == false)
                return result;

            if (result.StartsWith(NormalizePath(basePath), StringComparison.CurrentCulture) == true)
                return result;

            throw new ArgumentOutOfRangeException("Resulting path violates base directory boundaries: " + result);
        }

        public static string GetRelativePath(string basePath, string targetPath, char separator = '\\')
        {
            if (string.IsNullOrWhiteSpace(basePath) == true)
                throw new ArgumentException("Base path has invalid value.");

            if (string.IsNullOrWhiteSpace(targetPath) == true)
                throw new ArgumentException("Target path has invalid value.");

            if (Path.IsPathRooted(targetPath) == false && Path.IsPathRooted(basePath) == false)
                throw new ArgumentException("Both base and target paths should be absolute.");

            string baseRoot = Path.GetPathRoot(basePath);
            string targetRoot = Path.GetPathRoot(targetPath);

            if (string.Compare(baseRoot, targetRoot, StringComparison.OrdinalIgnoreCase) != 0)
                throw new ArgumentException("Base and target paths have different roots.");

            var baseParts = basePath.Split(FileTools.PathSeparators, StringSplitOptions.RemoveEmptyEntries);
            var targetParts = targetPath.Split(FileTools.PathSeparators, StringSplitOptions.RemoveEmptyEntries);

            if (baseParts.Length == 0 && targetParts.Length == 0)
                return new string(separator, 1);

            var sb = new StringBuilder();
            if (baseParts.Length == 0)
            {
                return string.Join(separator.ToString(), targetParts);
            }

            if (targetParts.Length == 0 && PathSeparators.Any(c => c == targetPath[0]) == true)
            {
                for (int j = 0; j < baseParts.Length; j++)
                    sb.Append(UpperFolder + separator);
                return sb.ToString();
            }

            if (baseParts.Length > 0 && targetParts.Length > 0 &&
                    (baseParts[0].Contains(Path.VolumeSeparatorChar) == true ||
                     targetParts[0].Contains(Path.VolumeSeparatorChar) == true) &&
                    string.Compare(baseParts[0], targetParts[0], StringComparison.CurrentCulture) != 0)
                throw new ArgumentException(
                    "Can't create relative path for the specified target and base paths.");

            for (int i = 0; i < targetParts.Length; i++)
            {
                if (i < baseParts.Length)
                {
                    if (string.Compare(baseParts[i], targetParts[i], StringComparison.CurrentCulture) == 0)
                        continue;

                    for (int j = i; j < baseParts.Length; j++)
                    {
                        sb.Append(UpperFolder + separator);
                    }
                }

                sb.Append(targetParts[i] + separator);
            }

            return sb.ToString();
        }

        public static string GetDirectoryName(string path)
        {
            if (string.IsNullOrWhiteSpace(path) == true)
                throw new ArgumentException("Argument 'path' has invalid value.");

            int len = path.Length;
            int index = -1;
            for (int i = len - 1; i >= 0; i--)
            {
                var c = path[i];
                if (PathSeparators.Any(x => x == c))
                {
                    if (index == -1)
                    {
                        index = i;
                        continue;
                    }

                    return path.Substring(i + 1, index - i);
                }
            }

            if (index == -1)
                return path;

            if (index == len - 1)
                return path.Substring(0, index);

            return path.Substring(index + 1);
        }
    }
}
