﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace Datawatch.Data.Utils.PackageHelper
{
    internal class PackageHelper
    {
        private ILogger logger;
        private CommandLineParser parser;
        private RepositoryConfigurationManager repoConfigMgr;
        private CommandDataValidator validator;

        public PackageHelper(ILogger logger)
        {
            this.logger = logger;
            this.parser = new CommandLineParser(logger);
            this.validator = new CommandDataValidator();
            this.repoConfigMgr = new RepositoryConfigurationManager(logger);
        }

        public bool NoConfiguration { get; private set; }

        public string GetHelpText()
        {
            var hcr = new HelperCommandRegistry();
            return hcr.GenerateCommandReference();
        }

        public void ProcessCommand(string[] args)
        {
            var cmd = ParseCommand(args);
            cmd.Execute(this.repoConfigMgr);
        }

        private HelperCommand ParseCommand(string[] args)
        {
            var cmd = this.parser.Parse(args);
            if (cmd is ShowHelpCommand == true)
                return cmd;

            ValidationResult validationResult = null;
            var pcmd = cmd as PackageBasedHelperCommand;
            if (pcmd != null &&
                this.repoConfigMgr.LoadConfiguration(pcmd.ConfigFilePath) != null)
            {
                validationResult = this.validator.Validate(
                    cmd, new RepositoryObjectVerifier(this.repoConfigMgr.Configuration));
            }
            else
            {
                validationResult = this.validator.Validate(cmd, null);
                this.NoConfiguration = true;
            }

            if (validationResult.CanWork == false)
                throw new PackageHelperException(
                    "command data validation failed:" + Environment.NewLine + validationResult);

            if (string.IsNullOrEmpty(validationResult.Warnings) == false)
                this.logger.Log(LogLevel.Warn, validationResult.Warnings);

            if (this.NoConfiguration == true &&
                cmd.CanExecuteWithoutConfig == false)
            {
                throw new PackageHelperException(
                    "command " + cmd.Token +
                    " cannot be run without configuration. " +
                    "Please use Init command to create a configuration file.");
            }

            return cmd;
        }
    }
}