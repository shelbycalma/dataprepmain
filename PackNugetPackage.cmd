@echo off

echo.
echo Packs the specified NuGet package.
echo Usage: PackNugetPackage.cmd ^<PackageId^>
echo.

if [%1]==[] goto :eof

call ph.cmd Pack -a -p %1
