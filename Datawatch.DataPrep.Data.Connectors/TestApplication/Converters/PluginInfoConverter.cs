﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Globalization;
using System.Windows.Data;

namespace TestApplication
{
    class PluginInfoConverter : System.Windows.Data.IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string result = string.Empty;
            IDataPlugin plugin = value as IDataPlugin;
            if (plugin != null)
            {
                result = string.Format("{0} (Id = '{1}', IsObsolete = '{2}')",
                    plugin.Title ?? string.Empty,
                    plugin.Id ?? string.Empty,
                    plugin.IsObsolete.ToString());
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    class RowLimitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var limit = (RowLimit)value;
            return (int)limit;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (targetType == typeof(RowLimit))
                return (RowLimit)value;

            throw new ValueUnavailableException();
        }
    }

}