﻿namespace TestApplication
{
    interface IDialogService
    {
        bool? ShowDialog(ViewModelBase viewModel);
        IFormLog AppLog { get; }
    }
}