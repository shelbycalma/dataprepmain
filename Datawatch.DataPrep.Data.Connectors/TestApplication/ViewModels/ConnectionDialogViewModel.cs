﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace TestApplication
{
    class ConnectionDialogViewModel : ViewModelBase
    {
        private ICommand okCommand;

        public ConnectionDialogViewModel(IDialogService dlgSvc)
            : base(dlgSvc)
        {
        }

        public object PluginConfigElement { get; set; }

        public string WindowTitle { get; set; }

        public string PluginName { get { return PluginUI.Plugin.Title; } }

        public PropertyBag Settings { get; private set; }

        public IPluginUI<IDataPlugin> PluginUI { get; set; }
        public void ApplySettings()
        {
            if (PluginUI != null && PluginConfigElement != null)
            {
                this.Settings = PluginUI.GetSetting(PluginConfigElement);
            }

        }

        public ICommand OkCommand
        {
            get
            {
                if (this.okCommand == null)
                    this.okCommand = new DelegateCommand(
                        OkClicked, CanClickOk);

                return this.okCommand;
            }
        }

        private bool CanClickOk()
        {
            throw new NotImplementedException();
        }

        private void OkClicked()
        {
        }
    }
}