﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;

namespace TestApplication
{
    abstract class ViewModelBase : INotifyPropertyChanged
    {
        protected IDialogService dialogService;

        public event PropertyChangedEventHandler PropertyChanged;

        public ViewModelBase(IDialogService dlgSvc)
        {
            if (dlgSvc == null)
                throw new ArgumentNullException("dlgSvc");

            this.dialogService = dlgSvc;
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}