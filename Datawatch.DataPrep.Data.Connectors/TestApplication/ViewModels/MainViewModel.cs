﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace TestApplication
{
    class MainViewModel : ViewModelBase
    {
        private readonly PluginManager pluginManager;
        private ICommand addParameterCommand;
        private ICommand connectCommand;
        string connectionInfo;
        RowLimit dataLimit;
        private string latestConnectorId;
        private PropertyBag latestConnectorSettings;
        private string pluginDirectory;
        private RealtimePreviewRunner realtimePreviewRunner;
        private List<Record> records;
        private ICommand refreshCommand;
        private ICommand reportCommand;
        private IDataPlugin selectedDataPlugin;
        private PropertyBag settings;
        private ITable table;
        private ICommand toggleRealTimeCommand;
        public MainViewModel(PluginManager pm, string pluginDir, IDialogService dlgSvc)
            : base(dlgSvc)
        {
            this.pluginManager = pm;
            this.pluginDirectory = pluginDir;

#if DEVELOP
            var excel = pluginManager.DataPlugins.Where(x => x.Title.Contains("Excel"));

            this.DataPlugins = new ObservableCollection<IDataPlugin>(
                excel
                .Union(
                    pluginManager.DataPlugins
                    .Except(excel)
                    .OrderBy(x => x.Title)));
#else

            this.DataPlugins = new ObservableCollection<IDataPlugin>(
                    pluginManager.DataPlugins.OrderBy(x => x.Title));
#endif
            this.FailedDataPlugins = new ObservableCollection<PluginLoadingFailureInfo>(
                pluginManager.NotLoadedPluginList.OrderBy(x => x.PluginTitle));

            this.DataRowsLimit = RowLimit.Ten;
            this.ConnectionInfo = "No connection";
            this.Parameters = new ObservableCollection<Parameter>();
        }

        public event EventHandler<AddParameterEventArgs> AddParameter;

        public event EventHandler<PrepareToLoadDataEventArgs> PrepareToLoadData;
        public ICommand AddParameterCommand
        {
            get
            {
                if (this.addParameterCommand == null)
                    this.addParameterCommand = new DelegateCommand(
                        AddNewParameter,
                        () => true);

                return this.addParameterCommand;
            }
        }

        public bool CanLimitOutput
        {
            get
            {
                return
                    this.SelectedDataPlugin != null &&
                    this.SelectedDataPlugin is IRealtimeDataPlugin == false;
            }
        }

        public ICommand ConnectCommand
        {
            get
            {
                if (connectCommand == null)
                {
                    connectCommand = new DelegateCommand(
                        DoConnect, () => SelectedDataPlugin != null);
                }
                return connectCommand;
            }
        }

        public string ConnectionInfo
        {
            get
            {
                return connectionInfo;
            }

            set
            {
                connectionInfo = value;
                OnPropertyChanged();
            }
        }

        public PropertyBag ConnectorSettings
        {
            get { return this.latestConnectorSettings; }
        }

        public ObservableCollection<IDataPlugin> DataPlugins { get; private set; }

        public RowLimit DataRowsLimit
        {
            get
            {
                return dataLimit;
            }

            set
            {
                dataLimit = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<PluginLoadingFailureInfo> FailedDataPlugins { get; private set; }
        public ObservableCollection<Parameter> Parameters
        {
            get; private set;
        }

        public List<Record> Records
        {
            get { return records; }
            set
            {
                records = value;
                OnPropertyChanged("Records");
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                if (refreshCommand == null)
                {
                    refreshCommand = new DelegateCommand(
                        DoRefresh, () => settings != null);
                }
                return refreshCommand;
            }
        }

        public ICommand ReportCommand
        {
            get
            {
                if (this.reportCommand == null)
                {
                    this.reportCommand = new DelegateCommand(
                        DoReport, () => this.pluginManager != null);
                }

                return this.reportCommand;
            }
        }

        public IDataPlugin SelectedDataPlugin
        {
            get { return selectedDataPlugin; }
            set
            {
                selectedDataPlugin = value;
                OnPropertyChanged();
                OnPropertyChanged("SelectedDataPluginInfo");
            }
        }

        public void OnWindowLoaded()
        {
            Log("{0} plugins were successfully loaded.", this.pluginManager.DataPlugins.Count().ToString());
            Log("{0} plugins failed to load. See report for details.", this.pluginManager.NotLoadedPluginList.Count().ToString());
        }
        protected virtual void FirePrepareToLoadData(PrepareToLoadDataEventArgs args)
        {
            var handler = this.PrepareToLoadData;
            if (handler != null)
                handler(this, args);
        }

        private void AddNewParameter()
        {
            int index = this.Parameters.Count;
            var p = new Parameter() { Name = "p" + index.ToString() };
            this.Parameters.Add(p);
            this.FireAddParameter(p, index);
        }

        private List<Record> CreateRecords()
        {
            List<Record> list = new List<Record>();

            for (int i = 0; i < table.RowCount; i++)
            {
                list.Add(new Record(table, table.GetRow(i)));
            }

            return list;
        }

        private void DoConnect()
        {
            try
            {
                IPluginUI<IDataPlugin> pluginUI = pluginManager.GetPluginUI(SelectedDataPlugin.Id);
                var parameters = this.GetParameterValues();

                if (string.IsNullOrEmpty(this.latestConnectorId) == true ||
                    this.latestConnectorId != SelectedDataPlugin.Id)
                    settings = null;

                if (settings == null)
                {
                    SelectedDataPlugin.Initialize(true, pluginManager, settings, new PropertyBag());
                    if (pluginUI == null) return;

                    settings = pluginUI.DoConnect(parameters);
                    this.latestConnectorId = SelectedDataPlugin.Id;
                    this.latestConnectorSettings = settings;
                }
                else
                {
                    var ci = new ConnectionDialogViewModel(this.dialogService)
                    {
                        WindowTitle = pluginUI.GetTitle(this.settings),
                        PluginConfigElement = pluginUI.GetConfigElement(settings, parameters),
                        PluginUI = pluginUI
                    };

                    var result = this.dialogService.ShowDialog(ci);
                    if (result == false)
                        return;

                    this.settings = ci.Settings;
                }

                if (this.settings == null)
                    return;

                this.PopulateDataTableView(pluginUI.Plugin, settings, parameters);
                this.ConnectionInfo = pluginUI.GetTitle(settings);
                OnPropertyChanged("CanLimitOutput");
                this.Log("Successfully connected to " + this.ConnectionInfo);
            }
            catch (Exception e)
            {
                this.Log("Connection failed:" + Environment.NewLine + e.ToString());
            }
        }

        private void DoRefresh()
        {
            try
            {
                IPluginUI<IDataPlugin> pluginUI = pluginManager.GetPluginUI(SelectedDataPlugin.Id);
                var parameters = GetParameterValues();
                this.PopulateDataTableView(SelectedDataPlugin, settings, parameters);
                this.ConnectionInfo = pluginUI.GetTitle(settings);
            }
            catch (Exception e)
            {
                this.Log("Error during data refreshing: " + Environment.NewLine +
                    e.ToString());
            }
        }

        private void DoReport()
        {
            try
            {
                var report = this.pluginManager.GeneratePluginLoadingReport();
                var vm = new ConnectorDiscoveryReportViewModel(
                    report, this.pluginDirectory, this.dialogService);
                this.dialogService.ShowDialog(vm);
            }
            catch (Exception e)
            {
                this.Log("Error while generating connector discovery report: " +
                    Environment.NewLine + e.ToString());
            }
        }
        private void FireAddParameter(Parameter p, int index)
        {
            var handler = this.AddParameter;
            if (handler != null)
                handler(this, new AddParameterEventArgs(p, index));
        }
        private IEnumerable<ParameterValue> GetParameterValues()
        {
            if (this.Parameters == null || this.Parameters.Any() == false)
                return null;

            return this.Parameters.Select(x =>
                new ParameterValue(x.Name,
                    new StringParameterValue()
                    {
                        Value = ParameterHelper.RemoveSingleQuotes(x.Value)
                    }));
        }
        private void PopulateDataTableView(IDataPlugin plugin, PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            var limit = this.DataRowsLimit.ToNumber();
            if (limit == 0 || plugin is IRealtimeDataPlugin)
            {
                table = plugin.GetAllData(null, null, settings, parameters);
            }
            else
            {
                table = plugin.GetData(null, null, settings, parameters, limit);
            }

            var headers = new List<string>();
            for (int i = 0; i < table.ColumnCount; i++)
            {
                var column = table.GetColumn(i);
                var metadata = column.MetaData;
                headers.Add(string.IsNullOrEmpty(metadata.Title) == false
                    ? metadata.Title
                    : column.Name ?? "Column " + i.ToString());
            }

            this.ConnectionInfo = "No connection";
            var args = new PrepareToLoadDataEventArgs(headers);
            this.FirePrepareToLoadData(args);
            this.Records = CreateRecords();
        }
#region Window log

        private void Log(string message)
        {
            if (this.dialogService.AppLog != null)
                this.dialogService.AppLog.Log(message);
        }

        private void Log(string format, params object[] args)
        {
            if (this.dialogService.AppLog != null)
                this.dialogService.AppLog.Log(format, args);
        }

#endregion Window log

#region RealTime Preview

        public bool IsRealtimePreviewRunning
        {
            get { return realtimePreviewRunner != null; }
        }

        public string RealtimePreviewLabel
        {
            get
            {
                return IsRealtimePreviewRunning ? "Stop" : "Start";
            }
        }

        public ICommand ToggleRealTimeCommand
        {
            get
            {
                if (toggleRealTimeCommand == null)
                {
                    toggleRealTimeCommand = new DelegateCommand(
                        ToggleRealTimePreview, CanToggleRealTime);
                }
                return toggleRealTimeCommand;
            }
        }

        public void StopRealtimePreview()
        {
            if (realtimePreviewRunner == null)
                return;

            DetachRealtimePreviewRunner(realtimePreviewRunner);
            realtimePreviewRunner.Stop();
            realtimePreviewRunner = null;
        }

        private void AttachRealtimePreviewRunner(RealtimePreviewRunner runner)
        {
            runner.DatasourceError += RealtimePreviewRunner_DatasourceError;
            runner.Stopped += RealtimePreviewRunner_Stopped;
        }

        private bool CanToggleRealTime()
        {
            return this.SelectedDataPlugin != null &&
                this.SelectedDataPlugin is IRealtimeDataPlugin &&
                this.table != null;
        }

        private void DetachRealtimePreviewRunner(RealtimePreviewRunner runner)
        {
            runner.DatasourceError -= RealtimePreviewRunner_DatasourceError;
            runner.Stopped -= RealtimePreviewRunner_Stopped;
        }

        private void RealtimePreviewRunner_DatasourceError(object sender,
            DatasourceErrorEventArgs e)
        {
            this.Log("Realtime preview error: " + Environment.NewLine + e.Error.ToString());
        }

        private void RealtimePreviewRunner_Stopped(object sender, EventArgs e)
        {
            DetachRealtimePreviewRunner(this.realtimePreviewRunner);
            this.realtimePreviewRunner = null;
            OnPropertyChanged("IsRealtimePreviewRunning");
            OnPropertyChanged("RealtimePreviewLabel");
        }

        private void RedrawGrid()
        {
            this.Records = CreateRecords();
        }

        private void ToggleRealTimePreview()
        {
            try
            {
                if (this.realtimePreviewRunner == null)
                {
                    var ctx = new DatasourceContext()
                    {
                        IsRealTime = true,
                        Table = this.table,
                        PluginId = this.SelectedDataPlugin.Id,
                        PluginSettings = settings
                    };

                    realtimePreviewRunner =
                                    new RealtimePreviewRunner(pluginManager, ctx);
                    AttachRealtimePreviewRunner(realtimePreviewRunner);
                    realtimePreviewRunner.Start(RedrawGrid);
                    this.Log("Real time data preview has started.");
                }
                else
                {
                    StopRealtimePreview();
                    this.Log("Real time data preview has stopped.");
                }

                OnPropertyChanged("IsRealtimePreviewRunning");
                OnPropertyChanged("RealtimePreviewLabel");
            }
            catch (Exception e)
            {
                this.Log("Failed to toggle realtime preview: " + 
                    Environment.NewLine + e.ToString());
            }
        }
#endregion RealTime Preview

    }
}

