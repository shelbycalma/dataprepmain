﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Microsoft.Win32;

namespace TestApplication
{
    class ConnectorDiscoveryReportViewModel : ViewModelBase
    {
        private ICommand saveReportCommand;

        public ConnectorDiscoveryReportViewModel(string report, string baseDir, IDialogService dlgSvc) 
            : base(dlgSvc)
        {
            this.Report = report;
            this.ConnectorsBaseDir = baseDir;
        }

        public string ConnectorsBaseDir { get; private set; }

        public string Report { get; private set; }

        public void SaveReport()
        {
            var sfd = new SaveFileDialog();
            sfd.FileName = "ConnectorDiscoveryReport_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt";
            sfd.DefaultExt = ".txt";
            var result = sfd.ShowDialog();
            if (result == true)
            {
                using (var fs = new FileStream(sfd.FileName, FileMode.Create))
                using (var writer = new StreamWriter(fs))
                {
                    writer.Write(this.Report);
                    writer.Flush();
                }
            }
        }

        public ICommand SaveReportCommand
        {
            get
            {
                if (this.saveReportCommand == null)
                    this.saveReportCommand = new DelegateCommand(
                        SaveReport,
                        () => string.IsNullOrWhiteSpace(this.Report) == false);

                return this.saveReportCommand;
            }
        }
    }
}
