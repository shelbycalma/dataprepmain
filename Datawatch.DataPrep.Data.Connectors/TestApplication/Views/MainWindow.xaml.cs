﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Controls;

namespace TestApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IFormLog
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnPrepareToLoadData(object sender, PrepareToLoadDataEventArgs e)
        {
            this.DataTableField.Columns.Clear();

            int i = 0;
            foreach (var header in e.Headers)
            {
                var binding = new Binding("Properties[" + i.ToString() + "].Value");
                this.DataTableField.Columns.Add(new DataGridTextColumn()
                {
                    Header = header,
                    Binding = binding
                });
                i++;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (this.DataContext is MainViewModel)
            {
                var ctx = (MainViewModel)this.DataContext;
                ctx.PrepareToLoadData += OnPrepareToLoadData;
                ctx.AddParameter += OnAddParameter;
                ctx.OnWindowLoaded();
            }
        }

        private void OnAddParameter(object sender, AddParameterEventArgs e)
        {
            var parent = this.ParametersPanel;
            var index = e.Index.ToString();
            var newParam = new ParameterControl();

            var nameBinding = new Binding("Parameters[" + index + "].Name");
            nameBinding.Mode = BindingMode.TwoWay;
            nameBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            newParam.SetBinding(ParameterControl.ParamNameProperty, nameBinding);

            var valueBinding = new Binding("Parameters[" + index + "].Value");
            valueBinding.Mode = BindingMode.TwoWay;
            valueBinding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            newParam.SetBinding(ParameterControl.ParamValueProperty, valueBinding);

            parent.Children.Insert(e.Index, newParam);
        }

        public void Log(string message)
        {
            if (Dispatcher.CheckAccess() == false)
            {
                Dispatcher.Invoke(() => this.Log(message));
                return;
            }

            this.LogField.AppendText(message + Environment.NewLine);
            this.LogField.ScrollToEnd();
        }

        public void Log(string format, params object[] args)
        {
            if (Dispatcher.CheckAccess() == false)
            {
                Dispatcher.Invoke(() => this.Log(string.Format(format, args)));
                return;
            }

            this.LogField.AppendText(string.Format(format, args) + Environment.NewLine);
            this.LogField.ScrollToEnd();
        }
    }
}
