﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Table;

namespace TestApplication
{
    public class Record
    {
        private readonly ObservableCollection<Property> properties =
            new ObservableCollection<Property>();

        public Record(ITable table, IRow row)
        {
            for (int i = 0; i < table.ColumnCount; i++)
            {
                IColumn col = table.GetColumn(i);
                string name = col.Name;
                object value;
                if (col is ITextColumn)
                {
                    value = ((ITextColumn)col).GetTextValue(row);
                }
                else if (col is INumericColumn)
                {
                    value = ((INumericColumn)col).GetNumericValue(row);
                }
                else if (col is ITimeColumn)
                {
                    value = ((ITimeColumn)col).GetTimeValue(row);
                }
                else
                {
                    value = "";
                }
                Properties.Add(new Property(name, value));
            }
        }

        public ObservableCollection<Property> Properties
        {
            get { return properties; }
        }
    }
}