﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace TestApplication
{
    class RealtimePreviewRunner
    {
        public event EventHandler<EventArgs> Stopped;
        public event EventHandler<DatasourceErrorEventArgs> DatasourceError;

        private readonly ITable previewTable;
        private IRealtimeDataPlugin activePlugin;
        private DatasourceContext currentCtx;
        private System.Action repaintCallback;
        private readonly IPluginManager pluginManager;
        private long lastRepaint;

        public RealtimePreviewRunner(IPluginManager pluginManager, DatasourceContext ctx)
        {
            if (pluginManager == null)
            {
                throw new ArgumentNullException("pluginManager");
            }
            if (ctx == null)
            {
                throw new ArgumentNullException("ctx");
            }

            if (ctx.Table == null)
                throw new ArgumentException("Context contains no table.");


            this.pluginManager = pluginManager;
            this.previewTable = ctx.Table;
            this.currentCtx = ctx;
        }

        private void Attach(IRealtimeDataPlugin realtimePlugin)
        {
            realtimePlugin.Updating += RealtimePlugin_Updating;
            realtimePlugin.Updated += RealtimePlugin_Updated;
            realtimePlugin.StreamingStatusChanged += RealtimePlugin_StreamingStatusChanged;
        }

        private void Detach(IRealtimeDataPlugin realtimePlugin)
        {
            realtimePlugin.Updating -= RealtimePlugin_Updating;
            realtimePlugin.Updated -= RealtimePlugin_Updated;
            realtimePlugin.StreamingStatusChanged -= RealtimePlugin_StreamingStatusChanged;
        }

        //private IEnumerable<Datasource> Datasources
        //{
        //    get
        //    {
        //        if (datasource != null)
        //        {
        //            return new Datasource[] { datasource };
        //        }
        //        return datatable.Datasources;
        //    }
        //}
        private static DatasourceContext FindRootSources(ITable table, DatasourceContext ctx)
        {
            if (!(table is IDerivedTable))
            {
                if (ctx.IsRealTime)
                {
                    //if (datasource is DataPrepDatasource)
                    //{
                    //    dataPrepDatasource = (DataPrepDatasource)datasource;
                    //    LoadPlanBase loadPlan = dataPrepDatasource.LoadPlan;
                    //    if (!(loadPlan is SimpleLoadPlan)) return;
                    //    datasource = ((SimpleLoadPlan)loadPlan).Connection;
                    //}

                    return new DatasourceContext(ctx.PluginId, table, ctx.PluginSettings);
                }

                return ctx;
            }

            //if (table is IDerivedFromDatasource)
            //{
            //    IDerivedFromDatasource sourced = (IDerivedFromDatasource)table;
            //    for (int i = 0; i < sourced.SourceTableCount; i++)
            //    {
            //        ITable sourceTable = sourced.GetSourceTable(i);
            //        FindRootSources(sourceTable,
            //            sourced.GetSourceDatasource(sourceTable));
            //    }

            //    return ctx;
            //}

            IDerivedTable derived = (IDerivedTable)table;
            return FindRootSources(derived.GetSourceTable(0), ctx);
        }

        private void RealtimePlugin_Updating(object sender, RealtimeDataPluginEventArgs e)
        {
            e.IsCancelled = false;
            Monitor.Enter(previewTable);
        }

        private void RealtimePlugin_Updated(object sender, RealtimeDataPluginEventArgs e)
        {
            Monitor.Exit(previewTable);

            long now = DateTime.Now.Ticks;
            // Skip repaint if already repainted during the last second.
            if (now - lastRepaint < TimeSpan.FromMilliseconds(1000).Ticks)
            {
                return;
            }
            repaintCallback();
            lastRepaint = now;
        }

        private void OnDatasourceError(DatasourceContext ctx, Exception error)
        {
            if (DatasourceError == null) return;

            DatasourceError(this, new DatasourceErrorEventArgs(ctx, error));
        }

        private void RealtimePlugin_StreamingStatusChanged(object sender, RealtimeStatusEventArgs e)
        {
            if (e.Status != StreamingStatus.Stopped) return;

            IErrorProducer errorProducer =
                TableUtils.FindErrorProducerInPipeline(e.Table);
            var error = errorProducer != null ? errorProducer.Error : null;
            if (error != null)
            {
                OnDatasourceError(this.currentCtx, error);
            }

            Stop();
        }

        internal void Start(System.Action repaintCallback)
        {
            if (repaintCallback == null)
            {
                throw new ArgumentNullException("repaintCallback");
            }

            this.repaintCallback = repaintCallback;

            if (this.currentCtx.IsRealTime == false)
                return;

            var ctx = FindRootSources(previewTable, this.currentCtx);
            Start(ctx);
        }

        private void Start(DatasourceContext ctx)
        {
            ITable table = Utils.GetNonDerivedSource(ctx.Table);
            IDataPlugin plugin = pluginManager.GetDataPlugin(ctx.PluginId);

            if (!(plugin is IRealtimeDataPlugin)) return;

            var realtimePlugin = (IRealtimeDataPlugin)plugin;
            Attach(realtimePlugin);
            activePlugin = realtimePlugin;

            realtimePlugin.StartRealtimeUpdates(table, ctx.PluginSettings);
            currentCtx = ctx;
        }

        internal void Stop()
        {
            Stop(this.currentCtx);
            this.currentCtx = null;

            Detach(this.activePlugin);
            this.activePlugin = null;

            if (Monitor.IsEntered(previewTable))
            {
                Monitor.Exit(previewTable);
            }

            if (Stopped != null)
            {
                Stopped(this, EventArgs.Empty);
            }
        }

        private void Stop(DatasourceContext ctx)
        {
            ITable table = Utils.GetNonDerivedSource(ctx.Table);
            IDataPlugin plugin = pluginManager.GetDataPlugin(ctx.PluginId);
            IRealtimeDataPlugin realtimePlugin = (IRealtimeDataPlugin)plugin;
            realtimePlugin.StopRealtimeUpdates(table, ctx.PluginSettings);
        }

    }

    class DatasourceContext
    {
        public ITable Table { get; set; }

        public string PluginId { get; set; }

        public bool IsRealTime { get; set; }
        public PropertyBag PluginSettings { get; set; }

        public DatasourceContext()
        {
        }

        public DatasourceContext(string pluginId, ITable table, PropertyBag settings)
        {
            this.PluginId = pluginId;
            this.Table = table;
            this.PluginSettings = settings;
        }
    }

    internal class DatasourceErrorEventArgs : EventArgs
    {
        public Exception Error { get; private set; }

        internal DatasourceContext Context { get; private set; }

        public DatasourceErrorEventArgs(DatasourceContext ctx, Exception error)
        {
            Context = ctx;
            Error = error;
        }
    }
}
