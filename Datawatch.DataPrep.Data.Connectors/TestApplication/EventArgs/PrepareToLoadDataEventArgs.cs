﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace TestApplication
{
    public class PrepareToLoadDataEventArgs
    {
        public PrepareToLoadDataEventArgs(IEnumerable<string> columnNames)
        {
            this.Headers = columnNames;
        }

        public IEnumerable<string> Headers { get; private set; }
    }
}