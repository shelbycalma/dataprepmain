﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;

namespace TestApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private Window mainWindow;
        private IDialogService dialogService;

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            DatawatchXmlLicenseProvider.LicenseFilePath = TestApplication.Properties.Settings.Default.LicenseFilePath;

            Log.ConfigureLog("ta.log", LogLevel.Info);
            this.mainWindow = new MainWindow();
            string path = TestApplication.Properties.Settings.Default.PluginBaseDir;
            var pluginManager = new PluginManager(path);
            pluginManager.LoadPlugins();
            pluginManager.InitializeDataPlugins(mainWindow, new PropertyBag());
            pluginManager.InitializePluginUIs(mainWindow);

            this.dialogService = new DialogService(this.mainWindow);
            var viewModel = new MainViewModel(pluginManager, pluginManager.PluginDirectory, this.dialogService);
            mainWindow.DataContext = viewModel;
            mainWindow.Show();
        }
    }
}
