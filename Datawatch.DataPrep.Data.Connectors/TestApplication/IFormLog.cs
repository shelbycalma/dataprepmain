﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.Windows.Data;
using System.Windows.Controls;

namespace TestApplication
{
    interface IFormLog
    {
        void Log(string message);
        void Log(string format, params object[] args);
    }
}