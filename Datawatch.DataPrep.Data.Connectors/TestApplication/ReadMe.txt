﻿TestApplication is an application for data connector testing.

Features

- Generating available connectors list by scanning a data connectors base directory.
- Generating a connector discovery report.
- Connecting to a picked connector's target system for data retrieval with displaying of the retrieved data.
- Data retrieval using realtime connectors.
- Connector settings parameterization.

As connectors are available for x86 and x64 platforms, TestApplication
is also provided for these platforms.

Setup

1. Open application configuration file and navigate to 
configuration\applicationSettings\TestApplication.Properties.Settings element.

2. Specify a relative path to connectors base directory in PluginBaseDir 
setting by modifying the content of the corresponding value element. 
Plugins can be obtained from the Dashboards.Plugins.Binaries NuGet package.

3. Specify a relative path to a license file in LicenseFilePath
setting by modifying the content of the corresponding value element.
TA can run without a license file but it won't be able to use connectors 
to connect to the target systems.

4. Do steps 1-3 both for x86 and x64 application configuration files.