﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestApplication
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ParameterControl : UserControl
    {
        public ParameterControl()
        {
            InitializeComponent();
        }

        public string ParamName
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }

        public static readonly DependencyProperty ParamNameProperty =
            DependencyProperty.Register("ParamName", typeof(string), typeof(ParameterControl), new PropertyMetadata(string.Empty));

        public string ParamValue
        {
            get { return (string)GetValue(ParamValueProperty); }
            set { SetValue(ParamValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ParamValue.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ParamValueProperty =
            DependencyProperty.Register("ParamValue", typeof(string), typeof(ParameterControl), new PropertyMetadata(string.Empty));
    }
}
