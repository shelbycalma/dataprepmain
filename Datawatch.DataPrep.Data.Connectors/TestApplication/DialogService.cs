﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Input;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using System;
using System.Collections.ObjectModel;
using System.IO;

namespace TestApplication
{
    class DialogService : IDialogService
    {
        private Stack<Window> windowStack;

        public DialogService(Window main)
        {
            this.windowStack = new Stack<Window>();
            this.windowStack.Push(main);
            if (main is IFormLog)
                this.AppLog = (IFormLog)main;
        }

        public bool? ShowDialog(ViewModelBase viewModel)
        {
            var window = FetchWindow(viewModel);
            if (window == null)
                throw new ArgumentException("Failed to fetch a View for the specified ViewModel");
            window.DataContext = viewModel;
            window.Owner = this.windowStack.Peek();
            this.windowStack.Push(window);

            try
            {
                return window.ShowDialog();
            }
            finally
            {
                this.windowStack.Pop();
            }
        }

        public IFormLog AppLog { get; private set; }

        private Window FetchWindow(ViewModelBase viewModel)
        {
            if (viewModel is ConnectionDialogViewModel)
                return new ConnectionWindow();

            if (viewModel is ConnectorDiscoveryReportViewModel)
                return new ConnectorDiscoveryReportWindow();

            return null;
        }
    }
}