﻿namespace TestApplication
{
    enum RowLimit
    {
        All = 0,
        Ten = 1,
        Fifty = 2,
        Hundred = 3
    }

    static class RowLimitExtension
    {
        public static int ToNumber(this RowLimit value)
        {
            switch (value)
            {
                case RowLimit.All:
                    return 0;

                default:
                case RowLimit.Ten:
                    return 10;

                case RowLimit.Fifty:
                    return 50;

                case RowLimit.Hundred:
                    return 100;
            }
        }
    }
}