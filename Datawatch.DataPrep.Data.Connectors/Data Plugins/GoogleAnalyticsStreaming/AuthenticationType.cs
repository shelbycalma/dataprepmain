﻿namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    public enum AuthenticationType
    {
        Service, OAuth
    }
}
