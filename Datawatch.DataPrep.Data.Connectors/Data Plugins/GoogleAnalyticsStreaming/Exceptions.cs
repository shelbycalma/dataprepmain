﻿using System;
using Panopticon.GoogleAnalyticsStreamingPlugin.Properties;

namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {

        internal static Exception ConnectionError(Exception innerException)
        {
            return LogException(new InvalidOperationException(
                Resources.ExConnectionError,
                innerException));
        }
    }
}
