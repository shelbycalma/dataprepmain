﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace Panopticon.GoogleAnalyticsStreamingPlugin.Helper
{
    class GoogleAnalyticsHelper
    {

        public static AnalyticsService Authenticate(
                        GoogleAnalyticsSettings settings,
                        IEnumerable<ParameterValue> parameters)
        {
            AnalyticsService service;

            string clientId = settings.ClientID;
            string clientSecret = settings.ClientSecret;
            string userName = settings.UserName;
            string serviceAccountEmail = settings.ServiceAccountEmail;
            string keyFilePath = settings.KeyFilePath;
            if (parameters != null)
            {
                clientId = DataUtils.ApplyParameters(clientId, parameters);
                clientSecret = DataUtils.ApplyParameters(clientSecret, parameters);
                userName = DataUtils.ApplyParameters(userName, parameters);
                serviceAccountEmail = DataUtils.ApplyParameters(serviceAccountEmail, parameters);
                keyFilePath = DataUtils.ApplyParameters(keyFilePath, parameters);
            }

            if (settings.AuthenticationType == AuthenticationType.OAuth)
            {
                service = AuthenticateOauth(clientId, clientSecret, userName);
            }
            else
            {
                service =
                    AuthenticateServiceAccount(serviceAccountEmail, keyFilePath);
            }
            return service;
        }

        /// <summary>
        /// Authenticate to Google Using Oauth2
        /// Documentation https://developers.google.com/accounts/docs/OAuth2
        /// </summary>
        /// <param name="clientId">From Google Developer console https://console.developers.google.com</param>
        /// <param name="clientSecret">From Google Developer console https://console.developers.google.com</param>
        /// <param name="userName">A string used to identify a user.</param>
        /// <returns></returns>
        public static AnalyticsService AuthenticateOauth(string clientId, string clientSecret, string userName)
        {

            string[] scopes = new string[] { AnalyticsService.Scope.Analytics,  // view and manage your analytics data
                                             AnalyticsService.Scope.AnalyticsEdit,  // edit management actives
                                             AnalyticsService.Scope.AnalyticsManageUsers,   // manage users
                                             AnalyticsService.Scope.AnalyticsReadonly};     // View analytics data

            // here is where we Request the user to give us access, or use the Refresh Token that was previously stored in %AppData%
            UserCredential credential =
                GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets
                    {
                        ClientId = clientId,
                        ClientSecret = clientSecret
                    }, scopes, userName, CancellationToken.None,
                    new FileDataStore("Daimto.GoogleAnalytics.Auth.Store")).Result;

            AnalyticsService service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Analytics API Sample",
            });
            return service;

        }

        /// <summary>
        /// Authenticating to Google using a Service account
        /// Documentation: https://developers.google.com/accounts/docs/OAuth2#serviceaccount
        /// </summary>
        /// <param name="serviceAccountEmail">From Google Developer console https://console.developers.google.com</param>
        /// <param name="keyFilePath">Location of the Service account key file downloaded from Google Developer console https://console.developers.google.com</param>
        /// <returns></returns>
        public static AnalyticsService AuthenticateServiceAccount(string serviceAccountEmail, string keyFilePath)
        {

            // check the file exists
            if (!File.Exists(keyFilePath))
            {
                Console.WriteLine("An Error occurred - Key file does not exist");
                return null;
            }

            string[] scopes = new string[] { AnalyticsService.Scope.Analytics,  // view and manage your analytics data
                                             AnalyticsService.Scope.AnalyticsEdit,  // edit management actives
                                             AnalyticsService.Scope.AnalyticsManageUsers,   // manage users
                                             AnalyticsService.Scope.AnalyticsReadonly};     // View analytics data            

            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    Scopes = scopes
                }.FromCertificate(certificate));

            // Create the service.
            AnalyticsService service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Analytics API Sample",
            });
            return service;
        }

        private ColumnType GetDataType(string dataType)
        {
            switch (dataType)
            {
                case "STRING":
                    return ColumnType.Text;
                case "INTEGER":
                case "FLOAT":
                case "PERCENT":
                case "CURRENCY":
                    return ColumnType.Numeric;
                case "TIME":
                    return ColumnType.Time;
                default:
                    return ColumnType.Text;
            }

        }


        public void GenerateColumns(GoogleAnalyticsSettings settings,
            IPluginErrorReportingService errorReporter)
        {
            StreamingColumnGenerator generator =
                new StreamingColumnGenerator(settings, 1, 0, errorReporter);
            try
            {
                string selectedDimensions = string.Join(",", settings.SelectedDimensions);
                string selectedMetrics = string.Join(",", settings.SelectedMetrics);

                StringBuilder sbFields = new StringBuilder(selectedDimensions);
                if(sbFields.Length > 0 && !string.IsNullOrEmpty(selectedMetrics))
                {
                    sbFields.Append(",");
                }
                sbFields.Append(selectedMetrics);

                //TODO:Have to parse a dummy data so that parser can generate columns. Improve the parser?
                //For dummy values, since all dimensions are string and metrics are  numeric,
                //we don't need to worry about schema
                string values = CreateDummyValues(settings.SelectedDimensions, settings.selectedMetrics);
                generator.MessageReceived(sbFields.ToString() + "\r\n" + values);
                generator.Generate();

            }
            catch (OperationCanceledException e)
            {
            }
        }

        private string CreateDummyValues(List<string> selectedDimension, List<string> selectedMetrics)
        {
            string result = string.Empty;
            foreach (string item in selectedDimension)
            {
                result += "Dummy,";
            }
            foreach (string item in selectedMetrics)
            {
                result += 1 + ",";
            }
            return result.Substring(0, result.Length - 1);
        }
    }
}
