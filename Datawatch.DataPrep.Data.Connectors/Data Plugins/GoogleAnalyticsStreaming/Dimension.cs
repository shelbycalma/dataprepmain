﻿using System.ComponentModel;

namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    public enum Dimension
    {
        [Description("User Type")]
        userType,
        [Description("Minutes Ago")]
        minutesAgo,
        [Description("Referral Path")]
        referralPath,
        [Description("Campaign")]
        campaign,
        [Description("Source")]
        source,
        [Description("Medium")]
        medium,
        [Description("Traffic Type")]
        trafficType,
        [Description("Keyword")]
        keyword,
        [Description("Browser")]
        browser,
        [Description("Browser Version")]
        browserVersion,
        [Description("Operating System")]
        operatingSystem,
        [Description("Operating System Version")]
        operatingSystemVersion,
        [Description("Device Category")]
        deviceCategory,
        [Description("Mobile Device Branding")]
        mobileDeviceBranding,
        [Description("Mobile Device Model")]
        mobileDeviceModel,
        [Description("Country")]
        country,
        [Description("Region")]
        region,
        [Description("City")]
        city,
        [Description("Latitude")]
        latitude,
        [Description("Longitude")]
        longitude,
        [Description("Page Path")]
        pagePath,
        [Description("Page Title")]
        pageTitle,
        [Description("App Name")]
        appName,
        [Description("App Version")]
        appVersion,
        [Description("Screen Name")]
        screenName,
        [Description("Event Action")]
        eventAction,
        [Description("Event Category")]
        eventCategory,
        [Description("Event Label")]
        eventLabel
    }
}
