﻿using System.ComponentModel;

namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    public enum Metrics
    {
        [Description("Active Users")]
        activeUsers,
        [Description("Page Views")]
        pageviews,
        [Description("Screen Views")]
        screenViews,
        [Description("Total Events")]
        totalEvents
    }
}
