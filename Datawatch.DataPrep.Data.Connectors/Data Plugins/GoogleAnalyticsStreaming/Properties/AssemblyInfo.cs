﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.GoogleAnalyticsStreamingPlugin;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Panopticon.GoogleAnalyticsStreamingPlugin.dll")]
[assembly: AssemblyDescription("Datawatch Google Analytics Streaming Plug-in")]
[assembly: AssemblyConfiguration("")]


[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
