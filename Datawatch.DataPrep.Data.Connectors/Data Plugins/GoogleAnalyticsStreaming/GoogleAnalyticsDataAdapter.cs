﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Panopticon.GoogleAnalyticsStreamingPlugin.Helper;

namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    public class GoogleAnalyticsDataAdapter : MessageQueueAdapterBase<ParameterTable,
        GoogleAnalyticsSettings, Dictionary<string, object>>
    {
        private Task task;
        private CancellationTokenSource cancelToken;

        public GoogleAnalyticsDataAdapter()
        {

        }

        public override void Start()
        {
            base.Start();
            settings.Parameters = table.Parameters;

            cancelToken = new CancellationTokenSource();
            task = new Task(StartListening, cancelToken.Token);
            task.Start();
        }

        public override void Stop()
        {
            cancelToken.Cancel();
            task = null;
            base.Stop();
        }

        private void StartListening()
        {
            AnalyticsService service;
            try
            {
                service =
                    GoogleAnalyticsHelper.Authenticate(settings, settings.Parameters);
            }
            catch (Exception ex)
            {
                Stop();
                throw Exceptions.ConnectionError(ex);
            }

                while (!cancelToken.IsCancellationRequested)
                {
                    try
                    {
                        RealtimeData realtimeData =
                            GoogleAnaltyicsRealTimeHelper.Get(service, settings);

                        if (realtimeData == null || realtimeData.Rows == null)
                        {
                            return;
                        }

                        foreach (List<string> row in realtimeData.Rows)
                        {
                            StringBuilder sb = new StringBuilder();
                            int counter = 0;
                            foreach (string col in row)
                            {
                                if (counter > 0 && counter <= row.Count - 1)
                                {
                                    sb.Append(",");
                                }
                                
                                sb.Append(col);
                                counter++;
                                //Console.Write(col + " ");  // writes the value of the column
                            }
                            MessageReceived(sb.ToString());
                            //Console.Write("\r\n");
                        }
                    }
                    catch (Exception)
                    {
                        return;
                    }

                }
        }

    }
}
