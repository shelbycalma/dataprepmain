﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.GoogleAnalyticsStreamingPlugin.Helper;

namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    public class GoogleAnalyticsSettings : MessageQueueSettingsBase
    {
        private IEnumerable<ParameterValue> parameters;

        internal List<string> selectedDimensions = new List<string>();

        internal List<string> selectedMetrics = new List<string>();
        private ICommand generateColumns;

        public GoogleAnalyticsSettings(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public GoogleAnalyticsSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : base(pluginManager, new PropertyBag(), parameters)
        {
            this.parameters = parameters;
        }

        public GoogleAnalyticsSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            this.parameters = parameters;
            selectedDimensions.Clear();
            PropertyBag dimensionsBag = bag.SubGroups["SelectedDimensions"];
            if (dimensionsBag != null)
            {
                foreach (PropertyValue item in dimensionsBag.Values)
                {
                    selectedDimensions.Add(item.Value);
                }
            }

            selectedMetrics.Clear();
            PropertyBag metricsBag = bag.SubGroups["SelectedMetrics"];
            if (metricsBag != null)
            {
                foreach (PropertyValue item in metricsBag.Values)
                {
                    selectedMetrics.Add(item.Value);
                }
            }
        }

        public AuthenticationType AuthenticationType
        {
            get
            {
                return (AuthenticationType)Enum.Parse(typeof(AuthenticationType),
                    GetInternal("AuthenticationType", "OAuth"));
            }
            set
            {
                SetInternal("AuthenticationType", value.ToString());
            }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public string ProfileID
        {
            get
            {
                return GetInternal("IDs");
            }
            set
            {
                SetInternal("IDs", value);
            }
        }

        public IEnumerable<Metrics> Metrics
        {
            get
            {
                return Enum.GetValues(typeof(Metrics)).Cast<Metrics>();
            }
        }

        public IEnumerable<Dimension> Dimensions
        {
            get
            {
                return Enum.GetValues(typeof(Dimension)).Cast<Dimension>();
            }
        }

        public List<string> SelectedDimensions
        {
            get
            {
                return selectedDimensions;
            }
            set
            {
                selectedDimensions = value;
            }
        }

        public List<string> SelectedMetrics
        {
            get
            {
                return selectedMetrics;
            }
            set
            {
                selectedMetrics = value;
            }
        }

        public string Filters
        {
            get
            {
                return GetInternal("Filters");
            }
            set
            {
                SetInternal("Filters", value);
            }
        }

        public string Fields
        {
            get
            {
                return GetInternal("Fields");
            }
            set
            {
                SetInternal("Fields", value);
            }
        }

        public override bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(UserName) &&
                    !string.IsNullOrEmpty(ClientID) &&
                    !string.IsNullOrEmpty(ClientSecret) &&
                    !string.IsNullOrEmpty(ProfileID) &&
                    SelectedMetrics.Count > 0;
            }
        }
        public ICommand GenerateColumns
        {
            get
            {
                if (generateColumns == null)
                {
                    generateColumns = new DelegateCommand(
                        DoGenerateColumns, CanGenerateColumns);
                }
                return generateColumns;
            }
        }

        public string ClientID
        {
            get
            {
                return GetInternal("ClientID");
            }
            set
            {
                SetInternal("ClientID", value);
            }
        }

        public string ClientSecret
        {
            get
            {
                return GetInternal("ClientSecret");
            }
            set
            {
                SetInternal("ClientSecret", value);
            }
        }

        public string ServiceAccountEmail
        {
            get
            {
                return GetInternal("ServiceAccountEmail");
            }
            set
            {
                SetInternal("ServiceAccountEmail", value);
            }
        }

        public string KeyFilePath
        {
            get
            {
                return GetInternal("KeyFilePath");
            }
            set
            {
                SetInternal("KeyFilePath", value);
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return "Text"; }
        }

        public override bool CanGenerateColumns()
        {
            return (selectedDimensions.Count > 0 || selectedMetrics.Count > 0);;
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        public override void DoGenerateColumns()
        {
            GoogleAnalyticsHelper helper = new GoogleAnalyticsHelper();
            ((TextFileParserSettings)this.ParserSettings).IsFirstRowHeading = true;
            helper.GenerateColumns(this, errorReporter);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is GoogleAnalyticsSettings)) return false;

            if (!base.Equals(obj)) return false;

            GoogleAnalyticsSettings cs = (GoogleAnalyticsSettings)obj;

            if (ClientID != cs.ClientID)
            {
                return false;
            }
            if (ProfileID != cs.ProfileID)
            {
                return false;
            }
            if (ClientSecret != cs.ClientSecret)
            {
                return false;
            }
            if (Metrics != cs.Metrics)
            {
                return false;
            }
            if (Dimensions != cs.Dimensions)
            {
                return false;
            }
            if (Filters != cs.Filters)
            {
                return false;
            }
            //TODO: Add rest of properties too here.

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            object[] fields = { ClientID, ClientSecret, ProfileID, SelectedMetrics, SelectedDimensions, Filters };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public override PropertyBag ToPropertyBag()
        {
            var bag = base.ToPropertyBag();
            int i = 0;
            var dimensionBag = new PropertyBag();
            foreach (string dimension in SelectedDimensions)
            {
                dimensionBag.Values["Dimension_" + i++] = dimension;
            }
            bag.SubGroups["SelectedDimensions"] = dimensionBag;

            i = 0;
            var metricsBag = new PropertyBag();
            foreach (string metric in SelectedMetrics)
            {
                metricsBag.Values["Metric_" + i++] = metric;
            }
            bag.SubGroups["SelectedMetrics"] = metricsBag;
            //DoGenerateColumns();
            return bag;
        }
    }
}
