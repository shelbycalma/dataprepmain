﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.GoogleAnalyticsStreamingPlugin
{
    /// <summary>
    /// DataPlugin for Google Analytics.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<ParameterTable, GoogleAnalyticsSettings,
                       Dictionary<string, object>, GoogleAnalyticsDataAdapter>
    {
        internal const string PluginId = "GoogleAnalyticsStreamingPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Google Analytics Streaming";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()  
            : base( "GoogleAnalyticsStreamingPlugin", Properties.Resources.UiPluginTitle)
        {
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override GoogleAnalyticsSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new GoogleAnalyticsSettings(pluginManager, parameters);
        }

        public override GoogleAnalyticsSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new GoogleAnalyticsSettings(pluginManager, bag, null);
        }
    }
}
