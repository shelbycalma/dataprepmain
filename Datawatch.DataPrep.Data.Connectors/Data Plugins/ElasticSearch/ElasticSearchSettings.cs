﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Newtonsoft.Json.Linq;

namespace Panopticon.ElasticSearchPlugin
{
    public class ElasticSearchSettings : TextSettingsBase
    {
        private const string Json = "Json";
        private ICommand fetchIndexes;
        private List<string> availableIndexes = new List<string>();
        private const string DEFAULT_PORT = "9200";
        private const string DEFAULT_QUERY = "{\n" +
            "\"query\": {\n" +
            "      \"match_all\": {}\n" +
            "},\n" +
            "\"from\": 0,\n" +
            "\"size\": -1\n" +
            "}";
        private string token;
        private const string TimeZoneHelperKey = "TimeZoneHelper";
        private TimeZoneHelper timeZoneHelper;


        public ElasticSearchSettings(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public ElasticSearchSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            PropertyBag timeZoneHelperBag =
               bag.SubGroups[TimeZoneHelperKey];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups[TimeZoneHelperKey] = timeZoneHelperBag;
            }
            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);
        }

        public IEnumerable<string> AvailableIndexes
        {
            get
            {
                return availableIndexes;
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return Json; }
        }

        public override PathType FilePathType
        {
            get
            {
                return PathType.WebURL;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public virtual string IndexName
        {
            get { return GetInternal("IndexName"); }
            set
            {
                SetInternal("IndexName", value);
            }
        }

        internal string RecordsPath
        {
            get
            {
                return GetInternal("RecordsJsonPath");
            }
            set
            {
                SetInternal("RecordsJsonPath", value);
            }
        }

        public string Query
        {
            get
            {
                return GetInternal("Query", DEFAULT_QUERY);
            }

            set
            {
                SetInternal("Query", value);
            }
        }

        /// <summary>
        /// Gets a value indicating wether the current JsonSettings 
        /// instance can be okeyed.
        /// </summary>
        public bool IsOK
        {
            get
            {
                if (!IsParserSettingsOk)
                {
                    return false;
                }
                return IsPathOk();
            }
        }

        public string Port
        {
            get
            {
                return GetInternal("Port", DEFAULT_PORT);
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public string URL
        {
            get
            {
                return GetInternal("URL", "http://localhost");
            }
            set
            {
                SetInternal("URL", value);
            }
        }

        public ICommand FetchIndexes
        {
            get
            {
                if(fetchIndexes == null)
                {
                    fetchIndexes = new DelegateCommand(DoFetchIndexes,
                        CanFetchIndexes);
                }
                return fetchIndexes;
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return timeZoneHelper;

            }
        }

        public string Token
        {
            get
            {
                return token;
            }
            set
            {
                token = value;
            }
        }

        public override string WebPath
        {
            get; set;
        }

        private bool CanFetchIndexes()
        {
            return !(string.IsNullOrEmpty(URL) || string.IsNullOrEmpty(Port));
        }

        private void DoFetchIndexes()
        {
            try
            {
                availableIndexes.Clear();
                Helper.TweakConnectionSettings(this, true, Parameters);

                DoGenerateColumns();

                var dataParser = (JsonParser)ParserSettings.CreateParser();

                using (StreamReader streamReader =
                    DataPluginUtils.GetStream(this, Parameters))
                {
                    JToken jRows = dataParser.FindRecords(streamReader,
                        "");
                    if (jRows == null)
                    {
                        throw new Exception(string.Format(
                            Properties.Resources.UiColumnGenerationWrongRecordPath, RecordsPath));
                    }

                    foreach (JToken t in jRows)
                    {
                        if (!t.HasValues) continue; // if null => continue

                        Dictionary<string, object> dict = dataParser.Parse((JToken)t);
                        foreach (string strValue in dict.Values)
                        {
                            availableIndexes.Add(strValue);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(WebException))
                {
                    Log.Error(Properties.Resources.ExQueryTimeout);
                }
                else
                {
                    Log.Error(Properties.Resources.ExFailedFetchIndexes);
                }

                this.errorReporter.Report(
                    Properties.Resources.ExError,
                    Properties.Resources.ExFailedFetchIndexes + "\n" +
                                    "Exception: " + e.Message);

                Log.Exception(e);
            }
            ParserSettings.ClearColumnDefinitions();
            FirePropertyChanged("AvailableIndexes");
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ElasticSearchSettings)) return false;

            if (!string.Equals(RecordsPath, ((ElasticSearchSettings)obj).RecordsPath))
            {
                return false;
            }

            if (!base.Equals(obj)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            hashCode ^= RecordsPath.GetHashCode();

            return hashCode;
        }

        public override void DoGenerateColumns()
        {
            ParserSettings.ClearColumnDefinitions();
            var parser = (JsonParser)ParserSettings.CreateParser();

            using (StreamReader streamReader =
                DataPluginUtils.GetStream(this, Parameters, this.FilePath))
            {
                JToken jRows = parser.FindRecords(streamReader,
                    RecordsPath);
                if (jRows == null)
                {
                    //check for recordspath = "aggregations.buckets";
                    throw new Exception(string.Format(
                        Properties.Resources.UiColumnGenerationWrongRecordPath, RecordsPath));
                }

                ColumnGenerator cg = new ColumnGenerator(ParserSettings, NumericDataHelper);

                if (jRows is JObject && jRows.First != null)
                {
                    // The record path is not pointing to an array but instead an object that 
                    // contains a comma separated list of value:object
                    // Add main column with just path key
                    JsonColumnDefinition cd =
                        (JsonColumnDefinition)ParserSettings.CreateColumnDefinition(new PropertyBag());
                    cd.Name = "KeyColumn";
                    cd.JsonPath = JsonParser.Key;
                    ParserSettings.AddColumnDefinition(cd);
                }
                int count = 0;
                foreach (JToken t in jRows)
                {
                    if (count >= 10)
                        break;

                    if (!t.HasValues || t.SelectToken(token) == null) continue;

                    cg.Generate(t.SelectToken(Token).ToString());

                    count++;
                }
            }
        }

    }
}
