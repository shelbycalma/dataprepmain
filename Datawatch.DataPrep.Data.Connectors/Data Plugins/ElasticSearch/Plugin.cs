﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Net;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Table;
using Newtonsoft.Json.Linq;
using Panopticon.ElasticSearchPlugin.Properties;

namespace Panopticon.ElasticSearchPlugin
{
    /// <summary>
    /// DataPlugin for JSON.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : TextPluginBase<ElasticSearchSettings>
    {
        internal const string PluginId = "ElasticSearchPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "ElasticSearch";
        internal const string PluginType = DataPluginTypes.Database;

        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override ITable GetData(string workbookDir, string dataDir, 
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            ElasticSearchSettings settings = 
                new ElasticSearchSettings(pluginManager, properties, parameters);

            Helper.TweakConnectionSettings(settings, false, parameters);


            StandaloneTable table = new StandaloneTable();


            table.BeginUpdate();
            try
            {
                if (settings.ParserSettings.ColumnCount == 0)
                {
                    settings.DoGenerateColumns();
                }

                JsonParser parser =
                    (JsonParser)settings.ParserSettings.CreateParser();

                DataPluginUtils.AddColumns(table, settings.ParserSettings.Columns);

                DateTime start = DateTime.Now;

                using (StreamReader streamReader =
                    DataPluginUtils.GetStream(settings, parameters))
                {
                    JToken jRows = parser.FindRecords(streamReader, 
                        settings.RecordsPath);

                    if (jRows != null)
                    {
                        ParameterFilter parameterFilter = null;
                        if (applyRowFilteration)
                        {
                            parameterFilter =
                                ParameterFilter.CreateFilter(table, parameters);
                        }

                        foreach (JToken t in jRows)
                        {
                            if (!t.HasValues || t.SelectToken(settings.Token) == null) continue; // if null => continue

                            if (maxRowCount > -1 && table.RowCount >= maxRowCount) break;
                            Dictionary<string, object> dict =
                                parser.Parse((JToken)t.SelectToken(settings.Token));
                            foreach (ColumnDefinition column in settings.ParserSettings.Columns)
                            {
                                if(column.Type == ColumnType.Time)
                                {
                                    DateTime dt = (DateTime)dict[column.Name];
                                    dt = settings.TimeZoneHelper.ConvertFromUTC(dt);
                                    dict[column.Name] = dt;
                                }
                            }
                            DataPluginUtils.AddRow(dict, table, 
                                settings.NumericDataHelper, parameterFilter, 
                                settings.ParserSettings);
                        }
                    }
                }

                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime,
                    table.RowCount, table.ColumnCount, seconds);

                // We're creating a new StandaloneTable on every request, so
                // allow EX to freeze it.
                table.CanFreeze = true;
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(WebException))
                {
                    Log.Error(Properties.Resources.ExQueryTimeout);
                }
                else
                {
                    Log.Error(Properties.Resources.ExFailedColumnGeneration);
                }

                this.errorReporter.Report(
                    Properties.Resources.ExError,
                    Properties.Resources.ExFailedColumnGeneration + "\n" +
                                    "Exception: " + e.Message);

                Log.Exception(e);
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        public override ElasticSearchSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new ElasticSearchSettings(pluginManager, new PropertyBag(), parameters);
        }

        public override ElasticSearchSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new ElasticSearchSettings(pluginManager, bag, null);
        }
    }
}
