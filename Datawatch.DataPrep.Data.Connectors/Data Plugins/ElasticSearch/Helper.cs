﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.ElasticSearchPlugin
{
    public class Helper
    {

        public const string INDEX_LIST_COMMNAD = "_aliases";
        public static void TweakConnectionSettings(ElasticSearchSettings settings,
            bool isFetchIndexes, IEnumerable<ParameterValue> parameters)
        {
            string host = DataUtils.ApplyParameters(settings.URL, parameters);
            string port = DataUtils.ApplyParameters(settings.Port, parameters);
            string URL = host + ":" + port + "/";

            if (isFetchIndexes)
            {
                URL += INDEX_LIST_COMMNAD;
                settings.RequestBody = "";
                settings.RecordsPath = "";
                settings.Token = "";
            }
            else
            {
                URL += settings.IndexName + "/_search";
                settings.RequestBody = settings.Query;
                settings.RecordsPath = "hits.hits";
                settings.Token = "_source";
            }
            settings.WebPath = URL;
        }
    }
}
