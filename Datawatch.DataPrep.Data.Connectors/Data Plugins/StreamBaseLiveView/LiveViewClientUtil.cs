﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.StreamBasePlugin;

using StreamBase.LiveView.API;
using StreamBase.SB;
using Exceptions = Panopticon.StreamBasePlugin.Exceptions;

namespace Panopticon.LiveViewPlugin
{
    public class LiveViewClientUtil
    {
        public static ILiveViewConnection GetLiveViewConnection(
            LiveViewSettings settings, IEnumerable<ParameterValue> parameters)
        {
            try
            {
                string url =
                    DataUtils.ApplyParameters(
                        settings.PrimaryURL, parameters, false);
                ILiveViewConnection connection =
                    LiveViewConnectionFactory.GetConnection(url);
                return connection;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                throw ex;
            }
        }

        public static FieldTreeTable CreateTableFromSettings(
            LiveViewSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            IList<Schema.Field> fieldList;

            fieldList = settings.IsQueryMode ?
                GetFieldsFromQuery(settings, parameters)
                : GetFieldsFromTable(settings, parameters);

            FieldTreeTable fieldTreeTable =
                CreateTableFromSchema(fieldList,
                    parameters, settings.IdColumnName);

            return fieldTreeTable;
        }

        private static IList<Schema.Field> GetFieldsFromTable(LiveViewSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            ILiveViewConnection connection = null;
            ITable table = null;
            try
            {
                connection = GetLiveViewConnection(settings, parameters);
                IList<ITable> tables = connection.ListTables();
                foreach (ITable t in tables)
                {
                    if (t.Name == settings.Table)
                    {
                        table = t;
                        break;
                    }
                }
                if (table == null)
                {
                    throw Exceptions.TableDoesNotExist(settings.Table);
                }
                return table.GetFields();
            }
            finally
            {
                if (connection != null && connection.IsConnected())
                {
                    connection.Close();
                }
            }
        }

        private static IList<Schema.Field> GetFieldsFromQuery(
            LiveViewSettings settings, IEnumerable<ParameterValue> parameters)
        {
            ILiveViewConnection connection = null;
            EventBasedQueryResult eventBasedQR = null;
            IQuery query;
            try
            {
                connection = GetLiveViewConnection(settings, parameters);

                eventBasedQR = new EventBasedQueryResult();
                eventBasedQR.QueryExceptionEvent +=
                    eventBasedQR_QueryExceptionEvent;

                QueryConfig queryConfig =
                    GetQueryConfig(settings, parameters,
                        QueryConfig.QueryTypes.SNAPSHOT);

                query = connection.ExecuteQuery(queryConfig, eventBasedQR);
                return query.Fields;
            }
            finally
            {
                if (connection != null && connection.IsConnected())
                {
                    connection.Close();
                }
                if (eventBasedQR != null)
                {
                    eventBasedQR.QueryExceptionEvent -=
                        eventBasedQR_QueryExceptionEvent;
                    eventBasedQR = null;
                }
            }
        }

        private static FieldTreeTable CreateTableFromSchema(
            IList<Schema.Field> fields,
            IEnumerable<ParameterValue> parameters, string idColumnName)
        {
            FieldTreeTable table = new FieldTreeTable(parameters);
            table.BeginUpdate();
            try
            {
                table.AddColumn(new TextColumn(idColumnName));

                foreach (Schema.Field field in fields)
                {
                    CommonUtil.AddColumnRecursive(table, table.Root, field, "");
                }
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        public static QueryConfig GetQueryConfig(LiveViewSettings settings,
            IEnumerable<ParameterValue> parameters,
            QueryConfig.QueryTypes queryType)
        {
            string predicate = null;
            if (!string.IsNullOrEmpty(settings.Predicate))
            {
                predicate = DataUtils.ApplyParameters(
                    settings.Predicate, parameters,
                    settings.EncloseParameterInQuote);
            }

            QueryConfig queryConfig = QueryConfig.CreateQueryConfig(
                (IList<string>)null, settings.Table, predicate, -1, queryType);
            if (settings.QueryMode == QueryMode.Query)
            {
                queryConfig.QueryString = DataUtils.ApplyParameters(
                    settings.Query, parameters, settings.EncloseParameterInQuote);
            }
            return queryConfig;
        }

        public static string[] GetTables(LiveViewSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            ILiveViewConnection connection =
                GetLiveViewConnection(settings, parameters);
            string[] tables = connection.ListTables().Select(o => o.Name).ToArray();
            if (connection.IsConnected())
            {
                connection.Close();
            }
            return tables;
        }


        private static void eventBasedQR_QueryExceptionEvent(object sender,
            QueryExceptionEventArgs e)
        {
            Log.Exception(e.Exception);
        }
    }
}
