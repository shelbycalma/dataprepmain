﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.StreamBasePlugin;
using StreamBase.LiveView.API;
using StreamBase.SB;

namespace Panopticon.LiveViewPlugin
{
    public class LiveViewSettings : RealtimeConnectionSettings
    {
        private TimeZoneHelper timeZoneHelper;
        private const string TimeZoneHelperKey = "TimeZoneHelper";
        private bool isQueryMode;

        public LiveViewSettings()
            : this(new PropertyBag())
        {
        }

        public LiveViewSettings(PropertyBag bag)
            : base(bag, true)
        {
            PropertyBag timeZoneHelperBag =
               bag.SubGroups[TimeZoneHelperKey];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups[TimeZoneHelperKey] = timeZoneHelperBag;
            }

            isQueryMode = QueryMode == QueryMode.Query;
            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);
        }

        public bool IsQueryMode
        {
            get { return isQueryMode; }
            set
            {
                if (value == isQueryMode) return;
                isQueryMode = value;
                QueryMode = value
                    ? QueryMode.Query : QueryMode.Table;
                FirePropertyChanged("IsQueryMode");
            }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public QueryMode QueryMode
        {
            get
            {
                return (QueryMode)
                    Enum.Parse(typeof(QueryMode),
                        GetInternal("QueryMode", "Table"));
            }
            set { SetInternal("QueryMode", value.ToString()); }
        }

        public string PrimaryURL
        {
            get { return GetInternal("PrimaryURL", "lv://localhost:10080/"); }
            set { SetInternal("PrimaryURL", value); }
        }

        public string Table
        {
            get { return GetInternal("Table"); }
            set { SetInternal("Table", value); }
        }

        public string Predicate
        {
            get { return GetInternal("Predicate"); }
            set
            {
                SetInternal("Predicate", value);
            }
        }

        public bool EncloseParameterInQuote
        {
            get
            {
                string value = GetInternal("EncloseParameterInQuote",
                    true.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("EncloseParameterInQuote", value.ToString()); }
        }

        public string IdColumnName
        {
            get { return GetInternal("IdColumnName", "Key"); }
            set { SetInternal("IdColumnName", value); }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return timeZoneHelper;
            }
        }

        protected override void FirePropertyChanged(string name)
        {
            base.FirePropertyChanged(name);

            if (name.Equals("IdColumnName"))
            {
                IdColumnCandidates = new string[] { IdColumnName };
                IdColumn = IdColumnName;
            }
        }

        public void UpdateIdCandidates(IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(PrimaryURL) ||
                string.IsNullOrEmpty(Table))
            {
                IdColumnCandidates = null;
                TimeIdColumnCandidates = null;
                return;
            }

            FieldTreeTable table = 
                LiveViewClientUtil.CreateTableFromSettings(this, parameters);

            List<string> timeIdCandidates = new List<string>();
            foreach (FieldTreeNode child in table.Root.Children)
            {
                UpdateTimeIdCandidatesHelper(timeIdCandidates, child);
            }

            IdColumnCandidates = new string[] { IdColumnName };
            TimeIdColumnCandidates = timeIdCandidates.ToArray();
        }

        public void UpdateQuery(IEnumerable<ParameterValue> parameters)
        {
            QueryConfig queryConfig =
                LiveViewClientUtil.GetQueryConfig(this, parameters,
                    QueryConfig.QueryTypes.SNAPSHOT);

            Query = queryConfig.QueryString;
        }

        private static void UpdateTimeIdCandidatesHelper(
            List<string> timeIdCandidates, FieldTreeNode node)
        {
            DataType dataType = node.Field.DataType;
            if (dataType == DataType.TIMESTAMP)
            {
                timeIdCandidates.Add(node.Column.Name);
            }

            if (dataType == DataType.TUPLE)
            {
                foreach (FieldTreeNode child in node.Children)
                {
                    UpdateTimeIdCandidatesHelper(timeIdCandidates, child);
                }
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is LiveViewSettings)) return false;

            if (!base.Equals(obj)) return false;

            LiveViewSettings cs = (LiveViewSettings)obj;

            // IdColumnName is also stored in IdColumn,
            // which is checked in RealtimeConnectionSettings.Equals(). 
            if (this.PrimaryURL != cs.PrimaryURL ||
                this.Predicate != cs.Predicate ||
                this.EncloseParameterInQuote != cs.EncloseParameterInQuote ||
                this.Table != cs.Table ||
                this.timeZoneHelper.SelectedTimeZone != cs.timeZoneHelper.SelectedTimeZone)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields = 
                { 
                    PrimaryURL, Table, IdColumn, Predicate, 
                    EncloseParameterInQuote
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }
    }
}
