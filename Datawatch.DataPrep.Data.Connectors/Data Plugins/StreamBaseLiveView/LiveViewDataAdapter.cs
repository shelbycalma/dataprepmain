﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;

using Panopticon.StreamBasePlugin;

using StreamBase.LiveView.API;
using StreamBase.SB;
using Tuple = StreamBase.SB.Tuple;

namespace Panopticon.LiveViewPlugin
{
    public class LiveViewDataAdapter : RealtimeDataAdapter<FieldTreeTable,
        LiveViewSettings, TupleWrapper>
    {
        
        private ILiveViewConnection connection;
        private IQuery query;
        private EventBasedQueryResult eventBasedQR;

        public override void Start()
        {
            ConnectAndSubscribe();
        }

        public override void Stop()
        {
            Disconnect();
        }

        private void ConnectAndSubscribe()
        {
            try
            {
                eventBasedQR = new EventBasedQueryResult();
                eventBasedQR.QueryExceptionEvent +=
                    eventBasedQR_QueryExceptionEvent;
                eventBasedQR.TupleAdded += eventBasedQR_TupleAdded;
                eventBasedQR.TupleRemoved += eventBasedQR_TupleRemoved;
                eventBasedQR.TupleUpdated += eventBasedQR_TupleUpdated;

                connection = 
                    LiveViewClientUtil.GetLiveViewConnection(settings, 
                        table.Parameters);

                QueryConfig queryConfig =
                    LiveViewClientUtil.GetQueryConfig(settings, table.Parameters,
                    QueryConfig.QueryTypes.CONTINUOUS);

                Log.Info(Properties.Resources.LogExecutingQuery, queryConfig.QueryString);
                query = connection.ExecuteQuery(queryConfig, eventBasedQR);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }
        
        private void Disconnect()
        {
            try
            {
                if (eventBasedQR != null)
                {
                    eventBasedQR.QueryExceptionEvent -=
                        eventBasedQR_QueryExceptionEvent;
                    eventBasedQR.TupleAdded -= eventBasedQR_TupleAdded;
                    eventBasedQR.TupleRemoved -= eventBasedQR_TupleRemoved;
                    eventBasedQR.TupleUpdated -= eventBasedQR_TupleUpdated;
                }
                if (query != null)
                {
                    query.Close();
                }

                if (connection != null && connection.IsConnected())
                {
                    connection.Close();
                }
            }
            catch { }
        }

        private void eventBasedQR_QueryExceptionEvent(object sender,
            QueryExceptionEventArgs e)
        {
            Log.Exception(e.Exception);
        }

        private void eventBasedQR_TupleAdded(object sender,
            TupleAddedEventArgs e)
        {
            TupleWrapper tuple = new TupleWrapper(e.Tuple);
            updater.EnqueueInsert(e.Key.ToString(), tuple);
        }

        private void eventBasedQR_TupleRemoved(object sender,
            TupleRemovedEventArgs e)
        {
            TupleWrapper tuple = new TupleWrapper(e.Tuple);
            updater.EnqueueDelete(e.Key.ToString(), tuple);
        }
        
        private void eventBasedQR_TupleUpdated(object sender,
            TupleUpdatedEventArgs e)
        {            
            TupleWrapper tuple = new TupleWrapper(e.Tuple, e.ChangedFields);
            updater.EnqueueUpdate(e.Key.ToString(), tuple);
        }

        public override void UpdateColumns(Row row, TupleWrapper tuple)
        {
            foreach (FieldTreeNode node in table.Root.Children)
            {
                if (node.Column.Name.Equals(settings.IdColumnName)) continue;

                if (tuple.ChangedFields != null && 
                    !tuple.ChangedFields.Contains(node.Field))
                {
                    continue;
                }

                try
                {
                    CommonUtil.ReadAndAssignFieldRecursive(tuple.Tuple, node,
                        row, null, settings.TimeZoneHelper);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
            }
        }

        public override DateTime GetTimestamp(TupleWrapper tuple)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return DateTime.MinValue;
            }

            Tuple actualTuple = tuple.Tuple;
            foreach (FieldTreeNode parent in table.TimeIdPath)
            {
                actualTuple = actualTuple.GetTuple(parent.Field);
            }

            if (actualTuple.IsNull(table.TimeIdNode.Field))
            {
                return DateTime.MinValue;
            }

            Timestamp value = actualTuple.GetTimestamp(table.TimeIdNode.Field);
            DateTime dateTime = value.ToDateTime();
            if (settings.TimeZoneHelper.TimeZoneSelected)
            {
                dateTime = settings.TimeZoneHelper.ConvertFromUTC(dateTime);
            }
            return dateTime;
        }
    }
}
