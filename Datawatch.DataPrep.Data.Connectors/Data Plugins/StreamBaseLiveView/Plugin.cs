﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.StreamBasePlugin;

using StreamBase.SB.Client;
using Panopticon.LiveViewPlugin.Properties;

namespace Panopticon.LiveViewPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin<FieldTreeTable,
        LiveViewSettings, TupleWrapper, LiveViewDataAdapter>
    {
        internal const string PluginId = "LiveViewPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "StreamBase LiveView";
        internal const string PluginType = DataPluginTypes.Streaming;

        private bool disposed;

        public Plugin()
        {
            Log.Info(Properties.Resources.LogStreamBaseClientVersion,
                PluginTitle, StreamBaseClient.GetVersion());
            try
            {
                // This is just to trigger loading of newer
                // versions of StreamBase.LiveView.API.dll.
                // To allow using later version of assembly then the referenced ones.
                StreamBase.LiveView.API.QueryConfig qc =
                    new StreamBase.LiveView.API.QueryConfig();                     
            }
            catch (Exception)
            {
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //Managed
                }
                //Unmanaged
                disposed = true;
            }

            base.Dispose(disposing);
        }

        protected override ITable CreateTable(LiveViewSettings settings,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;
            FieldTreeTable table =
                LiveViewClientUtil.CreateTableFromSettings(settings, parameters);

            if (!settings.IsTimeIdColumnGenerated &&
                settings.TimeIdColumn != null)
            {
                table.InitializeTimeIdColumn(settings.TimeIdColumn);
            }
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;
        }

        public override LiveViewSettings CreateSettings(PropertyBag bag)
        {
            LiveViewSettings sbSettings = new LiveViewSettings(bag);
            if (string.IsNullOrEmpty(sbSettings.Title))
            {
                sbSettings.Title = sbSettings.Table ?? Properties.Resources.UiLiveViewConnection;
            }
            return sbSettings;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
