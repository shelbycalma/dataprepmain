﻿using System.Collections.Generic;

using StreamBase.SB;

namespace Panopticon.LiveViewPlugin
{
    public class TupleWrapper
    {
        internal Tuple Tuple = null;
        internal IList<Schema.Field> ChangedFields = null;
        
        public TupleWrapper(Tuple tuple)
        {
            Tuple = tuple;
            ChangedFields = null;
        }

        public TupleWrapper(Tuple tuple, IList<Schema.Field> changedFields)
        {
            Tuple = tuple;
            ChangedFields = changedFields;
        }
    }
}
