﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.RabbitMQPlugin.UI.dll")]
[assembly: AssemblyDescription("Datawatch RabbitMQ Plug-in UI")]
[assembly: AssemblyConfiguration("")]
