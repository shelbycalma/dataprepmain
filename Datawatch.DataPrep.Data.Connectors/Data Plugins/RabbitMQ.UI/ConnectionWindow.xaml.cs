﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.RabbitMQPlugin.UI
{
    /// <summary>
    /// Interaction logic for QueryWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private RabbitMQSettings connection;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(RabbitMQSettings con)
        {
            this.connection = con;

            InitializeComponent();
        }

        public RabbitMQSettings Connection
        {
            get { return connection; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = connection.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
