﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.RabbitMQPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionControl.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private RabbitMQSettings settings;

        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(RabbitMQSettings settings)
        {
            InitializeComponent();
            DataContext = Settings = settings;
            DataContextChanged +=
                UserControl_DataContextChanged;
        }

        public RabbitMQSettings Settings
        {
            get { return settings; }
            private set {
                settings = value;
                if (settings != null)
                {
                    PasswordBox.Password = settings.Password;
                }
                else
                {
                    PasswordBox.Password = null;
                }
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            Settings = DataContext as RabbitMQSettings;
        }

    }
}
