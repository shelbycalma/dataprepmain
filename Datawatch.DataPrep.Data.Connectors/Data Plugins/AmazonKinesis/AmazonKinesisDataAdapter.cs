﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.AmazonKinesisPlugin
{
    public class AmazonKinesisDataAdapter :
        MessageQueueAdapterBase<ParameterTable,
            AmazonKinesisSettings, Dictionary<string, object>>
    {
        private AmazonKinesisListener listener;

        public AmazonKinesisDataAdapter()
        {
        }

        public override void Start()
        {
            settings.Parameters = table.Parameters;
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();

            listener = new AmazonKinesisListener(this, settings);

            listener.ListenToSelectedStream();
        }

        public override void Stop()
        {
            base.Stop();

            listener.Disconnect();
            listener = null;
        }

        public void MessageReceived(string message)
        {
            DataPluginUtils.EnqueueMessage(message, parser, idColumn, updater);
        }
    }
}