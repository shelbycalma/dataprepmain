﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.AmazonKinesisPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<ParameterTable,
        AmazonKinesisSettings, Dictionary<string, object>,
        AmazonKinesisDataAdapter>
    {
        internal const string PluginId = "AmazonKinesisPlugin";
        internal const string PluginTitle = "Amazon Kinesis";
        internal const string PluginType = DataPluginTypes.Streaming;
        internal const bool PluginIsObsolete = true;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override AmazonKinesisSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new AmazonKinesisSettings(pluginManager, new PropertyBag(),
                parameters);
        }

        public override AmazonKinesisSettings CreateConnectionSettings(
            PropertyBag bag)
        {
            return new AmazonKinesisSettings(pluginManager, bag);
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }
    }
}