﻿using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Panopticon.AmazonKinesisPlugin.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.AmazonKinesisPlugin
{
    public class AmazonKinesisSettings : MessageQueueSettingsBase
    {
        private const int DefaultRecordLimit = 5000;

        public AmazonKinesisSettings(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag())
        {
        }

        public AmazonKinesisSettings(IPluginManager pluginManager, PropertyBag bag)
            : this(pluginManager, bag, null)
        {
        }

        public AmazonKinesisSettings(
            IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            if (bag.IsEmpty())
            {
                Version = new Version("2.0.0.0");
            }

            HandlePropertyRenames();
        }

        public string AccessKeyID
        {
            get { return GetInternal("AccessKeyID"); }
            set { SetInternal("AccessKeyID", value); }
        }

        public string SecretAccessKey
        {
            get { return GetInternal("SecretAccessKey"); }
            set { SetInternal("SecretAccessKey", value); }
        }

        public string RegionEndpoint
        {
            get { return GetInternal("RegionEndpoint"); }
            set { SetInternal("RegionEndpoint", value); }
        }

        public string Stream
        {
            get { return GetInternal("Stream"); }
            set
            {
                SetInternal("Stream", value);
                Title = string.Format(
                    Properties.Resources.UiConnectionTitle, Stream);
            }
        }

        public string ShardIteratorType
        {
            get { return GetInternal("ShardIteratorType"); }
            set { SetInternal("ShardIteratorType", value); }
        }

        public string SequenceNumber
        {
            get { return GetInternal("SequenceNumber"); }
            set { SetInternal("SequenceNumber", value); }
        }

        public string RecordLimit
        {
            get { return GetInternal("RecordLimit", 
                DefaultRecordLimit.ToString()); }
            set { SetInternal("RecordLimit", value); }
        }

        public string LatestOnRefresh
        {
            get { return GetInternal("LatestOnRefresh"); }
            set { SetInternal("LatestOnRefresh", value); }
        }

        public bool IsPasswordParameterized
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsPasswordParameterized",
                        "false"));
            }
            set { SetInternal("IsPasswordParameterized", value.ToString()); }
        }

        public bool IsStreamParameterized
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsStreamParameterized",
                        "false"));
            }
            set { SetInternal("IsStreamParameterized", value.ToString()); }
        }

        protected override string DefaultParserPluginId
        {
            get { return "Json"; }
        }

        public override bool ShowParserSelector
        {
            get { return true; }
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }
                
        public Version Version
        {
            get
            {
                return new Version(GetInternal("Version", "1.0.0.0"));
            }
            private set
            {
                SetInternal("Version", value.ToString());
            }
        }

        private void HandlePropertyRenames()
        {
            if (Version.Major < 2)
            {
                string value = GetInternal("Access_Key_ID");
                if (!string.IsNullOrEmpty(value))
                {
                    AccessKeyID = value;
                }
                value = GetInternal("Secret_Access_Key");
                if (!string.IsNullOrEmpty(value))
                {
                    SecretAccessKey = value;
                }
                value = GetInternal("Region_End_Point");
                if (!string.IsNullOrEmpty(value))
                {
                    RegionEndpoint = value;
                }
                value = GetInternal("Shard_Iterator_Type");
                if (!string.IsNullOrEmpty(value))
                {
                    ShardIteratorType = value;
                }
                value = GetInternal("Sequence_Number");
                if (!string.IsNullOrEmpty(value))
                {
                    SequenceNumber = value;
                }
                value = GetInternal("Record_Limit");
                if (!string.IsNullOrEmpty(value))
                {
                    RecordLimit = value;
                }
                value = GetInternal("Is_Password_Parameterized");
                if (!string.IsNullOrEmpty(value))
                {
                    IsPasswordParameterized = Convert.ToBoolean(value);
                }
                value = GetInternal("Is_Stream_Parameterized");
                if (!string.IsNullOrEmpty(value))
                {
                    IsStreamParameterized = Convert.ToBoolean(value);
                }
                Version = new Version("2.0.0.0");
            }
        }

        public string ParameterizeValue(
            string value, string arraySeparator = "\n")
        {
            if (Parameters == null || String.IsNullOrEmpty(value))
                return value;
            else
            {
                var encoder = new ParameterEncoder();
                encoder.Parameters = Parameters;
                encoder.DefaultArraySeparator = arraySeparator;
                encoder.SourceString = value;
                return encoder.Encoded();
            }
        }

        public int ParseRecordLimit()
        {
            var paramLimit = this.ParameterizeValue(this.RecordLimit);
            try
            {
                int limit = Int32.Parse(paramLimit);
                return limit;
            }
            catch (FormatException)
            {
                Log.Error(Resources.LogInvalidRecordLimit, DefaultRecordLimit);
                this.RecordLimit = DefaultRecordLimit.ToString();
                return DefaultRecordLimit;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is AmazonKinesisSettings)) return false;

            if (!base.Equals(obj)) return false;

            AmazonKinesisSettings other = (AmazonKinesisSettings) obj;
            return this.AccessKeyID == other.AccessKeyID &&
                this.SecretAccessKey == other.SecretAccessKey &&
                this.RegionEndpoint == other.RegionEndpoint &&
                this.Stream == other.Stream &&
                this.ShardIteratorType == other.ShardIteratorType &&
                this.SequenceNumber == other.SequenceNumber &&
                this.RecordLimit == other.RecordLimit &&
                this.LatestOnRefresh == other.LatestOnRefresh &&
                this.IsPasswordParameterized == other.IsPasswordParameterized &&
                this.IsStreamParameterized == other.IsStreamParameterized &&
                this.Version == other.Version;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            object[] fields =
                {
                    AccessKeyID, SecretAccessKey, RegionEndpoint,
                    Stream, ShardIteratorType, SequenceNumber,
                    RecordLimit, LatestOnRefresh, IsPasswordParameterized,
                    IsStreamParameterized, Version
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public override bool CanGenerateColumns()
        {
            return !String.IsNullOrWhiteSpace(AccessKeyID)
                && !String.IsNullOrWhiteSpace(SecretAccessKey)
                && RegionEndpoint!=null
                && !String.IsNullOrWhiteSpace(Stream);
        }

        public override void DoGenerateColumns()
        {
            try
            {
                int startingColNum = parserSettings.ColumnCount;

                foreach (string shardIterType in new[] {"TRIM_HORIZON", "LATEST"})
                {
                    int messageCounter = 0;
                    int attemptsMade = 0;

                    ColumnGenerator cg = new ColumnGenerator(ParserSettings, NumericDataHelper);

                    AmazonKinesisHelper akHelper = new AmazonKinesisHelper(this);
                    AmazonKinesisClient client = akHelper.CreateClient();

                    foreach (Shard shard in akHelper.GetShards(client))
                    {
                        string shardIter = akHelper.GetShardIterator(shard,
                            shardIterType, client);

                        while ((messageCounter <= 10) && (attemptsMade <= 20))
                        {
                            GetRecordsResponse recordResponse;
                            foreach (Record record in akHelper.GetRecords(shardIter,
                                out recordResponse, client))
                            {
                                StreamReader reader =
                                    new StreamReader(record.Data);

                                string text = reader.ReadToEnd();

                                text = text.Replace(": \"", ":\"");

                                cg.Generate(text);
                                messageCounter++;
                            }
                            if (recordResponse != null)
                                shardIter = recordResponse.NextShardIterator;

                            attemptsMade++;
                        }
                    }
                }

                if (startingColNum == parserSettings.ColumnCount)
                {
                    this.errorReporter.Report(
                        Resources.ExError,
                        Resources.ExFailedColumnGeneration + "\n" +
                                    Resources.ExGenerationDataNotFound);
                }
            }
            catch (Exception e)
            {
                this.errorReporter.Report(
                    Resources.ExError,
                    Resources.ExFailedColumnGeneration + "\n" +
                        "Exception: " + e.Message);
                Log.Error(Resources.ExFailedColumnGeneration);
                Log.Exception(e);
            }
        }
    }
}