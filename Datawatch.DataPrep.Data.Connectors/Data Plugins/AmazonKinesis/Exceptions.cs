﻿using System;

namespace Panopticon.AmazonKinesisPlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        internal static Exception MalformedBrokerUrl(
            string url,
            Exception innerException)
        {
            return LogException(new InvalidOperationException(
                "Resources.MalformedBrokerUrl"
                ,
                innerException));
        }

        internal static Exception ConnectionError(Exception innerException)
        {
            return LogException(new InvalidOperationException(
                "Resources.ConnectionError",
                innerException));
        }

        internal static Exception NoColumns()
        {
            return CreateInvalidOperation("Resources.NoColumns");
        }

        internal static Exception NoIdColumn()
        {
            return CreateInvalidOperation("Resources.NoIdColumn");
        }
    }
}