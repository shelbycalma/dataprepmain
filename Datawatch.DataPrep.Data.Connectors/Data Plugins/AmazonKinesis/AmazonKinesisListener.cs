﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.AmazonKinesisPlugin.Properties;

namespace Panopticon.AmazonKinesisPlugin
{
    internal class AmazonKinesisListener
    {
        private readonly AmazonKinesisDataAdapter dataAdapter;
        private AmazonKinesisClient client;
        private CancellationTokenSource cancelToken;
        private Task task;

        private AmazonKinesisHelper akHelper;
        private AmazonKinesisSettings akSettings;

        public AmazonKinesisListener(
            AmazonKinesisDataAdapter dataAdapter, AmazonKinesisSettings settings)
        {
            if (dataAdapter == null)
            {
                throw Exceptions.DataAdaptorWasNull();
            }
            if (settings == null)
            {
                throw Exceptions.ArgumentNull("Settings");
            }
            this.dataAdapter = dataAdapter;

            akSettings = settings;

            akHelper = new AmazonKinesisHelper(settings);

            client = akHelper.CreateClient();
        }

        public void Disconnect()
        {
            try
            {
                cancelToken.Cancel();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }

            Log.Info(Resources.LogDisconnected);
        }

        public void ListenToSelectedStream()
        {
            if (client != null & !string.IsNullOrEmpty(akSettings.Stream))
            {
                cancelToken = new CancellationTokenSource();
                task = new Task(ReadMessagesFromStream, cancelToken.Token);
                task.Start();
            }
        }

        private void ReadMessagesFromStream()
        {
            client = akHelper.CreateClient();

            int messageCounter = 0;

            var shardList = akHelper.GetShards(client);

            foreach (var shard in shardList)
            {
                if (cancelToken.IsCancellationRequested)
                {
                    Disconnect();
                    return;
                }
                string shardIter = akHelper.GetShardIterator(shard, null, client);

                bool noRecords = true;

                while (!cancelToken.IsCancellationRequested)
                {
                    try
                    {
                        GetRecordsResponse recordResponse;

                        var recordList = akHelper.GetRecords(shardIter,
                            out recordResponse, client);

                        noRecords = true;
                        foreach (var record in recordList)
                        {
                            noRecords = false;
                            if (!cancelToken.IsCancellationRequested)
                            {
                                StreamReader reader =
                                    new StreamReader(record.Data);

                                string text = reader.ReadToEnd();

                                Debug.WriteLine(messageCounter++ + " " + text);

                                text = text.Replace(": \"", ":\"");

                                dataAdapter.MessageReceived(text);
                                messageCounter++;
                            }
                            else
                            {
                                Disconnect();
                                //TODO:- Not sure why continue after disconnect
                            }
                        }

                        if (recordResponse != null)
                        {
                            shardIter = recordResponse.NextShardIterator;
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error(Resources.LogStreamingError);
                        Log.Exception(e);
                    }

                    //check to see if there were any records in 
                    //the last check, if not then wait
                    if (noRecords)
                    {
                        Thread.Sleep(500);
                    }
                }
            }
        }

        //TODO:- remove ??
        public void ConnectionInterruptedListener()
        {
            Log.Warning(Resources.LogConnectionIterrupted);
        }

        public void ConnectionResumedListener()
        {
            Log.Warning(Resources.LogConnectionResumed);
        }

        public void ExceptionListener(Exception ex)
        {
            Log.Exception(ex);
        }

    }
}