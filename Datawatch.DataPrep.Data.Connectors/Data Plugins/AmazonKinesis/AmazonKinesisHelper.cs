﻿using System;
using System.Collections.Generic;
using Amazon;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.AmazonKinesisPlugin.Properties;

namespace Panopticon.AmazonKinesisPlugin
{
    public class AmazonKinesisHelper
    {
        private AmazonKinesisSettings settings;
        public AmazonKinesisHelper(AmazonKinesisSettings settings)
        {
            if (settings == null)
            {
                throw Exceptions.ArgumentNull("Settings");
            }
            this.settings = settings;
        }

        public AmazonKinesisClient CreateClient()
        {
            RegionEndpoint regionEndpoint;
            string endpointName = settings.ParameterizeValue(settings.RegionEndpoint);

            try
            {
                regionEndpoint = RegionEndpoint.GetBySystemName(endpointName);
            }
            catch
            {
                Log.Error(Resources.LogInvalidRegion, endpointName);
                return null;
            }

            try
            {
                var key = settings.ParameterizeValue(settings.AccessKeyID);
                var skey =
                    settings.ParameterizeValue(settings.SecretAccessKey);

                return new AmazonKinesisClient(key, skey, regionEndpoint);
            }
            catch (Exception e)
            {
                Log.Error(Resources.LogClientCreationFailure);
                Log.Error(e.ToString());
                return null;
            }
        }

        public IEnumerable<string> GetStreams(AmazonKinesisClient client = null)
        {
            if (client == null)
            {
                client = CreateClient();
            }

            if (client == null)
            {
                return new List<string>();
            }

            ListStreamsResponse streamResp = null;
            try
            {
                var listStreamRequest = new ListStreamsRequest();
                streamResp = client.ListStreams(listStreamRequest);
            }
            catch (Exception e)
            {
                Log.Error(Resources.LogStreamListFailure);
                Log.Error(e.ToString());
            }

            if (streamResp != null)
            {
                return streamResp.StreamNames;
            }

            return new List<string>();
        }

        public IEnumerable<Shard> GetShards(AmazonKinesisClient client = null)
        {
            if (client == null)
            {
                client = CreateClient();
            }
            var stream = settings.ParameterizeValue(settings.Stream);

            DescribeStreamResponse streamResponse = null;
            try
            {
                var describeStreamRequest = new DescribeStreamRequest()
                {
                    StreamName = stream
                };

                streamResponse = client.DescribeStream(describeStreamRequest);
            }
            catch (Exception e)
            {
                Log.Error(Resources.LogStreamResponseFailure);
                Log.Error(e.ToString());
            }

            if (streamResponse != null)
            {
                return streamResponse.StreamDescription.Shards;
            }

            return new List<Shard>();
        }

        public string GetShardIterator(
            Shard shard, ShardIteratorType iterType = null,
            AmazonKinesisClient client = null)
        {
            if (client == null)
            {
                client = CreateClient();
            }

            ShardIteratorType shardIteratorType = iterType ??
                settings.ParameterizeValue(settings.ShardIteratorType);

            string shardStart =
                settings.ParameterizeValue(settings.SequenceNumber);
            string stream = settings.ParameterizeValue(settings.Stream);

            GetShardIteratorResponse shardResponse = null;
            try
            {
                var getShardIteratorRequest = new GetShardIteratorRequest()
                {
                    StreamName = stream,
                    ShardId = shard.ShardId,
                    ShardIteratorType = shardIteratorType,
                    StartingSequenceNumber = shardStart,
                };
                shardResponse = client.GetShardIterator(getShardIteratorRequest);
            }
            catch (Exception e)
            {
                Log.Error(Resources.LogShardIteratorFailure);
                Log.Error(e.ToString());
            }

            if (shardResponse != null)
            {
                return shardResponse.ShardIterator;
            }

            return String.Empty;
        }

        public IEnumerable<Record> GetRecords(
            string shardIterator,
            out GetRecordsResponse recordsResponse,
            AmazonKinesisClient client = null)
        {
            if (client == null)
            {
                client = CreateClient();
            }

            int recLimit = settings.ParseRecordLimit();
            var getRecordsRequest = new GetRecordsRequest()
            {
                ShardIterator = shardIterator,
                Limit = recLimit
            };
            try
            {
                recordsResponse = client.GetRecords(getRecordsRequest);
                return recordsResponse.Records;
            }
            catch (Exception e)
            {
                Log.Error(Resources.LogNoRecords, e); ;
                recordsResponse = null;
                return new List<Record>();
            }
        }
    }
}