﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.DB2;

namespace Panopticon.DataDirect.UI.DB2.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.DB2.Plugin, DB2ConnectionSettings>
    {
        public PluginUI(DataDirect.DB2.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(DB2ConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new DatabaseConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            DatabaseConfigPanel panel = (DatabaseConfigPanel)element;

            return panel.Settings.ToPropertyBag();
        }
    }
}
