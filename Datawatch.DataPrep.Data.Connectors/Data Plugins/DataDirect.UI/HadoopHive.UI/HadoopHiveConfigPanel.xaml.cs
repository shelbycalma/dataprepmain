﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HadoopHive;

namespace Panopticon.DataDirect.UI.HadoopHive.UI
{
    /// <summary>
    /// Interaction logic for HadoopHiveConfigPanel.xaml
    /// </summary>
    public partial class HadoopHiveConfigPanel
    {
        public HadoopHiveConfigPanel()
            : this(null)
        {
        }

        public HadoopHiveConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HadoopHiveConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
