﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HadoopHive;

namespace Panopticon.DataDirect.UI.HadoopHive.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HadoopHive.Plugin, HadoopHiveConnectionSettings>
    {
        public PluginUI(DataDirect.HadoopHive.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HadoopHiveConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HadoopHiveConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HadoopHiveConfigPanel panel = (HadoopHiveConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
