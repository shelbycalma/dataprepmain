﻿using System.ComponentModel;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.Informix;

namespace Panopticon.DataDirect.UI.Informix.UI
{
    /// <summary>
    /// Interaction logic for InformixConfigPanel.xaml
    /// </summary>
    public partial class InformixConfigPanel 
    {
        public InformixConfigPanel() : this(null)
        {
            InitializeComponent();
        }

        public InformixConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            if(settings != null)
            {
                PasswordBox.Password = settings.Password;
                settings.PropertyChanged += settings_PropertyChanged;
            }
        }

        private void settings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Password" &&
                PasswordBox.Password != settings.Password)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as InformixConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
