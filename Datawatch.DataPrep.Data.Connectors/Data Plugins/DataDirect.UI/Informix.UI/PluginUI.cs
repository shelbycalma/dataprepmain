﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.Informix;

namespace Panopticon.DataDirect.UI.Informix.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.Informix.Plugin, InformixConnectionSettings>
    {
        public PluginUI(DataDirect.Informix.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(InformixConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new InformixConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            InformixConfigPanel panel = (InformixConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
