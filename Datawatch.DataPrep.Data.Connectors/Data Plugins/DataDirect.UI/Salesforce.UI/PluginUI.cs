﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.Salesforce;

namespace Panopticon.DataDirect.UI.Salesforce.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.Salesforce.Plugin, SalesforceConnectionSettings>
    {
        public PluginUI(DataDirect.Salesforce.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(SalesforceConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new SalesforceConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            SalesforceConfigPanel panel = (SalesforceConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
