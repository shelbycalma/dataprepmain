﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HiveIBMBigInsights;

namespace Panopticon.DataDirect.UI.HiveIBMBigInsights.UI
{
    /// <summary>
    /// Interaction logic for HiveIBMBigInsightsConfigPanel.xaml
    /// </summary>
    public partial class HiveIBMBigInsightsConfigPanel
    {
        public HiveIBMBigInsightsConfigPanel()
            : this(null)
        {
        }

        public HiveIBMBigInsightsConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HiveIBMBigInsightsConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
