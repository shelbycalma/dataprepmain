﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HiveIBMBigInsights;

namespace Panopticon.DataDirect.UI.HiveIBMBigInsights.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HiveIBMBigInsights.Plugin, HiveIBMBigInsightsConnectionSettings>
    {
        public PluginUI(DataDirect.HiveIBMBigInsights.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HiveIBMBigInsightsConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HiveIBMBigInsightsConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HiveIBMBigInsightsConfigPanel panel = (HiveIBMBigInsightsConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
