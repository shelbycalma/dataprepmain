﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.Impala;

namespace Panopticon.DataDirect.UI.Impala.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.Impala.Plugin, ImpalaConnectionSettings>
    {
        public PluginUI(DataDirect.Impala.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(ImpalaConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new DatabaseConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            DatabaseConfigPanel panel = (DatabaseConfigPanel)element;

            return panel.Settings.ToPropertyBag();
        }
    }
}
