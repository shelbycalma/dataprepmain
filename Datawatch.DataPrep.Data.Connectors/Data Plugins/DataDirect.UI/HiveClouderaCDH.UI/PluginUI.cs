﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HiveClouderaCDH;

namespace Panopticon.DataDirect.UI.HiveClouderaCDH.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HiveClouderaCDH.Plugin, HiveClouderaCDHConnectionSettings>
    {
        public PluginUI(DataDirect.HiveClouderaCDH.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HiveClouderaCDHConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HiveClouderaCDHConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HiveClouderaCDHConfigPanel panel = (HiveClouderaCDHConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
