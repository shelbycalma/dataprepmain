﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HiveClouderaCDH;

namespace Panopticon.DataDirect.UI.HiveClouderaCDH.UI
{
    /// <summary>
    /// Interaction logic for HiveClouderaCDHConfigPanel.xaml
    /// </summary>
    public partial class HiveClouderaCDHConfigPanel
    {
        public HiveClouderaCDHConfigPanel()
            : this(null)
        {
        }

        public HiveClouderaCDHConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HiveClouderaCDHConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
