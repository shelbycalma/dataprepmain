﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.MySql;

namespace Panopticon.DataDirect.UI.MySQL.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.MySql.Plugin, MySqlConnectionSettings>
    {
        public PluginUI(DataDirect.MySql.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(MySqlConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new DatabaseConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            DatabaseConfigPanel panel = (DatabaseConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
