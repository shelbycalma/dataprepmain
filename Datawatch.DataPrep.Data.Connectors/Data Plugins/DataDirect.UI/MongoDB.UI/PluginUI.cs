﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.MongoDB;

namespace Panopticon.DataDirect.UI.MongoDB.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.MongoDB.Plugin, MongoDBConnectionSettings>
    {
        public PluginUI(DataDirect.MongoDB.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(MongoDBConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new MongoDBConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            MongoDBConfigPanel panel = (MongoDBConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
