﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;

namespace Panopticon.DataDirect.UI.MongoDB.UI
{
    /// <summary>
    /// Interaction logic for MongoDBConfigPanel.xaml
    /// </summary>
    public partial class MongoDBConfigPanel 
    {
        
        public MongoDBConfigPanel()
            : this(null)
        {
        }

        public MongoDBConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as DatabaseSettingsBase;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
