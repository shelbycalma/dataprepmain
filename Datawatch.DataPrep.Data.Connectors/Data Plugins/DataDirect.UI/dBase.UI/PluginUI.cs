﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.dBase;

namespace Panopticon.DataDirect.UI.dBase.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.dBase.Plugin, dBaseConnectionSettings>
    {
        public PluginUI(DataDirect.dBase.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(dBaseConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new dBaseConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            dBaseConfigPanel panel = (dBaseConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
