﻿using Datawatch.DataPrep.Data.Core.Connectors.Database;

namespace Panopticon.DataDirect.UI.dBase.UI
{
    /// <summary>
    /// Interaction logic for dBaseConfigPanel.xaml
    /// </summary>
    public partial class dBaseConfigPanel 
    {
        public dBaseConfigPanel()
            : this(null)
        {
        }

        public dBaseConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
        }

    }
}
