﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.SparkSql;

namespace Panopticon.DataDirect.UI.SparkSql.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.SparkSql.Plugin, SparkSqlConnectionSettings>
    {
        public PluginUI(DataDirect.SparkSql.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(SparkSqlConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new SparkSqlConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            SparkSqlConfigPanel panel = (SparkSqlConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
