﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.DataDirect.UI
{
    public partial class DatabaseConfigPanel : UserControl,
        IDataPluginConfigElement
    {
        private DatabaseSettingsBase settings;
        
        public DatabaseConfigPanel()
            : this(null)
        {
        }

        public DatabaseConfigPanel(DatabaseSettingsBase settings)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }
        
        public DatabaseSettingsBase Settings
        {
            get { return settings; }
        }

        private void UserControl_DataContextChanged(object sender, 
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as DatabaseSettingsBase;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
        public void OnOk()
        {
            if (Settings == null)
            {
                return;
            }

            // This method is called when Ok button in DataSourceSettingsDialog is clicked.
            // Clear the column here, so that even if settings are not chagned, latest schema from 
            // DB is pulled.
            Settings.QuerySettings.SchemaColumnsSettings.ClearSchemaColumns();
        }

        public bool IsOk
        {
            get
            {
                if (Settings == null) return false;

                return Settings.IsOk;
            }
        }
    }
}
