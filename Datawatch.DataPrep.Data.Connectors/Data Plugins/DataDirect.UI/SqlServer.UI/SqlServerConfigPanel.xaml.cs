﻿using System.ComponentModel;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.SqlServer;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.DataDirect.UI.SqlServer.UI
{
    /// <summary>
    /// Interaction logic for SqlServerConfigPanel.xaml
    /// </summary>
    public partial class SqlServerConfigPanel
    {
        private static readonly string FileFilter =
        string.Concat(Properties.Resources.UiCerFileFilter, "|*.crt|",
        Properties.Resources.UiCerFileFilter, "|*.cer|",
        Properties.Resources.UiJKSFileFilter, "|*.jks|",
        Properties.Resources.UiPEMFileFilter, "|*.pem|",
        Properties.Resources.UiAllFileFilter, "|*.*");

        public SqlServerConfigPanel() : this(null)
        {
            InitializeComponent();
        }

        public SqlServerConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            if(settings != null)
            {
                PasswordBox.Password = settings.Password;
                settings.PropertyChanged += settings_PropertyChanged;
            }
        }

        private void settings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if(e.PropertyName == "Password" &&
                PasswordBox.Password != settings.Password)
            {
                PasswordBox.Password = settings.Password;
            }
            if (e.PropertyName == "TrustStorePassword" && TrustStorePassword.Password != ((SqlServerConnectionSettings)Settings).TrustStorePassword)
            {
                TrustStorePassword.Password = ((SqlServerConnectionSettings)Settings).TrustStorePassword;
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Should pass GlobalSettingsBag, so that InitialDirectory logic
            // inside BrowseForFile works.
            string path = DataPluginUtils.BrowseForFile(Window.GetWindow(this),
                ((SqlServerConnectionSettings)Settings).TrustStore, FileFilter, new PropertyBag());
            if (!string.IsNullOrEmpty(path))
                ((SqlServerConnectionSettings)Settings).TrustStore = path;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as SqlServerConnectionSettings;
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void TrustStorePassword_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                ((SqlServerConnectionSettings)Settings).TrustStorePassword = TrustStorePassword.Password;
            }
        }
    }
}
