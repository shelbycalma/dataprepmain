﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.SqlServer;

namespace Panopticon.DataDirect.UI.SqlServer.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.SqlServer.Plugin, SqlServerConnectionSettings>
    {
        public PluginUI(DataDirect.SqlServer.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(SqlServerConnectionSettings settings,
           IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new SqlServerConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            SqlServerConfigPanel panel = (SqlServerConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
