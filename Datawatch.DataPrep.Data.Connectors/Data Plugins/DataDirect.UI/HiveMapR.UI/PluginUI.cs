﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HiveMapR;

namespace Panopticon.DataDirect.UI.HiveMapR.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HiveMapR.Plugin, HiveMapRConnectionSettings>
    {
        public PluginUI(DataDirect.HiveMapR.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HiveMapRConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HiveMapRConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HiveMapRConfigPanel panel = (HiveMapRConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
