﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HiveMapR;

namespace Panopticon.DataDirect.UI.HiveMapR.UI
{
    /// <summary>
    /// Interaction logic for HiveMapRConfigPanel.xaml
    /// </summary>
    public partial class HiveMapRConfigPanel
    {
        public HiveMapRConfigPanel()
            : this(null)
        {
        }

        public HiveMapRConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HiveMapRConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
