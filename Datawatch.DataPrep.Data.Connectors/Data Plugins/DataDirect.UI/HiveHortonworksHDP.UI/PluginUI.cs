﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HiveHortonworksHDP;

namespace Panopticon.DataDirect.UI.HiveHortonworksHDP.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HiveHortonworksHDP.Plugin, HiveHortonworksHDPConnectionSettings>
    {
        public PluginUI(DataDirect.HiveHortonworksHDP.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HiveHortonworksHDPConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HiveHortonworksHDPConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HiveHortonworksHDPConfigPanel panel = (HiveHortonworksHDPConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
