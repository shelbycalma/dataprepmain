﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HiveHortonworksHDP;

namespace Panopticon.DataDirect.UI.HiveHortonworksHDP.UI
{
    /// <summary>
    /// Interaction logic for HiveHortonworksHDPConfigPanel.xaml
    /// </summary>
    public partial class HiveHortonworksHDPConfigPanel
    {
        public HiveHortonworksHDPConfigPanel()
            : this(null)
        {
        }

        public HiveHortonworksHDPConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HiveHortonworksHDPConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
