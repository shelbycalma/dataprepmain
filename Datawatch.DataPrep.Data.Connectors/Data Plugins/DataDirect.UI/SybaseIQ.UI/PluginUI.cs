﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.SybaseIQ;

namespace Panopticon.DataDirect.UI.SybaseIQ.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.SybaseIQ.Plugin, SybaseIQConnectionSettings>
    {
        public PluginUI(DataDirect.SybaseIQ.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(SybaseIQConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new DatabaseConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            DatabaseConfigPanel panel = (DatabaseConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
