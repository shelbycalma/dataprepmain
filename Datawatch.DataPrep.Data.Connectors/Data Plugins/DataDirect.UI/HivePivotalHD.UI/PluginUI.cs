﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HivePivotalHD;

namespace Panopticon.DataDirect.UI.HivePivotalHD.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HivePivotalHD.Plugin, HivePivotalHDConnectionSettings>
    {
        public PluginUI(DataDirect.HivePivotalHD.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HivePivotalHDConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HivePivotalHDConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HivePivotalHDConfigPanel panel = (HivePivotalHDConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
