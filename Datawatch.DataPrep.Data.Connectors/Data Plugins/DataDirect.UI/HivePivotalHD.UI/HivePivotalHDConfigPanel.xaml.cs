﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HivePivotalHD;

namespace Panopticon.DataDirect.UI.HivePivotalHD.UI
{
    /// <summary>
    /// Interaction logic for HivePivotalHDConfigPanel.xaml
    /// </summary>
    public partial class HivePivotalHDConfigPanel
    {
        public HivePivotalHDConfigPanel()
            : this(null)
        {
        }

        public HivePivotalHDConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HivePivotalHDConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
