﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.HiveAmazonEMR;

namespace Panopticon.DataDirect.UI.HiveAmazonEMR.UI
{
    public class PluginUI: OdbcPluginUIBase<DataDirect.HiveAmazonEMR.Plugin, HiveAmazonEMRConnectionSettings>
    {
        public PluginUI(DataDirect.HiveAmazonEMR.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(HiveAmazonEMRConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            // Uptill now we only need basic UI for Host/Port etc.
            // So DatabaseConfigPanel should work.
            return new HiveAmazonEMRConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            HiveAmazonEMRConfigPanel panel = (HiveAmazonEMRConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
