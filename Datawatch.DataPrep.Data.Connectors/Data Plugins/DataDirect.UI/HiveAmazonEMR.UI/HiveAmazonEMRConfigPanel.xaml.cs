﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.HiveAmazonEMR;

namespace Panopticon.DataDirect.UI.HiveAmazonEMR.UI
{
    /// <summary>
    /// Interaction logic for HiveAmazonEMRConfigPanel.xaml
    /// </summary>
    public partial class HiveAmazonEMRConfigPanel
    {
        public HiveAmazonEMRConfigPanel()
            : this(null)
        {
        }

        public HiveAmazonEMRConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HiveAmazonEMRConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
