﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.Teradata;

namespace Panopticon.DataDirect.UI.Teradata.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.Teradata.Plugin, TeradataConnectionSettings>
    {
        public PluginUI(DataDirect.Teradata.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(TeradataConnectionSettings settings,
           IEnumerable<ParameterValue> parameters)
        {
            return new TeradataConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            TeradataConfigPanel panel = (TeradataConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
