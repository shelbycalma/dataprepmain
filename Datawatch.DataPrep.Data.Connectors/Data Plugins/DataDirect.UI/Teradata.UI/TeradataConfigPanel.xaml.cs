﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.Teradata;

namespace Panopticon.DataDirect.UI.Teradata.UI
{
    /// <summary>
    /// Interaction logic for TeradataConfigPanel.xaml
    /// </summary>
    public partial class TeradataConfigPanel
    {
        public TeradataConfigPanel() 
            :this(null)
        {
            InitializeComponent();
        }

        public TeradataConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            if(settings != null)
            {
                PasswordBox.Password = settings.Password;
                settings.PropertyChanged += settings_PropertyChanged;
            }
        }

        public List<string> SecurityMechanismList
        {
            get
            {
                List<string> result = new List<string>();
                foreach(SecurityMechanism sm in ((TeradataConnectionSettings)Settings).SecurityMechanisms)
                {
                    result.Add(sm.ToString());
                }
                return result;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void settings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Password" &&
                PasswordBox.Password != settings.Password)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as TeradataConnectionSettings;
        }
    }
}
