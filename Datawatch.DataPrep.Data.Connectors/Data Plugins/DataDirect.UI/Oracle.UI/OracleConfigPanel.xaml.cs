﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.DataDirect.Oracle;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using System.ComponentModel;

namespace Panopticon.DataDirect.UI.Oracle.UI
{
    public partial class OracleConfigPanel
    {
        private static readonly string FileFilter =
            string.Concat(Properties.Resources.UiPEMFileFilter, "|*.pem|",
            Properties.Resources.UiJKSFileFilter, "|*.jks|",
            Properties.Resources.UiCerFileFilter, "|*.cer|",
            Properties.Resources.UiAllFileFilter, "|*.*");
        public OracleConfigPanel()
            : this(null)
        {
        }

        public OracleConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;

            if (settings != null)
            {
                TrustStorePassword.Password = ((OracleConnectionSettings)Settings).TrustStorePassword;
                KeyStorePassword.Password = ((OracleConnectionSettings)Settings).KeyStorePassword;
                KeyPassword.Password = ((OracleConnectionSettings)Settings).KeyPassword;
            }
        }
        
        private void UserControl_DataContextChanged(object sender, 
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as DatabaseSettingsBase;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Should pass GlobalSettingsBag, so that InitialDirectory logic
            // inside BrowseForFile works.
            string path = DataPluginUtils.BrowseForFile(Window.GetWindow(this),
                ((OracleConnectionSettings)Settings).TrustStore, FileFilter, new PropertyBag());
            if (!string.IsNullOrEmpty(path))
                ((OracleConnectionSettings)Settings).TrustStore = path;
        }

        private void KeyStoreBrowse_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Should pass GlobalSettingsBag, so that InitialDirectory logic
            // inside BrowseForFile works.
            string path = DataPluginUtils.BrowseForFile(Window.GetWindow(this),
                ((OracleConnectionSettings)Settings).KeyStore, FileFilter, new PropertyBag());
            if (!string.IsNullOrEmpty(path))
                ((OracleConnectionSettings)Settings).KeyStore = path;
        }

        private void settings_PropertyChanged(object sender,
                PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Password" &&
                PasswordBox.Password != settings.Password)
            {
                PasswordBox.Password = settings.Password;
            }
            if (e.PropertyName == "TrustStorePassword" && TrustStorePassword.Password != ((OracleConnectionSettings)Settings).TrustStorePassword)
            {
                TrustStorePassword.Password = ((OracleConnectionSettings)Settings).TrustStorePassword;
            }
        }

        private void TrustStorePassword_PasswordChanged(object sender,
                System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                ((OracleConnectionSettings)Settings).TrustStorePassword = TrustStorePassword.Password;
            }
        }

        private void KeyStorePassword_PasswordChanged(object sender,
        System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                ((OracleConnectionSettings)Settings).KeyStorePassword = KeyStorePassword.Password;
            }
        }

        private void KeyPassword_PasswordChanged(object sender,
        System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                ((OracleConnectionSettings)Settings).KeyPassword = KeyPassword.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

    }
}
