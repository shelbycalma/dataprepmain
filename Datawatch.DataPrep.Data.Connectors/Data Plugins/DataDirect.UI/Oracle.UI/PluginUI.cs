﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DataDirect.Oracle;

namespace Panopticon.DataDirect.UI.Oracle.UI
{
    public class PluginUI : OdbcPluginUIBase<DataDirect.Oracle.Plugin, OracleConnectionSettings>
    {
        public PluginUI(DataDirect.Oracle.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(OracleConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new OracleConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            OracleConfigPanel panel = (OracleConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
