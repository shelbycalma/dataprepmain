﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using aleri_pubsubconst;
using Datawatch.DataPrep.Data.Core.UI.Mediator;
using Microsoft.Win32;

namespace Panopticon.AleriPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        public static DependencyProperty SettingsProperty = 
            DependencyProperty.Register("Settings", 
                typeof(AleriSettings), typeof(ConnectionPanel), 
                    new PropertyMetadata(new PropertyChangedCallback(
                        Settings_Changed)));

        public static RoutedCommand SelectRsaKeyFileCommand =
            new RoutedCommand("SelectRsaKeyFile", typeof(ConnectionPanel));

        public ConnectionPanel()
        {
            Mediator.Instance.Register(this);
            InitializeComponent();
        }

        public AleriSettings Settings
        {
            get { return (AleriSettings)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj, 
            DependencyPropertyChangedEventArgs args) 
        {
            ((ConnectionPanel)obj).OnSettingsChanged(
                (AleriSettings)args.OldValue,
                (AleriSettings)args.NewValue);
        }

        protected void OnSettingsChanged(AleriSettings oldSettings,
            AleriSettings newSettings) 
        {
            DataContext = newSettings;

            if (newSettings != null) 
            {
                PasswordBox.Password = newSettings.Password;
            } 
            else 
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void SelectRsaKeyFile_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Settings != null && 
                Settings.AuthenticationType == SpAuthType.AUTH_RSA;
        }

        private void SelectRsaKeyFile_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Multiselect = false;

            if (dlg.ShowDialog() == true)
            {
                Settings.RsaKeyFile = 
                    System.IO.Path.GetFileName(dlg.FileName);
            }
        }

        [MediatorMessageSink("ShowErrorMessage",
            ParameterType = typeof(ShowErrorMessageArgs))]
        public void ShowErrorMessage(ShowErrorMessageArgs args)
        {
            if (args.ViewModel != DataContext)
            {
                return;
            }
            MessageBox.Show(args.Message, Properties.Resources.UiTitle,
                MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
