﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Panopticon.AleriPlugin.UI
{
    /// <summary>
    /// Interaction logic for QueryWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private AleriSettings settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(AleriSettings settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public AleriSettings Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOK;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
