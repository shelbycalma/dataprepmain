﻿using System;
using System.Windows.Data;
using aleri_pubsubconst;
using System.Windows;
using Exceptions = Datawatch.DataPrep.Data.Core.Exceptions;

namespace Panopticon.AleriPlugin.UI
{
    class AuthenticationTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is SpAuthType)) return DependencyProperty.UnsetValue;

            SpAuthType authType = (SpAuthType)value;

            switch (authType)
            {
                default:
                case SpAuthType.AUTH_NONE:
                    return Properties.Resources.UiAuthTypeNone;
                case SpAuthType.AUTH_PAM:
                    return Properties.Resources.UiAuthTypePam;
                case SpAuthType.AUTH_RSA:
                    return Properties.Resources.UiAuthTypeRsa;
                case SpAuthType.AUTH_KERBV5:
                    return Properties.Resources.UiAuthTypeKerberos;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
