﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AleriPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, AleriSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.AleriPlugin.UI;component" +
            "/aleri-small.png";

        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            AleriSettings con = new AleriSettings();
            ConnectionWindow win = new ConnectionWindow(con);
            win.Owner = owner;
            
            if (win.ShowDialog() == true)
            {
                PropertyBag bag = con.ToPropertyBag();

                return bag;
            }

            return null;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ConnectionPanel cp = new ConnectionPanel();

            cp.Settings = new AleriSettings(bag);

            return cp;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;

            return cp.Settings.ToPropertyBag();
        }
    }
}
