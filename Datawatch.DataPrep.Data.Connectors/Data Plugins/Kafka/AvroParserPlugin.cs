﻿using System;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaPlugin
{
    public class AvroParserPlugin : ParserPluginBase, IParserPlugin
    {
        internal const string Name = "Avro.GenericRecord";

        private readonly KafkaSettings pluginSettings;
        internal AvroParserPlugin(KafkaSettings pluginSettings)
        {
            this.pluginSettings = pluginSettings;
        }

        public Type GetParserType()
        {
            return typeof(AvroParserPlugin);
        }

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new AvroParserSettings(pluginSettings, bag);
        }

        public override string Id
        {
            get { return Name; }
        }

        public override string Title
        {
            get { return Name; }
        }

        public override bool IsObsolete
        {
            get { return true; }
        }
    }
}
