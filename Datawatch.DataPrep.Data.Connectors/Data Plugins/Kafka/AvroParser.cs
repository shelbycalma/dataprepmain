﻿using System;
using System.Collections.Generic;
using Avro.Generic;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaPlugin
{
    internal class AvroParser : ParserBase, IAvroParser
    {
        private readonly AvroParserSettings settings;

        public AvroParser(AvroParserSettings settings)
        {
            this.settings = settings;
        }

        public Dictionary<string, object> Parse(GenericRecord record)
        {
            if (record == null)
            {
                return null;
            }

            Dictionary<string, object> dictionary =
                new Dictionary<string, object>();

            int index = 0;
            object[] values = new object[settings.ColumnCount];

            foreach (ColumnDefinition column in settings.Columns)
            {
                if (!column.IsEnabled)
                {
                    index++;
                    continue;
                }

                object value;
                if (record.TryGetValue(column.Name, out value))
                {
                    if (column.Type == ColumnType.Time)
                    {
                        AvroColumnDefinition avroColumn =
                            (AvroColumnDefinition)column;
                        value = ConvertToTime(avroColumn.LogicalType, value);
                    }
                    values[index] = value;
                    dictionary[column.Name] = value;
                }
                index++;
            }
            return GetFilteredRecord(values, dictionary, 
                settings.Parameters, settings.Columns);
        }
        private static readonly long TicksAtEpoch = 621355968000000000L;
        private static readonly long TicksPerMillisecond = 10000;
        private static readonly long MillisPerDay = 1000 * 60 * 60 * 24;
        private object ConvertToTime(string logicalType, object value)
        {

            switch (logicalType)
            {
                case "date":
                    {
                        if (!(value is int)) return null;
                        // value is an int containing days since 1 January 1970
                        int i = (int)value;
                        return new DateTime(TicksAtEpoch + i * MillisPerDay * TicksPerMillisecond);
                    }
                case "time-millis":
                    {
                        if (!(value is int)) return null;
                        // value is an int containing the number of milliseconds after midnight
                        int i = (int)value;
                        DateTime today = DateTime.Now.Date;
                        return new DateTime(today.Ticks + i * TicksPerMillisecond);
                    }
                case "timestamp-millis":
                    {
                        if (!(value is long)) return null;
                        //value is a long containing the number of milliseconds from the unix epoch, 1 January 1970 00:00:00.000 UTC
                        long l = (long)value;
                        return new DateTime(TicksAtEpoch + l * TicksPerMillisecond);
                    }
            }
            return null;
        }

        public Dictionary<string, object> Parse(string message)
        {
            throw new System.NotImplementedException();
        }
    }
}
