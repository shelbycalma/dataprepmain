﻿using Kafka.Client.Serialization;
using Panopticon.KafkaPlugin.SchemaRegistry;
using Message = Kafka.Client.Messages.Message;

namespace Panopticon.KafkaPlugin.Avro
{
    public class KafkaAvroDecoder : AbstractKafkaAvroDeserializer, IDecoder<object>
    {
        public KafkaAvroDecoder(SchemaRegistryClient schemaRegistry) : base(schemaRegistry)
        {
        }

        public object ToEvent(Message message)
        {
            return Deserialize(message.Payload);
        }
    }
}