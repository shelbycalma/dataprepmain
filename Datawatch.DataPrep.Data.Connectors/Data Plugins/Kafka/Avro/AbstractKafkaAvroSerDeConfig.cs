﻿using System.Collections.Generic;

namespace Panopticon.KafkaPlugin.Avro
{
    public class AbstractKafkaAvroSerDeConfig
    {
        public static readonly string SCHEMA_REGISTRY_URL_CONFIG = "schema.registry.url";
        public static readonly string SCHEMA_REGISTRY_URL_DOC =
            "Comma-separated list of URLs for schema registry instances that can be used to register or look up schemas.";

        public static readonly string MAX_SCHEMAS_PER_SUBJECT_CONFIG = "max.schemas.per.subject";
        public static readonly int MAX_SCHEMAS_PER_SUBJECT_DEFAULT = 1000;
        public static readonly string MAX_SCHEMAS_PER_SUBJECT_DOC =
          "Maximum number of schemas to create or cache locally.";

        //public static ConfigDef baseConfigDef()
        //{
        //    return new ConfigDef()
        //        .define(SCHEMA_REGISTRY_URL_CONFIG, Codec.Type.LIST,
        //                Importance.HIGH, SCHEMA_REGISTRY_URL_DOC)
        //        .define(MAX_SCHEMAS_PER_SUBJECT_CONFIG, Type.INT, MAX_SCHEMAS_PER_SUBJECT_DEFAULT,
        //                Importance.LOW, MAX_SCHEMAS_PER_SUBJECT_DOC);
        //}

        public AbstractKafkaAvroSerDeConfig(/*ConfigDef config, */IDictionary<string, string> props)
        {
            //super(config, props);
        }

        //public int MaxSchemasPerSubject
        //{
        //    get { return this.getInt(MAX_SCHEMAS_PER_SUBJECT_CONFIG); }
        //}

        //public List<string> SchemaRegistryUrls
        //{
        //    get { return this.getList(SCHEMA_REGISTRY_URL_CONFIG); }
        //}

    }
}