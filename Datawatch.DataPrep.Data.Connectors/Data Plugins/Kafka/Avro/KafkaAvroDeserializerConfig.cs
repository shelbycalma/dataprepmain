﻿using System.Collections.Generic;

namespace Panopticon.KafkaPlugin.Avro
{
    public class KafkaAvroDeserializerConfig : AbstractKafkaAvroSerDeConfig
    {
        public static readonly string SPECIFIC_AVRO_READER_CONFIG = "specific.avro.reader";
        public static readonly bool SPECIFIC_AVRO_READER_DEFAULT = false;
        public static readonly string SPECIFIC_AVRO_READER_DOC =
            "If true, tries to look up the SpecificRecord class ";

        //private static ConfigDef config;

        static KafkaAvroDeserializerConfig()
        {
            //config = baseConfigDef().define(SPECIFIC_AVRO_READER_CONFIG, Type.bool, SPECIFIC_AVRO_READER_DEFAULT,
            //    Importance.LOW, SPECIFIC_AVRO_READER_DOC);
        }

        public KafkaAvroDeserializerConfig(IDictionary<string, string> props) : base(props)
        {
            //super(config, props);
        }
    }
}