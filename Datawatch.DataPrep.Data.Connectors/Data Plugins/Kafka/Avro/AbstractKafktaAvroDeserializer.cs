﻿using System;
using System.IO;
using System.Runtime.Serialization;
using Avro;
using Avro.Generic;
using Avro.IO;
using Kafka.Client.Messages.Compression;
using Panopticon.KafkaPlugin.SchemaRegistry;

namespace Panopticon.KafkaPlugin.Avro
{
    public abstract class AbstractKafkaAvroDeserializer
    {
        public static readonly string SCHEMA_REGISTRY_SCHEMA_VERSION_PROP =
            "schema.registry.schema.version";

        protected static readonly byte MAGIC_BYTE = 0x0;
        protected static readonly int idSize = 4;

        protected readonly SchemaRegistryClient schemaRegistry;

        protected AbstractKafkaAvroDeserializer(SchemaRegistryClient schemaRegistry)
        {
            this.schemaRegistry = schemaRegistry;
        }

        /**
         * Deserializes the payload without including Schema information for primitive types, maps, and arrays. Just the resulting
         * deserialized object is returned.
         *
         * This behavior is the norm for Decoders/Deserializers.
         *
         * @param payload serialized data
         * @return the deserialized object
         * @throws SerializationException
         */
        protected object Deserialize(byte[] payload)
        {
            return Deserialize(null, false, payload, null);
        }

        // The object return type is a bit messy, but this is the simplest way to have flexible decoding and not duplicate
        // deserialization code multiple times for different variants.
        protected object Deserialize(string topic, bool isKey, byte[] payload, Schema readerSchemaElement)
        {
            // Even if the caller requests Schema & version, if the payload is null we cannot include it. The caller must handle
            // this case.
            if (payload == null)
            {
                return null;
            }

            ByteBuffer buffer = GetByteBuffer(payload);
            int id = GetBigEndianInt32(buffer.Buffer, 1);
            Schema schema = schemaRegistry.GetBySubjectAndID(null, id);
            int length = buffer.Length - 1 - idSize;
            object result;
            if (schema.Tag.Equals(Schema.Type.Bytes))
            {
                byte[] bytes = new byte[length];
                Buffer.BlockCopy(buffer.Buffer, idSize + 1, bytes, 0, length);
                result = bytes;
            }
            else
            {
                int start = idSize + 1 + buffer.Offset;
                using (MemoryStream ms = new MemoryStream(buffer.Buffer))
                {
                    ms.Seek(start, SeekOrigin.Begin);
                    DatumReader<GenericRecord> reader = GetDatumReader(schema, schema);
                    object obj = reader.Read(null, new BinaryDecoder(ms));

                    if (schema.Tag.Equals(Schema.Type.String))
                    {
                        obj = obj.ToString(); // Utf8 -> String
                    }
                    result = obj;
                }
            }

            return result;
        }

        private ByteBuffer GetByteBuffer(byte[] payload)
        {
            ByteBuffer buffer = ByteBuffer.NewAsync(payload);
            if (buffer.Buffer[0] != MAGIC_BYTE)
            {
                throw new SerializationException("Unknown magic byte!");
            }
            return buffer;
        }

        private static int GetBigEndianInt32(byte[] buffer, int i)
        {
            return buffer[i + 3] + 
                (buffer[i + 2] << 8) + 
                (buffer[i + 1] << 16) + 
                (buffer[i] << 24);
        }

        private DatumReader<GenericRecord> GetDatumReader(
            Schema writerSchemaElement, Schema readerSchemaElement)
        {
            return new GenericDatumReader<GenericRecord>(
                writerSchemaElement, readerSchemaElement);
        }
    }
}