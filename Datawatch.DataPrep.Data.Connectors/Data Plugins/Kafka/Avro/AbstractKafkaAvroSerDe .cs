﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Avro;
using Avro.Generic;
using Panopticon.KafkaPlugin.SchemaRegistry;

namespace Panopticon.KafkaPlugin.Avro
{
    public abstract class AbstractKafkaAvroSerDe
    {
        protected static readonly byte MAGIC_BYTE = 0x0;
        protected static readonly int idSize = 4;

        private static readonly Dictionary<string, Schema> primitiveSchemas;
        protected SchemaRegistryClient schemaRegistry;

        static AbstractKafkaAvroSerDe()
        {
            primitiveSchemas = new Dictionary<string, Schema>();
            primitiveSchemas.Add("Null", CreatePrimitiveSchema("null"));
            primitiveSchemas.Add("Boolean", CreatePrimitiveSchema("boolean"));
            primitiveSchemas.Add("Integer", CreatePrimitiveSchema("int"));
            primitiveSchemas.Add("Long", CreatePrimitiveSchema("long"));
            primitiveSchemas.Add("Float", CreatePrimitiveSchema("float"));
            primitiveSchemas.Add("Double", CreatePrimitiveSchema("double"));
            primitiveSchemas.Add("String", CreatePrimitiveSchema("string"));
            primitiveSchemas.Add("Bytes", CreatePrimitiveSchema("bytes"));
        }

        private static Schema CreatePrimitiveSchema(string type)
        {
            string schemaString = string.Format("{{\"type\" : \"{0}\"}}", type);
            return Schema.Parse(schemaString);
        }

        /**
         * Get the subject name for the given topic and value type.
         */

        protected static string GetSubjectName(string topic, bool isKey)
        {
            if (isKey)
            {
                return topic + "-key";
            }
            return topic + "-value";
        }

        /**
         * Get the subject name used by the old Encoder interface, which relies only on the value type rather than the topic.
         */

        protected static string GetOldSubjectName(object value)
        {
            if (value is GenericRecord)
            {
                return ((GenericRecord) value).Schema.Name + "-value";
            }
            throw new SerializationException("Primitive types are not supported yet");
        }

        protected Schema GetSchema(object obj)
        {
            if (obj == null)
            {
                return primitiveSchemas["Null"];
            }
            if (obj is bool)
            {
                return primitiveSchemas["Boolean"];
            }
            if (obj is int)
            {
                return primitiveSchemas["Integer"];
            }
            if (obj is long)
            {
                return primitiveSchemas["Long"];
            }
            if (obj is float)
            {
                return primitiveSchemas["Float"];
            }
            if (obj is double)
            {
                return primitiveSchemas["Double"];
            }
            if (obj is string)
            {
                return primitiveSchemas["String"];
            }
            if (obj is byte[])
            {
                return primitiveSchemas["Bytes"];
            }
            if (obj is GenericRecord)
            {
                return ((GenericRecord) obj).Schema;
            }
            throw new ArgumentException(
                "Unsupported Avro type. Supported types are null, Boolean, Integer, Long, " +
                "Float, Double, String, byte[] and IndexedRecord");
        }

    }
}