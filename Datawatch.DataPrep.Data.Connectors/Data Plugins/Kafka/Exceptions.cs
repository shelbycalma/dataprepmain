﻿using System;

namespace Panopticon.KafkaPlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Framework.Exceptions
    {
        internal static Exception MalformedBrokerUrl(
            string url,
            Exception innerException)
        {
            return LogException(new InvalidOperationException(
                "Resources.MalformedBrokerUrl"
                ,
                innerException));
        }

        internal static Exception ConnectionError(Exception exception)
        {
            return LogException(exception);
        }

        internal static Exception NoColumns()
        {
            return CreateInvalidOperation("Resources.NoColumns");
        }

        internal static Exception NoIdColumn()
        {
            return CreateInvalidOperation("Resources.NoIdColumn");
        }
    }
}