﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Avro.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Kafka.Client.Consumers;
using Kafka.Client.Exceptions;
using Kafka.Client.Messages;
using Kafka.Client.Serialization;
using Panopticon.KafkaPlugin.Avro;
using Panopticon.KafkaPlugin.SchemaRegistry;

namespace Panopticon.KafkaPlugin
{
    class KafkaConsumer
    {
        private readonly IMessageHandler messageReceiver;
        private const int DefaultMaxWaitTime = 0;
        private readonly string parameterizedHost;
        private readonly string parameterizedPort;
        private readonly string parameterizedTopic;
        private readonly string parameterizedSchemaRegistryHost;
        private readonly string parameterizedSchemaRegistryPort;
        private readonly bool useAvro = false;

        private ZookeeperConsumerConnector consumer;

        internal KafkaConsumer(IMessageHandler messageReceiver, 
            KafkaSettings settings) :
            this(messageReceiver, settings, DefaultMaxWaitTime)
        {
        }

        internal KafkaConsumer(IMessageHandler messageReceiver,
            KafkaSettings settings, int maxWaitTime)
        {
            this.messageReceiver = messageReceiver;
            //this.maxWaitTime = maxWaitTime;
            this.messageReceiver = messageReceiver;
            parameterizedHost = KafkaHelper.GetParameterizedHost(settings.Host,
                settings.Parameters);
            parameterizedPort = DataUtils.ApplyParameters(settings.ZookeeperPort,
                        settings.Parameters);
            parameterizedTopic = DataUtils.ApplyParameters(settings.Topic,
                        settings.Parameters);

            useAvro = settings.UseSchemaRegistry;

            parameterizedSchemaRegistryHost = DataUtils.ApplyParameters(settings.Host,
                settings.Parameters);
            parameterizedSchemaRegistryPort =
                DataUtils.ApplyParameters(settings.SchemaRegistryServicePort,
                settings.Parameters);

        }

        internal void Stop()
        {
            new Thread(ShutDown).Start();
        }

        private void ShutDown()
        {
            lock (this)
            {
                if (consumer == null)
                {
                    return;
                }
                consumer.Dispose();
            }
        }

        internal void StartConsuming()
        {
            lock (this)
            {
                consumer =
                    KafkaHelper.CreateConsumer(parameterizedHost + ":" +
                                               parameterizedPort);
            }

            {
                IDictionary<string, int> topicMap =
                    new Dictionary<string, int> {{parameterizedTopic, 1}};

                if (useAvro)
                {
                    IAvroMessageHandler avroMessageHandler =
                        messageReceiver as IAvroMessageHandler;
                    if (avroMessageHandler == null)
                    {
                        throw new Exception("No IAvroMessageHandler");
                    }

                    SchemaRegistryClient schemaRegistry =
                        new CachedSchemaRegistryClient(
                            parameterizedSchemaRegistryHost + ":" +
                            parameterizedSchemaRegistryPort,
                            100);
                    KafkaAvroDecoder avroDecoder =
                        new KafkaAvroDecoder(schemaRegistry);

                    // get references to topic streams.
                    IDictionary<string, IList<KafkaMessageStream<object>>> streams;

                    lock (this)
                    {
                        streams = consumer.CreateMessageStreams(topicMap, 
                            avroDecoder);
                    }

                    IList<KafkaMessageStream<object>> topicData =
                        streams[parameterizedTopic];

                    IEnumerator<object> messageEnumerator =
                        topicData[0].GetEnumerator();
                    try
                    {
                        while (messageEnumerator.MoveNext())
                        {

                            object record = messageEnumerator.Current;
                            if (!(record is GenericRecord)) continue;
                            avroMessageHandler.MessageReceived(
                                (GenericRecord) record);
                        }
                    }
                    catch (ConsumerTimeoutException cte)
                    {
                        Log.Exception(cte);
                    }
                }
                else
                {
                    // get references to topic streams.
                    IDictionary<string, IList<KafkaMessageStream<Message>>> streams;

                    lock (this)
                    {
                        streams =
                            consumer.CreateMessageStreams(topicMap, 
                                new DefaultDecoder());
                    }

                    IList<KafkaMessageStream<Message>> topicData =
                        streams[parameterizedTopic];

                    IEnumerator<Message> messageEnumerator =
                        topicData[0].GetEnumerator();
                    try
                    {
                        while (messageEnumerator.MoveNext())
                        {
                            Message m = messageEnumerator.Current;
                            messageReceiver.MessageReceived(
                                Encoding.UTF8.GetString(m.Payload));
                        }
                    }
                    catch (ConsumerTimeoutException cte)
                    {
                        Log.Exception(cte);
                    }
                }
                lock (this)
                {
                    consumer = null;
                }
            }
        }
    }
}
