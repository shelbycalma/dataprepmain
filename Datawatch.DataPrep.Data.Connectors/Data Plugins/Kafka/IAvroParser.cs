﻿using System.Collections.Generic;
using Avro.Generic;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.KafkaPlugin
{
    public interface IAvroParser : IParser
    {
        Dictionary<string, object> Parse(GenericRecord record);
    }
}