﻿using Avro;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Panopticon.KafkaPlugin.SchemaRegistry;

namespace Panopticon.KafkaPlugin
{
    public class AvroParserSettings : ParserSettings
    {
        private readonly KafkaSettings settings;

        public AvroParserSettings(KafkaSettings settings, PropertyBag bag)
            : base(bag)
        {
            this.settings = settings;
        }

        public override void InitiateNew(bool isMessageQueueSetting)
        {
            base.InitiateNew(isMessageQueueSetting);
            InitializeSchema();
        }

        public override void InitiateOld(bool isMessageQueueSetting)
        {
            base.InitiateOld(isMessageQueueSetting);
            InitializeSchema();
        }

        private void InitializeSchema()
        {
            string parameterizedTopic = DataUtils.ApplyParameters(
                settings.Topic, settings.Parameters);

            if (!string.IsNullOrEmpty(propertyBag.Values["SchemaId"]) && 
                parameterizedTopic == propertyBag.Values["Topic"])
            {
                return;
            }

            SchemaRegistryClient client = settings.GetSchemaRegistryClient();
            SchemaMetadata schemaMetadata =
                client.GetLatestSchemaMetadata(parameterizedTopic + "-value");
            Schema schema = client.GetByID(schemaMetadata.Id);
            if (!(schema is RecordSchema)) return;
            ClearColumnDefinitions();
            propertyBag.Values["Topic"] = parameterizedTopic;
            propertyBag.Values["SchemaId"] = schemaMetadata.Id.ToString();
            RecordSchema recordSchema = (RecordSchema) schema;
            foreach (Field field in recordSchema.Fields)
            {
                AvroColumnDefinition column =
                        new AvroColumnDefinition(new PropertyBag());

                column.Name = field.Name;
                string logicalType = field.GetProperty("logicaltype");

                // http://avro.apache.org/docs/1.8.1/spec.html#schema_primitive
                switch (field.Schema.Tag)
                {
                    case Schema.Type.String:
                    case Schema.Type.Boolean:
                        column.Type = ColumnType.Text;
                        break;
                    case Schema.Type.Int:
                        if (logicalType == "date" || 
                            logicalType == "time-millis")
                        {
                            column.Type = ColumnType.Time;
                        }
                        else
                        {
                            column.Type = ColumnType.Numeric;
                        }
                        break;
                    case Schema.Type.Long:
                        if (logicalType == "timestamp-millis")
                        {
                            column.Type = ColumnType.Time;
                        }
                        else
                        {
                            column.Type = ColumnType.Numeric;;
                        }
                        break;
                    case Schema.Type.Float:
                    case Schema.Type.Double:
                        column.Type = ColumnType.Numeric;
                        break;
                    default:
                        Log.Warning(Properties.Resources.LogTypeNotSupported, 
                            field.Schema.Name);
                        break;
                }
                column.LogicalType = logicalType;
                AddColumnDefinition(column);
            }
        }

        public void UpdateSchema()
        {
            propertyBag.Values.Remove("SchemaId");
            InitializeSchema();
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new AvroColumnDefinition(bag);
        }
        
        public override string GetDescription()
        {
            return "";
        }
        
        public override IParser CreateParser()
        {
            return new AvroParser(this);
        }
    }
}
