﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.KafkaPlugin.SchemaRegistry;
using ZooKeeperNet;

namespace Panopticon.KafkaPlugin
{
    public class KafkaSettings : MessageQueueSettingsBase
    {
        private int DefaultPartitionIndex = -1;
        private ICommand fetchTopicsCommand;
        private IEnumerable<string> topics;
        private ISet<string> schemaRegistrySubjects = null;

        public KafkaSettings(IPluginManager pluginHost)
            : this(pluginHost, new PropertyBag())
        {
        }

        public KafkaSettings(IPluginManager pluginHost, PropertyBag bag)
            : this(pluginHost, bag, null)
        {
        }

        public KafkaSettings(
            IPluginManager pluginHost, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginHost, bag, parameters)
        {
        }
        
        public new string Broker
        {
            get
            {
                return Host + ":" + Port;
            }
        }

        public string Host
        {
            get
            {
                return GetInternal("Host", "http://localhost");
            }
            set
            {
                SetInternal("Host", value);
            }
        }

        public string Port
        {
            get
            {
                return GetInternal("Port", "9092");
            }
            set
            {
                ValidatePort(value);
                SetInternal("Port", value);
            }
        }

        public string ZookeeperPort
        {
            get
            {
                return GetInternal("JavaPort", "2181");
            }
            set
            {
                ValidatePort(value);
                SetInternal("JavaPort", value);
            }
        }

        private void ValidatePort(string value)
        {
            int port = Int32.Parse(value);
            if (port > 65535)
            {
                throw new Exception(Properties.Resources.ExBadPort);
            }
        }

        public string SchemaRegistryServicePort
        {
            get
            {
                return GetInternal("SchemaRegistryServicePort", "8081");
            }
            set
            {
                ValidatePort(value);
                SetInternal("SchemaRegistryServicePort", value);
                schemaRegistrySubjects = null;
                FirePropertyChanged("IsTopicSchemaAvailable");
            }
        }
        
        public bool UseSchemaRegistry
        {
            get
            {
                string value = GetInternal("UseSchemaRegistry",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                if (value == UseSchemaRegistry) return;
                SetInternal("UseSchemaRegistry", value.ToString());
                if (parserSettings != null)
                {
                    parserSettings.Dispose();
                    parserSettings = null;
                }
                FirePropertyChanged("ParserSettings");
            }
        }

        public override IParserPlugin ParserPlugin
        {
            get
            {
                if (UseSchemaRegistry)
                {
                    return new AvroParserPlugin(this);
                }
                return base.ParserPlugin;
            }
            set { base.ParserPlugin = value; }
        }

        protected override string DefaultParserPluginId
        {
            get
            {
                if (UseSchemaRegistry)
                {
                    return AvroParserPlugin.Name;
                }
                return "Json";                          
            }
        }

        public override string Topic
        {
            get { return base.Topic; }
            set
            {
                base.Topic = value;
                UseSchemaRegistry = IsTopicSchemaAvailable;
                FirePropertyChanged("ParserSettings");
                FirePropertyChanged("IsTopicSchemaAvailable");
            }
        }

        internal SchemaRegistryClient GetSchemaRegistryClient()
        {
            string parameterizedHost = DataUtils.ApplyParameters(Host, Parameters);
            string parameterizedPort = DataUtils.ApplyParameters(SchemaRegistryServicePort, Parameters);
            SchemaRegistryClient schemaRegistry = null;
            ISet<string> subjects = null;
            return new CachedSchemaRegistryClient(
                parameterizedHost + ":" + parameterizedPort, 100);
        }

        public bool IsTopicSchemaAvailable
        {
            get
            {
                if (string.IsNullOrEmpty(Topic))
                {
                    UseSchemaRegistry = false;
                    return false;
                }

                if (schemaRegistrySubjects == null)
                {
                    try
                    {
                        SchemaRegistryClient schemaRegistry = GetSchemaRegistryClient();
                        schemaRegistrySubjects = new HashSet<string>(schemaRegistry.GetAllSubjects());
                    }
                    catch (Exception ex)
                    {
                        schemaRegistrySubjects = new HashSet<string>();
                        Log.Error(Properties.Resources.ExSchemaRegistry, ex.Message);
                    }
                }

                string parameterizedTopic = DataUtils.ApplyParameters(Topic, Parameters);
                string subject = parameterizedTopic + "-value";
                return schemaRegistrySubjects.Contains(subject);
            }
        }

        public IEnumerable<string> Topics
        {
            get
            {
                return topics;
            }
        }

        public int PartitionIndex
        {
            // Return the partition that ftm is a constant. 
            // The only purpose for having this method (returning a constant) 
            // instead of using a constant directly is to being able to have 
            // code (in the calling method) ready when we want to start
            // alowing user to configre also the partition in the connector GUI.
            // Then we only need to add this property in the GUI/workbook and 
            // it should work.
            get { return DefaultPartitionIndex; }
        }

        public override bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(Host) &&
                    !string.IsNullOrEmpty(Port) &&
                    !string.IsNullOrEmpty(Topic) &&
                    !string.IsNullOrEmpty(IdColumn) &&
                    IsParserSettingsOk;

            }
        }

        public virtual string WindowTitle
        {
            get { return Properties.Resources.UiConnectionWindowTitle; }
        }

        public override bool CanGenerateColumns()
        {
            return !(string.IsNullOrEmpty(Host) || 
                string.IsNullOrEmpty(Port) || 
                string.IsNullOrEmpty(Topic));
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        public override void DoGenerateColumns()
        {
            if (UseSchemaRegistry)
            {
                return;
            }

            StreamingColumnGenerator columnGenerator = 
                new StreamingColumnGenerator(this, errorReporter);

            KafkaConsumer consumer = new KafkaConsumer(columnGenerator, this);
            if (consumer == null) return;

            Task task = new Task(consumer.StartConsuming);
            task.Start();

            columnGenerator.Generate();
            consumer.Stop();
        }

        public ICommand FetchTopicsCommand
        {
            get
            {
                return fetchTopicsCommand ??
                   (fetchTopicsCommand =
                    new DelegateCommand(FetchTopics, CanFetchTopics));
            }
        }
        private void FetchTopics()
        {
            string host = KafkaHelper.GetParameterizedHost(Host,
                 Parameters);
            string port = DataUtils.ApplyParameters(ZookeeperPort,
                        Parameters);
            ZooKeeper zk = null;
            try
            {
                // Fix for DDTV-6712,
                // apparently there is an issue in ZooKeeper,
                // if we pass an invalid host, it crashes.
                if (!KafkaHelper.CanPingHost(host))
                {
                    MessageBox.Show(string.Format(
                        Properties.Resources.ExHostNotReachable, host),
                        Properties.Resources.UiConnectionWindowTitle,
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ConnectedWatcher watcher = new ConnectedWatcher();
                zk = new ZooKeeper(host + ":" + port, new TimeSpan(0, 0, 5), watcher);
                watcher.WaitUntilConnected(TimeSpan.FromSeconds(1));
                List<string> list = new List<string>(
                    zk.GetChildren("/brokers/topics", false));
                list.Sort();
                topics = list;
                FirePropertyChanged("Topics");
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format(
                    Properties.Resources.ExFetchTopicsFailed, ex.Message),
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            finally
            {
                if (zk != null)
                {
                    zk.Dispose();
                }
            }
        }

        private class ConnectedWatcher : IWatcher
        {
            private readonly ManualResetEventSlim connected = new ManualResetEventSlim(false);
            private WatchedEvent evt;

            public void WaitUntilConnected(TimeSpan timeout)
            {
                connected.Wait(timeout);

                if (evt == null) throw new ApplicationException("Bad ZooKepper client state");
                if (evt.State != KeeperState.SyncConnected)
                    throw new ApplicationException("Cannot connect to ZooKeeper");
            }

            public void Process(WatchedEvent e)
            {
                evt = e;
                connected.Set();
            }
        }

        private bool CanFetchTopics()
        {
            return (!string.IsNullOrEmpty(Host));
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as KafkaSettings);
        }

        public bool Equals(KafkaSettings obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return base.Equals(obj) &&
                   Equals(obj.Broker, Broker) &&
                   obj.ZookeeperPort == ZookeeperPort &&
                   Equals(obj.SchemaRegistryServicePort, SchemaRegistryServicePort) &&
                   Equals(obj.UseSchemaRegistry, UseSchemaRegistry);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (!string.IsNullOrEmpty(Host))
            {
                code += 23 * code + Host.GetHashCode();
            }
            code += 23 * code + Port.GetHashCode();
            code += 23 * code + ZookeeperPort.GetHashCode();
            if (!string.IsNullOrEmpty(SchemaRegistryServicePort))
            {
                code += 23 * code + SchemaRegistryServicePort.GetHashCode();
            }
            code += 23 * code + UseSchemaRegistry.GetHashCode();
            return code;
        }

    }
}