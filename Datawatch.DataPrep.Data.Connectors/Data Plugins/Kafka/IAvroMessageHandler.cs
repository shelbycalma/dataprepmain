﻿using Avro.Generic;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaPlugin
{
    public interface IAvroMessageHandler : IMessageHandler
    {
        void MessageReceived(GenericRecord message);
    }
}