﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Kafka.Client.Cfg;
using Kafka.Client.Consumers;
using Kafka.Client.Requests;

namespace Panopticon.KafkaPlugin
{
    public class KafkaHelper
    {
        private static int groupId;
        private const string HTTP = "http://";
        private const string HTTPS = "https://";
        public static ZookeeperConsumerConnector CreateConsumer(string parameterizedBroker)
        {
            ZookeeperConsumerConnector.UseSharedStaticZookeeperClient = false;

            string strGroupId = Interlocked.Increment(ref groupId).ToString();
            try
            {
                ConsumerConfiguration configSettings = new ConsumerConfiguration
                {
                    ZooKeeper = new ZooKeeperConfiguration(parameterizedBroker, 5000, 4000, 200),
                    GroupId = "PanoNet" + strGroupId,
                    AutoOffsetReset = OffsetRequest.SmallestTime,
                    AutoCommit = false,
                    //ConsumerId = topic + "_Thread_" + threadID.ToString(),                    
                };


                ZookeeperConsumerConnector consumer =
                        new ZookeeperConsumerConnector(configSettings, true);

                return consumer;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                return null;
            }
        }
        
        public static string GetParameterizedHost(string host,
            IEnumerable<ParameterValue> parameters)
        {
            host = DataUtils.ApplyParameters(host,
                        parameters);
            if (host.ToLower().StartsWith(HTTP))
            {
                host = host.Substring(HTTP.Length);
            }
            else if (host.ToLower().StartsWith(HTTPS))
            {
                host = host.Substring(HTTPS.Length);
            }

            return host;
        }

        public static bool CanPingHost(string host)
        {
            Ping pinger = new Ping();
            try
            {
                PingReply reply = pinger.Send(host);
                return reply.Status == IPStatus.Success;
            }
            catch (Exception)
            {
            }
            return false;
        }
    }
}