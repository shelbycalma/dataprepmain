﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class RestService
    {
        private static readonly int JSON_PARSE_ERROR_CODE = 50005;

        public static readonly IDictionary<string, string> DEFAULT_REQUEST_PROPERTIES;

        static RestService()
        {
            DEFAULT_REQUEST_PROPERTIES = new Dictionary<string, string>();
            DEFAULT_REQUEST_PROPERTIES.Add("Content-Type", Versions.SCHEMA_REGISTRY_V1_JSON_WEIGHTED);
        }

        private readonly UrlList baseUrls;

        public RestService(UrlList baseUrls)
        {
            this.baseUrls = baseUrls;
        }

        public RestService(List<string> baseUrls) : this(new UrlList(baseUrls))
        {

        }

        public RestService(string baseUrlConfig) : this(ParseBaseUrl(baseUrlConfig))
        {

        }

        /**
         * @param baseUrl           HTTP connection will be established with this url.
         * @param method            HTTP method ("GET", "POST", "PUT", etc.)
         * @param requestBodyData   Bytes to be sent in the request body.
         * @param requestProperties HTTP header properties.
         * @param responseFormat    Expected format of the response to the HTTP request.
         * @param <T>               The type of the deserialized response to the HTTP request.
         * @return The deserialized response to the HTTP request, or null if no data is expected.
         */
        private T SendHttpRequest<T>(string baseUrl, string method, 
            Object requestBodyData, IDictionary<string, string> requestProperties)
        {

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseUrl);
            foreach (KeyValuePair<string, string> pair in requestProperties)
            {
                if (pair.Key == "Content-Type") continue;
                client.DefaultRequestHeaders.Add(pair.Key, pair.Value);
            }

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = null;
            if (method == "GET")
            {
                // List data response.
                response = client.GetAsync("").Result; // Blocking call!
            }
            else if (method == "POST")
            {
                response = client.PostAsJsonAsync("", requestBodyData).Result;
            }


            if (response.IsSuccessStatusCode)
            {
                // Parse the response body. Blocking!
                return response.Content.ReadAsAsync<T>().Result;
            }

            throw new Exception(string.Format("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase));

            //log.debug(string.format("Sending %s with input %s to %s",
            //    method, requestBodyData == null ? "null" : new string(requestBodyData),
            //    baseUrl));

            //HttpURLConnection connection = null;
            //try
            //{
            //    Url url = new Url(baseUrl);
            //    connection = (HttpURLConnection) url.openConnection();
            //    connection.setRequestMethod(method);

            //    // connection.getResponseCode() implicitly calls getInputStream, so always set to true.
            //    // On the other hand, leaving this out breaks nothing.
            //    connection.setDoInput(true);

            //    foreach (KeyValuePair<string, string> entry in requestProperties.GetEnumerator())
            //    {
            //        connection.setRequestProperty(entry.getKey(), entry.getValue());
            //    }

            //    connection.setUseCaches(false);

            //    if (requestBodyData != null)
            //    {
            //        connection.setDoOutput(true);
            //        OutputStream os = null;
            //        try
            //        {
            //            os = connection.getOutputStream();
            //            os.write(requestBodyData);
            //            os.flush();
            //        }
            //        catch (IOException e)
            //        {
            //            log.error("Failed to send HTTP request to endpoint: " + url, e);
            //            throw e;
            //        }
            //        finally
            //        {
            //            if (os != null) os.close();
            //        }
            //    }

            //    int responseCode = connection.getResponseCode();
            //    if (responseCode == HttpURLConnection.HTTP_OK)
            //    {
            //        InputStream is  = connection.getInputStream();
            //        T result = System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<T>(is)

            //    is.
            //        close();
            //        return result;
            //    }
            //    else if (responseCode == HttpURLConnection.HTTP_NO_CONTENT)
            //    {
            //        return null;
            //    }
            //    else
            //    {
            //        InputStream es = connection.getErrorStream();
            //        ErrorMessage errorMessage;
            //        try
            //        {
            //            errorMessage = jsonDeserializer.readValue(es, ErrorMessage.class)
            //            ;
            //        }
            //        catch (JsonProcessingException e)
            //        {
            //            errorMessage = new ErrorMessage(JSON_PARSE_ERROR_CODE, e.getMessage());
            //        }
            //        es.close();
            //        throw new RestClientException(errorMessage.getMessage(), responseCode,
            //            errorMessage.getErrorCode());
            //    }

            //}
            //finally
            //{
            //    if (connection != null)
            //    {
            //        connection.disconnect();
            //    }
            //}
        }

        private T HttpRequest<T>(string path, string method, 
            Object requestBodyData, IDictionary<string, string> requestProperties)
        {
            for (int i = 0, n = baseUrls.Count; i < n; i++)
            {
                string baseUrl = baseUrls.Current();
                try
                {
                    return SendHttpRequest<T>(baseUrl + path, method, 
                        requestBodyData, requestProperties);
                }
                catch (Exception e)
                {
                    baseUrls.Fail(baseUrl);
                    if (i == n - 1) throw e; // Raise the exception since we have no more urls to try
                }
            }
            throw new IOException("Internal HTTP retry error"); // Can't get here
        }

        public SubjectAndSchema LookUpSubjectVersion(string schemaString, string subject)
        {
            RegisterSchemaRequest request = new RegisterSchemaRequest();
            request.Schema = schemaString;
            return LookUpSubjectVersion(request, subject);
        }

        public SubjectAndSchema LookUpSubjectVersion(RegisterSchemaRequest registerSchemaRequest,
            string subject)
        {
            return LookUpSubjectVersion(DEFAULT_REQUEST_PROPERTIES, registerSchemaRequest, subject);
        }

        public SubjectAndSchema LookUpSubjectVersion(
            IDictionary<string, string> requestProperties,
            RegisterSchemaRequest registerSchemaRequest,
            string subject)
        {
            string path = string.Format("/subjects/{0}", subject);

            SubjectAndSchema subjectAndSchema = 
                HttpRequest<SubjectAndSchema>(path, "POST", 
                    registerSchemaRequest, requestProperties);

            return subjectAndSchema;
        }

        //public int registerSchema(string schemaString, string subject)
        //{
        //    RegisterSchemaRequest request = new RegisterSchemaRequest();
        //    request.setSchema(schemaString);
        //    return registerSchema(request, subject);
        //}

        //public int registerSchema(RegisterSchemaRequest registerSchemaRequest, string subject)
        //{
        //    return registerSchema(DEFAULT_REQUEST_PROPERTIES, registerSchemaRequest, subject);
        //}

        //public int registerSchema(IDictionary<string, string> requestProperties,
        //                          RegisterSchemaRequest registerSchemaRequest, string subject)
        //{
        //    string path = string.Format("/subjects/%s/versions", subject);

        //    RegisterSchemaResponse response = HttpRequest(path, "POST",
        //            registerSchemaRequest.toJson().getBytes(), requestProperties, REGISTER_RESPONSE_TYPE);

        //    return response.GetId();
        //}

        //public boolean testCompatibility(string schemaString, string subject, string version)
        //{
        //    RegisterSchemaRequest request = new RegisterSchemaRequest();
        //    request.setSchema(schemaString);
        //    return testCompatibility(request, subject, version);
        //}

        //public boolean testCompatibility(RegisterSchemaRequest registerSchemaRequest,
        //                                 string subject,
        //                                 string version)
        //{
        //    return testCompatibility(DEFAULT_REQUEST_PROPERTIES, registerSchemaRequest,
        //            subject, version);
        //}

        //public boolean testCompatibility(IDictionary<string, string> requestProperties,
        //                                 RegisterSchemaRequest registerSchemaRequest,
        //                                 string subject,
        //                                 string version)
        //{
        //    string path = string.format("/compatibility/subjects/%s/versions/%s", subject, version);

        //    CompatibilityCheckResponse response =
        //            HttpRequest(path, "POST", registerSchemaRequest.toJson().getBytes(),
        //                    requestProperties, COMPATIBILITY_CHECK_RESPONSE_TYPE_REFERENCE);
        //    return response.getIsCompatible();
        //}

        //public ConfigUpdateRequest updateCompatibility(string compatibility, string subject)
        //{
        //    ConfigUpdateRequest request = new ConfigUpdateRequest();
        //    request.setCompatibilityLevel(compatibility);
        //    return updateConfig(request, subject);
        //}

        //public ConfigUpdateRequest updateConfig(ConfigUpdateRequest configUpdateRequest,
        //                                        string subject)
        //{
        //    return updateConfig(DEFAULT_REQUEST_PROPERTIES, configUpdateRequest, subject);
        //}

        ///**
        // *  On success, this api simply echoes the request in the response.
        // */
        //public ConfigUpdateRequest updateConfig(IDictionary<string, string> requestProperties,
        //                                        ConfigUpdateRequest configUpdateRequest,
        //                                        string subject)
        //{
        //    string path = subject != null ? string.format("/config/%s", subject) : "/config";

        //    ConfigUpdateRequest response =
        //            HttpRequest(path, "PUT", configUpdateRequest.toJson().getBytes(),
        //                    requestProperties, UPDATE_CONFIG_RESPONSE_TYPE_REFERENCE);
        //    return response;
        //}

        //public Config getConfig(string subject)
        //{
        //    return getConfig(DEFAULT_REQUEST_PROPERTIES, subject);
        //}

        //public Config getConfig(IDictionary<string, string> requestProperties,
        //    string subject)
        //{
        //    string path = subject != null ? string.format("/config/%s", subject) : "/config";

        //    Config config =
        //        HttpRequest(path, "GET", null, requestProperties, GET_CONFIG_RESPONSE_TYPE);
        //    return config;
        //}

        public SchemaStringElement GetId(int id)
        {
            return GetId(DEFAULT_REQUEST_PROPERTIES, id);
        }

        public SchemaStringElement GetId(IDictionary<string, string> requestProperties,
            int id)
        {
            string path = string.Format("/schemas/ids/{0}", id);

            SchemaStringElement response = 
                HttpRequest<SchemaStringElement>(path, "GET", null, 
                    requestProperties);
            return response;
        }

        public SubjectAndSchema GetVersion(string subject, int version)
        {
            return GetVersion(DEFAULT_REQUEST_PROPERTIES, subject, version);
        }

        public SubjectAndSchema GetVersion(IDictionary<string, string> requestProperties,
            string subject, int version)
        {
            string path = string.Format("/subjects/{0}/versions/{1}", subject, version);

            SubjectAndSchema response = 
                HttpRequest<SubjectAndSchema>(path, "GET", null, requestProperties);
            return response;
        }

        public SubjectAndSchema GetLatestVersion(string subject)
        {
            return GetLatestVersion(DEFAULT_REQUEST_PROPERTIES, subject);
        }

        public SubjectAndSchema GetLatestVersion(IDictionary<string, string> requestProperties,
            string subject)
        {
            string path = string.Format("/subjects/{0}/versions/latest", subject);

            SubjectAndSchema response = HttpRequest<SubjectAndSchema>(path, "GET", null, requestProperties);
            return response;
        }

        public List<int> GetAllVersions(string subject)
        {
            return GetAllVersions(DEFAULT_REQUEST_PROPERTIES, subject);
        }

        public List<int> GetAllVersions(IDictionary<string, string> requestProperties,
            string subject)
        {
            string path = string.Format("/subjects/{0}/versions", subject);

            List<int> response = 
                HttpRequest<List<int>>(path, "GET", null, requestProperties);
            return response;
        }

        public List<string> GetAllSubjects()
        {
            return GetAllSubjects(DEFAULT_REQUEST_PROPERTIES);
        }

        public List<string> GetAllSubjects(IDictionary<string, string> requestProperties)
        {
            List<string> response = 
                HttpRequest<List<string>>("/subjects", "GET", null, requestProperties);
            return response;
        }

        private static List<string> ParseBaseUrl(string baseUrl)
        {
            List<string> baseUrls = new List<string>(Regex.Split(baseUrl, "\\s*,\\s*"));
            if (baseUrls.Count == 0)
            {
                throw new ArgumentException("Missing required schema registry url list");
            }
            return baseUrls;
        }

        public UrlList BaseUrls
        {
            get { return baseUrls; } 
        }

    }
}