﻿namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class SchemaMetadata
    {
        private readonly int id;
        private readonly int version;
        private readonly string schema;

        public SchemaMetadata(int id, int version, string schema)
        {
            this.id = id;
            this.version = version;
            this.schema = schema;

        }

        public int Id
        {
            get { return id; }
        }

        public int Version
        {
            get { return version; }
        }

        public string Schema
        {
            get { return schema; }
        }
    }
}