﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class RegisterSchemaRequest
    {
        private string schema;


        [JsonProperty("schema")]
        public string Schema
        {
            get { return this.schema; }
            set { schema = value; }
        }

        public override bool Equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || GetType() != o.GetType())
            {
                return false;
            }
            if (!base.Equals(o))
            {
                return false;
            }

            RegisterSchemaRequest that = (RegisterSchemaRequest)o;

            if (schema != null ? !schema.Equals(that.schema) : that.schema != null)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int result = base.GetHashCode();
            result = 31 * result + (schema != null ? schema.GetHashCode() : 0);
            return result;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{schema=" + this.schema + "}");
            return sb.ToString();
        }

    }
}