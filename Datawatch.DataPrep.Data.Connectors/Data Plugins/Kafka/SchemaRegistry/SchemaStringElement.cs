﻿using Newtonsoft.Json;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class SchemaStringElement
    {
        private string schema;

        public SchemaStringElement()
        {

        }

        public SchemaStringElement(string schema)
        {
            this.schema = schema;
        }

        [JsonProperty("schema")]
        public string Schema
        {
            get { return schema; }
            set { schema = value; }
        }

    }
}