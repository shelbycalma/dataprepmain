﻿using System.Collections.Generic;
using Avro;
using Kafka.Client.Exceptions;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class CachedSchemaRegistryClient : SchemaRegistryClient
    {
        private readonly RestService restService;
        private readonly int identityIDictionaryCapacity;
        private readonly IDictionary<string, IDictionary<int, Schema>> idCache;
        private readonly IDictionary<string, IDictionary<Schema, int>> versionCache;
        private readonly Dictionary<int, Schema> nullSubjectIdCache;

        public CachedSchemaRegistryClient(string baseUrl, int identityIDictionaryCapacity)
            : this(new RestService(baseUrl), identityIDictionaryCapacity)
        {
        }

        public CachedSchemaRegistryClient(List<string> baseUrls, int identityIDictionaryCapacity)
            : this(new RestService(baseUrls), identityIDictionaryCapacity)
        {
        }

        public CachedSchemaRegistryClient(RestService restService, int identityIDictionaryCapacity)
        {
            this.identityIDictionaryCapacity = identityIDictionaryCapacity;
            this.idCache = new Dictionary<string, IDictionary<int, Schema>>();
            this.versionCache = new Dictionary<string, IDictionary<Schema, int>>();
            this.restService = restService;
            this.nullSubjectIdCache = new Dictionary<int, Schema>();
        }

        private Schema GetSchemaByIdFromRegistry(int id)
        { 
            SchemaStringElement restSchema = restService.GetId(id);
            return Schema.Parse(restSchema.Schema);
        }

        private int GetVersionFromRegistry(string subject, Schema schema)
        {
            SubjectAndSchema response =
                restService.LookUpSubjectVersion(schema.ToString(), subject);
            return response.Version;
        }
        public Schema GetByID(int id)
        {
            lock (this)
            {

                return GetBySubjectAndID(null, id);
            }
        }

        public Schema GetBySubjectAndID(string subject, int id)
        {
            lock (this)
            {
                IDictionary<int, Schema> idSchemaIDictionary;

                if (subject == null)
                {
                    idSchemaIDictionary = nullSubjectIdCache;
                }
                else if (idCache.ContainsKey(subject))
                {
                    idSchemaIDictionary = idCache[subject];
                }
                else
                {
                    idSchemaIDictionary = new Dictionary<int, Schema>();
                    idCache.Add(subject, idSchemaIDictionary);
                }

                if (idSchemaIDictionary.ContainsKey(id))
                {
                    return idSchemaIDictionary[id];
                }

                Schema schema = GetSchemaByIdFromRegistry(id);
                idSchemaIDictionary.Add(id, schema);
                return schema;
            }
        }

        public SchemaMetadata GetLatestSchemaMetadata(string subject)
        {
            lock (this)
            {
                SubjectAndSchema response
                    = restService.GetLatestVersion(subject);
                int id = response.Id;
                int version = response.Version;
                string schema = response.Schema;
                return new SchemaMetadata(id, version, schema);
            }
        }


        public int GetVersion(string subject, Schema schema)
        {
            lock (this)
            {
                IDictionary<Schema, int> schemaVersionIDictionary;
                if (versionCache.ContainsKey(subject))
                {
                    schemaVersionIDictionary = versionCache[subject];
                }
                else
                {
                    schemaVersionIDictionary = new Dictionary<Schema, int>();
                    versionCache.Add(subject, schemaVersionIDictionary);
                }

                if (schemaVersionIDictionary.ContainsKey(schema))
                {
                    return schemaVersionIDictionary[schema];
                }
                else
                {
                    if (schemaVersionIDictionary.Count >= identityIDictionaryCapacity)
                    {
                        throw new IllegalStateException("Too many SubjectAndSchema objects created for " + subject + "!");
                    }
                    int version = GetVersionFromRegistry(subject, schema);
                    schemaVersionIDictionary.Add(schema, version);
                    return version;
                }
            }
        }

        public ICollection<string> GetAllSubjects()
        {
            return restService.GetAllSubjects();
        }

    }
}