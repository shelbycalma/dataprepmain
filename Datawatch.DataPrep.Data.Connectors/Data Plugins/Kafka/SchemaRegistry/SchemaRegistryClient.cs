﻿using System.Collections.Generic;

using Avro;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public interface SchemaRegistryClient
    {
        ICollection<string> GetAllSubjects();

        Schema GetByID(int id) ;

        Schema GetBySubjectAndID(string subject, int id) ;

        SchemaMetadata GetLatestSchemaMetadata(string subject) ;

        int GetVersion(string subject, Schema schema) ;

        //int Register(string subject, SchemaElement SchemaElement) ;
        //bool TestCompatibility(string subject, SchemaElement SchemaElement) ;
        //string UpdateCompatibility(string subject, string compatibility) ;
        //string GetCompatibility(string subject) ;
    }
}