﻿using System;
using System.Text;
using Newtonsoft.Json;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class SubjectAndSchema : IComparable<SubjectAndSchema>
    {
        private string subject;
        private int version;
        private int id;
        private string schema;

        public SubjectAndSchema(string subject, int version, int id, string schema)
        {
            this.subject = subject;
            this.version = version;
            this.id = id;
            this.schema = schema;
        }

        [JsonProperty("subject")]
        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }


        [JsonProperty("version")]
        public int Version
        {
            get { return this.version; }
            set { version = value; }
        }


         [JsonProperty("id")]
        public int Id
        {
            get { return this.id; }
            set { id = value; }
        }


        [JsonProperty("schema")]
        public string Schema
        {
            get { return this.schema; }
            set { schema = value; }
        }


        public override bool Equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if ((o == null) || (this.GetType() != o.GetType()))
            {
                return false;
            }

            SubjectAndSchema that = (SubjectAndSchema)o;

            if (!this.subject.Equals(that.subject))
            {
                return false;
            }
            if (!this.version.Equals(that.version))
            {
                return false;
            }
            if (!this.id.Equals(that.Id))
            {
                return false;
            }
            if (!this.schema.Equals(that.schema))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int result = subject.GetHashCode();
            result = 31 * result + version;
            result = 31 * result + id;
            result = 31 * result + schema.GetHashCode();
            return result;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{subject=" + this.subject + ",");
            sb.Append("version=" + this.version + ",");
            sb.Append("id=" + this.id + ",");
            sb.Append("schema=" + this.schema + "}");
            return sb.ToString();
        }

        public int CompareTo(SubjectAndSchema that)
        {
            int result = this.subject.CompareTo(that.subject);
            if (result != 0)
            {
                return result;
            }
            result = this.version - that.version;
            return result;
        }
    }
}