﻿using System;
using System.Collections.Generic;
using System.Threading;
using ZooKeeperNet;

namespace Panopticon.KafkaPlugin.SchemaRegistry
{
    public class UrlList
    {
        private long index;
        private readonly IList<string> urls;

        public UrlList(IList<string> urls)
        {
            if (urls == null || urls.IsEmpty())
            {
                throw new ArgumentException("Expected at least one URL to be passed in constructor");
            }

            this.urls = new List<String>(urls);
            Interlocked.Exchange(ref index, new Random().Next(urls.Count));
        }

        public UrlList(String url) : this(new List<string>(new string[] { url }))
        {

        }

        /**
         * Get the current url
         * @return the url
         */
        public string Current()
        {
            return urls[(int)Interlocked.Read(ref index)];
        }

        /**
         * Declare the given url as failed. This will cause the urls to
         * rotate, so that the next request will be done against a new url
         * (if one exists).
         * @param url the url that has failed
         */
        public void Fail(String url)
        {
            int currentIndex = (int)Interlocked.Read(ref index);
            if (urls[currentIndex].Equals(url))
            {
                Interlocked.Exchange(ref index, (currentIndex + 1) % urls.Count);
            }
        }

        /**
         * The number of unique urls contained in this collection.
         * @return the count of urls
         */
        public int Count
        {
            get { return urls.Count; }
        }

        public string toString()
        {
            return urls.ToString();
        }
    }
}