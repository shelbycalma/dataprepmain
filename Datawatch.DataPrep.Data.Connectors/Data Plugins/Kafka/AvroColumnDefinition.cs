﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaPlugin
{
    public class AvroColumnDefinition : ColumnDefinition
    {
        public AvroColumnDefinition(PropertyBag bag)
            :base(bag)
        {
        }

        public string LogicalType
        {
            get { return GetInternal("LogicalType"); }
            set { SetInternal("LogicalType", value);}
        }

        public override bool IsValid
        {
            get
            {
                return true;
            }
        }
    }
}
