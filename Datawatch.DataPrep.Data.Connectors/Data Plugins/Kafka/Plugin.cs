﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.KafkaPlugin
{
    /// <summary>
    /// DataPlugin implementation for for Apache Kafka.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin :
        MessageQueuePluginBase<ParameterTable, KafkaSettings,
            Dictionary<string, object>, KafkaDataAdapter>
    {
        internal const string PluginId = "KafkaPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Kafka";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : this(PluginId, PluginTitle)
        {
        }

        public Plugin(string Id, string title)
            : base(Id, title)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override KafkaSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new KafkaSettings(pluginManager, new PropertyBag(),
                parameters);
        }

        public override KafkaSettings CreateConnectionSettings(
            PropertyBag bag)
        {
            return new KafkaSettings(pluginManager, bag);
        }

        public KafkaSettings CreateConnectionSettings(
             PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            return new KafkaSettings(pluginManager, bag, parameters);
        }
    }
}