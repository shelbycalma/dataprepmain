﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Avro.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.KafkaPlugin
{
    public class KafkaDataAdapter :
        MessageQueueAdapterBase<ParameterTable,
            KafkaSettings, Dictionary<string, object>>, IAvroMessageHandler
    {
        private Task task;
        private KafkaConsumer consumer;

        public KafkaDataAdapter()
        {
        }

        public override void Start()
        {
            settings.Parameters = table.Parameters;
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();

            consumer = new KafkaConsumer(this, settings);
            task = new Task(consumer.StartConsuming);
            task.Start();
        }

        public override void Stop()
        {
            consumer.Stop();
            task = null;
            base.Stop();
        }

        public void MessageReceived(GenericRecord record)
        {
            if (!(parser is IAvroParser)) return;
            DataPluginUtils.EnqueueMessage(((IAvroParser)parser).Parse(record), 
                idColumn, updater);
        }
    }
}