﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.RMDSPlugin.UI
{
    internal partial class ConfigWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindow));

        public ConfigWindow(RMDSSettingsViewModel settings)
        {
            InitializeComponent();
            connectionPanel.Settings = settings;            
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = connectionPanel.Settings == null ? false: 
                connectionPanel.Settings.IsOK;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
