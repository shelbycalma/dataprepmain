﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.RMDSPlugin.UI.dll")]
[assembly: AssemblyDescription("Datawatch RMDS Plug-in UI")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription("Thomson Reuters TREP-RT", "Streaming")]
