﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.UI;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Utils = Datawatch.DataPrep.Data.Core.UI.Utils;

namespace Panopticon.RMDSPlugin.UI
{
    internal partial class ConfigPanel : UserControl
    {
        private ICommand fetchSchemaCommand;

        /// <summary>
        /// Creates a new instance of the ConfigPanel.
        /// </summary>
        public ConfigPanel()
            : this(null)
        {
        }

        public ConfigPanel(RMDSSettingsViewModel settings)
        {
            InitializeComponent();
            DataContext = this.Settings = settings;
        }
                
        public RMDSSettingsViewModel Settings
        {
            get { return (RMDSSettingsViewModel)this.DataContext; }
            set { this.DataContext = value; }
        }

        private void AvailableFieldMouseDoubleClick(object sender,
            MouseButtonEventArgs e)
        {
            ListBoxItem item = (ListBoxItem) sender;
            if (item.IsSelected)
            {
                Settings.AddSelectedField((RMDSField)item.Content);
            }
        }
        
        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Settings.DeleteSelectedField((RMDSField)e.Parameter);
        }

        private void DeleteSelectedColumns_Executed(object sender,
            ExecutedRoutedEventArgs e)
        {
            Settings.DeleteAllSelectedFields();
        }

        private void SelectAllColumns_Executed(object sender,
            ExecutedRoutedEventArgs e)
        {
            Settings.SelectAllFields();
        }

        public ICommand FetchSchemaCommand
        {
            get
            {
                if (fetchSchemaCommand == null)
                {
                    fetchSchemaCommand = new DelegateCommand(FetchSchema,
                        CanFetchSchema);
                }
                return fetchSchemaCommand;
            }
        }

        private void FetchSchema()
        {
            try
            {
                Settings.LoadAvailableFieldList();
            }
            catch (Exception exc)
            {
                Log.Exception(exc);

                MessageBoxEx.Show(Utils.GetOwnerWindow(this), 
                    exc.Message, Properties.Resources.UiWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanFetchSchema()
        {
            return Settings != null && Settings.CanFetchSchema();
        }
    }
}
