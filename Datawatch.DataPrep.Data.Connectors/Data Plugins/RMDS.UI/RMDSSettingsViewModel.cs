﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.RMDSPlugin.UI
{
    public class RMDSSettingsViewModel : ViewModelBase
    {
        private readonly IEnumerable<ParameterValue> parameters;
        private List<RMDSField> availableFieldList;
        private ObservableCollection<RMDSField> selectedFieldList;
        private string columnFilter;
        private ICollectionView columnView;

        public RMDSSettingsViewModel(RMDSSettings model,
            IEnumerable<ParameterValue> parameters)
        {
            this.Model = model;
            this.parameters = parameters;
            UpdateTimeIdCandidates();
        }

        public RMDSSettings Model { get; private set; }

        public List<RMDSField> AvailableFieldList
        {
            get { return availableFieldList; }
        }

        public ObservableCollection<RMDSField> SelectedFieldList
        {
            get
            {
                if (selectedFieldList == null)
                {
                    RMDSField[] selectedColumns = Model.SelectedColumns;

                    selectedFieldList = 
                        new ObservableCollection<RMDSField>(selectedColumns);
                }
                return selectedFieldList;
            }
        }

        private bool columnView_Filter(object item)
        {
            if (string.IsNullOrEmpty(columnFilter))
            {
                return true;
            }
            RMDSField column = item as RMDSField;
            if (column == null)
            {
                return false;
            }
            bool visible =
                column.Name.IndexOf(columnFilter,
                    StringComparison.InvariantCultureIgnoreCase) >= 0 ||
                column.LongName.IndexOf(columnFilter,
                    StringComparison.InvariantCultureIgnoreCase) >= 0;

            return visible;
        }

        public string ColumnFilter
        {
            get { return columnFilter; }
            set
            {
                if (value != columnFilter)
                {
                    columnFilter = value;
                    OnPropertyChanged("ColumnFilter");
                }
            }
        }

        public bool IsOK
        {
            get
            {
                if (this.Model == null)
                {
                    return false;
                }
                return CanFetchSchema() &&
                    !(string.IsNullOrEmpty(Model.IdColumn)) &&
                    Model.SelectedColumns.Length > 0;

            }
        }

        internal void AddSelectedField(RMDSField field)
        {
            if (field == null)
            {
                return;
            }
            if (selectedFieldList == null)
            {
                selectedFieldList = new ObservableCollection<RMDSField>();
            }

            if (selectedFieldList.Where(selectedcol =>
                selectedcol.Name.Equals(
                field.Name)).FirstOrDefault() != null)
            {
                return;
            }

            selectedFieldList.Add(field);
            Model.SelectedColumns = selectedFieldList.ToArray();
            UpdateTimeIdCandidates();
        }

        internal void DeleteSelectedField(RMDSField field)
        {
            if (field == null)
            {
                return;
            }
            if (selectedFieldList.Contains(field))
            {
                selectedFieldList.Remove(field);
            }
            Model.SelectedColumns = selectedFieldList.ToArray();
            UpdateTimeIdCandidates();
        }

        internal void DeleteAllSelectedFields()
        {
            selectedFieldList.Clear();
            Model.SelectedColumns = selectedFieldList.ToArray();
            UpdateTimeIdCandidates();
        }

        internal void SelectAllFields()
        {
            foreach (RMDSField field in columnView)
            {
                if (!selectedFieldList.Contains(field))
                {
                    selectedFieldList.Add(field);
                }
            }
            Model.SelectedColumns = selectedFieldList.ToArray();
            UpdateTimeIdCandidates();
        }

        internal bool CanFetchSchema()
        {            
            return !(string.IsNullOrEmpty(Model.Host) ||
                     string.IsNullOrEmpty(Model.Port) ||
                     string.IsNullOrEmpty(Model.Service) ||
                     string.IsNullOrEmpty(Model.Symbol));
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == "AvailableFieldList")
            {
                if (columnView != null)
                {
                    columnView.Filter = null;
                    columnView = null;
                }
                if (availableFieldList != null)
                {
                    columnView = CollectionViewSource.GetDefaultView(
                        availableFieldList);
                    columnView.Filter = columnView_Filter;
                }
            }
            else if (propertyName == "ColumnFilter")
            {
                if (columnView != null)
                {
                    columnView.Refresh();
                }
            }

        }

        internal void LoadAvailableFieldList()
        {
            availableFieldList = null;
            OnPropertyChanged("AvailableFieldList");

            availableFieldList = Util.GetSchema(Model, parameters);
            OnPropertyChanged("AvailableFieldList");
        }

        private void UpdateTimeIdCandidates()
        {
            List<string> timeIdCandidates = new List<string>();

            foreach (RMDSField field in Model.SelectedColumns)
            {
                Column column = field.GetStandaloneColumn();
                if (column is TimeColumn)
                {
                    timeIdCandidates.Add(column.Name);
                }
            }

            this.Model.TimeIdColumnCandidates =
                timeIdCandidates.Count > 0 ? timeIdCandidates.ToArray() : null;
        }
    }
}
