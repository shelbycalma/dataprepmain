﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.RMDSPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, RMDSSettings>
    {
        private const string imageUri = "pack://application:,,,/" +
            "Panopticon.RMDSPlugin.UI;component/rmds-plugin.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            RMDSSettings rmdsSettings = new RMDSSettings(bag);
            RMDSSettingsViewModel vm = new RMDSSettingsViewModel(rmdsSettings,
                parameters);
            ConfigPanel cp = new ConfigPanel(vm);
            return cp;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            RMDSSettingsViewModel vm = cp.Settings;
            return vm.Model.ToPropertyBag();
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            RMDSSettings rmdsSettings = new RMDSSettings();
            RMDSSettingsViewModel settingsView =
                new RMDSSettingsViewModel(rmdsSettings, parameters);
            ConfigWindow window = new ConfigWindow(settingsView);
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return rmdsSettings.ToPropertyBag();
            }
            return null;
        }
    }
}
