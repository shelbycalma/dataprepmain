﻿
namespace Panopticon.ODataPlugin
{
    public enum ODataFieldType
    {
        DateTimeOffset,
        Time,
        SByte,
        Byte,
        Binary,
        Boolean,
        DateTime,
        Single,
        Double,
        Decimal,
        Int32,
        Int64,
        Int16,
        Guid,
        String,
        ComplexType
    }
}
