﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Panopticon.ODataPlugin
{
    public class ODataParameter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ODataField field;
        private ODataQueryOperator queryOperator;
        private string parameterValue;
        private string selectedFieldName;
        private int selectedQueryOperatorId;

        public ODataField Field
        {
            get
            {
                return field;   
            }
            set
            {
                field = value;
                FirePropertyChanged("QueryOperatorList");
            }
        }

        public ODataQueryOperator QueryOperator 
        {
            get
            {
                return queryOperator;
            }
            set
            {
                queryOperator = value;
            }
        }

        public List<ODataQueryOperator> QueryOperatorList
        {
            get
            {
                return ODataQueryOperator.GetQueryOperators(field);
            }

        }
        
        public string ParameterValue
        {
            get
            {
                return parameterValue;
            }
            set
            {
                parameterValue = value;
                FirePropertyChanged("ParameterValue");
                
            }
        }

        public string SelectedFieldName
        {
            get
            {
                return selectedFieldName;
            }
            set
            {
                if (value != null && selectedFieldName != value)
                {
                    selectedFieldName = value;
                    FirePropertyChanged("SelectedFieldName");
                }
            }
        }

        public int SelectedQueryOperatorId
        {
            get
            {
                return selectedQueryOperatorId;
            }
            set
            {
                if (selectedQueryOperatorId != value)
                {
                    selectedQueryOperatorId = value;
                    FirePropertyChanged("SelectedQueryOperatorId");
                }
            }
        }

        public bool IsValidForQuery
        {
            get
            {
                if (Field != null && QueryOperator != null &&
                string.IsNullOrEmpty(ParameterValue) == false)
                {
                    return true;
                }
                return false;
            }
        }

        protected void FirePropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
    }
}
