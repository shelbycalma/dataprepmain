﻿using System;
using System.Text;
using System.Collections.Generic;

namespace Panopticon.ODataPlugin
{
    public class ODataQueryBuilder
    {
        public static string GetQuery(string BaseCollectionListURL, string Collection,
            List<ODataParameter> parameters)
        {
            if (string.IsNullOrEmpty(Collection))
            {
                return string.Empty;
            }

            StringBuilder odataQuery = new StringBuilder();
            odataQuery.Append(string.Format("{0}/{1}?$inlinecount=allpages",
                BaseCollectionListURL.TrimEnd('/'), Collection));
            
            StringBuilder filter = new StringBuilder();
            foreach (ODataParameter parameter in parameters)
            {
                if (parameter == null || parameter.IsValidForQuery == false)
                {
                    continue;
                }

                string parameterValue = parameter.ParameterValue;

                if (parameter.Field.FieldType == ODataFieldType.DateTime)
                {
                    DateTime dtValue;
                    if (DateTime.TryParse(parameterValue, out dtValue))
                    {
                        parameterValue = dtValue.ToString("yyyy-MM-ddThh:mm:ss");
                    }
                    else
                    {
                        //if fieldtype is datetime and value couldn't be parsed
                        //then skip the parameter
                        continue;
                    }
                }

                if (filter.Length > 0)
                {
                    filter.Append(" and ");
                }             

                filter.Append(string.Format(
                    parameter.QueryOperator.QueryExpression,
                    parameter.Field.FieldName, parameterValue));
            }

            if (filter.Length > 0)
            {
                odataQuery.Append("&$filter=");
                odataQuery.Append(filter);
            }
            return odataQuery.ToString();
        }
    }
}
