﻿
using System;

namespace Panopticon.ODataPlugin
{
    public class ODataField : IEquatable<ODataField>
    {
        public string FieldName { get; set; }
        public ODataFieldType FieldType { get; set; }
        public string ParentFieldName { get; set; } // parent name for complex type
        public string ChildFieldName { get; set; }
        public bool IsNumericField
        {
            get
            {
                if (FieldType == ODataFieldType.Int16 ||
                    FieldType == ODataFieldType.Int32 ||
                    FieldType == ODataFieldType.Int64 ||
                    FieldType == ODataFieldType.Decimal ||
                    FieldType == ODataFieldType.Double ||
                    FieldType == ODataFieldType.Single ||
                    FieldType == ODataFieldType.SByte ||
                    FieldType == ODataFieldType.Byte)
                {
                    return true;
                }

                return false;
            }
        }

        public bool Equals(ODataField other)
        {
            if (this.FieldName == other.FieldName)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
