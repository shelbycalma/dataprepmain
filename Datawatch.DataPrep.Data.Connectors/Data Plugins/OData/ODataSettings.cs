﻿using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.ODataPlugin
{
    public class ODataSettings : ConnectionSettings
    {
        string defaultCollectionListURL = "http://";
        private NumericDataHelper numericDataHelper;
        private const string NumericDataHelperKey = "NumericDataHelper";
        public ODataSettings()
            : this(new PropertyBag())
        {
        }

        public ODataSettings(PropertyBag properties)
            : base(properties)
        {
            PropertyBag numericDataHelperBag =
                properties.SubGroups[NumericDataHelperKey];
            if (numericDataHelperBag == null)
            {
                numericDataHelperBag = new PropertyBag();
                properties.SubGroups[NumericDataHelperKey] = numericDataHelperBag; 
            }

            numericDataHelper = new NumericDataHelper(numericDataHelperBag);
        }

        public string CollectionListURL
        {
            get { return GetInternal("CollectionListURL",
                defaultCollectionListURL); }
            set { SetInternal("CollectionListURL", value); }
        }

        public string UserId
        {
            get { return GetInternal("UserId"); }
            set { SetInternal("UserId", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string Collection
        {
            get { return GetInternal("Collection"); }
            set { SetInternal("Collection", value); }
        }

        public int RequestTimeout
        {
            get { return GetInternalInt("RequestTimeout", 10); }
            set { SetInternalInt("RequestTimeout", value); }
        }

        public string ODataQuery
        {
            get { return GetInternal("ODataQuery"); }
            set { SetInternal("ODataQuery", value); }
        }

        public string BaseCollectionListURL
        {
            get { return GetInternal("BaseCollectionListURL", CollectionListURL); }
            set { SetInternal("BaseCollectionListURL", value); }
        }

        public string DefaultCollectionListURL
        {
            get { return defaultCollectionListURL; }
        }

        public NumericDataHelper NumericDataHelper
        {
            get
            {
                return numericDataHelper;
            }
        }

        public int GetParameterCount()
        {
            return GetInternalInt("ODataParameterCount", 0);
        }

        public ODataParameter[] SavedODataParameters
        {
            get
            {
                return GetODataParameters();
            }
            set
            {
                SetODataParameters(value);
            }
        }

        public bool IsMetadataSupported
        {
            get
            {
                string value = GetInternal("IsMetadataSupported",
                  true.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("IsMetadataSupported", value.ToString()); }
        }

        private ODataParameter[] GetODataParameters()
        {
            int count = GetInternalInt("ODataParameterCount", 0);

            ODataParameter[] parameters = new ODataParameter[count];
            for (int i = 0; i < count; i++)
            {
                parameters[i] = GetODataParameter(i);

            }
            return parameters;
        }

        private void SetODataParameters(ODataParameter[] parameters)
        {
            if (parameters == null)
            {
                throw Exceptions.ArgumentNull("parameters");
            }
            ClearParameters();
            int count = parameters.Length;
            SetInternalInt("ODataParameterCount", count);
            for (int i = 0; i < count; i++)
            {
                SetParameter(i, parameters[i]);
            }
        }

        private ODataParameter GetODataParameter(int index)
        {
            string fieldName = GetInternal("ParameterFieldName" + index);
            int operatorId =
                GetInternalInt("ParameterOperatorId" + index,1);
            string parameterValue = GetInternal("ParameterValue" + index);
            ODataParameter parameter = new ODataParameter()
            {
                SelectedFieldName = fieldName,
                SelectedQueryOperatorId = operatorId,
                ParameterValue = parameterValue
            };
            
            return parameter;
        }

        protected void SetParameter(int index, ODataParameter parameter)
        {
            if (parameter == null)
            {
                return;
            }
            if (parameter.Field != null )
            {
                SetInternal("ParameterFieldName" + index,
                    parameter.Field.FieldName);
            }
            if (parameter.QueryOperator != null)
            {
                SetInternalInt("ParameterOperatorId" + index,
                    parameter.QueryOperator.QueryOperatorId);
            }
            if (!string.IsNullOrEmpty(parameter.ParameterValue))
            {
                SetInternal("ParameterValue" + index, parameter.ParameterValue);
            }
        }

        private void ClearParameters()
        {
            int count = GetInternalInt("ODataParameterCount", 0);
            for (int i = 0; i < count; i++)
            {
                SetInternal("ParameterFieldName" + i, null);
                SetInternalInt("ParameterOperatorId" + i, 0);
                SetInternal("ParameterValue" + i, null);
            }
            SetInternalInt("ODataParameterCount", 0);
        }
    }
}
