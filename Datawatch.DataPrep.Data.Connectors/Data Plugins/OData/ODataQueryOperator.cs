﻿using System.Collections.Generic;

namespace Panopticon.ODataPlugin
{
    public class ODataQueryOperator
    {
        public int QueryOperatorId { get; set; }
        public string DisplayText { get; set; }
        public string QueryExpression { get; set; }
        
        public static List<ODataQueryOperator> StringOperators
        {
            get
            {
                return new List<ODataQueryOperator>()
                {
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=1,
                        DisplayText = "Contains",
                        QueryExpression = "substringof('{1}',{0})"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=2,
                        DisplayText = "StartsWith",
                        QueryExpression = "startswith({0},'{1}')"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=3,
                        DisplayText = "Equals",
                        QueryExpression = "{0} eq '{1}'"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=4,
                        DisplayText = "EndsWith",
                        QueryExpression = "endswith({0},'{1}')"
                    }
                };
            }
        }

        public static List<ODataQueryOperator> NumericOperators
        {
            get
            {
                return new List<ODataQueryOperator>()
                {
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=1,
                        DisplayText = "<",
                        QueryExpression = "{0} lt {1}"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=2,
                        DisplayText = "<=",
                        QueryExpression = "{0} le {1}"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=3,
                        DisplayText = "=",
                        QueryExpression = "{0} eq {1}"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=4,
                        DisplayText = "=>",
                        QueryExpression = "{0} ge {1}"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=5,
                        DisplayText = ">",
                        QueryExpression = "{0} gt {1}"
                    }
                };
            }
        }

        public static List<ODataQueryOperator> DateOperators
        {
            get
            {
                return new List<ODataQueryOperator>()
                {
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=1,
                        DisplayText = "<",
                        QueryExpression = "{0} lt datetime'{1}'"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=2,
                        DisplayText = "<=",
                        QueryExpression = "{0} le datetime'{1}'"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=3,
                        DisplayText = "=",
                        QueryExpression = "{0} eq datetime'{1}'"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=4,
                        DisplayText = "=>",
                        QueryExpression = "{0} ge datetime'{1}'"
                    },
                    new ODataQueryOperator()
                    {
                        QueryOperatorId=5,
                        DisplayText = ">",
                        QueryExpression = "{0} gt datetime'{1}'"
                    }
                };
            }
        }

        public static List<ODataQueryOperator> GetQueryOperators(ODataField field)
        {
            if (field == null)
            {
                return null;
            }
            if (field.IsNumericField)
            {
                return ODataQueryOperator.NumericOperators;
            }
            else if (field.FieldType == ODataFieldType.DateTime)
            {
                return ODataQueryOperator.DateOperators;
            }
            else
            {
                return ODataQueryOperator.StringOperators;
            }
        }
    }
}
