﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.ODataPlugin
{
    public static class Util
    {
        private static readonly Dictionary<string, ODataFieldType> edmTypes =
            new Dictionary<string, ODataFieldType>
	        {
                {"Edm.DateTimeOffset", ODataFieldType.DateTimeOffset},
                {"Edm.Time", ODataFieldType.Time},
	            {"Edm.SByte", ODataFieldType.SByte},	
                {"Edm.Byte", ODataFieldType.Byte},	
                {"Edm.Binary", ODataFieldType.Binary},
	        	{"Edm.Boolean", ODataFieldType.Boolean},
	        	{"Edm.DateTime", ODataFieldType.DateTime},
	        	{"Edm.Single", ODataFieldType.Single},
	        	{"Edm.Double", ODataFieldType.Double},
	        	{"Edm.Decimal", ODataFieldType.Double},
	        	{"Edm.Int32", ODataFieldType.Int32},
	        	{"Edm.Int64", ODataFieldType.Int64},
	        	{"Edm.Int16", ODataFieldType.Int16},
	        	{"Edm.Guid", ODataFieldType.Guid},
	        	{"Edm.String", ODataFieldType.String}
	        };

        public static ODataFieldType ConvertEdmTypeToCsharpType(string edmType)
        {
            if (edmTypes.ContainsKey(edmType))
            {
                ODataFieldType eType = edmTypes[edmType];
                return eType;
            }
            else
            {
                return ODataFieldType.ComplexType;
            }
        }

        public static string GetRetryURL(string sourceURL)
        {
            //some URL doesn't support $inlinecount=allpages
            //retry after remove that.

            if (sourceURL.Contains('?'))
            {
                string[] urlParts = sourceURL.Split('?');

                NameValueCollection queryString =
                    System.Web.HttpUtility.ParseQueryString(
                    urlParts[1]);

                queryString.Remove("$inlinecount");

                return urlParts[0] + "?" + Uri.UnescapeDataString(
                    queryString.ToString());
            }

            return string.Empty;
        }

        public static int GetTopFromURL(string sourceURL)
        {
            int top = -1;

            if (sourceURL.Contains('?'))
            {
                string[] urlParts = sourceURL.Split('?');

                NameValueCollection queryString =
                    System.Web.HttpUtility.ParseQueryString(
                    urlParts[1]);
                if (queryString["$top"] != null)
                {
                    int.TryParse(queryString["$top"].ToString(), out top);
                }

            }

            return top;
        }

        public static string GetReloadURL(string sourceURL, int top, int skip)
        {
            string resultURL = sourceURL;

            if (resultURL.Contains('?'))
            {
                string[] urlParts = resultURL.Split('?');

                NameValueCollection queryString =
                    System.Web.HttpUtility.ParseQueryString(
                    urlParts[1]);

                queryString["$top"] = top.ToString();
                queryString["$skip"] = skip.ToString();

                resultURL = urlParts[0] + "?" + Uri.UnescapeDataString(
                    queryString.ToString());
            }
            else
            {
                resultURL += "?&$top=" + top + "&$skip=" + skip;
            }

            return resultURL;
        }

        public static XDocument GetXMLDoc(int requestTimeOut, string username,
            string password, string serviceUri)
        {
            StreamReader xmlStream = null;

            string URL = serviceUri;

            HttpWebRequest httpRequest =
                (HttpWebRequest)WebRequest.Create(URL);

            //set the timeout
            httpRequest.Timeout = requestTimeOut * 1000;
            //set cache policy to none
            HttpRequestCachePolicy noCachePolicy =
                new HttpRequestCachePolicy(
                    HttpRequestCacheLevel.NoCacheNoStore);
            httpRequest.CachePolicy = noCachePolicy;

            //get URI credential
            httpRequest.Credentials = GetAppropriateCredential(username, password);

            using (HttpWebResponse webResponse =
                (HttpWebResponse)httpRequest.GetResponse())
            {

				XmlReaderSettings settings = new XmlReaderSettings();
				settings.XmlResolver = null;
				
				//create stream object from webresponse
				xmlStream = new StreamReader(
                    webResponse.GetResponseStream(), UTF8Encoding.Default);
				XmlReader reader = XmlTextReader.Create(xmlStream, settings);
				return XDocument.Load(reader);
            }

        }

        public static NetworkCredential GetAppropriateCredential(
             string username, string password)
        {
            //apply credentials if userid/password not empty
            if (!(string.IsNullOrEmpty(username) ||
                    string.IsNullOrEmpty(password)))
            {

                //check if user has entered domain name like domain\username
                string[] userDomain = null;

                if (username.Contains(@"\") &&
                    username.Length > 2)
                {
                    userDomain = username.Split('\\');
                }

                if (userDomain != null && userDomain.Length > 0)
                {
                    return new
                            NetworkCredential(userDomain[1],
                            password, userDomain[0]);
                }
                else
                {
                    return new
                        NetworkCredential(username, password);
                }

            }
            return null;

        }

        public static string ApplyParameters(string query,
            IEnumerable<ParameterValue> parameters)
        {
            if (parameters == null) return query;
            return new ParameterEncoder
            {
                SourceString = query,
                Parameters = parameters,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }
    }
}
