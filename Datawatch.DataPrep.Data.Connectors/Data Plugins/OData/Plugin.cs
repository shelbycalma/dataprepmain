﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

using Panopticon.ODataPlugin.Properties;

namespace Panopticon.ODataPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ODataSettings>
    {
        internal const string PluginId = "ODataPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "OData";
        internal const string PluginType = DataPluginTypes.File;

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public override ODataSettings CreateSettings(PropertyBag properties)
        {
            ODataSettings settings = new ODataSettings(properties);
            if (string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = Resources.UiDefaultConnectionTitle;
            }
            return settings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();
            DateTime start = DateTime.Now;

            ODataSettings settings = CreateSettings(properties);
            ODataTableBuilder tableBuilder = new ODataTableBuilder(settings);
            StandaloneTable table = tableBuilder.CreateTable(parameters,
                maxRowCount);

            if (table == null)
            {
                return null;
            }

            // We're creating a new StandaloneTable on every request, so allow
            // EX to freeze it.
                table.BeginUpdate();
            try {
                table.CanFreeze = true;
            } finally {
                table.EndUpdate();
            }
            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Properties.Resources.LogQueryCompleted,
                table.RowCount, table.ColumnCount, seconds);
            return table;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
