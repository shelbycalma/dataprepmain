﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.ODataPlugin
{
    public class ODataTableBuilder
    {
        private ODataSettings settings;

        private const string metaNamespace =
                   "http://schemas.microsoft.com/ado/2007/08/dataservices/metadata";

        public ODataTableBuilder(ODataSettings settings)
        {
            this.settings = settings;
        }

        public List<string> GetCollections(out string serviceBaseURL)
        {
            serviceBaseURL = string.Empty;

            if (String.IsNullOrEmpty(settings.CollectionListURL))
            {
                return null;
            }
            List<string> collectionsList = new List<string>();
            XDocument xDoc = Util.GetXMLDoc(settings.RequestTimeout,
                settings.UserId, settings.Password, settings.CollectionListURL);

            XElement service =
               (from c in xDoc.Descendants()
                where c.Name.LocalName == "service"
                select c).FirstOrDefault();

            if (service == null)
            {
                return collectionsList;
            }

            XAttribute baseAttribute = service.Attributes()
            .FirstOrDefault(a => a.Name.LocalName == "base");

            if (baseAttribute == null)
            {
                return collectionsList;
            }

            serviceBaseURL = baseAttribute.Value;

            IEnumerable<XElement> collections =
                (from c in xDoc.Descendants()
                 where c.Name.LocalName == "collection"
                 select c);

            var titles = from d in collections.Descendants()
                         where d.Name.LocalName == "title"
                         select d;
            foreach (var title in titles)
            {
                collectionsList.Add(title.Value);
            }
            return collectionsList;
        }

        public bool GetFields(List<ODataField> fieldList,
            List<string> navList)
        {

            if (String.IsNullOrEmpty(settings.BaseCollectionListURL) ||
                String.IsNullOrEmpty(settings.Collection))
            {
                return false;
            }

            string metadataURL = string.Format(
                settings.BaseCollectionListURL.TrimEnd('/') + "/$metadata");
            XDocument xDoc = Util.GetXMLDoc(settings.RequestTimeout,
                settings.UserId, settings.Password, metadataURL);

            XElement selectedEntity =
                (from c in xDoc.Descendants()
                 where c.Name.LocalName == "EntityType" &&
                 c.Attribute("Name").Value == settings.Collection
                 select c).FirstOrDefault();

            //if selected entity couldn't be found in EntityType
            //try looking at entitySet
            if (selectedEntity == null ||
                selectedEntity.Elements().Count() == 0)
            {
                IEnumerable<XElement> entityContainer =
                (from c in xDoc.Descendants()
                 where c.Name.LocalName == "EntitySet"
                 select c);

                int entityIndex = -1;
                string entityTypeName = "";
                foreach (var element in entityContainer)
                {
                    entityIndex++;
                    if (element.Name.LocalName == "EntitySet" &&
                        element.Attribute("Name").Value == settings.Collection)
                    {
                        entityTypeName = element.Attribute(
                            "EntityType").Value;
                        break;
                    }
                }
                if (entityTypeName.Length <= 0)
                {
                    throw new Exception(Properties.Resources.ExErrorMetada);
                }

                if (entityTypeName.Contains('.'))
                {
                    entityTypeName = entityTypeName.Split('.').Last();
                }
                //re-search the Selected entity type name

                int selectedEntityCount =
                (from c in xDoc.Descendants()
                 where c.Name.LocalName == "EntityType" &&
                 c.Attribute("Name").Value == entityTypeName
                 select c).Count();

                //if there are multiple entities having same name try using index
                if (selectedEntityCount > 1)
                {
                    selectedEntity =
                    (from c in xDoc.Descendants()
                     where c.Name.LocalName == "EntityType" &&
                     c.Attribute("Name").Value == entityTypeName
                     select c).ElementAt(entityIndex);
                }
                else
                {
                    selectedEntity =
                    (from c in xDoc.Descendants()
                     where c.Name.LocalName == "EntityType" &&
                     c.Attribute("Name").Value == entityTypeName
                     select c).FirstOrDefault();
                }

            }

            //if selected entity is still null, throw exception

            if (selectedEntity == null ||
                selectedEntity.Elements().Count() == 0)
            {
                throw new Exception(Properties.Resources.ExErrorMetada);
            }
            IEnumerable<XElement> ComplexEntities =
                from c in xDoc.Descendants()
                where c.Name.LocalName == "ComplexType"
                select c;

            var fieldElements = from d in selectedEntity.Descendants()
                                where d.Name.LocalName == "Property"
                                select d;
            var navProperties = from d in selectedEntity.Descendants()
                                where d.Name.LocalName == "NavigationProperty"
                                select d;

            foreach (var element in fieldElements)
            {
                string fieldName = element.Attribute("Name").Value;
                string fieldTypeName = element.Attribute("Type").Value;
                ODataFieldType fieldType = Util.ConvertEdmTypeToCsharpType(
                    fieldTypeName);

                if (fieldType == ODataFieldType.ComplexType)
                {
                    foreach (var complexElement in ComplexEntities)
                    {
                        string complexTypeName = complexElement.Attribute(
                            "Name").Value;
                        //TODO: May be some better way to do this
                        string tempFieldTypeName = fieldTypeName;

                        if (tempFieldTypeName.Contains('.'))
                        {
                            tempFieldTypeName = fieldTypeName.Split('.').Last();
                        }
                        if (complexTypeName == tempFieldTypeName)
                        {
                            var complexProperties =
                                from d in complexElement.Descendants()
                                where d.Name.LocalName == "Property"
                                select d;
                            foreach (var complexProp in complexProperties)
                            {
                                string cFieldName = complexProp.Attribute(
                                    "Name").Value;
                                string cFieldTypeName = complexProp.Attribute(
                                    "Type").Value;
                                ODataFieldType cFieldType =
                                    Util.ConvertEdmTypeToCsharpType(cFieldTypeName);

                                if (!string.IsNullOrEmpty(cFieldName))
                                {
                                    fieldList.Add(new ODataField()
                                    {
                                        FieldName = string.Format("{0}.{1}",
                                            fieldName, cFieldName),
                                        FieldType = cFieldType,
                                        ParentFieldName = fieldName,
                                        ChildFieldName = cFieldName
                                    });
                                }
                            }
                        }
                    }
                }
                else
                {

                    fieldList.Add(
                        new ODataField()
                        {
                            FieldName = fieldName,
                            FieldType = fieldType
                        });
                }
            }
            foreach (var element in navProperties)
            {
                navList.Add(element.Attribute("Name").Value);
            }

            return true;
        }

        //TODO: XML parsing is being done twice i.e. 
        //Here and in the function ProcessRecords. Improve!
        public bool GetFieldsWithoutMetadata(List<ODataField> fieldList)
        {
            ODataField newField;

            int beforeCount = 0;
            int afterCount = 0;

            if (String.IsNullOrEmpty(settings.BaseCollectionListURL) ||
                String.IsNullOrEmpty(settings.Collection))
            {
                return false;
            }

            string nodeType = string.Empty;
            ODataFieldType fieldType;

            string queryURL = string.Format(
                settings.BaseCollectionListURL.TrimEnd('/'));

            queryURL = queryURL + "/" + this.settings.Collection;

            WebClient webClient = new WebClient();
            webClient.Credentials = Util.GetAppropriateCredential(
                settings.UserId, settings.Password);

            string encodedUrl = EncodeUrl(queryURL);

            string updatedXML = ReplaceBadCharacters(encodedUrl);

            Log.Info(Properties.Resources.LogQueryExecuting, encodedUrl);

            using (XmlTextReader reader = new XmlTextReader(
                new StringReader(updatedXML)))
            {

                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.MoveToContent();
                reader.ReadToFollowing("properties", metaNamespace);

                while (reader.Read())
                {
                    if (reader.NodeType.Equals(XmlNodeType.EndElement) &&
                        reader.LocalName.Equals("properties"))
                    {
                        reader.ReadToFollowing("properties", metaNamespace);
                        reader.Read();
                    }
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element: // The node is an element.
                            string fieldName = reader.LocalName;
                            nodeType = string.Empty;

                            beforeCount = fieldList.Count;
                            AddChildFields(reader, fieldName, fieldList);
                            afterCount = fieldList.Count;

                            while (reader.MoveToNextAttribute()) // Read the attributes.
                            {
                                if (reader.LocalName.Equals("type"))
                                {
                                    nodeType = reader.Value;
                                    break;
                                }
                            }

                            if (beforeCount == afterCount)
                            {
                                //For simple types
                                if (nodeType.Equals(string.Empty))
                                {
                                    fieldType = ODataFieldType.String;
                                }
                                else
                                {
                                    fieldType = Util.ConvertEdmTypeToCsharpType(
                                        nodeType);
                                }

                                newField = new ODataField()
                                {
                                    FieldName = fieldName,
                                    FieldType = fieldType
                                };

                                //check if field already exists.
                                if (!fieldList.Contains(newField))
                                {
                                    fieldList.Add(newField);
                                }
                            }
                            break;
                    }
                }
            }
            return true;
        }

        private static void AddChildFields(XmlReader innerReader, string nodeName,
            List<ODataField> fieldList)
        {
            string nodeType = string.Empty;
            ODataFieldType fieldType;

            using (XmlReader complexTypeReader = innerReader.ReadSubtree())
            {
                while (complexTypeReader.Read())
                {
                    if (complexTypeReader.IsStartElement())
                    {
                        while (complexTypeReader.Read())
                        {
                            if (complexTypeReader.NodeType.Equals(XmlNodeType.EndElement) &&
                                complexTypeReader.LocalName.Equals(nodeName))
                            {
                                break;
                            }
                            if (complexTypeReader.NodeType == XmlNodeType.Element)
                            {
                                string innerFieldName = complexTypeReader.LocalName;
                                nodeType = string.Empty;
                                // Read the attributes.
                                while (complexTypeReader.MoveToNextAttribute()) 
                                {
                                    if (complexTypeReader.LocalName.Equals("type"))
                                    {
                                        nodeType = innerReader.Value;
                                        break;
                                    }
                                }
                                if (nodeType.Equals(string.Empty))
                                {
                                    fieldType = ODataFieldType.String;
                                }
                                else
                                {
                                    fieldType = Util.ConvertEdmTypeToCsharpType(
                                        nodeType);
                                }

                                fieldList.Add(
                                    new ODataField()
                                    {
                                        FieldName = string.Format("{0}.{1}",
                                        nodeName, innerFieldName),
                                        FieldType = fieldType,
                                        ParentFieldName = nodeName,
                                        ChildFieldName = innerFieldName
                                    });
                            }
                        }
                    }
                }
            }
        }

        //TODO: complete XML string is loaded into memory, 
        //and then we are using XMLReader. Improve!
        /// <summary>
        /// Reads the XML from URI and replaces invalid colons(:) after d: 
        /// with underscore(_)
        /// </summary>
        /// <param name="XMLPath">URI of the XML</param>
        /// <returns></returns>
        public string ReplaceBadCharacters(string XMLPath)
        {
            string result = string.Empty;

            using (WebClient client = new WebClient())
            {
                client.Credentials = Util.GetAppropriateCredential(
                    settings.UserId, settings.Password);

                 result = client.DownloadString(XMLPath) ;

                //replacing colon with underscore
                result = Regex.Replace(result, "d:([a-z]+):([a-z]+)", "d:$1_$2");
            }
            return result;
        }

        internal StandaloneTable CreateTable(IEnumerable<ParameterValue> parameters,
            int maxRows)
        {
            if (string.IsNullOrEmpty(settings.ODataQuery))
            {
                return null;
            }

            string queryURL = Util.ApplyParameters(settings.ODataQuery, parameters);

            StandaloneTable table = new StandaloneTable();

            List<ODataField> odataFieldList = new
                List<ODataField>();

            //navlist can be used when working on ExpandList
            List<string> navigationProperties = new List<string>();

            bool isSuccess;
            if (settings.IsMetadataSupported == true)
            {
                odataFieldList = new List<ODataField>();
                navigationProperties = new List<string>();
                isSuccess = GetFields(odataFieldList, navigationProperties);
            }
            else
            {
                odataFieldList = new List<ODataField>();
                isSuccess = GetFieldsWithoutMetadata(odataFieldList);
            }

            if (!isSuccess)
            {
                return table;
            }

            table.BeginUpdate();
            try
            {
                #region Create Columns

                odataFieldList.ForEach(field =>
                    {
                        switch (field.FieldType)
                        {
                            case ODataFieldType.String:
                            case ODataFieldType.Binary:
                            case ODataFieldType.Boolean:
                            case ODataFieldType.Guid:
                            case ODataFieldType.Time:
                            case ODataFieldType.DateTimeOffset://it should be datetime
                                table.AddColumn(new TextColumn(field.FieldName));
                                break;
                            case ODataFieldType.Byte:
                            case ODataFieldType.SByte:
                            case ODataFieldType.Int16:
                            case ODataFieldType.Int32:
                            case ODataFieldType.Int64:
                            case ODataFieldType.Decimal:
                            case ODataFieldType.Double:
                            case ODataFieldType.Single:
                                table.AddColumn(new NumericColumn(field.FieldName));
                                break;
                            case ODataFieldType.DateTime:
                                table.AddColumn(new TimeColumn(field.FieldName));
                                break;
                        }
                    });
                #endregion

                if (!string.IsNullOrEmpty(settings.ODataQuery))
                {
                    int top = Util.GetTopFromURL(settings.ODataQuery);
                    ProcessRecords(queryURL, table, odataFieldList, maxRows,
                        top);
                }
            }
            finally {
                table.EndUpdate();
            }
            return table;
        }

        private static String EncodeUrl(string url)
        {
            string encodedUrl = Uri.EscapeUriString(url);
            return encodedUrl;
        }

        private bool ProcessRecords(string queryURL, StandaloneTable table,
            List<ODataField> odataFieldList, int maxRows,
            int top)
        {

            int totalRecords = 0;

            //if user has defined top in queryurl then 
            //totalrecords should be top

            if (top > 0)
            {
                totalRecords = top;
            }

            int recordsProcessed = 0;

            int topNumRecords = 500;

            bool isRetry;
            WebClient webClient = new WebClient();
            webClient.Credentials = Util.GetAppropriateCredential(
                settings.UserId, settings.Password);
            do
            {

                isRetry = false;
                try
                {
                    string encodedUrl = EncodeUrl(queryURL);

                    string updatedXML = ReplaceBadCharacters(encodedUrl);

                    Log.Info(Properties.Resources.LogQueryExecuting, encodedUrl);

                    using (XmlTextReader reader = new XmlTextReader(
                        new StringReader(updatedXML)))
                    {
                        reader.WhitespaceHandling = WhitespaceHandling.None;
                        reader.MoveToContent();

                        //get total count
                        if (totalRecords == 0)
                        {
                            while (reader.Read())
                            {
                                if (reader.IsStartElement() &&
                                    reader.LocalName == "count")
                                {   
                                    if (reader.Read())
                                    {
                                        int.TryParse(reader.Value,
                                            out totalRecords);

                                    }
                                    break;
                                }
                                else if (reader.IsStartElement() &&
                                    reader.LocalName == "entry")
                                {
                                    //if reached upto entry, count node doesn't exists.
                                    break;
                                }
                            }
                        }

                        while (reader.ReadToFollowing("properties", metaNamespace))
                        {
                            if (maxRows > -1 && table.RowCount >= maxRows) break;
                            Row row = table.AddRow();
                            recordsProcessed++;
                            while (reader.Read())
                            {
                                if (!reader.IsStartElement() || reader.IsEmptyElement)
                                {
                                    if (reader.LocalName == "properties")
                                    {
                                        //if end element of properties reached
                                        //break the inner loop
                                        break;
                                    }
                                    else
                                    {
                                        continue;
                                    }

                                }
                                ODataField field = odataFieldList.Where(o =>
                            o.FieldName == reader.LocalName
                            ).FirstOrDefault();
                                if (field != null)
                                {

                                    if (reader.Read())
                                    {

                                        if (!string.IsNullOrEmpty(reader.Value))
                                        {
                                            AddColumnValue(table, row, field,
                                                reader.Value);
                                        }
                                    }


                                }
                                else
                                {
                                    ODataField parentField = odataFieldList.Where(o =>
                                        o.ParentFieldName == reader.LocalName
                                        ).FirstOrDefault();
                                    if (parentField != null)
                                    {
                                        processComplexTypeRecord(reader, table,
                                                odataFieldList, row, parentField.ParentFieldName);
                                    }
                                }
                            }

                        }

                    }

                    if (recordsProcessed < totalRecords &&
                recordsProcessed < maxRows)
                    {
                        queryURL = Util.GetReloadURL(queryURL, topNumRecords,
                            recordsProcessed);
                    }
                }
                catch (Exception ex)
                {
                    String retryQueryURL = Util.GetRetryURL(queryURL);

                    if (!string.IsNullOrEmpty(retryQueryURL) &&
                        !(retryQueryURL.Equals(queryURL)))
                    {
                        queryURL = retryQueryURL;
                        isRetry = true;
                        continue;
                    }
                    else
                    {
                        throw new Exception(ex.Message);
                    }
                }
            } while ((recordsProcessed < totalRecords &&
                recordsProcessed < maxRows) ||
                isRetry);

            return true;

        }

        private void processComplexTypeRecord(XmlReader propertyReader,
            StandaloneTable table, List<ODataField> odataFieldList, Row row,
            string parentFieldName)
        {
            using (XmlReader complexPropertyReader = propertyReader.ReadSubtree())
            {

                while (complexPropertyReader.Read())
                {
                    if (complexPropertyReader.IsStartElement())
                    {
                        ODataField field = odataFieldList.Where(o =>
                            o.ChildFieldName == complexPropertyReader.LocalName
                            && o.ParentFieldName == parentFieldName
                            ).FirstOrDefault();
                        if (field != null)
                        {
                            if (complexPropertyReader.Read())
                            {

                                if (!string.IsNullOrEmpty(complexPropertyReader.Value))
                                {
                                    AddColumnValue(table, row, field,
                                        complexPropertyReader.Value);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void AddColumnValue(StandaloneTable table, Row r,
            ODataField field, string value)
        {
            switch (field.FieldType)
            {
                case ODataFieldType.String:
                case ODataFieldType.Binary:
                case ODataFieldType.Boolean:
                case ODataFieldType.Guid:
                case ODataFieldType.Time:
                case ODataFieldType.DateTimeOffset://it should be datetime
                    ((TextColumn)table.GetColumn(field.FieldName)
                        ).SetTextValue(r, value);
                    break;
                case ODataFieldType.Byte:
                case ODataFieldType.SByte:
                case ODataFieldType.Int16:
                case ODataFieldType.Int32:
                case ODataFieldType.Int64:
                case ODataFieldType.Decimal:
                case ODataFieldType.Double:
                case ODataFieldType.Single:
                    double numericValue = settings.NumericDataHelper.ParseNumber(
                    value);
                    ((NumericColumn) table.GetColumn(field.FieldName)
                        ).SetNumericValue(r, numericValue);
                    break;
                case ODataFieldType.DateTime:
                    DateTime dt;
                    DateTime.TryParse(value, out dt);
                    ((TimeColumn) table.GetColumn(field.FieldName)
                        ).SetTimeValue(r, dt);
                    break;
            }
        }
    }
}
