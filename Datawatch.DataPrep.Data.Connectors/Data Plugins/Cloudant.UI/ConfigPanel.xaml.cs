﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace Panopticon.CloudantPlugin.UI
{
    /// <summary>
    ///     Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("SettingsViewModel",
                typeof (CloudantSettingsViewModel), typeof (ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        private HashSet<DependencyObject> _validatedDependencyObjectsSearchIndex = 
            new HashSet<DependencyObject>();
        private HashSet<DependencyObject> _validatedDependencyObjectsView = 
            new HashSet<DependencyObject>();

        public CloudantSettingsViewModel SettingsViewModel
        {
            get
            {
                return (CloudantSettingsViewModel) GetValue(SettingsProperty);
            }
            set { SetValue(SettingsProperty, value); }
        }

        public ConfigPanel()
        {
            InitializeComponent();
        }

        private static void Settings_Changed(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (CloudantSettingsViewModel) args.OldValue,
                (CloudantSettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(
            CloudantSettingsViewModel oldSettingsViewModel,
            CloudantSettingsViewModel newSettingsViewModel)
        {
            DataContext = newSettingsViewModel;
            PasswordBox.Password = newSettingsViewModel != null
                ? newSettingsViewModel.Settings.Password
                : null;
        }

        private void PasswordBox_Changed(object sender, RoutedEventArgs e)
        {
            if (SettingsViewModel != null)
            {
                SettingsViewModel.Settings.Password = PasswordBox.Password;
            }
        }

        private void SpecificDbCheckbox_OnClick(object sender, RoutedEventArgs e)
        {
            SettingsViewModel.SpecificDbCheckboxCommand.Execute(null);
        }

        private void Validation_OnError(object sender, ValidationErrorEventArgs e)
        {
            var obj = e.OriginalSource as DependencyObject;
            var viewModel = DataContext as CloudantSettingsViewModel;
            if(viewModel != null)
            {
                if(SourceType.View == viewModel.Settings.SourceType)
                {
                    AddToHashSet(_validatedDependencyObjectsView, obj);
                    viewModel.HasValidationErrorsView = HasError(_validatedDependencyObjectsView);
                }
                else
                {
                    AddToHashSet(_validatedDependencyObjectsSearchIndex, obj);
                    viewModel.HasValidationErrorsSearchIndex = HasError(_validatedDependencyObjectsSearchIndex);
                }
            }
        }

        private bool HasError(HashSet<DependencyObject> objects)
        {
            return objects.Any(Validation.GetHasError);
        }

        private void AddToHashSet(HashSet<DependencyObject> objects, DependencyObject dependencyObject)
        {
            if (dependencyObject != null && !objects.Contains(dependencyObject))
            {
                objects.Add(dependencyObject);
            }
        }

        private void RemoveFromHashSet(HashSet<DependencyObject> objects, DependencyObject dependencyObject)
        {
            if (dependencyObject != null && objects.Contains(dependencyObject))
            {
                objects.Remove(dependencyObject);
            }
        }

        private void DoReduce_OnClicked(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = (CheckBox) sender;
            var viewModel = DataContext as CloudantSettingsViewModel;
            if ((bool) checkBox.IsChecked)
            {
                AddToHashSet(_validatedDependencyObjectsView, GroupLevelBox);
            }
            else
            {
                RemoveFromHashSet(_validatedDependencyObjectsView, GroupLevelBox);
            }
            viewModel.HasValidationErrorsView = HasError(_validatedDependencyObjectsView);
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}