﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Data;
using System.Windows.Input;
using Panopticon.CloudantPlugin.UI.Properties;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Model;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.CloudantPlugin.UI
{
    public class CloudantSettingsViewModel : ViewModelBase
    {
		#region Private Variables

		private ICommand _previewCommand;
		private DataTable _previewTable;

		private List<string> availableDatabases,
            availableViews,
            availableSearchIndicies;

        #endregion

        #region Properties

        public CloudantSettings Settings { get; set; }
        public bool ConnectionActive { get; set; }

		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				OnPropertyChanged("PreviewTable");
			}
		}

		public virtual bool IsOk
        {
            get
            {
                if (string.IsNullOrEmpty(Settings.Database))
                    return false;
                switch (Settings.SourceType)
                {
                    case SourceType.View:
                        return !string.IsNullOrEmpty(Settings.View) &&
                               !HasValidationErrorsView;
                    case SourceType.SearchIndex:
                        return !string.IsNullOrEmpty(Settings.SearchIndex) &&
                               !HasValidationErrorsSearchIndex;
                    default:
                        return false;
                }
            }
        }

        private bool _hasValidationErrorsView = false;

        public bool HasValidationErrorsView
        {
            get { return _hasValidationErrorsView; }
            set
            {
                _hasValidationErrorsView = value;
                OnPropertyChanged("HasValidationErrorsView");
            }
        }

        private bool _hasValidationErrorsSearchIndex = false;

        public bool HasValidationErrorsSearchIndex
        {
            get { return _hasValidationErrorsSearchIndex; }
            set
            {
                _hasValidationErrorsSearchIndex = value;
                OnPropertyChanged("HasValidationErrorsSearchIndex");
            }
        }

        public List<string> AvailableViews
        {
            get
            {
                if (availableViews == null)
                {
                    availableViews = new List<string>();
                }
                return availableViews;
            }
            set
            {
                if (availableViews == value)
                {
                    return;
                }
                availableViews = value;
                OnPropertyChanged("AvailableViews");
            }
        }

        public List<string> AvailableSearchIndicies
        {
            get
            {
                if (availableSearchIndicies == null)
                {
                    availableSearchIndicies = new List<string>();
                }
                return availableSearchIndicies;
            }
            set
            {
                if (availableSearchIndicies == value)
                {
                    return;
                }
                availableSearchIndicies = value;
                OnPropertyChanged("AvailableSearchIndicies");
            }
        }

        public List<string> AvailableDatabases
        {
            get
            {
                if (availableDatabases == null)
                {
                    availableDatabases = new List<string>();
                }
                return availableDatabases;
            }
            set
            {
                if (availableDatabases == value)
                {
                    return;
                }
                availableDatabases = value;
                OnPropertyChanged("AvailableDatabases");
            }
        }

        #endregion

        #region Constructors

        public CloudantSettingsViewModel(
            CloudantSettings settings, IEnumerable<ParameterValue> parameters)
        {
            Settings = settings;
            Settings.PropertyChanged += OnSettingsChanged;
        }

        #endregion

        #region Commands

        private ICommand loginCommand;
        private ICommand specificDbCheckboxCommand;

        public ICommand LoginCommand
        {
            get
            {
                return loginCommand ?? (loginCommand =
                    new DelegateCommand(Login, CanLogin));
            }
        }

        public ICommand SpecificDbCheckboxCommand
        {
            get
            {
                return specificDbCheckboxCommand ??
                       (specificDbCheckboxCommand =
                           new DelegateCommand(SpecificDbCheckboxClicked));
            }
        }

		public ICommand PreviewCommand
		{
			get
			{
				return _previewCommand ?? (_previewCommand = 
                    new DelegateCommand(PreviewData, CanPreview));
			}
		}

		#endregion

		#region Methods
		private bool CanLogin()
        {
            if (Settings == null ||
                string.IsNullOrEmpty(Settings.ServerAccountOrUrl))
            {
                return false;
            }
            return true;
        }

        private void Login()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                DoLogin();
                MessageBox.Show(Resources.LoginSuccessful,
                    Resources.LoginSuccessful, MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                ConnectionActive = false;
                OnPropertyChanged("ConnectionActive");
                Settings.Database = null;
                Settings.View = null;
                OnPropertyChanged("IsOk");
                OnPropertyChanged("AvailableDatabases");
                OnPropertyChanged("AvailableSearchIndicies");
                OnPropertyChanged("AvailableViews");
                Log.Exception(ex);
                MessageBox.Show(ex.Message, Resources.LoginFailed,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        public bool CanPreview()
        {

            if (string.IsNullOrEmpty(Settings.Database))
                return false;
            switch (Settings.SourceType)
            {
                case SourceType.View:
                    return !string.IsNullOrEmpty(Settings.View) &&
                           !HasValidationErrorsView;
                case SourceType.SearchIndex:
                    return !string.IsNullOrEmpty(Settings.SearchIndex) &&
                           !HasValidationErrorsSearchIndex;
                default:
                    return false;
            }
        }

        public virtual void DoLogin()
        {
            if (Settings.IncludeDatabase)
            {
                Settings.Database = Settings.IncludedDatabase;
                PopulateViews();
                PopulateSearchIndicies();
            }
            else
            {
                PopulateDatabases();
                if (!string.IsNullOrEmpty(Settings.Database))
                {
                    PopulateViews();
                    PopulateSearchIndicies();
                }
            }
            ConnectionActive = true;
            OnPropertyChanged("ConnectionActive");
        }

        private void SpecificDbCheckboxClicked()
        {
            ConnectionActive = false;
            OnPropertyChanged("ConnectionActive");
            Settings.Database = null;
            Settings.View = null;
            Settings.IncludedDatabase = null;
            OnPropertyChanged("IsOk");
        }

        private void OnSettingsChanged(object sender,
            PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Database":
                    try
                    {
                        if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                        {
                            Mouse.OverrideCursor = Cursors.Wait;
                        }
                        if (ConnectionActive)
                        {
                            PopulateViews();
                            PopulateSearchIndicies();
                        }
                    }
                    finally
                    {
                        if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                        {
                            Mouse.OverrideCursor = null;
                        }
                    }
                    break;
                case "View":
                    break;
            }
        }

        protected virtual List<string> GetAvailableDatabases()
        {
            return CloudantHelper.GetAvailableDatabases(Settings);
        }

        protected virtual Dictionary<string, CloudantIndex> GetAvailableViews()
        {
            return CloudantHelper.GetAvailableViews(Settings);
        }

        protected virtual Dictionary<string, CloudantIndex> GetAvailableSearchIndices()
        {
            return CloudantHelper.GetAvailableSearchIndices(Settings);
        }

        protected void PopulateDatabases()
        {
            Log.Info(Resources.LogPopulatingDatabases);
            availableDatabases = new List<string>();
            AvailableDatabases = GetAvailableDatabases();
        }

        protected void PopulateViews()
        {
            Log.Info(Resources.LogPopulatingViews);
            var viewNameList = new List<string>();
            availableViews = new List<string>();
            Settings.Views = GetAvailableViews();

            // construct the list of available views based on the view name 
            // and the design document it comes from so that each view has a 
            // unique name in the combo box.
            foreach (var kvp in Settings.Views)
            {
                viewNameList.Add(kvp.Key);
            }
            AvailableViews = viewNameList;
        }

        protected void PopulateSearchIndicies()
        {
            Log.Info(Resources.LogPopulatingSearchIndicies);
            var searchIndexNameList = new List<string>();
            availableSearchIndicies = new List<string>();
            Settings.SearchIndicies = GetAvailableSearchIndices();

            // construct the list of available views based on the view name 
            // and the design document it comes from so that each view has a 
            // unique name in the combo box.
            foreach (var kvp in Settings.SearchIndicies)
            {
                searchIndexNameList.Add(kvp.Key);
            }
            AvailableSearchIndicies = searchIndexNameList;
        }
		
		private PropertyBag CreatePropertyBag()
		{
			PropertyBag pb = new PropertyBag();
			PropertyValue pv = new PropertyValue("ServerAccountOrUrl", Settings.ServerAccountOrUrl);
			pb.Values.Add(pv);
			pv = new PropertyValue("User", Settings.User);
			pb.Values.Add(pv);
			pv = new PropertyValue("Password", Settings.Password);
			pb.Values.Add(pv);
			pv = new PropertyValue("ServerAccountOrUrlParameterized", Settings.ServerAccountOrUrlParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("ServiceRootUri", Settings.ServiceRootUri);
			pb.Values.Add(pv);
			pv = new PropertyValue("ServiceRootUriParameterized", Settings.ServiceRootUriParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("UserParameterized", Settings.UserParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("PasswordParameterized", Settings.PasswordParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("Database", Settings.Database);
			pb.Values.Add(pv);
			pv = new PropertyValue("DatabaseParameterized", Settings.DatabaseParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("View", Settings.View);
			pb.Values.Add(pv);
			pv = new PropertyValue("Title", Settings.Title);
			pb.Values.Add(pv);
			pv = new PropertyValue("SearchIndex", Settings.SearchIndex);
			pb.Values.Add(pv);
			pv = new PropertyValue("SearchIndexParameterized", Settings.SearchIndexParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("SearchTerm", Settings.SearchTerm);
			pb.Values.Add(pv);
			pv = new PropertyValue("SearchTermParameterized", Settings.SearchTermParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("Order", Settings.Order);
			pb.Values.Add(pv);
			pv = new PropertyValue("GroupLevel", Settings.GroupLevel.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("SpecificKeys", Settings.SpecificKeys);
			pb.Values.Add(pv);
			pv = new PropertyValue("SpecificKeysParameterized", Settings.SpecificKeysParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("StartKey", Settings.StartKey);
			pb.Values.Add(pv);
			pv = new PropertyValue("StartKeyParameterized", Settings.StartKeyParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("EndKey", Settings.EndKey);
			pb.Values.Add(pv);
			pv = new PropertyValue("EndKeyParameterized", Settings.EndKeyParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("Sort", Settings.Sort);
			pb.Values.Add(pv);
			pv = new PropertyValue("SortParameterized", Settings.SortParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("Limit", Settings.Limit.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("Skip", Settings.Skip.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("DisableInclusiveEnd", Settings.DisableInclusiveEnd.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("SourceType", Settings.SourceType.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("Stale", Settings.Stale.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("DoReduce", Settings.DoReduce.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("IncludeDocs", Settings.IncludeDocs.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("CanDoReduce", Settings.CanDoReduce.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("CanIncludeDocs", Settings.CanIncludeDocs.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("QuerySpecificKeys", Settings.QuerySpecificKeys.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("CanQuerySpecificKeys", Settings.CanQuerySpecificKeys.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("QueryByKeyBounds", Settings.QueryByKeyBounds.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("CanQueryByKeyBounds", Settings.CanQueryByKeyBounds.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("IncludeDatabase", Settings.IncludeDatabase.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("IncludedDatabaseParameterized", Settings.IncludedDatabaseParameterized);
			pb.Values.Add(pv);
			pv = new PropertyValue("IsViewSelected", Settings.IsViewSelected.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("IsSearchIndexSelected", Settings.IsSearchIndexSelected.ToString());
			pb.Values.Add(pv);

			return pb;
		}

		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}

				Panopticon.CloudantPlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				PropertyBag globalSettings = new PropertyBag();
				p.PluginHostSettings = new PluginHostSettingsViewModel(globalSettings);
				p.PluginHostSettings.IsParameterSupported = true;

				ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);

				if (t != null)
				{
					_previewTable = PreviewDataTable.GetPreviewTable(t);
					OnPropertyChanged("PreviewTable");
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				MessageBox.Show(ex.Message, null,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}
		#endregion
	}
}