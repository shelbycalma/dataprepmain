﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.CloudantPlugin.UI
{
    /// <summary>
    ///     Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        public static RoutedCommand OkCommand = new RoutedCommand("Ok",
            typeof (ConfigWindow));

        public CloudantSettingsViewModel SettingsViewModel { get; set; }

        public ConfigWindow(CloudantSettingsViewModel settingsViewModel)
        {
            SettingsViewModel = settingsViewModel;
            InitializeComponent();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SettingsViewModel != null && SettingsViewModel.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}