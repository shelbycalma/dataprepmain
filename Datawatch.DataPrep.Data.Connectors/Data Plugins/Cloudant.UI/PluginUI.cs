﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.CloudantPlugin.UI.Properties;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CloudantPlugin;

namespace Panopticon.CloudantPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, CloudantSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();
            var settings = Plugin.CreateSettings(new PropertyBag(), parameters);
            var settingsViewModel = CreateSettingsViewModel(settings,
                parameters);
            var window = new ConfigWindow(settingsViewModel);

            window.Owner = owner;

            bool? dialogResult = window.ShowDialog();

            if (dialogResult != true)
            {
                return null;
            }

            return settingsViewModel.Settings.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            var settings = Plugin.CreateSettings(bag, parameters);
            var settingsViewModel = CreateSettingsViewModel(settings,
                parameters);
            ConfigPanel panel = new ConfigPanel();
            panel.SettingsViewModel = settingsViewModel;

            if (settingsViewModel.IsOk)
            {
                try
                {
                    settingsViewModel.DoLogin();
                }
                catch (Exception ex)
                {
                    settingsViewModel.ConnectionActive = false;
                    settingsViewModel.Settings.Database = null;
                    settingsViewModel.Settings.View = null;
                    MessageBox.Show(ex.Message, Resources.LoginFailed,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            return configPanel.SettingsViewModel.Settings.ToPropertyBag();
        }

        protected virtual CloudantSettingsViewModel CreateSettingsViewModel(
            CloudantSettings settings, IEnumerable<ParameterValue> parameters)
        {
            return new CloudantSettingsViewModel(settings, parameters);
        }
    }
}
