﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.OneTickPlugin.UI
{
    internal partial class OneTickConnectionWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(OneTickConnectionWindow));
        IOneTickConnectionPanel connectionPanel;

        public OneTickConnectionWindow(IOneTickConnectionPanel connectionPanel)
        {
            this.connectionPanel = connectionPanel;

            InitializeComponent();
            cancelButton.Click += cancelButton_Click;
            
            this.connectionControl.Content = connectionPanel;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = connectionPanel.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;

            Close();
        }
    }
}
