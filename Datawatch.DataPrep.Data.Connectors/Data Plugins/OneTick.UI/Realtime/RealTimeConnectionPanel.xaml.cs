﻿using System.Windows.Controls;

namespace Panopticon.OneTickPlugin.UI.RealTime
{
    /// <summary>
    /// Interaction logic for RealTimeConnectionPanel.xaml
    /// </summary>
    public partial class RealTimeConnectionPanel : UserControl,
        IOneTickConnectionPanel
    {
        public RealTimeConnectionPanel(OneTickSettingsViewModel settings)
        {
            InitializeComponent();
            this.Settings = settings;
            connectionPanel.Settings = settings;
        }

        public OneTickSettingsViewModel Settings
        {
            get { return (OneTickSettingsViewModel)this.DataContext; }
            set { this.DataContext = (object)value; }
        }

        public bool IsOk
        {
            get
            {
                if (Settings == null)
                {
                    return false;
                }

                return !(string.IsNullOrEmpty(Settings.Model.IdColumn) ||
                    string.IsNullOrEmpty(Settings.ProcedureName) ||
                    (Settings.Model.IsTimeIdColumnGenerated &&
                    string.IsNullOrEmpty(Settings.Model.TimeIdColumn)));
            }
        }
    }
}
