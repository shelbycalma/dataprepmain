﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.OneTickPlugin.Properties;
using Panopticon.OneTickPlugin.RealTime;
using Panopticon.OneTickPlugin.UI.RealTime;

namespace Panopticon.OneTickPlugin.UI.Realtime
{
    public class PluginUI : PluginUIBase<Plugin, OneTickSettings>
    {
        private const string imageUri = "pack://application:,,,/" +
            "Panopticon.OneTickPlugin.UI;component/onetick-plugin.gif";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            OneTickSettings settings = new OneTickSettings(bag);
            OneTickSettingsViewModel vm = new OneTickSettingsViewModel(
                settings, parameters, this.Plugin.Id);
            RealTimeConnectionPanel connectionPanel =
                new RealTimeConnectionPanel(vm);
            return connectionPanel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            RealTimeConnectionPanel cp = (RealTimeConnectionPanel)obj;
            OneTickSettingsViewModel vm = (OneTickSettingsViewModel)cp.DataContext;
            return vm.Model.ToPropertyBag();
        }

        // Called when the user selects this plug-in in the "Connect to data" 
        // dialog or picks it from the data plug-in dropdown. The DoConnect 
        // method is resposible for allowing the user to create a new data 
        // source connection.
        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            OneTickSettings settings = new OneTickSettings();
            OneTickSettingsViewModel settingsView =
                new OneTickSettingsViewModel(settings, parameters, this.Plugin.Id);
            RealTimeConnectionPanel connectionPanel =
                new RealTimeConnectionPanel(settingsView);
            OneTickConnectionWindow window = new OneTickConnectionWindow(
                connectionPanel);
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                string title = Resources.UiDefaultConnectionTitle;
                if (settingsView.Query != null)
                {
                    title = settingsView.Query.QueryName;
                    if (string.IsNullOrEmpty(title))
                    {
                        title = settingsView.Query.ProcedureName;
                    }
                }
                settings.Title = title;

                return settings.ToPropertyBag();
            }
            return null;
        }
    }
}
