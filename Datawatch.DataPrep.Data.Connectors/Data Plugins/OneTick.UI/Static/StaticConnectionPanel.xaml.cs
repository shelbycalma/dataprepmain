﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.OneTickPlugin.UI.Static
{
    /// <summary>
    /// Interaction logic for StaticConnectionPanel.xaml
    /// </summary>
    public partial class StaticConnectionPanel : UserControl, IOneTickConnectionPanel, IDataPluginConfigElement
    {
        public StaticConnectionPanel(OneTickSettingsViewModel settings)
        {
            InitializeComponent();
            sqlQueryModeButton.Checked += sqlQueryModeButton_Checked;
            this.Settings = settings;
            connectionPanel.Settings = settings;
        }

        private void sqlQueryModeButton_Checked(object sender, RoutedEventArgs e)
        {
            this.Settings.SetExplicitSql();            
        }

        public OneTickSettingsViewModel Settings
        {
            get { return (OneTickSettingsViewModel)this.DataContext; }
            set { this.DataContext = (object)value; }
        }

        public void OnOk()
        {
            if (Settings == null)
            {
                return;
            }

            // This method is called when Ok button in DataSourceSettingsDialog is clicked.
            // Clear the column here, so that even if settings are not chagned, latest schema from 
            // DB is pulled.
            Settings.Model.SchemaColumnsSettings.ClearSchemaColumns();
        }
        
        public bool IsOk
        {
            get
            {
                if (Settings == null)
                {
                    return false;
                }

                return Settings.IsQueryConnectionMode
                    ? !string.IsNullOrEmpty(Settings.Sql) :
                    !string.IsNullOrEmpty(Settings.ProcedureName);
            }
        }
    }
}
