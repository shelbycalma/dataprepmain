﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.OneTickPlugin.Properties;
using Panopticon.OneTickPlugin.Static;

namespace Panopticon.OneTickPlugin.UI.Static
{
    public class PluginUI : PluginUIBase<Plugin, OneTickSettings>
    {
        private const string imageUri = "pack://application:,,,/" +
            "Panopticon.OneTickPlugin.UI;component/onetick-plugin.gif";

        public PluginUI(Plugin plugin, System.Windows.Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception) { }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            OneTickSettings settings = new OneTickSettings();
            OneTickSettingsViewModel settingsView =
                new OneTickSettingsViewModel(settings, parameters, this.Plugin.Id);
            StaticConnectionPanel connectionPanel =
                new StaticConnectionPanel(settingsView);
            OneTickConnectionWindow window =
                new OneTickConnectionWindow(connectionPanel);

            window.Owner = owner;
            
            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            string title = Resources.UiDefaultConnectionTitle;
            if (settingsView.Query != null)
            {
                title = settingsView.Query.QueryName;
                if (string.IsNullOrEmpty(title))
                {
                    title = settingsView.Query.ProcedureName;
                }
            }
            settings.Title = title;

            PropertyBag properties = settings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            OneTickSettings settings = Plugin.CreateSettings(bag);

            return new StaticConnectionPanel(
                new OneTickSettingsViewModel(settings, parameters, this.Plugin.Id));
        }

        public override PropertyBag GetSetting(object element)
        {
            StaticConnectionPanel panel = (StaticConnectionPanel)element;

            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
