﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Panopticon.OneTickPlugin.Schema;

namespace Panopticon.OneTickPlugin.UI
{
    internal partial class OneTickConnectionPanel : UserControl
    {
        public OneTickConnectionPanel()
        {
            InitializeComponent();
            
            loadSchemaButton.Click += SchemaButton_Click;
            reloadSchemaButton.Click += SchemaButton_Click;
            removeSchemaButton.Click += SchemaButton_Click;

            schemaQueryModeButton.Checked += schemaQueryModeButton_Checked;

            queryList.MouseDoubleClick += queryList_MouseDoubleClick;
        }

        private void queryList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox selector = sender as ListBox;
            if (selector is ListBox)
            {
                this.Settings.Query = (Query)selector.SelectedItem;
            }
        }

        private void SchemaButton_Click(object sender, RoutedEventArgs e)
        {
            if (sender == loadSchemaButton)
            {
                this.Settings.LoadSchema();
            }
            else if (sender == reloadSchemaButton)
            {
                this.Settings.ReloadSchema();
            }
            else if (sender == removeSchemaButton)
            {
                this.Settings.RemoveSchema();
            }
        }
        
        private void schemaQueryModeButton_Checked(object sender, RoutedEventArgs e)
        {
            this.Settings.ClearExplicitSql();
        }

        public OneTickSettingsViewModel Settings
        {
            get { return (OneTickSettingsViewModel)this.DataContext; }
            set { this.DataContext = (object)value; }
        }        
    }
}
