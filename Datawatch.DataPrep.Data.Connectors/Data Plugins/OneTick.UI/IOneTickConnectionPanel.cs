﻿namespace Panopticon.OneTickPlugin.UI
{
    internal interface IOneTickConnectionPanel
    {
        OneTickSettingsViewModel Settings { get; set; }
        bool IsOk { get; }
    }
}
