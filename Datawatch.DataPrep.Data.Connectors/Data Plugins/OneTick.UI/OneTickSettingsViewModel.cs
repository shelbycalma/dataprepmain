﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.OneTickPlugin.Schema;

namespace Panopticon.OneTickPlugin.UI
{
    public class OneTickSettingsViewModel : INotifyPropertyChanged
    {
        private enum ConnectionMode
        {
            Otq = 0,
            Query = 1
        }

        ConnectionMode connectionMode = ConnectionMode.Otq;

        private const string
            PropContextName = "ContextName",
            PropKnownContextNames = "KnownContextNames",
            PropIsContextCached = "IsContextCached",
            PropSchema = "Schema",
            PropHasSchema = "HasSchema",
            PropHasNoSchema = "HasNoSchema",
            PropHasNoSchemaButContextName = "HasNoSchemaButContextName",
            PropErrorMessage = "ErrorMessage",
            PropHasError = "HasError",
            PropSql = "Sql",
            PropQueryFilter = "QueryFilter",
            PropQuery = "Query",
            PropSymbolList = "SymbolList",
            PropStartTime = "StartTime",
            PropEndTime = "EndTime",
            PropTimeZone = "SelectedTimeZone",
            PropParameters = "Parameters",
            PropHasParameters = "HasParameters",
            PropProcedureName = "ProcedureName",
            PropEncloseParameterInQuote = "EncloseParameterInQuote",
            PropIsOtqConnectionMode = "IsOtqConnectionMode",
            PropIsQueryConnectionMode = "IsQueryConnectionMode",
            PropShowLocalOTQs = "ShowLocalOTQs",
            PropShowRemoteOTQs = "ShowRemoteOTQs",
            PropSeparateDBName = "SeparateDBName",
            PropShowSymbolErrorAsWarnings = "ShowSymbolErrorAsWarnings";

        private static readonly StringComparison QueryFilterComparison =
            StringComparison.InvariantCultureIgnoreCase;

        private ICommand fetchSchemaCommand;
        private IEnumerable<ParameterValue> tableParameters;

        public event PropertyChangedEventHandler PropertyChanged;

        private OneTickSchema schema;
        private ICollectionView queryView;
        private string queryFilter;
        private Query query;
        
        private string error;

        public OneTickSettingsViewModel(OneTickSettings model,
            IEnumerable<ParameterValue> tableParameters, string pluginId)
        {
            this.Model = model;
            this.PropertyChanged += this_PropertyChanged;
            this.TimeZoneHelper.PropertyChanged += this_PropertyChanged;
            this.Parameters = new ObservableCollection<OneTickParameter>();
            this.tableParameters = tableParameters;
            SchemaProvider.Initialize(pluginId);
            
            InitializeFromModel();
        }

        public void ClearExplicitSql()
        {
            this.Sql = null;
        }

        private void CreateParametersFromSchema()
        {
            if (this.query == null)
            {
                return;
            }

            List<OneTickParameter> oldParameters = this.Parameters.ToList();

            foreach (OneTickParameter parameter in this.Parameters) {
                parameter.PropertyChanged -= Parameter_PropertyChanged;
            }
            this.Parameters.Clear();
            foreach (Field field in this.Query.Inputs) {
                OneTickParameter oldParameter = oldParameters.Where(
                    p => p.Name == field.ColumnName).FirstOrDefault();
                if (oldParameter != null)
                {
                    this.Parameters.Add(oldParameter);
                    continue;
                }

                OneTickParameter parameter = OneTickParameter.FromField(field);
                
                if (parameter.IsUserVisible) {
                    parameter.PropertyChanged += Parameter_PropertyChanged;
                    this.Parameters.Add(parameter);
                }
            }
        }

        public string ContextName
        {
            get { return this.Model.ContextName; }
            set {
                if (value != this.Model.ContextName)
                {
                    this.Model.ContextName = value;
                    this.ErrorMessage = null;
                    if (this.IsContextCached) {
                        LoadSchema();
                    } else {
                        this.Schema = null;
                    }
                    NotifyChanged(PropContextName);
                    NotifyChanged(PropIsContextCached);
                    NotifyChanged(PropHasNoSchemaButContextName);
                }
            }
        }
                
        public string EndTime
        {
            get { return this.Model.EndTime; }
            set {
                if (value != this.Model.EndTime) {
                    this.Model.EndTime = value;
                    NotifyChanged(PropEndTime);
                }
            }
        }

        public bool EncloseParameterInQuote
        {
            get { return this.Model.EncloseParameterInQuote; }
            set
            {
                if (value != Model.EncloseParameterInQuote)
                {
                    Model.EncloseParameterInQuote = value;
                    NotifyChanged(PropEncloseParameterInQuote);
                }
            }
        }

        public string ErrorMessage
        {
            get { return error; }
            set {
                if (value != error) {
                    error = value;
                    NotifyChanged(PropErrorMessage);
                    NotifyChanged(PropHasError);
                }
            }
        }

        public ICommand FetchSchemaCommand
        {
            get
            {
                if (fetchSchemaCommand == null)
                {
                    fetchSchemaCommand = new DelegateCommand(FetchSchema,
                        CanFetchSchema);
                }
                return fetchSchemaCommand;
            }
        }

        private void FetchSchema()
        {
            if (Model != null)
            {
                Model.SchemaColumnsSettings.ClearSchemaColumns();
            }            
            UpdateIdCandidates();
        }

        private bool CanFetchSchema()
        {
            return !string.IsNullOrEmpty(ProcedureName);
        }

        private string GenerateSql()
        {
            return OneTickQueryBuilder.BuildQuery(this.Model,null);
        }

        public bool HasError
        {
            get { return this.ErrorMessage != null; }
        }

        public bool HasNoSchema
        {
            get { return this.Schema == null; }
        }

        public bool HasNoSchemaButContextName
        {
            get { return this.Schema == null &&
                !string.IsNullOrEmpty(this.ContextName); }
        }

        public bool HasParameters
        {
            get { return this.Parameters.Count > 0; }
        }

        public bool HasSchema
        {
            get { return this.Schema != null; }
        }

        public bool IsOtqConnectionMode
        {
            get { return connectionMode == ConnectionMode.Otq; }
            set
            {
                if (value)
                {
                    connectionMode = ConnectionMode.Otq;
                    NotifyChanged(PropIsOtqConnectionMode);
                    NotifyChanged(PropIsQueryConnectionMode);
                }
            }
        }

        public bool IsQueryConnectionMode
        {
            get { return connectionMode == ConnectionMode.Query; }
            set
            {
                if (value)
                {
                    connectionMode = ConnectionMode.Query;
                    NotifyChanged(PropIsOtqConnectionMode);
                    NotifyChanged(PropIsQueryConnectionMode);
                }
            }
        }

        private void InitializeFromModel()
        {
            if (string.IsNullOrEmpty(this.Model.ContextName)) {
                return;
            }

            LoadSchema();
            if (this.Schema == null) {
                return;
            }

            if (this.Schema.Queries.Count == 0)
            {
                return;
            }

            Query query = this.Schema.Queries
                .Where(p => p.ProcedureName.Equals(
                    this.Model.ProcedureName)).FirstOrDefault();
            if (query != null)
            {
                this.query = query;
            }


            CreateParametersFromSchema();

            if (this.Model.ProcedureName != null) {
                LoadParameterValuesFromModel();
            }

            if (this.Model.Sql != null)
            {
                connectionMode = ConnectionMode.Query;
            } else
            {
                connectionMode = ConnectionMode.Otq;
            }
        }

        public bool IsContextCached
        {
            get { return SchemaProvider.IsCached(this.ContextName); }
        }

        public IEnumerable<string> KnownContextNames
        {
            get { return SchemaProvider.KnownContextNames; }
        }

        private void LoadParameterValuesFromModel()
        {
            if (!HasParameters)
            {
                return;
            }

            List<OneTickParameter> savedInputParameters = 
                this.Model.InputParameters.ToList();

            if (savedInputParameters.Count == 0)
            {
                return;
            }
            //Actual parameters can be totally different then what is saved in 
            //this connection instance specially when cached schema is being used
            //as cache is central for all the instances.
            foreach (OneTickParameter parameter in this.Parameters)
            {
                OneTickParameter savedParameter = savedInputParameters.Where(
                    p => p.Name == parameter.Name).FirstOrDefault();
                
                if (savedParameter == null)
                {
                    continue;                    
                }
                parameter.Value = savedParameter.Value;
             
            }
        }

        public void LoadSchema()
        {
            this.ErrorMessage = null;
            if (string.IsNullOrEmpty(this.ContextName)) {
                this.ErrorMessage = Properties.Resources.UiMustProvideDataSource;
                return;
            }
            try {
                this.Schema = SchemaProvider.Load(this.ContextName,
                    ShowLocalOTQs, ShowRemoteOTQs);
                NotifyChanged(PropKnownContextNames);
                NotifyChanged(PropIsContextCached);
            } catch (Exception ex) {
                this.ErrorMessage = ex.Message;
            }
        }

        public OneTickSettings Model { get; private set; }

        private void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null) {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Parameter_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            SaveParametersToModel();
        }

        public ObservableCollection<OneTickParameter>
            Parameters { get; private set; }

        public Query Query
        {
            get { return query; }
            set {
                if (value != null && value != query)
                {
                    query = value;
                    this.Model.ProcedureName = query.ProcedureName;
                    NotifyChanged(PropQuery);
                }
            }
        }

        public string QueryFilter
        {
            get { return queryFilter; }
            set {
                if (value != queryFilter) {
                    queryFilter = value;
                    NotifyChanged(PropQueryFilter);
                }
            }
        }

        private bool queryView_Filter(object item)
        {
            if (string.IsNullOrEmpty(queryFilter)) {
                return true;
            }
            Query query = item as Query;
            if (query == null) {
                return false;
            }
            string name = query.ProcedureName;
            if (string.IsNullOrEmpty(name)) {
                return false;
            }
            bool visible = name.IndexOf(
                queryFilter, QueryFilterComparison) >= 0;
            return visible;
        }

        public void ReloadSchema()
        {
            this.ErrorMessage = null;
            try {
                this.Schema = SchemaProvider.Reload(this.ContextName,
                    ShowLocalOTQs, ShowRemoteOTQs);
                NotifyChanged(PropIsContextCached);

                //Set the query so that any change in parameters gets updated in GUI
                //and also saved inside model
                if (this.Schema.Queries.Count == 0 || string.IsNullOrEmpty(
                    this.Model.ProcedureName))
                {
                    return;
                }

                Query query = this.Schema.Queries
                    .Where(p => p.ProcedureName.Equals(
                        this.Model.ProcedureName)).FirstOrDefault();
                if (query == null)
                {
                    return;
                }

                this.Query = query;
                
            } catch (Exception ex) {
                this.ErrorMessage = ex.Message;
            }
        }

        public void RemoveSchema()
        {
            this.ErrorMessage = null;
            try {
                this.Schema = null;
                SchemaProvider.ClearCache(this.ContextName);
                NotifyChanged(PropKnownContextNames);
                NotifyChanged(PropIsContextCached);
            } catch (Exception ex) {
                this.ErrorMessage = ex.Message;
            }
        }

        public void ResetSql()
        {
            this.Sql = null;
        }

        private void SaveParametersToModel()
        {
            this.Model.InputParameters = this.Parameters.ToArray();
            NotifyChanged(PropSql);
        }

        public OneTickSchema Schema
        {
            get { return schema; }
            set {
                if (value != schema) {
                    schema = value;
                    NotifyChanged(PropSchema);
                    NotifyChanged(PropHasSchema);
                    NotifyChanged(PropHasNoSchema);
                    NotifyChanged(PropHasNoSchemaButContextName);
                }
            }
        }

        public void SetExplicitSql()
        {
            this.Sql = GenerateSql();
        }

        public string StartTime
        {
            get { return this.Model.StartTime; }
            set {
                if (value != this.Model.StartTime) {
                    this.Model.StartTime = value;
                    NotifyChanged(PropStartTime);
                }
            }
        }

        public string Sql
        {
            get {
                if (this.Model.Sql != null) {
                    return this.Model.Sql;
                }
                return GenerateSql();
            }
            set {
                if (value != this.Model.Sql) {
                    this.Model.Sql = value;
                    NotifyChanged(PropSql);
                }
            }
        }

        public string SymbolList
        {
            get { return this.Model.SymbolList; }
            set {
                if (value != this.Model.SymbolList) {
                    this.Model.SymbolList = value;
                    NotifyChanged(PropSymbolList);
                }
            }
        }

        public string ProcedureName
        {
            get { return this.Model.ProcedureName; }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return this.Model.TimeZoneHelper;
            }
        }

        public bool ShowLocalOTQs
        {
            get { return this.Model.ShowLocalOTQs; }
            set
            {
                if (value != Model.ShowLocalOTQs)
                {
                    Model.ShowLocalOTQs = value;
                    NotifyChanged(PropShowLocalOTQs);
                }
            }
        }

        public bool ShowRemoteOTQs
        {
            get { return this.Model.ShowRemoteOTQs; }
            set
            {
                if (value != Model.ShowRemoteOTQs)
                {
                    Model.ShowRemoteOTQs = value;
                    NotifyChanged(PropShowRemoteOTQs);
                }
            }
        }

        public bool SeparateDBName
        {
            get { return this.Model.SeparateDBName; }
            set
            {
                if (value != Model.SeparateDBName)
                {
                    Model.SeparateDBName = value;
                    NotifyChanged(PropSeparateDBName);
                }
            }
        }

        public bool ShowSymbolErrorAsWarnings
        {
            get { return this.Model.ShowSymbolErrorAsWarnings; }
            set
            {
                if (value != Model.ShowSymbolErrorAsWarnings)
                {
                    Model.ShowSymbolErrorAsWarnings = value;
                    NotifyChanged(PropShowSymbolErrorAsWarnings);
                }
            }
        }

        private void this_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            bool sql = false;
            if (PropSchema.Equals(e.PropertyName)) {
                if (queryView != null) {
                    queryView.Filter = null;
                    queryView = null;
                }
                if (this.Schema != null) {                    
                    queryView = CollectionViewSource.GetDefaultView(
                        this.Schema.Queries);
                    queryView.Filter = queryView_Filter;
                }
            }
            else if (PropQueryFilter.Equals(e.PropertyName)) {
                if (queryView != null) {
                    queryView.Refresh();
                }
            }
            else if (PropQuery.Equals(e.PropertyName)) {
                this.Model.ProcedureName = this.Query.ProcedureName;
                CreateParametersFromSchema();
                SaveParametersToModel();
                NotifyChanged(PropParameters);
                NotifyChanged(PropHasParameters);
                NotifyChanged(PropProcedureName);
                sql = true;
            }
            else if (PropParameters.Equals(e.PropertyName) ||
                PropSymbolList.Equals(e.PropertyName) ||
                PropStartTime.Equals(e.PropertyName) ||
                PropEndTime.Equals(e.PropertyName) ||
                PropTimeZone.Equals(e.PropertyName)) {
                sql = true;
            }
            if (sql) {
                NotifyChanged(PropSql);
            }
        }

        internal void UpdateIdCandidates()
        {
            try
            {
                ParameterTable table = (ParameterTable)
                    OneTickUtils.GetSchema(this.Model, tableParameters);
                List<string> idCandidates = new List<string>();
                List<string> timeIdCandidates = new List<string>();

                for (int c = 0; c < table.ColumnCount; c++)
                {
                    Column column = table.GetColumn(c);
                    if (column is TextColumn)
                    {    
                        idCandidates.Add(column.Name);
                    }
                    else if (column is TimeColumn)
                    {   
                        timeIdCandidates.Add(column.Name);
                    }
                }

                this.Model.IdColumnCandidates =
                        idCandidates.Count > 0 ? idCandidates.ToArray() : null;
                this.Model.TimeIdColumnCandidates =
                    timeIdCandidates.Count > 0 ? timeIdCandidates.ToArray() : null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                           Properties.Resources.UiWindowTitle,
                           MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }        
    }
}
