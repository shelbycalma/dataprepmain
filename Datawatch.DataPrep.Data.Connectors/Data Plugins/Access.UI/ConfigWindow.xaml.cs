﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.AccessPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        #region Fields
        public static RoutedCommand OkCommand =
            new RoutedCommand("Ok", typeof (ConfigWindow));
        #endregion Fields

        #region Properties
        public AccessSettingsViewModel AccessSettingsViewModel { get; set; }
        #endregion Properties

        #region Constructors
        public ConfigWindow(AccessSettingsViewModel viewModel)
        {
            AccessSettingsViewModel = viewModel;
            InitializeComponent();
        }
        #endregion Constructors

        #region Events
        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AccessSettingsViewModel.Settings.IsValid;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
        #endregion Events
    }
}
