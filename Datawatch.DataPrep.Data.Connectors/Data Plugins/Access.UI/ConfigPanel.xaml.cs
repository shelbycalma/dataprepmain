﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;

namespace Panopticon.AccessPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
        #region Fields
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register(
                "AccessSettingsViewModel",
                typeof(AccessSettingsViewModel),
                typeof(ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        public static RoutedCommand BrowseCommand =
            new RoutedCommand("Browse", typeof (ConfigPanel));
        #endregion Fields

        #region Properties
        public AccessSettingsViewModel AccessSettingsViewModel
        {
            get { return (AccessSettingsViewModel)GetValue(SettingsProperty); }
            set
            {
                SetValue(SettingsProperty, value);
            }
        }
        #endregion Properties

        #region Constructors
        public ConfigPanel()
        {
            InitializeComponent();
        }

        public ConfigPanel(AccessSettingsViewModel viewModel)
            : this()
        {
            //Reread the list of columns, preserving the status of selected columns
            AccessSettingsViewModel = viewModel;
        }
        #endregion Constructors

        #region Events
        private static void Settings_Changed(
            DependencyObject d,
            DependencyPropertyChangedEventArgs e)
        {
            ((ConfigPanel)d).OnSettingsChanged(
                (AccessSettingsViewModel)e.OldValue,
                (AccessSettingsViewModel)e.NewValue);
        }

        private void OnSettingsChanged(
            AccessSettingsViewModel oldSettings,
            AccessSettingsViewModel newSettings)
        {
            DataContext = newSettings;
        }

        private void CanExecuteBrowse(
            object sender,
            CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ExecuteBrowse(
            object sender,
            ExecutedRoutedEventArgs e)
        {
            var dialog = new OpenFileDialog { Filter = AccessUtils.FileFilter };
            if (dialog.ShowDialog() == true)
                AccessSettingsViewModel.FileName = dialog.FileName;
        }
        #endregion Events
    }
}
