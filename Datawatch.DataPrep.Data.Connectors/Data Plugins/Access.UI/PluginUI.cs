﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Microsoft.Win32;

namespace Panopticon.AccessPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, AccessSettings>, IFileOpenDataPluginUI<Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        { }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            var dialog = new OpenFileDialog { Filter = FileFilter };
            return (dialog.ShowDialog() == true)
                ? DoOpenFile(dialog.FileName)
                : null;
        }

        public override object GetConfigElement(
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            var viewModel = new AccessSettingsViewModel(bag, 
                new MessageBoxBasedErrorReportingService());
            return new ConfigPanel(viewModel);
        }

        public override PropertyBag GetSetting(object obj)
        {
            var panel = obj as ConfigPanel;
            return (panel != null)
                ? panel.AccessSettingsViewModel.Settings.ToPropertyBag()
                : null;
        }

        public PropertyBag DoOpenFile(string filePath)
        {
            var viewModel =
                new AccessSettingsViewModel(
                    filePath, new MessageBoxBasedErrorReportingService());

            var window = new ConfigWindow(viewModel);
            window.Owner = owner;
            
            return (window.ShowDialog() == true)
                ? viewModel.Settings.ToPropertyBag()
                : null;
        }

        public string[] FileExtensions
        {
            get { return AccessUtils.FileExtensions; }
        }

        public string FileFilter { get { return AccessUtils.FileFilter; } }

        public bool CanOpen(string filePath)
        {
            return DataUtils.CanOpen(filePath, FileExtensions);
        }

        public PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new System.NotImplementedException();
        }
    }
}
