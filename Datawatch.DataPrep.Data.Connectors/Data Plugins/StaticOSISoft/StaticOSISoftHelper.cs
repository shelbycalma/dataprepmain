﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Data;
using OSIsoft.AF.EventFrame;
using OSIsoft.AF.PI;
using OSIsoft.AF.Time;
using Panopticon.StaticOSISoftPlugin.Enums;
using Panopticon.StaticOSISoftPlugin.OnDemand_Classes;

namespace Panopticon.StaticOSISoftPlugin
{
    public class StaticOSISoftHelper : QueryOnDemandParameterHelperBase
    {
        #region Private Variables

        private IPluginErrorReportingService errorReporter;

        private Dictionary<string, AFElement> selectedElements
            = new Dictionary<string, AFElement>();

        private bool areElementsLoaded;
        private bool arePiPointFlagsLoaded;

        private Dictionary<string, KeyValuePair<bool, ColumnType>> hasPIPoint;
        private readonly Dictionary<string, Dictionary<string, FunctionType>> 
            aggregateAliases
            = new Dictionary<string, Dictionary<string, FunctionType>>();
        private HashSet<string> onDemandVisibleColumns;

        private List<string> visibleSelectedAttrNames
        {
            get
            {
                return new List<string>(onDemandVisibleColumns
                    .Intersect(GetAttributeList()));
            }
        }

        private readonly List<string> elementsNonQueryColumns  
            = new List<string>()
        {
          Properties.Resources.ColDb,
          Properties.Resources.ColSystem,
          Properties.Resources.ColEle,
          Properties.Resources.ColElePath         
        };

        private readonly List<string> historicNonQueryColumns 
            = new List<string>()
            {
                Properties.Resources.ColEle,
                Properties.Resources.ColSystem,
                Properties.Resources.ColAttribute,
                Properties.Resources.ColDb,
                Properties.Resources.ColEventPath
            }; 

        #endregion
         
        #region Constructors

        public StaticOSISoftHelper(
            StaticOSISoftSettings settings,
            OnDemandParameters demandParams,
            IPluginErrorReportingService errorReporter) 
            : base(demandParams)
        {
            OsiSettings = settings;
            Systems = new PISystems(true);
            Systems.SetApplicationIdentity("47f29cf8-98d6-48e4-8f35-c8e9cac161c9");
            this.errorReporter = errorReporter;

            if (demandParams != null)
            {
                //set OnDemand metadata
                OnDemandParams = demandParams;
                SetOnDemandFlags();
                SetVisibleOnDemandColumns();
            }
        }

        #endregion

        #region Properties

        public bool IsConnected
        {
            get { return System != null && System.ConnectionInfo.IsConnected; }
        }

        public bool AreOnDemandParamsEmpty
        {
            get
            {
                return HasAggregates && HasAuxiliaryCols && HasBreakDownCols 
                        && HasBucketings && HasDomains && HasDrillPath 
                        && HasDrillPath ;
            }
        }

        public Dictionary<string, KeyValuePair<bool, ColumnType>> HasPIPoint {
            get
            {
                if (!arePiPointFlagsLoaded)
                {
                    hasPIPoint = CheckAttributePIPoints();

                    arePiPointFlagsLoaded = true;
                }

                return hasPIPoint;
            } 
        }

        public StaticOSISoftSettings OsiSettings { get; set; }

        public AFDatabase Database
        {
            get
            {
                try
                {
                    return System.Databases[OsiSettings.Database];
                }
                catch
                {
                    Log.Error("Database not found.");
                    return null;
                }
            }
        }

        public PISystems Systems { get; set; }

        public PISystem System { get; set; }

        public Dictionary<string, AFElement> SelectedElements
        {
            get
            {
                if (!areElementsLoaded)
                {
                    selectedElements =
                        (Dictionary<string, AFElement>)
                            GetElementByPaths(GetPathList());

                    areElementsLoaded = true;
                }

                return selectedElements;
            }
            set { selectedElements = value; }
        }

        public OnDemandParameters OnDemandParams { get; set; }

        public bool HasBreakDownCols { get; set; }
        public bool HasAggregates { get; set; }
        public bool HasAuxiliaryCols { get; set; }
        public bool HasDrillPath { get; set; }
        public bool HasDomains { get; set; }
        public bool HasPredicates { get; set; }
        public bool HasBucketings { get; set; }

        #endregion

        //General Methods
        public bool Connect()
        {
            if (String.IsNullOrWhiteSpace(OsiSettings.SystemName)) return false;

            System = Systems[OsiSettings.SystemName];

            if (System == null) return false;
            if (System.ConnectionInfo.IsConnected) return true;

            try
            {
                if (OsiSettings.UseWindowsAuthentication)
                {
                    System.Connect();
                }
                else
                {
                    string paramUser =
                        OsiSettings.ParameterizeValue(OsiSettings.UserName);
                    string paramPass =
                        OsiSettings.ParameterizeValue(OsiSettings.Password);

                    System.Connect(new NetworkCredential(paramUser, paramPass));
                }
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.ConnectionError);
                Log.Exception(e);
                this.errorReporter.Report("Error",
                    Properties.Resources.ConnectionError + 
                        ".\n" + "Exception: " + e.Message);
            }

            return IsConnected;
        }

        public void Disconnect()
        {
            if (System != null && System.ConnectionInfo.IsConnected)
                System.Disconnect();
        }

        public void ElementSelectionChanged()
        {
            areElementsLoaded = false;
        }

        #region Elements
        //Elements Methods
        public StandaloneTable BuildElementsTable(StandaloneTable table)
        {
            for (int i = 0; i < GetDeepestLevelInSelected(); i++)
            {
                table.AddColumn(new TextColumn(
                    Properties.Resources.ColLevel + (i + 1)));
            }

            table.AddColumn(new TextColumn(Properties.Resources.ColEle));
            table.AddColumn(new TextColumn(Properties.Resources.ColEleCat));
            table.AddColumn(new TextColumn(Properties.Resources.ColSystem));
            table.AddColumn(new TextColumn(Properties.Resources.ColDb));

            List<string> attrNameList = new List<string>(GetAttributeList());
            foreach (AFElement element in SelectedElements.Values)
            {
                foreach (string attrName in attrNameList)
                {
                    if (table.ContainsColumn(attrName)) continue;

                    AFAttribute attr;
                    try
                    {
                        attr = element.Attributes[attrName];
                    }
                    catch (Exception)
                    {
                        Log.Info(
                            String.Format(
                                Properties.Resources.LogElementAttributeError,
                                attrName, element.Name));
                        continue;
                    }

                    Type t = attr.Type;
                    if (t == typeof(int) || t == typeof(double))
                        table.AddColumn(new NumericColumn(attr.Name));

                    else if (t == typeof(DateTime))
                        table.AddColumn(new TimeColumn(attr.Name));

                    else
                        table.AddColumn(new TextColumn(attr.Name));
                }
            }

            table.AddColumn(new TextColumn(Properties.Resources.ColElePath));

            return table;
        }

        public StandaloneTable PopulateElementsTable(
            StandaloneTable table, int rowCount)
        {
            //disable sample Table
            if (rowCount == 10) return table;

            AFAttributeList attrList =
                (AFAttributeList)GetAllSelectedAttributes();

            string[] selectedAttrNames = GetAttributeList();

            Stopwatch sw = new Stopwatch();

            if (OsiSettings.IsLoggingEnabled)
                sw.Start();
            AFValues results = attrList.GetValue(AFTime.Now);

            int rowsAdded = 0;

            int deepestLevel = GetDeepestLevelInSelected();

            foreach (KeyValuePair<string, AFElement> ele in SelectedElements)
            {
                if (rowCount >= 0 && rowsAdded > rowCount)
                    break;

                Row currRow = table.AddRow(new object[table.ColumnCount]);
                rowsAdded++;

                //add attribute values to row
                for (int i = 0; i < selectedAttrNames.Length; i++)
                {
                    if (results.Count == 0) continue;

                    Column valCol = table.GetColumn(results[0].Attribute.Name);

                    if (valCol == null
                        || ele.Value != results[0].Attribute.Element)
                        continue;

                    if (!results[0].IsGood)
                    {
                        results.RemoveAt(0);
                        continue;
                    }

                    valCol.SetValue(currRow, results[0].Value);
                    results.RemoveAt(0);
                }

                string[] elePath = ele.Key.Split(new[] { '\\' },
                    StringSplitOptions.RemoveEmptyEntries);

                //level columns
                for (int i = 0; i < deepestLevel; i++)
                {
                    Column levelColumn
                        = table.GetColumn(String.Format("Level{0}", i + 1));

                    if (levelColumn == null) break;

                    levelColumn.SetValue(currRow,
                        i < elePath.Length ? elePath[i] : null);
                }

                //other columns
                Column currCol = table.GetColumn("Element");
                if (currCol != null)
                    currCol.SetValue(currRow, ele.Value.Name);

                currCol = table.GetColumn("PI System");
                if (currCol != null)
                    currCol.SetValue(currRow, System.Name);

                currCol = table.GetColumn("Database");
                if (currCol != null)
                    currCol.SetValue(currRow, Database.Name);

                currCol = table.GetColumn("Element Path");
                if (currCol != null)
                    currCol.SetValue(currRow, ele.Key);

                currCol = table.GetColumn("Element Category");
                if (currCol != null && ele.Value.Categories != null
                    && ele.Value.Categories.Count > 0)
                    currCol.SetValue(currRow, ele.Value.Categories[0]
                        .ToString());
            }

            if (OsiSettings.IsLoggingEnabled)
            {
                //Logging
                string[] paths = GetPathList();
                string[] attrs = GetAttributeList();
                sw.Stop();

                Log.Info("**Queried " + attrs.Length + " attributes ("
                         + String.Join(",", attrs) + ") at time NOW" +
                         " on elements: " +
                         "[" + String.Join(",", paths) + "]. " +
                         OsiSettings.QueryType + " query completed. Table built"
                         + " - " + table.RowCount
                         + " rows added in " + sw.Elapsed + "seconds.**");
                //End Logging
            }

            return table;
        }

        private StandaloneTable FillDynamicElementsFields(StandaloneTable table,
            AFAttributeList attrList, int deepestLevel,
            Dictionary<string, AFElement> eleDict = null,
            Dictionary<string, List<string>> inMemoryFilters = null)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            AFValues results = attrList.GetValue(AFTime.Now);
            sw.Stop();

            HashSet<AFElement> filteredOutElements = new HashSet<AFElement>();

            if (inMemoryFilters != null)
            {
                results = (AFValues)PredicateTrimElementsResultsSet(
                    results, inMemoryFilters, out filteredOutElements);
            }

            foreach (var aliases in aggregateAliases.Values)
            {
                foreach (var alias in aliases)
                {
                    if (!table.ContainsColumn(alias.Key))
                        table.AddColumn(new NumericColumn(alias.Key));
                }             
            }

            //param: take a different or subset of elements
            eleDict = eleDict ?? SelectedElements;

            foreach (var ele in eleDict)
            {
                if (filteredOutElements.Contains(ele.Value)) continue;

                Row currRow = table.AddRow(new object[table.ColumnCount]);

                for (int i = 0; i < visibleSelectedAttrNames.Count(); i++)
                {
                    if (results.Count == 0) continue;

                    if (!results[0].IsGood)
                    {
                        results.RemoveAt(0);
                        continue;
                    }

                    if(ele.Value != results[0].Attribute.Element) continue;

                    Column valCol;
                    if (aggregateAliases.ContainsKey(visibleSelectedAttrNames[i]))
                    {
                        foreach (var alias in aggregateAliases
                            [visibleSelectedAttrNames[i]])
                        {
                            valCol = table.GetColumn(alias.Key);
                            if (valCol != null)
                            {
                                valCol.SetValue(currRow, 
                                    ApplyAggregateType(
                                        Double.Parse(results[0].Value.ToString()),
                                        alias.Value));
                            }
                        }
                    }
                    else
                    {
                        valCol = table.GetColumn(results[0].Attribute.Name);
                        if (valCol != null)
                            valCol.SetValue(currRow, results[0].Value);
                    }

                    results.RemoveAt(0);
                }

                table = FillStaticElementsFields(table, currRow, ele.Key,
                    ele.Value, deepestLevel);
            }

            if (OsiSettings.IsLoggingEnabled)
            {
                //Logging
                HashSet<string> uniqueAttrNames = new HashSet<string>();
                foreach (AFAttribute attr in attrList)
                {
                    uniqueAttrNames.Add(attr.Name);
                }
                sw.Stop();

                Log.Info("**Queried " + uniqueAttrNames.Count + " attributes ("
                         + String.Join(",", uniqueAttrNames) + ") at time NOW" +
                         " on elements: " +
                         "[" + String.Join(",", eleDict.Values) + "]. " +
                         OsiSettings.QueryType + " query completed in " 
                         + sw.Elapsed + "seconds.**");
                //End Logging
            }

            return table;
        }

        private StandaloneTable FillStaticElementsFields(StandaloneTable table,
            Row currRow, string elePath, AFElement ele, int deepestLevel)
        {
            string[] elePatharr = elePath.Split(new[] { '\\' },
                        StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < deepestLevel; i++)
            {
                if (!onDemandVisibleColumns.Contains(
                    String.Format(Properties.Resources.ColLevel + "{0}", i + 1)))
                    continue;

                Column levelColumn
                    = table.GetColumn(
                    String.Format(Properties.Resources.ColLevel + "{0}", i + 1));

                if (levelColumn == null) break;

                levelColumn.SetValue(currRow,
                    i < elePatharr.Length ? elePatharr[i] : null);
            }

            //other columns
            Column currCol;
            if (onDemandVisibleColumns.Contains(Properties.Resources.ColEle))
            {
                currCol = table.GetColumn(Properties.Resources.ColEle);
                if (currCol != null)
                    currCol.SetValue(currRow, elePatharr[elePatharr.Length - 1]);
            }

            if (onDemandVisibleColumns.Contains(Properties.Resources.ColSystem))
            {
                currCol = table.GetColumn(Properties.Resources.ColSystem);
                if (currCol != null)
                    currCol.SetValue(currRow, OsiSettings.SystemName);
            }

            if (onDemandVisibleColumns.Contains(Properties.Resources.ColDb))
            {
                currCol = table.GetColumn(Properties.Resources.ColDb);
                if (currCol != null)
                    currCol.SetValue(currRow, OsiSettings.Database);
            }

            if (onDemandVisibleColumns.Contains(Properties.Resources.ColElePath))
            {
                currCol = table.GetColumn(Properties.Resources.ColElePath);
                if (currCol != null)
                    currCol.SetValue(currRow, elePath);
            }

            if (onDemandVisibleColumns.Contains(Properties.Resources.ColEleCat))
            {
                currCol = table.GetColumn(Properties.Resources.ColEleCat);
                if (currCol != null && ele.Categories.Count > 0)
                    currCol.SetValue(currRow, ele.Categories[0]);
            }

            return table;
        }

        //OnDemand Elements Methods
        public StandaloneTable OnDemandPopulateElementsTable(StandaloneTable table)
        {
            int deepestLevel = GetDeepestLevelInSelected();

            //Predicated Query
            if (HasPredicates)
            {
                return FillPredicatedElementsTable(table, deepestLevel);
            }

            //Regular Query
            if (!NeedToQuery())
            {
                foreach (var ele in SelectedElements)
                {
                    Row currRow = table.
                        AddRow(new object[table.ColumnCount]);

                    table = FillStaticElementsFields(table, currRow, ele.Key,
                        ele.Value, deepestLevel);
                }
            }
            else
            {
                AFAttributeList attrList =
                    (AFAttributeList)GetAllSelectedAttributes(visibleSelectedAttrNames);

                table = FillDynamicElementsFields(table, attrList, deepestLevel);
            }


            return table;
        }

        public StandaloneTable CreateElementDomainTable()
        {
            if (!HasDomains) return new StandaloneTable();
            string domain = OnDemandParams.Domains[0];
            StandaloneTable domainTable = new StandaloneTable();

            domainTable = BuildElementsTable(domainTable);
            Column domainCol = domainTable.GetColumn(domain);
            if (domainCol == null) return domainTable;

            Row currRow;

            if (!NeedDomainQuery(domain))
            {
                switch (domain)
                {
                    case "Element":
                        foreach (var ele in SelectedElements)
                        {
                            currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                            domainCol.SetValue(currRow, ele.Value.Name);
                        }
                        break;
                    case "Element Path":
                        foreach (var ele in SelectedElements)
                        {
                            currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                            domainCol.SetValue(currRow, ele.Key);
                        }
                        break;
                    case "PI System":
                        currRow = domainTable.AddRow(
                            new object[domainTable.ColumnCount]);
                        domainCol.SetValue(currRow, OsiSettings.SystemName);
                        break;
                    case "Database":
                        currRow = domainTable.AddRow(
                            new object[domainTable.ColumnCount]);
                        domainCol.SetValue(currRow, OsiSettings.Database);
                        break;
                    //Level Column
                    default:
                        string level = domain.Remove(0, 5);
                        int levelNum;
                        bool success = Int32.TryParse(level, out levelNum);
                        if (!success) break;

                        foreach (var ele in SelectedElements)
                        {
                            string[] pathArr = ele.Key.Split(new[] { '\\' },
                                StringSplitOptions.RemoveEmptyEntries);

                            if (levelNum > pathArr.Length) continue;

                            currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                            domainCol.SetValue(currRow, pathArr[levelNum - 1]);
                        }

                        break;
                }
            }
            else
            {
                List<string> attrNameList = new List<string>(GetAttributeList());

                if (attrNameList.Contains(domain))
                {
                    //find AFAttributes, query and return values
                    AFAttributeList attrList =
                            (AFAttributeList)GetAllSelectedAttributes
                            (new List<string>(new[] { domain }));

                    AFValues results = attrList.GetValue(AFTime.Now);

                    foreach (AFValue value in results)
                    {
                        currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                        domainCol.SetValue(currRow, value.Value.ToString());
                    }

                    return domainTable;
                }
            }

            return domainTable;
        }

        private StandaloneTable FillPredicatedElementsTable(
            StandaloneTable table, int deepestLevel)
        {
            Dictionary<string, List<string>> inMemoryFilters
                = new Dictionary<string, List<string>>();

            Dictionary<string, AFElement> filteredElements
                = new Dictionary<string, AFElement>(SelectedElements);

            Dictionary<string, List<string>> predDict
                = base.GetOnDemandInclusivePredicates();

            foreach (var pred in predDict)
            {
                //PISystem and Database get checked here, if these contain
                //no values, then there is nothing to return for this table.
                if (elementsNonQueryColumns.Contains(pred.Key)
                    && pred.Value.Count == 0)
                    return table;

                bool isAttributeCol = false;
                List<string> keysToRemove = new List<string>();

                switch (pred.Key)
                {
                    case "Element":
                        foreach (var ele in filteredElements)
                        {
                            if (!pred.Value.Contains(ele.Value.Name))
                            {
                                keysToRemove.Add(ele.Key);
                            }
                        }
                        break;
                    case "Element Category":
                        foreach (var ele in filteredElements)
                        {
                            if (ele.Value.Categories.Count > 0
                                && !pred.Value.Contains(
                                    ele.Value.Categories[0].Name))
                            {
                                keysToRemove.Add(ele.Key);
                            }
                        }
                        break;
                    case "Element Path":
                        foreach (var ele in filteredElements)
                        {
                            if (!pred.Value.Contains(ele.Key))
                            {
                                keysToRemove.Add(ele.Key);
                            }
                        }
                        break;
                    default:
                        if (pred.Key.StartsWith("Level"))
                        {
                            string level = pred.Key.Remove(0, 5);
                            int levelNum;
                            bool success = Int32.TryParse(level, out levelNum);
                            if (!success) continue;
                            foreach (var ele in filteredElements)
                            {
                                string[] path = ele.Key.Split(new[] { '\\' },
                                    StringSplitOptions.RemoveEmptyEntries);
                                //check the value at given level col
                                if (!pred.Value.Contains(path[levelNum - 1]))
                                {
                                    keysToRemove.Add(ele.Key);
                                }
                            }
                        }
                        else
                        {
                            isAttributeCol = true;
                        }
                        break;
                }

                //take all the elements that don't fit out needs filter out.
                foreach (string key in keysToRemove)
                    filteredElements.Remove(key);

                if (isAttributeCol)
                {
                    inMemoryFilters.Add(pred.Key, pred.Value);
                }
            }

            if (inMemoryFilters.Count > 0)
            {
                CheckAttributePIPoints();
            }

            AFAttributeList attrList = (AFAttributeList)
                GetPredicateFilteredAttributes(null,
                new List<string>(filteredElements.Select(element
                    => element.Value.Name)),
                new List<string>(visibleSelectedAttrNames));

            Stopwatch sw = new Stopwatch();
            if(OsiSettings.IsLoggingEnabled)
                sw.Start();

            attrList.GetValue(AFTime.Now);

            if (OsiSettings.IsLoggingEnabled)
            {
                sw.Stop();

                List<string> paths = predDict.ContainsKey(Properties.Resources.ColEle) ?
                    predDict[Properties.Resources.ColEle]
                    : new List<string>(GetPathList());
                List<string> attrs = visibleSelectedAttrNames;

                Log.Info("**Queried " + attrs.Count + " attributes ("
                         + String.Join(",", attrs) + ") at time NOW"
                         + " on elements: [" + String.Join(",", paths)
                         + "]. "
                         + OsiSettings.QueryType + " - " +
                         OsiSettings.PointText
                         + " query completed in " + sw.Elapsed + "seconds.**");
            }
            

            table = FillDynamicElementsFields(table, attrList, deepestLevel,
                filteredElements, inMemoryFilters);

            return table;
        }

        private IEnumerable<AFValue> PredicateTrimElementsResultsSet
            (IEnumerable<AFValue> results,
            Dictionary<string, List<string>> inMemoryFilters,
            out HashSet<AFElement> filteredOutElements)
        {
            AFValues resultsList = (AFValues)results;

            filteredOutElements = new HashSet<AFElement>();

            foreach (var filter in inMemoryFilters)
            {
                if (!HasPIPoint.ContainsKey(filter.Key)) continue;

                switch (HasPIPoint[filter.Key].Value)
                {
                    case ColumnType.Numeric:
                        foreach (AFValue val in resultsList)
                        {
                            if (!val.IsGood || val.Attribute.Name != filter.Key)
                                continue;

                            double lower;
                            double upper;

                            bool hasLower = Double.TryParse(filter.Value[0], out lower);
                            bool hasUpper = Double.TryParse(filter.Value[1], out upper);

                            if ((hasLower && hasUpper) &&
                                (Double.Parse(val.Value.ToString()) < lower
                                && (Double.Parse(val.Value.ToString()) > upper)))
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);

                            else if (hasLower
                                && Double.Parse(val.Value.ToString()) < lower)
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);

                            else if (hasUpper
                                     && Double.Parse(val.Value.ToString()) > upper)
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);
                        }
                        break;
                    case ColumnType.Text:
                        foreach (AFValue val in resultsList)
                        {
                            if (!val.IsGood || val.Attribute.Name != filter.Key)
                                continue;

                            if (!filter.Value.Contains(val.Value.ToString()))
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);
                        }
                        break;
                    case ColumnType.Time:
                        foreach (AFValue val in resultsList)
                        {
                            if (!val.IsGood || val.Attribute.Name != filter.Key)
                                continue;
                            //create AFTimes with predcate range
                            AFTime start = new AFTime();
                            AFTime end = new AFTime();

                            bool hasStart = !String.IsNullOrWhiteSpace(filter.Value[0]);
                            bool hasEnd = !String.IsNullOrWhiteSpace(filter.Value[1]);

                            //populates text filters to comparable objects
                            if (hasStart)
                            {
                                start = new AFTime(filter.Value[0]);
                            }
                            if (hasEnd)
                            {
                                end = new AFTime(filter.Value[1]);
                            }

                            //comparison
                            AFTime attrTime = new AFTime(val.Value.ToString());

                            if ((hasStart && hasEnd) && (start.LocalTime.CompareTo(
                                attrTime.LocalTime) < 0 && end.LocalTime.CompareTo(
                                attrTime.LocalTime) > 0))
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);

                            else if (hasStart && start.LocalTime.CompareTo(
                                attrTime.LocalTime) < 0)
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);

                            else if (hasEnd && end.LocalTime.CompareTo(
                                attrTime.LocalTime) > 0)
                                filteredOutElements.Add(
                                    (AFElement)val.Attribute.Element);
                        }

                        break;
                }
            }

            //remove the attribute values of elements that we are discarding
            for (int i = 0; i < resultsList.Count; i++)
            {
                if (filteredOutElements.Contains(
                    (AFElement)resultsList[i].Attribute.Element))
                {
                    resultsList.RemoveAt(i);
                    i--;
                }
            }

            return resultsList;
        }
        #endregion

        #region Historic
        //Historic Methods
        public StandaloneTable BuildHistoricTable(StandaloneTable table)
        {
            table.AddColumn(new TextColumn(Properties.Resources.ColEle));
            table.AddColumn(new TextColumn(Properties.Resources.ColSystem));
            table.AddColumn(new TextColumn(Properties.Resources.ColAttribute));
            table.AddColumn(new TextColumn(Properties.Resources.ColDb));
            table.AddColumn(new TextColumn(Properties.Resources.ColEventPath));
            table.AddColumn(new TimeColumn(Properties.Resources.ColTime));

            List<string> attrNameList = new List<string>(GetAttributeList());
            foreach (AFElement element in SelectedElements.Values)
            {
                foreach (string attrName in attrNameList)
                {
                    if (table.ContainsColumn(attrName)) continue;

                    AFAttribute attr;
                    try
                    {
                        attr = element.Attributes[attrName];
                    }
                    catch (Exception)
                    {
                        Log.Info(
                            String.Format(
                                Properties.Resources.LogElementAttributeError,
                                attrName, element.Name));
                        continue;
                    }

                    Type t = attr.Type;
                    if (t == typeof(int) || t == typeof(double))
                        table.AddColumn(new NumericColumn(attr.Name));

                    else if (t == typeof(DateTime))
                        table.AddColumn(new TimeColumn(attr.Name));

                    else
                        table.AddColumn(new TextColumn(attr.Name));
                }
            }

            return table;
        }

        public StandaloneTable PopulateHistoricTable(
            StandaloneTable table, int rowCount)
        {
            //disable sample Table
            if (rowCount == 10) return table;

            AFAttributeList attrList =
                (AFAttributeList)GetAllSelectedAttributes();

            if (attrList == null || attrList.Count == 0) return table;

            HistoricAttributeValueQuery(attrList, table, rowCount);

            return table;
        }

        private StandaloneTable FillStaticHistoricFields(StandaloneTable table,
            Row currRow, KeyValuePair<string, AFElement> ele, string attrName)
        {
            Column currCol;
            if (onDemandVisibleColumns.Contains(Properties.Resources.ColEle))
            {
                currCol = table.GetColumn(Properties.Resources.ColEle);
                if (currCol != null)
                    currCol.SetValue(currRow, ele.Value.Name);
            }
            if (onDemandVisibleColumns.Contains(Properties.Resources.ColSystem))
            {
                currCol = table.GetColumn(Properties.Resources.ColSystem);
                if (currCol != null)
                    currCol.SetValue(currRow, OsiSettings.SystemName);
            }
            if (onDemandVisibleColumns.Contains(Properties.Resources.ColAttribute))
            {
                currCol = table.GetColumn(Properties.Resources.ColAttribute);
                if (currCol != null)
                    currCol.SetValue(currRow, attrName);
            }
            if (onDemandVisibleColumns.Contains(Properties.Resources.ColDb))
            {
                currCol = table.GetColumn(Properties.Resources.ColDb);
                if (currCol != null)
                    currCol.SetValue(currRow, OsiSettings.Database);
            }
            if (onDemandVisibleColumns.Contains(Properties.Resources.ColEventPath))
            {
                currCol = table.GetColumn(Properties.Resources.ColEventPath);
                if (currCol != null)
                    currCol.SetValue(currRow, ele.Key + "|" + attrName);
            }
            return table;
        }

        public void HistoricAttributeValueQuery(
            AFAttributeList dataAttrs,
            StandaloneTable table, int rowCount)
        {
            try
            {
                var parameterizedStartTime =
                    OsiSettings.ParameterizeValue(OsiSettings.StartTime);
                var parameterizedEndTime =
                    OsiSettings.ParameterizeValue(OsiSettings.EndTime);
                var timeRange =
                    new AFTimeRange(parameterizedStartTime,
                        parameterizedEndTime);

                List<Task> queryTasks = new List<Task>();
                TableUpdateManager tUpdater = new TableUpdateManager(table);
                tUpdater.Start();

                Stopwatch sw = new Stopwatch();

                if (OsiSettings.IsLoggingEnabled)
                    sw.Start();

                queryTasks.Add(Task.Factory.StartNew(
                    () =>
                        HistoricAttributeQueryThread(dataAttrs, timeRange,
                            tUpdater, rowCount)));

                while (!queryTasks.TrueForAll(element => element.IsCompleted))
                {
                    Thread.Sleep(200);
                }

                while (tUpdater.HasRowsQueued)
                {
                    Thread.Sleep(200);
                }

                //Logging
                if (OsiSettings.IsLoggingEnabled && !OsiSettings.IsQueryOnDemand)
                {
                    string[] paths = GetPathList();
                    string[] attrs = GetAttributeList();
                    sw.Stop();
                    Log.Info("**Queried " + attrs.Length + " attributes ("
                             + String.Join(",", attrs) + ") from "
                             +
                             OsiSettings.ParameterizeValue(OsiSettings.StartTime)
                             + " to "
                             + OsiSettings.ParameterizeValue(OsiSettings.EndTime)
                             + " on elements: [" + String.Join(",", paths)
                             + "]. "
                             + OsiSettings.QueryType + " - " +
                             OsiSettings.PointText
                             + " query completed. Table built"
                             + " - " + table.RowCount
                             + " rows added in " + sw.Elapsed + "seconds.**");
                    //End Logging
                }

                tUpdater.Stop();
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }

        private void HistoricAttributeQueryThread(
            AFAttributeList attrList,
            AFTimeRange timeRange, TableUpdateManager tUpdate, int rowCount)
        {
            if (attrList.Count == 0) return;

            var parameterizedFilterExpression =
                OsiSettings.ParameterizeValue(OsiSettings.FilterExpression);

            IEnumerable<AFValues> results;

            int queryRowCount = rowCount == 10 ? 10 : 0;

            if (HasPredicates)
            {
                results = HistoricPredicateQuery();
            }
            //Regular attribute query - No Predicates or Non-OnDemand
            else
            {
                switch (OsiSettings.PointType)
                {
                    case PointType.PlottedValues:
                        PIPagingConfiguration pageConfig =
                            new PIPagingConfiguration(PIPageType.TagCount, 10);
                        int intervals =
                            Int32.Parse(
                                OsiSettings.ParameterizeValue(
                                    OsiSettings.PlotIntervals));
                        int resolution =
                            (int)Math.Ceiling(SystemParameters.VirtualScreenWidth);

                        results = attrList.Data.PlotValues(timeRange,
                            intervals > resolution ? resolution : intervals,
                            pageConfig);
                        break;
                    case PointType.RecordedPoints:
                        PIPagingConfiguration pageConfigRecorded =
                            new PIPagingConfiguration(PIPageType.TagCount, 10);

                        results = attrList.Data.RecordedValues(timeRange,
                            AFBoundaryType.Inside, parameterizedFilterExpression,
                            false, pageConfigRecorded, queryRowCount);
                        break;

                    default:
                        throw new Exception(
                            String.Format("Query type not supported: {0}",
                                OsiSettings.ParameterizeValue(
                                OsiSettings.PointText)));
                }
            }

            foreach (AFValues values in results)
            {
                foreach (AFValue value in values)
                {
                    if (!value.IsGood) continue;

                    bool isAggregate = false;
                    //if we are dealing with numeric column and the aggregate 
                    //column for our respective value doesn't exist, create it.
                    if (aggregateAliases.ContainsKey(value.Attribute.Name))
                    {
                        isAggregate = true;
                        //-1 if col doesn't exist the table
                        foreach (var alias 
                            in aggregateAliases[value.Attribute.Name])
                        {
                            if (tUpdate.GetColumnIndex(alias.Key) == -1)
                            {
                                tUpdate.AddColumn(alias.Key, ColumnType.Numeric);
                            }
                        }
                    }

                    object[] row = new object[tUpdate.TableColumnCount];

                    //Element
                    row[tUpdate.GetColumnIndex(Properties.Resources.ColEle)]
                        = value.Attribute.Element.Name;
                    //Event PI System
                    row[tUpdate.GetColumnIndex(Properties.Resources.ColSystem)]
                        = OsiSettings.SystemName;
                    //Attribute
                    row[tUpdate.GetColumnIndex(Properties.Resources.ColAttribute)]
                        = value.Attribute.Name;
                    //Event Database
                    row[tUpdate.GetColumnIndex(Properties.Resources.ColDb)]
                        = OsiSettings.Database;
                    //Event Path
                    row[tUpdate.GetColumnIndex(Properties.Resources.ColEventPath)]
                        = value.Attribute.GetPath();
                    //TimeStamp
                    row[tUpdate.GetColumnIndex(Properties.Resources.ColTime)]
                        = value.Timestamp.LocalTime;

                    //Attribute Value
                    if (isAggregate)
                    {
                        foreach (var alias 
                            in aggregateAliases[value.Attribute.Name])
                        {
                            row[tUpdate.GetColumnIndex(alias.Key)]
                                = ApplyAggregateType(
                                    Double.Parse(value.Value.ToString()),
                                    alias.Value);
                        }
                    }
                    else
                    {
                        row[tUpdate.GetColumnIndex(value.Attribute.Name)]
                            = value.Value.ToString();
                    }

                    tUpdate.EnqueueRow(row);
                }
            }
        }

        //OnDemand Historic Methods
        public StandaloneTable OnDemandPopulateHistoricTable(StandaloneTable table)
        {
            if (!NeedToQuery())
            {
                foreach (var ele in SelectedElements)
                {
                    foreach (string attrName in GetAttributeList())
                    {
                        Row currRow = table.AddRow(new object[table.ColumnCount]);

                        table = FillStaticHistoricFields(table, currRow,
                            ele, attrName);
                    }
                }
            }
            else
            {
                AFAttributeList attrList =
                    (AFAttributeList) GetAllSelectedAttributes(visibleSelectedAttrNames);

                HistoricAttributeValueQuery(attrList, table, -1);
            }

            return table;
        }

        public StandaloneTable CreateHistoricDomainTable()
        {
            if (!HasDomains) return new StandaloneTable();
            string domain = OnDemandParams.Domains[0];
            StandaloneTable domainTable = new StandaloneTable();

            domainTable = BuildHistoricTable(domainTable);
            Column domainCol = domainTable.GetColumn(domain);
            if (domainCol == null) return domainTable;

            bool isTimeseries = false;
            string timestamp = "";
            Column timeColumn = null;
            if (HasAuxiliaryCols)
            {
                foreach (string auxCol in OnDemandParams.AuxiliaryColumns)
                {
                    if (auxCol == Properties.Resources.ColTime)
                    {
                        //Is timeseries domain table
                        isTimeseries = true;

                        timestamp = new AFTime(
                            OsiSettings.ParameterizeValue(
                                OsiSettings.StartTime)).LocalTime.ToString
                            (CultureInfo.InvariantCulture);

                        timeColumn = domainTable.GetColumn("TimeStamp");
                        if (timeColumn == null) return domainTable;
                    }
                }
            }

            Row currRow;

            if (!NeedDomainQuery(domain))
            {
                switch (domain)
                {
                    case "Element":
                        foreach (var ele in SelectedElements)
                        {
                            currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                            domainCol.SetValue(currRow, ele.Value.Name);

                            if (isTimeseries)
                                timeColumn.SetValue(currRow, timestamp);   
                        }
                        break;
                        
                    case "PI System":
                        currRow = domainTable.AddRow(
                            new object[domainTable.ColumnCount]);
                        domainCol.SetValue(currRow, OsiSettings.SystemName);

                        if (isTimeseries) 
                            timeColumn.SetValue(currRow, timestamp);                        
                        break;

                    case "Attribute":
                        foreach (string attrName in GetAttributeList())
                        {
                            currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                            domainCol.SetValue(currRow, attrName);

                            if (isTimeseries)
                                timeColumn.SetValue(currRow, timestamp);   
                        }
                        break;

                    case "Database":
                        currRow = domainTable.AddRow(
                            new object[domainTable.ColumnCount]);
                        domainCol.SetValue(currRow, OsiSettings.Database);

                        if (isTimeseries)
                            timeColumn.SetValue(currRow, timestamp);   
                        break;

                    case "Event Path":
                        foreach (var ele in SelectedElements)
                        {
                            foreach (string attrName in GetAttributeList())
                            {
                                currRow = domainTable.AddRow(
                                    new object[domainTable.ColumnCount]);
                                domainCol.SetValue(currRow, ele.Key + "|"
                                    + attrName);

                                if (isTimeseries)
                                    timeColumn.SetValue(currRow, timestamp);   
                            }
                        }
                        break;
                }
            }
            else
            {
                List<string> attrNameList = new List<string>(GetAttributeList());

                if (attrNameList.Contains(domain))
                {
                    AFAttributeList attrList =
                            (AFAttributeList)GetAllSelectedAttributes
                            (new List<string>(new[] { domain }));

                    HistoricAttributeValueQuery(attrList, domainTable, -1);

                    return domainTable;
                }
                if (domain == Properties.Resources.ColTime)
                {
                    AFTime start = new AFTime(OsiSettings.StartTime);
                    currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                    domainCol.SetValue(currRow, start.LocalTime);

                    AFTime end = new AFTime(OsiSettings.EndTime);
                    currRow = domainTable.AddRow(
                                new object[domainTable.ColumnCount]);
                    domainCol.SetValue(currRow, end.LocalTime);
                }
            }

            return domainTable;
        }

        private IEnumerable<AFValues> PredicateTrimHistoricResultsSet(
            IEnumerable<AFValues> results,
            Dictionary<string, List<string>> inMemoryFilters)
        {
            foreach (var filter in inMemoryFilters)
            {
                AFValues[] resultsArr = results as AFValues[] ?? results.ToArray();

                List<AFValues> trimmedResults = new List<AFValues>();

                //ColumnType
                switch (HasPIPoint[filter.Key].Value)
                {
                    case ColumnType.Numeric:
                        double lower;
                        double upper;

                        bool hasLower = Double.TryParse(filter.Value[0], out lower);
                        bool hasUpper = Double.TryParse(filter.Value[1], out upper);

                        foreach (AFValues vals in resultsArr)
                        {
                            if (vals.Count > 0
                                && vals[0].Attribute.Name == filter.Key)
                            {
                                for (int i = 0; i < vals.Count; i++)
                                {
                                    AFValue val = vals[i];

                                    if (!val.IsGood) continue;
                                    bool remove = false;

                                    if (hasLower && hasUpper)
                                    {
                                        if (Double.Parse(val.Value.ToString()) >= lower
                                            && Double.Parse(val.Value.ToString()) <= upper)
                                            continue;
                                        remove = true;
                                    }
                                    else if (hasLower)
                                    {
                                        if (Double.Parse(val.Value.ToString()) >= lower)
                                            continue;
                                        remove = true;
                                    }
                                    else if (hasUpper)
                                    {
                                        if (Double.Parse(val.Value.ToString()) <= upper)
                                            continue;
                                        remove = true;
                                    }

                                    if (!remove) continue;
                                    vals.RemoveAt(i);
                                    i--;
                                }
                            }
                            trimmedResults.Add(vals);
                        }
                        break;

                    case ColumnType.Text:
                        foreach (AFValues vals in resultsArr)
                        {
                            if (vals.Count > 0
                                && vals[0].Attribute.Name == filter.Key)
                            {
                                for (int i = 0; i < vals.Count; i++)
                                {
                                    AFValue val = vals[i];

                                    if (!val.IsGood) continue;

                                    if (filter.Value.Contains(val.Value)) continue;

                                    vals.RemoveAt(i);
                                    i--;
                                }
                            }
                            trimmedResults.Add(vals);
                        }
                        break;

                    case ColumnType.Time:
                        //create AFTimes with predcate range
                        AFTime start = new AFTime();
                        AFTime end = new AFTime();

                        bool hasStart = !String.IsNullOrWhiteSpace(filter.Value[0]);
                        bool hasEnd = !String.IsNullOrWhiteSpace(filter.Value[1]);

                        //populates text filters to comparable objects
                        if (hasStart)
                        {
                            start = new AFTime(filter.Value[0]);
                        }
                        if (hasEnd)
                        {
                            end = new AFTime(filter.Value[1]);
                        }

                        foreach (AFValues vals in resultsArr)
                        {
                            if (vals.Count > 0
                                && vals[0].Attribute.Name == filter.Key)
                            {
                                for (int i = 0; i < vals.Count; i++)
                                {
                                    AFValue val = vals[i];

                                    if (!val.IsGood) continue;

                                    //comparison
                                    AFTime attrTime = new AFTime(val.Value.ToString());

                                    if (hasStart && start.LocalTime.CompareTo(
                                        attrTime.LocalTime) >= 0) continue;
                                    if (hasEnd && end.LocalTime.CompareTo(
                                        attrTime.LocalTime) <= 0) continue;

                                    vals.RemoveAt(i);
                                    i--;
                                }
                            }
                            trimmedResults.Add(vals);
                        }
                        break;
                }
                results = trimmedResults;
            }
            return results;
        }

        private IEnumerable<AFValues> HistoricPredicateQuery()
        {
            //Filter objects
            List<string> attrFilter = new List<string>();
            List<string> eleFilter = new List<string>();
            Dictionary<string, List<string>> evPathDict
                = new Dictionary<string, List<string>>();
            StringBuilder attributePredFilterString = new StringBuilder();
            Dictionary<string, List<string>> inMemoryFilters
                = new Dictionary<string, List<string>>();

            AFTimeRange timeRange = new AFTimeRange(
                new AFTime(OsiSettings.ParameterizeValue(OsiSettings.StartTime)),
                new AFTime(OsiSettings.ParameterizeValue(OsiSettings.EndTime)));

            //Get the Predicates we need to eval now.
            Dictionary<string, List<string>> predDict
                = base.GetOnDemandInclusivePredicates();


            foreach (var kvp in predDict)
            {
                //PISystem and Database get checked here
                if (historicNonQueryColumns.Contains(kvp.Key)
                    && kvp.Value.Count == 0)
                    return new List<AFValues>();

                bool isAttributeCol = false;

                //switch on column name
                switch (kvp.Key)
                {
                    case "Attribute":
                        attrFilter = kvp.Value;
                        break;
                    case "Element":
                        eleFilter = kvp.Value;
                        break;

                    //Chunk it up and eval similar to Attr/Ele
                    case "Event Path":
                        foreach (string evPath in kvp.Value)
                        {
                            string[] evPathArr = evPath.Split(new[] { '|' },
                                StringSplitOptions.RemoveEmptyEntries);

                            //evPathArr[0] is the ele path
                            string[] elePathArr = evPathArr[0].Split(new[] { '\\' },
                                StringSplitOptions.RemoveEmptyEntries);

                            string eleName = elePathArr[elePathArr.Length - 1];

                            //evPathArr[1] is the attr name
                            evPathDict.Add(evPath,
                                new List<string>(new[] { eleName, evPathArr[1] }));
                        }
                        break;

                    //replace range
                    case "TimeStamp":
                        //kvp.Value[0] is change in start time
                        if (!String.IsNullOrWhiteSpace(kvp.Value[0]))
                            timeRange.StartTime = new AFTime(kvp.Value[0]);
                        //kvp.Value[1] is change in end time
                        if (!String.IsNullOrWhiteSpace(kvp.Value[1]))
                            timeRange.EndTime = new AFTime(kvp.Value[1]);
                        break;

                    default:
                        isAttributeCol = true;
                        break;
                }

                //only recorded points can handle a filter string.
                if (isAttributeCol 
                    && OsiSettings.PointType == PointType.RecordedPoints)
                {
                    CheckAttributePIPoints();

                    //If Pipoint exists, can use server-side filter expression
                    if (HasPIPoint[kvp.Key].Key)
                    {
                        //ColumnType
                        switch (HasPIPoint[kvp.Key].Value)
                        {
                            case ColumnType.Numeric:
                            case ColumnType.Time:

                                if (!String.IsNullOrWhiteSpace(
                                    attributePredFilterString.ToString()))
                                {
                                    attributePredFilterString.Append(" AND ");
                                }

                                //ranges - use >,<,>=,etc
                                if (!String.IsNullOrWhiteSpace(kvp.Value[0]))
                                {
                                    attributePredFilterString.Append(
                                    "'" + kvp.Key + "' >= " + kvp.Value[0]);

                                    if (!String.IsNullOrWhiteSpace(kvp.Value[1]))
                                    {
                                        attributePredFilterString.Append(" AND " +
                                           "'" + kvp.Key + "' <= " + kvp.Value[1]);
                                    }
                                }
                                else if (!String.IsNullOrWhiteSpace(kvp.Value[1]))
                                {
                                    attributePredFilterString.Append(
                                        "'" + kvp.Key + "' <= " + kvp.Value[1]);
                                }
                                break;
                            //text, list equality statements
                            default:
                                for (int i = 0; i < kvp.Value.Count; i++)
                                {
                                    if (i + 1 == kvp.Value.Count)
                                    {
                                        attributePredFilterString.Append(
                                            "'" + kvp.Key + "' == "
                                            + "\"" + kvp.Value[i] + "\"");
                                    }
                                    else
                                    {
                                        attributePredFilterString.Append(
                                            "'" + kvp.Key + "' == "
                                            + "\"" + kvp.Value[i] + "\""
                                            + " OR ");
                                    }
                                }
                                break;
                        }
                    }
                    else
                    {
                        inMemoryFilters.Add(kvp.Key, kvp.Value);
                    }
                }
            }

            //Request for specific element/attribute combos. Further filter by
            //element and attribute preds if they exist.
            AFAttributeList attrList = (AFAttributeList)
                GetPredicateFilteredAttributes(evPathDict, eleFilter, attrFilter);

            IEnumerable<AFValues> results = new List<AFValues>();

            Stopwatch sw = new Stopwatch();
            if (OsiSettings.IsLoggingEnabled)
                sw.Start();
            //Use all the information gathered from predicates to preform 
            //the new query.
            switch (OsiSettings.PointType)
            {
                case PointType.PlottedValues:
                    PIPagingConfiguration pageConfig =
                        new PIPagingConfiguration(PIPageType.TagCount, 10);
                    int intervals =
                        Int32.Parse(
                            OsiSettings.ParameterizeValue(
                                OsiSettings.PlotIntervals));
                    int resolution =
                        (int)Math.Ceiling(SystemParameters.VirtualScreenWidth);

                    results = attrList.Data.PlotValues(timeRange,
                        intervals > resolution ? resolution : intervals,
                        pageConfig);
                    break;
                case PointType.RecordedPoints:
                    PIPagingConfiguration pageConfigRecorded =
                        new PIPagingConfiguration(PIPageType.TagCount, 10);

                    results = attrList.Data.RecordedValues(timeRange,
                        AFBoundaryType.Inside, attributePredFilterString.ToString(),
                        false, pageConfigRecorded, 0);
                    break;
            }

            //Logging
            if (OsiSettings.IsLoggingEnabled)
            {
                sw.Stop();
                List<string> paths = predDict.ContainsKey(Properties.Resources.ColEle) ?
                    predDict[Properties.Resources.ColEle] 
                    : new List<string>(GetPathList());
                List<string> attrs = visibleSelectedAttrNames;
                Log.Info("**Queried " + attrs.Count + " attributes ("
                         + String.Join(",", attrs) + ") from "
                         +
                         OsiSettings.ParameterizeValue(OsiSettings.StartTime)
                         + " to "
                         + OsiSettings.ParameterizeValue(OsiSettings.EndTime)
                         + " on elements: [" + String.Join(",", paths)
                         + "]. "
                         + OsiSettings.QueryType + " - " +
                         OsiSettings.PointText
                         + " query completed in " + sw.Elapsed + "seconds.**");
            }
            //End Logging

            //if additional trimming needs to be preformed, do it.
            return inMemoryFilters.Count > 0 ?
                PredicateTrimHistoricResultsSet(results, inMemoryFilters) : results;
        }
        #endregion

        #region Event Frame
        //Event Frame Methods
        public StandaloneTable BuildEventFrameTable(StandaloneTable table)
        {
            table.AddColumn(new TextColumn("EventFrame Name"));
            table.AddColumn(new TextColumn("Attribute"));
            table.AddColumn(new NumericColumn("Value"));
            table.AddColumn(new TextColumn("Text Value"));
            table.AddColumn(new TimeColumn("Attribute TimeStamp"));
            table.AddColumn(new TimeColumn("EventFrame Start"));
            table.AddColumn(new TimeColumn("EventFrame End"));
            table.AddColumn(new TextColumn("EventFrame ID"));

            return table;
        }

        public StandaloneTable PopulateEventFrameTable(
            StandaloneTable table,
            int rowCount)
        {
            //disable sample Table
            if (rowCount == 10) return table;

            AFEventFrame evFrame
                = FindEventFrameByID(Guid.Parse(OsiSettings.EventFrameGuid));

            if (evFrame == null) return table;

            EventFrameAttributeValueQuery(evFrame, table, rowCount);

            return table;
        }

        private void EventFrameAttributeValueQuery(
            AFEventFrame evFrame,
            StandaloneTable table, int rowCount)
        {
            try
            {
                List<Task> queryTasks = new List<Task>();
                TableUpdateManager tUpdate = new TableUpdateManager(table);
                tUpdate.Start();

                Stopwatch sw = new Stopwatch();

                if (OsiSettings.IsLoggingEnabled)
                    sw.Start();

                queryTasks.Add(Task.Factory.StartNew(
                    () =>
                        EventFrameAttributeQueryThread(evFrame,
                            tUpdate, rowCount)));

                while (!queryTasks.TrueForAll(element => element.IsCompleted))
                {
                    Thread.Sleep(200);
                }

                while (tUpdate.HasRowsQueued)
                {
                    Thread.Sleep(200);
                }

                //Logging
                List<string> attrNames = new List<string>();
                foreach (var attr in evFrame.Attributes)
                {
                    attrNames.Add(attr.Name);
                }

                if (OsiSettings.IsLoggingEnabled)
                {
                    sw.Stop();
                    Log.Info("**Queried " + attrNames.Count + " attributes ("
                             + String.Join(",", attrNames) + ") from " +
                             evFrame.StartTime + " to " + evFrame.EndTime +
                             " on event frame: " + evFrame.Name + ". " +
                             OsiSettings.QueryType + " - " +
                             OsiSettings.PointText
                             + " query completed. Table built. "
                             + table.RowCount
                             + " rows added in " + sw.Elapsed + "seconds.**");
                }
                //End Logging

                tUpdate.Stop();
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }

        private void EventFrameAttributeQueryThread(
            AFEventFrame evFrame,
            TableUpdateManager tUpdate, int rowCount)
        {
            IEnumerable<AFValues> results;

            AFAttributeList attrList = new AFAttributeList(evFrame.Attributes);

            int queryRowCount = rowCount == 10 ? 10 : 0;

            switch (OsiSettings.PointType)
            {
                case PointType.PlottedValues:
                    PIPagingConfiguration pageConfig =
                        new PIPagingConfiguration(PIPageType.TagCount, 100);

                    int intervals =
                        Int32.Parse(
                            OsiSettings.ParameterizeValue(
                                OsiSettings.PlotIntervals));

                    int resolution =
                        (int)Math.Ceiling(SystemParameters.VirtualScreenWidth);

                    results = attrList.Data.PlotValues(evFrame.TimeRange,
                        intervals > resolution ? resolution : intervals,
                        pageConfig);
                    break;

                case PointType.RecordedPoints:
                    PIPagingConfiguration pageConfigRecorded =
                        new PIPagingConfiguration(PIPageType.TagCount, 100);
                    results = attrList.Data.RecordedValues(evFrame.TimeRange,
                        AFBoundaryType.Inside, null,
                        false, pageConfigRecorded, queryRowCount);
                    break;

                default:
                    throw new Exception(
                        String.Format("Query type not supported: {0}",
                            OsiSettings.ParameterizeValue(
                            OsiSettings.PointText)));
            }

            foreach (AFValues values in results)
            {
                foreach (AFValue value in values)
                {
                    if (!value.IsGood) continue;

                    object[] row = new object[tUpdate.TableColumnCount];

                    //EvFrame Name
                    row[0] = evFrame.Name;
                    //Attribute
                    row[1] = value.Attribute.Name;

                    if (value.Attribute.Type == typeof(string)
                        || value.Attribute.Type == typeof(char)
                        || value.Attribute.Type == typeof(bool))
                    {
                        //Text Value
                        row[3] = value.Value.ToString();
                    }
                    else
                    {
                        //Value
                        row[2] = value.Value.ToString();
                    }
                    //Attribute Time
                    row[4] = value.Timestamp.ToString();
                    //Event Start
                    row[5] = evFrame.StartTime.ToString();
                    //Event End
                    row[6] = evFrame.EndTime.ToString();
                    //ID
                    row[7] = evFrame.ID.ToString();


                    //pass to updater to start putting into Table.
                    tUpdate.EnqueueRow(row);
                }
            }
        }

        //todo OnDemand Event Frame Methods
        #endregion

        #region Metadata
        //Metadata Retrieval
        public IList<AFAttribute> GetAllSelectedAttributes(
            IEnumerable<string> selectedAttrNames = null)
        {
            AFAttributeList attrObjList = new AFAttributeList();

            //Attr names can be provided, if not just use what's in settings.
            IEnumerable<string> attrNameList = selectedAttrNames
                        ?? new List<string>(GetAttributeList());

            foreach (AFElement ele in SelectedElements.Values)
            {
                foreach (string attrName in attrNameList)
                {
                    try
                    {
                        attrObjList.Add(ele.Attributes[attrName]);
                    }
                    catch
                    {
                        Log.Info(String.Format(
                            Properties.Resources.LogElementAttributeError,
                            attrName, ele.Name));
                    }
                }
            }
            return attrObjList;
        }

        public int GetDeepestLevelInSelected()
        {
            int deepest = 0;
            foreach (string path in GetPathList())
            {
                int pathbreaks = path.Split('\\').Length;
                deepest = pathbreaks > deepest ? pathbreaks : deepest;
            }
            return deepest;
        }

        private Dictionary<string, AFElement> GetAfElementChildren(
            string elementPath, AFElement element,
            Dictionary<string, AFElement> elements = null,
            HashSet<AFElement> elementsEncountered = null)
        {
            if (elements == null)
                elements = new Dictionary<string, AFElement>();
            if (elementsEncountered == null)
                elementsEncountered = new HashSet<AFElement>();

            if (!elementsEncountered.Contains(element))
            {
                if (element.Elements.Count > 0)
                {
                    elementsEncountered.Add(element);
                    foreach (AFElement child in element.Elements)
                    {
                        string childPath = String.Format("{0}\\{1}",
                            elementPath.TrimEnd('\\'), child.Name);
                        GetAfElementChildren(childPath, child, elements,
                            elementsEncountered);
                    }
                }
                else
                {
                    elements.Add(elementPath, element);
                    elementsEncountered.Add(element);
                }
            }
            return elements;
        }

        public string[] GetSelectedElementNames()
        {
            List<string> eles = new List<string>();
            foreach (string path in GetPathList())
            {
                eles.Add(path.Split(new[] { '\\' },
                    StringSplitOptions.RemoveEmptyEntries)[path.Length - 1]);
            }
            return eles.ToArray();
        }

        public string[] GetPathList(
            IEnumerable<ParameterValue> parameters = null)
        {
            string[] pathList;
            if (OsiSettings.IsTreeSelectionMode)
            {
                pathList = OsiSettings.ElementPaths.Split(
                    new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                string test = OsiSettings.ParameterizeValue(
                    OsiSettings.ManualElementPaths);
                pathList = test.Split(
                    new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            }

            return pathList;
        }

        public string[] GetAttributeList(
            IEnumerable<ParameterValue> parameters = null)
        {
            string[] attrNameList;
            if (OsiSettings.IsTreeSelectionMode)
            {
                attrNameList = OsiSettings.Attributes.Split(
                    new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                attrNameList = OsiSettings.ParameterizeValue(
                    OsiSettings.ManualAttributes).Split(
                        new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            }
            return attrNameList;
        }

        public IEnumerable<AFEventFrame> SearchEventFrames(
            string eleName,
            string evFrameName, string start, string end)
        {
            if (!String.IsNullOrWhiteSpace(start)
                && String.IsNullOrWhiteSpace(end))
            {
                return AFEventFrame.FindEventFrames(Database, null, start, 0,
                    Int32.MaxValue, AFEventFrameSearchMode.ForwardFromStartTime,
                    evFrameName, eleName, null, null, true);
            }

            if (String.IsNullOrWhiteSpace(start)
                && !String.IsNullOrWhiteSpace(end))
            {
                return AFEventFrame.FindEventFrames(Database, null, end, 0,
                    Int32.MaxValue, AFEventFrameSearchMode.BackwardFromStartTime,
                    evFrameName, eleName, null, null, true);
            }

            if (!String.IsNullOrWhiteSpace(start)
                && !String.IsNullOrWhiteSpace(end))
            {
                return AFEventFrame.FindEventFrames(Database, null,
                    AFSearchMode.Inclusive, start, end, evFrameName, eleName,
                    null, null, null, true, AFSortField.Name,
                    AFSortOrder.Ascending, 0, Int32.MaxValue);
            }

            return AFEventFrame.FindEventFrames(Database, null, "*", 0,
                Int32.MaxValue, AFEventFrameSearchMode.BackwardFromStartTime,
                evFrameName, eleName, null, null, true);
        }

        public AFEventFrame FindEventFrameByID(Guid guid)
        {
            return AFEventFrame.FindEventFrame(System, guid);
        }

        /// <summary>
        //  This should only be used by the SelectedElements field. It is more 
        //  efficient to go through that property if you need access to elements.
        //  Otherwise the server is being queried repeatedly to find paths.
        /// </summary>
        /// <param name="elementPaths"></param>
        /// <returns></returns>
        private IDictionary<string, AFElement> GetElementByPaths(
            IEnumerable<string> elementPaths)
        {
            HashSet<AFElement> encounteredElements = new HashSet<AFElement>();
            IDictionary<string, AFElement> elementDictionary =
                new Dictionary<string, AFElement>();

            List<string> simplePaths = new List<string>();
            List<string> complexPaths = new List<string>();
            StringBuilder errorStringBuilder = new StringBuilder();

            if (elementPaths != null)
            {
                //sort the given paths
                foreach (string path in elementPaths)
                {
                    if (path.EndsWith(@"\"))
                    {
                        complexPaths.Add(path.TrimEnd('\\'));
                    }
                    else
                    {
                        simplePaths.Add(path);
                    }
                }

                //retrive the simple queries
                try
                {
                    if (simplePaths.Count > 0)
                    {
                        AFKeyedResults<string, AFElement> queryReturn
                            = AFElement.FindElementsByPath(simplePaths, Database);
                        IDictionary<string, AFElement> simpleElements
                            = queryReturn.Results;
                        IDictionary<string, Exception> errors
                            = queryReturn.Errors;

                        foreach (string err in errors.Keys)
                        {
                            errorStringBuilder.Append(err + ",");
                        }
                        if (simpleElements != null)
                        {
                            foreach (KeyValuePair<string, AFElement> elementPair
                                in simpleElements)
                            {
                                string elementPath = elementPair.Key;
                                AFElement elementObj = elementPair.Value;
                                if (!encounteredElements.Contains(elementObj))
                                {
                                    encounteredElements.Add(elementObj);
                                    elementDictionary.Add(elementPath, elementObj);
                                }
                            }
                        }
                        else
                        {
                            Log.Error(Properties.Resources.LogSimplePathError);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }

                //expand each of the complex paths and query for them
                try
                {
                    AFKeyedResults<string, AFElement> queryReturn
                        = AFElement.FindElementsByPath(complexPaths, Database);
                    IDictionary<string, AFElement> complexPathRootElements
                        = queryReturn.Results;
                    IDictionary<string, Exception> errors
                        = queryReturn.Errors;

                    foreach (string err in errors.Keys)
                    {
                        errorStringBuilder.Append(err + ", ");
                    }

                    if (complexPathRootElements == null)
                    {
                        Log.Error(Properties.Resources.LogComplexPathError);
                        complexPathRootElements =
                            new Dictionary<string, AFElement>();
                    }
                    foreach (
                        KeyValuePair<string, AFElement> kvp in
                            complexPathRootElements)
                    {
                        foreach (KeyValuePair<string, AFElement> childElement
                            in GetAfElementChildren(kvp.Key, kvp.Value))
                        {
                            string elementPath = childElement.Key;
                            AFElement elementObj = childElement.Value;

                            if (encounteredElements.Contains(elementObj))
                                continue;

                            encounteredElements.Add(elementObj);
                            elementDictionary.Add(elementPath, elementObj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
            }

            if (string.IsNullOrWhiteSpace(errorStringBuilder.ToString()))
                return elementDictionary;

            errorStringBuilder.Remove(errorStringBuilder.Length - 2, 2);
            Log.Error(String.Format(Properties.Resources.LogPathError,
                errorStringBuilder));

            return elementDictionary;
        }

        //OnDemand Metadata
        private void SetVisibleOnDemandColumns()
        {
            HashSet<string> visCols = new HashSet<string>();

            if (HasBreakDownCols)
            {
                foreach (string breakCol in OnDemandParams.BreakdownColumns)
                {
                    visCols.Add(breakCol);
                }
            }

            if (HasAggregates)
            {
                foreach (OnDemandAggregate aggregate in OnDemandParams.Aggregates)
                {
                    foreach (string col in aggregate.Columns)
                    {
                        if (aggregateAliases.ContainsKey(col))
                        {
                            aggregateAliases[col].Add(aggregate.Alias,
                                aggregate.FunctionType);
                        }
                        else
                        {
                            var dict = new Dictionary<string, FunctionType>
                            {
                                {aggregate.Alias, aggregate.FunctionType}
                            };
                            aggregateAliases.Add(col, dict);
                        }

                        try
                        {
                            visCols.Add(col);
                        }
                        catch
                        {
                            Log.Info("Aggregate column already in visible set.");
                        }
                    }
                }
            }

            if (HasAuxiliaryCols)
            {
                foreach (string auxCol in OnDemandParams.AuxiliaryColumns)
                {
                    try
                    {
                        visCols.Add(auxCol);
                    }
                    catch
                    {
                        Log.Info("Auxiliary column already in visible set.");
                    }
                }
            }

            onDemandVisibleColumns = visCols;
        }

        private void SetOnDemandFlags()
        {
            //auto-initialized as false
            if (OnDemandParams == null) return;

            if (OnDemandParams.BreakdownColumns != null)            
                HasBreakDownCols = true;         
            if (OnDemandParams.Aggregates != null)
                HasAggregates = true;
            if (OnDemandParams.AuxiliaryColumns != null)            
                HasAuxiliaryCols = true;           
            if (OnDemandParams.DrillPath != null)
                HasDrillPath = true;
            if (OnDemandParams.Domains != null)
                HasDomains = true;
            if (OnDemandParams.Predicates != null && OnDemandParams.Predicates.Length>0)
                HasPredicates = true;
            if (OnDemandParams.Bucketings != null)
                HasBucketings = true;
        }

        private bool NeedToQuery()
        {
            if (OsiSettings.IsElementsQuery)
            {
                foreach (string colName in onDemandVisibleColumns)
                {
                    if (!elementsNonQueryColumns.Contains(colName)
                        && !colName.StartsWith(Properties.Resources.ColLevel))
                        return true;
                }
                return false;
            }
            
            if (OsiSettings.IsHistoricQuery)
            {
                foreach (string colName in onDemandVisibleColumns)
                {
                    if (!historicNonQueryColumns.Contains(colName))
                        return true;
                }
                return false;
            }

            return false;
        }

        private bool NeedDomainQuery(string domain)
        {
            switch (OsiSettings.QueryType)
            {
                case "Element":
                    return !elementsNonQueryColumns.Contains(domain)
                        && !domain.StartsWith(Properties.Resources.ColLevel);
                case "Historic":
                    return !historicNonQueryColumns.Contains(domain);
                case "Event Frame":
                    return true;
            }
            return false;
        }

        private Dictionary<string, KeyValuePair<bool, ColumnType>>
            CheckAttributePIPoints()
        {
            AFAttributeList attrlist
                = (AFAttributeList)GetAllSelectedAttributes();

            Dictionary<string, KeyValuePair<bool, ColumnType>> hasPIPointReturn
                = new Dictionary<string, KeyValuePair<bool, ColumnType>>();

            foreach (AFAttribute attr in attrlist)
            {
                if (!hasPIPointReturn.ContainsKey(attr.Name))
                {
                    Type t = attr.Type;
                    KeyValuePair<bool, ColumnType> kvp;

                    if (t == typeof (int) || t == typeof (double) 
                        || t == typeof (float))
                    {
                        kvp = new KeyValuePair<bool, ColumnType>(
                            attr.PIPoint != null, ColumnType.Numeric);
                    }
                    else if (t == typeof (AFTime) || t == typeof (DateTime))
                    {
                        kvp = new KeyValuePair<bool, ColumnType>(
                            attr.PIPoint != null, ColumnType.Time);
                    }
                    else
                    {
                        kvp = new KeyValuePair<bool, ColumnType>(
                            attr.PIPoint != null, ColumnType.Text);
                    }
                  
                    hasPIPointReturn.Add(attr.Name, kvp);
                }
            }

            return hasPIPointReturn;
        }

        public IEnumerable<AFAttribute> GetPredicateFilteredAttributes(
           Dictionary<string, List<string>> evPathDict,
            List<string> eleFilter, List<string> attrFilter)
        {
            AFAttributeList attrList = new AFAttributeList();

            attrFilter = attrFilter ?? new List<string>();
            eleFilter = eleFilter ?? new List<string>();
            
            if (evPathDict != null && evPathDict.Count > 0)
            {              
                foreach (var eleAttrCombo in evPathDict.Values)
                {
                    if(!eleFilter.Contains(eleAttrCombo[0]) 
                        || !attrFilter.Contains(eleAttrCombo[1])) continue;

                    foreach (AFElement ele in SelectedElements.Values)
                    {
                        if (eleAttrCombo[0] != ele.Name) continue;

                        try
                        {
                            attrList.Add(ele.Attributes[eleAttrCombo[1]]);
                        }
                        catch
                        {
                            Log.Info(String.Format(
                                Properties.Resources.LogElementAttributeError,
                                eleAttrCombo[1], eleAttrCombo[0]));
                        }
                    }
                }
            }
            else
            {
                if (attrFilter.Count == 0)
                {
                    attrList =
                        (AFAttributeList) GetAllSelectedAttributes();
                }
                else
                {
                    attrList =
                        (AFAttributeList)GetAllSelectedAttributes(attrFilter);
                }

                AFAttributeList tempAttrList = new AFAttributeList();

                if (eleFilter.Count>0)
                {
                    foreach (AFAttribute attr in attrList)
                    {
                        if (eleFilter.Contains(attr.Element.Name))
                            tempAttrList.Add(attr);
                    }

                    attrList = tempAttrList;
                }
            }

            return attrList;
        }

        private double ApplyAggregateType(double value,
            FunctionType type)
        {
            switch (type)
            {
                case FunctionType.AbsSum:              
                    value = Math.Abs(value);
                    break;              
                case FunctionType.Count:
                    value = 1;
                    break;
                case FunctionType.NegativeSum:
                    value = Math.Abs(value)*-1;
                    break;
                case FunctionType.PositiveSum:
                    value = Math.Abs(value);
                    break;
                case FunctionType.Samples:
                    value = 1;
                    break;      
                    //the following can only happen in "elements" mode
                case FunctionType.SumOfProducts: 
                case FunctionType.SumOfQuotients:
                case FunctionType.SumOfSquares:
                case FunctionType.SumOfWeightedQuotients:
                case FunctionType.ConditionalSum:
                    break; 
                default:
                    //Default value return - External,First,Last,Min,Max,Sum
                    break;
            }
            return value;
        }
        #endregion
    }
}