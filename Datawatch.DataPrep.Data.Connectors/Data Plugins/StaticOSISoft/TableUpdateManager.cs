﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.StaticOSISoftPlugin
{
    public class TableUpdateManager
    {
        //Private variables
        private readonly ConcurrentQueue<object[]> rowQueue;
        private Task task;
        private CancellationTokenSource cancelToken;
        private Dictionary<string, int> SchemaDictionary
        {
            get; set;
        }

        //Properties
        public int TableColumnCount
        {
            get { return Table.ColumnCount; }
        }
        private readonly StandaloneTable Table;

        //Constructor
        public TableUpdateManager(StandaloneTable t)
        {
            Table = t;
            rowQueue = new ConcurrentQueue<object[]>();

            SchemaDictionary = new Dictionary<string, int>();
             
            for (int i = 0; i < t.ColumnCount; i++)
            {
                Column col = t.GetColumn(i);
                SchemaDictionary.Add(col.Name, i);
            }           
        }

        //Methods
        public void Start()
        {
            cancelToken = new CancellationTokenSource();
            task = new Task(ListenForRows, cancelToken.Token);
            task.Start();
        }

        public void Stop()
        {
            cancelToken.Cancel();
            task = null;
        }

        public bool HasRowsQueued
        {
            get { return rowQueue.Count > 0; }
        }

        public void EnqueueRow(object[] obj)
        {
            rowQueue.Enqueue(obj);
        }

        private void ListenForRows()
        {
            while (!cancelToken.IsCancellationRequested)
            {
                if (rowQueue.Count <= 0) continue;

                object[] arr;
                rowQueue.TryDequeue(out arr);

                if (arr != null)
                    Table.AddRow(arr);

                //string s = "[";
                //foreach (var val in arr)
                //{
                //    if (val != null)
                //        s += val.ToString() + ",";
                //    else
                //        s += "null,";
                //}
                //s += "]";
                //Debug.WriteLine(s);
                
            }
        }

        //Schema Methods
        public int GetColumnIndex(string name)
        {
            try
            {
                return SchemaDictionary[name];
            }
            catch (KeyNotFoundException e)
            {
                Log.Error("Column "+name+" Not Found!");
                Log.Exception(e);
                return -1;
            }
        }

        public bool AddColumn(string name, ColumnType type)
        {
            try
            {
                switch (type)
                {
                    case ColumnType.Numeric:
                        Table.AddColumn(new NumericColumn(name));                     
                        break;
                    case ColumnType.Time:
                        Table.AddColumn(new TimeColumn(name));
                        break;
                    default:
                        Table.AddColumn(new TextColumn(name));
                        break;
                }
            }
            catch (Exception e)
            {
                Log.Error("Cannot add column with name "+name+".");
                Log.Exception(e);
                return false;
            }
            SchemaDictionary.Add(name, TableColumnCount-1);
            return true;
        }
    }
}