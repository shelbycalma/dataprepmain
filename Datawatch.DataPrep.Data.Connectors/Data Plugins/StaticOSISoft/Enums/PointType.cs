﻿namespace Panopticon.StaticOSISoftPlugin.Enums
{
    public enum PointType
    {
        RecordedPoints = 1,
        PlottedValues = 2,
        Undefined = 3
    }
}
