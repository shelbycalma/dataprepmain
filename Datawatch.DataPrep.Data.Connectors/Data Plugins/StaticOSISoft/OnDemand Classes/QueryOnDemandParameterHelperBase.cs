﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.StaticOSISoftPlugin.OnDemand_Classes
{
    public class QueryOnDemandParameterHelperBase
    {
        #region Properties

        public OnDemandParameters Parameters { get; set; }

        #endregion

        #region Constructors

        public QueryOnDemandParameterHelperBase(OnDemandParameters parameters)
        {
            Parameters = parameters;
        }

        #endregion

        #region Methods

        public List<string> GetOnDemandAggregates()
        {
            var aggregates = new List<string>();

            if (Parameters.Aggregates != null)
            {
                foreach (var agg in Parameters.Aggregates)
                {
                    foreach (var col in agg.Columns)
                    {
                        if (!aggregates.Contains(col))
                        {
                            aggregates.Add(col);
                        }
                    }
                }
            }
            return aggregates;
        }

        public List<string> GetOnDemandAuxColumns()
        {
            if (Parameters == null || Parameters.AuxiliaryColumns == null)
                return new List<string>();

            return Parameters.AuxiliaryColumns.ToList();
        }

        public List<string> GetOnDemandBreakdownColumns()
        {
            var breakdownCols = new List<string>();

            if (Parameters == null)
                return breakdownCols;

            if (Parameters.BreakdownColumns != null)
            {
                for (int i = 0; i < Parameters.Depth; i++)
                {
                    breakdownCols.Add(Parameters.BreakdownColumns[i]);
                }
            }

            return breakdownCols;
        }

        public List<string> GetOnDemandDrillPath()
        {
            return Parameters.DrillPath != null
                ? Parameters.DrillPath.ToList()
                : new List<string>();
        }

        #region Inclusive Predicate Parsing

        /// <summary>
        /// Parses the OnDemandParameters' Predicate list for specific values
        /// to include on columns that are being filtered. The returned
        /// Dictionary is a mapping between column names and the values that
        /// should be INCLUDED after applying every filter except for a filter
        /// out.
        /// </summary>
        /// <returns>
        /// Dictionary that maps columns that are being filtered
        /// to the list of values that should be included for that column.
        /// </returns>
        public Dictionary<string, List<string>> GetOnDemandInclusivePredicates()
        {
            if (Parameters == null)
                return new Dictionary<string, List<string>>();

            if (Parameters.Predicates != null && Parameters.Predicates.Any())
            {
                var predicates = new Dictionary<string, List<string>>();
                foreach (var pred in Parameters.Predicates)
                {
                    ParsePredicate(pred, predicates);
                }
                return predicates;
            }

            return new Dictionary<string, List<string>>();
        }

        private void ParsePredicate(
            Predicate pred,
            Dictionary<string, List<string>> parsedPreds)
        {
            if (pred is Comparison)
            {
                var kvp = ParseTextComparisonPredicate((Comparison) pred);
                if (!parsedPreds.ContainsKey(kvp.Key))
                    parsedPreds.Add(kvp.Key, kvp.Value);
                else
                    parsedPreds[kvp.Key].AddRange(kvp.Value);
            }
            else if (pred is OrOperator)
            {
                ParseOrPredicate((OrOperator) pred, parsedPreds);
            }
            else if (pred is AndOperator)
            {
                ParseAndPredicate((AndOperator) pred, parsedPreds);
            }
        }

        private KeyValuePair<string, List<string>> ParseTextComparisonPredicate(
            Comparison compPred)
        {
            string column = ((ColumnParameter) compPred.Left).ColumnName;
            List<string> elements = new List<string>();
            if (compPred.Right is ListParameter)
            {
                elements.AddRange(
                    ((ListParameter) compPred.Right).Values.ToList());
            }
            else if (compPred.Right is StringParameter)
            {
                // todo this is a short term solution, the escaping that happens
                // in the tostring method could mess up some element names.
                var val =
                    ((StringParameter) compPred.Right).ToString(SqlDialectFactory.MySQL)
                        .TrimEnd('\'')
                        .TrimStart('\'');
                if (compPred.ToString().Contains(" LIKE "))
                    val += "WILD_CARD%";
                elements.Add(val);
            }

            return new KeyValuePair<string, List<string>>(column, elements);
        }

        private void ParseOrPredicate(
            OrOperator orPred,
            Dictionary<string, List<string>> parsedPreds)
        {
            ParsePredicate(orPred.A, parsedPreds);
            ParsePredicate(orPred.B, parsedPreds);
        }

        private void ParseAndPredicate(
            AndOperator andPred,
            Dictionary<string, List<string>> parsedPreds)
        {
            bool leftIsValue = andPred.A is Comparison &&
                               ((Comparison) andPred.A).Right is ValueParameter;
            bool rightIsValue = andPred.B is Comparison &&
                                ((Comparison) andPred.B).Right is ValueParameter;

            if (leftIsValue || rightIsValue)
            {
                List<string> valList = new List<string>();
                valList.Add(string.Empty);
                valList.Add(string.Empty);

                if (leftIsValue)
                {
                    var kvp = ParseValuePredicate((Comparison) andPred.A,
                        ComparisonOperator.GreaterThanEqualTo, valList);
                    if (!parsedPreds.ContainsKey(kvp.Key))
                        parsedPreds.Add(kvp.Key, valList);
                }
                if (rightIsValue)
                {
                    var kvp = ParseValuePredicate((Comparison) andPred.B,
                        ComparisonOperator.LessThanEqualTo, valList);
                    if (!parsedPreds.ContainsKey(kvp.Key))
                        parsedPreds.Add(kvp.Key, valList);
                }
            }
            else
            {
                ParsePredicate(andPred.A, parsedPreds);
                ParsePredicate(andPred.B, parsedPreds);
            }
        }

        private KeyValuePair<string, List<string>>
            ParseValuePredicate(
            Comparison numericPred,
            ComparisonOperator sign, List<string> valList)
        {
            string key = ((ColumnParameter) numericPred.Left).ColumnName;
            if (sign == ComparisonOperator.GreaterThanEqualTo)
            {
                var valueParam = (ValueParameter) numericPred.Right;
                valList[0] = valueParam.Value.ToString();
            }
            else
            {
                var valueParam = (ValueParameter) numericPred.Right;
                valList[1] = valueParam.Value.ToString();
            }

            return new KeyValuePair<string, List<string>>(key, valList);
        }

        #endregion

        #region Exclusive Predicate Parsing

        /// <summary>
        /// Parses the OnDemandParameters' Predicates list for values that
        /// should be excluded from the table given the current filter. It seems
        /// that the "Filter out" feature in Designer is the only feature that
        /// will produce an exclusive predicate. Each Dictionary in the returned
        /// List represents a row, where each key value pair in the dictionary
        /// represents a mapping of a column that is being filtered to the value
        /// that should be filtered out from that column.
        /// 
        /// A row should be filtered out if and only if the values of its 
        /// columns match up to every key-value mapping in one of the 
        /// Dictionaries.
        /// </summary>
        /// <returns>
        /// A List of Dictionaries representing the set of column-value pairs
        /// that would require the row to be filtered.
        /// </returns>
        public List<Dictionary<string, string>> GetOnDemandExclusivePredicates()
        {
            if (Parameters == null)
                return new List<Dictionary<string, string>>();

            if (Parameters.Predicates != null && Parameters.Predicates.Any())
            {
                var notPredicates = new List<Dictionary<string, string>>();
                foreach (var pred in Parameters.Predicates)
                {
                    if (pred is NotOperator)
                    {
                        ParseNotPredicate((NotOperator) pred, notPredicates);
                    }
                }
                return notPredicates;
            }

            return new List<Dictionary<string, string>>();
        }

        private void ParseNotPredicate(
            NotOperator notPred,
            List<Dictionary<string, string>> parsedNotPreds)
        {
            if (notPred.A is OrOperator)
            {
                ParseNotOrPredicate((OrOperator) notPred.A, parsedNotPreds,
                    new Dictionary<string, string>());
            }
        }

        private void ParseNotAndPredicate(
            AndOperator andPred,
            List<Dictionary<string, string>> parsedNotPreds,
            Dictionary<string, string> columnValuesInPredicate)
        {
            // base cases
            if (andPred.A == null && andPred.B is Comparison)
            {
                var kvp = ParseNotPredicateValue((Comparison) andPred.B);
                columnValuesInPredicate.Add(kvp.Key, kvp.Value);

                parsedNotPreds.Add(columnValuesInPredicate);
            }
            else if (andPred.A is Comparison && andPred.B is Comparison)
            {
                var kvp1 = ParseNotPredicateValue((Comparison) andPred.A);
                columnValuesInPredicate.Add(kvp1.Key, kvp1.Value);
                var kvp2 = ParseNotPredicateValue((Comparison) andPred.B);
                columnValuesInPredicate.Add(kvp2.Key, kvp2.Value);

                parsedNotPreds.Add(columnValuesInPredicate);
            }

                // recursive cases
            else if (andPred.A == null && andPred.B is OrOperator)
            {
                ParseNotOrPredicate((OrOperator) andPred.B, parsedNotPreds,
                    columnValuesInPredicate);
            }
                //todo I don't think this case is possible
            else if (andPred.A == null && andPred.B is AndOperator)
            {
                ParseNotAndPredicate((AndOperator) andPred.B, parsedNotPreds,
                    columnValuesInPredicate);
            }
            else if (andPred.A is Comparison && andPred.B is OrOperator)
            {
                var kvp = ParseNotPredicateValue((Comparison) andPred.A);
                columnValuesInPredicate.Add(kvp.Key, kvp.Value);

                ParseNotOrPredicate((OrOperator) andPred.B, parsedNotPreds,
                    columnValuesInPredicate);
            }
            else if (andPred.A is Comparison && andPred.B is AndOperator)
            {
                var kvp = ParseNotPredicateValue((Comparison) andPred.A);
                columnValuesInPredicate.Add(kvp.Key, kvp.Value);

                ParseNotAndPredicate((AndOperator) andPred.B, parsedNotPreds,
                    columnValuesInPredicate);
            }
        }

        private void ParseNotOrPredicate(
            OrOperator orPred,
            List<Dictionary<string, string>> parsedNotPreds,
            Dictionary<string, string> currentAndDictionary)
        {
            // if this is a wrapper on another kind of predicate, recurse down
            if (orPred.A == null && orPred.B is OrOperator)
            {
                ParseNotOrPredicate((OrOperator) orPred.B, parsedNotPreds,
                    currentAndDictionary);
            }
            else if (orPred.A == null && orPred.B is AndOperator)
            {
                ParseNotAndPredicate((AndOperator) orPred.B, parsedNotPreds,
                    currentAndDictionary);
            }
                // true "or" so we need a new dictionary on each side
            else if (orPred.A is OrOperator && orPred.B is OrOperator)
            {
                ParseNotOrPredicate((OrOperator) orPred.A, parsedNotPreds,
                    new Dictionary<string, string>());
                ParseNotOrPredicate((OrOperator) orPred.B, parsedNotPreds,
                    new Dictionary<string, string>());
            }
        }

        private KeyValuePair<string, string> ParseNotPredicateValue(
            Comparison pred)
        {
            string key = ((ColumnParameter) pred.Left).ColumnName;
            string value = "";
            if (pred.Right is StringParameter)
            {
                // todo this will probably mess up elements with apostrophes in them
                value =
                    ((StringParameter) pred.Right).ToString(SqlDialectFactory.MySQL)
                        .TrimEnd('\'')
                        .TrimStart('\'');
            }
            else if (pred.Right is ListParameter)
            {
                value = ((ListParameter) pred.Right).Values[0];
            }
            return new KeyValuePair<string, string>(key, value);
        }

        #endregion

        #endregion
    }

    public enum ComparisonOperator
    {
        GreaterThanEqualTo,
        LessThanEqualTo
    }
}