﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.StaticOSISoftPlugin.Enums;

namespace Panopticon.StaticOSISoftPlugin
{
    public class StaticOSISoftSettings : TextSettingsBase
    {
        #region Constructors

        public StaticOSISoftSettings(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public StaticOSISoftSettings(IPluginManager pluginManager, PropertyBag bag)
            : base(pluginManager, bag, null)
        {
        }

        public StaticOSISoftSettings(
            IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        #endregion

        #region Properties

        public string UserName
        {
            get { return GetInternal("Username"); }
            set { SetInternal("Username", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string SystemName
        {
            get { return GetInternal("SystemName"); }
            set { SetInternal("SystemName", value); }
        }

        public string Database
        {
            get { return GetInternal("Database"); }
            set { SetInternal("Database", value); }
        }

        public string ElementSelectionMode
        {
            get { return GetInternal("ElementSelectionMode"); }
            set { SetInternal("ElementSelectionMode", value); }
        }

        public string Attributes
        {
            get { return GetInternal("Attributes", String.Empty); }
            set { SetInternal("Attributes", value); }
        }

        public string ElementPaths
        {
            get { return GetInternal("ElementPaths", String.Empty); }
            set { SetInternal("ElementPaths", value); }
        }

        public string ManualAttributes
        {
            get { return GetInternal("ManualAttributes", String.Empty); }
            set { SetInternal("ManualAttributes", value); }
        }

        public string ManualElementPaths
        {
            get { return GetInternal("ManualElementPaths", String.Empty); }
            set { SetInternal("ManualElementPaths", value); }
        }

        public string StartTime
        {
            get { return GetInternal("StartTime", "*-1h"); }
            set { SetInternal("StartTime", value); }
        }

        public string EndTime
        {
            get { return GetInternal("EndTime", "*"); }
            set { SetInternal("EndTime", value); }
        }

        public string FilterExpression
        {
            get { return GetInternal("FilterExpression", String.Empty); }
            set { SetInternal("FilterExpression", value); }
        }

        public string QueryType
        {
            get { return GetInternal("QueryType", String.Empty); }
            set { SetInternal("QueryType", value); }
        }

        public string PointText
        {
            get { return GetInternal("PointText", String.Empty); }
            set { SetInternal("PointText", value); }
        }

        public string PlotIntervals
        {
            get { return GetInternal("PlotIntervals", "1000"); }
            set { SetInternal("PlotIntervals", value); }
        }

        public string EventFrameGuid
        {
            get { return GetInternal("EventFrameGuid", String.Empty); }
            set { SetInternal("EventFrameGuid", value); }
        }

        public override string Title
        {
            get
            {
                return GetInternal("Title", "OSISoft - Static - " + Database);
            }
            set { SetInternal("Title", value); }
        }

        public int DeepestLevelNumber
        {
            get { return GetInternalInt("DeepestLevelNumber", 0); }
            set { SetInternalInt("DeepestLevelNumber", value); }
        }

        public bool IsManualSelectionMode
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsManualSelectionMode",
                        "false"));
            }
            set { SetInternal("IsManualSelectionMode", value.ToString()); }
        }

        public bool IsTreeSelectionMode
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsTreeSelectionMode", "true"));
            }
            set { SetInternal("IsTreeSelectionMode", value.ToString()); }
        }

        public bool UseWindowsAuthentication
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("UseWindowsAuthentication",
                        "true"));
            }
            set { SetInternal("UseWindowsAuthentication", value.ToString()); }
        }

        public bool IsConnected
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsConnected", "false"));
            }
            set { SetInternal("IsConnected", value.ToString()); }
        }

        public bool IsElementsQuery
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsElementsQuery", "false"));
            }
            set { SetInternal("IsElementsQuery", value.ToString()); }
        }

        public bool IsEventFrameQuery
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsEventFrameQuery", "false"));
            }
            set { SetInternal("IsEventFrameQuery", value.ToString()); }
        }

        public bool IsHistoricQuery
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsHistoricQuery", "false"));
            }
            set { SetInternal("IsHistoricQuery", value.ToString()); }
        }

        public bool IsLoggingEnabled
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsLoggingEnabled",
                        "true"));
            }
            set { SetInternal("IsLoggingEnabled", value.ToString()); }
        }

        public bool IsQueryOnDemand
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsQueryOnDemand",
                        "true"));
            }
            set { SetInternal("IsQueryOnDemand", value.ToString()); }
        }

        public PointType PointType
        {
            get
            {
                string parameterizedPoint 
                    = ParameterizeValue(PointText);

                switch (parameterizedPoint)
                {
                    case "AFData Plotted Values":
                    case "plotted":
                    case "Plotted":
                    case "Plotted Values":
                        return PointType.PlottedValues;
                    case "PI Points Recorded Values":
                    case "recorded":
                    case "Recorded":
                    case "Recorded Points":
                        return PointType.RecordedPoints;
                    default:
                        return PointType.Undefined;
                }
            }
        }

        #endregion

        public override PathType FilePathType { get; set; }

        protected override string DefaultParserPluginId
        {
            get { return "Text"; }
        }

        //General Methods
        public string ParameterizeValue(
            string value, string arraySeparator = "\n")
        {
            if (Parameters == null || String.IsNullOrEmpty(value))
                return value;

            var encoder = new ParameterEncoder
            {
                Parameters = Parameters,
                DefaultArraySeparator = arraySeparator,
                SourceString = value
            };
            return encoder.Encoded();
        }
    }
}