﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.StaticOSISoftPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<StaticOSISoftSettings>, IOnDemandPlugin
    {
        internal const string PluginId = "StaticOSISoftPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "OSISoft - Static";
        internal const string PluginType = DataPluginTypes.Database;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        //Data Methods
        public override ITable GetData(
            string workbookDir, string dataDir, PropertyBag bag,
            IEnumerable<ParameterValue> parameters, int rowcount)
        {
            StaticOSISoftSettings osiSettings
                = new StaticOSISoftSettings(pluginManager, bag, parameters);

            StaticOSISoftHelper osiHelper
                = new StaticOSISoftHelper(osiSettings, null, this.errorReporter);
            osiHelper.Connect();

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            switch (osiSettings.QueryType)
            {
                case "Element":
                    table = osiHelper.BuildElementsTable(table);
                    osiHelper.PopulateElementsTable(table, rowcount);
                    break;
                case "Historic":
                    table = osiHelper.BuildHistoricTable(table);
                    osiHelper.PopulateHistoricTable(table, rowcount);
                    break;
                case "Event Frame":
                    table = osiHelper.BuildEventFrameTable(table);
                    osiHelper.PopulateEventFrameTable(table, rowcount);
                    break;
            }

            osiHelper.Disconnect();
            table.EndUpdate();
            return table;
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount,
                                       bool applyRowFilteration)
        {
            throw new System.NotImplementedException();
        }

        public ITable GetOnDemandData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            StaticOSISoftSettings osiSettings
                = new StaticOSISoftSettings(pluginManager, bag, parameters);

            StandaloneTable table = new StandaloneTable();

            StaticOSISoftHelper osiHelper 
                = new StaticOSISoftHelper(
                    osiSettings, onDemandParameters, this.errorReporter);
            osiHelper.Connect();

            //Check Domain - provide
            if (onDemandParameters != null 
                && onDemandParameters.Domains != null)
            {
                switch (osiSettings.QueryType)
                {
                    case "Element":
                        table = osiHelper.CreateElementDomainTable();
                        break;
                    case "Historic":
                        table = osiHelper.CreateHistoricDomainTable();
                        break;
                    case "Event Frame":
                        table = osiHelper.BuildEventFrameTable(table);
                        break;
                }                
            }          
            //if no breakdown, don't query
            else if (onDemandParameters == null || osiHelper.AreOnDemandParamsEmpty)
            {
                switch (osiSettings.QueryType)
                {
                    case "Element":
                        table = osiHelper.BuildElementsTable(table);
                        break;
                    case "Historic":
                        table = osiHelper.BuildHistoricTable(table);
                        break;
                    case "Event Frame":
                        table = osiHelper.BuildEventFrameTable(table);
                        break;
                }
            }
            //has breakdown, populate the table.
            else
            {
                switch (osiSettings.QueryType)
                {
                    case "Element":
                        table = osiHelper.BuildElementsTable(table);
                        table = osiHelper.OnDemandPopulateElementsTable(table);
                        break;
                    case "Historic":
                        table = osiHelper.BuildHistoricTable(table);
                        table = osiHelper.OnDemandPopulateHistoricTable(table);
                        break;
                    case "Event Frame":
                        table = osiHelper.BuildEventFrameTable(table);
                        break;
                }
            }

            osiHelper.Disconnect();
            return table;
        }

        //Property Bag Passing
        public override StaticOSISoftSettings CreateSettings(PropertyBag bag)
        {
            return new StaticOSISoftSettings(pluginManager, bag);
        }

        //General Methods
        public bool IsOnDemand(PropertyBag bag)
        {
            StaticOSISoftSettings osiSettings 
                = new StaticOSISoftSettings(pluginManager, bag);
            return osiSettings.IsQueryOnDemand;
        }
    }
}