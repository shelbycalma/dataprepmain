﻿namespace Panopticon.StaticOSISoftPlugin.Wrapper_Classes
{
    public class EventFrameGridItem
    {
        //Constuctors
        public EventFrameGridItem()
        {
        }

        public EventFrameGridItem(
            string name, string duration,
            string start, string end, string template,
            string guid, string element)
        {
            Name = name;
            Duration = duration;
            StartTime = start;
            EndTime = end;
            Template = template;
            Guid = guid;
            PrimaryElement = element;
        }

        //Properties
        public string Name { get; set; }
        public string Duration { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Template { get; set; }
        public string Guid { get; set; }
        public string PrimaryElement { get; set; }
    }
}