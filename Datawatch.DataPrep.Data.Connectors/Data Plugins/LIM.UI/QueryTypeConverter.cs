﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.LIMPlugin.UI
{
    class QueryTypeConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (!(value is LIMQueryType)) return DependencyProperty.UnsetValue;

            LIMQueryType authType = (LIMQueryType)value;

            switch (authType)
            {
                default:
                case LIMQueryType.Columns:
                    return Properties.Resources.UiQueryTypeColumns;
                case LIMQueryType.Series:
                    return Properties.Resources.UiQueryTypeSeries;
                case LIMQueryType.Curve:
                    return Properties.Resources.UiQueryTypeCurve;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
