﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Panopticon.LIMPlugin.UI
{
    /// <summary>
    /// Interaction logic for LIMConfigPanel.xaml
    /// </summary>
    public partial class LIMConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(LIMSettings), typeof(LIMConfigPanel),
                    new PropertyMetadata(new PropertyChangedCallback(
                        Settings_Changed)));

        private static void Settings_Changed(DependencyObject obj,
           DependencyPropertyChangedEventArgs args)
        {
            ((LIMConfigPanel)obj).OnSettingsChanged(
                (LIMSettings)args.OldValue,
                (LIMSettings)args.NewValue);
        }

        protected void OnSettingsChanged(LIMSettings oldSettings,
            LIMSettings newSettings)
        {
            if (oldSettings != null)
            {
                oldSettings.PropertyChanged -=
                    new PropertyChangedEventHandler(Settings_PropertyChanged);
            }

            if (newSettings != null)
            {

                newSettings.PropertyChanged +=
                    new PropertyChangedEventHandler(Settings_PropertyChanged);
                PasswordBox.Password = newSettings.ConnectionPassword;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void Settings_PropertyChanged(object sender,
           PropertyChangedEventArgs e)
        {

        }

        public LIMSettings Settings
        {
            get { return (LIMSettings)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        public bool IsOk
        {
            get
            {
                return Settings != null && !string.IsNullOrEmpty(
                    Settings.ConnectionString)
                    && !string.IsNullOrEmpty(Settings.Query)
                && !string.IsNullOrEmpty(Settings.ConnectionUserName)
                && !string.IsNullOrEmpty(Settings.ConnectionPassword);

            }
        }

        public LIMConfigPanel()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.ConnectionPassword = PasswordBox.Password;
            }
        }

       

    }
}
