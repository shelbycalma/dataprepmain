﻿using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.LIMPlugin.UI
{
    /// <summary>
    /// Interaction logic for LIMConfigWindow.xaml
    /// </summary>
    public partial class LIMConfigWindow : Window
    {
        public static RoutedCommand OkCommand =
            new RoutedCommand("OK", typeof(LIMConfigWindow));
        public static RoutedCommand CancelCommand =
            new RoutedCommand("Cancel", typeof(LIMConfigWindow));
        private LIMSettings settings;

        public LIMConfigWindow(LIMSettings settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public LIMSettings Settings
        {
            get { return settings; }
        }

        public PropertyBag PropertyBag
        {
            get { return ConfigPanel.Settings.ToPropertyBag(); }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ConfigPanel.IsOk;
        }

        private void Ok_Execute(object sender, ExecutedRoutedEventArgs e)
        {
           

            DialogResult = true;
            Close();
        }

        private void Cancel_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }

       
    }
}
