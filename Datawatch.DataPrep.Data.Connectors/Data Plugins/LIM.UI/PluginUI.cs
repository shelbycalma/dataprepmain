﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.LIMPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, LIMSettings>
    {
        private const string imageUri =
            "pack://application:,,,/Panopticon.LIMPlugin.UI;component" +
            "/lim.gif";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            LIMSettings con = new LIMSettings();

            LIMConfigWindow window = new LIMConfigWindow(con);
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return window.PropertyBag;
            }
            return null;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            LIMSettings settings = new LIMSettings(bag);

            return new LIMConfigPanel()
            {
                Settings = settings
            };
        }

        public override PropertyBag GetSetting(object obj)
        {
            LIMConfigPanel configPanel = (LIMConfigPanel)obj;

            // Create a new PropertyBag for the connection.            
            return configPanel.Settings.ToPropertyBag();
        }
    }
}
