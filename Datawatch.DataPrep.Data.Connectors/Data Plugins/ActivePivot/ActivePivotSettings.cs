﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.ActivePivot.MDXPlugin.ConnectionSettings;

namespace Panopticon.ActivePivot.MDXPlugin
{
    public class ActivePivotSettings : RealtimeConnectionSettings, IMdxSettings
    {
        private bool _isStatic;
        
        public ActivePivotSettings(PropertyBag properties, bool isStatic)
            : base(properties, true)
        {
            this.IdColumn = "id";
            String[] temp = new string[1];
            temp[0] = this.IdColumn;
            this.IdColumnCandidates = temp;
            this._isStatic = isStatic;
        }

        public string ServerName
        {
            get { return GetInternal("ServerName", "localhost"); }
            set { SetInternal("ServerName", value); }
        }

        public bool UseSSL
        {
            get
            {
                string s = GetInternal("UseSSL");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("UseSSL", Convert.ToString(value)); }
        }

        public string RowDimensionColumnsBackup
        {
            get { return GetInternal("RowDimensionColumnsBackup"); }
            set { SetInternal("RowDimensionColumnsBackup", value); }
        }


        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string Catalog
        {
            get { return GetInternal("Catalog"); }
            set { SetInternal("Catalog", value); }
        }

        public string CubeName
        {
            get { return GetInternal("Cube"); }
            set { SetInternal("Cube", value); }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public bool ManualEdit
        {
            get
            {
                string s = GetInternal("ManualEdit");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("ManualEdit", Convert.ToString(value)); }
        }

        

        public bool SkipAggregatedRows
        {
            get
            {
                string s = GetInternal("SkipAggregatedRows");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("SkipAggregatedRows", Convert.ToString(value)); }
        }

        public bool FillRaggedHierarchies
        {
            get
            {
                string s = GetInternal("FillRaggedHierarchies");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("FillRaggedHierarchies", Convert.ToString(value)); }
        }

        public FillRaggedHierarchyTypes FillRaggedHierarchyType
        {
            get
            {
                string s = GetInternal("FillRaggedHierarchyType");
                if (string.IsNullOrEmpty(s))
                {
                    return FillRaggedHierarchyTypes.Left;
                }
                return (FillRaggedHierarchyTypes) Enum.Parse(typeof (FillRaggedHierarchyTypes), s);
            }

            set { SetInternal("FillRaggedHierarchyType", Enum.GetName(typeof (FillRaggedHierarchyTypes), value)); }
        }

        public bool ExternalAggregates
        {
            get
            {
                string s = GetInternal("ExternalAggregates", "False");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("ExternalAggregates", Convert.ToString(value)); }
        }

        public bool NullSuppression
        {
            get
            {
                string s = GetInternal("NullSuppression");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("NullSuppression", Convert.ToString(value)); }
        }

        public string[] ColumnsAxis
        {
            get
            {
                var itemsCount = GetInternalInt("ColumnsItemsCount", 0);
                var resultItems = new string[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    resultItems[i] = GetInternal(string.Format("ColumnsItems_{0}", i));
                }

                return resultItems;
            }

            set
            {
                if (value == null)
                {
                    SetInternalInt("ColumnsItemsCount", 0);
                    return;
                }

                SetInternalInt("ColumnsItemsCount", value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    SetInternal(string.Format("ColumnsItems_{0}", i), value[i]);
                }
            }
        }

        public string[] RowsAxis
        {
            get
            {
                var itemsCount = GetInternalInt("RowsItemsCount", 0);
                var resultItems = new string[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    resultItems[i] = GetInternal(string.Format("RowsItems_{0}", i));
                }

                return resultItems;
            }

            set
            {
                if (value == null)
                {
                    SetInternalInt("RowsItemsCount", 0);
                    return;
                }

                SetInternalInt("RowsItemsCount", value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    SetInternal(string.Format("RowsItems_{0}", i), value[i]);
                }
            }
        }

        public string[] SlicerAxis
        {
            get
            {
                var itemsCount = GetInternalInt("SlicersItemsCount", 0);
                var resultItems = new string[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    resultItems[i] = GetInternal(string.Format("SlicersItems_{0}", i));
                }

                return resultItems;
            }

            set
            {
                if (value == null)
                {
                    SetInternalInt("SlicersItemsCount", 0);
                    return;
                }

                SetInternalInt("SlicersItemsCount", value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    SetInternal(string.Format("SlicersItems_{0}", i), value[i]);
                }
            }
        }

        public void SaveParameter(string itemName, string parameterName)
        {
            SetInternal(string.Format("Param_{0}", itemName), parameterName);
        }

        public string GetParameter(string itemName)
        {
            return GetInternal(string.Format("Param_{0}", itemName), string.Empty);
        }

        public void RemoveParameter(string itemName)
        {
            PropertyBag bag = this.ToPropertyBag();
            bag.Values.Remove(string.Format("Param_{0}", itemName));
        }

        public override PropertyBag ToPropertyBag()
        {
            if (this.ManualEdit)
            {
                this.ColumnsAxis = null;
                this.RowsAxis = null;
                this.SlicerAxis = null;
            }
            return base.ToPropertyBag();
        }

        public void RestoreDefaults()
        {
            this.ColumnsAxis = null;
            this.RowsAxis = null;
            this.SlicerAxis = null;
            if (!ManualEdit)
            {
                this.Query = string.Empty;
            }
        }

        public String AdministrationUrl
        {
            get { return (GetInternal("AdministrationURL", GetDefaultUrl("Administration"))); }
            set { SetInternal("AdministrationURL", value); }
        }

        public String LicensingUrl
        {
            get { return (GetInternal("LicensingURL", GetDefaultUrl("Licensing"))); }
            set { SetInternal("LicensingURL", value); }
        }

        public String GetDefaultUrl(String service)
        {
            return ServerName + "/webservices/" + service;
        }
    }

    public enum FillRaggedHierarchyTypes
    {
        Left,
        Right
    }
}