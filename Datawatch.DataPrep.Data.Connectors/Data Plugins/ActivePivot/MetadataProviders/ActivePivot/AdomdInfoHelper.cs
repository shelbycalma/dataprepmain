﻿using System;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Microsoft.AnalysisServices.AdomdClient;
using CubeType = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.CubeType;
using Dimension = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension;
using Hierarchy = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy;
using HierarchyOrigin = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.HierarchyOrigin;
using Level = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level;
using Measure = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Measure;

namespace Panopticon.ActivePivot.MDXPlugin.MetadataProviders.ActivePivot
{
    public class AdomdInfoHelper
    {
        public static Cube CreateCubeMetadataInfo(CubeDef cube)
        {
            if (cube == null)
                return null;
            var cubeMetadataInfo = new Cube
                {
                    Caption = cube.Caption,
                    Description = cube.Description,
                    Name = cube.Name,
                    //LastProcessed = cube.LastProcessed,
                    //LastUpdated = cube.LastUpdated,
                    Type = (CubeType) (cube.Type)
                };

            var propertiesToAdd = cube.Properties
                                          .Cast<Property>()
                                          .Select(prop => new PropertyInfo
                                          {
                                              Name = prop.Name,
                                              Value = prop.Value
                                          });
            foreach (var propertyInfo in propertiesToAdd)
            {
                cubeMetadataInfo.PropertyInfos.Add(propertyInfo);
            }

            return cubeMetadataInfo;
        }

        public static Dimension CreateDimensionMetadataInfo(Microsoft.AnalysisServices.AdomdClient.Dimension dimension)
        {
            if (dimension == null)
                return null;
            var dimensionMetadataInfo = new Dimension
                {
                    Caption = dimension.Caption,
                    Description = dimension.Description,
                    DimensionType = (DimensionType) (dimension.DimensionType),
                    Name = dimension.Name,
                    UniqueName = dimension.UniqueName
                };

            if (dimension.ParentCube != null)
            {
                dimensionMetadataInfo.ParentCubeId = dimension.ParentCube.Name;
                dimensionMetadataInfo.CustomPropertyInfos
                                     .Add(new PropertyInfo
                                         {
                                             Name = PropertiesBase.CUBE_CAPTION,
                                             Value = dimension.ParentCube.Caption
                                         });
            }

            var dimensionsToAdd = dimension.Properties
                                                                  .Cast<Property>()
                                                                  .Select(prop => new PropertyInfo
                                                                  {
                                                                      Name = prop.Name,
                                                                      Value = prop.Value
                                                                  }).ToList();
            foreach (var propertyInfo in dimensionsToAdd)
            {
                dimensionMetadataInfo.PropertyInfos.Add(propertyInfo);
            }

            return dimensionMetadataInfo;
        }

        public static Hierarchy CreateHierarchyMetadataInfo(Microsoft.AnalysisServices.AdomdClient.Hierarchy hierarchy)
        {
            if (hierarchy == null)
                return null;
            var hierarchyMetadataInfo = new Hierarchy
                {
                    Caption = hierarchy.Caption,
                    Description = hierarchy.Description,
                    DefaultMember = hierarchy.DefaultMember,
                    Name = hierarchy.Name,
                    HierarchyOrigin = (HierarchyOrigin) (hierarchy.HierarchyOrigin),
                    UniqueName = hierarchy.UniqueName
                };

            if (hierarchy.ParentDimension != null)
            {
                hierarchyMetadataInfo.ParentDimensionId = hierarchy.ParentDimension.UniqueName;
                hierarchyMetadataInfo.CustomPropertyInfos
                                     .Add(new PropertyInfo
                                         {
                                             Name = PropertiesBase.DIMENSION_CAPTION,
                                             Value = hierarchy.ParentDimension.Caption
                                         });

                if (hierarchy.ParentDimension.ParentCube != null)
                {
                    hierarchyMetadataInfo.ParentCubeId = hierarchy.ParentDimension.ParentCube.Name;
                    hierarchyMetadataInfo.CustomPropertyInfos
                                         .Add(new PropertyInfo
                                             {
                                                 Name = PropertiesBase.CUBE_CAPTION,
                                                 Value =
                                                     hierarchy.ParentDimension.ParentCube.Caption
                                             });
                }
            }

            #region Get Display Folder

            //TODO: move to separete method call
            try
            {
                hierarchyMetadataInfo.DisplayFolder = hierarchy.DisplayFolder;
            }
            catch (NotSupportedException)
            {
                hierarchyMetadataInfo.DisplayFolder = String.Empty;
            }

            #endregion

            var propertiesToAdd = hierarchy.Properties
                                                    .Cast<Property>()
                                                    .Select(prop => new PropertyInfo
                                                    {
                                                        Name = prop.Name,
                                                        Value = prop.Value
                                                    });
            foreach (var propertyInfo in propertiesToAdd)
            {
                hierarchyMetadataInfo.PropertyInfos.Add(propertyInfo);
            }


            return hierarchyMetadataInfo;
        }

        public static Level CreateLevelMetadataInfo(Microsoft.AnalysisServices.AdomdClient.Level level)
        {
            if (level == null)
                return null;
            var levelMetadataInfo = new Level
                {
                    Caption = level.Caption,
                    Description = level.Description,
                    LevelNumber = level.LevelNumber,
                    LevelType = (LevelType) (level.LevelType),
                    MemberCount = level.MemberCount,
                    Name = level.Name,
                    UniqueName = level.UniqueName
                };

            if (level.ParentHierarchy != null)
            {
                levelMetadataInfo.ParentHirerachyId = level.ParentHierarchy.UniqueName;
                levelMetadataInfo.CustomPropertyInfos.Add(new PropertyInfo
                    {
                        Name = PropertiesBase.HIERARCHY_CAPTION,
                        Value = level.ParentHierarchy.Caption
                    });
                if (level.ParentHierarchy.ParentDimension != null)
                {
                    levelMetadataInfo.ParentDimensionId = level.ParentHierarchy.ParentDimension.UniqueName;
                    levelMetadataInfo.CustomPropertyInfos.Add(new PropertyInfo
                        {
                            Name = PropertiesBase.DIMENSION_CAPTION,
                            Value = level.ParentHierarchy.ParentDimension.Caption
                        });
                    if (level.ParentHierarchy.ParentDimension.ParentCube != null)
                    {
                        levelMetadataInfo.ParentCubeId = level.ParentHierarchy.ParentDimension.ParentCube.Name;
                        levelMetadataInfo.CustomPropertyInfos.Add(new PropertyInfo
                            {
                                Name = PropertiesBase.CUBE_CAPTION,
                                Value = level.ParentHierarchy.ParentDimension.ParentCube.Caption
                            });
                    }
                }
            }

            var propertiesToAdd = level.Properties
                                            .Cast<Property>()
                                            .Select(property => new PropertyInfo
                                            {
                                                Name = property.Name,
                                                Value = property.Value
                                            }).ToList();
            foreach (var propertyInfo in propertiesToAdd)
            {
                levelMetadataInfo.PropertyInfos
                             .Add(propertyInfo);
            }

            return levelMetadataInfo;
        }

        public static Measure CreateMeasureMetadataInfo(Microsoft.AnalysisServices.AdomdClient.Measure measure)
        {
            if (measure == null)
                return null;
            var measureMetadataInfo = new Measure();
            try
            {
                measureMetadataInfo.Caption = measure.Caption;
                measureMetadataInfo.Description = measure.Description;
                measureMetadataInfo.Name = measure.Name;
                measureMetadataInfo.UniqueName = measure.UniqueName;
                measureMetadataInfo.Expression = measure.Expression;
                measureMetadataInfo.ParentCubeId = measure.ParentCube != null ? measure.ParentCube.Name : string.Empty;
                measureMetadataInfo.DisplayFolder = measure.DisplayFolder;
                //measureMetadataInfo = new Measure
                //{
                //    Caption = measure.Caption,
                //    Description = measure.Description,
                //    Name = measure.Name,
                //    UniqueName = measure.UniqueName,
                //    DisplayFolder = measure.DisplayFolder,
                //    Expression = measure.Expression,
                //    ParentCubeId = measure.ParentCube != null ? measure.ParentCube.Name : string.Empty
                //};
            }
            catch (Exception ex)
            {
            }

            //measureMetadataInfo.NumericPrecision = measure.NumericPrecision;
            //measureMetadataInfo.NumericScale = measure.NumericScale;
            //measureMetadataInfo.Units = measure.Units;

            var propertiesToAdd = measure.Properties
                                          .Cast<Property>()
                                          .Select(property => new PropertyInfo
                                          {
                                              Name = property.Name,
                                              Value = property.Value
                                          });
            foreach (var propertyInfo in propertiesToAdd)
            {
                measureMetadataInfo.PropertyInfos
                               .Add(propertyInfo);
            }
            
            return measureMetadataInfo;
        }

        public static string ConvertToNormalStyle(string name)
        {
            try
            {
                String result = String.Empty;
                if (!String.IsNullOrEmpty(name) && name.Length > 1)
                {
                    result = name;
                    if (name[0] == '[' && name[name.Length - 1] == ']')
                        result = name.Substring(1, name.Length - 2);
                }
                return result;
            }
            catch
            {
                return String.Empty;
            }
        }
    }
}