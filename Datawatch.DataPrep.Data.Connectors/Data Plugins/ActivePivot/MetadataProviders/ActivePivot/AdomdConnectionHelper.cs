﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Microsoft.AnalysisServices.AdomdClient;

namespace Panopticon.ActivePivot.MDXPlugin.MetadataProviders.ActivePivot
{
    public class AdomdConnectionHelper
    {
        public static AdomdConnection GetConnection(string connectionString)
        {
            var adomdConnection = new AdomdConnection(connectionString);
            adomdConnection.Open();
            return adomdConnection;
        }

        public static AdomdConnection GetConnection(ActivePivotSettings mdxSettings, IEnumerable<ParameterValue> parameters)
        {
            IParameterEncoder encoder = new ParameterEncoder();
            encoder.Parameters = parameters;

            string connectionString = BuildConnectionString(mdxSettings, parameters);
            if (!string.IsNullOrEmpty(mdxSettings.Catalog))
            {
                encoder.SourceString = mdxSettings.Catalog;
                var catalog = encoder.Encoded();
                connectionString = connectionString + ";Initial Catalog=" + catalog;
            }
            return GetConnection(connectionString);
        }

        internal static string BuildConnectionString(ActivePivotSettings settings, IEnumerable<ParameterValue> parameters)
        {
            if (settings == null || string.IsNullOrEmpty(settings.AdministrationUrl))
            {
                return string.Empty;
            }

            IParameterEncoder encoder = new ParameterEncoder();
            encoder.Parameters = parameters;

            StringBuilder connectionString = new StringBuilder(@"Provider=MSOLAP");

            encoder.SourceString = settings.ServerName;
            var serverName = encoder.Encoded();
            connectionString.Append("; Data Source=" + serverName);
            if (!string.IsNullOrEmpty(settings.UserName))
            {
                encoder.SourceString = settings.UserName;
                var userName = encoder.Encoded();
                connectionString.Append(";User ID=" + userName);
            }
            if (!string.IsNullOrEmpty(settings.Password))
            {
                encoder.SourceString = settings.Password;
                var password = encoder.Encoded();
                connectionString.Append(";Password=" + password);
            }

            return connectionString.ToString();
        }
    }
}