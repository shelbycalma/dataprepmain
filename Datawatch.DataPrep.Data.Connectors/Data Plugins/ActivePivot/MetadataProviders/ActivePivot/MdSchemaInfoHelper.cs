﻿using System;
using System.Data;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Framework;
using Microsoft.AnalysisServices.AdomdClient;
using CubeType = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.CubeType;
using Dimension = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension;
using Hierarchy = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy;
using Level = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level;
using Measure = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Measure;

namespace Panopticon.ActivePivot.MDXPlugin.MetadataProviders.ActivePivot
{
    public class MdSchemaInfoHelper
    {
        private readonly CubeDef _cube;

        public MdSchemaInfoHelper(CubeDef cube)
        {
            _cube = cube;
        }

        public void PopulateCubeSchemaInfo(Cube cubeMetadataInfo)
        {
            try
            {
                cubeMetadataInfo.Type = (CubeType) (int)_cube.Type;
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }
        }

        public void PopulateDimensionAndHierarchySchemaInfo(Cube cubeMetadataInfo)
        {
            try
            {
                foreach (var dimension in _cube.Dimensions)
                {
                    Dimension dim = new Dimension();
                    dim.Name = dimension.Name;
                    dim.UniqueName = dimension.UniqueName;
                    dim.Caption = dimension.Caption;
                    dim.DimensionType = (DimensionType)(int)dimension.DimensionType;
                    if(dim.DimensionType == DimensionType.Measure)
                    {
                    }
                    else
                    {
                        cubeMetadataInfo.Dimensions.Add(dim);

                        foreach (var hierarchy in dimension.Hierarchies)
                        {
                            var hier = new Hierarchy();
                            hier.Name = hierarchy.Name;
                            hier.UniqueName = hierarchy.UniqueName;
                            hier.Caption = hierarchy.Caption;
                            dim.HierarchyInfos.Add(hier);
                            foreach (var level in hierarchy.Levels)
                            {
                                var lev = new Level();
                                lev.Name = level.Name;
                                lev.Caption = level.Caption;
                                lev.UniqueName = level.UniqueName;
                                lev.LevelNumber = level.LevelNumber;
                                lev.LevelType = (LevelType)(int)level.LevelType;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }
        }

        public void PopulateMeasuresSchemaInfo(Cube cubeMetadataInfo)
        {
            try
            {
                foreach (var measure in _cube.Measures)
                {
                    Measure m = new Measure();
                    m.Name = measure.Name;
                    m.UniqueName = measure.UniqueName;
                    m.Caption = measure.Caption;
                    cubeMetadataInfo.Measures.Add(m);
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }
        }
    }
}
