﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.ActivePivot.MDXPlugin.MetadataProviders.ActivePivot;
using CubeType = Microsoft.AnalysisServices.AdomdClient.CubeType;
using Dimension = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension;
using Hierarchy = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy;
using Level = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level;

namespace Panopticon.ActivePivot.MDXPlugin.MetadataProviders
{
    public class ActivePivotMetaDataProvider : IMetadataProvider, IDisposable
    {
        protected ActivePivotSettings settings;
        private readonly IEnumerable<ParameterValue> _parameters;
        private AdomdConnection connection;

        public IDbConnection Connection
        {
            get
            {
                return connection;
            }
        }

        #region private
        protected string MakeCubeMetadataBackupString(Cube cubeMetadataInfo)
        {
            string hierarchyBackup = "";
            foreach (Dimension dimension in cubeMetadataInfo.Dimensions)
            {
                foreach (Hierarchy dimensionHierarchyInfo in dimension.HierarchyInfos)
                {
                    hierarchyBackup = hierarchyBackup + dimensionHierarchyInfo.UniqueName + "~" +
                                      dimensionHierarchyInfo.LevelInfos.Count + "~";
                    foreach (var levelInfo in dimensionHierarchyInfo.LevelInfos)
                    {
                        hierarchyBackup = hierarchyBackup + levelInfo.Name + "|";
                    }
                    hierarchyBackup = hierarchyBackup + "@";
                }
            }
            return hierarchyBackup;
        }

        #endregion

        #region constructor
        public ActivePivotMetaDataProvider(ActivePivotSettings mdxSettings, IEnumerable<ParameterValue> parameters, bool createConnection = true)
        {
            settings = mdxSettings;
            _parameters = parameters;
            if (createConnection)
            {
                connection = AdomdConnectionHelper
                    .GetConnection(mdxSettings, parameters);
            }
            else
            {
                connection = null;
            }
        }

        #endregion

        protected Cube GetCubeMetadata_AdomdInfoHelper(string cubeName)
        {
            Cube cubeMetadataInfo = null;
            try
            {
                var cube = FindCubeByName(cubeName, connection);
                if (cube != null)
                {
                    cubeMetadataInfo = AdomdInfoHelper.CreateCubeMetadataInfo(cube);

                    foreach (Microsoft.AnalysisServices.AdomdClient.Dimension dimension in cube.Dimensions)
                    {
                        if (dimension.DimensionType == DimensionTypeEnum.Measure)
                            continue;

                        var dimensionMetadataInfo = AdomdInfoHelper.CreateDimensionMetadataInfo(dimension);
                        cubeMetadataInfo.Dimensions.Add(dimensionMetadataInfo);

                        foreach (Microsoft.AnalysisServices.AdomdClient.Hierarchy hierarchy in dimension.Hierarchies)
                        {
                            Hierarchy hierarchyMetadata =
                                AdomdInfoHelper.CreateHierarchyMetadataInfo(hierarchy);
                            dimensionMetadataInfo.HierarchyInfos.Add(hierarchyMetadata);

                            foreach (Microsoft.AnalysisServices.AdomdClient.Level level in hierarchy.Levels)
                            {
                                Level levelMetadata =
                                    AdomdInfoHelper.CreateLevelMetadataInfo(level);
                                hierarchyMetadata.LevelInfos.Add(levelMetadata);
                            }
                        }
                    }

                    cubeMetadataInfo.Measures = cubeMetadataInfo.Measures.Concat(cube.Measures
                        .Cast<Microsoft.AnalysisServices.AdomdClient.Measure>()
                        .Select(AdomdInfoHelper.CreateMeasureMetadataInfo)).ToList();
                }
                return cubeMetadataInfo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #region provider implementation
        public Cube GetCubeMetaData(string cubeName)
        {
            Cube cubeMetadataInfo = GetCubeMetadata_AdomdInfoHelper(cubeName);
            if (cubeMetadataInfo == null)
            {
                cubeMetadataInfo = GetCubeMetadata_MdSchema(cubeName);
            }

            settings.RowDimensionColumnsBackup = MakeCubeMetadataBackupString(cubeMetadataInfo);
            return cubeMetadataInfo;
        }
        protected Cube GetCubeMetadata_MdSchema(
           string cubeName)
        {
            var cube = FindCubeByName(cubeName, connection);

            MdSchemaInfoHelper schemaInfoHelper = new MdSchemaInfoHelper(cube);

            var cubeMetadataInfo = new Cube();

            schemaInfoHelper.PopulateCubeSchemaInfo(cubeMetadataInfo);

            schemaInfoHelper.PopulateDimensionAndHierarchySchemaInfo(cubeMetadataInfo);

            schemaInfoHelper.PopulateMeasuresSchemaInfo(cubeMetadataInfo);

            return cubeMetadataInfo;
        }
        public IList<MeasureGroup> GetMeasureGroups(string cubeName)
        {
            throw new NotImplementedException();
        }

        public CubeDef FindCubeByName(string cubeName, AdomdConnection adomdConnection)
        {
            if (string.IsNullOrEmpty(cubeName))
            {
                return null;
            }
            return adomdConnection.Cubes.Cast<CubeDef>()
                           .FirstOrDefault(cubeDef => cubeDef.Name.ToLower() == cubeName.ToLower());
            
        }

        public IList<string> GetAllCubes()
        {
            var cubeNames = new List<string>(connection.Cubes.Count);
            foreach (var cube in connection.Cubes)
            {
                if (cube.Type == CubeType.Cube)
                {
                    cubeNames.Add(cube.Name);
                }
            }
            return cubeNames;
        }

        public IList<Catalog> GetAllCatalogs()
        {
            AdomdRestrictionCollection restrictions = new AdomdRestrictionCollection();
            DataSet resultSet = connection.GetSchemaDataSet("DBSCHEMA_CATALOGS", restrictions);
            var databases = new List<Catalog>();
            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                databases.Add(new Catalog
                {
                    Name = row["CATALOG_NAME"].ToString()
                });
            }


            return databases;
        }

        public void SetCatalogName(string catalogName)
        {
            if (!string.IsNullOrEmpty(catalogName) )
            {
                if (Connection.ConnectionString.IndexOf("initial catalog", StringComparison.CurrentCultureIgnoreCase) < 0)
                {
                    Connection.Dispose();
                    string connectionString = AdomdConnectionHelper.BuildConnectionString(settings, _parameters);
                    connection = AdomdConnectionHelper
                        .GetConnection(connectionString + ";Initial Catalog=" + catalogName);
                }
                else
                {
                    if (!string.IsNullOrEmpty(catalogName)
                            && Connection.Database != catalogName)
                    {
                        Connection.ChangeDatabase(catalogName);
                    }
                }
            }
        }
        #endregion

        public void Dispose()
        {
            if(Connection!= null)
            {
                Connection.Dispose();
            }
        }
    }
}
