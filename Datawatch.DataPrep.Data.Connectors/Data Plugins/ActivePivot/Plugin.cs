﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.ActivePivot.MDXPlugin;
using Panopticon.ActivePivot.MDXPlugin.Properties;


namespace Panopticon.ActivePivot.Static
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ActivePivotSettings>
    {
        internal const string PluginId = "ActivePivotStaticPlugin";
        internal const string PluginTitle = "ActivePivot Static Native";
        internal const string PluginType = DataPluginTypes.Database;
        internal const bool PluginIsObsolete = false;

        public const string partitionColumnName = "External Aggregate";

        private const string imageUri = "pack://application:,,,/" +
                                        "Panopticon.ActivePivotPlugin;component/mdx-plugin.gif";


        public Plugin()
        {
            try
            {
                var image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get
            {
                return LicenseCache.IsValid(GetType(), this);
            }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public override ActivePivotSettings CreateSettings(PropertyBag properties)
        {
            ActivePivotSettings mdxSettings = new ActivePivotSettings(properties, true);
            return mdxSettings;
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            CheckLicense();
            ActivePivotSettings mdxset = new ActivePivotSettings(settings,true);
            using (ActivePivotClientUtil activePivotClientUtil = new ActivePivotClientUtil(mdxset, parameters))
            {
                return activePivotClientUtil.Execute(mdxset);
            }
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            return GetData(workbookDir, dataDir, settings, parameters, rowcount);
        }
    }
}