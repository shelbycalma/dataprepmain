﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.ActivePivot.MDXPlugin;

namespace Panopticon.ActivePivot
{
    public class LoginInfo : ViewModelBase
    {
        private readonly ActivePivotSettings _mdxSettings;

        public LoginInfo(ActivePivotSettings mdxSettings)
        {
            _mdxSettings = mdxSettings;
        }

        public string ServerName
        {
            get
            {
                return _mdxSettings.ServerName;
            }
            set
            {
                _mdxSettings.ServerName = value;
                OnPropertyChanged("ServerName");
            }
        }

        public bool UseSSL
        {
            get { return _mdxSettings.UseSSL; }
            set
            {
                _mdxSettings.UseSSL = value;
                OnPropertyChanged("UseSSL");
            }
        }

        public string UserName
        {
            get { return _mdxSettings.UserName ?? string.Empty; }
            set
            {
                _mdxSettings.UserName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get { return _mdxSettings.Password ?? string.Empty; }
            set
            {
                _mdxSettings.Password = value;
                OnPropertyChanged("Password");
            }
        }
    }
}