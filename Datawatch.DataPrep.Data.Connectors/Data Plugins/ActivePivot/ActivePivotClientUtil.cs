﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.ActivePivot.MDXPlugin.MetadataProviders;
using Panopticon.ActivePivot.MDXPlugin.Properties;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.ActivePivot.Static;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;
using Level = Microsoft.AnalysisServices.AdomdClient.Level;
using Tuple = Microsoft.AnalysisServices.AdomdClient.Tuple;

namespace Panopticon.ActivePivot.MDXPlugin
{
    public class ActivePivotClientUtil: IDisposable
    {
        private Dictionary<string, string> encodedParametersMap;
        private readonly IMetadataProvider metadataProvider;
        private readonly ActivePivotSettings _settings;
        private readonly IEnumerable<ParameterValue> _parameters;

        public ActivePivotClientUtil(ActivePivotSettings mdxSettings, IEnumerable<ParameterValue> parameters)
        {
            metadataProvider = new ActivePivotMetaDataProvider(mdxSettings, parameters);
            metadataProvider.SetCatalogName(mdxSettings.Catalog);
            _settings = mdxSettings;
            _parameters = parameters;
        }

        public ITable Execute(ActivePivotSettings settings)
        { 
            var parameterEncoder = new ParameterEncoder
            {
                SourceString = settings.Query,
                Parameters = _parameters
            };
            string query = parameterEncoder.Encoded();
            if (string.IsNullOrEmpty(query))
            {
                throw new ApplicationException(Resources.ExNoData);
            }
            StandaloneTable table;
            Stopwatch sw = Stopwatch.StartNew();
            AdomdCommand cmd = new AdomdCommand(query, (AdomdConnection)this.metadataProvider.Connection);

            Log.Info(Properties.Resources.LogMDXExecutingQuery,
                cmd.CommandText);
            if (!string.IsNullOrEmpty(cmd.CommandText))
            {

                try
                {
                    CellSet cellSet = cmd.ExecuteCellSet();
                    table = CreateTable(cellSet, settings);
                }
                catch (AdomdConnectionException e)
                {
                    if(e.InnerException != null)
                    {
                        throw new ApplicationException(e.InnerException.Message);
                    }
                    throw;
                }

                Log.Info(Properties.Resources.LogMDXQueryCompleted,
                    table.RowCount, table.ColumnCount, sw.Elapsed.TotalSeconds);

                // We're creating a new StandaloneTable on every request, so allow
                // EX to freeze it.
                table.BeginUpdate();
                try
                {
                    table.CanFreeze = true;
                }
                finally
                {
                    table.EndUpdate();
                }

                return table;
            }
            else
            {
                table = new StandaloneTable();
                table.BeginUpdate();
                table.AddColumn(new TextColumn("EmptyCube"));
                try
                {
                    table.CanFreeze = true;
                }
                finally
                {
                    table.EndUpdate();
                }

                return table;
            }
        }

        //TODO this seems overcomplicated so ideally should be rewritten
        private StandaloneTable CreateTable(CellSet cellSet, ActivePivotSettings settings)
        {
            bool bUseBackupHierarchies = false;
            if (cellSet.Axes.Count == 0)
            {
                throw new Exception(Properties.Resources.ExNoData);
            }

            //current implementation only supports two exes
            if (cellSet.Axes.Count != 2)
            {
                throw new Exception(Properties.Resources.ExInvalidAxes);
            }

            if (!(cellSet.Axes[0].Positions.Count > 0) &&
                !(cellSet.Axes[1].Positions.Count > 0))
            {
                throw new Exception(Properties.Resources.ExNoData);
            }

            var actualLevelNames = new HashSet<string>();
            var columnsToRemove = new List<string>();
            foreach (var axis in cellSet.Axes)
            {
                foreach (var position in axis.Positions)
                {
                    foreach (var positionMember in position.Members)
                    {
                        actualLevelNames.Add(positionMember.LevelName);
                    }
                }
            }

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            // Go through the row axis.
            // Create text columns for each level of each hierarchy.
            HierarchyCollection rowHierarchies = cellSet.Axes[1].Set.Hierarchies;
            var hierarchyStartIndex = new int[rowHierarchies.Count];
            for (int i = 0; i < rowHierarchies.Count; i++)
            {
                Hierarchy hierarchy = rowHierarchies[i];
                hierarchyStartIndex[i] = table.ColumnCount;
                try
                {
                    Level lTest = hierarchy.Levels[0];
                }
                catch (Exception ex)
                {
                    bUseBackupHierarchies = true;
                }
                if (!bUseBackupHierarchies)
                {
                    foreach (Level level in hierarchy.Levels)
                    {
                        string columnName = hierarchy.Caption + " " + level.Caption;
                        TextColumn column = new TextColumn(columnName);
                        table.AddColumn(column);
                        if(!actualLevelNames.Contains(level.UniqueName))
                        {
                            columnsToRemove.Add(columnName);
                        }
                    }
                }
                else
                {
                    string[] backupHierarchies = settings.RowDimensionColumnsBackup.Split('@');
                    for (int h = 0; h <= backupHierarchies.GetUpperBound(0); h++)
                    {
                        string[] hierarchyParts = backupHierarchies[h].Split('~');
                        if (hierarchyParts[0].Equals(hierarchy.UniqueName) ||
                            hierarchyParts[0].Equals("[" + hierarchy.UniqueName + "]"))   // needed for SAP
                        {
                            string[] levels = hierarchyParts[2].Split('|');
                            for (int l = 0; l < levels.GetUpperBound(0); l++)
                            {
                                string columnName = hierarchyParts[0] + " " + levels[l];
                                TextColumn column = new TextColumn(columnName);
                                table.AddColumn(column);
                            }
                            break;
                        }
                    }
                }
            }
            string[] textValues = new string[table.ColumnCount];
            string[] raggedTextValues = new string[table.ColumnCount];

            // Go through the column axis.
            // Create one numeric column per tuple.
            TupleCollection columnTuples = cellSet.Axes[0].Set.Tuples;
            foreach (Tuple tuple in columnTuples)
            {
                StringBuilder columnName = new StringBuilder();
                foreach (Member member in tuple.Members)
                {
                    if (columnName.Length > 0)
                    {
                        columnName.Append(" ");
                    }
                    columnName.Append(member.Caption);
                }
                NumericColumn column = new NumericColumn(columnName.ToString());
                table.AddColumn(column);
            }
            if (settings.ExternalAggregates)
            {
                TextColumn column = new TextColumn(Plugin.partitionColumnName);
                table.AddColumn(column);
            }

            // Pull out the data
            TupleCollection rowTuples = cellSet.Axes[1].Set.Tuples;
            for (int rowTupleIndex = 0; rowTupleIndex < rowTuples.Count; rowTupleIndex++)
            {
                Tuple rowTuple = rowTuples[rowTupleIndex];
                IsRaggedToAdd isRaggedToAdd = IsRaggedToAdd.Undefined;
                bool[] raggedHierarchies = new bool[rowTuple.Members.Count];
                if (settings.FillRaggedHierarchies)
                {
                    for (int i = 0; i < rowTuple.Members.Count; i++)
                    {
                        Member member = rowTuple.Members[i];
                        if (member.GetChildren(0, 1).Count == 0)
                        {
                            if (member.LevelDepth + 1 != member.ParentLevel.ParentHierarchy.Levels.Count)
                            {
                                isRaggedToAdd = IsRaggedToAdd.Yes;
                                raggedHierarchies[i] = true;
                            }
                        }
                        else if (isRaggedToAdd == IsRaggedToAdd.Undefined)
                        {
                            isRaggedToAdd = IsRaggedToAdd.No;
                        }
                    }
                }

                bool isAggregateRow = false;
                for (int i = 0; i < rowTuple.Members.Count; i++)
                {
                    Member member = rowTuple.Members[i];
                    if (settings.FillRaggedHierarchies)
                    {
                        isAggregateRow = isAggregateRow || member.ChildCount > 0;
                        if (!raggedHierarchies[i] && settings.SkipAggregatedRows && isAggregateRow)
                        {
                            isRaggedToAdd = IsRaggedToAdd.No;
                        }
                    }
                    else
                    {
                        isAggregateRow = isAggregateRow || (member.ChildCount > 0 && !raggedHierarchies[i]);
                    }
                    int levelDepth = member.LevelDepth;
                    int textValueIndex = hierarchyStartIndex[i] + levelDepth;
                    string value = member.Caption;
                    textValues[textValueIndex] = value;
                    raggedTextValues[textValueIndex] = value;


                    // blank out row axis values to the right of this level
                    if (!bUseBackupHierarchies &&
                        (!IsParentSameAsPrevious(rowTuples, i, rowTupleIndex) || isRaggedToAdd == IsRaggedToAdd.Yes)
                         )
                    {
                        int lastIndex = member.ParentLevel.ParentHierarchy.Levels.Count
                                        - member.LevelDepth
                                        + textValueIndex;
                        for (int j = textValueIndex + 1; j < lastIndex; j++)
                        {
                            if (!IsParentSameAsPrevious(rowTuples, i, rowTupleIndex))
                            {
                                textValues[j] = null;
                            }
                            if (isRaggedToAdd == IsRaggedToAdd.Yes)
                            {
                                switch (settings.FillRaggedHierarchyType)
                                {
                                    case FillRaggedHierarchyTypes.Left:
                                        raggedTextValues[j] = "-";
                                        break;
                                    case FillRaggedHierarchyTypes.Right:
                                        raggedTextValues[j] = raggedTextValues[j - 1];
                                        break;
                                }
                            }
                        }
                    }
                }

                if (!settings.SkipAggregatedRows || !isAggregateRow)
                {
                    SetRow(table, textValues, columnTuples, cellSet, rowTupleIndex, isAggregateRow);
                }
                if (isRaggedToAdd == IsRaggedToAdd.Yes)
                {
                    switch (settings.FillRaggedHierarchyType)
                    {
                        case FillRaggedHierarchyTypes.Right:
                            SetRow(table, raggedTextValues, columnTuples, cellSet, rowTupleIndex, isAggregateRow);
                            break;
                        case FillRaggedHierarchyTypes.Left:
                            table.AddRow(raggedTextValues);
                            if (settings.ExternalAggregates)
                            {
                                AddExtraExternalAggregateRowIfNecessary(table, raggedTextValues);
                            }
                            break;
                    }
                }
            }

            //Remove columns
            foreach (var columnToRemove in columnsToRemove)
            {
                table.RemoveColumn(table.GetColumn(columnToRemove));
            }
#if DEBUG
            Log.Info("********************");
            for (int r = 0; r < table.RowCount; r++)
            {
                Row rw = table.GetRow(r);
                string s = "";
                for (int c = 0; c < table.ColumnCount; c++)
                {
                    var t = table.GetColumn(c).GetType();
                    if (t.Name.Equals("TextColumn"))
                    {
                        TextColumn cl = (TextColumn)table.GetColumn(c);
                        s = s + cl.GetTextValue(r) + ", ";
                    }
                    else
                    {
                        NumericColumn cl = (NumericColumn)table.GetColumn(c);
                        s = s + cl.GetNumericValue(r).ToString() + ", ";
                    }
                }
                Log.Info(s);
            }
            Log.Info("********************");
#endif
            table.EndUpdate();

            return table;
        }

        private bool IsParentSameAsPrevious(TupleCollection rowTuples, int memberIndex, int rowTupleIndex)
        {
            Tuple currentRowTuple = rowTuples[rowTupleIndex];
            Member currentMember = currentRowTuple.Members[memberIndex];

            //bool check = currentMember.ParentSameAsPrevious;
            bool result = false;
            if (rowTupleIndex != 0)
            {
                Tuple previousRowTuple = rowTuples[rowTupleIndex - 1];
                Member previousMember = previousRowTuple.Members[memberIndex];
                if (currentMember.Parent == null)
                {
                    if (previousMember.Parent == null)
                    {
                        result = currentMember.UniqueName.Equals(previousMember.UniqueName);
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    if (previousMember.Parent != null)
                        result = currentMember.Parent.UniqueName.Equals(previousMember.Parent.UniqueName);
                }
            }
            //if (result != check)
            //{
            //    throw new ApplicationException("Members check code is wrong");
            //}
            return result;
        }

        private void SetRow(StandaloneTable table, string[] textValues,
            TupleCollection columnTuples, CellSet cellSet, int rowTupleIndex,
            bool isAggregateRow)
        {
            object[] ExtAggRow = new object[table.ColumnCount];
            Row row = table.AddRow(textValues);
            if (_settings.ExternalAggregates)
            {
                textValues.CopyTo(ExtAggRow, 0);
            }

            for (int colTupleIndex = 0; colTupleIndex < columnTuples.Count; colTupleIndex++)
            {
                int columnIndex = textValues.Length + colTupleIndex;
                NumericColumn column = (NumericColumn)table.GetColumn(columnIndex);
                Cell cell = cellSet.Cells[colTupleIndex, rowTupleIndex];
                object value = cell.Value;

                double numericValue;
                if (value == null)
                {
                    numericValue = NumericValue.Empty;
                }
                else
                {
                    if (value is double)
                    {
                        numericValue = (double)value;
                    }
                    else if (value is string)
                    {
                        if (value.ToString().StartsWith("[day-"))
                        {
                            numericValue = Convert.ToDouble(value.ToString().Substring(value.ToString().LastIndexOf(']') + 1));
                        }
                        else
                        {
                            numericValue = NumericValue.Empty;
                        }
                    }
                    else
                    {
                        numericValue = Convert.ToDouble(value);
                    }
                }

                column.SetNumericValue(row, numericValue);
                if (_settings.ExternalAggregates)
                {
                    ExtAggRow[columnIndex] = numericValue;
                }
            }
            if (_settings.ExternalAggregates && !isAggregateRow)
            {
                ExtAggRow[table.ColumnCount - 1] = "leaf";
                table.AddRow(ExtAggRow);
            }
        }

        public string[] GetCatalogsList()
        {
            return metadataProvider.GetAllCatalogs().Select(c => c.Name).ToArray();
        }

        public string[] GetCubesList(string catalogName)
        {
            metadataProvider.SetCatalogName(catalogName);
            return metadataProvider.GetAllCubes().ToArray();
        }

        public Cube GetCubeMetadata(string catalogName, string cubeName)
        {
            metadataProvider.SetCatalogName(catalogName);
            return metadataProvider.GetCubeMetaData(cubeName);
        }

        private object GetParameterValue(TypedParameterValue typedParameterValue)
        {
            if (typedParameterValue is StringParameterValue)
            {
                return ((StringParameterValue) typedParameterValue).Value;
            }
            if (typedParameterValue is DateTimeParameterValue)
            {
                return ((DateTimeParameterValue) typedParameterValue).Value;
            }
            if (typedParameterValue is NumberParameterValue)
            {
                return ((NumberParameterValue) typedParameterValue).Value;
            }

            throw new ArgumentException(string.Format(Resources.ExInvalidParameterType,
                typedParameterValue.GetType()));
        }

        protected void AddExtraExternalAggregateRowIfNecessary(
            StandaloneTable table, object[] row)
        {
            // check to see if this is a leaf row and therefore
            // needs a special external aggregate row added
            bool isExternalAggregateRow = false;
            //bool isFilterBoxTable = onDemandParameters != null &&
            //    onDemandParameters.Domains != null &&
            //    onDemandParameters.Domains.Any();
            //if (!isFilterBoxTable)
            //{
            //    for (int colchk = 0; colchk < dataStartColumn; colchk++)
            //    {
            //        var col = table.GetColumn(colchk);
            //        if (table.ColumnsToUse.Contains(col.Name) &&
            //            row[colchk] == null)
            //        {
            //            isExternalAggregateRow = true;
            //            break;
            //        }
            //    }
            //}
            if (!isExternalAggregateRow)
            {
                object[] newRowBottom = new object[row.Length];
                row.CopyTo(newRowBottom, 0);
                newRowBottom[table.ColumnCount - 1] = "leaf";
                table.AddRow(newRowBottom);
            }
        }

        private enum IsRaggedToAdd
        {
            Undefined,
            Yes,
            No
        }

        public void Dispose()
        {
            var disposable = metadataProvider as IDisposable;
            if (disposable != null) disposable.Dispose();
        }
    }
}