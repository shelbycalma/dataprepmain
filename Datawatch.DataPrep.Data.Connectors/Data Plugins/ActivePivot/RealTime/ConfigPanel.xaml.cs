﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Panopticon.Dashboards.Model;
using Panopticon.Dashboards.UI;
using Panopticon.Dashboards.ViewModel;
using Panopticon.ActivePivot.MdxQueryBuilder;
using Panopticon.ActivePivot.Metadata;

namespace Panopticon.ActivePivot
{
    internal partial class ConfigPanel
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                                        typeof (ActivePivotSettingsViewModel), typeof (ConfigPanel),
                                        new PropertyMetadata(Settings_Changed));

        
        
        public ConfigPanel()
        {
            InitializeComponent();
        }

        public ActivePivotSettingsViewModel Settings
        {
            get { return (ActivePivotSettingsViewModel) GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        

        

        private static void Settings_Changed(DependencyObject obj,
                                             DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (ActivePivotSettingsViewModel) args.OldValue,
                (ActivePivotSettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(ActivePivotSettingsViewModel oldSettings,
                                         ActivePivotSettingsViewModel newSettings)
        {
            DataContext = newSettings;

            this.UiPasswordBox.Password = newSettings != null ? newSettings.Model.Password : null;
        }

        

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.LoginInfo.Password = UiPasswordBox.Password;
            }
        }


        private void ManualEditChecked(object sender, RoutedEventArgs e)
        {
            var checkboxValue = (bool) ((CheckBox) sender).IsChecked;
            MeasuresDimensionsExpander.IsExpanded = !checkboxValue;
            Settings.Model.Query
                = checkboxValue ? Settings.ManualQuery : Settings.GeneratedQuery;
        }

        private void ParameterChanged(object sender, RoutedEventArgs e)
        {
            Settings.GenerateMdxQuery();
        }
    }
}