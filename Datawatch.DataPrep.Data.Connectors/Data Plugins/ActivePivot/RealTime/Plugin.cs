﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Panopticon.ActivePivot.MDXPlugin;
using Panopticon.ActivePivot.Properties;
using Panopticon.Dashboards.Data.Plugin;
using Panopticon.Dashboards.Model;
using Panopticon.Dashboards.Plugin;
using Panopticon.Developer.Framework.Table;
using Panopticon.Developer.Licensing;


namespace Panopticon.ActivePivot
{
    [PluginDescription("MDX", "Database")]
    [LicenseProvider(typeof(PanopticonXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin<ParameterTable, ActivePivotSettings, LPEvent, ActivePivotAdapter>
    {
        public const string PluginId = "ActivePivotMDXPlugin";

        private const string imageUri = "pack://application:,,,/" +
                                        "Panopticon.ActivePivotPlugin;component/mdx-plugin.gif";


        public Plugin()
        {           

            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get
            {
                return LicenseCache.IsValid(GetType(), this);
            }
        }

        public override string Title
        {
            get { return Resources.UiPluginTitle; }
        }

        protected override ITable CreateTable(ActivePivotSettings settings, IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            CheckLicense();

            ActivePivotClientUtil activePivotClientUtil = new ActivePivotClientUtil(settings);

            return activePivotClientUtil.Execute(settings, parameters);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        protected override ActivePivotSettings CreateSettings(PropertyBag properties)
        {
            ActivePivotSettings mdxSettings = new ActivePivotSettings(properties, false);
            return mdxSettings;
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            ActivePivotSettings mdxSettings = new ActivePivotSettings(new PropertyBag(), false);
            ActivePivotSettingsViewModel settingsView =
                new ActivePivotSettingsViewModel(mdxSettings, parameters);
            ConfigWindow window =
                new ConfigWindow(settingsView);

            if (host is IWindowsHost)
            {
                window.Owner = ((IWindowsHost) host).MainWindow;
            }

            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            mdxSettings.Title = mdxSettings.Cube;
            settingsView.SaveParameters();
            PropertyBag properties = mdxSettings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ActivePivotSettings mdxSettings = CreateSettings(bag);

            ActivePivotSettingsViewModel activePivotSettingsViewModel =
                new ActivePivotSettingsViewModel(mdxSettings, parameters);
            ConfigPanel panel = new ConfigPanel();
            panel.Settings = activePivotSettingsViewModel;
            activePivotSettingsViewModel.LoadAllDataBySettings();
            return panel;
        }

        

       
        public override PropertyBag GetSetting(object element)
        {
            ConfigPanel panel = (ConfigPanel) element;
            panel.Settings.SaveParameters();
            return panel.Settings.Model.ToPropertyBag();
        }
    }
}