﻿using System;
using System.Collections.Generic;
using Panopticon.ActivePivot.MDXPlugin;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;

namespace Panopticon.ActivePivot.MdxQueryBuilder
{
    public class QueryBuilder: AbstractQueryBuilder
    {
        private readonly ActivePivotSettings settings;
        
        public QueryBuilder(ActivePivotSettings settings): base(settings)
        {
            this.settings = settings;
        }
    }
}