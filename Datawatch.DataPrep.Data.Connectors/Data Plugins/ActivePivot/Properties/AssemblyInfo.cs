﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using System.Runtime.InteropServices;
using Panopticon.ActivePivot.Static;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.ActivePivot.MDXPlugin.dll")]
[assembly: AssemblyDescription("ActivePivot MDX Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("cc1dcb21-2418-4e48-9d69-34bea71b0c30")]
