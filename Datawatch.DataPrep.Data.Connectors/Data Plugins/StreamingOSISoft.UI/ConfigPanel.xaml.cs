﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.StreamingOSISoftPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    internal partial class ConfigPanel
    {
        #region Private Variables

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                typeof (StreamingOSISoftViewModel),
                typeof (ConfigPanel), new PropertyMetadata(SettingsChanged));

        #endregion

        #region Constructors

        public ConfigPanel()
            : this(null)
        {
        }

        public ConfigPanel(StreamingOSISoftSettings settings)
        {
            InitializeComponent();

            ReconstituteSettings(settings);

            OsiSettings = settings;
        }

        #endregion

        #region Properties

        public StreamingOSISoftViewModel ViewModel
        {
            get
            {
                return (StreamingOSISoftViewModel) GetValue(ViewModelProperty);
            }
            set
            {
                SetValue(ViewModelProperty, value);
                OsiSettings = value.OsiSettings;
            }
        }

        public StreamingOSISoftSettings OsiSettings { get; set; }

        public StreamingOSISoftHelper OsiHelper
        {
            get { return ViewModel.OsiHelper; }
        }

        #endregion

        #region General Methods

        private static void SettingsChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (StreamingOSISoftViewModel) args.OldValue,
                (StreamingOSISoftViewModel) args.NewValue);
            ((ConfigPanel) obj).OsiSettings =
                ((StreamingOSISoftViewModel) args.NewValue).OsiSettings;
        }

        protected void OnSettingsChanged(
            StreamingOSISoftViewModel oldModel,
            StreamingOSISoftViewModel newModel)
        {
            DataContext = newModel;
        }

        private void ReconstituteSettings(StreamingOSISoftSettings settings)
        {
            if (ViewModel != null || settings == null) return;

            ViewModel = new StreamingOSISoftViewModel(settings,
                new StreamingOSISoftHelper(settings,
                new MessageBoxBasedErrorReportingService()));

            ViewModel.InitializeDatabases();
            ViewModel.InitializeTreeView();
            ViewModel.InitializeAttributeList();
            PopulateAttributeSelections();
            PopulatePasswordBox();
        }

        private void ConnectOnClick(object sender, RoutedEventArgs e)
        {
            OsiSettings.IsConnected = OsiHelper.Connect();
            if (OsiSettings.IsConnected)
                ViewModel.InitializeDatabases();
        }

        private void PopulatePasswordBox()
        {
            PasswordBox.Password = OsiSettings.Password;
        }

        private void PopulateAttributeSelections()
        {
            List<string> attrNames = new List<string>(
                OsiSettings.Attributes.Split(new[] {'\n'},
                    StringSplitOptions.RemoveEmptyEntries));

            foreach (string attrListItem in ViewModel.AttributeList)
            {
                if (attrNames.Contains(attrListItem))
                    AttributeListBox.SelectedItems.Add(attrListItem);
            }
        }

        private void LoadAttributesOnClick(object sender, RoutedEventArgs e)
        {
            OsiHelper.CreateColumns();
            ViewModel.SerializeNodeSelection();
            ViewModel.InitializeAttributeList();
        }

        private void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            OsiSettings.Password = PasswordBox.Password;
        }

        private void OnDatabaseChanged(
            object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            ViewModel.InitializeTreeView();
        }

        private void OnAttributeChanged(
            object sender, SelectionChangedEventArgs e)
        {
            List<string> selectedAttrs = new List<string>();
            foreach (var attrName in AttributeListBox.SelectedItems)
            {
                selectedAttrs.Add(attrName.ToString());
            }
            OsiSettings.Attributes = String.Join("\n", selectedAttrs);
        }

        private void OnTreeSelectionChanged(object sender, RoutedEventArgs e)
        {
            ViewModel.ClearAttributeSelection();
        }

        private void ImportTreeSelectionsOnClick(
            object sender, RoutedEventArgs e)
        {
            ViewModel.SerializeNodeSelection();
            OsiSettings.ManualElementPaths = OsiSettings.ElementPaths;
            OsiSettings.ManualAttributes = OsiSettings.Attributes;
        }

        #endregion
    }
}