﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using OSIsoft.AF;
using OSIsoft.AF.Asset;

namespace Panopticon.StreamingOSISoftPlugin.UI
{
    internal class StreamingOSISoftViewModel : ViewModelBase
    {
        #region Private Variables

        private StreamingOSISoftSettings settings;

        #endregion

        #region Constructors

        public StreamingOSISoftViewModel(
            StreamingOSISoftSettings osiSettings,
            StreamingOSISoftHelper osiHelper)
        {
            OsiSettings = osiSettings;

            OsiHelper = osiHelper 
                ?? new StreamingOSISoftHelper(osiSettings,
                    new MessageBoxBasedErrorReportingService());

            if (!OsiHelper.IsConnected)
                OsiHelper.Connect();

            InitializeSystems();
        }

        #endregion

        #region Properties

        public StreamingOSISoftSettings OsiSettings
        {
            get { return settings; }
            set
            {
                settings = value;
                OnPropertyChanged("OsiSettings");
            }
        }

        public StreamingOSISoftHelper OsiHelper { get; set; }

        public List<string> AvailableSystems { get; set; }
        public List<string> AvailableDatabases { get; set; }
        public List<string> AttributeList { get; set; }
        public ObservableCollection<Node> NodeCollection { get; set; }

        public static string[] PointTypes
        {
            get { return new[] {"Recorded Points", "Plotted Values"}; }
        }

        #endregion

        public bool IsOk()
        {
            if (OsiSettings.IsTreeSelectionMode)
            {
                return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.ElementPaths) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.Attributes);
            }

            if (OsiSettings.IsManualSelectionMode)
            {
                return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.ManualElementPaths) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.ManualAttributes);
            }

            return false;
        }

        public void InitializeSystems()
        {
            List<string> systemNames = new List<string>();

            foreach (PISystem system in OsiHelper.Systems)
            {
                systemNames.Add(system.Name);
            }

            AvailableSystems = systemNames;
            OnPropertyChanged("AvailableSystems");
        }

        public void InitializeDatabases()
        {
            List<string> databaseNames = new List<string>();

            foreach (AFDatabase db in OsiHelper.System.Databases)
            {
                databaseNames.Add(db.Name);
            }

            AvailableDatabases = databaseNames;
            OnPropertyChanged("AvailableDatabases");
        }

        public void InitializeTreeView()
        {
            NodeCollection = String.IsNullOrWhiteSpace(settings.ElementPaths)
                ? CreateElementTree(OsiHelper.Database.Elements)
                : PathStringToNodeSelection();

            OnPropertyChanged("NodeCollection");
        }

        public void SerializeNodeSelection()
        {
            if (NodeCollection == null) return;
            //Save the element paths in settings.
            List<string> paths = new List<string>();
            foreach (Node node in NodeCollection)
            {
                foreach (string path in node.GetPathList())
                {
                    if (!paths.Contains(path))
                        paths.Add(path);
                }
            }
            OsiSettings.ElementPaths = String.Join("\n", paths);
        }

        public void InitializeAttributeList()
        {
            string[] paths = OsiSettings.ElementPaths.Split(
                new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

            List<string> attrNameList = new List<string>();
            AFElementTemplate lastSeen = new AFElementTemplate();

            foreach (KeyValuePair<string, AFElement> ele
                in OsiHelper.GetElementByPaths(paths))
            {
                if (ele.Value.Template == lastSeen) continue;

                lastSeen = ele.Value.Template;

                foreach (AFAttribute attr in ele.Value.Attributes)
                {
                    if (!attrNameList.Contains(attr.Name))
                        attrNameList.Add(attr.Name);
                }
            }

            AttributeList = attrNameList;
            OnPropertyChanged("AttributeList");
        }

        public void ClearAttributeSelection()
        {
            AttributeList = new List<string>();
            OnPropertyChanged("AttributeList");

            OsiSettings.Attributes = String.Empty;
        }

        public ObservableCollection<Node> CreateElementTree(
            AFElements eles,
            Action nodeSelectionChangedAction = null,
            HashSet<AFElement> encounteredElements = null)
        {
            if (encounteredElements == null)
                encounteredElements = new HashSet<AFElement>();
            ObservableCollection<Node> nodes = new ObservableCollection<Node>();

            //base case: no children
            foreach (AFElement ele in eles)
            {
                if (!encounteredElements.Contains(ele))
                {
                    //add ele node 
                    Node eleNode = new Node(ele.Name, nodeSelectionChangedAction);
                    nodes.Add(eleNode);

                    //recurse and add ele children
                    if (ele.HasChildren)
                    {
                        HashSet<AFElement> pathElements =
                            new HashSet<AFElement>(encounteredElements) {ele};
                        //recursive step
                        ObservableCollection<Node> children =
                            CreateElementTree(ele.Elements,
                                encounteredElements: pathElements);
                        foreach (Node eleChild in children)
                        {
                            eleNode.AddNode(eleChild);
                        }
                    }
                }
            }
            return nodes;
        }

        public ObservableCollection<Node> PathStringToNodeSelection()
        {
            string[] pathList = OsiSettings.ElementPaths.Split(
                new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

            if (NodeCollection == null)
                NodeCollection = CreateElementTree(OsiHelper.Database.Elements);

            //parse each path(relative to db)
            foreach (string pathStr in pathList)
            {
                //temp variables to hold nodes/names we are looking at
                string currNodeName = "";
                ObservableCollection<Node> currCollection = NodeCollection;
                bool pathIsValid = true;

                //parse the path and find the nodes in the tree
                for (int i = 0; i < pathStr.Length; i++)
                {
                    if (pathIsValid)
                    {
                        //case: path ends with "\" - which selects all from that directory
                        if (pathStr.Substring(i, 1).Equals(@"\") &&
                            i + 1 == pathStr.Length)
                        {
                            int index = -1;
                            //find the node by name in the collection
                            foreach (Node n in currCollection)
                            {
                                if (n.Name.Equals(currNodeName))
                                {
                                    index = currCollection.IndexOf(n);
                                    break;
                                }
                            }
                            //try to access the next node in the path, if it's not found skip the rest of the path
                            try
                            {
                                currCollection = currCollection[index].Children;
                                foreach (Node n in currCollection)
                                {
                                    NodeCollection.Add(n);
                                    n.IsChecked = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.ToString());
                                pathIsValid = false;
                            }
                        }
                            //case: "\" between two node names
                        else if (pathStr.Substring(i, 1).Equals(@"\"))
                        {
                            int index = -1;
                            //find the node by name in the collection
                            foreach (Node n in currCollection)
                            {
                                if (n.Name.Equals(currNodeName))
                                {
                                    index = currCollection.IndexOf(n);
                                    break;
                                }
                            }
                            //try to access the next node in the path, if it's not found skip the rest of the path
                            try
                            {
                                currCollection = currCollection[index].Children;
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                pathIsValid = false;
                            }

                            currNodeName = "";
                        }
                            //case: path ends in last letter of node name
                        else if (i + 1 == pathStr.Length)
                        {
                            currNodeName += pathStr.Substring(i, 1);
                            foreach (Node n in currCollection)
                            {
                                if (n.Name.Equals(currNodeName))
                                {
                                    n.IsChecked = true;
                                    NodeCollection.Add(n);
                                    break;
                                }
                            }
                        }
                            //case: normal character - build the node name
                        else
                        {
                            currNodeName += pathStr.Substring(i, 1);
                        }
                    }
                }
            }

            return NodeCollection;
        }
    }
}