﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.StreamingOSISoftPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    internal partial class ConfigWindow
    {
        public StreamingOSISoftViewModel ViewModel { get; set; }
        public StreamingOSISoftSettings OsiSettings { get; set; }


        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof (ConfigWindow));

        public ConfigWindow(
            StreamingOSISoftSettings osiSettings,
            StreamingOSISoftViewModel osiView)
        {
            osiView.OsiSettings = osiSettings;
            ViewModel = osiView;
            OsiSettings = osiSettings;

            InitializeComponent();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ViewModel.IsOk();
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ClickOK(object sender, RoutedEventArgs e)
        {
            new RoutedCommand("Ok", typeof (ConfigWindow));
        }
    }
}