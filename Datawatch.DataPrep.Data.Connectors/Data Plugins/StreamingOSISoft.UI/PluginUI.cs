﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.StreamingOSISoftPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, StreamingOSISoftSettings,
            Dictionary<string, object>, StreamingOSISoftDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner,
            "pack://application:,,,/Panopticon.Dashboards.Examples.StreamingOSISoftPlugin.UI;component/aws-small.png")
        {
        }

        public override Window CreateConnectionWindow(
            StreamingOSISoftSettings osiSettings)
        {
            return new ConfigWindow(osiSettings,
                new StreamingOSISoftViewModel(osiSettings, null));
        }

        public override object GetConfigElement(
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return
                new ConfigPanel(new StreamingOSISoftSettings(Plugin.PluginManager, bag,
                    parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            StreamingOSISoftSettings s = cp.OsiSettings;
            cp.OsiHelper.CreateColumns();
            return cp.OsiSettings.ToPropertyBag();
        }
    }
}
