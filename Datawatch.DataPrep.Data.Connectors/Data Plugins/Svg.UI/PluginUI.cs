﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Microsoft.Win32;
using Panopticon.SvgPlugin.UI.Properties;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.SvgPlugin.UI
{
    public class PluginUI : IFileOpenDataPluginUI<Plugin>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.Dashboards.Common;component/" +
            "Images/SvgLoader.png";

        private static readonly string[] fileExtensions = new[] { "svg" };

        private Window owner;

        public PluginUI(Plugin plugin, Window owner)
        {
            Plugin = plugin;
            this.owner = owner;

            try
            {
                Image = new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            SvgConfigPanel configPanel = new SvgConfigPanel(this);
            configPanel.Path = bag.Values[Plugin.PathKey];
            return configPanel;
        }

        public PropertyBag GetSetting(object obj)
        {
            SvgConfigPanel configPanel = (SvgConfigPanel)obj;
            PropertyBag bag = new PropertyBag();
            bag.Values[Plugin.PathKey] = configPanel.Path;
            return bag;
        }

        public PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            if (!this.IsLicensed)
            {
                throw Exceptions.PluginNotLicensed(GetType());
            }

            string file = BrowseForFile();
            if (file == null || !file.ToLower().EndsWith(".svg"))
            {
                return null;
            }

            PropertyBag settings = new PropertyBag();
            settings.Values[Plugin.PathKey] = file;
            return settings;
        }

        public string GetTitle(PropertyBag settings)
        {
            string path = settings.Values[Plugin.PathKey];
            return path.Substring(
                path.LastIndexOf(Path.DirectorySeparatorChar) + 1);
        }

        public Plugin Plugin { get; private set; }
        public string Id { get { return Plugin.Id; } }
        public ImageSource Image { get; private set; }
        public bool IsLicensed { get { return Plugin.IsLicensed; } }

        public bool CanOpen(string filePath)
        {
            return DataUtils.CanOpen(filePath, fileExtensions);
        }

        public PropertyBag DoOpenFile(string filePath)
        {
            if (!this.IsLicensed)
            {
                throw Exceptions.PluginNotLicensed(GetType());
            }

            PropertyBag settings = new PropertyBag();
            settings.Values[Plugin.PathKey] = filePath;
            return settings;
        }

        public PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new NotImplementedException();
        }

        public string[] FileExtensions
        {
            get { return fileExtensions; }
        }

        public string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Resources.UiSvgFiles, fileExtensions);
            }
        }

        //TODO: Should use DataPluginUtils.BrowseForFile
        internal string BrowseForFile(string oldFileName)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.FileName = oldFileName;
            dlg.Filter = string.Concat(Resources.UiSvgFilter, "|*.svg");
            string dir = SettingsHelper.GetLastDataDirectory(Plugin.GlobalSettings);
            if (dir != null && Directory.Exists(dir))
            {
                dlg.InitialDirectory = dir;
            }
            if (dlg.ShowDialog(owner) == true)
            {
                string fileName = dlg.FileName;

                SettingsHelper.SetLastDataDirectory(Plugin.GlobalSettings, Path.GetDirectoryName(fileName));

                return fileName;
            }
            return null;
        }

        private string BrowseForFile()
        {
            return BrowseForFile(null);
        }
    }
}
