﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Panopticon.SvgPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class SvgConfigPanel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string path;
        private PluginUI pluginUI;

        public SvgConfigPanel(PluginUI pluginUI)
        {
            this.pluginUI = pluginUI;
            InitializeComponent();
            DataContext = this;
        }

        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Path"));
                }
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = pluginUI.BrowseForFile(Path);
            if (path != null)
            {
                Path = path;
            }
        }
    }
}
