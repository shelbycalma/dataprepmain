﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.JsonPlugin.UI
{
    /// <summary>
    /// The ConfigPanel implementa a WPF UserControl allowing editing of data 
    /// connections for the streaming DataPlugin. The ConfigPanel is used  both 
    /// when a connection is created and when edited in the data source settings
    /// dialog.
    /// </summary>

    internal partial class ConfigPanel : UserControl
    {

        private static readonly string FileFilter = string.Concat(Properties.Resources.UiJsonDialogFilter, "|*.json");
        private JsonSettings settings;
        private Window owner;
        public PropertyBag GlobalSettings { get; set; }

        /// <summary>
        /// Creates a new instance of the ConfigPanel.
        /// </summary>
        public ConfigPanel() : this(null, null)
        {

        }

        public ConfigPanel(JsonSettings settings, Window owner)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            this.owner = owner;
            DataContextChanged +=
                UserControl_DataContextChanged;
            if (settings != null)
            {
                PasswordBox.Password = settings.WebPassword;
            }
        }

        /// <summary>
        /// Gets or sets the current JsonSettings instance.
        /// </summary>
        public JsonSettings Settings
        {
            get { return settings; }
        }

        private void Delete_CanExecute(object sender,
            CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter is ColumnDefinition;
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Settings.ParserSettings.RemoveColumnDefinition(
                (Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing.ColumnDefinition) e.Parameter);
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = DataPluginUtils.BrowseForFile(owner,
                Settings.FilePath, FileFilter, GlobalSettings);
            if (path != null)
            {
                Settings.FilePath = path;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as JsonSettings;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.WebPassword = PasswordBox.Password;
            }
        }
    }

}
