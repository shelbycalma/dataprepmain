﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.JsonPlugin.UI.Properties;

namespace Panopticon.JsonPlugin.UI
{
    public class PluginUI : TextPluginUIBase<JsonSettings, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner,
            "pack://application:,,,/Panopticon.JsonPlugin.UI;component/json-plugin.png")
        {
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            JsonSettings settings =
                new JsonSettings(Plugin.PluginManager, bag, parameters);
            return new ConfigPanel(settings, owner);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel conWindow = (ConfigPanel)obj;
            return conWindow.Settings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(JsonSettings settings)
        {
            return new ConfigWindow(settings, Plugin.GlobalSettings);
        }

        private static readonly string[] fileExtensions = new[] { "json" };

        public override string[] FileExtensions
        {
            get { return fileExtensions; }
        }

        public override string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Resources.UiJsonFiles, fileExtensions);
            }
        }
    }
}
