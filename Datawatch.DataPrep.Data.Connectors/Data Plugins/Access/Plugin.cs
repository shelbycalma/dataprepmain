﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.AccessPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<AccessSettings>
    {
        internal const string PluginId = "Access Plugin";
        internal const string PluginTitle = "Access";
        internal const string PluginType = DataPluginTypes.File;
        internal const bool PluginIsObsolete = false;

        #region DataPluginBase<C>
        public override string Id
        {
            get { return PluginId; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override ITable GetData(string workbookDir, string dataDir, 
            PropertyBag bag, IEnumerable<ParameterValue> parameters, 
            int rowcount, bool applyRowFilteration)
        {
            AccessSettingsViewModel connectionSettings = new AccessSettingsViewModel(bag, this.errorReporter);
            connectionSettings.FixFileName(
                DataUtils.FixFilePath(workbookDir,
                    dataDir, connectionSettings.FileName, parameters));

            ITable table = connectionSettings
                .BuildTable(rowcount, applyRowFilteration, parameters);
            return table;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override AccessSettings CreateSettings(PropertyBag bag)
        {
            return new AccessSettings(bag);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            return new[] { new AccessSettings(settings).FileName };
        }

        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            AccessSettings accessSettings = new AccessSettings(settings);

            if (Path.GetFileName(accessSettings.FileName) ==
                Path.GetFileName(oldPath))
            {
                accessSettings.FileName =
                    Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }

        #endregion DataPluginBase<C>
    }
}
