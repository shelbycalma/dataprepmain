﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AccessPlugin
{
    public class AccessSettings : ConnectionSettings 
    {
        #region Fields
        private ObservableCollection<AccessColumn> columns;
        private ObservableCollection<string> tableNames;
        
        #endregion Fields

        #region Properties

        /// <summary>
        /// The full filename of the Access file.
        /// </summary>
        public string FileName
        {
            get { return GetInternal("FileName"); }
            set
            {
                if (ValidAccdbFile(value))
                {
                    SetInternal("FileName", value);
                }
            }
        }

        /// <summary>
        /// The name of the table to open contained within the Access file.
        /// </summary>
        public string TableName {
            get { return GetInternal("TableName"); }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    SetInternal("TableName", value);
                    Title = value;
                }
            }
        }

        /// <summary>
        /// The columns selected by the user.
        /// </summary>
        public ObservableCollection<AccessColumn> Columns
        {
            get { return columns
                ?? (columns = new ObservableCollection<AccessColumn>()); }
        }

        public IEnumerable<string> SelectedColumns
        {
            get
            {
                return Columns.Where(s => s.Select)
                              .Select(c => c.ColumnName);
            }
        }

        public ObservableCollection<string> TableNames
        {
            get
            {
                return tableNames
                    ?? (tableNames = new ObservableCollection<string>());
            }
        }

        public bool IsValid { get { return TableNames.Contains(TableName); } }
        #endregion Properties

        #region Constructors
        /// <summary>
        /// Constructor: AccessSettings from an initialized property bag.
        /// </summary>
        /// <param name="bag"></param>
        public AccessSettings(PropertyBag bag) : base(bag)
        {
           
        }

        /// <summary>
        /// Constructor: AccessSettings with a file.
        /// This constructor will create a new property bag.
        /// </summary>
        public AccessSettings(string fileName)
            : base(new PropertyBag())
        {
            FileName = fileName;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Make sure the passed file is a valid accdb file by:
        /// checking that the file has the proper extension, and exists.
        /// </summary>
        /// <param name="file">file to check.</param>
        /// <returns>True if file  is a valid accdb file.</returns>
        private static bool ValidAccdbFile(string file)
        {
            if (string.IsNullOrWhiteSpace(file))
            {
                return false;
            }

            if (AccessUtils.FileExtensions.Contains(
                Path.GetExtension(file).Replace(".", "").ToLower()))
            {
                return true;
            }
            return false;
        }

        public override PropertyBag ToPropertyBag()
        {
            var bag = base.ToPropertyBag();
            int i = 0;
            var colBag = new PropertyBag();
            foreach (var col in Columns)
            {
                if (col.Select)
                {
                    colBag.SubGroups["Column_" + i++] = col.GetPropertyBag();
                }
            }
            bag.SubGroups["Columns"] = colBag;
            return bag;
        }

        #endregion Methods
    }
}
