﻿using System.ComponentModel;

namespace Panopticon.AccessPlugin
{
    public enum ColumnType
    {
        [Description("UiColumnNumeric")]
        Numeric,
        [Description("UiColumnText")]
        Text,
        [Description("UiColumnTime")]
        Time
    }
}