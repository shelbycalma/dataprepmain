﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.AccessPlugin.Properties;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.AccessPlugin
{
    public class AccessSettingsViewModel : ViewModelBase
    {
        protected IPluginErrorReportingService errorReporter;

        public AccessSettings Settings { get; private set; }

        public AccessSettingsViewModel(
            PropertyBag bag, IPluginErrorReportingService errorReporter)
        {
            Settings = new AccessSettings(bag);
            this.errorReporter = errorReporter;
            PropertyBag columnsBag = bag.SubGroups["Columns"];
            PopulateColumns(columnsBag);
        }

        public AccessSettingsViewModel(
            string fileName, IPluginErrorReportingService errorReporter)
        {
            Settings = new AccessSettings(fileName);
            this.errorReporter = errorReporter;
            PropertyBag columnsBag =
                Settings.ToPropertyBag().SubGroups["Columns"];
            PopulateColumns(columnsBag);
        }

        /// <summary>
        /// Constructs the Query with the quoted table name.
        /// </summary>
        public string Query
        {
            get { return BuildQuery(); }
        }

        public string FileName
        {
            get { return Settings.FileName; }
            set
            {
                Settings.FileName = value;
                UpdateFileName();
                OnPropertyChanged("FileName");
            }
        }

        public string TableName
        {
            get { return Settings.TableName; }
            set
            {
                Settings.TableName = value;
                RefreshColumns();
                OnPropertyChanged("TableName");
            }
        }

        public ObservableCollection<string> TableNames
        {
            get
            {
                if (Settings.TableNames.Count == 0)
                {
                    ResetTableNames();
                }
                return Settings.TableNames;
            }
        }

        public ObservableCollection<AccessColumn> Columns
        {
            get
            {
                return Settings.Columns;
            }
        }

        /// <summary>
        /// Returns a connection string suitable for opening Access Db via
        /// ACE/JET provider.
        /// Constructs a connection string something like the following:
        /// Provider=Microsoft.ACE.OLEDB.12.0;User ID=Admin;
        /// Data Source=D:\Documents\Work\BeantownWithIdColumn.mdb;
        /// Mode=Share Deny None;
        /// Extended Properties="";
        /// Jet OLEDB:System database="";
        /// Jet OLEDB:Registry Path="";
        /// Jet OLEDB:Database Password="";
        /// Jet OLEDB:Engine Type=5;
        /// Jet OLEDB:Database Locking Mode=1;
        /// Jet OLEDB:Global Partial Bulk Ops=2;
        /// Jet OLEDB:Global Bulk Transactions=1;
        /// Jet OLEDB:New Database Password="";
        /// Jet OLEDB:Create System Database=False;
        /// Jet OLEDB:Encrypt Database=False;
        /// Jet OLEDB:Don't Copy Locale on Compact=False;
        /// Jet OLEDB:Compact Without Replica Repair=False;
        /// Jet OLEDB:SFP=False
        /// </summary>
        /// <returns>an OleDb connection string</returns>
        private string ConnectionString()
        {
            var connStr = new OleDbConnectionStringBuilder();
            try
            {
                connStr.Provider = AccessUtils.GetAccessProvider(Settings.FileName);
                connStr.DataSource = Settings.FileName;
                connStr.Add("User ID", "Admin");
                connStr.Add("Mode", "Share Deny None");
                return connStr.ConnectionString;
            }
            catch
            {
                Log.Error("Access:{0}", Resources.NoADO);
                throw new Exception(Resources.NoADO);
            }
        }

        /// <summary>
        /// Create a OleDbConnection
        /// </summary>
        /// <returns>OleDbConnection</returns>
        private OleDbConnection MakeConnection()
        {
            return new OleDbConnection(ConnectionString());
        }

        /// <summary>
        /// Returns selected columns OR all columns (if columns list is empty)
        /// </summary>
        /// <returns></returns>
        private string BuildQuery()
        {
            QueryBuilder builder = new QueryBuilder();
            if (Settings.Columns.Any())
            {
                foreach (string selectedColumn in Settings.SelectedColumns)
                {
                    builder.Select.AddColumn(selectedColumn);
                }
            }
            else
            {
               builder.Select.AddStar();
            }
            SchemaAndTable schemaAndTable = new SchemaAndTable(null,
                    Settings.TableName);
            builder.SchemaAndTable = schemaAndTable;
            return builder.ToString(SqlDialectFactory.AccessExcel);
        }

        /// <summary>
        /// Can be used to set FileName without triggering table load.
        /// </summary>
        /// <param name="correctedFileName"></param>
        public void FixFileName(string correctedFileName)
        {
            if (Settings.FileName != correctedFileName)
            {
                Settings.FileName = correctedFileName;
            }
        }

        /// <summary>
        /// Populate the 'Columns' Property from a 'PropertyBag'.
        /// </summary>
        /// <param name="columnsBag">PropertyBag full of column information.</param>
        private void PopulateColumns(PropertyBag columnsBag)
        {
            if (columnsBag == null ||
                columnsBag.SubGroups.Count == 0 ||
                columnsBag.SubGroups[0].Values.Count == 0)
            {
                return;
            }

            // load selected columns
            Settings.Columns.Clear();
            foreach (var bag in columnsBag.SubGroups)
            {
                Settings.Columns.Add(new AccessColumn(bag));
            }
        }

        /// <summary>
        /// Reset the 'TableNames' Property.
        /// </summary>
        private void ResetTableNames()
        {
            Settings.TableNames.Clear();

            // If the file is not an empty string.
            if (!string.IsNullOrEmpty(Settings.FileName))
            {
                try
                {
                    using (OleDbConnection connection = MakeConnection())
                    {
                        foreach (var tableName in AccessUtils
                            .DiscoverTableNames(connection))
                        {
                            Settings.TableNames.Add(tableName);
                        }
                    }

                    OnPropertyChanged("TableNames");
                }
                catch(Exception ex)
                {
                    this.errorReporter.Report(
                        Resources.ErrorTableRetrieval, ex.Message);
                }
            }
        }

        /// <summary>
        /// Refresh the 'Columns' Property.
        /// </summary>
        public void RefreshColumns()
        {
            Settings.Columns.Clear();
            var cols = GetColumns().Select(c => new AccessColumn(c));
            foreach (var col in cols)
            {
                Settings.Columns.Add(col);
            }
        }

        public void UpdateColumns()
        {
            if (Settings.Columns != null)
            {
                IEnumerable<AccessColumn> selectedColumns = Settings
                    .Columns.Where(c => c.Select).ToList();
                Settings.Columns.Clear();
                IEnumerable<Column> cols = GetColumns();
                foreach (Column column in cols)
                {
                    AccessColumn accessColumn = new AccessColumn(column);
                    Settings.Columns.Add(accessColumn);

                    if (selectedColumns
                        .Any(c => c.ColumnName == accessColumn.ColumnName))
                    {
                        accessColumn.Select = true;
                    }
                    else
                    {
                        accessColumn.Select = false;
                    }
                }
            }
        }

        /// <summary>
        /// Update the settings file name will cause a reload of all other settings.
        /// </summary>
        public void UpdateFileName()
        {
            //Reset the TableNames collection.
            ResetTableNames();
            // Get a table name if there is one to get.
            TableName = Settings.TableNames.FirstOrDefault() 
                ?? string.Empty;
        }

        /// <summary>
        /// Create a standalone table and load it with the access table.
        /// </summary>
        /// <param name="rowCount">Row count of returned table.</param>
        /// <returns>StandaloneTable</returns>
        public StandaloneTable BuildTable(int rowCount, 
            bool applyRowFilteration, IEnumerable<ParameterValue> parameters)
        {
            var table = new StandaloneTable();
            OleDbConnection connection = MakeConnection();
            IDataReader reader = connection.Open(string.Format(Query, Settings.TableName));
            try
            {
                table.BeginUpdate();

                // Load the schema
                var schema = reader.GetSchemaTable();
                // schema should be non-null.
                Debug.Assert(schema != null);
                
                foreach (DataRow row in schema.Rows)
                {
                    table.AddColumn(row.CreateColumnFromSchemaRow());
                }

                ParameterFilter parameterFilter = null;
                if (applyRowFilteration)
                {
                    parameterFilter = ParameterFilter.CreateFilter(table, parameters);
                }

                // if the initial rowcount is less than 1,
                // then we want all the rows.
                if (rowCount < 0)
                {
                    rowCount = int.MaxValue;
                }

                // load the table.
                while (reader.Read() && rowCount > 0)
                {
                    var rowdata = new object[table.ColumnCount];
                    for (int i = 0; i < table.ColumnCount; i++)
                    {
                        rowdata[i] = (reader.IsDBNull(i))
                            ? null
                            : rowdata[i] = reader.GetValue(i);
                    }

                    if (parameterFilter == null 
                        || parameterFilter.Contains(rowdata))
                    {
                        table.AddRow(rowdata);
                    }

                    rowCount--;
                }
            }
            finally
            {
                reader.Close();
                connection.Close();
                table.EndUpdate();
            }

            return table;
        }

        /// <summary>
        /// Get the columns within a table.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Column> GetColumns()
        {
            try
            {
                // TODO: Consider using GetSchema, or should use just
                // a schema query to fetch columns.
                using (OleDbConnection connection = MakeConnection())
                using (IDataReader reader = connection.Open(Query))
                {
                    var schema = reader.GetSchemaTable();
                    // schema should be non-null.
                    return schema != null
                        ? schema.Rows.OfType<DataRow>()
                                     .Select(r => r.CreateColumnFromSchemaRow())
                        : null;
                }
            }
            catch (Exception)
            {
                return Enumerable.Empty<Column>();
            }
        }
    }
}
