﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.AccessPlugin.Properties;
using Utils = Datawatch.DataPrep.Data.Core.Utils;

namespace Panopticon.AccessPlugin
{
    /// <summary>
    /// Utilities for Access Databases/Files.
    /// </summary>
    public static class AccessUtils
    {
        internal static readonly string Jet4 = "Microsoft.Jet.OLEDB.4.0";
        internal static readonly string Ace12 = "Microsoft.ACE.OLEDB.12.0";
        internal static readonly string Ace15 = "Microsoft.ACE.OLEDB.15.0";

        public static readonly string[] FileExtensions = 
            { "mdb", "mde", "accdb", "accde" };

        public static string FileFilter =
            Utils.CreateFilterString(Resources.UIFileFilter, FileExtensions);

        /// <summary>
        /// Get the installed oledb providers.
        /// </summary>
        /// <returns>An array of strings with provider names.</returns>
        internal static string[] GetAccessProviders()
        {
            List<string> providers = new List<string>();
            DataTable table = new OleDbEnumerator().GetElements();
            foreach (DataRow row in table.Rows)
            {
                string sourcesName = row["SOURCES_NAME"].ToString();
                if (!(sourcesName.Contains("ACE.OLEDB") || 
                    sourcesName.Contains("Jet.OLEDB"))) 
                {
                    continue;                    
                }
                providers.Add(sourcesName);
            }
            return providers.ToArray();
        }

        /// <summary>
        /// Get either ACE 12 or Jet4
        /// </summary>
        /// <param name="file">The file to get an oledb provider for.</param>
        /// <returns></returns>
        internal static string GetAccessProvider(string file)
        {
            var ext = Path.GetExtension(file);
            // We shouldn't get here with an empty extension.
            Debug.Assert(!string.IsNullOrEmpty(ext));

            var providers = GetAccessProviders();
            // if there is not a provider.
            Debug.Assert(providers.Any());

            // Jet is 32 bit
            if (ext.ToLower() == ".mdb" && !Environment.Is64BitProcess)
            {
                return Jet4;
            }
            // Return something other than Jet
            var aceProviders = providers.Where(p => p != Jet4).ToList();

            // Return the first ace provider in the list.
            return aceProviders.FirstOrDefault();
        }

        /// <summary>
        /// Take an oledb connection and create an IDataReader.
        /// </summary>
        /// <param name="connection">OleDbConnection to get</param>
        /// <param name="query"></param>
        /// <returns></returns>
        internal static IDataReader Open(
            this OleDbConnection connection,
            string query)
        {
            if (connection.State == ConnectionState.Closed)
            {
                connection.Open();
            }
            IDataReader reader;
            using (var cmd = connection.CreateCommand())
            {
                cmd.CommandText = String.Format(query);
                cmd.CommandTimeout = 60;
                reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);
            }
            return reader;
        }

        /// <summary>
        /// Constructs a list of table names for this source.
        /// </summary>
        /// <returns>a list of table names.</returns>
        public static IEnumerable<string> DiscoverTableNames(
            OleDbConnection connection)
        {
            var tables = new List<string>();
            using (connection)
            {
                connection.Open();
                try
                {
                    var tableSchema = connection.GetSchema("Tables");
                    foreach (DataRow row in tableSchema.Rows)
                    {
                        var tableType = row[3] as string; // TABLE_TYPE
                        var name = row[2] as string; // TABLE_NAME

                        if (tableType == "SYSTEM TABLE"
                            || tableType == "ACCESS TABLE")
                        {
                            continue;
                        }

                        tables.Add(name);
                    }
                }
                catch (Exception e)
                {
                    Log.Exception(e);
                }
                finally
                {
                    connection.Close();
                }
            }
            return tables;
        }

        /// <summary>
        /// Extract properties from a row from an OleDb schema table.
        /// </summary>
        /// <param name="schemaRow">schema table row</param>
        internal static Column CreateColumnFromSchemaRow(this DataRow schemaRow)
        {
            var columnName = schemaRow["ColumnName"].ToString();

            const string prefix = "System.";
            var dataTypeName = schemaRow["DataType"].ToString();
            if (dataTypeName.StartsWith(prefix))
            {
                dataTypeName = dataTypeName.Substring(prefix.Length);
            }

            switch (dataTypeName)
            {
                case "Int32":
                case "Int16":
                case "Int64":
                case "Byte":
                case "Boolean":
                case "Single":
                case "Double":
                case "Decimal":
                    return new NumericColumn(columnName);

                case "DateTime":
                    return new TimeColumn(columnName);

                case "String":
                    return new TextColumn(columnName);

                default:
                    // Any other types ("OLE object", etc.) are not supported.
                    // For now return text.
                    return new TextColumn(columnName);
            }
        }

        public static ColumnType GetColumnType(Column column)
        {
            if (column is NumericColumn)
            {
                return ColumnType.Numeric;
            }
            if (column is TimeColumn)
            {
                return ColumnType.Time;
            }
            return ColumnType.Text;
        }
    }
}
