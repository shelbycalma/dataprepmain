﻿using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AccessPlugin
{
    public class AccessColumn : PropertyBagViewModel
    {
        #region Properties
        public string ColumnName
        {
            get { return GetInternal("ColumnName"); }
            set { SetInternal("ColumnName", value); }
        }

        public ColumnType DataType
        {
            get { return GetInternalEnum("DataType", ColumnType.Text); }
            set { SetInternalEnum("DataType", value); }
        }

        public bool Select
        { 
            get { return GetInternalBool("Select", false); }
            set { SetInternalBool("Select", value); }
        }
        #endregion Properties

        #region Constructors
        public AccessColumn(PropertyBag bag) : base(bag) { }

        public AccessColumn(Column column)
            : this(new PropertyBag())
        {
            ColumnName = column.Name;
            DataType = AccessUtils.GetColumnType(column);
            Select = true;
        }
        #endregion Constructors

        #region Methods
        public PropertyBag GetPropertyBag()
        {
            return propertyBag;
        }
        #endregion Methods
    }
}
