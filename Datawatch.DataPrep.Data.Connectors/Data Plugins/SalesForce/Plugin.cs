﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.SalesForcePlugin.Enums;
using Panopticon.SalesForcePlugin.Properties;

namespace Panopticon.SalesForcePlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<SalesForceSettings>
    {
        internal const string PluginId = "SalesForceConnector";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Salesforce";
        internal const string PluginType = DataPluginTypes.Database;

        public override SalesForceSettings CreateSettings(PropertyBag bag)
        {
            return new SalesForceSettings(bag);
        }

        public override ITable GetData(
            string workbookDir, string dataDir, PropertyBag settingsBag,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingQuery);
            DateTime start = DateTime.Now;

            // make a client
            SalesForceSettings dataSettings = new SalesForceSettings(settingsBag);
            SalesForceAnalyticsClient client = new SalesForceAnalyticsClient();
            client.Authenticate(parameters, dataSettings.LoginServer, dataSettings.User,
                dataSettings.GetFullPassword(parameters));
            ITable result = null;
            if (dataSettings.ObjectType == ObjectType.Report)
            {
                result = client.GetReportDetails(dataSettings.ReportId, rowcount);
            }
            else if (dataSettings.ObjectType == ObjectType.Table)
            {
                result = client.GetTableDetails(dataSettings.TableName, dataSettings.ColumnNames, rowcount);
            }
            if (result != null)
            {
                // Done, log out how long it took and how much we got.
                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime,
                    result.RowCount, result.ColumnCount, seconds);
                return result;
            }
            throw Exceptions.UnableToLoadTable();
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}