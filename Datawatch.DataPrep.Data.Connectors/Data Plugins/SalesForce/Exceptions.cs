﻿using System;
using Panopticon.SalesForcePlugin.Properties;

namespace Panopticon.SalesForcePlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Core.Exceptions
    {
        public static Exception UnableToLoadTable()
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExUnableToLoadData, ""));
        }

        public static Exception UnableToConnectToServer(string message)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExUnableToConnectToServer, message));
        }

    }
}