﻿namespace Panopticon.SalesForcePlugin.Enums
{
    public enum FieldGroupingType
    {
        Across,
        Down
    }
}