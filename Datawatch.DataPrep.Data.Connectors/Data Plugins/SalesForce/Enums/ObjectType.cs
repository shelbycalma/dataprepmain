﻿namespace Panopticon.SalesForcePlugin.Enums
{
    public enum ObjectType
    {
        Unknown,
        Table,
        Report,
    }
}