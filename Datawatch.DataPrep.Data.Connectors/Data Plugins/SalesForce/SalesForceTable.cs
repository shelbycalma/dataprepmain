﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Panopticon.SalesForcePlugin.DataTypes;

namespace Panopticon.SalesForcePlugin
{
    internal class SalesForceTable
    {
        public class SalesForceColumn
        {
            public string Name { get; private set; }
            public string Label { get; private set; }
            public SalesForceDataType Type { get; private set; }
            public int ColumnIndex { get; private set; }

            public SalesForceColumn(
                string name, string label, SalesForceDataType type,
                int columnIndex)
            {
                Name = name;
                Label = label;
                Type = type;
                ColumnIndex = columnIndex;
            }
        }

        public class SalesForceDetailColumn : SalesForceColumn
        {
            public int DetailIndex { get; set; }

            public SalesForceDetailColumn(
                int detailIndex, string name, string label,
                SalesForceDataType type, int columnIndex)
                : base(name, label, type, columnIndex)
            {
                DetailIndex = detailIndex;
            }
        }

        public StandaloneTable Table { get; private set; }

        public int ColumnCount
        {
            get { return Table.ColumnCount; }
        }

        public List<string> ColumnNames
        {
            get { return new List<string>(DetailColumns.Keys); }
        }

        public Dictionary<int, SalesForceColumn> AcrossGroupingColumns { get;
            set; }

        public Dictionary<int, SalesForceColumn> DownGroupingColumns { get; set;
        }

        public Dictionary<string, SalesForceDetailColumn> DetailColumns { get;
            set; }

        public SalesForceTable(StandaloneTable table)
        {
            Table = table;
            AcrossGroupingColumns = new Dictionary<int, SalesForceColumn>();
            DownGroupingColumns = new Dictionary<int, SalesForceColumn>();
            DetailColumns = new Dictionary<string, SalesForceDetailColumn>();
        }

        public bool GetDetailColumnInfo(
            string name, out SalesForceColumn colInfo)
        {
            if (DetailColumns.ContainsKey(name))
            {
                colInfo = DetailColumns[name];
                return true;
            }
            colInfo = null;
            return false;
        }

        public void AddDetailColumn(string name, string label, string type)
        {
            SalesForceDataType sfType = SalesForceDataType.GetDataTypeFromString(type);
            string uniqueLabel = GetUniqueColumnLabel(label);

            DetailColumns.Add(name,
                new SalesForceDetailColumn(DetailColumns.Count, name, uniqueLabel,
                    sfType, ColumnCount));
            Table.AddColumn(sfType.MakeDesignerColumn(uniqueLabel));
        }

        private string GetUniqueColumnLabel(string label)
        {
            string uniqueLabel = label;
            int counter = 2;
            while (Table.ContainsColumn(uniqueLabel))
            {
                uniqueLabel = String.Format("{0}({1})", label, counter);
                counter++;
            }
            return uniqueLabel;
        }

        public void AddGroupingColumn(
            string name, string label, string type,
            Enums.FieldGroupingType groupingType, int level)
        {
            SalesForceDataType sfType = SalesForceDataType.GetDataTypeFromString(type);
            string uniqueLabel = GetUniqueColumnLabel(label);

            SalesForceColumn newColumn = new SalesForceColumn(name, uniqueLabel, sfType,
                ColumnCount);
            switch (groupingType)
            {
                case Enums.FieldGroupingType.Across:
                    AcrossGroupingColumns.Add(level, newColumn);
                    break;
                case Enums.FieldGroupingType.Down:
                    DownGroupingColumns.Add(level, newColumn);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("groupingType");
            }
            Table.AddColumn(sfType.MakeDesignerColumn(uniqueLabel));
        }
    }
}