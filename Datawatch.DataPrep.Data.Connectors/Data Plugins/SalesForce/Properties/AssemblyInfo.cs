﻿using System.Reflection;
using System.Runtime.InteropServices;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.SalesForcePlugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("SalesForceConnector")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("bfbb7ecd-fd65-4d28-bd24-71106f0477b4")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]