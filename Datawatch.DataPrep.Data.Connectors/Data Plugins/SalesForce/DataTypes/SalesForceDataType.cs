﻿using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal abstract class SalesForceDataType
    {
        public abstract Column MakeDesignerColumn(string name);
        public abstract object ReadFromToken(JToken valueToken);
        public abstract object ConvertFromJValue(JValue jvalue);

        internal static SalesForceDataType GetDataTypeFromString(string dataType)
        {
            switch (dataType)
            {
                case "url":
                    return new SalesForceUrl();
                case "date":
                case "datetime":
                    return new SalesForceDateTime();
                case "currency":
                    return new SalesForceCurrency();
                case "int":
                    return new SalesForceInt();
                case "double":
                    return new SalesForceDouble();
                case "percent":
                    return new SalesForcePercent();
                default:
                    return new SalesForceString();
            }
        }
    }
}