using System;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal class SalesForcePercent : SalesForceDouble
    {
        public override object ReadFromToken(JToken valueToken)
        {
            try
            {
                JValue jvalue = valueToken.SelectToken("value") as JValue;
                return jvalue != null
                    ? Convert.ToDouble(jvalue.Value)
                    : new double();
            }
            catch (Exception)
            {
                return new double();
            }
        }
    }
}