using System;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal class SalesForceString : SalesForceDataType
    {
        public override Column MakeDesignerColumn(string name)
        {
            return new TextColumn(name);
        }

        public override object ReadFromToken(JToken valueToken)
        {
            JValue value = valueToken.SelectToken("value") as JValue;
            if (value != null && value.Value == null)
            {
                return null;
            }
            JValue labelJValue = valueToken.SelectToken("label") as JValue;
            return labelJValue != null ? labelJValue.Value as String : null;
        }

        public override object ConvertFromJValue(JValue jvalue)
        {
            if (jvalue == null || jvalue.Value == null)
            {
                return null;
            }
            return jvalue.Value.ToString();
        }
    }
}