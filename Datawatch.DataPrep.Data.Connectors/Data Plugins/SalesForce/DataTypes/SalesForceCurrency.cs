﻿using System;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal class SalesForceCurrency : SalesForceDataType
    {
        public override Column MakeDesignerColumn(string name)
        {
            return new NumericColumn(name);
        }

        public override object ReadFromToken(JToken valueToken)
        {
            try
            {
                JValue jvalue = valueToken.SelectToken("value.amount") as JValue;
                return jvalue != null
                    ? Convert.ToDouble(jvalue.Value)
                    : new double();
            }
            catch (Exception)
            {
                return new double();
            }
        }

        public override object ConvertFromJValue(JValue jvalue)
        {
            return jvalue != null ? Convert.ToDouble(jvalue.Value) : new double();
        }
    }
}