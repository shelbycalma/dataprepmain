using System;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal class SalesForceUrl : SalesForceString
    {
        public override object ReadFromToken(JToken valueToken)
        {
            JValue jvalue = valueToken.SelectToken("value") as JValue;
            return jvalue != null ? jvalue.Value as String : null;
        }
    }
}