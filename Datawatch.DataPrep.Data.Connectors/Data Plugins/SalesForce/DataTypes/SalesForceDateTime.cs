using System;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal class SalesForceDateTime : SalesForceDataType
    {
        public override Column MakeDesignerColumn(string name)
        {
            return new TimeColumn(name);
        }

        public override object ReadFromToken(JToken valueToken)
        {
            JValue jvalue = valueToken.SelectToken("value") as JValue;
            if (jvalue != null)
            {
                if (jvalue.Value is DateTime)
                {
                    return jvalue.Value;
                }
                return jvalue.Value as String;
            }
            return null;
        }

        public override object ConvertFromJValue(JValue jvalue)
        {
            if (jvalue == null || jvalue.Value == null)
            {
                return null;
            }
            if (jvalue.Value is DateTime)
            {
                return jvalue.Value;
            }
            return jvalue.Value.ToString();
        }
    }
}