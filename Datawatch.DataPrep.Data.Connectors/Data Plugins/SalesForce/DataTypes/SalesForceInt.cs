﻿using System;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Newtonsoft.Json.Linq;

namespace Panopticon.SalesForcePlugin.DataTypes
{
    internal class SalesForceInt : SalesForceDataType
    {
        public override Column MakeDesignerColumn(string name)
        {
            NumericColumn col = new NumericColumn(name);
            return col;
        }

        public override object ReadFromToken(JToken valueToken)
        {
            try
            {
                JValue jvalue = valueToken.SelectToken("value") as JValue;
                return jvalue != null ? Convert.ToInt32(jvalue.Value) : new int();
            }
            catch (Exception)
            {
                return new int();
            }
        }

        public override object ConvertFromJValue(JValue jvalue)
        {
            return jvalue != null ? Convert.ToDouble(jvalue.Value) : new double();
        }
    }
}