﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.SalesForcePlugin.Enums;
using Panopticon.SalesForcePlugin.Properties;

namespace Panopticon.SalesForcePlugin
{
    public class SalesForceSettings : ConnectionSettings
    {
		private readonly ObservableCollection<DatabaseColumn> selectedColumns =
				new ObservableCollection<DatabaseColumn>();

		public SalesForceSettings(PropertyBag bag)
            : base(bag)
        {
        }

        public void RestoreDefaults()
        {
        }


        public ObjectType ObjectType
        {
            get
            {
                string s = GetInternal("ObjectType",
                    Enum.GetName(typeof (ObjectType), ObjectType.Unknown));
                return (ObjectType) Enum.Parse(typeof (ObjectType), s, true);
            }
            set
            {
                SetInternal("ObjectType",
                    Enum.GetName(typeof (ObjectType), value));
                SetTitle();
            }
        }

        public string ReportName
        {
            get { return GetInternal("ReportName"); }
            set
            {
                SetInternal("ReportName", value);
                SetTitle();
            }
        }

        public string TableName
        {
            get { return GetInternal("TableName"); }
            set
            {
                SetInternal("TableName", value);
                SetTitle();
            }
        }

		public ObservableCollection<DatabaseColumn> SelectedColumns
		{
			get { return selectedColumns; }
		}

		public string ColumnNames
		{
			get
			{
				return GetInternal("ColumnNames");
			}
			set
			{
				SetInternal("ColumnNames", value);
			}
		}

		public string ReportId
        {
            get { return GetInternal("ReportId"); }
            set
            {
                SetInternal("ReportId", value);
                SetTitle();
            }
        }

        public string ReportFolderId
        {
            get { return GetInternal("ReportFolderId"); }
            set
            {
                SetInternal("ReportFolderId", value);
                SetTitle();
            }
        }

        public bool ReportCsvExport
        {
            get { return Convert.ToBoolean(GetInternal("ReportCsvExport")); }
            set { SetInternal("ReportCsvExport", value.ToString()); }
        }

        public string ReportCsvExportFileName
        {
            get { return GetInternal("ReportCsvExportFileName"); }
            set { SetInternal("ReportCsvExportFileName", value); }
        }

        public string LoginServer
        {
            get { return GetInternal("LoginServer"); }
            set { SetInternal("LoginServer", value); }
        }

        public string User
        {
            get { return GetInternal("User"); }
            set { SetInternal("User", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string SecurityToken
        {
            get { return GetInternal("SecurityToken"); }
            set { SetInternal("SecurityToken", value); }
        }

        public string SalesForceClientId
        {
            get { return GetInternal("SalesForceClientId"); }
            set { SetInternal("SalesForceClientId", value); }
        }

        public string SalesForceClientSecret
        {
            get { return GetInternal("SalesForceClientSecret"); }
            set { SetInternal("SalesForceClientSecret", value); }
        }

        public string GetFullPassword(IEnumerable<ParameterValue> parameters)
        {
            string parameterizedPassword = DataUtils.ApplyParameters(Password,
                parameters);
            if (!string.IsNullOrEmpty(SecurityToken))
            {
                string parameterizedSecurityToken = DataUtils.ApplyParameters(SecurityToken,
                    parameters);
                return parameterizedPassword + parameterizedSecurityToken;
            }
            return parameterizedPassword;
        }

        private void SetTitle()
        {
            if (ObjectType == ObjectType.Report)
            {
                Title = (ReportName != null) ? ReportName : Resources.UiTitlebarReport;
            }
            else if (ObjectType == ObjectType.Table)
            {
                Title = (TableName != null) ? TableName : Resources.UiTable;
            }
        }
    }
}