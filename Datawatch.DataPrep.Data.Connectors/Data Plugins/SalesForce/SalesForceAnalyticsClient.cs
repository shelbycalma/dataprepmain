﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using Panopticon.SalesForcePlugin.Enums;
using Panopticon.SalesForcePlugin.Properties;
using Panopticon.SalesForcePlugin.SalesForceWebService;
using SalesforceSharp;
using System.Net;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using System.Linq;

namespace Panopticon.SalesForcePlugin
{
    public class SalesForceAnalyticsClient
    {
        private SforceService SoapClient { get; set; }
        public string CurrentUid { get; private set; }
        public string SoapApiVersion { get; private set; }

        // SalesforceSharp
        private readonly SalesforceClient _client;

        public SalesForceAnalyticsClient()
        {
            _client = new SalesforceClient();
            SoapApiVersion = "30.0";
            ApiVersion = "v30.0";
        }

        public string ApiVersion
        {
            get { return _client.ApiVersion; }
            set { _client.ApiVersion = value; }
        }

        public bool IsAuthenticated
        {
            get { return _client.IsAuthenticated; }
        }

        public string InstanceUrl
        {
            get { return _client.InstanceUrl; }
        }

        public IList<T> Query<T>(string q) where T : new()
        {
            Log.Info(Resources.ExQueryVsServer, q);
            return _client.Query<T>(q);
        }

        /// <summary>
        /// Request() replaces the protected method call from when the class was being inherited from SalesForceClient.
        /// The issue of dynamically loading dependent assemblies prevents us from doing this inheritance.
        /// </summary>
        /// <typeparam name="T">The type of object we are requesting</typeparam>
        /// <param name="query">The SOQL query that we want to run</param>
        /// <returns>A response with the records from the query</returns>
        public IRestResponse<T> Request<T>(string query)
        {
            MethodInfo requestMethod = typeof (SalesforceClient).GetMethod("Request",
                BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo genericRequestMethod = requestMethod.MakeGenericMethod(typeof (T));
            return
                genericRequestMethod.Invoke(_client,
                    new object[] {query, null, null, Method.GET}) as
                    IRestResponse<T>;
        }

        /// <summary>
        /// Intended to circumvent the standard Authenticate method because it is insufficient for our needs.
        /// Implant the authentication results from the SOAP login() into the SalesforceClient.
        /// </summary>
        /// <param name="accessToken">The SOAP session ID, can also be used as a REST access token</param>
        /// <param name="instanceUrl">The SFDC instance that has been assigned to the client during the login</param>
        private void SetClientAccessInfo(string accessToken, string instanceUrl)
        {
            try
            {
                FieldInfo tokenField =
                    typeof (SalesforceClient).GetField("m_accessToken",
                        BindingFlags.NonPublic | BindingFlags.Instance);
                PropertyInfo isAuthenticatedProperty =
                    typeof (SalesforceClient).GetProperty("IsAuthenticated",
                        BindingFlags.Public | BindingFlags.Instance);
                PropertyInfo instanceUriProperty =
                    typeof (SalesforceClient).GetProperty("InstanceUrl",
                        BindingFlags.Public | BindingFlags.Instance);

                if (tokenField != null && isAuthenticatedProperty != null &&
                    instanceUriProperty != null)
                {
                    tokenField.SetValue(_client, accessToken);
                    isAuthenticatedProperty.SetValue(_client, true, null);
                    instanceUriProperty.SetValue(_client, instanceUrl, null);
                }
                else
                {
                    throw Exceptions.UnableToConnectToServer(
                        Resources.ExFailedToSetAuthInfo);
                }
            }
            catch (Exception)
            {
                throw Exceptions.UnableToConnectToServer(
                    Resources.ExFailedToSetAuthInfo);
            }
        }

        public string ReadMetaData(string name)
        {
            return _client.ReadMetaData(name);
        }

        /// <summary>
        /// Authenticate using the SOAP API, since the vanilla SalesForceSharp uses the REST API, and the authentication requires an OAuth client ID and client secret.
        /// The session ID that gets returned can be used as an access token for the rest API. This replaces the Authenticate() method in SalesforceClient.
        /// </summary>
        /// <param name="loginUrl"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void Authenticate(IEnumerable<ParameterValue> parameters, 
            string loginUrl, string userName, string password)
        {
            string parameterizedLoginUrl = DataUtils.ApplyParameters(loginUrl,
                parameters);
            string parameterizedUserName = DataUtils.ApplyParameters(userName,
                parameters);
            string url = String.Format("https://{0}/services/Soap/u/{1}", parameterizedLoginUrl,
                SoapApiVersion);
            SoapClient = new SforceService
            {
                Url = url
            };
            LoginResult response;


            //Remove setting the SecurityProtocol  in a specific Connector, as it is better set in DPS or 
            //in an application level rather than in a library.
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 |
            //    SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            try
            {
                Log.Info(Resources.ExConnectingData, url, parameterizedUserName);
                response = SoapClient.login(parameterizedUserName, password);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            if (response != null &&
                !String.IsNullOrEmpty(response.sessionId) &&
                !String.IsNullOrEmpty(response.serverUrl))
            {
                Uri metadataUrl = new Uri(response.serverUrl);
                string instanceUri = String.Format("https://{0}", metadataUrl.Host);

                SetClientAccessInfo(response.sessionId, instanceUri);

                SoapClient.Url = response.serverUrl;
                SessionHeader sessionHeader = new SessionHeader
                {
                    sessionId = response.sessionId
                };
                SoapClient.SessionHeaderValue = sessionHeader;

                CurrentUid = response.userId;
            }
            else
            {
                throw Exceptions.UnableToConnectToServer(String.Empty);
            }
        }

        public T? GetTokenValue<T>(JToken container, string jPath)
            where T : struct
        {
            JValue resultValueToken = container.SelectToken(jPath) as JValue;
            if (resultValueToken != null)
            {
                T? value = resultValueToken.Value as T?;
                return value;
            }
            return null;
        }

        public T GetObjectTokenValue<T>(JToken container, string jPath)
            where T : class
        {
            JValue resultValueToken = container.SelectToken(jPath) as JValue;
            if (resultValueToken != null)
            {
                T value = resultValueToken.Value as T;
                return value;
            }
            return null;
        }

        public StandaloneTable GetReportDetails(string reportId, int rowCount)
        {
            StandaloneTable dt = new StandaloneTable();
            SalesForceTable sfTable = new SalesForceTable(dt);
            string url =
                String.Format(
                    "{0}/services/data/{1}/analytics/reports/{2}?includeDetails=true",
                    InstanceUrl, ApiVersion, reportId);

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 |
                SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            IRestResponse<dynamic> response = Request<dynamic>(url);
            TextReader textReader = new StringReader(response.Content);
            sfTable.Table.BeginUpdate();
            try
            {
                using (JsonReader jsonReader = new JsonTextReader(textReader))
                {
                    JToken data = JToken.Load(jsonReader);

                    bool hasAllData = GetTokenValue<bool>(data, "allData") ??
                                     false;
                    if (!hasAllData && rowCount >= 2000)
                    {
                        //throw new InvalidOperationException(
                        //    Resources.UiErrorRowLimit);
                        MessageBox.Show(Resources.UiErrorRowLimit,
                                                        Properties.Resources.UiTitlebarReport,
                                                        MessageBoxButton.OK, MessageBoxImage.Information);
                    }

                    AddGroupingColumns(sfTable, data);

                    AddDetailColumns(sfTable, data);

                    JToken factMap = data.SelectToken("factMap");
                    if (factMap == null)
                    {
                        return sfTable.Table;
                    }

                    List<Grouping> rowGroupingKeys = GetRowGroupingKeys(data);
                    List<Grouping> colGroupingKeys = GetColumnGroupingKeys(data);

                    foreach (Grouping rowGrouping in rowGroupingKeys)
                    {
                        foreach (Grouping colGrouping in colGroupingKeys)
                        {
                            AddRowsFromGrouping(sfTable, factMap, rowGrouping,
                                colGrouping, rowCount);
                        }
                    }
                }
            }
            finally
            {
                sfTable.Table.EndUpdate();
            }
            return sfTable.Table;
        }

        private int AddDetailColumns(SalesForceTable table, JToken reportData)
        {
            JContainer detailColumnInfo =
                (JContainer)
                    reportData.SelectToken(
                        "reportExtendedMetadata.detailColumnInfo");
            if (detailColumnInfo == null)
            {
                return 0;
            }

            int columnsAdded = 0;
            foreach (JToken obj in detailColumnInfo)
            {
                JToken propValue = ((JProperty) obj).Value;
                string label = (string) propValue.SelectToken("label");
                string dataType = (string) propValue.SelectToken("dataType");
                table.AddDetailColumn(((JProperty) obj).Name, label, dataType);
                columnsAdded++;
            }
            return columnsAdded;
        }

        private void AddGroupingColumns(
            FieldGroupingType type, SalesForceTable table, JToken reportData)
        {
            JToken extendedGroupingColumnInfo =
                reportData.SelectToken(
                    "reportExtendedMetadata.groupingColumnInfo");
            JArray groupingColumns = null;
            if (type == FieldGroupingType.Across)
            {
                groupingColumns =
                    reportData.SelectToken("reportMetadata.groupingsAcross") as
                        JArray;
            }
            else if (type == FieldGroupingType.Down)
            {
                groupingColumns =
                    reportData.SelectToken("reportMetadata.groupingsDown") as
                        JArray;
            }
            if (groupingColumns != null)
            {
                foreach (JToken groupingColumn in groupingColumns)
                {
                    string name;
                    string dt;
                    int groupLevel;
                    string label;
                    try
                    {
                        JValue nameToken =
                            groupingColumn.SelectToken("name") as JValue;
                        if (nameToken == null || !(nameToken.Value is string))
                        {
                            continue;
                        }

                        name = nameToken.Value as string;
                        JObject detail =
                            extendedGroupingColumnInfo.SelectToken(name) as
                                JObject;

                        if (detail == null)
                        {
                            continue;
                        }

                        dt = detail["dataType"].Value<string>();
                        groupLevel = detail["groupingLevel"].Value<int>();
                        label = detail["label"].Value<string>();
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                    table.AddGroupingColumn(name, label, dt, type, groupLevel);
                }
            }
        }

        private void AddGroupingColumns(SalesForceTable table, JToken reportData)
        {
            AddGroupingColumns(FieldGroupingType.Across, table, reportData);
            AddGroupingColumns(FieldGroupingType.Down, table, reportData);
        }

        private void AddRowsFromGrouping(
            SalesForceTable sfTable, JToken factMapJson, Grouping downGrouping,
            Grouping acrossGrouping, int rowCount)
        {
            JArray rows =
                factMapJson.SelectToken(String.Format("{0}!{1}.rows",
                    downGrouping.Key, acrossGrouping.Key)) as JArray;
            if (rows == null)
            {
                return;
            }
            int rowsAdded = 0;
            foreach (JToken row in rows)
            {
                if (rowCount > -1 && rowsAdded >= rowCount)
                {
                    return;
                }
                Row dtRow = sfTable.Table.AddRow();

                if (acrossGrouping.GroupingToken != null)
                {
                    Grouping currentGrouping = acrossGrouping;
                    while (currentGrouping != null)
                    {
                        SalesForceTable.SalesForceColumn acrossGroupingCol =
                            sfTable.AcrossGroupingColumns[
                                currentGrouping.GroupingLevel];
                        Column col =
                            sfTable.Table.GetColumn(acrossGroupingCol.ColumnIndex);

                        if (col != null)
                        {
                            col.SetValue(dtRow,
                                acrossGroupingCol.Type.ReadFromToken(
                                    currentGrouping.GroupingToken));
                        }

                        currentGrouping = currentGrouping.Parent;
                    }
                }
                if (downGrouping.GroupingToken != null)
                {
                    Grouping currentGrouping = downGrouping;
                    while (currentGrouping != null)
                    {
                        SalesForceTable.SalesForceColumn downGroupingCol =
                            sfTable.DownGroupingColumns[
                                currentGrouping.GroupingLevel];
                        Column col =
                            sfTable.Table.GetColumn(downGroupingCol.ColumnIndex);
                        if (col != null)
                        {
                            col.SetValue(dtRow,
                                downGroupingCol.Type.ReadFromToken(
                                    currentGrouping.GroupingToken));
                        }
                        currentGrouping = currentGrouping.Parent;
                    }
                }
                JArray cellData = (JArray) row.SelectToken("dataCells");
                foreach (KeyValuePair<string, SalesForceTable.SalesForceDetailColumn> column in sfTable.DetailColumns)
                {
                    int index = column.Value.ColumnIndex;
                    int detailIndex = column.Value.DetailIndex;
                    Column col = sfTable.Table.GetColumn(index);

                    if (col != null)
                    {
                        col.SetValue(dtRow,
                            column.Value.Type.ReadFromToken(cellData[detailIndex]));
                    }
                }
                rowsAdded++;
            }
        }

        public List<KeyValuePair<string, string>> GetQueryableTables()
        {
            List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
            DescribeGlobalResult tableList = SoapClient.describeGlobal();
            foreach (DescribeGlobalSObjectResult sobject in tableList.sobjects)
            {
                if (sobject.queryable)
                {
                    result.Add(new KeyValuePair<string, string>(sobject.name,
                        sobject.label));
                }
            }
            return result;
        }

        private SalesForceTable MakeSalesforceSObjectTable(
            string sobjectName, string columns, StandaloneTable table)
        {
            SalesForceTable sfTable = new SalesForceTable(table);
            List<string> collist = null;
            if (columns != null)
            {
                collist = columns.Split(',').ToList();
            }
            sfTable.Table.BeginUpdate();
            string metadata = ReadMetaData(sobjectName);
            TextReader textReader = new StringReader(metadata);
            using (JsonReader jsonReader = new JsonTextReader(textReader))
            {
                JToken data = JToken.Load(jsonReader);
                JArray fieldListToken = data.SelectToken("fields") as JArray;
                if (fieldListToken == null)
                {
                    return sfTable;
                }
                foreach (JToken fieldToken in fieldListToken)
                {
                    string name = fieldToken.SelectToken("name").Value<string>();
                    string type = fieldToken.SelectToken("type").Value<string>();
                    string label = fieldToken.SelectToken("label").Value<string>();
                    if (collist != null)
                    {
                        if (collist.Contains(name))
                        {
                            sfTable.AddDetailColumn(name, label, type);
                        }
                    }
                    else
                    {
                        sfTable.AddDetailColumn(name, label, type);
                    }
                }
            }

            Log.Info("FieldCount:" + sobjectName + ":" + sfTable.ColumnCount.ToString(), sobjectName + ":" + sfTable.ColumnCount.ToString());
            return sfTable;
        }

        private string BuildColumnListFromTable(SalesForceTable table)
        {
            StringBuilder sb = new StringBuilder();
            List<string> columnNames = table.ColumnNames;
            int count = 0;
            foreach (string name in columnNames)
            {
                if (count != 0)
                {
                    sb.Append(",");
                }
                sb.Append(name);
                count++;
            }
            return sb.ToString();
        }

        public string GetTableColumns(string tableName)
        {
            StandaloneTable dt = new StandaloneTable();
            SalesForceTable sfTable = MakeSalesforceSObjectTable(tableName, null, dt);
            
            string columnNames = BuildColumnListFromTable(sfTable);
            StringBuilder sb = new StringBuilder();
            string[] col = columnNames.Split(',');
            int count = 0;
            foreach (string s in col)
            {
                SalesForceTable.SalesForceColumn colInfo;

                if (sfTable.GetDetailColumnInfo(s, out colInfo))
                {
                    Log.Info("ColumnDetail (Label, Name):" + colInfo.Label + "~~~~~" + colInfo.Name, "");
                    if (count != 0)
                    {
                        //sb.Append(",");
                        sb.Append("~");
                    }
                    sb.Append(s + '^' + colInfo.Label);
                    count++;
                }
            }
            Log.Info("Columns Selected: " + sb.ToString(), "");
            return sb.ToString();
        }

        public StandaloneTable GetTableDetails(string tableName, string columns, int rowCount)
        {
            StandaloneTable dt = new StandaloneTable();
            SalesForceTable sfTable = MakeSalesforceSObjectTable(tableName, columns, dt);

            string query = String.Format("SELECT {0} FROM {1}", (columns != null ? columns : BuildColumnListFromTable(sfTable)), tableName);
            if (rowCount != -1)
            {
                query = string.Format("{0} limit {1}", query, rowCount);
            }

            IList<dynamic> response = Query<dynamic>(query);
            foreach (dynamic row in response)
            {
                JObject rowObject = row as JObject;
                if (rowObject == null)
                {
                    continue;
                }
                Row tblRow = sfTable.Table.AddRow();
                foreach (KeyValuePair<string, JToken> prop in rowObject)
                {
                    string columnName = prop.Key;
                    SalesForceTable.SalesForceColumn colInfo;
                    if (sfTable.GetDetailColumnInfo(columnName, out colInfo))
                    {
                        Column col = sfTable.Table.GetColumn(colInfo.Label);

                        if (col != null)
                        {
                            col.SetValue(tblRow,
                                colInfo.Type.ConvertFromJValue(
                                    prop.Value as JValue));
                        }
                    }
                }
            }
            sfTable.Table.EndUpdate();
            return sfTable.Table;
        }

        private List<Grouping> GetRowGroupingKeys(JToken data)
        {
            JToken rowGroupingsToken = data.SelectToken("groupingsDown");
            return GetGroupingKeys(rowGroupingsToken, FieldGroupingType.Down);
        }

        private List<Grouping> GetColumnGroupingKeys(JToken data)
        {
            JToken columnGroupingsToken = data.SelectToken("groupingsAcross");
            return GetGroupingKeys(columnGroupingsToken, FieldGroupingType.Across);
        }

        private List<Grouping> GetGroupingKeys(
            JToken groupingToken, FieldGroupingType groupingType,
            List<Grouping> results = null, int level = -1, Grouping parent = null)
        {
            if (results == null)
            {
                results = new List<Grouping>
                {
                    new Grouping("T", null, groupingType, 0, null)
                };
            }
            if (groupingToken == null)
            {
                return results;
            }

            JValue groupingKey = groupingToken.SelectToken("key") as JValue;
            Grouping newGrouping = null;
            if (groupingKey != null && groupingKey.Value is String)
            {
                newGrouping = new Grouping(groupingKey.Value as String,
                    groupingToken, groupingType, level, parent);
                results.Add(newGrouping);
            }

            JArray groupingsArray = groupingToken.SelectToken("groupings") as JArray;
            if (groupingsArray != null)
            {
                foreach (JToken group in groupingsArray)
                {
                    GetGroupingKeys(group, groupingType, results, level + 1,
                        newGrouping);
                }
            }
            return results;
        }

        private class Grouping
        {
            public string Key { get; private set; }
            public JToken GroupingToken { get; private set; }
            public int GroupingLevel { get; private set; }
            public FieldGroupingType GroupingType { get; private set; }
            public Grouping Parent { get; private set; }

            public Grouping(
                string key, JToken groupingToken, FieldGroupingType groupingType,
                int level, Grouping parent)
            {
                Key = key;
                GroupingToken = groupingToken;
                GroupingLevel = level;
                GroupingType = groupingType;
                Parent = parent;
            }
        }
    }
}