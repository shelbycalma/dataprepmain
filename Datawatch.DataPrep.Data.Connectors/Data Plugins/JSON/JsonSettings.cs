﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Newtonsoft.Json.Linq;

namespace Panopticon.JsonPlugin
{
    public class JsonSettings : TextSettingsBase
    {
        private const string Json = "Json";

        public JsonSettings(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public JsonSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        public override PathType FilePathType
        {
            get
            {
                string s = GetInternal("JsonFilePathType");
                if (s != null)
                {
                    try
                    {
                        return (PathType)Enum.Parse(
                            typeof(PathType), s);
                    }
                    catch (Exception)
                    {
                    }
                }
                return PathType.File;
            }
            set
            {
                SetInternal("JsonFilePathType", value.ToString());
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return Json; }
        }

        public string RecordsPath
        {
            get
            {
                return GetInternal("RecordsJsonPath");

            }
            set
            {
                SetInternal("RecordsJsonPath", value);
            }
        }

        /// <summary>
        /// Gets a value indicating wether the current JsonSettings 
        /// instance can be okeyed.
        /// </summary>
        public bool IsOK
        {
            get
            {
                if (!IsParserSettingsOk)
                {
                    return false;
                }
                return IsPathOk();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is JsonSettings)) return false;

            if (!string.Equals(RecordsPath, ((JsonSettings)obj).RecordsPath))
            {
                return false;
            }

            if (!base.Equals(obj)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            hashCode ^= RecordsPath.GetHashCode();

            return hashCode;
        }

        public override bool CanGenerateColumns()
        {
            return IsPathOk();
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        public override bool ShowFilter
        {
            get
            {
                return false;
            }
        }

        public override void DoGenerateColumns()
        {
            try
            {
                var parser = (JsonParser)parserSettings.CreateParser();

                using (StreamReader streamReader =
                    DataPluginUtils.GetStream(this, Parameters, this.FilePath))
                {
                    JToken jRows = parser.FindRecords(streamReader,
                        RecordsPath);
                    if (jRows == null)
                    {
                        throw new Exception(string.Format(
                            Properties.Resources.UiColumnGenerationWrongRecordPath, RecordsPath));
                    }

                    ColumnGenerator cg = new ColumnGenerator(ParserSettings, NumericDataHelper);

                    if (jRows is JObject && jRows.First != null)
                    {
                        // The record path is not pointing to an array but instead an object that 
                        // contains a comma separated list of value:object
                        // Add main column with just path key
                        JsonColumnDefinition cd =
                            (JsonColumnDefinition)parserSettings.CreateColumnDefinition(new PropertyBag());
                        cd.Name = "KeyColumn";
                        cd.JsonPath = JsonParser.Key;
                        parserSettings.AddColumnDefinition(cd);
                    }
                    int count = 0;
                    foreach (JToken t in jRows)
                    {
                        if (count >= 10)
                            break;

                        if (!t.HasValues) continue;

                        cg.Generate(t.ToString());

                        count++;
                    }
                }
            }
            catch (Exception e)
            {
                if (e.GetType() == typeof(WebException))
                {
                    Log.Error(Properties.Resources.ExQueryTimeout);
                }
                else
                {
                    Log.Error(Properties.Resources.ExFailedColumnGeneration);
                }

                this.errorReporter.Report(
                    Properties.Resources.ExError,
                    Properties.Resources.ExFailedColumnGeneration + "\n" +
                                    "Exception: " + e.Message);

                Log.Exception(e);
            }
        }

    }
}
