﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Newtonsoft.Json.Linq;
using System;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.JsonPlugin.Properties;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.JsonPlugin
{
    /// <summary>
    /// DataPlugin for JSON.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : TextPluginBase<JsonSettings>
    {
        internal const string PluginId = "JsonPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "JSON";
        internal const string PluginType = DataPluginTypes.File;

        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override ITable GetData(string workbookDir, string dataDir, 
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            JsonSettings settings = 
                new JsonSettings(pluginManager, properties, parameters);

            //Handle File Path
            string fixedFilePath = null;
            if (settings.FilePathType == PathType.File)
            {
                fixedFilePath = DataUtils.FixFilePath(workbookDir, dataDir,
                    settings.FilePath, parameters);
                if (!settings.IsFilePathParameterized)
                {
                    settings.FilePath = fixedFilePath;
                }
            }

            JsonParser parser = 
                (JsonParser)settings.ParserSettings.CreateParser();

            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();
            try
            {
                DataPluginUtils.AddColumns(table, settings.ParserSettings.Columns);

                DateTime start = DateTime.Now;

                using (StreamReader streamReader =
                    DataPluginUtils.GetStream(settings, parameters, fixedFilePath))
                {
                    JToken jRows = parser.FindRecords(streamReader, 
                        settings.RecordsPath);

                    if (jRows != null)
                    {
                        ParameterFilter parameterFilter = null;
                        if (applyRowFilteration)
                        {
                            parameterFilter =
                                ParameterFilter.CreateFilter(table, parameters);
                        }

                        foreach (JToken t in jRows)
                        {
                            if (!t.HasValues) continue; // if null => continue

                            if (maxRowCount > -1 && table.RowCount >= maxRowCount) break;
                            Dictionary<string, object> dict = parser.Parse((JToken) t);
                            DataPluginUtils.AddRow(dict, table, 
                                settings.NumericDataHelper, parameterFilter, 
                                settings.ParserSettings);
                        }
                    }
                }

                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime,
                    table.RowCount, table.ColumnCount, seconds);

                // We're creating a new StandaloneTable on every request, so
                // allow EX to freeze it.
                table.CanFreeze = true;
            }
            finally {
                table.EndUpdate();
            }

            return table;
        }

        public override JsonSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new JsonSettings(pluginManager, new PropertyBag(), parameters);
        }

        public override JsonSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new JsonSettings(pluginManager, bag, null);
        }
    }
}
