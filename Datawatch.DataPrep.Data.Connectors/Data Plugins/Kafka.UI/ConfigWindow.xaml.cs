﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.KafkaPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    internal partial class ConfigWindow : Window
    {
        private KafkaSettings kafkaSettings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof (ConfigWindow));

        public ConfigWindow(KafkaSettings kafkaSettings)
        {
            this.kafkaSettings = kafkaSettings;
            InitializeComponent();
        }

        public KafkaSettings Settings
        {
            get
            {
                return kafkaSettings;
            }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = kafkaSettings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ClickOK(object sender, RoutedEventArgs e)
        {
            new RoutedCommand("Ok", typeof (ConfigWindow));
        }
    }
}