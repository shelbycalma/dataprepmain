﻿namespace Panopticon.DatabasePlugin
{
    public enum ProviderType
    {
        SqlServer,
        Odbc,
        OleDb
    }
}
