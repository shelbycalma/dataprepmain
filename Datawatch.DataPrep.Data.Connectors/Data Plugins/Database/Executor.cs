﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.DatabasePlugin.Properties;

namespace Panopticon.DatabasePlugin
{
    /// <summary>
    /// Builds and executes queries for the plugin.
    /// </summary>
    public class Executor : DatabaseExecutorBase
    {
        /// <summary>
        /// The id (-1) to assign to the next request.
        /// </summary>
        /// <remarks>
        /// <para>Each call to Get(OnDemand)Data gets assigned a running id,
        /// so that the user can connect the log messages. For now, these ids
        /// are local to the plugin (can't identify requests across
        /// plugins).</para>
        /// </remarks>
        private int nextRequestId;

        public Executor()
            : base()
        {
        }

        /// <summary>
        /// Creates a predicate that checks each row in the query result
        /// against the connection parameters. If the predicate returns true
        /// the row should be loaded. If it returns false the row should be
        /// filtered out. If the method returns null all rows should be
        /// loaded.
        /// </summary>
        protected override Predicate<object[]> CreateFilter(
            StandaloneTable table, IEnumerable<ParameterValue> parameters,
            bool inMemoryFiltering, QueryMode queryMode)
        {
            // This type of filter should only be used if the user has
            // enabled "in-memory" filtering, and is in Manual QueryMode
            if (!inMemoryFiltering || queryMode == QueryMode.Table)
            {
                return null;
            }

            // Plop the parameters into a collection so we can look
            // them up by name.
            ParameterValueCollection lookup = new ParameterValueCollection();
            lookup.AddRange(parameters);

            // Create an array of parameter values of the same length as the
            // number of columns in the table.
            TypedParameterValue[] parameterValues =
                new TypedParameterValue[table.ColumnCount];

            // Now match the column names against the parameter names. If
            // we find a match, put the parameter value in the array in the
            // same position as the column in the table.
            bool anyMatch = false;
            for (int i = 0; i < table.ColumnCount; i++)
            {
                string name = table.GetColumn(i).Name;
                ParameterValue parameterValue = lookup[name];
                if (parameterValue != null)
                {
                    parameterValues[i] = parameterValue.TypedValue;
                    anyMatch = true;
                }
            }
            if (!anyMatch)
            {
                // No column had the same name as a parameter, so it will be
                // more efficient to return null instead of a filter that
                // will return true for every row.
                return null;
            }

            // Filter delegate is now easy to write. Just check each cell
            // value against our parameter value array.
            // TODO: Possible optimization here would be to do it the other
            // way round - for each parameter check the corresponding cell.
            return delegate(object[] values)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    TypedParameterValue parameterValue = parameterValues[i];
                    if (parameterValue != null &&
                        !ParameterHelper.IsEqual(parameterValue, values[i]))
                    {
                        return false;
                    }
                }
                return true;
            };
        }

        public override DbDataAdapter GetAdapter(string queryString,
            string connectionString)
        {

            ProviderType provider = Utils.GetProviderType(connectionString);

            string correctedConnectionString =
                Utils.GetCorrectConnectionString(connectionString);

            switch (provider)
            {
                case ProviderType.SqlServer:
                    return new SqlDataAdapter(queryString,
                        Utils.GetCorrectSqlServerConnectionString(
                        correctedConnectionString));
                case ProviderType.Odbc:
                    return new OdbcDataAdapter(queryString,
                        correctedConnectionString);
                default:
                    return new OleDbDataAdapter(queryString,
                        correctedConnectionString);
            }
        }

        
        
        public override DbConnection GetConnection(string connectionString)
        {

            ProviderType provider = Utils.GetProviderType(connectionString);
            string correctedConnectionString =
                Utils.GetCorrectConnectionString(connectionString);

            switch (provider)
            {
                case ProviderType.SqlServer:
                    return new SqlConnection(
                        Utils.GetCorrectSqlServerConnectionString(
                        correctedConnectionString));
                case ProviderType.Odbc:
                    return new OdbcConnection(correctedConnectionString);
                default:
                    return new OleDbConnection(correctedConnectionString);
            }
        }

        public DatabaseDataReader GetDataReader(
            DatabaseConnectionSettings settings,
            IEnumerable<ParameterValue> parameters,
            int maxRowCount)
        {
            // Get an ID to identify this particular request in log.
            int requestId = Interlocked.Increment(ref nextRequestId);

            ParameterValueCollection escapedParameters = null;
            if (parameters != null)
            {
                escapedParameters =
                DatabaseUtils.EscapeParameters(parameters);
            }

            // First, build the query.
            string query = BuildQuery(settings.QuerySettings,
                maxRowCount, escapedParameters);

            //Then replace special parameters
            query = Utils.ApplySpecialParameters(query, parameters,
                settings.QuerySettings.TimeZoneHelper,
                settings.QuerySettings.EncloseParameterInQuote);

            // Then replace parameters with values.
            query = DataUtils.ApplyParameters(query, escapedParameters,
                settings.QuerySettings.EncloseParameterInQuote);

            string connectionString = DataUtils.ApplyParameters(
                settings.QuerySettings.ConnectionString, parameters,
                settings.QuerySettings.EncloseParameterInQuote);

            // Fix the connection string returned by the connect dialog.
            connectionString =
                Utils.GetCorrectConnectionString(connectionString);

            // Log fixed connection string.
            Log.Info(Resources.LogConnectionString, requestId, CleanConnectionString(connectionString));

            // Run the query and get the result as an ITable. This call
            // also handles in-memory filtering.
            DatabaseDataReader dataReader = new DatabaseDataReader(
                requestId, connectionString, query, settings, maxRowCount);

            return dataReader;
        }

		private string CleanConnectionString(string connstr)
		{
			string newStr = "";

			string[] conn = connstr.Split(';');
			foreach (string s in conn)
			{
				if (!(s.ToUpper().StartsWith("PASSWORD") || s.ToUpper().StartsWith("PWD") || s.ToUpper().StartsWith("RTK")))
				{
					newStr += (newStr.Length > 0 ? ";" : String.Empty) + s;
				}
			}
			return newStr;
		}

		protected override Dictionary<string, string> RenameTimeBucketingAliases(
            OnDemandParameters onDemandParameters)
        {
            return DatabaseUtils.RenameTimeBucketingAliases(onDemandParameters);
        }
    }
}
