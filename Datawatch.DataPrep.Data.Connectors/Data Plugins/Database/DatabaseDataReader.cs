﻿using System;
using System.Data;
using System.Data.Common;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.IntelligentCache;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.DatabasePlugin.Properties;

namespace Panopticon.DatabasePlugin
{
    public class DatabaseDataReader : DataReaderBase
    {
        private DbDataReader reader;
        private Func<object, object>[] converters;
        
        private readonly int maxRowCount;
        private int rowsRead;
        
        public DatabaseDataReader(int requestId,
            string connectionString, string query,
            DatabaseConnectionSettings settings,
            int maxRowCount)
        {
            this.maxRowCount = maxRowCount;

            Initialize(requestId, connectionString, query, settings);

            if (reader == null)
            {
                throw new Exception("Failed to initialize data reader");
            }
        }

        private void Initialize(int requestId,
            string connectionString, string query,
            DatabaseConnectionSettings settings)
        {
            if (settings == null) return;

            DatabaseQuerySettings databaseQuerySettings =
                settings.QuerySettings as DatabaseQuerySettings;

            if (databaseQuerySettings == null) return;

            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingQuery, requestId, query);
            DateTime start = DateTime.Now;
            Executor executor = new Executor();
            DbDataAdapter adapter =
                executor.GetAdapter(query, connectionString);

            adapter.SelectCommand.CommandTimeout = databaseQuerySettings.QueryTimeout;

            // Have to manually open the connection as the adapter will
            // only do that when you use Fill or GetData.
            adapter.SelectCommand.Connection.Open();

            // Get the reader so we can process row by row.
            reader =
                adapter.SelectCommand.ExecuteReader(
                    CommandBehavior.CloseConnection |
                    CommandBehavior.SingleResult |
                    CommandBehavior.KeyInfo);

            SchemaTable = DatabaseUtils.CreateStandaloneTable(reader, databaseQuerySettings);
            this.AdoSchemaTable = GetAdoSchemaFromReader(reader);

            values = new object[FieldCount];

            // Each column may have a value converter associated,
            // typically ADO TimeSpan columns need to be converted
            // into DateTimes for SDK Time columns, but most are
            // converted "naturally" (and for those the converter
            // will be set to null).
            converters =
                executor.CreateConverters(reader, settings.QuerySettings);
            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Resources.LogColumnsRetrievedAndTime,
                requestId, reader.FieldCount, seconds);

            rowsRead = 0;
        }

        protected static DataTable GetAdoSchemaFromReader(DbDataReader reader)
        {
            // See DataReaderBase.CreateAdoSchemaFromITable.
            DataTable schema = new DataTable();
            DataColumn[] columns = {
                new DataColumn("ColumnName", typeof(string)),
                new DataColumn("ColumnOrdinal", typeof(int)),
                new DataColumn("DbType", typeof(int)),
                new DataColumn("Size", typeof(int)),
                new DataColumn("IsLong", typeof(bool))
            };
            schema.Columns.AddRange(columns);
            DataTable providerSchema = reader.GetSchemaTable();
            switch (reader.GetType().Name) {
                case "OdbcDataReader":
                    GetAdoSchemaFromOdbcSchema(
                        schema, reader, providerSchema);
                    break;
                case "OleDbDataReader":
                    GetAdoSchemaFromOleDbSchema(
                        schema, reader, providerSchema);
                    break;
                case "OracleDbDataReader":
                    GetAdoSchemaFromOracleSchema(
                        schema, reader, providerSchema);
                    break;
                case "SqlDataReader":
                    GetAdoSchemaFromSqlSchema(
                        schema, reader, providerSchema);
                    break;
                default:
                    Log.Warning("Unknown data provider " +
                        reader.GetType().Name + ", cannot get native " +
                        "schema.");
                    // Fall back on creating schema from ITable.
                    schema = null;
                    break;
            }
            return schema;
        }

        protected static void GetAdoSchemaFromOdbcSchema(DataTable schema,
            DbDataReader odbcReader, DataTable odbcSchema)
        {
            foreach (DataRow odbcRow in odbcSchema.Rows) {
                DataRow schemaRow = schema.NewRow();
                int ordinal = Convert.ToInt32(odbcRow["ColumnOrdinal"]);
                schemaRow["ColumnName"] = odbcRow["ColumnName"];
                schemaRow["ColumnOrdinal"] = ordinal;
                Type fieldType = odbcReader.GetFieldType(ordinal);
                schemaRow["DbType"] = GetCoercedAdoSchemaColumnType(fieldType);
                int fieldSize = Convert.ToInt32(odbcRow["ColumnSize"]);
                schemaRow["Size"] = GetCoercedAdoSchemaColumnSize(fieldType,
                    fieldSize);
                schemaRow["IsLong"] = odbcRow["IsLong"];
                schema.Rows.Add(schemaRow);
            }
        }

        protected static void GetAdoSchemaFromOleDbSchema(DataTable schema,
            DbDataReader oleDbReader, DataTable oleDbSchema)
        {
            foreach (DataRow oleDbRow in oleDbSchema.Rows) {
                DataRow schemaRow = schema.NewRow();
                int ordinal = Convert.ToInt32(oleDbRow["ColumnOrdinal"]);
                schemaRow["ColumnName"] = oleDbRow["ColumnName"];
                schemaRow["ColumnOrdinal"] = ordinal;
                Type fieldType = oleDbReader.GetFieldType(ordinal);
                schemaRow["DbType"] = GetCoercedAdoSchemaColumnType(fieldType);
                int fieldSize = Convert.ToInt32(oleDbRow["ColumnSize"]);
                schemaRow["Size"] = GetCoercedAdoSchemaColumnSize(fieldType,
                    fieldSize);
                schemaRow["IsLong"] = oleDbRow["IsLong"];
                schema.Rows.Add(schemaRow);
            }
        }

        protected static void GetAdoSchemaFromOracleSchema(DataTable schema,
            DbDataReader oracleReader, DataTable oracleSchema)
        {
            foreach (DataRow oracleRow in oracleSchema.Rows) {
                DataRow schemaRow = schema.NewRow();
                int ordinal = Convert.ToInt32(oracleRow["ColumnOrdinal"]);
                schemaRow["ColumnName"] = oracleRow["ColumnName"];
                schemaRow["ColumnOrdinal"] = ordinal;
                Type fieldType = oracleReader.GetFieldType(ordinal);
                schemaRow["DbType"] = GetCoercedAdoSchemaColumnType(fieldType);
                int fieldSize = Convert.ToInt32(oracleRow["ColumnSize"]);
                schemaRow["Size"] = GetCoercedAdoSchemaColumnSize(fieldType,
                    fieldSize);
                schemaRow["IsLong"] = oracleRow["IsLong"];
                schema.Rows.Add(schemaRow);
            }
        }

        protected static void GetAdoSchemaFromSqlSchema(DataTable schema,
            DbDataReader sqlReader, DataTable sqlSchema)
        {
            foreach (DataRow sqlRow in sqlSchema.Rows) {
                DataRow schemaRow = schema.NewRow();
                int ordinal = Convert.ToInt32(sqlRow["ColumnOrdinal"]);
                schemaRow["ColumnName"] = sqlRow["ColumnName"];
                schemaRow["ColumnOrdinal"] = ordinal;
                Type fieldType = sqlReader.GetFieldType(ordinal);
                schemaRow["DbType"] = GetCoercedAdoSchemaColumnType(fieldType);
                int fieldSize = Convert.ToInt32(sqlRow["ColumnSize"]);
                schemaRow["Size"] = GetCoercedAdoSchemaColumnSize(fieldType,
                    fieldSize);
                schemaRow["IsLong"] = sqlRow["IsLong"];
                schema.Rows.Add(schemaRow);
            }
        }

        protected static DbType GetCoercedAdoSchemaColumnType(Type fieldType)
        {
            ColumnType type = DatabaseUtils.GetColumnType(fieldType, false);
            switch (type) {
                case ColumnType.Text:
                    return DbType.String;
                case ColumnType.Numeric:
                    return DbType.Double;
                case ColumnType.Time:
                    return DbType.DateTime;
            }
            throw Datawatch.DataPrep.Data.Core.Exceptions.BadColumnType(fieldType);
        }

        protected static int GetCoercedAdoSchemaColumnSize(Type fieldType,
            int fieldSize)
        {
            ColumnType type = DatabaseUtils.GetColumnType(fieldType, false);
            if (type == ColumnType.Text)
            {
                if (fieldType == typeof(Boolean))
                {
                    return 5;
                }
                else if (fieldType == typeof(Guid))
                {
                    return 36;
                }
                else
                {
                    return fieldSize;
                }
            }
            return 0;
        }

        public override void Close()
        {
            reader.Close();
        }

        public override string GetName(int i)
        {
            return reader.GetName(i);
        }

        public override bool IsClosed
        {
            get 
            {
                return reader.IsClosed; 
            }
        }

        public override bool IsDBNull(int i)
        {
            return reader.IsDBNull(i);
        }

        public override bool Read()
        {
            if (maxRowCount >= 0 && rowsRead >= maxRowCount) {
                return false;
            }

            if (!reader.Read()) return false;

            try
            {
                //Sybase may get through reader.read() but would 
                //throw an exception with blank message when there 
                //is really no rows in case of grouping statements
                reader.GetValues(values);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    string.Format(Properties.Resources.
                    ExReadingFailedAtRow,
                    (SchemaTable.RowCount + 1) + ". " + ex.Message, ex));
            }

            DatabaseUtils.ConvertNulls(values);
            for (int i = 0; i < FieldCount; i++)
            {
                if (converters[i] != null)
                {
                    values[i] = converters[i](values[i]);
                }
            }
            rowsRead++;

            return true;
        }
    }
}
