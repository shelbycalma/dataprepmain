﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Core.ViewModel;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DatabasePlugin
{
    // TODO need to use DatabaseQuerySettings, and remove redundant properties
    // which are already there in QuerySettings.
    public class DatabaseConnectionSettings :
        ConnectionSettings, IDatabaseConnectionStringProvider,
        IQueryBuilderTableClient
    {
        private IEnumerable<ParameterValue> parameters;
        private ParameterValueCollection escapedParameters;
        protected TimeZoneHelper timeZoneHelper;
        private readonly DatabaseConnectorQuerySettings querySettings;

        private readonly QueryBuilderTableViewModel<DatabaseColumn> queryBuilderViewModel;

        public DatabaseConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bag"></param>
        /// <param name="parameters"></param>
        /// <param name="initObjectsFromBag">Pass false as parameter value 
        /// when you want to avoid loading properties like SelectedColumns/PredicateColumns/TimeZoneHelper.
        /// This is typically used, when we want to access only property values like IsOnDemand etc.
        /// An example use is there in Plugin.GetTitle method.
        /// </param>
        public DatabaseConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters, bool initObjectsFromBag = true)
            : base(bag)
        {
            if (bag.IsEmpty())
            {
                Version = new Version("2.0.0.0");
            }

            this.parameters = parameters;

            string table = GetInternal("Table");
            SchemaAndTable selectedTable = null;

            if (!string.IsNullOrEmpty(table))
            {
                selectedTable = SchemaAndTable.Parse(table);
            }

            // Fix for DDTV-3322, the Database connector used to store
            // each predicate column with the PredicateColumn_ prefix
            // while the refactored code uses the Column_ prefix. So for previous workbooks
            //changed the name from PredicateColumn_ to Column_
            PropertyBag predicateColumnsBag = bag.SubGroups["PredicateColumns"];
            if (predicateColumnsBag != null && predicateColumnsBag.SubGroups.Count > 0)
            {
                foreach (PropertyBag subGroup in predicateColumnsBag.SubGroups)
                {
                    string subGroupName = subGroup.Name;
                    if (subGroupName.Contains("PredicateColumn_"))
                    {
                        subGroup.Name =
                            subGroupName.Replace("PredicateColumn_", "Column_");
                    }
                }
            }

            querySettings = new DatabaseConnectorQuerySettings(bag, this,
                selectedTable);

            if (Version.Major < 2)
            {
                HandlePropertyRenames();
            }

            // TODO: Should refactor to lazy loading of selected/Predicate columns
            if (!initObjectsFromBag)
            {
                return;
            }

            if (Version.Major < 2)
            {
                //if bag has columns and it doesn't contain subgroups
                //then its an old workbook and selected columns needs to be rebuilt
                //so that it will sync with new workbook like  having column type & default aggregation method.
                PropertyBag columnsBag = bag.SubGroups["Columns"];
                if (columnsBag != null && columnsBag.SubGroups.Count == 0 &&
                    columnsBag.Values.Count > 0)
                {
                    PopulateSelectedColumnsFromOldWorkbook(columnsBag);
                }
                else
                {
                    PopulateSelectedColumns(columnsBag);
                }

                PropertyBag predicatesBag = bag.SubGroups["PredicateColumns"];
                if (predicatesBag != null)
                {
                    PopulatePredicateColumns(predicatesBag);
                }
            }

            queryBuilderViewModel =
                new QueryBuilderTableViewModel<DatabaseColumn>(this, QuerySettings);
            queryBuilderViewModel.DoUpdateQuery +=
                QueryBuilderViewModel_DoUpdateQuery;
        }

        private void QueryBuilderViewModel_DoUpdateQuery(object sender,
            EventArgs e)
        {
            UpdateQuery();
        }

        public QueryBuilderTableViewModel<DatabaseColumn> QueryBuilderViewModel
        {
            get { return queryBuilderViewModel; }
        }

        public string ConnectionString
        {
            get { return GetInternal("ConnectionString"); }
            set
            {
                string oldActual = DatabaseConnectionString;

                SetInternal("ConnectionString", value);

                if (oldActual != DatabaseConnectionString)
                {
                    FirePropertyChanged("ConnectionStringActual");
                }
            }
        }

        public string ConnectionStringSettingsName
        {
            get { return GetInternal("ConnectionStringSettingsName"); }
            set
            {
                string oldActual = DatabaseConnectionString;

                SetInternal("ConnectionStringSettingsName", value);

                if (oldActual != DatabaseConnectionString)
                {
                    FirePropertyChanged("ConnectionStringActual");
                }
            }
        }

        public string[] ConnectionStringSettingsNames
        {
            get
            {
                List<string> list = new List<string>();
                foreach (ConnectionStringSettings css in
                    ConfigurationManager.ConnectionStrings)
                {
                    list.Add(css.Name);
                }

                return list.ToArray();
            }
        }

        public string DatabaseConnectionString
        {
            get
            {
                if (ConnectionStringSettingsName == null)
                {
                    return ConnectionString;
                }

                ConnectionStringSettings connStrSettings =
                    ConfigurationManager.ConnectionStrings[ConnectionStringSettingsName];
                return connStrSettings != null
                    ? connStrSettings.ConnectionString
                    : ConnectionString;
            }
        }

        public string PreviewQuery
        {
            get { return GetInternal("previewquery"); }
            set { SetInternal("previewquery", value); }
        }

        public IEnumerable<DatabaseColumn> Columns
        {
            get
            {
                if (querySettings.SelectedTable == null)
                {
                    return null;
                }
                try
                {
                    string connectionString =
                        DataUtils.ApplyParameters(
                        querySettings.ConnectionString, parameters, false);

                    connectionString =
                        Utils.GetCorrectConnectionString(connectionString);
                    Executor executor = new Executor();
                    return executor.GetColumns(querySettings, parameters,
                        connectionString);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }

                return null;
            }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public ParameterValueCollection EscapedParameters
        {
            get
            {
                if (parameters == null) return null;

                if (escapedParameters == null)
                {
                    escapedParameters =
                        DatabaseUtils.EscapeParameters(parameters);
                }
                return escapedParameters;
            }
        }

        public string JavaJndiResourceName
        {
            get { return GetInternal("KeyJavaJndiResourceName"); }
            set { SetInternal("KeyJavaJndiResourceName", value); }
        }

        public string JavaDriverClassName
        {
            get { return GetInternal("JavaDriverClassName"); }
            set { SetInternal("JavaDriverClassName", value); }
        }

        public string JavaUrl
        {
            get { return GetInternal("KeyJavaUrl"); }
            set { SetInternal("KeyJavaUrl", value); }
        }

        public string JavaUsername
        {
            get { return GetInternal("KeyJavaUsername"); }
            set { SetInternal("KeyJavaUsername", value); }
        }

        public string JavaPassword
        {
            get { return GetInternal("KeyJavaPassword"); }
            set { SetInternal("KeyJavaPassword", value); }
        }

        public string SelectedDatabase
        {
            get { return GetInternal("SelectedDatabase"); }
            set { SetInternal("SelectedDatabase", value); }
        }

        public virtual bool IsDatabasePlugin
        {
            get { return true; }
        }

        public void InitConnectionString(string connectionString)
        {
            ConnectionString = connectionString;
            querySettings.SqlDialect = SqlDialectFactory.GetDialectByConnectionString(connectionString);
        }

        protected void PopulateSelectedColumns(PropertyBag columnsBag)
        {
            if (columnsBag == null || columnsBag.SubGroups.Count == 0 ||
                columnsBag.SubGroups[0].Values.Count == 0)
            {
                return;
            }

            querySettings.SelectedColumns.Clear();
            foreach (PropertyBag column in columnsBag.SubGroups)
            {
                // Handle backward compatibility with respect
                // to AggregationMethod changed to AggregateType.

                string oldAggregationMethod = column.Values["AggregationMethod"];

                if (!string.IsNullOrEmpty(oldAggregationMethod))
                {
                    try
                    {
                        AggregationType aggregationType =
                            ((AggregationType)Convert.ToInt16(oldAggregationMethod));

                        AggregateType aggregateType = (AggregateType)Enum.Parse(
                                typeof(AggregateType), aggregationType.ToString());
                        int index =
                            Array.IndexOf(Enum.GetValues(aggregateType.GetType()),
                            aggregateType);
                        column.Values["AggregateType"] = index.ToString();
                    }
                    catch
                    {
                    }
                }

                DatabaseColumn col = new DatabaseColumn(column);
                querySettings.SelectedColumns.Add(col);
            }
        }

        /// <summary>
        /// Tries to build Selected Columns from Old workbooks
        /// </summary>
        private void PopulateSelectedColumnsFromOldWorkbook(PropertyBag columnsBag)
        {
            if (querySettings.SelectedTable == null ||
                columnsBag == null || columnsBag.Values.Count == 0)
            {
                return;
            }
            Executor executor = new Executor();
            string connectionString =
                            DataUtils.ApplyParameters(
                            querySettings.ConnectionString, parameters, false);

            connectionString =
                Utils.GetCorrectConnectionString(connectionString);

            List<DatabaseColumn> availableColumns =
                executor.GetColumns(querySettings, parameters,
                connectionString).ToList();
            querySettings.SelectedColumns.Clear();
            for (int i = 0; i < columnsBag.Values.Count; i++)
            {
                string colName = columnsBag.Values["Column_" + i];
                if (string.IsNullOrEmpty(colName))
                {
                    continue;
                }
                foreach (DatabaseColumn availableColumn in availableColumns)
                {
                    if (colName.Equals(availableColumn.ColumnName))
                    {
                        DatabaseColumn selectedCol = new DatabaseColumn(new PropertyBag());
                        selectedCol.ColumnName = availableColumn.ColumnName;
                        selectedCol.AggregateType =
                            Utils.GetDefaultAggregationMethod(
                            availableColumn.ColumnType);
                        selectedCol.ColumnType = availableColumn.ColumnType;
                        querySettings.SelectedColumns.Add(selectedCol);
                        break;
                    }
                }
            }

        }

        protected void PopulatePredicateColumns(
            PropertyBag predicatesBag)
        {
            if (predicatesBag == null || predicatesBag.SubGroups.Count == 0 ||
                predicatesBag.SubGroups[0].Values.Count == 0)
            {
                return;
            }
            querySettings.PredicateColumns.Clear();
            foreach (PropertyBag column in predicatesBag.SubGroups)
            {
                DatabaseColumn predicateCol = new DatabaseColumn(column);
                querySettings.PredicateColumns.Add(predicateCol);
            }
        }

        private void HandlePropertyRenames()
        {
            // Handle backward compatibility for property renames.


            // Fix for [17815] Backwards compatibility break between 6.3.0 
            // and 6.3.1 - all ODBC DSN data source SQL queries blanked.
            // The default value needs to be "Query" to not break backward
            // compatibility.

            // TODO can we get rid of still supporting 6.3 workbooks?
            // Ideally We should only set querySettings.QueryMode when SourceMode is not null.
            string sourceMode = GetInternal("SourceMode", "Query");
            try
            {
                querySettings.QueryMode = (QueryMode)Enum.Parse(
                    typeof(QueryMode), sourceMode);
            }
            catch
            {
            }

            string onDemand = GetInternal("OnDemand", false.ToString());
            querySettings.IsOnDemand = bool.Parse(onDemand);

            string query = GetInternal("query");
            if (!string.IsNullOrEmpty(query))
            {
                querySettings.Query = query;

            }

            string isConstrain = GetInternal("IsConstrain");
            if (!string.IsNullOrEmpty(isConstrain))
            {
                querySettings.IsConstrainByDateTime = bool.Parse(isConstrain);
                SetInternal("IsConstrainByDateTime", isConstrain);
            }

            //populate AggregationMethod
            string aggregationMethod = GetInternal("AggregationMethod");
            if (!string.IsNullOrEmpty(aggregationMethod))
            {
                SetInternal("AggregateType", aggregationMethod);
            }

            string encloseParameterInQuote =
                GetInternal("KeyEncloseParameterInQuote");
            if (!string.IsNullOrEmpty(encloseParameterInQuote))
            {
                querySettings.EncloseParameterInQuote =
                    bool.Parse(encloseParameterInQuote);
            }
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag bag = base.ToPropertyBag();

            bag.Merge(querySettings.ToPropertyBag());
            //save selected table into bag.
            SetInternal("Table", querySettings.Table);

            if (Version.Major < 2)
            {
                // Set Columns and PredicateColumns subgroups to null.
                // These are already loaded and set to QuerySettings.
                bag.SubGroups["Columns"] = null;
                bag.SubGroups["PredicateColumns"] = null;

                SetInternal("OnDemand", null);
                SetInternal("query", null);

                SetInternal("isConstrain", null);

                SetInternal("AggregationMethod", null);
                SetInternal("KeyEncloseParameterInQuote", null);
                // Old workbook settings have already being handled,
                // now we can upgrade the Version.
                Version = new Version("2.0.0.0");

            }
            return bag;
        }

        private void UpdateQuery()
        {
            if (querySettings.QueryMode == QueryMode.Table)
            {
                Executor executor = new Executor();

                querySettings.Query = executor.GenerateQuery(querySettings,
                    EscapedParameters);
            }
        }

        public Version Version
        {
            get
            {
                return new Version(GetInternal("Version", "1.0.0.0"));
            }
            private set
            {
                SetInternal("Version", value.ToString());
            }
        }

        public DatabaseQuerySettings QuerySettings
        {
            get { return querySettings; }
        }

        public static bool GetIsOnDemand(PropertyBag bag)
        {
            bool isOnDemand = false;
            string value = bag.Values["OnDemand"];
            if (string.IsNullOrEmpty(value))
            {
                value = bag.Values["IsOnDemand"];
            }

            if (!string.IsNullOrEmpty(value))
            {
                isOnDemand = bool.Parse(value);
            }
            return isOnDemand;
        }

        public SchemaAndTable[] Tables
        {
            get
            {
                string connectionString =
                 Utils.ApplyParameters(querySettings.ConnectionString,
                                       this.Parameters, false);
                return Utils.GetTablesAndViews(connectionString);
            }
        }

        public bool CanLoadTables
        {
            get
            {
                if (this != null &&
                    this.QuerySettings.ConnectionString != null
                    && this.QuerySettings.ConnectionString.Length > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

    }
}

