﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Common;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.DatabasePlugin.Properties;

namespace Panopticon.DatabasePlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<DatabaseConnectionSettings>,
        IOnDemandPlugin, IDataReaderPlugin, ISchemaSavingPlugin
    {
        internal const string PluginId = "DatabasePlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "OLEDB";
        internal const string PluginType = DataPluginTypes.Database;

        protected bool canSelectColumns = true;

        private readonly Executor executor;

        public Plugin()
        {
            canSelectColumns = true;
            executor = new Executor();
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override DatabaseConnectionSettings CreateSettings(PropertyBag bag)
        {
            return new DatabaseConnectionSettings(bag, null);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            CheckLicense();

            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(settings,parameters);
            if (connectionSettings.QuerySettings != null)
            {
                connectionSettings.QuerySettings.ApplyRowFilteration =
                    applyRowFilteration;
            }
            return executor.GetData(
                connectionSettings.QuerySettings, parameters, rowcount);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            return null;
        }

        public IDataReaderWithSchema GetDataReader(
            string workbookDir, string dataDir, PropertyBag settings,
            IEnumerable<ParameterValue> parameters, int maxRowCount)
        {
            CheckLicense();

            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(settings, parameters);
            if (connectionSettings.QuerySettings != null)
            {
                connectionSettings.QuerySettings.ApplyRowFilteration =
                    false;
            }
            return executor.GetDataReader(
                connectionSettings, parameters, maxRowCount);
        }

        public ITable GetOnDemandData(string workbookDir, string dataDir,
                            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            CheckLicense();

            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(settings, parameters);

            return executor.GetOnDemandData(connectionSettings.QuerySettings,
                parameters, onDemandParameters, rowcount, false);
        }
        public bool IsOnDemand(PropertyBag settings)
        {
            return DatabaseConnectionSettings.GetIsOnDemand(settings);
        }

        public void SaveSchema(string workbookDir,
            string dataDir, PropertyBag settings,
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(settings, parameters);
            connectionSettings.QuerySettings.IsSchemaRequest =
                    true;
            if (connectionSettings.QuerySettings.IsOnDemand)
            {
                executor.GetOnDemandData(connectionSettings.QuerySettings,
                    parameters, null, 0, false);
            }
            else
            {
                executor.GetData(
                    connectionSettings.QuerySettings, parameters, 0);
            }
        }

        public override void UpdateFileLocation(PropertyBag settings,
                    string oldPath, string newPath, string workbookPath)
        {
        }
        //private static readonly string[] fileExtensions = 
        //    {"mdb","mde","accdb","accde"};

        //public string[] FileExtensions
        //{
        //    get { return fileExtensions; }
        //}

        //public string FileFilter
        //{
        //    get
        //    {
        //        return Dashboards.Utils.CreateFilterString(
        //            Resources.UiAccessDatabases, fileExtensions);
        //    }
        //}

        //public bool CanOpen(string filePath)
        //{
        //    return DataPluginUtils.CanOpen(filePath, fileExtensions);
        //}

        //public PropertyBag DoOpenFile(string filePath)
        //{
        //    string connectionString = GetConnectionString(filePath);
        //    DatabaseConnectionSettings connectionSettings =
        //        new DatabaseConnectionSettings(new PropertyBag(), null);
        //    connectionSettings.QuerySettings.QueryMode = QueryMode.Table;
        //    connectionSettings.QuerySettings.SqlDialect = SqlDialectFactory.AccessExcel;

        //    connectionSettings.InitConnectionString(connectionString);
        //    QueryWindow win =
        //        new QueryWindow(connectionSettings)
        //        {
        //            Owner = Application.Current.MainWindow
        //        };

        //    return DataPluginUtils.ShowDialog(host, win,
        //        win.ConnectionSettings); 
        //}

        private string GetConnectionString(string filePath)
        {
            if (Utils.IsAceOleDb12Present)
            {
                return string.Format(
                    "Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source={0};Persist Security Info=False;",
                    filePath);
            }
            return string.Format(
                "Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source={0};User Id=admin;Password=;", filePath);
        }
    }    
}
