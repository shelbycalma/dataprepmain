﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.OleDb;
using System.IO;
using System.Reflection;

using ADODB;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DatabasePlugin
{
    public class Utils
    {
        private static readonly string TimeWindowStart = "TimeWindowStart";
        private static readonly string TimeWindowEnd = "TimeWindowEnd";
        private static readonly string Snapshot = "Snapshot";
        private const string monarchConnectionString =
            "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Persist Security Info=False";
        private const string monarchExportSearchPattern = "*.dwx";

        public static List<string> SpecialParameters = new List<string>()
        {            
            TimeWindowStart,
            TimeWindowEnd,
            Snapshot
        };


        /// <summary>
        /// Replaces parameters in query with actual values.
        /// </summary>
        public static string ApplyParameters(string query,
            IEnumerable<ParameterValue> parameters, bool encloseParameterInQuote,
            bool enforceSingleQuoteHanding = true)
        {
            // No parameters, do nothing. Testing for count == 0 would
            // be nice but might be expensive with enumerable?
            if (parameters == null)
            {
                return query;
            }

            return new ParameterEncoder
            {
                Parameters = parameters,
                SourceString = query,
                EnforceSingleQuoteEnclosure = encloseParameterInQuote,
                EnforceSingleQuoteHandling = enforceSingleQuoteHanding,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }

        /// <summary>
        /// Replaces Special parameters in query like TimeWindowStart and
        /// applies timezone shift if selected.
        /// </summary>
        public static string ApplySpecialParameters(string sourceString,
            IEnumerable<ParameterValue> parameters, TimeZoneHelper timeZoneHelper,
            bool encloseParameterInQuote)
        {
            //if timezone is not selected then no need process, all the parameters
            //would be taken care in ApplyParameters method.
            if (!timeZoneHelper.TimeZoneSelected)
            {
                return sourceString;
            }

            try
            {
                if (parameters == null) return sourceString;

                foreach (string specialParam in SpecialParameters)
                {
                    string oldKey = "$" + specialParam;
                    string newKey = "{" + specialParam + "}";
                    if (!(sourceString.IndexOf(oldKey) != -1
                    || sourceString.IndexOf(newKey) != -1))
                    {
                        continue; // parameter not used in source string
                    }

                    while (sourceString.Contains(oldKey))
                    {
                        sourceString = sourceString.Replace(oldKey, newKey);
                    }

                    foreach (ParameterValue parameterValue in parameters)
                    {
                        if (string.IsNullOrEmpty(parameterValue.Name) ||
                            parameterValue.TypedValue == null)
                        {
                            continue;
                        }

                        if (specialParam.Equals(parameterValue.Name) &&
                            parameterValue.TypedValue is DateTimeParameterValue)
                        {
                            DateTime value = ((DateTimeParameterValue)
                                parameterValue.TypedValue).Value;
                            value = timeZoneHelper.ConvertToUTC(value);
                            string strValue = value.ToString();
                            if (encloseParameterInQuote)
                            {
                                strValue = "'" + strValue + "'";
                            }
                            sourceString = sourceString.Replace(
                                    newKey, strValue);
                            break;

                        }
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                throw ex;
            }
            return sourceString;
        }

        public static void FixPath(string workbookDir, string dataDir,
            DatabaseConnectionSettings connectionSettings)
        {
            string databasePath =
                DataUtils.FixFilePath(workbookDir, dataDir,
                    Utils.ApplyParameters(connectionSettings.SelectedDatabase,
                        connectionSettings.Parameters, false),
                        connectionSettings.Parameters);

            connectionSettings.ConnectionString =
                Utils.GetMonarchConnectionString(databasePath);
        }

        public static AggregateType GetDefaultAggregationMethod(
            ColumnType type)
        {
            if (type == ColumnType.Numeric)
            {
                return AggregateType.Sum;
            }
            else
            {
                return AggregateType.None;
            }
        }

        public static Dictionary<string, string> GetDatabases(string path)
        {

            string[] filePaths = Directory.GetFiles(path, monarchExportSearchPattern,
                                         SearchOption.AllDirectories);
                      
            Dictionary<string, string> fileDict = new Dictionary<string, string>();
            for (int i = 0; i < filePaths.Length; i++)
            {
                string filePath = filePaths[i];
                string fileName = Path.GetFileNameWithoutExtension(filePath);
                fileDict.Add(fileName, filePath);
            }
                            
            return fileDict;
        }

        public static string GetMonarchConnectionString(string databasePath)
        {
            return string.Format(monarchConnectionString,
                databasePath);
        }

        public static SchemaAndTable[] GetTablesAndViews(string connectionString)
        {
            List<SchemaAndTable> list = new List<SchemaAndTable>();
            ConnectionClass connection = new ConnectionClass();

            connection.Open(connectionString, null, null,
                (int)ConnectOptionEnum.adConnectUnspecified);

            Recordset recordset =
                connection.OpenSchema(ADODB.SchemaEnum.adSchemaTables,
                    new object[] { null, null, null, "TABLE" }, Missing.Value);

            while (!recordset.EOF)
            {
                string table = recordset.Fields["TABLE_NAME"].Value as string;
                string schema = recordset.Fields["TABLE_SCHEMA"].Value as string;
                if (schema != null && schema.Length == 0)
                {
                    schema = null;
                }

                list.Add(new SchemaAndTable(schema, table));

                recordset.MoveNext();
            }

            recordset.Close();

            recordset =
                connection.OpenSchema(ADODB.SchemaEnum.adSchemaTables,
                    new object[] { null, null, null, "VIEW" }, Missing.Value);
            while (!recordset.EOF)
            {
                string view = recordset.Fields["TABLE_NAME"].Value as string;
                string schema = recordset.Fields["TABLE_SCHEMA"].Value as string;
                if (schema != null && schema.Length == 0)
                {
                    schema = null;
                }

                list.Add(new SchemaAndTable(schema, view));
                recordset.MoveNext();
            }

            recordset.Close();
            connection.Close();

            return list.ToArray();
        }

        public static bool IsSpecialParameter(string paramName)
        {
            if (string.IsNullOrEmpty(paramName))
            {
                return false;
            }
            if (SpecialParameters.Contains(paramName))
            {
                return true;
            }

            return false;
        }



        /// <summary>
        /// Returns a connection string that is correct.
        /// UDL connection dialog returns erroneous connection string when it's using ODBC.
        /// </summary>
        /// <param name="connectionString">The connection string to correct.</param>
        /// <returns>A correct connection string.</returns>
        public static string GetCorrectConnectionString(string connectionString)
        {
            if (GetProviderType(connectionString) == ProviderType.Odbc)
            {
                DbConnectionStringBuilder dbcsb = new DbConnectionStringBuilder(true);
                dbcsb.ConnectionString = connectionString;

                string dataSource = GetConnectionStringValue(dbcsb, "data source");
                string userId = GetConnectionStringValue(dbcsb, "user id");
                string password = GetConnectionStringValue(dbcsb, "password");
                string database = GetConnectionStringValue(dbcsb, "initial catalog");

                if (dataSource != null)
                {
                    dbcsb.Remove("data source");

                    if (!dbcsb.ContainsKey("dsn"))
                    {
                        dbcsb["dsn"] = dataSource;
                    }
                }

                if (userId != null)
                {
                    dbcsb.Remove("user id");

                    if (!dbcsb.ContainsKey("uid"))
                    {
                        dbcsb["uid"] = userId;
                    }
                }

                if (password != null)
                {
                    dbcsb.Remove("password");

                    if (!dbcsb.ContainsKey("pwd"))
                    {
                        dbcsb["pwd"] = password;
                    }
                }

                if (database != null)
                {
                    dbcsb.Remove("initial catalog");

                    if (!dbcsb.ContainsKey("database"))
                    {
                        dbcsb["database"] = database;
                    }
                }

                connectionString = dbcsb.ConnectionString;
            }

            return connectionString;
        }

        public static string GetCorrectSqlServerConnectionString(
            string connectionString)
        {
            DbConnectionStringBuilder builder =
                    new DbConnectionStringBuilder()
                    {
                        ConnectionString = connectionString
                    };
            builder.Remove("Provider");
            return builder.ToString();
        }

        private static string GetConnectionStringValue(
            DbConnectionStringBuilder dbcsb, string key)
        {
            if (dbcsb.ContainsKey(key))
            {
                return (string)dbcsb[key];
            }
            return null;
        }

        public static ProviderType GetProviderType(string connectionString)
        {
            DbConnectionStringBuilder strBuilder = new DbConnectionStringBuilder();
            strBuilder.ConnectionString = connectionString;
            string provider = "";
            if (strBuilder.ContainsKey("Provider"))
            {
                provider = (string)strBuilder["Provider"];
                provider = provider.ToLower();
            }
            if (provider.StartsWith("sqloledb"))
            {
                return ProviderType.SqlServer;	// SQL Server 7.0 and later.
            }
            else if (provider.StartsWith("msdasql"))
            {
                return ProviderType.Odbc;
            }
            else
            {
                return ProviderType.OleDb;	// Can handle all except ODBC.
            }
        }

        public static bool IsAceOleDb12Present
        {
            get
            {
                OleDbEnumerator enumerator = new OleDbEnumerator();
                DataTable table = enumerator.GetElements();

                foreach (DataRow row in table.Rows)
                {
                    bool nameFound = row["SOURCES_NAME"].ToString().Equals("Microsoft.ACE.OLEDB.12.0");
                    bool clsidFound = row["SOURCES_CLSID"].ToString().Equals("{3BE786A0-0366-4F5C-9434-25CF162E475E}");
                    if (nameFound && clsidFound)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
    }
}
