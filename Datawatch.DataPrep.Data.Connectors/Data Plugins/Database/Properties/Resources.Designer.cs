﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Panopticon.DatabasePlugin.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Panopticon.DatabasePlugin.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Column &apos;{0}&apos; requested with different aggregates (&apos;{1}&apos; and &apos;{2}&apos;)..
        /// </summary>
        public static string ExAggregateMissmatch {
            get {
                return ResourceManager.GetString("ExAggregateMissmatch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aggregate &apos;{0}&apos; not supported..
        /// </summary>
        public static string ExAggregateNotSupported {
            get {
                return ResourceManager.GetString("ExAggregateNotSupported", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aggregate &apos;{0}&apos; is missing auxiliary column..
        /// </summary>
        public static string ExAuxColumnMissing {
            get {
                return ResourceManager.GetString("ExAuxColumnMissing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reading failed at row {0}.
        /// </summary>
        public static string ExReadingFailedAtRow {
            get {
                return ResourceManager.GetString("ExReadingFailedAtRow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - {1} columns retrieved in {2} seconds.
        /// </summary>
        public static string LogColumnsRetrievedAndTime {
            get {
                return ResourceManager.GetString("LogColumnsRetrievedAndTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - connection string: {1}.
        /// </summary>
        public static string LogConnectionString {
            get {
                return ResourceManager.GetString("LogConnectionString", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - failed to get domain for column &apos;{1}&apos;: {2}.
        /// </summary>
        public static string LogDomainQueryFailed {
            get {
                return ResourceManager.GetString("LogDomainQueryFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - executing database column domain query: &apos;{1}&apos;.
        /// </summary>
        public static string LogExecutingDomainQuery {
            get {
                return ResourceManager.GetString("LogExecutingDomainQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - executing database on-demand query: &apos;{1}&apos;.
        /// </summary>
        public static string LogExecutingOnDemandQuery {
            get {
                return ResourceManager.GetString("LogExecutingOnDemandQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - executing database query: &apos;{1}&apos;.
        /// </summary>
        public static string LogExecutingQuery {
            get {
                return ResourceManager.GetString("LogExecutingQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - executing database schema query: &apos;{1}&apos;.
        /// </summary>
        public static string LogExecutingSchemaQuery {
            get {
                return ResourceManager.GetString("LogExecutingSchemaQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - {1} rows, {2} columns retrieved in {3} seconds.
        /// </summary>
        public static string LogQueryResultSizeAndTime {
            get {
                return ResourceManager.GetString("LogQueryResultSizeAndTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - query executed in {1} seconds.
        /// </summary>
        public static string LogQueryResultTime {
            get {
                return ResourceManager.GetString("LogQueryResultTime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Request #{0} - reading from Sybase end of file reached. {0} rows read..
        /// </summary>
        public static string LogSybaseEndOfFile {
            get {
                return ResourceManager.GetString("LogSybaseEndOfFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Datawatch Modeler.
        /// </summary>
        public static string UiMonarchWindowTitle {
            get {
                return ResourceManager.GetString("UiMonarchWindowTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Database.
        /// </summary>
        public static string UiPluginTitle {
            get {
                return ResourceManager.GetString("UiPluginTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Database  Connection.
        /// </summary>
        public static string UiWindowTitle {
            get {
                return ResourceManager.GetString("UiWindowTitle", resourceCulture);
            }
        }
    }
}
