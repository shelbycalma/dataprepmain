﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.DatabasePlugin;
using MonarchPlugin = Panopticon.DatabasePlugin.Monarch.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.DatabasePlugin.dll")]
[assembly: AssemblyDescription("Datawatch Database Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
[assembly: PluginDescription(
    MonarchPlugin.PluginTitle, MonarchPlugin.PluginType, MonarchPlugin.PluginId, MonarchPlugin.PluginIsObsolete)]
