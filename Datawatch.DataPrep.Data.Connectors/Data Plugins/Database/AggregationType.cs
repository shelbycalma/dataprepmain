﻿using System;

namespace Panopticon.DatabasePlugin
{
    // Kept to support backward compatibility only, DatabaseColumn now uses common AggregateType.
    // Old workbooks had it as numeric value,
    // so need it to map to matching AggregateType counterpart.
    [Obsolete("Use Datawatch.DataPrep.Data.Framework.Model.AggregateType instead")]
    public enum AggregationType
    {
        Sum,
        Count,
        Min,
        Max,
        GroupBy
    }
}
