﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.DatabasePlugin.Monarch
{
    public class MonarchConnectionSettings : DatabaseConnectionSettings
    {
        public MonarchConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public MonarchConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            :base( bag, parameters)
        {
        }

        public override bool IsDatabasePlugin
        {
            get { return false; }
        }
    }
}
