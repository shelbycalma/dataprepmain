﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.DatabasePlugin.Monarch
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ConnectionSettings>, IOnDemandPlugin
    {
        internal const string PluginId = "MonarchPlugin";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "Datawatch Modeler";
        internal const string PluginType = DataPluginTypes.File;

        protected bool canSelectColumns = true;

        private readonly Executor executor;

        public Plugin()
        {
            canSelectColumns = true;
            executor = new Executor();
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override ConnectionSettings CreateSettings(PropertyBag bag)
        {
            throw new System.NotImplementedException();
        }

        public override ITable GetAllData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters, -1);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            CheckLicense();

            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(settings, parameters);

            Utils.FixPath(workbookDir, dataDir, connectionSettings);

            return executor.GetData(connectionSettings.QuerySettings,
                parameters, rowcount);
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            return GetData(workbookDir, dataDir, settings, parameters, rowcount);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(settings, null);

            if (connectionSettings.SelectedDatabase.Length > 0)
            {
                return new string[] { connectionSettings.SelectedDatabase };
            }
            return null;
        }

        public ITable GetOnDemandData(string workbookDir, string dataDir,
                    PropertyBag settings, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            CheckLicense();

            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(settings, parameters);

            Utils.FixPath(workbookDir, dataDir, connectionSettings);

            return executor.GetOnDemandData(connectionSettings.QuerySettings,
                parameters, onDemandParameters, rowcount);
        }
        public bool IsOnDemand(PropertyBag settings)
        {
            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(settings, null);
            return connectionSettings != null &&
                connectionSettings.QuerySettings.IsOnDemand;
        }

        public override void UpdateFileLocation(PropertyBag settings,
                   string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();

            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(settings, null);

            if (connectionSettings.SelectedDatabase.Length > 0 &&
                Path.GetFileName(connectionSettings.SelectedDatabase) ==
                Path.GetFileName(oldPath))
            {
                string databasePath = 
                    Datawatch.DataPrep.Data.Core.Utils.ComputeRelativePath(
                    workbookPath, newPath);

                if (connectionSettings.SelectedDatabase != databasePath)
                {
                    connectionSettings.SelectedDatabase = databasePath;
                    //update connection string as well
                    connectionSettings.ConnectionString =
                        Utils.GetMonarchConnectionString(databasePath);
                }                    
            }
        }
    }
}
