﻿namespace Panopticon.DatabasePlugin
{
    public enum JavaConnectionMode
    {
        JndiName,
        Url
    }
}
