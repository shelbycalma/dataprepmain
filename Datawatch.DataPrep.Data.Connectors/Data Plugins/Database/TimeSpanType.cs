﻿namespace Panopticon.DatabasePlugin
{
    public enum TimeSpanType
    {
        DateTime,
        Date,
        Time,
        None
    }
}
