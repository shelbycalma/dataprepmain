﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.DatabasePlugin.Properties;

namespace Panopticon.DatabasePlugin
{
    public class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        public static Exception AuxColumnMissing(FunctionType agg)
        {
            return CreateNotSupported(
                Format(Resources.ExAuxColumnMissing, agg));
        }

        public static Exception BadAggregationType(AggregateType agg)
        {
            return CreateNotSupported(
                Format(Resources.ExAggregateNotSupported, agg));
        }
    }
}
