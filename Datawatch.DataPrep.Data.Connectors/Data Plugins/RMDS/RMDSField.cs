﻿using System;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Table;
using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Data;
using ThomsonReuters.RFA.RDM;

namespace Panopticon.RMDSPlugin
{
    public class RMDSField
    {
        public string Name { get; private set; }
        public string LongName { get; private set; }
        public readonly int FieldId;
        public readonly byte Type;
        public readonly object Value;

        public RMDSField(RDMFidDef fidDef, DataBuffer dataBuffer,
            bool valueRequired)
        {
            if (fidDef == null)
            {
                throw Exceptions.ArgumentNull("fidDef");
            }
            if (dataBuffer == null)
            {
                throw Exceptions.ArgumentNull("dataBuffer");
            }

            Name = fidDef.Name.ToString();
            LongName = fidDef.LongName.ToString();
            FieldId = fidDef.FieldId;
            Type = dataBuffer.DataBufferType;
            if (valueRequired)
            {
                Value = DecodeData(dataBuffer, fidDef);
            }
        }

        public RMDSField(string name, string longName, int fieldId, byte type)
        {
            Name = name;
            LongName = longName;
            FieldId = fieldId;
            Type = type;
        }

        private static object DecodeData(DataBuffer dataBuffer, RDMFidDef fiddef)
        {
            object fieldValue = null;
                        
            switch (dataBuffer.DataBufferType)
            {
                case DataBuffer.DataBufferEnum.Enumeration:
                    if (dataBuffer.IsBlank)
                    {
                        return null;
                    }
                    try
                    {
                        RMTESConverter rmtesConverter = new RMTESConverter();
                        ushort enumValue = dataBuffer.Enumeration;
                        using (RFA_String enumString = 
                            fiddef.DisplayValueFor(enumValue))
                        {
                            ThomsonReuters.RFA.Common.Buffer buffer =
                                new ThomsonReuters.RFA.Common.Buffer(
                                    enumString.Data());
                            rmtesConverter.SetBuffer(buffer);
                        }
                        fieldValue = rmtesConverter.ToString();
                        return fieldValue;
                    }
                    catch (System.Exception)
                    {
                        return null;
                    }
                case DataBuffer.DataBufferEnum.Float:
                    if (dataBuffer.IsBlank)
                    {
                        return NumericValue.Empty;
                    }
                    fieldValue = Convert.ToDouble(dataBuffer.Float);
                    break;
                case DataBuffer.DataBufferEnum.Double:
                    if (dataBuffer.IsBlank)
                    {
                        return NumericValue.Empty;
                    }
                    fieldValue = dataBuffer.Double;
                    break;
                case DataBuffer.DataBufferEnum.Int:
                    if (dataBuffer.IsBlank)
                    {
                        return NumericValue.Empty;
                    }
                    fieldValue = Convert.ToDouble(dataBuffer.Int);
                    break;
                case DataBuffer.DataBufferEnum.UInt:
                    if (dataBuffer.IsBlank)
                    {
                        return NumericValue.Empty;
                    }
                    fieldValue = Convert.ToDouble(dataBuffer.UInt);
                    break;
                case DataBuffer.DataBufferEnum.StringAscii:
                case DataBuffer.DataBufferEnum.StringUTF8:
                case DataBuffer.DataBufferEnum.StringRMTES:
                    if (dataBuffer.IsBlank)
                    {
                        return null;
                    }
                    fieldValue = dataBuffer.GetAsString().ToString();
                    break;
                case DataBuffer.DataBufferEnum.Time:
                    if (dataBuffer.IsBlank)
                    {
                        return TimeValue.Empty;
                    }
                    ThomsonReuters.RFA.Data.Time time = dataBuffer.Time;

                    TimeSpan t = new TimeSpan(0, time.Hour, time.Minute,
                        time.Second, time.Millisecond);
                    fieldValue = System.DateTime.Today.AddTicks(t.Ticks);
                    break;
                case DataBuffer.DataBufferEnum.Date:
                    if (dataBuffer.IsBlank)
                    {
                        return TimeValue.Empty;
                    }                    
                    Date date = dataBuffer.Date;
                    if (date.Year == 0 || date.Month == 0 || date.Day == 0)
                    {
                        return TimeValue.Empty;
                    }
                    fieldValue =
                        new System.DateTime(date.Year, date.Month, date.Day);
                    break;
                case DataBuffer.DataBufferEnum.DateTime:
                    if (dataBuffer.IsBlank)
                    {
                        return TimeValue.Empty;
                    }
                    ThomsonReuters.RFA.Data.DateTime rfaDateTime =
                        dataBuffer.DateTime;
                    if (rfaDateTime.Year == 0 || rfaDateTime.Month == 0 ||
                        rfaDateTime.Day == 0)
                    {
                        return TimeValue.Empty;
                    }
                    fieldValue =
                        new System.DateTime(rfaDateTime.Year,
                            rfaDateTime.Month,
                            rfaDateTime.Day,
                            rfaDateTime.Hour,
                            rfaDateTime.Minute,
                            rfaDateTime.Second, rfaDateTime.Millisecond);
                    break;
                case DataBuffer.DataBufferEnum.Real:
                    if (dataBuffer.IsBlank)
                    {
                        return NumericValue.Empty;
                    }
                    fieldValue = DecodeReal(dataBuffer.Real);
                    break;
                case DataBuffer.DataBufferEnum.UnknownDataBuffer:
                default:
                    break;
            }

            return fieldValue;
        }

        public static double DecodeReal(Real real)
        {
            int index = real.MagnitudeType;
            double scale = MagnitudeTypeToScale[index];
            double value = real.Value * scale;
            return value;
        }

        private static readonly double[] MagnitudeTypeToScale = new double[] {
            1e-14, // ExponentNeg14 = 0;
            1e-13, // ExponentNeg13 = 1;
            1e-12, // ExponentNeg12 = 2;
            1e-11, // ExponentNeg11 = 3;
            1e-10, // ExponentNeg10 = 4;
            1e-9, // ExponentNeg9 = 5;
            1e-8, // ExponentNeg8 = 6;
            1e-7, // ExponentNeg7 = 7;
            1e-6, // ExponentNeg6 = 8;
            1e-5, // ExponentNeg5 = 9;
            1e-4, // ExponentNeg4 = 10;
            1e-3, // ExponentNeg3 = 11;
            1e-2, // ExponentNeg2 = 12;
            1e-1, // ExponentNeg1 = 13;
            1e0, // Exponent0 = 14;
            1e1, // ExponentPos1 = 15;
            1e2, // ExponentPos2 = 16;
            1e3, // ExponentPos3 = 17;
            1e4, // ExponentPos4 = 18;
            1e5, // ExponentPos5 = 19;
            1e6, // ExponentPos6 = 20;
            1e7, // ExponentPos7 = 21;
            1.0 / 1.0, // Divisor1 = 22;
            1.0 / 2.0, // Divisor2 = 23;
            1.0 / 4.0, // Divisor4 = 24;
            1.0 / 8.0, // Divisor8 = 25;
            1.0 / 16.0, // Divisor16 = 26;
            1.0 / 32.0, // Divisor32 = 27;
            1.0 / 64.0, // Divisor64 = 28;
            1.0 / 128.0, // Divisor128 = 29;
            1.0 / 256.0 // Divisor256 = 30;
        };

        public Column GetStandaloneColumn()
        {
            switch (Type)
            {
                case DataBuffer.DataBufferEnum.StringAscii:
                case DataBuffer.DataBufferEnum.StringUTF8:
                case DataBuffer.DataBufferEnum.StringRMTES:
                case DataBuffer.DataBufferEnum.ANSI_Page:
                case DataBuffer.DataBufferEnum.Buffer:
                case DataBuffer.DataBufferEnum.Enumeration:
                case DataBuffer.DataBufferEnum.RespStatus:
                case DataBuffer.DataBufferEnum.XML:
                    TextColumn textColumn = new TextColumn(Name);
                    ((IMutableColumnMetaData)textColumn.MetaData).Title = 
                        LongName;
                    return textColumn;
                case DataBuffer.DataBufferEnum.Date:
                case DataBuffer.DataBufferEnum.DateTime:
                case DataBuffer.DataBufferEnum.Time:
                    TimeColumn timeColumn = new TimeColumn(Name);
                    ((IMutableColumnMetaData)timeColumn.MetaData).Title = 
                        LongName;
                    return timeColumn;
                case DataBuffer.DataBufferEnum.Int:
                case DataBuffer.DataBufferEnum.Double:
                case DataBuffer.DataBufferEnum.Float:
                case DataBuffer.DataBufferEnum.Real:
                case DataBuffer.DataBufferEnum.UInt:
                    NumericColumn numericColumn = new NumericColumn(Name);
                    ((IMutableColumnMetaData)numericColumn.MetaData).Title = 
                        LongName;
                    return numericColumn;
                default:
                    throw Exceptions.UnknownDataBufferType(Type);
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RMDSField)) return false;
            RMDSField field = (RMDSField)obj;

            if (this.Name != field.Name ||
                this.LongName != field.LongName ||
                this.FieldId != field.FieldId ||
                this.Type != field.Type ||
                this.Value != field.Value)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    Name, LongName, FieldId, Type, Value
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }
    }
}
