﻿using System;
using System.Collections.Generic;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

using Panopticon.RMDSPlugin.Properties;

namespace Panopticon.RMDSPlugin
{
    public class RMDSDataAdapter :
        RealtimeDataAdapter<ParameterTable, RMDSSettings, RMDSRecord>,
        ICombineEvents<RMDSRecord>
    {
        private RMDSConnection connection;
        private Thread thread;
        private string[] subscribedSymbols;

        public override void Start()
        {
            subscribedSymbols = Util.ApplyParametersToSymbol(settings.Symbol,
                table.Parameters);
            string userName = Util.ApplyParameters(settings.UserName,
                table.Parameters);
            
            connection = new RMDSConnection(settings, subscribedSymbols,
                userName, table.Parameters);

            thread = new Thread(StartSubscription);
            thread.Name = "RMDS subscription reader";
            thread.Start();
        }

        private void StartSubscription()
        {
            try
            {
                string symbol = String.Join(",", subscribedSymbols);
                string service = Util.ApplyParameters(settings.Service,
                table.Parameters);
                Log.Info(Resources.LogStartSubscription,
                    service, symbol);
                connection.StartRealTime(this);
            }
            catch (Exception exc)
            {
                Log.Exception(exc);
            }
        }

        public override void Stop()
        {
            string symbol = String.Join(",", subscribedSymbols);
            string service = Util.ApplyParameters(settings.Service,
                table.Parameters);
            Log.Info(Resources.LogStopSubscription,
                    service, symbol);
            thread = null;
        }

        internal bool IsRunning
        {
            get { return thread != null; }
        }

        internal void ReadRecord(string recordName, List<RMDSField> fieldList)
        {
            RMDSRecord record = new RMDSRecord(recordName);

            foreach (RMDSField field in fieldList)
            {
                record.Values[field.Name] = field.Value;
            }

            updater.EnqueueUpdate(record.Name, record);
        }

        public override void UpdateColumns(Row row, RMDSRecord evt)
        {
            for (int c = 0; c < table.ColumnCount; c++)
            {
                Column column = table.GetColumn(c);
                string columnName = column.Name;

                // TableUpdater already assigned a value to the id column
                if (columnName == settings.IdColumnName) continue;
                
                // Reuters didn't send any data for this column
                if (!evt.Values.ContainsKey(columnName)) continue;

                object newValue = evt.Values[columnName];

                if (column is TextColumn)
                {
                    TextColumn textColumn = (TextColumn) column; 
                    string textValue = (string) newValue;
                    textColumn.SetTextValue(row, textValue);
                }
                else if (column is NumericColumn)
                {
                    NumericColumn numericColumn = (NumericColumn) column;
                    double numericValue = (double) newValue;
                    numericColumn.SetNumericValue(row, numericValue);
                }
                else if (column is TimeColumn)
                {
                    TimeColumn timeColumn = (TimeColumn) column;
                    DateTime timeValue = (DateTime) newValue;
                    timeColumn.SetTimeValue(row, timeValue);
                }
            }
        }

        public override DateTime GetTimestamp(RMDSRecord row)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0 ||
                !row.Values.ContainsKey(settings.TimeIdColumn))
            {
                return TimeValue.Empty;
            }

            DateTime value = (DateTime) row.Values[settings.TimeIdColumn];
            return value;
        }

        // Might not be true for all model types, but needed for market price.
        public bool DoCombineEvents
        {
            get { return true; }
        }

        public RMDSRecord Combine(RMDSRecord event1, RMDSRecord event2)
        {
            foreach (KeyValuePair<string, object> keyValuePair in event2.Values)
            {
                event1.Values[keyValuePair.Key] = keyValuePair.Value;
            }
            return event1;
        }
    }
}
