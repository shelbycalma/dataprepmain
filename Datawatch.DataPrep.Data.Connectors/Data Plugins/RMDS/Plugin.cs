﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

using Panopticon.RMDSPlugin.Properties;

using ThomsonReuters.RFA.Common;

namespace Panopticon.RMDSPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin<ParameterTable, RMDSSettings,
        RMDSRecord, RMDSDataAdapter>
    {
        internal const string PluginId = "RMDSPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Thomson Reuters TREP-RT";
        internal const string PluginType = DataPluginTypes.Streaming;

        private readonly ImageSource connectImage;
        private bool disposed;

        public Plugin()
        {
            Context.Initialize();
            Log.Info(Resources.LogRFAVersion,
                PluginTitle, Context.RFAVersionInfo.ProductVersion.ToString());
            Context.Uninitialize();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

        public override RMDSSettings CreateSettings(PropertyBag bag)
        {
            RMDSSettings rmdsSettings = new RMDSSettings(bag);
            if (string.IsNullOrEmpty(rmdsSettings.Title))
            {
                rmdsSettings.Title = rmdsSettings.Service + rmdsSettings.Symbol;
            }
            return rmdsSettings;
        }

        protected override ITable CreateTable(RMDSSettings rmdsSettings,
             IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            CheckLicense();

            DateTime start = DateTime.Now;
            ParameterTable table = new ParameterTable(parameters);
            table.BeginUpdate();

            table.AddColumn(new TextColumn(rmdsSettings.IdColumnName));

            RMDSField[] selectedColumns = rmdsSettings.SelectedColumns;
            for (int i = 0; i < selectedColumns.Length; i++)
            {
                RMDSField field = selectedColumns[i];
                table.AddColumn(field.GetStandaloneColumn());
            }

            table.EndUpdate();
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
