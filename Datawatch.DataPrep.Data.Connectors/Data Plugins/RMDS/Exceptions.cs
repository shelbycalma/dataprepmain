﻿using System;
using System.IO;

using Panopticon.RMDSPlugin.Properties;

namespace Panopticon.RMDSPlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        protected static FileNotFoundException CreateFileNotFound(
            string message, string file)
        {
            return (FileNotFoundException) LogException(
                new FileNotFoundException(message, file));
        }

        public static Exception EmptySchema()
        {
            return CreateInvalidOperation(Resources.ExEmptySchema);
        }

        public static Exception LoginFailed(string username)
        {
            string message = Format(Resources.ExLoginFailed, username);
            return CreateInvalidOperation(message);
        }

        public static Exception MissingDictionaryFile(string file)
        {
            string message = Format(Resources.ExMissingDictionaryFile, file);
            return CreateFileNotFound(message, file);
        }

        public static Exception NetworkDictionaryNotLoaded()
        {
            return CreateInvalidOperation(
                Resources.ExNetworkDictionaryNotLoaded);
        }

        public static Exception UnknownDataBufferType(byte dataBufferType)
        {
            string message = Format(Resources.ExUnknownDataBufferType,
                dataBufferType);
            return CreateNotSupported(message);
        }

        public static Exception UnknownMessageModelType(
            MessageModelType messageModelType)
        {
            string message = Format(Resources.ExUnknownMessageModelType,
                messageModelType);
            return CreateNotSupported(message);
        }
    }
}
