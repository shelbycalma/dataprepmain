﻿using System;
using System.Collections.Generic;

namespace Panopticon.RMDSPlugin
{
    public class RMDSRecord
    {
        public Dictionary<string, Object> Values { get; private set; }
        public string Name { get; private set; }
        
        public RMDSRecord(string name)
        {
            Values = new Dictionary<string, Object>();
            this.Name = name;
        }
    }
}
