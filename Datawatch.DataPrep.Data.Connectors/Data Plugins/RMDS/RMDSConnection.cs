﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.RMDSPlugin.Properties;

using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Config;
using ThomsonReuters.RFA.RDM;
using ThomsonReuters.RFA.SessionLayer;

namespace Panopticon.RMDSPlugin
{
    internal class RMDSConnection
    {
        private ConfigDatabase configDb = null;
        private EventQueue eventQueue = null;
        private OMMConsumer ommConsumer = null;
        private long loginHandle = 0;
        private Session session = null;
        private RDMFieldDictionary rdmFieldDict;

        private readonly RMDSSettings settings;
        private readonly string[] subscribedSymbols;
        private readonly string userName;

        private IMessageModelManager realtimeManager;
        private readonly IEnumerable<ParameterValue> parameters;
        private readonly string service;

        public RMDSConnection(RMDSSettings settings, string[] subscribedSymbols,
            string userName, IEnumerable<ParameterValue> parameters)
        {
            if (settings == null)
            {
                throw Exceptions.ArgumentNull("settings");
            }
            if (subscribedSymbols == null)
            {
                throw Exceptions.ArgumentNull("subscribedSymbols");
            }
            this.settings = settings;
            this.subscribedSymbols = subscribedSymbols;
            this.userName = Util.ApplyParameters(userName, parameters);
            this.service = Util.ApplyParameters(settings.Service, parameters);
            this.parameters = parameters;
        }

        private void CleanUp()
        {
            try
            {
                if (eventQueue != null)
                {
                    eventQueue.Deactivate();
                }
                if (ommConsumer != null)
                {
                    //un register all clients, except login
                    ommConsumer.UnregisterClient();
                    //un register login client.
                    if (loginHandle != 0)
                    {
                        ommConsumer.UnregisterClient(loginHandle);
                    }
                    
                    ommConsumer.Destroy();
                }
                if (session != null)
                {
                    session.Release();
                }

                if (eventQueue != null)
                {
                    eventQueue.Destroy();
                    eventQueue = null;
                }

                if (configDb != null)
                {
                    configDb.Release();
                }
                
                if (rdmFieldDict != null)
                {
                    rdmFieldDict.Destroy();
                }
                
                if ((!Context.Uninitialize()) && (Context.InitializedCount == 0))
                {
                    Log.Warning(Resources.LogUninitializeFailed);
                }
            }
            catch (System.Exception ex)
            {
                Log.Exception(ex);
            }
        }

        private static IMessageModelManager GetMessageModelManager(
            OMMConsumer consumer, EventQueue eventQueue, RMDSSettings settings,
            RDMFieldDictionary rdmFieldDict,
            string[] subscribedSymbols, RMDSField[] selectedFields, string service)
        {
            if (settings.MessageModelType == MessageModelType.MarketPrice)
            {
                return new MarketPriceManager(
                    consumer, eventQueue, service, rdmFieldDict,
                    subscribedSymbols, selectedFields, settings.ProcessLinks);
            }

            throw Exceptions.UnknownMessageModelType(settings.MessageModelType);
        }

        public List<RMDSField> GetSchema()
        {
            try
            {
                Initialize();

                loginHandle = LoginManager.Login(ommConsumer, eventQueue,
                    userName, settings.MaxEventDispatchSeconds);

                LoadDict();
                IMessageModelManager schemaManager = GetMessageModelManager(
                    ommConsumer, eventQueue, settings, rdmFieldDict,
                    subscribedSymbols, null, service);

                List<RMDSField> schema = schemaManager.GetSchema(
                    settings.MaxEventDispatchSeconds);
                return schema;
            }
            finally
            {
                CleanUp();
            }
        }
        
        private void Initialize()
        {
            Context.Initialize();

            string id = Guid.NewGuid().ToString();
            InitConfigDB(id);

            eventQueue = EventQueue.Create(new RFA_String(id));

            session = Session.Acquire(new RFA_String(id));

            ommConsumer = session.CreateOMMConsumer(new RFA_String(id));            
        }

        private void InitConfigDB(string id)
        {
            StagingConfigDatabase stgConfigDb = StagingConfigDatabase.Create();
            //TODO, only RSSL connection is supported?
            stgConfigDb.SetString(
                new RFA_String(@"\Connections\Connection_RSSL\connectionType"),
                new RFA_String("RSSL"));

            string host = Util.ApplyParameters(settings.Host, parameters);
            stgConfigDb.SetString(
                new RFA_String(@"\Connections\Connection_RSSL\hostName"),
                new RFA_String(host));

            string port = Util.ApplyParameters(settings.Port, parameters);
            stgConfigDb.SetString(
                new RFA_String(@"\Connections\Connection_RSSL\rsslPort"),
                new RFA_String(port));
            
            stgConfigDb.SetBool(
                new RFA_String(@"\Logger\AppLogger\fileLoggerEnabled"),
                false);

            //WindowsLogger needs admin permissions, so it should be disabled.
            stgConfigDb.SetBool(
                new RFA_String(@"\Logger\AppLogger\windowsLoggerEnabled"),
                false);
            
            stgConfigDb.SetString(
                new RFA_String(@"\Sessions\" + id + @"\connectionList"),
                new RFA_String("Connection_RSSL"));

            // Based on RFA dev team and EnterprisePlatform.APISupport@thomsonreuters.com
            // We should Set threadModel= Single and Use Horizontal scaling mode.

            stgConfigDb.SetString(
               new RFA_String(@"\Sessions\" + id + @"\threadModel"),
               new RFA_String("Single"));

            stgConfigDb.SetBool(
                            new RFA_String(@"\Adapters\RSSL_Cons_Adapter\singleton"),
                            false);

            configDb = ConfigDatabase.Acquire(new RFA_String("RFA"));
            configDb.Merge(stgConfigDb);

            stgConfigDb.Destroy();
        }

        private void LoadDict()
        {
            rdmFieldDict = RDMFieldDictionary.Create();

            if (settings.DictLocationType == DictionaryLocationType.File)
            {
                DictionaryManager.LoadFromFile(rdmFieldDict);
            }
            else
            {
                DictionaryManager.LoadFromNetwork(
                    rdmFieldDict, ommConsumer, eventQueue, service,
                    settings.MaxEventDispatchSeconds);
            }            
        }

        public void StartRealTime(RMDSDataAdapter dataAdapter)
        {
            try
            {
                Initialize();

                loginHandle = LoginManager.Login(ommConsumer, eventQueue,
                    userName, settings.MaxEventDispatchSeconds);

                LoadDict();
                realtimeManager = GetMessageModelManager(
                    ommConsumer, eventQueue, settings, rdmFieldDict,
                    subscribedSymbols, settings.SelectedColumns, service);

                realtimeManager.StartRealtime(dataAdapter); // Blocking call
            }
            finally
            {
                CleanUp();
            }
        }
    }
}
