﻿using System;

using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Message;
using ThomsonReuters.RFA.RDM;
using ThomsonReuters.RFA.SessionLayer;

namespace Panopticon.RMDSPlugin
{
    internal class LoginManager
    {
        private static ReqMsg CreateRequestMessage(string userName)
        {
            ReqMsg reqMsg = new ReqMsg();
            reqMsg.MsgModelType = RDM.MESSAGE_MODEL_TYPES.MMT_LOGIN;
            reqMsg.InteractionType =
                ReqMsg.InteractionTypeFlag.InitialImage |
                ReqMsg.InteractionTypeFlag.InterestAfterRefresh;
            AttribInfo attribInfo = new AttribInfo();
            attribInfo.NameType =
                ThomsonReuters.RFA.RDM.Login.USER_ID_TYPES.USER_NAME;
            attribInfo.Name = new RFA_String(userName);
            reqMsg.AttribInfo = attribInfo;

            return reqMsg;
        }

        public static long Login(OMMConsumer consumer,
            EventQueue eventQueue, string userName, int timeoutSeconds)
        {
            OMMItemIntSpec ommItemIntSpec = new OMMItemIntSpec();
            ommItemIntSpec.Msg = CreateRequestMessage(userName);

            LoginClient loginClient = new LoginClient();

            long handle = consumer.RegisterClient(eventQueue,
                ommItemIntSpec, loginClient);

            DateTime currentTime = DateTime.Now;
            DateTime endTime =
                currentTime.AddSeconds(timeoutSeconds);
            while (!loginClient.Completed && currentTime < endTime)
            {
                currentTime = DateTime.Now;

                int dispatchReturn = eventQueue.Dispatch(200);
                if ((dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedInActive) ||
                    (dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedNoActiveEventStreams))
                    break;
            }
            if (!loginClient.LoggedIn)
            {
                throw Exceptions.LoginFailed(userName);
            }

            return handle;
        }

        private class LoginClient : Client
        {
            public volatile bool Completed = false;
            public bool LoggedIn = false;

            public void ProcessEvent(Event evt)
            {
                RespMsg respMsg = Util.GetRespMsg(evt,
                    RDM.MESSAGE_MODEL_TYPES.MMT_LOGIN);

                if (respMsg == null)
                {
                    return;
                }

                RespStatus status = respMsg.RespStatus;

                switch (respMsg.RespType)
                {
                    case RespMsg.RespTypeEnum.Refresh:
                        if ((respMsg.HintMask & RespMsg.HintMaskFlag.RespStatus) == 0)
                        {
                            return;
                        }
                        if (status.StreamState == RespStatus.StreamStateEnum.Open &&
                            status.DataState == RespStatus.DataStateEnum.Ok)
                        {
                            LoggedIn = true;
                            Completed = true;
                        }
                        else if (status.StreamState == RespStatus.StreamStateEnum.Open)
                        {
                            // Login Pending
                        }
                        else
                        {
                            LoggedIn = false;
                            Completed = true;
                        }
                        break;
                    case RespMsg.RespTypeEnum.Status:
                        if ((respMsg.HintMask & RespMsg.HintMaskFlag.RespStatus) == 0)
                        {
                            return;
                        }
                        if (status.StreamState == RespStatus.StreamStateEnum.Open &&
                            status.DataState == RespStatus.DataStateEnum.Ok)
                        {
                            LoggedIn = true;
                            Completed = true;
                        }
                        else if (status.StreamState == RespStatus.StreamStateEnum.Open)
                        {
                            // Login Pending
                        }
                        else
                        {
                            LoggedIn = false;
                            Completed = true;
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
