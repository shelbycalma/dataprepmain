﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.RMDSPlugin.Properties;

using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Data;
using ThomsonReuters.RFA.Message;
using ThomsonReuters.RFA.RDM;
using ThomsonReuters.RFA.SessionLayer;

namespace Panopticon.RMDSPlugin
{
    internal class MarketPriceManager : Client, IMessageModelManager
    {
        private readonly OMMConsumer consumer;
        private readonly EventQueue eventQueue;
        private readonly string[] initialSymbols;
        private readonly string serviceName;
        private readonly RDMFieldDictionary rdmFieldDict;
        private readonly RMDSField[] selectedFields;
        private readonly bool processLinks;
        private readonly HashSet<string> subscribedSymbols = 
            new HashSet<string>();

        // Only needed if realtime, not for schema.
        private RMDSDataAdapter dataAdapter;

        private readonly List<int> specialFids = new List<int>();
        private RequestTypes requestType;

        private readonly List<RMDSField> schema = new List<RMDSField>();
        private readonly Dictionary<long, string> itemHandles =
            new Dictionary<long, string>();
        private int marketDataMessageCount;
        private int marketDataRequestCount;

        public MarketPriceManager(OMMConsumer consumer,
            EventQueue eventQueue, string serviceName,
            RDMFieldDictionary rdmFieldDict,
            string[] initialSymbols, RMDSField[] selectedFields,
            bool processLinks)
        {
            this.consumer = consumer;
            this.eventQueue = eventQueue;
            this.serviceName = serviceName;
            this.rdmFieldDict = rdmFieldDict;
            this.initialSymbols = initialSymbols;
            this.selectedFields = selectedFields;
            this.processLinks = processLinks;

            //create a list of specialFids to add them into the viewdata.
            specialFids.Add(Util.REF_COUNT);
            specialFids.AddRange(Util.LinkFid);
            specialFids.AddRange(Util.LongFid);
            specialFids.Add(Util.NEXT_LR);
            specialFids.Add(Util.LONGNEXTLR);
            specialFids.Add(Util.PREF_LINK);
            specialFids.Add(Util.LONGLINK);
            specialFids.Add(Util.LINK);
        }

        private ReqMsg CreateRequestMessage(string[] symbols)
        {
            ReqMsg reqMsg = new ReqMsg();
            AttribInfo attribInfo = new AttribInfo();

            reqMsg.MsgModelType = RDM.MESSAGE_MODEL_TYPES.MMT_MARKET_PRICE;
            if (requestType == RequestTypes.Schema)
            {
                reqMsg.InteractionType = ReqMsg.InteractionTypeFlag.InitialImage;
            }
            else
            {
                reqMsg.InteractionType =
                    ReqMsg.InteractionTypeFlag.InitialImage |
                    ReqMsg.InteractionTypeFlag.InterestAfterRefresh;
            }

            attribInfo.ServiceName = new RFA_String(serviceName);
            reqMsg.AttribInfo = attribInfo;

            if (symbols.Length == 1)
            {
                attribInfo.Name = new RFA_String(symbols[0]);
            }
            else
            {
                reqMsg.Payload = GetRequestPayLoad(symbols, reqMsg);
            }

            return reqMsg;
        }

        private ElementList GetRequestPayLoad(string[] symbols, ReqMsg reqMsg)
        {
            //***Encode the Batch request from recordNameList
            reqMsg.IndicationMask = ReqMsg.IndicationMaskFlag.Batch;
            ElementList elementList = new ElementList();
            ElementListWriteIterator elwiter = new ElementListWriteIterator();
            elwiter.Start(elementList);

            ArrayWriteIterator arrWIt = new ArrayWriteIterator();
            ThomsonReuters.RFA.Data.Array elementData =
                new ThomsonReuters.RFA.Data.Array();
            arrWIt.Start(elementData);
            DataBuffer dataBuffer = new DataBuffer();
            ArrayEntry arrayEntry = new ArrayEntry();

            for (int i = 0; i < symbols.Length; i++)
            {
                dataBuffer.Clear();
                arrayEntry.Clear();
                dataBuffer.SetFromString(new RFA_String(symbols[i]),
                    DataBuffer.DataBufferEnum.StringAscii);
                arrayEntry.Data = dataBuffer;
                arrWIt.Bind(arrayEntry);
            }
            arrWIt.Complete();

            ElementEntry element = new ElementEntry();
            element.Name = RDM.REQUEST_MSG_PAYLOAD_ELEMENT_NAME.ENAME_BATCH_ITEM_LIST;
            element.Data = elementData;
            elwiter.Bind(element);

            //***Encode the ViewData from selectedFields to save bandwidth.
            //Pull all fields on snapshot request
            if ((selectedFields != null) && (selectedFields.Length > 0) &&
                requestType == RequestTypes.Realtime)
            {
                reqMsg.IndicationMask = (byte)(reqMsg.IndicationMask |
                    ReqMsg.IndicationMaskFlag.View);
                element.Clear();
                element.Name = RDM.REQUEST_MSG_PAYLOAD_ELEMENT_NAME.ENAME_VIEW_TYPE;

                dataBuffer.Clear();
                dataBuffer.SetUInt(RDM.VIEW_TYPES.VT_FIELD_ID_LIST,
                    DataBuffer.DataBufferEnum.UInt);
                element.Data = dataBuffer;
                elwiter.Bind(element);

                elementData.Clear();
                ArrayWriteIterator arrWriterIterator = new ArrayWriteIterator();
                arrWriterIterator.Start(elementData);

                for (int i = 0; i < selectedFields.Length; i++)
                {
                    try
                    {
                        dataBuffer.Clear();
                        arrayEntry.Clear();
                        dataBuffer.Int = selectedFields[i].FieldId;
                        arrayEntry.Data = dataBuffer;
                        arrWriterIterator.Bind(arrayEntry);
                    }
                    catch (System.Exception)
                    {
                        // Ignored
                    }
                }

                //append all special fields, wouldn't hurt if they get duplicate
                //or acutual stream doesn't contain these fields, RFA would such cases.

                foreach (int fid in specialFids)
                {
                    dataBuffer.Clear();
                    arrayEntry.Clear();
                    dataBuffer.Int = fid;
                    arrayEntry.Data = dataBuffer;
                    arrWriterIterator.Bind(arrayEntry);
                }

                arrWriterIterator.Complete();

                element.Clear();
                element.Name = RDM.REQUEST_MSG_PAYLOAD_ELEMENT_NAME.ENAME_VIEW_DATA;
                element.Data = elementData;
                elwiter.Bind(element);
            }
            //***End encode ViewData

            elwiter.Complete();

            return elementList;
        }

        private string GetRICName(RespMsg respMsg, long handle)
        {
            string ricName = null;
            if (((respMsg.RespType == RespMsg.RespTypeEnum.Refresh) ||
                (respMsg.RespType == RespMsg.RespTypeEnum.Status)) &&
                ((respMsg.HintMask & RespMsg.HintMaskFlag.AttribInfo) != 0))
            {
                AttribInfo attribInfo = respMsg.AttribInfo;
                ricName = attribInfo.Name.ToString();
                if (!string.IsNullOrEmpty(ricName))
                {
                    itemHandles[handle] = ricName;
                }
                else
                {
                    if (itemHandles.ContainsKey(handle))
                    {
                        ricName = itemHandles[handle];
                    }
                }
            }
            else
            {
                if (itemHandles.ContainsKey(handle))
                {
                    ricName = itemHandles[handle];
                }
            }

            return ricName;
        }

        public List<RMDSField> GetSchema(int timeoutSeconds)
        {
            Log.Info(Resources.LogGetSchemaStart);
            System.DateTime start = System.DateTime.Now;

            requestType = RequestTypes.Schema;

            SendMarketDataRequest(initialSymbols);

            System.DateTime currentTime = System.DateTime.Now;
            System.DateTime endTime =
                currentTime.AddSeconds(timeoutSeconds);

            while (!(marketDataRequestCount > 0 &&
                     marketDataMessageCount >= marketDataRequestCount) &&
                currentTime < endTime)
            {
                currentTime = System.DateTime.Now;

                int dispatchReturn = eventQueue.Dispatch(200);
                if ((dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedInActive) ||
                    (dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedNoActiveEventStreams))
                    break;
            }

            System.DateTime end = System.DateTime.Now;
            double seconds = (end - start).TotalSeconds;

            Log.Info(Resources.LogGetSchemaEnd, marketDataRequestCount,
               schema.Count, seconds, marketDataMessageCount);

            return schema;
        }

        public void StartRealtime(RMDSDataAdapter dataAdapter)
        {
            if (dataAdapter == null)
            {
                throw Exceptions.ArgumentNull("dataAdapter");
            }
            
            this.dataAdapter = dataAdapter;
            requestType = RequestTypes.Realtime;

            SendMarketDataRequest(initialSymbols);

            while (this.dataAdapter.IsRunning)
            {
                int dispatchReturn = eventQueue.Dispatch(200);
                if ((dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedInActive) ||
                    (dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedNoActiveEventStreams))
                    break;
            }
        }

        public void ProcessEvent(Event evt)
        {
            RespMsg respMsg = Util.GetRespMsg(evt,
                RDM.MESSAGE_MODEL_TYPES.MMT_MARKET_PRICE);
                
            if (respMsg == null)
            {
                return;
            }

            long handle = evt.Handle;

            string ricName = GetRICName(respMsg, handle);

            if (string.IsNullOrEmpty(ricName))
            {
                return;
            }

            if (respMsg.Payload.DataType != DataEnum.FieldList)
            {
                return;
            }
            
            FieldList fieldList = (FieldList) respMsg.Payload;

            switch (respMsg.RespType)
            {
                case RespMsg.RespTypeEnum.Refresh:
                case RespMsg.RespTypeEnum.Update:
                    OnMarketDataReceived(ricName, fieldList);
                    break;
                case RespMsg.RespTypeEnum.Status:
                    if ((respMsg.HintMask & RespMsg.HintMaskFlag.RespStatus) == 0)
                    {
                        break;
                    }
                    RespStatus.StreamStateEnum strState =
                        respMsg.RespStatus.StreamState;

                    if (strState == RespStatus.StreamStateEnum.Closed ||
                        strState == RespStatus.StreamStateEnum.ClosedRecover)
                    {
                        //TODO event steam for a symbol is closed,
                        //should we send a delete message to realtime updater?
                        itemHandles.Remove(handle);
                    }
                    break;
            }
        }

        private void OnMarketDataReceived(string ricName, FieldList fieldList)
        {
            if (requestType == RequestTypes.Schema)
            {
                marketDataMessageCount++;
                List<RMDSField> rmdsFieldList = ProcessFieldList(fieldList, false);
                if (rmdsFieldList != null && rmdsFieldList.Count > 0)
                {
                    foreach (RMDSField field in rmdsFieldList)
                    {
                        if (!schema.Contains(field))
                        {
                            schema.Add(field);
                        }
                    }                    
                }
            }
            else
            {
                List<RMDSField> rmdsFieldList = ProcessFieldList(fieldList, true);
                if (rmdsFieldList != null && rmdsFieldList.Count > 0)
                {
                    dataAdapter.ReadRecord(ricName, rmdsFieldList);
                }
            }
        }

        private void SendMarketDataRequest(string[] symbols)
        {
            symbols = RemoveAlreadySubscribedSymbols(symbols);
            if (symbols.Length == 0) return;

            OMMItemIntSpec ommItemIntSpec = new OMMItemIntSpec();
            ommItemIntSpec.Msg = CreateRequestMessage(symbols);

            if (symbols.Length == 1)
            {
                consumer.RegisterClient(eventQueue, ommItemIntSpec,
                this, symbols[0]);
            }
            else
            {
                consumer.RegisterClient(eventQueue, ommItemIntSpec,
                    this);
            }
            subscribedSymbols.UnionWith(symbols);
            marketDataRequestCount += symbols.Length;
        }

        private string[] RemoveAlreadySubscribedSymbols(string[] symbols)
        {
            if (subscribedSymbols.Count == 0) return symbols;

            List<string> list = new List<string>();
            foreach (string symbol in symbols)
            {
                if (subscribedSymbols.Contains(symbol)) continue;
                list.Add(symbol);
            }
            return list.ToArray();
        }

        private void ProcessChainItems(FieldList fieldList, int refCount)
        {
            bool isLINK_A = false;
            bool isLONGLINK = false;
            string NEXTLR = null;
            string LONGNEXTLR = null;
            string PREF_LINK = null;
            List<string> chainRICs = new List<string>();
            string linkRIC = null;

            foreach (FieldEntry fieldEntry in fieldList)
            {
                short fid = fieldEntry.FieldID;

                RDMFidDef fidDef = null;

                try
                {
                    fidDef = rdmFieldDict.GetFidDef(fid);
                }
                catch (System.Exception ex)
                {
                    Log.Info(Properties.Resources.LogOldDict, fid,
                        rdmFieldDict.Version.ToString());
                }
                if (fidDef == null) continue;

                if (refCount > 0 &&
                    (Util.IsValidLongFid(fid, refCount) ||
                    Util.IsValidLinkFid(fid, refCount)))
                {
                    Data dataEntry = fieldEntry.GetData(fidDef.OMMType);
                    if (dataEntry.DataType == DataEnum.DataBuffer)
                    {
                        DataBuffer dataBuffer = (DataBuffer) dataEntry;
                        linkRIC = dataBuffer.GetAsString().ToString().Trim();
                    }

                    if (!string.IsNullOrEmpty(linkRIC))
                    {
                        chainRICs.Add(linkRIC);
                    }
                }

                if (fid == Util.LONGLINK)
                {
                    // if there is FID 813, this chain record has LONGLINK template.
                    isLONGLINK = true;
                }
                else if (fid == Util.LINK)
                {
                    // if there is FID 253, this chain record has LINK_A template.
                    isLINK_A = true;
                }
                else if (fid == Util.NEXT_LR)
                {
                    // store the value of next link for retrieving the subsequence
                    // of the chain in LINK_A template.
                    Data dataEntry = fieldEntry.GetData(fidDef.OMMType);
                    if (dataEntry.DataType == DataEnum.DataBuffer)
                    {
                        DataBuffer dataBuffer = dataEntry as DataBuffer;
                        NEXTLR = dataBuffer.GetAsString().ToString();
                    }
                }
                else if (fid == Util.LONGNEXTLR)
                {
                    // store the value of next link for retrieving the subsequence
                    // of the chain in LONGLINK template.
                    Data dataEntry = fieldEntry.GetData(fidDef.OMMType);
                    if (dataEntry.DataType == DataEnum.DataBuffer)
                    {
                        DataBuffer dataBuffer = (DataBuffer) dataEntry;
                        LONGNEXTLR = dataBuffer.GetAsString().ToString();
                    }
                }
                else if (fid == Util.PREF_LINK)
                {
                    // store the value of next link for retrieving the subsequence
                    // of the chain in PREF_LINK template.
                    Data dataEntry = fieldEntry.GetData(fidDef.OMMType);
                    if (dataEntry.DataType == DataEnum.DataBuffer)
                    {
                        DataBuffer dataBuffer = (DataBuffer) dataEntry;
                        PREF_LINK = dataBuffer.GetAsString().ToString();
                    }
                }
            }

            if (chainRICs.Count > 0)
            {
                SendMarketDataRequest(chainRICs.ToArray());
            }

            if ((isLONGLINK && !(string.IsNullOrEmpty(LONGNEXTLR)))
                && (string.IsNullOrEmpty(PREF_LINK) ||
                LONGNEXTLR.Equals(PREF_LINK, StringComparison.InvariantCultureIgnoreCase)))
            {
                // if this is LONKLINK template, retrieve its subsequence using
                // LONGNEXTLR.
                SendMarketDataRequest(new string[] { LONGNEXTLR });
            }

            if ((isLINK_A && !(string.IsNullOrEmpty(NEXTLR)))
                && (string.IsNullOrEmpty(PREF_LINK) ||
                NEXTLR.Equals(PREF_LINK, StringComparison.InvariantCultureIgnoreCase)))
            {
                // if this is LINK_A template, retrieve its subsequence using
                // NEXTLR.
                SendMarketDataRequest(new string[] { NEXTLR });
            }
        }

        private List<RMDSField> ProcessFieldList(FieldList fieldList,
            bool valueRequired)
        {
            int refCount = 0;
            if (processLinks)
            {
                foreach (FieldEntry fieldEntry in fieldList)
                {
                    short fid = fieldEntry.FieldID;
                    if (fid != Util.REF_COUNT) continue;

                    RDMFidDef fidDef = null;
                    try
                    {
                        fidDef = rdmFieldDict.GetFidDef(
                            fieldEntry.FieldID);
                    }
                    catch (System.Exception ex)
                    {
                        Log.Info(Properties.Resources.LogOldDict, fid,
                            rdmFieldDict.Version.ToString());
                    }
                    if (fidDef == null) continue;
                    Data dataEntry = fieldEntry.GetData(fidDef.OMMType);
                
                    if (dataEntry.DataType != DataEnum.DataBuffer) continue;

                    DataBuffer dataBuffer = (DataBuffer)dataEntry;

                    if (dataBuffer.DataBufferType !=
                        DataBuffer.DataBufferEnum.UInt) continue;

                    // This is a safe cast since the max value is 14
                    refCount = (int)dataBuffer.UInt;
                }
            }

            ProcessChainItems(fieldList, refCount);

            if (processLinks && refCount != 0)
            {
                return null;
            }

            return UpdateFieldList(fieldList, valueRequired);
        }

        private List<RMDSField> UpdateFieldList(FieldList fieldList,
            bool valueRequired)
        {
            List<RMDSField> rmdsFieldList = new List<RMDSField>();
            
            foreach (FieldEntry fieldEntry in fieldList)
            {
                try
                {
                    short fid = fieldEntry.FieldID;
                    RDMFidDef fidDef = null;
                    try
                    {
                        fidDef = rdmFieldDict.GetFidDef(fid);
                    }
                    catch (System.Exception ex)
                    {
                        Log.Info(Properties.Resources.LogOldDict, fid,
                            rdmFieldDict.Version.ToString());
                    }
                    if (fidDef == null) continue;
                    
                    Data dataEntry = fieldEntry.GetData(fidDef.OMMType);
                    if (dataEntry.DataType == DataEnum.DataBuffer)
                    {
                        DataBuffer dataBuffer = (DataBuffer) dataEntry;
                        RMDSField field = new RMDSField(fidDef, dataBuffer,
                            valueRequired);
                        rmdsFieldList.Add(field);
                    }
                }
                catch (InvalidUsageException ie)
                {
                    Log.Exception(ie);
                    break;
                }
            }

            return rmdsFieldList;
        }
    }
}
