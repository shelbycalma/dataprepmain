﻿using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.RMDSPlugin.Properties;

using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Data;
using ThomsonReuters.RFA.Message;
using ThomsonReuters.RFA.RDM;
using ThomsonReuters.RFA.SessionLayer;

namespace Panopticon.RMDSPlugin
{
    internal class DictionaryManager
    {
        private const string FieldDictionaryName = "RWFFld";
        private const string EnumTypeDefName = "RWFEnum";

        private static ReqMsg CreateRequestMessage(string serviceName,
            string dictionaryName)
        {
            ReqMsg reqMsg = new ReqMsg();
            reqMsg.MsgModelType = RDM.MESSAGE_MODEL_TYPES.MMT_DICTIONARY;
            reqMsg.InteractionType =
                ReqMsg.InteractionTypeFlag.InitialImage |
                ReqMsg.InteractionTypeFlag.InterestAfterRefresh;

            AttribInfo attribInfo = new AttribInfo();
            attribInfo.DataMask =
                Dictionary.DICTIONARY_VERBOSITY_VALUES.DICTIONARY_NORMAL;
            attribInfo.ServiceName = new RFA_String(serviceName);

            attribInfo.Name = new RFA_String(dictionaryName);
            reqMsg.AttribInfo = attribInfo;

            return reqMsg;
        }

        public static void LoadFromFile(RDMFieldDictionary rdmFieldDict)
        {

            string assemblyPath = Util.GetAssemblyPath();
            string fieldDictPath = Path.Combine(assemblyPath,
                "RDMFieldDictionary");
            if (!File.Exists(fieldDictPath))
            {
                Log.Error(Resources.LogMissingDictionaryFile, fieldDictPath);
                throw Exceptions.MissingDictionaryFile(fieldDictPath);
            }

            string enumPath = Path.Combine(assemblyPath, "enumtype.def");

            if (!File.Exists(enumPath))
            {
                Log.Error(Resources.LogMissingDictionaryFile, enumPath);
                throw Exceptions.MissingDictionaryFile(enumPath);
            }

            rdmFieldDict.ReadRDMFieldDictionary(new RFA_String(fieldDictPath));
            rdmFieldDict.ReadRDMEnumTypeDef(new RFA_String(enumPath));
        }

        public static void LoadFromNetwork(RDMFieldDictionary rdmFieldDict,
            OMMConsumer consumer, EventQueue eventQueue, string serviceName,
            int timeoutSeconds)
        {            
            OMMItemIntSpec ommItemIntSpec = new OMMItemIntSpec();
            ReqMsg reqMsg = CreateRequestMessage(serviceName, FieldDictionaryName);
            ommItemIntSpec.Msg = reqMsg;

            DictionaryClient dictionaryClient =
                new DictionaryClient(rdmFieldDict);

            long handle1 = consumer.RegisterClient(eventQueue, ommItemIntSpec,
                dictionaryClient);

            //EnumDef
            reqMsg = CreateRequestMessage(serviceName, EnumTypeDefName);
            ommItemIntSpec.Msg = reqMsg;

            long handle2 = consumer.RegisterClient(eventQueue, ommItemIntSpec,
                dictionaryClient);

            System.DateTime currentTime = System.DateTime.Now;
            System.DateTime endTime =
                currentTime.AddSeconds(timeoutSeconds);
            while (!dictionaryClient.Completed && currentTime < endTime)
            {
                currentTime = System.DateTime.Now;

                int dispatchReturn = eventQueue.Dispatch(200);
                if ((dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedInActive) ||
                    (dispatchReturn ==
                    Dispatchable.DispatchReturnEnum.NothingDispatchedNoActiveEventStreams))
                    break;
            }

            List<long> handles = new List<long>();
            handles.Add(handle1);
            handles.Add(handle2);
            consumer.UnregisterClient(handles);

            if (!dictionaryClient.Completed)
            {
                throw Exceptions.NetworkDictionaryNotLoaded();
            }
        }

        private class DictionaryClient : Client
        {
            private readonly RDMFieldDictionary rdmFieldDict;
            private bool rdmFidDefCompleted;
            private bool enumDefCompleted;
            public volatile bool Completed = false;

            public DictionaryClient(RDMFieldDictionary rdmFieldDict)
            {
                this.rdmFieldDict = rdmFieldDict;
            }

            public void ProcessEvent(Event evt)
            {
                RespMsg respMsg = Util.GetRespMsg(evt,
                    RDM.MESSAGE_MODEL_TYPES.MMT_DICTIONARY);

                if (respMsg == null)
                {
                    return;
                }

                if (!(respMsg.RespType == RespMsg.RespTypeEnum.Refresh ||
                    respMsg.RespType == RespMsg.RespTypeEnum.Update))
                {
                    return;
                }

                if (respMsg.AttribInfo.Name.ToString() == FieldDictionaryName)
                {
                    // RWFFld
                    Series payload = (Series) respMsg.Payload;
                    rdmFieldDict.DecodeRDMFieldDictionary(payload);
                    rdmFidDefCompleted = true;
                }
                else if (respMsg.AttribInfo.Name.ToString() == EnumTypeDefName)
                {
                    // RWFEnum
                    Series payload = (Series) respMsg.Payload;
                    rdmFieldDict.DecodeRDMEnumDictionary(payload);
                    enumDefCompleted = true;
                }

                Completed = rdmFidDefCompleted && enumDefCompleted;
            }
        }
    }
}
