﻿using System;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Utils = Datawatch.DataPrep.Data.Framework.Utils;

namespace Panopticon.RMDSPlugin
{    
    public class RMDSSettings : RealtimeConnectionSettings
    {
        public RMDSSettings()
            : this(new PropertyBag())
        {
        }

        public RMDSSettings(PropertyBag bag)
            : base(bag, true)
        {
            IdColumnCandidates = new string[] { IdColumnName };
        }

        public string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value); }
        }

        public string Port
        {
            get { return GetInternal("Port", "14002"); }
            set { SetInternal("Port", value); }
        }

        public DictionaryLocationType DictLocationType
        {
            get
            {
                string s = GetInternal("DictLocationType");
                if (s != null)
                {
                    try
                    {
                        return (DictionaryLocationType)Enum.Parse(
                            typeof(DictionaryLocationType), s);
                    }
                    catch
                    {
                    }
                }
                return DictionaryLocationType.File;
            }
            set
            {

                SetInternal("DictLocationType", value.ToString());
            }
        }

        public string Service
        {
            get { return GetInternal("Service"); }
            set { SetInternal("Service", value); }
        }

        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        public string Symbol
        {
            get { return GetInternal("Symbol"); }
            set { SetInternal("Symbol", value); }
        }

        public string IdColumnName
        {
            get { return GetInternal("IdColumnName", "RIC"); }
            set { SetInternal("IdColumnName", value); }
        }

        public bool ProcessLinks
        {
            get
            {
                string s = GetInternal("ProcessLinks", Convert.ToString(true));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("ProcessLinks", Convert.ToString(value));
            }
        }

        public MessageModelType MessageModelType
        {
            get
            {
                string s = GetInternal("MessageModelType");
                if (s != null)
                {
                    try
                    {
                        return (MessageModelType)Enum.Parse(
                            typeof(MessageModelType), s);
                    }
                    catch
                    {
                    }
                }
                return MessageModelType.MarketPrice;
            }
            set
            {

                SetInternal("MessageModelType", value.ToString());
            }
        }

        public int MaxEventDispatchSeconds
        {
            get { return GetInternalInt("MaxEventDispatchSeconds", 20); }
            set { SetInternalInt("MaxEventDispatchSeconds", value); }
        }

        private void ClearSelectedColumns()
        {
            int count = GetInternalInt("SelectedColumnsCount", 0);
            for (int i = 0; i < count; i++)
            {
                SetInternal("SelectedColumnName", null);
                SetInternal("SelectedColumnLongName", null);
                SetInternal("SelectedColumnId", null);
                SetInternal("SelectedColumnType", null);
            }
            SetInternalInt("SelectedColumnsCount", 0);
        }

        public RMDSField[] SelectedColumns
        {
            get
            {
                int count = GetInternalInt("SelectedColumnsCount", 0);
                RMDSField[] fields = new RMDSField[count];
                for (int i = 0; i < count; i++)
                {
                    string name = GetInternal("SelectedColumnName" + i);
                    string longName = GetInternal("SelectedColumnLongName" + i);
                    int id = GetInternalInt("SelectedColumnId" + i, 0);
                    string type = GetInternal("SelectedColumnType" + i);
                    fields[i] = new RMDSField(name, longName, id, Convert.ToByte(type));
                }
                return fields;
            }
            set
            {
                if (value == null)
                {
                    throw Exceptions.ArgumentNull("value");
                }
                ClearSelectedColumns();
                int count = value.Length;
                SetInternalInt("SelectedColumnsCount", count);
                for (int i = 0; i < count; i++)
                {
                    SetInternal("SelectedColumnName" + i, value[i].Name);
                    SetInternal("SelectedColumnLongName" + i, value[i].LongName);
                    SetInternalInt("SelectedColumnId" + i, value[i].FieldId);
                    SetInternal("SelectedColumnType" + i, value[i].Type.ToString());
                }
            }
        }

        protected override void FirePropertyChanged(string name)
        {
            base.FirePropertyChanged(name);

            if (name.Equals("IdColumnName"))
            {
                IdColumnCandidates = new string[] { IdColumnName };
            }
        }
        
        public override bool Equals(object obj)
        {           
            if (!(obj is RMDSSettings)) return false;

            if (!base.Equals(obj)) return false;

            RMDSSettings cs = (RMDSSettings)obj;

            if (this.Host != cs.Host ||
                 this.Port != cs.Port ||
                 this.ProcessLinks != cs.ProcessLinks ||
                 this.DictLocationType != cs.DictLocationType ||
                 this.Service != cs.Service ||
                 this.UserName != cs.UserName ||
                 this.Symbol != cs.Symbol ||
                 this.IdColumnName != cs.IdColumnName)
            {
                return false;
            }

            if (!Utils.IsEqual(this.SelectedColumns,
                cs.SelectedColumns))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    Host, Port, ProcessLinks,
                    DictLocationType, Service, UserName, Symbol, IdColumnName
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            //TODO consider adding selected columns here.
            return hashCode;
        }        
    }
}
