﻿using System.Collections.Generic;

namespace Panopticon.RMDSPlugin
{
    internal interface IMessageModelManager
    {
        List<RMDSField> GetSchema(int timeoutSeconds);

        void StartRealtime(RMDSDataAdapter dataAdapter);
    }
}
