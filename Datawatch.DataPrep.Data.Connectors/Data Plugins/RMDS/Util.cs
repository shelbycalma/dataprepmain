﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.RMDSPlugin.Properties;

using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Message;
using ThomsonReuters.RFA.RDM;
using ThomsonReuters.RFA.SessionLayer;

namespace Panopticon.RMDSPlugin
{
    public class Util
    {
        //LINK 1 to LINK 14
        public static List<int> LinkFid = new List<int> { 240, 241, 242, 243,
        244, 245, 246, 247, 248, 249, 250, 251, 252, 253};

        //LONGLINK1 to LONGLINK14
        public static List<int> LongFid = new List<int> { 800, 801, 802, 803,
        804, 805, 806, 807, 808, 809, 810, 811, 812, 813};

        public const int NEXT_LR = 238;
        public const int LONGNEXTLR = 815;
        public const int PREF_LINK = 1081;
        public const int LONGLINK = 813;
        public const int LINK = 253;
        public const int REF_COUNT = 239;
        
        public static string ApplyParameters(string source,
            IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(source)) return null;
            if (parameters == null) return source;

            ParameterEncoder encoder = new ParameterEncoder();
            encoder.SourceString = source;
            encoder.Parameters = parameters;
            encoder.NullOrEmptyString = "NULL";
            return encoder.Encoded();            
        }

        public static string[] ApplyParametersToSymbol(string source,
            IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(source)) return null;

            string symbol = ApplyParameters(source, parameters);

            string[] subscribedSymbols = symbol.Split(new char[] { ',' },
                StringSplitOptions.RemoveEmptyEntries);
            return subscribedSymbols;
        }

        public static string GetAssemblyPath()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            return Path.GetDirectoryName(path);
        }

        public static List<RMDSField> GetSchema(RMDSSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            string[] subscribedSymbols = ApplyParametersToSymbol(
                settings.Symbol, parameters);
            string userName = ApplyParameters(settings.UserName, parameters);
            RMDSConnection connection = new RMDSConnection(settings,
                subscribedSymbols, userName, parameters);
            List<RMDSField> fieldList = connection.GetSchema();
            if (fieldList == null || fieldList.Count == 0)
            {
                throw Exceptions.EmptySchema();
            }
            return fieldList;
        }

        public static bool IsValidLinkFid(int fid, int refCount)
        {
            int minLinkFid = LinkFid[0];
            int maxLinkFid = minLinkFid + refCount - 1;
            return minLinkFid <= fid && fid <= maxLinkFid;
        }

        public static bool IsValidLongFid(int fid, int refCount)
        {
            int minLongFid = LongFid[0];
            int maxLongFid = minLongFid + refCount - 1;
            return minLongFid <= fid && fid <= maxLongFid;
        }

        public static void LogStreamStatus(RespMsg respMsg)
        {
            RespStatus status = respMsg.RespStatus;
            if (status.StatusCode == RespStatus.StatusCodeEnum.None)
            {
                return;
            }

            //log stream status only if status text is not empty
            //to let user know if service is unavailable.
            if (string.IsNullOrEmpty(status.StatusText.ToString().Trim()))
            {
                return;
            }
            StringBuilder statusText = new StringBuilder();

            statusText.Append("\r\n\tstreamState = ");
            statusText.Append(status.StreamState.ToString());
            statusText.Append("\r\n\tdataState = ");
            statusText.Append(status.DataState.ToString());
            statusText.Append("\r\n\tstatusCode = ");
            statusText.Append(status.StatusCode.ToString());
            statusText.Append("\r\n\tstatusText = ");
            statusText.Append(status.StatusText);
            statusText.Append("\r\n");

            string source;
            switch (respMsg.MsgModelType)
            {
                case RDM.MESSAGE_MODEL_TYPES.MMT_DICTIONARY:
                    source = "Dictionary";
                    break;
                case RDM.MESSAGE_MODEL_TYPES.MMT_LOGIN:
                    source = "Login";
                    break;
                case RDM.MESSAGE_MODEL_TYPES.MMT_MARKET_PRICE:
                    source = "Market Price";
                    break;
                default:
                    source = "Unknown(" + respMsg.MsgModelType + ")";
                    break;
            }

            Log.Info(Resources.LogStreamStatus, source, statusText.ToString());
        }

        public static RespMsg GetRespMsg(Event evt, byte messageModelType)
        {
            if (evt.Type != SessionLayerEventTypeEnum.OMMItemEvent)
            {
                return null;
            }

            OMMItemEvent ommItemEvent = (OMMItemEvent) evt;
            Msg msg = ommItemEvent.Msg;
            if (msg.MsgType != MsgTypeEnum.RespMsg)
            {
                return null;
            }

            RespMsg respMsg = (RespMsg) msg;
            
            LogStreamStatus(respMsg);
            
            if (respMsg.MsgModelType != messageModelType)
            {
                return null;
            }
            
            return respMsg;
        }
    }
}
