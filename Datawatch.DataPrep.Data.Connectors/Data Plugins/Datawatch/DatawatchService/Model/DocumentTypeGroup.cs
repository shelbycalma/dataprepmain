﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class DocumentTypeGroup : IWithId
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}