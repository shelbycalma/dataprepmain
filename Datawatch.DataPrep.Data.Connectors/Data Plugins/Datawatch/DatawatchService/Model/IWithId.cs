namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public interface IWithId
    {
        int Id { get; set; }
    }
}