﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class Field
    {
        public string Name { get; set; }

        public FieldType Type { get; set; }

        public int Length { get; set; }

        public int Decimals { get; set; }

        public int Format { get; set; }

        public FieldClass FieldClass { get; set; } //FieldClass 1 - report, 2 - calculated

        public bool IsHidden { get; set; }

        public bool IsIndexable { get; set; }

        public int TemplateType { get; set; }
    }

    public enum FieldType
    {
        String = 0,
        Date = 1,
        Number = 2
    }

    public enum FieldClass
    {
        Report = 1,
        Calculated = 2
    }
}