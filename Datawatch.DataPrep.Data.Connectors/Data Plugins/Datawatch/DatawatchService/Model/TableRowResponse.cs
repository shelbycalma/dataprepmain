﻿using System.Collections.Generic;

namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class TableRowResponse
    {
        public List<object> Values { get; set; }

        public TableLineageRowResponse Lineage { get; set; }
    }
}