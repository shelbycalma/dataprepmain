﻿using System.Collections.Generic;

namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class Table
    {
        public List<Field> Fields { get; set; }

        public List<TableRowResponse> Rows { get; set; }

        public int TotalRows { get; set; }
    }
}