﻿using System;

namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class Model : IWithId
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}