﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class LoginError : ErrorResult
    {
        public string ErrorMessage { get; set; }

        public int Code { get; set; }
    }
}