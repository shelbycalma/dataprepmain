﻿using System;

namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class Report : IWithId
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public DateTime PartitionDate { get; set; }

        public DateTime ReportFilingDate { get; set; }
    }
}