﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public enum LoginResult
    {
        Success,
        InvalidUserNameOrPassword = 410,
        PasswordExpired = 411,
        Unknown = -1,
    }
}