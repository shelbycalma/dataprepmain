﻿using System.Collections.Generic;

namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class ModelInfo : IWithId
    {
        public int Id { get; set; }

        public List<string> Summaries { get; set; }
    }
}