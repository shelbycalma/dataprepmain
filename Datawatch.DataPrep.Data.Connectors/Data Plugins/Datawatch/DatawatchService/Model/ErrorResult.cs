﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class ErrorResult
    {
        public string Message { get; set; }

        public string ExceptionMessage { get; set; }
    }
}