﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Model
{
    public class TableLineageRowResponse
    {
        public int PageNumber { get; set; }

        public int LineNumber { get; set; }

        public int FileId { get; set; }
    }
}