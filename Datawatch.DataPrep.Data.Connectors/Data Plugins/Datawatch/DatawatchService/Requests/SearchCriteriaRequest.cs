﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Requests
{
    public class SearchCriteriaRequest
    {
        public string CompareType { get; set; }

        public string FieldType { get; set; }

        public string JoinType { get; set; }

        public string Value { get; set; }
    }
}