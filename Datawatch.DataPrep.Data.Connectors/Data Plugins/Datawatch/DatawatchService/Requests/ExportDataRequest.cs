﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Requests
{
    public class ExportDataRequest : ExportRequest
    {
        public bool IsSyncToSource { get; set; }
    }
}