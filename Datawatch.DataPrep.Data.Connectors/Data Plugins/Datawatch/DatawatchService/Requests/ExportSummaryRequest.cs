﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Requests
{
    public class ExportSummaryRequest : ExportRequest
    {
        public string SummaryName { get; set; }
    }
}