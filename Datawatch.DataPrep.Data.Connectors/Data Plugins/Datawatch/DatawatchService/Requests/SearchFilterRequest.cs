﻿using System.Collections.Generic;

namespace Panopticon.DatawatchPlugin.DatawatchService.Requests
{
    public class SearchFilterRequest
    {
        public List<SearchCriteriaRequest> SearchCriteria { get; set; }
    }
}