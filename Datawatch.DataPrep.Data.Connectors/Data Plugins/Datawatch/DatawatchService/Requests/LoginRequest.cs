﻿using Datawatch.DataPrep.Data.Core;
using System.Security;

namespace Panopticon.DatawatchPlugin.DatawatchService.Requests
{
    public class LoginRequest
    {
        private SecureString password;
        public string Username { get; set; }

        public string Password
        {
            get
            {
                return SecurityUtils.ToInsecureString(password);
            }
            set
            {
                password = SecurityUtils.ToSecureString(value);
            }
        }

        public string Domain { get; set; }
    }
}