﻿namespace Panopticon.DatawatchPlugin.DatawatchService.Requests
{
    public class ExportRequest
    {
        public int[] ItemIds { get; set; }
        public string SearchType { get; set; }
        public int ModelId { get; set; }
        public string ModelType { get; set; }
    }
}