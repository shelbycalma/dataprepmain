﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Panopticon.DatawatchPlugin.DatawatchService.Model;
using System.Security;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.DatawatchPlugin.DatawatchService
{
    public class DatawatchClientAsync : DatawatchClient
    {
        public DatawatchClientAsync(string url) : base(url)
        {
        }

        public Task<LoginResult> LoginAsync(string username, SecureString password, string domain = null)
        {
            return Task.Factory.StartNew(() => this.Login(username, SecurityUtils.ToInsecureString(password), domain));
        }

        public Task<List<DocumentTypeGroup>> GetDocumentTypeGroupsAsync()
        {
            return Task.Factory.StartNew(() => this.GetDocumentTypeGroups());
        }

        public Task<List<DocumentType>> GetDocumentTypesAsync()
        {
            return Task.Factory.StartNew(() => this.GetDocumentTypes()); 
        }

        public Task<List<DocumentType>> GetDocumentTypesByDocumentTypeGroupAsync(int documentTypeGroupId)
        {
            return Task.Factory.StartNew(() => this.GetDocumentTypesByDocumentTypeGroup(documentTypeGroupId));
        }

        public Task<List<Model.Model>> GetModelsByDocumentTypeAsync(int documentTypeId)
        {
            return Task.Factory.StartNew(() => this.GetModelsByDocumentType(documentTypeId));
        }

        public Task<ModelInfo> GetModelInfoAsync(int modelId)
        {
            return Task.Factory.StartNew(() => this.GetModelInfo(modelId));
        }

        public Task<List<Report>> GetReportsByDocumentTypeAsync(int documentTypeId)
        {
            return Task.Factory.StartNew(() => GetReportsByDocumentType(documentTypeId));
        }

        public Task<List<Report>> GetReportsByDocumentTypeAsync(int documentTypeId, DateTime? fromDate, DateTime? toDate)
        {
            return Task.Factory.StartNew(() => GetReportsByDocumentType(documentTypeId, fromDate, toDate));
        }

        public Task<Table> ExportDataAsync(int[] reportId, int modelId)
        {
            return Task.Factory.StartNew(() => this.ExportData(reportId, modelId));
        }

        public Task<Table> ExportSummaryAsync(int[] reportId, int modelId, string summary)
        {
            return Task.Factory.StartNew(() => this.ExportSummary(reportId, modelId, summary));
        }
    }
}