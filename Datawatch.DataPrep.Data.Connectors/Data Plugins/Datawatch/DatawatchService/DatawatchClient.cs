﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.DatawatchPlugin.DatawatchService.Model;
using Panopticon.DatawatchPlugin.DatawatchService.Requests;
using Panopticon.DatawatchPlugin.Properties;
using RestSharp;
using Parameter = RestSharp.Parameter;

namespace Panopticon.DatawatchPlugin.DatawatchService
{
    public class DatawatchClient
    {
        private static readonly String apiPrefix = "api";
        protected readonly String url;
        protected readonly CookieContainer cookieContainer = new CookieContainer();

        public DatawatchClient(String url)
        {
            this.url = AddApiPrefix(url);
        }

        public LoginResult Login(String username, String password, String domain = null)
        {
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.Username = username;
            loginRequest.Password = password;
            loginRequest.Domain = domain;

            Tuple<object, LoginError> result = this.Execute<object, LoginError>("login", Method.POST, loginRequest);
            if (result.Item2 == null) return LoginResult.Success;
            var loginResult = ToLoginResult(result.Item2.Code);
            if (loginResult == LoginResult.Success) return LoginResult.Unknown;
            return loginResult;
        }

        public List<DocumentTypeGroup> GetDocumentTypeGroups()
        {
            return this.Execute<List<DocumentTypeGroup>>("documenttypegroup");
        }

        public List<DocumentType> GetDocumentTypes()
        {
            return this.Execute<List<DocumentType>>("documenttype");
        }

        public List<DocumentType> GetDocumentTypesByDocumentTypeGroup(int documentTypeGroupId)
        {
            Dictionary<String, object> urlsegments = new Dictionary<String, object> { { "id", documentTypeGroupId } };
            return this.Execute<List<DocumentType>>("documenttypegroup/{id}/documenttypes", urlsegments: urlsegments);
        }

        public List<Model.Model> GetModelsByDocumentType(int documentTypeId)
        {
            Dictionary<String, object> urlsegments = new Dictionary<String, object> { { "id", documentTypeId } };
            return this.Execute<List<Model.Model>>("documenttype/{id}/models", urlsegments: urlsegments);
        }

        public ModelInfo GetModelInfo(int modelId)
        {
            Dictionary<String, object> urlsegment = new Dictionary<String, object> { { "id", modelId } };
            return this.Execute<ModelInfo>("model/{id}/modelinfo", urlsegments: urlsegment);
        }

        public List<Report> GetReportsByDocumentType(int documentTypeId)
        {
            Dictionary<String, object> urlsegments = new Dictionary<String, object> { { "id", documentTypeId } };
            return this.Execute<List<Report>>("documenttype/{id}/reports", urlsegments: urlsegments);
        }

        public List<Report> GetReportsByDocumentType(int documentTypeId, DateTime? fromDate, DateTime? toDate)
        {
            if (!fromDate.HasValue && !toDate.HasValue)
                return GetReportsByDocumentType(documentTypeId);

            List<SearchCriteriaRequest> searchCriteria = new List<SearchCriteriaRequest>();
            if (fromDate.HasValue)
            {
                SearchCriteriaRequest fromSearchCriteria = new SearchCriteriaRequest();
                fromSearchCriteria.CompareType = "GreaterOrEqual";
                fromSearchCriteria.FieldType = "String";
                fromSearchCriteria.JoinType = "And";
                fromSearchCriteria.Value = fromDate.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

                searchCriteria.Add(fromSearchCriteria);
            }
            if (toDate.HasValue)
            {
                SearchCriteriaRequest fromSearchCriteria = new SearchCriteriaRequest();
                fromSearchCriteria.CompareType = "LessOrEqual";
                fromSearchCriteria.FieldType = "String";
                fromSearchCriteria.JoinType = "And";
                fromSearchCriteria.Value = toDate.Value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);

                searchCriteria.Add(fromSearchCriteria);
            }

            Dictionary<String, object> urlsegments = new Dictionary<String, object> { { "id", documentTypeId } };
            SearchFilterRequest searchFilterRequest = new SearchFilterRequest();
            searchFilterRequest.SearchCriteria = searchCriteria;

            return this.Execute<List<Report>>("documenttype/{id}/reports", Method.POST, searchFilterRequest, urlsegments);
        }

        public Table ExportData(int[] reportId, int modelId)
        {
            ExportDataRequest exportDataRequest = new ExportDataRequest();
            exportDataRequest.ItemIds = reportId;
            exportDataRequest.SearchType = "Report";
            exportDataRequest.ModelId = modelId;
            exportDataRequest.IsSyncToSource = true;

            return this.Execute<Table>("export/data", Method.POST, exportDataRequest);
        }

        public Table ExportSummary(int[] reportId, int modelId, String summary)
        {
            ExportSummaryRequest exportSummaryRequest = new ExportSummaryRequest();
            exportSummaryRequest.ItemIds = reportId;
            exportSummaryRequest.SearchType = "Report";
            exportSummaryRequest.ModelId = modelId;
            exportSummaryRequest.ModelType = "Xmod";
            exportSummaryRequest.SummaryName = summary;

            return this.Execute<Table>("export/summary", Method.POST, exportSummaryRequest);
        }

        private T Execute<T>(String resource, Method method = Method.GET, object body = null, Dictionary<String, object> urlsegments = null) where T : new()
        {
            return this.Execute<T, object>(resource, method, body, urlsegments).Item1;
        }

        private Tuple<T, TError> Execute<T, TError>(String resource, Method method = Method.GET, object body = null, Dictionary<String, object> urlsegments = null)
            where T : new()
            where TError : class
        {
            RestClient client = new RestClient(this.url) {CookieContainer = this.cookieContainer};
            RestRequest request = new RestRequest(resource, method) {RequestFormat = DataFormat.Json};
            if (urlsegments != null)
            {
                foreach (KeyValuePair<String, object> urlsegment in urlsegments)
                    request.AddUrlSegment(urlsegment.Key, urlsegment.Value.ToString());
            }
            if (body != null)
            {
                request.AddBody(body);
                Parameter bodyParameter = request.Parameters.First(p => p.Type == ParameterType.RequestBody);
                Log.Info(Resources.LogQueryExecutedWithBody, client.BuildUri(request).AbsolutePath,
                    ScrubPassword(bodyParameter.Value.ToString()));
            }
            else
            {
                Log.Info(Resources.LogQueryExecuted, client.BuildUri(request).AbsolutePath);
            }

            IRestResponse<T> response = client.Execute<T>(request);
            TError terror = null;
            if (response.StatusCode != HttpStatusCode.OK)
            {
                if (response.ContentType == null || !response.ContentType.Contains("application/json"))
                {
                    throw ResponseErrorException(response);
                }

                if (typeof (TError) == typeof (object))
                {
                    ErrorResult error = SimpleJson.DeserializeObject<ErrorResult>(response.Content);
                    throw new Exception(error.Message);
                }
                terror = SimpleJson.DeserializeObject<TError>(response.Content);
            }

            return Tuple.Create(response.Data, terror);
        }

		private string ScrubPassword(string s)
		{
			//"{\"Username\":\"admin\",\"Password\":\"password\",\"Domain\":null}"
			string[] k = s.Split(',');
			string ret = string.Empty;
			for (int i = 0; i < k.Length; i++)
			{
				if (!k[i].ToUpper().Contains("PASSWORD"))
				{
					ret += ((ret.Length > 0) ? "," : "") + k[i];
				}
			}
			return ret;
		}

		private Exception ResponseErrorException<T>(IRestResponse<T> response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.NotFound:
                    return new Exception(string.Format(Resources.ExNotFound, response.ResponseUri));
                default:
                {
                    if (string.IsNullOrEmpty(response.StatusDescription) || response.StatusCode == 0)
                        return new Exception(response.ErrorMessage);
                    return new Exception(string.Format("'{0}' ({1})", response.StatusDescription,
                        (int)response.StatusCode));
                }
            }
        }

        private static String AddApiPrefix(String url)
        {
            if (url.ToLower().EndsWith("/" + apiPrefix))
                return url;
            if (url.EndsWith("/"))
                return url + apiPrefix;
            return url + "/" + apiPrefix;
        }

        private static LoginResult ToLoginResult(int code)
        {
            return (LoginResult)code;
        }
    }
}