﻿using System;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.DatawatchPlugin.Properties;

namespace Panopticon.DatawatchPlugin
{
    public class DatawatchSettings : ConnectionSettings
    {
        private readonly TimeZoneHelper timeZoneHelper;

        public DatawatchSettings() : this(new PropertyBag())
        {
        }

        public DatawatchSettings(PropertyBag properties) : base(properties)
        {
            PropertyBag timeZoneHelperBag = properties.SubGroups["TimeZone"];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                properties.SubGroups["TimeZone"] = timeZoneHelperBag;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);
        }

        public string ClientUrl
        {
            get { return this.GetInternal("CollectionListURL", "http://"); }
            set { this.SetInternal("CollectionListURL", value); }
        }

        public string Username
        {
            get { return this.GetInternal("Username"); }
            set { this.SetInternal("Username", value); }
        }

        public string Password
        {
            get { return this.GetInternal("Password"); }
            set { this.SetInternal("Password", value); }
        }

        public int DocumentTypeGroupId
        {
            get { return this.GetInternalInt("DocumentTypeGroupId", -1); }
            set { this.SetInternalInt("DocumentTypeGroupId", value); }
        }

        public int DocumentTypeId
        {
            get { return this.GetInternalInt("DocumentTypeId", -1); }
            set { this.SetInternalInt("DocumentTypeId", value); }
        }

        public int ModelId
        {
            get { return this.GetInternalInt("ModelId", -1); }
            set { this.SetInternalInt("ModelId", value); }
        }

        public int[] ReportIds
        {
            get { return this.GetReportIds(); }
            set { this.SetReportIds(value); }
        }

        public ViewType View
        {
            get { return (ViewType)this.GetInternalInt("View", (int)ViewType.Data); }
            set { this.SetInternalInt("View", (int)value); }
        }

        public string SummaryName
        {
            get { return this.GetInternal("SummaryName"); }
            set { this.SetInternal("SummaryName", value); }
        }

        public DateTime? FromDate
        {
            get
            {
                // TODO: Looks like TimeValue.Parse can first try to parse value from CurrentCulture
                DateTime parsedValue = TimeValue.Parse(this.GetInternal("FromDate", TimeValue.EmptyText));
                return parsedValue == TimeValue.Empty
                           ? (DateTime?)null
                           : parsedValue;
            }
            set { this.SetInternal("FromDate", value.HasValue ? TimeValue.ToString(value.Value) : TimeValue.EmptyText); }
        }

        public DateTime? ToDate
        {
            get
            {
                // TODO: Looks like TimeValue.Parse can first try to parse value from CurrentCulture
                DateTime parsedValue = TimeValue.Parse(this.GetInternal("ToDate", TimeValue.EmptyText));
                return parsedValue == TimeValue.Empty
                           ? (DateTime?)null
                           : parsedValue;
            }
            set { this.SetInternal("ToDate", value.HasValue ? TimeValue.ToString(value.Value) : TimeValue.EmptyText); }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return timeZoneHelper; }
        }

        public void VeritySettings()
        {
            if (DocumentTypeId == -1)
                throw new Exception(Resources.ExDocumentTypeIsNotSelected);
            if (ModelId == -1)
                throw new Exception(Resources.ExModelIsNotSelected);
            if (ReportIds == null || ReportIds.Length == 0)
                throw new Exception(Resources.ExReportsIsNotSelected);
            if (View == ViewType.Summary && SummaryName == null)
                throw new Exception(Resources.ExSummaryNameIsNotSelected);
        }

        private void SetReportIds(int[] value)
        {
            if (value != null)
            {
                this.SetInternalInt("ReportIdCount", value.Length);
                for (int i = 0; i < value.Length; i++)
                {
                    int reportId = value[i];
                    this.SetInternalInt(string.Format("ReportId_{0}", i), reportId);
                }
            }
            else
            {
                int reportIdCount = this.GetInternalInt("ReportIdCount", 0);
                // WORKAROUND for remove old report ids
                PropertyBag propertyBag = this.ToPropertyBag();
                for (int i = 0; i < reportIdCount; i++)
                {
                    propertyBag.Values.Remove(string.Format("ReportId_{0}", i));
                }
            }
        }

        private int[] GetReportIds()
        {
            int reportIdCount = this.GetInternalInt("ReportIdCount", 0);
            int[] reportIds = new int[reportIdCount];
            for (int i = 0; i < reportIdCount; i++)
            {
                reportIds[i] = this.GetInternalInt(string.Format("ReportId_{0}", i), -1);
            }
            return reportIds;
        }
    }
}