﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.DatawatchPlugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.DatawatchPlugin.dll")]
[assembly: AssemblyDescription("Datawatch Content Server Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
