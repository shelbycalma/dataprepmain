﻿namespace Panopticon.DatawatchPlugin
{
    public enum ViewType
    {
        Data,
        Summary
    }
}