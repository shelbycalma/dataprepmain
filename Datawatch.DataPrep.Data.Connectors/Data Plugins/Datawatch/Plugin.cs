﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.DatawatchPlugin.DatawatchService;
using Panopticon.DatawatchPlugin.DatawatchService.Model;
using Panopticon.DatawatchPlugin.Properties;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.DatawatchPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<DatawatchSettings>
    {
        internal const string PluginId = "DatawatchEnterpriseServerPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Monarch Server - Content";
        internal const string PluginType = DataPluginTypes.File;


        private static readonly Dictionary<int, TypedParameterValue> EmptyDictionary = new Dictionary<int, TypedParameterValue>();

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {

            get
            {
                string title = Resources.UiPluginTitle;

                //IMPORTANT: Remove this section of code when the required dll is being copied by the build and install scripts   
                if (System.Globalization.CultureInfo.CurrentUICulture.TwoLetterISOLanguageName == "fr")
                {
                    if (title == PluginTitle)
                    {
                        title = "Monarch Server - Contenu";
                    }
                }
                return title;
            }
        }

        public override DatawatchSettings CreateSettings(PropertyBag bag)
        {
            return new DatawatchSettings(bag);
        }

        public override ITable GetData(string workbookDir,
            string dataDir, PropertyBag properties,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            this.CheckLicense();

            ParameterEncoder parameterEncoder = new ParameterEncoder { Parameters = parameters };

            DatawatchSettings datawatchSettings = this.CreateSettings(properties);

            datawatchSettings.VeritySettings();

            ((IParameterEncoder)parameterEncoder).SourceString = datawatchSettings.ClientUrl;
            string url = ((IParameterEncoder)parameterEncoder).Encoded();
            DatawatchClient client = new DatawatchClient(url);

            parameterEncoder.SourceString = datawatchSettings.Username;
            string encodedUsername = parameterEncoder.Encoded();

            parameterEncoder.SourceString = datawatchSettings.Password;
            string encodedPassword = parameterEncoder.Encoded();
            LoginResult loginResult = client.Login(encodedUsername, encodedPassword);
            if (loginResult != LoginResult.Success)
                throw new Exception(GetLoginErrorString(loginResult));

            DateTime start = DateTime.Now;
            Table tableData = ExportData(datawatchSettings, client);
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Resources.LogQueryResultSizeAndTime,
                tableData.Rows.Count, tableData.TotalRows, tableData.Fields.Count, seconds);

            TimeZoneHelper timeZoneHelper = datawatchSettings.TimeZoneHelper;

            StandaloneTable table = new StandaloneTable();

            try
            {
                table.BeginUpdate();
                for (int i = 0; i < tableData.Fields.Count; i++)
                {
                    Field field = tableData.Fields[i];

                    table.AddColumn(CreateColumn(field));
                }

                bool addLineageUrl = false;
                for (int i = 0; i < tableData.Rows.Count; i++)
                {
                    if (tableData.Rows[i].Lineage != null)
                    {
                        addLineageUrl = true;
                        break;
                    }
                }

                if (addLineageUrl)
                    table.AddColumn(new TextColumn("LineageUrl"));

                ParameterFilter parameterFilter = ParameterFilter.CreateFilter((ITable)table, parameters);

				int j = 0;
				foreach (TableRowResponse row in tableData.Rows)
				{
					if (j <= rowcount || rowcount < 0)
					{
						List<object> copyRow = new List<object>(row.Values);
						ProcessDateTimeColumns(copyRow, tableData.Fields);
						ConvertTimezone(copyRow, timeZoneHelper);
						if (addLineageUrl && row.Lineage != null)
							copyRow.Add(GenerateLineageUrl(url, row.Lineage));
						if (applyRowFilteration && parameterFilter != null &&
							parameterFilter.Contains(row.Values.ToArray()))
							continue;
						table.AddRow(copyRow.ToArray());
						j++;
					}
				}
            }
            finally
            {
                table.EndUpdate();
            }
            Log.Info(Resources.LogQueryResultFiltered, tableData.Rows.Count, table.RowCount);

            return (ITable) table;
        }

        private static void ConvertTimezone(List<Object> row, TimeZoneHelper timeZoneHelper)
        {
            if (timeZoneHelper == null || !timeZoneHelper.TimeZoneSelected) return;

            for (int i = 0; i < row.Count; i++)
            {
                if (row[i] is DateTime)
                {
                    row[i] = timeZoneHelper.ConvertFromUTC((DateTime)row[i]);
                }
            }
        }

        private static Column CreateColumn(Field field)
        {
            Column column;
            switch (field.Type)
            {
                case FieldType.Number:
                    column = new NumericColumn(field.Name);
                    break;
                case FieldType.Date:
                    column = new TimeColumn(field.Name);
                    break;
                default:
                    column = new TextColumn(field.Name);
                    break;
            }
            return column;
        }

        private static Table ExportData(DatawatchSettings datawatchSettings, DatawatchClient client)
        {
            switch (datawatchSettings.View)
            {
                case ViewType.Data:
                    return client.ExportData(datawatchSettings.ReportIds, datawatchSettings.ModelId);
                case ViewType.Summary:
                    return client.ExportSummary(datawatchSettings.ReportIds, datawatchSettings.ModelId, datawatchSettings.SummaryName);
                default:
                    throw new NotSupportedException();
            }
        }

        private static void ProcessDateTimeColumns(List<object> row, List<Field> fields)
        {
            for (int i = 0; i < fields.Count; i++)
            {
                Field field = fields[i];
                if (field.Type != FieldType.Date || row[i] == null) continue;
                row[i] = DateTime.ParseExact((string)row[i], "yyyy'-'MM'-'dd'T'HH':'mm':'ss.FFFFFFFK",
                    CultureInfo.InvariantCulture, DateTimeStyles.None).ToUniversalTime();
            }
        }

        private string GenerateLineageUrl(string url, TableLineageRowResponse lineage)
        {
            string clientUrl = url;
            if (clientUrl.EndsWith("/api", StringComparison.InvariantCultureIgnoreCase))
                clientUrl = clientUrl.Substring(0, clientUrl.Length - 4);
            return string.Format("{0}/SourceSyncronizedReportView/OpenSyncronizedReportView?" +
                                 "lineNo={1}&pageNo={2}&fileId={3}&searchType=Report",
                                 clientUrl, lineage.LineNumber, lineage.PageNumber, lineage.FileId);
        }

        private string GetLoginErrorString(LoginResult loginResult)
        {
            switch (loginResult)
            {
                case LoginResult.Success:
                    return null;
                case LoginResult.InvalidUserNameOrPassword:
                    return Resources.ErrorConnectStatusMessageInvalidUsernameOrPassword;
                case LoginResult.PasswordExpired:
                    return Resources.ErrorConnectStatusMessagePasswordExpired;
                default:
                    return string.Format(Resources.ErrorConnectStatusMessageUnknowError,
                        (int)loginResult);
            }
        }
    }
}
