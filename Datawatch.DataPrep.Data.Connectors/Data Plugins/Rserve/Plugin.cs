﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.RScriptsHelpers;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.RservePlugin.Properties;

namespace Panopticon.RservePlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<RservePluginSettings>
    {
        internal const string PluginId = "RservePlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Rserve";
        internal const string PluginType = DataPluginTypes.Database;

        public override RservePluginSettings CreateSettings(PropertyBag bag)
        {
            RservePluginSettings rservePluginSettings = new RservePluginSettings(bag);
            if (string.IsNullOrEmpty(rservePluginSettings.Title))
            {
                rservePluginSettings.Title = Resources.UiDefaultConnectionTitle;
            }

            return rservePluginSettings;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            CheckLicense();
            RservePluginSettings rservePluginSettings = CreateSettings(bag);

            StandaloneTable table = ExecutionHelper.ExecuteTableScript(rservePluginSettings.ToRSettings(),
                                                                       parameters, rowcount);

            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }
    }
}
