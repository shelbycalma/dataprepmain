﻿using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.RScriptsHelpers;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.RservePlugin
{
    public class RservePluginSettings : ConnectionSettings
    {
        public RservePluginSettings() :
            this(new PropertyBag())
        {}

        public RservePluginSettings(PropertyBag properties)
            : base(properties)
        {}

        public string Host
        {
            get { return GetInternal("host", "localhost"); }
            set { SetInternal("host", value); }
        }

        public string Port
        {
            get { return GetInternal("port", "6311"); }
            set { SetInternal("port", value); }
        }

        public string Username
        {
            get { return GetInternal("username"); }
            set { SetInternal("username", value); }
        }

        public string Password
        {
            get { return GetInternal("password"); }
            set { SetInternal("password", value); }
        }

        public string Script
        {
            get { return GetInternal("script"); }
            set { SetInternal("script", value); }
        }

        public bool EncloseParametersInQuotes
        {
            get
            {
                string value = GetInternal("encloseParametersInQuotes", false.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("encloseParametersInQuotes", value.ToString()); }
        }

        public int Timeout
        {
            get { return GetInternalInt("timeout", 10); }
            set { SetInternalInt("timeout", value); }
        }

        public RSettings ToRSettings()
        {
            return new RSettings
                {
                    Address = Host,
                    Port = Port,
                    User = Username,
                    Password = Password,
                    EncloseParametersInQuotes = EncloseParametersInQuotes,
                    ScriptContent = Script,
                    Timeout = Timeout
                };
        }
    }
}
