﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.QpidPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionControl.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private QpidSettings settings;

        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(QpidSettings settings)
        {
            InitializeComponent();
            DataContext = Settings = settings;
            DataContextChanged +=
                UserControl_DataContextChanged;
        }

        public QpidSettings Settings
        {
            get { return settings; }
            private set {
                settings = value;
                if (settings != null)
                {
                    PasswordBox.Password = settings.Password;
                }
                else
                {
                    PasswordBox.Password = null;
                }
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            Settings = DataContext as QpidSettings;
        }

    }
}
