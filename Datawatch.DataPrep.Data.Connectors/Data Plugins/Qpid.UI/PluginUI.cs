﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.QpidPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, QpidSettings,
        Dictionary<string, object>, QpidDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) :
            base(plugin, owner, "pack://application:,,,/Panopticon.QpidPlugin.UI;component" +
            "/qpid-small.png")
        {
        }

        public override Window CreateConnectionWindow(QpidSettings settings)
        {
            return new ConnectionWindow(settings);
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new QpidSettings(Plugin.PluginManager, bag, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }
    }
}
