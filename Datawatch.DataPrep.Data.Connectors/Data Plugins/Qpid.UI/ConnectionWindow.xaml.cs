﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.QpidPlugin.UI
{
    /// <summary>
    /// Interaction logic for QueryWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private QpidSettings connection;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(QpidSettings con)
        {
            this.connection = con;

            InitializeComponent();
        }

        public QpidSettings Connection
        {
            get { return connection; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = connection.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
