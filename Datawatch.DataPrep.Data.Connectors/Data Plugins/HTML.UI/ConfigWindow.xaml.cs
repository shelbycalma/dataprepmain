﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.HtmlPlugin.UI
{
    internal partial class ConfigWindow : Window
    {
        public HtmlViewModel ViewModel { get; set; }
        public HtmlSettings HSettings { get; set; }


        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof (ConfigWindow));

        public ConfigWindow(HtmlSettings HSettings, HtmlViewModel HView)
        {
            HView.HSettings = HSettings;
            this.ViewModel = HView;
            this.HSettings = HSettings;
            InitializeComponent();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ViewModel.IsOk();
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}