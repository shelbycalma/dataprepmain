﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.HtmlPlugin.HtmlParsingClasses;

namespace Panopticon.HtmlPlugin.UI
{
    public partial class ConfigPanel
    {
        #region Private Variables

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                typeof(HtmlViewModel),
                typeof(ConfigPanel), new PropertyMetadata(SettingsChanged));

        private HtmlSettings hSettings;

        #endregion

        #region Constructors

        public ConfigPanel()
            : this(null)
        {
        }

        public ConfigPanel(HtmlSettings settings)
        {
            HtmlDropTargetAdvisor = new HtmlDropTargetAdvisor();
            HtmlDropTargetAdvisor.HtmlDropped += HtmlDropHandler;
            InitializeComponent();

            HSettings = settings;
            HHelper = new HtmlHelper(settings);
            ReconstituteSettings();
        }

        #endregion

        #region Properties
        public HtmlDropTargetAdvisor HtmlDropTargetAdvisor { get; set; }

        public HtmlViewModel ViewModel
        {
            get { return (HtmlViewModel)GetValue(ViewModelProperty); }
            set
            {
                SetValue(ViewModelProperty, value);
            }
        }

        public HtmlSettings HSettings
        {
            get
            {
                return ViewModel != null ? ViewModel.HSettings : hSettings;
            }

            set
            {
                hSettings = value;
                if (ViewModel != null)
                {
                    ViewModel.HSettings = value;
                }
            }
        }

        public HtmlHelper HHelper { get; set; }

        #endregion

        #region General Methods

        private static void SettingsChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (HtmlViewModel)args.OldValue,
                (HtmlViewModel)args.NewValue);
            ((ConfigPanel)obj).HHelper =
                new HtmlHelper(
                    ((HtmlViewModel)args.NewValue).HSettings);
        }

        protected void OnSettingsChanged(
            HtmlViewModel oldModel, HtmlViewModel newModel)
        {
            DataContext = newModel;
            HHelper = new HtmlHelper(newModel.HSettings);
            ReconstituteSettings();
        }

        private void ReconstituteSettings()
        {
            if (ViewModel == null)
            {
                ViewModel = new HtmlViewModel(HSettings, HHelper);
                //OnSettingsChanged will call again with new model
                return;
            }
            if (HSettings != null)
            {
                string HtmlString = string.Empty;
                if (HSettings.FilePathType == PathType.Text)
                {
                    HtmlString = HSettings.HtmlString;
                }
                else
                {
                    HtmlString = HSettings.GetHtmlStreamData();
                }

                if (!string.IsNullOrEmpty(HtmlString))
                {
                    List<ListBoxData> listBoxData
                    = HHelper.ParseAndCreateTables(HtmlString);
                    ViewModel.TableList = listBoxData;
                    TablesListBox.SelectedIndex = HSettings.TableIndex;

                    TableData bagTable
                        = HHelper.TableBagToTableData(HSettings.TableIndex);

                    if (bagTable != null)
                    {
                        listBoxData[HSettings.TableIndex].TableData = bagTable;
                        listBoxData[HSettings.TableIndex].DataView
                            = bagTable.DataTable.AsDataView();
                    }
                }
            }
        }

        public string GetContentType(string url)
        {
            try
            {
                Uri uri = new Uri(url);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Accept = "*/*";
                request.Method = "HEAD";

                // Get the response
                int nStatusCode = 0;
                HttpWebResponse response = null;
                response = (HttpWebResponse)request.GetResponse();
                nStatusCode = (int)response.StatusCode;
                if (nStatusCode < 200 || nStatusCode > 300)
                    return "";
                string contentType = response.ContentType;
                if (string.IsNullOrWhiteSpace(contentType))
                    return "";
                return contentType.Split(';')[0].Trim();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return "";
            }
        }

        private void HtmlDropHandler(object sender, HtmlDropEventArgs e)
        {
            List<ListBoxData> listBoxData
                = HHelper.ParseAndCreateTables(e.HtmlString);
            ViewModel.TableList = listBoxData;
            TablesListBox.SelectedIndex = 0;
        }

        private void listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            HSettings.TableIndex = TablesListBox.SelectedIndex;
            ListBoxData item = TablesListBox.SelectedItem as ListBoxData;
            if (item != null)
            {
                TrySerializeTable(item);
                ViewModel.CurrentTableView = item.DataView;
            }

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            ListBoxData item = TablesListBox.SelectedItem as ListBoxData;
            if (item == null)
            {
                return;
            }

            System.Windows.Forms.SaveFileDialog dlg =
                new System.Windows.Forms.SaveFileDialog
                {
                    Filter = "Delimited Text Files (*.csv)|*.csv|All Files (*.*)|*.*",
                    DefaultExt = ".csv",
                    OverwritePrompt = true
                };

            if (dlg.ShowDialog() != System.Windows.Forms.DialogResult.OK)
            {
                return;
            }

            item.TableData.WriteCsvFile(dlg.FileName);
        }

        private void ConfigPanel_OnLostFocus(object sender, RoutedEventArgs e)
        {
            TrySerializeTable(TablesListBox.SelectedItem as ListBoxData);
        }

        private void TrySerializeTable(ListBoxData item)
        {
            if (item == null) return;
            HSettings.SelectedTable = HHelper.SerializeTableData(
                item.TableData.DataTable);
        }

        #endregion

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (HSettings != null)
            {
                HSettings.WebPassword = PasswordBox.Password;
            }
        }

        private void RetrieveTables_Click(object sender, RoutedEventArgs e)
        {
            string HtmlString = HSettings.GetHtmlStreamData();

            List<ListBoxData> listBoxData = HHelper.ParseAndCreateTables(HtmlString);
            ViewModel.TableList = listBoxData;
            //TODO: Should move this handler to view model with command, need to align UI logic then.
            TablesListBox.SelectedIndex = 0;
        }
    }
}