﻿using System;
using System.Windows;
using Datawatch.DataPrep.Data.Core.UI.DragDrop;

namespace Panopticon.HtmlPlugin.UI
{
    public class HtmlDropEventArgs : EventArgs
    {
        public IDataObject Obj;
        public string HtmlString;

        public HtmlDropEventArgs(IDataObject o)
        {
            Obj = o;
            HtmlString = GetHtmlFromClipboard(Obj);
        }

        private static string GetHtmlFromClipboard(IDataObject data)
        {
            try
            {
                string html = data.GetData("HTML Format") as string;
                return string.IsNullOrEmpty(html) ? string.Empty : html;
            }
            catch
            {
                return string.Empty;
            }
        }
    }

    public class HtmlDropTargetAdvisor : IDropTargetAdvisor
    {
        public EventHandler<HtmlDropEventArgs> HtmlDropped;

        public UIElement TargetUI { get; set; }
        public bool IsValidDataObject(IDataObject obj)
        {
            var valid = obj.GetDataPresent(DataFormats.Html);
            return valid;
        }

        public void OnDropCompleted(IDataObject obj, Point dropPoint)
        {
            if (HtmlDropped != null)
            {
                HtmlDropped(this, new HtmlDropEventArgs(obj));
            }
        }

        public UIElement GetVisualFeedback(IDataObject obj)
        {
            return null;
        }

        public Point GetOffsetPoint(IDataObject obj)
        {
            return new Point();
        }

        public Point AdjustDropPoint(IDataObject obj, Point p)
        {
            return p;
        }
    }
}