﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.HtmlPlugin.HtmlParsingClasses;

namespace Panopticon.HtmlPlugin.UI
{
    public class HtmlViewModel : ViewModelBase
    {
        #region Private Variables
        private HtmlSettings settings;
        private List<ListBoxData> tableList;
        private DataView currentTableView;
        #endregion

        #region Constructors

        public HtmlViewModel(
            HtmlSettings HSettings, HtmlHelper HHelper)
        {
            this.HSettings = HSettings;
            this.HHelper = HHelper;
        }

        #endregion

        #region Properties

        public bool HasData
        {
            get { return TableList != null && TableList.Any(); }
        }

        public HtmlSettings HSettings
        {
            get { return settings; }
            set
            {
                settings = value;
                OnPropertyChanged("HSettings");
            }
        }

        public HtmlHelper HHelper { get; set; }

        public List<ListBoxData> TableList
        {
            get { return tableList; }
            set
            {
                tableList = value;
                OnPropertyChanged("TableList");
                OnPropertyChanged("HasData");
            }
        }

        public DataView CurrentTableView
        {
            get { return currentTableView; }
            set
            {
                currentTableView = value;
                OnPropertyChanged("CurrentTableView");
            }
        }

        #endregion

        #region General Methods
        public bool IsOk()
        {
            return HSettings.TableIndex != -1;
        }
        #endregion
    }

}