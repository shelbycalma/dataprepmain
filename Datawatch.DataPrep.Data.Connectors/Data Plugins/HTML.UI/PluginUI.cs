﻿using System;
using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.HtmlPlugin;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.HtmlPlugin.HtmlParsingClasses;

namespace Panopticon.HtmlPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, HtmlSettings>, IFileOpenDataPluginUI<Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            //todo: fix the repetition between this method and DoOpenFile
            HtmlSettings hSettings
                = new HtmlSettings(Plugin.PluginManager, new PropertyBag(), parameters);

            HtmlViewModel viewModel = new HtmlViewModel(hSettings,
                    new HtmlHelper(hSettings));

            ConfigWindow window = new ConfigWindow(hSettings, viewModel);

            bool? dialogResult = window.ShowDialog();

            return dialogResult != true
                ? null
                : viewModel.HSettings.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            return new ConfigPanel(new HtmlSettings(Plugin.PluginManager, bag, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            return cp.HSettings.ToPropertyBag();
        }

        public PropertyBag DoOpenFile(string filePaths)
        {
            HtmlSettings setts = new HtmlSettings(Plugin.PluginManager, new PropertyBag())
            {
                HtmlString = filePaths,
                FilePathType = PathType.Text
            };

            HtmlViewModel viewModel = new HtmlViewModel(setts,
                    new HtmlHelper(setts));

            ConfigWindow window = new ConfigWindow(setts, viewModel);

            bool? dialogResult = window.ShowDialog();

            return dialogResult != true
                ? null
                : viewModel.HSettings.ToPropertyBag();
        }

        public bool CanOpen(string filePath)
        {
            try
            {
                DataObject dataObj = new DataObject(DataFormats.Html, filePath);
                bool isHtml = !String.IsNullOrEmpty(
                    TextConverterBase.GetHtmlFromClipboard(dataObj));
                return isHtml;
            }
            catch
            {
                return false;
            }
        }

        public string[] FileExtensions
        {
            get { return new[] { String.Empty }; }
        }

        public string FileFilter
        {
            get { return String.Empty; }
        }

        public PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new NotImplementedException();
        }
    }
}
