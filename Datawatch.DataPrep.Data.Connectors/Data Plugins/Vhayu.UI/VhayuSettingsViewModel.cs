﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Threading;
using Panopticon.VhayuPlugin.Schema;

namespace Panopticon.VhayuPlugin.UI
{
    public class VhayuSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private VhayuSchema schema;

        private ProcedureType procedureType;
        private Procedure procedure;

        private bool isDSNChangeInProcess;
        private bool isLastSelectedProcedureLoaded;

        private string procedureNameFromModel;
        private bool loadingProcedureMeta;

        private string error;

        private Dispatcher dispatcher;


        public VhayuSettingsViewModel(VhayuSettings model)
        {
            this.Model = model;

            dispatcher = Dispatcher.CurrentDispatcher;

            this.PropertyChanged += this_PropertyChanged;

            this.Parameters = new ObservableCollection<VhayuParameter>();

            if (string.IsNullOrEmpty(model.DataSourceName))
            {
                string dsn = this.KnownDataSourceNames.FirstOrDefault();
                if (!string.IsNullOrEmpty(dsn))
                {
                    model.DataSourceName = dsn;
                }
            }

            InitializeFromModel();
            isDSNChangeInProcess = false;
            isLastSelectedProcedureLoaded = false;
        }

        public string DataSourceName
        {
            get { return this.Model.DataSourceName; }
            set
            {
                if (value != this.Model.DataSourceName)
                {
                    isDSNChangeInProcess = true;

                    this.Model.DataSourceName = value;
                    this.ErrorMessage = null;

                    if (this.IsDataSourceCached)
                    {
                        LoadSchema();

                    }
                    else
                    {
                        this.Schema = null;
                    }

                    SetLastSelectedProcedureType();
                    isDSNChangeInProcess = false;

                    NotifyChanged("DataSourceName");
                    NotifyChanged("IsDataSourceCached");
                    NotifyChanged("HasNoSchemaButDataSourceName");
                }
            }
        }

        public bool IsOK
        {
            get { return !string.IsNullOrEmpty(DataSourceName); }
        }

        public string ErrorMessage
        {
            get { return error; }
            set
            {
                if (value != error)
                {
                    error = value;
                    NotifyChanged("ErrorMessage");
                    NotifyChanged("HasError");
                }
            }
        }

        private string GenerateSql()
        {
            return VhayuExecutor.BuildQuery(this.Model);
        }

        public bool GuessTimeType
        {
            get { return this.Model.GuessTimeType; }
            set
            {
                if (value != this.Model.GuessTimeType)
                {
                    this.Model.GuessTimeType = value;
                    NotifyChanged("GuessTimeType");
                }
            }
        }

        public bool HasError
        {
            get { return this.ErrorMessage != null; }
        }

        public bool HasExplicitSql
        {
            get { return this.Model.Sql != null; }
        }

        public bool HasNoExplicitSql
        {
            get { return this.Model.Sql == null; }
        }

        public bool HasNoSchema
        {
            get { return this.Schema == null; }
        }

        public bool HasNoSchemaButDataSourceName
        {
            get
            {
                return this.Schema == null &&
                    !string.IsNullOrEmpty(this.DataSourceName);
            }
        }

        public bool HasParameters
        {
            get { return this.Parameters.Count > 0; }
        }

        public bool HasSchema
        {
            get { return this.Schema != null; }
        }

        private void InitializeFromModel()
        {
            procedureNameFromModel = this.Model.ProcedureName;

            if (string.IsNullOrEmpty(this.Model.DataSourceName))
            {
                return;
            }

            LoadSchema();
            if (this.Schema == null)
            {
                return;
            }

            Procedure procedure = this.Schema.AllProcedures
                .FirstOrDefault(p => p.ProcedureName.Equals(
                    this.Model.ProcedureName));
            if (procedure == null)
            {
                procedure = this.Schema.AllProcedures.First();
            }

            this.procedureType = procedure.Type;
            this.procedure = procedure;

            if (string.IsNullOrEmpty(this.Model.ProcedureName))
            {
                this.Model.ProcedureName = procedure.ProcedureName;
            }

            if (string.IsNullOrEmpty(this.Model.LastSelectedProcedure))
            {
                this.Model.LastSelectedProcedure = procedure.ProcedureName;
            }
            if (string.IsNullOrEmpty(this.Model.LastSelectedProcedureType))
            {
                this.Model.LastSelectedProcedureType =
                    procedure.Type.TypeName;
            }

            UpdateParameters();
        }

        public bool IsDataSourceCached
        {
            get { return SchemaProvider.IsCached(this.DataSourceName); }
        }

        public bool IsLoadingProcedureMetaData
        {
            get { return loadingProcedureMeta; }
            set
            {
                if (value != loadingProcedureMeta)
                {
                    loadingProcedureMeta = value;
                    NotifyChanged("IsLoadingProcedureMetaData");
                }
            }
        }

        public IEnumerable<string> KnownDataSourceNames
        {
            get { return SchemaProvider.KnownDataSourceNames; }
        }

        private void LoadMetaData(Procedure procedure)
        {
            this.IsLoadingProcedureMetaData = false;
            if (procedure.IsMetaDataLoaded)
            {
                return;
            }

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += delegate(object sender, DoWorkEventArgs e)
            {
                try
                {
                    this.IsLoadingProcedureMetaData = true;
                    NotifyChanged("IsLoadingProcedureMetaData");
                    SchemaProvider.LoadMetaData(
                        procedure.Type.Schema, procedure);
                    if (procedure.Type.Schema == this.Schema &&
                        procedure == this.Procedure)
                    {
                        NotifyChanged("Procedure");
                        UpdateParameters();
                        this.IsLoadingProcedureMetaData = false;
                    }
                }
                catch (Exception ex)
                {
                    this.ErrorMessage = ex.Message;
                    this.IsLoadingProcedureMetaData = false;
                }
            };
            worker.RunWorkerAsync();
        }

        public void LoadSchema()
        {
            this.ErrorMessage = null;
            if (string.IsNullOrEmpty(this.DataSourceName))
            {
                this.ErrorMessage = "Must provide a data source name.";
                return;
            }
            try
            {
                this.Schema = SchemaProvider.Load(this.DataSourceName);
                NotifyChanged("KnownDataSourceNames");
                NotifyChanged("IsDataSourceCached");
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }

        public VhayuSettings Model { get; private set; }

        private void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Parameter_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            VhayuParameter parameter = (VhayuParameter)sender;
            int index = this.Parameters.IndexOf(parameter);
            VhayuParameter[] parameters = this.Model.InputParameters;
            parameters[index].Value = parameter.Value;
            this.Model.InputParameters = parameters;
            NotifyChanged("Sql");
        }

        public ObservableCollection<VhayuParameter>
            Parameters { get; private set; }

        public Procedure Procedure
        {
            get { return procedure; }
            set
            {
                if (value != procedure)
                {
                    if (isDSNChangeInProcess == false)
                    {
                        procedure = value;
                        this.Model.LastSelectedProcedure = value.ProcedureName;
                        NotifyChanged("Procedure");
                    }
                }
            }
        }

        public ProcedureType ProcedureType
        {
            get { return procedureType; }
            set
            {
                if (value != procedureType)
                {
                    if (isDSNChangeInProcess == false)
                    {
                        procedureType = value;
                        this.Model.LastSelectedProcedureType = value.TypeName;


                    }

                }
            }
        }

        public void ReloadSchema()
        {
            this.ErrorMessage = null;
            try
            {
                this.Schema = SchemaProvider.Reload(this.DataSourceName);
                NotifyChanged("IsDataSourceCached");
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }

        public void RemoveSchema()
        {
            this.ErrorMessage = null;
            try
            {
                this.Schema = null;
                SchemaProvider.ClearCache(this.DataSourceName);
                NotifyChanged("KnownDataSourceNames");
                NotifyChanged("IsDataSourceCached");
            }
            catch (Exception ex)
            {
                this.ErrorMessage = ex.Message;
            }
        }

        public void ResetSql()
        {
            this.Sql = null;
        }

        public VhayuSchema Schema
        {
            get { return schema; }
            set
            {
                if (value != schema)
                {
                    schema = value;
                    NotifyChanged("Schema");
                    NotifyChanged("HasSchema");
                    NotifyChanged("HasNoSchema");
                    NotifyChanged("HasNoSchemaButDataSourceName");
                }
            }
        }

        public string Sql
        {
            get
            {
                if (this.Model.Sql != null)
                {
                    return this.Model.Sql;
                }
                return GenerateSql();
            }
            set
            {
                if (value != this.Model.Sql)
                {
                    this.Model.Sql = value;
                    NotifyChanged("Sql");
                    NotifyChanged("HasExplicitSql");
                    NotifyChanged("HasNoExplicitSql");
                }
            }
        }

        private void SynchronizeParameterValues()
        {
            if (procedureNameFromModel != null &&
                procedureNameFromModel.Equals(this.Procedure.ProcedureName) &&
                this.Procedure.IsMetaDataLoaded)
            {
                int count = this.Model.InputParameters.Length;
                for (int i = 0; i < count; i++)
                {
                    this.Parameters[i].Value =
                        this.Model.InputParameters[i].Value;
                }
            }
            else
            {
                procedureNameFromModel = null;
                this.Model.InputParameters = this.Parameters.ToArray();
            }
        }

        private void this_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            if (isLastSelectedProcedureLoaded && isDSNChangeInProcess)
            {
                return;
            }

            bool sql = false;
            if ("Procedure".Equals(e.PropertyName))
            {
                if (!this.Procedure.ProcedureName.Equals(
                    this.Model.ProcedureName))
                {
                    this.Model.ProcedureName = this.Procedure.ProcedureName;
                    LoadMetaData(this.Procedure);
                    UpdateParameters();
                    sql = true;
                }
            }
            if ("Parameters".Equals(e.PropertyName))
            {
                SynchronizeParameterValues();
                sql = true;
            }
            if ("GuessTimeType".Equals(e.PropertyName))
            {
                UpdateParameters();
                sql = true;
            }
            if ("TimeZone".Equals(e.PropertyName))
            {
                UpdateParameters();
                sql = true;
            }
            if (sql)
            {
                NotifyChanged("Sql");
            }
        }

        private delegate void UpdateParameterDelegate();

        private void UpdateParameters()
        {
            if (dispatcher.Thread != Thread.CurrentThread)
            {
                dispatcher.Invoke(
                    new UpdateParameterDelegate(UpdateParameters), null);
                return;
            }

            bool changed = false;
            foreach (VhayuParameter parameter in this.Parameters)
            {
                parameter.PropertyChanged -= Parameter_PropertyChanged;
                changed = true;
            }
            this.Parameters.Clear();
            if (this.Procedure.IsMetaDataLoaded)
            {
                foreach (Field field in this.Procedure.Inputs)
                {
                    VhayuParameter parameter = VhayuParameter.FromField(
                        field, this.GuessTimeType);
                    parameter.PropertyChanged += Parameter_PropertyChanged;
                    this.Parameters.Add(parameter);
                    changed = true;
                }
                this.Model.OutputParameters = this.Procedure.Outputs
                    .Select(field => VhayuParameter.FromField(
                        field, this.GuessTimeType))
                    .ToArray();
            }
            if (changed)
            {
                NotifyChanged("Parameters");
                NotifyChanged("HasParameters");
            }
        }

        public string TimeZone
        {
            get { return this.Model.TimeZone; }
            set
            {
                if (value != this.Model.TimeZone)
                {
                    this.Model.TimeZone = value;
                    NotifyChanged("TimeZone");
                }
            }
        }

        public string[] TimeZones
        {
            get
            {
                #region TimeZones
                return new string[]
                {
                    "",
                    "PST",
                    "AFST",
                    "ALST",
                    "ARST",
                    "ARNST",
                    "ARCST",
                    "ARGST",
                    "ARMST",
                    "AST",
                    "AUCST",
                    "AUEST",
                    "AZBST",
                    "AZST",
                    "BNGST",
                    "CCST",
                    "CVST",
                    "CACST",
                    "CAUST",
                    "CAST",
                    "CASST",
                    "CBST",
                    "CEST",
                    "CENST",
                    "CPST",
                    "CST",
                    "CSTMX",
                    "CHST",
                    "DST",
                    "EAST",
                    "EAUST",
                    "EEST",
                    "ESAST",
                    "EST",
                    "EGST",
                    "EKST",
                    "FJST",
                    "FLST",
                    "GEST",
                    "GST",
                    "GLST",
                    "GMT",
                    "GTBST",
                    "HST",
                    "INST",
                    "IRST",
                    "ISST",
                    "JORST",
                    "KAMST",
                    "KOST",
                    "MAGST",
                    "MRIST",
                    "MXST",
                    "MXST2",
                    "MAST",
                    "MEST",
                    "MOST",
                    "MORST",
                    "MST",
                    "MSTMX",
                    "MYST",
                    "NCAST",
                    "NAMST",
                    "NEPST",
                    "NZST",
                    "NFST",
                    "NAEST",
                    "NAST",
                    "PSAST",
                    "PSTMX",
                    "PAKST",
                    "PARST",
                    "RST",
                    "RUST",
                    "SAEST",
                    "SAPST",
                    "SAWST",
                    "SAMST",
                    "SEAST",
                    "SIST",
                    "SAST",
                    "SLST",
                    "SYRST",
                    "TAIST",
                    "TASST",
                    "TOKST",
                    "TNGST",
                    "ULAST",
                    "USEST",
                    "USMST",
                    "UTC",
                    "UTCP12",
                    "UTCM02",
                    "UTCM11",
                    "VENST",
                    "VLST",
                    "WAUST",
                    "WCAST",
                    "WEUST",
                    "WAST",
                    "WPST",
                    "YAKST"           
                };

                #endregion
            }
        }

        private void SetLastSelectedProcedureType()
        {
            if (this.Schema == null)
            {
                return;
            }

            if (schema.ProcedureTypes.Where(pt => pt.TypeName ==
                this.Model.LastSelectedProcedureType).FirstOrDefault() != null)
            {
                procedureType = schema.ProcedureTypes.Where(pt => pt.TypeName ==
                this.Model.LastSelectedProcedureType).FirstOrDefault();
                NotifyChanged("ProcedureType");
                SetLastSelectedProcedure();
            }

        }

        private void SetLastSelectedProcedure()
        {
            Procedure procedure = this.Schema.AllProcedures
                .FirstOrDefault(p => p.ProcedureName.Equals(
                    this.Model.LastSelectedProcedure));
            if (procedure == null)
            {
                this.procedure = this.Schema.AllProcedures.First();
                isLastSelectedProcedureLoaded = false;

                NotifyChanged("Procedure");
            }
            else
            {
                this.procedure = procedure;
                isLastSelectedProcedureLoaded = true;
                NotifyChanged("Procedure");

            }
        }
    }
}
