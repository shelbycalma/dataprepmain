﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.VhayuPlugin.Properties;

namespace Panopticon.VhayuPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, VhayuSettings>
    {
        private const string imageUri = "pack://application:,,,/" +
            "Panopticon.VhayuPlugin.UI;component/vhayu-plugin.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception) { }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            VhayuSettings settings = Plugin.CreateSettings(bag);

            return new VhayuConnectionPanel()
            {
                Settings = new VhayuSettingsViewModel(settings)
            };
        }

        public override PropertyBag GetSetting(object element)
        {
            VhayuConnectionPanel panel = (VhayuConnectionPanel)element;

            return panel.Settings.Model.ToPropertyBag();
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            VhayuSettings settings = new VhayuSettings();
            VhayuSettingsViewModel settingsView =
                new VhayuSettingsViewModel(settings);
            VhayuConnectionWindow window =
                new VhayuConnectionWindow(settingsView);

            window.Owner = owner;
            
            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            string title = Resources.UiDefaultConnectionTitle;
            if (settingsView.Procedure != null)
            {
                title = settingsView.Procedure.DisplayName;
                if (string.IsNullOrEmpty(title))
                {
                    title = settingsView.Procedure.ProcedureName;
                }
            }
            settings.Title = title;

            PropertyBag properties = settings.ToPropertyBag();
            return properties;
        }
    }
}
