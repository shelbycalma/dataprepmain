﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using Panopticon.VhayuPlugin.Schema;

namespace Panopticon.VhayuPlugin.UI
{
    internal partial class VhayuConnectionPanel : UserControl
    {
        public VhayuConnectionPanel()
        {
            InitializeComponent();

            loadSchemaButton.Click +=
                delegate(object sender, RoutedEventArgs e) {
                    this.Settings.LoadSchema();
                };
            reloadSchemaButton.Click +=
                delegate(object sender, RoutedEventArgs e) {
                    this.Settings.ReloadSchema();
                };
            removeSchemaButton.Click +=
                delegate(object sender, RoutedEventArgs e) {
                    this.Settings.RemoveSchema();
                };

            resetSqlButton.Click +=
                delegate(object sender, RoutedEventArgs e) {
                    this.Settings.ResetSql();
                };
        }

        public VhayuSettingsViewModel Settings
        {
            get { return (VhayuSettingsViewModel) this.DataContext; }
            set {
                this.DataContext = (object) value;
                entryTabs.SelectedIndex = value.HasExplicitSql ? 1 : 0;
            }
        }
    }
}
