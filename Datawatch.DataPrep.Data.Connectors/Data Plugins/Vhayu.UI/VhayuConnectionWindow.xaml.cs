﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.VhayuPlugin.UI
{
    internal partial class VhayuConnectionWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(VhayuConnectionWindow));

        public VhayuConnectionWindow(VhayuSettingsViewModel settings)
        {
            InitializeComponent();

            connectionPanel.Settings = settings;
        }

        public bool IsOkButtonEnabled
        {
            get { return true; }
        }

        /// <summary>
        /// Checks if the Ok command can be executed. 
        /// If the ConfigPanel exists and it's IsOK property is true, the
        /// command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (connectionPanel.Settings == null)
            {
                e.CanExecute = false;
                return;
            }
            e.CanExecute = connectionPanel.Settings.IsOK;
        }

        /// <summary>
        /// Executes the Ok command, setting the DialogResult to true.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
            Close();
        }
    }
}
