﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.VerticaPlugin.UI
{
    public class PluginUI : OdbcPluginUIBase<Plugin, VerticaConnectionSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(VerticaConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new DatabaseConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            DatabaseConfigPanel panel = (DatabaseConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
