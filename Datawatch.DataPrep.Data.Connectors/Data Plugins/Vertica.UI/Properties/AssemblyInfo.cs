﻿using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Panopticon.VerticaPlugin.UI.dll")]
[assembly: AssemblyDescription("Panopticon Vertica Plug-in UI")]