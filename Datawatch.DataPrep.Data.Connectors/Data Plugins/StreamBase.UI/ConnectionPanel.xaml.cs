﻿namespace Panopticon.StreamBasePlugin.UI
{
    internal partial class ConnectionPanel
    {
        public ConnectionPanel(ConnectionViewModel viewModel)
            : this()
        {
            DataContext = viewModel;
        }

        public ConnectionPanel()
        {
            InitializeComponent();                        
        }
    }
}
