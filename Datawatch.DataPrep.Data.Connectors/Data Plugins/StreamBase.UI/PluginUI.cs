﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.StreamBasePlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, StreamBaseSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.StreamBasePlugin.UI;component" +
            "/streambase-small.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            StreamBaseSettings con = new StreamBaseSettings();
            ConnectionViewModel vm = new ConnectionViewModel(con, parameters);
            ConnectionWindow win = new ConnectionWindow(vm);
            win.Owner = owner;
            
            if (win.ShowDialog() == true)
            {
                PropertyBag bag = con.ToPropertyBag();
                return bag;
            }
            return null;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new ConnectionViewModel(
                new StreamBaseSettings(bag), parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return ((ConnectionViewModel)cp.DataContext).Connection.ToPropertyBag();
        }
    }
}
