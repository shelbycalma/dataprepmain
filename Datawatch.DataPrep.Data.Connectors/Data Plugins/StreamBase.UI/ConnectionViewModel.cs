﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.UI.Mediator;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.StreamBasePlugin.UI
{
    public class ConnectionViewModel : ViewModelBase
    {
        private StreamBaseSettings connection;
        private ICommand okCommand;
        private ICommand fetchStreamsCommand;
        private IEnumerable<ParameterValue> parameters;

        public ConnectionViewModel(StreamBaseSettings connection, 
            IEnumerable<ParameterValue> parameters)
        {
            Mediator.Instance.Register(this);
            this.connection = connection;
            this.parameters = parameters;
        }

        public StreamBaseSettings Connection
        {
            get { return connection; }
            set
            {
                if (value != connection)
                {
                    connection = value;
                    OnPropertyChanged("Connection");
                }
            }
        }

        public string[] AvailableStreams
        {
            get
            {
                if (string.IsNullOrEmpty(connection.PrimaryURL))
                {
                    return new string[] { };
                }

                string[] streams =
                    StreamBaseClientUtil.GetOutputStreams(connection, parameters);
                    
                if (string.IsNullOrEmpty(Stream) && streams.Length > 0)
                {
                    Stream = streams[0];
                }
                return streams;
            }
        }

        public string Stream
        {
            get { return connection.Stream; }
            set
            {
                if (connection.Stream != value)
                {
                    connection.Stream = value;
                    connection.UpdateIdCandidates(parameters);
                    OnPropertyChanged("Stream");
                }
            }
        }

        public string TimeZone
        {

            get { return connection.TimeZone; }
            set
            {
                if (connection.TimeZone != value)
                {
                    connection.TimeZone = value;
                }
            }
        }

        public ICommand OkCommand
        {
            get
            {
                if (okCommand == null)
                {
                    okCommand = new DelegateCommand(Ok, CanOk);
                }
                return okCommand;
            }
        }

        private void Ok()
        {
            Mediator.Instance.NotifyColleagues(
                "Panopticon.StreamBasePlugin.CloseDialog", 
                new CloseDialogArgs(this, true));
        }

        private bool CanOk()
        {
            return !string.IsNullOrEmpty(connection.PrimaryURL) &&
                !string.IsNullOrEmpty(connection.Stream) &&
                !string.IsNullOrEmpty(connection.IdColumn);
        }

        public ICommand FetchStreamsCommand
        {
            get
            {
                if (fetchStreamsCommand == null)
                {
                    fetchStreamsCommand = new DelegateCommand(FetchStreams,
                        CanFetchStreams);
                }
                return fetchStreamsCommand;
            }
        }

        private void FetchStreams()
        {
            try
            {
                OnPropertyChanged("AvailableStreams");
                connection.UpdateIdCandidates(parameters);
            }
            catch (Exception exc)
            {
                Log.Exception(exc);
                MessageBox.Show(exc.Message, "StreamBase",
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanFetchStreams()
        {
            return !string.IsNullOrEmpty(connection.PrimaryURL);
        }
        
        public string[] TimeZones
        {
            get
            {
                #region TimeZones
                return new string[]
                {
                    "",
                    "Greenwich Standard Time",
                    "GMT Standard Time",
                    "W. Europe Standard Time",
                    "Central Europe Standard Time",
                    "Central European Standard Time",
                    "W. Central Africa Standard Time",
                    "E. Europe Standard Time",
                    "Egypt Standard Time",
                    "South Africa Standard Time",
                    "Israel Standard Time",
                    "Russian Standard Time",
                    "E. Africa Standard Time",
                    "Iran Standard Time",
                    "Arabian Standard Time",
                    "Caucasus Standard Time",
                    "West Asia Standard Time",
                    "India Standard Time",
                    "N. Central Asia Standard Time",
                    "Central Asia Standard Time",
                    "Sri Lanka Standard Time",
                    "SE Asia Standard Time",
                    "North Asia Standard Time",
                    "China Standard Time",
                    "North Asia East Standard Time",
                    "Singapore Standard Time",
                    "W. Australia Standard Time",
                    "Taipei Standard Time",
                    "Tokyo Standard Time",
                    "Korea Standard Time",
                    "Yakutsk Standard Time",
                    "Cen. Australia Standard Time",
                    "E. Australia Standard Time",
                    "West Pacific Standard Time",
                    "Tasmania Standard Time",
                    "Vladivostok Standard Time",
                    "Central Pacific Standard Time",
                    "New Zealand Standard Time",
                    "E. South America Standard Time",
                    "SA Eastern Standard Time",
                    "Greenland Standard Time",
                    "Newfoundland Standard Time",
                    "Atlantic Standard Time",
                    "SA Western Standard Time",
                    "Pacific SA Standard Time",
                    "SA Pacific Standard Time",
                    "Eastern Standard Time",
                    "US Eastern Standard Time",
                    "Central America Standard Time",
                    "Central Standard Time",
                    "Mountain Standard Time",
                    "Pacific Standard Time",
                    "Alaskan Standard Time",
                    "Hawaiian Standard Time",
                    "Samoa Standard Time",
                    "Dateline Standard Time"
                };

                #endregion
            }
        }
    }
}
