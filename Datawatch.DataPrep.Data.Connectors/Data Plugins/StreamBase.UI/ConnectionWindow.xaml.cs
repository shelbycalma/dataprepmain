﻿using Datawatch.DataPrep.Data.Core.UI.Mediator;

namespace Panopticon.StreamBasePlugin.UI
{
    public partial class ConnectionWindow
    {

        public ConnectionWindow(ConnectionViewModel connection)
        {
            Mediator.Instance.Register(this);
            InitializeComponent();
            DataContext = connection;
        }

        [MediatorMessageSink("Panopticon.StreamBasePlugin.CloseDialog", 
            ParameterType = typeof(CloseDialogArgs))]
        public void CloseDialog(CloseDialogArgs args)
        {
            if (args.ViewModel != DataContext) return;
            DialogResult = args.Result;
            Close();
        }
    }
}
