﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Panopticon.StreamBasePlugin.UI
{
    public class CloseDialogArgs
    {
        public readonly ConnectionViewModel ViewModel;
        public readonly bool Result;

        public CloseDialogArgs(ConnectionViewModel vm, bool result)
        {
            ViewModel = vm;
            Result = result;
        }
    }
}
