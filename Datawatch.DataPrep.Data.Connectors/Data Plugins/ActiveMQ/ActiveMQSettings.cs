﻿using System.Collections.Generic;
using System.Xml;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.ActiveMQPlugin
{
    public class ActiveMQSettings : MessageQueueSettingsBase
    {
        public ActiveMQSettings(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public ActiveMQSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
        }

        public ActiveMQSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        public bool IsDurableConsumer
        {
            get
            {
                string defaultValue = XmlConvert.ToString(true);
                return XmlConvert.ToBoolean(
                    GetInternal("IsDurableConsumer", defaultValue));
            }
            set
            {
                SetInternal("IsDurableConsumer",
                    XmlConvert.ToString(value));
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return "Xml"; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is ActiveMQSettings)) return false;

            if (!base.Equals(obj)) return false;

            ActiveMQSettings cs = (ActiveMQSettings)obj;

            if (IsDurableConsumer != cs.IsDurableConsumer)
            {
                return false;
            }
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            object[] fields = { IsDurableConsumer };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public override void DoGenerateColumns()
        {
            StreamingColumnGenerator columnGenerator = 
                new StreamingColumnGenerator(this, this.errorReporter);

            // Set up connection to ActiveMQ
            ActiveMQListener listener = new ActiveMQListener(columnGenerator,
                Parameters);
            listener.Connect(Broker, UserName, Password);
            listener.ListenTo(Topic, IsDurableConsumer);

            // Start receiving data to generate columns
            columnGenerator.Generate();

            listener.Disconnect();
        }

        public override bool CanGenerateColumns()
        {
            return !string.IsNullOrEmpty(Topic) && !string.IsNullOrEmpty(Broker);
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }
    }
}
