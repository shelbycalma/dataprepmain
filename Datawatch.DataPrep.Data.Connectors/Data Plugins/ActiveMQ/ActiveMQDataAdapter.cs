﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;

//------------------------------------------------//
// Some instructions to get started with ActiveMQ //
//------------------------------------------------//
//
// Download and unzip the ActiveMQ broker:
//
// http://apache.dataphone.se//activemq/apache-activemq/5.4.1/apache-activemq-5.4.1-bin.zip
//
// Now open a console window, navigate to the unzipped folder and start the
// broker (it’s important to start the broker from the top directory so do not
// double click the broker exe):
//
//  C:\apache-activemq-5.4.1>bin\activemq
//
// The broker also starts a simple web server at port 8161, it lets you run a
// demo to use as a data source so navigate your browser to the
// Market Data Publisher example:
//
// http://localhost:8161/demo/portfolioPublish?count=1&refresh=2&stocks=IBMW&stocks=BEAS&stocks=MSFT&stocks=SUNW
//
// If you look at the broker console window you can see how the xml data sent
// is structured:
//
//  INFO | PortfolioPublishServlet: Sending:
//  <price stock='MSFT' bid='26.046312939047425'
//  offer='26.07235925198647'
//  movement='down'/>
//  on destination: topic://STOCKS.MSFT
//
// Now, assuming you’ve done your XPath homework you can tell that we can setup
// the following expressions to retrieve the relevant data (these go into the
// ActiveMQ Data Source Settings dialog):
//
//  Id	Name	XPath	        Type
//      Bid	    price/@bid	    Numeric
//      Offer	price/@offer	Numeric
//  X	Stock	price/@stock	Text
//
// Two more things need to go into the ActiveMQ settings dialog, note that we
// don’t specify a username or password and that the demo sends each stock on a
// separate topic so listen to the MSFT topic:
//
//  Broker:	tcp://0.0.0.0:61616
//  Topic:	topic://STOCKS.MSFT
//
// If you want to listen to all topics you can specify a wildcard:
//
//  Topic:	topic://STOCKS.*
//
// That’s all there is to it! Just create a treemap with stock as breakdown,
// bid/offer as size/color and go into Presentation Mode. The treemap should
// show the MSFT node with changing values.
//
// Godspeed

namespace Panopticon.ActiveMQPlugin
{
    public class ActiveMQDataAdapter : MessageQueueAdapterBase<ParameterTable,
        ActiveMQSettings, Dictionary<string, object>>
    {
        private ActiveMQListener listener;

        public override void Start()
        {
            settings.Parameters = table.Parameters;
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();
            listener = new ActiveMQListener(this, table.Parameters);

            // Connect
            listener.Connect(settings.Broker, settings.UserName,
                settings.Password);
            // Subscribe
            listener.ListenTo(settings.Topic, settings.IsDurableConsumer);
        }

        public override void Stop()
        {
            base.Stop();
            listener.Disconnect();
            listener = null;
        }
    }
}
