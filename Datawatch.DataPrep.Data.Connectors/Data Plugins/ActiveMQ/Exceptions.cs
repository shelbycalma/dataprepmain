﻿using System;
using Panopticon.ActiveMQPlugin.Properties;

namespace Panopticon.ActiveMQPlugin
{
    class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        internal static Exception MalformedBrokerUrl(string url,
            Exception innerException)
        {
            return LogException(new InvalidOperationException(
                Resources.ExMalformedBrokerUrl,
                innerException));
        }

        internal static Exception ConnectionError(Exception innerException)
        {
            return LogException(new InvalidOperationException(
                Resources.ExConnectionError,
                innerException));
        }

        internal static Exception NoColumns()
        {
            return CreateInvalidOperation(Resources.ExNoColumns);
        }

        internal static Exception NoIdColumn()
        {
            return CreateInvalidOperation(Resources.ExNoIdColumn);
        }
    }
}
