﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.ActiveMQPlugin
{
    /// <summary>
    /// DataPlugin for ActiveMQ.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<ParameterTable, ActiveMQSettings,
                       Dictionary<string, object>, ActiveMQDataAdapter>
    {
        internal const string PluginId = "ActiveMQPlugin";
        internal const string PluginTitle = "Active MQ";
        internal const string PluginType = DataPluginTypes.Streaming;
        internal const bool PluginIsObsolete = false;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override ActiveMQSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new ActiveMQSettings(pluginManager, parameters);
        }

        public override ActiveMQSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new ActiveMQSettings(pluginManager, bag, null);
        }
    }
}
