﻿using System;
using System.Collections.Generic;
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Apache.NMS.ActiveMQ.Commands;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Panopticon.ActiveMQPlugin.Properties;

namespace Panopticon.ActiveMQPlugin
{
    class ActiveMQListener
    {
        private ISession session;
        private IConnection connection;
        private ITopic topic;
        private IMessageConsumer consumer;
        private readonly IMessageHandler messageReceiver;
        private readonly IEnumerable<ParameterValue> parameters;

        public ActiveMQListener(IMessageHandler messageReceiver, 
            IEnumerable<ParameterValue> parameters)
        {
            if (messageReceiver == null)
            {
                throw Exceptions.DataAdaptorWasNull();
            }
            this.messageReceiver = messageReceiver;
            this.parameters = parameters;
        }

        public void Connect(string broker, string username, string password)
        {
            broker = DataUtils.ApplyParameters(broker, parameters);
            username = DataUtils.ApplyParameters(username, parameters);
            password = DataUtils.ApplyParameters(password, parameters);

            // Parse the broker
            Uri brokerUri = null;
            try
            {
                brokerUri = new Uri(broker);
            }
            catch (FormatException ex)
            {
                throw Exceptions.MalformedBrokerUrl(broker, ex);
            }

            // Setup the connection
            IConnectionFactory factory = new ConnectionFactory(brokerUri);
            connection = null;
            try
            {
                if (string.IsNullOrEmpty(username) ||
                    string.IsNullOrEmpty(password))
                {
                    connection = factory.CreateConnection();
                }
                else
                {
                    connection = factory.CreateConnection(username, password);
                }
            }
            catch (NMSConnectionException ex)
            {
                throw Exceptions.ConnectionError(ex);
            }

            // Hook up some logging, just because its nice
            connection.ConnectionInterruptedListener +=
                ConnectionInterruptedListener;
            connection.ConnectionResumedListener += ConnectionResumedListener;
            connection.ExceptionListener += ExceptionListener;

            // Start the connection
            connection.RequestTimeout = TimeSpan.FromSeconds(10);
            try
            {
                connection.Start();
            }
            catch (IOException ex)
            {
                throw Exceptions.ConnectionError(ex);
            }
            Log.Info(string.Format(Resources.LogConnected, broker));

            // Create the session
            session = connection.CreateSession(
                AcknowledgementMode.AutoAcknowledge);
        }

        public void Disconnect()
        {
            try
            {
                if (consumer != null)
                {
                    consumer.Close();
                    consumer.Dispose();
                    consumer = null;
                }

                if (session != null)
                {
                    session.Close();
                    session.Dispose();
                    session = null;
                }

                if (connection != null)
                {
                    connection.Stop();
                    connection.Close();
                    connection.Dispose();
                    connection = null;
                }
            }
            catch (NMSException ex)
            {
                // I dont really know what could go wrong here, catch the
                // internal base exception and log everything.
                Log.Error(string.Format(Resources.LogInternalError,
                    ex.Message));
                Log.Exception(ex);
            }

            topic = null;

            Log.Info(Resources.LogDisconnected);
        }

        public void ListenTo(string topicName,
            bool isDurableConsumer)
        {
            topicName = DataUtils.ApplyParameters(topicName,
                parameters);
            topicName = ParseTopic(topicName);
            topic = new ActiveMQTopic(topicName);
            if (isDurableConsumer)
            {
                string clientName = "ActiveMQ" + Guid.NewGuid();
                consumer = session.CreateDurableConsumer(topic, clientName,
                            null, false);
            }
            else
            {
                consumer = session.CreateConsumer(topic);
            }
            consumer.Listener += ConsumerListener;
            Log.Info(string.Format(Resources.LogListeningTo, topicName));
        }

        public void ConnectionInterruptedListener()
        {
            // TODO do we need to call updater.ConnectionLost(true); ?
            Log.Warning(Resources.LogConnectionInterrupted);
        }

        public void ConnectionResumedListener()
        {
            Log.Warning(Resources.LogConnectionResumed);
        }

        public void ExceptionListener(Exception ex)
        {
            // TODO do we need to call updater.ConnectionLost(true); ?
            Log.Error(string.Format(Resources.LogInternalError,
                ex.Message));
            Log.Exception(ex);
        }

        public void ConsumerListener(IMessage message)
        {
            if (message is ITextMessage)
            {
                string xml = ((ITextMessage)message).Text;
                messageReceiver.MessageReceived(xml);
            }
            else
            {
                Log.Warning(Resources.LogNotTextMessage);
            }
        }
        
        private static string ParseTopic(string source)
        {
            // Some hardcoded cleaning of the string here, seems easier than
            // trying to communicate in the GUI that the user shouldnt enter
            // the topic part.
            string topicName = source.StartsWith("topic://")
                ? source.Substring("topic://".Length) : source;
            return topicName;
        }
    }
}
