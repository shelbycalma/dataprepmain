﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using IRow = NPOI.SS.UserModel.IRow;

namespace Panopticon.ExcelNpoiPlugin
{
    using Panopticon.ExcelNpoiPlugin.Properties;
    using Datawatch.DataPrep.Data.Framework;

    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ExcelSettings>, ISchemaSavingPlugin
    {
        public const string PathKey = "path";
        public const string RangeKey = "range";
        public const string UnpivotKey = "unpivot";

        internal const string PluginId = "b3fc1120-32d7-45e9-aaa0-cf31c93104a1";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "MS Excel";
        internal const string PluginType = DataPluginTypes.File;

        private const ColumnType UnknownColumnType = (ColumnType)(-1);
        private bool disposed;

        public Plugin()
        {
            AppDomain.CurrentDomain.AssemblyResolve += CurrentDomainOnAssemblyResolve;
        }

        ~Plugin()
        {
            Dispose(false);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public static string GetTitle(string path, string range)
        {
            range = RemoveEncapsChars(range);

            int index = range.LastIndexOf('$');
            string rangeDesc = range;

            if (index != -1)
            {
                rangeDesc = RemoveEncapsChars(range.Substring(0, index));

                if (index < range.Length - 1)
                {
                    string subDesc =
                        range.Substring(index + 1, range.Length - index - 1);
                    rangeDesc += string.Format(
                        " ({0})", RemoveEncapsChars(subDesc));
                }
            }

            if (path != null)
            {
                return string.Format("{0} - {1}",
                    Path.GetFileName(path), rangeDesc);
            }
            return rangeDesc;
        }

        public override ExcelSettings CreateSettings(PropertyBag bag)
        {
            ExcelSettings settings = new ExcelSettings(bag);
            if (string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = Properties.Resources.UiPluginTitle;
            }
            return settings;
        }

        public override ITable GetAllData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters,
                -1, false);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
                int rowcount)
        {
            return GetData(workbookDir, dataDir, settings, parameters,
                rowcount, true);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag propertyBag, IEnumerable<ParameterValue> parameters,
                int rowcount, bool applyRowFilteration)
        {
            return GetDataInternal(workbookDir, dataDir, propertyBag, parameters,
                rowcount, applyRowFilteration, false);
        }

        private ITable GetDataInternal(string workbookDir, string dataDir,
            PropertyBag propertyBag, IEnumerable<ParameterValue> parameters,
                int rowcount, bool applyRowFilteration, bool schemaRequest)
        {
            CheckLicense();

            ExcelSettings excelSettings = CreateSettings(propertyBag);
            excelSettings.SchemaRequest = schemaRequest;

            string path = excelSettings.Path;
            string range = excelSettings.Range;
            range = range == null ? string.Empty : RemoveEncapsChars(range);

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try
            {
                if (!Path.IsPathRooted(path))
                {
                    path = Path.Combine(workbookDir, path);
                }

                if (!File.Exists(path))
                {
                    string fileName = Path.GetFileName(path);
                    string dataPath = Path.Combine(dataDir, fileName);
                    if (File.Exists(dataPath))
                    {
                        path = Path.GetFullPath(dataPath);
                        excelSettings.Path = path;
                    }
                }

                IWorkbook workbook;
                using (FileStream stream = new FileStream(path, FileMode.Open,
                    FileAccess.Read, FileShare.ReadWrite))
                {
                    workbook = WorkbookFactory.Create(stream);
                }
                string[] sheetNameAndNameName = range.Split('$');
                string sheetName = null;
                string nameName = null;

                if (range.Contains("$"))
                {
                    sheetName = sheetNameAndNameName[0];
                    if (sheetNameAndNameName.Length == 2)
                    {
                        nameName = sheetNameAndNameName[1];
                    }
                }
                else
                {
                    nameName = sheetNameAndNameName[0];
                }

                ISheet sheet;
                int rowStart = -1;
                int rowEnd = -1;
                int columnStart = -1;
                int columnEnd = -1;
                if (string.IsNullOrEmpty(nameName))
                {
                    sheet = workbook.GetSheet(sheetName);
                    if (sheet == null) throw Datawatch.DataPrep.Data.Core.Exceptions.IncorrectSettingValue("Sheet");
                    rowStart = sheet.FirstRowNum;
                    rowEnd = sheet.LastRowNum;
                    IRow firstPoiRow =
                        sheet.GetRow(rowStart);
                    if (firstPoiRow != null)
                    {
                        columnStart = firstPoiRow.FirstCellNum;
                        columnEnd = firstPoiRow.LastCellNum - 1;
                    }
                }
                else
                {
                    int sheetIndex = sheetName != null
                                         ? workbook.GetSheetIndex(sheetName)
                                         : -1;
                    int numberOfNames = workbook.NumberOfNames;
                    IName name = null;
                    for (int i = 0; i < numberOfNames; i++)
                    {
                        IName tempName = workbook.GetNameAt(i);
                        if (nameName.Equals(tempName.NameName) &&
                            sheetIndex == tempName.SheetIndex)
                        {
                            name = tempName;
                            break;
                        }
                    }
                    sheet = workbook.GetSheet(name.SheetName);
                    string formula = name.RefersToFormula;
                    AreaReference areaReference = new AreaReference(formula);
                    CellReference firstPoiCell = areaReference.FirstCell;
                    CellReference lastPoiCell = areaReference.LastCell;
                    if (firstPoiCell != null && lastPoiCell != null)
                    {
                        rowStart = firstPoiCell.Row;
                        rowEnd = lastPoiCell.Row;
                        columnStart = firstPoiCell.Col;
                        columnEnd = lastPoiCell.Col;
                    }
                }

                IFormulaEvaluator evaluator =
                        workbook.GetCreationHelper().CreateFormulaEvaluator();
                if (rowStart >= 0 && rowEnd >= 0 &&
                    columnStart >= 0 && columnEnd >= 0)
                {
                    DateTime start = DateTime.Now;

                    PopulateTable(table, excelSettings, rowcount, parameters, sheet,
                        rowStart, rowEnd, columnStart, columnEnd, evaluator,
                        applyRowFilteration);

                    DateTime end = DateTime.Now;
                    double seconds = (end - start).TotalSeconds;
                    Log.Info(Resources.LogPopulateTableCompleted,
                        table.RowCount, table.ColumnCount, seconds);
                }
                else
                {
                    CreateTableFromSchema(table, excelSettings);
                }
            }
            finally
            {
                table.EndUpdate();
            }

            string unpivot = excelSettings.Unpivot;
            if (unpivot != null && Boolean.Parse(unpivot))
            {
                const string keyColumnName = "Id";
                string numericColumnName = GetTitle(null, range);
                return ExcelSourceTransform(table, keyColumnName, numericColumnName);
            }

            return table;
        }

        private void CreateTableFromSchema(StandaloneTable table, 
            ExcelSettings settings)
        {
            if (settings.SchemaRequest)
            {
                return;
            }

            List <DatabaseColumn> schemaColumns =
                settings.SchemaColumnsSettings.SchemaColumns;
            if (schemaColumns == null || schemaColumns.Count <= 0)
            {
                return;
            }
            for (int i = 0; i < schemaColumns.Count; i++)
            {
                DatabaseColumn col = schemaColumns[i];
                AddColumn(table, col.ColumnName, col.ColumnType);
            }
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            string path = settings.Values[PathKey];
            return path != null ? new[] { path } : null;
        }

        public string[] GetRanges(string path)
        {
            IWorkbook workbook;
            using (FileStream stream = new FileStream(path, FileMode.Open,
                FileAccess.Read, FileShare.ReadWrite))
            {
                workbook = WorkbookFactory.Create(stream);
            }
            if (workbook == null)
            {
                return null;
            }

            List<string> result = new List<string>();
            for (int i = 0; i < workbook.NumberOfNames; i++)
            {
                var name = workbook.GetNameAt(i);
                if (name.SheetIndex == -1)
                {
                    result.Add(name.NameName);
                }
                else
                {
                    result.Add(name.SheetName + "$" + name.NameName);
                }
            }
            for (int i = 0; i < workbook.NumberOfSheets; i++)
            {
                result.Add(workbook.GetSheetAt(i).SheetName + '$');
            }

            return result.ToArray();
        }
        
        public void SaveSchema(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {

            GetDataInternal(workbookDir, dataDir, settings, parameters,
                0, false, true);
        }

        public string GetTitle(PropertyBag settings)
        {
            return GetTitle(
                settings.Values[PathKey], settings.Values[RangeKey]);
        }

        public override void UpdateFileLocation(PropertyBag settings, string oldPath,
            string newPath, string workbookPath)
        {
            if (Path.GetFileName(settings.Values[PathKey]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values[PathKey] =
                    Datawatch.DataPrep.Data.Core.Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    AppDomain.CurrentDomain.AssemblyResolve -= CurrentDomainOnAssemblyResolve;
                }
                disposed = true;
            }
        }

        private static Column AddColumn(ICell headerCell, ColumnType columnType,
            StandaloneTable table)
        {
            string columnName = null;
            if (headerCell != null &&
                    headerCell.CellType == CellType.String)
            {
                columnName = headerCell.StringCellValue;
            }
            else if (headerCell != null &&
                    headerCell.CellType == CellType.Numeric)
            {
                columnName = headerCell.NumericCellValue.ToString();
            }
            if (columnName == null)
            {
                columnName = "F" + (table.ColumnCount + 1);
            }
            string tempColumnName = columnName;
            int duplicateNameCount = 0;
            while (table.ContainsColumn(tempColumnName))
            {
                duplicateNameCount++;
                tempColumnName = columnName + duplicateNameCount;
            }
            return AddColumn(table, tempColumnName, columnType);
        }

        private static Column AddColumn(StandaloneTable table, string columnName, ColumnType columnType)
        {
            Column column = null;
            switch (columnType)
            {
                case ColumnType.Numeric:
                    {
                        column = new NumericColumn(columnName);
                        break;
                    }
                case ColumnType.Text:
                    {
                        column = new TextColumn(columnName);
                        break;
                    }
                case ColumnType.Time:
                    column = new TimeColumn(columnName);
                    break;
            }
            table.AddColumn(column);
            return column;
        }

        private static ITable ExcelSourceTransform(ITable sourceTable,
            string keyColumnName, string numericColumnName)
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            try
            {
                TextColumn keyColumn = new TextColumn(keyColumnName);
                table.AddColumn(keyColumn);

                //Dictionary<string, Row> keyLookup =
                //    new Dictionary<string, Row>();

                NumericTimeseriesColumn ntc =
                    new NumericTimeseriesColumn(numericColumnName);
                table.AddColumn(ntc);

                Row[] rowIndex = new Row[sourceTable.ColumnCount - 1];

                for (int i = 1; i < sourceTable.ColumnCount; i++)
                {
                    string key = sourceTable.GetColumn(i).Name;
                    Row row = table.AddRow();
                    keyColumn.SetTextValue(row, key);
                    //keyLookup.Add(key, row);
                    rowIndex[i - 1] = row;
                }

                ColumnReader[] sourceReaders =
                    ColumnReader.ForTable(sourceTable);

                for (int row = 0; row < sourceTable.RowCount; row++)
                {
                    if (sourceReaders[0].GetValue(row) == null)
                    {
                        continue;
                    }

                    DateTime dateTime =
                        (DateTime)sourceReaders[0].GetValue(row);
                    Time time = table.GetTime(dateTime);
                    if (time == null)
                    {
                        time = table.AddTime(dateTime);
                    }
                    for (int j = 1; j < sourceTable.ColumnCount; j++)
                    {
                        object value = sourceReaders[j].GetValue(row);
                        if (value != null)
                        {
                            ntc.SetNumericValue(rowIndex[j - 1], time,
                                Convert.ToDouble(value));
                        }
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        private static object GetCellValue(ICell cell, ColumnType columnType, IFormulaEvaluator evaluator, CellType cachedFormulaResultType = CellType.Unknown)
        {
            CellType cellType = cachedFormulaResultType;
            if (cachedFormulaResultType == CellType.Unknown)
            {
                cellType = cell.CellType;
            }

            switch (cellType)
            {
                case CellType.Error:
                    switch (columnType)
                    {
                        case ColumnType.Text:
                            return cell.ToString();
                        case ColumnType.Numeric:
                            return double.NaN;
                        case ColumnType.Time:
                            return null;
                        default:
                            throw new NotSupportedException(Resources.ExColumnTypeNotSupported);
                    }
                case CellType.Blank:
                    return null;
                case CellType.Boolean:
                    if (columnType == ColumnType.Text)
                    {
                        return cell.BooleanCellValue;
                    }
                    return null;
                case CellType.String:
                    if (columnType == ColumnType.Text)
                    {
                        return cell.StringCellValue;
                    }
                    return null;
                case CellType.Numeric:
                    if (columnType == ColumnType.Time &&
                        DateUtil.IsCellDateFormatted(cell))
                    {
                        return cell.DateCellValue;
                    }
                    if (columnType == ColumnType.Numeric)
                    {
                        return cell.NumericCellValue;
                    }
                    if (columnType == ColumnType.Text &&
                        DateUtil.IsCellDateFormatted(cell))
                    {
                        return Convert.ToString(cell.DateCellValue);
                    }
                    if (columnType == ColumnType.Text)
                    {
                        return Convert.ToString(cell.NumericCellValue);
                    }
                    return null;
                case CellType.Formula:
                    if (cachedFormulaResultType == CellType.Formula)
                    {
                        evaluator.EvaluateFormulaCell(cell);
                    }
                    return GetCellValue(cell, columnType, evaluator, cell.CachedFormulaResultType);
                default:
                    throw new NotSupportedException(Resources.ExCellTypeNotSupported);
            }
        }

        private static ColumnType GetColumnType(
            int columnIndex, IFormulaEvaluator evaluator,
            IRow[] typeCheckRows)
        {
            ColumnType columnType = UnknownColumnType;
            for (int j = 0; j < typeCheckRows.Length; j++)
            {
                IRow typeCheckRow = typeCheckRows[j];
                if (typeCheckRow == null) continue;
                ICell typeCheckCell = typeCheckRow.GetCell(columnIndex);
                if (typeCheckCell == null) continue;
                CellType cellType;
                try
                {
                    evaluator.EvaluateInCell(typeCheckCell);
                    cellType = typeCheckCell.CellType;
                }
                catch (Exception ex)
                {
                    Log.Error(Resources.ExCannotEvaluateCellType,
                        typeCheckCell.CellType == CellType.Formula ?
                            typeCheckCell.CellFormula : typeCheckCell.StringCellValue,
                        ex.Message);
                    cellType = CellType.Error;
                }

                ColumnType checkColumnType = ColumnType.Numeric;
                if (cellType == CellType.Blank) continue;
                if (cellType == CellType.Error) continue;
                if (cellType == CellType.Numeric &&
                    DateUtil.IsCellDateFormatted(typeCheckCell))
                {
                    checkColumnType = ColumnType.Time;
                }
                if (cellType == CellType.Boolean)
                {
                    checkColumnType = ColumnType.Text;
                }
                if (columnType == UnknownColumnType)
                {
                    columnType = checkColumnType;
                }
                if (checkColumnType != columnType || cellType == CellType.String)
                {
                    return ColumnType.Text;
                }
            }
            return columnType != UnknownColumnType ? columnType : ColumnType.Text;
        }

        private static void PopulateTable(StandaloneTable table, ExcelSettings settings, int maxRowCount,
                                    IEnumerable<ParameterValue> parameters, ISheet sheet,
            int rowStart, int rowEnd, int columnStart, int columnEnd,
            IFormulaEvaluator evaluator, bool applyRowFilteration)
        {
            ColumnType[] columnTypes;

            if (settings == null) return;
            // Make sure so the schema will work even if columns where removed or added from the 
            // source file after the schema was created.
            Dictionary<string, int> indexLookup = new Dictionary<string, int>();
             
            if (settings.SchemaColumnsSettings.SchemaColumns.Count > 0)
            {
                columnTypes = new ColumnType[settings.SchemaColumnsSettings.SchemaColumns.Count];

                for (int i = 0; i < settings.SchemaColumnsSettings.SchemaColumns.Count; i++)
                {
                    DatabaseColumn col = settings.SchemaColumnsSettings.SchemaColumns[i];
                    AddColumn(table, col.ColumnName, col.ColumnType);
                    columnTypes[i] = col.ColumnType;
                    indexLookup[col.ColumnName] = i;
                }
            }
            else
            {
                columnTypes = new ColumnType[columnEnd - columnStart + 1];
                IRow[] typeCheckRows = new IRow[Math.Min(8, rowEnd - rowStart)];
                for (int i = 0; i < typeCheckRows.Length; i++)
                {
                    typeCheckRows[i] = sheet.GetRow(rowStart + i + 1);
                }
                for (int i = 0; i <= columnEnd - columnStart; i++)
                {
                    ColumnType columnType = GetColumnType(i + columnStart, evaluator, typeCheckRows);
                    columnTypes[i] = columnType;
                    ICell headerCell = sheet.GetRow(rowStart).GetCell(i + columnStart);

                    Column column = AddColumn(headerCell, columnType, table);
                    indexLookup[column.Name] = i;

                    DatabaseColumn excelColumn = new DatabaseColumn();
                    excelColumn.ColumnType = columnType;
                    excelColumn.ColumnName = column.Name;

                    if (settings.SchemaRequest)
                    {
                        settings.SchemaColumnsSettings.SchemaColumns.Add(
                            excelColumn);
                    }
                }

                // We should save schema only when request is coming from
                // GetSchema, for other cases it's not safe to update PropertyBag.
                if (settings.SchemaRequest)
                {
                    settings.SchemaColumnsSettings.SerializeSchemaColumns();
                }
            }
            if (settings.SchemaRequest) return;

            ParameterFilter parameterFilter = null;
            if (applyRowFilteration)
            {
                parameterFilter= ParameterFilter.CreateFilter(table, parameters);
            }
            Dictionary<string, int> sourceFileLookup = new Dictionary<string, int>();
            // Get what index schema-column-name have in source excel file
            for (int i = 0; i < sheet.GetRow(rowStart).Cells.Count; i++)
            {
                ICell cell = sheet.GetRow(rowStart).Cells[i];
                string name = cell.StringCellValue;
                if (string.IsNullOrEmpty(name))
                {
                    name = string.Format("F{0}", i + 1);
                }
                if (indexLookup.ContainsKey(name))
                {
                    sourceFileLookup[name] = i;
                }
            }
            for (int i = rowStart + 1; i <= rowEnd; i++)
            {
                if (maxRowCount >= 0 && table.RowCount >= maxRowCount)
                {
                    break;
                }
                IRow poiRow = sheet.GetRow(i);
                object[] values = new object[columnTypes.Length];
                if (poiRow != null)
                {
                    foreach (string columnName in sourceFileLookup.Keys)
                    {
                        ICell cell = poiRow.GetCell(sourceFileLookup[columnName]);
                        if (cell == null) continue;
                        int index = indexLookup[columnName];
                        values[index] = GetCellValue(cell, columnTypes[index], evaluator);
                    }
                }
                if (parameterFilter == null || parameterFilter.Contains(values))
                {
                    table.AddRow(values);
                }
            }
        }

        private static string RemoveEncapsChars(string text)
        {
            int offset = 0;
            int length = text.Length;

            if (text.StartsWith("'"))
            {
                offset++;
                length--;
            }

            if (text.EndsWith("'"))
            {
                length--;
            }

            return text.Substring(offset, length);
        }
        private Assembly CurrentDomainOnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            string assemblyName = args.Name;
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();

            foreach (Assembly ayAssembly in assemblies)
            {
                if (assemblyName == ayAssembly.FullName)
                {
                    return ayAssembly;
                }
            }

            return null;
        }
    }
}