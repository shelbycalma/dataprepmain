﻿using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.ExcelNpoiPlugin
{
    public class ExcelSettings : ConnectionSettings, ISchemaColumnsProvider<DatabaseColumn>
    {
        private SchemaColumnsSettings<DatabaseColumn> schemaColumnSettings;

        public ExcelSettings(PropertyBag bag)
            : base(bag)
        {
        }

        public string Path 
        {
            get
            {
                return GetInternal("path");
            }
            set
            {
                SetInternal("path", value);
            }
        }

        public string Range
        {
            get
            {
                return GetInternal("range");
            }
            set
            {
                SetInternal("range", value);
            }
        }

        public string Unpivot
        {
            get
            {
                return GetInternal("unpivot");
            }
            set
            {
                SetInternal("unpivot", value);
            }
        }

        internal bool SchemaRequest { get; set; }

        public SchemaColumnsSettings<DatabaseColumn> SchemaColumnsSettings
        {
            get
            {
                if (schemaColumnSettings == null)
                {
                    schemaColumnSettings = new SchemaColumnsSettings<DatabaseColumn>(ToPropertyBag());
                }
                return schemaColumnSettings;
            }
        }

    }
}
