﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.TruvisoCQPlugin.UI
{
    public partial class ConnectionWindow : Window
    {
        public static RoutedCommand OkCommand =
            new RoutedCommand("OK", typeof(ConnectionWindow));

        private TruvisoCQSettingsViewModel settings;

        public ConnectionWindow(TruvisoCQSettingsViewModel settings)
        {
            this.settings = settings;
            InitializeComponent();
            DataContext = settings;
            cancelButton.Click += cancelButton_Click;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
        
    }
}
