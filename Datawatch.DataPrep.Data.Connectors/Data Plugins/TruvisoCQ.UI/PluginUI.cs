﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.TruvisoCQPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, TruvisoCQSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.TruvisoCQPlugin.UI;component/ConnectImage.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            TruvisoCQSettings settings = new TruvisoCQSettings(bag);
            TruvisoCQSettingsViewModel vm = new TruvisoCQSettingsViewModel(
                settings, parameters);
            return new ConfigPanel(vm);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            TruvisoCQSettingsViewModel vm = (TruvisoCQSettingsViewModel)cp.DataContext;
            return vm.Model.ToPropertyBag();
        }

        // Called when the user selects this plug-in in the "Connect to data" 
        // dialog or picks it from the data plug-in dropdown. The DoConnect 
        // method is resposible for allowing the user to create a new data 
        // source connection.
        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            TruvisoCQSettings settings = new TruvisoCQSettings();
            TruvisoCQSettingsViewModel settingsView =
                new TruvisoCQSettingsViewModel(settings, parameters);
            ConnectionWindow window = new ConnectionWindow(settingsView);

            window.Owner = owner;
            
            if (window.ShowDialog() == true)
            {
                return settings.ToPropertyBag();
            }
            return null;
        }
    }
}
