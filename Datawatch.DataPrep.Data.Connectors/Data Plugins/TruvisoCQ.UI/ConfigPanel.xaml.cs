﻿using System.Windows.Controls;

namespace Panopticon.TruvisoCQPlugin.UI
{
    public partial class ConfigPanel : UserControl
    {        
        public ConfigPanel(TruvisoCQSettingsViewModel settings)
            :this()
        {            
            DataContext = settings;
        }

        public ConfigPanel()
        {
            InitializeComponent();
        }
    }
}
