﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.TruvisoCQPlugin.UI
{
    public class TruvisoCQSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ICommand fetchSchemaCommand;
        private IEnumerable<ParameterValue> parameters;

        public TruvisoCQSettings Model { get; private set; }

        public TruvisoCQSettingsViewModel(TruvisoCQSettings model,
            IEnumerable<ParameterValue> parameters)
        {
            this.Model = model;
            this.parameters = parameters;
        }

        public string DSN
        {
            get { return this.Model.DSN; }
            set
            {
                if (value != this.Model.DSN)
                {
                    this.Model.DSN = value;
                }
            }
        }
        
        public string Query
        {
            get { return this.Model.Query; }
            set
            {
                if (value != this.Model.Query)
                {
                    this.Model.Query = value;
                }
            }
        }
        
        public bool IsOk
        {
            get
            {
                return !(string.IsNullOrEmpty(this.Model.IdColumn)
                    ||string.IsNullOrEmpty(DSN) 
                    ||string.IsNullOrEmpty(Query)
                    || string.IsNullOrEmpty(this.Model.DSN) ||
                    (this.Model.IsTimeIdColumnGenerated &&
                    string.IsNullOrEmpty(this.Model.TimeIdColumn)));

            }
        }
        
        public ICommand FetchSchemaCommand
        {
            get
            {
                if (fetchSchemaCommand == null)
                {
                    fetchSchemaCommand = new DelegateCommand(FetchSchema,
                        CanFetchSchema);
                }
                return fetchSchemaCommand;
            }
        }

        private void FetchSchema()
        {
            UpdateIdCandidates();
        }

        private bool CanFetchSchema()
        {
            return !(string.IsNullOrEmpty(DSN) ||
                    string.IsNullOrEmpty(Query));
        }

        private void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        internal void UpdateIdCandidates()
        {
            try
            {
                string query = Util.ApplyParameters(Query, parameters);
                List<TruvisoColumn> columns = Util.GetColumns(DSN, query);
                List<string> idCandidates = new List<string>();
                List<string> timeIdCandidates = new List<string>();

                foreach (TruvisoColumn column in columns)
                {
                    if (column.Type == typeof(string))                        
                    {
                        idCandidates.Add(column.Name);
                    }
                    else if (column.Type == typeof(DateTime))
                    {
                        timeIdCandidates.Add(column.Name);
                    }
                }

                this.Model.IdColumnCandidates =
                        idCandidates.Count > 0 ? idCandidates.ToArray() : null;
                this.Model.TimeIdColumnCandidates =
                    timeIdCandidates.Count > 0 ? timeIdCandidates.ToArray() : null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                           Properties.Resources.UiWindowTitle,
                           MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
