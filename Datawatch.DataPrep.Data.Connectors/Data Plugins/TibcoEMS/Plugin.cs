﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.TibcoEMSPlugin
{
    /// <summary>
    /// DataPlugin for TibcoEMS.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<ParameterTable, TibcoEMSSettings,
        Dictionary<string, object>, TibcoEMSDataAdapter>
    {
        internal const string PluginId = "TibcoEMSPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Tibco EMS";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override TibcoEMSSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new TibcoEMSSettings(pluginManager);
        }

        public override TibcoEMSSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new TibcoEMSSettings(pluginManager, bag);
        }
    }
}
