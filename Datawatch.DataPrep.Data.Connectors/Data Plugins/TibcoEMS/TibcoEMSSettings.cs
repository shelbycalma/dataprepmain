using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.TibcoEMSPlugin
{
    public class TibcoEMSSettings : MessageQueueSettingsBase
    {
        public TibcoEMSSettings(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public TibcoEMSSettings(IPluginManager pluginManager, PropertyBag bag)
            : base(pluginManager, bag)
        {
        }

        protected override string DefaultParserPluginId
        {
            get { return "Xml"; }
        }
    }
}
