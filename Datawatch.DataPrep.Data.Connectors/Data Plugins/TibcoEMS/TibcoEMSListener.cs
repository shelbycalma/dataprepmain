﻿using System;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.TibcoEMSPlugin.Properties;
using TIBCO.EMS;

namespace Panopticon.TibcoEMSPlugin
{
    class TibcoEMSListener : IMessageListener
    {
        private readonly TibcoEMSDataAdapter dataAdapter;
        Connection jmsConnection;
        Session session;
        Topic jddTopic;
        MessageConsumer consumer;

        public TibcoEMSListener(TibcoEMSDataAdapter dataAdapter)
        {
            if (dataAdapter == null)
            {
                throw Datawatch.DataPrep.Data.Core.Connectors.Exceptions.DataAdaptorWasNull();
            }
            this.dataAdapter = dataAdapter;
        }

        public void Connect(string broker, string username, string password)
        {
            // Parse the broker
            Uri brokerUri = null;
            try
            {
                brokerUri = new Uri(broker);
            }
            catch (FormatException ex)
            {
                throw Exceptions.MalformedBrokerUrl(broker, ex);
            }

            ConnectionFactory factory = new ConnectionFactory(broker);

            if (!string.IsNullOrEmpty(username))
            {
                factory.SetUserName(username);
                factory.SetUserPassword(password);
            }
            jmsConnection = null;
            session = null;


            try
            {
                jmsConnection = factory.CreateConnection();
                jmsConnection.Start();
                
                Log.Info(string.Format(Resources.LogConnected, broker));

                session = jmsConnection.CreateSession(false, Session.AUTO_ACKNOWLEDGE);
            }
            catch (Exception ex)
            {   
                if (session != null)
                {
                    session.Close();
                }

                if (jmsConnection != null)
                {
                    jmsConnection.Close();
                }
                throw Exceptions.ConnectionError(ex);
            }
        }

        public void Disconnect()
        {
            try
            {
                if (consumer != null)
                {
                    consumer.Close();
                    consumer = null;
                }

                if (session != null)
                {
                    session.Close();
                    session = null;
                }

                if (jmsConnection != null)
                {
                    jmsConnection.Stop();
                    jmsConnection.Close();
                    jmsConnection = null;
                }
            }
            catch (EMSException ex)
            {
                // I dont really know what could go wrong here, catch the
                // internal base exception and log everything.
                Log.Error(string.Format(Resources.LogInternalError,
                    ex.Message));
                Log.Exception(ex);
            }

            jddTopic = null;

            Log.Info(Resources.LogDisconnected);
        }

        public void ListenTo(string topicName)
        {
            jddTopic = session.CreateTopic(topicName);
            consumer = session.CreateConsumer(jddTopic);
            consumer.MessageListener = this;
            Log.Info(string.Format(Resources.LogListeningTo, topicName));
        }
        
        public void OnMessage(Message message)
        {
            if (message is TextMessage)
            {
                string xml = ((TextMessage)message).Text;
                dataAdapter.MessageReceived(xml);
            }
            else
            {
                Log.Warning(Resources.LogNotTextMessage);
            }
        }
    }
}
