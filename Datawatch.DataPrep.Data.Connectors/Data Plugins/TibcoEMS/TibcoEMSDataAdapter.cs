﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.TibcoEMSPlugin
{
    public class TibcoEMSDataAdapter : MessageQueueAdapterBase<ParameterTable,
        TibcoEMSSettings, Dictionary<string, object>>
    {
        private TibcoEMSListener listener;

        public override void Start()
        {
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();
            listener = new TibcoEMSListener(this);

            // Connect
            listener.Connect(settings.Broker, settings.UserName,
                settings.Password);

            // Subscribe
            string topicName = ParseTopic(settings.Topic);
            topicName = DataUtils.ApplyParameters(topicName, 
                table.Parameters);

            listener.ListenTo(topicName);

        }
        
        public override void Stop()
        {
            listener.Disconnect();
            listener = null;
            base.Stop();
        }

        internal static string ParseTopic(string source)
        {
            // Some hardcoded cleaning of the string here, seems easier than
            // trying to communicate in the GUI that the user shouldnt enter
            // the topic part.
            string topicName = source.StartsWith("topic://")
                ? source.Substring("topic://".Length) : source;
            return topicName;
        }

    }
}
