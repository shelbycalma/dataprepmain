﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.Definitions;
using Datawatch.SDK;
using Microsoft.Win32;
using Panopticon.MonarchPlugin.Properties;

namespace Panopticon.MonarchPlugin.UI
{
    public class MonarchSettingsViewModel : ViewModelBase
    {
        private bool hasValidProject;
        private string previousProjectPath;
        private string previousModelPath;
        private List<string> filtersList;
        private List<string> sortsList;
        private List<string> tableSummariesList;
        private List<string> exportTables;
        private bool isSortSensitive;
        private ViewMode viewMode;

        #region Properties

        public MonarchSettings Settings { get; set; }

        public bool IsOk
        {
            get
            {
                bool isExportValid =
                    ViewMode != ViewMode.Export || 
                    (!string.IsNullOrWhiteSpace(Settings.ExportTableName));

                return Settings != null && HasValidProject && isExportValid; 
            }
        }

        public bool HasValidProject
        {
            get { return hasValidProject; }
            set
            {
                hasValidProject = value;
                OnPropertyChanged("HasValidProject");
            }
        }

        public string PreviousProjectPath
        {
            get { return previousProjectPath; }
            set
            {
                previousProjectPath = value;
                OnPropertyChanged("PreviousProjectPath");
            }
        }

        public string PreviousModelPath
        {
            get { return previousModelPath; }
            set
            {
                previousModelPath = value;
                OnPropertyChanged("PreviousModelPath");
            }
        }
        
        public ViewMode ViewMode 
        {
            get { return viewMode; }
            set
            {
                viewMode = value;
                OnPropertyChanged("ViewMode");
            }
        }
        
        public List<string> FiltersList
        {
            get { return filtersList; }
            set
            {
                filtersList = ResetList(value);
                OnPropertyChanged("FiltersList");
            }
        }

        public List<string> SortsList
        {
            get { return sortsList; }
            set
            {
                sortsList = ResetList(value);
                OnPropertyChanged("SortsList");
            }
        }

        public List<string> TableSummariesList
        {
            get { return tableSummariesList; }
            set
            {
                tableSummariesList = ResetList(value);
                OnPropertyChanged("TableSummariesList");
            }
        }

        public List<string> ExportTables
        {
            get { return exportTables; }
            set
            {
                exportTables = value;
                OnPropertyChanged("ExportTables");
            }
        }

        public bool IsSortSensitive
        {
            get { return isSortSensitive; }
            set
            {
                isSortSensitive = value;
                OnPropertyChanged("IsSortSensitive");
            }
        }

        private ModelDefinition ModelDefinition { get; set; }

        #endregion

        #region constructor

        public MonarchSettingsViewModel(
            MonarchSettings settings, IEnumerable<ParameterValue> parameters)
        {
            Settings = settings;
            Settings.PropertyChanged += OnSettingsChanged;
            Settings.ReportPaths.CollectionChanged += ReportPaths_CollectionChanged;
        }

        #endregion

        #region methods

        public bool CheckModelerInstall()
        {
            const string keyPath = @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\DWModeler.exe";
            using 
            (
                RegistryKey key = Registry.LocalMachine.OpenSubKey(keyPath, false)
            )
            {
                if (key == null)
                {
                    MessageBox.Show(Properties.Resources.ModelerInstallErrorBody,
                        Properties.Resources.ModelerInstallErrorTitle);
                    return false;
                }
                return true;
            }
        }

        private static bool ValidProjectExtension(string path)
        {
            if (String.IsNullOrEmpty(path))
                return false;
            string ext = Path.GetExtension(path);
            return (ext == ".dprj" || ext == ".xprj");
        }

        private static bool ValidModelExtension(string path)
        {
            if (String.IsNullOrEmpty(path))
                return false;
            string ext = Path.GetExtension(path);
            return (ext == ".dmod" || ext == ".xmod");
        }

        private List<string> ResetList(IEnumerable<string> listToReset)
        {
            List<string> newList = new List<string>() {String.Empty};
            if (listToReset == null)
            {
                return newList;
            }
            newList.AddRange(listToReset);
            return newList;
        }

        public void UpdateControls()
        {
            // if the project path is null get out. Validation will happen when it is set, and it will only be set when valid.
            if (string.IsNullOrWhiteSpace(Settings.ProjectPath) && 
                string.IsNullOrWhiteSpace(Settings.ModelPath) &&
                string.IsNullOrWhiteSpace(Settings.ExportPath)) 
                return;
            try
            {
                using 
                ( 
                    EngineApi engine =
                        new EngineApi(new RegistryDefinition(Registry.CurrentUser))
                )
                {
                    if (!string.IsNullOrWhiteSpace(Settings.ModelPath))
                    {
                        engine.LoadModel(Settings.ModelPath);
                        ViewMode = ViewMode.Model;
                    }
                    else if (!string.IsNullOrWhiteSpace(Settings.ExportPath))
                    {
                        ViewMode = ViewMode.Export;
                    }
                    else
                    {
                        engine.LoadProject(Settings.ProjectPath);
                        ViewMode = ViewMode.Project;
                    }
                    if (ViewMode == ViewMode.Export)
                    {
                        string connectionString =
                            MonarchUtils.GetMonarchConnectionString(Settings.ExportPath);
                        ExportTables = engine.ImportDbGetTables(connectionString, null);
                    }
                    else
                    {
                        ModelDefinition = engine.ModelDefinition;
                        engine.Open();

                        // Update the filters and sorts with what is in the engine.
                        TableSummariesList =
                            engine.Summaries.Select(x => x.Name).ToList();
                        FiltersList = engine.Filters.Select(x => x.Name).ToList();
                        SortsList = engine.Sorts.Select(x => x.Name).ToList();

                        if (
                            String.Compare(Settings.ProjectPath,
                                PreviousProjectPath,
                                StringComparison.InvariantCultureIgnoreCase) != 0)
                        {
                            Settings.CurrentSummaryName = String.Empty;
                            Settings.CurrentFilter = engine.CurrentFilter;
                            Settings.CurrentSort = engine.CurrentSort;
                        }

                        SetSortSensitive();
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(Properties.Resources.NoSdkErrorBody,
                    Properties.Resources.NoSdkErrorTitle);
            }
        }

        private void SetSortSensitive()
        {
            if (Settings.IsTable)
            {
                IsSortSensitive = true;
            }
            else
            {
                var summaryDefinition =
                    DefinitionBase.Find(ModelDefinition.Summaries,
                        Settings.CurrentSummaryName);
                IsSortSensitive = GetIsSortSensitive(ModelDefinition,
                    summaryDefinition);
                if (!IsSortSensitive)
                    Settings.CurrentSort = string.Empty;
            }
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Handler for the ModelerSettings properties.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSettingsChanged(object sender, PropertyChangedEventArgs e)
        {
            var settings = (sender as MonarchSettings);
            if (settings == null) return;
            switch (e.PropertyName)
            {
                case "ProjectPath":
                    HasValidProject =
                        File.Exists(settings.ProjectPath) &&
                        ValidProjectExtension(settings.ProjectPath);
                    if (HasValidProject)
                    {
                        UpdateControls();
                        PreviousProjectPath = Settings.ProjectPath;
                    }
                    break;
                case "ModelPath":
                case "ReportPathList":
                    HasValidProject =
                        File.Exists(settings.ModelPath) &&
                        ValidModelExtension(settings.ModelPath) &&
                        settings.ReportPaths.Count > 0;
                    if (HasValidProject)
                    {
                        UpdateControls();
                        PreviousModelPath = Settings.ModelPath;
                    }
                    break;
                case "ExportPath":
                    HasValidProject = File.Exists(settings.ExportPath);
                    Settings.IsTable = true;
                    if (HasValidProject)
                    {
                        UpdateControls();
                    }
                    break;
                case "CurrentSummaryName":
                    Settings.IsTable =
                        String.IsNullOrEmpty(Settings.CurrentSummaryName);
                    SetSortSensitive();
                    break;
            }
        }

        #endregion

        #region Static Methods

        // The 3 folowing methods were copied from ExportDesign Controls in Modeler.Controls.Wpf.
        public static bool IsFilterIsSpecified(
            ModelDefinition.SummaryDefinition summaryDefinition)
        {
            if (summaryDefinition == null)
                return false;
            return !summaryDefinition.SummaryFilter.UseDefaultFilter;
        }

        // The 2 foloving overloads renamed to not conflict with property name
        public static bool GetIsSortSensitive(
            ModelDefinition.FilterDefinition filterDefinition)
        {
            return filterDefinition.RowCountLimit != 0;
        }

        public static bool GetIsSortSensitive(
            ModelDefinition modelDefinition,
            ModelDefinition.SummaryDefinition summary)
        {
            if (summary == null)
                return false;
            if (IsFilterIsSpecified(summary))
            {
                ModelDefinition.FilterDefinition filter = DefinitionBase.Find(modelDefinition.Filters,
                    summary.SummaryFilter.FilterName);
                return filter != null && GetIsSortSensitive(filter);
            }
            return false;
        }

        void ReportPaths_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Settings.SaveReportPaths();
        }

        #endregion
    }
}