﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Panopticon.MonarchPlugin.UI
{
    /// <summary>
    /// Interaction logic for ModelReportControl.xaml
    /// </summary>
    public partial class ModelReportControl : UserControl
    {
        public static RoutedCommand BrowseForModelCommand = new RoutedCommand("BrowseForModel",
            typeof(ModelReportControl));
        public static RoutedCommand AddReportCommand = new RoutedCommand("AddReport",
            typeof(ModelReportControl));
        public static RoutedCommand RemoveReportCommand = new RoutedCommand("RemoveReport",
            typeof(ModelReportControl));
        public static RoutedCommand MoveReportUpCommand = new RoutedCommand("MoveReportUp",
            typeof(ModelReportControl));
        public static RoutedCommand MoveReportDownCommand = new RoutedCommand("MoveReportDown",
            typeof(ModelReportControl));

        public ListBox ReportListBox { get { return reportListBox; } }

        static ModelReportControl()
        {
            CommandManager.RegisterClassCommandBinding(typeof(ModelReportControl), 
                new CommandBinding(MoveReportUpCommand, MoveReportUp_Executed, MoveReportUp_CanExecute));
            CommandManager.RegisterClassCommandBinding(typeof(ModelReportControl),
                new CommandBinding(MoveReportDownCommand, MoveReportDown_Executed, MoveReportDown_CanExecute));
        }

        public ModelReportControl()
        {
            InitializeComponent();
            RemoveReportButton.DataContext = this;
            DataContextChanged += ModelReportControl_DataContextChanged;
        }

        void ModelReportControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            RemoveReportButton.DataContext = this;
        }

        private static void MoveReportUp_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            int index = control.ReportListBox.SelectedIndex;
            e.CanExecute = index > 0;
        }

        private static void MoveReportUp_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            int index = control.ReportListBox.SelectedIndex;
            if (index < 1)
                return;
            ObservableCollection<string> list = control.ReportListBox.ItemsSource as ObservableCollection<string>;
            list.Move(index, index - 1);
        }

        private static void MoveReportDown_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            int index = control.ReportListBox.SelectedIndex;
            e.CanExecute = index >= 0 && index < control.ReportListBox.Items.Count - 1;
        }

        private static void MoveReportDown_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            int index = control.ReportListBox.SelectedIndex;
            if (index >= control.ReportListBox.Items.Count - 1)
                return;
            ObservableCollection<string> list = control.ReportListBox.ItemsSource as ObservableCollection<string>;
            list.Move(index, index + 1);
        }
    }
}
