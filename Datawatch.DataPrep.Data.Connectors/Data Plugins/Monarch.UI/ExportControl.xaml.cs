﻿using System.Windows.Input;

namespace Panopticon.MonarchPlugin.UI
{
    public partial class ExportControl
    {
        public static RoutedCommand BrowseForExportCommand = new RoutedCommand("BrowseForExport",
            typeof(ExportControl));

        public ExportControl()
        {
            InitializeComponent();
        }
    }
}
