﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.MonarchPlugin.UI.Converters
{
    public class EmptyStringConverter : IValueConverter
    {
        public string EmptyValue { get; set; }

        public object Convert(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
                return null;
            if ((string) value == String.Empty)
                return EmptyValue;
            return value;
        }

        public object ConvertBack(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
                return null;
            if ((string)value == EmptyValue)
                return String.Empty;
            return value;
        }
    }
}