﻿using System.Windows;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;

namespace Panopticon.MonarchPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("SettingsViewModel",
                typeof (MonarchSettingsViewModel), typeof (ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        public MonarchSettingsViewModel SettingsViewModel
        {
            get { return (MonarchSettingsViewModel) GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        #region Constructors

        static ConfigPanel()
        {
            // Register the commands that support the interaction between the project and model/report user controls
            // and the config panel.
            CommandManager.RegisterClassCommandBinding(typeof(ProjectControl),
                new CommandBinding(ProjectControl.BrowseForProjectCommand, BrowseForProject_Executed));
            CommandManager.RegisterClassCommandBinding(typeof(ModelReportControl),
                new CommandBinding(ModelReportControl.BrowseForModelCommand, BrowseForModel_Executed));
            CommandManager.RegisterClassCommandBinding(typeof(ExportControl),
                new CommandBinding(ExportControl.BrowseForExportCommand, BrowseForExport_Executed));
            CommandManager.RegisterClassCommandBinding(typeof(ModelReportControl),
                new CommandBinding(ModelReportControl.AddReportCommand, AddReport_Executed));
            CommandManager.RegisterClassCommandBinding(typeof(ModelReportControl),
                new CommandBinding(ModelReportControl.RemoveReportCommand, RemoveReport_Executed, RemoveReport_CanExecute));
        }

        public ConfigPanel()
        {
            InitializeComponent();
        }

        public ConfigPanel(MonarchSettingsViewModel settingsViewModel)
        {
            SettingsViewModel = settingsViewModel;

            // Reset the previous path to project/model path so that the user's choice 
            // for sort/filter/table/summary will be maintained as they
            // enter the Settings property sheet.
            settingsViewModel.PreviousProjectPath =
                settingsViewModel.Settings.ProjectPath;
            settingsViewModel.PreviousModelPath =
                settingsViewModel.Settings.ModelPath;

            InitializeComponent();

            SettingsViewModel.UpdateControls();
        }

        #endregion

        #region Methods

        private static void Settings_Changed(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (MonarchSettingsViewModel) args.OldValue,
                (MonarchSettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(
            MonarchSettingsViewModel oldSettingsViewModel,
            MonarchSettingsViewModel newSettingsViewModel)
        {
            DataContext = newSettingsViewModel;
        }


        private static void BrowseForProject_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            ProjectControl control = sender as ProjectControl;
            MonarchSettingsViewModel modelerSettingsViewModel = control.DataContext as MonarchSettingsViewModel;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".dprj";
            dialog.Filter =
                Properties.Resources.Open_Project_Extension_Filter;
            bool? result = dialog.ShowDialog();
            if (result == true)
                modelerSettingsViewModel.Settings.ProjectPath = dialog.FileName;
        }

        private static void BrowseForExport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            ExportControl control = sender as ExportControl;
            MonarchSettingsViewModel modelerSettingsViewModel = control.DataContext as MonarchSettingsViewModel;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".dwx";
            dialog.Filter =
                Properties.Resources.Open_Export_Extension_Filter;
            bool? result = dialog.ShowDialog();
            if (result == true)
                modelerSettingsViewModel.Settings.ExportPath = dialog.FileName;
        }

        private static void BrowseForModel_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            MonarchSettingsViewModel modelerSettingsViewModel = control.DataContext as MonarchSettingsViewModel;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".dmod";
            dialog.Filter =
                Properties.Resources.Open_Model_Extension_Filter; 
            bool? result = dialog.ShowDialog();
            if (result == true)
                modelerSettingsViewModel.Settings.ModelPath = dialog.FileName;
        }

        private static void RemoveReport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            if (control.ReportListBox == null)
                return;
            e.CanExecute = control.ReportListBox.SelectedItem != null;
        }

        private static void RemoveReport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            ModelReportControl control = sender as ModelReportControl;
            string item = control.ReportListBox.SelectedItem as string;
            if (item == null)
                return;
            ObservableCollection<string> list = control.ReportListBox.ItemsSource as ObservableCollection<string>;
            list.Remove(item);
        }

        private static void AddReport_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            e.Handled = true;
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".prn";
            dialog.Filter =
                Properties.Resources.Open_Report_Extension_Filter + "|" +
                Properties.Resources.Open_All_Extension_Filter;
            dialog.Multiselect = true;
            bool? result = dialog.ShowDialog();
            if (result == true)
            {
                ModelReportControl control = sender as ModelReportControl;
                ObservableCollection<string> list = control.ReportListBox.ItemsSource as ObservableCollection<string>;
                foreach (string addPath in dialog.FileNames)
                {
                    bool add = true;
                    foreach (string report in list)
                    {
                        if (report.Equals(addPath, System.StringComparison.InvariantCultureIgnoreCase))
                        {
                            add = false;
                            break;
                        }
                    }
                    if (add)
                        list.Add(addPath);
                }
            }
        }
        #endregion
    }
}