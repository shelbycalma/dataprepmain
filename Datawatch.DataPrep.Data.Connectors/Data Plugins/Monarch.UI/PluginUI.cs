﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Microsoft.Win32;
using Panopticon.MonarchPlugin;

namespace Panopticon.MonarchPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, MonarchSettings>, IFileOpenDataPluginUI<Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            MonarchSettings modelerSettings = new MonarchSettings(new PropertyBag());
            MonarchSettingsViewModel settingsViewModel = new MonarchSettingsViewModel(
                modelerSettings, parameters);

            // Display the project dialog when first opening ModelConnector.
            string fileName = OpenProjectDialog(settingsViewModel);
            if (string.IsNullOrWhiteSpace(fileName))
                return null;

            return ShowConfig(settingsViewModel, fileName);
        }

        private PropertyBag ShowConfig(MonarchSettingsViewModel settingsViewModel, string fileName)
        {
            UpdateSettings(settingsViewModel, fileName);

            // Display the panel to set sort, filter and summary settings.
            ConfigWindow window = new ConfigWindow(settingsViewModel);
            window.Owner = owner;
            bool? dialogResult = window.ShowDialog();
            if (dialogResult != true)
                return null;

            return settingsViewModel.Settings.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            MonarchSettings modelerSettings = Plugin.CreateSettings(bag);
            MonarchSettingsViewModel settingsViewModel = new MonarchSettingsViewModel(
                modelerSettings, parameters);
            ConfigPanel panel = new ConfigPanel(settingsViewModel);

            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            return configPanel.SettingsViewModel.Settings.ToPropertyBag();
        }

        public string OpenProjectDialog(MonarchSettingsViewModel settingsViewModel)
        {
            OpenFileDialog dialog = new OpenFileDialog();

            dialog.DefaultExt = ".dprj";
            dialog.Filter =
                Properties.Resources.Open_Project_Extension_Filter + "|" +
                Properties.Resources.Open_Model_Extension_Filter + "|" +
                Properties.Resources.Open_Report_Extension_Filter + "|" +
                Properties.Resources.Open_Export_Extension_Filter + "|" +
                Properties.Resources.Open_All_Extension_Filter;

            bool? result = dialog.ShowDialog();

            if (result != true) return null;
            return dialog.FileName;
        }

        private void UpdateSettings(MonarchSettingsViewModel settingsViewModel, string fileName)
        {
            settingsViewModel.Settings.ProjectPath = "";
            settingsViewModel.Settings.ModelPath = "";
            settingsViewModel.Settings.ExportPath = "";
            string extension = Path.GetExtension(fileName).ToLower();

            // Set the previous project/model path to match the current project/model path. 
            // The sort/filter/table/summary settings set by the user need to be maintained when
            // UpdateControls is called for the CurrentSummaryName property.
            if (extension == ".dprj" || extension == ".xprj")
            {
                settingsViewModel.Settings.ProjectPath = fileName;
                settingsViewModel.PreviousProjectPath = fileName;
                settingsViewModel.ViewMode = ViewMode.Project;
            }
            else if (extension == ".dmod" || extension == ".xmod")
            {
                settingsViewModel.Settings.ModelPath = fileName;
                settingsViewModel.PreviousModelPath = fileName;
                settingsViewModel.ViewMode = ViewMode.Model;
            }
            else if (extension == ".dwx")
            {
                settingsViewModel.Settings.ExportPath = fileName;
                settingsViewModel.ViewMode = ViewMode.Export;
            }
            else
                settingsViewModel.Settings.ReportPaths.Add(fileName);
        }

        public string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Properties.Resources.UiDialogDatawatchFilesFilter,
                        fileExtensions);
            }
        }

        public bool CanOpen(string filePath)
        {
            return DataUtils.CanOpen(filePath, fileExtensions);
        }

        public PropertyBag DoOpenFile(string filePath)
        {
            MonarchSettings modelerSettings = new MonarchSettings(new PropertyBag());
            MonarchSettingsViewModel settingsViewModel = new MonarchSettingsViewModel(
                modelerSettings, null);

            return ShowConfig(settingsViewModel, filePath);
        }

        public PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new System.NotImplementedException();
        }

        private static readonly string[] fileExtensions = new[] { "dwx" };

        public string[] FileExtensions
        {
            get { return fileExtensions; }
        }
    }
}
