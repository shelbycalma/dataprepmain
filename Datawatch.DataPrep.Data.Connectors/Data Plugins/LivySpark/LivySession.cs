﻿namespace Panopticon.LivySparkPlugin
{
    public class LivyBaseObject
    {
        public int id;
        public string state;
        public string ServerResponse;
    }
    public class LivySession : LivyBaseObject
    {
        public string appId;
        public string owner;
        public string proxyUser;
        public string kind;
        public object appInfo;
        public object[] log;
    }

    public class LivyStatement : LivyBaseObject
    {
        public object output;
    }

    public class LivyStatementOutput
    {
        public int execution_count;
        public string status;
        public object data;
    }
}
