﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;

namespace Panopticon.LivySparkPlugin
{
    public class LivySparkSettings : ConnectionSettings
    {
        private IEnumerable<ParameterValue> parameters;

        public LivySparkSettings()
            : this(new PropertyBag(), null)
        {
        }

        public LivySparkSettings( PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag)
        {
            this.parameters = parameters;
        }

        public string Host
        {
            get
            {
                return GetInternal("Host", "http://");
            }
            set
            {
                SetInternal("Host", value);
            }
        }

        public string UserId
        {
            get
            {
                return GetInternal("UserId");
            }
            set
            {
                SetInternal("UserId", value);
            }
        }

        public string Password
        {
            get
            {
                return GetInternal("Password");
            }
            set
            {
                SetInternal("Password", value);
            }
        }

        public string Kind
        {
            get
            {
                return GetInternal("Kind", "pyspark");
            }
            set
            {
                SetInternal("Kind", value);
            }
        }

        public int Timeout
        {
            get
            {
                return GetInternalInt("Timeout", 30);
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(
                        Properties.Resources.ExInvalidTimeout);
                }
                SetInternalInt("Timeout", value);
            }
        }

        public int PollingFrequency
        {
            get
            {
                return GetInternalInt("PollingFrequency", 2);
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(
                        Properties.Resources.ExInvalidPollingFrequency);
                }
                SetInternalInt("PollingFrequency", value);
            }
        }

        public int PollingCount
        {
            get
            {
                return GetInternalInt("PollingCount", 150);
            }
            set
            {
                if (value <= 0)
                {
                    throw new ArgumentException(
                        Properties.Resources.ExInvalidPollingCount);
                }
                SetInternalInt("PollingCount", value);
            }
        }

        public string Script
        {
            get
            {
                return GetInternal("Script", "");
            }
            set
            {
                SetInternal("Script", value);
            }
        }

        public string[] LivyKinds
        {
            get { return new string[]{"pyspark"/*,"spark", "sparkr"*/ }; }
        }

        public string ConnectionString
        {
            get
            {
                string host = DataUtils.ApplyParameters(Host, parameters);
                string kind = DataUtils.ApplyParameters(Kind, parameters);
                string userId = DataUtils.ApplyParameters(UserId, parameters);
                string pwd = DataUtils.ApplyParameters(Password, parameters);
                return string.Format("host:{0};kind:{1};userid:{2};password:{3}",
                    host, kind, userId, pwd);
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!(obj is LivySparkSettings)) return false;

            LivySparkSettings sparkSettings = (LivySparkSettings)obj;
            return (string.Equals(Host, sparkSettings.Host)
                && string.Equals(Kind, sparkSettings.Kind)
                && string.Equals(UserId, sparkSettings.UserId)
                && string.Equals(Password, sparkSettings.Password)
                && string.Equals(Script, sparkSettings.Script)
                && PollingCount == sparkSettings.PollingCount
                && PollingFrequency == sparkSettings.PollingFrequency
                && base.Equals(obj));
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();
            object[] fields =
            {
                Host, Kind,
                UserId, Password,
                Script, PollingCount,
                PollingFrequency
            };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }
            return hashCode;
        }

    }
}
