﻿using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.LivySparkPlugin
{
    /// <summary>
    /// DataPlugin for LivySpark.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<LivySparkSettings>
    {
        private bool disposed;

        internal const string PluginId = "LivySparkPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "LivySpark";
        internal const string PluginType = DataPluginTypes.Database;

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override LivySparkSettings CreateSettings(PropertyBag bag)
        {
            LivySparkSettings livySettings = new LivySparkSettings(bag, null);
            if (livySettings.Title == null)
            {
                livySettings.Title = Properties.Resources.UiLivySparkTitle;
            }
            return livySettings;
        }

        public override ITable GetData(string workbookDir, string dataDir, 
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            LivySparkSettings settings = 
                new LivySparkSettings(properties, parameters);
            LivyClient client = new LivyClient(settings, parameters);
            ITable table = null;
            DateTime start = DateTime.Now;

            table = client.GetData(maxRowCount);
            Log.Info(Properties.Resources.LogPluginQueryCompleted,
                (DateTime.Now - start).TotalSeconds,
                table.ColumnCount, table.RowCount);
            return table;
        }
    }
}
