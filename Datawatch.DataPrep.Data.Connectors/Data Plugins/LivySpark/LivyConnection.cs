﻿using Datawatch.DataPrep.Data.Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;

namespace Panopticon.LivySparkPlugin
{
    public class LivyConnection
    {
        public const string LivySession = "sessions";
        public const string LivyStatement = "statements";

        private const string KindObjectFormat = "{{ \"kind\" : \"{0}\"}}";
        private const string KindObjectUserIdFormat = "{{ \"kind\" : \"{0}\", \"proxyUser\":\"{1}\"}}";
        private const string CodeObjectFormat = "{{\"code\": \"{0}\"}}";
        private const string SessionStateNotStarted = "not_started";
        private const string SessionStateStarting = "starting";
        private const string SessionStateIdle = "idle";
        private const string SessionStateBusy = "busy";
        private const string SessionStateError = "error";
        private const string SessionStateDead = "dead";
        private const string StatementStateRunning = "running";
        private const string StatementStateAvailable = "available";
        private const string StatementStateError = "error";
        private const string OutputStatusOk = "ok";
        private const string OutputStatusError = "error";

        private int pollCount;

        private readonly string host;
        private readonly string kind;
        private readonly string userId;
        private readonly string password;
        private readonly int maxPollCount;
        private readonly int pollFrequency;
        private readonly int connectionTimeout;

        private LivySession session;

        public LivyConnection(string host, string kind, string userId, 
            string password, int maxPollCount, int pollFrequency,
            int connectionTimeout)
        {
            this.host = host;
            this.kind = kind;
            this.userId = userId;
            this.password = password;
            this.maxPollCount = maxPollCount;
            this.pollFrequency = pollFrequency;
            this.connectionTimeout = connectionTimeout;
        }

        public void CreateSession()
        {
            string url = "/" + LivySession;
                       
            String data = string.Format(KindObjectFormat, kind);
            if (!string.IsNullOrEmpty(userId))
            {
                data = string.Format(KindObjectUserIdFormat, kind, userId);
            }

            StreamReader streamReader = DoServiceCall(url, HttpVerb.POST, data);

            session = JsonConvert.DeserializeObject<LivySession>(
                streamReader.ReadToEnd());

            if (session != null && 
                (session.state.Equals(SessionStateStarting)))
            {
                Log.Info(Properties.Resources.LogSessionCreated,
                    session.id, session.state);
                return;
            }

            if (session == null)
            {
                throw new Exception(
                    Properties.Resources.LogUnableToCreateSession);
            }
        }

        public bool CheckSessionReady()
        {
            if (session == null)
            {
                return false;
            }
            string url = string.Format("/{0}/{1}", LivySession, session.id);
            pollCount = 0;
            while (pollCount++ < maxPollCount)
            {
                Log.Info(Properties.Resources.LogQuerySessionState,
                        session.id);
                StreamReader streamReader = DoServiceCall(url, HttpVerb.GET);

                session = JsonConvert.DeserializeObject<LivySession>(
                    streamReader.ReadToEnd());

                if (session != null &&
                    (session.state.Equals(SessionStateIdle)))
                {
                    Log.Info(Properties.Resources.LogSessionActive,
                        session.id, session.state, pollCount);
                    return true;
                }
                //retry after some rest
                Thread.Sleep(pollFrequency * 1000);
            }
            return false;
        }

        public void DeleteSession()
        {
            if (session == null)
            {
                return;
            }
            Log.Info(Properties.Resources.LogDeleteSessionRequest,
                session.id);
            string url = string.Format("/{0}/{1}", LivySession, session.id);
            StreamReader streamReader = DoServiceCall(url, HttpVerb.DELETE);
            string message = streamReader.ReadToEnd();
            if (!message.Contains("deleted"))
            {
                Log.Info(Properties.Resources.LogUnableToDeleteSession,
                       session.id, message);
                return;
            }
            Log.Info(Properties.Resources.LogSessionDeleted, session.id);
        }

        public JToken ExecuteScript(string script)
        {
            if (session == null)
            {
                throw new Exception(Properties.Resources.ExNotConnected);
            }
            string url = string.Format("/{0}/{1}/{2}",
                LivySession, session.id,
                LivyStatement);
            LivyStatement statement = null;

            string code = string.Format(CodeObjectFormat, script);
            Log.Info(Properties.Resources.LogSubmitStatement,
                   session.id);
            StreamReader streamReader = DoServiceCall(
                    url, HttpVerb.POST, code);

            statement = JsonConvert.DeserializeObject<LivyStatement>(
                streamReader.ReadToEnd());
            if (statement == null)
                return null;
            Log.Info(Properties.Resources.LogStatementStatus,
                   statement.id, statement.state, statement.output);
            if (statement.state.Equals(StatementStateError))
                return null;
            //polling to keep check when the statement is available
            url = string.Format("{0}/{1}", url, statement.id);
            pollCount = 0;
            while (pollCount++ < maxPollCount)
            {
                Log.Info(Properties.Resources.LogQueryStatement,
                   statement.id);
                streamReader = DoServiceCall(url, HttpVerb.GET, null);
                //TODO:- may not be best way in case of large response objects
                statement = JsonConvert.DeserializeObject<LivyStatement>(
                    streamReader.ReadToEnd());
                if (statement != null &&
                    (statement.state.Equals(StatementStateError) ||
                        statement.state.Equals(StatementStateAvailable)))
                {
                    Log.Info(Properties.Resources.LogQueryStatementState,
                        statement.id, statement.state, pollCount);
                    break;
                }
                //rest all cases retry
                Thread.Sleep(pollFrequency * 1000);
            }
            if (statement != null &&
                statement.state.Equals(StatementStateAvailable))
            {
                LivyStatementOutput output =
                    JsonConvert.DeserializeObject<LivyStatementOutput>
                    (statement.output.ToString());

                if (output.status.Equals(OutputStatusError))
                {
                    string error = string.Format(
                        Properties.Resources.LogStatementOutputState,
                        statement.id, output.status, statement.output.ToString());
                    Log.Info(error);
                    throw new Exception(error);
                }
                return (JToken)output.data;
            }
            return null;

        }

        private StreamReader DoServiceCall(
            string url, HttpVerb verb, string data = null)
        {
            url = host + url;
            HttpWebRequest httpRequest = GetHttpWebRequestInstance(
            url, connectionTimeout, userId, password, data, verb.ToString());
            HttpWebResponse webResponse =
                    (HttpWebResponse)httpRequest.GetResponse();

            //create stream object from webresponse
            StreamReader streamReader = new StreamReader(
                webResponse.GetResponseStream(), Encoding.UTF8);

            return streamReader;
        }

        private static HttpWebRequest GetHttpWebRequestInstance(
            string URL, int requestTimeout, string userId, 
            string pwd, string postData, string method = "POST")
        {

            HttpWebRequest httpRequest =
                (HttpWebRequest)WebRequest.Create(URL);

            //set the timeout
            httpRequest.Timeout = requestTimeout * 1000;
            //set cache policy to none
            HttpRequestCachePolicy noCachePolicy =
                new HttpRequestCachePolicy(
                    HttpRequestCacheLevel.NoCacheNoStore);
            httpRequest.CachePolicy = noCachePolicy;

            //get URI credential
            httpRequest.Credentials = DataPluginUtils
                .GetAppropriateCredential(userId, pwd);

            httpRequest.Method = method;

            httpRequest.ContentType = "application/json";
            //if the request body is not emty do a post request
            if (postData != null)
            {
                using (var streamWriter = 
                    new StreamWriter(httpRequest.GetRequestStream()))
                {
                    streamWriter.Write(postData);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
            }
            return httpRequest;
        }
    }
}
