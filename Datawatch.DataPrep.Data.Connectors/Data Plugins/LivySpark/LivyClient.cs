﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.LivySparkPlugin
{
    public class LivyClient
    {
        private readonly LivySparkSettings settings;
        private readonly IEnumerable<ParameterValue> parameters;
        private static readonly string iso8601SecondsFormat = "yyyy-MM-ddTHH:mm:ss";
        private static readonly string iso8601MillisFormat = "yyyy-MM-ddTHH:mm:ss.ffffff";

        public LivyClient(LivySparkSettings settings,
           IEnumerable<ParameterValue> parameters)
        {
            this.settings = settings;
            this.parameters = parameters;
        }

        public void TestConnection() {
            LivyConnection connection = PrepareConnection();
            connection.CreateSession();
            connection.DeleteSession();
        }

        public RowLimitTable GetData(int rowCount)
        {
            LivyConnection connection = PrepareConnection();
            try
            {
                connection.CreateSession();
                if (!connection.CheckSessionReady())
                {
                    return null;
                }

                JToken jTokenData = connection.ExecuteScript(RemoveLineEndings(
                    DataUtils.ApplyParameters(settings.Script, parameters)));

                return CreateTable(jTokenData, rowCount);
            }

            catch (Exception ex)
            {
                Log.Exception(ex);
                throw;
            }
            finally
            {
                connection.DeleteSession();
            }
        }

        private LivyConnection PrepareConnection()
        {
            string host = DataUtils.ApplyParameters(
                settings.Host, parameters);
            string kind = DataUtils.ApplyParameters(
                settings.Kind, parameters);
            string userId = DataUtils.ApplyParameters(
                settings.UserId, parameters);
            string pwd = DataUtils.ApplyParameters(
                settings.Password, parameters);

            return new LivyConnection(host, kind, userId,
                    pwd, settings.PollingCount, settings.PollingFrequency,
                    settings.Timeout);
        }

        private string RemoveLineEndings(string input)
        {
            if (input.Contains("\r\n"))
            {
                input = input.Replace("\r\n", "");
            }
            if (input.Contains("\r"))
            {
                input = input.Replace("\r", "");
            }
            if (input.Contains("\n"))
            {
                input = input.Replace("\n", "");
            }
            if (input.Contains("\""))
            {
                input = input.Replace("\"", "\\\"");
            }

            return input;
        }

        private RowLimitTable CreateTable(JToken outputData, int rowCount)
        {
            //var data = outputData.First.First;
            if (outputData == null)
            {
                Log.Error(Properties.Resources.LogNoParsableData);
                return null;
            }
            JArray dataArray = null;
            try
            {
                dataArray = JArray.Parse(outputData.ToString());
            }
            catch (Exception ex)
            {
                Log.Info(ex.Message);
            }

            if (dataArray == null)
            {
                return CreateTable(outputData.First, rowCount);
            }

            RowLimitTable table = new RowLimitTable();
            table.BeginUpdate();
            try
            {
                int rowNum = 0;
                foreach (JToken jRow in dataArray.Children())
                {
                    //create columns based on first row
                    if (table.ColumnCount == 0)
                    {
                        CreateColumns(jRow, table);
                    }
                    //Fill the rows
                    object[] values = new object[table.ColumnCount];
                    foreach (JProperty currProp in jRow)
                    {
                        if (!currProp.HasValues) continue;

                        for (int i = 0; i < table.ColumnCount; i++)
                        {
                            string columnName = table.GetColumn(i).Name;
                            if (columnName.Equals(currProp.Name))
                            {
                                values[i] = currProp.Value;
                                break;
                            }
                        }
                    }
                    table.AddRow(values);

                    if(rowCount > 0)
                    {
                        rowNum++;
                        if (rowNum == rowCount) break;
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        private void CreateColumns(JToken jRow, RowLimitTable table)
        {
            foreach (JToken currToken in jRow)
            {
                if (!currToken.HasValues) continue;

                JProperty currProp = currToken as JProperty;

                string name = currProp == null ? currToken.Path : currProp.Name;
                JTokenType type = 
                    currProp == null ? currToken.Type : currProp.Value.Type;

                try
                {
                    switch (type)
                    {
                        case JTokenType.Date:
                            table.AddColumn(new TimeColumn(name));
                            break;
                        case JTokenType.Integer:
                        case JTokenType.Float:
                            table.AddColumn(new NumericColumn(name));
                            break;
                        
                        case JTokenType.Boolean:
                            table.AddColumn(new TextColumn(name));
                            break;
                        case JTokenType.String:
                            if (TryParseDate(currProp.Value.ToString()) != null)
                            {
                                table.AddColumn(new TimeColumn(name));
                                continue;
                            }
                            table.AddColumn(new TextColumn(name));
                            break;
                        default:
                            table.AddColumn(new TextColumn(name));
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(Properties.Resources.LogColumnDefinitionError, ex);
                    throw ex;
                }
            }
        }

        private static object TryParseDate(string value)
        {
            DateTime dateTime;
            if (DateTime.TryParseExact(value,
                iso8601MillisFormat, CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return dateTime;
            }
            if (DateTime.TryParseExact(value,
                iso8601SecondsFormat, CultureInfo.InvariantCulture,
                DateTimeStyles.None, out dateTime))
            {
                return dateTime;
            }
            return null;
        }
    }
}
