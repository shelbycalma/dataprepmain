﻿namespace Panopticon.LivySparkPlugin
{
    public enum HttpVerb
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}
