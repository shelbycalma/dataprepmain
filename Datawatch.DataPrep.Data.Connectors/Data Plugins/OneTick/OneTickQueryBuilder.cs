﻿using System;
using System.Collections.Generic;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.OneTickPlugin
{
    public class OneTickQueryBuilder
    {
        private static List<string>
            BuildWhereConditions(OneTickSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            //Replce datetime type parameters first, so that 
            //value for $TimeWindowStart etc. can be converted to UTC

            string fromDate = settings.StartTime;
            string toDate = settings.EndTime;
            if (parameters != null)
            {
                fromDate = OneTickUtils.ReplaceParameters(fromDate, parameters, false);
                toDate = OneTickUtils.ReplaceParameters(toDate, parameters, false);
            }

            List<string> conditions = new List<string>();

            if (!string.IsNullOrEmpty(settings.SymbolList))
            {
                conditions.Add(string.Format("symbol_name in ({0})",
                    OneTickUtils.QuoteLiteralParameterValue(settings.SymbolList)));
            }
            if (!string.IsNullOrEmpty(settings.StartTime))
            {
                conditions.Add(string.Format("timestamp >= {0}",
                    OneTickUtils.QuoteLiteralParameterValue(
                    OneTickUtils.ConvertToUTC(fromDate, settings.TimeZoneHelper))));
            }
            if (!string.IsNullOrEmpty(settings.EndTime))
            {
                conditions.Add(string.Format("timestamp < {0}",
                    OneTickUtils.QuoteLiteralParameterValue(
                    OneTickUtils.ConvertToUTC(toDate, settings.TimeZoneHelper))));
               
            }

            OneTickParameter[] oneTickParameters = settings.InputParameters;
            foreach (OneTickParameter parameter in oneTickParameters)
            {
                if (!string.IsNullOrEmpty(parameter.Value))
                {
                    conditions.Add(string.Format(
                        "param_assign('{0}', {1}) = 1", parameter.Name,
                        OneTickUtils.QuoteLiteralParameterValue(parameter.Value)));
                }
            }

            return conditions;
        }

        private static string BuildProcedureName(OneTickSettings settings)
        {
            return string.Format("OTQ_FILES.\"{0}\"", settings.ProcedureName);
        }

        public static string BuildQuery(OneTickSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            if (settings.Sql != null)
            {
                return settings.Sql;
            }
            if (string.IsNullOrEmpty(settings.ProcedureName))
            {
                return null;
            }

            StringBuilder query = new StringBuilder();
            query.AppendLine("select *");
            query.Append("from ");
            query.AppendLine(BuildProcedureName(settings));

            List<string> conditions = BuildWhereConditions(settings,parameters);
            if (conditions.Count > 0)
            {
                query.AppendLine("where");
                for (int i = 0; i < conditions.Count; i++)
                {
                    if (i == 0)
                    {
                        query.Append("  ");
                    }
                    query.Append(conditions[i]);
                    if (i < conditions.Count - 1)
                    {
                        query.AppendLine();
                        query.Append("  ");
                        query.Append("and ");
                    }
                    
                }
            }

            return query.ToString();
        }        
    }
}
