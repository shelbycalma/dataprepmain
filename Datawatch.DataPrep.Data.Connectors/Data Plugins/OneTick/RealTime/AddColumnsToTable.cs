﻿using System;
using System.Threading;
using Datawatch.DataPrep.Data.Framework;
using omd.onetick;

namespace Panopticon.OneTickPlugin.RealTime
{
    public class AddColumnsToTable
    {
        private OneTickTable table;
        private OneTickSettings settings;
        private QueryCancellationHandle cancelHandle;
        private bool isDone = false;

        public AddColumnsToTable(OneTickTable table, OneTickSettings settings)
        {        
            this.table = table;
            this.settings = settings;
        }

        public void ConnectAndBuildSchema()
        {
            // Create a handle that can be used to cancel the query after we obtained 
            // all columns.
            cancelHandle = QueryCancellationHandle.create_instance();
            // set up the connection
            Connection conn = new Connection();
            conn.connect(settings.ContextName);

            // create the query
            AddColumnsToTableCallback callback =
                    new AddColumnsToTableCallback(this);
            String queryString = "";
            try
            {
                bool isOtqQueryValid = OneTickUtils.IsOtqQueryValid(settings);
                if (settings.Sql != null || !isOtqQueryValid)
                {
                    // Create the SQL query
                    SqlQuery query = OneTickUtils.CreateSqlQuery(settings, 
                            table.Parameters, queryString);
                    // Send the SQL query to OneTick with a cancelHandle and wait 
                    // for the callback to signal to Plugin that all columns
                    // have been obtained and query can be canceled using the 
                    // cancelHandle.
                    RequestGroup.process_sql_query(query, callback, conn,
                            cancelHandle);
                }
                else
                {
                    // Create the normal OTQ-file query
                    OtqQuery query = OneTickUtils.CreateOtqQuery(settings, 
                            table.Parameters, queryString);
                    // Send the OTQ query to OneTick with a cancelHandle and wait 
                    // for the callback to signal to Plugin that all columns
                    // have been obtained and query can be canceled using the 
                    // cancelHandle.
                    
                    RequestGroup.process_otq_file(query, callback, conn,
                            cancelHandle);
                    if (!IsDone())
                    {
                        SetDone();
                    }
                }
            }
            catch (Exception e)
            {
                OneTickUtils.LogQueryException(e, queryString);
                // Wake up the thread waiting for a notify so it wont hang                
                SetDone();                                    
            }
            finally
            {
                if (conn != null)
                {
                    // Disconnect from OneTick
                    conn.disconnect();
                }
            }
        }

        public bool IsDone()
        {
            lock (this)
            {
                return isDone;
            }
        }

        public void SetDone()
        {
            lock (this)
            {
                isDone = true;
                // Now, we got all we needed and the columns have been added to 
                // the table so now wake up the plugin and stop this query. :)
                //this.notify();
                Monitor.Pulse(this);
            }
        }

        public void StopQuery()
        {
           OneTickUtils.CancelQuery(cancelHandle);
        }

        // AddColumnsToTableCallback is a OneTick callback class. This one is 
        // used to get what columns to expect and then add them to table.
        internal class AddColumnsToTableCallback : CSharpOutputCallback 
        {
            AddColumnsToTable parent;
            
            public AddColumnsToTableCallback(AddColumnsToTable parent)
            {
                this.parent = parent;
            }

            public override void process_error(int error_code, string error_msg)
            {
                base.process_error(error_code, error_msg);

                if (parent.settings.ShowSymbolErrorAsWarnings)
                {
                    Log.Warning(Properties.Resources.LogErrorCode,
                        error_code, error_msg);
                }
                else
                {
                    throw new Exception(error_msg);
                }                
            }

            public override void done()
            {
                // We can't reliably say when this callback method is invoked,
                // It can happen when there are no more ticks to process, or per symbol
                // when symbol error has occured. So we should use it for cleanup.
            }

            // OneTick callback method to process a tick descriptor. Is called before 
            // the first call to process_event. Class omd::TickDescriptor contains 
            // methods that describe the tick, such as getting the number of fields, 
            // their names, sizes, and types. The ticks for same security may have 
            // different sizes, and sizes, and The process_tick_descriptor() method 
            // is invoked every time before structure of the tick changes.

            public override void process_tick_descriptor(TickDescriptor tickDescriptor)
            {
                if (parent.IsDone())
                {
                    // Have to have this check in case of a new tickDescriptor
                    // arrives before the query is stoped properly.
                    return;
                }

                OneTickUtils.GenerateColumns(tickDescriptor,
                    parent.table, parent.settings);

                OneTickUtils.AddAdditionalColumns(parent.table,
                    parent.settings.SeparateDBName);
                // Set that we now have got info from TickDescriptor 
                parent.SetDone();
            }

            // OneTick callback method to deliver/process a tick. Just dump the 
            // ticks that will arrive before the query is properly stoped. We are 
            // not interested in them, .... yet.
            
            public override void process_event(Tick tick, DateTime time)
            {
                // Ignore all incomming ticks
            }

            
            public override void process_symbol_name(String symbol)
            {
                Log.Info(Properties.Resources.LogMethodInvoked, symbol);
            }

            // Method to create a new instance of a callback object. If the graph is 
            // processed for multiple securities, ticks for each security will be 
            // processed in its own replica of the callback object.

            public override CSharpOutputCallback replicate()
            {
                AddColumnsToTableCallback copy = new AddColumnsToTableCallback(parent);
                return copy;
            }
        }
    }
}
