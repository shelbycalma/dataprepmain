﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using omd.onetick;

using Panopticon.OneTickPlugin.Properties;

namespace Panopticon.OneTickPlugin.RealTime
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin: RealtimeDataPlugin<OneTickTable, OneTickSettings,
        TickWrapper, OneTickDataAdapter>, ISchemaSavingPlugin
    {
        // The id of the data plug-in, this string must not be changed after
        // the plug-in has been used in workbooks or the workbooks created 
        // using the plug-in will loose their data connections.
        internal const string PluginId = "OneTickCEPPlugin";
        internal const bool PluginIsObsolete = false;
        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        internal const string PluginTitle = "OneTick CEP";
        internal const string PluginType = DataPluginTypes.Streaming;

        private bool disposed;

        private readonly OneTickLib oneTickLib;

        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
        {
            try
            {
                oneTickLib = new OneTickLib(null);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

        public override OneTickSettings CreateSettings(PropertyBag bag)
        {
            return new OneTickSettings(bag);
        }

        protected override ITable CreateTable(OneTickSettings settings,
             IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            CheckLicense();

            return OneTickUtils.GetSchema(settings, parameters);
        }

        public void SaveSchema(string workbookDir,
            string dataDir, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();
            OneTickSettings settings = CreateSettings(bag);
            settings.IsSchemaRequest = true;
            OneTickUtils.GetSchema(settings, parameters);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
