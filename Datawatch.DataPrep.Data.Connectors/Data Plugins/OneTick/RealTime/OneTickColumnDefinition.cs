﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.OneTickPlugin.RealTime
{
    [Obsolete ("Use only for backward compability")]
    internal class OneTickColumnDefinition : ColumnDefinition
    {
        public OneTickColumnDefinition(PropertyBag bag)
            : base(bag)
        {
        }

        public override bool IsValid
        {
            get { return !string.IsNullOrEmpty(Name); }
        }

    }
}
