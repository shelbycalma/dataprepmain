﻿using System;
using System.Collections.Generic;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using omd.onetick;

namespace Panopticon.OneTickPlugin.RealTime
{
    public class OneTickDataAdapter :
        RealtimeDataAdapter<OneTickTable, OneTickSettings, TickWrapper>,
        ICombineEvents<TickWrapper>
    {
        private OneTickLocation idColumnLocation = OneTickLocation.NONE;
        private OneTickLocation timeIdColumnLocation = OneTickLocation.NONE;
        private volatile Thread dequeueThread = null;
        private AddRowsToTable dataLoader;

        public String GetIdString(TickWrapper tickWrapper)
        {        
            Object value = tickWrapper.GetValue(settings.IdColumn);
        
            if (value is Double)
            {
                return ((Double)value).ToString();
            }
            if (value is String)
            {
                return value.ToString();
            }
            if (value is DateTime)
            {
                return settings.TimeZoneHelper.ConvertFromUTC((DateTime)value).
                        ToString();
            }
            // Should never happen
            return "";
        }

        public override DateTime GetTimestamp(TickWrapper evt)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return TimeValue.Empty;
            }

            DateTime date = TimeValue.Empty;
            if (OneTickLocation.TICK.Equals(timeIdColumnLocation))
            {
                // time value is expected to be received from OneTick in the Tick 
                // object that is delivered as argument in process_event() in the 
                // callback.
                String timeIdColumn = settings.TimeIdColumn;
                Object timeIdValue = evt.GetValue(timeIdColumn);
                if (timeIdValue is DateTime)
                {
                    date = (DateTime)timeIdValue;
                }
                else if (timeIdValue.GetType() == typeof(int))
                {
                    date = new DateTime(Convert.ToInt32(timeIdValue) * 1000);
                }
                else if (timeIdValue.GetType() == typeof(long))
                {
                    date = new DateTime(Convert.ToInt64(timeIdValue));
                }
                else
                {
                    // throw unsupported date Type
                }
            }
            else
            {
                // timeIdColumnLocation == OneTickLocation.TIMESTAMP
                // The time value is expected to be received from OneTick in the 
                // time variable / TIMESTAMP delivered as argument in process_event() 
                // in the callback.
                date = evt.Time;
            }
            return settings.TimeZoneHelper.ConvertFromUTC(date);

        }

        public override void Start()
        {
            dataLoader = new AddRowsToTable(this);
            dequeueThread = new Thread(dataLoader.ConnectAndLoadData);
            dequeueThread.Name = "OneTick Dequeue Stream";
            dequeueThread.Start();
        }

        public override void Stop()
        {
            dataLoader.StopQuery();
            dequeueThread = null; 
        }

        public override void UpdateColumns(Row row, TickWrapper evt)
        {
            OneTickUtils.InsertTickInfoIntoRowRealTime(settings, table, row, evt);
        }

        public bool DoCombineEvents
        {
            get { return true; }
        }

        public TickWrapper Combine(TickWrapper event1, TickWrapper event2)
        {
            foreach (KeyValuePair<string, object> keyValuePair in event2.Values)
            {
                event1.Values[keyValuePair.Key] = keyValuePair.Value;
            }
            return event1;
        }

        // AddRowsToTableRunnable: Run this to obtain all rows/ticks from OneTick in 
        // realtime. 
        class AddRowsToTable
        {
        
            OneTickDataAdapter parent;
            private QueryCancellationHandle cancelHandle;
            private Connection conn;
        
            public AddRowsToTable(OneTickDataAdapter parent)
            {
                this.parent = parent;
            }

            public void ConnectAndLoadData()
            {
                // Create a handle that can be used to cancel the query on stop.
                cancelHandle = QueryCancellationHandle.create_instance();

                // set up the connection
                conn = new Connection(); 
                conn.connect(parent.settings.ContextName);
            
                AddRowsToTableCallback callback = 
                        new AddRowsToTableCallback(parent);
                String queryString = "";
                try
                {
                    bool isOtqQueryValid = OneTickUtils.IsOtqQueryValid(
                        parent.settings);
                    if (parent.settings.Sql != null || !isOtqQueryValid)
                    {
                        // Create the SQL query
                        SqlQuery query = OneTickUtils.CreateSqlQuery(parent.settings, 
                                parent.table.Parameters, queryString);
                        // Send the SQL query to OneTick and wait here while our 
                        // callback obtain ticks/rows from OneTick that we insert in 
                        // our table.
                        RequestGroup.process_sql_query(query, callback, conn, cancelHandle);
                    }
                    else
                    {
                        // Create the normal OTQ-file query
                        OtqQuery query = OneTickUtils.CreateOtqQuery(parent.settings, 
                                parent.table.Parameters, queryString);
                        RunningQueryProperties running_query_properties = 
                                new RunningQueryProperties();
                        // Set Stitch to false = we thrust that OneTick send correct
                        // information first time. So no corrections afterwards is 
                        // done.
                        running_query_properties.set_send_stitch_event_flag(false);
                        // Set true => Make OneTick deliver new incomming data in 
                        // realtime. The OTQ-file will have "RunningQuery = 1".
                        query.set_running_query_properties(true, 
                                running_query_properties);
                        // Send the OTQ query to OneTick and wait here while our 
                        // callback obtain ticks/rows from OneTick that we insert in 
                        // our table.
                        RequestGroup.process_otq_file(query, callback, conn, cancelHandle);
                    }
                }
                catch (Exception e)
                {
                    OneTickUtils.LogQueryException(e, queryString);
                }
                finally
                {
                    if (conn != null)
                    {
                        // Disconnect from OneTick
                        conn.disconnect();
                    }
                }
            }

            public void StopQuery()
            {
                OneTickUtils.CancelQuery(cancelHandle);
            }

            // AddRowsToTableCallback is a OneTickCallback and is called by OneTick
            // when OneTick have a new tick / row to deliver to us. When we get it
            // we insert it into our table.
            class AddRowsToTableCallback : CSharpOutputCallback
            {
                OneTickDataAdapter parent;

                private String dbName;
                private String symbolName;
                private String tickType;
                Dictionary<string, int> latestSchema;
                public AddRowsToTableCallback(OneTickDataAdapter parent)
                {
                    this.parent = parent;
                }

                // OneTick callback method to process a tick descriptor. Is called 
                // before the first call to process_event. Class omd::TickDescriptor 
                // contains methods that describe the tick, such as getting the 
                // number of fields, their names, sizes, and types. The ticks for 
                // same security may have different sizes, and sizes, and The 
                // process_tick_descriptor() method is invoked every time before 
                // structure of the tick changes.
                
                public override void process_tick_descriptor(
                    TickDescriptor tickDescriptor) 
                {
                    
                    //set latest schema
                    latestSchema = new Dictionary<string, int>();
                    for (int i = 0; i < tickDescriptor.get_num_of_fields(); i++)
                    {

                        String fieldName = tickDescriptor.get_field(i).get_name();

                        latestSchema.Add(fieldName, i);

                    }

                    if (!parent.idColumnLocation.Equals(OneTickLocation.NONE))
                    {
                        return;
                    }
                    // Check if the IdColumn is stored in the TickDescriptor. 
                    for (int i=0; i<tickDescriptor.get_num_of_fields(); i++)
                    {
                        if (string.Equals(tickDescriptor.get_field(i).get_name(), 
                                parent.settings.IdColumn))
                        {
                            // The idColumn was found here!! So then it will be 
                            // delivered by OneTick in the Tick. So remember that.
                            parent.idColumnLocation = OneTickLocation.TICK;
                        }
                        if (string.Equals(tickDescriptor.get_field(i).get_name(), 
                                parent.settings.TimeIdColumn))
                        {
                            // The TimeidColumn was found here!! So then it will be 
                            // delivered by OneTick in the Tick. Remember that and 
                            // what position.
                            parent.timeIdColumnLocation = OneTickLocation.TICK;
                        }
                    }
                    if (parent.idColumnLocation.Equals(OneTickLocation.NONE))
                    {
                        // The idColumn was not found in the TickDescriptor, then it 
                        // must be one of the special delevered OneTick variables. 
                        // This check must be done after checking the TickDescriptor
                        // because sometimes for some OTQ's for example TICK_TYPE will
                        // be stored in the Tick and sometimes not. So always check 
                        // the TickDescriptor (describing the fields of the Tick) 
                        // first!
                        if (string.Equals(parent.settings.IdColumn, 
                                OneTickLocation.SYMBOL_NAME.ToString()))
                        {                        
                            parent.idColumnLocation = OneTickLocation.SYMBOL_NAME;
                        
                        }
                        else if (string.Equals(parent.settings.IdColumn, 
                                OneTickLocation.DB_NAME.ToString())) 
                        {                        
                            parent.idColumnLocation = OneTickLocation.DB_NAME;
                        
                        }
                        else if (string.Equals(parent.settings.IdColumn, 
                                OneTickLocation.TICK_TYPE.ToString()))
                        {                        
                            parent.idColumnLocation = OneTickLocation.TICK_TYPE;
                        
                        }
                        else if (string.Equals(parent.settings.IdColumn, 
                                OneTickLocation.TIMESTAMP.ToString()))
                        {                        
                            parent.idColumnLocation = OneTickLocation.TIMESTAMP;                        
                        } 
                    }
                    if (parent.timeIdColumnLocation.Equals(OneTickLocation.NONE) && 
                            string.Equals(parent.settings.IdColumn, 
                            OneTickLocation.TIMESTAMP.ToString()))
                    {                    
                        parent.timeIdColumnLocation = OneTickLocation.TIMESTAMP;
                    }
                
                }

                // OneTick callback method to deliver/process a tick.                 
                public override void process_event(Tick tick, DateTime time)
                {
                    DateTime adjustedTime = 
                        parent.settings.TimeZoneHelper.ConvertToUTC(time);
                    TickWrapper tickWrapper = new TickWrapper(adjustedTime, 
                            dbName, symbolName, tickType, tick, latestSchema);
                    
                    // Fetch the idString from the info OneTick just delivered.
                    String idString = null;
                    switch(parent.idColumnLocation)
                    {
                        case OneTickLocation.TICK:
                            idString = parent.GetIdString(tickWrapper);
                            break;
                        case OneTickLocation.SYMBOL_NAME:
                            idString = symbolName;
                            break;
                        case OneTickLocation.DB_NAME:
                            idString = dbName;
                            break;
                        case OneTickLocation.TICK_TYPE:
                            idString = tickType;
                            break;
                        case OneTickLocation.TIMESTAMP:
                            idString = parent.settings.TimeZoneHelper.
                                    ConvertFromUTC(adjustedTime).ToString();
                            break;                            
                    }                    
                    parent.updater.EnqueueUpdate(idString, tickWrapper);
                }

                
                public override void process_symbol_name(String symbol)
                {
                    if (parent.settings.SeparateDBName)
                    {
                        dbName = OneTickUtils.ExtractDBName(symbol);
                        symbolName = OneTickUtils.ExtractSymbolName(symbol);
                    }
                    else
                    {
                        symbolName = symbol;
                    }
                }
                
                public override void process_tick_type(tick_type_t tick_type)
                {
                    tickType = tick_type.get_tick_type();
                }

                // Method that is called after all the ticks for a given request were
                // delivered to this callback object using process_event().
                
                public override void done()
                {
                }

                // Method to create a new instance of a callback object. If the graph  
                // is processed for multiple securities, ticks for each security 
                // will be processed in its own replica of the callback object.

                public override CSharpOutputCallback replicate()
                {
                    AddRowsToTableCallback copy = 
                            new AddRowsToTableCallback(parent);
                    copy.dbName = dbName;
                    copy.symbolName = symbolName;
                    copy.tickType = tickType;
                    return copy;
                }
            }
        }
    }
}
