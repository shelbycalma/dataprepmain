﻿using System;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Microsoft.Win32;

using Panopticon.OneTickPlugin.RealTime;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.OneTickPlugin
{
    public class OneTickSettings : RealtimeConnectionSettings, IDisposable
    {
        private const string DataSourceNameProperty = "DataSourceName";
        private const string ContextNameProperty = "ContextName";

        private const string ProcedureNameProperty = "ProcedureName";

        private const string SymbolListProperty = "SymbolList";
        private const string StartTimeProperty = "StartTime";
        private const string EndTimeProperty = "EndTime";
        private const string TimeZoneProperty = "TimeZone";

        private const string EncloseParameterInQuoteProperty = "EncloseParameterInQuote";
        private const string ParameterInputDirection = "Input";
        private const string ParameterCountProperty = "{0}ParameterCount";
        private const string ParameterNameProperty = "{0}Parameter_{1}_Name";
        private const string ParameterValueProperty = "{0}Parameter_{1}_Value";

        private const string SqlProperty = "Sql";
        private const string SqlGeneratedProperty = "SqlGenerated";

        private const string TimeZoneHelperProperty = "TimeZoneHelper";
        private TimeZoneHelper timeZoneHelper;

        private const string VersionProperty = "Version";

        private const string ShowLocalOTQsProperty = "ShowLocalOTQs";
        private const string ShowRemoteOTQsProperty = "ShowRemoteOTQs";
        private const string SeparateDBNameProperty = "SeparateDBName";
        private const string ShowSymbolErrorAsWarningsProperty = "ShowSymbolErrorAsWarnings";

        private SchemaColumnsSettings<OnetickColumn> schemaColumnSettings;
        private bool disposed;

        public OneTickSettings()
            : this(new PropertyBag())
        {
            Version = new Version("3.0.0.0");
        }

        public OneTickSettings(PropertyBag properties)
            : base(properties, true)
        {
            timeZoneHelper = GetTimeZoneHelper(properties);

            //try to load Context name from registry, this is for backward compatibility.
            if (!string.IsNullOrEmpty(DataSourceName))
            {
                string context = GetDSNSetting("Context", DataSourceName);
                if (!string.IsNullOrEmpty(context) && !context.Equals(ContextName))
                {
                    ContextName = context;
                    DataSourceName = null;
                }
            }
            if (Version.Major < 3 && properties.SubGroups["Columns"] != null)
            {
                // move over the old column definitions to the new SchemaColumnSettings
                PropertyBagCollection columnsBag =
                    properties.SubGroups["Columns"].SubGroups;
                SchemaColumnsSettings.ClearSchemaColumns();
                foreach (PropertyBag bag in columnsBag)
                {
                    if (bag == null) continue;

                    OneTickColumnDefinition colDef = new OneTickColumnDefinition(bag);

                    OnetickColumn onetickColumn = new OnetickColumn();
                    onetickColumn.ColumnName = colDef.Name;
                    onetickColumn.ColumnType = colDef.Type;
                    SchemaColumnsSettings.SchemaColumns.Add(onetickColumn);
                }

                SchemaColumnsSettings.SerializeSchemaColumns();
                // Completed loading the old columns, can clean the bag now. 
                properties.SubGroups["Columns"] = null;
            }
        }

        //This property is obsolute, kept just to have backward compatibility.
        private string DataSourceName
        {
            get { return GetInternal(DataSourceNameProperty); }
            set { SetInternal(DataSourceNameProperty, value); }
        }

        public string ContextName
        {
            get { return GetInternal(ContextNameProperty, "REMOTE"); }
            set { SetInternal(ContextNameProperty, value); }
        }

        public bool EncloseParameterInQuote
        {
            get
            {
                string s = GetInternal(EncloseParameterInQuoteProperty,
                    Convert.ToString(true));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal(EncloseParameterInQuoteProperty,
                    Convert.ToString(value));
            }
        }

        public Version Version
        {
            get
            {
                return new Version(GetInternal(VersionProperty, "1.0.0.0"));
            }
            private set
            {
                SetInternal(VersionProperty, value.ToString());
            }
        }

        public string ProcedureName
        {
            get { return GetInternal(ProcedureNameProperty); }
            set { SetInternal(ProcedureNameProperty, value); }
        }

        public string SymbolList
        {
            get { return GetInternal(SymbolListProperty); }
            set { SetInternal(SymbolListProperty, value); }
        }

        public string StartTime
        {
            get { return GetInternal(StartTimeProperty); }
            set { SetInternal(StartTimeProperty, value); }
        }

        public string EndTime
        {
            get { return GetInternal(EndTimeProperty); }
            set { SetInternal(EndTimeProperty, value); }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return timeZoneHelper;
            }
        }

        public bool ShowLocalOTQs
        {
            get
            {
                string s = GetInternal(ShowLocalOTQsProperty,
                    Convert.ToString(true));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal(ShowLocalOTQsProperty,
                    Convert.ToString(value));
            }
        }

        public bool ShowRemoteOTQs
        {
            get
            {
                string s = GetInternal(ShowRemoteOTQsProperty,
                    Convert.ToString(false));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal(ShowRemoteOTQsProperty,
                    Convert.ToString(value));
            }
        }

        public bool SeparateDBName
        {
            get
            {
                string s = GetInternal(SeparateDBNameProperty,
                    Convert.ToString(false));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal(SeparateDBNameProperty,
                    Convert.ToString(value));
            }
        }

        public bool ShowSymbolErrorAsWarnings
        {
            get
            {
                string s = GetInternal(ShowSymbolErrorAsWarningsProperty,
                    Convert.ToString(true));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal(ShowSymbolErrorAsWarningsProperty,
                    Convert.ToString(value));
            }
        }

        public OneTickParameter[] InputParameters
        {
            get { return GetParameters(ParameterInputDirection); }
            set { SetParameters(ParameterInputDirection, value); }
        }

        private OneTickParameter[] GetParameters(string direction)
        {
            int count = GetInternalInt(
                GetParameterCountProperty(direction), 0);
            OneTickParameter[] parameters = new OneTickParameter[count];
            for (int i = 0; i < count; i++)
            {
                parameters[i] = GetParameter(direction, i);
            }
            return parameters;
        }

        private void SetParameters(
            string direction, OneTickParameter[] parameters)
        {
            if (parameters == null)
            {
                throw Exceptions.ArgumentNull("parameters");
            }
            ClearParameters(direction);
            int count = parameters.Length;
            SetInternalInt(GetParameterCountProperty(direction), count);
            for (int i = 0; i < count; i++)
            {
                SetParameter(direction, i, parameters[i]);
            }
        }

        private int GetParameterCount(string direction)
        {
            return GetInternalInt(GetParameterCountProperty(direction), 0);
        }

        private void SetParameterCount(string direction, int count)
        {
            SetInternalInt(GetParameterCountProperty(direction), count);
        }

        private string GetParameterCountProperty(string direction)
        {
            return string.Format(ParameterCountProperty, direction);
        }

        private OneTickParameter GetParameter(string direction, int index)
        {
            string name = GetInternal(
                GetParameterNameProperty(direction, index));
            string value = GetInternal(
                GetParameterValueProperty(direction, index));

            return new OneTickParameter(name) { Value = value };
        }

        protected void SetParameter(
            string direction, int index, OneTickParameter parameter)
        {
            SetInternal(
                GetParameterNameProperty(direction, index), parameter.Name);
            SetInternal(
                GetParameterValueProperty(direction, index), parameter.Value);
        }

        private string GetParameterNameProperty(string direction, int index)
        {
            return string.Format(ParameterNameProperty, direction, index);
        }

        private string GetParameterValueProperty(string direction, int index)
        {
            return string.Format(ParameterValueProperty, direction, index);
        }

        private void ClearParameters(string direction)
        {
            int count = GetParameterCount(direction);
            for (int i = 0; i < count; i++)
            {
                SetInternal(GetParameterNameProperty(direction, i), null);
                SetInternal(GetParameterValueProperty(direction, i), null);
            }
            SetParameterCount(direction, 0);
        }

        public string Sql
        {
            get { return GetInternal(SqlProperty); }
            set { SetInternal(SqlProperty, value); }
        }

        // This is to make the Java side work when From date is set but not To date.
        // See CreateSqlMethod() in OneTickUtils.
        public string SqlGenerated
        {
            get { return GetInternal(SqlGeneratedProperty, ""); }
            set { SetInternal(SqlGeneratedProperty, value); }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (timeZoneHelper != null)
                    {
                        timeZoneHelper.Dispose();
                    }
                }
                disposed = true;
            }
        }

        internal static TimeZoneHelper GetTimeZoneHelper(PropertyBag bag)
        {
            //TODO: This should may be shifted to TimeZoneHelper,
            //as this may be required in other connector using old timezone implementation.
            TimeZoneHelper timeZoneHelper;

            PropertyBag timeZoneHelperBag =
               bag.SubGroups[TimeZoneHelperProperty];
            bool isOldWorkbook = false;
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups[TimeZoneHelperProperty] = timeZoneHelperBag;
                isOldWorkbook = true;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);

            //For backward compatibility
            if (isOldWorkbook)
            {
                string timeZone = bag.Values[TimeZoneProperty];
                if (!string.IsNullOrEmpty(timeZone) && !timeZone.Equals("null"))
                {
                    timeZoneHelper.SelectedTimeZone = timeZone;
                }
            }

            return timeZoneHelper;
        }

        public static string DSNRegPath
        {
            get
            {
                if (Environment.Is64BitOperatingSystem)
                    return "SOFTWARE\\Wow6432Node\\ODBC\\ODBC.INI\\";
                else
                    return "SOFTWARE\\ODBC\\ODBC.INI\\";
            }
        }

        private string GetDSNSetting(string name, string dsn)
        {
            using (RegistryKey Key = Registry.LocalMachine.OpenSubKey(
                DSNRegPath + dsn))
            {
                if (Key != null)
                {
                    Object value = Key.GetValue(name);
                    if (value != null)
                    {
                        return value.ToString();
                    }
                }
            }
            return string.Empty;
        }

        public override bool Equals(Object obj)
        {
            if (!(obj is OneTickSettings))
            {
                return false;
            }

            if (!base.Equals(obj))
            {
                return false;
            }

            OneTickSettings cs = (OneTickSettings)obj;

            // IdColumnName is also stored in IdColumn,
            // which is checked in RealtimeConnectionSettings.Equals(). 
            if (this.ContextName != cs.ContextName
                 || this.ProcedureName != cs.ProcedureName
                 || this.SymbolList != cs.SymbolList
                 || this.EncloseParameterInQuote != cs.EncloseParameterInQuote
                 || this.StartTime != cs.StartTime
                 || this.EndTime != cs.EndTime
                 || this.Sql != cs.Sql
                 || this.ShowLocalOTQs != cs.ShowLocalOTQs
                 || this.ShowRemoteOTQs != cs.ShowRemoteOTQs
                 || this.SeparateDBName != cs.SeparateDBName
                 || this.ShowSymbolErrorAsWarnings != cs.ShowSymbolErrorAsWarnings
                 || !inputParametersEquals(this.InputParameters,
                            cs.InputParameters)
                 || !this.SchemaColumnsSettings.Equals(cs.SchemaColumnsSettings))
            {
                return false;
            }

            return true;
        }

        private bool inputParametersEquals(OneTickParameter[] params1,
                OneTickParameter[] params2)
        {
            if (params1.Length != params2.Length)
            {
                return false;
            }
            for (int i = 0; i < params1.Length; i++)
            {
                if (params1[i].Name != params2[i].Name ||
                    params1[i].Value != params2[i].Value)
                {
                    return false;
                }
            }
            return true;
        }

        public SchemaColumnsSettings<OnetickColumn> SchemaColumnsSettings
        {
            get
            {
                if (schemaColumnSettings == null)
                {
                    schemaColumnSettings = new SchemaColumnsSettings<OnetickColumn>(ToPropertyBag());
                }
                return schemaColumnSettings;
            }
        }

        public bool IsSchemaRequest { get; set; }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields = {ContextName, ProcedureName,
                SymbolList, StartTime, EndTime, Sql, EncloseParameterInQuote,
                ShowLocalOTQs, ShowRemoteOTQs, SeparateDBName,
                ShowSymbolErrorAsWarnings};

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            foreach (OneTickParameter otp in InputParameters)
            {
                if (otp.Name != null)
                {
                    hashCode ^= otp.Name.GetHashCode();
                }
                if (otp.Value != null)
                {
                    hashCode ^= otp.Value.GetHashCode();
                }
            }

            if(SchemaColumnsSettings != null)
            {
                hashCode ^= SchemaColumnsSettings.GetHashCode();
            }

            return hashCode;
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag bag = base.ToPropertyBag();
            if (Version.Major < 3 && bag.SubGroups["Columns"] != null)
            {
                // Set Columns subgroups to null.
                // These are already loaded and set to SchemaColumnsSettings.
                bag.SubGroups["Columns"] = null;
                // Old workbook settings have already being handled,
                // now we can upgrade the Version.
                Version = new Version("3.0.0.0");

            }
            return bag;
        }
    }
}
