﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using omd.onetick;
using Panopticon.OneTickPlugin.Properties;

// TODO: Change location of schema cache - Temp folder?
// TODO: Fallback if cache writing fails (e.g. on server).

namespace Panopticon.OneTickPlugin.Static
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<OneTickSettings>, ISchemaSavingPlugin
    {
        internal const string PluginId = "OneTickPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "OneTick";
        internal const string PluginType = DataPluginTypes.Database;

        private readonly OneTickLib oneTickLib;

        public Plugin()
        {
            try
            {
                oneTickLib = new OneTickLib(null);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
            }
        }

        public override OneTickSettings CreateSettings(PropertyBag properties)
        {
            OneTickSettings settings = new OneTickSettings(properties);
            if (string.IsNullOrEmpty(settings.Title)) {
                settings.Title = Resources.UiDefaultConnectionTitle;
            }
            return settings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            OneTickSettings settings = CreateSettings(properties);
            OneTickTableBuilder tableBuilder = new OneTickTableBuilder(settings);

            return tableBuilder.CreateTable(parameters, maxRowCount);
        }

        public void SaveSchema(string workbookDir,
            string dataDir, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            OneTickSettings settings = CreateSettings(bag);
            settings.IsSchemaRequest = true;
            OneTickTableBuilder tableBuilder = new OneTickTableBuilder(settings);

            tableBuilder.CreateTable(parameters, 0);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
