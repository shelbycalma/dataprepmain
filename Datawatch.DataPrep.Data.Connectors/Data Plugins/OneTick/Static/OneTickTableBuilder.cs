﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using omd.onetick;

using Panopticon.OneTickPlugin.Properties;

namespace Panopticon.OneTickPlugin.Static
{
    class OneTickTableBuilder
    {
        private OneTickSettings settings;
        private OneTickTable table;
        private int maxRows;
        private QueryCancellationHandle cancelHandle;

        public OneTickTableBuilder(OneTickSettings settings)
        {
            this.settings = settings;
        }

        public OneTickTable CreateTable(IEnumerable<ParameterValue> parameters,
                int maxRows)
        {
            this.maxRows = maxRows;
            table = new OneTickTable(parameters);
            table.BeginUpdate();
            try
            {
                DateTime start = DateTime.Now;

                if (settings.SchemaColumnsSettings.SchemaColumns.Count > 0)
                {
                    DataPluginUtils.AddColumns(table, settings.SchemaColumnsSettings);
                    OneTickUtils.AddAdditionalColumns(table, settings.SeparateDBName);
                }

                ExecuteTableBuilderCallback();
                if (settings.IsSchemaRequest)
                {
                    settings.SchemaColumnsSettings.SerializeSchemaColumns();
                }
                // We're creating a new StandaloneTable on every call, so allow
                // EX to freeze it.
                table.CanFreeze = true;

                // Done, log out how long it took and how much we got.
                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime, table.RowCount,
                    table.ColumnCount, seconds);
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        // Makes a OneTick query to get all TickDescriptor/columns,
        // ticks/rows and add them to our table.
        private void ExecuteTableBuilderCallback()
        {
            // set up the connection
            Connection conn = new Connection();
            conn.connect(settings.ContextName);

            // create the query
            TableBuilderCallback callback =
                    new TableBuilderCallback(this);
            String queryString = "";
            // Send the query
            try
            {
                // Create a handle that can be used to cancel the query on stop.
                cancelHandle = QueryCancellationHandle.create_instance();

                bool isOtqQueryValid = OneTickUtils.IsOtqQueryValid(settings);
                if (settings.Sql != null || !isOtqQueryValid)
                {
                    // Create the SQL query
                    SqlQuery query = OneTickUtils.CreateSqlQuery(settings,
                                table.Parameters, queryString);
                    // Send the SQL query to OneTick and wait here while our 
                    // callback obtain ticks from onetick that we insert in our 
                    // table.
                    RequestGroup.process_sql_query(query, callback, conn, cancelHandle);
                }
                else
                {
                    // Create the normal OTQ-file query
                    OtqQuery query = OneTickUtils.CreateOtqQuery(settings,
                                table.Parameters, queryString);
                    // Send the OTQ query to OneTick and wait here while our 
                    // callback obtain ticks from onetick that we insert in our 
                    // table.

                    RequestGroup.process_otq_file(query, callback, conn, cancelHandle);
                }
            }
            catch (Exception e)
            {
                OneTickUtils.LogQueryException(e, queryString);
                if (!e.Message.Contains(OneTickUtils.IgnoredErrorMessage))
                {
                    throw e;
                }
            }
            finally
            {
                if (conn != null)
                {
                    // Disconnect from OneTick
                    conn.disconnect();
                }
            }
        }

        public void StopQuery()
        {
            OneTickUtils.CancelQuery(cancelHandle);
        }

        // TableBuilderCallback is a OneTick callback class. This one is 
        // used to get the ticks/rows from OneTick and to insert them into the 
        // table.
        class TableBuilderCallback : CSharpOutputCallback
        {

            String symbolName;
            String dbName;
            String tickType;
            Dictionary<string, int> latestSchema;
            OneTickTableBuilder parent;
            bool cancellationStarted = false;

            public TableBuilderCallback(OneTickTableBuilder parent)
            {
                this.parent = parent;
            }

            public override void process_error(int error_code, string error_msg)
            {
                base.process_error(error_code, error_msg);

                if (parent.settings.ShowSymbolErrorAsWarnings)
                {
                    Log.Warning(Properties.Resources.LogErrorCode,
                        error_code, error_msg);
                }
                else
                {
                    throw new Exception(error_msg);
                }
            }

            // OneTick callback method to process a tick descriptor. Is called before 
            // the first call to process_event. Class omd::TickDescriptor contains 
            // methods that describe the tick, such as getting the number of fields, 
            // their names, sizes, and types. The ticks for same security may have 
            // different sizes, and sizes, and The process_tick_descriptor() method 
            // is invoked every time before structure of the tick changes.

            public override void process_tick_descriptor(TickDescriptor tickDescriptor)
            {
                if (parent.settings.SchemaColumnsSettings.SchemaColumns.Count > 0)
                {
                    latestSchema = new Dictionary<string, int>();
                    for (int i = 0; i < tickDescriptor.get_num_of_fields(); i++)
                    {
                        latestSchema.Add(tickDescriptor.get_field(i).get_name(), i);
                    }
                }
                else
                {
                    latestSchema = OneTickUtils.GenerateColumns(tickDescriptor,
                        parent.table, parent.settings);

                    // Add column definations, so that we don't serialize additional columns.

                    OneTickUtils.AddColumnDefinition(parent.settings, parent.table);
                    //If additional columns are already added do nothing and return
                    if (parent.table.AdditionalColumnCount > 0)
                    {
                        return;
                    }

                    OneTickUtils.AddAdditionalColumns(parent.table,
                        parent.settings.SeparateDBName);
                }                    
            }

            // OneTick callback method to deliver/process a tick. 

            public override void process_event(Tick tick, DateTime time)
            {
                if (parent.maxRows > -1 &&
                    parent.table.RowCount >= parent.maxRows)
                {
                    if (!cancellationStarted)
                    {
                        parent.StopQuery();
                        cancellationStarted = true;
                    }
                    //running queries stops immedietly after cancel,
                    //for static queries we may still get few ticks
                    return;
                }
                Object[] values = new Object[parent.table.ColumnCount];
                Row row = parent.table.AddRow(values);

                DateTime adjustedTime =
                        parent.settings.TimeZoneHelper.ConvertToUTC(time);
                TickWrapper tickWrapper = new TickWrapper(adjustedTime,
                        dbName, symbolName, tickType, tick, latestSchema);
                    
                OneTickUtils.InsertTickInfoIntoRowStatic(parent.settings, parent.table,
                    row, tickWrapper);                
            }

            public override void process_symbol_name(String symbol)
            {
                Log.Info(Properties.Resources.LogOneTickDataMethodInvoked, symbol);
                if (parent.settings.SeparateDBName)
                {
                    dbName = OneTickUtils.ExtractDBName(symbol);
                    symbolName = OneTickUtils.ExtractSymbolName(symbol);
                }
                else
                {
                    symbolName = symbol;
                }
            }
            
            public override void process_tick_type(tick_type_t tick_type)
            {
                tickType = tick_type.get_tick_type();
            }

            public override void done()
            {
                // We can't reliably say when this callback method is invoked,
                // It can happen when there are no more ticks to process, or per symbol
                // when symbol error has occured. So we should use it for cleanup.
            }

            // Method to create a new instance of a callback object. If the graph  
            // is processed for multiple securities, ticks for each security 
            // will be processed in its own replica of the callback object.

            public override CSharpOutputCallback replicate()
            {
                TableBuilderCallback copy =
                        new TableBuilderCallback(parent);
                return copy;
            }
        }

    }
}
