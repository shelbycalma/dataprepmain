﻿using System.ComponentModel;
using System.Linq;

namespace Panopticon.OneTickPlugin
{
    public class OneTickParameter : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private static readonly string[] SpecialNames = {
            "_START_TIME_", "_END_TIME_"
        };

        private string name;
        private string value;

        public OneTickParameter()
        {
        }

        public OneTickParameter(string name)
        {
            this.name = name;
        }

        public static OneTickParameter FromField(Schema.Field field)
        {
            string name = field.ColumnName;

            return new OneTickParameter(name);
        }

        private static bool IsSpecialName(string name)
        {
            return SpecialNames.Contains(name);
        }

        public bool IsUserVisible
        {
            get { return !IsSpecialName(name); }
        }

        public string Name
        {
            get { return name; }
            set {
                if (value != name) {
                    name = value;
                    NotifyChanged("Name");
                    NotifyChanged("IsUserVisible");
                }
            }
        }

        public string Value
        {
            get { return value; }
            set {
                if (value != this.value) {
                    this.value = value;
                    NotifyChanged("Value");
                }
            }
        }

        protected void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null) {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
