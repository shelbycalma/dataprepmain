﻿using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.OneTickPlugin
{
    public class OnetickColumn : DatabaseColumn
    {
        public bool IsValid
        {
            get { return !string.IsNullOrEmpty(ColumnName); }
        }
    }
}
