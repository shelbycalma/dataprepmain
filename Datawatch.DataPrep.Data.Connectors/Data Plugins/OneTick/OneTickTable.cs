﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.OneTickPlugin
{
    public class OneTickTable : ParameterTable
    {
        private readonly HashSet<OneTickLocation> additionalColumnsAdded = 
                new HashSet<OneTickLocation>();

        public OneTickTable(IEnumerable<ParameterValue> parameters)
            :base(parameters)
        {        
        }
    
        public void AddAdditionalColumn(OneTickLocation extraColumn)
        {
            additionalColumnsAdded.Add(extraColumn);
        }

        public void ClearAdditionalColumns()
        {
            additionalColumnsAdded.Clear();
        }

        public bool ContainsAdditionalColumn(OneTickLocation location)
        {
            return additionalColumnsAdded.Contains(location);
        }

        public int AdditionalColumnCount
        {
            get { return additionalColumnsAdded.Count; }
        }
    }
}
