﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Panopticon.OneTickPlugin.Schema
{
    public class Query
    {
        public Query()
        {
            this.Inputs = new List<Field>();
        }

        [XmlIgnore]
        public QueryFile File { get; set; }

        [XmlAttribute("Name")]
        public string ProcedureName { get; set; }

        public List<Field> Inputs { get; set; }

        [XmlIgnore]
        public string QueryFileName
        {
            get {
                if (this.ProcedureName == null) {
                    return null;
                }
                int index = this.ProcedureName.IndexOf("::");
                if (index < 0) {
                    return this.ProcedureName;
                }
                return this.ProcedureName.Substring(0, index);
            }
        }

        [XmlIgnore]
        public string QueryName
        {
            get {
                if (this.ProcedureName == null) {
                    return null;
                }
                int index = this.ProcedureName.IndexOf("::");
                if (index < 0) {
                    return this.ProcedureName;
                }
                return this.ProcedureName.Substring(index + 2);
            }
        }
    }
}
