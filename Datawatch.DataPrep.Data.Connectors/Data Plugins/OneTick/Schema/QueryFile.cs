﻿using System.Collections.Generic;

namespace Panopticon.OneTickPlugin.Schema
{
    public class QueryFile
    {
        public QueryFile()
        {
            this.Queries = new List<Query>();
        }

        public OneTickSchema Schema { get; set; }

        public string QueryFileName { get; set; }

        public List<Query> Queries { get; set; }
    }
}
