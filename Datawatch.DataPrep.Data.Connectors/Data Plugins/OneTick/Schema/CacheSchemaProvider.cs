﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.OneTickPlugin.Schema
{
    internal static class CacheSchemaProvider
    {
        private const string FilePrefix = "OneTickConnector.";
        private const string FileSuffix = ".cache";
        private static string pluginId;

        private static string BuildCacheFilename(string contextName)
        {
            string directory = CacheSchemaProvider.CacheDirectory;
            string name = string.Format("{1}{0}{2}",
                contextName, FilePrefix, FileSuffix);
            string path = Path.Combine(directory, name);
            return path;
        }

        public static IEnumerable<string> CachedContextNames
        {
            get {
                List<string> dsns = new List<string>();
                string directory = CacheSchemaProvider.CacheDirectory; 
                if (Directory.Exists(directory)) {
                    foreach (string file in Directory.GetFiles(directory)) {
                        string name = Path.GetFileName(file);
                        if (name.StartsWith(FilePrefix) &&
                            name.EndsWith(FileSuffix)) {
                            string dsn = name.Substring(FilePrefix.Length,
                                name.Length -
                                    (FilePrefix.Length + FileSuffix.Length));
                            dsns.Add(dsn);
                        }
                    }
                }
                return dsns;
            }
        }

        public static string CacheDirectory
        {
            get {
                // deprecated path
                // but we need to check it to achieve backward compatibility
                string path = Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, "Panopticon");
                path = Path.Combine(path, "EX");
                path = Path.Combine(path, pluginId);


                if (Directory.Exists(path))
                {
                    return path;
                }

                path = Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, "Datawatch Desktop");
                path = Path.Combine(path, "Designer");
                path = Path.Combine(path, pluginId);

                return path;
            }
        }

        public static string PluginId 
        {
            get
            {
                return pluginId;
            }
            set
            {
                pluginId = value;
            }
        }        

        public static bool IsCached(string contextName)
        {
            string filename = BuildCacheFilename(contextName);
            return File.Exists(filename);
        }

        public static OneTickSchema Load(string contextName)
        {
            OneTickSchema schema = null;
            string filename = BuildCacheFilename(contextName);
            try {
                XmlSerializer serializer =
                    new XmlSerializer(typeof(OneTickSchema));
                XmlReaderSettings settings = new XmlReaderSettings() {
                };
                using (Stream stream = new FileStream(filename, FileMode.Open))
                using (XmlReader reader =
                    XmlTextReader.Create(stream, settings)) {
                    schema = (OneTickSchema) serializer.Deserialize(reader);
                }
                schema.ContextName = contextName;
            } catch (Exception ex) {
                Log.Exception(ex);
                throw ex;
            }
            return schema;
        }

        public static void Remove(string contextName)
        {
            string filename = BuildCacheFilename(contextName);
            File.Delete(filename);
        }

        public static void Save(string contextName, OneTickSchema schema)
        {
            string filename = BuildCacheFilename(contextName);
            try {
                string folder = Path.GetDirectoryName(filename);
                if (!Directory.Exists(folder)) {
                    Directory.CreateDirectory(folder);
                }
                XmlSerializer serializer =
                    new XmlSerializer(typeof(OneTickSchema));
                XmlWriterSettings settings = new XmlWriterSettings() {
                    Encoding = Encoding.UTF8,
                    Indent = true,
                    NewLineChars = "\n",
                    NewLineHandling = NewLineHandling.Entitize
                };
                using (XmlWriter writer =
                    XmlTextWriter.Create(filename, settings)) {
                    serializer.Serialize(writer, schema);
                }
            } catch (Exception ex) {
                Log.Exception(ex);
            }
        }
    }
}
