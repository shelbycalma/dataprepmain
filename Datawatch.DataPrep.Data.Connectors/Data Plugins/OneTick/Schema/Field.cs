﻿using System.Xml.Serialization;

namespace Panopticon.OneTickPlugin.Schema
{
    public class Field
    {
        public Field()
        {
        }

        [XmlAttribute("Name")]
        public string ColumnName { get; set; }
    }
}
