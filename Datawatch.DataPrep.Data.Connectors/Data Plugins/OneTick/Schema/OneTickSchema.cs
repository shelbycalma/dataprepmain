﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Panopticon.OneTickPlugin.Schema
{
    [Serializable]
    public class OneTickSchema
    {
        public OneTickSchema()
        {
            this.Queries = new List<Query>();
        }

        public string ContextName { get; set; }

        public DateTime LastUpdated { get; set; }

        public List<Query> Queries { get; set; }

        [XmlIgnore]
        public IEnumerable<QueryFile> QueryFiles
        {
            get {
                return this.Queries
                    .Select(q => q.File)
                    .Distinct()
                    .OrderBy(f => f.QueryFileName);
            }
        }
    }
}
