﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Threading;
using System.Windows;
using System.Windows.Resources;
using Datawatch.DataPrep.Data.Framework;
using omd.onetick;

using Panopticon.OneTickPlugin.Properties;

namespace Panopticon.OneTickPlugin.Schema
{
    internal class ServerSchemaProvider
    {
        private Connection conn;
        private OneTickSchema schema;
        private string contextName;
        private const string showOtqUri =
            "pack://application:,,,/Panopticon.OneTickPlugin;component" +
            "/Schema/SHOW_OTQS.otq";
        //For remote OTQs format is
        //remote://REMOTE@{tickserver}::{OTQ_Name}::{Query_Name}
        private const string remoteProcedureName =
            "remote://REMOTE@{0}::{1}::{2}";
        //For local OTQs format is
        //{OTQ_Name}::{Query_Name}
        private const string localProcedureName =
            "{0}::{1}";
        private const string remoteParentOtqFormat =
            "remote://REMOTE@{0}::{1}";

        private bool isDone = false;
        private bool showLocalOTQs = false;
        private bool showRemoteOTQs = false;
        private bool isStatic;

        public ServerSchemaProvider(string contextName, bool showLocalOTQs,
            bool showRemoteOTQs, string pluginId)
        {
            this.contextName = contextName;
            this.showLocalOTQs = showLocalOTQs;
            this.showRemoteOTQs = showRemoteOTQs;

            if (pluginId.Equals(Static.Plugin.PluginId))
            {
                isStatic = true;
            }
            else
            {
                isStatic = false;
            }
        }

        private Query CreateQuery(string parentOtq, string tickServer,
            RequestGroupDescription description)
        {
            string queryName = description.get_query_name();
            string procedureName = GenerateProcedureName(parentOtq, tickServer,
                queryName);
            Query query = new Query()
            {
                ProcedureName = procedureName
            };

            int num = description.get_num_parameters();

            for (int i = 0; i < num; i++)
            {
                RequestGroupParameter param = description.get_parameter(i);

                Field parameter = new Field()
                {
                    ColumnName = param.get_name()
                };
                query.Inputs.Add(parameter);
            }

            return query;
        }

        private string GenerateProcedureName(string parentOtq,
            string tickServer, string queryName)
        {   
            string procedureName = string.Empty;
            string parentOTQName = parentOtq.Replace(OneTickUtils.EXTENSION, "");

            if (!string.IsNullOrEmpty(tickServer))
            {
                procedureName = string.Format(remoteProcedureName, tickServer,
                    parentOTQName, queryName);
            }
            else
            {
                procedureName = string.Format(localProcedureName, parentOTQName,
                    queryName);
            }

            return procedureName;
        }

        public OneTickSchema Load()
        {
            Thread initTableThread = new Thread(LoadQueries);
            initTableThread.Name = "OneTick - Load Schema";
            initTableThread.Start();

            // Wait until our OneTickCallback loads schema.
            lock (this)
            {
                while (!IsDone())
                {
                    Monitor.Wait(this);
                }
            }
            //if schema is still null, throw exception
            if (schema == null)
            {
                throw new Exception(Properties.Resources.ExCouldntLoadShema);
            }
            schema.ContextName = contextName;
            schema.LastUpdated = DateTime.Now;
            return schema;
        }

        private void LoadQueries()
        {
            // set up the connection
            conn = new Connection();
            conn.connect(contextName);

            AddRowsToTableCallback callback = new AddRowsToTableCallback(this);

            try
            {
                String queryString = "";

                StreamResourceInfo showOtqResource = Application.GetResourceStream(
                    new Uri(showOtqUri, UriKind.Absolute));
                using (Stream stream = showOtqResource.Stream)
                {
                    using (StreamReader reader = new StreamReader(stream))
                    {
                        queryString = reader.ReadToEnd();
                    }
                }
                

                if (showLocalOTQs == true)
                {
                    OtqQuery query = new OtqQuery(queryString, queryString.Length);
                    otq_parameters_t otq_params = new otq_parameters_t();                
                    otq_params.set("OTQ_Locality", "LOCAL");
                    query.set_otq_parameters(otq_params);
                    RequestGroup.process_otq_file(query, callback, conn);
                }

                if (showRemoteOTQs == true)
                {
                    //No need to set parameter, default is REMOTE.
                    OtqQuery query = new OtqQuery(queryString, queryString.Length);
                    RequestGroup.process_otq_file(query, callback, conn);
                }
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.LogExecutinQueryError,
                    e.Message, "SHOW_OTQS.otq");
                   
            }
            finally
            {
                if (conn != null)
                {
                    // Disconnect from OneTick
                    conn.disconnect();
                }
                SetDone();
            }
        }

        private void LoadInnerQueries(NameValueCollection OtqFiles)
        {
            if (schema == null)
            {
                schema = new OneTickSchema();
            }
            Dictionary<string, Query> queries =
                    new Dictionary<string, Query>();
            //loop through all the .otq fils
            foreach (string parentOtq in OtqFiles)
            {
                foreach (string tickServer in OtqFiles.GetValues(parentOtq))
                {
                    try
                    {
                        string parentOtqPath = parentOtq;
                        if (!string.IsNullOrEmpty(tickServer))
                        {
                            parentOtqPath = string.Format(remoteParentOtqFormat,
                                    tickServer, parentOtq);
                        }

                        OtqQuery OtqQuery = new OtqQuery(parentOtqPath);
                        OtqQuery.parse(conn);
                        RequestGroupsWithTIP withTip = new RequestGroupsWithTIP();
                        EmptyOutputCallback ep = new EmptyOutputCallback();
                        OtqQuery.extract_queries(withTip, ep);

                        RequestGroupsWithTIP.RequestGroupsWithTIPEnumerator e =
                            new RequestGroupsWithTIP.RequestGroupsWithTIPEnumerator(withTip);

                        //loop through all inner queries.
                        //load based on RunningQueryProperty
                        while (e.MoveNext())
                        {
                            TimeIntervalProperties tip = 
                                e.Current.get_time_interval_properties();

                            bool isRunning = tip.get_running_query_flag();
                            if (isStatic && isRunning)
                            {
                                continue;
                            }
                            else if (!isStatic && !isRunning)
                            {
                                continue;
                            }

                            RequestGroupDescription description =
                                e.Current.get_description();

                            Query query = CreateQuery(parentOtq, tickServer,  description);

                            if (!queries.ContainsKey(query.ProcedureName))
                            {
                                schema.Queries.Add(query);
                                queries.Add(query.ProcedureName, query);
                            }
                            else
                            {
                                Log.Warning(Resources.LogDuplicateProcedure,
                                    query.ProcedureName);
                            }

                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error(Properties.Resources.LogCouldntLoadQuery,
                        parentOtq, e.Message);
                    }
                }
            }

            schema.Queries.Sort((x, y) =>
                    x.ProcedureName.CompareTo(y.ProcedureName));
        }

        public bool IsDone()
        {
            lock (this)
            {
                return isDone;
            }
        }

        public void SetDone()
        {
            lock (this)
            {
                isDone = true;
                Monitor.Pulse(this);
            }
        }

        // AddRowsToTableCallback is a OneTick callback class. This one is 
        // used to get the ticks/rows from OneTick and to insert them into the 
        // table.
        class AddRowsToTableCallback : CSharpOutputCallback
        {
            ServerSchemaProvider parent;
            NameValueCollection OTQFiles;
            public AddRowsToTableCallback(ServerSchemaProvider parent)
            {
                this.parent = parent;
                OTQFiles = new NameValueCollection();
            }

            // OneTick callback method to process a tick descriptor. Is called before 
            // the first call to process_event. Class omd::TickDescriptor contains 
            // methods that describe the tick, such as getting the number of fields, 
            // their names, sizes, and types. The ticks for same security may have 
            // different sizes, and sizes, and The process_tick_descriptor() method 
            // is invoked every time before structure of the tick changes.

            public override void process_tick_descriptor(
                TickDescriptor tickDescriptor)
            {
                // Do nothing, hardwired column names will be used.                
            }

            // OneTick callback method to deliver/process a tick. 

            public override void process_event(Tick tick, DateTime time)
            {
                string otqName = tick.get_string("OTQ_QUERY");
                string tickServer = tick.get_string("TICK_SERVER");
                
                OTQFiles.Add(otqName, tickServer);
            }

            public override void process_symbol_name(String symbol)
            {
                //we don't need to store symbol, db name here.
            }


            public override void process_tick_type(tick_type_t tick_type)
            {
            }

            // Method that is called after all the ticks for a given request were
            // delivered to this callback object using process_event().

            public override void done()
            {
                parent.LoadInnerQueries(OTQFiles);
                parent.SetDone();
            }

            // Method to create a new instance of a callback object. If the graph  
            // is processed for multiple securities, ticks for each security 
            // will be processed in its own replica of the callback object.

            public override CSharpOutputCallback replicate()
            {
                AddRowsToTableCallback copy =
                        new AddRowsToTableCallback(parent);
                return copy;
            }
        }
    }
}
