﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework;


namespace Panopticon.OneTickPlugin.Schema
{
    public static class SchemaProvider
    {
        public static void ClearCache(string contextName)
        {
            if (CacheSchemaProvider.IsCached(contextName)) {
                CacheSchemaProvider.Remove(contextName);
            }
        }

        private static void CreateQueryFiles(OneTickSchema schema)
        {
            Dictionary<string, QueryFile> files =
                new Dictionary<string, QueryFile>();

            foreach (Query query in schema.Queries) {
                string fileName = query.QueryFileName;
                QueryFile file;
                if (!files.TryGetValue(fileName, out file)) {
                    file = new QueryFile() {
                        QueryFileName = fileName
                    };
                    files.Add(fileName, file);
                }
                query.File = file;
                file.Queries.Add(query);
            }
        }

        public static void Initialize(string pluginId)
        {
            CacheSchemaProvider.PluginId = pluginId;
        }

        public static bool IsCached(string contextName)
        {
            return CacheSchemaProvider.IsCached(contextName);
        }

        public static IEnumerable<string> KnownContextNames
        {
            get { return CacheSchemaProvider.CachedContextNames; }
        }

        public static OneTickSchema Load(string contextName,
            bool showLocalOTQs, bool showRemoteOTQs)
        {
            OneTickSchema schema = null;
            if (CacheSchemaProvider.IsCached(contextName)) {
                try {
                    schema = CacheSchemaProvider.Load(contextName);
                } catch (Exception ex) {
                    Log.Exception(ex);
                }
            }
            if (schema == null) {
                ServerSchemaProvider provider =
                    new ServerSchemaProvider(contextName, showLocalOTQs,
                        showRemoteOTQs, CacheSchemaProvider.PluginId);
                schema = provider.Load();
                CacheSchemaProvider.Save(contextName, schema);
            }
            CreateQueryFiles(schema);
            SetParentReferences(schema);
            return schema;
        }

        public static OneTickSchema Reload(string contextName,
            bool showLocalOTQs, bool showRemoteOTQs)
        {
            OneTickSchema schema = null;
            try {
                ServerSchemaProvider provider =
                    new ServerSchemaProvider(contextName, showLocalOTQs,
                        showRemoteOTQs, CacheSchemaProvider.PluginId);
                schema = provider.Load();
                CacheSchemaProvider.Save(contextName, schema);
            } catch (Exception ex) {
                Log.Exception(ex);
                throw ex;
            }
            CreateQueryFiles(schema);
            SetParentReferences(schema);
            return schema;
        }

        internal static void SetParentReferences(OneTickSchema schema)
        {
            foreach (QueryFile file in schema.QueryFiles) {
                file.Schema = schema;
            }
        }
    }
}
