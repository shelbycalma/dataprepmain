﻿namespace Panopticon.OneTickPlugin
{
    // An enum used to remember where we can expect the idColumn and timeIdColumn to 
    // be stored.
    public enum OneTickLocation
    {
        NONE,
        TICK,           // info expected in the Tick variable obtained process_event()
        TIMESTAMP,      // info expected in the time variable obtained process_event()
        SYMBOL_NAME,
        DB_NAME,
        TICK_TYPE
    }
}
