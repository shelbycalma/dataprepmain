﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using omd.onetick;

using Panopticon.OneTickPlugin.Properties;
using Panopticon.OneTickPlugin.RealTime;

namespace Panopticon.OneTickPlugin
{
    public class OneTickUtils
    {
        public static readonly String ONETICK_TIMEFORMAT = "yyyy-MM-dd HH:mm:ss.fff";
        public static readonly String SEPARATOR = "::";
        public static readonly String EXTENSION = ".otq";
        public static readonly String IgnoredErrorMessage = "Terminated by user";
        public static readonly string TimeWindowStart = "TimeWindowStart";
        public static readonly string TimeWindowEnd = "TimeWindowEnd";
        public static readonly string Snapshot = "Snapshot";

        public static readonly string[] SpecialParameters = new[]
        {            
            TimeWindowStart,
            TimeWindowEnd,
            Snapshot
        };

        internal static string ApplySpecialParameters(string sourceString,
            IEnumerable<ParameterValue> parameters, TimeZoneHelper timeZoneHelper,
            bool encloseParameterInQuote)
        {
            if (!timeZoneHelper.TimeZoneSelected)
            {
                return sourceString;
            }

            try
            {
                if (parameters == null) return sourceString;

                foreach (string specialParam in SpecialParameters)
                {
                    string oldKey = "$" + specialParam;
                    string newKey = "{" + specialParam;
                    if (!(sourceString.IndexOf(oldKey) != -1
                    || sourceString.IndexOf(newKey) != -1))
                    {
                        continue; // parameter not used in source string
                    }

                    while (sourceString.Contains(oldKey))
                    {
                        sourceString = sourceString.Replace(oldKey, newKey + "}");
                    }

                    int startBrace = sourceString.IndexOf(newKey);
                    int endBrace = sourceString.IndexOf("}", startBrace);
                    newKey = sourceString.Substring(startBrace, endBrace - startBrace + 1);

                    foreach (ParameterValue parameterValue in parameters)
                    {
                        if (string.IsNullOrEmpty(parameterValue.Name) ||
                            parameterValue.TypedValue == null)
                        {
                            continue;
                        }

                        if (!specialParam.Equals(parameterValue.Name))
                        {
                            continue;
                        }
                        string newValue = "";
                        if (parameterValue.TypedValue is DateTimeParameterValue)
                        {
                            DateTime value = ((DateTimeParameterValue)
                                parameterValue.TypedValue).Value;
                            value = timeZoneHelper.ConvertToUTC(value);
                            newValue = value.ToString(ONETICK_TIMEFORMAT) + " UTC";
                        }
                        else if (parameterValue.TypedValue is StringParameterValue)
                        {
                            String value = ((StringParameterValue)
                                parameterValue.TypedValue).Value;
                            newValue = ConvertToUTC(value, timeZoneHelper);
                        }

                        if (!string.IsNullOrEmpty(newValue))
                        {
                            if (encloseParameterInQuote)
                            {
                                newValue = QuoteLiteralParameterValue(newValue);
                            }
                            sourceString = sourceString.Replace(newKey,
                                newValue);
                        }

                        break;
                    }

                }

            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                Log.Error(Properties.Resources.LogReplacingSpecialParameter,
                    sourceString);
            }
            return sourceString;
        }

        private static String BuildQueryString(OneTickSettings settings)
        {
            String name = settings.ProcedureName;
            String[] names = name.Split(new string[]{SEPARATOR},
                StringSplitOptions.RemoveEmptyEntries);
            switch (names.Length)
            {
                case 1:
                    // OTQ-file
                    return names[0] + EXTENSION;
                case 2:
                    if (names[0].Contains("://") || names[0].Contains("@")) 
                    {
                        // URL:PORT + OTQ-file
                        return names[0] + SEPARATOR + names[1] + EXTENSION;
                    }
                    // OTQ-file + QUERY
                    return names[0] + EXTENSION + SEPARATOR + names[1];
                case 3:
                    // URL:PORT + OTQ-file + QUERY
                    return names[0] + SEPARATOR + names[1] + EXTENSION + SEPARATOR 
                            + names[2];
                default:
                    // Worst case scenario, try this
                    return name;
            }
        }

        public static string ConvertToUTC(string sourceString,
            TimeZoneHelper timeZoneHelper)
        {

            //Apply the time zone offset,from required time zone to UTC 
            string adjustedString = sourceString;
            DateTime adjustedDateTime;
            try
            {
                if (timeZoneHelper.TimeZoneSelected)
                {
                    adjustedDateTime = timeZoneHelper.ConvertToUTC(
                        Convert.ToDateTime(sourceString));

                    //convert it to required format and append UTC
                    adjustedString = adjustedDateTime.ToString(
                        ONETICK_TIMEFORMAT);
                }
                else if (IsDate(sourceString, out adjustedDateTime))
                {
                    adjustedString = adjustedDateTime.ToString(
                        ONETICK_TIMEFORMAT);
                }
            }
            catch
            {

            }

            // timestamps always requires a timezone in the data time value
            // So even no timezone is selected we should append a UTC to it.
            return adjustedString + " UTC"; ;
        }

        public static bool IsOtqQueryValid(OneTickSettings settings)
        {
            if (string.IsNullOrEmpty(settings.ProcedureName))
            {
                return false;
            }
            if ((string.IsNullOrEmpty(settings.StartTime) && 
                string.IsNullOrEmpty(settings.EndTime)) ||
                (!string.IsNullOrEmpty(settings.StartTime) &&
                !string.IsNullOrEmpty(settings.EndTime)))
            {
                return true;
            }

            return false;
        }
        
        public static void CancelQuery(QueryCancellationHandle cancelHandle)
        {
            if (cancelHandle == null)
            {
                return;
            }
            try
            {
                // Return type of cancel_query method changed in API since BUILD_20160130223253.
                // Need to use reflection to call this method, to avoid method not found exception.
                //TODO: Should work on performance penalty due to reflection. Cache the reflected method?
                var type = cancelHandle.GetType();
                MethodInfo cancelQueryMethod = type.GetMethod("cancel_query");
                if (cancelQueryMethod != null)
                {
                    cancelQueryMethod.Invoke(cancelHandle, null);
                    Log.Info(Properties.Resources.LogSentCancelQuery);
                }
                else
                {
                    Log.Error(Properties.Resources.LogStopQueryFailed);
                }
            }
            catch
            {
                //Errors when trying to cancel twice,
                //and couldn't find a method to check if cancellation is already
                //triggered
            }
        }

        public static OtqQuery CreateOtqQuery(OneTickSettings settings, 
                IEnumerable<ParameterValue> parameters, String queryString) 
        {
            queryString = BuildQueryString(settings);

            Log.Info(Resources.LogExecutingQuery, queryString);
            
            OtqQuery query = new OtqQuery(queryString);
            SetQueryParameters(settings, parameters, query);
            return query;
        }
    
        public static SqlQuery CreateSqlQuery(OneTickSettings settings, 
            IEnumerable<ParameterValue> parameters, String queryString) 
        {                
            //Setting only FromDate or ToDate is not allowed in API when using OtqQuery
            //but its allowed for SqlQuery.
            //historically we allowed this when using DSN. For such cases,
            //intead of using OTQ file, we can build a query and use SqlQuery.
            if (settings.Sql != null)
            {
                queryString = ApplySpecialParameters(settings.Sql, parameters,
                    settings.TimeZoneHelper, settings.EncloseParameterInQuote);
                queryString = ReplaceParameters(queryString, parameters,
                    settings.EncloseParameterInQuote);
            }
            else
            {
                queryString = OneTickQueryBuilder.BuildQuery(settings, null);
                // Save this to SqlGenerated make Java work if From date is only set but not 
                // the To date. To avoid Exception.
                settings.SqlGenerated = queryString;
            }

            Log.Info(Resources.LogExecutingQuery, queryString);

            SqlQuery query = new SqlQuery();
            query.set_sql_statement(queryString);
            
            SqlQueryProperties sqlProperties = new SqlQueryProperties();
            sqlProperties.set_separate_dbname_flag(settings.SeparateDBName);
            query.set_sql_query_properties(sqlProperties);
            return query;
        }

        public static String ExtractDBName(String symbol)
        {
            int index = symbol.IndexOf("::");
            if (index > 0) {
                return symbol.Substring(0, index);
            } 
            return "";
        }

        public static String ExtractSymbolName(String symbol)
        {
            int index = symbol.IndexOf("::");
            if (index > 0)
            {
                return symbol.Substring(index + 2);
            } 
            return symbol;
        }

        private static DateTime GetAdjustedTime(OneTickSettings settings,
                String dateString)
        {
            DateTime dt;
            if (DateTime.TryParse(dateString, out dt))
            {
                return settings.TimeZoneHelper.ConvertToUTC(dt);
            }

            throw new Exception(string.Format(Properties.Resources.ExUnableToParseDateTime,
                dateString));
            
        }


        /// <summary>
        /// Creates columns based on TickDescriptor, adds missing additional columns,
        /// and returns TickDescriptor field names and indexes in a dictionary.
        /// </summary>
        /// <param name="tickDescriptor"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public static Dictionary<string, int>  GenerateColumns(
            TickDescriptor tickDescriptor, OneTickTable table,
            OneTickSettings settings)
        {
            Dictionary<string, int> latestSchema = new Dictionary<string, int>();

            try
            {
                table.BeginUpdate();
                for (int i = 0; i < tickDescriptor.get_num_of_fields(); i++)
                {


                    String fieldName = tickDescriptor.get_field(i).get_name();

                    if (latestSchema.ContainsKey(fieldName))
                    {
                        continue;
                    }

                    latestSchema.Add(fieldName, i);

                    if (settings.Version.Major < 2
                            && fieldName.IndexOf(".") != -1)
                    {
                        fieldName = "\"" + fieldName + "\"";
                    }

                    if (table.ContainsColumn(fieldName))
                    {
                        continue;
                    }
                    OnetickColumn onetickColumn = new OnetickColumn();
                    onetickColumn.ColumnName = fieldName;
                    DataType.data_type_t type = tickDescriptor.get_field(i).get_type();
                    if ((type == DataType.data_type_t.TYPE_INT8)
                            || (type == DataType.data_type_t.TYPE_INT16)
                            || (type == DataType.data_type_t.TYPE_INT32)
                            || (type == DataType.data_type_t.TYPE_UINT32)
                            || (type == DataType.data_type_t.TYPE_INT64)
                            || (type == DataType.data_type_t.TYPE_FLOAT)
                            || (type == DataType.data_type_t.TYPE_DOUBLE))
                    {
                        table.AddColumn(new NumericColumn(fieldName));
                        onetickColumn.ColumnType = ColumnType.Numeric;
                    }
                    else if (type == DataType.data_type_t.TYPE_STRING
                        || type == DataType.data_type_t.TYPE_STRING_VARLEN_TMP)
                    {
                        table.AddColumn(new TextColumn(fieldName));
                        onetickColumn.ColumnType = ColumnType.Text;
                    }
                    else if (type == DataType.data_type_t.TYPE_TIME32
                            || type == DataType.data_type_t.TYPE_TIME_MSEC64
                            || type == DataType.data_type_t.TYPE_TIME_NSEC64)
                    {
                        table.AddColumn(new TimeColumn(fieldName));
                        onetickColumn.ColumnType = ColumnType.Time;
                    }
                    bool exist = false;
                    if (settings.IsSchemaRequest)
                    {
                        foreach (OnetickColumn c in
                            settings.SchemaColumnsSettings.SchemaColumns)
                        {
                            if (c.ColumnName.Equals(fieldName))
                            {
                                exist = true;
                            }
                        }
                        if (!exist)
                        {
                            settings.SchemaColumnsSettings.SchemaColumns.Add(
                                onetickColumn);
                        }
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }


            return latestSchema;
        }

        public static void AddAdditionalColumns(OneTickTable table, bool separateDBName)
        {
            table.ClearAdditionalColumns();
            // Add the core columns if they not already have been added
            // Also remember that you added these "manually", because 
            // sometimes they comes with the tick descriptor.
            if (!table.ContainsColumn(
                    OneTickLocation.TIMESTAMP.ToString()))
            {
                table.AddColumn(
                        new TimeColumn(OneTickLocation.TIMESTAMP.ToString()));
                table.AddAdditionalColumn(OneTickLocation.TIMESTAMP);
            }
            if (!table.ContainsColumn(
                    OneTickLocation.DB_NAME.ToString()) &&
                separateDBName)
            {
                table.AddColumn(
                        new TextColumn(OneTickLocation.DB_NAME.ToString()));
                table.AddAdditionalColumn(OneTickLocation.DB_NAME);
            }
            if (!table.ContainsColumn(
                    OneTickLocation.SYMBOL_NAME.ToString()))
            {
                table.AddColumn(
                        new TextColumn(OneTickLocation.SYMBOL_NAME.ToString()));
                table.AddAdditionalColumn(OneTickLocation.SYMBOL_NAME);
            }
            if (!table.ContainsColumn(
                    OneTickLocation.TICK_TYPE.ToString()))
            {
                table.AddColumn(
                        new TextColumn(OneTickLocation.TICK_TYPE.ToString()));
                table.AddAdditionalColumn(OneTickLocation.TICK_TYPE);
            }
        }

        public static ITable GetSchema(OneTickSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
           
            try
            {
                DateTime start = DateTime.Now;
                OneTickTable table = new OneTickTable(parameters);
                table.BeginUpdate();
                try
                {
                    
                    if (settings.SchemaColumnsSettings.SchemaColumns.Count > 0)
                    {
                        DataPluginUtils.AddColumns(table, settings.SchemaColumnsSettings);
                        return table;
                    }
         
                    // Start a query to get the TickDescriptor to add the columns to the 
                    // table. Need to create a thread to being able to stop the query 
                    // when we got the column info.
                    AddColumnsToTable schemaBuilder =
                            new AddColumnsToTable(table, settings);
                    Thread initTableThread = new Thread(
                        schemaBuilder.ConnectAndBuildSchema);
                    initTableThread.Name = "OneTick - Add Columns";
                    initTableThread.Start();

                    // Wait until our OneTickCallback have added all columns to the 
                    // table.
                    lock (schemaBuilder)
                    {
                        while (!schemaBuilder.IsDone())
                        {
                            Monitor.Wait(schemaBuilder);
                        }
                        // We got the columns from OneTick and added them to the table
                        // so now we can tell OneTick to cancel the query.
                        schemaBuilder.StopQuery();
                    }
                }
                finally
                {
                    table.EndUpdate();
                }

                if (settings.IsSchemaRequest)
                {
                    AddColumnDefinition(settings, table);
                    settings.SchemaColumnsSettings.SerializeSchemaColumns();
                }
                Log.Info(Resources.LogCreateTableCompleted,
                    table.ColumnCount, (DateTime.Now - start).TotalSeconds);

                return table;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public static void AddColumnDefinition(OneTickSettings settings, OneTickTable table)
        {
            for (int c = 0; c < table.ColumnCount; c++)
            {
                Column column = table.GetColumn(c);
                OnetickColumn onetickColumn = new OnetickColumn();
                onetickColumn.ColumnName = column.Name;

                if (column is TextColumn)
                {
                    onetickColumn.ColumnType =
                        ColumnType.Text;
                }
                else if (column is TimeColumn)
                {
                    onetickColumn.ColumnType =
                        ColumnType.Time;
                }
                else if (column is NumericColumn)
                {
                    onetickColumn.ColumnType =
                        ColumnType.Numeric;
                }

                settings.SchemaColumnsSettings.SchemaColumns.Add(onetickColumn);
            }
        }

        public static void InsertTickInfoIntoRowRealTime(OneTickSettings settings,
            OneTickTable table, Row row, TickWrapper tickWrapper)
        {
            // Loop through the table columns and update values, if applicable
            // data types exists in tick.
            for (int i = 0; i < table.ColumnCount; i++)
            {
                Column col = table.GetColumn(i);
                String fieldName = col.Name;

                if (fieldName.Equals(settings.IdColumn))
                {
                    continue;
                }

                fieldName = SanitizeFieldName(settings, fieldName);
                if (!tickWrapper.ValueExists(fieldName))
                {
                    SetAdditionalColumnValue(settings, table, row, tickWrapper);
                    continue;
                }
                Object value = tickWrapper.GetValue(fieldName);
                SetColumnValue(settings, row, col, fieldName, value);
            }
        }

        public static void InsertTickInfoIntoRowStatic(OneTickSettings settings,
            OneTickTable table, Row row, TickWrapper tickWrapper)
        {
            // Loop through the table columns and update values, if applicable
            // data types exists in tick.
            for (int i = 0; i < table.ColumnCount; i++)
            {
                Column col = table.GetColumn(i);
                String fieldName = col.Name;

                OneTickLocation location;
                if (Enum.TryParse(fieldName, out location))
                {
                    if (table.ContainsAdditionalColumn(location))
                    {
                        continue;
                    }
                }

                fieldName = SanitizeFieldName(settings, fieldName);
                if (!tickWrapper.ValueExists(fieldName))
                {
                    continue;
                }
                Object value = tickWrapper.GetValue(fieldName);
                SetColumnValue(settings, row, col, fieldName, value);
            }

            // Following columns have beeen added "manually", now these can be
            // set.
            SetAdditionalColumnValue(settings, table, row, tickWrapper);
        }

        private static string SanitizeFieldName(OneTickSettings settings,
            string fieldName)
        {
            if (settings.Version.Major < 2
                                    && fieldName[0] == '"'
                                    && fieldName[fieldName.Length - 1] == '"')
            {
                // Solve problem with old workbooks accepting column names in EXD
                // with " around column name, like "Country", instead of just 
                // Country.
                fieldName = fieldName.Substring(1, fieldName.Length - 2);
            }

            return fieldName;
        }

        private static void SetAdditionalColumnValue(OneTickSettings settings,
            OneTickTable table, Row row, TickWrapper tickWrapper)
        {
            string timestampColumn = OneTickLocation.TIMESTAMP.ToString();
            string symbolColumn = OneTickLocation.SYMBOL_NAME.ToString();
            string DBNameColumn = OneTickLocation.DB_NAME.ToString();
            string tickTypeColumn = OneTickLocation.TICK_TYPE.ToString();

            if (table.ContainsColumn(timestampColumn) &&
              !timestampColumn.Equals(settings.IdColumn))
            {
                DateTime adjustedTime =
                    settings.TimeZoneHelper.ConvertFromUTC(tickWrapper.Time);
                ((TimeColumn)table.GetColumn(timestampColumn)).SetTimeValue(row,
                        adjustedTime);
            }

            if (table.ContainsColumn(symbolColumn) &&
              !symbolColumn.Equals(settings.IdColumn))
            {
                ((TextColumn)table.GetColumn(symbolColumn)).SetTextValue(row,
                        tickWrapper.SymbolName);
            }
            if (table.ContainsColumn(DBNameColumn) &&
              !DBNameColumn.Equals(settings.IdColumn))
            {
                ((TextColumn)table.GetColumn(DBNameColumn)).SetTextValue(row,
                        tickWrapper.DBName);
            }
            if (table.ContainsColumn(tickTypeColumn) &&
              !tickTypeColumn.Equals(settings.IdColumn))
            {
                ((TextColumn)table.GetColumn(tickTypeColumn)).SetTextValue(row,
                        tickWrapper.TickType);
            }
        }

        private static void SetColumnValue(OneTickSettings settings,
            Row row, Column col, string fieldName, object value)
        {
            if (value is Double && col is NumericColumn)
            {
                double doubleValue = ((Double)value);
                ((NumericColumn)col).SetNumericValue(row, doubleValue);
            }
            else if (value is DateTime && col is TimeColumn)
            {
                DateTime dateValue = (DateTime)value;
                // Fix for DDTV-5356.
                // Set Kind property to Unspecified, so TimeZoneHelper always
                // applies select TimeZone. And convert the local date value to UTC.
                if(dateValue.Kind == DateTimeKind.Local)
                {                    
                    dateValue = DateTime.SpecifyKind(dateValue,
                    DateTimeKind.Unspecified);
                    dateValue = TimeZoneHelper.ConvertToUTC(dateValue,
                        TimeZoneInfo.Local.Id);
                }
                
                ((TimeColumn)col).SetTimeValue(row,
                     settings.TimeZoneHelper.ConvertFromUTC(dateValue));
            }
            else if (value is string && col is TextColumn)
            {
                string textValue = value.ToString();
                if (fieldName.Equals(OneTickLocation.SYMBOL_NAME.ToString()))
                {
                    if (settings.SeparateDBName)
                    {
                        textValue = OneTickUtils.ExtractSymbolName(textValue);
                    }
                }
                ((TextColumn)col).SetTextValue(row, textValue);
            }
        }

        private static bool IsDate(string inputDate, out DateTime dt)
        {
            return DateTime.TryParse(inputDate, out dt);
        }

        public static void LogQueryException(Exception ex, string queryString)
        {
            if (!ex.Message.Contains(IgnoredErrorMessage))
            {
                Log.Error(Properties.Resources.LogOneTickQuery, ex.Message, queryString);
            }
        }

        public static string QuoteLiteralParameterValue(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }            
            if (value[0] == '\'')
            {
                return value;
            }
            return string.Format("'{0}'", value);
        }

        // Replaces parameters in query with actual values.
        public static String ReplaceParameters(String query,
                IEnumerable<ParameterValue> parameters,
                bool enforceSingleQuote)
        {
        
            // No parameters, do nothing. Testing for count == 0 would
            // be nice but might be expensive with enumerable?
            if (parameters == null)
            {
                return query;
            }

            ParameterEncoder parameterEncoder = new ParameterEncoder();
            parameterEncoder.Parameters = parameters;
            parameterEncoder.EnforceSingleQuoteEnclosure = enforceSingleQuote;
            parameterEncoder.DefaultDateTimeFormat = ONETICK_TIMEFORMAT;
            parameterEncoder.NullOrEmptyString = "NULL";

            parameterEncoder.SourceString = query;

            return parameterEncoder.Encoded();
        }    
   
        private static void SetQueryParameters(OneTickSettings settings, 
                IEnumerable<ParameterValue> parameters, OtqQuery query)
        {
            otq_parameters_t otq_params = new otq_parameters_t();
            foreach (OneTickParameter parameter in settings.InputParameters)
            {
                if (string.IsNullOrEmpty(parameter.Value))
                {
                    continue;
                }
                String encodedValue = ReplaceParameters(parameter.Value,
                    parameters, false);
                Log.Info(Properties.Resources.LogOTQParameters,
                    parameter.Name, parameter.Value);
                otq_params.set(parameter.Name, encodedValue);                
            }
            query.set_otq_parameters(otq_params);
            // Set Symbol List
            if (!string.IsNullOrEmpty(settings.SymbolList))
            {
                String encodedValue = ReplaceParameters(settings.SymbolList,
                    parameters, false);
                StringCollection collection = new StringCollection();
                collection.Add(encodedValue);
                Log.Info(Properties.Resources.LogOTQParameters,
                    "Symbols", encodedValue);
                query.set_symbols(collection);
            }
            // Set From
            if (!string.IsNullOrEmpty(settings.StartTime))
            {
                String encodedValue = ReplaceParameters(settings.StartTime,
                    parameters, false);
                DateTime date = GetAdjustedTime(settings, encodedValue);

                Log.Info(Properties.Resources.LogOTQParameters,
                    "Start_Time", date.ToString(ONETICK_TIMEFORMAT));
                query.set_start_time(date);
            }
            // Set To
            if (!string.IsNullOrEmpty(settings.EndTime))
            {
                String encodedValue = ReplaceParameters(settings.EndTime,
                    parameters, false);
                DateTime date = GetAdjustedTime(settings, encodedValue);

                Log.Info(Properties.Resources.LogOTQParameters,
                    "End_Time", date.ToString(ONETICK_TIMEFORMAT));
                query.set_end_time(date);
            }
        }
    }
}
