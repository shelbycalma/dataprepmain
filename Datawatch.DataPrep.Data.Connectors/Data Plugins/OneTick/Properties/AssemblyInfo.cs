﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using ST = Panopticon.OneTickPlugin.Static.Plugin;
using RT = Panopticon.OneTickPlugin.RealTime.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.OneTickPlugin.dll")]
[assembly: AssemblyDescription("Datawatch OneTick Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription(RT.PluginTitle, RT.PluginType, RT.PluginId, RT.PluginIsObsolete)]
[assembly: PluginDescription(ST.PluginTitle, ST.PluginType, ST.PluginId, ST.PluginIsObsolete)]
