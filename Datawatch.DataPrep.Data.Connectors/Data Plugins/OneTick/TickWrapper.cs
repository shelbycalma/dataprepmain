﻿using System;
using System.Collections.Generic;

using omd.onetick;

namespace Panopticon.OneTickPlugin
{
    public class TickWrapper
    {
        private DateTime timestamp;
        private String dbName;
        private String symbolName;
        private String tickType;

        private Dictionary<string, Object> values;
        
        public TickWrapper(DateTime timestamp, String dbName,
            String symbolName, String tickType, Tick tick,
            Dictionary<string, int> tickSchema)
        {        
            this.timestamp = timestamp;
            this.dbName = dbName;
            this.symbolName = symbolName;
            this.tickType = tickType;
            values = new Dictionary<string, Object>();

            foreach (string fieldName in tickSchema.Keys)
            {
                DataType.data_type_t type;
                // Now that we are using field index, we should be able to remove
                // this try catch block. Which was there just to check whether we
                // can use a field name safely against tick.
                try
                {
                    type = tick.get_type(tickSchema[fieldName]);
                }
                catch (OneTickException rte)
                {
                    continue;
                }

                Object value = GetTickValue(tickSchema[fieldName], tick);
                values[fieldName] = value;
            }
        }

        public DateTime Time
        {
            get { return timestamp; }
        }
    
        public String DBName
        {
            get { return dbName; }
        }
    
        public String SymbolName
        {
            get { return symbolName; }
        }
    
        public String TickType
        {
            get { return tickType; }
        }

        public Dictionary<string, Object> Values
        {
            get { return values; }
        }

        public object GetValue(string fieldName)
        {
            if (!values.ContainsKey(fieldName))
            {
                return null;
            }
            return values[fieldName];
        }
        
        // Return the value from the tick for the field with name fieldName.
        // The returned Object can after this call be cast to right type.
        private static Object GetTickValue(int fieldIndex, Tick tick)
        {
            DataType.data_type_t type = tick.get_type(fieldIndex);
            if ((type == DataType.data_type_t.TYPE_INT8)
                || (type == DataType.data_type_t.TYPE_INT16)
                || (type == DataType.data_type_t.TYPE_INT32)
                || (type == DataType.data_type_t.TYPE_UINT32))
            {
                return (double)tick.get_int(fieldIndex);
            }
            else if (type == DataType.data_type_t.TYPE_INT64)
            {
                return (double)tick.get_int64(fieldIndex);

            }
            else if ((type == DataType.data_type_t.TYPE_FLOAT)
                    || (type == DataType.data_type_t.TYPE_DOUBLE))
            {
                return tick.get_double(fieldIndex);

            }
            else if (type == DataType.data_type_t.TYPE_TIME32)
            {
                int time = tick.get_int(fieldIndex);
                if (time == 0)
                {
                    return new DateTime(long.MinValue);
                }
                return new DateTime(time * 1000);

            }
            else if (type == DataType.data_type_t.TYPE_TIME_MSEC64 ||
                type == DataType.data_type_t.TYPE_TIME_NSEC64)
            {
                long time = tick.get_int64(fieldIndex);
                if (time == 0)
                {
                    return DateTime.MinValue;
                }

                DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                DateTime date = start.AddMilliseconds(time).ToLocalTime();
                return date;
            }
            else if (type == DataType.data_type_t.TYPE_STRING ||
                type == DataType.data_type_t.TYPE_STRING_VARLEN_TMP)
            {
                return tick.get_string(fieldIndex);
            }
            return null;
        }

        public bool ValueExists(string fieldName)
        {
            return values.ContainsKey(fieldName);
        }
    }
}
