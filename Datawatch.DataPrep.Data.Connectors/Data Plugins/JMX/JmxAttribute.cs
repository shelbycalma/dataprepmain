﻿namespace Panopticon.JMXPlugin
{
    public class JmxAttribute
    {
        public string Name { get; set; }
        public DataType DataType { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            JmxAttribute other = (JmxAttribute) obj;
            return Name.Equals(other.Name) && DataType.Equals(other.DataType);
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() ^ DataType.GetHashCode();
        }
    }
}
