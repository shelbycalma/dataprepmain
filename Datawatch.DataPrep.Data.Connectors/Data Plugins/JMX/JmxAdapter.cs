﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.JMXPlugin
{
    public class JmxAdapter: RealtimeDataAdapter<ParameterTable, JmxSettings, object[]>
    {
        public override void Start()
        {
            throw Exceptions.WrongLocationException();
        }

        public override void Stop()
        {
            throw Exceptions.WrongLocationException();
        }

        public override void UpdateColumns(Row row, object[] evt)
        {
            throw Exceptions.WrongLocationException();
        }

        public override DateTime GetTimestamp(object[] evt)
        {
            throw Exceptions.WrongLocationException();
        }
    }
}