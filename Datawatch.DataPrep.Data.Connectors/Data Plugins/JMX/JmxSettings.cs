﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.JMXPlugin
{
    public class JmxSettings: RealtimeConnectionSettings
    {
        public JmxSettings(PropertyBag settings)
            : base(settings, false)
        {
            InitializeCandidates();
        }

        public string JmxServiceUrl
        {
            get { return GetInternal("JmxServiceUrl"); }
            set { SetInternal("JmxServiceUrl", value); }
        }

        public string MBeanName
        {
            get { return GetInternal("MBeanName"); }
            set { SetInternal("MBeanName", value); }
        }

        public IList<JmxAttribute> JmxAttributes
        {
            get
            {
                List<JmxAttribute> result = new List<JmxAttribute>();
                PropertyBag attributes = GetInternalSubGroup("JmxAttributes");
                if (attributes == null)
                {
                    return null;
                }
                foreach (PropertyValue value in attributes.Values)
                {
                    result.Add(new JmxAttribute
                    {
                        Name = value.Name,
                        DataType = new DataType(value.Value)
                    });
                }
                return result;
            }
            set
            {
                PropertyBag attributes = new PropertyBag();
                foreach (JmxAttribute newValue in value)
                {
                    if (newValue.DataType == null || newValue.Name == null)
                    {
                        continue;
                    }
                    attributes.Values.Add(new PropertyValue(newValue.Name,
                        newValue.DataType.ToString()));
                }

                SetInternalSubGroup("JmxAttributes", attributes);
            }
        }

        public override string IdColumn
        {
            get { return Properties.Resources.UiAttributeName; }
            set
            {
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is JmxSettings))
            {
                return false;
            }

            if (!(base.Equals(obj)))
            {
                return false;
            }

            JmxSettings other = (JmxSettings) obj;
            if (!JmxServiceUrl.Equals(other.JmxServiceUrl) ||
                !MBeanName.Equals(other.MBeanName) ||
                !JmxAttributes.SequenceEqual(other.JmxAttributes))
            {
                return false;
            }
            
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;
            object[] fields =
            {
                JmxServiceUrl, MBeanName,
                TimeIdColumn, IdColumn,
                TimeWindowSeconds, Limit
            };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            foreach (JmxAttribute attribute in JmxAttributes)
            {
                hashCode ^= attribute.GetHashCode();
            }

            return hashCode;
        }
    }
}