﻿using System;

namespace Panopticon.JMXPlugin
{
    public class DataType
    {
        private string fullName;
        private string name;
        private string image;
        private Type type;

        public const string TextName = "Text";
        public const string NumericName = "Numeric";
        public const string DateTimeName = "DateTime";

        public DataType(Type type)
        {
            this.type = type;
            this.fullName = type.FullName;

            if (type == typeof(String))
            {
                this.name = "Text";
                this.image = "/Images/TextColumn.png";
            }
            else if (type == typeof(Double))
            {
                this.name = "Numeric";
                this.image = "/Images/NumericColumn.png";
            }
            else if (type == typeof(DateTime))
            {
                this.name = "DateTime";
                this.image = "/Images/TimeColumn.png";
            }
        }

        public DataType(string typeName)
        {
            this.name = typeName;
            if (typeName == "Text")
            {
                this.type = typeof (String);
                this.image = "/Images/TextColumn.png";
            }
            else if (typeName == "Numeric")
            {
                this.type = typeof (Double);
                this.image = "/Images/NumericColumn.png";
            }
            else if (typeName == "DateTime")
            {
                this.type = typeof (DateTime);
                this.image = "/Images/TimeColumn.png";
            }
            this.fullName = this.type.FullName;
        }

        public string FullName
        {
            get { return fullName; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Image
        {
            get { return image; }
        }

        public bool IsFormatMutable
        {
            get { return type == typeof(Double); }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is DataType))
            {
                return false;
            }

            DataType other = (DataType) obj;

            return this.fullName.Equals(other.fullName);
        }

        public override int GetHashCode()
        {
            return fullName.GetHashCode();
        }

        public override string ToString()
        {
            return name;
        }
    }
}
