﻿using System;

namespace Panopticon.JMXPlugin
{
    public class Exceptions: Datawatch.DataPrep.Data.Framework.Exceptions
    {
        public static Exception WrongLocationException()
        {
            return CreateNotSupported(Properties.Resources.ExInvalidLocation);
        } 
    }
}   