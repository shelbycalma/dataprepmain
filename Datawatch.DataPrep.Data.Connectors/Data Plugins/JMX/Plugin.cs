﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.JMXPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin: RealtimeDataPlugin<ParameterTable, JmxSettings, 
        object[], JmxAdapter>, ILocationSpecific
    {
        internal const string PluginId = "JMX";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "JMX";
        internal const string PluginType = DataPluginTypes.Streaming;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override JmxSettings CreateSettings(PropertyBag bag)
        {
            return new JmxSettings(bag);
        }

        public DataLocation GetEnabledLocation(PropertyBag settings)
        {
            return DataLocation.ServerSide;
        }

        protected override ITable CreateTable(JmxSettings settings, 
            IEnumerable<ParameterValue> parameters, 
            string dataDirectory)
        {
            if (settings == null) throw new ArgumentNullException(nameof(settings));

            ParameterTable table = new ParameterTable(parameters);
            IList<JmxAttribute> attributes = settings.JmxAttributes;

            table.BeginUpdate();

            try
            {
                table.AddColumn(new TextColumn(settings.IdColumn));
                table.AddColumn(
                    new TextColumn(Properties.Resources.UiMBeanNameKey));
                if (attributes.All(attribute => Equals(attribute.DataType,
                    new DataType(typeof (double)))))
                {
                    table.AddColumn(
                        new NumericColumn(
                            Properties.Resources.UiAttributeValue));
                }
                else
                {
                    table.AddColumn(new TextColumn(
                        Properties.Resources.UiAttributeValue));
                }
                table.AddColumn(new TimeColumn(
                    Properties.Resources.UiEventTime));
            }

            finally
            {
                table.EndUpdate();
            }
            return table;
        }
    }
}