﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.MDXExtendedPlugin.ClientUtils;
using Panopticon.MDXExtendedPlugin.MdxQueryBuilder;

namespace Panopticon.MDXExtendedPlugin.UI
{
    public class MDXSettingsViewModel : ViewModelBase
    {
        private readonly ConnectionPropertiesViewModel _connectionPropertiesViewModel;
        private readonly MDXSettings model;

        private IMDXClientUtil MdxClientUtil;

        private string[] availableCubes;
        private string[] avaliableCatalogs;
        private string catalog;
        private ObservableCollection<IParameterizedMetadata> columns;
        private string cube;

        private bool isCubeRetrieved;

        private bool isCubesRetrieved;
        private bool isLogged;
        private ObservableCollection<IParameterizedMetadata> measures;

        private bool nullSuppression;
        private ObservableCollection<ParameteredDimension> parameteredDimensions;
        private ObservableCollection<ParameterValue> parametersCollection;
        private Cube retrievedCube;

        private ObservableCollection<IParameterizedMetadata> rows;

        private object selectedColumn;
        private ParameteredMeasure selectedMeasure;

        private object selectedTreeViewItem;
        private ObservableCollection<IParameterizedMetadata> slicers;

        private string manualQuery;
        private string generatedQuery;

        public MDXSettingsViewModel(MDXSettings model, IEnumerable<ParameterValue> parameters)
        {
            if (model == null)
            {
                throw Exceptions.ArgumentNull("model");
            }

            this.model = model;
            this._connectionPropertiesViewModel = new ConnectionPropertiesViewModel(model);
            this.catalog = model.Catalog;
            this.cube = model.CubeName;

            if (parameters != null)
                parameters.ToList().ForEach(p => ParametersCollection.Add(p));

            if (model.ManualEdit)
            {
                this.ManualQuery = model.Query;
                GenerateMdxQuery();
            }
        }

        public object SelectedTreeViewItem
        {
            get { return selectedTreeViewItem; }
            set
            {
                selectedTreeViewItem = value;
                OnPropertyChanged("SelectedTreeViewItem");
            }
        }

        public ProviderNames Provider
        {
            get { return Model.Provider; }
            set
            {
                Model.Provider = value;
                OnPropertyChanged("Provider");
            }
        }

        public List<ProviderNames> Providers
        {
            get
            {
                // PROVIDER IMPLEMENTATION : Add new provider names to list
                return new List<ProviderNames>(new[]
                {
                    ProviderNames.Adomd,
                    //ProviderNames.SampleProvider, //MB: removed it for now
                    ProviderNames.SapAdo,
                    ProviderNames.Generic
                });
            }
        }

        public ConnectionPropertiesViewModel ConnectionPropertiesViewModel
        {
            get { return _connectionPropertiesViewModel; }
        }

        public string Catalog
        {
            get { return catalog; }
            set
            {
                catalog = value;
                OnPropertyChanged("Catalog");
            }
        }

        public string Cube
        {
            get { return cube; }
            set
            {
                cube = value;
                OnPropertyChanged("Cube");
                OnPropertyChanged("HasCubeSelected");
            }
        }

        public ObservableCollection<ParameterValue> ParametersCollection
        {
            get
            {
                return parametersCollection ?? (parametersCollection = new ObservableCollection<ParameterValue>
                {
                    new ParameterValue
                    {
                        Name = ""
                    }
                });
            }
            set
            {
                if (parametersCollection == value) return;
                parametersCollection = value;
                OnPropertyChanged("ParametersCollection");
            }
        }

        public ObservableCollection<IParameterizedMetadata> Columns
        {
            get { return columns ?? (columns = new ObservableCollection<IParameterizedMetadata>()); }
            set
            {
                if (columns == value) return;
                columns = value;
                OnPropertyChanged("Columns");
            }
        }

        public ObservableCollection<IParameterizedMetadata> Rows
        {
            get { return rows ?? (rows = new ObservableCollection<IParameterizedMetadata>()); }
            set
            {
                if (rows == value) return;
                rows = value;
                OnPropertyChanged("Rows");
            }
        }

        public ObservableCollection<IParameterizedMetadata> Slicers
        {
            get { return slicers ?? (slicers = new ObservableCollection<IParameterizedMetadata>()); }
            set
            {
                if (slicers == value) return;
                slicers = value;
                OnPropertyChanged("Slicers");
            }
        }

        public ObservableCollection<IParameterizedMetadata> Measures
        {
            get { return measures ?? (measures = new ObservableCollection<IParameterizedMetadata>()); }
            set
            {
                if (measures == value) return;
                measures = value;
                OnPropertyChanged("Measures");
            }
        }

        public string ManualQuery
        {
            get { return manualQuery; }
            set
            {
                manualQuery = value;
                Model.Query = ManualQuery;
                OnPropertyChanged("ManualQuery");
            }
        }

        public string GeneratedQuery
        {
            get { return generatedQuery; }
            set
            {
                generatedQuery = value;
                if (!Model.ManualEdit)
                {
                    ManualQuery = value;
                    Model.Query = GeneratedQuery;
                }
                OnPropertyChanged("GeneratedQuery");
            }
        }

        public bool HasCubeSelected
        {
            get { return !String.IsNullOrWhiteSpace(Cube); }
        }

        public IEnumerable<FillRaggedHierarchyTypes> FillRaggedHierarchyTypes
        {
            get
            {
                return Enum.GetValues(typeof (FillRaggedHierarchyTypes))
                    .Cast<FillRaggedHierarchyTypes>();
            }
        }

        public object SelectedColumn
        {
            get { return selectedColumn; }
            set
            {
                if (selectedColumn == value) return;
                selectedColumn = value;
                OnPropertyChanged("SelectedColumn");
            }
        }

        public ParameteredMeasure SelectedMeasure
        {
            get { return selectedMeasure; }
            set
            {
                if (selectedMeasure == value) return;
                selectedMeasure = value;
                OnPropertyChanged("SelectedMeasure");
            }
        }

        public string[] AvaliableCubes
        {
            get { return availableCubes ?? (availableCubes = new string[] {}); }
            set
            {
                if (availableCubes == value) return;
                availableCubes = value;
                OnPropertyChanged("AvaliableCubes");
            }
        }
        public string[] AvaliableCatalogs
        {
            get { return avaliableCatalogs ?? (avaliableCatalogs = new string[] {}); }
            set
            {
                if (avaliableCatalogs == value) return;
                avaliableCatalogs = value;
                OnPropertyChanged("AvaliableCatalogs");
            }
        }

        public Cube RetrievedCube
        {
            get { return retrievedCube ?? (retrievedCube = new Cube()); }
            set
            {
                if (retrievedCube == value) return;
                retrievedCube = value;
                OnPropertyChanged("RetrievedCube");
            }
        }

        public bool IsLogged
        {
            get {
                return isLogged; 
            }
            set
            {
                if (isLogged == value) return;
                isLogged = value;
                OnPropertyChanged("IsLogged");
            }
        }

        public bool IsCubeRetrieved
        {
            get {
                return isCubeRetrieved; 
            }
            set
            {
                if (isCubeRetrieved == value) return;
                isCubeRetrieved = value;
                OnPropertyChanged("IsCubeRetrieved");
            }
        }

        public bool IsCubesRetrieved
        {
            get {
                return isCubesRetrieved;
            }
            set
            {
                if (isCubesRetrieved == value) return;
                isCubesRetrieved = value;
                OnPropertyChanged("IsCubesRetrieved");
            }
        }

        public bool NullSuppression
        {
            get { return model.NullSuppression; }
            set
            {
                if (Model.NullSuppression == value) return;
                Model.NullSuppression = value;
                GenerateMdxQuery();
                OnPropertyChanged("NullSuppression");
            }
        }

        public bool IsOK
        {
            get
            {
                try
                {
                    return model != null &&
                       !string.IsNullOrWhiteSpace(model.CubeName) &&
                       (model.ManualEdit
                           ? !string.IsNullOrWhiteSpace(model.Query)
                           : (Rows.Any() && Columns.Any()));
                }
                catch (Exception Ex)
                {
                    Log.Exception(Ex);
                    return false;
                }
            }
        }

        public MDXSettings Model
        {
            get { return model; }
        }

        public ObservableCollection<ParameteredDimension> ParameteredDimensions
        {
            get
            {
                return parameteredDimensions ??
                       (parameteredDimensions = new ObservableCollection<ParameteredDimension>());
            }
            set
            {
                if (parameteredDimensions == value) return;
                parameteredDimensions = value;
                OnPropertyChanged("ParameteredDimensions");
            }
        }

        #region Commands Fields and Properties

        private ICommand addToColumnsCommand;
        private ICommand addToRowsCommand;
        private ICommand addToSlicerCommand;
        private ICommand fetchCubesCommand;
        private ICommand loginCommand;
        private ICommand removeColumnCommand;
        private ICommand removeRowsCommand;
        private ICommand removeSlicerCommand;
        private ICommand retrieveCommand;

        public ICommand AddToColumnsCommand
        {
            get
            {
                return addToColumnsCommand ??
                       (addToColumnsCommand = new DelegateCommand<object>(this.AddToColumns));
            }
        }

        public ICommand AddToRowsCommand
        {
            get
            {
                return addToRowsCommand ??
                       (addToRowsCommand = new DelegateCommand<object>(this.AddToRows));
            }
        }

        public ICommand AddToSlicerCommand
        {
            get
            {
                return addToSlicerCommand ??
                       (addToSlicerCommand = new DelegateCommand<object>(AddToSlicer));
            }
        }

        public ICommand RemoveSlicerCommand
        {
            get
            {
                return removeSlicerCommand ??
                       (removeSlicerCommand = new DelegateCommand<object>(RemoveSlicer));
            }
        }

        public ICommand RemoveRowsCommand
        {
            get
            {
                return removeRowsCommand ??
                       (removeRowsCommand = new DelegateCommand<object>(RemoveRows));
            }
        }

        public ICommand RemoveColumnsCommand
        {
            get
            {
                return removeColumnCommand ??
                       (removeColumnCommand = new DelegateCommand<object>(RemoveColumns));
            }
        }

        public ICommand LoginCommand
        {
            get
            {
                return loginCommand ??
                       (loginCommand = new DelegateCommand(Login, LoginGranted));
            }
        }

        public ICommand RetrieveCommand
        {
            get
            {
                return retrieveCommand ??
                       (retrieveCommand = new DelegateCommand(RetrieveMetadata, () => IsLogged));
            }
        }

        public ICommand FetchCubesCommand
        {
            get
            {
                return fetchCubesCommand ??
                       (fetchCubesCommand = new DelegateCommand(RetrieveCubes, () => IsLogged));
            }
        }

        #endregion

        internal void GenerateMdxQuery()
        {
            QueryBuilder queryBuilder = new QueryBuilder(Model);

            List<string> columnsUniqueNames = new List<string>(this.Columns.Count);
            List<string> rowsUniqueNames = new List<string>(this.Rows.Count);
            List<string> slicersUniqueNames = new List<string>(this.Slicers.Count);

            this.BuildAxis(this.Columns, queryBuilder.AddColumn, null, columnsUniqueNames);
            this.BuildAxis(this.Rows, queryBuilder.AddRow, queryBuilder.AddRowParameter, rowsUniqueNames);
            this.BuildAxis(this.Slicers, queryBuilder.AddSlicer, queryBuilder.AddSlicerParameter, slicersUniqueNames);

            SaveParameters();

            this.Model.ColumnsAxis = columnsUniqueNames.ToArray();
            this.Model.RowsAxis = rowsUniqueNames.ToArray();
            this.Model.SlicerAxis = slicersUniqueNames.ToArray();

            GeneratedQuery = queryBuilder.BuildExpression().ToExpression();
        }

        public void SaveParameters()
        {
            foreach (IParameterizedMetadata item in this.Rows.Concat(this.Slicers))
            {
                if (item.Parameter != null && !string.IsNullOrEmpty(item.Parameter.Name))
                {
                    this.Model.SaveParameter(item.UniqueName, item.Parameter.Name);
                }
                else
                {
                    this.Model.RemoveParameter(item.UniqueName);
                }
            }
        }

        private void BuildAxis(IEnumerable<IParameterizedMetadata> elements, Action<MetadataElementBase> addMethod,
            Action<MetadataElementBase, string> addParameterMethod, List<string> uniqueNames)
        {
            foreach (IParameterizedMetadata element in elements)
            {
                uniqueNames.Add(element.UniqueName);
                TryAdd(element, addMethod, addParameterMethod);
            }
        }

        private static void TryAdd(IParameterizedMetadata element, Action<MetadataElementBase> addMethod,
            Action<MetadataElementBase, string> addParameterMethod)

        {
            MetadataElementBase parameteredDimension = element.Element;
            if (parameteredDimension == null) return;

            addMethod(parameteredDimension);

            if (addParameterMethod == null) return;

            ParameterValue parameter = element.Parameter;

            if (parameter != null && !string.IsNullOrEmpty(parameter.Name))
                addParameterMethod(parameteredDimension, parameter.Name);
        }

        private void Login()
        {
            Login(true);
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            if (propertyName == "Provider")
            {
                ConnectionPropertiesViewModel.RefreshSettings();
                RestoreDefaults();
            }
            base.OnPropertyChanged(propertyName);
        }

        private void Login(bool restoreSettings)
        {
            try
            {
                if (restoreSettings)
                {
                    RestoreDefaults();
                }
                _connectionPropertiesViewModel.SaveToPropertyBag();
                MdxClientUtil = Plugin.GetClientUtil(model);
                RetrieveCatalogs();
            }
            catch (Exception exc)
            {
                MessageBox.Show(
                    //Utils.GetOwnerWindow(this),
                    exc.Message,
                    "MDX",
                    MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }


        public void LoadAllDataBySettings()
        {
            if (string.IsNullOrWhiteSpace(Model.ConnectionProperties["ServerName"].Value)) return;

            this.Login(false);
            if (!IsLogged) return;

            this.RetrieveCubes(false);
            this.RetriveCubeMetadata();
            if (!IsCubeRetrieved) return;

            this.RetrieveMetadata(false);

            foreach (string columnAxis in this.Model.ColumnsAxis)
            {
                IParameterizedMetadata parameteredValue = this.GetParameteredAxis(columnAxis);
                if (parameteredValue != null && !String.IsNullOrEmpty(parameteredValue.UniqueName))
                    this.Columns.Add(parameteredValue);
            }

            foreach (string rowAxis in this.Model.RowsAxis)
            {
                IParameterizedMetadata parameteredValue = this.GetParameteredAxis(rowAxis);
                if(parameteredValue != null && !String.IsNullOrEmpty(parameteredValue.UniqueName))
                    this.Rows.Add(parameteredValue);
            }

            foreach (string slicerAxis in this.Model.SlicerAxis)
            {
                IParameterizedMetadata parameteredValue = this.GetParameteredAxis(slicerAxis);
                if (parameteredValue != null && !String.IsNullOrEmpty(parameteredValue.UniqueName))
                    this.Slicers.Add(parameteredValue);
            }
        }

        private IParameterizedMetadata GetParameteredAxis(string uniqueName)
        {
            IParameterizedMetadata parameteredValue;
            string[] levels = uniqueName.Split('.').ToArray();

            if (levels[0].Contains("[Measures]"))
            {
                Measure measure = this.RetrievedCube.GetMeasure(uniqueName);
                parameteredValue = new ParameteredMeasure {Element = measure};
            }
            else
            {
                Dimension dim = this.RetrievedCube.GetDimenision(levels[0]);
                if (dim == null)
                {
                    // todo: might want to think of a better solution to this, this is an SAP specific fix. 
                    // SAP UniqueNames do not always include the dimension name, which GetParameteredAxis depends upon
                    // possibly cleaner solution to this is to not use UniqueName to re-lookup the axis specifications,
                    // and rather use a property which for most providers is equal to UniqueName, but for SAP it can be
                    // overridden to be something we construct to include the dimension name.
                    return FindAxisWithoutDimensionName(levels);
                }
                if (levels.Length >= 2)
                {
                    Hierarchy h = dim.GetHierarchy(levels[1]);
                    if (levels.Length == 3)
                    {
                        Level l = h.GetLevel(levels[2]);
                        parameteredValue = this.ToParameteredLevel(l);
                    }
                    else
                    {
                        parameteredValue = this.ToParameteredHierarchy(h);
                    }
                }
                else
                {
                    parameteredValue = this.ToParameteredDimension(dim);
                }
            }
            return parameteredValue;
        }

        /// <summary>
        /// Some providers (like SAP) not include the entire object path in the UniqueName for a hierarchy
        /// This attempts to see if the axis specification name starts with a hierarchy rather than a dimension name.
        /// </summary>
        /// <param name="axisNameLevels"></param>
        /// <returns></returns>
        private IParameterizedMetadata FindAxisWithoutDimensionName(string[] axisNameLevels)
        {
            IParameterizedMetadata parameteredValue = null;

            foreach (var dimnension in RetrievedCube.Dimensions)
            {
                // we do not have the dimension name, so we don't want to use GetHierarchy(string)
                Hierarchy hierarchy = dimnension.GetHierarchyByFullName(axisNameLevels[0]);

                if (hierarchy != null)
                {
                    if (axisNameLevels.Length == 2)
                    {
                        Level level = hierarchy.GetLevel(axisNameLevels[1]);
                        if (level != null)
                        {
                            parameteredValue = this.ToParameteredLevel(level);
                        }
                    }
                    else
                    {
                        parameteredValue = this.ToParameteredHierarchy(hierarchy);
                    }
                    break;
                }
            }
            return parameteredValue;
        }

        private ParameteredDimension ToParameteredDimension(Dimension dim)
        {
            return new ParameteredDimension
            {
                Element = dim,
                Parameter = this.GetParameterValue(this.Model.GetParameter(dim.UniqueName)),
                ParameteredHierarchyInfos = dim.HierarchyInfos.Select(this.ToParameteredHierarchy).ToList()
            };
        }

        private ParameteredHierarchy ToParameteredHierarchy(Hierarchy h)
        {
            return new ParameteredHierarchy
            {
                Element = h,
                Parameter = this.GetParameterValue(this.Model.GetParameter(h.UniqueName)),
                ParameteredLevelInfos = h.LevelInfos.Select(this.ToParameteredLevel).ToList()
            };
        }

        private ParameteredLevel ToParameteredLevel(Level l)
        {
            return new ParameteredLevel
            {
                Element = l,
                Parameter = this.GetParameterValue(this.Model.GetParameter(l.UniqueName))
            };
        }

        private void RetrieveMetadata()
        {
            RetrieveMetadata(true);
            GenerateMdxQuery();
        }

        private void RetrieveMetadata(bool restoreSettings)
        {
            if (restoreSettings)
            {
                RestoreDefaults(false, false);
            }
            model.CubeName = this.Cube;
            RetriveCubeMetadata();
            ParameteredDimensions =
                new ObservableCollection<ParameteredDimension>(
                    RetrievedCube.Dimensions.Select(this.ToParameteredDimension));
            Measures =
                new ObservableCollection<IParameterizedMetadata>(
                    RetrievedCube.Measures.Select(m => new ParameteredMeasure {Element = m}));
        }

        private ParameterValue GetParameterValue(string paramName)
        {
            return ParametersCollection.FirstOrDefault(p => p.Name.Equals(paramName));
        }

        public void RetriveCubeMetadata()
        {
            try
            {
                if (IsLogged)
                {
                    ClearCollections();

                    RetrievedCube
                        = MdxClientUtil.GetCubeMetadata(model.Catalog, model.CubeName);

                    IsCubeRetrieved = true;
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
                return;
            }

        }

        public bool LoginGranted()
        {
            return ConnectionPropertiesViewModel.IsOk;
        }

        public void RetrieveCubes()
        {
            RetrieveCubes(true);
        }

        public void RetrieveCubes(bool restoreSettings)
        {
            try
            {
                if (restoreSettings)
                {
                    RestoreDefaults(false);
                }
                if (string.IsNullOrEmpty(this.Catalog))
                {
                    this.Catalog = "AllCubes";    
                }
                if (!string.IsNullOrEmpty(this.Catalog))
                {
                    model.Catalog = this.Catalog;
                    AvaliableCubes = MdxClientUtil.GetCubesList(model.Catalog);
                    if (AvaliableCubes.Length > 0)
                        IsLogged = true;

                    IsCubesRetrieved = true;

                    if (string.IsNullOrEmpty(Cube) && availableCubes.Length > 0)
                    {
                        Cube = availableCubes[0];
                    }
                }
            }
            catch (Exception exc)
            {
                Log.Exception(exc);
             //   IsLogged = false;
                availableCubes = new string[0];
             //   throw;
            }
        }

        public void RetrieveCatalogs()
        {
            try
            {
                
                AvaliableCatalogs = MdxClientUtil.GetCatalogsList();
            }
            catch (Exception ex)
            {
                string sCatalogException = ex.Message;
            }
            try
            {
                if (AvaliableCatalogs.Length > 0)
                    IsLogged = true;
                if (string.IsNullOrEmpty(Catalog))
                {
                    if (AvaliableCatalogs.Length > 0)
                    {
                        Catalog = AvaliableCatalogs[0];
                    }
                    else
                    {
                        Catalog = "AllCubes";
                    }
                }

            }
            catch (Exception exc)
            {
                Log.Exception(exc);
                IsLogged = false;
                AvaliableCatalogs = new string[0];
                throw;
            }
        }

        public void ClearCollections()
        {
            Columns.Clear();
            Rows.Clear();
            Slicers.Clear();
        }

        public void AddToColumns(object selectedObject)
        {
            this.RemoveFromAllAxis(selectedObject);
            if (selectedObject != null && !Columns.Contains(selectedObject))
                Columns.Add((IParameterizedMetadata) selectedObject);
            GenerateMdxQuery();
        }

        public void AddToRows(object selectedObject)
        {
            this.RemoveFromAllAxis(selectedObject);
            if (selectedObject != null && !Rows.Contains(selectedObject))
                Rows.Add((IParameterizedMetadata) selectedObject);
            GenerateMdxQuery();
        }

        public void AddToSlicer(object selectedObject)
        {
            this.RemoveFromAllAxis(selectedObject);
            if (selectedObject != null && !Slicers.Contains(selectedObject))
                Slicers.Add((IParameterizedMetadata) selectedObject);
            GenerateMdxQuery();
        }

        public void RemoveColumns(object selectedObj)
        {
            if (selectedObj != null)
            {
                Columns.Remove((IParameterizedMetadata) selectedObj);
            }
            else
            {
                Columns.Clear();
            }

            this.GenerateMdxQuery();
        }

        private void RemoveRows(object selectedObject)
        {
            if (selectedObject != null)
            {
                this.Rows.Remove((IParameterizedMetadata) selectedObject);
            }
            else
            {
                this.Rows.Clear();
            }

            this.GenerateMdxQuery();
        }

        public void RemoveSlicer(object selectedObject)
        {
            if (selectedObject != null)
            {
                this.Slicers.Remove((IParameterizedMetadata) selectedObject);
            }
            else
            {
                this.Slicers.Clear();
            }

            this.GenerateMdxQuery();
        }

        private void RemoveFromAllAxis(object selectedObject)
        {
            if (selectedObject != null)
            {
                this.RemoveColumns(selectedObject);
                this.RemoveRows(selectedObject);
                this.RemoveSlicer(selectedObject);
            }
        }

        private void RestoreDefaults(bool earseCatalogs = true,
            bool earseCubes = true)
        {
            if (earseCatalogs)
            {
                this.AvaliableCatalogs = new string[] {};
                this.Catalog = null;
                this.Model.Catalog = null;
                this.IsLogged = false;
            }
            if (earseCubes)
            {
                this.AvaliableCubes = new string[] {};
                this.Cube = null;
                this.Model.CubeName = null;
                this.IsCubesRetrieved = false;
            }
            this.Measures
                = new ObservableCollection<IParameterizedMetadata>();
            this.ParameteredDimensions
                = new ObservableCollection<ParameteredDimension>();
            this.Columns = new ObservableCollection<IParameterizedMetadata>();
            this.Rows = new ObservableCollection<IParameterizedMetadata>();
            this.Slicers = new ObservableCollection<IParameterizedMetadata>();
            this.GeneratedQuery = string.Empty;

            Model.RestoreDefaults();
        }
    }
}