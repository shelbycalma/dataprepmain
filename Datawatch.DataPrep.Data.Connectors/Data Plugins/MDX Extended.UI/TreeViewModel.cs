﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.MDXExtendedPlugin.UI
{
    using System.Collections.ObjectModel;
    

    public class TreeViewModel : ViewModelBase
    {
        private ObservableCollection<Dimension> dimensions;
        private ObservableCollection<Hierarchy> hierarchies;
        private ObservableCollection<Level> levels;
        private ObservableCollection<LevelProperty> levelProperties;

        public TreeViewModel()
        {

        }

        public ObservableCollection<Dimension> Dimensions
        {
            get
            {
                return dimensions ?? (dimensions = new ObservableCollection<Dimension>());
            }
            set
            {
                if (dimensions == value) return;
                dimensions = value;
                this.OnPropertyChanged("Dimensions");
            }
        }

        public ObservableCollection<Hierarchy> Hierarchies
        {
            get
            {
                return hierarchies ?? (hierarchies = new ObservableCollection<Hierarchy>());
            }
            set
            {
                if (hierarchies == value) return;
                hierarchies = value;
                this.OnPropertyChanged("Hierarchies");
            }
        }

        public ObservableCollection<Level> Levels
        {
            get
            {
                return levels ?? (levels = new ObservableCollection<Level>());
            }
            set
            {
                if (levels == value) return;
                levels = value;
                this.OnPropertyChanged("Levels");
            }
        }

        public ObservableCollection<LevelProperty> LevelProperties
        {
            get
            {
                return levelProperties ?? (LevelProperties = new ObservableCollection<LevelProperty>());
            }
            set
            {
                if (levelProperties == value) return;
                levelProperties = value;
                this.OnPropertyChanged("LevelProperties");
            }
        }
    }
}
