﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.MDXExtendedPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionSettingsGrid.xaml
    /// </summary>
    public partial class ConnectionSettingsGrid : UserControl
    {
        public ConnectionSettingsGrid()
        {
            InitializeComponent();
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                var pwBox = sender as PasswordBox;
                if (pwBox != null)
                {
                    var connectionProperty = GetConnectionPropertyFromPasswordBox(pwBox);
                    if (connectionProperty != null && connectionProperty.Value != pwBox.Password)
                        connectionProperty.Value = pwBox.Password;
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
                return;
            }
        }

        private ConnectionProperty GetConnectionPropertyFromPasswordBox(PasswordBox pwBox)
        {
            if (pwBox != null)
            {
                var uiParent = FindVisualParent<ContentPresenter>(pwBox) as UIElement;
                if (uiParent != null)
                {
                    var item = ConnectionSettingsItemsPanel.ItemContainerGenerator
                        .ItemFromContainer(uiParent) as ConnectionProperty;
                    if (item != null)
                        return item;
                }
            }
            return null;
        }

        private static T FindVisualParent<T>(DependencyObject control) where T : DependencyObject
        {
            while (control != null && control.GetType() != typeof (T))
            {
                control = VisualTreeHelper.GetParent(control);
            }
            return control as T;
        }

        private void FrameworkElement_OnLoaded(object sender, RoutedEventArgs e)
        {
            try
            {
                var pwBox = sender as PasswordBox;
                if (pwBox != null)
                {
                    var connectionProperty = GetConnectionPropertyFromPasswordBox(pwBox);
                    if (connectionProperty != null && connectionProperty.Value != pwBox.Password)
                        pwBox.Password = connectionProperty.Value;
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
                return;
            }
        }
    }

    public class SettingsGridTemplateSelector : DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var connectionProperty = item as ConnectionProperty;
            if (connectionProperty != null)
            {
                DataTemplate dataTemplate;
                switch (connectionProperty.FieldType)
                {
                    case FieldType.TextBox:
                        dataTemplate = GetStringDataTemplate(container);
                        break;
                    case FieldType.PasswordBox:
                        dataTemplate = GetPasswordDataTemplate(container);
                        break;
                    case FieldType.ComboBox:
                        dataTemplate = GetComboBoxDataTemplate(container);
                        break;
                    case FieldType.CheckBox:
                        dataTemplate = GetCheckboxDataTemplate(container);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                if (dataTemplate != null)
                    return dataTemplate;
            }

            return base.SelectTemplate(item, container);
        }

        private DataTemplate GetStringDataTemplate(DependencyObject container)
        {
            var containerElement = container as FrameworkElement;
            if (containerElement != null)
            {
                var stringDataTemplate =
                    containerElement.TryFindResource("StringTemplate") as DataTemplate;
                if (stringDataTemplate != null)
                {
                    return stringDataTemplate;
                }
            }
            return null;
        }

        private DataTemplate GetPasswordDataTemplate(DependencyObject container)
        {
            var containerElement = container as FrameworkElement;
            if (containerElement != null)
            {
                var passwordTemplate =
                    containerElement.TryFindResource("PasswordTemplate") as DataTemplate;
                if (passwordTemplate != null)
                {
                    return passwordTemplate;
                }
            }
            return null;
        }

        private DataTemplate GetCheckboxDataTemplate(DependencyObject container)
        {
            var containerElement = container as FrameworkElement;
            if (containerElement != null)
            {
                var checkboxTemplate =
                    containerElement.TryFindResource("CheckboxTemplate") as DataTemplate;
                if (checkboxTemplate != null)
                {
                    return checkboxTemplate;
                }
            }
            return null;
        }

        private DataTemplate GetComboBoxDataTemplate(DependencyObject container)
        {
            var containerElement = container as FrameworkElement;
            if (containerElement != null)
            {
                var comboBoxTemplate =
                    containerElement.TryFindResource("ComboBoxTemplate") as DataTemplate;
                if (comboBoxTemplate != null)
                {
                    return comboBoxTemplate;
                }
            }
            return null;
        }
    }
}
