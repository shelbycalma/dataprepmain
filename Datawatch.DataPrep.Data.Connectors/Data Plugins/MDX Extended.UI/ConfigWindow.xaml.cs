﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Panopticon.MDXExtendedPlugin.UI
{
    partial class ConfigWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindow));

        private readonly MDXSettingsViewModel settings;

        public ConfigWindow(MDXSettingsViewModel settings)
        {
            this.settings = settings;
            InitializeComponent();
        }

        public MDXSettingsViewModel Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            try
            {
                e.CanExecute = settings != null && settings.IsOK;
            }
            catch (Exception Ex)
            {
                e.CanExecute = false;
            }

        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }

   
}
