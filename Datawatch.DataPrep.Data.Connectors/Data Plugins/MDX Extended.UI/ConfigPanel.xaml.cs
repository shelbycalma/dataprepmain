﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.MDXExtendedPlugin.UI
{
    internal partial class ConfigPanel
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                                        typeof (MDXSettingsViewModel), typeof (ConfigPanel),
                                        new PropertyMetadata(Settings_Changed));
        
        public ConfigPanel()
        {
            InitializeComponent();
        }

        public MDXSettingsViewModel Settings
        {
            get { return (MDXSettingsViewModel) GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
                                             DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (MDXSettingsViewModel) args.OldValue,
                (MDXSettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(MDXSettingsViewModel oldSettings,
                                         MDXSettingsViewModel newSettings)
        {
            DataContext = newSettings;
        }

        private void ManualEditChecked(object sender, RoutedEventArgs e)
        {
            var checkboxValue = (bool) ((CheckBox) sender).IsChecked;
            MeasuresDimensionsExpander.IsExpanded = !checkboxValue;
            Settings.Model.Query
                = checkboxValue ? Settings.ManualQuery : Settings.GeneratedQuery;
        }

        private void ParameterChanged(object sender, RoutedEventArgs e)
        {
            Settings.GenerateMdxQuery();
        }
    }
}