﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.MDXExtendedPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, MDXSettings>
    {

        private const string imageUri = "pack://application:,,,/" +
                                        "Panopticon.MDXExtendedPlugin;component/mdx-plugin.gif";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            MDXSettings mdxSettings = new MDXSettings(new PropertyBag());
            MDXSettingsViewModel settingsView =
                new MDXSettingsViewModel(mdxSettings, parameters);
            ConfigWindow window =
                new ConfigWindow(settingsView);
            window.Owner = owner;
            
            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            mdxSettings.Title = mdxSettings.CubeName;
            settingsView.SaveParameters();
            PropertyBag properties = mdxSettings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            MDXSettings mdxSettings = Plugin.CreateSettings(bag);

            MDXSettingsViewModel mdxSettingsViewModel =
                new MDXSettingsViewModel(mdxSettings, parameters);
            ConfigPanel panel = new ConfigPanel();
            panel.Settings = mdxSettingsViewModel;
            mdxSettingsViewModel.LoadAllDataBySettings();
            return panel;
        }

        public override PropertyBag GetSetting(object element)
        {
            ConfigPanel panel = (ConfigPanel)element;
            panel.Settings.SaveParameters();
            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
