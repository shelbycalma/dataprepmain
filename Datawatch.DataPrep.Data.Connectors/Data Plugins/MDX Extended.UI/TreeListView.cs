﻿namespace Panopticon.MDXExtendedPlugin.UI
{
    using System.Windows;
    using System.Windows.Controls;

    public class TreeListView : TreeView
    {
        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }


        protected override void OnSelectedItemChanged(RoutedPropertyChangedEventArgs<object> e)
        {
            SelectedTreeViewItem = e.NewValue;
            base.OnSelectedItemChanged(e);
        }

        public object SelectedTreeViewItem
        {
            get { return GetValue(SelectedTreeViewItemProperty); }
            set { SetValue(SelectedTreeViewItemProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedTreeViewItem.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedTreeViewItemProperty =
            DependencyProperty.Register("SelectedTreeViewItem", typeof(object), typeof(TreeListView), new UIPropertyMetadata(null));


       
       

    }

    public class TreeListViewItem : TreeViewItem
    {
        public int Level
        {
            get
            {
                if (this._level == -1)
                {
                    TreeListViewItem parent = ItemsControl.ItemsControlFromItemContainer(this) as TreeListViewItem;
                    this._level = (parent != null) ? parent.Level + 1 : 0;
                }
                return this._level;
            }
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            return new TreeListViewItem();
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            return item is TreeListViewItem;
        }

        private int _level = -1;
    }
}
