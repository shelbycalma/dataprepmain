﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Panopticon.MDXExtendedPlugin.UI.Converters
{
    class ProviderNameConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is ProviderNames))
                return String.Empty;
            return MDXSettings.ProviderNameToString((ProviderNames) value);
        }

        public object ConvertBack(
            object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
                return String.Empty;
            return MDXSettings.StringToProviderName((string)value);
        }
    }
}
