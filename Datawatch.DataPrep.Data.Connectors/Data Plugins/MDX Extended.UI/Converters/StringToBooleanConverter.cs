﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Panopticon.MDXExtendedPlugin.UI.Converters
{
    class StringToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            var stringValue = value as string;
            if (stringValue == null)
                return false;
            bool result;
            if (bool.TryParse(stringValue, out result))
            {
                return result;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return value.Equals(true) ? "True" : "False";
        }
    }
    
}
