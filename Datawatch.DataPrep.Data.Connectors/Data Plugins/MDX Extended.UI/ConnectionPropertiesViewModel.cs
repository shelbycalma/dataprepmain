﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.MDXExtendedPlugin.UI
{
    public sealed class ConnectionPropertiesViewModel : ViewModelBase
    {
        public ICollection<ConnectionProperty> ConnectionProperties 
        {
            get
            {
                return settings.ConnectionProperties.Values;
            } 
        }

        public bool IsOk
        {
            get { return settings.ConnectionProperties.IsOk; }
        }

        private MDXSettings settings;

        public ConnectionPropertiesViewModel(MDXSettings mdxSettings)
        {
            settings = mdxSettings;
        }

        public void RefreshSettings()
        {
            settings.ConnectionProperties.LoadFromPropertyBag();
            OnPropertyChanged();
        }

        public void SaveToPropertyBag()
        {
            settings.ConnectionProperties.SaveToPropertyBag();
        }
    }
}