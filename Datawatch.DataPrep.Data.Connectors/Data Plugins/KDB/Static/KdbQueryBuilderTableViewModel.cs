﻿using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.ViewModel;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.KDBPlugin.Static
{
    public class KdbQueryBuilderTableViewModel: QueryBuilderTableViewModel<KdbColumn>
    {
        public KdbQueryBuilderTableViewModel(IQueryBuilderTableClient client,
            QuerySettings<KdbColumn> querySettings) :base(client,querySettings)
        {

        }
        public override DatabaseColumn CreateColumn(object v)
        {
            return new KdbColumn((KdbColumn)v);
        }

        public override bool EditableParameters
        {
            get { return true; }
        }
    }
}
