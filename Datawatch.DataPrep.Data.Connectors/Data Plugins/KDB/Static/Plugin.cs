﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.KDBPlugin.Static
{
    /// <summary>
    /// DataPlugin for KX/KDB.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<KdbSettings>, IOnDemandPlugin,
        ISchemaSavingPlugin
    {
        // The id of the data plug-in, this string must not be changed after
        // the plug-in has been used in workbooks or the workbooks created 
        // using the plug-in will loose their data connections.
        internal const string PluginId = "KDBPlugin";

        internal const bool PluginIsObsolete = false;
        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        internal const string PluginTitle = "KDB+";

        internal const string PluginType = DataPluginTypes.Database;

        private readonly ImageSource connectImage;
        private readonly KdbQueryBuilder kdbQueryBuilder;
        private bool disposed;
        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
        {

            kdbQueryBuilder = new KdbQueryBuilder();
            try
            {
                //Set unicode encoding as default
                c.e = Encoding.UTF8;
            }
            catch (Exception)
            {
            }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        /// <summary>
        /// Gets the plug-in id.
        /// </summary>
        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        /// <summary>
        /// Gets a value indicating the title of the plug-in.
        /// </summary>
        public override string Title
        {
            get { return PluginTitle; }
        }


        /// <summary>
        /// Gets a value indicating whether the plug-in has a valid license.
        /// </summary>
        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public virtual KdbSettings CreateConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            KdbSettings settings = new KdbSettings(bag, parameters);
            return settings;
        }

        public override KdbSettings CreateSettings(PropertyBag bag)
        {
            KdbSettings kdbSettings = CreateConnectionSettings(bag, null);
            if (string.IsNullOrEmpty(kdbSettings.Title))
            {
                kdbSettings.Title = Properties.Resources.UiKDBTitle;
            }
            return kdbSettings;
        }

        /// <summary>
        /// GetData returns the first n number of rows of the data set 
        /// specified by the settings and parameters.
        /// </summary>
        /// <param name="workbookDir">the current workbook directory.</param>
        /// <param name="dataDir">the current data directory.</param>
        /// <param name="propertyBagSettings">the settings <see cref="PropertyBag"/>.</param>
        /// <param name="parameters">a <see cref="Dictionary{T,T}"/> 
        ///   containing parameters.</param>
        /// <param name="rowcount">The number of rows to be returned.</param>
        /// <returns>An <see cref="ITable"/> instance.</returns>
        /// 
        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            KdbSettings settings = CreateConnectionSettings(bag, parameters);
            return GetDataInternal(workbookDir, dataDir, settings, parameters,
                rowcount, applyRowFilteration);
        }

        private ITable GetDataInternal(string workbookDir, string dataDir,
            KdbSettings settings,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            CheckLicense();
            
            if (settings.QuerySettings.QueryMode == QueryMode.Table &&
                settings.QuerySettings.SelectedTable == null)
            {
                throw Exceptions.IncorrectSettingValue("SelectedTable");
            }
            if (settings.QuerySettings != null)
            {
                settings.QuerySettings.ApplyRowFilteration =
                    applyRowFilteration;
            }
            return kdbQueryBuilder.GetData(settings, parameters, rowcount);
        }

        public ITable GetOnDemandData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            KdbSettings settings = CreateConnectionSettings(bag, parameters);
            return GetOnDemandDataInternal(workbookDir, dataDir,
                settings, parameters, onDemandParameters, rowcount);
        }

        private ITable GetOnDemandDataInternal(string workbookDir, string dataDir,
            KdbSettings settings, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            CheckLicense();
            SchemaAndTable selectedTable = settings.QuerySettings.SelectedTable;
            if (settings.QuerySettings.QueryMode == QueryMode.Table &&
                selectedTable == null)
            {
                throw Exceptions.IncorrectSettingValue("SelectedTable");
            }
            return kdbQueryBuilder.GetOnDemandData(settings,
                settings.QuerySettings, new Dictionary<string, string>(), 
                parameters, onDemandParameters, 
                selectedTable != null ? selectedTable.Table : null,
                rowcount, false);
        }

        public bool IsOnDemand(PropertyBag settings)
        {
            return KdbSettings.GetIsOnDemand(settings);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

        public void SaveSchema(string workbookDir,
            string dataDir, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            KdbSettings settings = CreateConnectionSettings(bag, parameters);
            settings.QuerySettings.IsSchemaRequest = true;

            if (settings.QuerySettings.IsOnDemand)
            {
                GetOnDemandDataInternal(workbookDir, dataDir, settings,
                    parameters, null, 0);
            }
            else
            {
                GetDataInternal(workbookDir, dataDir, settings, parameters,
                    0, false);
            }
        }
    }
}
