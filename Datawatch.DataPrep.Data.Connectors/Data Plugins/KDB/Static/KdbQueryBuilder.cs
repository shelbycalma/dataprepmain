﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Connectors.Query.Q;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.KDBPlugin.Static
{
    public class KdbQueryBuilder : KdbExecutorBase
    {

        private const string tableListQuery = "tables`";
        private const string viewsListQuery = "views`";
        public static string exDateTimeColumnName = "EXDateTime";

        private int nextRequestId;

        public KdbQueryBuilder()
        {
        }

        private static void AddSelectList(KdbQuerySettings querySettings,
            QQueryBuilder queryBuilder, IEnumerable<ParameterValue> parameters)
        {
            if (querySettings.SelectedColumns.Count == 0)
            {
                return;
            }
            bool isAggregationRequired = false;
            bool isAggregateTypePeriod = false;
            bool isZColumnSelected = false;
            bool isCompositeDateTimeColumnSelected = false;
            string periodText = "";

            // Variable initialization based on settings
            if (querySettings.AggregationRequired &&
                !querySettings.IsOnDemand)
            {
                isAggregationRequired = true;
                periodText = querySettings.PeriodText;
                periodText = KdbUtil.ApplyParameters(periodText, parameters);
                if (querySettings.IsPeriod &&
                    !string.IsNullOrEmpty(querySettings.PeriodText))
                {
                    isAggregateTypePeriod = true;
                }
            }
            if (!string.IsNullOrEmpty(querySettings.SelectedDatetimeColumn))
            {
                isZColumnSelected = true;
            }
            else if (!string.IsNullOrEmpty(querySettings.SelectedDateColumn) &&
                !string.IsNullOrEmpty(querySettings.SelectedTimeColumn))
            {
                isCompositeDateTimeColumnSelected = true;
            }


            foreach (DatabaseColumn column in querySettings.SelectedColumns)
            {
                if (querySettings.AggregationRequired &&
                    column.AggregateType.Equals(AggregateType.None))
                {
                    //TODO:-adding alias to groupby columns as it
                    //was with earlier versions this can be handled
                    //better with option to add alias with groupby columns
                    queryBuilder.GroupBy.AddColumn(
                        string.Format("by{0}: {0}", column.ColumnName));
                }
                else
                {
                    AddSelectedColumn(queryBuilder,
                        querySettings.AggregationRequired,
                        column.ColumnName, column.ColumnName, column.AggregateType);
                }
            }

            //add ExDateTime column based on z,d,t
            string dateColumnName = "";
            if (isZColumnSelected)
            {
                dateColumnName = querySettings.SelectedDatetimeColumn;
            }
            else if (isCompositeDateTimeColumnSelected)
            {
                //constructing EXDateTime: (DateText + TimeText) 
                dateColumnName = string.Format("({0} + {1})",
                    KdbUtil.GetDateColumn(querySettings.SelectedDateColumn),
                    querySettings.SelectedTimeColumn);
            }
            else if (!string.IsNullOrEmpty(querySettings.SelectedDateColumn))
            {
                //constructing EXDateTime: (DateText)
                dateColumnName = string.Format("({0})",
                    KdbUtil.GetDateColumn(querySettings.SelectedDateColumn));
            }

            if (!string.IsNullOrEmpty(dateColumnName))
            {
                AddSelectedColumn(queryBuilder, isAggregationRequired,
                   exDateTimeColumnName, dateColumnName, AggregateType.Last);
            }

            if (isAggregationRequired)
            {
                if (isAggregateTypePeriod && (isZColumnSelected ||
                    isCompositeDateTimeColumnSelected))
                {
                    string datetimeColumnName = querySettings.SelectedDatetimeColumn;
                    datetimeColumnName = KdbUtil.AppendPeriodAggregation(
                        querySettings.AggregationPeriodType, periodText,
                        datetimeColumnName, "bar", isCompositeDateTimeColumnSelected,
                        querySettings.SelectedDateColumn,
                        querySettings.SelectedTimeColumn);
                    queryBuilder.GroupBy.AddColumn(datetimeColumnName);
                }
            }
        }

        private static void AddWhereClause(KdbQuerySettings kdbQuerySettings,
            IEnumerable<ParameterValue> parameters, StringBuilder sbQuery)
        {
            bool isConstrainByDateFromRequired = false;
            bool isConstrainByDateToRequired = false;
            bool isConstrainByPredicates = false;
            string predicates = null;

            //either z column is there or combination of d+t is there
            //only then  isConstrainByDateFrom/TO should be true.

            if (!string.IsNullOrEmpty(kdbQuerySettings.SelectedDatetimeColumn) ||
                !string.IsNullOrEmpty(kdbQuerySettings.SelectedDateColumn) ||
                !string.IsNullOrEmpty(kdbQuerySettings.SelectedTimeColumn))
            {
                if (kdbQuerySettings.IsConstrainByDateTime &&
                    !(string.IsNullOrEmpty(kdbQuerySettings.FromDate)))
                {
                    isConstrainByDateFromRequired = true;
                }
                if (kdbQuerySettings.IsConstrainByDateTime &&
                    !(string.IsNullOrEmpty(kdbQuerySettings.ToDate)))
                {
                    isConstrainByDateToRequired = true;
                }
            }

            if (kdbQuerySettings.AutoParameterize)
            {
                Predicate predicate = KdbUtil.GetParameterizedPredicate(
                    kdbQuerySettings, parameters, null);
                predicates = KdbUtil.GetPridicateString(predicate);
                if (!string.IsNullOrEmpty(predicates))
                {
                    isConstrainByPredicates = true;
                }
            }

            //add where clause if any of the constrain is valid
            if (isConstrainByDateFromRequired ||
                isConstrainByDateToRequired || isConstrainByPredicates)
            {
                sbQuery.AppendLine();
                sbQuery.AppendLine(" where ");
            }

            string dateConstrainColumnName;
            //, [z column] > [Entered From DateTime]
            if (isConstrainByDateFromRequired)
            {
                string fromDate =
                    KdbUtil.GetDateConstraint(kdbQuerySettings.TimeZoneHelper,
                    parameters, kdbQuerySettings.FromDate,
                    kdbQuerySettings.SelectedDateColumn,
                    kdbQuerySettings.SelectedTimeColumn);
                dateConstrainColumnName = GetConstrainByDateColumnName(
                    kdbQuerySettings.SelectedDatetimeColumn,
                    kdbQuerySettings.SelectedDateColumn,
                    kdbQuerySettings.SelectedTimeColumn);

                if (isConstrainByDateToRequired)
                {
                    string toDate =
                        KdbUtil.GetDateConstraint(kdbQuerySettings.TimeZoneHelper,
                        parameters, kdbQuerySettings.ToDate,
                        kdbQuerySettings.SelectedTimeColumn);
                    if (!string.IsNullOrEmpty(kdbQuerySettings.SelectedDateColumn) &&
                        !string.IsNullOrEmpty(kdbQuerySettings.SelectedTimeColumn))
                    {
                        sbQuery.AppendFormat("{0} within(\"d\"${1};\"d\"${2}), ",
                            kdbQuerySettings.SelectedDateColumn, fromDate, toDate);
                    }
                    sbQuery.AppendFormat("{0} within({1};{2})",
                        dateConstrainColumnName, fromDate, toDate);
                }
                else
                {
                    if (!string.IsNullOrEmpty(kdbQuerySettings.SelectedDateColumn) &&
                        !string.IsNullOrEmpty(kdbQuerySettings.SelectedTimeColumn))
                    {
                        sbQuery.AppendFormat("{0} >= \"d\"${1}, ",
                            kdbQuerySettings.SelectedDateColumn, fromDate);
                    }
                    sbQuery.AppendFormat("{0} >= {1}", dateConstrainColumnName,
                        fromDate);
                }
            }
            //], [z column] < [Entered To DateTime]
            if (isConstrainByDateToRequired && !isConstrainByDateFromRequired)
            {
                string toDate =
                    KdbUtil.GetDateConstraint(kdbQuerySettings.TimeZoneHelper,
                    parameters, kdbQuerySettings.ToDate,
                    kdbQuerySettings.SelectedTimeColumn);

                if (!string.IsNullOrEmpty(kdbQuerySettings.SelectedDateColumn) &&
                    !string.IsNullOrEmpty(kdbQuerySettings.SelectedTimeColumn))
                {
                    sbQuery.AppendFormat("{0} <= \"d\"${1}, ",
                        kdbQuerySettings.SelectedDateColumn, toDate);
                }
                sbQuery.Append(GetConstrainByDateColumnName(
                    kdbQuerySettings.SelectedDatetimeColumn,
                    kdbQuerySettings.SelectedDateColumn,
                    kdbQuerySettings.SelectedTimeColumn));

                sbQuery.AppendFormat(" <= {0}", toDate);
            }

            if (isConstrainByPredicates)
            {
                if (isConstrainByDateToRequired || isConstrainByDateFromRequired)
                {
                    sbQuery.Append(", ");
                }
                sbQuery.Append(predicates);
            }
        }

        private static string GetConstrainByDateColumnName(
            string dateTimeZColumn, string dateDColumn,
            string timeTColumn)
        {
            StringBuilder sbQuery = new StringBuilder();
            if (!string.IsNullOrEmpty(dateTimeZColumn))
            {
                sbQuery.Append(dateTimeZColumn);
            }
            else if (!string.IsNullOrEmpty(dateDColumn) &&
                !string.IsNullOrEmpty(timeTColumn))
            {
                sbQuery.Append("(");
                sbQuery.Append(KdbUtil.GetDateColumn(dateDColumn));
                sbQuery.Append(" + ");
                sbQuery.Append(timeTColumn);
                sbQuery.Append(")");
            }
            else if (!string.IsNullOrEmpty(dateDColumn))
            {
                sbQuery.Append(dateDColumn);
            }
            else if (!string.IsNullOrEmpty(timeTColumn))
            {
                sbQuery.Append(timeTColumn);
            }
            return sbQuery.ToString();
        }

        private static void AddSelectedColumn(QQueryBuilder queryBuilder,
            bool isAggregationRequired, string dateTimeColumnAlias,
            string columnName, AggregateType aggregateType)
        {
            if (isAggregationRequired)
            {
                //[agg method]_EXDateTime: [agg method] [z column]
                queryBuilder.Select.AddAggregate(new[] { columnName },
                    DataUtils.ToFunctionType(aggregateType),
                    aggregateType.ToString().ToLower() + "_" +
                    dateTimeColumnAlias);
            }
            else
            {
                //[ExDateTime: DateTimeText]
                queryBuilder.Select.AddColumn(columnName, dateTimeColumnAlias);
            }
        }

        public static List<KdbColumn> GetColumns(c cInstance, KdbSettings settings)
        {
            if (cInstance == null)
            {
                return null;
            }
            string query = GetSchemaObjectQuery(SchemaObjectType.Columns,
                settings.QuerySettings.SelectedTable.Table);
            IQueryResult result = KdbUtil.DoQuery(query, cInstance);

            ParameterValueCollection lookup = new ParameterValueCollection();
            lookup.AddRange(settings.Parameters);

            List<KdbColumn> columns = new List<KdbColumn>();
            MetaQueryResult metaResult = result as MetaQueryResult;

            if (metaResult == null)
            {
                return columns;
            }
            for (int i = 0; i < metaResult.ColumnCount; i++)
            {
                //use 1st column i.e. 0 for object names
                KdbColumn column = new KdbColumn();
                column.ColumnName = result.Names[i];
                column.KdbType = Convert.ToChar(metaResult.GetKdbType(i));
                column.ColumnType = TableBuilder.ToColumnType(
                    metaResult.GetFieldType(i));

                bool loadDefaultParameterName = true;
                if (settings.QuerySettings.PredicateColumns != null &&
                    settings.QuerySettings.PredicateColumns.Count > 0)
                {
                    foreach (KdbColumn predicateCol in
                        settings.QuerySettings.PredicateColumns)
                    {
                        string appliedParameter =
                                    predicateCol.AppliedParameterName;
                        if (predicateCol.ColumnName.Equals(column.ColumnName) &&
                            (lookup.ContainsKey(appliedParameter) ||
                                    (appliedParameter != null)))
                        {
                            column.AppliedParameterName = appliedParameter;
                            loadDefaultParameterName = false;
                            break;
                        }
                    }
                }
                if (loadDefaultParameterName &&
                        lookup.ContainsKey(column.ColumnName))
                {
                    column.AppliedParameterName = lookup[column.ColumnName].Name;
                }
                columns.Add(column);
            }

            settings.QuerySettings.PredicateColumns.Clear();
            foreach (KdbColumn availableColumn in columns)
            {
                if (availableColumn.AppliedParameterName != null)
                {
                    settings.QuerySettings.PredicateColumns.Add(availableColumn);
                }
            }

            return columns.OrderBy(col => col.ColumnName).ToList();
        }

        public static string GetGeneratedQuery(
            KdbQuerySettings kdbQuerySettings,
            IEnumerable<ParameterValue> parameters,
            string selectedTableName, int rowCount = -1)
        {
            if (kdbQuerySettings == null)
            {
                throw Exceptions.ArgumentNull("querySettings");
            }
            QQueryBuilder queryBuilder = new QQueryBuilder();
            queryBuilder.IsSelectAllQuery = true;
            queryBuilder.Limit = rowCount;
            if (!string.IsNullOrEmpty(selectedTableName))
            {
                queryBuilder.SchemaAndTable =
                    new SchemaAndTable("", selectedTableName);
            }

            // Columns List
            AddSelectList(kdbQuerySettings, queryBuilder, parameters);

            //TODO:- use query builder for predicates as well
            // Need to add between/within to operators
            StringBuilder sbQuery = new StringBuilder();
            if (kdbQuerySettings.ApplyRowFilteration)
            {
                AddWhereClause(kdbQuerySettings, parameters, sbQuery);
            }
            return queryBuilder.ToString(SqlDialectFactory.KxQ) +
                sbQuery.ToString();
        }

        private static List<SchemaAndTable> GetSchemaObjectList(
            IQueryResult result)
        {
            List<SchemaAndTable> objects = new List<SchemaAndTable>();
            for (int i = 0; i < result.RowCount; i++)
            {
                //use 1st column i.e. 0 for object names
                objects.Add(new SchemaAndTable(null,
                    result.GetValue(0, i).ToString()));
            }
            return objects;
        }

        private static string GetSchemaObjectQuery(SchemaObjectType schemaType,
            string selectedTable)
        {
            string query = "";
            switch (schemaType)
            {
                case SchemaObjectType.Table:
                    query = tableListQuery;
                    break;
                case SchemaObjectType.Columns:
                    query = string.Format(schemaQuery, selectedTable);
                    break;
                case SchemaObjectType.Views:
                    query = viewsListQuery;
                    break;
            }
            return query;
        }

        public static SchemaAndTable[] GetTables(c cInstance)
        {
            if (cInstance == null)
            {
                return null;
            }
            List<SchemaAndTable> tables = new List<SchemaAndTable>();

            string query = GetSchemaObjectQuery(SchemaObjectType.Table, "");
            try
            {
                IQueryResult result = KdbUtil.DoQuery(query, cInstance);
                tables = GetSchemaObjectList(result);
            }
            catch (Exception ex)
            {
                Log.Error(Properties.Resources.LogErrorExecutingQuery, query,
                    ex.Message);
            }
            query = GetSchemaObjectQuery(SchemaObjectType.Views, "");
            try
            {
                IQueryResult result = KdbUtil.DoQuery(query, cInstance);
                tables.AddRange(GetSchemaObjectList(result));
            }
            catch (Exception ex)
            {
                Log.Error(Properties.Resources.LogErrorExecutingQuery, query,
                    ex.Message);
            }
            tables.Sort();
            return tables.ToArray();
        }

        public ITable GetData(KdbSettings settings,
            IEnumerable<ParameterValue> parameters, int rowcount)
        {
            KdbQuerySettings kdbQuerySettings = settings.QuerySettings;
            // Get an ID to identify this particular request in log.
            int requestId = Interlocked.Increment(ref nextRequestId);
            string strQuery = kdbQuerySettings.Query;

            //if query type if auto generated then regenerate the query
            //so that value for special parameter can be converted to UTC
            if (kdbQuerySettings.QueryMode == QueryMode.Table)
            {
                strQuery = KdbQueryBuilder.GetGeneratedQuery(kdbQuerySettings,
                    parameters, settings.QuerySettings.SelectedTable.Table,
                    rowcount);
            }
            else
            {
                //apply special parameters so that $TimeWindowStart etc. can get timezone shift.
                strQuery = KdbUtil.ApplySpecialParameters(strQuery, parameters,
                    kdbQuerySettings.TimeZoneHelper);
            }
            if (string.IsNullOrEmpty(strQuery))
            {
                throw new Exception(Properties.Resources.ExEmptyQuery);
            }
            ValidateDeferredSyncQuery(kdbQuerySettings);



            c cInstance = settings.GetConnection(parameters);
            strQuery = CheckPassToFunctionQuery(strQuery,
                kdbQuerySettings.PassToFunction, kdbQuerySettings.FunctionName);

            strQuery = ApplyDeferredSyncQuery(kdbQuerySettings, strQuery);

            IQueryResult query = null;

            try
            {
                query = KdbUtil.DoQuery(
                    KdbUtil.ApplyParameters(strQuery, parameters,
                        kdbQuerySettings.QueryMode), cInstance,
                        requestId, kdbQuerySettings.IsDeferredSyncQuery);
            }
            finally
            {
                KdbUtil.CloseConnection(cInstance);
            }

            ParameterTable table = TableBuilder.CreateTable(query, rowcount,
                kdbQuerySettings, parameters);

            // We're creating a new ParameterTable (: StandaloneTable) on each
            // request, so allow EX to freeze it.

            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        public override void AddPredicateColumnsToQuery(
            KdbQuerySettings kdbQuerySettings, QQueryBuilder queryBuilder,
            IEnumerable<ParameterValue> parameters,
            Dictionary<string, string> columnMap)
        {
            Predicate parameterPredicate = KdbUtil.GetParameterizedPredicate(
                kdbQuerySettings, parameters, columnMap);

            bool isConstrainByDateFromRequired = false;
            bool isConstrainByDateToRequired = false;

            if (!string.IsNullOrEmpty(kdbQuerySettings.SelectedDatetimeColumn) ||
                !string.IsNullOrEmpty(kdbQuerySettings.SelectedDateColumn) ||
                !string.IsNullOrEmpty(kdbQuerySettings.SelectedTimeColumn))
            {
                if (kdbQuerySettings.IsConstrainByDateTime &&
                    !(string.IsNullOrEmpty(kdbQuerySettings.FromDate)))
                {
                    isConstrainByDateFromRequired = true;
                }
                if (kdbQuerySettings.IsConstrainByDateTime &&
                    !(string.IsNullOrEmpty(kdbQuerySettings.ToDate)))
                {
                    isConstrainByDateToRequired = true;
                }
            }

            if (isConstrainByDateFromRequired)
            {
                string fromDate = KdbUtil.GetDateConstraint(
                                    kdbQuerySettings.TimeZoneHelper,
                                    parameters, kdbQuerySettings.FromDate,
                                    kdbQuerySettings.SelectedDateColumn,
                                    kdbQuerySettings.SelectedTimeColumn);
                string columnName = GetConstrainByDateColumnName(
                    kdbQuerySettings.SelectedDatetimeColumn,
                    kdbQuerySettings.SelectedDateColumn,
                    kdbQuerySettings.SelectedTimeColumn);
                parameterPredicate = new AndOperator(parameterPredicate,
                    new Comparison(Operator.GreaterThanOrEqualsTo,
                        new ColumnParameter(columnName),
                        new ValueParameter(fromDate)));
            }
            if (isConstrainByDateToRequired)
            {
                string toDate = KdbUtil.GetDateConstraint(
                                   kdbQuerySettings.TimeZoneHelper,
                                   parameters, kdbQuerySettings.ToDate,
                                   kdbQuerySettings.SelectedDateColumn,
                                   kdbQuerySettings.SelectedTimeColumn);
                string columnName = GetConstrainByDateColumnName(
                    kdbQuerySettings.SelectedDatetimeColumn,
                    kdbQuerySettings.SelectedDateColumn,
                    kdbQuerySettings.SelectedTimeColumn);
                parameterPredicate = new AndOperator(parameterPredicate,
                    new Comparison(Operator.LessThanOrEqualsTo,
                        new ColumnParameter(columnName),
                        new ValueParameter(toDate)));
            }

            queryBuilder.Where.Predicate = parameterPredicate;

        }
    }
}
