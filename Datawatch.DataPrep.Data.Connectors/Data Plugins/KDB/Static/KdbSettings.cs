﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.KDBPlugin.Static
{
    public class KdbSettings : ConnectionSettings,
        IKdbConnectionSettings, IQueryBuilderTableClient
    {
        private TimeZoneHelper timeZoneHelper;
        private readonly KdbQuerySettings querySettings;
        private readonly KdbQueryBuilderTableViewModel queryBuilderViewModel;
        private IEnumerable<ParameterValue> parameters;
        private IEnumerable<KdbColumn> columns;
        private const string defaultPort = "5001";

        private SchemaAndTable[] tables;
        private c cInstance;

        private ICollectionView tablesCollectionView;
        private ICollectionView columnsCollectionView;

        public KdbSettings()
            : this(new PropertyBag(), null)
        {

        }

        public KdbSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag)
        {
            if (bag.IsEmpty())
            {
                Version = new Version("2.0.0.0");
            }
            this.parameters = parameters;
            timeZoneHelper = KdbUtil.GetTimeZoneHelper(bag);

            querySettings = new KdbQuerySettings(bag);
            querySettings.PropertyChanged += querySettings_PropertyChanged;

            if (Version.Major < 2)
            {
                HandlePropertyRenames();
                PopulateOldSelectedColumns();
            }

            //removing constrain by symbol and add predicate instead
            string s = GetInternal("IsConstrainBySymbol");
            bool isConstrainBySymbol = !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            if (isConstrainBySymbol && QuerySettings.QueryMode == QueryMode.Table)
            {
                c cInstance = null;
                try
                {
                    cInstance = GetConnection(parameters);
                    columns = KdbQueryBuilder.GetColumns(cInstance, this);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }
                foreach (KdbColumn col in columns)
                {
                    if (col.ColumnName.Equals(GetInternal("SelectedSymbolColumnConstrain")))
                    {
                        col.AppliedParameterName = GetInternal("SymbolConstrainText");
                        if (!querySettings.AutoParameterize)
                        {
                            querySettings.AutoParameterize = true;
                        }
                        if (!querySettings.PredicateColumns.Contains(col))
                        {
                            querySettings.PredicateColumns.Add(col);
                        }
                    }
                }

            }

            queryBuilderViewModel =
                new KdbQueryBuilderTableViewModel(this, querySettings);
            queryBuilderViewModel.DoUpdateQuery +=
                QueryBuilderViewModel_DoUpdateQuery;
        }

        void querySettings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SelectedTable"))
            {
                columns = null;
            }
        }

        private void QueryBuilderViewModel_DoUpdateQuery(
            object sender, EventArgs e)
        {
            UpdateQuery();
        }

        public IEnumerable<DatabaseColumn> Columns
        {
            get
            {
                if (QuerySettings.SelectedTable == null)
                {
                    return null;
                }

                if (columns != null)
                {
                    return columns;
                }
                c cInstance = null;
                try
                {
                    cInstance = GetConnection(parameters);
                    columns = KdbQueryBuilder.GetColumns(cInstance, this);
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                    columns = new List<KdbColumn>();
                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }

                return columns;
            }
        }

        public string Host
        {
            get
            {
                string host = KdbUtil.ApplyParameters(
                    GetInternal("Host", "localhost"), parameters);
                HostUriParser uriParser = new HostUriParser(-1, host);
                return uriParser.Host;
            }
            set
            {
                SetInternal("Host", value);
            }
        }

        /// <summary>
        /// This property is only intended for UI binding.
        /// Use IKdbConnectionSettings.Host for connection, which supports old semantics i.e. host:port.
        /// </summary>
        public string HostConfigurator
        {
            get
            {
                return GetInternal("Host", "localhost");
            }
            set { Host = value; }
        }

        public string Port
        {
            get
            {
                return GetPort(true);
            }
            set { SetInternal("Port", value); }
        }

        /// <summary>
        /// This property is only intended for UI binding.
        /// Use IKdbConnectionSettings.Port for connection, which first looks up for port inside Host.
        /// </summary>
        public string PortConfigurator
        {
            get
            {
                return GetPort(false);
            }
            set { Port = value; }
        }

        public string UserName
        {
            get
            {
                string userName = KdbUtil.ApplyParameters(
                    GetInternal("UserName"), parameters);

                if (!string.IsNullOrEmpty(userName))
                {
                    string[] userCredentials = userName.Split(':');

                    if (userCredentials.Length == 2)
                    {
                        return userCredentials[0];
                    }
                }
                return userName;
            }
            set { SetInternal("UserName", value); }
        }

        public string UserNameConfigurator
        {
            get
            {
                return GetInternal("UserName");
            }
            set { UserName = value; }
        }

        public string Password
        {
            get
            {
                string userName = KdbUtil.ApplyParameters(
                    GetInternal("UserName"), parameters);
                if (!string.IsNullOrEmpty(userName))
                {
                    string[] userCredentials = userName.Split(':');

                    if (userCredentials.Length == 2 &&
                        !string.IsNullOrEmpty(userCredentials[1]))
                    {
                        return userCredentials[1];
                    }
                }
                return GetInternal("Password");
            }
            set { SetInternal("Password", value); }
        }

        public string PasswordConfigurator
        {
            get
            {
                return GetInternal("Password");
            }
            set { Password = value; }
        }

        public int Timeout
        {
            get
            {
                return GetInternalInt("Timeout", 30);
            }
            set
            {
                SetInternalInt("Timeout", value);
            }
        }

        public bool IsPasswordEncrypted
        {
            get { return false; }
        }



        public string TablesFilter
        {
            get
            {
                return GetInternal("TablesFilter");
            }
            set
            {
                SetInternal("TablesFilter", value);
            }
        }

        public string ColumnsFilter
        {
            get
            {
                return GetInternal("ColumnsFilter");
            }
            set
            {
                SetInternal("ColumnsFilter", value);
            }
        }

        public Version Version
        {
            get
            {
                return new Version(GetInternal("Version", "1.0.0.0"));
            }
            private set
            {
                SetInternal("Version", value.ToString());
            }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        internal string[] GetOldSelectedColumns()
        {
            int count = GetInternalInt("SelectedColumnsCount", 0);
            string[] Columns = new string[count];
            for (int i = 0; i < count; i++)
            {
                Columns[i] = GetInternal("SelectedColumnName" + i);

            }
            return Columns;
        }

        private void HandlePropertyRenames()
        {
            // Handle backward compatibility for property renames.
            try
            {
                string s = GetInternal("UIType");
                if (!string.IsNullOrEmpty(s))
                {
                    // Since UIType had identical values, can directly cast to QueryMode.
                    querySettings.QueryMode = (QueryMode)Enum.Parse(
                        typeof(QueryMode), s);
                }
                //UIType can be null in workbooks created till 5.8.2
                else if (!string.IsNullOrEmpty(querySettings.Query))
                {
                    querySettings.QueryMode = QueryMode.Query;
                }
            }
            catch
            {
            }

            string datetimeZColumn = GetInternal("DatetimeZColumn");
            if (!string.IsNullOrEmpty(datetimeZColumn))
            {
                querySettings.SelectedDatetimeColumn = datetimeZColumn;
            }

            string dateDColumn = GetInternal("DateDColumn");
            if (!string.IsNullOrEmpty(dateDColumn))
            {
                querySettings.SelectedDateColumn = dateDColumn;
            }

            string timeTColumn = GetInternal("TimeTColumn");
            if (!string.IsNullOrEmpty(timeTColumn))
            {
                querySettings.SelectedTimeColumn = timeTColumn;
            }

            string symbolSColumnConstrain = GetInternal("SymbolSColumnConstrain");
            if (!string.IsNullOrEmpty(symbolSColumnConstrain))
            {
                SetInternal("SelectedSymbolColumnConstrain", symbolSColumnConstrain);
            }

            string symbolSColumnAggregate = GetInternal("SymbolSColumnAggregate");
            if (!string.IsNullOrEmpty(symbolSColumnAggregate))
            {
                querySettings.SelectedSymbolColumnAggregate =
                    symbolSColumnAggregate;
            }

            string isConstrainBySym = GetInternal("IsConstrainBySym");
            if (!string.IsNullOrEmpty(isConstrainBySym))
            {
                SetInternal("IsConstrainBySymbol", isConstrainBySym);
            }

            string symbolConstranText = GetInternal("SymbolConstranText");
            if (!string.IsNullOrEmpty(symbolConstranText))
            {
                SetInternal("SymbolConstrainText", symbolConstranText);
            }

            string constranByDateFrom = GetInternal("ConstranByDateFrom");
            if (!string.IsNullOrEmpty(constranByDateFrom))
            {
                querySettings.FromDate = constranByDateFrom;
            }

            string constranByDateTo = GetInternal("ConstranByDateTo");
            if (!string.IsNullOrEmpty(constranByDateTo))
            {
                querySettings.ToDate = constranByDateTo;
            }

            string onDemand = GetInternal("OnDemand");
            if (!string.IsNullOrEmpty(onDemand))
            {
                querySettings.IsOnDemand = bool.Parse(onDemand);
            }

            string isAggregate = GetInternal("IsAggregate");
            if (!string.IsNullOrEmpty(isAggregate))
            {
                querySettings.AggregationRequired =
                   bool.Parse(isAggregate) &&
                   !querySettings.IsOnDemand;
            }

            string UserId = GetInternal("UserId");
            if (!string.IsNullOrEmpty(UserId))
            {
                UserName = UserId;
            }
        }

        protected void PopulateOldSelectedColumns()
        {
            if (querySettings.QueryMode == QueryMode.Query)
            {
                return;
            }
            //backward compatibility to remove series
            if (querySettings.IsSeries &&
                !string.IsNullOrEmpty(querySettings.SelectedSymbolColumnAggregate))
            {
                KdbColumn column = new KdbColumn();
                column.ColumnName = querySettings.SelectedSymbolColumnAggregate;
                column.AggregateType = AggregateType.None;
                //only text columns were available in series columns shown
                column.ColumnType = ColumnType.Text;
                querySettings.SelectedColumns.Add(column);
            }

            if (SelectedColumnsCount == 0)
            {
                return;
            }
            List<KdbColumn> availableColumns = null;
            c cInstance = null;
            try
            {
                cInstance = GetConnection(parameters);
                availableColumns = KdbQueryBuilder.GetColumns(cInstance, this);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            finally
            {
                KdbUtil.CloseConnection(cInstance);
            }

            if (availableColumns == null || availableColumns.Count == 0) return;

            string[] selectedColumnNames = GetOldSelectedColumns();
            foreach (string columnName in selectedColumnNames)
            {
                KdbColumn column = new KdbColumn();
                column.ColumnName = columnName;
                //fallback to last
                column.AggregateType = AggregateType.Last;

                //try retrieving from old AggregationMethodType
                string s = GetInternal(columnName + "AggregationPeriodType");
                if (s != null)
                {
                    try
                    {
                        column.AggregateType = (AggregateType)Enum.Parse(
                            typeof(AggregateType), s, true);
                    }
                    catch
                    {
                    }
                }
                column.ColumnType = GetColumnType(availableColumns, columnName);

                querySettings.SelectedColumns.Add(column);
            }
        }

        public int SelectedColumnsCount
        {
            get
            {
                return GetInternalInt("SelectedColumnsCount", 0);
            }
        }

        public KdbQuerySettings QuerySettings
        {
            get { return querySettings; }
        }

        public KdbQueryBuilderTableViewModel QueryBuilderViewModel
        {
            get { return queryBuilderViewModel; }
        }

        public override PropertyBag ToPropertyBag()
        {
            if (Version.Major < 2)
            {
                SetInternal("UIType", null);
                SetInternal("DatetimeZColumn", null);
                SetInternal("DateDColumn", null);
                SetInternal("TimeTColumn", null);
                SetInternal("SymbolSColumnConstrain", null);
                SetInternal("SymbolSColumnAggregate", null);
                SetInternal("IsConstrainBySym", null);
                SetInternal("SymbolConstranText", null);
                SetInternal("ConstranByDateFrom", null);
                SetInternal("ConstranByDateTo", null);
                SetInternal("OnDemand", null);
                SetInternal("IsAggregate", null);
                SetInternal("UserId", null);

                // clear old serialized selected columns
                int count = GetInternalInt("SelectedColumnsCount", 0);
                string[] selectedColumnNames = GetOldSelectedColumns();
                for (int i = 0; i < count; i++)
                {
                    SetInternal("SelectedColumnName" + i, null);
                    SetInternal(selectedColumnNames[i] + "AggregationPeriodType",
                        null);
                }

                SetInternal("SelectedColumnsCount", null);
                // Old workbook settings have already being handled,
                // now we can upgrade the Version.
                Version = new Version("2.0.0.0");
            }

            //removing constraint by symbol
            SetInternal("SelectedSymbolColumnConstrain", null);
            SetInternal("IsConstrainBySymbol", null);
            SetInternal("SymbolConstrainText", null);

            PropertyBag bag = base.ToPropertyBag();

            bag.Merge(querySettings.ToPropertyBag());


            return bag;
        }

        private ColumnType GetColumnType(List<KdbColumn> kdbColumns,
            string columnName)
        {
            foreach (KdbColumn column in kdbColumns)
            {
                if (column.ColumnName == columnName)
                {
                    return column.ColumnType;
                }
            }

            return ColumnType.Text;
        }

        public static bool GetIsOnDemand(PropertyBag bag)
        {
            bool isOnDemand = false;
            string value = bag.Values["OnDemand"];
            if (string.IsNullOrEmpty(value))
            {
                value = bag.Values["IsOnDemand"];
            }

            if (!string.IsNullOrEmpty(value))
            {
                isOnDemand = bool.Parse(value);
            }

            return isOnDemand;
        }

        private string GetPort(bool useHost)
        {
            // Try to get port from Host field to continue support
            // old semantics like host:port.
            string host = KdbUtil.ApplyParameters(
                GetInternal("Host", "localhost"), parameters);
            HostUriParser uriParser = new HostUriParser(-1, host);
            int port = uriParser.Port;
            if (port == -1)
            {
                return GetInternal("Port", defaultPort);
            }
            if (useHost)
            {
                return port.ToString();
            }
            return GetInternal("Port");
        }

        public void UpdateQuery()
        {
            //regenerate the query as settings changed
            if (QuerySettings.QueryMode == QueryMode.Table)
            {
                if (QuerySettings.SelectedTable == null)
                {
                    return;
                }
                QuerySettings.Query =
                    KdbQueryBuilder.GetGeneratedQuery(QuerySettings,
                    Parameters, QuerySettings.SelectedTable.Table);
                QuerySettings.IsQueryMode = false;
            }
            else
            {
                QuerySettings.IsQueryMode = true;
            }
        }

        public virtual c GetConnection(IEnumerable<ParameterValue> parameters)
        {
            return KdbUtil.GetConnection(this, parameters);
        }

        public bool CanLoadTables
        {
            get
            {
                if (!string.IsNullOrEmpty(Host) &&
                    !string.IsNullOrEmpty(Port))
                {
                    return true;
                }
                return false;
            }
        }

        public SchemaAndTable[] Tables
        {
            get
            {
                c cInstance = null;
                try
                {
                    cInstance = GetConnection(parameters);
                    tables = KdbQueryBuilder.GetTables(cInstance);
                    return tables;

                }
                finally
                {
                    KdbUtil.CloseConnection(cInstance);
                }
            }
        }
    }
}
