﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using RT = Panopticon.KDBPlugin.Realtime.Plugin;
using SP = Panopticon.KDBPlugin.Static.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.KDBPlugin.dll")]
[assembly: AssemblyDescription("Datawatch KDB Plug-in")]

[assembly: PluginDescription(SP.PluginTitle, SP.PluginType, SP.PluginId, SP.PluginIsObsolete)]
[assembly: PluginDescription(RT.PluginTitle, RT.PluginType, RT.PluginId, RT.PluginIsObsolete)]
