﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.KDBPlugin.Properties;

namespace Panopticon.KDBPlugin.Realtime
{
    /// <summary>
    /// DataPlugin for realtime KX/KDB.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin<ParameterTable, RealtimeKdbSettings,
        KdbTuple, KdbDataAdapter>, ISchemaSavingPlugin
    {
        // The id of the data plug-in, this string must not be changed after
        // the plug-in has been used in workbooks or the workbooks created 
        // using the plug-in will loose their data connections.
        internal const string PluginId = "KDBPlusTickPlugin";

        internal const bool PluginIsObsolete = false;
        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        internal const string PluginTitle = "KDB+ Tick";

        internal const string PluginType = DataPluginTypes.Streaming;

        private bool disposed;
        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
        {
            try
            {
                //Set unicode encoding as default
                c.e = Encoding.UTF8;
            }
            catch (Exception)
            {
            }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        /// <summary>
        /// Gets the plug-in id.
        /// </summary>
        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        /// <summary>
        /// Gets a value indicating whether the plug-in has a valid license.
        /// </summary>
        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        /// <summary>
        /// Gets a value indicating the title of the plug-in.
        /// </summary>
        public override string Title
        {
            get { return PluginTitle; }
        }
        
        public virtual RealtimeKdbSettings CreateConnectionSettings(
            PropertyBag bag)
        {
            RealtimeKdbSettings settings = new RealtimeKdbSettings(bag);
            return settings;
        }

        public override RealtimeKdbSettings CreateSettings(PropertyBag bag)
        {
            return CreateConnectionSettings(bag);
        }

        protected override ITable CreateTable(RealtimeKdbSettings settings,
             IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            return CreateTableInternal(settings, parameters, dataDirectory,
                false);
        }

        private ITable CreateTableInternal(
            RealtimeKdbSettings settings,
            IEnumerable<ParameterValue> parameters, string dataDirectory,
            bool isSchemaRequest)
        {
            CheckLicense();

            DateTime start = DateTime.Now;

            IQueryResult result = RealtimeKdbUtil.GetTickSchema(settings,
                parameters);

            ParameterTable table;
            if (settings.SchemaColumnsSettings.SchemaColumns.Count > 0)
            {
                table = TableBuilder.CreateTableFromSavedSchema(
                    settings.SchemaColumnsSettings, parameters);
            }
            else
            {
                table = TableBuilder.CreateTableSchema(result, 
                    settings.SchemaColumnsSettings, isSchemaRequest,
                    parameters, null);
            }

            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

        public void SaveSchema(string workbookDir,
            string dataDir, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            RealtimeKdbSettings settings = CreateSettings(bag);
            CreateTableInternal(settings, parameters, dataDir, true);
        }
    }
}
