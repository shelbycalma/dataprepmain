﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.KDBPlugin.Realtime
{
    public static class RealtimeKdbUtil
    {
        public static IQueryResult GetTickSchema(RealtimeKdbSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            IQueryResult result;
            c cInstance = settings.GetConnection(parameters);
            if (settings.FunctionalSubscription)
            {
                string serviceName = KdbUtil.ApplyParameters(
                    settings.Service, parameters);
                object queryResult = cInstance.k(serviceName);

                result = ArrayQueryResult.GetSchemaQueryResult(queryResult);
            }
            else
            {
                string tableName = KdbUtil.ApplyParameters(
                    settings.Table, parameters);
                result = KdbUtil.DoQuery(tableName, cInstance);
            }
            KdbUtil.CloseConnection(cInstance);
            return result;
        }

    }
}
