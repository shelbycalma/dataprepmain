﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.KDBPlugin.Realtime
{
    public class KdbHistoricSetting : ConnectionSettings,
        IKdbConnectionSettings
    {
        public KdbHistoricSetting(PropertyBag bag)
            : base(bag)
        {
        }

        public string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value); }
        }

        public string Port
        {
            get { return GetInternal("Port", "5010"); }
            set { SetInternal("Port", value); }
        }

        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public int Timeout
        {
            get
            {
                return GetInternalInt("Timeout", 0);
            }
            set
            {
                SetInternalInt("Timeout", value);
            }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public bool IsPasswordEncrypted
        {
            get { return false; }
        }

        public bool IsDeferredSyncQuery
        {
            get
            {
                string value = GetInternal("IsDeferredSyncQuery",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("IsDeferredSyncQuery", value.ToString());
            }
        }

        public string DeferredSyncQuery
        {
            get
            {
                return GetInternal("DeferredSyncQuery",
                    KdbUtil.DefaultDeferredSyncQuery);
            }
            set { SetInternal("DeferredSyncQuery", value); }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is KdbHistoricSetting)) return false;

            KdbHistoricSetting cs = (KdbHistoricSetting)obj;

            if (this.Host != cs.Host ||
                 this.Password != cs.Password ||
                 this.Port != cs.Port ||
                 this.Query != cs.Query ||
                 this.UserName != cs.UserName ||
                 this.IsPasswordEncrypted != cs.IsPasswordEncrypted ||
                 this.Timeout != cs.Timeout ||
                 this.IsDeferredSyncQuery != cs.IsDeferredSyncQuery ||
                 this.DeferredSyncQuery != cs.DeferredSyncQuery)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    Host, Password,
                    Port, UserName, Query, 
                    Timeout, IsPasswordEncrypted,
                    IsDeferredSyncQuery, DeferredSyncQuery                    
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public virtual c GetConnection(IEnumerable<ParameterValue> parameters)
        {
            return KdbUtil.GetConnection(this, parameters);
        }
    }
}
