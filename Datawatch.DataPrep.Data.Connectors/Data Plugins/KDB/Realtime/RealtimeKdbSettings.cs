﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.KDBPlugin.Realtime
{
    public class RealtimeKdbSettings : RealtimeConnectionSettings,
        IKdbConnectionSettings
    {
        private TimeZoneHelper timeZoneHelper;
        private readonly KdbHistoricSetting historicSettings;
        private const string historicSettingsKey = "HistoricSettings";

        public const string IdSymbolColumn = "[Id Column]";
        private SchemaColumnsSettings<KdbColumn> schemaColumnsSettings;

        public RealtimeKdbSettings()
            : this(new PropertyBag())
        {
        }

        public RealtimeKdbSettings(PropertyBag bag)
            : base(bag, true)
        {
            timeZoneHelper = KdbUtil.GetTimeZoneHelper(bag);

            PropertyBag historicSetingsBag =
                bag.SubGroups[historicSettingsKey];
            if (historicSetingsBag == null)
            {
                historicSetingsBag = new PropertyBag();
            }
            historicSettings =
                CreateHistoricConnectionSettings(historicSetingsBag);
        }

        public string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value); }
        }

        public string Port
        {
            get { return GetInternal("Port", "5010"); }
            set { SetInternal("Port", value); }
        }

        public string UserName
        {
            // Property key was always "Username", instead of "UserName"
            // so have to live with it.
            get { return GetInternal("Username"); }
            set { SetInternal("Username", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public int Timeout
        {
            get
            {
                // So far kdb+tick doesn't support timeout.
                // To make it behave consistently, the default value zero would make it infinite.
                return GetInternalInt("Timeout", 0);
            }
            set
            {
                SetInternalInt("Timeout", value);
            }
        }

        public bool IsPasswordEncrypted
        {
            get { return false; }
        }

        public string Service
        {
            get 
            {
                string service = GetInternal("Service");
                if (string.IsNullOrEmpty(service))
                {
                    //old name for Service was Subscribe.
                    service = GetInternal("Subscribe");
                }
                return service;
            }
            set { SetInternal("Service", value); }
        }

        public string Table
        {
            get { return GetInternal("Table"); }
            set { SetInternal("Table", value); }
        }

        public string Symbol
        {
            get { return GetInternal("Symbol"); }
            set { SetInternal("Symbol", value); }
        }

        public string SymbolColumn
        {
            get { return GetInternal("SymbolColumn", IdSymbolColumn); }
            set { SetInternal("SymbolColumn", value); }
        }

        public bool IsConstrainBySym
        {
            get
            {
                string s = GetInternal("IsConstrainBySym", "true");
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IsConstrainBySym", Convert.ToString(value));
            }
        }

        public bool SubscribeHistorical
        {
            get
            {
                string s = GetInternal("SubscribeHistorical", "true");
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("SubscribeHistorical", Convert.ToString(value));
            }
        }

        public bool FunctionalSubscription
        {
            get
            {
                string s = GetInternal("FunctionalSubscription", "false");
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("FunctionalSubscription", Convert.ToString(value));
            }
        }

        public KdbHistoricSetting HistoricSettings
        {
            get
            {
                return historicSettings;
            }
        } 

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return timeZoneHelper;
            }
        }
        
        public override string Title
        {
            get
            {
                //TODO: tablename should come here, but it can contain parameters
                return Properties.Resources.UiRealiteKDBTitle;
            }
        }

        public virtual KdbHistoricSetting CreateHistoricConnectionSettings(
            PropertyBag bag)
        {
            KdbHistoricSetting settings = new KdbHistoricSetting(bag);
            return settings;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RealtimeKdbSettings)) return false;

            if (!base.Equals(obj)) return false;

            RealtimeKdbSettings cs = (RealtimeKdbSettings)obj;

            if (this.Host != cs.Host ||
                 this.Password != cs.Password ||
                 this.Port != cs.Port ||
                 this.Service != cs.Service ||
                 this.Table != cs.Table ||
                 this.UserName != cs.UserName ||
                 this.Symbol != cs.Symbol ||
                 this.IsConstrainBySym != cs.IsConstrainBySym ||
                 this.SymbolColumn != cs.SymbolColumn ||
                 this.SubscribeHistorical != cs.SubscribeHistorical ||
                 this.FunctionalSubscription != cs.FunctionalSubscription ||
                 !this.HistoricSettings.Equals(cs.HistoricSettings) ||
                 !this.timeZoneHelper.Equals(cs.TimeZoneHelper) ||
                 !this.SchemaColumnsSettings.Equals(cs.SchemaColumnsSettings))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    Host, Password,
                    Port, Service, Table, UserName, Symbol, 
                    IsConstrainBySym, SymbolColumn, 
                    SubscribeHistorical,
                    FunctionalSubscription,
                    HistoricSettings
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            if (SchemaColumnsSettings != null)
            {
                hashCode ^= SchemaColumnsSettings.GetHashCode();
            }

            return hashCode;
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag bag = base.ToPropertyBag();
            bag.SubGroups[historicSettingsKey] = null;
            bag.SubGroups[historicSettingsKey] = 
                historicSettings.ToPropertyBag();
            return bag;
        }

        public virtual c GetConnection(IEnumerable<ParameterValue> parameters)
        {
            return KdbUtil.GetConnection(this, parameters);
        }

        public SchemaColumnsSettings<KdbColumn> SchemaColumnsSettings
        {
            get
            {
                if (schemaColumnsSettings == null)
                {
                    schemaColumnsSettings =
                        new SchemaColumnsSettings<KdbColumn>(ToPropertyBag());
                }
                return schemaColumnsSettings;
            }
        }
    }
}
