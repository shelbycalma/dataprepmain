﻿using System;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.KDBPlugin.Properties;

namespace Panopticon.KDBPlugin.Realtime
{
    public class KdbDataAdapter :
        RealtimeDataAdapter<ParameterTable, RealtimeKdbSettings, KdbTuple>
    {
        private Thread thread;
        c cInstance;
        string tableName;
        string[] symbolList;
        object subscriptionResult;

        public override void Start()
        {
            try
            {
                string service = KdbUtil.ApplyParameters(settings.Service,
                    table.Parameters);
                tableName = KdbUtil.ApplyParameters(settings.Table,
                    table.Parameters);

                cInstance = settings.GetConnection(table.Parameters);

                if (settings.FunctionalSubscription)
                {
                    subscriptionResult = cInstance.k(service);
                }
                else 
                {
                    // Allow subscription to all symbols, symbol will be blank in that case.
                    if (string.IsNullOrEmpty(settings.Symbol))
                    {
                        symbolList = null;
                        subscriptionResult = cInstance.k(service, tableName, "");
                    }
                    else
                    {
                        string symbol = KdbUtil.ApplyParameters(settings.Symbol,
                            table.Parameters, null);

                        symbolList = symbol.Split(',').Select(s => s.Trim()).ToArray();
                        subscriptionResult = cInstance.k(service, tableName, symbolList);
                    }
                }

                string historicQuery =
                        settings.HistoricSettings.Query;

                if (settings.SubscribeHistorical &&
                    !string.IsNullOrEmpty(historicQuery))
                {
                    c cHistoric = settings.HistoricSettings.GetConnection(
                        table.Parameters);
                    bool isDeferredSync =
                        settings.HistoricSettings.IsDeferredSyncQuery;
                    string deferredSyncQuery =
                        settings.HistoricSettings.DeferredSyncQuery;
                    

                    KdbUtil.ValidateDeferredSyncQuery(isDeferredSync,
                        deferredSyncQuery);

                    historicQuery = KdbUtil.ApplyParameters(historicQuery,
                        table.Parameters, null);

                    historicQuery = KdbUtil.ApplyDeferredSyncQuery(
                        isDeferredSync,
                        deferredSyncQuery,
                        historicQuery);

                    try
                    {
                        subscriptionResult = KdbUtil.DoQuery(historicQuery,
                            cHistoric, -1, isDeferredSync);
                        
                    }
                    finally
                    {
                        KdbUtil.CloseConnection(cHistoric);
                    }
                }

                thread = new Thread(new ThreadStart(ReadSubscription));
                thread.Name = "KxPlusTick subscription reader";
                thread.Start();

            }
            catch (Exception ex)
            {
                if (settings.Service.Equals(ex.Message))
                {
                    string message = string.Format(
                        Properties.Resources.ExInvalidServiceName, 
                        ex.Message);
                    Log.Error(message);
                    throw new Exception(message);
                }
                Log.Exception(ex);
                throw;
            }
        }

        private void ReadSubscription()
        {
            //First populate initial subscription/history data if its there.
            if (subscriptionResult != null &&
                settings.SubscribeHistorical)
            {
                EnqueueSubscription();
            }

            try
            {
                while (thread != null)
                {
                    object r = null;
                    // read message from ticker plant
                    if (cInstance != null)
                    {
                        r = cInstance.k();
                    }

                    ArrayQueryResult result = new ArrayQueryResult(r);
                    if (result.TableName.Equals(tableName) ||
                        settings.FunctionalSubscription)
                    {
                        for (int i = 0; i < result.RowCount; i++)
                        {
                            if (thread == null) break;
                            KdbTuple tuple =
                                KdbUtil.GetTupleFromArrayResult(result, i);
                            Enqueue(tuple);
                        }
                    }
                }

            }
            catch (Exception e)
            {
                // Fix for DDTV-902, Don't know if this can be handled in a better way.
                // Shouldn't hurt if we just swallow Interrupted function call exception.
                SocketException socketEx = e.InnerException as SocketException;
                if (socketEx == null || socketEx.ErrorCode != 10004)
                {
                    Log.Exception(e);
                }
                thread = null;
            }

        }

        private void Enqueue(KdbTuple tuple)
        {
            if (!tuple.Values.ContainsKey(settings.IdColumn))
            {
                Log.Error(Properties.Resources.LogErrorInvalidIDColumn,
                    settings.IdColumn);
                thread = null;
                return;
            }

            if (symbolList != null && settings.IsConstrainBySym)
            {
                string symbolColumn = settings.SymbolColumn;
                if (symbolColumn == RealtimeKdbSettings.IdSymbolColumn)
                {
                    symbolColumn = settings.IdColumn;
                }
                KdbValue symbol = tuple.Values[symbolColumn];
                if (symbol != null && !isSubscribedSymbol(symbol.Value.ToString()))
                {
                    //process monitored symbols only.
                    return;
                }
            }
            string id = tuple.Values[settings.IdColumn].Value.ToString();

            updater.EnqueueUpdate(id, tuple);
        }

        private void EnqueueSubscription()
        {
            if (subscriptionResult == null)
                return;

            IQueryResult result = subscriptionResult as IQueryResult;

            if(result == null)
            {
                return;
            }

            Log.Info(Resources.LogEnqueueHistoricalData);
            for (int i = 0; i < result.RowCount; i++)
            {
                KdbTuple tuple =
                    KdbUtil.GetTupleFromQueryResult(result, i);
                Enqueue(tuple);
            }
        }

        public override void Stop()
        {
            thread = null;
            KdbUtil.CloseConnection(cInstance);

        }

        public override void UpdateColumns(Row row, KdbTuple evt)
        {
            
            foreach (string name in evt.Values.Keys)
            {
                if (name == settings.IdColumn) continue;
                
                KdbValue v = evt.Values[name];

                TableBuilder.SetColumnValue(table, row, v.Value,
                    name,settings.TimeZoneHelper);
            }
        }

        public override DateTime GetTimestamp(KdbTuple row)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return TimeValue.Empty;
            }

            object value = row.Values[settings.TimeIdColumn].Value;
            return value != null ?
                ConvertToDateTime(value) : TimeValue.Empty;

        }

        private DateTime ConvertToDateTime(object value)
        {
            DateTime dt;

            if (value is TimeSpan)
            {
                return KdbUtil.ConvertFromTimeSpan(value,
                    settings.TimeZoneHelper);
            }
            if (value is c.KTimespan)
            {
                return KdbUtil.ConvertFromKTimeSpan(value,
                    settings.TimeZoneHelper);
            }
            else if (DateTime.TryParse(value.ToString(), out dt))
            {
                return dt;
            }
            
            return TimeValue.Empty;
        }

        bool isSubscribedSymbol(string symbol)
        {
            foreach (string sym in symbolList)
            {
                if (sym.Equals(symbol))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
