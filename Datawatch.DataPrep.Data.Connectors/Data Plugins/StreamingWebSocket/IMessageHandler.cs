﻿using System.Xml;
using Newtonsoft.Json.Linq;

namespace Panopticon.StreamingWebSocketPlugin
{
    interface IMessageHandler
    {
        bool ThrowAwayFirstRow();
        void HandleIncomingXml(XmlElement element);
        void HandleIncomingJson(JToken token);
        void HandleIncomingMessage(string message);
    }
}
