﻿using System;
using System.Collections.Generic;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Newtonsoft.Json.Linq;

/**
 * This plugin do one of the following two types of connections:
 * 1. WebSocket (if url start with "ws://" or "wss://")
 * 2. Long Polling (if url starts with "http://" or "https://")
 * 
 * When doing Long Polling the requests are done in certain intervals.
 * The interval time is set in the "Real-time Limit" property.
 */

namespace Panopticon.StreamingWebSocketPlugin
{
    public class StreamingWebSocketAdapter :
        MessageQueueAdapterBase<ParameterTable,
            StreamingWebSocketSettings, Dictionary<string, object>>,
        IMessageHandler
    {
        private ClientBase client = null;

        public override void Start()
        {
            settings.Parameters = table.Parameters;
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();

            if (string.IsNullOrWhiteSpace(settings.WebPath))
            {
                return;
            }

            if (settings.IsWebSocketUrl)
            {
                client = new WebSocketClient(this, settings, parser,
                    Table.Parameters);
            }
            else if (settings.IsHttpUrl)
            {
                client = new LongPollingClient(this, settings, parser,
                    Table.Parameters);
            }
            else
            {
                Exception ex = new Exception(
                    string.Format(Properties.Resources.ExErrorUnsupportedUrl,
                    settings.IsWebSocketUrl));
                Log.Exception(ex);
                throw ex;
            }

            if (client != null)
            {
                client.Start();
            }
        }

        public override void Stop()
        {
            base.Stop();
            client.Stop();
            client = null;
        }

        public ParameterTable Table
        {
            get { return table; }
        }

        public string IdColumn
        {
            get { return idColumn; }
        }

        public TableUpdater<ParameterTable,
            StreamingWebSocketSettings, Dictionary<string, object>> Updater
        {
            get { return updater; }
        }

        public IParser Parser
        {
            get { return parser; }
        }

        public bool ThrowAwayFirstRow()
        {
            if (!(settings.ParserSettings is TextFileParserSettings))
            {
                return false;
            }
            return ((TextFileParserSettings)settings.ParserSettings).
                IsFirstRowHeading;
        }
        public void HandleIncomingXml(XmlElement element)
        {
            XPathParser xpathParser = (XPathParser)parser;
            Dictionary<string, object> dict =
                xpathParser.Parse(element);
            DataPluginUtils.EnqueueMessage(dict, IdColumn, Updater);
        }

        public void HandleIncomingJson(JToken token)
        {
            JsonParser jsonParser = (JsonParser)parser;
            Dictionary<string, object> dict = jsonParser.Parse(token);
            DataPluginUtils.EnqueueMessage(dict, IdColumn, Updater);
        }

        public void HandleIncomingMessage(string message)
        {
            MessageReceived(message);
        }
    }
}
