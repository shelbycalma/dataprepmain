﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.StreamingWebSocketPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<ParameterTable, StreamingWebSocketSettings,
        Dictionary<string, object>, StreamingWebSocketAdapter>
    {
        internal const string PluginId = "StreamingWebSocketPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "WebSocket";
        internal const string PluginType = DataPluginTypes.Streaming;

        /// <summary>
        /// DataPlugin for Streaming WebSocket.
        /// </summary>
        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override StreamingWebSocketSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new StreamingWebSocketSettings(pluginManager, parameters);
        }

        public override StreamingWebSocketSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new StreamingWebSocketSettings(pluginManager, bag, null);
        }
    }
}
