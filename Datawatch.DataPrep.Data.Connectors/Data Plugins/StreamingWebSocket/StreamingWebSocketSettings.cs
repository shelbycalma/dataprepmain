﻿using System.Collections.Generic;
using System.Net;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.StreamingWebSocketPlugin
{
    public class StreamingWebSocketSettings : MessageQueueSettingsBase,
        IWebPathSettings
    {
        public const string Http = "http://";
        public const string Https = "https://";
        public const string WS = "ws://";
        public const string WSS = "wss://";

        public StreamingWebSocketSettings(IPluginManager pluginManager)
            : base(pluginManager, null)
        {
        }
        public StreamingWebSocketSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
            if (string.IsNullOrEmpty(WebPath))
            {
                WebPath = WS;
            }
        }

        public StreamingWebSocketSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        protected override string DefaultParserPluginId
        {
            get { return "Xml"; }
        }

        public string WebPath
        {
            get
            {
                return GetInternal("WebPath");
            }
            set
            {
                SetInternal("WebPath", value);
                FirePropertyChanged("IsWebSocketUrl");
            }
        }

        public string WebUserID
        {
            get
            {
                return GetInternal("WebUserID");
            }

            set
            {
                SetInternal("WebUserID", value);
            }
        }

        public string RecordsPath
        {
            get
            {
                return GetInternal("RecordsPath");

            }
            set
            {
                SetInternal("RecordsPath", value);
            }
        }

        public string WebPassword
        {
            get
            {
                return GetInternal("WebPassword");
            }

            set
            {
                SetInternal("WebPassword", value);
            }
        }

        public string RequestBody
        {
            get
            {
                return GetInternal("RequestBody");
            }

            set
            {
                SetInternal("RequestBody", value);
            }
        }

        public int RequestTimeout
        {
            get
            {
                return GetInternalInt("RequestTimeout", 10);
            }
            set
            {
                SetInternalInt("RequestTimeout", value);
            }
        }

        public WebHeaderCollection WebHeaderCollection
        {
            get { return null; }
        }

        public int[] RequestTimeouts
        {
            get
            {
                return new int[]{
                    10,
                    20,
                    30,
                    60
                };
            }
        }
        
        public override IParserPlugin ParserPlugin
        {
            get { return base.ParserPlugin; }
            set
            {
                base.ParserPlugin = value;
                FirePropertyChanged("IsRecordPathMessageType");
            }
        }

        public bool IsRecordPathMessageType
        {
            get
            {
                return string.Equals("Xml", ParserPlugin.Id) || string.Equals("Json", ParserPlugin.Id);
            }
        }

        // Remove the FixPlugin. It's not supported here
        public override IEnumerable<IParserPlugin> ParserPlugins
        {
            get
            {
                List<IParserPlugin> newPluginList = new List<IParserPlugin>();
                foreach (IParserPlugin p in base.ParserPlugins)
                {
                    if (p.Id == "Fix")
                    {
                        continue;
                    }
                    newPluginList.Add(p);
                }
                return newPluginList;
            }
        }

        public bool IsWebSocketUrl
        {
            get
            {
                string url = WebPath;
                if (string.IsNullOrWhiteSpace(url))
                {
                    return false;
                }
                if (Parameters != null)
                {
                    url = DataUtils.ApplyParameters(url, Parameters);
                }
                return url.ToLower().StartsWith(StreamingWebSocketSettings.WS) ||
                       url.ToLower().StartsWith(StreamingWebSocketSettings.WSS);
            }
        }

        public bool IsHttpUrl
        {
            get
            {
                string url = WebPath;
                if (string.IsNullOrWhiteSpace(url))
                {
                    return false;
                }
                return url.ToLower().StartsWith(StreamingWebSocketSettings.Http) ||
                       url.ToLower().StartsWith(StreamingWebSocketSettings.Https);
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StreamingWebSocketSettings)) return false;

            if (!base.Equals(obj)) return false;

            StreamingWebSocketSettings cs = (StreamingWebSocketSettings)obj;

            if (!string.Equals(WebPath, cs.WebPath) ||
                !string.Equals(WebUserID, cs.WebUserID) ||
                !string.Equals(WebPassword, cs.WebPassword) ||
                !string.Equals(RequestBody, cs.RequestBody))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            object[] fields = { WebPath, WebUserID, WebPassword, RequestBody };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public override bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(WebPath) &&
                    IsParserSettingsOk;
            }
        }

        public override void DoGenerateColumns()
        {
            WebSocketColumnGenerator columnGenerator =
                new WebSocketColumnGenerator(this, this.errorReporter);
            ClientBase client = null;
            IParser parser = ParserSettings.CreateParser();

            if (IsWebSocketUrl)
            {
                client = new WebSocketClient(columnGenerator, this, parser,
                    Parameters);
            }
            else if (IsHttpUrl)
            {
                client = new LongPollingClient(columnGenerator, this, parser,
                    Parameters);
            }
            if (client != null)
            {
                client.Start();
                columnGenerator.Generate();
                client.Stop();
            }
            // Start receiving data to generate columns

        }

        public override bool CanGenerateColumns()
        {
            return !string.IsNullOrEmpty(WebPath);
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }
    }
}
