﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Newtonsoft.Json.Linq;
using Panopticon.StreamingWebSocketPlugin.Properties;

namespace Panopticon.StreamingWebSocketPlugin
{
    class LongPollingClient : ClientBase
    {
        private readonly string postData;
        private readonly Thread pollingThread;

        public LongPollingClient(IMessageHandler messageHandler,
            StreamingWebSocketSettings settings, IParser parser,
            IEnumerable<ParameterValue> parameters) :
            base(messageHandler, settings, parser, parameters)
        {
            postData = null;
            if (!string.IsNullOrEmpty(settings.RequestBody))
            {
                postData = DataUtils.ApplyParameters(settings.RequestBody,
                    parameters);
            }
            pollingThread = new Thread(LongPoll);
            pollingThread.Name = "Streaming WS - Polling Thread";
        }

        // HTTP: Simulate long polling
        public void LongPoll()
        {
            HttpWebResponse webResponse;
            StreamReader stream = null;
            int realtimeUpdateLimit = settings.Limit;
            long realtimeUpdateLimitTicks =
                realtimeUpdateLimit * TimeSpan.TicksPerMillisecond;
            string parserPluginId = settings.ParserPluginId;
            long lastPollTicks;
            while (!stop)
            {
                // Request new data
                lastPollTicks = DateTime.Now.Ticks;
                try
                {
                    HttpWebRequest httpWebRequest = DataPluginUtils.GetHttpWebRequestInstance(url, settings.RequestTimeout,
                        userId, pwd, postData);
                    webResponse =
                        (HttpWebResponse)httpWebRequest.GetResponse();
                    stream = new StreamReader(webResponse.GetResponseStream(),
                        Encoding.UTF8);
                    // Handle the data
                    switch (parserPluginId)
                    {
                        case "Text":
                            ReadTextStream(stream);
                            break;
                        case "Xml":
                            ReadXmlStream(stream);
                            break;
                        case "Json":
                            ReadJsonStream(stream);
                            break;
                    }
                    if (stop)
                    {
                        return;
                    }
                }
                catch (Exception e)
                {
                    Log.Error(Resources.ExErrorConnectionError, e.Message);
                }
                finally
                {
                    if (stream != null)
                    {
                        stream.Close();
                        stream.Dispose();
                    }
                }
                long currentTimeTicks = DateTime.Now.Ticks;
                long timeSinceLastPollTicks = currentTimeTicks - lastPollTicks;
                if (timeSinceLastPollTicks < realtimeUpdateLimitTicks)
                {
                    // Not yet time to do another poll => take a nap.
                    int timeToSleepMillis = (int)((realtimeUpdateLimitTicks - timeSinceLastPollTicks) / TimeSpan.TicksPerMillisecond);
                    Thread.Sleep(timeToSleepMillis);
                }
            }
        }

        private void ReadTextStream(StreamReader stream)
        {
            string line = stream.ReadLine();
            if (messageHandler.ThrowAwayFirstRow())
            {
                // Consume the first row that is a heading row
                line = stream.ReadLine();
            }
            while (line != null)
            {
                if (stop)
                {
                    return;
                }
                messageHandler.HandleIncomingMessage(line);
                line = stream.ReadLine();
            }
        }

        private void ReadXmlStream(StreamReader stream)
        {
            if (recordPathUsed)
            {
                XPathParser xpathParser = (XPathParser)parser;
                // Can be several rows. But most likely multi rows.
                XmlNodeList xmlRows = xpathParser.GetRecords(stream, settings.RecordsPath);
                // Add the rows
                foreach (XmlElement element in xmlRows)
                {
                    if (stop)
                    {
                        return;
                    }
                    messageHandler.HandleIncomingXml(element);
                }
            }
            else
            {
                // Must me single row
                MessageReceived(stream);
            }
        }

        private void ReadJsonStream(StreamReader stream)
        {
            if (recordPathUsed)
            {
                JsonParser jsonParser = (JsonParser)parser;
                JToken jRows = jsonParser.FindRecords(stream,
                    jsonRecordsPath);
                if (jRows != null)
                {
                    foreach (JToken t in jRows)
                    {
                        if (!t.HasValues) continue; // if null => continue
                        if (stop)
                        {
                            return;
                        }
                        messageHandler.HandleIncomingJson(t);
                    }
                }
            }
            else
            {
                // Must me single row
                MessageReceived(stream);
            }
        }

        private void MessageReceived(StreamReader stream)
        {
            string line = stream.ReadToEnd();
            if (!string.IsNullOrWhiteSpace(line))
            {
                messageHandler.HandleIncomingMessage(line);
            }
        }

        public override void Start()
        {
            base.Start();
            pollingThread.Start();
        }

    }
}
