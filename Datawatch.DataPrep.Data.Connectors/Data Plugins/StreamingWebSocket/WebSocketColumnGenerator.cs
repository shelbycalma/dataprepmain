﻿using System.Xml;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Newtonsoft.Json.Linq;

namespace Panopticon.StreamingWebSocketPlugin
{
    class WebSocketColumnGenerator : StreamingColumnGenerator, IMessageHandler
    {
        public WebSocketColumnGenerator(
            StreamingWebSocketSettings settings, IPluginErrorReportingService errorReporter)
            : base(settings, 10, settings.RequestTimeout, errorReporter)
        {
        }

        public bool ThrowAwayFirstRow()
        {
            // If there are a header row we want to use those headers => do not throw it away
            return false;
        }

        public void HandleIncomingXml(XmlElement element)
        {
            string xml = element.OuterXml;
            MessageReceived(xml);
        }

        public void HandleIncomingJson(JToken token)
        {
            // provide the data to the column generator
            string json = token.ToString();
            MessageReceived(json);
        }

        public void HandleIncomingMessage(string message)
        {
            MessageReceived(message);
        }
    }
}
