﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.StreamingWebSocketPlugin
{
    abstract class ClientBase
    {
        protected readonly IMessageHandler messageHandler;
        protected readonly StreamingWebSocketSettings settings;
        protected readonly bool recordPathUsed;
        protected readonly string url;
        protected readonly string userId;
        protected string pwd;
        protected bool stop;
        protected readonly string jsonRecordsPath;
        protected readonly IParser parser;

        protected ClientBase(IMessageHandler messageHandler,
            StreamingWebSocketSettings settings, IParser parser,
            IEnumerable<ParameterValue> parameters)
        {
            this.messageHandler = messageHandler;
            this.settings = settings;
            this.parser = parser;

            url = DataUtils.ApplyParameters(settings.WebPath, parameters);
            userId = DataUtils.ApplyParameters(settings.WebUserID, parameters);
            pwd = DataUtils.ApplyParameters(settings.WebPassword, parameters);

            recordPathUsed = !string.IsNullOrWhiteSpace(settings.RecordsPath);

            if ("Json".Equals(settings.ParserPluginId) && recordPathUsed)
            {
                // If we expect an json array with rows => recordPath must be set to something. 
                // If a array comes directly we then can use "." to indicate that. But that has to be replaced by "" so 
                // we know if we need to first extract jArray (with a number of json objects) first or we have a json 
                // object directly. 
                jsonRecordsPath = ".".Equals(settings.RecordsPath.Trim()) ?
                    "" : settings.RecordsPath;
            }

        }

        public virtual void Start()
        {
            stop = false;
        }

        public virtual void Stop()
        {
            stop = true;
        }
    }
}
