﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Newtonsoft.Json.Linq;
using Panopticon.StreamingWebSocketPlugin.Properties;
using SuperSocket.ClientEngine;
using WebSocket4Net;

namespace Panopticon.StreamingWebSocketPlugin
{
    class WebSocketClient : ClientBase
    {
        private const int InitialStringBuilderSize = 1024;
        private WebSocket websocket;
        private StringBuilder sb;
        private XmlDocument reusableDoc;
        private readonly object lockObject = new object();
        private bool wrongUrl;

        public WebSocketClient(IMessageHandler messageHandler,
            StreamingWebSocketSettings settings,
            IParser parser, IEnumerable<ParameterValue> parameters) :
            base(messageHandler, settings, parser, parameters)
        {
            InitWebSocket();
        }

        private void InitWebSocket()
        {
            if (string.IsNullOrWhiteSpace(userId))
            {
                websocket = new WebSocket(url);
            }
            else
            {
                List<KeyValuePair<string, string>> customHeaderItemList = new List<KeyValuePair<string, string>>();
                pwd = pwd ?? "";
                string authInfo = userId + ":" + pwd;
                authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
                customHeaderItemList.Add(new KeyValuePair<string, string>("Authorization", "Basic " + authInfo));

                websocket = new WebSocket(url, "", null, customHeaderItemList, "", "", WebSocketVersion.Rfc6455);
            }
            websocket.Opened += new EventHandler(websocket_Opened);
            websocket.Error += new EventHandler<ErrorEventArgs>(websocket_Error);
            websocket.Closed += new EventHandler(websocket_Closed);

            switch (settings.ParserPluginId)
            {
                case "Text":
                    websocket.MessageReceived +=
                        new EventHandler<MessageReceivedEventArgs>(websocket_MessageReceived_Text);
                    break;
                case "Xml":
                    websocket.MessageReceived +=
                        new EventHandler<MessageReceivedEventArgs>(websocket_MessageReceived_Xml);
                    break;
                case "Json":
                    websocket.MessageReceived +=
                        new EventHandler<MessageReceivedEventArgs>(websocket_MessageReceived_Json);
                    break;
            }
        }

        private void websocket_Opened(object sender, EventArgs e)
        {
            Log.Info(Resources.ExWebSocketConnectionOpened);
        }

        private void websocket_Error(object sender, ErrorEventArgs e)
        {
            // Try restart websocket.=> Close the old websocket first.
            if ("No such host is known".Equals(e.Exception.Message))
            {
                Log.Error(Resources.ErrorOpeningWebSocket, url, e.Exception.Message);
                wrongUrl = true;
                return;
            }
            else
            {
                Log.Error(Resources.ExErrorConnectionError, e.Exception.Message);
            }
            lock (lockObject)
            {
                if (stop)
                {
                    return;
                }
                StopWebSocket();
                InitWebSocket();
                StartWebSocket();
            }
        }

        private void websocket_Closed(object sender, EventArgs e)
        {
            Log.Info(Resources.ExWebSocketConnectionClosed);
            lock (lockObject)
            {
                if (stop || wrongUrl)
                {
                    return;
                }
                // The connection was not stoped by the user => try to reopen the connection again
                StopWebSocket();
                InitWebSocket();
                StartWebSocket();
            }
        }

        private void websocket_MessageReceived_Text(object sender, MessageReceivedEventArgs e)
        {
            string uncutMessage = e.Message;

            try
            {
                if (sb == null)
                {
                    sb = new StringBuilder(InitialStringBuilderSize);
                }
                sb.Length = 0;
                int rowCharIndex = -1;
                for (int i = 0; i < uncutMessage.Length; i++)
                {
                    rowCharIndex++;
                    sb.Append(uncutMessage[i]);
                    bool hadNewLine = StripNewLineIfExist(rowCharIndex);
                    if (sb.Length > 0 && (hadNewLine || i >= uncutMessage.Length - 1))
                    {
                        // StringBuilder contain anything to send && 
                        // (full row have been loaded or all have been read)
                        if (stop)
                        {
                            return;
                        }
                        messageHandler.HandleIncomingMessage(sb.ToString());
                        sb.Length = 0;
                        rowCharIndex = -1;
                    }
                }
            }
            catch
            {
                return;
            }
        }

        // Return true if NewLine was found
        private bool StripNewLineIfExist(int index)
        {
            if (sb[index] == '\n')
            {
                if (index - 1 >= 0 && sb[index - 1] == '\r')
                {
                    // Remove also the carrige return
                    sb.Length = sb.Length - 2;
                    return true;
                }
                // New Line found
                sb.Length = sb.Length - 1;
                return true;
            }
            return false;
        }

        private void websocket_MessageReceived_Xml(object sender, MessageReceivedEventArgs e)
        {
            string uncutMessage = e.Message;

            try
            {
                if (recordPathUsed)
                {
                    if (reusableDoc == null)
                    {
                        reusableDoc = new XmlDocument();
                    }
                    XPathParser xpathParser = (XPathParser)parser;
                    // Can be several rows. But most likely multi rows.
                    XmlNodeList xmlRows = xpathParser.GetRecords(reusableDoc, uncutMessage,
                        settings.RecordsPath);
                    // Add the rows
                    foreach (XmlElement element in xmlRows)
                    {
                        if (stop)
                        {
                            return;
                        }
                        messageHandler.HandleIncomingXml(element);
                    }
                }
                else
                {
                    // Must me single rows
                    messageHandler.HandleIncomingMessage(uncutMessage);
                }
            }
            catch
            {
                // Keeping the try / catch in case the below error wont arrise.
                // Solves issue 16325.
                // If wrong message type is sent to the parser it throws Exception. 
                // Catch it to avoid for example the QpidListener -> Listener crash 
                // internally because MessageTransfer() calls this method, and then
                // the session is dead and hangs at session.Close().
                return;
            }
        }

        private void websocket_MessageReceived_Json(object sender, MessageReceivedEventArgs e)
        {
            string uncutMessage = e.Message;

            try
            {
                if (recordPathUsed)
                {
                    JsonParser jsonParser = (JsonParser)parser;
                    JToken jRows = jsonParser.FindRecords(uncutMessage,
                        jsonRecordsPath);
                    if (jRows != null)
                    {
                        foreach (JToken t in jRows)
                        {
                            if (!t.HasValues) continue; // if null => continue

                            if (stop)
                            {
                                return;
                            }
                            messageHandler.HandleIncomingJson(t);
                        }
                    }
                }
                else
                {
                    messageHandler.HandleIncomingMessage(uncutMessage);
                }
            }
            catch
            {
                return;
            }
        }

        public override void Start()
        {
            lock (lockObject)
            {
                StartWebSocket();
            }
        }

        public override void Stop()
        {
            lock (lockObject)
            {
                StopWebSocket();
            }
        }

        private void StartWebSocket()
        {
            base.Start();
            websocket.Open();
        }

        private void StopWebSocket()
        {
            base.Stop();
            websocket.Opened -= new EventHandler(websocket_Opened);
            websocket.Error -= new EventHandler<ErrorEventArgs>(websocket_Error);
            websocket.Closed -= new EventHandler(websocket_Closed);
            websocket.Close();
            websocket = null;
        }

    }
}
