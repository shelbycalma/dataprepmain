﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panopticon.Dashboards.Plugin;
using Panopticon.Developer.Licensing;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using Panopticon.Developer.Framework.Table;
using Panopticon.Dashboards.Model;
using System.Windows.Media;
using System.Windows;
using Microsoft.Win32;
using System.IO;
using Panopticon.Dashboards;

namespace Panopticon.ExcelPlugin
{
    public abstract class PluginBase : IDataPlugin
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.ExcelPlugin;component" +
            "/connect-excel.gif";

        protected IPluginHost pluginHost;
        private ImageSource connectImage;
        private PropertyBag defaultSettings;

        private const string id = "08ADA421-533F-4b33-ACA9-722A872083CD";

        public const string RangeKey = "range";
        public const string PathKey = "path";
        public const string UnpivotKey = "unpivot";

        protected string title;
        protected PanopticonXmlLicense license = null;
        private bool disposed;

        public PluginBase()
        {
            try
            {
                connectImage = new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }

            license = CheckLicense();
        }

        ~PluginBase()
        {
            Dispose(false);
        }

        internal string BrowseForFile()
        {
            return BrowseForFile(null);
        }

        internal string BrowseForFile(string oldFilename)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            IWindowsHost windowsHost = pluginHost as IWindowsHost;
            Window owner = (windowsHost != null ? windowsHost.MainWindow : null);
            dlg.FileName = oldFilename;
            dlg.Filter = "Microsoft Office Excel Files (*.xls, *.xslx)|*.xls;*.xlsx";
            if (dlg.ShowDialog() == true)
            {
                return dlg.FileName;
            }
            return null;
        }

        protected virtual PanopticonXmlLicense CheckLicense()
        {
            try
            {
                return (PanopticonXmlLicense)
                    LicenseManager.Validate(this.GetType(), this);
            }
            catch (Exception)
            {
            }
            return null;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (license != null)
                    {
                        license.Dispose();
                    }
                }
                license = null;
                disposed = true;
            }
        }

        public abstract PropertyBag[] DoConnect();

        public ITable GetAllData(string workbookDir, string dataDir,
            PropertyBag settings, Dictionary<string, string> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters, -1);
        }

        public object GetConfigElement(PropertyBag bag)
        {
            ConfigPanel cp = new ConfigPanel(this);

            cp.Path = bag.Values[PathKey];
            cp.Range = bag.Values[RangeKey];
            cp.Unpivot = Convert.ToBoolean(bag.Values[UnpivotKey]);

            return cp;
        }

        public abstract ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, Dictionary<string, string> parameters, int rowcount);

        public string[] GetDataFiles(PropertyBag settings)
        {
            if (settings.Values.ContainsKey(PathKey))
            {
                string path = settings.Values[PathKey];
                return new string[] { path };
            }
            return null;
        }

        public PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            PropertyBag bag = new PropertyBag();
            bag.Values[PathKey] = configPanel.Path;
            bag.Values[RangeKey] = configPanel.Range;
            bag.Values[UnpivotKey] = Convert.ToString(configPanel.Unpivot);
            return bag;
        }

        public string GetTitle(PropertyBag settings)
        {
            return GetTitle(settings.Values[PathKey], settings.Values[RangeKey]);
        }

        protected string GetTitle(string path, string range)
        {
            range = RemoveEncapsChars(range);

            int index = range.LastIndexOf('$');
            string rangeDesc = range;

            if (index != -1)
            {
                rangeDesc = RemoveEncapsChars(range.Substring(0, index));

                if (index < range.Length - 1)
                {
                    string subDesc =
                        range.Substring(index + 1, range.Length - index - 1);
                    rangeDesc += string.Format(" ({0})", RemoveEncapsChars(subDesc));
                }
            }

            if (path != null)
            {
                return string.Format("{0} - {1}",
                    Path.GetFileName(path), rangeDesc);
            }
            else
            {
                return rangeDesc;
            }
        }

        public string Id
        {
            get { return id; }
        }

        public ImageSource Image
        {
            get { return connectImage; }
        }

        public void Initialize(IPluginHost host, PropertyBag settings)
        {
            pluginHost = host;
            this.defaultSettings = settings;
        }

        public bool IsLicensed
        {
            get { return license != null; }
        }

        public PropertyBag Settings
        {
            get { return defaultSettings; }
        }

        public string Title
        {
            get { return title; }
        }

        private string RemoveEncapsChars(string text)
        {
            int offset = 0;
            int length = text.Length;

            if (text.StartsWith("'"))
            {
                offset++;
                length--;
            }

            if (text.EndsWith("'"))
            {
                length--;
            }

            return text.Substring(offset, length);
        }

        public void UpdateFileLocation(PropertyBag settings, string oldPath,
            string newPath, string workbookPath)
        {
            if (Path.GetFileName(settings.Values[PathKey]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values[PathKey] =
                    Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }
    }
}
