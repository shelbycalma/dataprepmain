﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;


namespace Panopticon.ExcelPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ConnectionSettings>
    {
        public const string PathKey = "path";

        public const string RangeKey = "range";

        public const string RealtimeKey = "realtime";

        public const string UnpivotKey = "unpivot";

        internal const string PluginId = "08ADA421-533F-4b33-ACA9-722A872083CD";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "MS Excel - Streaming";
        internal const string PluginType = DataPluginTypes.Streaming;

        private readonly Realtime realtimeExcelConnector;

        private readonly Static staticExcelConnector;

        private bool disposed;

        public Plugin()
        {
            staticExcelConnector = new Static();
            realtimeExcelConnector = new Realtime(this);
        }

        ~Plugin()
        {
            Dispose(false);
        }

        public event EventHandler<RealtimeStatusEventArgs> StreamingStatusChanged;

        public event EventHandler<RealtimeDataPluginEventArgs> Updated;

        public event EventHandler<RealtimeDataPluginEventArgs> Updating;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public Realtime RealtimeExcelConnector
        {
            get { return realtimeExcelConnector; }
        }

        public Static StaticExcelConnector
        {
            get { return staticExcelConnector; }
        }
        
        public static string GetTitle(string path, string range)
        {
            range = RemoveEncapsChars(range);

            int index = range.LastIndexOf('$');
            string rangeDesc = range;

            if (index != -1)
            {
                rangeDesc = RemoveEncapsChars(range.Substring(0, index));

                if (index < range.Length - 1)
                {
                    string subDesc =
                        range.Substring(index + 1, range.Length - index - 1);
                    rangeDesc += string.Format(
                        " ({0})", RemoveEncapsChars(subDesc));
                }
            }

            if (path != null)
            {
                return string.Format("{0} - {1}",
                    Path.GetFileName(path), rangeDesc);
            }
            else
            {
                return rangeDesc;
            }
        }

        public override ConnectionSettings CreateSettings(PropertyBag bag)
        {
            throw new NotImplementedException();
        }

        public override ITable GetAllData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters, -1);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            int rowcount)
        {
            CheckLicense();

            ITable table;

            string path = settings.Values[PathKey];
            string range = settings.Values[RangeKey];
            bool unpivot = Convert.ToBoolean(settings.Values[UnpivotKey]);

            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(workbookDir, path);
            }

            if (!File.Exists(path))
            {
                string dir = path.Substring(0,
                    path.LastIndexOf(Path.DirectorySeparatorChar));
                string fileName = Path.GetFileName(path);
                string testPath = Path.Combine(dataDir, fileName);
                if (File.Exists(testPath))
                {
                    path = testPath;
                    settings.Values[PathKey] = path;
                }
            }

            bool realtime = realtimeExcelConnector.IsLicensed &&
                settings.Values.ContainsKey(RealtimeKey) &&
                Boolean.Parse(settings.Values[RealtimeKey]);

            if (realtime)
            {
                table = realtimeExcelConnector.GetData(path, range,
                    parameters, rowcount);
            }
            else
            {
                table = staticExcelConnector.GetData(path, range,
                    parameters, rowcount);
            }

            if (unpivot)
            {
                const string keyColumnName = "Id";
                string numericColumnName = GetTitle(null, range);
                table = ExcelSourceTransform(table,
                    keyColumnName, numericColumnName);
            }

            if (!realtime || unpivot)
            {
                // For non-real-time data, and when real-time data is
                // unpivotted we're creating a new StandaloneTable  on each
                // request, so allow EX to freeze the table we return.
                Debug.Assert(table is StandaloneTable);
                StandaloneTable freezable = (StandaloneTable)table;
                freezable.BeginUpdate();
                try
                {
                    freezable.CanFreeze = true;
                }
                finally
                {
                    freezable.EndUpdate();
                }
            }

            return table;
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            return GetData(workbookDir, dataDir, settings, parameters, rowcount);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            if (settings.Values.ContainsKey(PathKey))
            {
                string path = settings.Values[PathKey];
                return new[] { path };
            }
            return null;
        }

        public bool IsRealtime(PropertyBag settings)
        {
            if (settings != null &&
                settings.Values.ContainsKey(RealtimeKey))
            {
                return Convert.ToBoolean(settings.Values[RealtimeKey]);
            }
            return false;
        }

        public void StartRealtimeUpdates(ITable table, PropertyBag settings)
        {
            bool isRealtime = Convert.ToBoolean(settings.Values[RealtimeKey]);

            // only start tables that is selected as real time 
            if (isRealtime)
            {
                realtimeExcelConnector.StartRealtimeUpdates(table);
            }
        }

        public void StopRealtimeUpdates(ITable table, PropertyBag settings)
        {
            // stop regardless of data source settings
            realtimeExcelConnector.StopRealtimeUpdates(table);
        }

        public override void UpdateFileLocation(PropertyBag settings, string oldPath,
            string newPath, string workbookPath)
        {
            if (Path.GetFileName(settings.Values[PathKey]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values[PathKey] =
                    Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }

        internal void FireStreamingStatusChanged(
            ITable table, StreamingStatus status)
        {
            if (StreamingStatusChanged != null)
            {
                StreamingStatusChanged(this,
                    new RealtimeStatusEventArgs(table, status));
            }
        }

        internal void FireUpdated(ITable table)
        {
            if (Updated != null)
            {
                Updated(this, new RealtimeDataPluginEventArgs(table));
            }
        }

        internal void FireUpdating(ITable table)
        {
            if (Updating != null)
            {
                Updating(this, new RealtimeDataPluginEventArgs(table));
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }
        private static ITable ExcelSourceTransform(ITable sourceTable,
            string keyColumnName, string numericColumnName)
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            try
            {
                TextColumn keyColumn = new TextColumn(keyColumnName);
                table.AddColumn(keyColumn);

                //Dictionary<string, Row> keyLookup =
                //    new Dictionary<string, Row>();

                NumericTimeseriesColumn ntc =
                    new NumericTimeseriesColumn(numericColumnName);
                table.AddColumn(ntc);

                Row[] rowIndex = new Row[sourceTable.ColumnCount - 1];

                for (int i = 1; i < sourceTable.ColumnCount; i++)
                {
                    string key = sourceTable.GetColumn(i).Name;
                    Row row = table.AddRow();
                    keyColumn.SetTextValue(row, key);
                    //keyLookup.Add(key, row);
                    rowIndex[i - 1] = row;
                }

                ColumnReader[] sourceReaders =
                    ColumnReader.ForTable(sourceTable);

                for (int row = 0; row < sourceTable.RowCount; row++)
                {
                    if (sourceReaders[0].GetValue(row) == null)
                    {
                        continue;
                    }

                    DateTime dateTime =
                        (DateTime)sourceReaders[0].GetValue(row);
                    Time time = table.GetTime(dateTime);
                    if (time == null)
                    {
                        time = table.AddTime(dateTime);
                    }
                    for (int j = 1; j < sourceTable.ColumnCount; j++)
                    {
                        object value = sourceReaders[j].GetValue(row);
                        if (value != null)
                        {
                            ntc.SetNumericValue(rowIndex[j - 1], time,
                                Convert.ToDouble(value));
                        }
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        private static string RemoveEncapsChars(string text)
        {
            int offset = 0;
            int length = text.Length;

            if (text.StartsWith("'"))
            {
                offset++;
                length--;
            }

            if (text.EndsWith("'"))
            {
                length--;
            }

            return text.Substring(offset, length);
        }
    }
}
