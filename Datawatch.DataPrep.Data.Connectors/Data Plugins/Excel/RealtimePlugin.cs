using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Panopticon.Dashboards;
using Panopticon.Dashboards.Data;
using Panopticon.Dashboards.Model;
using Panopticon.Dashboards.Plugin;
using Panopticon.Developer.Framework.Table;
using Panopticon.Developer.Licensing;
using Panopticon.Developer.Tables.RealTimeExcel;

using StreamingStatus = Panopticon.Dashboards.Plugin.StreamingStatus;
using PDStreamingStatus = Panopticon.Developer.Tables.RealTimeExcel.StreamingStatus;
using Panopticon.Dashboards.Data.AdoFilter;

namespace Panopticon.ExcelPlugin
{
    [LicenseProvider(typeof(PanopticonXmlLicenseProvider))]
    public class RealtimePlugin : PluginBase, IRealtimeDataPlugin
    {
        public event EventHandler<RealtimeDataPluginEventArgs> Updating;
        public event EventHandler<RealtimeDataPluginEventArgs> Updated;
        public event EventHandler<RealtimeStatusEventArgs> StreamingStatusChanged;

        private List<RealtimeExcelTable> startedTables = 
            new List<RealtimeExcelTable>();

        private bool disposed;

        public RealtimePlugin()
        {
            title = "Real-time Excel";    
        }

        protected override PanopticonXmlLicense CheckLicense()
        {
            PanopticonXmlLicense license = base.CheckLicense();
            if (license != null)
            {
                try
                {
                    // Also check for a license for the StandaloneTable
                    Type rteType = typeof(RealtimeExcelTable);
                    PanopticonXmlLicense rteLicense = null;

                    try
                    {
                        License generic = null;
                        if (!LicenseManager.IsValid(rteType, null, out generic)) 
                        {
                            license = null;
                        }
                    }
                    catch
                    {
                        license = null;
                    }
                    finally
                    {
                        if (rteLicense != null)
                        {
                            rteLicense.Dispose();
                        }
                    }
                }
                catch (Exception)
                {
                    license = null;
                }
            }
            return license;
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (RealtimeExcelTable table in startedTables)
                    {
                        table.Stop();
                        Detach(table);
                        table.Dispose();
                    }
                    startedTables.Clear();
                }
                disposed = true;
            }
        }

        private void Attach(RealtimeExcelTable table)
        {
            table.StreamingStatusChanged +=
                new EventHandler(RealtimeExcelTable_StreamingStatusChanged);
            table.Updating += new EventHandler(RealtimeExcelTable_Updating);
            table.Updated += new EventHandler(RealtimeExcelTable_Updated);
        }

        private void Detach(RealtimeExcelTable table)
        {
            table.StreamingStatusChanged -=
                new EventHandler(RealtimeExcelTable_StreamingStatusChanged);
        }

        public override PropertyBag[] DoConnect()
        {
            if (!IsLicensed)
            {
                throw Exceptions.PluginNotLicensed(GetType().ToString());
            }

            string path = BrowseForFile();
            if (path == null)
            {
                return null;
            }

            string[] ranges = RealtimeExcelTable.GetRanges(path);
            string[] titles = new string[ranges.Length];

            for (int i = 0; i < ranges.Length; i++)
            {
                titles[i] = GetTitle(null, ranges[i]);
            }

            TableSelectionWindow dlg = new TableSelectionWindow(titles);

            IWindowsHost windowsHost = pluginHost as IWindowsHost;
            Window owner = (windowsHost != null ? windowsHost.MainWindow : null);
            dlg.Owner = owner;

            if (dlg.ShowDialog().Value != true)
            {
                return null;
            }

            int selectedSheet = dlg.SelectedSheet;
            if (selectedSheet < 0)
            {
                return null; // Can this happen?
            }

            PropertyBag bag = CreatePropertyBag(path, ranges[selectedSheet]);
            return new PropertyBag[] { bag };
        }      

        public override ITable GetData(string workbookDir,  string dataDir, 
            PropertyBag settings, Dictionary<string,string> parameters, int rowcount)
        {

            string path = settings.Values[PathKey];
            string range = settings.Values[RangeKey];
            bool unpivot = Convert.ToBoolean(settings.Values[UnpivotKey]);


            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(workbookDir, path);
            }
            
            if (!File.Exists(path))
            {
                string dir = path.Substring(0,
                    path.LastIndexOf(Path.DirectorySeparatorChar));
                string fileName = Path.GetFileName(path); 
                string testPath = Path.Combine(dataDir, fileName);
                if (File.Exists(testPath))
                {
                    path = testPath;
                    settings.Values[PathKey] = path;
                }
            }

            try
            {
                RealtimeExcelTable reTable = new RealtimeExcelTable(path, range);
                ITable table;

                if ((parameters != null && parameters.Count > 0) || rowcount != -1)
                {
                    table = new AdoFilterTable(reTable, parameters, rowcount);
                }
                else
                {
                    table = reTable;
                }

                if (unpivot)
                {
                    string keyColumnName = "Id";
                    string numericColumnName = GetTitle(null, range);
                    table = TableTransformUtil.ExcelSourceTranform(table,
                        keyColumnName, numericColumnName);
                }
                return table;
            }
            catch (InvalidOperationException e)
            {
                string lowPath = path.ToLower();

                if (e.TargetSite.Name.Equals("CreateProviderError") &&
                    Path.GetExtension(lowPath).Equals(".xlsx"))
                {
                    throw Exceptions.NoOffice2007(e.Message, e.InnerException);
                }
                throw e;
            }
        }

        public static PropertyBag CreatePropertyBag(string path, string range)
        {
            PropertyBag settings = new PropertyBag();
            settings.Values[RealtimePlugin.PathKey] = path;
            settings.Values[RealtimePlugin.RangeKey] = range;
            return settings;
        }

        private void RealtimeExcelTable_StreamingStatusChanged(object sender, EventArgs e)
        {
            RealtimeExcelTable table = (RealtimeExcelTable)sender;
            if (StreamingStatusChanged != null)
            {
                StreamingStatus status;
                
                switch (table.StreamingStatus) 
                {
                    default:
                    case PDStreamingStatus.Waiting:
                        status = StreamingStatus.Waiting;
                        break;
                    case PDStreamingStatus.Streaming:
                        status = StreamingStatus.Streaming;
                        break;
                    case PDStreamingStatus.Stopped:
                        status = StreamingStatus.Stopped;
                        break;
                }

                StreamingStatusChanged(this, 
                    new RealtimeStatusEventArgs(table, status));
            }
        }

        private void RealtimeExcelTable_Updated(object sender, EventArgs e)
        {
            RealtimeExcelTable table = (RealtimeExcelTable)sender;
            table.EndUpdate();
            if (Updated != null)
            {
                Updated(this, new RealtimeDataPluginEventArgs((ITable)sender));
            }
        }

        private void RealtimeExcelTable_Updating(object sender, EventArgs e)
        {
            RealtimeExcelTable table = (RealtimeExcelTable)sender;
            table.BeginUpdate();
            if (Updating != null)
            {
                Updating(this, new RealtimeDataPluginEventArgs((ITable)sender));
            }
        }

        public void StartRealtimeUpdates(ITable table)
        {
            if (table is AdoFilterTable)
            {
                table = ((AdoFilterTable)table).SourceTable;
            }
            if (table is RealtimeExcelTable)
            {
                RealtimeExcelTable ret = (RealtimeExcelTable)table;
                Attach(ret);
                startedTables.Add(ret);
                ret.Start();
            }
        }

        public void StopRealtimeUpdates(ITable table)
        {
            if (table is AdoFilterTable)
            {
                table = ((AdoFilterTable)table).SourceTable;
            }
            if (table is RealtimeExcelTable)
            {
                RealtimeExcelTable ret = (RealtimeExcelTable)table;
                Detach(ret);
                startedTables.Remove(ret);
                ret.Stop();
            }
        }
    }
}
