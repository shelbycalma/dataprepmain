using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.ExcelPlugin.Properties;

namespace Panopticon.ExcelPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Static
    {
        private const string connectionTemplate =
            "Provider=Microsoft.Jet.OLEDB.4.0;" +
            "Mode=Read;" + 
            "Data Source={0};Extended Properties=" +
            "\"Excel 8.0;HDR=Yes;IMEX=1;\"";
        private const string office2007ConnectionTemplate =
            "Provider=Microsoft.ACE.OLEDB.12.0;" +
            "Mode=Read;" +
            "Data Source={0};Extended Properties=" +
            "\"Excel 12.0;HDR=YES\";";        

        public Static()
        {
        }

        public string[] GetRanges(string path)
        {
            string connectionString = GetConnectionString(path);
            if (connectionString == null)
            {
                return null;
            }

            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                return GetAllValidRanges(conn, path);
            }
        }

        internal static string GetConnectionString(string path)
        {
            if (path.EndsWith(".xls", StringComparison.CurrentCultureIgnoreCase))
            {
                return string.Format(connectionTemplate, path);
            }
            if (path.EndsWith(".xlsx", StringComparison.CurrentCultureIgnoreCase) || 
                path.EndsWith(".xlsm", StringComparison.InvariantCultureIgnoreCase))
            {
                return string.Format(office2007ConnectionTemplate, path);
            }
            return null;
        }

        public ITable GetData(string path, string range, 
            IEnumerable<ParameterValue> parameters, int rowcount)
        {
            string connectionString = GetConnectionString(path);
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                try
                {
                    return GetDataInternal(conn, range, parameters,
                        rowcount);
                }
                catch (InvalidOperationException e)
                {
                    string lowPath = path.ToLower();

                    if (e.TargetSite.Name.Equals("CreateProviderError") &&
                        ".xlsx".Equals(Path.GetExtension(lowPath)))
                    {
                        throw Exceptions.NoOffice2007(e.Message, e.InnerException);
                    }
                    throw;
                }
            }
        }

        private static ITable GetDataInternal(OleDbConnection conn,
            string range, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            string command;
            if (rowcount < 0)
            {
                command = string.Format("SELECT * FROM [{0}]", range);
            }
            else if (rowcount == 0)
            {
                command = string.Format("SELECT * FROM [{0}] where 1=0", range);
            }
            else
            {
                command = string.Format("SELECT TOP {0} * FROM [{1}]", 
                    rowcount, range);
            }
            Log.Info(Properties.Resources.LogGetDataInternal, 
                command, conn.ConnectionString);
            DateTime start = DateTime.Now;

            using (OleDbCommand oleDbCommand = new OleDbCommand(command, conn))
            using (OleDbDataReader reader = oleDbCommand.ExecuteReader())
            {
                if (reader == null) return EmptyTable.Instance;

                StandaloneTable table = new StandaloneTable();
                table.BeginUpdate();
                try
                {
                    bool[] numeric = new bool[reader.FieldCount];
                    bool[] time = new bool[reader.FieldCount];

                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        Type fieldType = reader.GetFieldType(i);
                        string name = fieldType != null ? fieldType.Name : "";
                        switch (name) 
                        {
                            case "Float":
                            case "Decimal":
                            case "Double":
                            case "Int32":
                            case "Int64":
                                numeric[i] = true;
                                table.AddColumn(new NumericColumn(reader.GetName(i)));
                                break;
                            case "DateTime":
                                time[i] = true;
                                table.AddColumn(new TimeColumn(reader.GetName(i)));
                                break;
                            default:
                                table.AddColumn(new TextColumn(reader.GetName(i)));
                                break;
                        }
                    }

                    if (!reader.HasRows)
                    {
                        return table;
                    }

                    int rownum = 0;
                    while (reader.Read() && (rowcount == -1 || rownum < rowcount))
                    {
                        if (!Include(reader, parameters))
                        {
                            continue;
                        }

                        Row row = table.AddRow();
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            if (reader.IsDBNull(i))
                            {
                                continue;
                            }

                            if (numeric[i])
                            {
                                double d = Convert.ToDouble(reader.GetValue(i));
                                ((NumericColumn) table.GetColumn(i)).SetNumericValue(row, d);
                            }
                            else if (time[i])
                            {
                                ((TimeColumn) table.GetColumn(i)).SetTimeValue(row, reader.GetDateTime(i));
                            }
                            else
                            {
                                ((TextColumn) table.GetColumn(i)).SetTextValue(row, Convert.ToString(reader.GetValue(i)));
                            }
                        }
                        rownum++;
                    }

                    table.Compact();
                }
                finally {
                    table.EndUpdate();
                }

                // NOTE: We don't set the table's CanFreeze property to true
                // here, that's done in the Plugin class (because even real-
                // time results can be frozen if they're unpivotted).

                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryCompleted,
                    table.RowCount, table.ColumnCount, seconds);
                return table;
            }
        }

        private static bool Include(OleDbDataReader reader,
            IEnumerable<ParameterValue> parameters)
        {
            if (parameters == null) return true;
            foreach (ParameterValue parameterValue in parameters)
            {
                // TODO parameter values from old workbooks doesnt work
                // they have been saved with single quotes, and should
                // (somewhere) be removed.
                // Parameters thats set (for example through the ButtonPart)
                // doesnt add single quotes and works fine.

                for (int i = 0; i < reader.FieldCount; i++)
                {
                    if (reader.GetName(i) != parameterValue.Name) continue;
                    object readerValue = reader.GetValue(i);
                    if (!ParameterHelper.IsEqual(
                        parameterValue.TypedValue, readerValue))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        internal string[] GetAllValidRanges(OleDbConnection conn, string path)
        {
            List<string> list = new List<string>();

            // TODO: if Microsoft ACE OLE DB 12.0 provider is missing
            // Then these code will not 
            using (DataTable schemaTable = conn.GetOleDbSchemaTable(
                OleDbSchemaGuid.Tables,
                new object[] { null, null, null, "TABLE" }))
            {
                if (schemaTable != null)
                {
                    foreach (DataRow row in schemaTable.Rows)
                    {
                        string name = (string)row["TABLE_NAME"];

                        if (name.StartsWith("xlMap")) // SPREADSHEET_MAPPER_PREFIX
                        {
                            continue;
                        }

                        if (!name.EndsWith("'") && (name.IndexOf("$'") != -1))
                        {
                            name = name.Replace("$'", "'$"); // Fixes a bug
                        }

                        try
                        {
                            GetDataInternal(conn, name, null, 3);
                            list.Add(name);
                        }
                        catch (Exception e)
                        {
                            Log.Error(Properties.Resources.ExErrorReadingRange,
                                name, Path.GetFileName(path), e.Message);
                        }
                    }
                }
            }
            
            return list.ToArray();
        }


        //private void HandleInvalidOperation(InvalidOperationException e, string path)
        //{
        //    // TODO: Create Hyperlinked message to PX driver pack
        //    string lowPath = path.ToLower();

        //    if (e.TargetSite.Name.Equals("CreateProviderError") &&
        //        Path.GetExtension(lowPath).Equals(".xlsx"))
        //    {
        //        throw Exceptions.NoOffice2007(e.Message, e.InnerException);
        //    }
        //    throw e;
        //}

    }
}
