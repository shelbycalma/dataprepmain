using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Tables.AdoFilter;
using Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using StreamingStatus = Datawatch.DataPrep.Data.Framework.Plugin.StreamingStatus;
using Utils = Datawatch.DataPrep.Data.Core.Utils;
using PDStreamingStatus = Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel.StreamingStatus;

namespace Panopticon.ExcelPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Realtime : IDisposable
    {
        private readonly Plugin plugin;

        private List<RealtimeExcelTable> startedTables = 
            new List<RealtimeExcelTable>();
        private bool disposed;

        public Realtime(Plugin plugin)
        {
            this.plugin = plugin;
        }

        ~Realtime()
        {
            Dispose(false);
        }

        private void CheckLicense()
        {
            if (!this.IsLicensed) {
                throw Datawatch.DataPrep.Data.Core.Connectors.Exceptions.PluginNotLicensed(GetType());
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    foreach (RealtimeExcelTable table in startedTables)
                    {
                        table.Stop();
                        Detach(table);
                        table.Dispose();
                    }
                    startedTables.Clear();
                }
                disposed = true;
            }
        }

        private void Attach(RealtimeExcelTable table)
        {
            table.StreamingStatusChanged +=
                RealtimeExcelTable_StreamingStatusChanged;
            table.Updating += RealtimeExcelTable_Updating;
            table.Updated += RealtimeExcelTable_Updated;
        }

        private void Detach(RealtimeExcelTable table)
        {
            table.StreamingStatusChanged -=
                RealtimeExcelTable_StreamingStatusChanged;
            table.Updating -= RealtimeExcelTable_Updating;
            table.Updated -= RealtimeExcelTable_Updated;
        }

        public string[] GetRanges(string path)
        {
            return RealtimeExcelTable.GetRanges(path);
        }      

        public ITable GetData(string path, string range,
            IEnumerable<ParameterValue> parameters, int rowcount)
        {
            CheckLicense();

            try
            {
                RealtimeExcelTable reTable = new RealtimeExcelTable(path, range);
                ITable table;

                if ((parameters != null && parameters.Count() > 0)
                    || rowcount != -1)
                {
                    table = new AdoFilterTable(reTable, parameters, rowcount);
                }
                else
                {
                    table = reTable;
                }
                return table;
            }
            catch (InvalidOperationException e)
            {
                string lowPath = path.ToLower();

                if (e.TargetSite.Name.Equals("CreateProviderError") &&
                    ".xlsx".Equals(Path.GetExtension(lowPath)))
                {
                    throw Exceptions.NoOffice2007(e.Message, e.InnerException);
                }
                throw;
            }
        }

        public bool IsLicensed
        {
            get
            {
                return LicenseCache.IsValid(GetType(), this);
            }
        }

        private void RealtimeExcelTable_StreamingStatusChanged(object sender, EventArgs e)
        {
            RealtimeExcelTable table = (RealtimeExcelTable)sender;
            StreamingStatus status;
            
            switch (table.StreamingStatus) 
            {
                default:
                case PDStreamingStatus.Waiting:
                    status = StreamingStatus.Waiting;
                    break;
                case PDStreamingStatus.Streaming:
                    status = StreamingStatus.Streaming;
                    break;
                case PDStreamingStatus.Stopped:
                    status = StreamingStatus.Stopped;
                    break;
            }
            plugin.FireStreamingStatusChanged(table, status);
        }

        private void RealtimeExcelTable_Updated(object sender, EventArgs e)
        {
            RealtimeExcelTable table = (RealtimeExcelTable)sender;
            table.EndUpdate();
            plugin.FireUpdated((ITable)sender);
        }

        private void RealtimeExcelTable_Updating(object sender, EventArgs e)
        {
            RealtimeExcelTable table = (RealtimeExcelTable)sender;
            table.BeginUpdate();
            plugin.FireUpdating((ITable)sender);
        }

        internal void StartRealtimeUpdates(ITable table)
        {
            table = Utils.GetNonDerivedSource(table);
            if (table is RealtimeExcelTable)
            {
                RealtimeExcelTable ret = (RealtimeExcelTable)table;
                if (startedTables.Contains(ret)) return;
                Attach(ret);
                startedTables.Add(ret);
                try
                {
                    ret.Start();
                }
                catch
                {
                }
            }
        }

        internal void StopRealtimeUpdates(ITable table)
        {
            table = Utils.GetNonDerivedSource(table);
            RealtimeExcelTable ret = table as RealtimeExcelTable;
            if (ret != null && startedTables.Contains(ret))
            {
                Detach(ret);
                startedTables.Remove(ret);
                try
                {
                    ret.Stop();
                }
                catch
                {
                }
            }
        }
    }
}
