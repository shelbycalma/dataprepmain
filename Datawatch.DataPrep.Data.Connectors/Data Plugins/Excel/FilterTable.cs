﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Panopticon.Developer.Framework.Table;
using Panopticon.Dashboards.Data;
using Panopticon.Developer.Tables.Ado;

namespace Panopticon.ExcelPlugin
{
    public class FilterTable : IMutableTable, IDisposable
    {
        public event MutableTableColumnEventHandler ColumnAdded;
        public event MutableTableColumnEventHandler ColumnRemoved;
        public event MutableTableRowEventHandler RowAdded;
        public event MutableTableRowEventHandler RowRemoved; 
        
        private ITable source;
        private Dictionary<string, string> parameters;
        private int rowcount;
        private List<FilterTableRow> rows = new List<FilterTableRow>();
        private Dictionary<IRow, FilterTableRow> rowLookup = new Dictionary<IRow,FilterTableRow>();
        private List<FilterTableColumn> columns = new List<FilterTableColumn>();
        private Dictionary<string, FilterTableColumn> columnLookup = 
            new Dictionary<string, FilterTableColumn>();
        private List<IColumn> parameterColumns = new List<IColumn>();

        private bool disposed;

        public FilterTable(ITable source, Dictionary<string, string> parameters, int rowcount)
        {
            this.source = source;
            this.parameters = parameters;
            this.rowcount = rowcount;
            UpdateColumns();
            UpdateParameterColumns();
            UpdateRows();
            Attach(source);
        }

        ~FilterTable()
        {
            Dispose(false);
        }

        protected virtual void Attach(ITable source)
        {
            if (source is IMutableTable)
            {
                IMutableTable mutableTable = (IMutableTable)source;
                mutableTable.RowAdded += 
                    new MutableTableRowEventHandler(Source_RowAdded);
                mutableTable.RowRemoved += 
                    new MutableTableRowEventHandler(Source_RowRemoved);
            }
        }

        protected virtual void Detach(ITable source)
        {
            if (source is IMutableTable)
            {
                IMutableTable mutableTable = (IMutableTable)source;
                mutableTable.RowAdded -=
                    new MutableTableRowEventHandler(Source_RowAdded);
                mutableTable.RowRemoved -=
                    new MutableTableRowEventHandler(Source_RowRemoved);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Detach(source);
                    foreach (IColumn col in parameterColumns)
                    {
                        DetachParameterColumn(col);
                    }
                    foreach (FilterTableColumn col in columns)
                    {
                        col.Dispose();
                    }
                    columns.Clear();
                    columnLookup.Clear();
                    rows.Clear();
                    rowLookup.Clear();
                    if (source is IDisposable)
                    {
                        ((IDisposable)source).Dispose();
                    }
                }
                disposed = true;
            }
        }

        private void AttachParameterColumn(IColumn col)
        {
            if (col is IRealtimeColumn)
            {
                ((IRealtimeColumn)col).ValueChanged += 
                    new RealtimeColumnEventHandler(ParameterColumn_ValueChanged);
            }
        }

        private void DetachParameterColumn(IColumn col)
        {
            if (col is IRealtimeColumn)
            {
                ((IRealtimeColumn)col).ValueChanged +=
                    new RealtimeColumnEventHandler(ParameterColumn_ValueChanged);
            }
        }

        public int ColumnCount
        {
            get { return columns.Count; }
        }

        public IColumn GetColumn(int index)
        {
            return columns[index];
        }

        public IRow GetRow(int index)
        {
            return rows[index];
        }

        protected bool Include(IRow row)
        {
            if (parameterColumns.Count > 0)
            {
                foreach (IColumn col in parameterColumns)
                {
                    object obj = row.GetValue(col);
                    string value = obj != null ? obj.ToString() : "";
                    if (parameters[col.Name].IndexOf("'" + value + "'") == -1)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public IColumn KeyColumn
        {
            get { return source.KeyColumn; }
        }

        public Panopticon.Developer.Licensing.PanopticonXmlLicense License
        {
            get { return source.License; }
        }

        public ITableMetaData MetaData
        {
            get { return null; }
        }

        protected void OnColumnAdded(MutableTableColumnEventArgs e)
        {
            if (ColumnAdded != null)
            {
                ColumnAdded(this, e);
            }
        }

        protected void OnColumnRemoved(MutableTableColumnEventArgs e)
        {
            if (ColumnRemoved != null)
            {
                ColumnRemoved(this, e);
            }
        }

        private void ParameterColumn_ValueChanged(object sender, RealtimeColumnEventArgs e)
        {
            UpdateRows();
        }

        public int RowCount
        {
            get { return rows.Count; }
        }

        void Source_RowRemoved(object sender, MutableTableRowEventArgs e)
        {
            UpdateRows();
        }

        void Source_RowAdded(object sender, MutableTableRowEventArgs e)
        {
            UpdateRows();
        }

        public ITable SourceTable
        {
            get { return source; }
        }

        private void UpdateColumns()
        {
            for (int i = 0; i < source.ColumnCount; i++)
            {
                IColumn col = source.GetColumn(i);
                FilterTableColumn wrapped = null;

                if (col is INumericColumn)
                {
                    wrapped = new FilterTableNumericColumn(this, (INumericColumn)col);
                }
                else if (col is ITextColumn)
                {
                    wrapped = new FilterTableTextColumn(this, (ITextColumn)col);
                }
                else if (col is ITimeColumn)
                {
                    wrapped = new FilterTableTimeColumn(this, (ITimeColumn)col);
                }
                columns.Add(wrapped);
                columnLookup[wrapped.Name] = wrapped;
            }
        }

        private void UpdateParameterColumns()
        {            
            foreach (string name in parameters.Keys)
            {
                if (columnLookup.ContainsKey(name))
                {
                    IColumn col = columnLookup[name].Source;
                    AttachParameterColumn(col);
                    parameterColumns.Add(col);
                }
            }
        }

        private void UpdateRows()
        {
            if (rows.Count > 0)
            {
                rows.Clear();
                rowLookup.Clear();

                if (RowRemoved != null)
                {
                    RowRemoved(this, MutableTableRowEventArgs.Empty);
                }
            }

            for (int i = 0; i < source.RowCount; i++)
            {
                IRow row = source.GetRow(i);
                if (Include(row))
                {
                    FilterTableRow r = new FilterTableRow(this, row);
                    rows.Add(r);
                    rowLookup.Add(row, r);
                }
                if (rowcount != -1 && rows.Count == rowcount)
                {
                    break;
                }
            }

            if (rows.Count > 0)
            {
                if (RowAdded != null)
                {
                    RowAdded(this, MutableTableRowEventArgs.Empty);
                }
            }
        }

        private class FilterTableColumn : IRealtimeColumn, IColumnMetaData,
            IMutableColumnTitle, IMutableHiddenColumn,
            IDisposable
        {
            public event RealtimeColumnEventHandler ValueChanged;

            protected FilterTable table;
            private IColumn source;
            private bool disposed;

            internal FilterTableColumn(FilterTable table, IColumn source)
            {
                this.table = table;
                this.source = source;
                Attach(source);
            }

            public void Dispose()
            {
                Dispose(true);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!disposed)
                {
                    if (disposing)
                    {
                        Detach(source);           
                    }
                    disposed = true;
                }
            }

            protected virtual void Attach(IColumn source)
            {
                if (source is IRealtimeColumn)
                {
                    ((IRealtimeColumn)source).ValueChanged += 
                        new RealtimeColumnEventHandler(Source_ValueChanged);
                }
            }

            protected virtual void Detach(IColumn source)
            {
                if (source is IRealtimeColumn)
                {
                    ((IRealtimeColumn)source).ValueChanged -=
                        new RealtimeColumnEventHandler(Source_ValueChanged);
                }
            }

            public string ColumnEntity
            {
                get { return source.MetaData != null ? source.MetaData.ColumnEntity : null; }
            }

            public Type DataType
            {
                get { return source.MetaData != null ? source.MetaData.DataType : null; }
            }

            public bool Hidden
            {
                get { return source.MetaData != null ? source.MetaData.Hidden : false; }
            }

            public IColumnMetaData MetaData
            {
                get { return this; }
            }

            public string Name
            {
                get { return source.Name; }
            }

            internal IColumn Source
            {
                get { return source; }
            }

            private void Source_ValueChanged(object sender, RealtimeColumnEventArgs e)
            {
                if (ValueChanged != null)
                {
                    RealtimeColumnEventArgs eventArgs;
                    if (e == RealtimeColumnEventArgs.Empty)
                    {
                        eventArgs = RealtimeColumnEventArgs.Empty;
                    }
                    else
                    {
                        FilterTableRow row = table.rowLookup[e.Row];
                        object before = e.ValueBefore;
                        eventArgs = new RealtimeColumnEventArgs(row, before);
                    }
                    ValueChanged(this, e);
                }
            }

            public ITable Table
            {
                get { return table; }
            }

            public string Title
            {
                get { return source.MetaData != null ? source.MetaData.Title : null; }
            }

            string IMutableColumnTitle.Title
            {
                get
                {
                    return Title;
                }
                set
                {
                    if (source is AdoColumn)
                    {
                        ((AdoColumn) source).Title = value;
                    }
                }
            }

            bool IMutableHiddenColumn.Hidden
            {
                get
                {
                    return Hidden;
                }
                set
                {
                    if (source is AdoColumn)
                    {
                        ((AdoColumn) source).Hidden = value;
                    }
                }
            }
        }

        private class FilterTableNumericColumn : FilterTableColumn, 
            INumericRealtimeColumn, INumericColumnMetaData, 
            IMutableFormatString
        {
            public event NumericRealtimeColumnEventHandler NumericValueChanged;

            private INumericColumn numericColumn;

            internal FilterTableNumericColumn(FilterTable table, INumericColumn col) : base(table, col)
            {
                numericColumn = col;
            }

            protected override void Attach(IColumn source)
            {
                base.Attach(source);
                if (source is INumericRealtimeColumn)
                {
                    ((INumericRealtimeColumn)source).NumericValueChanged += Source_NumericValueChanged;
                }
            }

            void Source_NumericValueChanged(object sender, NumericRealtimeColumnEventArgs e)
            {
                if (NumericValueChanged == null)
                {
                    return;
                }

                NumericRealtimeColumnEventArgs eventArgs;
                if (e == NumericRealtimeColumnEventArgs.Empty)
                {
                    eventArgs = NumericRealtimeColumnEventArgs.Empty;
                }
                else
                {
                    FilterTableRow row = table.rowLookup[e.Row];
                    double before = e.NumericValueBefore;
                    eventArgs = new NumericRealtimeColumnEventArgs(row, before);
                }
                NumericValueChanged(this, eventArgs);
            }

            protected override void Detach(IColumn source)
            {
                if (source is INumericRealtimeColumn)
                {
                    ((INumericRealtimeColumn)source).NumericValueChanged -= Source_NumericValueChanged;
                }
                base.Detach(source);
            }

            public double GetNumericValue(IRow row)
            {
                return numericColumn.GetNumericValue(((FilterTableRow)row).Source);
            }

            public NumericInterval Domain
            {
                get 
                { 
                    if (numericColumn.MetaData is INumericColumnMetaData)
                    {
                        return ((INumericColumnMetaData) numericColumn).Domain;
                    }
                    return null;
                }
            }

            public string Format
            {
                get 
                { 
                    if (numericColumn.MetaData is INumericColumnMetaData)
                    {
                        return ((INumericColumnMetaData) numericColumn).Format;
                    }
                    return null;
                }
            }

            public double Mean
            {
                get 
                { 
                    if (numericColumn.MetaData is INumericColumnMetaData)
                    {
                        return ((INumericColumnMetaData) numericColumn).Mean;
                    }
                    return NumericValue.Empty;
                }
            }

            public double StandardDeviation
            {
                get 
                { 
                    if (numericColumn.MetaData is INumericColumnMetaData)
                    {
                        return ((INumericColumnMetaData) numericColumn).StandardDeviation;
                    }
                    return NumericValue.Empty;
                }
            }

            string IMutableFormatString.Format
            {
                get
                {
                    return Format;
                }
                set
                {
                    if (numericColumn is AdoNumericColumn)
                    {
                        ((AdoNumericColumn) numericColumn).MetaFormat = value;
                    }
                }
            }
        }

        private class FilterTableTextColumn : FilterTableColumn, ITextColumn,
            ITextRealtimeColumn
        {
            public event TextRealtimeColumnEventHandler TextValueChanged;

            private ITextColumn textColumn;

            internal FilterTableTextColumn(FilterTable table, ITextColumn col)
                : base(table, col)
            {
                textColumn = col;   
            }

            public string GetTextValue(IRow row)
            {
                return textColumn.GetTextValue(((FilterTableRow)row).Source);   
            }

            protected override void Attach(IColumn source)
            {
                base.Attach(source);
                if (source is ITextRealtimeColumn)
                {
                    ((ITextRealtimeColumn)source).TextValueChanged += Source_TextValueChanged;
                }
            }

            void Source_TextValueChanged(object sender, TextRealtimeColumnEventArgs e)
            {
                if (TextValueChanged == null)
                {
                    return;
                }

                TextRealtimeColumnEventArgs eventArgs;
                if (e == TextRealtimeColumnEventArgs.Empty)
                {
                    eventArgs = TextRealtimeColumnEventArgs.Empty;
                }
                else
                {
                    FilterTableRow row = table.rowLookup[e.Row];
                    string before = e.TextValueBefore;
                    eventArgs = new TextRealtimeColumnEventArgs(row, before);
                }
                TextValueChanged(this, eventArgs);
            }

            protected override void Detach(IColumn source)
            {
                if (source is ITextRealtimeColumn)
                {
                    ((ITextRealtimeColumn)source).TextValueChanged -= Source_TextValueChanged;
                }
                base.Detach(source);
            }
        }

        private class FilterTableTimeColumn : FilterTableColumn, 
            ITimeColumn, ITimeColumnMetaData, ITimeRealtimeColumn
        {
            public event TimeRealtimeColumnEventHandler TimeValueChanged;

            private ITimeColumn timeColumn;

            internal FilterTableTimeColumn(FilterTable table, ITimeColumn col)
                : base(table, col)
            {
                timeColumn = col;   
            }

            public DateTime GetTimeValue(IRow row)
            {
                return timeColumn.GetTimeValue(((FilterTableRow)row).Source);
            }

            public TimeInterval Domain
            {
                get 
                { 
                    if (timeColumn.MetaData is ITimeColumnMetaData)
                    {
                        return ((ITimeColumnMetaData) timeColumn).Domain;
                    }
                    return null; 
                }            
            }

            public string Format
            {
                get 
                { 
                    if (timeColumn.MetaData is ITimeColumnMetaData)
                    {
                        return ((ITimeColumnMetaData) timeColumn).Format;
                    }
                    return null; 
                }            
            }

            protected override void Attach(IColumn source)
            {
                base.Attach(source);
                if (source is ITimeRealtimeColumn)
                {
                    ((ITimeRealtimeColumn)source).TimeValueChanged += Source_TimeValueChanged;
                }
            }

            void Source_TimeValueChanged(object sender, TimeRealtimeColumnEventArgs e)
            {
                if (TimeValueChanged == null)
                {
                    return;
                }

                TimeRealtimeColumnEventArgs eventArgs;
                if (e == TimeRealtimeColumnEventArgs.Empty)
                {
                    eventArgs = TimeRealtimeColumnEventArgs.Empty;
                }
                else
                {
                    FilterTableRow row = table.rowLookup[e.Row];
                    DateTime before = e.TimeValueBefore;
                    eventArgs = new TimeRealtimeColumnEventArgs(row, before);
                }
                TimeValueChanged(this, eventArgs);
            }

            protected override void Detach(IColumn source)
            {
                if (source is ITimeRealtimeColumn)
                {
                    ((ITimeRealtimeColumn)source).TimeValueChanged -= Source_TimeValueChanged;
                }
                base.Detach(source);
            }
        }


        private class FilterTableRow : IRow
        {
            private FilterTable table;
            private IRow source;

            internal FilterTableRow(FilterTable table, IRow source)
            {
                this.table = table;
                this.source = source;
            }

            public object GetValue(IColumn column)
            {
                return source.GetValue(((FilterTableColumn)column).Source);
            }

            internal IRow Source
            {
                get { return source; }
            }

            public ITable Table
            {
                get { return table; }
            }
        }
    }
}
