﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Panopticon.ExcelPlugin.Properties;

namespace Panopticon.ExcelPlugin
{
    public class Exceptions : Datawatch.DataPrep.Data.Framework.Exceptions
    {
        public static Exception NoOffice2007(string message, Exception inner)
        {
            return CreateNotSupported(
                Format(Resources.ExNoOffice2007, message),
                inner);
        }
    }
}
