using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using org.apache.qpid.transport;

namespace Panopticon.QpidPlugin
{
    /// <summary>
    /// The "Topic" property here is used as what in the Qpid documentation is
    /// refered to as "destination".
    /// </summary>
    public class QpidSettings : MessageQueueSettingsBase
    {
        public QpidSettings(IPluginManager pluginManager)
            : this(pluginManager, null)
        {
        }

        public QpidSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
        }

        public QpidSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        public override bool Equals(object obj)
        {
            if (!(obj is QpidSettings)) return false;

            if (!base.Equals(obj)) return false;

            QpidSettings o = (QpidSettings)obj;
            if (RoutingKey != o.RoutingKey) return false;
            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();
            if (RoutingKey != null) hashCode ^= RoutingKey.GetHashCode();
            return hashCode;
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag bag = base.ToPropertyBag();
            bag.Values["RoutingKey"] = RoutingKey;
            return bag;
        }

        protected override string DefaultParserPluginId
        {
            get { return "Xml"; }
        }

        public override bool IsOk
        {
            get
            {
                return base.IsOk &&
                    !string.IsNullOrEmpty(RoutingKey);
            }
        }

        /// <summary>
        /// The Exchange property is just an alias for Topic to be more in line
        /// with Qpid terminology.
        /// </summary>
        public string Exchange
        {
            get { return Topic; }
            set
            {
                if (value != Topic)
                {
                    // Note that this might very well fire two PropertyChanged
                    // events (as it should).
                    // TODO but we could probably make this clearer once we
                    // make the *MQ systems share more code.
                    Topic = value;
                    FirePropertyChanged("Exchange");
                }
            }
        }

        public string RoutingKey
        {
            get { return GetInternal("RoutingKey"); }
            set { SetInternal("RoutingKey", value); }
        }
        
        public override void DoGenerateColumns()
        {
            StreamingColumnGenerator columnGenerator = 
                new StreamingColumnGenerator(this, this.errorReporter);

            // Set up connection to Qpid
            Listener listener = new Listener(columnGenerator);

            try
            {
                // Verify settings
                HostUriParser hostParser =
                    new HostUriParser(5672, Broker);
                string brokerHost = hostParser.Host;
                int brokerPort = hostParser.Port;
                if (brokerHost == null)
                {
                    throw Exceptions.MalformedBrokerUrl(Broker);
                }

                string username = UserName ?? "";
                string password = Password ?? "";

                // TODO do we need to be able to set "virtual host"?
                try
                {
                    listener.Connect(brokerHost, brokerPort, username,
                        password);
                }
                catch (TransportException ex)
                {
                    throw Exceptions.ConnectionError(ex);
                }

                //// Handle parameterized connection
                string exchange = DataUtils.ApplyParameters(Exchange,
                    Parameters);
                string routingKey = DataUtils.ApplyParameters(RoutingKey,
                    Parameters);

                // Route messages to the new queue if they match the routing
                // key.
                listener.ListenTo(exchange, routingKey);

                // Start receiving data to generate columns
                columnGenerator.Generate();

                listener.Disconnect();

            }
            catch (Exception ex)
            {
                // Exceptions are caught and logged in
                // RealtimeDataPlugin.StartRealtimeUpdates() so just rethrow.
                this.errorReporter.Report(
                    Properties.Resources.ExError,
                    Properties.Resources.ExFailedColumnGeneration +
                    "\nException: " + ex.Message);
            }
        }

        public override bool CanGenerateColumns()
        {
            return !string.IsNullOrEmpty(Topic) && !string.IsNullOrEmpty(Broker);
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }
    }
}
