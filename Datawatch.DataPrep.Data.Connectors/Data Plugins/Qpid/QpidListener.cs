﻿using System;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using org.apache.qpid.client;
using org.apache.qpid.transport;
using Panopticon.QpidPlugin.Properties;

namespace Panopticon.QpidPlugin
{
    internal class Agent
    {
        protected IClientSession session;
        private Client connection;

        public virtual void Connect(string host, int port, string username,
            string password)
        {
            connection = new Client();

            // Username, password and virtual host are not used
            connection.Connect(host, port, "", username, password);
            session = connection.CreateSession(5000);

            Log.Info(string.Format(Resources.LogConnected, host, port));
        }

        public virtual void Disconnect()
        {
            if (session != null)
            {
                try
                {
                    session.Close();
                }
                catch (SessionException ex)
                {
                    Log.Warning(string.Format(
                        Resources.LogClosingSessionExchangeError, ex.Message));
                }
                session = null;
            }
            if (connection != null)
            {
                try
                {
                    connection.Close();
                    connection = null;
                }
                catch (NullReferenceException ex)
                {
                    // Got this exception from within Close, I have no idea what
                    // caused it or how to prevent it so I just log it.
                    Log.Warning(string.Format(
                        Resources.LogClosingConnectionBrokerError, ex.Message));
                }
                catch (ArgumentNullException ex)
                {
                    // Change from .NET 3.5 to 4.0 - "A delegate now throws an
                    // ArgumentNullException instead of a NullReferenceException
                    // when a null value is passed to the delegate's
                    // constructor." - so added this extra catch just in case
                    // it was because of faulty delefate construction.
                    Log.Warning(string.Format(Resources.LogClosingConnectionError,
                        ex.Message));
                }
            }

            Log.Info(Resources.LogDisconnected);
        }
    }

    internal class Listener : Agent, IMessageListener
    {
        private string queue;
        private readonly IMessageHandler messageReceiver;

        public Listener(IMessageHandler messageReceiver)
        {
            if (messageReceiver == null)
            {
                throw Exceptions.DataAdaptorWasNull();
            }
            this.messageReceiver = messageReceiver;
        }

        public void ListenTo(string exchange, string routingKey)
        {
            // NOTE If a queue with the given name already exists the bind will
            // silently fail and no messages will be received.
            // An error message will be printed in the broker output but I
            // havent found a way to detect it here (the workaround would be to
            // create a new queue name).
            //
            // This most probably indicate that another plugin didnt shut down
            // properly. If thats the case, the broker needs to be restarted.
            session.ExchangeBind(queue, exchange, routingKey);

            Log.Info(string.Format(Resources.LogExchangeBind, exchange,
                routingKey));
            return;
        }

        public override void Connect(string host, int port, string username,
            string password)
        {
            base.Connect(host, port, username, password);

            // Create the queue for received messages
            queue = GenerateQueueId();
            session.QueueDeclare(queue, Option.EXCLUSIVE,
                Option.AUTO_DELETE);

            Log.Info(string.Format(Resources.LogQueueDeclare, queue));

            // Attach ourselves as a listener
            session.AttachMessageListener(this, queue);
            session.MessageSubscribe(queue);
        }

        public override void Disconnect()
        {
            if (session != null)
            {
                try
                {
                    session.QueuePurge(queue);
                }
                catch (SessionException)
                {
                    // Could happen if queue is empty
                }
                try
                {
                    session.QueueDelete(queue);
                }
                catch (SessionException)
                {
                    // Happens if if the queue havent been declared,
                    // but this should never happen
                }
            }

            base.Disconnect();
        }

        private static string GenerateQueueId()
        {
            // Create a unique queue name for this consumer
            return Guid.NewGuid().ToString();
        }

        private static string DecodeMessage(IMessage message)
        {
            MemoryStream body = message.Body;
            BinaryReader reader = new BinaryReader(body, Encoding.UTF8);
            byte[] bodyBytes = new byte[body.Length - body.Position];
            reader.Read(bodyBytes, 0, (int)body.Length);
            ASCIIEncoding enc = new ASCIIEncoding();
            string decodedMessage = enc.GetString(bodyBytes);
            return decodedMessage;
        }

        #region IMessageListener Members

        public void MessageTransfer(IMessage message)
        {
            // Notify system of message
            messageReceiver.MessageReceived(DecodeMessage(message));
        }

        #endregion
    }
}
