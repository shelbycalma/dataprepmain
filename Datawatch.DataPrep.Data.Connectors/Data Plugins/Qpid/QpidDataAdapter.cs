﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using org.apache.qpid.transport;

//---------------------------------------------//
// Some instructions to get started with Qpid  //
//---------------------------------------------//
//
// Download and install the Qpid broker:
// 32-bit: http://www.riverace.com/qpid/qpidc-0.6-x86.msi
// 64-bit: http://www.riverace.com/qpid/qpidc-0.6-x64.msi
//
// Navigate to the installed folder and start the broker (if you’re on a 32bit
// windows you probably need to tweak the path), note that it’s important to
// turn off authentication (I haven’t figured that part out yet):
//
//      cd "C:\Program Files (x86)\Apache\qpidc-0.6\bin" && qpidd --auth no
//
// With the broker running we need a client to publish data. I haven’t been able
// to find any examples for Qpid so I wrote my own. Unzip the attached publisher
// somewhere, navigate to the folder and (with the broker running) double click
// QpidPublisher.exe. 
//
// Now with both broker and publisher running we can start Datawatch Designer.
// The connection settings dialog for Qpid is the same as for ActiveMQ.
// If you’ve looked at the QpidPublisher console window (press any key to shut
// it down) you can see it sends xml data structured like so:
//
//      <transaction stock="EBAY" price="48" volume="163" />
//
// So set up the XPath expressions like with ActiveMQ:
//
//  Id	Name	XPath	            Type
//      Price	transaction/@price	Numeric
//      Volume  transaction/@volume	Numeric
//  X	Stock	transaction/@stock	Text
//
// The other settings for the Qpid data source is as follows:
//
//      Broker:       localhost:5672
//      Exchange:     amq.topic
//      Routing Key:  routing_key
//
// Note that this time around we have the two options, exchange and routing key,
// instead of topic. Heres a quick rundown of how Qpid handles the topic
// concept (safe to skip):
//
//    An exchange and a routing key together form what is the closest
//    thing that Qpid has to a topic/channel. Every message transmitted
//    specify those, the message is sent through an exchange with a
//    routing key. Each listening client then sets up a (unique) queue
//    and through an ExchangeBind request to the broker makes every
//    message on a given exchange, specifying a given routing key,
//    end up in the listeners queue and will eventually be parsed.
//
// And that’s it! Use the columns in a visualization and enter presentation
// mode. 
//
// Just one more thing. I put the source code for the (attached) publisher along
// with the listener I made when experimenting with Qpid in a mercurial
// repository. It’s on Robot:
//
//      \\robot\Repositories\QpidExamples

namespace Panopticon.QpidPlugin
{
    public class QpidDataAdapter : MessageQueueAdapterBase<ParameterTable,
        QpidSettings, Dictionary<string, object>>
    {
        private Listener listener;

        public override void Start()
        {
            listener = new Listener(this);
            try
            {
                settings.Parameters = table.Parameters;
                settings.ParserSettings.Parameters = table.Parameters;
                base.Start();

                // Verify settings
                HostUriParser hostParser =
                    new HostUriParser(5672, settings.Broker);
                string brokerHost = hostParser.Host;
                int brokerPort = hostParser.Port;
                if (brokerHost == null)
                {
                    throw Exceptions.MalformedBrokerUrl(settings.Broker);
                }

                string username = settings.UserName ?? "";
                string password = settings.Password ?? "";

                // TODO do we need to be able to set "virtual host"?
                try
                {
                    listener.Connect(brokerHost, brokerPort, username,
                        password);
                }
                catch (TransportException ex)
                {
                    throw Exceptions.ConnectionError(ex);
                }

                // Handle parameterized connection
                string exchange = DataUtils.ApplyParameters(settings.Exchange,
                    table.Parameters);
                string routingKey = DataUtils.ApplyParameters(settings.RoutingKey,
                    table.Parameters);

                // Route messages to the new queue if they match the routing
                // key.
                listener.ListenTo(exchange, routingKey);

                // Create a listener and subscribe it to the queue.
            }
            catch (Exception ex)
            {
                // Exceptions are caught and logged in
                // RealtimeDataPlugin.StartRealtimeUpdates() so just rethrow.
                throw ex;
            }
        }

        public override void Stop()
        {
            if (listener != null)
            {
                listener.Disconnect();
                listener = null;
            }
            base.Stop();
        }
    }
}
