﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;

using StreamBase.SB;
using StreamBase.SB.Client;
using Tuple = StreamBase.SB.Tuple;

namespace Panopticon.StreamBasePlugin
{
    public class StreamBaseDataAdapter : RealtimeDataAdapter<FieldTreeTable,
        StreamBaseSettings, Tuple>
    {
        private volatile Thread dequeueThread = null;

        //StreamBase outputs time in UTC, so adjust offset based on
        //selected timezone.
        private TimeZoneInfo timeZoneInfo = null;

        internal static string ApplyParameters(string query,
            IEnumerable<ParameterValue> parameters,
            bool encloseParameterInQuote)
        {
            if (parameters == null) return query;
            return new ParameterEncoder
            {
                SourceString = query,
                Parameters = parameters,
                EnforceSingleQuoteEnclosure = encloseParameterInQuote,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }

        public override void Start()
        {
            dequeueThread = new Thread(ConnectAndSubscribe);
            dequeueThread.Name = "SB Dequeue Stream";
            dequeueThread.Start();
        }

        public override void Stop()
        {
            dequeueThread = null;
        }

        private void ConnectAndSubscribe()
        {
            StreamBaseClient client = null;
            StreamProperties streamProperties = null;
            try
            {
                client = StreamBaseClientUtil.GetStreamBaseClient(settings, 
                    table.Parameters);

                string predicate = null;
                if (!string.IsNullOrEmpty(settings.Predicate))
                {
                    predicate = ApplyParameters(
                        settings.Predicate, table.Parameters,
                        settings.EncloseParameterInQuote);
                }

                if (!string.IsNullOrEmpty(predicate))
                {
                    // the sLogicalStream must be unique for each client stream, 
                    // therefore we add the current clock ticks to the stream name.
                    string logicalName = settings.Stream + DateTime.Now.Ticks;
                    streamProperties = client.Subscribe(settings.Stream,
                        logicalName, predicate);
                }
                else
                {
                    streamProperties = client.Subscribe(settings.Stream);
                }

                if (!"null".Equals(settings.TimeZone))
                {
                    timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(
                        settings.TimeZone);
                }
                else
                {
                    timeZoneInfo = null;
                }

                DequeueStream(client);
            }
            catch (StreamBaseException sbExc)
            {
                // Problems starting realtime, try connect again.
                Log.Exception(sbExc);
                updater.ConnectionLost(true);
            }
            catch(Exception exc)
            {
                Log.Exception(exc);
            }
            finally
            {
                if (client != null)
                {
                    try
                    {
                        if (streamProperties != null)
                        {
                            client.Unsubscribe(streamProperties);
                        }
                        client.Close();
                    }
                    catch(StreamBaseException exc)
                    {
                        Log.Exception(exc);
                    }
                }
            }
        }

        private void DequeueStream(StreamBaseClient client)
        {
            Thread currentThread = Thread.CurrentThread;
            while (currentThread == dequeueThread)
            {
                // 1 second timeout, will result in DequeueResult.TIMEOUT.
                DequeueResult dr = client.Dequeue(1000);

                // Only process tuples where
                // the leadership status is set to LEADER.
                // LeadershipStatus.NON_LEADER means it's a fallback server.
                LeadershipStatus leaderstatus = dr.CurrentLeadershipStatus;
                if (leaderstatus != LeadershipStatus.LEADER)
                {
                    continue;
                }

                // And status has to be GOOD!
                int status = dr.GetStatus();
                if (status != DequeueResult.GOOD)
                {
                    if (status == DequeueResult.TIMEOUT)
                    {
                        continue;
                    }
                    if (status == DequeueResult.CLOSED)
                    {
                        if (dequeueThread == currentThread)
                        {
                            updater.ConnectionLost(true);
                        }
                        return;
                    }
                }

                foreach (Tuple tuple in dr)
                {
                    if (dequeueThread != currentThread)
                    {
                        return;
                    }

                    Tuple actualTuple = tuple;
                    foreach (FieldTreeNode parent in table.IdPath)
                    {
                        actualTuple = actualTuple.GetTuple(parent.Field);
                    }
            
                    string id = GetIdStringRecursive(actualTuple, table.IdNode);
                    if (id != null)
                    {
                        updater.EnqueueUpdate(id, tuple);
                    }
                }
            }
        }

        public override void UpdateColumns(Row row, Tuple tuple)
        {
            foreach(FieldTreeNode node in table.Root.Children)
            {
                ReadAndAssignFieldRecursive(tuple, node, row, null);
            }
        }

        public override DateTime GetTimestamp(Tuple tuple)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return DateTime.MinValue;
            }

            Tuple actualTuple = tuple;
            foreach (FieldTreeNode parent in table.TimeIdPath)
            {
                actualTuple = actualTuple.GetTuple(parent.Field);
            }
            
            if (actualTuple.IsNull(table.TimeIdNode.Field))
            {
                return DateTime.MinValue;
            }

            Timestamp value = actualTuple.GetTimestamp(table.TimeIdNode.Field);
            DateTime dateTime = timeZoneInfo != null ?
                    TimeZoneInfo.ConvertTimeFromUtc(value.ToDateTime(), timeZoneInfo) :
                    value.ToDateTime();
            return dateTime;
        }

        private string GetIdStringRecursive(Tuple tuple, FieldTreeNode node)
        {
            if (tuple.IsNull(node.Field))
            {
                return null;
            }

            DataType dataType = node.Field.DataType;

            if (dataType == DataType.STRING)
            {
                string textValue = tuple.GetString(node.Field);
                return textValue;
            }
            if (dataType == DataType.INT)
            {
                int value = tuple.GetInt(node.Field);
                double doubleValue = (double) value;
                return doubleValue.ToString();
            }
            if (dataType == DataType.LONG)
            {
                long value = tuple.GetLong(node.Field);
                double doubleValue = (double) value;
                return doubleValue.ToString();
            }
            if (dataType == DataType.BOOL)
            {
                bool value = tuple.GetBoolean(node.Field);
                string textValue = value.ToString();
                return textValue;
            }
            if (dataType == DataType.DOUBLE)
            { 
                double doubleValue = tuple.GetDouble(node.Field);
                return doubleValue.ToString();
            }
            if (dataType == DataType.TIMESTAMP)
            {
                Timestamp value = tuple.GetTimestamp(node.Field);
                DateTime dateTimeValue = timeZoneInfo != null ?
                    TimeZoneInfo.ConvertTimeFromUtc(value.ToDateTime(), timeZoneInfo) :
                    value.ToDateTime();
                return dateTimeValue.ToString();
            }

            if (dataType != DataType.TUPLE)
            {
                throw Exceptions.UnsupportedDataType(dataType);
            }

            StringBuilder tupleBuilder = new StringBuilder(256);
            Tuple childTuple = tuple.GetTuple(node.Field);
            foreach (FieldTreeNode child in node.Children)
            {
                if (tupleBuilder.Length > 0)
                {
                    tupleBuilder.Append(",");
                }
                if (child.Field.DataType == DataType.TUPLE)
                {
                    tupleBuilder.Append("(");
                }
                string s = GetIdStringRecursive(childTuple, child);
                tupleBuilder.Append(s);
                if (child.Field.DataType == DataType.TUPLE)
                {
                    tupleBuilder.Append(")");
                }
            }
            string tupleString = tupleBuilder.ToString();
            return tupleString;
        }

        private void ReadAndAssignFieldRecursive(Tuple tuple,
            FieldTreeNode node, Row row, StringBuilder parentBuilder)
        {
            if (tuple.IsNull(node.Field))
            {
                node.Column.SetValue(row, null);
                
                // StreamBase uses "null" in it's ToString() implementation.
                if (parentBuilder != null)
                {
                    parentBuilder.Append("null");
                }
                return;
            }

            DataType dataType = node.Field.DataType;

            if (dataType == DataType.DOUBLE)
            { 
                double doubleValue = tuple.GetDouble(node.Field);
                ((NumericColumn) node.Column).SetNumericValue(row, doubleValue);
                if (parentBuilder != null)
                {
                    parentBuilder.Append(doubleValue.ToString());
                }
                return;
            }
            if (dataType == DataType.INT)
            {
                int value = tuple.GetInt(node.Field);
                double doubleValue = (double) value;
                ((NumericColumn) node.Column).SetNumericValue(row, doubleValue);
                if (parentBuilder != null)
                {
                    parentBuilder.Append(doubleValue.ToString());
                }
                return;
            }
            if (dataType == DataType.LONG)
            {
                long value = tuple.GetLong(node.Field);
                double doubleValue = (double) value;
                ((NumericColumn) node.Column).SetNumericValue(row, doubleValue);
                if (parentBuilder != null)
                {
                    parentBuilder.Append(doubleValue.ToString());
                }
                return;
            }
            if (dataType == DataType.STRING)
            {
                string textValue = tuple.GetString(node.Field);
                ((TextColumn) node.Column).SetTextValue(row, textValue);
                if (parentBuilder != null) {
                    parentBuilder.Append(textValue);
                }
                return;
            }
            if (dataType == DataType.BOOL)
            {
                bool value = tuple.GetBoolean(node.Field);
                string textValue = value.ToString();
                ((TextColumn) node.Column).SetTextValue(row, textValue);
                if (parentBuilder != null)
                {
                    parentBuilder.Append(textValue);
                }
                return;
            }
            if (dataType == DataType.TIMESTAMP)
            {
                Timestamp value = tuple.GetTimestamp(node.Field);
                DateTime dateTimeValue = timeZoneInfo != null ?
                    TimeZoneInfo.ConvertTimeFromUtc(value.ToDateTime(), timeZoneInfo) :
                    value.ToDateTime();
                ((TimeColumn) node.Column).SetTimeValue(row, dateTimeValue);
                if (parentBuilder != null)
                {
                    parentBuilder.Append(dateTimeValue.ToString());
                }
                return;
            }

            if (dataType != DataType.TUPLE)
            {
                throw Exceptions.UnsupportedDataType(dataType);
            }

            StringBuilder tupleBuilder = new StringBuilder(256);
            Tuple childTuple = tuple.GetTuple(node.Field);
            foreach (FieldTreeNode child in node.Children)
            {
                if (tupleBuilder.Length > 0)
                {
                    tupleBuilder.Append(",");
                }
                ReadAndAssignFieldRecursive(childTuple, child, row, tupleBuilder);
            }
            string tupleString = tupleBuilder.ToString();
            ((TextColumn)node.Column).SetTextValue(row, tupleString);
            if (parentBuilder != null)
            {
                parentBuilder.Append("(");
                parentBuilder.Append(tupleString);
                parentBuilder.Append(")");
            }
        }
    }
}
