﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;

using StreamBase.SB;
using StreamBase.SB.Client;

namespace Panopticon.StreamBasePlugin
{
    public static class StreamBaseClientUtil
    {
        public static StreamBaseClient GetStreamBaseClient(
            StreamBaseSettings settings, IEnumerable<ParameterValue> parameters)
        {
            string server = settings.GetHostUrl(parameters);

            // TODO: check if StreamBaseURI.ArrayFromString(string)
            // is doing the same work as below or just skip splitting the string
            // and work with settings.PrimaryURL and settings.SecondaryURL
            // directly.

            // Create new Streambase Client using StreambaseURIs, and not Server strings.
            // This is to support high availability.
            // First the serverlist string is used to populate a SBURI array.
            // Then the clietn is created using this SBURI array.
            string[] delim = { "," };
            string[] serverlist = server.Split(
                delim, StringSplitOptions.None);

            bool appendUserInURL = true;
            if (server.Replace(" ", "").ToLower().Contains("user=") ||
                string.IsNullOrEmpty(settings.UserName))
            {
                appendUserInURL = false;
            }

            string username = settings.UserName;
            string password = settings.Password;
            int uricount = serverlist.Length;
            StreamBaseURI[] sburi = new StreamBaseURI[uricount];
            for (int i = 0; i < uricount; i++)
            {
                string url = serverlist[i];
                if (appendUserInURL)
                {
                    if (!url.EndsWith("/"))
                    {
                        url += "/";
                    }

                    url += ";user=" + username + ";password=" + password;

                }

                sburi[i] = new StreamBaseURI(url.Trim());

            }

            StreamBaseClient client = new StreamBaseClient(sburi);
            return client;
        }

        // Used in the plugin to create the table based on a scheme.
        // But also in the StreamBaseSettings to find
        // id and timeid column candidates.
        public static FieldTreeTable CreateTableFromSettings(
            StreamBaseSettings sbSettings,
            IEnumerable<ParameterValue> parameters)
        {
            //Retrieve Schema through StreamProperties
            //and NOT through client.GetSchemaForStream().
            StreamBaseClient client = GetStreamBaseClient(sbSettings, parameters);
            StreamProperties streamproperties =
                client.GetStreamProperties(sbSettings.Stream);
            Schema schema = streamproperties.GetSchema();

            FieldTreeTable table = CreateTableFromSchema(schema, parameters);

            client.Close();

            return table;
        }

        private static FieldTreeTable CreateTableFromSchema(Schema schema,
            IEnumerable<ParameterValue> parameters)
        {
            FieldTreeTable table = new FieldTreeTable(parameters);
            foreach (Schema.Field field in schema.Fields)
            {
                CommonUtil.AddColumnRecursive(table, table.Root, field, "");
            }
            return table;
        }

        public static string[] GetOutputStreams(StreamBaseSettings settings, 
            IEnumerable<ParameterValue> parameters)
        {
            StreamBaseClient client = GetStreamBaseClient(settings, parameters);
            string[] streams = client.ListEntities(EntityType.OUTPUT_STREAMS);
            client.Close();
            return streams;
        }
    }
}
