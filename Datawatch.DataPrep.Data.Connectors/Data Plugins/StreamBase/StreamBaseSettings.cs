﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

using StreamBase.SB;

namespace Panopticon.StreamBasePlugin
{
    public class StreamBaseSettings : RealtimeConnectionSettings
    {
        public StreamBaseSettings()
            : base()
        {
        }

        public StreamBaseSettings(PropertyBag bag)
            : base(bag, true)
        {
        }

        public string GetHostUrl(IEnumerable<ParameterValue> parameters)
        {
            string host = StreamBaseDataAdapter.
                ApplyParameters(PrimaryURL, parameters, false);

            if (!host.StartsWith("sb://"))
            {
                host = "sb://" + host;
            }
            string secondary = StreamBaseDataAdapter.
                ApplyParameters(SecondaryURL, parameters, false);
            if (secondary != null && secondary.Length > 0)
            {
                host += "," + secondary;
            }
            return host;
        }

        public string PrimaryURL
        {
            get { return GetInternal("PrimaryURL", "sb://localhost:10000"); }
            set { SetInternal("PrimaryURL", value); }
        }
        public string SecondaryURL
        {
            get { return GetInternal("SecondaryURL",string.Empty); }
            set { SetInternal("SecondaryURL", value); }
        }

        public string Stream
        {
            get { return GetInternal("Stream"); }
            set { SetInternal("Stream", value); }
        }

        public string UserName
        {
            get { return GetInternal("UserName",string.Empty); }
            set { SetInternal("UserName", value); }
        }

        public string Password
        {
            get { return GetInternal("Password", string.Empty); }
            set { SetInternal("Password", value); }
        }

        public string Predicate
        {
            get { return GetInternal("Predicate"); }
            set { SetInternal("Predicate", value); }
        }

        public bool EncloseParameterInQuote
        {
            get
            {
                string value = GetInternal("EncloseParameterInQuote",
                    true.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("EncloseParameterInQuote", value.ToString()); }
        }

        public string TimeZone
        {
            get { return GetInternal("TimeZone", "null"); }
            set
            {
                if (value.Length <= 0)
                {
                    SetInternal("TimeZone", "null");
                }
                else
                {
                    SetInternal("TimeZone", value);
                }
            }
        }

        public void UpdateIdCandidates(IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(PrimaryURL) ||
                string.IsNullOrEmpty(Stream))
            {
                IdColumnCandidates = null;
                TimeIdColumnCandidates = null;
                return;
            }

            FieldTreeTable table =
                StreamBaseClientUtil.CreateTableFromSettings(this, parameters);

            List<string> idCandidates = new List<string>();
            List<string> timeIdCandidates = new List<string>();
            foreach (FieldTreeNode child in table.Root.Children)
            {
                UpdateIdCandidatesHelper(idCandidates, timeIdCandidates, child);
            }

            IdColumnCandidates = idCandidates.ToArray();
            TimeIdColumnCandidates = timeIdCandidates.ToArray();
        }

        private static void UpdateIdCandidatesHelper(List<string> idCandidates,
            List<string> timeIdCandidates, FieldTreeNode node)
        {
            DataType dataType = node.Field.DataType;
            if (dataType == DataType.STRING ||
                dataType == DataType.INT ||
                dataType == DataType.LONG ||
                dataType == DataType.BOOL ||
                dataType == DataType.TUPLE)
            {
                idCandidates.Add(node.Column.Name);
            }
            else if (dataType == DataType.TIMESTAMP)
            {
                timeIdCandidates.Add(node.Column.Name);
            }

            if (dataType == DataType.TUPLE)
            {
                foreach (FieldTreeNode child in node.Children)
                {
                    UpdateIdCandidatesHelper(idCandidates, timeIdCandidates,
                        child);
                }
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StreamBaseSettings)) return false;
            
            if (!base.Equals(obj)) return false;

            StreamBaseSettings cs = (StreamBaseSettings)obj;

            if (this.PrimaryURL != cs.PrimaryURL ||
                this.SecondaryURL != cs.SecondaryURL ||
                this.Predicate != cs.Predicate ||
                this.EncloseParameterInQuote != cs.EncloseParameterInQuote ||
                this.Stream != cs.Stream ||
                this.Password != cs.Password ||
                this.UserName != cs.UserName ||
                this.TimeZone != cs.TimeZone)
            {                
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields = 
                { 
                    PrimaryURL, SecondaryURL, UserName, Password, 
                    Stream, IdColumn, Predicate, EncloseParameterInQuote,
                    TimeZone
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }
    }
}
