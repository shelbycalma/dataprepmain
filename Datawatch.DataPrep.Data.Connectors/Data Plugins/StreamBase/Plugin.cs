﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

using StreamBase.SB.Client;
using Tuple = StreamBase.SB.Tuple;
using Panopticon.StreamBasePlugin.Properties;

namespace Panopticon.StreamBasePlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin<FieldTreeTable, StreamBaseSettings,
        Tuple, StreamBaseDataAdapter>
    {
        internal const string PluginId = "StreamBasePlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "StreamBase 7.1";
        internal const string PluginType = DataPluginTypes.Streaming;

        private bool disposed;

        public Plugin()
        {
            Log.Info(Resources.LogStreamBaseClientVersion,
                PluginTitle, StreamBaseClient.GetVersion());
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    //Managed
                }
                //Unmanaged
                disposed = true;
            }

            base.Dispose(disposing);
        }

        protected override ITable CreateTable(StreamBaseSettings sbSettings,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;
            FieldTreeTable table = StreamBaseClientUtil.CreateTableFromSettings(
                sbSettings, parameters);
            table.InitializeIdColumn(sbSettings.IdColumn);

            if (!sbSettings.IsTimeIdColumnGenerated &&
                sbSettings.TimeIdColumn != null)
            {
                table.InitializeTimeIdColumn(sbSettings.TimeIdColumn);
            }
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;
        }

        public override StreamBaseSettings CreateSettings(PropertyBag bag)
        {
            StreamBaseSettings sbSettings = new StreamBaseSettings(bag);
            if (string.IsNullOrEmpty(sbSettings.Title))
            {
                sbSettings.Title = sbSettings.Stream ?? Properties.Resources.UiConnectionWindowStreamBaseConnection;
            }
            return sbSettings;
        }
    }
}
