﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors.Properties;
using StreamBase.SB;

namespace Panopticon.StreamBasePlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        public static Exception UnsupportedDataType(DataType dataType)
        {
            string message = string.Format(Resources.LogUnSupportedDataType,
                dataType);
            return CreateNotSupported(message);
        }

        public static Exception TableDoesNotExist(string tableName)
        {
            return CreateInvalidOperation(string.Format(
                Datawatch.DataPrep.Data.Core.Properties.Resources.ExTableDoesNotExist, tableName));
        }
    }
}
