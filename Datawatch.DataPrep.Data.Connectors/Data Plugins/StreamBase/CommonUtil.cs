﻿using System;
using System.Text;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors.Properties;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using StreamBase.SB;
using Tuple = StreamBase.SB.Tuple;

namespace Panopticon.StreamBasePlugin
{
    internal static class CommonUtil
    {
        public static void AddColumnRecursive(FieldTreeTable table,
            FieldTreeNode parent, Schema.Field field, string namePrefix)
        {
            string name = (namePrefix.Length > 0 ? namePrefix + "." : "")
                + field.Name;
            Column column = CreateColumn(name, field.DataType);

            if (column == null)
            {
                // No column was created.
                // Data type of the field was not supported and will be ignored.
                return;
            }

            FieldTreeNode node = table.AddFieldTreeNode(column, field, parent);

            if (field.DataType != DataType.TUPLE)
            {
                return;
            }

            Schema schema = field.Schema;
            foreach (Schema.Field childField in schema.Fields)
            {
                AddColumnRecursive(table, node, childField, name);
            }
        }

        private static Column CreateColumn(string name, DataType dataType)
        {
            // TODO: maybe assign an "integer" format string for int and long.
            if (dataType == DataType.DOUBLE ||
                dataType == DataType.INT ||
                dataType == DataType.LONG)
            {
                return new NumericColumn(name);
            }

            if (dataType == DataType.STRING ||
                dataType == DataType.BOOL ||
                dataType == DataType.TUPLE)
            {
                return new TextColumn(name);
            }

            if (dataType == DataType.TIMESTAMP)
            {
                return new TimeColumn(name);
            }

            // In 7.0 unsupported can be: <blob> and <list>.
            // In 7.2 it can also be <capture>
            Log.Info(Resources.LogUnSupportedDataType, dataType);
            return null;
        }

        // OPTIMIZE: Who calls this? Can we move Begin/EndUpdate higher up?
        public static void ReadAndAssignFieldRecursive(Tuple tuple,
            FieldTreeNode node, Row row, StringBuilder parentBuilder,
            TimeZoneHelper timeZoneHelper)
        {
            node.Column.Table.BeginUpdate();

            try
            {
                if (tuple.IsNull(node.Field))
                {
                    node.Column.SetValue(row, null);

                    // StreamBase uses "null" in it's ToString() implementation.
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append("null");
                    }
                    return;
                }

                DataType dataType = node.Field.DataType;

                if (dataType == DataType.DOUBLE)
                {
                    double doubleValue = tuple.GetDouble(node.Field);
                    ((NumericColumn)node.Column).SetNumericValue(row, doubleValue);
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append(doubleValue.ToString());
                    }
                    return;
                }
                if (dataType == DataType.INT)
                {
                    int value = tuple.GetInt(node.Field);
                    double doubleValue = (double)value;
                    ((NumericColumn)node.Column).SetNumericValue(row, doubleValue);
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append(doubleValue.ToString());
                    }
                    return;
                }
                if (dataType == DataType.LONG)
                {
                    long value = tuple.GetLong(node.Field);
                    double doubleValue = (double)value;
                    ((NumericColumn)node.Column).SetNumericValue(row, doubleValue);
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append(doubleValue.ToString());
                    }
                    return;
                }
                if (dataType == DataType.STRING)
                {
                    string textValue = tuple.GetString(node.Field);
                    ((TextColumn)node.Column).SetTextValue(row, textValue);
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append(textValue);
                    }
                    return;
                }
                if (dataType == DataType.BOOL)
                {
                    bool value = tuple.GetBoolean(node.Field);
                    string textValue = value.ToString();
                    ((TextColumn)node.Column).SetTextValue(row, textValue);
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append(textValue);
                    }
                    return;
                }
                if (dataType == DataType.TIMESTAMP)
                {
                    Timestamp value = tuple.GetTimestamp(node.Field);
                    DateTime dateTimeValue = value.ToDateTime();

                    if (timeZoneHelper.TimeZoneSelected)
                    {
                        dateTimeValue = timeZoneHelper.ConvertFromUTC(dateTimeValue);
                    }
                    ((TimeColumn)node.Column).SetTimeValue(row, dateTimeValue);
                    if (parentBuilder != null)
                    {
                        parentBuilder.Append(dateTimeValue.ToString());
                    }
                    return;
                }

                if (dataType != DataType.TUPLE)
                {
                    throw Exceptions.UnsupportedDataType(dataType);
                }

                StringBuilder tupleBuilder = new StringBuilder(256);
                Tuple childTuple = tuple.GetTuple(node.Field);
                foreach (FieldTreeNode child in node.Children)
                {
                    if (tupleBuilder.Length > 0)
                    {
                        tupleBuilder.Append(",");
                    }
                    ReadAndAssignFieldRecursive(childTuple, child, row, tupleBuilder,
                        timeZoneHelper);
                }
                string tupleString = tupleBuilder.ToString();
                ((TextColumn)node.Column).SetTextValue(row, tupleString);
                if (parentBuilder != null)
                {
                    parentBuilder.Append("(");
                    parentBuilder.Append(tupleString);
                    parentBuilder.Append(")");
                }
            }
            finally {
                node.Column.Table.EndUpdate();
            }
        }
    }
}
