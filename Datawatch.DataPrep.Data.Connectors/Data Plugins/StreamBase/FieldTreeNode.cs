﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using StreamBase.SB;

namespace Panopticon.StreamBasePlugin
{
    public class FieldTreeNode
    {
        internal FieldTreeNode Parent = null;
        internal List<FieldTreeNode> Children = new List<FieldTreeNode>();
        internal Schema.Field Field = null;
        internal Column Column = null;
    }
}
