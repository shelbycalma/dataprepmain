﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using StreamBase.SB;

namespace Panopticon.StreamBasePlugin
{
    public class FieldTreeTable : ParameterTable
    {
        internal FieldTreeNode Root = new FieldTreeNode();
        internal FieldTreeNode[] IdPath = null;
        internal FieldTreeNode IdNode = null;
        internal FieldTreeNode[] TimeIdPath = null;
        internal FieldTreeNode TimeIdNode = null;

        public FieldTreeTable(IEnumerable<ParameterValue> parameters)
            : base(parameters)
        {
        }

        public void InitializeIdColumn(string idColumn)
        {
            IdNode = GetFieldTreeNode(Root, idColumn);
            IdPath = CreatePathFromRoot(IdNode);
        }

        public void InitializeTimeIdColumn(string timeIdColumn)
        {
            TimeIdNode = GetFieldTreeNode(Root, timeIdColumn);
            TimeIdPath = CreatePathFromRoot(TimeIdNode);
        }

        private static FieldTreeNode[] CreatePathFromRoot(FieldTreeNode node)
        {
            List<FieldTreeNode> path = new List<FieldTreeNode>();
            FieldTreeNode tempNode = node;
            while (tempNode.Parent != null && tempNode.Parent.Field != null)
            {
                tempNode = tempNode.Parent;
                path.Insert(0, tempNode);
            }

            return path.ToArray();
        }

        private static FieldTreeNode GetFieldTreeNode(FieldTreeNode node,
            string columnName)
        {
            if (node.Column != null && node.Column.Name == columnName)
            {
                return node;
            }

            foreach (FieldTreeNode childNode in node.Children)
            {
                FieldTreeNode result = GetFieldTreeNode(childNode, columnName);
                if (result != null)
                {
                    return result;
                }
            }

            return null;
        }

        public FieldTreeNode AddFieldTreeNode(Column column, Schema.Field field,
            FieldTreeNode parent)
        {
            FieldTreeNode node = new FieldTreeNode();

            this.BeginUpdate();

            try {
                this.AddColumn(column);
                node.Column = column;
                node.Field = field;
                parent.Children.Add(node);
                node.Parent = parent;
            }
            finally {
                this.EndUpdate();
            }

            return node;
        }
    }
}
