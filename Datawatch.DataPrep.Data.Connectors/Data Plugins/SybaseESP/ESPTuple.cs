﻿using System.Collections.Generic;

using SYBASE.Esp.SDK;

namespace Panopticon.SybaseESPPlugin
{
    public class ESPTuple
    {
        public Dictionary<string, ESPValue> Values { get; set; }
        public NetEspStream.NET_ESP_OPERATION_T opcode { get; set; }
        public ESPTuple()
        {
            Values = new Dictionary<string, ESPValue>();
        }

    }
}
