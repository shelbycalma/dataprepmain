﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Xml;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using SYBASE.Esp.SDK;

namespace Panopticon.SybaseESPPlugin
{
    public enum ConnectionMode
    {
        Stream = 0,
        Query = 1
    }

    public class SybaseESPSettings : RealtimeConnectionSettings
    {
        private ICommand fetchStreamsCommand;
        internal SybaseESPConnectionHelper espCon;
        private IEnumerable<ParameterValue> parameters;
        private IPluginErrorReportingService errorReporter;

        private const string TimeZoneHelperProperty = "TimeZoneHelper";
        private TimeZoneHelper timeZoneHelper;
        private bool disposed;

        public SybaseESPSettings(IPluginErrorReportingService errorReporter = null)
            : base()
        {
            this.errorReporter = errorReporter
                ?? new MessageBoxBasedErrorReportingService();
        }

        public SybaseESPSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : this(bag, parameters, null)
        {
        }

        public SybaseESPSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters,
            IPluginErrorReportingService errorReporter)
            : base(bag, true)
        {

            timeZoneHelper = GetTimeZoneHelper(bag);
            this.parameters = parameters;
            this.errorReporter = errorReporter 
                ?? new MessageBoxBasedErrorReportingService();
        }

        public bool IsOK
        {
            get
            {
                bool hasError =
                    string.IsNullOrEmpty(Host) ||
                    // Port is an int
                    (!string.IsNullOrEmpty(Username) && string.IsNullOrEmpty(Password)) ||
                    string.IsNullOrEmpty(Workspace) ||
                    string.IsNullOrEmpty(Application) ||
                    (ConnectionMode == ConnectionMode.Stream && string.IsNullOrEmpty(Stream)) ||
                    (ConnectionMode == ConnectionMode.Query && string.IsNullOrEmpty(Query)) ||
                    string.IsNullOrEmpty(IdColumn) ||
                    (IsTimeIdColumnGenerated && string.IsNullOrEmpty(TimeIdColumn))
                    // Limit is an int
                    ;
                return !hasError;
            }
        }

        public ConnectionMode ConnectionMode
        {
            get
            {
                string s = GetInternal("ConnectionMode");
                if (s != null)
                {
                    try
                    {
                        return (ConnectionMode)Enum.Parse(
                            typeof(ConnectionMode), s);
                    }
                    catch
                    {
                    }
                }
                return ConnectionMode.Stream;
            }
            set
            {
                SetInternal("ConnectionMode", value.ToString());
            }
        }

        public string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value); }
        }

        public string Port
        {
            get { return GetInternal("Port", "9786"); }
            set { SetInternal("Port", value); }
        }

        public string Uri
        {
            get
            {
                string uri = "esp://";
                if (IsEncrypted)
                {
                    uri = "esps://";
                }
                return uri + Utils.ApplyParameters(Host, parameters, false)
                    + ":" + Utils.ApplyParameters(Port, parameters, false);
            }
        }

        public string Application
        {
            get { return GetInternal("Application"); }
            set { SetInternal("Application", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public string Stream
        {
            get { return GetInternal("Stream"); }
            set { SetInternal("Stream", value); }
        }

        public string Username
        {
            get { return GetInternal("Username"); }
            set { SetInternal("Username", value); }
        }

        public string Workspace
        {
            get { return GetInternal("Workspace", "default"); }
            set { SetInternal("Workspace", value); }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public int ServerQueue
        {
            get { return GetInternalInt("ServerQueue", 12000); }
            set { SetInternalInt("ServerQueue", value); }
        }

        public int ServerWait
        {
            get { return GetInternalInt("ServerWait", 8000); }
            set { SetInternalInt("ServerWait", value); }
        }

        public int PulseInterval
        {
            //Subscription option pulse_interval was defaulted to 0 as 
            //per suggested by David, setting it back to 1 to reduce CPU usages  
            get { return GetInternalInt("PulseInterval", 1); }
            set { SetInternalInt("PulseInterval", value); }
        }

        public bool IsEncrypted
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsEncrypted", defaultValue));
            }
            set
            {
                SetInternal("IsEncrypted",
                    XmlConvert.ToString(value));
            }
        }

        public override string Title
        {
            get
            {
                if (ConnectionMode == ConnectionMode.Query)
                {
                    return Properties.Resources.UiDataTableNameSQL;
                }
                else if (!string.IsNullOrEmpty(Stream))
                {
                    return Stream;
                }
                else
                {
                    return Properties.Resources.UiDataTableDefaultName;
                }
            }
        }

        public bool FlagsSet
        {
            get
            {
                if (this.IsDroppable | this.IsLossy | this.IsDeltasOnly)
                    return true;
                else return false;
            }
        }
        public NetEspSubscriberOptions.NET_ESP_SUBSCRIBER_FLAG SubscriberFlags
        {
            get
            {
                NetEspSubscriberOptions.NET_ESP_SUBSCRIBER_FLAG flags = 0;
                if (FlagsSet)
                {
                    if (this.IsDroppable) flags = NetEspSubscriberOptions
                        .NET_ESP_SUBSCRIBER_FLAG.NET_ESP_SUBSCRIBER_FLAG_DROPPABLE;
                    if (this.IsLossy) flags = flags | NetEspSubscriberOptions
                        .NET_ESP_SUBSCRIBER_FLAG.NET_ESP_SUBSCRIBER_FLAG_LOSSY;
                    if (this.IsDeltasOnly) flags = flags | NetEspSubscriberOptions
                        .NET_ESP_SUBSCRIBER_FLAG.NET_ESP_SUBSCRIBER_FLAG_NOBASE;
                }
                return flags;
            }
        }

        public bool IsCallback
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsCallback", defaultValue));
            }
            set
            {
                SetInternal("IsCallback",
                    XmlConvert.ToString(value));
            }
        }
        public bool IsDroppable
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsDroppable", defaultValue));
            }
            set
            {
                SetInternal("IsDroppable",
                    XmlConvert.ToString(value));
            }
        }
        public bool IsLossy
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsLossy", defaultValue));
            }
            set
            {
                SetInternal("IsLossy",
                    XmlConvert.ToString(value));
            }
        }
        public bool IsDeltasOnly
        {
            get
            {
                string defaultValue = XmlConvert.ToString(false);
                return XmlConvert.ToBoolean(
                    GetInternal("IsDeltasOnly", defaultValue));
            }
            set
            {
                SetInternal("IsDeltasOnly",
                    XmlConvert.ToString(value));
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return timeZoneHelper;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (timeZoneHelper != null)
                    {
                        timeZoneHelper.Dispose();
                    }
                }
                disposed = true;
            }
        }

        internal static TimeZoneHelper GetTimeZoneHelper(PropertyBag bag)
        {
            TimeZoneHelper timeZoneHelper;

            PropertyBag timeZoneHelperBag =
               bag.SubGroups[TimeZoneHelperProperty];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups[TimeZoneHelperProperty] = timeZoneHelperBag;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);

            
            return timeZoneHelper;
        }


        protected override void FirePropertyChanged(string name)
        {
            base.FirePropertyChanged(name);

            if (name == "Host" ||
                name == "Port" ||
                name == "Username" ||
                name == "Password" ||
                name == "IsEncrypted")
            {
                if (espCon != null)
                {
                    espCon.DisConnect();
                }
                return;
            }
            if (name == "Workspace" ||
                name == "Application")
            {
                if (espCon != null)
                {
                    espCon.ResetProject();
                }
                return;
            }
            if (name == "Query")
            {
                UpdateIdCandidates();
                return;
            }
            if (name == "ConnectionMode")
            {
                base.FirePropertyChanged("IsStreamConnectionMode");
                base.FirePropertyChanged("IsQueryConnectionMode");

                UpdateIdCandidates();
                return;
            }
        }

        public ICommand FetchStreamsCommand
        {
            get
            {
                if (fetchStreamsCommand == null)
                {
                    fetchStreamsCommand = new DelegateCommand(FetchStreams,
                        CanFetchStreams);
                }
                return fetchStreamsCommand;
            }
        }

        public bool IsStreamConnectionMode
        {
            get { return ConnectionMode == ConnectionMode.Stream; }
            set
            {
                if (value)
                {
                    ConnectionMode = ConnectionMode.Stream;
                }
            }
        }

        public bool IsQueryConnectionMode
        {
            get { return ConnectionMode == ConnectionMode.Query; }
            set
            {
                if (value)
                {
                    ConnectionMode = ConnectionMode.Query;
                }
            }
        }

        private void FetchStreams()
        {
            FirePropertyChanged("AvailableStreams");
        }

        private bool CanFetchStreams()
        {
            return !(string.IsNullOrEmpty(Host) ||
                    string.IsNullOrEmpty(Workspace) ||
                    string.IsNullOrEmpty(Application));
        }

        private void UpdateIdCandidates()
        {
            if (!CanFetchStreams())
            {
                IdColumnCandidates = null;
                TimeIdColumnCandidates = null;
                return;
            }

            if (espCon == null)
            {
                espCon = new SybaseESPConnectionHelper();
                espCon = espCon.Connect(this);
            }

            NetEspSubscriber subscriber;
            NetEspSchema schema = espCon.GetSchema(this, parameters, out subscriber);

            Utils.UpdateIdCandidates(schema, this);
            if (subscriber != null)
            {
                subscriber.disconnect(new NetEspError());
            }
            if (espCon != null)
            {
                espCon.DisConnect();
            }
            espCon = null;

        }

        public List<string> AvailableStreams
        {
            get
            {
                List<string> modelstreamnames = new List<string>();

                try
                {
                    if (string.IsNullOrEmpty(Host) ||
                        string.IsNullOrEmpty(Workspace) ||
                        string.IsNullOrEmpty(Application))
                    {
                        return null;
                    }
                    if (espCon == null)
                    {
                        espCon = new SybaseESPConnectionHelper();
                        espCon = espCon.Connect(this);
                    }
                    modelstreamnames = espCon.GetAvailableStreams(this);

                    //select 1st stream if other stream is not already selected
                    if (modelstreamnames != null && modelstreamnames.Count > 0 &&
                        (Stream == null || modelstreamnames.Contains(Stream) == false))
                    {
                        Stream = modelstreamnames.First();
                    }
                    if (Stream != null)
                    {
                        UpdateIdCandidates();
                    }
                }
                catch (Exception ex)
                {
                    this.errorReporter.Report(
                        Properties.Resources.UiConnectionWindowTitle,
                        ex.Message);
                }
                if (espCon != null)
                {
                    espCon.DisConnect();
                }
                espCon = null;

                return modelstreamnames;
            }
        }

        internal IPluginErrorReportingService ErrorReporter
        {
            get
            {
                return errorReporter;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is SybaseESPSettings)) return false;

            if (!base.Equals(obj)) return false;

            SybaseESPSettings cs = (SybaseESPSettings)obj;

            // TODO: should equals and hashcode only use the stream
            // if in stream-mode, and query only if in query-mode?

            if (this.Application != cs.Application ||
                this.ConnectionMode != cs.ConnectionMode ||
                this.Host != cs.Host ||
                this.Password != cs.Password ||
                this.Port != cs.Port ||
                this.Query != cs.Query ||
                this.Stream != cs.Stream ||
                this.Username != cs.Username ||
                this.Workspace != cs.Workspace ||
                this.IsEncrypted != cs.IsEncrypted ||
                this.ServerQueue != cs.ServerQueue ||
                this.ServerWait != cs.ServerWait ||
                this.PulseInterval != cs.PulseInterval ||
                this.FlagsSet != cs.FlagsSet ||
                this.IsCallback != cs.IsCallback ||
                this.IsDroppable != cs.IsDroppable ||
                this.IsLossy != cs.IsLossy ||
                this.IsDeltasOnly != cs.IsDeltasOnly)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    Application, ConnectionMode, Host, Password,
                    Port, Query, Stream, Username, Workspace,IsEncrypted,
                    ServerQueue, ServerWait, PulseInterval, FlagsSet,
                    IsCallback, IsDroppable, IsLossy,
                    IsDeltasOnly
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }
    }
}
