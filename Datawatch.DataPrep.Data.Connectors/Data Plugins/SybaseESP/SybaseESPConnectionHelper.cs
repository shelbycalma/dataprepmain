﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using SYBASE.Esp.SDK;

namespace Panopticon.SybaseESPPlugin
{
    internal class SybaseESPConnectionHelper
    {
        private static NetEspSdk s_sdk;
        private NetEspError espError;
        NetEspServer server;
        NetEspProject project;
        NetEspSubscriberOptions suboptions = new NetEspSubscriberOptions();

        public SybaseESPConnectionHelper()
        {
            espError = new NetEspError();
        }
        public SybaseESPConnectionHelper(NetEspServer espServer)
        {
            server = espServer;
            espError = new NetEspError();
        }

        public void DisConnect()
        {
            try
            {                
                if(server != null)
                {
                    server.disconnect(espError);
                }

                server = null;
                project = null;
                
            }
            catch { }
            finally
            {
                s_sdk.stop(espError);
            }
        }

        public void ResetProject()
        {
            if (project != null)
            {
                project.disconnect(espError);
                project.Dispose();
                project = null;
            }
        }

        public SybaseESPConnectionHelper Connect(SybaseESPSettings settings)
        {
            InitiEspServer(settings);
            return new SybaseESPConnectionHelper(server);
        }

        public void InitiEspServer(SybaseESPSettings settings)
        {
            s_sdk = NetEspSdk.get_sdk();
            s_sdk.start(espError);
            server = null;

            NetEspCredentials creds;
            NetEspServerOptions soptions = new NetEspServerOptions();
            soptions.set_retries(2);
            creds = new NetEspCredentials(
                NetEspCredentials.NET_ESP_CREDENTIALS_T
                .NET_ESP_CREDENTIALS_USER_PASSWORD);
            creds.set_user(Utils.ApplyParameters(
                settings.Username, settings.Parameters, false));
            creds.set_password(Utils.ApplyParameters(
                settings.Password, settings.Parameters, false));

            NetEspUri espuri = new NetEspUri();
            espuri.set_uri(settings.Uri, espError);

            int rc = -1;

            server = s_sdk.get_server(espuri, creds, soptions, espError);

            if (server != null)
            {
                rc = server.connect(espError);
            }

            if (rc != 0)
            {
                throw new Exception(Properties.Resources.ExServerConFailed);
            }
        }

        public void InitEspProject(SybaseESPSettings settings)
        {
            if (server == null)
            {
                InitiEspServer(settings);
            }

            NetEspProjectOptions projoptions = new NetEspProjectOptions();
            project = new NetEspProject();
            
            int rc = -1;

            project = server.get_project(
                Utils.ApplyParameters(settings.Workspace, 
                    settings.Parameters, false),
                Utils.ApplyParameters(settings.Application,
                    settings.Parameters, false),
                 projoptions, espError);

            if (project != null)
            {
                rc = project.connect(espError);
            }

            if (rc != 0)
            {
                throw new Exception(string.Format(
                    Properties.Resources.ExProjectConFailed, settings.Workspace,
                    settings.Application));
            }
        }

        internal List<string> GetAvailableStreams(SybaseESPSettings settings)
        {
            if (server == null)
            {
                InitiEspServer(settings);
            }
            if (project == null)
            {
                InitEspProject(settings);
            }
            List<string> modelstreamnames = new List<string>();
            modelstreamnames = project.get_model_stream_names(espError);
            return modelstreamnames;
        }

        internal NetEspSchema GetSchema(SybaseESPSettings settings,
            IEnumerable<ParameterValue> parameters,
            out NetEspSubscriber outsubscriber)
        {           
            if (project == null)
            {
                InitEspProject(settings);
            }
            int rc = -1;
            NetEspSubscriber subscriber = null;
            NetEspStream stream=null;
            NetEspSchema schema = null;

            SubOptions(settings);

            if (settings.ConnectionMode == ConnectionMode.Stream)
            {
                string strStream = Utils.ApplyParameters(settings.Stream,
                    parameters, false);
                stream = project.get_stream(strStream, espError);
                subscriber = project.create_subscriber(suboptions, espError);

                Log.Info(Properties.Resources.LogStartSubscription, strStream);

                if (subscriber != null)
                {
                    subscriber.subscribe_stream(stream, espError);
                    rc = subscriber.connect(espError);
                }

                if (rc != 0)
                {
                    throw new Exception(Properties.Resources.ExStreamSubscriptionFailed);
                }

            }
            else if (settings.ConnectionMode == ConnectionMode.Query)
            {
                subscriber = project.create_subscriber(suboptions, espError);
                if (subscriber != null)
                {
                    ParameterValueCollection parameterValues =
                        new ParameterValueCollection(parameters);
                    parameterValues = DatabaseUtils.ConvertDateTimeParametersToUTC(
                        parameterValues, settings.TimeZoneHelper);

                    string query = Utils.ApplyParameters(settings.Query,
                        parameterValues);
                    Log.Info(Properties.Resources.LogQuery, query);
                    subscriber.connect(espError);
                    rc = subscriber.subscribe_sql(query ?? "", espError);
                }

                if (rc != 0)
                {
                    throw new Exception(Properties.Resources.ExSQLSubscriptionFailed);
                }
            }

            
            if (schema == null)
            {
                if (settings.ConnectionMode == ConnectionMode.Stream)
                {
                    schema = stream.get_schema(espError);
                }
                else
                {
                    //for query type subscription, i could not get a better approach
                    //to get the schema. So using the first stream from list of 
                    //subscribed streams seems to fit the bill. May be need to change
                    List<NetEspStream> streams = subscriber.get_subscribed_streams(espError);
                    if (streams != null && streams.First() != null)
                    {
                        schema = streams.First().get_schema(espError);

                    }
                }
            }
            if (schema == null)
            {
                throw new Exception(Properties.Resources.ExSchema);
            }

            outsubscriber = subscriber;
            return schema;
        }

        internal void SubOptions(SybaseESPSettings settings)
        {
            // There should be only one subscription options object - this initializes all the bits
            // Since we are putting all the options in try catch then even if we error getting the setting
            // from settings object, it should be set to a reasonable default value
            try
            {
                if (settings.FlagsSet)
                {
                    suboptions.set_flags((int)settings.SubscriberFlags);
                }
            }
            catch (Exception e)
            {
                Log.Info(Properties.Resources.LogSubscriptionFlagIgnored,
                    e.Message);
            }
            finally
            {
                if (!settings.FlagsSet)
                {
                    suboptions.set_flags(0);
                }
            }
            suboptions.set_pulse_interval(settings.PulseInterval);
            suboptions.set_queue_size(settings.ServerQueue);
            suboptions.set_base_drain_timeout(settings.ServerWait);

            try
            {
                if (!settings.IsCallback)
                    suboptions.set_mode(NetEspSubscriberOptions
                        .NET_ESP_ACCESS_MODE_T.NET_DIRECT_ACCESS);
                else
                    suboptions.set_mode(NetEspSubscriberOptions
                        .NET_ESP_ACCESS_MODE_T.NET_CALLBACK_ACCESS);
            }
            catch (Exception e)
            {
                suboptions.set_mode(NetEspSubscriberOptions
                    .NET_ESP_ACCESS_MODE_T.NET_DIRECT_ACCESS);
                Log.Info(Properties.Resources.LogDirectSubscriptionSet,
                    e.Message);
            }
        }

    }
}
