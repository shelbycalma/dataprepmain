﻿using System;
using System.Runtime.ExceptionServices;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;
using SYBASE.Esp.SDK;

namespace Panopticon.SybaseESPPlugin
{
    public class SybaseESPDataAdapter :
        RealtimeDataAdapter<ParameterTable, SybaseESPSettings, ESPTuple>
    {        
        private NetEspError espError;
        NetEspSchema schema;
        NetEspSubscriber subscriber;
        private Thread thread;
        SybaseESPConnectionHelper espCon;

        [HandleProcessCorruptedStateExceptions]
        public override void Start()
        {
            try
            {
                espError = new NetEspError();
                if (espCon == null)
                {
                    settings.Parameters = table.Parameters;
                    espCon = new SybaseESPConnectionHelper();
                    espCon = espCon.Connect(settings);

                }
                schema = espCon.GetSchema(settings, table.Parameters, out subscriber);
                
                thread = new Thread(new ThreadStart(ReadSubscription));
                thread.Name = "Sybase ESP event reader";
                thread.Start();
                
            }
            catch(Exception ex)
            {
                this.ErrorReporter.Report("Start failure", ex.Message);
                Log.Error(ex.Message);
            }
        }

        private void Enqueue(ESPTuple evt)
        {
            lock (this)
            {
                string id = evt.Values[settings.IdColumn].Value.ToString();

                if (evt.opcode ==
                    NetEspStream.NET_ESP_OPERATION_T.
                    NET_ESP_STREAM_OP_INSERT)
                {
                    updater.EnqueueInsert(id, evt);
                }
                else if (evt.opcode ==
                    NetEspStream.NET_ESP_OPERATION_T.
                    NET_ESP_STREAM_OP_DELETE ||
                    evt.opcode ==
                    NetEspStream.NET_ESP_OPERATION_T.
                    NET_ESP_STREAM_OP_SAFEDELETE)
                {
                    updater.EnqueueDelete(id, evt);
                }
                else
                {
                    updater.EnqueueUpdate(id, evt);
                }
            }
        }

        [HandleProcessCorruptedStateExceptions]
        private void ReadSubscription()
        {
            try
            {
                while (thread != null)
                {
                    // This is a blocking call
                    NetEspSubscriberEvent event1 =
                        subscriber.get_next_event(espError);

                    switch (event1.getType())
                    {
                        case (uint)NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT
                                .NET_ESP_SUBSCRIBER_EVENT_SYNC_START:
                            Log.Info(Properties.Resources.LogSubscribeEventSyncStart,
                                event1.getCategory().ToString(), event1.getType().ToString());
                            break;
                        case (uint)NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT
                                .NET_ESP_SUBSCRIBER_EVENT_SYNC_END:
                            // if you want to exit after reading data set done = true here
                            // This is the end of the base data AKA snapshot for a window
                            Log.Info(Properties.Resources.LogSubscribeEventSyncEnd, 
                                event1.getCategory().ToString(), event1.getType().ToString());
                            break;
                        case (uint)NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT.
                                    NET_ESP_SUBSCRIBER_EVENT_DATA:
                            NetEspMessageReader reader = event1.getMessageReader();
                            NetEspRowReader row = reader.next_row(espError);

                            while (row != null && thread != null)
                            {
                                Enqueue(Utils.EspReaderToTuple(row, schema,
                                    espError, table, settings));
                                //row.Dispose(); //crashing designer
                                row = reader.next_row(espError);
                            }
                            break;

                        case (uint)NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT.
                                NET_ESP_SUBSCRIBER_EVENT_CLOSED:
                        case (uint)NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT.
                                NET_ESP_SUBSCRIBER_EVENT_DISCONNECTED:
                            thread = null;
                            break;
                        case (uint)NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT
                                .NET_ESP_SUBSCRIBER_EVENT_ERROR:
                            Log.Error(Properties.Resources.LogSubscribeEventError, 
                                event1.getError().get_message());
                            break;
                        case (uint)(NetEspSubscriber.NET_ESP_SUBSCRIBER_EVENT
                                .NET_ESP_SUBSCRIBER_EVENT_CONNECTED):
                            Log.Info(Properties.Resources.LogSubscribeEventConnect);
                            break;
                        case (uint)(SYBASE.Esp.SDK.NetEspSubscriber
                                .NET_ESP_SUBSCRIBER_EVENT.NET_ESP_SUBSCRIBER_EVENT_SUBSCRIBED):
                            Log.Info(Properties.Resources.LogSubscribeEvent);
                            break;
                        default:
                            break;
                    }
                    event1.release();
                }
            }
            catch (Exception e)
            {
                Log.Exception(e);
            }
        }

        public override void Stop()
        {
            thread = null;
            try
            {
                if (subscriber != null)
                {
                    subscriber.disconnect(espError);
                }

                if (espCon != null)
                {
                    espCon.DisConnect();
                }   
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
            }

            espCon = null;
            
            Log.Info(Properties.Resources.LogStopSubscription,
                settings.IsQueryConnectionMode ? 
                Properties.Resources.LogSqlProjection : 
                Utils.ApplyParameters(settings.Stream, table.Parameters, false));
        }

        public override void UpdateColumns(Row row, ESPTuple evt)
        {
            object newValue;
            foreach (string name in evt.Values.Keys)
            {
                if (name == settings.IdColumn) continue;

                ESPValue v = evt.Values[name];

                if (v.Type == ColumnType.Text)
                {
                    newValue = v.Value;
                    ProcessTextValue(row, newValue, name);
                }
                else if (v.Type == ColumnType.Numeric)
                {
                    newValue = v.Value;
                    ProcessNumericValue(row, newValue, name);
                }
                else if (v.Type == ColumnType.Time)
                {
                    newValue = v.Value;
                    ProcessDateValue(row, newValue, name);
                }
            }
        }
        
        private void ProcessTextValue(Row row, object newValue, string colName)
        {
            TextColumn textCol = (TextColumn) table.GetColumn(colName);
            string oldTextValue = textCol.GetTextValue(row);
            string newTextValue = newValue != null ?
                newValue.ToString() : null;

            if (newTextValue != oldTextValue)
            {
                textCol.SetTextValue(row, newTextValue);
            }
        }

        private void ProcessNumericValue(Row row, object newValue, string colName)
        {
            NumericColumn numericCol = (NumericColumn) table.GetColumn(colName);
            double oldNumericValue = numericCol.GetNumericValue(row);
            double newNumericValue = newValue != null ?
                Convert.ToDouble(newValue) : NumericValue.Empty;

            if (newNumericValue != oldNumericValue)
            {
                numericCol.SetNumericValue(row, newNumericValue);
            }
        }

        private void ProcessDateValue(Row row, object newValue, string colName)
        {
            TimeColumn timeColumn = (TimeColumn) table.GetColumn(colName);
            DateTime oldTimeValue = timeColumn.GetTimeValue(row);
            DateTime newTimeValue = newValue != null ?
                Convert.ToDateTime(newValue) : TimeValue.Empty;

            if (newTimeValue != oldTimeValue)
            {
                timeColumn.SetTimeValue(row, newTimeValue);
            }
        }

        public override DateTime GetTimestamp(ESPTuple row)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return TimeValue.Empty;
            }

            object value = row.Values[settings.TimeIdColumn].Value;
            return value != null ?
                Convert.ToDateTime(value) : TimeValue.Empty;
        }
    }
}
