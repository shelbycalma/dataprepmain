﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.SybaseESPPlugin
{
    public class ESPValue
    {
        public object Value { get; set; }
        public ColumnType Type { get; set; }
    }
}
