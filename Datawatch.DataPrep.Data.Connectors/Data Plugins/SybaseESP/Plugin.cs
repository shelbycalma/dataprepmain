﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

using SYBASE.Esp.SDK;
using Panopticon.SybaseESPPlugin.Properties;


namespace Panopticon.SybaseESPPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin :
        RealtimeDataPlugin<ParameterTable, SybaseESPSettings,
            ESPTuple, SybaseESPDataAdapter>
    {
        internal const string PluginId = "SybaseESPPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Sybase ESP";
        internal const string PluginType = DataPluginTypes.Streaming;

        private bool disposed;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposed)
            {
                if (disposing)
                {
                }
                //Unmanaged
                disposed = true;
            }
        }

        public override SybaseESPSettings CreateSettings(PropertyBag bag)
        {
            return new SybaseESPSettings(bag, null, this.ErrorReporter);
        }

        protected override ITable CreateTable(SybaseESPSettings settings,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;

            if (settings.espCon == null)
            {
                //TODO:- In case of real time data plugin 
                //CreateSettings should be a overloaded method to 
                settings.Parameters = parameters;
                settings.espCon = new SybaseESPConnectionHelper();
                settings.espCon = settings.espCon.Connect(settings);
            }
            
            NetEspSubscriber subscriber;
            NetEspSchema schema = settings.espCon.GetSchema(settings, 
                parameters, out subscriber);

            ParameterTable table = (ParameterTable)
                Utils.BuildTable(schema, parameters);
            if (subscriber != null)
            {
                subscriber.disconnect(new NetEspError());
            }

            if (settings.espCon != null)
            {
                settings.espCon.DisConnect();
            }
            settings.espCon = null;
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
