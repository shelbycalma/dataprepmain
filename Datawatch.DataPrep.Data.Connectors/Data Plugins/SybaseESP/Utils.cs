﻿using System;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using SYBASE.Esp.SDK;
using Panopticon.SybaseESPPlugin.Properties;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.SybaseESPPlugin
{
    internal class Utils
    {

        [HandleProcessCorruptedStateExceptions]
        internal static ITable BuildTable(NetEspSchema schema,
            IEnumerable<ParameterValue> parameters)
        {
            ParameterTable table = new ParameterTable(parameters);

            if (schema == null)
            {
                return EmptyTable.Instance;
            }

            try
            {
                int colCount = schema.get_numcolumns();
                List<string> colnames = schema.get_column_names();
                List<int> coltypes = schema.get_column_types();

                table.BeginUpdate();
                for (int i = 0; i < colCount; i++)
                {
                    //set colType to string as default
                    NetEspStream.NET_DATA_TYPE_T colType =
                        NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_STRING;
                    try
                    {
                        colType = (NetEspStream.NET_DATA_TYPE_T)Enum.Parse(
                            typeof(NetEspStream.NET_DATA_TYPE_T), coltypes[i].ToString());
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex);
                    }

                    switch (colType)
                    {
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_STRING:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_NULL:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_OBJECT:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_BOOLEAN:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_BINARY:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_INTERVAL:
                            table.AddColumn(new TextColumn(colnames[i]));
                            break;

                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_INTEGER:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_LONG:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_FLOAT:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY01:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY02:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY03:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY04:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY05:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY06:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY07:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY08:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY09:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY10:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY11:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY12:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY13:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY14:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY15:
                            table.AddColumn(new NumericColumn(colnames[i]));
                            break;

                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_SECONDDATE:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_BIGDATETIME:
                        case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MSDATE:
                            table.AddColumn(new TimeColumn(colnames[i]));
                            break;
                        default:
                            Log.Warning(Resources.LogColumnTypeNotSupported,
                                colType.ToString());
                            break;
                    }
                }
                table.EndUpdate();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            return table;
        }

        // TODO: Rename timestamp either here or in the switch below.
        private static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        [HandleProcessCorruptedStateExceptions]
        internal static ESPTuple EspReaderToTuple(
            NetEspRowReader row, NetEspSchema schema, NetEspError espError,
            ParameterTable table, SybaseESPSettings settings)
        {
            //NetEspError espError = new NetEspError();

            ESPTuple tupple = new ESPTuple();

            tupple.opcode = row.get_opcode();

            int rowcolCount = row.get_num_columns();

            for (int i = 0; i < rowcolCount; i++)
            {
                ESPValue espValue = new ESPValue();

                //set colType to string as default
                NetEspStream.NET_DATA_TYPE_T colType =
                    NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_STRING;
                string colName = null;
                try
                {
                    colType = (NetEspStream.NET_DATA_TYPE_T)Enum.Parse(
                        typeof(NetEspStream.NET_DATA_TYPE_T),
                        schema.get_column_type((uint)i, espError).ToString());

                    colName = schema.get_column_name((uint)i, espError);
                }
                catch(Exception ex)
                {
                    Log.Exception(ex);
                }
                if (string.IsNullOrEmpty(colName))
                {
                    Log.Warning(Resources.LogBlankColumnName);
                    continue;
                }
                if (!table.ContainsColumn(colName))
                {
                    continue;
                }

                object value = null;
                ColumnType columnType;
                bool hasValue = row.is_null(i) == 0;

                switch (colType)
                {
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_STRING:
                        if (hasValue)
                        {
                            value = row.get_string(i, espError);
                        }
                        columnType = ColumnType.Text;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_BOOLEAN:
                        if (hasValue)
                        {
                            int boolVal = row.get_boolean(i, espError);
                            value = Convert.ToBoolean(boolVal).ToString();
                        }
                        columnType = ColumnType.Text;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_BINARY:
                        if (hasValue)
                        {
                            uint buffersize = 256;
                            // TODO: To use ToString below will result in just the class name. Leaving it for now.
                            value = row.get_binary(i, buffersize, espError).ToString();
                        }
                        columnType = ColumnType.Text;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_INTERVAL:
                        if (hasValue)
                        {
                            value = row.get_interval(i, espError).ToString();
                        }
                        columnType = ColumnType.Text;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_INTEGER:
                        if (hasValue)
                        {
                            value = row.get_integer(i, espError);
                        }
                        columnType = ColumnType.Numeric;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_FLOAT:
                        if (hasValue)
                        {
                            value = row.get_float(i, espError);
                        }
                        columnType = ColumnType.Numeric;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_LONG:
                        if (hasValue)
                        {
                            value = row.get_long(i, espError);
                        }
                        columnType = ColumnType.Numeric;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY:
                        if (hasValue)
                        {
                            NetEspMoney moneycell = row.get_legacy_money(i, espError);
                            long prec = moneycell.get_precision(espError);
                            long moneylong = moneycell.get_long(espError);
                            value = Convert.ToDouble(moneylong)/
                                    Math.Pow(10, Convert.ToDouble(prec));
                        }
                        columnType = ColumnType.Numeric;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY01:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY02:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY03:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY04:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY05:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY06:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY07:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY08:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY09:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY10:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY11:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY12:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY13:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY14:
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MONEY15:
                        if (hasValue)
                        {
                            NetEspMoney money1cell = row.get_money(i, espError);
                            long money1long = money1cell.get_long(espError);
                            long precision = money1cell.get_precision(espError);
                            value = Convert.ToDouble(money1long)/
                                    Math.Pow(10, Convert.ToDouble(precision));
                        }
                        columnType = ColumnType.Numeric;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_SECONDDATE:
                        if (hasValue)
                        {
                            double dateValue = row.get_seconddate(i, espError);
                            value = ConvertFromUnixTimestamp(dateValue);
                        }
                        columnType = ColumnType.Time;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_BIGDATETIME:
                        if (hasValue)
                        {
                            NetEspBigDatetime bigDt = row.get_bigdatetime(i, espError);
                            long microSeconds = bigDt.get_microseconds(espError);
                            double dateValue = TimeSpan.FromMilliseconds(
                                microSeconds/1000).TotalSeconds;
                            value = ConvertFromUnixTimestamp(dateValue);
                        }
                        columnType = ColumnType.Time;
                        break;
                    case NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MSDATE:
                        if (hasValue)
                        {
                            double timestamp = row.get_msdate(i, espError);
                            double dateValue = TimeSpan.FromMilliseconds(
                                timestamp).TotalSeconds;
                            value = ConvertFromUnixTimestamp(dateValue);
                        }
                        columnType = ColumnType.Time;
                        break;
                    default:
                        if (hasValue)
                        {
                            value = row.get_string(i, espError);
                        }
                        columnType = ColumnType.Text;
                        break;
                }
                if(value is DateTime)
                {
                    DateTime dateValue = (DateTime)value;

                    // Set Kind property to Unspecified, so TimeZoneHelper always
                    // applies select TimeZone. And convert the local date value to UTC.
                    if (dateValue.Kind == DateTimeKind.Local)
                    {
                        dateValue = DateTime.SpecifyKind(dateValue,
                        DateTimeKind.Unspecified);
                        dateValue = TimeZoneHelper.ConvertToUTC(dateValue,
                            TimeZoneInfo.Local.Id);
                    }

                    dateValue =
                     settings.TimeZoneHelper.ConvertFromUTC(dateValue);

                    espValue.Value = dateValue;
                }
                else
                {
                    espValue.Value = value;
                }

                espValue.Type = columnType;
                tupple.Values.Add(colName, espValue);
            }

            return tupple;
        }

        [HandleProcessCorruptedStateExceptions]
        internal static void UpdateIdCandidates(NetEspSchema schema,
            SybaseESPSettings settings)
        {
            try
            {

                if (schema != null)
                {
                    int colCount = schema.get_numcolumns();
                    List<string> colnames = schema.get_column_names();
                    List<int> coltypes = schema.get_column_types();

                    List<string> idCandidates = new List<string>();
                    List<string> timeIdCandidates = new List<string>();

                    for (int i = 0; i < colCount; i++)
                    {

                        if (coltypes[i] == Convert.ToInt32(
                            NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_STRING))
                        {
                            idCandidates.Add(colnames[i]);
                        }
                        else if (coltypes[i] ==
                            Convert.ToInt32(
                                NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_SECONDDATE) ||
                            coltypes[i] == Convert.ToInt32(
                            NetEspStream.NET_DATA_TYPE_T.NET_ESP_DATATYPE_MSDATE))
                        {
                            timeIdCandidates.Add(colnames[i]);
                        }
                    }

                    settings.IdColumnCandidates =
                        idCandidates.Count > 0 ? idCandidates.ToArray() : null;
                    settings.TimeIdColumnCandidates =
                        timeIdCandidates.Count > 0 ? timeIdCandidates.ToArray() : null;
                }
            }
            catch (Exception ex)
            {
                settings.ErrorReporter.Report(
                    Properties.Resources.UiConnectionWindowTitle,
                    ex.Message);
            }
        }

        internal static bool IsSettingsValid(SybaseESPSettings settings)
        {
            settings.IdColumnCandidates = null;
            settings.TimeIdColumnCandidates = null;

            if (string.IsNullOrEmpty(settings.Host) ||
                        string.IsNullOrEmpty(settings.Workspace) ||
                        string.IsNullOrEmpty(settings.Application))
            {
                return false;
            }

            if (settings.ConnectionMode == ConnectionMode.Query &&
                (settings.Query == null || settings.Query.Length <= 0))
            {
                return false;
            }

            if (settings.ConnectionMode == ConnectionMode.Stream &&
                (settings.Stream == null || settings.Stream.Length <= 0))
            {
                return false;
            }

            return true;
        }

        public static string ApplyParameters(string query,
            IEnumerable<ParameterValue> parameters, 
            bool encloseParameterInQuote = true)
        {
            if (parameters == null) return query;
            return new ParameterEncoder
            {
                SourceString = query,
                Parameters = parameters,
                EnforceSingleQuoteEnclosure = encloseParameterInQuote,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }


    }
}
