﻿using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.GoogleAnalyticsPlugin
{
    public class GADatabaseColumn : DatabaseColumn
    {
        public string Type { get; set; }
        public new string DisplayName { get; set; }
    }
}
