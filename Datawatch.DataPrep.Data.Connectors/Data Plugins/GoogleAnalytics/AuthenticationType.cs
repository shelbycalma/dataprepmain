﻿namespace Panopticon.GoogleAnalyticsPlugin
{
    public enum AuthenticationType
    {
        Service, OAuth
    }
}
