﻿using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.GoogleAnalyticsPlugin
{
    //TODO: This should be inherited from DatabaseColumnProperties
    internal class GADatabaseColumnProperties
    {
        public readonly SharedStringProperty Type;
        public readonly SharedStringProperty DisplayName;
        public readonly SharedStringProperty ColumnName;
        public readonly SharedEnumProperty<ColumnType> ColumnType;
        public readonly SharedStringProperty AppliedParameterName;
        public readonly SharedEnumProperty<AggregateType> AggregateType;

        public GADatabaseColumnProperties(string rootPath, int columnIndex)
        {
            string propPrefix = rootPath + ".Column_" + columnIndex;

            Type = new SharedStringProperty(propPrefix + ".Type");
            DisplayName = new SharedStringProperty(propPrefix + ".DisplayName");
            ColumnName = new SharedStringProperty(propPrefix + ".ColumnName");
            ColumnType = new SharedEnumProperty<ColumnType>(propPrefix +
                ".ColumnType");
            AppliedParameterName = new SharedStringProperty(propPrefix +
                ".AppliedParameterName");
            AggregateType = new SharedEnumProperty<AggregateType>(propPrefix +
                ".AggregateType");
        }
    }
}
