﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.GoogleAnalyticsPlugin
{
    /// <summary>
    /// DataPlugin for Google Analytics.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<GASettings>
    {
        internal const string PluginId = "GoogleAnalyticsPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Google Analytics";
        internal const string PluginType = DataPluginTypes.Database;
        public const string PathKey = "KeyFilePath";

        protected bool canSelectColumns = true;

        private readonly Executor executor;

        public Plugin()
        {
            canSelectColumns = true;
            executor = new Executor();
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override GASettings CreateSettings(PropertyBag bag)
        {
            GASettings settings = new GASettings(bag, null);
            if(string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = Properties.Resources.DefaultConnectionTitle;
            }
            return new GASettings(bag, null);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            CheckLicense();

            GASettings connectionSettings =
                new GASettings(settings, parameters);

            if (applyRowFilteration)
                connectionSettings.MaxResults = rowcount;
            else
                rowcount = 0;

            return executor.GetData(connectionSettings, parameters, rowcount);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            if (settings.Values.ContainsKey(PathKey))
            {
                string path = settings.Values[PathKey];
                return new[] { path };
            }
            return null;
        }

        public bool IsOnDemand(PropertyBag settings)
        {
            return false;
        }

        public override void UpdateFileLocation(PropertyBag settings,
                    string oldPath, string newPath, string workbookPath)
        {
            if (Path.GetFileName(settings.Values[PathKey]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values[PathKey] =
                    Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }
    }
}
