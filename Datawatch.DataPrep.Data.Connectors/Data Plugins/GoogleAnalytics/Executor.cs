﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Google.Apis.Analytics.v3;

namespace Panopticon.GoogleAnalyticsPlugin
{
    class Executor
    {
        /// <summary>
        /// Runs a regular (non-on-demand) query, and returns the result.
        /// </summary>
        /// 

        private List<int> columnOrder = new List<int>();
        public ITable GetData(GASettings settings,
            IEnumerable<ParameterValue> parameters, int rowcount)
        {
            // Log the query and time the execution.
            //Log.Info(Resources.LogExecutingQuery, requestId, query);
            DateTime start = DateTime.Now;

            StandaloneTable table = null;

            AnalyticsService service =
                Helper.GAHelper.Authenticate(settings, parameters);

            var data =
                Helper.GAReportingHelper.GetData(service, settings, parameters, rowcount);

            table = CreateStandaloneTable(settings);

            table.BeginUpdate();

            string[] rowOrder = new string[columnOrder.Count];
            int i = 0;
            foreach (List<string> row in data.Rows)
            {
                i = 0;
                foreach (int ctr in columnOrder)
                {
                    rowOrder[ctr] = row[i].ToString();
                    i++;
                }
                table.AddRow(rowOrder);
            }
            
            table.CanFreeze = true;
            table.EndUpdate();
            return table;
        }

        public StandaloneTable CreateStandaloneTable(GASettings settings)
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();
            try
            {
                List<GADatabaseColumn> allColumns = new List<GADatabaseColumn>();
                List<GADatabaseColumn> orderColumns = new List<GADatabaseColumn>();
                allColumns.AddRange(settings.SelectedDimensions);
                allColumns.AddRange(settings.SelectedMetrics);

                //check if from old GA and populate order
                if ((settings.OrderedColumns.Count == 0) && (settings.SelectedDimensions.Count > 0 || settings.SelectedMetrics.Count > 0))
                {
                    settings.OrderedColumns.AddRange(settings.SelectedDimensions);
                    settings.OrderedColumns.AddRange(settings.SelectedMetrics);
                }

                //get order of columns
                columnOrder.Clear();
                foreach (GADatabaseColumn column in allColumns)
                {
                    if (!table.ContainsColumn(column.DisplayName))
                    {
                        int i = settings.OrderedColumns.FindIndex(x => x.ColumnName == column.ColumnName);
                        columnOrder.Add(i);
                    }
                }
                orderColumns.AddRange(settings.OrderedColumns);
                foreach (GADatabaseColumn column in orderColumns)
                {
                    if (!table.ContainsColumn(column.DisplayName))
                    {
                        table.AddColumn(CreateStandaloneColumn(column.DisplayName,
                            column.ColumnType));
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        private Column CreateStandaloneColumn(string name, ColumnType type)
        {
            switch (type)
            {
                case ColumnType.Numeric:
                    return new NumericColumn(name);
                case ColumnType.Time:
                    return new TimeColumn(name);
            }
            return new TextColumn(name);
        }
    }
}
