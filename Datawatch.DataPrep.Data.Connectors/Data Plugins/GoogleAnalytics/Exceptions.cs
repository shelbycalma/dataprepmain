﻿using System;
using Panopticon.GoogleAnalyticsPlugin.Properties;

namespace Panopticon.GoogleAnalyticsPlugin
{
    class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        internal static Exception ConnectionError(Exception innerException)
        {
            return LogException(new InvalidOperationException(
                Resources.ExConnectionError,
                innerException));
        }

        internal static Exception FileNotFound()
        {
            return LogException(new System.IO.FileNotFoundException(
                Properties.Resources.LogKeyFileNotExists));
        }
    }
}
