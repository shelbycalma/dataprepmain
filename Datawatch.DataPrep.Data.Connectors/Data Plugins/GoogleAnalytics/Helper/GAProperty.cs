﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panopticon.GoogleAnalyticsPlugin.Helper
{
    public class GAProperty
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public ICollection<GAView> GAViews
        {
            get;
            set;
        }

    }
}
