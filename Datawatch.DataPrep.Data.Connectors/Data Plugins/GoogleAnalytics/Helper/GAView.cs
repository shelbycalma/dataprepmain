﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panopticon.GoogleAnalyticsPlugin.Helper
{
    public class GAView
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

    }
}
