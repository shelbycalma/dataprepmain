﻿
using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using System.Windows;

namespace Panopticon.GoogleAnalyticsPlugin.Helper
{
    public class GAReportingHelper
    {
        public static bool isConnectionOk(AnalyticsService service,
            GASettings settings, IEnumerable<ParameterValue> parameters)
        {
            DataResource.GaResource.GetRequest request = getRequest(service, settings, 
                parameters);
            try
            {
                GaData result = request.Execute();
            }
            catch (Exception e)
            {
                Log.Exception(e);
                return false;
            }
            return true;
        }

        public static bool ParameterCheck(AnalyticsService service,
            GASettings settings, IEnumerable<ParameterValue> parameters)
        {
            DataResource.GaResource.GetRequest request = getRequest(service, settings,
                parameters);
            try
            {
                GaData result = request.Execute();
            }
            catch (Exception e)
            {
                Log.Exception(e);
                MessageBox.Show(e.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        public static GaData GetData(AnalyticsService service,
            GASettings settings, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            DataResource.GaResource.GetRequest request = getRequest(service, settings, 
                parameters);
            return ProcessResults(request, rowcount);
        }

        private static DataResource.GaResource.GetRequest getRequest(AnalyticsService service,
            GASettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            try
            {
                string startDate =
                    DataUtils.ApplyParameters(settings.StartDate, parameters);
                string endDate =
                    DataUtils.ApplyParameters(settings.EndDate, parameters);
                string profileId =
                    DataUtils.ApplyParameters(settings.ProfileID, parameters);

                Log.Info(Properties.Resources.LogGetDataRequest,
                    Properties.Resources.UiPluginTitle, profileId,
                    startDate, endDate);

                string parameterizedStartDate = startDate;
                string parameterizedEndDate = endDate;
                DateTime outStartDateTime;
                DateTime outEndDateTime;
                if (!DateTime.TryParse(startDate, out outStartDateTime))
                {
                    throw new InvalidCastException(
                        string.Format(Properties.Resources.ExInvalidDate, Properties.Resources.LogStartDate));
                }
                if (!DateTime.TryParse(endDate, out outEndDateTime))
                {
                    throw new InvalidCastException(
                        string.Format(Properties.Resources.ExInvalidDate, Properties.Resources.LogEndDate));
                }

                string selectedMetrics = GetPreparedString(settings.SelectedMetrics);
                DataResource.GaResource.GetRequest request =
                    service.Data.Ga.Get(String.Format("ga:{0}", profileId),
                        parameterizedStartDate, parameterizedEndDate, selectedMetrics);

                request.MaxResults = settings.MaxResults;
                string selectedDimensions = GetPreparedString(settings.SelectedDimensions);
                request.Dimensions = selectedDimensions;
                request.SamplingLevel =
                   DataResource.GaResource.GetRequest.SamplingLevelEnum.DEFAULT__;
                request.Filters = settings.Filters;
                Log.Info(Properties.Resources.LogExecuteDataRequest,
                    Properties.Resources.UiPluginTitle, selectedMetrics,
                    selectedDimensions);
                return request;
            }
            catch (Exception e)
            {
                return null;
            }
        }



        // Just loops though getting all the rows.  
        private static GaData ProcessResults(DataResource.GaResource.GetRequest request, int rowcount)
        {
            GaData result = request.Execute();
            List<IList<string>> allRows = new List<IList<string>>();

            //set initial value of StartIndex
            if (request != null)
                request.StartIndex = 1;

            //// Loop through until we arrive at an empty page
            while (result.Rows != null)
            {
                //Add the rows to the final list
                allRows.AddRange(result.Rows);

                if ((rowcount <= result.Rows.Count) && (rowcount != 0))
                    break;

                // We will know we are on the last page when the next page token is
                // null.
                // If this is the case, break.
                if (result.NextLink == null)
                {
                    break;
                }

                // Prepare the next page of results             
                request.StartIndex = request.StartIndex + request.MaxResults;
                // Execute and process the next page request
                result = request.Execute();

            }
            GaData allData = result;
            allData.Rows = (List<IList<string>>)allRows;
            return allData;
        }

        private static string GetPreparedString(List<GADatabaseColumn> columns)
        {
            string result = string.Empty;
            foreach (GADatabaseColumn column in columns)
            {
                if (!string.IsNullOrEmpty(result))
                {
                    result += ",";
                }
                result += column.ColumnName;

            }
            return result;
        }

    }
}

