﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Panopticon.GoogleAnalyticsPlugin.Helper
{
    public class GAAccount
    {
        public string ID
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public ICollection<GAProperty> Properties
        {
            get;
            set;
        }

    }
}
