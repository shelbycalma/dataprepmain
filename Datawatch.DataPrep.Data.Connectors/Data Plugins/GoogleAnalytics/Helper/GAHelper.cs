﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Google.Apis.Analytics.v3;
using Google.Apis.Analytics.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Security;
using System.Windows;

namespace Panopticon.GoogleAnalyticsPlugin.Helper
{
    public class GAHelper
    {
        private static Columns Columns;
        const int keySize = 256;
        const int blockSize = 128;
        const int iterations = 1000;

        public static AnalyticsService Authenticate(
                        GASettings settings,
                        IEnumerable<ParameterValue> parameters)
        {
            AnalyticsService service;

            string clientId = settings.ClientID;
            string clientSecret = settings.ClientSecret;
            string userName = settings.UserName;
            string serviceAccountEmail = settings.ServiceAccountEmail;
            string keyFilePath = settings.KeyFilePath;
            if (parameters != null)
            {
                clientId = DataUtils.ApplyParameters(clientId, parameters);
                clientSecret = DataUtils.ApplyParameters(clientSecret, parameters);
                userName = DataUtils.ApplyParameters(userName, parameters);
                serviceAccountEmail = DataUtils.ApplyParameters(serviceAccountEmail, parameters);
                keyFilePath = DataUtils.ApplyParameters(keyFilePath, parameters);
            }

            if (settings.AuthenticationType == AuthenticationType.OAuth)
            {
                service = AuthenticateOauth(clientId, clientSecret, userName);
            }
            else
            {
                service =
                    AuthenticateServiceAccount(serviceAccountEmail, keyFilePath);
            }
            return service;
        }

        private static AnalyticsService AuthenticateOauth(string clientId,
            string clientSecret, string userName)
        {
            Log.Info(Properties.Resources.LogOAuthRequest,
                Properties.Resources.UiPluginTitle, clientId, userName);
            string[] scopes = new string[] { AnalyticsService.Scope.Analytics,  // view and manage your analytics data
                                             AnalyticsService.Scope.AnalyticsEdit,  // edit management actives
                                             AnalyticsService.Scope.AnalyticsManageUsers,   // manage users
                                             AnalyticsService.Scope.AnalyticsReadonly};     // View analytics data

            //TODO: FileDataStore can be configured. By default,
            //it makes a folder with the given name in appdata/roaming folder
            UserCredential credential =
                GoogleWebAuthorizationBroker.AuthorizeAsync(
                    new ClientSecrets { ClientId = clientId, ClientSecret = clientSecret },
                    scopes, userName, CancellationToken.None,
                    new FileDataStore("Datawatch.GoogleAnalytics.Auth.Store")).Result;

            AnalyticsService service =
                new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = Properties.Resources.UiPluginTitle,
                });
            return service;
        }

        private static AnalyticsService AuthenticateServiceAccount(
            string serviceAccountEmail, string keyFilePath)
        {
            Log.Info(Properties.Resources.LogServiceAcRequest,
                Properties.Resources.UiPluginTitle, serviceAccountEmail, keyFilePath);

            string[] scopes = new string[] { AnalyticsService.Scope.Analytics,  // view and manage your analytics data
                                             AnalyticsService.Scope.AnalyticsEdit,  // edit management actives
                                             AnalyticsService.Scope.AnalyticsManageUsers,   // manage users
                                             AnalyticsService.Scope.AnalyticsReadonly};     // View analytics data       
            
            string notASecret = SecurityUtils.SALTDecrypt(Properties.Resources.NotASecret, "", keySize, blockSize, iterations);

            // check the file exists
            if (!File.Exists(keyFilePath))
            {
                Log.Error(Properties.Resources.LogKeyFileNotExists);
                throw Exceptions.FileNotFound();
            }
            else if (!GAChecker.ValidatePath(ref keyFilePath, "", ".p12"))
            {
                throw Exceptions.FileNotFound();
            }

            var certificate = new X509Certificate2(keyFilePath, notASecret, X509KeyStorageFlags.Exportable);

            ServiceAccountCredential credential = new ServiceAccountCredential(
                new ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    Scopes = scopes
                }.FromCertificate(certificate));

            // Create the service.
            AnalyticsService service =
                new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = Properties.Resources.UiPluginTitle,
                });
            return service;
        }

        public static Goals GetGoals(GASettings settings)
        {
            AnalyticsService service1 = Authenticate(settings,
                settings.Parameters);
            Goals goals = service1.Management.Goals.List(settings.AccountID, settings.PropertyID, settings.ProfileID).Execute();

            return goals;
        }

            public static Columns LoadColumns(GASettings settings)
        {
            //TODO:- rethink of caching of columns
            if (Columns != null)
            {
                return Columns;
            }
            try
            {
                AnalyticsService service = Authenticate(settings,
                    settings.Parameters);
                Google.Apis.Analytics.v3.MetadataResource metadataResource =
                    service.Metadata;
                MetadataResource.ColumnsResource.ListRequest listRequest =
                    metadataResource.Columns.List("ga");
                Columns = listRequest.Execute();
                //TODO: -cache the columns with key and time to live
                //    Columns = listRequest.Execute();
                //Columns = GetMockedColumns();
            }
            catch (OperationCanceledException ex)
            {
                Log.Exception(ex);
                throw new Exception(Properties.Resources.ExLoadColumnException, ex);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                throw ex;
            }
            return Columns;
        }

        public IList<string> LoadCategories(GASettings settings)
        {
            LoadColumns(settings);
            //TODO:- improvise linq call to generic to be at par in java
            return Columns.Items.Select(item => item.Attributes["group"])
                .Distinct().ToList<string>();
        }

        public static List<GAAccount> LoadGAAccounts(GASettings settings)
        {
            try
            {
                AnalyticsService service = Authenticate(settings,
                        settings.Parameters);
                ManagementResource.AccountSummariesResource.ListRequest list =
                    service.Management.AccountSummaries.List();
                ManagementResource.AccountsResource.ListRequest request = service.Management.Accounts.List();
                Accounts result = request.Execute();

                AccountSummaries feed = list.Execute();
                List<AccountSummary> allRows = new List<AccountSummary>();
                while (feed.Items != null)
                {
                    allRows.AddRange(feed.Items);
                    if (feed.NextLink == null)
                        break;
                    list.StartIndex = feed.StartIndex + list.MaxResults;
                    feed = list.Execute();
                }
                feed.Items = allRows;

                List<GAAccount> gaAccounts = new List<GAAccount>();
                foreach (AccountSummary account in feed.Items)
                {
                    List<GAProperty> gaProperties = new List<GAProperty>();
                    foreach (WebPropertySummary wp in account.WebProperties)
                    {
                        List<GAView> gaViews = new List<GAView>();
                        if (wp.Profiles != null)
                        {
                            foreach (ProfileSummary profile in wp.Profiles)
                            {
                                GAView gaView = new GAView();
                                gaView.Name = profile.Name;
                                gaView.ID = profile.Id;
                                gaViews.Add(gaView);
                            }
                        }
                        GAProperty gaProperty = new GAProperty();
                        gaProperty.Name = wp.Name;
                        gaProperty.ID = wp.Id;
                        gaProperty.GAViews = gaViews;
                        gaProperties.Add(gaProperty);
                    }
                    GAAccount gaAccount = new GAAccount();
                    gaAccount.ID = account.Id;
                    gaAccount.Name = account.Name;
                    gaAccount.Properties = gaProperties;
                    gaAccounts.Add(gaAccount);
                }
                return (gaAccounts);
            }
            catch (Exception e)
            {
                Log.Exception(e);
                throw (e);
            }
        }

        public static ColumnType GetDataType(string dataType)
        {
            switch (dataType)
            {
                case "STRING":
                    return ColumnType.Text;
                case "INTEGER":
                case "FLOAT":
                case "PERCENT":
                case "CURRENCY":
                case "TIME":
                    return ColumnType.Numeric;
                default:
                    return ColumnType.Text;
            }

        }
    }
}
