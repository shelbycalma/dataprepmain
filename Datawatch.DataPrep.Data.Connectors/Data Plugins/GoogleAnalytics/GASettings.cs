﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Google.Apis.Analytics.v3;
using Panopticon.GoogleAnalyticsPlugin.Helper;

namespace Panopticon.GoogleAnalyticsPlugin
{
    public class GASettings : ConnectionSettings
    {
        private const string selectedDimensionsKey = "SelectedDimensions";
        private const string selectedMetricsKey = "SelectedMetrics";
        private const string selectedColumnKey = "SelectedColumns";
        private IEnumerable<ParameterValue> parameters;

        private List<GADatabaseColumn> selectedDimensions = null;
        private List<GADatabaseColumn> selectedMetrics = null;
        private List<GADatabaseColumn> orderedColumns = null;
        private GAAccount selectedGAAccount = null;
        private GAProperty selectedGAProperty = null;
        private GAView selectedGAView = null;
        private PropertyBag propertyBag;

        public GASettings()
        {

        }

        public GASettings(PropertyBag bag, IEnumerable<ParameterValue> parameters)
            : base(bag)
        {
            this.propertyBag = bag;
            this.parameters = parameters;
        }

        public AuthenticationType AuthenticationType
        {
            get
            {
                return (AuthenticationType)Enum.Parse(typeof(AuthenticationType),
                    GetInternal("AuthenticationType", "Service"));
            }
            set
            {
                SetInternal("AuthenticationType", value.ToString());
            }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
            set { parameters = value; }
        }

        public string UserName
        {
            get
            {
                return GetInternal("UserName");
            }
            set
            {
                SetInternal("UserName", value);
            }
        }
        public string ClientID
        {
            get
            {
                return GetInternal("ClientID");
            }
            set
            {
                SetInternal("ClientID", value);
            }
        }

        public string ClientSecret
        {
            get
            {
                return GetInternal("ClientSecret");
            }
            set
            {
                SetInternal("ClientSecret", value);
            }
        }

        public string ServiceAccountEmail
        {
            get
            {
                return GetInternal("ServiceAccountEmail");
            }
            set
            {
                SetInternal("ServiceAccountEmail", value);
            }
        }

        public string KeyFilePath
        {
            get
            {
                return GetInternal("KeyFilePath");
            }
            set
            {
                SetInternal("KeyFilePath", value);
            }
        }

        public string ProfileID
        {
            get
            {
                return GetInternal("ProfileID");
            }
            set
            {
                SetInternal("ProfileID", value);
            }
        }

        public string AccountID
        {
            get
            {
                return GetInternal("AccountID");
            }
            set
            {
                SetInternal("AccountID", value);
            }
        }

        public string PropertyID
        {
            get
            {
                return GetInternal("PropertyID");
            }
            set
            {
                SetInternal("PropertyID", value);
            }
        }

        public string StartDate
        {
            get
            {
                return GetInternal("StartDate",
                    DateTime.Now.AddMonths(-1).ToString("yyyy-MM-dd"));
            }
            set
            {
                SetInternal("StartDate", value);
            }
        }

        public string EndDate
        {
            get
            {
                return GetInternal("EndDate",
                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"));
            }
            set
            {
                SetInternal("EndDate", value);
            }
        }

        public string Filters
        {
            get
            {
                return GetInternal("Filters");
            }
            set
            {
                SetInternal("Filters", value);
            }
        }

        public string CategoriesFilter
        {
            get
            {
                return GetInternal("CategoriesFilter");
            }
            set
            {
                SetInternal("CategoriesFilter", value);
            }
        }

        public string DimensionsFilter
        {
            get
            {
                return GetInternal("DimensionsFilter");
            }
            set
            {
                SetInternal("DimensionsFilter", value);
            }
        }

        public string MetricsFilter
        {
            get
            {
                return GetInternal("MetricsFilter");
            }
            set
            {
                SetInternal("MetricsFilter", value);
            }
        }

        public int MaxResults
        {
            get
            {
                return int.Parse(GetInternal("MaxResults", "1000"));
            }
            set
            {
                SetInternal("MaxResults", value.ToString());
            }
        }

        public string Fields
        {
            get
            {
                return GetInternal("Fields");
            }
            set
            {
                SetInternal("Fields", value);
            }
        }
        
        public bool validated
        {
            get
            {
                string s = GetInternal("validated");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("validated",
                    Convert.ToString(value));
            }
        }
        //TODO: not used now.
        public bool ShowAllowedInSegmentOnly
        {
            get
            {
                string s = GetInternal("ShowAllowedInSegmentOnly");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("ShowAllowedInSegmentOnly",
                    Convert.ToString(value));
            }
        }

        public bool IsOk
        {
            get
            {
                if (!CanFetchDimensionsAndMetrics()) return false;

                if (((SelectedDimensions == null || SelectedDimensions.Count <= 0)
                     && (SelectedMetrics == null || SelectedMetrics.Count <= 0)))
                {
                    return false;
                }
                AnalyticsService service;
                try
                {
                     service = GAHelper.Authenticate(this, parameters);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return false;
                }
                return GAReportingHelper.isConnectionOk(service, this, Parameters);
            }
        }

        public bool IsParamOk
        {
            get
            {
                if (!CanFetchDimensionsAndMetrics()) return false;

                if (((SelectedDimensions == null || SelectedDimensions.Count <= 0)
                     && (SelectedMetrics == null || SelectedMetrics.Count <= 0)))
                {
                    return false;
                }
                AnalyticsService service;
                try
                {
                    service = GAHelper.Authenticate(this, parameters);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    return false;
                }
                return GAReportingHelper.ParameterCheck(service, this, Parameters);
            }
        }

        public List<GADatabaseColumn> SelectedDimensions
        {
            get
            {
                if (selectedDimensions != null)
                {
                    return selectedDimensions;
                }
                selectedDimensions = new List<GADatabaseColumn>();
                PropertyBag dimensionsBag =
                    propertyBag.SubGroups[selectedDimensionsKey];
                if (dimensionsBag != null &&
                    dimensionsBag.SubGroups.Count > 0)
                {
                    selectedDimensions = PopulateColumns(
                        propertyBag, selectedDimensionsKey,
                        dimensionsBag.SubGroups.Count);
                }
                return selectedDimensions;
            }
        }

        public List<GADatabaseColumn> SelectedMetrics
        {
            get
            {
                if (selectedMetrics != null)
                {
                    return selectedMetrics;
                }
                selectedMetrics = new List<GADatabaseColumn>();
                PropertyBag metricsBag =
                    propertyBag.SubGroups[selectedMetricsKey];
                if (metricsBag != null &&
                    metricsBag.SubGroups.Count > 0)
                {
                    selectedMetrics = PopulateColumns(
                        propertyBag, selectedMetricsKey,
                        metricsBag.SubGroups.Count);
                }
                return selectedMetrics;
            }
        }

        public List<GADatabaseColumn> OrderedColumns
        {
            get
            {
                if (orderedColumns != null) 
                {
                    return orderedColumns;
                }
                //if ((orderedColumns == null) && ((selectedMetrics != null) || (selectedDimensions != null)))
                //{
                //    orderedColumns = new List<GADatabaseColumn>();
                //    orderedColumns.AddRange(selectedDimensions);
                //    orderedColumns.AddRange(selectedMetrics);
                //    PropertyBag columnBag =
                //        propertyBag.SubGroups[selectedColumnKey];
                //    if (columnBag != null &&
                //        columnBag.SubGroups.Count > 0)
                //    {
                //        orderedColumns = PopulateColumns(
                //            propertyBag, selectedColumnKey,
                //            columnBag.SubGroups.Count);
                //    }
                //}
                //else
                {
                    orderedColumns = new List<GADatabaseColumn>();
                    PropertyBag columnBag =
                        propertyBag.SubGroups[selectedColumnKey];
                    if (columnBag != null &&
                        columnBag.SubGroups.Count > 0)
                    {
                        orderedColumns = PopulateColumns(
                            propertyBag, selectedColumnKey,
                            columnBag.SubGroups.Count);
                    }
                }
                return orderedColumns;
            }
        }

        public void ClearColumns()
        {
            SelectedMetrics.Clear();
            SelectedDimensions.Clear();
            OrderedColumns.Clear();
        }

        public void DeleteColumn(GADatabaseColumn column)
        {
            if (column == null) return;
            if (column.Type.Equals("DIMENSION"))
                SelectedDimensions.RemoveAt(SelectedDimensions.FindIndex(x => x.ColumnName==column.ColumnName));
            if (column.Type.Equals("METRIC"))
                SelectedMetrics.RemoveAt(SelectedMetrics.FindIndex(x => x.ColumnName == column.ColumnName));
            OrderedColumns.Remove(column);
        }

        private List<GADatabaseColumn> PopulateColumns(PropertyBag bag,
            string subGroupKey, int count)
        {
            List<GADatabaseColumn> columns = new List<GADatabaseColumn>();

            for (int i = 0; i < count; i++)
            {
                GADatabaseColumnProperties colProperties =
                    new GADatabaseColumnProperties(subGroupKey, i);
                GADatabaseColumn column = new GADatabaseColumn()
                {
                    Type = colProperties.Type.GetString(bag),
                    DisplayName = colProperties.DisplayName.GetString(bag),
                    ColumnName = colProperties.ColumnName.GetString(bag),
                    ColumnType = colProperties.ColumnType.GetValue(bag),
                    AppliedParameterName = colProperties.AppliedParameterName
                        .GetString(bag),
                };
                columns.Add(column);
            }
            return columns;
        }

        public bool CanGetAccounts()
        {
            return !string.IsNullOrEmpty(ServiceAccountEmail) &&
                !string.IsNullOrEmpty(KeyFilePath);
        }

        public bool CanFetchDimensionsAndMetrics()
        {
            if (AuthenticationType == AuthenticationType.OAuth)
            {
                return !string.IsNullOrEmpty(ClientID) &&
                    !string.IsNullOrEmpty(ClientSecret) &&
                    !string.IsNullOrEmpty(ProfileID);
            }
            else
            {
                return !string.IsNullOrEmpty(ServiceAccountEmail) &&
                    !string.IsNullOrEmpty(KeyFilePath) &&
                    !string.IsNullOrEmpty(ProfileID);
            }
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag propertyBag = base.ToPropertyBag();
            
            AddColumnsToBag(selectedDimensions,
                propertyBag, selectedDimensionsKey);
            AddColumnsToBag(selectedMetrics,
                propertyBag, selectedMetricsKey);
            AddColumnsToBag(orderedColumns,
                propertyBag, selectedColumnKey);

            return propertyBag;
        }

        private void AddColumnsToBag(List<GADatabaseColumn> columns,
            PropertyBag propertyBag, string bagSubGroupKey)
        {
            if (columns == null) return;
            propertyBag.SubGroups[bagSubGroupKey] = null;
            int i = 0;
            foreach (GADatabaseColumn col in columns)
            {
                GADatabaseColumnProperties colProperties =
                    new GADatabaseColumnProperties(bagSubGroupKey, i++);
                colProperties.Type.SetString(propertyBag,
                   col.Type);
                colProperties.DisplayName.SetString(propertyBag,
                   col.DisplayName);
                colProperties.ColumnName.SetString(propertyBag,
                    col.ColumnName);
                colProperties.ColumnType.SetValue(propertyBag,
                    col.ColumnType);
                colProperties.AppliedParameterName.SetString(propertyBag,
                    col.AppliedParameterName);
                colProperties.AggregateType.SetValue(propertyBag,
                    col.AggregateType);
            }
        }
    }
}
