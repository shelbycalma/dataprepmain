﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Panopticon.MDXPlugin.Converters
{
    public class ParametrizationVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !(value is ParameteredDimension) ? Visibility.Visible : Visibility.Hidden;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
