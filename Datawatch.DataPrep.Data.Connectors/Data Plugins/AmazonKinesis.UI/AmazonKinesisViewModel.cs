﻿using System.Collections.Generic;
using Amazon;
using Amazon.Kinesis;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.AmazonKinesisPlugin.UI
{
    internal class AmazonKinesisViewModel : ViewModelBase
    {
        private AmazonKinesisSettings settings;
        private bool isStartingShardTextBoxEnabled = false;
        private List<string> textStreams;

        public AmazonKinesisViewModel(AmazonKinesisSettings settings)
        {
            if (settings == null)
            {
                throw Exceptions.ArgumentNull("Settings");
            }
            this.settings = settings;
            this.settings.PropertyChanged += settings_PropertyChanged;
        }

        void settings_PropertyChanged(object sender, 
            System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("AccessKeyID") ||
                e.PropertyName.Equals("SecretAccessKey") ||
                e.PropertyName.Equals("RegionEndpoint"))
            {
                OnPropertyChanged("CanRetrieveStreams");
            }
        }

        public bool IsStartingShardTextBoxEnabled
        {
            get { return isStartingShardTextBoxEnabled; }
            set
            {
                isStartingShardTextBoxEnabled = value;
                OnPropertyChanged("IsStartingShardTextBoxEnabled");
            }
        }

        public bool CanRetrieveStreams
        {
            get
            {
                return
                    !string.IsNullOrEmpty(Settings.RegionEndpoint) &&
                    !string.IsNullOrEmpty(Settings.AccessKeyID) &&
                    !string.IsNullOrEmpty(Settings.SecretAccessKey);
            }
        }

        public List<string> TextStreams
        {
            get
            {
                if (textStreams == null)
                {
                    textStreams = new List<string>();
                }
                return textStreams;
            }
            set
            {
                textStreams = value;
                OnPropertyChanged("TextStreams");
            }
        }

        public IEnumerable<string> RegionEndpoints
        {
            get
            {
                List<string> regionEndpoints = new List<string>();
                foreach (RegionEndpoint rep in RegionEndpoint.EnumerableAllRegions)
                {
                    regionEndpoints.Add(rep.SystemName);
                }
                return regionEndpoints;
            }
        }

        public IEnumerable<string> ShardIteratorTypes
        {
            get
            {
                List<string> types = new List<string>();
                types.Add(ShardIteratorType.TRIM_HORIZON.ToString());
                types.Add(ShardIteratorType.AT_SEQUENCE_NUMBER.ToString());
                types.Add(ShardIteratorType.AFTER_SEQUENCE_NUMBER.ToString());
                types.Add(ShardIteratorType.LATEST.ToString());
                return types;
            }
        }

        public AmazonKinesisSettings Settings
        {
            get { return settings; }
        }

        public void RetrieveStreamList()
        {
            TextStreams.Clear();
            AmazonKinesisHelper helper = new AmazonKinesisHelper(settings);
            TextStreams = helper.GetStreams() as List<string>;
        }

        public bool IsOk()
        {
            if (Settings == null)
                return false;
            if (Settings.ParserSettings.ColumnCount == 0)
                return false;
            if (!Settings.IsParserSettingsOk)
                return false;

            bool ret = (!string.IsNullOrEmpty(Settings.AccessKeyID))
                       && (!string.IsNullOrEmpty(Settings.SecretAccessKey))
                       && (!string.IsNullOrEmpty(Settings.RecordLimit))
                       && (!string.IsNullOrEmpty(Settings.RegionEndpoint))
                       && (!string.IsNullOrEmpty(Settings.ShardIteratorType))
                       && (!string.IsNullOrEmpty(Settings.Stream));
            if (Settings.ShardIteratorType == 
                Amazon.Kinesis.ShardIteratorType.AT_SEQUENCE_NUMBER ||
                Settings.ShardIteratorType == 
                Amazon.Kinesis.ShardIteratorType.AFTER_SEQUENCE_NUMBER)
            {
                return ret && (!string.IsNullOrEmpty(Settings.SequenceNumber));
            }
            return ret;
        }

        public void CheckIfIteratorNeedsStartingShard()
        {
            switch (Settings.ShardIteratorType)
            {
                case "AT_SEQUENCE_NUMBER":
                case "AFTER_SEQUENCE_NUMBER":
                    IsStartingShardTextBoxEnabled = true;
                    break;
                case "TRIM_HORIZON":
                case "LATEST":
                    IsStartingShardTextBoxEnabled = false;
                    break;
                default: //Other text entry - e.g. parameterization
                    IsStartingShardTextBoxEnabled = true;
                    break;
            }
        }
    }
}