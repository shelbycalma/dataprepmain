﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.AmazonKinesisPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof (ConnectionWindow));

        public ConnectionWindow(AmazonKinesisSettings settings)
        {
            InitializeComponent();

            ConnectionPanel.ViewModel = new AmazonKinesisViewModel(settings);
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = false;
            if (ConnectionPanel.ViewModel != null)
            {
                e.CanExecute = ConnectionPanel.ViewModel.IsOk();
            }
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}