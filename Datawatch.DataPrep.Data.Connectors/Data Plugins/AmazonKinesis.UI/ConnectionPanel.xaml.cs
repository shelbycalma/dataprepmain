﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.AmazonKinesisPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private AmazonKinesisViewModel viewModel;
        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(AmazonKinesisSettings settings)
        {
            InitializeComponent();
            if (settings != null)
            {
                ViewModel = new AmazonKinesisViewModel(settings);
            }
        }

        public AmazonKinesisViewModel ViewModel
        {
            get { return viewModel; }
            set 
            {
                this.DataContext = viewModel = value;
                InitializeViewModel(value);
            }
        }

        private void InitializeViewModel(AmazonKinesisViewModel viewModel)
        {
            if (!string.IsNullOrEmpty(viewModel.Settings.SecretAccessKey)
                && viewModel.Settings.IsPasswordParameterized == false)
            {
                PasswordBoxSecretAccessKey.Password =
                    viewModel.Settings.SecretAccessKey;
            }
            else if (!string.IsNullOrEmpty(viewModel.Settings.SecretAccessKey)
                     && viewModel.Settings.IsPasswordParameterized)
            {
                TextBoxSecretAccessKey.Text =
                    viewModel.Settings.SecretAccessKey;
            }

            viewModel.CheckIfIteratorNeedsStartingShard();
            viewModel.RetrieveStreamList();
        }

        private static void SettingsChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConnectionPanel) obj).OnSettingsChanged(
                (AmazonKinesisViewModel) args.OldValue,
                (AmazonKinesisViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(
            AmazonKinesisViewModel oldModel, AmazonKinesisViewModel newModel)
        {
            DataContext = newModel;
        }

       
        private void RetrieveStreamsButton_OnClick(
            object sender, RoutedEventArgs e)
        {
            ViewModel.TextStreams.Clear();

            if (!string.IsNullOrEmpty(ViewModel.Settings.AccessKeyID)
                && !string.IsNullOrEmpty(ViewModel.Settings.SecretAccessKey)
                && !string.IsNullOrEmpty(ViewModel.Settings.RegionEndpoint))
            {
                ViewModel.RetrieveStreamList();
            }
        }

        private void SecretAccessKeyPasswordChanged(
            object sender, RoutedEventArgs e)
        {
            var key = PasswordBoxSecretAccessKey.Password;
            if (!string.IsNullOrEmpty(key))
            {
                ViewModel.Settings.SecretAccessKey = key;
            }
        }

        private void SecretAccessKeyTextChanged(
            object sender, TextChangedEventArgs e)
        {
            var key = TextBoxSecretAccessKey.Text;
            if (!string.IsNullOrEmpty(key))
            {
                ViewModel.Settings.SecretAccessKey = key;
            }
        }

        private void ParamPasswordOnChecked(object sender, RoutedEventArgs e)
        {
            var key = TextBoxSecretAccessKey.Text;
            if (!string.IsNullOrEmpty(key))
            {
                ViewModel.Settings.SecretAccessKey = key;
            }
        }

        private void ParamPasswordOnUnchecked(object sender, RoutedEventArgs e)
        {
            var key = PasswordBoxSecretAccessKey.Password;
            if (!string.IsNullOrEmpty(key))
            {
                ViewModel.Settings.SecretAccessKey = key;
            }
        }

        private void StreamTextBox_OnGotFocus(object sender, RoutedEventArgs e)
        {
            StreamListBox.SelectedIndex = -1;
        }

    }
}