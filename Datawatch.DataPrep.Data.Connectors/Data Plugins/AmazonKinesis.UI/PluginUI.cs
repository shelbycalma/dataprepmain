﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AmazonKinesisPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable,
        AmazonKinesisSettings, Dictionary<string, object>,
        AmazonKinesisDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner, "pack://application:,,,/Panopticon.AmazonKinesisPlugin.UI;" +
                "component/aws-small.png")
        {
        }

        public override Window CreateConnectionWindow(
            AmazonKinesisSettings settings)
        {
            return new ConnectionWindow(settings);
        }

        public override object GetConfigElement(
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(
                new AmazonKinesisSettings(Plugin.PluginManager, bag, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.ViewModel.Settings.ToPropertyBag();
        }
    }
}
