﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.RabbitMQPlugin
{
    /// <summary>
    /// DataPlugin for RabbitMQ.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<ParameterTable, RabbitMQSettings,
        Dictionary<string, object>, RabbitMQDataAdapter>
    {
        internal const string PluginId = "RabbitMQPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "RabbitMQ";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override RabbitMQSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new RabbitMQSettings(pluginManager, parameters);
        }

        public override RabbitMQSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new RabbitMQSettings(pluginManager, bag, null);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }
    }
}
