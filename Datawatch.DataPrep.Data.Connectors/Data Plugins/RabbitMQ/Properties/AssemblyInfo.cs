﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.RabbitMQPlugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.RabbitMQPlugin.dll")]
[assembly: AssemblyDescription("Datawatch RabbitMQ Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
