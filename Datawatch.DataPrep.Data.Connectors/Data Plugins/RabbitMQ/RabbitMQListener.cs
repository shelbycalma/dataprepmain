﻿using System;
using System.Text;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Panopticon.RabbitMQPlugin.Properties;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Panopticon.RabbitMQPlugin
{
    internal class Agent
    {
        protected IModel session;
        private IConnection connection;

        public virtual void Connect(string host, int port, string username,
            string password)
        {
            var factory = new ConnectionFactory() { HostName = host };
            if (!(port == 5672)) { factory.Port = port; }
            if (!string.IsNullOrEmpty(username)) { factory.UserName = username; }
            if (!string.IsNullOrEmpty(password)) { factory.Password = password; }

            connection = factory.CreateConnection();
            session = connection.CreateModel();
            Log.Info(string.Format(Resources.LogConnected, host, port));
        }

        public virtual void Disconnect()
        {
            if (session != null)
            {
                try
                {
                    session.Close();
                }
                catch (Exception ex)
                {
                    Log.Warning(string.Format(
                        Resources.LogClosingSessionExchangeError, ex.Message));
                }
                session = null;
            }
            if (connection != null)
            {
                try
                {
                    connection.Close();
                    connection = null;
                }
                catch (NullReferenceException ex)
                {
                    Log.Warning(string.Format(
                        Resources.LogClosingConnectionBrokerError, ex.Message));
                }
                catch (ArgumentNullException ex)
                {
                    Log.Warning(string.Format(Resources.LogClosingConnectionError,
                        ex.Message));
                }
            }

            Log.Info(Resources.LogDisconnected);
        }
    }

    internal class RabbitMQListener : Agent
    {
        private string queue;
        private readonly IMessageHandler messageReceiver;

        public RabbitMQListener(IMessageHandler messageReceiver)
        {
            if (messageReceiver == null)
            {
                throw Exceptions.DataAdaptorWasNull();
            }
            this.messageReceiver = messageReceiver;
        }

        public void ListenTo(string exchange, string routingKey, string queue,
            string exchangeType, bool exchangeDurable, bool exchangeAutoDelete,
            bool queueDurable, bool queueAutoDelete, bool queueExclusive)
        {
            // Create the queue for received messages
            var result = session.QueueDeclare(queue, queueDurable,
                queueExclusive, queueAutoDelete, null);
            this.queue = result.QueueName;

            Log.Info(string.Format(Resources.LogQueueDeclare, queue));

            session.ExchangeDeclare(exchange: exchange, type: exchangeType,
                durable: exchangeDurable, autoDelete: exchangeAutoDelete);

            session.QueueBind(queue: queue,
                              exchange: exchange,
                              routingKey: routingKey);

            var consumer = new EventingBasicConsumer(session);
            consumer.Received += (model, ea) =>
            {
                messageReceiver.MessageReceived(DecodeMessage(ea));
            };

            session.BasicConsume(queue: queue,
                                noAck: true,
                                consumer: consumer);

            Log.Info(string.Format(Resources.LogExchangeBind, exchange,
                routingKey));
            return;
        }

        public override void Disconnect()
        {
            if (session != null)
            {
                try
                {
                    session.QueuePurge(queue);
                }
                catch (Exception)
                {
                    // Could happen if queue is empty
                }
                try
                {
                    session.QueueDelete(queue);
                }
                catch (Exception)
                {
                    // Happens if if the queue havent been declared,
                    // but this should never happen
                }
            }

            base.Disconnect();
        }

        private static string DecodeMessage(BasicDeliverEventArgs message)
        {
            byte[] bodyBytes = message.Body;
            ASCIIEncoding enc = new ASCIIEncoding();
            string decodedMessage = enc.GetString(bodyBytes);
            return decodedMessage;
        }
    }
}
