﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.RabbitMQPlugin
{
    public class RabbitMQDataAdapter : MessageQueueAdapterBase<ParameterTable,
        RabbitMQSettings, Dictionary<string, object>>
    {
        private RabbitMQListener listener;

        public override void Start()
        {
            listener = new RabbitMQListener(this);
            try
            {
                settings.Parameters = table.Parameters;
                settings.ParserSettings.Parameters = table.Parameters;
                base.Start();

                string broker = DataUtils.ApplyParameters(settings.Broker,
                    table.Parameters);
                string username = DataUtils.ApplyParameters(settings.UserName,
                    table.Parameters);
                string password = DataUtils.ApplyParameters(settings.Password,
                    table.Parameters);

                // Verify settings
                HostUriParser hostParser =
                    new HostUriParser(5672, settings.Broker);
                string brokerHost = hostParser.Host;
                int brokerPort = hostParser.Port;
                if (brokerHost == null)
                {
                    throw Exceptions.MalformedBrokerUrl(settings.Broker);
                }

                // TODO do we need to be able to set "virtual host"?
                try
                {
                    listener.Connect(brokerHost, brokerPort, username,
                        password);
                }
                catch (Exception ex)
                {
                    throw Exceptions.ConnectionError(ex);
                }

                // Handle parameterized connection
                string exchange = DataUtils.ApplyParameters(settings.Exchange,
                    table.Parameters);
                string routingKey = DataUtils.ApplyParameters(settings.RoutingKey,
                    table.Parameters);
                string queue = DataUtils.ApplyParameters(settings.Queue,
                    table.Parameters);
               
                // Route messages to the new queue if they match the routing
                // key.
                listener.ListenTo(exchange, routingKey, queue, 
                    settings.SelectedExchangeType,
                    settings.ExchangeDurable, settings.ExchangeAutoDelete,
                    settings.QueueDurable, settings.QueueAutoDelete, 
                    settings.QueueExclusive);
            }
            catch (Exception ex)
            {
                listener.Disconnect();
                // Exceptions are caught and logged in
                // RealtimeDataPlugin.StartRealtimeUpdates() so just rethrow.
                throw ex;
            }
        }

        public override void Stop()
        {
            if (listener != null)
            {
                listener.Disconnect();
                listener = null;
            }
            base.Stop();
        }
    }
}
