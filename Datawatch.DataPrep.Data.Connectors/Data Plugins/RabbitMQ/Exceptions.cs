﻿using System;
using Panopticon.RabbitMQPlugin.Properties;

namespace Panopticon.RabbitMQPlugin
{
    class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        internal static Exception MalformedBrokerUrl(string url)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExMalformedBrokerUrl, url));
        }

        internal static Exception ConnectionError(Exception innerException)
        {
            return LogException(new InvalidOperationException(
                Resources.ExConnectionError,
                innerException));
        }

        internal static Exception NoColumns()
        {
            return CreateInvalidOperation(Resources.ExNoColumns);
        }

        internal static Exception NoIdColumn()
        {
            return CreateInvalidOperation(Resources.ExNoIdColumn);
        }
    }
}
