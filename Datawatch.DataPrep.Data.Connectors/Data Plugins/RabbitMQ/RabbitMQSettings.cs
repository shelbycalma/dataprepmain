using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using RabbitMQ.Client;

namespace Panopticon.RabbitMQPlugin
{
    /// <summary>
    /// The "Topic" property here is used as what in the RabbitMQ documentation is
    /// refered to as "destination".
    /// </summary>
    public class RabbitMQSettings : MessageQueueSettingsBase
    {
        public RabbitMQSettings(IPluginManager pluginManager)
            : this(pluginManager, null)
        {
        }

        public RabbitMQSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
        }

        public RabbitMQSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        public override bool Equals(object obj)
        {
            if (!(obj is RabbitMQSettings)) return false;

            if (!base.Equals(obj)) return false;

            RabbitMQSettings o = (RabbitMQSettings)obj;
            if (RoutingKey != o.RoutingKey) return false;
            if (SelectedExchangeType != o.SelectedExchangeType) return false;
            if (Queue != o.Queue) return false;
            if (ExchangeDurable != o.ExchangeDurable) return false;
            if (ExchangeAutoDelete != o.ExchangeAutoDelete) return false;
            if (QueueDurable != o.QueueDurable) return false;
            if (QueueAutoDelete != o.QueueAutoDelete) return false;
            if (QueueExclusive != o.QueueExclusive) return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    RoutingKey, SelectedExchangeType,
                    Queue, ExchangeDurable, ExchangeAutoDelete,
                    QueueDurable, QueueAutoDelete,
                    QueueExclusive
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public override PropertyBag ToPropertyBag()
        {
            PropertyBag bag = base.ToPropertyBag();
            bag.Values["RoutingKey"] = RoutingKey;
            return bag;
        }

        protected override string DefaultParserPluginId
        {
            get { return "Xml"; }
        }

        public override bool IsOk
        {
            get
            {
                return base.IsOk &&
                    !string.IsNullOrEmpty(RoutingKey);
            }
        }

        /// <summary>
        /// The Exchange property is just an alias for Topic to be more in line
        /// with RabbitMQ terminology.
        /// </summary>
        public string Exchange
        {
            get { return Topic; }
            set
            {
                if (value != Topic)
                {
                    // Note that this might very well fire two PropertyChanged
                    // events (as it should).
                    // TODO but we could probably make this clearer once we
                    // make the *MQ systems share more code.
                    Topic = value;
                    FirePropertyChanged("Exchange");
                }
            }
        }

        public string SelectedExchangeType
        {
            get { return GetInternal("ExchangeType", "topic"); }
            set { SetInternal("ExchangeType", value); }
        }

        public ICollection<string> ExchangeTypes
        {
            get
            {
                return ExchangeType.All();
            }
        }

        public string RoutingKey
        {
            get { return GetInternal("RoutingKey"); }
            set { SetInternal("RoutingKey", value); }
        }

        public string Queue
        {
            get { return GetInternal("Queue"); }
            set { SetInternal("Queue", value); }
        }

        public bool ExchangeDurable
        {
            get
            {
                string value = GetInternal("ExchangeDurable",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("ExchangeDurable", value.ToString());
            }
        }

        public bool ExchangeAutoDelete
        {
            get
            {
                string value = GetInternal("ExchangeAutoDelete",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("ExchangeAutoDelete", value.ToString());
            }
        }

        public bool QueueDurable
        {
            get
            {
                string value = GetInternal("QueueDurable",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("QueueDurable", value.ToString());
            }
        }

        public bool QueueAutoDelete
        {
            get
            {
                string value = GetInternal("QueueAutoDelete",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("QueueAutoDelete", value.ToString());
            }
        }

        public bool QueueExclusive
        {
            get
            {
                string value = GetInternal("QueueExclusive",
                    false.ToString());
                return bool.Parse(value);
            }
            set
            {
                SetInternal("QueueExclusive", value.ToString());
            }
        }


        public override void DoGenerateColumns()
        {
            StreamingColumnGenerator columnGenerator =
                new StreamingColumnGenerator(this, this.errorReporter);

            // Set up connection to RabbitMQ
            RabbitMQListener listener = 
                new RabbitMQListener(columnGenerator);

            try
            {
                string broker = DataUtils.ApplyParameters(Broker,
                    Parameters);
                string username = DataUtils.ApplyParameters(UserName,
                    Parameters);
                string password = DataUtils.ApplyParameters(Password,
                    Parameters);

                // Verify settings
                HostUriParser hostParser =
                    new HostUriParser(5672, broker);
                string brokerHost = hostParser.Host;
                int brokerPort = hostParser.Port;
                if (brokerHost == null)
                {
                    throw Exceptions.MalformedBrokerUrl(Broker);
                }

                // TODO do we need to be able to set "virtual host"?
                try
                {
                    listener.Connect(brokerHost, brokerPort, username,
                        password);
                }
                catch (Exception ex)
                {
                    throw Exceptions.ConnectionError(ex);
                }

                // Handle parameterized connection
                string exchange = DataUtils.ApplyParameters(Exchange,
                    Parameters);
                string routingKey = DataUtils.ApplyParameters(RoutingKey,
                    Parameters);
                string queue = DataUtils.ApplyParameters(Queue,
                    Parameters);
               
                // Route messages to the new queue if they match the routing
                // key.
                listener.ListenTo(exchange, routingKey, queue,
                    SelectedExchangeType, ExchangeDurable, ExchangeAutoDelete,
                    QueueDurable, QueueAutoDelete, QueueExclusive);

                // Start receiving data to generate columns
                columnGenerator.Generate();

                listener.Disconnect();
            }
            catch (Exception ex)
            {
                listener.Disconnect();
                // Exceptions are caught and logged in
                // RealtimeDataPlugin.StartRealtimeUpdates() so just rethrow.
                this.errorReporter.Report(
                    Properties.Resources.ExError,
                    Properties.Resources.ExFailedColumnGeneration +
                    "\nException: " + ex.Message);
            }
        }

        public override bool CanGenerateColumns()
        {
            return !string.IsNullOrEmpty(Topic) && !string.IsNullOrEmpty(Broker);
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }
    }
}
