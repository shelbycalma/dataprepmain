﻿using System.Data;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.AnalysisServicesPlugin
{
    public abstract class MDXProvider
    {
        public abstract IDbConnection Connect(MDXConnectionSettings settings);

        public abstract ITable GetSchema(
            IDbConnection connection,
            MDXConnectionSettings settings);

        public abstract ITable Execute(
            IDbConnection connection, 
            MDXConnectionSettings settings, 
            OnDemandParameters qod);

        public abstract ITable GetDomain(
            IDbConnection connection,
            MDXConnectionSettings settings,
            string column);
    }
}
