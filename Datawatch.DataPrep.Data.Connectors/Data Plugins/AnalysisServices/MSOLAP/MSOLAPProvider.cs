﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.AnalysisServicesPlugin.MDX;
using Panopticon.AnalysisServicesPlugin;
using Dimension = Microsoft.AnalysisServices.AdomdClient.Dimension;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;
using Level = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level;
using Measure = Microsoft.AnalysisServices.AdomdClient.Measure;
using Tuple = Microsoft.AnalysisServices.AdomdClient.Tuple;

namespace Panopticon.AnalysisServicesPlugin.MSOLAP
{
    public class MSOLAPProvider
    {
        private const string catalogFormat = "[{0}]";
        private ParameterEncoder parameterEncoder;
        private readonly object syncRoot = new object();

        public MSOLAPProvider(IEnumerable<ParameterValue> parameters)
        {
            parameterEncoder = new ParameterEncoder
            {
                Parameters = parameters
            };
        }

        private string GetConnectionString(MDXConnectionSettings settings)
        {
            StringBuilder cs = new StringBuilder();
            cs.Append("Provider=MSOLAP");
            cs.Append(";Initial Catalog=");
            cs.Append(Encoded(settings.CatalogName));
            string dataSource = null;
            if (settings.Authentication != AuthenticationType.WindowsAuthentication)
            {
                string protocol = settings.UseSsl ? "https://" : "http://";
                dataSource =
                    protocol + Encoded(settings.ServerName)
                    + "/olap/msmdpump.dll";
                cs.Append("; Data Source=" + dataSource);
                if (!string.IsNullOrEmpty(settings.UserName))
                {
                    cs.Append(";User ID=" + Encoded(settings.UserName));
                }
                if (!string.IsNullOrEmpty(settings.Password))
                {
                    cs.Append(";Password=" + Encoded(settings.Password));
                }
            }
            else
            {
                dataSource = Encoded(settings.ServerName);
                cs.Append(";Data Source="); cs.Append(dataSource);
            }
            Log.Info(Panopticon.AnalysisServicesPlugin.Properties.Resources.LogConnectionEstablishing, dataSource);
            return cs.ToString();
        }

        public IDbConnection Connect(MDXConnectionSettings settings)
        {
            string connectionString = GetConnectionString(settings);
            AdomdConnection connection = new AdomdConnection(connectionString);
            try
            {
                connection.Open();
            }
            catch (Exception e)
            {
                Log.Exception(e);
                throw;
            }
            return connection;
        }

        private Member RetrieveMemberWithLevel(Member member, int levelDepth)
        {
            if (member == null)
            {
                return null;
            }
            if (member.LevelDepth == levelDepth)
            {
                return member;
            }
            return RetrieveMemberWithLevel(member.Parent, levelDepth);
        }

        private StandaloneTable CreateTable(
            IDbConnection connection,
            MDXConnectionSettings settings,
            CellSet cells, OnDemandParameters qod, OnDemandSchemaTableCache cache)
        {
            StandaloneTable table = GetSchema(connection, settings, cache);
            table.BeginUpdate();
            if (cells.Axes.Count > 1)
            {
                var tuples = cells.Axes[1].Set.Tuples;
                table.AddRows(tuples.Count);

                Dictionary<string, TextColumn> levelsToColumnsDictionary = new Dictionary<string, TextColumn>(table.ColumnCount);
                for (int i = 0; i < table.ColumnCount; i++)
                {
                    var column = table.GetColumn(i) as TextColumn;
                    if (column != null)
                    {
                        string key = MDXUtils.GetLevelName(column.Name);
                        levelsToColumnsDictionary[key] = column;
                    }
                }
                IEnumerable<OnDemandAggregate> aggregates =
                    qod.Aggregates ?? Enumerable.Empty<OnDemandAggregate>();


                //Retrieve measures
                string[] measureNames = aggregates
                    .Where(a => a.FunctionType == FunctionType.External)
                    .Select(a => a.Columns.Single()).ToArray();
                for (int i = 0; i < tuples.Count; i++)
                {
                    var tuple = tuples[i];
                    foreach (var member in tuple.Members)
                    {
                        for (int t = member.LevelDepth; t >= 0; t--)
                        {
                            var targetMember = RetrieveMemberWithLevel(member, t);
                            if (targetMember != null)
                            {
                                levelsToColumnsDictionary[targetMember.LevelName].SetTextValue(i, targetMember.Caption);
                            }
                        }
                    }
                    for (int j = 0; j < measureNames.Length; j++)
                    {
                        var column = (NumericColumn)table.GetColumn(measureNames[j]);
                        column.SetNumericValue(i, MDXUtils.GetNumericValue(cells.Cells[j, i].Value));
                    }
                }
            }
            table.EndUpdate();
#if (DEBUG)
            Log.Info("********************");
            for (int r = 0; r < table.RowCount; r++)
            {
                string s = "";
                for (int c = 0; c < table.ColumnCount; c++)
                {
                    var t = table.GetColumn(c).GetType();
                    if (t.Name.Equals("TextColumn"))
                    {
                        TextColumn cl = (TextColumn)table.GetColumn(c);
                        s = s + cl.GetTextValue(r) + ", ";
                    }
                    else
                    {
                        NumericColumn cl = (NumericColumn)table.GetColumn(c);
                        s = s + cl.GetNumericValue(r).ToString() + ", ";
                    }
                }
                Log.Info(s);
            }
            Log.Info("********************");
#endif
            return table;
        }

        private string[] GetRowFilteredAttributes(OnDemandParameters qod)
        {
            // Dimension attributes from drill path that we cannot use as
            // slicers (member is different from caption).
            string[] drill = qod.DrillPath ?? new string[0];
            string[] breakdown = qod.BreakdownColumns ?? new string[0];

            IEnumerable<string> slicers = drill
                .Zip(breakdown,
                    (path, column) => TryCreateFilter(column, path))
                .Where(s => s != null);

            return slicers.ToArray();
        }

        private string TryCreateFilter(string column, string path)
        {
            return string.Format("Filter({0}.MEMBERS, " +
                "{0}.CurrentMember.MEMBER_CAPTION = \"{1}\")",
                column, path);
        }

        public IList<string> GetCatalogs(MDXConnectionSettings settings)
        {
            AdomdRestrictionCollection restrictions = new AdomdRestrictionCollection();
            using (var connection = (AdomdConnection)Connect(settings))
            {
                DataSet resultSet = connection.GetSchemaDataSet("DBSCHEMA_CATALOGS", restrictions);
                List<string> catalogNames = new List<string>();
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {
                    catalogNames.Add(row["CATALOG_NAME"].ToString());
                }

                //return catalogNames;
                return catalogNames;
            }
        }

        public StandaloneTable GetSchema(IDbConnection connection, MDXConnectionSettings settings, OnDemandSchemaTableCache cache)
        {
            var columnNames = settings.SelectedDimensionsAndMeasures.Values
                .Select(x => x.Name).ToArray();
            OnDemandSchemaTableCacheKey cacheKey
                = new OnDemandSchemaTableCacheKey("", GetConnectionString(settings), columnNames);
            StandaloneTable schema = cache[cacheKey];
            if (schema == null)
            {
                lock (syncRoot)
                {
                    schema = cache[cacheKey];
                    if (schema == null)
                    {
                        schema = CreateSchemaTable(connection, settings);
                        cache[cacheKey] = schema;
                    }
                }
            }
            return schema;
        }

        private StandaloneTable CreateSchemaTable(IDbConnection connection,
            MDXConnectionSettings settings)
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            try
            {
                AdomdConnection adomdConnection = (AdomdConnection)connection;
                CubeDef cube = adomdConnection.Cubes[settings.CubeName];
                if (cube == null)
                {
                    return table;
                }

                foreach (Measure measure in cube.Measures)
                {
                    if (!settings.SelectedDimensionsAndMeasures.Values.
                        ContainsKey(measure.UniqueName))
                    {
                        continue;
                    }
                    AddNumericColumn(table, measure.UniqueName, measure.Caption);
                }

                foreach (Dimension dimension in cube.Dimensions)
                {
                    if (dimension.DimensionType == DimensionTypeEnum.Measure)
                    {
                        continue;
                    }

                    foreach (Hierarchy hierarchy in dimension.Hierarchies)
                    {
                        if (!settings.SelectedDimensionsAndMeasures.Values.
                            ContainsKey(hierarchy.UniqueName))
                        {
                            continue;
                        }
                        foreach (var level in hierarchy.Levels)
                        {
                            AddTextColumn(table,
                                MDXUtils.CreateHierarchyLevelName(hierarchy.UniqueName, level.UniqueName),
                                hierarchy.Caption + "/" + level.Caption);
                        }
                    }
                }
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        private void AddTextColumn(
            StandaloneTable table, string name, string title)
        {
            TextColumn column = new TextColumn(name);
            ((TextColumnMetaData)column.MetaData).Title = title;
            table.AddColumn(column);
        }
        private void AddNumericColumn(
            StandaloneTable table, string name, string title)
        {
            NumericColumn column = new NumericColumn(name);
            ((NumericColumnMetaData)column.MetaData).Title = title;
            table.AddColumn(column);
        }

        public ITable Execute(IDbConnection connection, MDXConnectionSettings settings,
            OnDemandParameters qod, OnDemandSchemaTableCache cache)
        {
            string query = MDXExecutor.BuildOnDemandQuery(settings, qod)
                .ToExpression();
            Log.Info(Properties.Resources.LogQuery, query);

            StandaloneTable table = null;
            AdomdCommand command =
                new AdomdCommand(query, (AdomdConnection)connection);
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                var cellSet = command.ExecuteCellSet();
                sw.Stop();
                int rows = 0, columns = 0;
                if (cellSet.Axes.Count > 1)
                {
                    columns = cellSet.Axes[0].Set.Tuples.Count;
                    rows = cellSet.Axes[1].Set.Tuples.Count;
                }
                Log.Info(Properties.Resources.LogRetrievedColumnsRows, columns, 
                    rows, sw.ElapsedMilliseconds);
                table = CreateTable(connection, settings,
                    cellSet, qod, cache);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                    Log.Error(e.InnerException.Message);
                    throw e.InnerException;
                }
                else
                {
                    throw;
                }
            }
            finally
            {
                command.Dispose();
            }

            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }
            return table;
        }

        public ITable GetDomain(
            IDbConnection connection,
            MDXConnectionSettings settings,
            string columnName)
        {
            StandaloneTable table = CreateSchemaTable(connection, settings);
            table.BeginUpdate();

            Column column = table.GetColumn(columnName);
            if (column is NumericColumn)
            {
                string query = BuildNumericDomainQuery(columnName, settings, table);
                Log.Info(Properties.Resources.LogDomainQuery, query);
                using (AdomdCommand command =
                   new AdomdCommand(query, (AdomdConnection)connection))
                {
                    Stopwatch sw = Stopwatch.StartNew();
                    var cellSet = command.ExecuteCellSet();
                    sw.Stop();
                    int columns = cellSet.Axes[0].Set.Tuples.Count;
                    int rows = 0;
                    if (cellSet.Axes.Count > 1)
                    {
                        rows = cellSet.Axes[1].Set.Tuples.Count;
                    }
                    Log.Info(Properties.Resources.LogRetrievedColumnsRows, columns,
                        rows, sw.ElapsedMilliseconds);
                    SetNumericDomain(table, (NumericColumn)column, cellSet);
                }
            }
            else if (column is TextColumn)
            {
                string query = BuildTextDomainQuery(columnName, settings);
                Log.Info(Properties.Resources.LogDomainQuery, query);
                using (AdomdCommand command =
                    new AdomdCommand(query, (AdomdConnection)connection))
                {
                    Stopwatch sw = Stopwatch.StartNew();
                    var cellSet = command.ExecuteCellSet();
                    sw.Stop();
                    int columns = cellSet.Axes[0].Set.Tuples.Count;
                    int rows = 0;
                    if (cellSet.Axes.Count > 1)
                    {
                        rows = cellSet.Axes[1].Set.Tuples.Count;
                    }
                    Log.Info(Properties.Resources.LogRetrievedColumnsRows, columns,
                        rows, sw.ElapsedMilliseconds);
                    SetTextDomain(table,
                        (TextColumn)column, cellSet);
                }
            }

            table.EndUpdate();
            return table;
        }

        private string BuildNumericDomainQuery(string column, MDXConnectionSettings settings, StandaloneTable table)
        {
            QueryBuilder queryBuilder = new QueryBuilder(settings);
            queryBuilder.FromNode = new CubeNode(settings.CubeName);
            queryBuilder.AddColumn(new MetadataElementBase
            {
                UniqueName = column
            });
            var textColumnNames = settings.SelectedDimensionsAndMeasures.Values
                .Select(x => x.Name)
                //The text column level will already be processed and will have
                //a different name, therefore this will work
                .Where(s => table.GetColumn(s) == null)
                .Distinct()
                .ToArray();
            foreach (var textColumn in textColumnNames)
            {
                queryBuilder.AddRow(new Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension
                {
                    UniqueName = textColumn
                });
            }
            return queryBuilder.ToExpression();

        }

        private string BuildTextDomainQuery(string column, MDXConnectionSettings settings)
        {
            QueryBuilder queryBuilder = new QueryBuilder(settings);
            queryBuilder.FromNode = new CubeNode(settings.CubeName);
            queryBuilder.AddColumnDimension(new Level()
            {
                UniqueName = MDXUtils.GetLevelName(column)
            });
            return queryBuilder.ToExpression();
        }

        private void SetNumericDomain(StandaloneTable table,
            NumericColumn column, CellSet cells)
        {
            var values = new List<double>();
            foreach (var cell in cells.Cells)
            {
                var value = MDXUtils.GetNumericValue(cell.Value);
                if (!double.IsNaN(value))
                {
                    values.Add(value);
                }
            }
            if (values.Count != 0)
            {
                var min = values.Min();
                var max = values.Max();
                ((NumericColumnMetaData)column.MetaData).Domain = new NumericInterval(min, max);
            }
        }

        private void SetTextDomain(StandaloneTable table,
            TextColumn column, CellSet cells)
        {
            TupleCollection rows = cells.Axes[0].Set.Tuples;
            int rowCount = rows.Count;
            string[] domain = new string[rowCount];

            for (int row = 0; row < rowCount; row++)
            {
                Tuple tuple = rows[row];
                Member member = tuple.Members[0];
                string value = member.Caption;
                domain[row] = value;
            }

            ((TextColumnMetaData)column.MetaData).Domain = domain;
        }

        private string Encoded(string source)
        {
            parameterEncoder.SourceString = source;
            return parameterEncoder.Encoded();
        }
    }
}