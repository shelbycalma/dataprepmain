﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.AnalysisServicesPlugin.MSOLAP;

namespace Panopticon.AnalysisServicesPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<MDXConnectionSettings>,
        IAggregatingOnDemandPlugin
    {
        internal const string PluginId = "AnalysisServicesPlugin";
        internal const string PluginTitle = "Analysis Services";
        internal const string PluginType = DataPluginTypes.Database;
        internal const bool PluginIsObsolete = false;

        private readonly MDXExecutor executor = new MDXExecutor();

        public override ITable GetData(
            string workbooksFolder, string dataFolder,
            PropertyBag connectionBag, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            MDXConnectionSettings connection =
                new MDXConnectionSettings(connectionBag);

            throw new NotImplementedException();
        }

        public ITable GetOnDemandData(
            string workbooksFolder, string dataFolder,
            PropertyBag connectionBag, IEnumerable<ParameterValue> parameters,
            OnDemandParameters qod, int maxRowCount)
        {
            CheckLicense();

            MDXConnectionSettings connection =
                new MDXConnectionSettings(connectionBag);

            MSOLAPProvider provider = new MSOLAPProvider(parameters);
            return executor.GetData(provider,
                connection, parameters, qod, maxRowCount);
        }

        #region IAggregatingOnDemandPlugin Plumbing

        public string GetPartitionValue(PropertyBag connectionBag)
        {
            return MDXConnectionSettings.GetPartitionValue(connectionBag);
        }

        public bool IncludesAggregateData(PropertyBag connectionBag)
        {
            return MDXConnectionSettings.GetIsAggregated(connectionBag);
        }

        #endregion // IAggregatingOnDemandPlugin Plumbing

        #region IOnDemandPlugin Plumbing

        public bool IsOnDemand(PropertyBag connectionBag)
        {
            return MDXConnectionSettings.GetIsOnDemand(connectionBag);
        }

        #endregion // IOnDemandPlugin Plumbing

        #region DataPluginBase Plumbing

        public override MDXConnectionSettings CreateSettings(
            PropertyBag connectionBag)
        {
            return new MDXConnectionSettings(connectionBag);
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        #endregion // DataPluginBase Plumbing
    }
}
