﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AnalysisServicesPlugin
{
    public class MDXConnectionSettings : ConnectionSettings, IMdxSettings
    {
        public MDXConnectionSettings() : this(new PropertyBag())
        {
        }

        public MDXConnectionSettings(PropertyBag bag) : base(bag)
        {
        }

        public string CatalogName
        {
            get {
                return GetInternal("CatalogName", null);
            }
            set { SetInternal("CatalogName", value); }
        }

        public string ServerName
        {
            get { return GetInternal("ServerName", "localhost"); }
            set { SetInternal("ServerName", value); }
        }

        public string CubeName
        {
            get { return GetInternal("CubeName", null); }
            set { SetInternal("CubeName", value); }
        }

        public bool NullSuppression
        {
            get
            {
                string text = GetInternal("NullSuppression");
                return string.IsNullOrEmpty(text) || bool.Parse(text);
            }
            set
            {
                SetInternal("NullSuppression", Convert.ToString(value));
            }
        }

        public bool IsAggregated
        {
            get {
                string text = GetInternal("IsAggregated");
                return string.IsNullOrEmpty(text) || Convert.ToBoolean(text);
            }
            set { SetInternal("IsAggregated", Convert.ToString(value)); }
        }

        public bool IsOnDemand
        {
            get {
                string text = GetInternal("IsOnDemand");
                return string.IsNullOrEmpty(text) || Convert.ToBoolean(text);
            }
            set { SetInternal("IsOnDemand", Convert.ToString(value)); }
        }

        public PropertyBag SelectedDimensionsAndMeasures
        {
            get
            {
                PropertyBag bag = 
                    GetInternalSubGroup("SelectedDimensionsAndMeasures");
                if (bag == null)
                {
                    SelectedDimensionsAndMeasures = bag = new PropertyBag();
                }
                return bag;
            }
            set
            {
                SetInternalSubGroup("SelectedDimensionsAndMeasures", value);                
            }
        }

        public override string Title 
        { 
            get { return CubeName; }
            set
            {
                
            }
        }

        public AuthenticationType Authentication
        {
            get
            {
                return (AuthenticationType)Enum.Parse(typeof(AuthenticationType),
                    GetInternal("AuthenticationType", "WindowsAuthentication"));
            }
            set
            {
                SetInternal("AuthenticationType", value.ToString());
            }
        }

        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public bool UseSsl
        {
            get
            {
                string text = GetInternal("UseSsl");
                return string.IsNullOrEmpty(text) || Convert.ToBoolean(text);
            }
            set
            {
                SetInternal("UseSsl", Convert.ToString(value));
            }
        }

        #region Plugin Helper Methods

        internal static string GetPartitionValue(PropertyBag bag)
        {
            return bag.Values["PartitionValue"];
        }

        internal static bool GetIsAggregated(PropertyBag bag)
        {
            string text = bag.Values["IsAggregated"];
            return string.IsNullOrEmpty(text) || Convert.ToBoolean(text);
        }

        internal static bool GetIsOnDemand(PropertyBag bag)
        {
            string text = bag.Values["IsOnDemand"];
            return string.IsNullOrEmpty(text) || Convert.ToBoolean(text);
        }

        #endregion
    }
}
