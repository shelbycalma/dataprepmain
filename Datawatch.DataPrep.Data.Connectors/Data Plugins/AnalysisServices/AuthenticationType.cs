﻿using System.ComponentModel;

namespace Panopticon.AnalysisServicesPlugin
{
    public enum AuthenticationType
    {
        [Description("UiWindowsAuthentication")]
        WindowsAuthentication,
        [Description("UiUsernamePassword")]
        UsernamePassword
    }
}
