﻿using Datawatch.DataPrep.Data.Core.UI.Mediator;

namespace Panopticon.LiveViewPlugin.UI
{
    public partial class ConnectionWindow
    {

        public ConnectionWindow(ConnectionViewModel connection)
        {
            DataContext = connection;
            Mediator.Instance.Register(this);
            InitializeComponent();
        }

        [MediatorMessageSink("Panopticon.LiveViewPlugin.CloseDialog", 
            ParameterType = typeof(CloseDialogArgs))]
        public void CloseDialog(CloseDialogArgs args)
        {
            if (args.ViewModel != DataContext) return;
            DialogResult = args.Result;
            Close();
        }
    }
}
