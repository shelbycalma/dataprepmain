﻿namespace Panopticon.LiveViewPlugin.UI
{
    public class CloseDialogArgs
    {
        public readonly ConnectionViewModel ViewModel;
        public readonly bool Result;

        public CloseDialogArgs(ConnectionViewModel vm, bool result)
        {
            ViewModel = vm;
            Result = result;
        }
    }
}
