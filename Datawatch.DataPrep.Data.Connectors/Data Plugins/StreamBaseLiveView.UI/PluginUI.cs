﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.LiveViewPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, LiveViewSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.LiveViewPlugin.UI;component" +
            "/liveview-small.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            LiveViewSettings con = new LiveViewSettings();
            ConnectionViewModel vm = new ConnectionViewModel(con, parameters);
            ConnectionWindow win = new ConnectionWindow(vm);
            win.Owner = owner;

            if (win.ShowDialog() == true)
            {
                PropertyBag bag = con.ToPropertyBag();
                return bag;
            }
            return null;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            LiveViewSettings con = new LiveViewSettings(bag);
            ConnectionViewModel vm = new ConnectionViewModel(con, parameters);

            return new ConnectionPanel(vm);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            ConnectionViewModel vm = (ConnectionViewModel)cp.DataContext;
            return vm.Connection.ToPropertyBag();
        }
    }
}
