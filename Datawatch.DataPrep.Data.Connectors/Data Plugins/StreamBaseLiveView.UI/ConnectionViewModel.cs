﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Database;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.UI.Mediator;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.LiveViewPlugin.Properties;

namespace Panopticon.LiveViewPlugin.UI
{
    public class ConnectionViewModel : ViewModelBase
    {
        private LiveViewSettings connection;
        private ICommand okCommand;
        private ICommand fetchStreamsCommand;
        private IEnumerable<ParameterValue> parameters;
        private ParameterValueCollection escapedParameters;

        public ConnectionViewModel(LiveViewSettings connection, 
            IEnumerable<ParameterValue> parameters)
        {
            Mediator.Instance.Register(this);
            this.connection = connection;
            this.parameters = parameters;
            connection.PropertyChanged += connection_PropertyChanged;
        }

        public LiveViewSettings Connection
        {
            get { return connection; }
            set
            {
                if (connection != value)
                {
                    connection = value;
                    OnPropertyChanged("Connection");
                }
            }
        }

        public string[] AvailableTables
        {
            get
            {
                if (string.IsNullOrEmpty(connection.PrimaryURL))
                {
                    return new string[] { };
                }

                string[] streams;
                try
                {
                    // AvailableTables is only called from a binding in
                    // the setting gui.
                    // The binding used to swallow any exception thrown here.
                    // So now they are just logged.
                    // Specifically we want to log if the LiveView.API.dll
                    // failed to load.
                    streams = LiveViewClientUtil.GetTables(connection, EscapedParameters);
                }
                catch (Exception exc)
                {
                    streams = new string[0];
                    Log.Exception(exc);
                }
                   

                if (string.IsNullOrEmpty(Table) && streams.Length > 0)
                {
                    Table = streams[0];
                }
                return streams;
            }
        }

        public ParameterValueCollection EscapedParameters
        {
            get
            {
                if (parameters == null) return null;

                if (escapedParameters == null)
                {
                    escapedParameters =
                        DatabaseUtils.EscapeParameters(parameters);
                }
                return escapedParameters;
            }
        }

        public string Table
        {
            get { return connection.Table; }
            set
            {
                if (connection.Table != value)
                {
                    connection.Table = value;
                    connection.UpdateQuery(EscapedParameters);
                    connection.UpdateIdCandidates(EscapedParameters);
                    OnPropertyChanged("Table");
                }
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return connection.TimeZoneHelper;
            }
        }

        public ICommand OkCommand
        {
            get
            {
                if (okCommand == null)
                {
                    okCommand = new DelegateCommand(Ok, CanOk);
                }
                return okCommand;
            }
        }

        private void Ok()
        {
            Mediator.Instance.NotifyColleagues(
                "Panopticon.LiveViewPlugin.CloseDialog",
                new CloseDialogArgs(this, true));
        }

        private bool CanOk()
        {
            return !string.IsNullOrEmpty(connection.PrimaryURL) &&
                (connection.IsQueryMode ?
                    !string.IsNullOrEmpty(connection.Query) :
                    !string.IsNullOrEmpty(connection.Table)) &&
                !string.IsNullOrEmpty(connection.IdColumnName);
        }

        public ICommand FetchStreamsCommand
        {
            get
            {
                if (fetchStreamsCommand == null)
                {
                    fetchStreamsCommand = new DelegateCommand(FetchStreams,
                        CanFetchStreams);
                }
                return fetchStreamsCommand;
            }
        }

        private void connection_PropertyChanged(object sender,
            System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (connection.QueryMode == QueryMode.Table)
            {
                connection.UpdateQuery(EscapedParameters);
            }
        }

        private void FetchStreams()
        {
            try
            {
                OnPropertyChanged("AvailableTables");
                connection.UpdateIdCandidates(EscapedParameters);
            }
            catch (Exception exc)
            {
                Log.Exception(exc);
                MessageBox.Show(exc.Message, Resources.UiPluginTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanFetchStreams()
        {
            return !string.IsNullOrEmpty(connection.PrimaryURL);
        }

    }
}
