﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Panopticon.LiveViewPlugin.UI.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Panopticon.LiveViewPlugin.UI.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string UiCommonCancel {
            get {
                return ResourceManager.GetString("UiCommonCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        public static string UiCommonOK {
            get {
                return ResourceManager.GetString("UiCommonOK", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fetch Tables.
        /// </summary>
        public static string UiConnectionPanelFetchTables {
            get {
                return ResourceManager.GetString("UiConnectionPanelFetchTables", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Id Column Name.
        /// </summary>
        public static string UiConnectionPanelIdColumnName {
            get {
                return ResourceManager.GetString("UiConnectionPanelIdColumnName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Predicate.
        /// </summary>
        public static string UiConnectionPanelPredicate {
            get {
                return ResourceManager.GetString("UiConnectionPanelPredicate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Primary URL.
        /// </summary>
        public static string UiConnectionPanelPrimaryURL {
            get {
                return ResourceManager.GetString("UiConnectionPanelPrimaryURL", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Query.
        /// </summary>
        public static string UiConnectionPanelQuery {
            get {
                return ResourceManager.GetString("UiConnectionPanelQuery", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Table.
        /// </summary>
        public static string UiConnectionPanelTable {
            get {
                return ResourceManager.GetString("UiConnectionPanelTable", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Timezone.
        /// </summary>
        public static string UiConnectionPanelTimezone {
            get {
                return ResourceManager.GetString("UiConnectionPanelTimezone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to StreamBase LiveView Connection.
        /// </summary>
        public static string UiConnectionWindowStreamBaseLiveViewConnection {
            get {
                return ResourceManager.GetString("UiConnectionWindowStreamBaseLiveViewConnection", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enclose parameters in quotes.
        /// </summary>
        public static string UiParametersInQuotes {
            get {
                return ResourceManager.GetString("UiParametersInQuotes", resourceCulture);
            }
        }
    }
}
