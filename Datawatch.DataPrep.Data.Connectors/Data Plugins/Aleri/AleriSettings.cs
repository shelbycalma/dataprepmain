﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using aleri_pubsubconst;
using aleri_pubsubnet;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.UI.Mediator;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AleriPlugin
{

    public enum ConnectionMode
    {
        Stream, Query
    }

    public class AleriSettings : RealtimeConnectionSettings
    {
        private string[] streams;

        private ICommand connectCommand;

        public AleriSettings() : base()
        {
        }

        public AleriSettings(PropertyBag bag)
            : base(bag, true)
        {
            if (Stream != null)
            {
                streams = new string[] { Stream };
            }
        }

        public SpAuthType AuthenticationType
        {
            get 
            {
                string s = GetInternal("AuthenticationType");
                if (s != null)
                {
                    try
                    {
                        return (SpAuthType)Enum.Parse(typeof(SpAuthType), s);
                    }
                    catch 
                    { 
                    }
                }
                return SpAuthType.AUTH_NONE;
            }
            set
            {
                SetInternal("AuthenticationType", value.ToString());
            }
        }

        public ConnectionMode ConnectionMode
        {
            get 
            {
                string s = GetInternal("ConnectionMode");
                if (s != null)
                {
                    try
                    {
                        return (ConnectionMode)Enum.Parse(
                            typeof(ConnectionMode), s);
                    }
                    catch
                    {
                    }
                }
                return ConnectionMode.Stream;
            }
            set
            {
                SetInternal("ConnectionMode", value.ToString());
            }
        }

        public bool EncloseParameterInQuote
        {
            get
            {
                string s = GetInternal("EncloseParameterInQuote",
                    Convert.ToString(true));
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("EncloseParameterInQuote",
                    Convert.ToString(value));
            }
        }

        public string Host
        {
            get { return GetInternal("Host"); }
            set { SetInternal("Host", value); }
        }

        public string HotSpareHost
        {
            get { return GetInternal("HotSpareHost"); }
            set { SetInternal("HotSpareHost", value);  }
        }

        public int HotSparePort
        {
            get { return GetInternalInt("HotSparePort", 22000); }
            set { SetInternalInt("HotSparePort", value); }
        }

        public bool IgnoreNulls
        {
            get
            {
                string s = GetInternal("IgnoreNulls");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IgnoreNulls", Convert.ToString(value));
            }
        }

        public bool IsEncrypted
        {
            get 
            {
                string s = GetInternal("Encrypted");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("Encrypted", Convert.ToString(value));
            }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public int Port
        {
            get { return GetInternalInt("Port", 22000); }
            set { SetInternalInt("Port", value); }
        }

        public string RsaKeyFile
        {
            get { return GetInternal("RsaKeyFile"); }
            set { SetInternal("RsaKeyFile", value); }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public string Stream
        {
            get { return GetInternal("Stream"); }
            set { SetInternal("Stream", value); }
        }

        public override string Title
        {
            get
            {
                try
                {
                    if (ConnectionMode == ConnectionMode.Stream)
                    {
                        return Stream;
                    }
                    else
                    {
                        string query = Query;
                        if (query != null && query.Length > 0)
                        {
                            int index = query.ToUpper().IndexOf("FROM");
                            if (index != -1)
                            {
                                index += "FROM".Length;
                                string from = query.Substring(index, query.Length - index);
                                from = from.Trim();
                                if (from[0] < 'A' || from[0] > 'z')
                                {
                                    from = from.Substring(1, from.Length - 2);
                                }
                                return from;
                            }
                        }
                        return Properties.Resources.UiStreamProjection;
                    }
                }
                catch
                {
                    return Properties.Resources.UiNoTitle;
                }
            }
            set
            {
                base.Title = value;
            }
        }

        public string UserName
        {
            get { return GetInternal("UserName"); }
            set { SetInternal("UserName", value); }
        }

        protected override void FirePropertyChanged(string name)
        {
            base.FirePropertyChanged(name);
            if (name == "Stream" || name == "Query" || name == "ConnectionMode")
            {
                UpdateColumns();
            }
            if (name == "ConnectionMode")
            {
                base.FirePropertyChanged("IsStreamConnectionMode");
                base.FirePropertyChanged("IsQueryConnectionMode");
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is AleriSettings)) return false;

            if (!base.Equals(obj)) return false;

            AleriSettings cs = (AleriSettings)obj;

            // TODO: should equals and hashcode only use the stream
            // if in stream-mode, and query only if in query-mode?

            if (this.AuthenticationType != cs.AuthenticationType ||
                this.ConnectionMode != cs.ConnectionMode ||
                this.EncloseParameterInQuote != cs.EncloseParameterInQuote ||
                this.Host != cs.Host ||
                this.HotSpareHost != cs.HotSpareHost ||
                this.HotSparePort != cs.HotSparePort ||
                this.IgnoreNulls != cs.IgnoreNulls ||
                this.IsEncrypted != cs.IsEncrypted ||
                this.Password != cs.Password ||
                this.Port != cs.Port ||
                this.Query != cs.Query ||
                this.RsaKeyFile != cs.RsaKeyFile ||
                this.Stream != cs.Stream ||
                this.UserName != cs.UserName)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields = 
                { 
                    Host, Port, UserName, Password, 
                    Stream, IdColumn, IsEncrypted, 
                    HotSpareHost, HotSparePort, 
                    Query, AuthenticationType, RsaKeyFile,
                    IgnoreNulls, EncloseParameterInQuote
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }

        public bool IsStreamConnectionMode
        {
            get { return ConnectionMode == ConnectionMode.Stream; }
            set
            {
                if (value)
                {
                    ConnectionMode = ConnectionMode.Stream;
                }
            }
        }
        
        public bool IsQueryConnectionMode
        {
            get { return ConnectionMode == ConnectionMode.Query; }
            set
            {
                if (value)
                {
                    ConnectionMode = ConnectionMode.Query;
                }
            }
        }

        public SpAuthType[] AuthenticationTypes
        {
            get
            {
                return new SpAuthType[] 
                    { 
                        SpAuthType.AUTH_NONE, 
                        SpAuthType.AUTH_PAM,
                        SpAuthType.AUTH_RSA,
                        SpAuthType.AUTH_KERBV5
                    };
            }
        }

        public string[] Streams
        {
            get { return streams; }
            set
            {
                if (streams != value)
                {
                    streams = value;
                    FirePropertyChanged("Streams");
                }
            }
        }

        public ICommand ConnectCommand
        {
            get
            {
                if (connectCommand == null) {
                    connectCommand = new DelegateCommand(Connect, CanConnect);
                }
                return connectCommand;
            }
        }

        private bool CanConnect()
        {
            return Host != null && Host.Length > 0;
        }

        public void Connect()
        {
            try {
                ConnectionInfo info = ConnectionInfo.Connect(this);
                Streams = info.GetStreamNames();
                UpdateColumns();
            }
            catch (Exception e)
            {
                ShowErrorMessageArgs args = new ShowErrorMessageArgs(this,
                    e.Message);
                Mediator.Instance.NotifyColleagues("ShowErrorMessage", args);
            }
        }

        public void UpdateColumns()
        {
            try
            {
                if (Host == null || Host.Length == 0)
                {
                    IdColumnCandidates = null;
                    TimeIdColumnCandidates = null;
                    return;
                }

                ConnectionInfo info = ConnectionInfo.Connect(this);
                SpStream stream;
                SpStreamDefinition sdef;


                if (ConnectionMode == ConnectionMode.Stream)
                {
                    if (Stream == null) return;
                    stream = info.GetStream(Stream);
                    sdef = stream.getDefinition();
                }
                else
                {
                    if (Query == null) return;

                    SpSubscriptionProjection projectionSubscription = 
                        info.CreateSubscriptionProjection(Query);

                    if (projectionSubscription == null) return;

                    SpStreamProjection streamProjection = 
                        projectionSubscription.getStreamProjection();
                    
                    sdef = streamProjection.getDefinition();
                }

                int numColumns = sdef.getNumColumns();
                int[] colTypes = sdef.getColumnTypes();
                string[] colNames = sdef.getColumnNames();
                List<string> idColumns = new List<string>();
                List<string> timeIdColumns = new List<string>();
                
                for (int i = 0; i < numColumns; i++)
                {
                    if (colTypes[i] == SpDataTypes.STRING ||
                        colTypes[i] == SpDataTypes.INT16 ||
                        colTypes[i] == SpDataTypes.INT32 ||
                        colTypes[i] == SpDataTypes.INT64)
                    {
                        idColumns.Add(colNames[i]);
                    }
                    else if (colTypes[i] == SpDataTypes.TIMESTAMP ||
                        colTypes[i] == SpDataTypes.DATE)
                    {
                        timeIdColumns.Add(colNames[i]);
                    }
                }

                IdColumnCandidates = idColumns.ToArray();
                TimeIdColumnCandidates = timeIdColumns.ToArray();
                
                info.Status.clear();
            }
            catch(Exception e)
            {
                ShowErrorMessageArgs args = new ShowErrorMessageArgs(this,
                    e.Message);
                Mediator.Instance.NotifyColleagues("ShowErrorMessage", args);
            }
        }

        public bool IsOK
        {
            get
            {
                return Host != null &&
                    Host.Length > 0 &&
                    ((ConnectionMode == ConnectionMode.Stream && 
                      Stream != null &&
                      Stream.Length > 0) ||
                     (ConnectionMode == ConnectionMode.Query &&
                      Query != null && 
                      Query.Length > 0)) && 
                    IdColumn != null &&
                    IdColumn.Length > 0 &&
                    (!IsTimeIdColumnGenerated ||
                     (TimeIdColumn != null && TimeIdColumn.Length > 0));
            }
        }
    }
}
