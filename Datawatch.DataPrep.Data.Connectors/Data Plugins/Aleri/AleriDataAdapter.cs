﻿using System;
using aleri_pubsubnet;
using aleri_pubsubconst;
using Panopticon.AleriPlugin.Properties;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.AleriPlugin
{
    public class AleriDataAdapter : 
        RealtimeDataAdapter<ParameterTable, AleriSettings, SpSubscriptionEvent>, 
        SpObserver
    {
        private ConnectionInfo connection;
        private SpStreamDefinition streamDefinition;
        private SpSubscription subscription;
        private SpSubscriptionProjection subscriptionProjection;
        private int cookie;
        int[] colTypes;
        private int idColumnIndex = -1;
        private int timeIdColumnIndex = -1;
        private bool ignoreNulls;

        private void Enqueue(SpSubscriptionEvent evt)
        {
            //DateTime start = DateTime.Now;
            Object[] dvec = (Object[])evt.getData();

            if (dvec == null)
            {
                Log.Error(Resources.ExDataArrayNull, getName());
                return;
            }

            int numFields = dvec.Length;
            if (settings.IsTimeIdColumnGenerated &&
                settings.TimeIdColumn != null &&
                settings.TimeIdColumn.Length > 0)
            {
                numFields++;
            }

            //
            // For a sanity check, make sure numFields in the event structure
            // is equal to the number of columns in the stream definition.
            //
            if (numFields != table.ColumnCount)
            {
                Log.Error(Resources.ExNumFields, getName(), numFields,
                    table.ColumnCount);
                return;
            }

            if (idColumnIndex > numFields)
            {
                return;
            }

            int opcode = evt.getStreamOpCode();

            if (dvec[idColumnIndex] == null)
            {
                return;
            }

            string id = dvec[idColumnIndex].ToString();

            lock (this)
            {
                if (opcode == SpOpCodes.DELETE ||
                    opcode == SpOpCodes.SAFEDELETE)
                {
                    updater.EnqueueDelete(id, evt);
                }
                else if (opcode == SpOpCodes.INSERT)
                {
                    updater.EnqueueInsert(id, evt);
                }
                else
                {
                    updater.EnqueueUpdate(id, evt);
                }
            }
        }

        public string getName()
        {
            if (settings.ConnectionMode == ConnectionMode.Stream)
            {
                return settings.Stream + "_observer";
            }
            return settings.Query + "_observer";
        }

        public void notify(SpSubscriptionEvent[] theEvents)
        {
            int numEvents = theEvents.Length;

            for (int i = 0; i < numEvents; i++)
            {
                SpSubscriptionEvent ev = theEvents[i];
                if (ev.getType() == SpEventType.PARSED_DATA)
                {
                    if (ev.getId() == SpEventId.GATEWAY_SYNC_START)
                    {
                        Log.Info(Resources.LogGatewaySynchStart);
                        Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                    }
                    else
                    {
                        if (ev.getId() == SpEventId.GATEWAY_SYNC_END)
                        {
                            Log.Info(Resources.LogGatewaySynchEnd);
                        }
                        else
                        {
                            if (ev.getId() == SpEventId.PARSED_FIELD_DATA)
                            {
                                //
                                // Output the parsed field data information.
                                //
                                if (ev.getStreamOpCode() != SpOpCodes.NOOP)
                                {
                                    Enqueue(ev);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (ev.getType() == SpEventType.SYSTEM)
                    {
                        //
                        // System Event Type Detected.
                        //
                        Log.Info(Resources.LogSystemEvent,
                            getName(), ev.getId(), ev.getIdName());

                        if (ev.getId() == SpEventId.COMMUNICATOR_HALTED ||
                            ev.getId() == SpEventId.PLATFORM_SHUTDOWN)
                        {
                            updater.ConnectionLost(true);
                        }
                    }
                }
            }
        }

        public override void Start()
        {
            for (int i = 0; i < table.ColumnCount; i++)
            {
                if (table.GetColumn(i).Name.Equals(settings.IdColumn))
                {
                    idColumnIndex = i;
                }
                else if (table.GetColumn(i).Name.Equals(settings.TimeIdColumn))
                {
                    timeIdColumnIndex = i;
                }
            }

            ignoreNulls = settings.IgnoreNulls;
            connection = ConnectionInfo.Connect(settings);
            connection.Status.clear();

            if (settings.ConnectionMode == ConnectionMode.Stream)
            {
                Log.Info(Properties.Resources.LogStartStream, settings.Stream);

                SpStream stream = connection.GetStream(settings.Stream);
                streamDefinition = stream.getDefinition();

                subscription = connection.CreateSubscription(settings.Stream);

                if (subscription == null)
                {
                    throw Exceptions.CreateSubscription(getName(),
                        connection.Status.getErrorMessage());
                }

                // According to Dave Rosenblum, something like this would
                // make pulse streams in the Aleri model unnecessary.
                //
                //if (settings.PulseInterval > 0) {
                //    subscription.setPulseInterval(1);
                //}

                //
                // 2) Add the "observer" for the stream to the subscription.  If successfull, this call returns a 
                // "handle"/"cookie" for the observer that was added.  This handle/cookie can be used later on
                // to remove the observer if so desired.
                //
                cookie = subscription.addStreamObserver(settings.Stream, this);
            }
            else
            {
                string query = Utils.ApplyParameters(settings.Query,
                    table.Parameters, settings.EncloseParameterInQuote);

                Log.Info(Properties.Resources.LogStartStreamProjection, query);

                subscriptionProjection =
                    connection.CreateSubscriptionProjection(query);
                if (subscriptionProjection == null)
                {
                    throw Exceptions.CreateSubscriptionProjection(getName(),
                        connection.Status.getErrorMessage());
                }
                SpStreamProjection streamProjection =
                    subscriptionProjection.getStreamProjection();
                streamDefinition = streamProjection.getDefinition();
                cookie = subscriptionProjection.addObserver(this);
            }

            colTypes = streamDefinition.getColumnTypes();

            if (cookie <= 0)
            {
                throw Exceptions.AddStreamObserver(getName(), cookie);
            }

            //
            // 2a) If the pulse interval is specified then set the pulse interval.  This automatically
            //     adds the necessary subscription flags for pulse subscription.

            //if (pulseInterval > 0)
            //{
            //    sub.setPulseInterval(pulseInterval);
            //}
            //
            // 3) Start the subscription object, which opens up an underlying 
            //    Gateway I/O socket connection.
            //
            int rc;

            if (settings.ConnectionMode == ConnectionMode.Stream)
            {
                rc = subscription.start();
            }
            else
            {
                rc = subscriptionProjection.start();
            }

            if (0 != rc)
            {
                throw Exceptions.StartSubscription(getName(), SpUtils.getErrorMessage(rc));
            }
            
        }

        public override void Stop()
        {
            if (subscription != null)
            {
                Log.Info(Properties.Resources.LogStopStream, settings.Stream);

                if (cookie > 0)
                {
                    subscription.removeObserver(cookie);
                }

                int rc = subscription.stop();
                if (0 != rc)
                {
                    Log.Error(Properties.Resources.LogStopStreamError,
                        SpUtils.getErrorMessage(rc));
                    return;
                }

                subscription = null;
            }

            if (subscriptionProjection != null)
            {
                Log.Info(Properties.Resources.LogStopStreamProjection,
                    Utils.ApplyParameters(settings.Query, table.Parameters,
                    settings.EncloseParameterInQuote));

                if (cookie > 0)
                {
                    subscriptionProjection.removeObserver(cookie);
                }

                int rc = subscriptionProjection.stop();
                if (0 != rc)
                {
                    Log.Error(Properties.Resources.LogStopStreamProjectionError,
                        SpUtils.getErrorMessage(rc));
                    return;
                }

                subscriptionProjection = null;
            }

            streamDefinition = null;
            connection = null;
            cookie = 0;
            
        }

        public override void UpdateColumns(Row row, SpSubscriptionEvent evt)
        {
            Object[] dvec = (Object[])evt.getData();

            for (int i = 0; i < dvec.Length; i++)
            {
                if (i == idColumnIndex) continue;

                Object dv = dvec[i];
                int type = colTypes[i];

                if (ignoreNulls && dv == null)
                {
                    continue;
                }

                if (colTypes[i] == SpDataTypes.STRING)
                {
                    TextColumn textColumn = (TextColumn)table.GetColumn(i);
                    string str = (string)dv;
                    string oldStr = textColumn.GetTextValue(row);
                    if (oldStr != str)
                    {
                        textColumn.SetTextValue(row, str);
                    }
                }
                else if (colTypes[i] == SpDataTypes.DOUBLE ||
                            colTypes[i] == SpDataTypes.MONEY)
                {
                    double d;

                    if (dv == null)
                    {
                        d = Double.NaN;
                    }
                    else if (colTypes[i] == SpDataTypes.MONEY)
                    {
                        d = connection.Platform.moneyToDouble(
                            Convert.ToInt64(dv));
                    }
                    else
                    {
                        d = (double)dv;
                    }
                    NumericColumn numericColumn =
                        (NumericColumn) table.GetColumn(i);
                    double oldVal = numericColumn.GetNumericValue(row);
                    if (oldVal != d)
                    {
                        numericColumn.SetNumericValue(row, d);
                    }
                }
                else if (colTypes[i] == SpDataTypes.TIMESTAMP ||
                            colTypes[i] == SpDataTypes.DATE)
                {
                    if (dv == null)
                    {
                        dv = DateTime.MinValue;
                    }
                    TimeColumn timeColumn = (TimeColumn) table.GetColumn(i);
                    DateTime dateTime = (DateTime)dv;
                    if (timeColumn.GetTimeValue(row) != dateTime)
                    {
                        timeColumn.SetTimeValue(row, dateTime);
                    }
                }
                else if (colTypes[i] == SpDataTypes.INT16 ||
                            colTypes[i] == SpDataTypes.INT32 ||
                            colTypes[i] == SpDataTypes.INT64)
                {
                    NumericColumn numericColumn =
                        (NumericColumn) table.GetColumn(i);
                    double oldVal = numericColumn.GetNumericValue(row);
                    double newVal = Convert.ToDouble(dv);
                    if (oldVal != newVal)
                    {
                        numericColumn.SetNumericValue(row, newVal);
                    }
                }
            }
        }

        public override DateTime GetTimestamp(SpSubscriptionEvent evt)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return DateTime.MinValue;
            }

            Object[] dvec = (Object[])evt.getData();
            object dv = dvec[timeIdColumnIndex];
            return dv != null ? (DateTime) dv : DateTime.MinValue;
        }
    }
}
