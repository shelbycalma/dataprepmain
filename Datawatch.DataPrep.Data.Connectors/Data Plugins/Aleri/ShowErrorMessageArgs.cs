﻿namespace Panopticon.AleriPlugin
{
    public class ShowErrorMessageArgs
    {
        private AleriSettings viewModel;
        private string message;

        public ShowErrorMessageArgs(AleriSettings viewModel, string message)
        {
            this.viewModel = viewModel;
            this.message = message;
        }

        public AleriSettings ViewModel
        {
            get { return viewModel; }
        }

        public string Message
        {
            get { return message; }
        }
    }
}
