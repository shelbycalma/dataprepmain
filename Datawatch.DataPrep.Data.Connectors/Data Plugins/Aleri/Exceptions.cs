﻿using System;
using Panopticon.AleriPlugin.Properties;

namespace Panopticon.AleriPlugin
{
    class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        public static Exception CreatePlatform(string errorMessage)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExCreatePlatform, errorMessage));
        }

        public static Exception CreatePlatformStatusAndParams()
        {
            return CreateInvalidOperation(Resources.ExCreatePlatformStatusAndParams);
        }

        public static Exception CreateSubscription(string name, string stream)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExCreateSubscription, name, stream));
        }

        public static Exception CreateSubscriptionProjection(string name, string query)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExCreateSubscriptionProjection, 
                    name, query));
        }

        public static Exception ExFailedSubscriptionProjection()
        {
            return CreateArgument(Resources.ExFailedSubscriptionProjection);
        }
        
        public static Exception GetStream(string name)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExGetStream, name));
        }

        public static Exception AddStreamObserver(string name, int cookie)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExAddStreamObserver, name, cookie));
        }

        public static Exception StartSubscription(string name, string error)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExStartSubscription, name, error));
        }
    }
}
