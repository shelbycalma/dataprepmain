﻿using System.Collections.Generic;
using aleri_pubsubconst;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.AleriPlugin
{
    internal class Utils
    {      
        public static string ApplyParameters(string query,
            IEnumerable<ParameterValue> parameters,
            bool enforceSingleQuoteEnclosure)
        {
            if (parameters == null) return query;
            return new ParameterEncoder
            {
                SourceString = query,
                Parameters = parameters,
                EnforceSingleQuoteEnclosure = enforceSingleQuoteEnclosure,
                NullOrEmptyString = "NULL"
            }.Encoded();
        }

        public static string GetStreamOpCodeName(int streamOpCode)
        {

            if (streamOpCode == SpOpCodes.NOOP) return "NOOP";
            else if (streamOpCode == SpOpCodes.INSERT) return "INSERT";
            else if (streamOpCode == SpOpCodes.UPSERT) return "UPSERT";
            else if (streamOpCode == SpOpCodes.UPDATE) return "UPDATE";
            else if (streamOpCode == SpOpCodes.DELETE) return "DELETES";

            return "UNKNOWN";
        }

        //
        // Given the numeric representation of the DataType, lookup the corresponding DataType name.
        //
        public static string GetDataTypeName(int dataType)
        {
            if (dataType == SpDataTypes.INT16) return "INT16";
            else if (dataType == SpDataTypes.INT32) return "INT32";
            else if (dataType == SpDataTypes.INT64) return "INT64";
            else if (dataType == SpDataTypes.DOUBLE) return "DOUBLE";
            else if (dataType == SpDataTypes.DATE) return "DATE";
            else if (dataType == SpDataTypes.STRING) return "STRING";
            else if (dataType == SpDataTypes.MONEY) return "MONEY";
            else if (dataType == SpDataTypes.TIMESTAMP) return "TIMESTAMP";
            else if (dataType == SpDataTypes.NULLVALUE) return "NULLVALUE";

            return "UNKNOWN";
        }

        // Returns true for a valid field type
        public static bool IsValidFieldType(int fieldType)
        {
            if (fieldType == SpDataTypes.INT16 ||
                fieldType == SpDataTypes.INT32 ||
                fieldType == SpDataTypes.INT64 ||
                fieldType == SpDataTypes.DOUBLE ||
                fieldType == SpDataTypes.DATE ||
                fieldType == SpDataTypes.STRING ||
                fieldType == SpDataTypes.MONEY ||
                fieldType == SpDataTypes.TIMESTAMP ||
                fieldType == SpDataTypes.NULLVALUE)
            {
                return true;
            }

            return false;
        }
    }
}
