﻿using System;
using System.Collections.Generic;
using System.Text;
using aleri_pubsubnet;
using aleri_pubsubconst;

namespace Panopticon.AleriPlugin
{
    class ConnectionInfo
    {
        public readonly SpPlatform Platform;
        public readonly SpPlatformStatus Status;
        public readonly SpPlatformParms Params;

        private static readonly string ALERI_SUBSCRIPTIONPREFIX = "ex_subscription_";
        private static int subscriptionIndex = 1;

        public ConnectionInfo(SpPlatform platform, SpPlatformStatus status, SpPlatformParms para) 
        {
            Platform = platform;
            Status = status;
            Params = para;
        }

        public static ConnectionInfo Connect(AleriSettings settings)
        {
            try
            {
                int rc = SpFactory.init();

                // Create the SpPlatformParms and SpPlatformStatus objects need to create the SpPlatform object.
                SpPlatformParms spParms;

                string password;

                if (settings.AuthenticationType == SpAuthType.AUTH_RSA)
                {
                    string assemblyPath = 
                        typeof(ConnectionInfo).Assembly.Location;
                    
                    password = System.IO.Path.Combine(
                        System.IO.Path.GetDirectoryName(assemblyPath), 
                            settings.RsaKeyFile);
                }
                else
                {
                    password = settings.Password;
                }

                spParms = SpFactory.createPlatformParms(
                    settings.Host,
                    settings.Port,
                    settings.UserName != null ? settings.UserName : "",
                    password != null ? password : "",
                    settings.IsEncrypted,
                    settings.AuthenticationType,
                    settings.HotSpareHost != null ? settings.HotSpareHost : "",
                    settings.HotSparePort);                               

                SpPlatformStatus spStatus = SpFactory.createPlatformStatus();

                if (spStatus != null && spParms != null)
                {
                    SpPlatform spPlatform = SpFactory.createPlatform(spParms, spStatus);
                    if (spPlatform == null)
                    {
                        throw Exceptions.CreatePlatform(spStatus.getErrorMessage());
                    }
                    return new ConnectionInfo(spPlatform, spStatus, spParms);
                }
                throw Exceptions.CreatePlatformStatusAndParams();
            }
            finally
            {
                SpFactory.dispose();
            }
        }

        public SpStream GetStream(string name)
        {
            if (name == null) throw Exceptions.ArgumentNull("name");

            SpStream stream = Platform.getStream(name);
            if (stream == null)
            {
                throw Exceptions.GetStream(name);
            }
            return stream;
        }

        public SpSubscription CreateSubscription(string stream)
        {
            return Platform.createSubscription(
                string.Format(ALERI_SUBSCRIPTIONPREFIX, subscriptionIndex),
                SpSubFlags.BASE | SpSubFlags.LOSSY, 
                SpDeliveryType.DELIVER_PARSED,
                Status);
        }

        public SpSubscriptionProjection CreateSubscriptionProjection(string query)
        {
            return Platform.createSubscriptionProjection(
                string.Format(ALERI_SUBSCRIPTIONPREFIX, subscriptionIndex++),
                SpSubFlags.BASE | SpSubFlags.LOSSY,
                SpDeliveryType.DELIVER_PARSED,
                query,
                Status);
        }

        public string[] GetStreamNames()
        {
            List<string> names = new List<string>();
            SpStream[] streams = Platform.getStreams();

            foreach (SpStream stream in streams)
            {
                names.Add(stream.getName());
            }

            return names.ToArray();
        }
    }
}
