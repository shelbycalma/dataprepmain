﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using aleri_pubsubnet;
using aleri_pubsubconst;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.AleriPlugin.Properties;

namespace Panopticon.AleriPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : 
        RealtimeDataPlugin<ParameterTable,AleriSettings,
            SpSubscriptionEvent,AleriDataAdapter>
    {
        internal const string PluginId = "AleriPlugin";
        internal const string PluginTitle = "Sybase Aleri";
        internal const string PluginType = DataPluginTypes.Streaming;
        internal const bool PluginIsObsolete = true;

        public override AleriSettings CreateSettings(PropertyBag bag)
        {
            return new AleriSettings(bag);
        }

        protected override ITable CreateTable(AleriSettings settings,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;
            ConnectionInfo info = ConnectionInfo.Connect(settings);

            SpStreamDefinition sdef = null;

            if (settings.ConnectionMode == ConnectionMode.Stream)
            {
                SpStream stream = info.GetStream(settings.Stream);
                sdef = stream.getDefinition();
            }
            else
            {
                string query = Utils.ApplyParameters(
                    settings.Query, parameters,
                    settings.EncloseParameterInQuote);
                SpSubscriptionProjection subscriptionProjection =
                    info.CreateSubscriptionProjection(query);
                
                if (subscriptionProjection == null)
                {
                    throw Exceptions.ExFailedSubscriptionProjection();
                }

                sdef = subscriptionProjection.getStreamProjection().getDefinition();
            }

            if (sdef == null) return EmptyTable.Instance;

            int numColumns = sdef.getNumColumns();
            int[] colTypes = sdef.getColumnTypes();
            string[] colNames = sdef.getColumnNames();

            ParameterTable table = new ParameterTable(parameters);
            table.BeginUpdate();
            try
            {
                for (int i = 0; i < numColumns; i++)
                {
                    if (colNames[i] == settings.IdColumn ||
                        colTypes[i] == SpDataTypes.STRING)
                    {
                        table.AddColumn(new TextColumn(colNames[i]));
                    }
                    else if (colTypes[i] == SpDataTypes.DOUBLE ||
                             colTypes[i] == SpDataTypes.MONEY)
                    {
                        table.AddColumn(new NumericColumn(colNames[i]));
                    }
                    else if (colTypes[i] == SpDataTypes.TIMESTAMP ||
                             colTypes[i] == SpDataTypes.DATE)
                    {
                        table.AddColumn(new TimeColumn(colNames[i]));
                    }
                    else if (colTypes[i] == SpDataTypes.INT16 ||
                             colTypes[i] == SpDataTypes.INT32 ||
                             colTypes[i] == SpDataTypes.INT64)
                    {
                        table.AddColumn(new NumericColumn(colNames[i]));
                    }
                }
            }
            finally {
                table.EndUpdate();
            }
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;            
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }
    }
}
