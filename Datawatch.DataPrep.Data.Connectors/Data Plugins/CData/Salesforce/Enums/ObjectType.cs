﻿namespace Panopticon.CData.Salesforce.Enums
{
    public enum ObjectType
    {
        Unknown,
        Table,
        Report,
    }
}