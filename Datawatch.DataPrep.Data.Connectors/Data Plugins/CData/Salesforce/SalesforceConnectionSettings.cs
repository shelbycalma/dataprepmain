﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;
using Panopticon.CData.Salesforce.Enums;


namespace Panopticon.CData.Salesforce
{
    [DataHelpKey("SalesforceCom")]
    public class SalesforceConnectionSettings : CDataSettingsBase
    {
        private string salesforceDirectory;
        private const string SalesforceKey = "Salesforce CData";

		public SalesforceConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public SalesforceConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
            salesforceDirectory = Path.Combine(Path.GetTempPath(), "Datawatch Designer",
                    SalesforceKey);

            if (!Directory.Exists(salesforceDirectory))
            {
                Directory.CreateDirectory(salesforceDirectory);
                Log.Info(Resources.LogSalesforceCreateFolder, salesforceDirectory);
            }

            QuerySettings.IsOnDemand = false;
            QuerySettings.IsReportSupported = true;

            QuerySettings.ReportGroupListSelectQuery = "SELECT FolderName FROM Report ";
            QuerySettings.ReportGroupListWhereQuery = "";
            QuerySettings.ReportGroupListOrderByQuery = " ORDER BY FolderName";

            QuerySettings.ReportListSelectQuery = "SELECT Name, ID FROM Report ";
            QuerySettings.ReportListWhereQuery = "  WHERE FolderName = '";
            QuerySettings.ReportListOrderByQuery = "' ORDER BY Name";

            QuerySettings.ReportSelectQuery = "EXECUTE GetCustomReport ";
            QuerySettings.ReportWhereQuery = " ReportID = '";
            QuerySettings.ReportOrderByQuery = "";
        }

		public ObjectType ObjectType
		{
			get
			{
				string s = GetInternal("ObjectType",
					Enum.GetName(typeof(ObjectType), ObjectType.Unknown));
				return (ObjectType)Enum.Parse(typeof(ObjectType), s, true);
			}
			set
			{
				SetInternal("ObjectType",
					Enum.GetName(typeof(ObjectType), value));
				//SetTitle();
			}
		}

		public override string Host
        {
            get { return GetInternal("Host", "login.salesforce.com"); }
            set { SetInternal("Host", value); }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for Salesforce";
            }
        }

        public override bool IsCDataDriver => true;

        public override string DatabaseName
        {
            get 
            {
                return salesforceDirectory + @"\" + UserId;
            }
            set
            {
                base.DatabaseName = value;
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSalesforcePluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSalesforceWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Host) || string.IsNullOrEmpty(SecurityToken) || string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public string SecurityToken
        {
            get { return GetInternal("SecurityToken"); }
            set { SetInternal("SecurityToken", value); }
        }

        public override string PluginConnectionString
        {
            get
            {
                StringBuilder connectionString = new StringBuilder();
                if (string.IsNullOrEmpty(ConnectionOptions))
                {
                    connectionString.Append("ConfigOptions={AuditColumns=All}");
                }
                if (!string.IsNullOrEmpty(SecurityToken))
                {
                    if (connectionString.Length > 0)
                    {
                        connectionString.Append(";");
                    }
                    connectionString.Append("Security Token=" + SecurityToken);
                }
                return connectionString.ToString();
            }
        }
    }
}
