﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;


namespace Panopticon.CData.Zendesk
{
    [DataHelpKey("ZendeskCom")]
    public class ZendeskConnectionSettings : CDataSettingsBase
    {
		public ZendeskConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public ZendeskConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
        }

		public override string Url
        {
            get { return GetInternal("Url", ""); }
            set { SetInternal("Url", value); }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for Zendesk";
            }
        }

        public override bool IsCDataDriver => true;

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiZendeskPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiZendeskWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Url) || string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                StringBuilder connectionString = new StringBuilder();
                if (string.IsNullOrEmpty(ConnectionOptions))
                {
                    connectionString.Append("ConfigOptions={AuditColumns=All}");
                }
                return connectionString.ToString();
            }
        }
    }
}
