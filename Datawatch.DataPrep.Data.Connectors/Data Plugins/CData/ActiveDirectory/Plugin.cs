﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.CData.ActiveDirectory
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : OdbcPluginBase<ActiveDirectoryConnectionSettings>
    {
        internal const string PluginId = "CDataOdbcActiveDirectory";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "Active Directory";
        internal const string PluginType = DataPluginTypes.Database;

        public override ActiveDirectoryConnectionSettings GetSettings(PropertyBag properties,
            IEnumerable<ParameterValue> parameters)
        {
            return new ActiveDirectoryConnectionSettings(properties, parameters);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return Plugin.PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override string DriverName
        {
            get { return "DATAWATCH ODBC DRIVER FOR ACTIVEDIRECTORY"; }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }
    }
}
