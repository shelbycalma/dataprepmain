﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.ActiveDirectory
{
    public class ActiveDirectoryConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

		public ActiveDirectoryConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public ActiveDirectoryConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for ActiveDirectory";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiActiveDirectoryPluginTitle;
            }
        }

		public string IgnoreSSLServerCert
		{
			get { return GetInternal("IgnoreSSLServerCert", "false"); }
			set { SetInternal("IgnoreSSLServerCert", value.ToString()); }
		}

		public string BaseDN
		{
			get { return GetInternal("BaseDN", ""); }
			set { SetInternal("BaseDN", value); }
		}

		public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiActiveDirectoryWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Host))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

		public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();

				if (!string.IsNullOrEmpty(IgnoreSSLServerCert))
				{
					if (IgnoreSSLServerCert != "false")
					{
						if (connectionString.Length > 0)
						{
							connectionString.Append(";");
						}
						connectionString.Append("SSL Server Cert=*");
					}
				}

				if (!string.IsNullOrEmpty(BaseDN))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("BaseDN=" + BaseDN);
				}
                connectionString.Append(";ConnectOnOpen=true");

                return connectionString.ToString();
			}
		}
	}
}
