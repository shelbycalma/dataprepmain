﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.CData.SparkSQL
{
	[PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
	public class Plugin : OdbcPluginBase<SparkSQLConnectionSettings>
	{
		internal const string PluginId = "CDataOdbcSparkSQL";
		internal const bool PluginIsObsolete = false;
		internal const string PluginTitle = "Spark SQL";
		internal const string PluginType = DataPluginTypes.Database;

		public override SparkSQLConnectionSettings GetSettings(PropertyBag properties,
			IEnumerable<ParameterValue> parameters)
		{
			return new SparkSQLConnectionSettings(properties, parameters);
		}

		public override string DataPluginType
		{
			get { return DataPluginTypes.GetLocalized(PluginType); }
		}

		public override string Id
		{
			get { return PluginId; }
		}

		public override bool IsLicensed
		{
			get { return LicenseCache.IsValid(GetType(), this); }
		}

		public override bool IsObsolete
		{
			get { return PluginIsObsolete; }
		}

		public override string Title
		{
			get { return PluginTitle; }
		}

		public override string DriverName
		{
			get { return "DATAWATCH ODBC DRIVER FOR SparkSQL"; }
		}
	}
}
