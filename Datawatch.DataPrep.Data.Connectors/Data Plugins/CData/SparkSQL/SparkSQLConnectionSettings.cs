﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.SparkSQL
{
    public class SparkSQLConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

        public SparkSQLConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public SparkSQLConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
            //set default Port number
            Port = "10015";
		}

        protected override string DatabaseListQuery
        {
            get
            {
                return "SELECT SchemaName FROM sys_schemas";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "10015");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for SparkSQL";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSparkSqlPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSparkSqlWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

    }
}
