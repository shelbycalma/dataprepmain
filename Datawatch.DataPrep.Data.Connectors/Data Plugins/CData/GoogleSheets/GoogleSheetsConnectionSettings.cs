﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.GoogleSheets
{
    public class GoogleSheetsConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

		public GoogleSheetsConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public GoogleSheetsConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for GoogleSpreadsheets";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiGoogleSheetsPluginTitle;
            }
        }

        public string IgnoreSSLServerCert
        {
            get { return GetInternal("IgnoreSSLServerCert", "false"); }
            set { SetInternal("IgnoreSSLServerCert", value.ToString()); }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiGoogleSheetsWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public string ShowTrashedFiles
        {
            get { return GetInternal("ShowTrashedFiles", "false"); }
            set { SetInternal("ShowTrashedFiles", value.ToString()); }
        }

        public override string IsReset
        {
            get { return GetInternal("IsReset", "false"); }
            set { SetInternal("IsReset", value.ToString()); }
        }

		public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();
				//if (string.IsNullOrEmpty(ConnectionOptions))
				//{
				//	connectionString.Append("Other='PromptMode=prompt;InitiateOAuth=GETANDREFRESH;OAuthSettingsLocation=%AppData%\\CData\\GoogleSheets Data Provider\\OAuthSettings.txt'");
				//}

				if (!string.IsNullOrEmpty(IgnoreSSLServerCert))
                {
                    if (IgnoreSSLServerCert != "false")
                    {
                        if (connectionString.Length > 0)
                        {
                            connectionString.Append(";");
                        }
                        connectionString.Append("SSL Server Cert=*");
                    }
                }

                if (!string.IsNullOrEmpty(ShowTrashedFiles))
                {
                    if (connectionString.Length > 0)
                    {
                        connectionString.Append(";");
                    }
                    connectionString.Append("ShowTrashedFiles=" + ShowTrashedFiles);
                }

                if (connectionString.Length > 0)
                {
                    connectionString.Append(";");
                }
                connectionString.Append("UseSimpleNames=true");

                return connectionString.ToString();
			}
		}

	}
}
