﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.Box
{
    public class BoxConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

		public BoxConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public BoxConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for Box";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiBoxPluginTitle;
            }
        }

        public string IgnoreSSLServerCert
        {
            get { return GetInternal("IgnoreSSLServerCert", "false"); }
            set { SetInternal("IgnoreSSLServerCert", value.ToString()); }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiBoxWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                //if (string.IsNullOrEmpty(Url))
                //{
                //    return false;
                //}

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public override string IsReset
        {
            get { return GetInternal("IsReset", "false"); }
            set { SetInternal("IsReset", value.ToString()); }
        }

        public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();
				//if (string.IsNullOrEmpty(ConnectionOptions))
				//{
				//	connectionString.Append("Other='PromptMode=prompt;InitiateOAuth=GETANDREFRESH;OAuthSettingsLocation=%AppData%\\CData\\Box Data Provider\\OAuthSettings.txt'");
				//}

				if (!string.IsNullOrEmpty(IgnoreSSLServerCert))
                {
                    if (IgnoreSSLServerCert != "false")
                    {
                        if (connectionString.Length > 0)
                        {
                            connectionString.Append(";");
                        }
                        connectionString.Append("SSL Server Cert=*");
                    }
                }

				return connectionString.ToString();
			}
		}

	}
}
