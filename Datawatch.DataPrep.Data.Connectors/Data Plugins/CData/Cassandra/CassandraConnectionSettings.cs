﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using System.Text;

namespace Panopticon.CData.Cassandra
{
    [DataHelpKey("Cassandra")]
    public class CassandraConnectionSettings : CDataSettingsBase
    {
        private IEnumerable<ParameterValue> parameters;

        public CassandraConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public CassandraConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
            this.parameters = parameters;
            //set default Port number
            Port = "9042";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "SELECT SchemaName FROM sys_schemas";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "9042");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public virtual bool IsLegacy
        {
            get { return false; }
        }

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for Cassandra";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiCassandraPluginTitle;
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                StringBuilder connectionString = new StringBuilder();
                if (string.IsNullOrEmpty(ConnectionOptions))
                {
                    connectionString.Append("Timeout=1200");
                }
                else
                { 
                    if (!ConnectionOptions.Contains("Timeout"))
                    {
                        if (connectionString.Length > 0)
                        {
                            connectionString.Append(";");
                        }
                        connectionString.Append("Timeout=1200");
                    }
                }
                return connectionString.ToString();
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiCassandraWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

    }
}
