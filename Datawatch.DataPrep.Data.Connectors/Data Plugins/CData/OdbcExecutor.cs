﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.CData
{
    internal class OdbcExecutor : OdbcExecutorBase
    {
        public override DbDataAdapter GetAdapter(string queryString,
            string connectionString)
        {
            OdbcDataAdapter adapter =
                new OdbcDataAdapter(queryString, connectionString);
            return adapter;
        }

        public override DbConnection GetConnection(string connectionString)
        {
            OdbcConnection connection = new OdbcConnection(connectionString);
            return connection;
        }

    }
}
