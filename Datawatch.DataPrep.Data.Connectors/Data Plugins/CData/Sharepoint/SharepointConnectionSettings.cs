﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.Sharepoint
{
    public class SharepointConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;
        private bool _OnPremise = true;
        private bool _Online = false;

        public SharepointConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public SharepointConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for Sharepoint";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSharepointPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSharepointWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Url) || string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public bool OnPremise
        {
            get { return (GetInternal("OnPremise","true") == "true" ? true : false); }
            set { SetInternal("OnPremise", value == true ? "true":"false"); }
        }

        public bool Online
        {
            get { return (GetInternal("Online") == "true" ? true : false); }
            set { SetInternal("Online", value == true ? "true" : "false"); }
        }

        public override string PluginConnectionString
        {
            get
            {
                StringBuilder connectionString = new StringBuilder();

                if (Online)
                {
                    connectionString.Append(";" + "Share Point Edition=SharePoint Online");
                }
                if (OnPremise)
                {
                    connectionString.Append(";" + "Share Point Edition=SharePoint OnPremise");
                }

                return connectionString.ToString();
            }
        }
    }
}
