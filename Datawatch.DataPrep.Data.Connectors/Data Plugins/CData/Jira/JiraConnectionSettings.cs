﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.Jira
{
    public class JiraConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

        public JiraConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public JiraConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for JIRA";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiJiraPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiJiraWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Url) || string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                StringBuilder connectionString = new StringBuilder();

                //Jira is deprecating the password property so we will use the APIToken property instead
                //As of March 27, 2019 the APIToken property accepts the account password and APIToken as valid APIToken values
                //so users can enter the APIToken or password which comes from the UI and stored in the Password property below 
                if (string.IsNullOrEmpty(Password))
                {
                    connectionString.Append("APIToken=" + Password);
                }
                else
                {
                    if (connectionString.Length > 0)
                    {
                        connectionString.Append(";");
                    }
                    connectionString.Append("APIToken=" + Password);
                }

                //Added ConnectOnOpen=True to validate credentials on opening a connection
                if (connectionString.Length > 0)
                {
                    connectionString.Append(";");
                }
                connectionString.Append("ConnectOnOpen=True");

                return connectionString.ToString();
            }
        }
    }
}
