﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.SugarCRM
{
    public class SugarCRMConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

        public SugarCRMConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public SugarCRMConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for SugarCRM";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSugarCRMPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSugarCRMWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Url) || string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

    }
}
