﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.Netsuite
{
    public class NetsuiteConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

		public NetsuiteConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public NetsuiteConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for NetSuite";
            }
        }

		public string AccountID
		{
			get { return GetInternal("AccountID", ""); }
			set { SetInternal("AccountID", value); }
		}

		public string RoleID
		{
			get { return GetInternal("RoleID", ""); }
			set { SetInternal("RoleID", value); }
		}

		public string MetadataFolder
		{
			get { return GetInternal("MetadataFolder", ""); }
			set { SetInternal("MetadataFolder", value); }
		}

		public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiNetsuitePluginTitle;
            }
        }

		public string UseSandbox
		{
			get { return GetInternal("UseSandbox", "false"); }
			set { SetInternal("UseSandbox", value.ToString()); }
		}

		public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiNetsuiteWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(AccountID))
                {
                    return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

		public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();
				if (string.IsNullOrEmpty(ConnectionOptions))
				{
					connectionString.Append("Other='PromptMode=prompt;InitiateOAuth=GETANDREFRESH;OAuthSettingsLocation=%AppData%\\CData\\Netsuite Data Provider\\OAuthSettings.txt'");
				}

				if (!string.IsNullOrEmpty(AccountID))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Account ID=" + AccountID);
				}

				if (!string.IsNullOrEmpty(UseSandbox))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Use Sandbox=" + UseSandbox);
				}

				if (!string.IsNullOrEmpty(RoleID))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Role ID=" + RoleID);
				}

				if (!string.IsNullOrEmpty(MetadataFolder))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Netsuite Metadata Folder=" + MetadataFolder);
				}

				return connectionString.ToString();
			}
		}
	}
}
