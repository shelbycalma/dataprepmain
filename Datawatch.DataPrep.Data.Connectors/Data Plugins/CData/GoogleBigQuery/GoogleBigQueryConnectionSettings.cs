﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.GoogleBigQuery
{
    public class GoogleBigQueryConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

		public GoogleBigQueryConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public GoogleBigQueryConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for GoogleBigQuery";
            }
        }

		public string ProjectId
		{
			get { return GetInternal("ProjectId", ""); }
			set { SetInternal("ProjectId", value); }
		}

		public string DatasetId
		{
			get { return GetInternal("DatasetId", ""); }
			set { SetInternal("DatasetId", value); }
		}

		public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiGoogleBigQueryPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiGoogleBigQueryWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
				if (string.IsNullOrEmpty(ProjectId) || string.IsNullOrEmpty(DatasetId))
				{
					return false;
                }

                return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public override string IsReset
        {
            get { return GetInternal("IsReset", "false"); }
            set { SetInternal("IsReset", value.ToString()); }
        }

        public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();
				//if (string.IsNullOrEmpty(ConnectionOptions))
				//{
				//	connectionString.Append("Other='PromptMode=prompt;InitiateOAuth=GETANDREFRESH;OAuthSettingsLocation=%AppData%\\CData\\GoogleAdWords Data Provider\\OAuthSettings.txt'");
				//}

				if (!string.IsNullOrEmpty(ProjectId))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Project Id=" + ProjectId);
				}

				if (!string.IsNullOrEmpty(DatasetId))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Dataset Id = " + DatasetId);
				}

				return connectionString.ToString();
			}
		}
	}
}
