﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;
using Panopticon.CData.GoogleAdWords;


namespace Panopticon.CData.GoogleAdWords
{
    public class GoogleAdWordsConnectionSettings : CDataSettingsBase
    {
        private string GoogleAdWordsDirectory;
        private const string GoogleAdWordsKey = "GoogleAdWords";

        public GoogleAdWordsConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public GoogleAdWordsConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			GoogleAdWordsDirectory = Path.Combine(Path.GetTempPath(), "Datawatch Designer",
					GoogleAdWordsKey);

            if (!Directory.Exists(GoogleAdWordsDirectory))
            {
                Directory.CreateDirectory(GoogleAdWordsDirectory);
                Log.Info(Resources.LogSalesforceCreateFolder, GoogleAdWordsDirectory);
            }

            QuerySettings.IsOnDemand = false;
			
            //set default Schema
            Schema = "v201710";
        }

		public string DeveloperToken
		{
			get { return GetInternal("DeveloperToken", ""); }
			set { SetInternal("DeveloperToken", value); }
		}

		public string AccessToken
        {
            get { return GetInternal("AccessToken", ""); }
            set { SetInternal("AccessToken", value); }
        }

		public string RefreshToken
		{
			get { return GetInternal("RefreshToken", ""); }
			set { SetInternal("RefreshToken", value); }
		}

		public string ClientCustomerID
		{
			get { return GetInternal("ClientCustomerID", ""); }
			set { SetInternal("ClientCustomerID", value); }
		}

		public string Schema
		{
			get { return GetInternal("Schema", "v201710"); }
			set { SetInternal("Schema", value); }
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for GoogleAdWords";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiGoogleAdWordsPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiGoogleAdWordsWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(DeveloperToken) || string.IsNullOrEmpty(ClientCustomerID))
                {
                    return false;
                }

                return true;
            }
        }

        public override string IsReset
        {
            get { return GetInternal("IsReset", "false"); }
            set { SetInternal("IsReset", value.ToString()); }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

		public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();
				//if (string.IsNullOrEmpty(ConnectionOptions))
				//{
				//	connectionString.Append("Other='PromptMode=prompt;InitiateOAuth=GETANDREFRESH;OAuthSettingsLocation=%AppData%\\CData\\GoogleAdWords Data Provider\\OAuthSettings.txt'");
				//}

				if (!string.IsNullOrEmpty(DeveloperToken))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Developer Token=" + DeveloperToken);
				}

				if (!string.IsNullOrEmpty(ClientCustomerID))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Client Customer ID=" + ClientCustomerID);
				}

				return connectionString.ToString();
			}
		}
	}
}
