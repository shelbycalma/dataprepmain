﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.CData.Properties;

namespace Panopticon.CData.Access
{
    public class AccessConnectionSettings : CDataSettingsBase
    {
		private IEnumerable<ParameterValue> parameters;

		public AccessConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public AccessConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
			this.parameters = parameters;
		}

		public override string DatabaseName
		{
			get { return GetInternal("DatabaseName", ""); }
			set { SetInternal("DatabaseName", value); }
		}

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for Access";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiAccessPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiAccessWindowTitle;
            }
        }


        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool CanLoadTables
        {
            get
            {
				if (string.IsNullOrEmpty(DatabaseName))
				{
					return false;
				}

				return true;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

		public override string PluginConnectionString
		{
			get
			{
				StringBuilder connectionString = new StringBuilder();

				if (!string.IsNullOrEmpty(DatabaseName))
				{
					if (connectionString.Length > 0)
					{
						connectionString.Append(";");
					}
					connectionString.Append("Data Source=" + DatabaseName);
				}

				return connectionString.ToString();
			}
		}

	}
}
