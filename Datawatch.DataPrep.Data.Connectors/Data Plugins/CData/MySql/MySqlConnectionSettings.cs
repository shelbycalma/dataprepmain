﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using System.Text;

namespace Panopticon.CData.MySql
{
    [DataHelpKey("MySQL")]
    public class MySqlConnectionSettings : CDataSettingsBase
    {
        private IEnumerable<ParameterValue> parameters;

        public MySqlConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public MySqlConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.MySQL)
        {
            this.parameters = parameters;
            //set default Port number
            Port = "3306";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "Show databases";
            }
        }

        public override string SystemDatabaseName
        {
            get { return "mysql"; }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "3306");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public virtual bool IsLegacy
        {
            get { return false; }
        }

		public override bool IsCDataDriver => true;

		public override string PluginDriverName
        {
            get
            {
                return "Datawatch ODBC Driver for MySQL";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiMySqlPluginTitle;
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                StringBuilder connectionString = new StringBuilder();
                if (string.IsNullOrEmpty(ConnectionOptions))
                {
                    connectionString.Append("Timeout=1200");
                }
                else
                { 
                    if (!ConnectionOptions.Contains("Timeout"))
                    {
                        if (connectionString.Length > 0)
                        {
                            connectionString.Append(";");
                        }
                        connectionString.Append("Timeout=1200");
                    }
                }
                return connectionString.ToString();
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiMySQLWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override string[] SchemaRestrictions
        {
            get
            {
                return new string[] { DataUtils.ApplyParameters(
                DatabaseName, parameters, false) };
            }
        }
    }
}
