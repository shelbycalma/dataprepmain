﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.XmlFilePlugin.UI.Properties;

namespace Panopticon.XmlFilePlugin.UI
{
    public class PluginUI : TextPluginUIBase<XmlFileSettings, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) :
            base(plugin, owner, "pack://application:,,,/Panopticon.XmlFilePlugin.UI;component/connect-xmlfile.gif")
        {
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            XmlFileSettings settings =
                new XmlFileSettings(Plugin.PluginManager, bag, parameters);
            return new ConnectionPanel(settings, owner);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel conWindow = (ConnectionPanel)obj;
            return conWindow.ConnectionSettings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(XmlFileSettings settings)
        {
            return new ConnectionWindow(settings, Plugin.GlobalSettings);
        }

        private static readonly string[] fileExtensions = new[] { "xml" };

        public override string[] FileExtensions
        {
            get { return fileExtensions; }
        }

        public override string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Resources.UiXmlFiles, fileExtensions);
            }
        }
    }
}
