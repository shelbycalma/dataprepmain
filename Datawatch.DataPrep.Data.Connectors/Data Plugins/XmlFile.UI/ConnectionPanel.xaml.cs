﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.XmlFilePlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private static readonly string FileFilter = string.Concat(Properties.Resources.UiDialogXmlFilesFilter, "|*.xml");
        private XmlFileSettings settings;

        private Window owner;
        public PropertyBag GlobalSettings { get; set; }

        public ConnectionPanel()
            :this(null, null)
        {
        }

        public ConnectionPanel(XmlFileSettings settings, Window owner)
        {
            InitializeComponent();
            DataContext = ConnectionSettings = settings;
            this.owner = owner;
            DataContextChanged +=
                UserControl_DataContextChanged;
        }

        public XmlFileSettings ConnectionSettings
        {
            get { return settings; }
            private set
            {
                settings = value;
                if (settings != null)
                {
                    PasswordBox.Password = settings.WebPassword;
                }
                else
                {
                    PasswordBox.Password = null;
                }
            }
        }

        public ColumnType[] ColumnDataTypes
        {
            get
            {
                return new ColumnType[]{
                    ColumnType.Text,
                    ColumnType.Numeric,
                    ColumnType.Time
                };
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = DataPluginUtils.BrowseForFile(owner, 
                ConnectionSettings.FilePath, FileFilter, GlobalSettings);
            if (path != null)
            {
                ConnectionSettings.FilePath = path;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (ConnectionSettings != null)
            {
                ConnectionSettings.WebPassword = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            ConnectionSettings = DataContext as XmlFileSettings;
        }
    }
}
