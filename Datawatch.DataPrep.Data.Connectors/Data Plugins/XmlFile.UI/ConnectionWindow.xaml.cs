﻿using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.XmlFilePlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private XmlFileSettings settings;
        public static RoutedCommand OkCommand
           = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(XmlFileSettings con, PropertyBag globalSettings)
        {
            this.settings = con;
            InitializeComponent();
            ConnectionPanel.GlobalSettings = globalSettings;
        }

        public XmlFileSettings ConnectionSettings
        {
            get
            {
                return settings;
            }

        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOK;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
