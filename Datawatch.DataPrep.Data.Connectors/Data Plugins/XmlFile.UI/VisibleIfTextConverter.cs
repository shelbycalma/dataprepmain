﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Framework.Model;
using Exceptions = Datawatch.DataPrep.Data.Core.Exceptions;

namespace Panopticon.XmlFilePlugin.UI
{
    public class VisibleIfTextConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value is ColumnType &&
                ((ColumnType)value) == ColumnType.Text)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }

    }
}
