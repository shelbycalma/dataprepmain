﻿using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace Panopticon.LivySparkPlugin.UI
{
    public class SettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private ICommand testConnectionCommand;
        private ICommand browseFileCommand;
        private LivySparkSettings settings;
        private IEnumerable<ParameterValue> parameters;

        public SettingsViewModel(LivySparkSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            this.settings = settings;
            this.parameters = parameters;
        }
        public LivySparkSettings Settings
        {
            set { settings = value; }
            get { return settings; }
        }

        public Window OwnerWindow { get; set; }

        public ICommand TestConnectionCommand
        {
            get
            {
                return testConnectionCommand ??
                   (testConnectionCommand = 
                    new DelegateCommand(TestConnection, CanTestConnection));
            }
        }

        public ICommand BrowseFileCommand
        {
            get
            {
                return browseFileCommand ??
                   (browseFileCommand =
                    new DelegateCommand(BrowseScriptFile, CanBrowseScriptFile));
            }
        }

        private void TestConnection()
        {
            try
            {
                LivyClient client = new LivyClient(settings, parameters);
                client.TestConnection();
                MessageBox.Show(Properties.Resources.UiTestConnectionSucceed,
                                Properties.Resources.UiConfigWindowTitle,
                                MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                                Properties.Resources.UiConfigWindowTitle,
                                MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public bool CanTestConnection()
        {
            return (!string.IsNullOrEmpty(Settings.Host) ||
                !string.IsNullOrEmpty(Settings.Kind));
        }

        private void BrowseScriptFile()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            bool? result = dialog.ShowDialog(OwnerWindow);
            if (!result.Value) return;

            string fileName = dialog.FileName;
            if (File.Exists(fileName))
            {
                Settings.Script = File.ReadAllText(fileName);
            }
        }

        private bool CanBrowseScriptFile()
        {
            return true;
        }
    }
}
