﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.LivySparkPlugin.UI
{
    public partial class ConfigWindow : Window
    {
        private readonly SettingsViewModel viewModel;

        public static RoutedCommand OkCommand
           = new RoutedCommand("Ok", typeof(ConfigWindow));

        public ConfigWindow() : this(null)
        {
            
        }

        public ConfigWindow(SettingsViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.viewModel.OwnerWindow = this;
            InitializeComponent();
        }

        public SettingsViewModel ViewModel
        {
            get
            {
                return viewModel;
            }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ConfigPanel.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
