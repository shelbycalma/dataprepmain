﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;

namespace Panopticon.LivySparkPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, LivySparkSettings>
    {
        public PluginUI(Plugin plugin, Window owner) 
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            LivySparkSettings settings = Plugin.CreateSettings(new PropertyBag());
            SettingsViewModel viewModel = new SettingsViewModel(settings, parameters);
            ConfigWindow window = new ConfigWindow(viewModel);
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return window.ConfigPanel.ViewModel.Settings.ToPropertyBag();
            }
            return null;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            LivySparkSettings settings =
                new LivySparkSettings(bag, parameters);
            SettingsViewModel viewModel = 
                new SettingsViewModel(settings, parameters);
            return new ConfigPanel(viewModel);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            return configPanel.ViewModel.Settings.ToPropertyBag();
        }
    }
}
