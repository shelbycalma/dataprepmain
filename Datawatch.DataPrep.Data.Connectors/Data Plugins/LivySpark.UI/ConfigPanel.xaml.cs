﻿using Datawatch.DataPrep.Data.Framework.Model;
using Microsoft.Win32;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace Panopticon.LivySparkPlugin.UI
{

    internal partial class ConfigPanel : UserControl
    {
        

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                                        typeof(SettingsViewModel),
                                        typeof(ConfigPanel),
                                        new PropertyMetadata(Settings_Changed));
        public ConfigPanel() : this(null)
        {
        }

        public ConfigPanel(SettingsViewModel viewModel)
        {
            InitializeComponent();

            DataContext = ViewModel = viewModel;
            DataContextChanged +=
                UserControl_DataContextChanged;
            if (viewModel != null)
            {
                PasswordBox.Password = viewModel.Settings.Password;
            }
        }
        public SettingsViewModel ViewModel
        {
            get { return (SettingsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (SettingsViewModel)args.OldValue,
                (SettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(SettingsViewModel oldViewModel,
            SettingsViewModel newViewModel)
        {
            DataContext = newViewModel;

            PasswordBox.Password = newViewModel != null ? newViewModel.Settings.Password : null;
        }

        public bool IsOk
        {
            get
            {
                if (ViewModel.Settings == null) return false;

                return ViewModel.CanTestConnection() &&
                    !string.IsNullOrEmpty(ViewModel.Settings.Script);
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            ViewModel = DataContext as SettingsViewModel;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (ViewModel != null && ViewModel.Settings != null)
            {
                ViewModel.Settings.Password = PasswordBox.Password;
            }
        }
    }

}
