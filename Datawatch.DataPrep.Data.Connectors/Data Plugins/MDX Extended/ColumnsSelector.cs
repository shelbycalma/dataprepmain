﻿using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;

namespace Panopticon.MDXExtendedPlugin
{
    using System.Windows;
    using System.Windows.Controls;

    public class ColumnsSelector : DataTemplateSelector
    {
        public DataTemplate ColumnsMeasuresDataTemplate { get; set; }
        public DataTemplate ColumnsDimensionsDataTemplate { get; set; }
        public DataTemplate ColumnsLevelsDataTemplate { get; set; }
        public DataTemplate ColumnsHierarchiesDataTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var asMeasure = item as Measure;
            if (asMeasure != null)
            {
                return this.ColumnsMeasuresDataTemplate;
            }

            var asDimension = item as Dimension;
            if (asDimension != null)
            {
                return this.ColumnsDimensionsDataTemplate;
            }

            var asHierarchy = item as Hierarchy;
            if (asHierarchy != null)
            {
                return this.ColumnsHierarchiesDataTemplate;
            }

            var asLevel = item as Level;
            if (asLevel != null)
            {
                return this.ColumnsLevelsDataTemplate;
            }
            return null;
        }
    }
}
