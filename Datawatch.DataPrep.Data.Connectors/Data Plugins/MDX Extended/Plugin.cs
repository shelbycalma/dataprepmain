﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.MDXExtendedPlugin.ClientUtils;
using Panopticon.MDXExtendedPlugin.MetadataProviders;
using Panopticon.MDXExtendedPlugin.MetadataProviders.AdomdMetadataProvider;
using Panopticon.MDXExtendedPlugin.Properties;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.MDXExtendedPlugin
{
    [LicenseProvider(typeof (DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<MDXSettings>, IAggregatingOnDemandPlugin
    {
        public const string partitionColumnName = "External Aggregate";
        public const string partitionColumnValue = "";

        internal const string PluginId = "MDX Extended";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "MDX Extended";
        internal const string PluginType = DataPluginTypes.Database;

        public Plugin()
        {
            // prevent plugin load if we dont have AdomdClient library
            try
            {
                Log.Info(Resources.LogLibraryLoaded,
                    typeof (AdomdCommand).Assembly.FullName);
            }
            catch (Exception ex)
            {
                throw Exceptions.PluginDependenciesAreNotFound(PluginId, ex);
            }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public static IMDXClientUtil GetClientUtil(MDXSettings settings)
        {

            //todo: add factory class to create instances
            // PROVIDER IMPLEMENTATION : Add construction of specific provider
            AdomdMdxClientUtil adomd;
            IMetadataProvider md;
            switch (settings.Provider)
            {
                case ProviderNames.Adomd:
                    adomd = new AdomdMdxClientUtil(settings);
                    md = new AdomdMetadataProvider(settings);
                    adomd.SetMetaDataProvider(md);
                    return adomd;

                case ProviderNames.SapAdo:
                    adomd = new AdomdMdxClientUtil(settings);
                    md = new SapAdoMetadataProvider(settings);
                    adomd.SetMetaDataProvider(md);
                    return adomd;

                case ProviderNames.Generic:
                    md = new GenericAdoMetadataProvider(settings);
                    adomd = new AdomdMdxClientUtil(settings);
                    adomd.SetMetaDataProvider(md);
                    return adomd;

                case ProviderNames.SampleProvider:
                    return new SampleMdxClientUtil(settings);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public override MDXSettings CreateSettings(PropertyBag properties)
        {
            MDXSettings mdxSettings = new MDXSettings(properties);
            return mdxSettings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            MDXSettings mdxSettings = CreateSettings(properties);

            IMDXClientUtil mdxClientUtil = GetClientUtil(mdxSettings);

            Log.Info("*********Ext Agg: "
                + this.IncludesAggregateData(properties)
                + " partition: " + this.GetPartitionColumnName(properties)
                + " value: " + this.GetPartitionValue(properties) + "*****");
            return mdxClientUtil.Execute(mdxSettings, parameters, maxRowCount);
        }

        public ITable GetOnDemandData(string workbookDir, string dataDir,
            PropertyBag settings, IEnumerable<ParameterValue> parameters,
            OnDemandParameters onDemandParameters, int rowcount)
        {
            CheckLicense();

            MDXSettings mdxSettings = CreateSettings(settings);

            IMDXClientUtil mdxClientUtil = GetClientUtil(mdxSettings);

            return mdxClientUtil.ExecuteOnDemandQuery(mdxSettings, parameters, rowcount);
        }

        public string GetPartitionColumnName(PropertyBag settings)
        {
            return partitionColumnName;
        }

        public string GetPartitionValue(PropertyBag settings)
        {
            return partitionColumnValue;
        }

        public bool IncludesAggregateData(PropertyBag settings)
        {
            if (settings != null)
            {
                var mdxSettings = new MDXSettings(settings);
                return mdxSettings.ExternalAggregates;
            }
            return false;
        }

        public bool IsOnDemand(PropertyBag settings)
        {
            if (settings != null)
            {
                var mdxSettings = new MDXSettings(settings);
                mdxSettings.QueryOnDemand = mdxSettings.QueryOnDemandColumns || mdxSettings.QueryOnDemandRows;
                return mdxSettings.QueryOnDemand;
            }
            return false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
    }
}