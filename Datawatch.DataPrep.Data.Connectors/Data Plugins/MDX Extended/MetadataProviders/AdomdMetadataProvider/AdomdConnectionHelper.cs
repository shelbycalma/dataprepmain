﻿using System.Text;
using Microsoft.AnalysisServices.AdomdClient;

namespace Panopticon.MDXExtendedPlugin.MetadataProviders.AdomdMetadataProvider
{
    public class AdomdConnectionHelper
    {
        public static AdomdConnection GetConnection(string connectionString)
        {
            var adomdConnection = new AdomdConnection(connectionString);
            adomdConnection.Open();
            return adomdConnection;
        }

        public static AdomdConnection GetConnection(MDXSettings mdxSettings)
        {
            string connectionString = BuildConnectionString(mdxSettings);
            if (!string.IsNullOrEmpty(mdxSettings.Catalog))
            {
                connectionString = connectionString + ";Initial Catalog=" + mdxSettings.Catalog;
            }
            return GetConnection(connectionString);
        }
        
        internal static string BuildConnectionString(MDXSettings settings)
        {
            if (settings == null || string.IsNullOrEmpty(settings.ConnectionProperties["ServerName"].Value))
            {
                return string.Empty;
            }

            StringBuilder connectionString = new StringBuilder(@"Provider=MSOLAP");

            connectionString.Append("; Data Source=" + settings.ConnectionProperties["ServerName"].Value);

            if (settings.ConnectionProperties["AuthType"].Value != "Windows")
            {
                if (!string.IsNullOrEmpty(settings.ConnectionProperties["UserName"].Value))
                {
                    connectionString.Append(";User ID=" + settings.ConnectionProperties["UserName"].Value);
                }
                if (!string.IsNullOrEmpty(settings.ConnectionProperties["Password"].Value))
                {
                    connectionString.Append(";Password=" + settings.ConnectionProperties["Password"].Value);
                }
            }
            return connectionString.ToString();
        }
    }
}