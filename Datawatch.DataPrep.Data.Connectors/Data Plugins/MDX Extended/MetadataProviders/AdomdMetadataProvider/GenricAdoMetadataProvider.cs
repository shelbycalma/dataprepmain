﻿using System;
using System.Data;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.MDXExtendedPlugin.Helpers;
using Microsoft.AnalysisServices.AdomdClient;

namespace Panopticon.MDXExtendedPlugin.MetadataProviders.AdomdMetadataProvider
{
    class GenericAdoMetadataProvider : AdomdMetadataProvider, IMetadataProvider
    {

        public GenericAdoMetadataProvider(MDXSettings mdxSettings)
            : base(mdxSettings, false) // call base constructor without connection
        {
            String connectionString = BuildConnectString(mdxSettings);
            base.Connection = AdomdConnectionHelper.GetConnection(connectionString);
        }

        private String BuildConnectString(MDXSettings settings)
        {
            String ServerName = settings.ConnectionProperties["ServerName"].Value;
            if (settings == null || string.IsNullOrEmpty(ServerName))
            {
                return string.Empty;
            }

            StringBuilder connectionString = new StringBuilder(@"Provider=MSOLAP");

            connectionString.Append("; Data Source=");

            connectionString.Append(settings.ConnectionProperties["ServerName"].Value);
            

            if (settings.ConnectionProperties["AuthType"].Value != "Windows")
            {
               
                if (!string.IsNullOrEmpty(settings.ConnectionProperties["UserName"].Value))
                {
                    connectionString.Append(";User ID=" + settings.ConnectionProperties["UserName"].Value);
                }
                if (!string.IsNullOrEmpty(settings.ConnectionProperties["Password"].Value))
                {
                    connectionString.Append(";Password=" + settings.ConnectionProperties["Password"].Value);
                }
            }
            
            SecLogger.LogConString("GenericAdoMetadataProvider:", connectionString.ToString());
            return connectionString.ToString();
        }

        

        public new void SetCatalogName(string catalogName) 
        {
           
            if (!string.IsNullOrEmpty(catalogName) && Connection.State == ConnectionState.Open)
            {
                if (settings.ConnectionProperties[GenericMdxConnectionProperties.Reconnect].BoolValue() == false)
                {
                    try
                    {
                        if (!string.IsNullOrEmpty(catalogName)
                            && Connection.Database != catalogName)
                        {
                            Connection.ChangeDatabase(catalogName);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error("Database reconnect is not supported. " 
                            + e.GetBaseException().Message);
                        throw new Exception(Properties.Resources.ExChangeDBNotSupported);
                    }
                    return;
                }
                else
                {
                    Connection.Close();
                }
            }

            
            string connectionString = this.BuildConnectString(settings);
            Connection = AdomdConnectionHelper
                .GetConnection(connectionString + ";Initial Catalog=" + catalogName);           
        }

        public new Cube GetCubeMetaData(string cubeName)
        {
            Cube cubeMetadataInfo;

            GetCubeMetadata_MdSchema(cubeName, out cubeMetadataInfo); 
            settings.RowDimensionColumnsBackup = MakeCubeMetadataBackupString(cubeMetadataInfo);
            return cubeMetadataInfo;
        }

        public CubeDef FindCubeByName(string cubeName)
        {
            throw new NotImplementedException();
        }
    }
}
