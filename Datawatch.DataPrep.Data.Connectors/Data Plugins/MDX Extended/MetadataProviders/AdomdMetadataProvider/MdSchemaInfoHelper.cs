﻿using System;
using System.Data;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Framework;
using Microsoft.AnalysisServices.AdomdClient;
using CubeType = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.CubeType;
using Dimension = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension;
using Hierarchy = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy;
using Level = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level;
using Measure = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Measure;

namespace Panopticon.MDXExtendedPlugin.MetadataProviders.AdomdMetadataProvider
{
    public class MdSchemaInfoHelper
    {
        AdomdConnection Connection { get; set; }
        AdomdRestrictionCollection Restrictions { get; set; }

        public MdSchemaInfoHelper(
            AdomdConnection connection, AdomdRestrictionCollection restrictions)
        {
            Connection = connection;
            Restrictions = restrictions;
        }
        public void PopulateCubeSchemaInfo(ref Cube cubeMetadataInfo)
        {
            try
            {
                DataSet resultSet = Connection.GetSchemaDataSet("MDSCHEMA_CUBES", Restrictions);
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {
                    foreach (DataColumn column in resultSet.Tables[0].Columns)
                    {
                        String colName = column.ColumnName;
                        if (colName.Equals("Name", StringComparison.CurrentCultureIgnoreCase) || colName.Equals("CUBE_NAME", StringComparison.CurrentCultureIgnoreCase))
                            cubeMetadataInfo.Name = row[column].ToString();
                        else if (colName.Equals("description", StringComparison.CurrentCultureIgnoreCase) || colName.Equals("CUBE_DESCRIPTION", StringComparison.CurrentCultureIgnoreCase))
                            cubeMetadataInfo.Description = row[column].ToString();
                        else if (colName.Equals("caption", StringComparison.CurrentCultureIgnoreCase) || colName.Equals("CUBE_CAPTION", StringComparison.CurrentCultureIgnoreCase))
                            cubeMetadataInfo.Caption = row[column].ToString();
                        else if (colName.Equals("Type", StringComparison.CurrentCultureIgnoreCase) || colName.Equals("CUBE_TYPE", StringComparison.CurrentCultureIgnoreCase))
                        {
                            if (row[column].ToString().Equals("CUBE", StringComparison.CurrentCultureIgnoreCase))
                            {
                                cubeMetadataInfo.Type = CubeType.Cube;
                            }
                            else if (row[column].ToString().Equals("DIMENSION", StringComparison.CurrentCultureIgnoreCase))
                            {
                                cubeMetadataInfo.Type = CubeType.Dimension;
                            }
                            else
                            {
                                cubeMetadataInfo.Type = CubeType.Unknown;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }
        }

        public void PopulateDimensionSchemaInfo(ref Cube cubeMetadataInfo)
        {
            try
            {
                var resultSet = Connection.GetSchemaDataSet("MDSCHEMA_DIMENSIONS", Restrictions);
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {
                    Dimension dim = new Dimension();
                    dim.Name = row["DIMENSION_NAME"].ToString();
                    dim.UniqueName = row["DIMENSION_UNIQUE_NAME"].ToString();
                    dim.Caption = row["DIMENSION_CAPTION"].ToString();
                    dim.DimensionType = (DimensionType)Convert.ToInt32(row["DIMENSION_TYPE"].ToString());
                    if (dim.DimensionType == DimensionType.Measure)
                    {
                    }
                    else
                    {
                        cubeMetadataInfo.Dimensions.Add(dim);
                    }
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }
        }

        public void PopulateHierarchySchemaInfo(ref Cube cubeMetadataInfo)
        {
            try
            {
                var resultSet = Connection.GetSchemaDataSet("MDSCHEMA_HIERARCHIES", Restrictions);
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {
                    string dimUniqueName = row["DIMENSION_UNIQUE_NAME"].ToString();
                    int dIndex = -1;
                    for (int d = 0; d < cubeMetadataInfo.Dimensions.Count; d++)
                    {
                        if (cubeMetadataInfo.Dimensions[d].UniqueName.Equals(dimUniqueName))
                        {
                            dIndex = d;
                            break;
                        }
                    }
                    if (dIndex < 0)
                    {
                        continue;
                    }

                    //                if (dim.DimensionType != DimensionType.Measure) 
                    Hierarchy hier = new Hierarchy();
                    hier.Name = row["HIERARCHY_NAME"].ToString();
                    hier.Caption = row["HIERARCHY_CAPTION"].ToString();
                    hier.UniqueName = row["HIERARCHY_UNIQUE_NAME"].ToString();
                    cubeMetadataInfo.Dimensions[dIndex].HierarchyInfos.Add(hier);
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }

        }

        public void PopulateLevelSchemaInfo(ref Cube cubeMetadataInfo)
        {
            try
            {
                var resultSet = Connection.GetSchemaDataSet("MDSCHEMA_LEVELS", Restrictions);
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {
                    string dimUniqueName = row["DIMENSION_UNIQUE_NAME"].ToString();
                    int dIndex = -1;
                    for (int d = 0; d < cubeMetadataInfo.Dimensions.Count; d++)
                    {
                        if (cubeMetadataInfo.Dimensions[d].UniqueName.Equals(dimUniqueName))
                        {
                            dIndex = d;
                            break;
                        }
                    }
                    if (dIndex < 0)
                    {
                        continue;
                    }

                    string hierUniqueName = row["HIERARCHY_UNIQUE_NAME"].ToString();
                    int hIndex = -1;
                    for (int hierarchyInfo = 0; hierarchyInfo < cubeMetadataInfo.Dimensions[dIndex].HierarchyInfos.Count; hierarchyInfo++)
                    {
                        if (cubeMetadataInfo.Dimensions[dIndex].HierarchyInfos[hierarchyInfo].UniqueName.Equals(hierUniqueName))
                        {
                            hIndex = hierarchyInfo;
                            break;
                        }
                    }
                    if (hIndex < 0)
                    {
                        continue;
                    }

                    //                if (dim.DimensionType != DimensionType.Measure) 
                    Level lev = new Level();
                    lev.Name = row["LEVEL_NAME"].ToString();
                    lev.Caption = row["LEVEL_CAPTION"].ToString();
                    lev.UniqueName = row["LEVEL_UNIQUE_NAME"].ToString();
                    lev.LevelNumber = Convert.ToInt32(row["LEVEL_NUMBER"].ToString());
                    lev.LevelType = (LevelType)Convert.ToInt32(row["LEVEL_TYPE"].ToString());
                    //                lev.MemberCount = Convert.ToInt32(row["LEVEL_MEMBER_COUNT"].ToString());
                    cubeMetadataInfo.Dimensions[dIndex].HierarchyInfos[hIndex].LevelInfos.Add(lev);
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }

        }

        public void PopulateMeasuresSchemaInfo(ref Cube cubeMetadataInfo)
        {
            try
            {
                var resultSet = Connection.GetSchemaDataSet("MDSCHEMA_MEASURES", Restrictions);
                foreach (DataRow row in resultSet.Tables[0].Rows)
                {
                    Measure measure = new Measure();
                    measure.Name = row["MEASURE_NAME"].ToString();
                    measure.UniqueName = row["MEASURE_UNIQUE_NAME"].ToString();
                    measure.Caption = row["MEASURE_CAPTION"].ToString();
                    cubeMetadataInfo.Measures.Add(measure);
                }
            }
            catch (Exception Ex)
            {
                Log.Exception(Ex);
            }
        }
    }
}
