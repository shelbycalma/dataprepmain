﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Panopticon.MDXExtendedPlugin.Helpers;

namespace Panopticon.MDXExtendedPlugin.MetadataProviders.AdomdMetadataProvider
{

    /* todo: inherit from basic provider rather than from the MSSQL specific one. 
       or make AdomdMetadataProvider more general*/

    class SapAdoMetadataProvider : AdomdMetadataProvider
    {
        private String BuildConnectString(MDXSettings settings)
        {
            if (settings == null || string.IsNullOrEmpty(settings.ConnectionProperties["ServerName"].Value))
            {
                return string.Empty;
            }

            StringBuilder connectionString = new StringBuilder(@"Provider=MSOLAP");

            if (settings.ConnectionProperties["AuthType"].Value == "Windows")
            {
                connectionString.Append("; Data Source=" + settings.ConnectionProperties["ServerName"].Value);
            }
            else
            {
                connectionString.Append("; Data Source=" + settings.ConnectionProperties["ServerName"].Value);
            }


            if (settings.ConnectionProperties["AuthType"].Value != "Windows")
            {
               
                if (!string.IsNullOrEmpty(settings.ConnectionProperties["UserName"].Value))
                {
                    connectionString.Append(";User ID=" + settings.ConnectionProperties["UserName"].Value);
                }
                if (!string.IsNullOrEmpty(settings.ConnectionProperties["Password"].Value))
                {
                    connectionString.Append(";Password=" + settings.ConnectionProperties["Password"].Value);
                }
            }

            SecLogger.LogConString("SapAdoMetadataProvider", connectionString.ToString());
            return connectionString.ToString();
        }

        public SapAdoMetadataProvider(MDXSettings mdxSettings) 
            :base(mdxSettings, false) // call base constructor without connection
        {
            String connectionString = BuildConnectString(mdxSettings);
            base.Connection = AdomdConnectionHelper.GetConnection(connectionString);
        }

        public new void SetCatalogName(string catalogName) 
        {
            // reconnect on set catalog name. Connection.Database is not supported
            // with SAP
            if (!string.IsNullOrEmpty(catalogName) && Connection.State == ConnectionState.Open)
            {
                Connection.Close();
            }

            
            string connectionString = this.BuildConnectString(settings);
            Connection = AdomdConnectionHelper
                .GetConnection(connectionString + ";Initial Catalog=" + catalogName);           
        }

        public new Cube GetCubeMetaData(string cubeName)
        {
            Cube cubeMetadataInfo;
            if (!GetCubeMetadata_AdomdInfoHelper(cubeName, out cubeMetadataInfo)) 
                return null;

            settings.RowDimensionColumnsBackup = MakeCubeMetadataBackupString(cubeMetadataInfo);
            return cubeMetadataInfo;
        }

        
    }
}
