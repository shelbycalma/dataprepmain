﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.MDXPlugin.MetadataProviders.AdomdMetadataProvider;
using CubeType = Microsoft.AnalysisServices.AdomdClient.CubeType;
using Dimension = Microsoft.AnalysisServices.AdomdClient.Dimension;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;
using Level = Microsoft.AnalysisServices.AdomdClient.Level;
using Measure = Microsoft.AnalysisServices.AdomdClient.Measure;

namespace Panopticon.MDXExtendedPlugin.MetadataProviders.AdomdMetadataProvider
{
    public class AdomdMetadataProvider : IMetadataProvider
    {

        protected MDXSettings settings;
        private AdomdConnection connection;
        public IDbConnection Connection
        {
            get { return connection; }
            set { connection = (AdomdConnection)value; }
        }

        public AdomdConnection AdomdConnection
        {
            get { return connection; }
            private set { connection = value; }
        }


        public AdomdMetadataProvider(MDXSettings mdxSettings, bool createConnection = true)
        {
            settings = mdxSettings;
            if (createConnection)
            {
                Connection = AdomdConnectionHelper
                    .GetConnection(mdxSettings);
            }
            else
            {
                Connection = null;
            }
        }

        // todo: redo this. this method of storing the hierarchies is seems quite dangerous. If we must store the hierarchy, we definitely need to move to something like hierarchical property bag storage
        protected string MakeCubeMetadataBackupString(Cube cubeMetadataInfo)
        {
            string hierarchyBackup = "";
            foreach (Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension dimension in cubeMetadataInfo.Dimensions)
            {
                foreach (Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy dimensionHierarchyInfo in dimension.HierarchyInfos)
                {
                    hierarchyBackup = hierarchyBackup + dimensionHierarchyInfo.UniqueName + "~" +
                                      dimensionHierarchyInfo.LevelInfos.Count + "~";
                    foreach (var levelInfo in dimensionHierarchyInfo.LevelInfos)
                    {
                        hierarchyBackup = hierarchyBackup + levelInfo.Name + "|";
                    }
                    hierarchyBackup = hierarchyBackup + "@";
                }
            }
            return hierarchyBackup;
        }

        protected bool GetCubeMetadata_AdomdInfoHelper(string cubeName, out Cube cubeMetadataInfo)
        {
            try
            {
                var cube = FindCubeByName(cubeName);
                if (cube != null)
                {
                    cubeMetadataInfo = AdomdInfoHelper.CreateCubeMetadataInfo(cube);

                    foreach (Dimension dimension in cube.Dimensions)
                    {
                        if (dimension.DimensionType == DimensionTypeEnum.Measure)
                            continue;

                        var dimensionMetadataInfo = AdomdInfoHelper.CreateDimensionMetadataInfo(dimension);
                        cubeMetadataInfo.Dimensions.Add(dimensionMetadataInfo);

                        foreach (Hierarchy hierarchy in dimension.Hierarchies)
                        {
                            Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy hierarchyMetadata = AdomdInfoHelper.CreateHierarchyMetadataInfo(hierarchy);
                            dimensionMetadataInfo.HierarchyInfos.Add(hierarchyMetadata);

                            foreach (Level level in hierarchy.Levels)
                            {
                                Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level levelMetadata = AdomdInfoHelper.CreateLevelMetadataInfo(level);
                                hierarchyMetadata.LevelInfos.Add(levelMetadata);
                            }
                        }
                    }

                    var properties = cube.Measures
                                                  .Cast<Measure>()
                                                  .Select(AdomdInfoHelper.CreateMeasureMetadataInfo);
                    foreach (var property in properties)
                    {
                        cubeMetadataInfo.Measures.Add(property);
                    }
                    return true;
                }
                cubeMetadataInfo = null;
                return false;
            }
            catch (Exception ex)
            {
                cubeMetadataInfo = null;
                return false;
            }
        }

        protected void GetCubeMetadata_MdSchema(
            string cubeName, out Cube cubeMetadataInfo)
        {
            AdomdRestrictionCollection restrictions = new AdomdRestrictionCollection();
            restrictions.Add("CUBE_NAME", cubeName);


            int MAX_MDDIMENSION_LENGTH = 30;

            MdSchemaInfoHelper schemaInfoHelper = new MdSchemaInfoHelper(connection, restrictions);

            List<string> details = new List<string>();
            cubeMetadataInfo = new Cube();
            
            schemaInfoHelper.PopulateCubeSchemaInfo(ref cubeMetadataInfo);

            schemaInfoHelper.PopulateDimensionSchemaInfo(ref cubeMetadataInfo);

            schemaInfoHelper.PopulateHierarchySchemaInfo(ref cubeMetadataInfo);

            schemaInfoHelper.PopulateLevelSchemaInfo(ref cubeMetadataInfo);

            schemaInfoHelper.PopulateMeasuresSchemaInfo(ref cubeMetadataInfo);
        }

        public Cube GetCubeMetaData(string cubeName)
        {
            Cube cubeMetadataInfo;
            bool gotCubeMetadata = GetCubeMetadata_AdomdInfoHelper(cubeName, out cubeMetadataInfo);
            if (!gotCubeMetadata)
            {
                GetCubeMetadata_MdSchema(cubeName, out cubeMetadataInfo);
            }

            settings.RowDimensionColumnsBackup = MakeCubeMetadataBackupString(cubeMetadataInfo);
            return cubeMetadataInfo;
        }

        public IList<MeasureGroup> GetMeasureGroups(string cubeName)
        {
            var list = new Dictionary<String, MeasureGroup>();
            var restrictions = new AdomdRestrictionCollection {{"CATALOG_NAME", Connection.Database}};
            if (!String.IsNullOrEmpty(cubeName))
            {
                restrictions.Add("CUBE_NAME", AdomdInfoHelper.ConvertToNormalStyle(cubeName));
            }

            DataSet ds = AdomdConnection.GetSchemaDataSet("MDSCHEMA_MEASUREGROUPS", restrictions);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable table = ds.Tables[0];

                if (ds.Tables[0].Columns.Count > 0)
                {
                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                    {
                        var measureGroupMetadataInfo = new MeasureGroup();

                        if (table.Columns.Contains("CATALOG_NAME"))
                        {
                            if (dataRow["CATALOG_NAME"] != null)
                            {
                                measureGroupMetadataInfo.CatalogName = dataRow["CATALOG_NAME"].ToString();
                            }
                        }

                        if (table.Columns.Contains("CUBE_NAME"))
                        {
                            if (dataRow["CUBE_NAME"] != null)
                            {
                                measureGroupMetadataInfo.CubeName = dataRow["CUBE_NAME"].ToString();
                            }
                        }

                        if (table.Columns.Contains("MEASUREGROUP_NAME"))
                        {
                            if (dataRow["MEASUREGROUP_NAME"] != null)
                            {
                                measureGroupMetadataInfo.Name = dataRow["MEASUREGROUP_NAME"].ToString();
                            }
                        }

                        if (table.Columns.Contains("DESCRIPTION"))
                        {
                            if (dataRow["DESCRIPTION"] != null)
                            {
                                measureGroupMetadataInfo.Description = dataRow["DESCRIPTION"].ToString();
                            }
                        }

                        if (table.Columns.Contains("MEASUREGROUP_CAPTION"))
                        {
                            if (dataRow["MEASUREGROUP_CAPTION"] != null)
                            {
                                measureGroupMetadataInfo.Caption = dataRow["MEASUREGROUP_CAPTION"].ToString();
                            }
                        }

                        if (!list.ContainsKey(measureGroupMetadataInfo.Name))
                        {
                            list.Add(measureGroupMetadataInfo.Name, measureGroupMetadataInfo);
                        }
                    }
                }
            }

            ds = AdomdConnection.GetSchemaDataSet("MDSCHEMA_MEASURES", restrictions);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable table = ds.Tables[0];

                if (ds.Tables[0].Columns.Count > 0)
                {
                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                    {
                        String measuresGroupName = string.Empty;
                        if (table.Columns.Contains("MEASUREGROUP_NAME"))
                        {
                            if (dataRow["MEASUREGROUP_NAME"] != null)
                            {
                                measuresGroupName = dataRow["MEASUREGROUP_NAME"].ToString();
                            }
                        }

                        String measureUniqueName = String.Empty;
                        if (table.Columns.Contains("MEASURE_UNIQUE_NAME"))
                        {
                            if (dataRow["MEASURE_UNIQUE_NAME"] != null)
                            {
                                measureUniqueName = dataRow["MEASURE_UNIQUE_NAME"].ToString();
                            }
                        }

                        if (!String.IsNullOrEmpty(measuresGroupName) &&
                            !String.IsNullOrEmpty(measureUniqueName))
                        {
                            if (list.ContainsKey(measuresGroupName))
                            {
                                if (!list[measuresGroupName].Measures.Contains(measureUniqueName))
                                    list[measuresGroupName].Measures.Add(measureUniqueName);
                            }
                        }
                    }
                }
            }

            foreach (MeasureGroup measureGroupMetadataInfo in list.Values)
            {
                var restrictionCollection = new AdomdRestrictionCollection
                    {
                        {"CATALOG_NAME", Connection.Database},
                        {"CUBE_NAME", AdomdInfoHelper.ConvertToNormalStyle(cubeName)},
                        {"MEASUREGROUP_NAME", measureGroupMetadataInfo.Name}
                    };
                ds = AdomdConnection.GetSchemaDataSet("MDSCHEMA_MEASUREGROUP_DIMENSIONS", restrictionCollection);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable table = ds.Tables[0];

                    if (ds.Tables[0].Columns.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            if (table.Columns.Contains("DIMENSION_UNIQUE_NAME"))
                            {
                                if (row["DIMENSION_UNIQUE_NAME"] != null)
                                {
                                    String dimensionUniqueName = row["DIMENSION_UNIQUE_NAME"].ToString();
                                    if (!String.IsNullOrEmpty(dimensionUniqueName))
                                    {
                                        if (!measureGroupMetadataInfo.Dimensions.Contains(dimensionUniqueName))
                                            measureGroupMetadataInfo.Dimensions.Add(dimensionUniqueName);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return list.Values.ToList();
        }

        public CubeDef FindCubeByName(string cubeName)
        {
            if (string.IsNullOrEmpty(cubeName))
            {
                return null;
            }
            return
                AdomdConnection.Cubes.Cast<CubeDef>()
                           .FirstOrDefault(cubeDef => cubeDef.Name.ToLower() == cubeName.ToLower());
        }

        public IList<string> GetAllCubes()
        {
            AdomdRestrictionCollection restrictions = new AdomdRestrictionCollection();
            DataSet resultSet = AdomdConnection.GetSchemaDataSet("MDSCHEMA_CUBES", restrictions);
            return GetResultSetNames(resultSet, "CUBE_NAME"); 
            //return
            //    Connection.Cubes.Cast<CubeDef>()
            //        .Where(cubeDef => cubeDef.Type == CubeType.Cube)
            //        .Select(cubeDef => cubeDef.Name)
            //        .ToList();
        }
        private List<string> GetResultSetNames(DataSet resultSet, String nameColumn)
        {
            List<string> names = new List<string>();

            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                names.Add(row[nameColumn].ToString());
            }

            return names;
        }

        public IList<Catalog> GetAllCatalogs()
        {

            AdomdRestrictionCollection restrictions = new AdomdRestrictionCollection();
            DataSet resultSet = AdomdConnection.GetSchemaDataSet("DBSCHEMA_CATALOGS", restrictions);

            List<string> catalogNames = new List<string>();
            var databases = new List<Catalog>();
            foreach (DataRow row in resultSet.Tables[0].Rows)
            {
                catalogNames.Add(row["CATALOG_NAME"].ToString());
                databases.Add(new Catalog
                    {
                        Name = row["CATALOG_NAME"].ToString()
                    });
            }

            //return catalogNames;
            return databases;
            //var result = new DataTable();

            //using (var adapter =
            //    new AdomdDataAdapter(
            //        "SELECT [CATALOG_NAME] FROM $system.dbschema_catalogs",
            //        Connection))
            //{
            //    adapter.Fill(result);
            //}

            //var databases = new List<Catalog>();
            //for (int i = 0; i < result.Rows.Count; i++)
            //{
            //    DataRow dataRow = result.Rows[i];
            //    databases.Add(new Catalog
            //        {
            //            Name = dataRow["CATALOG_NAME"].ToString()
            //        });
            //}
            //return databases;
        }

        public void SetCatalogName(string catalogName)
        {
            if (!string.IsNullOrEmpty(catalogName) && Connection.State == ConnectionState.Open)
            {
                if (Connection.ConnectionString.IndexOf("initial catalog", StringComparison.CurrentCultureIgnoreCase) < 0)
                {
                    Connection.Close();
                    string connectionString = AdomdConnectionHelper.BuildConnectionString(settings);
                    Connection = AdomdConnectionHelper
                        .GetConnection(connectionString + ";Initial Catalog=" + catalogName);
                }
                else
                {
                    if (!string.IsNullOrEmpty(catalogName)
                            && Connection.Database != catalogName)
                    {
                        Connection.ChangeDatabase(catalogName);
                    }
                }
            }
        }

        public AdomdConnection GetConnection(MDXSettings setting)
        {
            return connection ?? (connection = AdomdConnectionHelper.GetConnection(settings));
        }

        public void Dispose()
        {
            Connection.Close();
        }

        public void Connect(AdomdConnection adomdConnection)
        {
            connection = adomdConnection;
        }
    }
}