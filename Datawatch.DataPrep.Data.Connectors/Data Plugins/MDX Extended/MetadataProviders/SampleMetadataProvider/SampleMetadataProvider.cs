﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Microsoft.AnalysisServices.AdomdClient;
using Dimension = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Dimension;
using Hierarchy = Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy;

namespace Panopticon.MDXExtendedPlugin.MetadataProviders.SampleMetadataProvider
{
    class SampleMetadataProvider : IMetadataProvider
    {
        public IDbConnection Connection { get; set; }

        public void Dispose()
        {
            return;
        }

        public Cube GetCubeMetaData(string cubeName)
        {
            return new Cube()
            {
                Caption = "Test Cube",
                Description = "Test Cube",
                Dimensions = new List<Dimension>(new[]
                {
                    new Dimension()
                    {
                        Caption = "Dimension1",
                        Description = "Dimension1",
                        DimensionType = DimensionType.Measure,
                        Name = "Dimension1",
                        UniqueName = "Dimension1",
                        HierarchyInfos = new List<Hierarchy>(new[]
                        {
                            new Hierarchy()
                            {
                                AllMemberUniqueName = "Members",
                                Caption = "Hierarchy1",
                                DefaultMember = "DefaultMember",
                                Description = "Description",
                                Name = "Hierarchy1",
                                UniqueName = "Hierarchy1"
                            }
                        })
                    }
                }),
                MeasureGroups = new List<MeasureGroup>(new []
                {
                    new MeasureGroup()
                    {   
                        Caption = "Test Measure Group", 
                        CatalogName = "TestCatalog", 
                        CubeName = "Test Cube", 
                        Description = "Measure Group", 
                        Dimensions = new List<string>(new[] { "Dimension1" }),
                        Name = "TestMeasureGroup",
                        UniqueName = "TestMeasureGroup"
                    } 
                }),
            };
        }

        public IList<MeasureGroup> GetMeasureGroups(string cubeName)
        {
            return new List<MeasureGroup>(new []
            {
                new MeasureGroup()
                {   
                    Caption = "Test Measure Group", 
                    CatalogName = "TestCatalog", 
                    CubeName = "Test Cube", 
                    Description = "Measure Group", 
                    Dimensions = new List<string>(new[] { "Dimension1" }),
                    Name = "TestMeasureGroup",
                    UniqueName = "TestMeasureGroup"
                } 
            });
        }

        public CubeDef FindCubeByName(string cubeName)
        {
            throw new NotImplementedException();
        }

        public IList<string> GetAllCubes()
        {
            return new List<string>() { "Test Cube" };
        }

        public IList<Catalog> GetAllCatalogs()
        {
            return new List<Catalog>(new[] { new Catalog() { Caption = "Test Catalog", Name = "TestCatalog", UniqueName = "TestCatalog", Description = "Test Catalog" } });
        }

        public void SetCatalogName(string catalogName)
        {
            return;
        }

        public AdomdConnection GetConnection(MDXSettings setting)
        {
            throw new NotImplementedException();
        }
    }
}
