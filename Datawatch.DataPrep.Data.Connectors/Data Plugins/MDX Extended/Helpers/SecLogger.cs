﻿using System;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.MDXExtendedPlugin.Helpers
{
    class SecLogger
    {
        public static void LogConString(String source, String constring)
        {
            try
            {
                String toLog;
                int start, end;

                start = constring.IndexOf("password", StringComparison.CurrentCultureIgnoreCase);
                if (start > -1)
                {
                    start = constring.IndexOf("=", start, StringComparison.CurrentCultureIgnoreCase) + 1;
                    end = constring.IndexOf(";", start, StringComparison.CurrentCultureIgnoreCase);
                    toLog = constring.Replace(
                        constring.Substring(start, (end > -1 ? end : constring.Length)
                                                   - start),
                        "*****");
                }
                else
                {
                    Log.Info(source, " connectionString = " + constring);
                }
            }
            catch (Exception e)
            {
                Log.Info("connection string logging failed: " + source);
            }
        }
    }
}
