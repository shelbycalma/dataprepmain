﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;
using Datawatch.DataPrep.Data.Framework.Model;

using Panopticon.MDXExtendedPlugin.ClientUtils;

namespace Panopticon.MDXExtendedPlugin
{
    public class MDXSettings : ConnectionSettings, IMdxSettings
    {
        public MDXSettings(PropertyBag properties)
            : base(properties)
        {
            InitializeConnectionProperties();
        }

        public ConnectionProperties ConnectionProperties { get; set; }

        public string RowDimensionColumnsBackup
        {
            get { return GetInternal("RowDimensionColumnsBackup"); }
            set { SetInternal("RowDimensionColumnsBackup", value); }
        }

        public string Catalog
        {
            get { return GetInternal("Catalog"); }
            set { SetInternal("Catalog", value); }
        }

        public string CubeName
        {
            get { return GetInternal("Cube"); }
            set { SetInternal("Cube", value); }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public bool ManualEdit
        {
            get
            {
                string s = GetInternal("ManualEdit");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("ManualEdit", Convert.ToString(value)); }
        }

        public bool SkipAggregatedRows
        {
            get
            {
                string s = GetInternal("SkipAggregatedRows");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("SkipAggregatedRows", Convert.ToString(value)); }
        }

        public bool ExternalAggregates
        {
            get
            {
                string s = GetInternal("ExternalAggregates", "False");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("ExternalAggregates", Convert.ToString(value)); }
        }

        public bool QueryOnDemand
        {
            get
            {
                string s = GetInternal("QueryOnDemand");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("QueryOnDemand", Convert.ToString(value)); }
        }

        public bool QueryOnDemandRows
        {
            get
            {
                string s = GetInternal("QueryOnDemandRows");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("QueryOnDemandRows", Convert.ToString(value)); }
        }

        public bool QueryOnDemandColumns
        {
            get
            {
                string s = GetInternal("QueryOnDemandColumns");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("QueryOnDemandColumns", Convert.ToString(value)); }
        }

        public bool FillRaggedHierarchies
        {
            get
            {
                string s = GetInternal("FillRaggedHierarchies");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("FillRaggedHierarchies", Convert.ToString(value)); }
        }

        public FillRaggedHierarchyTypes FillRaggedHierarchyType
        {
            get
            {
                string s = GetInternal("FillRaggedHierarchyType");
                if (string.IsNullOrEmpty(s))
                {
                    return FillRaggedHierarchyTypes.Left;
                }
                return (FillRaggedHierarchyTypes) Enum.Parse(typeof (FillRaggedHierarchyTypes), s);
            }

            set { SetInternal("FillRaggedHierarchyType", Enum.GetName(typeof (FillRaggedHierarchyTypes), value)); }
        }

        public bool NullSuppression
        {
            get
            {
                string s = GetInternal("NullSuppression");
                return !string.IsNullOrEmpty(s) && Convert.ToBoolean(s);
            }

            set { SetInternal("NullSuppression", Convert.ToString(value)); }
        }

        public string[] ColumnsAxis
        {
            get
            {
                var itemsCount = GetInternalInt("ColumnsItemsCount", 0);
                var resultItems = new string[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    resultItems[i] = GetInternal(string.Format("ColumnsItems_{0}", i));
                }

                return resultItems;
            }

            set
            {
                if (value == null)
                {
                    SetInternalInt("ColumnsItemsCount", 0);
                    return;
                }

                SetInternalInt("ColumnsItemsCount", value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    SetInternal(string.Format("ColumnsItems_{0}", i), value[i]);
                }
            }
        }

        public string[] RowsAxis
        {
            get
            {
                var itemsCount = GetInternalInt("RowsItemsCount", 0);
                var resultItems = new string[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    resultItems[i] = GetInternal(string.Format("RowsItems_{0}", i));
                }

                return resultItems;
            }

            set
            {
                if (value == null)
                {
                    SetInternalInt("RowsItemsCount", 0);
                    return;
                }

                SetInternalInt("RowsItemsCount", value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    SetInternal(string.Format("RowsItems_{0}", i), value[i]);
                }
            }
        }

        public string[] SlicerAxis
        {
            get
            {
                var itemsCount = GetInternalInt("SlicersItemsCount", 0);
                var resultItems = new string[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    resultItems[i] = GetInternal(string.Format("SlicersItems_{0}", i));
                }

                return resultItems;
            }

            set
            {
                if (value == null)
                {
                    SetInternalInt("SlicersItemsCount", 0);
                    return;
                }

                SetInternalInt("SlicersItemsCount", value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    SetInternal(string.Format("SlicersItems_{0}", i), value[i]);
                }
            }
        }

        public ProviderNames Provider
        {
            get { return StringToProviderName(GetInternal("Provider","ADOMD")); }
            set
            {
                SetInternal("Provider", ProviderNameToString(value));
                InitializeConnectionProperties();
            }
        }

        public void InitializeConnectionProperties()
        {
            // PROVIDER IMPLEMENTATION : Add initialization of overridden ConnectionProperties object
            switch (Provider)
            {
                case ProviderNames.SapAdo:
                    ConnectionProperties =
                        new SapMdxConnectionProperties(this.ToPropertyBag());
                    break;
                case ProviderNames.Adomd:
                    ConnectionProperties =
                        new AdomdMdxConnectionProperties(this.ToPropertyBag());
                    break;
                case ProviderNames.SampleProvider:
                    ConnectionProperties =
                        new SampleProviderProperties(this.ToPropertyBag());
                    break;

                case ProviderNames.Generic:
                    ConnectionProperties =
                        new GenericMdxConnectionProperties(this.ToPropertyBag());
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SaveParameter(string itemName, string parameterName)
        {
            SetInternal(string.Format("Param_{0}", itemName), parameterName);
        }

        public string GetParameter(string itemName)
        {
            return GetInternal(string.Format("Param_{0}", itemName), string.Empty);
        }

        public void RemoveParameter(string itemName)
        {
            PropertyBag bag = this.ToPropertyBag();
            bag.Values.Remove(string.Format("Param_{0}", itemName));
        }

        public void RestoreDefaults()
        {
            this.ColumnsAxis = null;
            this.RowsAxis = null;
            this.SlicerAxis = null;
            if (!ManualEdit)
            {
                this.Query = string.Empty;
            }
        }

        public static ProviderNames StringToProviderName(string providerNameString)
        {
            // PROVIDER IMPLEMENTATION : Add new provider names to conversion
            switch (providerNameString)
            {
                case AdomdMdxClientUtil.AdomdSAPProviderName:
                    return ProviderNames.SapAdo;

                case AdomdMdxClientUtil.AdomdProviderName:
                    return ProviderNames.Adomd;

                case AdomdMdxClientUtil.AdomdGenericProviderName:
                    return ProviderNames.Generic;

                case SampleMdxClientUtil.ProviderName:
                    return ProviderNames.SampleProvider;

               
            }
            return ProviderNames.Unknown;
        }

        public static string ProviderNameToString(ProviderNames providerName)
        {
            // PROVIDER IMPLEMENTATION : Add new provider names to conversion
            switch (providerName)
            {
                case ProviderNames.SapAdo:
                    return AdomdMdxClientUtil.AdomdSAPProviderName;

                case ProviderNames.Adomd:
                    return AdomdMdxClientUtil.AdomdProviderName;

                case ProviderNames.Generic:
                    return AdomdMdxClientUtil.AdomdGenericProviderName;

                case ProviderNames.SampleProvider:
                    return SampleMdxClientUtil.ProviderName;

                default:
                    return String.Empty;
            }
        }
    }

    public enum FillRaggedHierarchyTypes
    {
        Left,
        Right
    }

    // PROVIDER IMPLEMENTATION : Add new provider names to enum
    public enum ProviderNames
    {
        Adomd,
        SampleProvider,
        SapAdo,
        Generic,
        Unknown
    }
}