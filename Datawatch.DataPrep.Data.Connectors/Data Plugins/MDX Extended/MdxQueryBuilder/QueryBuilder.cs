﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;


namespace Panopticon.MDXExtendedPlugin.MdxQueryBuilder
{
    public class QueryBuilder: AbstractQueryBuilder
    {
        private readonly MDXSettings settings;


        public QueryBuilder(MDXSettings settings):base(settings)
        {
            this.settings = settings;
        }
    }
}