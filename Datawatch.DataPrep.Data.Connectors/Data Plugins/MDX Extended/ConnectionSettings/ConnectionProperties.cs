﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.MDXExtendedPlugin.Annotations;

namespace Panopticon.MDXExtendedPlugin
{
    public abstract class ConnectionProperties : INotifyPropertyChanged
    {
        private readonly Dictionary<string, ConnectionProperty> properties = new Dictionary<string, ConnectionProperty>();
        private readonly PropertyBag connectionSettingsBag;

        public ICollection<ConnectionProperty> Values
        {
            get
            {
                return properties.Values;
            }
        }

        public ConnectionProperty this[string settingName]
        {
            get
            {
                ConnectionProperty setting;
                bool hasValue = properties.TryGetValue(settingName, out setting);
                return hasValue ? setting : new ConnectionProperty(null, null);
            }
        }

        public ConnectionProperties(PropertyBag settingsBag, string connectionSettingsBagName = "ConnectionSettings")
        {
            connectionSettingsBag = settingsBag.SubGroups[connectionSettingsBagName];
            if (connectionSettingsBag == null)
            {
                connectionSettingsBag = new PropertyBag();
                settingsBag.SubGroups[connectionSettingsBagName] = connectionSettingsBag;
            }
            LoadFromPropertyBag();
        }

        private void AddProperty(ConnectionProperty property)
        {
            properties.Add(property.Id, property);
        }

        // PROVIDER IMPLMENTATION : Override this class and implement this to provide default settings
        protected abstract List<ConnectionProperty> GetDefaultConnectionSettings();

        public abstract bool IsOk { get; }

        public void SaveToPropertyBag()
        {
            connectionSettingsBag.Clear();
            foreach (var kvp in properties)
            {
                var propertyId = kvp.Key;
                var connectionProperty = kvp.Value;
                var settingBag = connectionSettingsBag.SubGroups[propertyId];
                if (settingBag == null)
                {
                    settingBag = new PropertyBag();
                    connectionSettingsBag.SubGroups[propertyId] = settingBag;
                }
                connectionProperty.SaveToPropertyBag(settingBag);
            }
        }

        public void LoadFromPropertyBag()
        {
            properties.Clear();
            var defaultSettings = GetDefaultConnectionSettings();
            foreach (var defaultProperty in defaultSettings)
            {
                var connectionProperty =
                    GetSettingFromPropertyBag(defaultProperty.Id);
                var propertyToUse = connectionProperty.Id != null
                    ? connectionProperty
                    : defaultProperty;
                AddProperty(propertyToUse);
            }
        }

        private ConnectionProperty GetSettingFromPropertyBag(string settingName)
        {
            var settingBag = connectionSettingsBag.SubGroups[settingName];
            if (settingBag != null)
            {
                return new ConnectionProperty(settingBag);
            }
            return new ConnectionProperty(null, null);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
