﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.MDXExtendedPlugin
{
    class GenericMdxConnectionProperties : ConnectionProperties
    {
        public const string None = "none";
        public const string Reconnect = "none";
        public GenericMdxConnectionProperties(PropertyBag properties)
            : base(properties)
        {
        }

        protected override List<ConnectionProperty> GetDefaultConnectionSettings()
        {
            var connectionSettings = new List<ConnectionProperty>();
            connectionSettings.Add(
                new ConnectionProperty("ServerName", "localhost", FieldType.TextBox, "Server URL"));

            connectionSettings.Add(
                new ConnectionProperty("AuthType", "Windows", FieldType.ComboBox, "Authorization Type", 
                                           new List<string> { "Basic", "Windows" }));
            connectionSettings.Add(
                new ConnectionProperty("UserName", "", FieldType.TextBox, "User Name"));
            connectionSettings.Add(
                new ConnectionProperty("Password", "", FieldType.PasswordBox, "Password"));

            connectionSettings.Add(
               new ConnectionProperty(Reconnect, "", FieldType.CheckBox, "Reconnect on Catalog Change"));
            return connectionSettings;
        }

        public override bool IsOk
        {
            get
            {
                try
                {
                    if (String.IsNullOrEmpty(this["ServerName"].Value))
                        return false;
                    if (this["AuthType"].Value == "Windows")
                    {
                        return true;
                    }
                    else if (this["AuthType"].Value == "Basic")
                    {
                        return !String.IsNullOrEmpty(this["UserName"].Value);
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception Ex)
                {
                    Log.Exception(Ex);
                    return false;
                }
            }
        }
    }
}
