﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.MDXExtendedPlugin.Annotations;

namespace Panopticon.MDXExtendedPlugin
{
    public class ConnectionProperty : INotifyPropertyChanged 
    {
        private const string IdPropertyName = "Id";
        private const string DescriptionPropertyName = "Description";
        private const string OptionsPropertyBagName = "Options";
        private const string ValuePropertyName = "Value";
        private const string FieldTypePropertyName = "FieldType";

        public ConnectionProperty(
            string id, string value, FieldType fieldType = FieldType.TextBox, string description = null, List<string> options = null)
        {
            Id = id;
            Value = value;
            FieldType = fieldType;
            Description = description;
            Options = options ?? new List<string>();
        }

        public ConnectionProperty(PropertyBag settingBag)
        {
            LoadProperties(settingBag);
            LoadOptions(settingBag);
        }

        public void LoadProperties(PropertyBag settingBag)
        {
            foreach (var settingProperty in settingBag.Values)
            {
                switch (settingProperty.Name)
                {
                    case IdPropertyName:
                        Id = settingProperty.Value;
                        break;
                    case DescriptionPropertyName:
                        Description = settingProperty.Value;
                        break;
                    case ValuePropertyName:
                        Value = settingProperty.Value;
                        break;
                    case FieldTypePropertyName:
                        FieldType =
                            StringToFieldType(settingProperty.Value);
                        break;
                }
            }
        }

        public void LoadOptions(PropertyBag settingBag)
        {
            if (Options == null)
                Options = new List<string>();
            PropertyBag optionsBag =
                settingBag.SubGroups[OptionsPropertyBagName];
            foreach (var value in optionsBag.Values)
            {
                Options.Add(value.Value);
            }
        }

        /// <summary>
        /// unique id under which this property is stored in the property bag
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Description: something which could be used as a label. The string 
        /// is returned from a resource rather than being hardcoded 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// in case there are options available this returns a list of 
        /// potential options. if there are not options this value returns a 
        /// list of size 0
        /// </summary>
        public List<string> Options { get; set; }

        /// <summary>
        /// the property value, only strings are supported
        /// </summary>
        private string _value;
        public string Value
        {
            get { return _value; }
            set
            {
                _value = value;
                OnPropertyChanged("Value");
            }
        }

        /// <summary>
        /// Controls which field type is used to configure the property,
        /// determines how the row will show up in the configuration grid.
        /// </summary>
        public FieldType FieldType { get; set; }

        public bool? BoolValue()
        {
            bool boolValue;
            if(bool.TryParse(Value, out boolValue))
            {
                return boolValue;
            }
            return null;
        }

        public void SaveToPropertyBag(PropertyBag bag)
        {   
            bag.Clear();
            bag.Values.Add(new PropertyValue(IdPropertyName, Id));
            bag.Values.Add(new PropertyValue(DescriptionPropertyName, Description));
            bag.Values.Add(new PropertyValue(ValuePropertyName, Value));
            bag.Values.Add(new PropertyValue(FieldTypePropertyName, FieldTypeToString(FieldType)));
            var optionsBag = GetOptionsBag(bag);
            optionsBag.Clear();
            int i = 0;
            foreach (var option in Options)
            {
                optionsBag.Values.Add(new PropertyValue(i.ToString(CultureInfo.InvariantCulture), option));
                i++;
            }
        }

        private PropertyBag GetOptionsBag(PropertyBag bag)
        {
            PropertyBag optionsBag =
                bag.SubGroups[OptionsPropertyBagName];
            if (optionsBag == null)
            {
                optionsBag = new PropertyBag();
                bag.SubGroups[OptionsPropertyBagName] = optionsBag;
            }
            return optionsBag;
        }

        public static FieldType StringToFieldType(string fieldTypeStr)
        {
            switch (fieldTypeStr)
            {
                case "TextBox":
                    return FieldType.TextBox;
                case "PasswordBox":
                    return FieldType.PasswordBox;
                case "ComboBox":
                    return FieldType.ComboBox;
                case "CheckBox":
                    return FieldType.CheckBox;
                default:
                    return FieldType.TextBox;
            }
        }

        public static string FieldTypeToString(FieldType fieldType)
        {
            switch (fieldType)
            {
                case FieldType.TextBox:
                    return "TextBox";
                    break;
                case FieldType.PasswordBox:
                    return "PasswordBox";
                    break;
                case FieldType.ComboBox:
                    return "ComboBox";
                    break;
                case FieldType.CheckBox:
                    return "CheckBox";
                    break;
                default:
                    return "TextBox";
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public enum FieldType
    {
        TextBox,
        PasswordBox,
        ComboBox,
        CheckBox
    }
}
