﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.MDXExtendedPlugin
{
    class SampleProviderProperties : ConnectionProperties
    {
        public SampleProviderProperties(PropertyBag settingsBag, string connectionSettingsBagName = "ConnectionSettings") : base(settingsBag, connectionSettingsBagName)
        {
        }

        protected override List<ConnectionProperty> GetDefaultConnectionSettings()
        {
            var connectionSettings = new List<ConnectionProperty>();
            connectionSettings.Add(new ConnectionProperty("TestText", "Text", FieldType.TextBox, "TextBox"));
            connectionSettings.Add(new ConnectionProperty("TestCombo", "ComboBox", FieldType.ComboBox, "ComboBox", new List<string> { "Item1", "Item2" }));
            connectionSettings.Add(new ConnectionProperty("TestCheckbox", "False", FieldType.CheckBox, "CheckBox"));
            connectionSettings.Add(new ConnectionProperty("TestPassword", "", FieldType.PasswordBox, "PasswordBox"));
            return connectionSettings;
        }

        public override bool IsOk
        {
            get { return true; }
        }
    }
}
