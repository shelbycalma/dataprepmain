﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.MDXExtendedPlugin.MetadataProviders.SampleMetadataProvider;

namespace Panopticon.MDXExtendedPlugin.ClientUtils
{
    class SampleMdxClientUtil : IMDXClientUtil
    {
        public const string ProviderName = "Sample";
        SampleMetadataProvider metadataProvider = new SampleMetadataProvider();

        public SampleMdxClientUtil(MDXSettings settings)
        {
        }

        public ITable ExecuteOnDemandQuery(
            MDXSettings settings, IEnumerable<ParameterValue> parameters, int maxRows)
        {
            return new StandaloneTable();
        }

        public ITable Execute(MDXSettings settings, IEnumerable<ParameterValue> parameters, int maxRows)
        {
            return new StandaloneTable();
        }

        public string[] GetCatalogsList()
        {
            return metadataProvider.GetAllCatalogs().Select(catalog => catalog.Name).ToArray();
        }

        public string[] GetCubesList(string catalogName)
        {
            return metadataProvider.GetAllCubes().ToArray();
        }

        public Cube GetCubeMetadata(string catalogName, string cubeName)
        {
            metadataProvider.SetCatalogName(catalogName);
            return metadataProvider.GetCubeMetaData(cubeName);
        }
    }
}
