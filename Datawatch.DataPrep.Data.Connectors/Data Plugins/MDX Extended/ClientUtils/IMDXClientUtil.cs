﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.MDXExtendedPlugin.ClientUtils
{
    // PROVIDER IMPLEMENTATION : Implement this interface
    /// <summary>
    /// 
    /// </summary>
    public interface IMDXClientUtil
    {
        /// <summary>
        /// Create the necessary MDX statement and returns the content as 
        /// ITable.
        /// </summary>
        /// <param name="settings">property bag</param>
        /// <param name="parameters">ui parameters</param>
        /// <param name="maxRows">Maximum number of rows which can returned</param>
        /// <returns>Raw ITable resulting from the query which can be used for 
        /// further processing e.g. External Aggregates</returns>
        ITable ExecuteOnDemandQuery(
            MDXSettings settings, IEnumerable<ParameterValue> parameters,
            int maxRows);

        /// <summary>
        ///  Creates and executes a MDX query
        /// </summary>
        /// <param name="settings">property bag</param>
        /// <param name="parameters">UI Parameters as used in the UI</param>
        /// <param name="maxRows">maximum number of Rows to be returned</param>
        /// <returns>Raw ITable resulting from the query which can be used for 
        /// further processing e.g. Default Parameter handling</returns>
        ITable Execute(
            MDXSettings settings,
            IEnumerable<ParameterValue> parameters, int maxRows);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        string[] GetCatalogsList();

        /// <summary>
        /// Returns a list of cubes for the given catalog
        /// </summary>
        /// <param name="catalogName">name of the catalog to query</param>
        /// <returns></returns>
        string[] GetCubesList(string catalogName);

        /// <summary>
        /// Fills the whole meta data structure 
        /// </summary>
        /// <param name="catalogName">the database/catalog/server name. </param>
        /// <param name="cubeName">name of the cube </param>
        /// <returns></returns>
        Cube GetCubeMetadata(string catalogName, string cubeName);
    }
}
