﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.MDXExtendedPlugin.ClientUtils;
using Panopticon.MDXExtendedPlugin.MetadataProviders;

using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;
using Level = Microsoft.AnalysisServices.AdomdClient.Level;
using Tuple = Microsoft.AnalysisServices.AdomdClient.Tuple;

namespace Panopticon.MDXExtendedPlugin
{
    internal class AdomdMdxClientUtil : IMDXClientUtil
    {  
        public const string AdomdProviderName = "ADOMD";
        public const string AdomdSAPProviderName = @"SAP-BW/XMLA";
        public const string AdomdGenericProviderName = "Generic";
        private Dictionary<string, string> encodedParametersMap;
        //private AdomdConnection connection;
        private IMetadataProvider metadataProvider;
        private readonly MDXSettings settings;

        public AdomdMdxClientUtil(MDXSettings mdxSettings)
        {
            settings = mdxSettings;
        }

        public void SetMetaDataProvider(IMetadataProvider provider)
        {
            this.metadataProvider = provider;
        }

        private string ApplyParameters(string source,
            IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(source)) return null;
            if (parameters == null) return source;

            //var mdxParameteres = parameters.Select(p => new ParameterValue(p.Name, new StringParameterValue()
            //    {
            //        Value = string.Format("@{0}", p.Name)
            //    }));

            //return new ParameterEncoder
            //{
            //    SourceString = source,
            //    Parameters = mdxParameteres,
            //    NullOrEmptyString = "NULL"
            //}.Encoded();

            List<ParameterValue> paramsList = parameters.ToList();
            if (paramsList.Any())
            {
                encodedParametersMap = new Dictionary<string, string>();
            }

            for (int i = 0; i < paramsList.Count; i++)
            {
                ParameterValue parameter = paramsList[i];
                string newValue = string.Format("@p{0}", i);
                string paramName = string.Format("@{0}", parameter.Name);
                source = source.Replace(paramName, newValue);
                encodedParametersMap.Add(parameter.Name, "p" + i);
            }

            return source;
        }

        public ITable ExecuteOnDemandQuery(
            MDXSettings settings, IEnumerable<ParameterValue> parameters,
            int maxRows)
        {
            return new StandaloneTable();
        }

        public ITable Execute(MDXSettings settings,
            IEnumerable<ParameterValue> parameters, int maxRows)
        {
            string query = ApplyParameters(settings.Query, parameters);
            StandaloneTable table;

            AdomdCommand cmd = new AdomdCommand(query, (AdomdConnection) this.metadataProvider.Connection);

            if (parameters != null)
            {
                foreach (ParameterValue parameterValue in parameters)
                {
                    cmd.Parameters.Add(new AdomdParameter(encodedParametersMap[parameterValue.Name],
                        GetParameterValue(parameterValue.TypedValue)));
                }
            }

            Log.Info(Properties.Resources.LogMDXExecutingQuery,
                cmd.CommandText);
            if (!string.IsNullOrEmpty(cmd.CommandText))
            {
                try
                {
                    CellSet cellSet = cmd.ExecuteCellSet();
                    table = CreateTable(cellSet, settings, maxRows);
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

                Log.Info(Properties.Resources.LogMDXQueryCompleted,
                    table.RowCount, table.ColumnCount);

                // We're creating a new StandaloneTable on every request, so allow
                // EX to freeze it.
                table.BeginUpdate();
                try
                {
                    table.CanFreeze = true;
                }
                finally
                {
                    table.EndUpdate();
                }

                return table;
            }
            else
            {
                table = new StandaloneTable();
                table.BeginUpdate();
                   table.AddColumn(new TextColumn("EmptyCube"));
                try
                {
                    table.CanFreeze = true;
                }
                finally
                {
                    table.EndUpdate();
                }

                return table;
            }

        }

        private StandaloneTable CreateTable(CellSet cellSet, MDXSettings settings, int maxRows)
        {
            bool bUseBackupHierarchies = false;
            if (cellSet.Axes.Count == 0)
            {
                throw new Exception(Properties.Resources.ExNoData);
            }

            //current implementation only supports two exes
            if (cellSet.Axes.Count != 2)
            {
                throw new Exception(Properties.Resources.ExInvalidAxes);
            }

            if (!(cellSet.Axes[0].Positions.Count > 0) &&
                !(cellSet.Axes[1].Positions.Count > 0))
            {
                throw new Exception(Properties.Resources.ExNoData);
            }

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            // Go through the row axis.
            // Create text columns for each level of each hierarchy.
            HierarchyCollection rowHierarchies = cellSet.Axes[1].Set.Hierarchies;
            var hierarchyStartIndex = new int[rowHierarchies.Count];
            for (int i = 0; i < rowHierarchies.Count; i++)
            {
                Hierarchy hierarchy = rowHierarchies[i];
                hierarchyStartIndex[i] = table.ColumnCount;
                try
                {
                    Level lTest = hierarchy.Levels[0];
                }
                catch (Exception ex)
                {
                    bUseBackupHierarchies = true;
                }
                if (!bUseBackupHierarchies)
                {
                    foreach (Level level in hierarchy.Levels)
                    {
                        string columnName = hierarchy.Caption + " " + level.Caption;
                        TextColumn column = new TextColumn(columnName);
                        table.AddColumn(column);
                    }
                }
                else
                {
                    string[] backupHierarchies = settings.RowDimensionColumnsBackup.Split('@');
                    for (int h = 0; h <= backupHierarchies.GetUpperBound(0); h++)
                    {
                        string[] hierarchyParts = backupHierarchies[h].Split('~');
                        if (hierarchyParts[0].Equals(hierarchy.UniqueName) ||
                            hierarchyParts[0].Equals("[" + hierarchy.UniqueName + "]"))   // needed for SAP
                        {
                            string[] levels = hierarchyParts[2].Split('|');
                            for (int l = 0; l < levels.GetUpperBound(0) ; l++)
                            {
                                string columnName = hierarchyParts[0] + " " + levels[l];
                                TextColumn column = new TextColumn(columnName);
                                table.AddColumn(column);
                            }
                            break;
                        }
                    }
                }
            }
            string[] textValues = new string[table.ColumnCount];
            string[] raggedTextValues = new string[table.ColumnCount];

            // Go through the column axis.
            // Create one numeric column per tuple.
            TupleCollection columnTuples = cellSet.Axes[0].Set.Tuples;
            foreach (Tuple tuple in columnTuples)
            {
                StringBuilder columnName = new StringBuilder();
                foreach (Member member in tuple.Members)
                {
                    if (columnName.Length > 0)
                    {
                        columnName.Append(" ");
                    }
                    columnName.Append(member.Caption);
                }
                NumericColumn column = new NumericColumn(columnName.ToString());
                table.AddColumn(column);
            }
            if (settings.ExternalAggregates)
            {
                TextColumn column = new TextColumn(Plugin.partitionColumnName);
                table.AddColumn(column);
            }

            // Pull out the data
            TupleCollection rowTuples = cellSet.Axes[1].Set.Tuples;
            for (int rowTupleIndex = 0; rowTupleIndex < rowTuples.Count; rowTupleIndex++)
            {
                if (maxRows > -1 && table.RowCount >= maxRows)
                {
                    break;
                }

                Tuple rowTuple = rowTuples[rowTupleIndex];
                IsRaggedToAdd isRaggedToAdd = IsRaggedToAdd.Undefined;
                bool[] raggedHierarchies = new bool[rowTuple.Members.Count];
                if (settings.FillRaggedHierarchies)
                {
                    for (int i = 0; i < rowTuple.Members.Count; i++)
                    {
                        Member member = rowTuple.Members[i];
                        if (member.GetChildren(0, 1).Count == 0)
                        {
                            if (member.LevelDepth + 1 != member.ParentLevel.ParentHierarchy.Levels.Count)
                            {
                                isRaggedToAdd = IsRaggedToAdd.Yes;
                                raggedHierarchies[i] = true;
                            }
                        }
                        else if (isRaggedToAdd == IsRaggedToAdd.Undefined)
                        {
                            isRaggedToAdd = IsRaggedToAdd.No;
                        }
                    }
                }

                bool isAggregateRow = false;
                for (int i = 0; i < rowTuple.Members.Count; i++)
                {
                    Member member = rowTuple.Members[i];
                    if (settings.FillRaggedHierarchies)
                    {
                        isAggregateRow = isAggregateRow || member.ChildCount > 0;
                        if (!raggedHierarchies[i] && settings.SkipAggregatedRows && isAggregateRow)
                        {
                            isRaggedToAdd = IsRaggedToAdd.No;
                        }
                    }
                    else
                    {
                        isAggregateRow = isAggregateRow || (member.ChildCount > 0 && !raggedHierarchies[i]);
                    }
                    int levelDepth = member.LevelDepth;
                    int textValueIndex = hierarchyStartIndex[i] + levelDepth;
                    string value = member.Caption;
                    textValues[textValueIndex] = value;
                    raggedTextValues[textValueIndex] = value;


                    // blank out row axis values to the right of this level
                    if (!bUseBackupHierarchies &&
                        (!IsParentSameAsPrevious(rowTuples, i, rowTupleIndex) || isRaggedToAdd == IsRaggedToAdd.Yes)
                         )
                    {
                        int lastIndex = member.ParentLevel.ParentHierarchy.Levels.Count
                                        - member.LevelDepth
                                        + textValueIndex;
                        for (int j = textValueIndex + 1; j < lastIndex; j++)
                        {
                            if (!IsParentSameAsPrevious(rowTuples, i, rowTupleIndex))
                            {
                                textValues[j] = null;
                            }
                            if (isRaggedToAdd == IsRaggedToAdd.Yes)
                            {
                                switch (settings.FillRaggedHierarchyType)
                                {
                                    case FillRaggedHierarchyTypes.Left:
                                        raggedTextValues[j] = "-";
                                        break;
                                    case FillRaggedHierarchyTypes.Right:
                                        raggedTextValues[j] = raggedTextValues[j - 1];
                                        break;
                                }
                            }
                        }
                    }
                }

                if (!settings.SkipAggregatedRows || !isAggregateRow)
                {
                    SetRow(table, textValues, columnTuples, cellSet, rowTupleIndex, isAggregateRow);
                }
                if (isRaggedToAdd == IsRaggedToAdd.Yes)
                {
                    switch (settings.FillRaggedHierarchyType)
                    {
                        case FillRaggedHierarchyTypes.Right:
                            SetRow(table, raggedTextValues, columnTuples, cellSet, rowTupleIndex,isAggregateRow);
                            break;
                        case FillRaggedHierarchyTypes.Left:
                            table.AddRow(raggedTextValues);
                            if (settings.ExternalAggregates)
                            {
                                AddExtraExternalAggregateRowIfNecessary(table, raggedTextValues);
                            }
                            break;
                    }
                }
            }
            Log.Info("********************");
            for (int r = 0; r < table.RowCount; r++)
            {
                Row rw = table.GetRow(r);
                string s = "";
                for (int c = 0; c < table.ColumnCount ; c++)
                {
                    var t = table.GetColumn(c).GetType();
                    if (t.Name.Equals("TextColumn"))
                    {
                        TextColumn cl = (TextColumn)table.GetColumn(c);
                        s = s + cl.GetTextValue(r) + ", ";
                    }
                    else
                    {
                        NumericColumn cl = (NumericColumn)table.GetColumn(c);
                        s = s + cl.GetNumericValue(r).ToString() + ", ";
                    }
                }
                Log.Info(s);
            }
            Log.Info("********************");
            table.EndUpdate();

            return table;
        }

        protected void AddExtraExternalAggregateRowIfNecessary(
            StandaloneTable table, object[] row)
        {
            // check to see if this is a leaf row and therefore
            // needs a special external aggregate row added
            bool isExternalAggregateRow = false;
            //bool isFilterBoxTable = onDemandParameters != null &&
            //    onDemandParameters.Domains != null &&
            //    onDemandParameters.Domains.Any();
            //if (!isFilterBoxTable)
            //{
            //    for (int colchk = 0; colchk < dataStartColumn; colchk++)
            //    {
            //        var col = table.GetColumn(colchk);
            //        if (table.ColumnsToUse.Contains(col.Name) &&
            //            row[colchk] == null)
            //        {
            //            isExternalAggregateRow = true;
            //            break;
            //        }
            //    }
            //}
            if (!isExternalAggregateRow)
            {
                object[] newRowBottom = new object[row.Length];
                row.CopyTo(newRowBottom, 0);
                newRowBottom[table.ColumnCount - 1] = "leaf";
                table.AddRow(newRowBottom);
            }
        }


        private bool IsParentSameAsPrevious(TupleCollection rowTuples, int memberIndex, int rowTupleIndex)
        {
            Tuple currentRowTuple = rowTuples[rowTupleIndex];
            Member currentMember = currentRowTuple.Members[memberIndex];

            //bool check = currentMember.ParentSameAsPrevious;
            bool result = false;
            if (rowTupleIndex != 0)
            {
                Tuple previousRowTuple = rowTuples[rowTupleIndex - 1];
                Member previousMember = previousRowTuple.Members[memberIndex];
                if (currentMember.Parent == null)
                {
                    if (previousMember.Parent == null)
                    {
                        result = currentMember.UniqueName.Equals(previousMember.UniqueName);
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    if (previousMember.Parent != null)
                        result = currentMember.Parent.UniqueName.Equals(previousMember.Parent.UniqueName);
                }
            }
            //if (result != check)
            //{
            //    throw new ApplicationException("Members check code is wrong");
            //}
            return result;
        }

        private void SetRow(StandaloneTable table, string[] textValues,
            TupleCollection columnTuples, CellSet cellSet, int rowTupleIndex,
            bool isAggregateRow)
        {
            object[] ExtAggRow = new object[table.ColumnCount];
            Row row = table.AddRow(textValues);
            if (settings.ExternalAggregates)
            {
                textValues.CopyTo(ExtAggRow, 0);
            }

            for (int colTupleIndex = 0; colTupleIndex < columnTuples.Count; colTupleIndex++)
            {
                int columnIndex = textValues.Length + colTupleIndex;
                NumericColumn column = (NumericColumn)table.GetColumn(columnIndex);
                Cell cell = cellSet.Cells[colTupleIndex, rowTupleIndex];
                object value = cell.Value;

                double numericValue;
                if (value == null)
                {
                    numericValue = NumericValue.Empty;
                }
                else
                {
                    if (value is double)
                    {
                        numericValue = (double)value;
                    }
                    else if (value is string)
                    {
                        if (value.ToString().StartsWith("[day-"))
                        {
                            numericValue = Convert.ToDouble(value.ToString().Substring(value.ToString().LastIndexOf(']') + 1));
                        }
                        else
                        {
                            numericValue = NumericValue.Empty;
                        }
                    }
                    else
                    {
                        numericValue = Convert.ToDouble(value);
                    }
                }

                column.SetNumericValue(row, numericValue);
                if (settings.ExternalAggregates)
                {
                    ExtAggRow[columnIndex] = numericValue;
                }
            }
            if (settings.ExternalAggregates && !isAggregateRow)
            {
                ExtAggRow[table.ColumnCount - 1] = "leaf";
                table.AddRow(ExtAggRow);
            }
        }

        public string[] GetCatalogsList()
        {
            return metadataProvider.GetAllCatalogs().Select(c => c.Name).ToArray();
        }

        public string[] GetCubesList(string catalogName)
        {
            try
            {
                metadataProvider.SetCatalogName(catalogName);
            }
            catch (Exception ex)
            {
            }
            return metadataProvider.GetAllCubes().ToArray();
        }

        public Cube GetCubeMetadata(string catalogName, string cubeName)
        {
            metadataProvider.SetCatalogName(catalogName);
            return metadataProvider.GetCubeMetaData(cubeName);
        }

        private object GetParameterValue(TypedParameterValue typedParameterValue)
        {
            if (typedParameterValue is StringParameterValue)
            {
                return ((StringParameterValue) typedParameterValue).Value;
            }
            if (typedParameterValue is DateTimeParameterValue)
            {
                return ((DateTimeParameterValue) typedParameterValue).Value;
            }
            if (typedParameterValue is NumberParameterValue)
            {
                return ((NumberParameterValue) typedParameterValue).Value;
            }

            throw new ArgumentException(string.Format(Properties.Resources.ExInvalidParameterType,
                typedParameterValue.GetType()));
        }

        private enum IsRaggedToAdd
        {
            Undefined,
            Yes,
            No
        }
    }
}