﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Panopticon.StreamingWebSocketPlugin.UI.dll")]
[assembly: AssemblyDescription("Datawatch StreamingWebSocket Plug-in UI")]

[assembly: PluginDescription("StreamingWebSocket", "Streaming")]