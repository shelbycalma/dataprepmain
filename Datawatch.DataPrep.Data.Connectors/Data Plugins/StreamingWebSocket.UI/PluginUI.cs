﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.StreamingWebSocketPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, StreamingWebSocketSettings,
        Dictionary<string, object>, StreamingWebSocketAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner,
            "pack://application:,,,/Panopticon.StreamingWebSocketPlugin.UI;component/blank16.png")
        {
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new StreamingWebSocketSettings(Plugin.PluginManager, bag, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(StreamingWebSocketSettings settings)
        {
            return new ConnectionWindow(settings);
        }
    }
}
