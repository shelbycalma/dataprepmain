﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.StreamingWebSocketPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private StreamingWebSocketSettings settings;

        public ConnectionPanel() 
            :this(null)
        {
        }

        public ConnectionPanel(StreamingWebSocketSettings settings)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            DataContextChanged +=
                UserControl_DataContextChanged;
            if (settings != null)
            {
                PasswordBox.Password = settings.WebPassword;
            }
        }

        public StreamingWebSocketSettings Settings
        {
            get { return settings; }
        }

        protected void OnConnectionChanged(StreamingWebSocketSettings newConnection)
        {
            DataContext = newConnection;

            if (newConnection != null)
            {
                PasswordBox.Password = newConnection.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.WebPassword = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
        DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as StreamingWebSocketSettings;
        }

    }
}
