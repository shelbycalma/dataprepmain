﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.StreamingWebSocketPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private StreamingWebSocketSettings settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(StreamingWebSocketSettings settings)
        {
            this.settings = settings;
            InitializeComponent();
        }

        public StreamingWebSocketSettings Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
