﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AMPSPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<SowTable, AMPSSettings,
            Dictionary<string, object>, AMPSDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner, "pack://application:,,,/Panopticon.AMPS.UI;component/amps.png")
        {
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConfigPanel(new AMPSSettings(Plugin.PluginManager, bag));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;

            return cp.Settings.ToPropertyBag();
        }
        public override Window CreateConnectionWindow(AMPSSettings settings)
        {
            return new ConfigWindow(settings);
        }
    }
}
