﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AMPS.Client;
using AMPS.Client.Exceptions;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.UI;
using Utils = Datawatch.DataPrep.Data.Core.UI.Utils;

namespace Panopticon.AMPSPlugin.UI
{
    /// <summary>
    /// The ConfigPanel implementa a WPF UserControl allowing editing of data 
    /// connections for the streaming DataPlugin. The ConfigPanel is used  both 
    /// when a connection is created and when edited in the data source settings
    /// dialog.
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
        private ICommand testConnection;
        private ICommand generateColumns;

        private AMPSSettings settings;

        private int messagesReceived;

        /// <summary>
        /// Creates a new instance of the ConfigPanel.
        /// </summary>
        public ConfigPanel()
            : this(null)
        {

        }

        public ConfigPanel(AMPSSettings settings)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        /// <summary>
        /// Gets or sets the current AMPSSettings instance.
        /// </summary>
        public AMPSSettings Settings
        {
            get { return settings; }
        }

        private void Delete_CanExecute(object sender, 
            CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter is ColumnDefinition;
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MessageBox.Show(
                Datawatch.DataPrep.Data.Core.MessageParsing.Properties.Resources.UiAskRemoveColumn,
                AMPSPlugin.Properties.Resources.UiPluginTitle,
                MessageBoxButton.OKCancel,
                MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                Settings.ParserSettings.RemoveColumnDefinition(
                    (Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing.ColumnDefinition) e.Parameter);
            }
        }

        public ICommand GenerateColumns
        {
            get
            {
                if (generateColumns == null)
                {
                    generateColumns = new DelegateCommand(
                        DoGenerateColumns, CanGenerateColumns);
                }
                return generateColumns;
            }
        }

        private void DoGenerateColumns()
        {
            ColumnDefinitionGenerator generator = new ColumnDefinitionGenerator();
            try
            {
                generator.GenerateColumns(settings);
                settings.UpdateIdCandidates();
            }
            catch (Exception e)
            {
                MessageBoxEx.Show(
                    Utils.GetOwnerWindow(this),
                    e.Message,
                    "Datawatch",
                    MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);
                return;
            }
        }

        private bool CanGenerateColumns()
        {
            return Settings != null &&
                   !string.IsNullOrEmpty(Settings.Host) && 
                   !string.IsNullOrEmpty(Settings.Topic) &&
                   (Settings.MessageType == MessageType.Fix || 
                    Settings.MessageType == MessageType.NvFix);            
        }

        public ICommand TestConnection
        {
            get
            {
                if (testConnection == null)
                {
                    testConnection = new DelegateCommand(DoTestConnection, 
                        CanTestConnection);
                }
                return testConnection;
            }
        }

        private bool CanTestConnection()
        {
            return Settings != null &&
                   !string.IsNullOrEmpty(Settings.Host) &&
                   !string.IsNullOrEmpty(Settings.Topic);

        }

        private void DoTestConnection()
        {
            string uriString = Settings.Uri;

            Client client;

            using (client = new Client("Panopticon.AMPSSettings-" + Guid.NewGuid()))
            {
                try
                {
                    client.connect(uriString);
                    client.logon();
                    ParameterValue[] parameters = new ParameterValue[0];
                    string topic = DataUtils.ApplyParameters(
                        settings.Topic, parameters);
                    string filter = DataUtils.ApplyParameters(
                        settings.Filter, parameters);

                    if (!string.IsNullOrEmpty(topic))
                    {
                        TestSubscribe(client, topic, filter);
                    }
                    client.close();
                }
                catch (InvalidTopicException e)
                {
                    MessageBoxEx.Show(
                        Utils.GetOwnerWindow(this),
                        Properties.Resources.UiTopicSubscriptionModeError,
                        "Datawatch",
                        MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                    return;
                }
                catch (Exception e)
                {
                    MessageBoxEx.Show(
                        Utils.GetOwnerWindow(this),
                        e.Message,
                        "Datawatch",
                        MessageBoxButton.OK,
                        MessageBoxImage.Exclamation);
                    return;
                }
                MessageBoxEx.Show(
                    Utils.GetOwnerWindow(this),
                    string.Format(Properties.Resources.UiMessageConnected, 
                        uriString),
                    "Datawatch",
                    MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
        }

        private void TestSubscribe(Client client, string topic, string filter)
        {
            CommandId subscriptionId;
            switch (settings.SubscriptionMode)
            {
                case SubscriptionMode.Sow:
                case SubscriptionMode.SowAndSubscribe:
                case SubscriptionMode.SowAndDeltaSubscribe:
                    subscriptionId = client.sow(
                        dummyMessageHandler,
                        topic, filter, settings.BatchSize, true,
                        settings.Timeout);
                    break;
                default:
                    subscriptionId = client.subscribe(
                        dummyMessageHandler,
                        topic, filter, settings.Timeout);
                    break;
            }
            DateTime t1 = DateTime.Now;
            messagesReceived = 0;
            while (messagesReceived == 0 && (DateTime.Now - t1).Seconds < 3)
            {
                lock (this)
                {
                    Monitor.Wait(this, 100);
                }
            }
            client.unsubscribe(subscriptionId);            
            if (messagesReceived == 0)
            {
                throw new Exception(string.Format(
                    Properties.Resources.UiMessageNoDataReceived, 
                    topic, filter));
            }
        }

        private void dummyMessageHandler(Message message)
        {
            lock (this)
            {
                messagesReceived++;
                Monitor.Pulse(this);
            }
        }

        private void UserControl_DataContextChanged(object sender, 
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as AMPSSettings;
        }
    }
}
