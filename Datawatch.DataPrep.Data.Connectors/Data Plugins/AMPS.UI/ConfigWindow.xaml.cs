﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.AMPSPlugin.UI
{
    /// <summary>
    /// The ConfigWindow is used to show the ConfigPanel in a window with 
    /// OK/Cancel buttons.
    /// </summary>
    public partial class ConfigWindow : Window
    {
        private readonly AMPSSettings settings;

        /// <summary>
        /// Defines the Ok command, used by the Ok button.
        /// </summary>
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindow));

        public ConfigWindow() : this(null)
        {
        }

        /// <summary>
        /// Creates a new ConfigWindow instance for a specific 
        /// AMPSSettings instance.
        /// </summary>
        /// <param name="settings">The AMPSSettings.</param>
        internal ConfigWindow(AMPSSettings settings)
        {
            this.settings = settings;
            InitializeComponent();
        }

        /// <summary>
        /// Gets the current AMPSSettings instance.
        /// </summary>
        public AMPSSettings Settings
        {
            get { return settings; }
        }

        /// <summary>
        /// Checks if the Ok command can be executed. 
        /// If the ConfigPanel exists and it's IsOK property is true, the
        /// command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        /// <summary>
        /// Executes the Ok command, setting the DialogResult to true.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
