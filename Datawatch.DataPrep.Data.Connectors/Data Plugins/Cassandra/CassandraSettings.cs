﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.CassandraPlugin
{
    public class CassandraSettings : ConnectionSettings
    {
        private TimeZoneHelper timeZoneHelper;

        public CassandraSettings() :
            this(new PropertyBag())
        {}

        public CassandraSettings(PropertyBag properties)
            : base(properties)
        {
            PropertyBag timeZoneHelperBag = properties.SubGroups["TimeZone"];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                properties.SubGroups["TimeZone"] = timeZoneHelperBag;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);
        }

        public string ConnectionUrl
        {
            get { return GetInternal("ConnectionUrl", "localhost"); }
            set { SetInternal("ConnectionUrl", value); }
        }

        public string Port
        {
            get { return GetInternal("Port", "9042"); }
            set { SetInternal("Port", value); }
        }

        public string KeySpace
        {
            get { return GetInternal("KeySpace"); }
            set { SetInternal("KeySpace", value); }
        }

        public string UserId
        {
            get { return GetInternal("UserId"); }
            set { SetInternal("UserId", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public bool EncloseParameterInQuote
        {
            get
            {
                string value = GetInternal("EncloseParameterInQuote", false.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("EncloseParameterInQuote", value.ToString()); }
        }
        
        public string CassandraQuery
        {
            get { return GetInternal("CassandraQuery"); }
            set { SetInternal("CassandraQuery", value); }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return timeZoneHelper; }
        }
    }
}
