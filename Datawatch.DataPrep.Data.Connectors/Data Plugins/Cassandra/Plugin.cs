﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.CassandraPlugin.Properties;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.CassandraPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<CassandraSettings>
    {
        internal const string PluginId = "CassandraPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Cassandra";
        internal const string PluginType = DataPluginTypes.Database;

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override CassandraSettings CreateSettings(PropertyBag bag)
        {
            CassandraSettings cassandraSettings = new CassandraSettings(bag);
            if (string.IsNullOrEmpty(cassandraSettings.Title))
            {
                cassandraSettings.Title = Resources.UiDefaultConnectionTitle;
            }

            return cassandraSettings;
        }

        public override ITable GetData(string workbookDir,
            string dataDir, PropertyBag propertyBag,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            CheckLicense();
            CassandraSettings cassandraSettings = CreateSettings(propertyBag);
            CassandraClient client = new CassandraClient(cassandraSettings, parameters);

            StandaloneTable table = client.GetTable(rowcount);

            // We're creating a new StandaloneTable on every request, so allow
            // EX to freeze it.
            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }
    }
}
