﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cassandra;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.CassandraPlugin.Properties;
using CassandraRow = Cassandra.Row;

namespace Panopticon.CassandraPlugin
{
    public class CassandraTableBuilder
    {
        public StandaloneTable CreateTable(IEnumerable<CassandraRow> rows, CqlColumn[] columns, int maxRows, TimeZoneHelper timeZoneHelper)
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try
            {
                CreateColumns(columns, table);
                int rowNum = 0;

                foreach (CassandraRow cassandraRow in rows)
                {
                    if (rowNum == maxRows)
                        break;

                    object[] values = new object[columns.Count()];
                    for (int i = 0; i < columns.Count(); i++)
                    {
                        values[i] = GetValue(cassandraRow, columns[i], timeZoneHelper);
                    }

                    table.AddRow(values);
                    rowNum++;
                }
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        private static void CreateColumns(IEnumerable<CqlColumn> columns, StandaloneTable table)
        {
            foreach (var column in columns)
            {
                ColumnType type = GetColumnType(column.Type.Name);

                switch (type)
                {
                    case ColumnType.Numeric:
                        table.AddColumn(new NumericColumn(column.Name));
                        break;
                    case ColumnType.Text:
                        table.AddColumn(new TextColumn(column.Name));
                        break;
                    case ColumnType.Time:
                        table.AddColumn(new TimeColumn(column.Name));
                        break;
                }
            }
        }

        private static object GetValue(CassandraRow cassandraRow, CqlColumn column, TimeZoneHelper timeZoneHelper)
        {
            if (cassandraRow.IsNull(column.Name))
                return null;

            string columnTypeName = column.Type.Name;
            if (columnTypeName == "Double")
                return cassandraRow.GetValue<double>(column.Name);
            if (columnTypeName == "Int32")
                return cassandraRow.GetValue<int>(column.Name);
            if (columnTypeName == "Int64")
                return cassandraRow.GetValue<long>(column.Name);
            if (columnTypeName == "Single")
                return cassandraRow.GetValue<float>(column.Name);
            if (columnTypeName == "Decimal")
                return cassandraRow.GetValue<decimal>(column.Name);
            if (columnTypeName == "String")
                return cassandraRow.GetValue<string>(column.Name);
            if (columnTypeName == "Boolean")
                return cassandraRow.GetValue<bool>(column.Name);
            if (columnTypeName == "Guid")
                return cassandraRow.GetValue<Guid>(column.Name);
            if (columnTypeName == "DateTimeOffset")
            {
                DateTimeOffset value = cassandraRow.GetValue<DateTimeOffset>(column.Name);
                return timeZoneHelper.TimeZoneSelected ? timeZoneHelper.ConvertFromUTC(value.DateTime) : value.DateTime;
            }

            throw new NotSupportedException(string.Format(Resources.ExUnsupportedType, column.Type.Name));
        }

        private static ColumnType GetColumnType(string typeName)
        {
            HashSet<string> numericTypes = new HashSet<string>(new[] { "Int32", "Int64", "Double", "Single", "Decimal" });
            HashSet<string> textTypes = new HashSet<string>(new[] { "String", "Boolean", "Guid" });
            HashSet<string> dateTypes = new HashSet<string>(new[] { "DateTimeOffset" });

            if (numericTypes.Contains(typeName))
                return ColumnType.Numeric;
            if (textTypes.Contains(typeName))
                return ColumnType.Text;
            if (dateTypes.Contains(typeName))
                return ColumnType.Time;

            throw new NotSupportedException(string.Format(Resources.ExUnsupportedType, typeName));
        }
    }
}
