﻿using System;
using System.Collections.Generic;
using System.Text;
using Cassandra;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using CassandraRow = Cassandra.Row;
using System.Security;

namespace Panopticon.CassandraPlugin
{
    public class CassandraClient
    {
        private readonly string url;
        private readonly int port;
        private readonly string keyspace;
        private readonly string userId;
        private readonly SecureString password;
        private readonly string query;
        private readonly TimeZoneHelper timeZoneHelper;

        public CassandraClient(CassandraSettings settings, IEnumerable<ParameterValue> parameters)
        {
            string portString = settings.Port;
            if (parameters != null)
            {
                url = ApplyParameters(settings.ConnectionUrl, parameters, false);
                portString = ApplyParameters(portString, parameters, false);
                keyspace = ApplyParameters(settings.KeySpace, parameters, false);
                userId = ApplyParameters(settings.UserId, parameters, false);
                password = SecurityUtils.ToSecureString(ApplyParameters(settings.Password, parameters, false));
                query = ApplyParameters(settings.CassandraQuery, parameters, settings.EncloseParameterInQuote);
            }
            else
            {
                url = settings.ConnectionUrl;
                keyspace = settings.KeySpace;
                userId = settings.UserId;
                password = SecurityUtils.ToSecureString(settings.Password);
                query = settings.CassandraQuery;
            }

            if (!int.TryParse(portString, out port))
                throw new ArgumentException(Properties.Resources.ExInvalidPort);

            timeZoneHelper = settings.TimeZoneHelper;
        }

        public void TestConnection()
        {
            Log.Info(Properties.Resources.LogTestingConnection);
            using (Cluster cluster = CreateCluster())
                using (ISession session = cluster.Connect(keyspace))
                    using (session.Execute(string.Format("use {0}", keyspace))) { }
        }


        public string GetTableName()
        {
            Log.Info(Properties.Resources.LogRecievingDataForTableName, url, port, keyspace, query);
            using (Cluster cluster = CreateCluster())
            {
                using (ISession session = cluster.Connect(keyspace))
                {
                    using (RowSet rowSet = session.Execute(query))
                    {
                        StringBuilder sb = new StringBuilder();
                        IList<string> distinctTableNames = new List<string>();
                        string delimiter = string.Empty;
                        foreach (CqlColumn column in rowSet.Columns)
                        {
                            if (!distinctTableNames.Contains(column.Table))
                            {
                                distinctTableNames.Add(column.Table);
                                sb.AppendFormat("{0}{1}", delimiter, column.Table);
                                if (delimiter == string.Empty)
                                    delimiter = "_";
                            }
                        }

                        return sb.ToString();
                    }
                }
            }
        }

        public StandaloneTable GetTable(int maxRows)
        {
            Log.Info(Properties.Resources.LogRecievingData, url, port, keyspace, query);
            DateTime startTime = DateTime.Now;
            using (Cluster cluster = CreateCluster())
            {
                using (ISession session = cluster.Connect(keyspace))
                {
                    using (RowSet rowSet = session.Execute(query))
                    {
                        StandaloneTable table = new CassandraTableBuilder().CreateTable(rowSet.GetRows(), rowSet.Columns, maxRows, timeZoneHelper);
                        DateTime endTime = DateTime.Now;
                        Log.Info(Properties.Resources.LogRecievedData, table.RowCount, table.ColumnCount, (endTime-startTime).TotalSeconds, url, port, keyspace, query);
                        return table;
                    }
                }
            }
        }

        private string ApplyParameters(string source, IEnumerable<ParameterValue> parameters, bool encloseParameterInQuote)
        {
            if (source == null)
                return null;

            return new ParameterEncoder
            {
                SourceString = source,
                Parameters = parameters,
                NullOrEmptyString = "NULL",
                EnforceSingleQuoteEnclosure = encloseParameterInQuote,
            }.Encoded();
        }

        private Cluster CreateCluster()
        {
            if (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(SecurityUtils.ToInsecureString(password)))
                return Cluster.Builder().WithCredentials(userId, SecurityUtils.ToInsecureString(password)).WithPort(port).AddContactPoint(url).Build();

            return Cluster.Builder().AddContactPoint(url).WithPort(port).Build();
        }
    }
}
