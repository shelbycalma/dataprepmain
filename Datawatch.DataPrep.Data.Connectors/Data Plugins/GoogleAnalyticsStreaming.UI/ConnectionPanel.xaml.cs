﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.GoogleAnalyticsStreamingPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private GoogleAnalyticsSettings settings;
        private const int MAXDIMENSIONS = 7;
        private const int MAXMETRICS = 10;

        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(GoogleAnalyticsSettings settings)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            if (this.settings != null)
            {
                PasswordBox.Password = this.settings.ClientSecret;
            }
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        public GoogleAnalyticsSettings Settings
        {
            get { return settings; }
        }

        protected void OnConnectionChanged(GoogleAnalyticsSettings newConnection)
        {
            DataContext = newConnection;

            if (newConnection != null)
            {
                PasswordBox.Password = newConnection.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.ClientSecret = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as GoogleAnalyticsSettings;
        }

        private void MetricsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            settings.SelectedMetrics.Clear();

            if (MetricsListBox.SelectedItems.Count == 0)
            {
                return;
            }
            if (MetricsListBox.SelectedItems.Count > MAXMETRICS)
            {
                MessageBox.Show(Properties.Resources.ExLimitMessage,
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                string value = e.AddedItems[0].ToString();
                foreach (string item in MetricsListBox.SelectedItems)
                {
                    if (item == value)
                    {
                        MetricsListBox.SelectedItems.Remove(item);
                        return;
                    }
                }
                return;
            }

            foreach (string item in MetricsListBox.SelectedItems)
            {
                settings.SelectedMetrics.Add(item);
            }
        }

        private void DimensionListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            settings.SelectedDimensions.Clear();

            if (DimensionListBox.SelectedItems.Count == 0)
            {
                return;
            }
            if (DimensionListBox.SelectedItems.Count > MAXDIMENSIONS)
            {
                MessageBox.Show(Properties.Resources.ExLimitMessage,
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                string value = e.AddedItems[0].ToString();
                foreach (string item in DimensionListBox.SelectedItems)
                {
                    if(item == value)
                    {
                        DimensionListBox.SelectedItems.Remove(item);
                        return;
                    }
                }
                return;
            }
            foreach (string item in DimensionListBox.SelectedItems)
            {
                settings.SelectedDimensions.Add(item);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //selecting dimensions and metrics
            if (this.settings != null)
            {
                DimensionListBox.SelectionChanged -= DimensionListBox_SelectionChanged;
                MetricsListBox.SelectionChanged -= MetricsListBox_SelectionChanged;
                for(int i=0;i<DimensionListBox.Items.Count - 1;i++)
                {
                    if (settings.SelectedDimensions.Contains(DimensionListBox.Items[i].ToString()))
                    {
                        DimensionListBox.SelectedItems.Add(DimensionListBox.Items[i]);
                    }
                }
                for (int i = 0; i < MetricsListBox.Items.Count - 1; i++)
                {
                    if (settings.SelectedMetrics.Contains(MetricsListBox.Items[i].ToString()))
                    {
                        MetricsListBox.SelectedItems.Add(MetricsListBox.Items[i]);
                    }
                }
                DimensionListBox.SelectionChanged += DimensionListBox_SelectionChanged;
                MetricsListBox.SelectionChanged += MetricsListBox_SelectionChanged;
            }

        }

    }
}
