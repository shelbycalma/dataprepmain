﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.GoogleAnalyticsStreamingPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, GoogleAnalyticsSettings,
                       Dictionary<string, object>, GoogleAnalyticsDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner,
                  "pack://application:,,,/Panopticon.GoogleAnalyticsStreamingPlugin.UI;component/Google-Analytics-Logo.png")
        {
        }

        public override Window CreateConnectionWindow(GoogleAnalyticsSettings settings)
        {
            return new ConnectionWindow(settings);
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new GoogleAnalyticsSettings(Plugin.PluginManager, bag, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }
    }
}
