﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.GoogleAnalyticsStreamingPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private GoogleAnalyticsSettings settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(GoogleAnalyticsSettings settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public GoogleAnalyticsSettings Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
