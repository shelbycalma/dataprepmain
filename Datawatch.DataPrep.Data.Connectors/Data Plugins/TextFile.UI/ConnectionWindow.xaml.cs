﻿using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.TextFilePlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        private readonly TextFileSettings settings;

        /// <summary>
        /// Defines the Ok command, used by the Ok button.
        /// </summary>
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow()
            : this(null, null)
        {
        }

        public ConnectionWindow(TextFileSettings settings, PropertyBag globalSettings)
        {
            this.settings = settings;
            InitializeComponent();
            ConfigPanel.GlobalSettings = globalSettings;
        }

        /// <summary>
        /// Gets the current TextFileSettings instance.
        /// </summary>
        public TextFileSettings Settings
        {
            get
            {
                return settings;
            }

        }

        /// <summary>
        /// Checks if the Ok command can be executed. 
        /// If the ConfigPanel exists and it's IsOK property is true, the
        /// command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOK;
        }


        /// <summary>
        /// Executes the Ok command, setting the DialogResult to true.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
