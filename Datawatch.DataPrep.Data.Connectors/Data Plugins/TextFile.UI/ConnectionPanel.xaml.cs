using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.TextFilePlugin.UI
{
    /// <summary>
    /// The ConfigPanel implementa a WPF UserControl allowing editing of data 
    /// connections for the streaming DataPlugin. The ConfigPanel is used  both 
    /// when a connection is created and when edited in the data source settings
    /// dialog.
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private static readonly string FileFilter = string.Concat(Properties.Resources.UiTextFileFilter, "|*.csv;*.txt;*.tsv|", Properties.Resources.UiAllFileFilter, "|*.*");
        private readonly Window owner;
        public PropertyBag GlobalSettings { get; set; }

        /// <summary>
        /// Creates a new instance of the ConnectionPanel.
        /// </summary>
        public ConnectionPanel()
            : this(null, null)
        {
        }

        public ConnectionPanel(TextFileSettings settings, Window owner)
        {
            InitializeComponent();
            DataContext = settings;
            this.owner = owner;
            if (settings != null)
            {
                PasswordBox.Password = settings.WebPassword;
            }
        }

        /// <summary>
        /// Gets or sets the current TextFileSettings instance.
        /// </summary>
        public TextFileSettings Settings
        {
            get { return DataContext as TextFileSettings; }
        }
        
        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = DataPluginUtils.BrowseForFile(owner, Settings.FilePath,
                FileFilter, GlobalSettings);
            if (path != null)
            {
                Settings.FilePath = path;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.WebPassword = PasswordBox.Password;
            }
        }
    }
}