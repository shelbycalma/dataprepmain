﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.TextFilePlugin.UI.Properties;

namespace Panopticon.TextFilePlugin.UI
{
    public class PluginUI : TextPluginUIBase<TextFileSettings, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner, "pack://application:,,,/Panopticon.TextFilePlugin.UI;component" +
            "/connect-textfile.gif")
        {
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            TextFileSettings settings =
                new TextFileSettings(Plugin.PluginManager, bag, parameters);
            return new ConnectionPanel(settings, owner);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel conWindow = (ConnectionPanel)obj;
            return conWindow.Settings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(TextFileSettings settings)
        {
            return new ConnectionWindow(settings, Plugin.GlobalSettings);
        }

        public override string[] FileExtensions
        {
            get { return fileExtensions; }
        }

        private static readonly string[] fileExtensions = new[] { "csv", "txt" };

        public override string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Resources.UiTextFiles, fileExtensions);
            }
        }

        public override PropertyBag DoOpenFile(string filePath)
        {
            CheckLicense();

            TextFileSettings con = new TextFileSettings(Plugin.PluginManager)
            {
                FilePath = filePath,
                FilePathType = PathType.File
            };

            ConnectionWindow conWindow = new ConnectionWindow(con, Plugin.GlobalSettings);

            return DataPluginUtils.ShowDialog(owner, conWindow,
                conWindow.Settings);
        }
    }
}
