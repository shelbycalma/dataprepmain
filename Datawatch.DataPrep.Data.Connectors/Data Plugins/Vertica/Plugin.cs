﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.VerticaPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : OdbcPluginBase<VerticaConnectionSettings>
    {
        internal const string PluginId = "VerticaPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Vertica";
        internal const string PluginType = DataPluginTypes.Database;

        public override VerticaConnectionSettings GetSettings(PropertyBag properties,
            IEnumerable<ParameterValue> parameters)
        {
            return new VerticaConnectionSettings(properties, parameters);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        /// <summary>
        /// Gets the plug-in id.
        /// </summary>
        public override string Id
        {
            get { return PluginId; }
        }

        /// <summary>
        /// Gets a value indicating whether the plug-in has a valid license.
        /// </summary>
        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        /// <summary>
        /// Gets a value indicating the title of the plug-in.
        /// </summary>
        public override string Title
        {
            get { return PluginTitle; }
        }

        public override string DriverName
        {
            get { return "Vertica"; }
        }
    }
}
