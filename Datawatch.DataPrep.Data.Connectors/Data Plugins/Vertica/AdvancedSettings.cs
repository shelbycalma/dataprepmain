﻿using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.VerticaPlugin
{
    //TODO: add documentation.
    public class AdvancedSettings : OdbcAdvancedSettingsBase
    {
        public AdvancedSettings(PropertyBag settings)
            : base(settings)
        {
        }


        [OdbcAdvancedSettings("Prompt On No Password")]
        public bool PromptOnNoPassword
        {
            get { return GetInternalBool("PromptOnNoPassword", true); }
            set { SetInternalBool("PromptOnNoPassword", value); }
        }

        [OdbcAdvancedSettings("Use Windows Authentication")]
        public bool WindowsAuthentication
        {
            get { return GetInternalBool("WindowsAuthentication", false); }
            set { SetInternalBool("WindowsAuthentication", value); }
        }

        [OdbcAdvancedSettings("Autocommit")]
        public bool Autocommit
        {
            get { return GetInternalBool("Autocommit", true); }
            set { SetInternalBool("Autocommit", value); }
        }

        [OdbcAdvancedSettings("Backup Server Node")]
        public string BackupServerNode
        {
            get { return GetInternal("BackupServerNode"); }
            set { SetInternal("BackupServerNode", value); }
        }


        [OdbcAdvancedSettings("ConnectionLoadBalance")]
        public bool ConnectionLoadBalance
        {
            get { return GetInternalBool("ConnectionLoadBalance", false); }
            set { SetInternalBool("ConnectionLoadBalance", value); }
        }

        [OdbcAdvancedSettings("Address family preference",
            new string[] { "None", "Ipv4", "Ipv6" })]
        public string PreferredAddressFamily
        {
            get { return GetInternal("PreferredAddressFamily", "None"); }
            set { SetInternal("PreferredAddressFamily", value); }
        }


        [OdbcAdvancedSettings("ConnSettings")]
        public string ConnSettings
        {
            get { return GetInternal("ConnSettings"); }
            set { SetInternal("ConnSettings", value); }
        }


        [OdbcAdvancedSettings("DirectBatchInsert")]
        public bool DirectBatchInsert
        {
            get { return GetInternalBool("DirectBatchInsert", false); }
            set { SetInternalBool("DirectBatchInsert", value); }
        }

        [OdbcAdvancedSettings("DriverStringConversions",
            new string[] { "None", "Input", "Output", "Both" })]
        public string DriverStringConversions
        {
            get { return GetInternal("DriverStringConversions", "Output"); }
            set { SetInternal("DriverStringConversions", value); }
        }

        [OdbcAdvancedSettings("Locale")]
        public string Locale
        {
            get { return GetInternal("Locale", "en_US@collation=binary"); }
            set { SetInternal("Locale", value); }
        }

        [OdbcAdvancedSettings("ReadOnly")]
        public bool ReadOnly
        {
            get { return GetInternalBool("ReadOnly", false); }
            set { SetInternalBool("ReadOnly", value); }
        }

        [OdbcAdvancedSettings("ResultBufferSize")]
        public string ResultBufferSize
        {
            get { return GetInternal("ResultBufferSize", "131072"); }
            set { SetInternal("ResultBufferSize", value); }
        }

        [OdbcAdvancedSettings("Transaction Isolation",
            new string[] { "Read Commited", "Serializable", "Server Default" })]
        public string TransactionIsolation
        {
            get { return GetInternal("TransactionIsolation", "Server Default"); }
            set { SetInternal("TransactionIsolation", value); }
        }

        [OdbcAdvancedSettings("KerberosHostname")]
        public string KerberosHostname
        {
            get { return GetInternal("KerberosHostname"); }
            set { SetInternal("KerberosHostname", value); }
        }

        [OdbcAdvancedSettings("KerberosServiceName")]
        public string KerberosServiceName
        {
            get { return GetInternal("KerberosServiceName", "vertica"); }
            set { SetInternal("KerberosServiceName", value); }
        }

        [OdbcAdvancedSettings("SSLMode",
            new string[] { "Require", "Prefer", "Allow", "Disable" })]
        public string SSLMode
        {
            get { return GetInternal("SSLMode", "Prefer"); }
            set { SetInternal("SSLMode", value); }
        }

        [OdbcAdvancedSettings("SSLCertFile")]
        public string SSLCertFile
        {
            get { return GetInternal("SSLCertFile"); }
            set { SetInternal("SSLCertFile", value); }
        }

        [OdbcAdvancedSettings("SSLKeyFile")]
        public string SSLKeyFile
        {
            get { return GetInternal("SSLKeyFile"); }
            set { SetInternal("SSLKeyFile", value); }
        }

        [OdbcAdvancedSettings("ColumnsAsChar")]
        public bool ColumnsAsChar
        {
            get { return GetInternalBool("ColumnsAsChar", false); }
            set { SetInternalBool("ColumnsAsChar", value); }
        }

        [OdbcAdvancedSettings("ThreePartNaming")]
        public bool ThreePartNaming
        {
            get { return GetInternalBool("ThreePartNaming", true); }
            set { SetInternalBool("ThreePartNaming", value); }
        }
    }
}
