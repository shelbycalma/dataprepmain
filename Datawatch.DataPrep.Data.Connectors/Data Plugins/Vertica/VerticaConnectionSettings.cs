﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.VerticaPlugin
{
    public class VerticaConnectionSettings : DatabaseSettingsBase
    {
        private readonly AdvancedSettings additionalSettings;

        public VerticaConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public override OdbcAdvancedSettingsBase AdvancedSettings
        {
            get
            {
                return additionalSettings;
            }
        }

        public VerticaConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.Vertica, new OdbcExecutorBase())
        {
            additionalSettings = new AdvancedSettings(ToPropertyBag());

            //set default Port number
            Port = "5433";
        }

        public override string SystemDatabaseName
        {
            get { return ""; }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "5433");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string DriverName
        {
            get
            {
                return "Vertica";
            }
        }

        protected override string DatabaseKey
        {
            get { return "Database"; }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool FilterSchema(string schema)
        {
            List<string> schemas = base.RestrictedSchemas;
            schemas.Add("V_CATALOG");
            schemas.Add("V_MONITOR");
            return schemas.Contains(schema.ToUpper());
        }
    }
}
