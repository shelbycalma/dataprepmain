﻿namespace Panopticon.DatawatchPlugin.UI.ViewModels
{
    public class ViewTypeViewModel
    {
        public ViewType View { get; set; }

        public string Name { get; set; }
    }
}