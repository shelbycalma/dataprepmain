﻿namespace Panopticon.DatawatchPlugin.UI.ViewModels
{
    public interface IUpdatableFromModel<in TModel>
    {
        void Update(TModel model);
    }
}