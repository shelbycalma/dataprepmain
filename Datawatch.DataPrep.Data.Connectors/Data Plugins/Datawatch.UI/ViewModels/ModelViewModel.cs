﻿using System.ComponentModel;
using Panopticon.DatawatchPlugin.DatawatchService.Model;

namespace Panopticon.DatawatchPlugin.UI.ViewModels
{
    public class ModelViewModel : INotifyPropertyChanged, IUpdatableFromModel<Model>, IWithId
    {
        private int id;
        private string name;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Id
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged("Id");
            }
        }

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        public void Update(Model model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
        }

        public bool EqualId(Model model)
        {
            return this.Id == model.Id;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}