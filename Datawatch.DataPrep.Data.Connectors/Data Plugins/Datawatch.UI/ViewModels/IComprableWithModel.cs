﻿namespace Panopticon.DatawatchPlugin.UI.ViewModels
{
    public interface IComprableWithModel<in TModel>
    {
        bool EqualId(TModel model);
    }
}