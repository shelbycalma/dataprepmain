﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Panopticon.DatawatchPlugin.DatawatchService;
using Panopticon.DatawatchPlugin.DatawatchService.Model;
using Panopticon.DatawatchPlugin.UI.Properties;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;

namespace Panopticon.DatawatchPlugin.UI.ViewModels
{
    public class DatawatchSettingsViewModel : INotifyPropertyChanged
    {
        private static readonly Task CompletedTask;

        private static readonly List<DocumentTypeGroupViewModel> PersistenDocumentTypeGroups = new List
            <DocumentTypeGroupViewModel>
        {
            new DocumentTypeGroupViewModel
            {
                Id = -1,
                Name = Resources.UiDocumentTypeGroupAll
            }
        };

        private DatawatchClientAsync datawatchClient;
        private ICommand connectCommand;
		private ICommand _previewCommand;
		private DataTable _previewTable;
		private string connectButtonText;
        private string connectErrorMessage;
        private LoginResult loginResult;
        private bool isDocumentTypeGroupEnabled;
        private bool isDocumentTypeEnabled;
        private bool isSummariesEnabled;
        private bool isModelsEnabled;
        private bool isReportsEnabled;
        private bool isConnectEnabled = true;
        private bool isViewsEnabled;
        private Task<LoginResult> lastLoginTask;
        private Task<List<DocumentTypeGroup>> lastDocumentTypeGroupsTask;
        private Task<List<DocumentType>> lastDocumentTypesTask;
        private Task<List<Report>> lastReportsTask;
        private Task<List<Model>> lastModelsTask;
        private Task<ModelInfo> lastSummariesTask;
        private ParameterEncoder parameterEncoder;
        private DateTime fromReportDate;
        private DateTime toReportDate;
        private bool isFromSelected;
        private bool isToSelected;
        private bool isLogined;

        public event PropertyChangedEventHandler PropertyChanged;

        static DatawatchSettingsViewModel()
        {
            TaskCompletionSource<bool> source = new TaskCompletionSource<bool>();
            source.SetResult(true);
            CompletedTask = source.Task;
        }

        public DatawatchSettingsViewModel()
        {
            this.InitProperties(new DatawatchSettings(), null);
        }

        public DatawatchSettingsViewModel(DatawatchSettings model, IEnumerable<ParameterValue> parameters)
        {
            this.InitProperties(model, parameters);

            if (this.CanConnect())
                this.Connect();
        }

        private void InitProperties(DatawatchSettings model, IEnumerable<ParameterValue> parameters)
        {
            this.parameterEncoder = new ParameterEncoder { Parameters = parameters };
            this.Model = model;

            this.DocumentTypeGroups = new ObservableCollection<DocumentTypeGroupViewModel>(PersistenDocumentTypeGroups);
            this.DocumentTypes = new ObservableCollection<DocumentTypeViewModel>();
            this.Models = new ObservableCollection<ModelViewModel>();
            this.Reports = new ObservableCollection<ReportViewModel>();
            this.Views = new ObservableCollection<ViewTypeViewModel>
            {
                new ViewTypeViewModel
                {
                    View = ViewType.Data,
                    Name = Resources.UiViewTypeData,
                },
                new ViewTypeViewModel
                {
                    View = ViewType.Summary,
                    Name = Resources.UiViewTypeSummary,
                },
            };
            this.SelectedReports = new ObservableCollection<int>();
            this.SelectedReports.CollectionChanged += (sender, args) =>
            {
                this.ReportIds = this.SelectedReports.ToArray();
                this.OnPropertyChanged("IsModelValid");
            };
            this.Summaries = new ObservableCollection<string>();
            this.SelectedReportNames = new ObservableCollection<string>();
            this.IsLogined = false;
            this.ConnectButtonText = Resources.UiConnectButtonConnectText;
            this.ConnectErrorMessage = Resources.UiConnectStatusMessagePressConnect;

            if (model.FromDate.HasValue)
            {
                this.FromReportDate = model.FromDate.Value;
                this.IsFromSelected = true;
            }
            else
            {
                this.FromReportDate = DateTime.Now;
            }
            if (model.ToDate.HasValue)
            {
                this.ToReportDate = model.ToDate.Value;
                this.IsToSelected = true;
            }
            else
            {
                this.ToReportDate = DateTime.Now;
            }
        }

        public DatawatchSettings Model { get; private set; }

		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				OnPropertyChanged("PreviewTable");
			}
		}

		public string ClientUrl
        {
            get { return this.Model.ClientUrl; }
            set
            {
                if (this.Model.ClientUrl == value) return;
                this.Model.ClientUrl = value;
                this.OnPropertyChanged("Username");
            }
        }

        public string Username
        {
            get { return this.Model.Username; }
            set
            {
                if (this.Model.Username == value) return;
                this.Model.Username = value;
                this.OnPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return this.Model.Password; }
            set
            {
                if (this.Model.Password == value) return;
                this.Model.Password = value;
                this.OnPropertyChanged("Password");
            }
        }

        public int DocumentTypeGroupId
        {
            get { return this.Model.DocumentTypeGroupId; }
            set
            {
                if (this.Model.DocumentTypeGroupId == value) return;
                this.Model.DocumentTypeGroupId = value;
                this.UpdateDocumentTypes();
                this.OnPropertyChanged("DocumentTypeGroupId");
            }
        }

        public int DocumentTypeId
        {
            get { return this.Model.DocumentTypeId; }
            set
            {
                if (this.Model.DocumentTypeId == value) return;
                this.Model.DocumentTypeId = value;
                this.UpdateModels();
                this.UpdateReports();
                this.OnPropertyChanged("DocumentTypeId");
                this.OnPropertyChanged("IsDocumentTypeEnabled");
            }
        }

        public int ModelId
        {
            get { return this.Model.ModelId; }
            set
            {
                if (this.Model.ModelId == value) return;
                this.Model.ModelId = value;
                this.OnPropertyChanged("ModelId");
                this.UpdateSummaries();
            }
        }

        public int[] ReportIds
        {
            get { return this.Model.ReportIds; }
            set
            {
                if (this.Model.ReportIds == value) return;
                this.Model.ReportIds = value;
                this.OnPropertyChanged("ReportIds");
            }
        }

        public ViewType View
        {
            get { return this.Model.View; }
            set
            {
                if (this.Model.View == value) return;
                this.Model.View = value;
                this.OnPropertyChanged("View");
                this.OnPropertyChanged("IsSummaryNameVisible");
                if (value == ViewType.Summary)
                    UpdateSummaries();
            }
        }

        public bool IsSummaryNameVisible
        {
            get { return this.View == ViewType.Summary; }
        }

        public string SummaryName
        {
            get { return this.Model.SummaryName; }
            set
            {
                if (this.Model.SummaryName == value) return;
                this.Model.SummaryName = value;
                this.OnPropertyChanged("SummaryName");
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return (TimeZoneHelper)this.Model.TimeZoneHelper; }
        }

        public bool IsLogined
        {
            get { return this.isLogined; }
            set
            {
                if (this.isLogined == value) return;
                this.isLogined = value;
                this.OnPropertyChanged("IsLogined");
            }
        }

		public ICommand PreviewCommand
		{
			get
			{
				return _previewCommand ??
					   (_previewCommand = new DelegateCommand(PreviewData, CanPreview));
			}
		}

		public ICommand ConnectCommand
        {
            get
            {
                return this.connectCommand ??
                        (this.connectCommand = new DelegateCommand(() => this.Connect(), this.CanConnect));
            }
        }

        public string ConnectButtonText
        {
            get { return this.connectButtonText; }
            set
            {
                if (value == this.connectButtonText) return;
                this.connectButtonText = value;
                this.OnPropertyChanged("ConnectButtonText");
            }
        }

        public string ConnectErrorMessage
        {
            get { return this.connectErrorMessage; }
            set
            {
                if (value == this.connectErrorMessage) return;
                this.connectErrorMessage = value;
                this.OnPropertyChanged("ConnectErrorMessage");
                this.OnPropertyChanged("ConnectErrorForeground");
            }
        }

        public Brush ConnectErrorForeground
        {
            get
            {
                if (this.LoginResult == LoginResult.Success) return Brushes.Black;
                return Brushes.Red;
            }
        }

        protected LoginResult LoginResult
        {
            get { return this.loginResult; }
            set
            {
                this.loginResult = value;
                this.UpdateConnectStatus();
            }
        }

        public bool IsModelValid
        {
            get
            {
                bool summaryViewValid = this.View == ViewType.Summary && !string.IsNullOrEmpty(this.SummaryName);
                bool viewValid = this.View != ViewType.Summary || summaryViewValid;

                return this.CanConnect() &&
                       this.DocumentTypeId != -1 &&
                       this.SelectedReports.Count > 0 &&
                       this.ModelId != -1 &&
                       this.IsConnectEnabled &&
                       viewValid;
            }
        }

        public bool CanPreview()
        {
            bool summaryViewValid = this.View == ViewType.Summary && !string.IsNullOrEmpty(this.SummaryName);
            bool viewValid = this.View != ViewType.Summary || summaryViewValid;

            return this.CanConnect() &&
                   this.DocumentTypeId != -1 &&
                   this.SelectedReports.Count > 0 &&
                   this.ModelId != -1 &&
                   this.IsConnectEnabled &&
                   viewValid;
        }
        public DateTime FromReportDate
        {
            get { return this.fromReportDate; }
            set
            {
                if (value.Equals(this.fromReportDate)) return;
                this.fromReportDate = value;
                this.OnPropertyChanged("FromReportDate");
                this.UpdateModelFromDate();
                this.UpdateReports();
            }
        }

        public DateTime ToReportDate
        {
            get { return this.toReportDate; }
            set
            {
                if (value.Equals(this.toReportDate)) return;
                this.toReportDate = value;
                this.OnPropertyChanged("ToReportDate");
                this.UpdateModelToDate();
                this.UpdateReports();
            }
        }

        public bool IsFromSelected
        {
            get { return this.isFromSelected; }
            set
            {
                if (value.Equals(this.isFromSelected)) return;
                this.isFromSelected = value;
                this.OnPropertyChanged("IsFromSelected");
                this.UpdateModelFromDate();
                this.UpdateReports();
            }
        }

        public bool IsToSelected
        {
            get { return this.isToSelected; }
            set
            {
                if (value.Equals(this.isToSelected)) return;
                this.isToSelected = value;
                this.OnPropertyChanged("IsToSelected");
                this.UpdateModelToDate();
                this.UpdateReports();
            }
        }

        public ObservableCollection<DocumentTypeGroupViewModel> DocumentTypeGroups { get; private set; }

        public ObservableCollection<DocumentTypeViewModel> DocumentTypes { get; private set; }

        public ObservableCollection<ModelViewModel> Models { get; private set; }

        public ObservableCollection<ReportViewModel> Reports { get; private set; }

        public ObservableCollection<ViewTypeViewModel> Views { get; private set; }

        public ObservableCollection<string> Summaries { get; private set; }

        public ObservableCollection<int> SelectedReports { get; private set; }

        public ObservableCollection<string> SelectedReportNames { get; private set; }

        public bool IsDocumentTypeGroupEnabled
        {
            get { return this.isDocumentTypeGroupEnabled; }
            set
            {
                if (value.Equals(this.isDocumentTypeGroupEnabled)) return;
                this.isDocumentTypeGroupEnabled = value;
                this.OnPropertyChanged("IsDocumentTypeGroupEnabled");
            }
        }

        public bool IsDocumentTypeEnabled
        {
            get { return this.isDocumentTypeEnabled; }
            set
            {
                if (value.Equals(this.isDocumentTypeEnabled)) return;
                this.isDocumentTypeEnabled = value;
                this.OnPropertyChanged("IsDocumentTypeEnabled");
            }
        }

        public bool IsModelsEnabled
        {
            get { return this.isModelsEnabled; }
            set
            {
                if (value.Equals(this.isModelsEnabled)) return;
                this.isModelsEnabled = value;
                this.OnPropertyChanged("IsModelsEnabled");
            }
        }

        public bool IsReportsEnabled
        {
            get { return this.isReportsEnabled; }
            set
            {
                if (value.Equals(this.isReportsEnabled)) return;
                this.isReportsEnabled = value;
                this.OnPropertyChanged("IsReportsEnabled");
            }
        }

        public bool IsViewsEnabled
        {
            get { return this.isViewsEnabled; }
            set
            {
                if (value.Equals(this.isViewsEnabled)) return;
                this.isViewsEnabled = value;
                this.OnPropertyChanged("IsViewsEnabled");
            }
        }

        public bool IsSummariesEnabled
        {
            get { return this.isSummariesEnabled; }
            set
            {
                if (value.Equals(this.isSummariesEnabled)) return;
                this.isSummariesEnabled = value;
                this.OnPropertyChanged("IsSummariesEnabled");
            }
        }

        public bool IsConnectEnabled
        {
            get { return this.isConnectEnabled; }
            set
            {
                if (value.Equals(this.isConnectEnabled)) return;
                this.isConnectEnabled = value;
                this.OnPropertyChanged("IsConnectEnabled");
            }
        }

        private bool CanConnect()
        {
            return !string.IsNullOrEmpty(this.ClientUrl) &&
                   !string.IsNullOrEmpty(this.Username) &&
                   !string.IsNullOrEmpty(this.Password);
        }

        private Task Connect()
        {
            if (!this.CanConnect()) return CompletedTask;
            if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
            {
                Mouse.OverrideCursor = Cursors.Wait;
            }
            this.IsConnectEnabled = false;
            this.IsDocumentTypeGroupEnabled = false;
            this.IsDocumentTypeEnabled = false;
            this.IsModelsEnabled = false;
            this.IsReportsEnabled = false;
            this.IsViewsEnabled = false;
            this.IsSummariesEnabled = false;

            ((IParameterEncoder)this.parameterEncoder).SourceString = this.Model.ClientUrl;
            string url = ((IParameterEncoder)this.parameterEncoder).Encoded();
            this.datawatchClient = new DatawatchClientAsync(url);

            this.ConnectButtonText = Resources.UiConnectStatusMessageConnecting;

            parameterEncoder.SourceString = this.Model.Username;
            string encodedUserName = parameterEncoder.Encoded();

            parameterEncoder.SourceString = this.Model.Password;
            string encodedPassword = parameterEncoder.Encoded();
            this.lastLoginTask = this.datawatchClient.LoginAsync(encodedUserName, SecurityUtils.ToSecureString(encodedPassword));
            return this.lastLoginTask.ContinueWith(t =>
            {
                if (t != this.lastLoginTask) return;
                bool isError = IsError(t);
                if (!isError)
                {
                    this.LoginResult = t.Result;
                    isError = this.LoginResult != LoginResult.Success;
                }
                else
                {
                    this.LoginResult = LoginResult.Success;
                }
                if (isError)
                {
                    this.ConnectButtonText = Resources.UiConnectButtonConnectText;
                    if (this.LoginResult == LoginResult.Success)
                        this.ConnectErrorMessage = Resources.UiConnectStatusMessagePressConnect;
                    this.IsLogined = false;
                    this.IsConnectEnabled = true;
                    this.IsViewsEnabled = false;
                    if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                    {
                        Mouse.OverrideCursor = null;
                    }
                    return;
                }
                this.IsConnectEnabled = true;
                this.IsViewsEnabled = true;
                this.IsLogined = true;
                this.ConnectButtonText = Resources.UiConnectButtonReconnectText;

                this.UpdateDocumentTypeGroups();
                this.UpdateDocumentTypes();
                this.UpdateModels();
                this.UpdateReports();
                if (this.View == ViewType.Summary)
                {
                    this.UpdateSummaries();
                }
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void UpdateConnectStatus()
        {
            switch (this.LoginResult)
            {
                case LoginResult.Success:
                    this.ConnectErrorMessage = null;
                    break;
                case LoginResult.InvalidUserNameOrPassword:
                    this.ConnectErrorMessage = Resources.ErrorConnectStatusMessageInvalidUsernameOrPassword;
                    break;
                case LoginResult.PasswordExpired:
                    this.ConnectErrorMessage = Resources.ErrorConnectStatusMessagePasswordExpired;
                    break;
                default:
                    this.ConnectErrorMessage = string.Format(Resources.ErrorConnectStatusMessageUnknowError,
                                                             (int)this.LoginResult);
                    break;
            }
        }

        private void UpdateDocumentTypeGroups()
        {
            if (this.datawatchClient == null) return;

            this.IsDocumentTypeGroupEnabled = false;

            this.lastDocumentTypeGroupsTask = this.datawatchClient.GetDocumentTypeGroupsAsync();
            this.lastDocumentTypeGroupsTask.ContinueWith(t =>
            {
                if (t != this.lastDocumentTypeGroupsTask) return;
                if (IsError(t)) return;
                UpdateObservableCollection(this.DocumentTypeGroups, t.Result.OrderBy(e => e.Name), () => this.DocumentTypeGroupId, PersistenDocumentTypeGroups);
                this.IsDocumentTypeGroupEnabled = true;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void UpdateDocumentTypes()
        {
            if (this.datawatchClient == null) return;

            this.IsDocumentTypeEnabled = false;

            this.lastDocumentTypesTask = this.DocumentTypeGroupId != -1
                                    ? this.datawatchClient.GetDocumentTypesByDocumentTypeGroupAsync(this.DocumentTypeGroupId)
                                    : this.datawatchClient.GetDocumentTypesAsync();

            this.lastDocumentTypesTask.ContinueWith(t =>
            {
                if (t != this.lastDocumentTypesTask) return;
                if (IsError(t)) return;
                UpdateObservableCollection(this.DocumentTypes, t.Result.OrderBy(e => e.Name), () => this.DocumentTypeId);
                this.IsDocumentTypeEnabled = true;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void UpdateReports()
        {
            if (this.DocumentTypeId == -1 || this.datawatchClient == null) return;

            if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
            {
                Mouse.OverrideCursor = Cursors.Wait;
            }

            this.lastReportsTask = this.datawatchClient.GetReportsByDocumentTypeAsync(this.DocumentTypeId, this.Model.FromDate, this.Model.ToDate);

            this.IsReportsEnabled = false;

            this.lastReportsTask.ContinueWith(t =>
            {
                if (t != this.lastReportsTask) return;
                if (IsError(t)) return;
                List<Report> reports = t.Result;
                UpdateObservableCollection(this.Reports, reports);
                foreach (int selectedReportId in this.ReportIds)
                {
                    ReportViewModel reportViewModel = this.Reports.FirstOrDefault(e => e.Id == selectedReportId);
                    if (reportViewModel != null) reportViewModel.IsChecked = true;
                }
                this.UpdateSelectedReports();
                this.IsReportsEnabled = true;
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    if (Mouse.OverrideCursor == Cursors.Wait) Mouse.OverrideCursor = null;
                }
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void UpdateSelectedReports()
        {
            this.SelectedReports.Clear();
            List<int> selectedReports = this.Reports.Where(e => e.IsChecked).Select(e => e.Id).ToList();
            foreach (int selectedReport in selectedReports)
                this.SelectedReports.Add(selectedReport);

            List<string> selectedReportName = Reports.Where(e => e.IsChecked).Select(e => e.Name).ToList();
            foreach (string reportName in selectedReportName)
            {
                SelectedReportNames.Add(reportName);
                if (selectedReportName.Count >= 3)
                {
                    break;
                }
            }

            this.Model.Title = string.Join("_", SelectedReportNames);

            this.OnPropertyChanged("IsModelValid");
        }

        private void UpdateModels()
        {
            if (this.DocumentTypeId == -1 || this.datawatchClient == null) return;

            this.lastModelsTask = this.datawatchClient.GetModelsByDocumentTypeAsync(this.DocumentTypeId);

            this.IsModelsEnabled = false;

            this.lastModelsTask.ContinueWith(t =>
            {
                if (t != this.lastModelsTask) return;
                if (IsError(t)) return;
                UpdateObservableCollection(this.Models, t.Result.OrderBy(e => e.Name), () => this.ModelId);
                this.IsModelsEnabled = true;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void UpdateSummaries()
        {
            if (this.ModelId == -1 || this.View != ViewType.Summary) return;

            this.IsSummariesEnabled = false;
            this.lastSummariesTask = this.datawatchClient.GetModelInfoAsync(this.ModelId);
            this.lastSummariesTask.ContinueWith(t =>
            {
                if (t != this.lastSummariesTask) return;
                if (IsError(t)) return;
                this.Summaries.Clear();
                foreach (string summary in t.Result.Summaries)
                    this.Summaries.Add(summary);
                this.IsSummariesEnabled = true;
            }, TaskScheduler.FromCurrentSynchronizationContext());
        }

        private void UpdateModelFromDate()
        {
            this.Model.FromDate = this.IsFromSelected
                                      ? (DateTime?)this.FromReportDate
                                      : null;
        }

        private void UpdateModelToDate()
        {
            this.Model.ToDate = this.IsToSelected
                                    ? (DateTime?)this.ToReportDate
                                    : null;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            this.RaisePropertyChanged(propertyName);
            this.RaisePropertyChanged("IsModelValid");
        }

        protected virtual void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private static bool IsError(Task task)
        {
            if (task.Exception != null)
            {
                AggregateException ex = task.Exception.Flatten();
                Log.Exception(ex);
                MessageBox.Show(string.Format(Resources.ErrorBackendError, ex.InnerException.Message), null, MessageBoxButton.OK, MessageBoxImage.Error);
                return true;
            }
            return false;
        }

        private void UpdateObservableCollection<TViewModel, TModel>(ObservableCollection<TViewModel> collection, IEnumerable<TModel> models, Expression<Func<int>> propertySelected, List<TViewModel> persistentViewModels = null)
            where TViewModel : class, IUpdatableFromModel<TModel>, IWithId, new()
            where TModel : class, IWithId
        {
            PropertyInfo property = (PropertyInfo)(((MemberExpression)propertySelected.Body).Member);
            int selectedId = (int)property.GetValue(this, null);

            // Clear collection
            List<TViewModel> removedItems = collection
                // Don't try remove persisten ViewModels.
                .Where(e => persistentViewModels == null || !persistentViewModels.Contains(e))
                .ToList();

            foreach (TViewModel removedItem in removedItems)
            {
                collection.Remove(removedItem);
            }

            UpdateObservableCollection(collection, models, persistentViewModels);
            if (collection.All(e => e.Id != selectedId))
                property.SetValue(this, -1, null);
            this.OnPropertyChanged(property.Name);
        }

        private static void UpdateObservableCollection<TViewModel, TModel>(ObservableCollection<TViewModel> collection, IEnumerable<TModel> models, List<TViewModel> persistentViewModels = null)
            where TViewModel : class, IUpdatableFromModel<TModel>, IWithId, new()
            where TModel : class, IWithId
        {
            foreach (TModel model in models)
            {
                TViewModel viewModel = collection.FirstOrDefault(e => e.Id == model.Id);
                if (viewModel == null)
                {
                    viewModel = new TViewModel();
                    collection.Add(viewModel);
                }
                viewModel.Update(model);
            }

            List<TViewModel> removedItems = collection
                // Don't try remove persisten ViewModels.
                .Where(e => persistentViewModels == null || !persistentViewModels.Contains(e))
                // Select only items that is not exist in models collection
                .Where(e => models.All(m => m.Id != e.Id))
                .ToList();

            foreach (TViewModel removedItem in removedItems)
            {
                collection.Remove(removedItem);
            }
        }
		
		private PropertyBag CreatePropertyBag()
		{
			PropertyBag pb = new PropertyBag();
			PropertyValue pv = new PropertyValue("CollectionListURL", Model.ClientUrl);
			pb.Values.Add(pv);
			pv = new PropertyValue("ClientUrl", Model.ClientUrl);
			pb.Values.Add(pv);
			pv = new PropertyValue("Username", Model.Username);
			pb.Values.Add(pv);
			pv = new PropertyValue("Password", Model.Password);
			pb.Values.Add(pv);
			pv = new PropertyValue("DocumentTypeGroupId", Model.DocumentTypeGroupId.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("DocumentTypeId", Model.DocumentTypeId.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("ModelId", Model.ModelId.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("SummaryName", Model.SummaryName);
			pb.Values.Add(pv);
			pv = new PropertyValue("View", ((int)Model.View).ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("FromDate", Model.FromDate.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("ToDate", Model.ToDate.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("ReportIdCount", Model.ReportIds.Count().ToString());
			pb.Values.Add(pv);

			Model.ReportIds.Count();
			const string ReportIdKey = "ReportId_{0}";
			for (int i = 0; i < Model.ReportIds.Count(); i++)
			{
				pv = new PropertyValue(String.Format(ReportIdKey, i), Model.ReportIds[i].ToString());
				pb.Values.Add(pv);
			}

			TimeZoneHelper tzh = Model.TimeZoneHelper;
			PropertyBag timeZoneHelperBag = new PropertyBag();
			timeZoneHelperBag.Name = "TimeZone";
			pb.SubGroups.Add(timeZoneHelperBag);

			return pb;
		}

		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}

				Panopticon.DatawatchPlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);
				if (t != null)
				{
					_previewTable = PreviewDataTable.GetPreviewTable(t);
					OnPropertyChanged("PreviewTable");
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				MessageBox.Show(ex.Message, null,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

	}
}