﻿using System;
using System.ComponentModel;
using Panopticon.DatawatchPlugin.DatawatchService.Model;

namespace Panopticon.DatawatchPlugin.UI.ViewModels
{
    public class ReportViewModel : INotifyPropertyChanged, IUpdatableFromModel<Report>, IWithId
    {
        private int id;
        private string name;
        private bool isChecked;
        private DateTime fillingDate;
        private DateTime partitionDate;

        public event PropertyChangedEventHandler PropertyChanged;

        public int Id
        {
            get { return this.id; }
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged("Id");
            }
        }

        public string Name
        {
            get { return this.name; }
            set
            {
                if (value == this.name) return;
                this.name = value;
                this.OnPropertyChanged("Name");
            }
        }

        public bool IsChecked
        {
            get { return this.isChecked; }
            set
            {
                if (value.Equals(this.isChecked)) return;
                this.isChecked = value;
                this.OnPropertyChanged("IsChecked");
            }
        }

        public DateTime FillingDate
        {
            get { return this.fillingDate; }
            set
            {
                if (value.Equals(this.fillingDate)) return;
                this.fillingDate = value;
                this.OnPropertyChanged("FillingDate");
            }
        }

        public DateTime PartitionDate
        {
            get { return this.partitionDate; }
            set
            {
                if (value.Equals(this.partitionDate)) return;
                this.partitionDate = value;
                this.OnPropertyChanged("PartitionDate");
            }
        }

        public void Update(Report model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.FillingDate = model.ReportFilingDate;
            this.PartitionDate = model.PartitionDate;
        }

        public bool EqualId(Report model)
        {
            return this.Id == model.Id;
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}