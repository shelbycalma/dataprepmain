﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.DatawatchPlugin.UI.ViewModels;

namespace Panopticon.DatawatchPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, DatawatchSettings>
    {

        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            this.CheckLicense();

            DatawatchSettings datawatchSettings = new DatawatchSettings();
            DatawatchSettingsViewModel viewModel = new DatawatchSettingsViewModel(datawatchSettings, parameters);
            ConfigWindow window = new ConfigWindow(viewModel) {Owner = owner};

            if (window.ShowDialog() != true)
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    if (Mouse.OverrideCursor == Cursors.Wait) Mouse.OverrideCursor = null;
                }
                return null;
            }

            if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
            {
                if (Mouse.OverrideCursor == Cursors.Wait) Mouse.OverrideCursor = null;
            }

            if (string.IsNullOrEmpty(datawatchSettings.Title))
                datawatchSettings.Title = GetTitle(viewModel);

            return datawatchSettings.ToPropertyBag();
        }

        private static string GetTitle(DatawatchSettingsViewModel viewModel)
        {
            List<string> selectedReports = new List<string>();
            foreach (var report in viewModel.Reports)
            {
                if (report.IsChecked)
                {
                    selectedReports.Add(report.Name);
                }
                if (selectedReports.Count >= 3)
                {
                    break;
                }
            }
            return string.Join("_", selectedReports);
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            DatawatchSettings datawatchSettings = new DatawatchSettings(bag);

            if (string.IsNullOrEmpty(datawatchSettings.Title))
                datawatchSettings.Title = datawatchSettings.DocumentTypeId.ToString();

            return new ConfigPanel(new DatawatchSettingsViewModel(datawatchSettings, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;

            return configPanel.Settings.Model.ToPropertyBag();
        }
    }
}
