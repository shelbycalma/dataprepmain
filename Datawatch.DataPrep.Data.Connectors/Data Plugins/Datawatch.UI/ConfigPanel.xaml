﻿<UserControl x:Class="Panopticon.DatawatchPlugin.UI.ConfigPanel"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
             xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
             help:DataHelpProvider.HelpKey="DatawatchServerContent"
             xmlns:ViewModels="clr-namespace:Panopticon.DatawatchPlugin.UI.ViewModels"
             xmlns:DateTimePicker="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Widgets.DateTimePicker;assembly=Datawatch.DataPrep.Data.Core.UI"
             xmlns:Properties="clr-namespace:Panopticon.DatawatchPlugin.UI.Properties"
             xmlns:converters="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Converters;assembly=Datawatch.DataPrep.Data.Core.UI"
             xmlns:help="clr-namespace:Datawatch.DataPrep.Data.Core.Help;assembly=Datawatch.DataPrep.Data.Core"
             mc:Ignorable="d"
             d:DesignHeight="300" d:DesignWidth="440" 
             d:DataContext="{d:DesignInstance Type=ViewModels:DatawatchSettingsViewModel, IsDesignTimeCreatable=True}"
             HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
             xmlns:Behaviors="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Behaviors;assembly=Datawatch.DataPrep.Data.Core.UI"
             Behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme">
    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/ResourceDictionaries/CommonStyles.xaml"/>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/Themes/Generic.xaml"/>
            </ResourceDictionary.MergedDictionaries>
            <BooleanToVisibilityConverter x:Key="boolToVisibleConverter"/>
            <converters:TimeZoneConverter x:Key="TimeZoneConverter" />
            <Style BasedOn="{StaticResource CommonComboBoxStyle}" TargetType="{x:Type ComboBox}"/>
            <Style BasedOn="{StaticResource CommonCheckBoxStyle}" TargetType="{x:Type CheckBox}" />
            <Style BasedOn="{StaticResource CommonPasswordBoxStyle}" TargetType="{x:Type PasswordBox}"/>
            <Style BasedOn="{StaticResource CommonLabelStyle}" TargetType="{x:Type Label}" />
            <Style BasedOn="{StaticResource CommonTextBoxStyle}" TargetType="{x:Type TextBox}"/>
            <Style TargetType="{x:Type TextBlock}">
                <Setter Property="VerticalAlignment" Value="Center" />
                <Setter Property="HorizontalAlignment" Value="Left" />
                <Setter Property="Margin" Value="10 5 5 5" />
            </Style>
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid Margin="5" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" Grid.IsSharedSizeScope="True" Behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*" />
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*" />
            <ColumnDefinition Width="Auto" />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
			<RowDefinition Height="Auto"/>
			<RowDefinition Height="Auto"/>
			<RowDefinition Height="*"/>
        </Grid.RowDefinitions>

        <TextBlock Grid.Row="0" Grid.Column="0" Text="{x:Static Properties:Resources.UiClientUrl}" Margin="5" TextAlignment="Right"/>
        <TextBox   Grid.Row="0" Grid.Column="1" Grid.ColumnSpan="3" Margin="5" Text="{Binding ClientUrl, UpdateSourceTrigger=PropertyChanged}" GotFocus="ConnectionElement_OnGotFocus" LostFocus="ConnectionElement_OnLostFocus"/>

        <TextBlock Grid.Row="1" Grid.Column="0" Text="{x:Static Properties:Resources.UiUsername}" Margin="5" TextAlignment="Right"/>
        <TextBox   Grid.Row="1" Grid.Column="1" Margin="5" Text="{Binding Username, UpdateSourceTrigger=PropertyChanged}" GotFocus="ConnectionElement_OnGotFocus" LostFocus="ConnectionElement_OnLostFocus"/>

        <TextBlock   Grid.Row="1" Grid.Column="2" Margin="5" Text="{x:Static Properties:Resources.UiPassword}" TextAlignment="Right"/>
        <PasswordBox Grid.Row="1" Grid.Column="3" Margin="5" Name="passwordBox" PasswordChanged="PasswordBox_OnPasswordChanged" GotFocus="ConnectionElement_OnGotFocus" LostFocus="ConnectionElement_OnLostFocus"/>

        <Button Grid.RowSpan="2" Grid.Column="4" Margin="5" VerticalAlignment="Stretch"
                Content="{Binding ConnectButtonText}" Command="{Binding ConnectCommand}" IsEnabled="{Binding IsConnectEnabled}" x:Name="connectButton"/>

        <TextBlock Grid.Row="2" Grid.Column="1" Grid.ColumnSpan="5" 
            Margin="5" FontWeight="Bold" 
            HorizontalAlignment="Left" VerticalAlignment="Top"
                   Text="{Binding ConnectErrorMessage}" Foreground="{Binding ConnectErrorForeground}" />

        <Grid Grid.Row="3" Grid.ColumnSpan="5" Visibility="{Binding IsLogined, Converter={StaticResource boolToVisibleConverter}}" IsSharedSizeScope="True">
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto" SharedSizeGroup="Title"/>
                <ColumnDefinition Width="*"/>
            </Grid.ColumnDefinitions>

            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="Auto"/>
				<RowDefinition Height="Auto"/>
				<RowDefinition Height="*"/>
			</Grid.RowDefinitions>

            <TextBlock Grid.Row="0" Grid.Column="0" Text="{x:Static Properties:Resources.UiDocumentTypeGroup}" Margin="5"/>
            <ComboBox  Grid.Row="0" Grid.Column="1" Margin="5" ItemsSource="{Binding DocumentTypeGroups}" SelectedValuePath="Id" DisplayMemberPath="Name" SelectedValue="{Binding DocumentTypeGroupId}" IsSynchronizedWithCurrentItem="True"
                       IsEnabled="{Binding IsDocumentTypeGroupEnabled}"/>

            <TextBlock Grid.Row="1" Grid.Column="0" Margin="5" Text="{x:Static Properties:Resources.UiDocumentType}" />
            <ComboBox  Grid.Row="1" Grid.Column="1" Margin="5" ItemsSource="{Binding DocumentTypes}" SelectedValuePath="Id" DisplayMemberPath="Name" SelectedValue="{Binding DocumentTypeId}" IsSynchronizedWithCurrentItem="True"
                       IsEnabled="{Binding IsDocumentTypeEnabled}"/>

            <TextBlock Grid.Row="2" Grid.Column="0" Margin="5" Text="{x:Static Properties:Resources.UiModel}" />
            <ComboBox  Grid.Row="2" Grid.Column="1" Margin="5" ItemsSource="{Binding Models}" SelectedValuePath="Id" DisplayMemberPath="Name" SelectedValue="{Binding ModelId}" IsSynchronizedWithCurrentItem="True"
                       IsEnabled="{Binding IsModelsEnabled}"/>

            <Grid  Grid.Row="3" Grid.Column="0" Grid.ColumnSpan="2">
                <Grid.ColumnDefinitions>
                    <ColumnDefinition Width="Auto" SharedSizeGroup="Title"/>
                    <ColumnDefinition Width="Auto"/>
                    <ColumnDefinition Width="Auto"/>
                </Grid.ColumnDefinitions>

                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto"/>
                    <RowDefinition Height="Auto"/>
                </Grid.RowDefinitions>

                <TextBlock Grid.Column="0" Grid.RowSpan="2" Margin="5" VerticalAlignment="Center" Text="{x:Static Properties:Resources.UiReportDate}" />
                <StackPanel Grid.Row="0" Grid.Column="1" Orientation="Horizontal">
                    <CheckBox Margin="5" VerticalAlignment="Center" IsChecked="{Binding IsFromSelected}"/>
                    <TextBlock Margin="5" Text="{x:Static Properties:Resources.UiFrom}" HorizontalAlignment="Right" VerticalAlignment="Center"/>
                </StackPanel>
                <DateTimePicker:DateTimePicker Name="fromDateTimePicker" Grid.Row="0" Grid.Column="2" Margin="5" Background="White" FormatString="M/d/yyyy" DateTime="{Binding FromReportDate}" IsEnabled="{Binding IsFromSelected}"/>
                <StackPanel Grid.Row="1" Grid.Column="1" Orientation="Horizontal">
                    <CheckBox Margin="5" VerticalAlignment="Center" IsChecked="{Binding IsToSelected}"/>
                    <TextBlock Margin="5" Text="{x:Static Properties:Resources.UiTo}" HorizontalAlignment="Right" VerticalAlignment="Center"/>
                </StackPanel>
                <DateTimePicker:DateTimePicker Name="toDateTimePicker" Grid.Row="1" Grid.Column="2" Margin="5" Background="White" FormatString="M/d/yyyy" DateTime="{Binding ToReportDate}" IsEnabled="{Binding IsToSelected}"/>
            </Grid>

            <TextBlock Grid.Row="4" Grid.Column="0" Margin="5" Text="{x:Static Properties:Resources.UiReport}" VerticalAlignment="Top" />

            <ScrollViewer Grid.Row="4" Grid.Column="1" ScrollViewer.VerticalScrollBarVisibility="Auto">
                <DataGrid MinHeight="100"  MaxHeight="200" Margin="5" ItemsSource="{Binding Reports}" IsEnabled="{Binding IsReportsEnabled}" SelectionChanged="ReportList_OnSelectionChanged"
                      SelectionMode="Extended" SelectionUnit="FullRow" IsReadOnly="True" AutoGenerateColumns="False" VerticalAlignment="Top">
                    <DataGrid.Columns>
                        <DataGridTextColumn Header="{x:Static Properties:Resources.UiReportListColumnName}"          IsReadOnly="True" Width="*" 
                                        Binding="{Binding Name}" />
                        <DataGridTextColumn Header="{x:Static Properties:Resources.UiReportListColumnDocumentDate}" IsReadOnly="True" Width="Auto"
                                        Binding="{Binding PartitionDate}" />
                    </DataGrid.Columns>
                    <DataGrid.CellStyle>
                        <Style TargetType="DataGridCell">
                            <Setter Property="BorderThickness" Value="0"/>
                        </Style>
                    </DataGrid.CellStyle>
                    <DataGrid.RowStyle>
                        <Style TargetType="DataGridRow">
                            <Setter Property="IsSelected" Value="{Binding IsChecked}"/>
                        </Style>
                    </DataGrid.RowStyle>
                </DataGrid>
            </ScrollViewer>

            <TextBlock Grid.Row="5" Grid.Column="0" Margin="5" Text="{x:Static Properties:Resources.UiView}" />
            <ComboBox  Grid.Row="5" Grid.Column="1" Margin="5" ItemsSource="{Binding Views}" SelectedValuePath="View" DisplayMemberPath="Name" SelectedValue="{Binding View}" 
                       IsEnabled="{Binding IsViewsEnabled}" IsSynchronizedWithCurrentItem="True"/>

            <TextBlock Grid.Row="6" Grid.Column="0" Margin="5" Text="{x:Static Properties:Resources.UiSummaryName}" Visibility="{Binding IsSummaryNameVisible, Converter={StaticResource boolToVisibleConverter}}" VerticalAlignment="Top"/>
            <ComboBox  Grid.Row="6" Grid.Column="1" Margin="5" Visibility="{Binding IsSummaryNameVisible, Converter={StaticResource boolToVisibleConverter}}"
                        ItemsSource="{Binding Summaries}" SelectedValue="{Binding SummaryName}" IsSynchronizedWithCurrentItem="True" IsEnabled="{Binding IsSummariesEnabled}" VerticalAlignment="Top"/>

            <Label Grid.Row="7" Grid.Column="0" Content="{x:Static Properties:Resources.UiTimezoneLabel}" VerticalAlignment="Top"/>
            <ComboBox Grid.Row="7" Grid.Column="1" Margin="5"
                      ItemsSource="{Binding TimeZoneHelper.TimeZones}"
                      SelectedItem="{Binding TimeZoneHelper.SelectedTimeZone}"
                      IsSynchronizedWithCurrentItem="True" VerticalAlignment="Top"/>
			<Grid Grid.Row="8" Grid.Column="0" Grid.ColumnSpan="2" VerticalAlignment="Top">
				<Grid.RowDefinitions>
					<RowDefinition Height="*" />
					<RowDefinition Height="*" />
				</Grid.RowDefinitions>
				<Button Content="{x:Static Properties:Resources.UiPreviewData}" Grid.Row="0" MinWidth="100" MaxWidth="150" Height="20" HorizontalAlignment="Left" x:Name="PreviewData"
                                Command="{Binding PreviewCommand}" VerticalAlignment="Top"/>
				<DataGrid Behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme"
                            Grid.Row="1" MinHeight="120" MaxHeight="255" Width="Auto" Name="PreviewGrid"
                            AutoGenerateColumns="True"
                            AutoGeneratingColumn="PreviewGrid_AutoGeneratingColumn"
                            ScrollViewer.CanContentScroll="True"
                            ScrollViewer.HorizontalScrollBarVisibility="Auto"
                            ScrollViewer.VerticalScrollBarVisibility="Auto"
                            CanUserAddRows="false" IsReadOnly="True"
                            ItemsSource="{Binding PreviewTable, Mode=TwoWay,
                                UpdateSourceTrigger=PropertyChanged}" VerticalAlignment="Top">
				</DataGrid>
			</Grid>
		</Grid>
	</Grid>
</UserControl>
