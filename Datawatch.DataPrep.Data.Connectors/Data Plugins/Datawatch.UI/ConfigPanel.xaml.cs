using System;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.UI.Widgets;
using Panopticon.DatawatchPlugin.DatawatchService.Model;
using Panopticon.DatawatchPlugin.UI.ViewModels;

namespace Panopticon.DatawatchPlugin.UI
{
    public partial class ConfigPanel
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                                        typeof (DatawatchSettingsViewModel), typeof (ConfigPanel),
                                        new PropertyMetadata(Settings_Changed));

        private static void Settings_Changed(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ConfigPanel configPanel = (ConfigPanel)d;
            configPanel.OnSettingsChanged((DatawatchSettingsViewModel)e.OldValue, (DatawatchSettingsViewModel)e.NewValue);
        }

        public ConfigPanel()
        {
            this.InitializeComponent();
            DateTimeInterval interval = new DateTimeInterval(new DateTime(1900, 1, 1), new DateTime(2099, 1, 1));
            this.fromDateTimePicker.EnabledInterval = interval;
            this.toDateTimePicker.EnabledInterval = interval;
        }

        public ConfigPanel(DatawatchSettingsViewModel settings) : this()
        {
            this.Settings = settings;
        }

        public DatawatchSettingsViewModel Settings
        {
            get { return (DatawatchSettingsViewModel)this.GetValue(SettingsProperty); }
            set { this.SetValue(SettingsProperty, value); }
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.Settings == null) return;
            this.Settings.Password = this.passwordBox.Password;
        }

        private void OnSettingsChanged(DatawatchSettingsViewModel oldValue, DatawatchSettingsViewModel newValue)
        {
            this.DataContext = newValue;
            this.passwordBox.Password = newValue != null
                                            ? newValue.Password
                                            : null;
        }

        private void ConnectionElement_OnGotFocus(object sender, RoutedEventArgs e)
        {
            this.connectButton.IsDefault = true;
        }

        private void ConnectionElement_OnLostFocus(object sender, RoutedEventArgs e)
        {
            this.connectButton.IsDefault = false;
        }

        private void ReportList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (object addedItem in e.AddedItems)
            {
                int id = ((ReportViewModel)addedItem).Id;
                if (!this.Settings.SelectedReports.Contains(id))
                    this.Settings.SelectedReports.Add(id);
                string reportname = ((ReportViewModel) addedItem).Name;
                if(!this.Settings.SelectedReportNames.Contains(reportname))
                    this.Settings.SelectedReportNames.Add(reportname);
            }

            foreach (object removedItem in e.RemovedItems)
            {
                this.Settings.SelectedReports.Remove(((ReportViewModel)removedItem).Id);
                this.Settings.SelectedReportNames.Remove(((ReportViewModel) removedItem).Name);
            }
                this.Settings.Model.Title = string.Join("_", this.Settings.SelectedReportNames);
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}
