﻿using Panopticon.DatawatchPlugin.UI.ViewModels;

namespace Panopticon.DatawatchPlugin.UI
{
    public partial class ConfigWindow
    {
        public ConfigWindow(DatawatchSettingsViewModel settings)
        {
            InitializeComponent();

            this.configPanel.Settings = settings;
        }

        private void OkButton_OnClick(object sender, System.Windows.RoutedEventArgs e)
        {
            this.DialogResult = true;

            this.Close();
        }

        private void CancelButton_OnClick(object sender, System.Windows.RoutedEventArgs e)
        {
            this.DialogResult = false;

            this.Close();
        }
    }
}
