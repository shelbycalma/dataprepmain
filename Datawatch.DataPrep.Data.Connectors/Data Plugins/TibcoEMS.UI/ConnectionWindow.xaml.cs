﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.TibcoEMSPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private TibcoEMSSettings settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(TibcoEMSSettings settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public TibcoEMSSettings Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
