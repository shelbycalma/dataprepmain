﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.TibcoEMSPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, TibcoEMSSettings,
        Dictionary<string, object>, TibcoEMSDataAdapter, Plugin>
    {
        private const string imageUri = "pack://application:,,,/Panopticon.TibcoEMSPlugin.UI;component"
                                        + "/tibcoems-small.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner, imageUri)
        {
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new TibcoEMSSettings(Plugin.PluginManager, bag));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(TibcoEMSSettings settings)
        {
            return new ConnectionWindow(settings);
        }
    }
}

