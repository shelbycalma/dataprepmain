﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.TibcoEMSPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private TibcoEMSSettings settings;

        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(TibcoEMSSettings settings)
        {
            InitializeComponent();
            DataContext = this.Settings = settings;
            DataContextChanged +=
                UserControl_DataContextChanged;
        }

        public TibcoEMSSettings Settings
        {
            get { return settings; }
            private set
            {
                settings = value;
                if (settings != null)
                {
                    PasswordBox.Password = settings.Password;
                }
                else
                {
                    PasswordBox.Password = null;
                }
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            Settings = DataContext as TibcoEMSSettings;
        }

    }
}
