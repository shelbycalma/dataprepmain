﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.StaticOSISoftPlugin.UI
{
    public partial class ConfigWindow
    {
        public StaticOSISoftViewModel ViewModel { get; set; }
        public StaticOSISoftSettings OsiSettings { get; set; }


        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof (ConfigWindow));

        public ConfigWindow(
            StaticOSISoftSettings osiSettings, StaticOSISoftViewModel osiView)
        {
            osiView.OsiSettings = osiSettings;
            ViewModel = osiView;
            OsiSettings = osiSettings;

            InitializeComponent();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ViewModel.IsOk();
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ClickOK(object sender, RoutedEventArgs e)
        {
            new RoutedCommand("Ok", typeof (ConfigWindow));
        }
    }
}