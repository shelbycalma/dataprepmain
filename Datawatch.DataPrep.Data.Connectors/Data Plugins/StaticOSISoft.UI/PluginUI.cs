﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.StaticOSISoftPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, StaticOSISoftSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            StaticOSISoftSettings osiSettings
                = new StaticOSISoftSettings(Plugin.PluginManager, new PropertyBag(), parameters);

            StaticOSISoftViewModel viewModel
                = new StaticOSISoftViewModel(osiSettings,
                    new StaticOSISoftHelper(osiSettings, null,
                        new MessageBoxBasedErrorReportingService()));

            ConfigWindow window
                = new ConfigWindow(osiSettings, viewModel);
            window.Owner = owner;
            
            bool? dialogResult = window.ShowDialog();

            viewModel.OsiHelper.Disconnect();
            return dialogResult != true
                ? null
                : viewModel.OsiSettings.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return
                new ConfigPanel(new StaticOSISoftSettings(Plugin.PluginManager, bag,
                    parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            return cp.OsiSettings.ToPropertyBag();
        }
    }
}
