﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Plugin;
using Panopticon.StaticOSISoftPlugin.Wrapper_Classes;

namespace Panopticon.StaticOSISoftPlugin.UI
{
    public partial class ConfigPanel
    {
        #region Private Variables

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                typeof (StaticOSISoftViewModel),
                typeof (ConfigPanel), new PropertyMetadata(SettingsChanged));

        #endregion

        #region Constructors

        public ConfigPanel()
            : this(null)
        {
        }

        public ConfigPanel(StaticOSISoftSettings settings)
        {
            InitializeComponent();

            ReconstituteSettings(settings);

            OsiSettings = settings;
        }

        #endregion

        #region Properties

        public StaticOSISoftViewModel ViewModel
        {
            get { return (StaticOSISoftViewModel) GetValue(ViewModelProperty); }
            set
            {
                SetValue(ViewModelProperty, value);
                OsiSettings = value.OsiSettings;
            }
        }

        public StaticOSISoftSettings OsiSettings { get; set; }

        public StaticOSISoftHelper OsiHelper
        {
            get { return ViewModel.OsiHelper; }
        }

        #endregion

        #region General Methods

        private static void SettingsChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (StaticOSISoftViewModel) args.OldValue,
                (StaticOSISoftViewModel) args.NewValue);
            ((ConfigPanel) obj).OsiSettings =
                ((StaticOSISoftViewModel) args.NewValue).OsiSettings;
        }

        protected void OnSettingsChanged(
            StaticOSISoftViewModel oldModel, StaticOSISoftViewModel newModel)
        {
            DataContext = newModel;
        }

        private void ReconstituteSettings(StaticOSISoftSettings settings)
        {
            if (ViewModel == null && settings != null)
            {
                ViewModel = new StaticOSISoftViewModel(settings,
                    new StaticOSISoftHelper(settings, null, 
                        new MessageBoxBasedErrorReportingService()));

                ViewModel.InitializeDatabases();
                ViewModel.InitializeTreeView();
                ViewModel.InitializeAttributeList();
                PopulateAttributeSelections();
                PopulatePasswordBox();
            }
        }

        private void ConnectOnClick(object sender, RoutedEventArgs e)
        {
            OsiSettings.IsConnected = OsiHelper.Connect();
            if (OsiSettings.IsConnected)
                ViewModel.InitializeDatabases();
        }

        private void PopulatePasswordBox()
        {
            PasswordBox.Password = OsiSettings.Password;
        }

        private void PopulateAttributeSelections()
        {
            List<string> attrNames = new List<string>(
                OsiSettings.Attributes.Split(new[] {'\n'},
                    StringSplitOptions.RemoveEmptyEntries));

            AttributeListBox.SelectedItems.Clear();

            foreach (string attrListItem in ViewModel.AttributeList)
            {
                if (attrNames.Contains(attrListItem))
                    AttributeListBox.SelectedItems.Add(attrListItem);
            }
        }

        private void LoadAttributesOnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.SerializeNodeSelection();
            ViewModel.InitializeAttributeList();
            PopulateAttributeSelections();
        }

        private void OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            OsiSettings.Password = PasswordBox.Password;
        }

        private void OnDatabaseChanged(
            object sender, SelectionChangedEventArgs selectionChangedEventArgs)
        {
            ViewModel.InitializeTreeView();
        }

        private void OnAttributeChanged(
            object sender, SelectionChangedEventArgs e)
        {
            List<string> selectedAttrs = new List<string>();
            foreach (var attrName in AttributeListBox.SelectedItems)
            {
                selectedAttrs.Add(attrName.ToString());
            }
            OsiSettings.Attributes = String.Join("\n", selectedAttrs);
        }

        private void OnTreeSelectionChanged(object sender, RoutedEventArgs e)
        {
            ViewModel.ClearAttributeSelection();
            OsiHelper.ElementSelectionChanged();
        }

        private void ImportTreeSelectionsOnClick(
            object sender, RoutedEventArgs e)
        {
            ViewModel.SerializeNodeSelection();
            OsiSettings.ManualElementPaths = OsiSettings.ElementPaths;
            OsiSettings.ManualAttributes = OsiSettings.Attributes;
        }

        private void OnQueryTypeChanged(
            object sender, SelectionChangedEventArgs e)
        {
            OsiSettings.IsElementsQuery 
                = (OsiSettings.QueryType == "Element");
            OsiSettings.IsEventFrameQuery 
                = (OsiSettings.QueryType == "Event Frame");
            OsiSettings.IsHistoricQuery 
                = (OsiSettings.QueryType == "Historic");
        }

        private void EventFrameSearchOnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.InitializeEventFrameTable();
        }

        private void OnEventFrameChanged(
            object sender, SelectionChangedEventArgs e)
        {
            if (EventFrameDataGrid.SelectedItem != null)
                OsiSettings.EventFrameGuid =
                    ((EventFrameGridItem) EventFrameDataGrid.SelectedItem).Guid;
        }

        #endregion
    }
}