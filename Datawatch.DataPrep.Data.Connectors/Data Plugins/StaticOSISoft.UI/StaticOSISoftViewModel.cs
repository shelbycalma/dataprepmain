﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.EventFrame;
using OSIsoft.AF.Time;
using Panopticon.StaticOSISoftPlugin.Wrapper_Classes;

namespace Panopticon.StaticOSISoftPlugin.UI
{
    public class StaticOSISoftViewModel : ViewModelBase
    {
        #region Private Variables

        private StaticOSISoftSettings settings;

        #endregion

        #region Constructors

        public StaticOSISoftViewModel(
            StaticOSISoftSettings osiSettings, StaticOSISoftHelper osiHelper)
        {
            this.OsiSettings = osiSettings;

            OsiHelper = osiHelper 
                ?? new StaticOSISoftHelper(osiSettings, null, 
                    new MessageBoxBasedErrorReportingService());

            if (!OsiHelper.IsConnected)
                OsiHelper.Connect();

            InitializeSystems();
        }

        #endregion

        #region Properties

        public StaticOSISoftSettings OsiSettings
        {
            get { return settings; }
            set
            {
                settings = value;
                OnPropertyChanged("OsiSettings");
            }
        }

        public StaticOSISoftHelper OsiHelper { get; set; }

        public List<string> AvailableSystems { get; set; }
        public List<string> AvailableDatabases { get; set; }
        public List<string> AttributeList { get; set; }
        public ObservableCollection<Node> NodeCollection { get; set; }

        public ObservableCollection<EventFrameGridItem>
            EventFrameCollection { get; set; }

        public static string[] PointTypes
        {
            get { return new[] {"Recorded Points", "Plotted Values"}; }
        }

        public static string[] QueryTypes
        {
            get { return new[] {"Element", "Historic", "Event Frame"}; }
        }

        public string SearchEventFrameName { get; set; }
        public string SearchElementName { get; set; }

        public string SearchStartTime { get; set; }
        public string SearchEndTime { get; set; }

        #endregion

        public bool IsOk()
        {
            if (OsiSettings.IsTreeSelectionMode)
            {
                if (OsiSettings.IsHistoricQuery)
                {
                    return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.ElementPaths) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.Attributes) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.PointText);
                }

                if (OsiSettings.IsElementsQuery)
                {
                    return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.ElementPaths) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.Attributes);
                }

                //IsEventFrameQuery = true
                return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.EventFrameGuid) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.PointText);
            }
            
                //Manual Selction Mode
            else
            {
                if (OsiSettings.IsHistoricQuery)
                {
                    return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                           !String.IsNullOrWhiteSpace(OsiSettings.ElementPaths) &&
                           !String.IsNullOrWhiteSpace(
                               OsiSettings.ManualElementPaths) &&
                           !String.IsNullOrWhiteSpace(
                               OsiSettings.ManualAttributes);
                }

                //IsElementsQuery                   
                return !String.IsNullOrWhiteSpace(OsiSettings.SystemName) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.Database) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.ManualElementPaths) &&
                       !String.IsNullOrWhiteSpace(OsiSettings.ManualAttributes);
            }
        }

        public void InitializeSystems()
        {
            List<string> systemNames = new List<string>();

            foreach (PISystem system in OsiHelper.Systems)
            {
                systemNames.Add(system.Name);
            }

            AvailableSystems = systemNames;
            OsiSettings.SystemName = OsiSettings.SystemName 
                ?? OsiHelper.Systems.DefaultPISystem.Name;
            OnPropertyChanged("AvailableSystems");
        }

        public void InitializeDatabases()
        {
            List<string> databaseNames = new List<string>();

            foreach (AFDatabase db in OsiHelper.System.Databases)
            {
                databaseNames.Add(db.Name);
            }

            AvailableDatabases = databaseNames;
            OsiSettings.Database = OsiSettings.Database 
                ?? OsiHelper.System.Databases.DefaultDatabase.Name;      
            OnPropertyChanged("AvailableDatabases");
        }

        public void InitializeTreeView()
        {
            if (!String.IsNullOrWhiteSpace(OsiSettings.Database)
                && !String.IsNullOrWhiteSpace(settings.ElementPaths))
            {
                NodeCollection = CreateElementTree(OsiHelper.Database.Elements);
                NodeCollection = PathStringToNodeSelection();
            }
            else if (String.IsNullOrWhiteSpace(settings.ElementPaths))
            {
                NodeCollection = CreateElementTree(OsiHelper.Database.Elements);
            }
            else if (!String.IsNullOrWhiteSpace(settings.ElementPaths))
            {
                NodeCollection = CreateElementTree(OsiHelper.Database.Elements);
            }
            //If null database then nothing should happen

            OnPropertyChanged("NodeCollection");
        }

        public void SerializeNodeSelection()
        {
            if (NodeCollection == null) return;

            //save level information
            OsiSettings.DeepestLevelNumber
                = CalculateTreeLevels(NodeCollection);

            //Save the element paths in settings.
            List<string> paths = new List<string>();
            foreach (Node node in NodeCollection)
            {
                foreach (string path in node.GetPathList())
                {
                    if (!paths.Contains(path))
                        paths.Add(path);
                }
            }
            OsiSettings.ElementPaths = String.Join("\n", paths);
        }

        public void InitializeAttributeList()
        {
            List<string> attrNameList = new List<string>();
            AFElementTemplate lastSeen = new AFElementTemplate();

            foreach (AFElement ele in OsiHelper.SelectedElements.Values)
            {
                if (ele.Template == lastSeen) continue;

                lastSeen = ele.Template;

                foreach (AFAttribute attr in ele.Attributes)
                {
                    if (!attrNameList.Contains(attr.Name))
                        attrNameList.Add(attr.Name);
                }
            }

            AttributeList = attrNameList;
            OnPropertyChanged("AttributeList");
        }

        public void InitializeEventFrameTable()
        {
            AFNamedCollectionList<AFEventFrame> evFrames
                =
                (AFNamedCollectionList<AFEventFrame>)
                    OsiHelper.SearchEventFrames(SearchElementName,
                        SearchEventFrameName, SearchStartTime, SearchEndTime);

            ObservableCollection<EventFrameGridItem> tableCollection
                = new ObservableCollection<EventFrameGridItem>();

            foreach (AFEventFrame evFrame in evFrames)
            {
                if (evFrame == null) continue;

                AFTimeRange range
                    = new AFTimeRange(evFrame.StartTime, evFrame.EndTime);

                EventFrameGridItem rowItem = new EventFrameGridItem()
                {
                    Name = evFrame.Name,
                    Duration = range.Span.ToString(),
                    StartTime = evFrame.StartTime.ToString(),
                    EndTime = evFrame.EndTime.ToString(),
                    Guid = evFrame.ID.ToString()
                };

                if (evFrame.Template != null)
                    rowItem.Template = evFrame.Template.Name;

                if (evFrame.PrimaryReferencedElement != null)
                    rowItem.PrimaryElement
                        = evFrame.PrimaryReferencedElement.Name;

                tableCollection.Add(rowItem);
            }

            EventFrameCollection = tableCollection;
            OnPropertyChanged("EventFrameCollection");
        }

        public void ClearAttributeSelection()
        {
            AttributeList = new List<string>();
            OnPropertyChanged("AttributeList");
        }

        public ObservableCollection<Node> CreateElementTree(
            AFElements eles,
            Action nodeSelectionChangedAction = null,
            HashSet<AFElement> encounteredElements = null)
        {
            if (encounteredElements == null)
                encounteredElements = new HashSet<AFElement>();
            ObservableCollection<Node> nodes = new ObservableCollection<Node>();

            //base case: no children
            foreach (AFElement ele in eles)
            {
                if (!encounteredElements.Contains(ele))
                {
                    //add ele node 
                    Node eleNode = new Node(ele.Name, nodeSelectionChangedAction);
                    nodes.Add(eleNode);

                    //recurse and add ele children
                    if (ele.HasChildren)
                    {
                        HashSet<AFElement> pathElements =
                            new HashSet<AFElement>(encounteredElements) {ele};
                        //recursive step
                        ObservableCollection<Node> children =
                            CreateElementTree(ele.Elements,
                                encounteredElements: pathElements);
                        foreach (Node eleChild in children)
                        {
                            eleNode.AddNode(eleChild);
                        }
                    }
                }
            }
            return nodes;
        }

        public ObservableCollection<Node> PathStringToNodeSelection()
        {
            string[] pathList = OsiSettings.ElementPaths.Split(
                new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

            if (NodeCollection == null)
                NodeCollection = CreateElementTree(OsiHelper.Database.Elements);

            //parse each path(relative to db)
            foreach (string pathStr in pathList)
            {
                //temp variables to hold nodes/names we are looking at
                string currNodeName = "";
                ObservableCollection<Node> currCollection = NodeCollection;
                bool pathIsValid = true;

                //parse the path and find the nodes in the tree
                for (int i = 0; i < pathStr.Length; i++)
                {
                    if (pathIsValid)
                    {
                        //case: path ends with "\" - which selects all from that directory
                        if (pathStr.Substring(i, 1).Equals(@"\") &&
                            i + 1 == pathStr.Length)
                        {
                            int index = -1;
                            //find the node by name in the collection
                            foreach (Node n in currCollection)
                            {
                                if (n.Name.Equals(currNodeName))
                                {
                                    index = currCollection.IndexOf(n);
                                    break;
                                }
                            }
                            //try to access the next node in the path, if it's not found skip the rest of the path
                            try
                            {
                                currCollection = currCollection[index].Children;
                                foreach (Node n in currCollection)
                                {
                                    NodeCollection.Add(n);
                                    n.IsChecked = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex.ToString());
                                pathIsValid = false;
                            }
                        }
                            //case: "\" between two node names
                        else if (pathStr.Substring(i, 1).Equals(@"\"))
                        {
                            int index = -1;
                            //find the node by name in the collection
                            foreach (Node n in currCollection)
                            {
                                if (n.Name.Equals(currNodeName))
                                {
                                    index = currCollection.IndexOf(n);
                                    break;
                                }
                            }
                            //try to access the next node in the path, if it's not found skip the rest of the path
                            try
                            {
                                currCollection = currCollection[index].Children;
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                pathIsValid = false;
                            }

                            currNodeName = "";
                        }
                            //case: path ends in last letter of node name
                        else if (i + 1 == pathStr.Length)
                        {
                            currNodeName += pathStr.Substring(i, 1);
                            foreach (Node n in currCollection)
                            {
                                if (n.Name.Equals(currNodeName))
                                {
                                    n.IsChecked = true;
                                    NodeCollection.Add(n);
                                    break;
                                }
                            }
                        }
                            //case: normal character - build the node name
                        else
                        {
                            currNodeName += pathStr.Substring(i, 1);
                        }
                    }
                }
            }

            return NodeCollection;
        }

        public int CalculateTreeLevels(ObservableCollection<Node> nodes)
        {
            int deepestLevel = 0;

            // recursive step
            foreach (Node n in nodes)
            {
                int currLevelDepth = 1;

                if (n.Children.Any())
                {
                    currLevelDepth += CalculateTreeLevels(n.Children);
                }
                if (currLevelDepth > deepestLevel)
                    deepestLevel = currLevelDepth;
            }

            return deepestLevel;
        }
    }
}