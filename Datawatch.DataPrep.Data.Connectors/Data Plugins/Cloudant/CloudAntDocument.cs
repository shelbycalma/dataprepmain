﻿using Newtonsoft.Json.Linq;

namespace CloudantConnector
{
    public class CloudantViewRow
    {
        #region Properties

        public JToken Key { get; set; }
        public JToken Value { get; set; }
        public string Id { get; set; }

        #endregion

        public CloudantViewRow(JToken key, JToken value, string id)
        {
            Key = key;
            Value = value;
            Id = id;
        }
    }
}