﻿namespace Panopticon.CloudantPlugin
{
    public enum SourceType
    {
        View,
        SearchIndex
    }
}