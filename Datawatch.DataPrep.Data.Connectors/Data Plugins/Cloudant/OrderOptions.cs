﻿using System.ComponentModel;

namespace Panopticon.CloudantPlugin
{
    public enum OrderOptions
    {
        [Description("UiSortAscending")]
        Ascending,

        [Description("UiSortDescending")]
        Descending
    }
}