﻿using System;
using Panopticon.CloudantPlugin.Properties;

namespace Panopticon.CloudantPlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        public static Exception UnableToLoadTable(string message)
        {
            return
                CreateInvalidOperation(string.Format(
                    Resources.ExUnableToLoadData, message));
        }
    }
}