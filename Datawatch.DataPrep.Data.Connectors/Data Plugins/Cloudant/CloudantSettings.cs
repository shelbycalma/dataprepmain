﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.CloudantPlugin.Properties;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.CloudantPlugin
{
    public class CloudantSettings : ConnectionSettings
    {
        #region Properties
        protected readonly IEnumerable<ParameterValue> parameters;


        public Dictionary<string, CloudantIndex> Views { get; set; }
        public Dictionary<string, CloudantIndex> SearchIndicies { get; set; }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public string ServerAccountOrUrl
        {
            get { return GetInternal("ServerAccountOrUrl"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("ServerAccountOrUrl", value.Trim());
                SetTitle();
            }
        }

        public string ServerAccountOrUrlParameterized
        {
            get { return ParameterizeValue(ServerAccountOrUrl); }
        }

        public string ServiceRootUri
        {
            get
            {
                if (string.IsNullOrEmpty(ServerAccountOrUrl))
                {
                    return string.Empty;
                }
                if (ServerAccountOrUrl.StartsWith("http:") ||
                    ServerAccountOrUrl.StartsWith("https:"))
                {
                    return ServerAccountOrUrl;
                }
                return string.Format("https://{0}.cloudant.com",
                    ServerAccountOrUrl);
            }
        }

        public string ServiceRootUriParameterized
        {
            get { return ParameterizeValue(ServiceRootUri); }
        }

        public virtual string User
        {
            get { return GetInternal("User"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("User", value.Trim());
            }
        }

        public string UserParameterized
        {
            get { return ParameterizeValue(User); }
        }

        public virtual string Password
        {
            get { return GetInternal("Password"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("Password", value.Trim());
            }
        }

        public string PasswordParameterized
        {
            get { return ParameterizeValue(Password); }
        }

        public string Database
        {
            get { return GetInternal("Database"); }
            set
            {
                SetInternal("Database", value);
                SetTitle();
            }
        }

        public string DatabaseParameterized
        {
            get { return ParameterizeValue(Database); }
        }

        public string View
        {
            get { return GetInternal("View"); }
            set
            {
                SetInternal("View", value);
                SetTitle();
            }
        }

        public string SearchIndex
        {
            get { return GetInternal("SearchIndex"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("SearchIndex", value.Trim());
                SetTitle();
            }
        }

        public string SearchIndexParameterized
        {
            get { return ParameterizeValue(SearchIndex); }
        }

        public string SearchTerm
        {
            get { return GetInternal("SearchTerm"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("SearchTerm", value.Trim());
                SetTitle();
            }
        }

        public string SearchTermParameterized
        {
            get { return ParameterizeValue(SearchTerm); }
        }

        public string Order
        {
            get { return GetInternal("Order", "Ascending"); }
            set { SetInternal("Order", value); }
        }

        public int GroupLevel
        {
            get { return GetInternalInt("GroupLevel", 0); }
            set
            {
                if (value < 0)
                    throw new Exception(Resources.ExNegativeNumber);
                SetInternalInt("GroupLevel", value);
            }
        }

        public string SpecificKeys
        {
            get { return GetInternal("SpecificKeys"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("SpecificKeys", value.Trim());
            }
        }

        public string SpecificKeysParameterized
        {
            get { return ParameterizeValue(SpecificKeys); }
        }

        public string StartKey
        {
            get { return GetInternal("StartKey"); }
            set { SetInternal("StartKey", value); }
        }

        public string StartKeyParameterized
        {
            get { return ParameterizeValue(StartKey); }
        }

        public string EndKey
        {
            get { return GetInternal("EndKey"); }
            set { SetInternal("EndKey", value); }
        }

        public string EndKeyParameterized
        {
            get { return ParameterizeValue(EndKey); }
        }

        public string Sort
        {
            get { return GetInternal("Sort"); }
            set { SetInternal("Sort", value); }
        }

        public string SortParameterized
        {
            get { return ParameterizeValue(Sort); }
        }

        public int Limit
        {
            get { return GetInternalInt("Limit" , 0); }
            set
            {
                if (value < 0)
                    throw new Exception(Resources.ExNegativeNumber);
                SetInternalInt("Limit", value);
            }
        }

        public int Skip
        {
            get { return GetInternalInt("Skip", 0); }
            set
            {
                if (value < 0)
                    throw new Exception(Resources.ExNegativeNumber);
                SetInternalInt("Skip", value);
            }
        }

        public bool DisableInclusiveEnd
        {
            get
            {
                string s = GetInternal("DisableInclusiveEnd", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("DisableInclusiveEnd", Convert.ToString(value)); }
        }

        public SourceType SourceType {
            get
            {
                // legacy support
                SourceType st = SourceType.View;
                bool isLegacy = false;
                string isViewString = GetInternal("ViewMode");
                string isSearchString = GetInternal("SearchIndexMode");

                if (!string.IsNullOrEmpty(isViewString))
                {
                    SetInternal("ViewMode", null);

                    bool isView = Convert.ToBoolean(isViewString);
                    st = isView ? SourceType.View : SourceType.SearchIndex;
                    isLegacy = true;
                }
                if (!string.IsNullOrEmpty(isSearchString))
                {
                    SetInternal("SearchIndexMode", null);

                    if (!isLegacy)
                    {
                        bool isSearch = Convert.ToBoolean(isSearchString);
                        st = isSearch ? SourceType.SearchIndex : SourceType.View;
                        isLegacy = true;
                    }
                }

                if (isLegacy)
                {
                    SetInternal("SourceType",
                        Enum.GetName(typeof(SourceType), st));
                    return st;
                }

                string s = GetInternal("SourceType",
                    Enum.GetName(typeof(SourceType), SourceType.View));
                return (SourceType)Enum.Parse(typeof(SourceType), s, true);
            }
            set
            {
                SetInternal("SourceType",
                    Enum.GetName(typeof(SourceType), value));
                SetTitle();
                FirePropertyChanged("IsViewSelected");
                FirePropertyChanged("IsSearchIndexSelected");
            }
        }

        public bool Stale
        {
            get
            {
                string s = GetInternal("Stale", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("Stale", Convert.ToString(value)); }
        }

        public bool DoReduce
        {
            get
            {
                string s = GetInternal("DoReduce", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("DoReduce", Convert.ToString(value));
                SetInternal("CanIncludeDocs", Convert.ToString(!value));
            }
        }

        public bool IncludeDocs
        {
            get
            {
                string s = GetInternal("IncludeDocs", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IncludeDocs", Convert.ToString(value));
                SetInternal("CanDoReduce", Convert.ToString(!value));
            }
        }

        public bool CanDoReduce
        {
            get
            {
                string s = GetInternal("CanDoReduce", true.ToString());
                return Convert.ToBoolean(s);
            }
        }

        public bool CanIncludeDocs
        {
            get
            {
                string s = GetInternal("CanIncludeDocs", true.ToString());
                return Convert.ToBoolean(s);
            }
        }

        public bool QuerySpecificKeys
        {
            get
            {
                string s = GetInternal("QuerySpecificKeys", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("QuerySpecificKeys", Convert.ToString(value));
                SetInternal("CanQueryByKeyBounds", Convert.ToString(!value));
            }
        }

        public bool CanQuerySpecificKeys
        {
            get
            {
                string s = GetInternal("CanQuerySpecificKeys", true.ToString());
                return Convert.ToBoolean(s);
            }
        }

        public bool QueryByKeyBounds
        {
            get
            {
                string s = GetInternal("QueryByKeyBounds", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("QueryByKeyBounds", Convert.ToString(value));
                SetInternal("CanQuerySpecificKeys", Convert.ToString(!value));
            }
        }

        public bool CanQueryByKeyBounds
        {
            get
            {
                string s = GetInternal("CanQueryByKeyBounds", true.ToString());
                return Convert.ToBoolean(s);
            }
        }

        public bool IncludeDatabase
        {
            get
            {
                string s = GetInternal("IncludeDatabase", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IncludeDatabase", Convert.ToString(value));
            }
        }

        public virtual string IncludedDatabase
        {
            get { return GetInternal("IncludedDatabase"); }
            set
            {
                if (value == null)
                    value = String.Empty;
                SetInternal("IncludedDatabase", value.Trim());
            }
        }

        public string IncludedDatabaseParameterized
        {
            get { return ParameterizeValue(IncludedDatabase); }
        }

        public bool IsViewSelected
        {
            get { return SourceType == SourceType.View; }
        }

        public bool IsSearchIndexSelected
        {
            get { return SourceType == SourceType.SearchIndex; }
        }

        public PluginHostSettingsViewModel PluginHostSettings { get; set; }

        #endregion

        #region Constructors

        public CloudantSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters, PluginHostSettingsViewModel hostSettingsViewModel)
            : base(bag)
        {
            this.parameters = parameters;
            this.PluginHostSettings = hostSettingsViewModel;
        }

        #endregion

        #region Methods

        private void SetTitle()
        {
            Title = string.Format("{0}-{1}-{2}", ServiceRootUri ?? "",
                Database ?? "", View ?? "");
        }

        public string ParameterizeValue(
            string value, string arraySeparator = "\n")
        {
            if (!this.PluginHostSettings.IsParameterSupported)
            {
                return value;
            }

            var parameters = Parameters;

            if (parameters == null || String.IsNullOrEmpty(value))
                return value;
            else
            {
                var encoder = new ParameterEncoder
                {
                    Parameters = parameters,
                    DefaultArraySeparator = arraySeparator,
                    SourceString = value
                };
                return encoder.Encoded();
            }
        }

        #endregion
    }
}