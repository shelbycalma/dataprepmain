﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Datawatch.DataPrep.Data.Framework;
using Newtonsoft.Json.Linq;

namespace Panopticon.CloudantPlugin
{
    public class CloudantHelper
    {
        #region Getters

        public static List<string> GetAvailableDatabases(
            CloudantSettings settings, bool rethrow = false)
        {
            var request = string.Format("{0}/_all_dbs",
                settings.ServiceRootUriParameterized.TrimEnd('/'));
            string response = ExecuteRequest(settings, request);
            return ParseDatabaseList(response);
        }

        public static Dictionary<string, CloudantIndex> GetAvailableViews(
            CloudantSettings settings)
        {
            var views = new Dictionary<string, CloudantIndex>();

            // get the paths to the design documents so that they can be 
            // used to get the views
            var designDocPaths = GetDesignDocPaths(settings);

            foreach (string path in designDocPaths)
            {
                // create a new entry in the map for the current design 
                // document
                var designDocName = path.Substring("_design/".Length,
                    path.Length - "_design/".Length);

                var request = string.Format("{0}/{1}/{2}",
                    settings.ServiceRootUriParameterized.TrimEnd('/'),
                    settings.DatabaseParameterized,
                    path);
                string response = ExecuteRequest(settings, request);

                // populate the list of views for the current design 
                // document
                var viewNamesForDoc = ParseViewsFromDesignDoc(response);
                foreach (var name in viewNamesForDoc)
                {
                    CloudantIndex newView = new CloudantIndex(name,
                        designDocName);
                    views.Add(newView.UniqueName, newView);
                }
            }
            
            return views;
        }

        public static Dictionary<string, CloudantIndex>
            GetAvailableSearchIndices(CloudantSettings settings)
        {
            // create a new entry in the map for the current design document
            var indicies = new Dictionary<string, CloudantIndex>();
            
            // get the paths to the design documents so that they can be 
            // used to get the views
            var designDocPaths = GetDesignDocPaths(settings);

            foreach (string path in designDocPaths)
            {
                // create a new entry in the map for the current design 
                // document
                var designDocName = path.Substring("_design/".Length,
                    path.Length - "_design/".Length);

                var request = string.Format("{0}/{1}/{2}",
                    settings.ServiceRootUriParameterized.TrimEnd('/'),
                    settings.DatabaseParameterized,
                    path);
                string response = ExecuteRequest(settings, request);

                // populate the list of search indicies for the current 
                // design document
                var searchIndexNamesForDoc =
                    ParseSearchIndiciesFromDesignDoc(response);
                foreach (var name in searchIndexNamesForDoc)
                {
                    CloudantIndex newIndex = new CloudantIndex(name,
                        designDocName);
                    indicies.Add(newIndex.UniqueName, newIndex);
                }
            }
            
            return indicies;
        }

        public static List<string> GetDesignDocPaths(CloudantSettings settings)
        {
            // create a request to get the list of all design documents on the 
            // server
            var request =
                string.Format(
                    "{0}/{1}/_all_docs?&startkey=\"_design\"&endkey=\"_e\"",
                    settings.ServiceRootUriParameterized.TrimEnd('/'),
                    settings.DatabaseParameterized);
            string response = ExecuteRequest(settings, request);

            // populate a list of design documents based on the response
            return ParseDesignDocList(response);
        }

        public static int GetRowsForViewAsJTokenList(
            CloudantSettings settings, int queryRowCount, ResponseObject ro,
            bool firstQuery)
        {
            return GetRowsForViewAsJTokenList(settings, queryRowCount, ro, firstQuery, 0);
        }

        public static int GetRowsForViewAsJTokenList(
            CloudantSettings settings, int queryRowCount, ResponseObject ro,
            bool firstQuery, int startKeyOffset)
        {
            ro.rows = new List<JToken>();
            string oldStartKey = ro.startKey;
            ro.startKey = "";
            Dictionary<string, CloudantIndex> views = settings.Views;
            CloudantIndex currView = views[settings.View];

            bool querySuccess = false;
            // perform the query with increasingly smaller response sizes until 
            // it succeeds
            while (!querySuccess)
            {
                // skip over the first record because it will be the last record
                // from the previous set.
                var request =
                    string.Format("{0}/{1}/_design/{2}/_view/{3}?limit={4}",
                        settings.ServiceRootUriParameterized.TrimEnd('/'),
                        settings.DatabaseParameterized,
                        currView.DesignDoc, currView.Name, queryRowCount);

                if (firstQuery)
                {
                    request += "&skip=" +
                               (settings.Skip >= 0 ? settings.Skip : 0);
                }
                else
                {
                    request += "&skip=" + startKeyOffset;
                }

                if (firstQuery && settings.QueryByKeyBounds)
                {
                    request += "&startkey=" + settings.StartKeyParameterized;
                }
                else if (!string.IsNullOrEmpty(oldStartKey) &&
                         !settings.QuerySpecificKeys)
                {
                    request += "&startkey=" + oldStartKey;
                }

                if (settings.QueryByKeyBounds)
                {
                    request += "&endkey=" + settings.EndKeyParameterized;
                }

                if (settings.QueryByKeyBounds && settings.DisableInclusiveEnd)
                {
                    request += "&inclusive_end=false";
                }

                if (settings.Order == "Ascending")
                {
                    request += "&descending=false";
                }
                else if (settings.Order == "Descending")
                {
                    request += "&descending=true";
                }

                if (settings.Stale)
                {
                    request += "&stale=ok";
                }

                if (settings.QuerySpecificKeys)
                {
                    request += "&keys=" + settings.SpecificKeysParameterized;
                }

                if (settings.IncludeDocs)
                {
                    request += "&include_docs=true";
                }

                if (settings.DoReduce)
                {
                    request += "&reduce=true&group_level=" + settings.GroupLevel;
                }
                else
                {
                    request += "&reduce=false";
                }

                try
                {
                    string response = ExecuteRequest(settings, request);
                    ro.startKey = ParseNewStartKey(response);
                    ro.rows = ParseRowsAsJTokens(response);
                    querySuccess = true;
                }
                catch (OutOfMemoryException)
                {
                    queryRowCount /= 2;
                    if (queryRowCount <= 0)
                    {
                        throw;
                    }
                }
            }
            return queryRowCount;
        }

        public static string GetResponse(
            CloudantSettings settings, int rowCount, string bookmark)
        {
            var indicies = settings.SearchIndicies;
            var currIndex = indicies[settings.SearchIndexParameterized];

            var request =
                string.Format("{0}/{1}/_design/{2}/_search/{3}?q={4}&limit={5}",
                    settings.ServiceRootUriParameterized.TrimEnd('/'),
                    settings.DatabaseParameterized,
                    currIndex.DesignDoc, currIndex.Name,
                    settings.SearchTermParameterized,
                    rowCount);

            if (settings.IncludeDocs)
            {
                request += "&include_docs=true";
            }

            if (!string.IsNullOrEmpty(bookmark))
            {
                request += "&bookmark=" + bookmark;
            }

            if (settings.Stale)
            {
                request += "&stale=ok";
            }

            if (!string.IsNullOrEmpty(settings.SortParameterized))
            {
                request += "&sort=" + settings.SortParameterized;
            }

            return ExecuteRequest(settings, request);
        }

        #endregion

        #region Parsers

        public static string ParseIndexBookmark(string response)
        {
            JToken result = JToken.Parse(response).SelectToken("bookmark");
            return result == null ? string.Empty : result.ToString();
        }

        public static string ParseNewStartKey(string response)
        {
            string newStartKey;

            JArray rows = (JArray) JToken.Parse(response).SelectToken("rows");

            JToken keyToken = rows != null && rows.Any()
                ? rows.Last.SelectToken("key")
                : null;
            
            if (keyToken == null)
            {
                return String.Empty;
            }

            switch (keyToken.Type)
            {
                case JTokenType.String:
                    newStartKey = "\"" + keyToken.ToString() + "\"";
                    break;
                default:
                    newStartKey = keyToken.ToString();
                    break;
            }

            return newStartKey;
        }

        public static List<JToken> ParseRowsAsJTokens(string response)
        {
            JArray rows = (JArray) JToken.Parse(response).SelectToken("rows");
            
            return rows.ToList();
        }

        /// <summary>
        ///     Parses a list of design document names from response.
        /// </summary>
        /// <param name="response">
        ///     A single object that both describes and contains the list of 
        ///     "rows" which are themselves the metadata for the design 
        ///     documents.
        /// </param>
        /// <returns></returns>
        public static List<string> ParseDesignDocList(string response)
        {
            JArray designDocPaths = (JArray)JToken.Parse(response)
                .SelectToken("rows");
            if (designDocPaths == null) return new List<string>();
            return designDocPaths
                .Select<JToken, string>(x => (string)x.SelectToken("id") ?? string.Empty)
                .ToList();
        }

        /// <summary>
        ///     Parses a list of database names from response.
        /// </summary>
        /// <param name="response">
        ///     A simple json list of database names with format 
        ///     ["db1", "db2", ..., "dbn"], parse them to populate the list.
        /// </param>
        /// <returns></returns>
        public static List<string> ParseDatabaseList(string response)
        {
            JArray databases = JArray.Parse(response);
            return databases
                .Select(x => x.Value<string>())
                .ToList();
        }

        /// <summary>
        ///     Parses a list of view names from the design document represented 
        ///     by response.
        /// </summary>
        /// <param name="response">
        ///     A single json object representing the design doc to be parsed.
        /// </param>
        /// <returns></returns>
        public static List<string> ParseViewsFromDesignDoc(string response)
        {
            JObject views = (JObject) JObject.Parse(response).SelectToken("views");
            if (views == null) return new List<string>();
            return views
                .Select<KeyValuePair<string, JToken>, string>(x => x.Key)
                .ToList();
        }

        public static List<string> ParseSearchIndiciesFromDesignDoc(
            string response)
        {
            JObject indexes = (JObject)JObject.Parse(response).SelectToken("indexes");
            if(indexes == null) return new List<string>();
            return indexes
                .Select<KeyValuePair<string, JToken>, string>(x => x.Key)
                .ToList();
        }

        private static List<KeyValuePair<string, JToken>> ParseComplexJArray(
            JArray jarray, string LEVEL_DELIMITER, string rootName,
            int index)
        {
            var rowList = new List<KeyValuePair<string, JToken>>();

            foreach (JToken token in jarray)
            {
                string label = rootName + index;

                if (token.Type == JTokenType.Array)
                {
                    rowList.AddRange(ParseComplexJArray((JArray) token,
                        LEVEL_DELIMITER, rootName, index));
                }
                else
                {
                    rowList.AddRange(ParseComplexJToken(token, LEVEL_DELIMITER,
                        label, 0));
                }
                index++;
            }

            return rowList;
        }

        public static List<KeyValuePair<string, JToken>> ParseComplexJToken(
            JToken jtoken, string LEVEL_DELIMITER, string rootName,
            int level)
        {
            var columnValueList = new List<KeyValuePair<string, JToken>>();

            // base case
            if (!jtoken.Any())
            {
                // make sure we don't pollute the column list with empty object 
                // or array fields.
                var type = jtoken.Type;
                if (type == JTokenType.Array || type == JTokenType.Object)
                {
                    return columnValueList;
                }

                if (level > 1)
                {
                    columnValueList.Add(
                        new KeyValuePair<string, JToken>(
                            rootName + " - (Level " + level + ")", jtoken));
                }
                else
                {
                    columnValueList.Add(new KeyValuePair<string, JToken>(
                        rootName, jtoken));
                }

                return columnValueList;
            }

            // recursive step
            foreach (JToken token in jtoken)
            {
                string label = rootName;

                if (token.Type == JTokenType.Array)
                {
                    columnValueList.AddRange(ParseComplexJArray((JArray) token,
                        LEVEL_DELIMITER, label, 1));
                }
                else if (token.Type == JTokenType.Property)
                {
                    label += !string.IsNullOrWhiteSpace(label)
                        ? LEVEL_DELIMITER + ((JProperty) token).Name
                        : ((JProperty) token).Name;
                    columnValueList.AddRange(ParseComplexJToken(token,
                        LEVEL_DELIMITER, label, 0));
                }
                else
                {
                    columnValueList.AddRange(ParseComplexJToken(token,
                        LEVEL_DELIMITER, label, level + 1));
                }
            }
            return columnValueList;
        }

        #endregion

        #region Web Request Handlers

        public static string ExecuteRequest(
            CloudantSettings settings, string request)
        {
            string response = string.Empty;
            try
            {
                using (var client = new WebClient())
                {
                    client.Headers.Add(HttpRequestHeader.ContentType,
                        "text/plan, application/json");
                    client.Headers.Add(HttpRequestHeader.Accept,
                        "text/plain, application/json");
                    client.Credentials = new NetworkCredential(
                        settings.UserParameterized,
                        settings.PasswordParameterized);
                    string normalizedUrl = Uri.EscapeUriString(request);
                    response = client.DownloadString(normalizedUrl);
                }
                return response;
            }
            catch (WebException ex)
            {
                Log.Exception(ex);
                throw;
            }
        }

        #endregion
    }
}