﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.CloudantPlugin.Properties;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.CloudantPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<CloudantSettings>
    {
        internal const string PluginId = "IBMCloudantConnector";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "IBM Cloudant";
        internal const string PluginType = DataPluginTypes.Database;

        #region Properties

        public override string Id
        {
            get { return PluginId; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public PluginHostSettingsViewModel PluginHostSettings { get; set; }

        #endregion

        public override void Initialize(bool isHosted, IPluginManager pluginManager,
            PropertyBag settings, PropertyBag globalSettings)
        {
            base.Initialize(isHosted, pluginManager, settings, globalSettings);
            this.PluginHostSettings = new PluginHostSettingsViewModel(globalSettings);
        }

        public override CloudantSettings CreateSettings(PropertyBag bag)
        {
            return new CloudantSettings(bag , null, this.PluginHostSettings);
        }

        public virtual CloudantSettings CreateSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new CloudantSettings(bag, parameters, this.PluginHostSettings);
        }

        public override ITable GetData(
            string workbookDir, string dataDir, PropertyBag settings,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingQuery);
            DateTime start = DateTime.Now;

            var tableBuilder =
                new CloudantTableBuilder(CreateSettings(settings, parameters),
                    rowcount);

            StandaloneTable result;

            try
            {
                result = tableBuilder.BuildTable();
            }
            catch (Exception ex)
            {
                Exception e = Exceptions.UnableToLoadTable(ex.Message);
                Log.Exception(e);
                throw e;
            }

            if (result != null)
            {
                // Done, log out how long it took and how much we got.
                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime, result.RowCount,
                    result.ColumnCount, seconds);
                return result;
            }

            Exception exception = Exceptions.UnableToLoadTable(string.Empty);
            Log.Exception(exception);
            throw exception;
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }
    }
}