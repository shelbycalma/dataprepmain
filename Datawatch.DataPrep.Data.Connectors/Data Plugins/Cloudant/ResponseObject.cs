﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Panopticon.CloudantPlugin
{
    public class ResponseObject
    {
        internal List<JToken> rows;
        internal string startKey = "";

        internal void Init()
        {
            rows = new List<JToken>();
        }
    }
}
