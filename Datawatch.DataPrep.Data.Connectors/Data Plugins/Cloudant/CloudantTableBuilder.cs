﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Panopticon.CloudantPlugin.Properties;
using Newtonsoft.Json.Linq;

namespace Panopticon.CloudantPlugin
{
    internal class CloudantTableBuilder
    {
        public CloudantSettings Settings { get; set; }

        public int RowsPerQuery
        {
            get
            {
                if (_safeRowsPerQuery > 0)
                {
                    return _safeRowsPerQuery;
                }
                else
                {
                    return MAX_ROWS_PER_QUERY;
                }
            }
        }

        public int RequestedRowCount
        {
            get
            {
                if (Settings.Limit > 0)
                {
                    if (_requestedRowCount - 1 > 0)
                    {
                        return Math.Min(Settings.Limit, _requestedRowCount - 1);
                    }
                    return Settings.Limit;
                }
                return _requestedRowCount - 1;
            }
        }

        private readonly int _requestedRowCount;
        private const int MAX_ROWS_PER_QUERY = 10000;
        private const int MAX_ROWS_PER_SEARCH_INDEX_QUERY = 200;
        private int _safeRowsPerQuery = -1;
        private const string LEVEL_DELIMITER = "--";

        public CloudantTableBuilder(
            CloudantSettings settings, int requestedRowCount)
        {
            Settings = settings;
            _requestedRowCount = requestedRowCount;
        }

        public StandaloneTable BuildTable()
        {
            if (Settings.SourceType == SourceType.View)
            {
                return BuildTableForView();
            }
            else //Settings.SearchIndexMode
            {
                return BuildTableForSearchIndex();
            }
        }

        private StandaloneTable BuildTableForView()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            Settings.Views = CloudantHelper.GetAvailableViews(Settings);

            if (!IsViewExist())
            {
                throw new ApplicationException(Resources.ExViewDoesntExist);
            }

            // dictionary to hold the multiplicity associated with specific 
            // column names (for column names that show up multiple times but 
            // are actually unique based on their fully qualified name).
            var columnPathMap = new Dictionary<string, string>();
            var columnLeafMap = new Dictionary<string, int>();
            var columnTypeMap = new Dictionary<string, string>();

            int rowIndex = 0;
            bool firstQuery = true;
            int numEndKeyOccurrences = 1;

            ResponseObject ro = new ResponseObject();
            while (rowIndex < RequestedRowCount || RequestedRowCount < 0)
            {
                int getRowCount = (RequestedRowCount > 0)
                    ? Math.Min(RowsPerQuery, RequestedRowCount - rowIndex)
                    : RowsPerQuery;

                ro.Init();
                // if this is our first time through, skip the number of rows 
                // specified by the user in the config panel
                if (firstQuery && Settings.Skip >= 0)
                {
                    _safeRowsPerQuery = CloudantHelper.GetRowsForViewAsJTokenList(Settings,
                        getRowCount, ro, firstQuery);
                    //rows = CloudantHelper.GetRowsForViewAsJTokenList(Settings,
                    //    getRowCount, startkey, firstQuery, out startkey,
                    //    out _safeRowsPerQuery);
                    firstQuery = false;
                }
                else
                {
                    _safeRowsPerQuery = CloudantHelper.GetRowsForViewAsJTokenList(Settings,
                        getRowCount, ro, firstQuery, numEndKeyOccurrences);
                    //rows = CloudantHelper.GetRowsForViewAsJTokenList(Settings,
                    //    getRowCount, startkey, firstQuery, out startkey,
                    //    out _safeRowsPerQuery, numEndKeyOccurrences);
                }

                // break if we have reached the end of the table before 
                // satisfying our row quota
                if (!ro.rows.Any())
                {
                    break;
                }

                JToken prevKey = null;

                foreach (var row in ro.rows)
                {
                    var currKey = row.SelectToken("key");
                    if (prevKey != null && prevKey.Equals(currKey))
                    {
                        numEndKeyOccurrences++;
                    }
                    else
                    {
                        prevKey = row.SelectToken("key");
                        numEndKeyOccurrences = 1;
                    }
                    var tableRow = table.AddRow();

                    List<KeyValuePair<string, JToken>> jTokenRow =
                        CloudantHelper.ParseComplexJToken(row, LEVEL_DELIMITER, "", 0);

                    for (int i = 0; i < jTokenRow.Count; i++)
                    {
                        // parse out the name of the leaf node
                        string baseLeafName = ParseLabelLeaf(jTokenRow[i].Key);
                        string label;
                        // find out if this path has already been asigned a 
                        // label
                        if (columnPathMap.ContainsKey(jTokenRow[i].Key))
                        {
                            // if it has, use that label
                            label = columnPathMap[jTokenRow[i].Key];
                        }
                        // if it hasn't, assign it a label and add the path 
                        // and label to the path map
                        else
                        {
                            // if the base leaf name is already in use then the 
                            // label should be the base leaf name plus a number
                            if (columnLeafMap.ContainsKey(baseLeafName))
                            {
                                label = baseLeafName + "-" +
                                        columnLeafMap[baseLeafName]++;
                            }
                            // if the base leaf name is not already in use then
                            // the label should just be the base leaf name, 
                            // then add the base leaf name to the leaf map
                            else
                            {
                                columnLeafMap.Add(baseLeafName, 2);
                                label = baseLeafName;
                            }
                            // once the label has been generated, assign it to 
                            // the new path in the path map
                            columnPathMap.Add(jTokenRow[i].Key, label);
                        }

                        if (!table.ContainsColumn(label))
                        {
                            AddTableColumn(table, label, jTokenRow[i].Value,
                                columnTypeMap);
                        }

                        AddValueToTable(table, label, tableRow,
                            jTokenRow[i].Value, columnTypeMap);
                    }
                    rowIndex++;
                }

                if (Settings.QuerySpecificKeys)
                {
                    break;
                }
            }
            return table;
        }

        private bool IsViewExist()
        {
            CloudantIndex currView;
            return Settings.Views.TryGetValue(Settings.View, out currView);
        }

        private StandaloneTable BuildTableForSearchIndex()
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            Settings.SearchIndicies =
                CloudantHelper.GetAvailableSearchIndices(Settings);

            // dictionary to hold the multiplicity associated with 
            // specific column names (for column names that show up multiple 
            // times but are actually unique based on their fully qualified 
            // name).
            var columnPathMap = new Dictionary<string, string>();
            var columnLeafMap = new Dictionary<string, int>();
            var columnTypeMap = new Dictionary<string, string>();

            int rowIndex = 0;
            string bookmark = "";

            while (rowIndex < RequestedRowCount || RequestedRowCount < 0)
            {
                int getRowCount = (RequestedRowCount > 0)
                    ? Math.Min(MAX_ROWS_PER_SEARCH_INDEX_QUERY,
                        RequestedRowCount - rowIndex)
                    : MAX_ROWS_PER_SEARCH_INDEX_QUERY;

                string response = CloudantHelper.GetResponse(Settings,
                    getRowCount, bookmark);
                List<JToken> rows = CloudantHelper.ParseRowsAsJTokens(response);
                bookmark = CloudantHelper.ParseIndexBookmark(response);

                // break if we have reached the end of the table before 
                // satisfying our row quota
                if (!rows.Any())
                {
                    break;
                }

                foreach (var row in rows)
                {
                    var tableRow = table.AddRow();

                    List<KeyValuePair<string, JToken>> jTokenRow =
                        CloudantHelper.ParseComplexJToken(row, LEVEL_DELIMITER, "", 0);

                    for (int i = 0; i < jTokenRow.Count; i++)
                    {
                        // parse out the name of the leaf node
                        string baseLeafName = ParseLabelLeaf(jTokenRow[i].Key);
                        string label;
                        // find out if this path has already been asigned a 
                        // label
                        if (columnPathMap.ContainsKey(jTokenRow[i].Key))
                        {
                            // if it has, use that label
                            label = columnPathMap[jTokenRow[i].Key];
                        }
                        // if it hasn't, assign it a label and add the path 
                        // and label to the path map
                        else
                        {
                            // if the base leaf name is already in use then the 
                            // label should be the base leaf name plus a number
                            if (columnLeafMap.ContainsKey(baseLeafName))
                            {
                                label = baseLeafName + "-" +
                                        columnLeafMap[baseLeafName]++;
                            }
                            // if the base leaf name is not already in use 
                            // then the label should just be the base leaf name,
                            // then add the base leaf name to the leaf map
                            else
                            {
                                columnLeafMap.Add(baseLeafName, 2);
                                label = baseLeafName;
                            }
                            // once the label has been generated, assign it to 
                            // the new path in the path map
                            columnPathMap.Add(jTokenRow[i].Key, label);
                        }

                        if (!table.ContainsColumn(label))
                        {
                            AddTableColumn(table, label, jTokenRow[i].Value,
                                columnTypeMap);
                        }

                        AddValueToTable(table, label, tableRow,
                            jTokenRow[i].Value, columnTypeMap);
                    }
                    rowIndex++;
                }
            }
            return table;
        }

        /// <summary>
        ///     Parses the leaf name from the hierarchically structured label 
        ///     returned by ParseComplexJToken.
        /// </summary>
        /// <returns></returns>
        private string ParseLabelLeaf(string label)
        {
            // find the last occurence of the level delimiter, any text past 
            // this should be our leaf.
            int leafDelimiterBeginIndex = label.LastIndexOf(LEVEL_DELIMITER);

            if (leafDelimiterBeginIndex < 0)
            {
                return label;
            }

            int leafStartIndex = 
                leafDelimiterBeginIndex + LEVEL_DELIMITER.Length;

            return label.Substring(leafStartIndex);
        }

        private void AddTableColumn(
            StandaloneTable table, string label, JToken testValue,
            Dictionary<string, string> columnTypeMap)
        {
            switch (testValue.Type)
            {
                case JTokenType.Integer:
                case JTokenType.Float:
                    table.AddColumn(new NumericColumn(label));
                    columnTypeMap.Add(label, "Numeric");
                    break;
                case JTokenType.Date:
                    table.AddColumn(new TimeColumn(label));
                    columnTypeMap.Add(label, "Date");
                    break;
                default:
                    DateTime dt;
                    var parsed = DateTime.TryParse(testValue.ToString(), 
                        out dt);
                    if (parsed)
                    {
                        table.AddColumn(new TimeColumn(label));
                        columnTypeMap.Add(label, "Date");
                        break;
                    }
                    table.AddColumn(new TextColumn(label));
                    columnTypeMap.Add(label, "Text");
                    break;
            }
        }

        private void AddValueToTable(
            StandaloneTable table, string columnName, Row tableRow,
            JToken cellToken, Dictionary<string, string> columnTypeMap)
        {
            var column = table.GetColumn(columnName);

            switch (cellToken.Type)
            {
                case JTokenType.Integer:
                case JTokenType.Float:
                    column.SetValue(tableRow, cellToken.ToString());
                    break;
                default:
                    if (columnTypeMap[columnName] == "Numeric")
                    {
                        double numericVal;
                        bool parseSuccess = Double.TryParse(
                            cellToken.ToString(), out numericVal);
                        if (parseSuccess)
                        {
                            column.SetValue(tableRow, numericVal);
                        }
                        else
                        {
                            column.SetValue(tableRow, null);
                        }
                        break;
                    }
                    column.SetValue(tableRow, cellToken.ToString());
                    break;
            }
        }
    }
}