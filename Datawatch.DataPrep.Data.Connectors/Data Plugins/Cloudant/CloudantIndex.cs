﻿namespace Panopticon.CloudantPlugin
{
    public class CloudantIndex
    {
        public string Name { get; set; }
        public string DesignDoc { get; set; }

        public string UniqueName
        {
            get { return string.Format("{0}/{1}", DesignDoc, Name); }
        }

        public CloudantIndex(string name, string designDoc)
        {
            Name = name;
            DesignDoc = designDoc;
        }
    }
}