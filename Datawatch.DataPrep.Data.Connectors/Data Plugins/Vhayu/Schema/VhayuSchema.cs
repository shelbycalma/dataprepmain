﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Panopticon.VhayuPlugin.Schema
{
    [Serializable]
    public class VhayuSchema
    {
        public VhayuSchema()
        {
            this.ProcedureTypes = new List<ProcedureType>();
        }

        [XmlIgnore]
        public IEnumerable<Procedure> AllProcedures
        {
            get {
                foreach (ProcedureType type in this.ProcedureTypes) {
                    foreach (Procedure procedure in type.Procedures) {
                        yield return procedure;
                    }
                }
            }
        }

        public string DataSourceName { get; set; }

        public DateTime LastUpdated { get; set; }

        public List<ProcedureType> ProcedureTypes { get; set; }
    }
}
