﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Panopticon.VhayuPlugin.Schema
{
    public class Field
    {
        public Field()
        {
        }

        [XmlAttribute("Name")]
        public string ParameterName { get; set; }

        public string FieldSource { get; set; }

        public string Description { get; set; }

        public byte[] DefaultValue { get; set; }

        public string DefaultSource { get; set; }

        [XmlAttribute("Type")]
        public FieldType FieldType { get; set; }

        [XmlAttribute("Length")]
        public short FieldLength { get; set; }
    }
}
