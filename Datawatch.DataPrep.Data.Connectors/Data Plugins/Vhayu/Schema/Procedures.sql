﻿select
        P.RDB$PROCEDURE_NAME,
        P.RDB$PROCEDURE_TYPE,
        UP.RDB$USER,
        SP.STOREDPROC_TYPE,
        SP.YFILEINFO_EXISTS,
        SP.DISPLAY_NAME,
        SPT.TYPE_NAME,
        SPT.DISPLAY_ORDER
from
        RDB$PROCEDURES as P
        join
        RDB$USER_PRIVILEGES as UP
        on P.RDB$PROCEDURE_NAME = UP.RDB$RELATION_NAME
        left outer join
        VA_STOREDPROC as SP
        on P.RDB$PROCEDURE_NAME = SP.STOREDPROC_NAME
        left outer join
        VA_STOREDPROCTYPE as SPT
        on SP.STOREDPROC_TYPE = SPT.TYPE_VAL
where
        (UP.RDB$USER = 'PUBLIC' or
         UP.RDB$RELATION_NAME not in
              (select RDB$RELATION_NAME
               from   RDB$USER_PRIVILEGES
               where  RDB$USER = 'PUBLIC')
        ) and
        UP.RDB$PRIVILEGE = 'X' and
        UP.RDB$OBJECT_TYPE = 5
order by
        SPT.DISPLAY_ORDER,
        P.RDB$PROCEDURE_NAME

