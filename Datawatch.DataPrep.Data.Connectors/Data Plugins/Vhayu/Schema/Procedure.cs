﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace Panopticon.VhayuPlugin.Schema
{
    public class Procedure
    {
        public Procedure()
        {
            this.Inputs = new List<Field>();
            this.Outputs = new List<Field>();
        }

        [XmlIgnore]
        public ProcedureType Type { get; set; }

        [XmlAttribute("Name")]
        public string ProcedureName { get; set; }

        public string Description { get; set; }

        // The RDB$PROCEDURES.RDB$PROCEDURE_TYPE field.
        // Not to be confused with ProcedureType, which comes from
        // the VA_STOREDPROC.STOREDPROC_TYPE field.
        public int ProcedureType { get; set; }

        public string User { get; set; }

        [DefaultValue(false)]
        public bool YFileInfoExists { get; set; }

        public string DisplayName { get; set; }

        public List<Field> Inputs { get; set; }

        public List<Field> Outputs { get; set; }

        [XmlIgnore]
        public bool IsMetaDataLoaded
        {
            get { return this.Inputs.Count + this.Outputs.Count > 0; }
        }

        [XmlIgnore]
        public string FriendlyName
        {
            get {
                if (!string.IsNullOrEmpty(this.DisplayName)) {
                    return this.DisplayName;
                }
                return this.ProcedureName;
            }
        }
    }
}
