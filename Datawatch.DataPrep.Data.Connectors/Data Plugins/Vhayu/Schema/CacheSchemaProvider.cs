﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Windows;

namespace  Panopticon.VhayuPlugin.Schema
{
    internal static class CacheSchemaProvider
    {
        private const string FilePrefix = "VhayuConnector.";
        private const string FileSuffix = ".cache";

        private static string BuildCacheFilename(string dsn)
        {
            string directory = CacheSchemaProvider.CacheDirectory;
            string name = string.Format("{1}{0}{2}",
                dsn, FilePrefix, FileSuffix);
            string path = Path.Combine(directory, name);
            return path;
        }

        public static IEnumerable<string> CachedDataSourceNames
        {
            get {
                List<string> dsns = new List<string>();
                string directory = CacheSchemaProvider.CacheDirectory; 
                if (Directory.Exists(directory)) {
                    foreach (string file in Directory.GetFiles(directory)) {
                        string name = Path.GetFileName(file);
                        if (name.StartsWith(FilePrefix) &&
                            name.EndsWith(FileSuffix)) {
                            string dsn = name.Substring(FilePrefix.Length,
                                name.Length -
                                    (FilePrefix.Length + FileSuffix.Length));
                            dsns.Add(dsn);
                        }
                    }
                }
                return dsns;
            }
        }

        public static string CacheDirectory
        {
            get {
                // deprecated path
                // but we need to check it to achieve backward compatibility
                string path = Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, "Panopticon");
                path = Path.Combine(path, "EX");
                path = Path.Combine(path, Plugin.PluginId);

                if (Directory.Exists(path))
                {
                    return path;
                }

                path = Environment.GetFolderPath(
                    Environment.SpecialFolder.ApplicationData);
                path = Path.Combine(path, "Datawatch Desktop");
                path = Path.Combine(path, "Designer");
                path = Path.Combine(path, Plugin.PluginId);
                
                return path;
            }
        }

        public static bool IsCached(string dataSourceName)
        {
            string filename = BuildCacheFilename(dataSourceName);
            return File.Exists(filename);
        }

        public static VhayuSchema Load(string dataSourceName)
        {
            VhayuSchema schema = null;
            string filename = BuildCacheFilename(dataSourceName);
            try {
                XmlSerializer serializer =
                    new XmlSerializer(typeof(VhayuSchema));
                XmlReaderSettings settings = new XmlReaderSettings() {
                };
                using (Stream stream = new FileStream(filename, FileMode.Open))
                using (XmlReader reader =
                    XmlTextReader.Create(stream, settings)) {
                    schema = (VhayuSchema) serializer.Deserialize(reader);
                }
                schema.DataSourceName = dataSourceName;
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw ex;
            }
            return schema;
        }

        public static void Remove(string dataSourceName)
        {
            string filename = BuildCacheFilename(dataSourceName);
            File.Delete(filename);
        }

        public static void Save(string dataSourceName, VhayuSchema schema)
        {
            string filename = BuildCacheFilename(dataSourceName);
            try {
                string folder = Path.GetDirectoryName(filename);
                if (!Directory.Exists(folder)) {
                    Directory.CreateDirectory(folder);
                }
                XmlSerializer serializer =
                    new XmlSerializer(typeof(VhayuSchema));
                XmlWriterSettings settings = new XmlWriterSettings() {
                    Encoding = Encoding.UTF8,
                    Indent = true,
                    NewLineChars = "\n",
                    NewLineHandling = NewLineHandling.Entitize
                };
                using (XmlWriter writer =
                    XmlTextWriter.Create(filename, settings)) {
                    serializer.Serialize(writer, schema);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
            }
        }
    }
}
