﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Panopticon.VhayuPlugin.Schema
{
    public class ProcedureType
    {
        public ProcedureType()
        {
            this.Procedures = new List<Procedure>();
        }

        [XmlIgnore]
        public VhayuSchema Schema { get; set; }

        [XmlAttribute("Name")]
        public string TypeName { get; set; }

        public List<Procedure> Procedures { get; set; }
    }
}
