﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Data.Odbc;

namespace Panopticon.VhayuPlugin.Schema
{
    public static class SchemaProvider
    {
        public static void ClearCache(string dataSourceName)
        {
            if (CacheSchemaProvider.IsCached(dataSourceName)) {
                CacheSchemaProvider.Remove(dataSourceName);
            }
        }

        public static bool IsCached(string dataSourceName)
        {
            return CacheSchemaProvider.IsCached(dataSourceName);
        }

        public static IEnumerable<string> KnownDataSourceNames
        {
            get { return CacheSchemaProvider.CachedDataSourceNames; }
        }

        public static VhayuSchema Load(string dataSourceName)
        {
            VhayuSchema schema = null;
            if (CacheSchemaProvider.IsCached(dataSourceName)) {
                try {
                    schema = CacheSchemaProvider.Load(dataSourceName);
                } catch (Exception ex) {
                    Console.WriteLine(ex);
                }
            }
            if (schema == null) {
                schema = ServerSchemaProvider.Load(dataSourceName);
                CacheSchemaProvider.Save(dataSourceName, schema);
            }
            SetParentReferences(schema);
            return schema;
        }

        public static void LoadMetaData(
            VhayuSchema schema, Procedure procedure)
        {
            ServerSchemaProvider.LoadMetaData(schema, procedure);
            CacheSchemaProvider.Save(schema.DataSourceName, schema);
        }

        public static VhayuSchema Reload(string dataSourceName)
        {
            VhayuSchema schema = null;
            try {
                schema = ServerSchemaProvider.Load(dataSourceName);
                CacheSchemaProvider.Save(dataSourceName, schema);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw ex;
            }
            SetParentReferences(schema);
            return schema;
        }

        internal static void SetParentReferences(VhayuSchema schema)
        {
            foreach (ProcedureType type in schema.ProcedureTypes) {
                type.Schema = schema;
                foreach (Procedure procedure in type.Procedures) {
                    procedure.Type = type;
                }
            }
        }
    }
}
