﻿select
        PP.RDB$PARAMETER_NAME,
        PP.RDB$PARAMETER_NUMBER,
        PP.RDB$PARAMETER_TYPE,
		PP.RDB$FIELD_SOURCE,
        PP.RDB$DESCRIPTION,
        F.RDB$DEFAULT_VALUE,
        F.RDB$DEFAULT_SOURCE,
        F.RDB$FIELD_LENGTH,
        F.RDB$FIELD_TYPE
from
        RDB$PROCEDURE_PARAMETERS as PP
        left outer join
        RDB$FIELDS as F
        on PP.RDB$FIELD_SOURCE = F.RDB$FIELD_NAME
where
        PP.RDB$PROCEDURE_NAME = ?
order by
        PP.RDB$PARAMETER_TYPE,
        PP.RDB$PARAMETER_NUMBER