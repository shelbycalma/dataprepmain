﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panopticon.VhayuPlugin.Schema
{
    public enum FieldType : short
    {
        SmallInt = 7,
        Int = 8,
        Date = 12,
        Time = 13,
        Char = 14,
        BigInt = 16,
        DoublePrecision = 27,
        Timestamp = 35,
        VarChar = 37,
        Blob = 261
    }
}
