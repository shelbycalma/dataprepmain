﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Data.Odbc;

namespace Panopticon.VhayuPlugin.Schema
{
    internal static class ServerSchemaProvider
    {
        public static string BuildConnectionString(string dsn)
        {
            return string.Format("Dsn={0}", dsn);
        }

        private static void FromDescriptionTable(
            Procedure procedure, DataTable table)
        {
            DataRow row = table.Rows[0];
            if (!row.IsNull("RDB$DESCRIPTION")) {
                procedure.Description =
                    ((string) row["RDB$DESCRIPTION"]).TrimEnd();
            }
        }

        private static void FromFieldsTable(
            Procedure procedure, DataTable table)
        {
            foreach (DataRow row in table.Rows) {
                Field field = new Field() {
                    ParameterName =
                        ((string) row["RDB$PARAMETER_NAME"]).TrimEnd(),
                    FieldSource =
                        ((string) row["RDB$FIELD_SOURCE"]).TrimEnd(),
                    FieldLength = (short) row["RDB$FIELD_LENGTH"]
                };
                if (field.FieldSource.StartsWith("RDB$") &&
                    field.FieldSource.Substring(4)
                        .ToCharArray().All(c => char.IsDigit(c))) {
                    field.FieldSource = null;
                }
                short fieldType = (short) row["RDB$FIELD_TYPE"];
                if (!Enum.IsDefined(typeof(FieldType), fieldType)) {
                    throw new Exception(string.Format(
                        Properties.Resources.UiUnknownFieldType, fieldType,
                        field.ParameterName, procedure.ProcedureName));
                }
                field.FieldType = (FieldType) fieldType;
                if (!row.IsNull("RDB$DESCRIPTION")) {
                    field.Description = (string) row["RDB$DESCRIPTION"];
                }
                if (!row.IsNull("RDB$DEFAULT_VALUE")) {
                    field.DefaultValue = (byte[]) row["RDB$DEFAULT_VALUE"];
                }
                if (!row.IsNull("RDB$DEFAULT_SOURCE")) {
                    field.DefaultSource =
                        (string) row["RDB$DEFAULT_SOURCE"];
                }
                short type = (short) row["RDB$PARAMETER_TYPE"];
                if (type == 0) {
                    procedure.Inputs.Add(field);
                } else {
                    procedure.Outputs.Add(field);
                }
            }
        }

        private static VhayuSchema FromProceduresTable(DataTable table)
        {
            VhayuSchema schema = new VhayuSchema();
            
            Dictionary<int, ProcedureType> typeLookup =
                new Dictionary<int, ProcedureType>();
            Dictionary<ProcedureType, int> typeOrderLookup =
                new Dictionary<ProcedureType, int>();

            foreach (DataRow row in table.Rows) {
                Procedure procedure = new Procedure() {
                    ProcedureName =
                        ((string) row["RDB$PROCEDURE_NAME"]).TrimEnd(),
                    ProcedureType = (short) row["RDB$PROCEDURE_TYPE"],
                    User = ((string) row["RDB$USER"]).TrimEnd()
                };
                if (!row.IsNull("YFILEINFO_EXISTS")) {
                    int yFiles = (int) row["YFILEINFO_EXISTS"];
                    procedure.YFileInfoExists = yFiles == 1;
                }
                if (!row.IsNull("DISPLAY_NAME")) {
                    string displayName = (string) row["DISPLAY_NAME"];
                    if (!procedure.ProcedureName.Equals(displayName)) {
                        procedure.DisplayName = displayName;
                    }
                }
                int typeValue = -1;
                if (!row.IsNull("STOREDPROC_TYPE")) {
                    typeValue = (int) row["STOREDPROC_TYPE"];
                }
                ProcedureType type = null;
                if (!typeLookup.TryGetValue(typeValue, out type)) {
                    type = new ProcedureType();
                    int order = int.MaxValue;
                    if (typeValue >= 0) {
                        type.TypeName = (string) row["TYPE_NAME"];
                        order = (int) row["DISPLAY_ORDER"];
                    } else {
                        type.TypeName = "(unknown)";
                    }
                    schema.ProcedureTypes.Add(type);
                    typeLookup.Add(typeValue, type);
                    typeOrderLookup.Add(type, order);
                }
                type.Procedures.Add(procedure);
            }

            schema.ProcedureTypes.Sort((x, y) => {
                int xo = typeOrderLookup[x];
                int yo = typeOrderLookup[y];
                return xo.CompareTo(yo);
            });

            return schema;
        }

        private static string GetDescriptionQuery()
        {
            return GetQuery("Description");
        }

        private static string GetFieldsQuery()
        {
            return GetQuery("Fields");
        }

        private static string GetProceduresQuery()
        {
            return GetQuery("Procedures");
        }

        private static string GetQuery(string name)
        {
            string query;
            Type type = typeof(ServerSchemaProvider);
            string resource = string.Format("{0}.sql", name);
            using (Stream stream =
                type.Assembly.GetManifestResourceStream(type, resource))
            using (StreamReader reader = new StreamReader(stream)) {
                query = reader.ReadToEnd();
            }
            return query;
        }

        private static void LoadDescription(
            VhayuSchema schema, Procedure procedure)
        {
            string dsn = schema.DataSourceName;
            string procedureName = procedure.ProcedureName;
            DataTable description = LoadDescriptionTable(dsn, procedureName);
            FromDescriptionTable(procedure, description);
        }

        private static DataTable LoadDescriptionTable(
            string dsn, string procedureName)
        {
            DataTable description = null;
            string connectionString = BuildConnectionString(dsn);
            string descriptionQuery = GetDescriptionQuery();
            try {
                using (OdbcConnection connection =
                    new OdbcConnection(connectionString))
                using (OdbcDataAdapter adapter =
                    new OdbcDataAdapter(descriptionQuery, connection)) {
                    description = new DataTable("Description");
                    adapter.SelectCommand.Parameters.Add(
                        "@PROCEDURE_NAME", OdbcType.VarChar, 31).Value =
                            procedureName;
                    adapter.Fill(description);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw ex;
            }
            return description;
        }

        private static void LoadFields(VhayuSchema schema, Procedure procedure)
        {
            string dsn = schema.DataSourceName;
            string procedureName = procedure.ProcedureName;
            DataTable fields = LoadFieldsTable(dsn, procedureName);
            FromFieldsTable(procedure, fields);
        }

        private static DataTable LoadFieldsTable(
            string dsn, string procedureName)
        {
            DataTable fields = null;
            string connectionString = BuildConnectionString(dsn);
            string fieldsQuery = GetFieldsQuery();
            try {
                using (OdbcConnection connection =
                    new OdbcConnection(connectionString))
                using (OdbcDataAdapter adapter =
                    new OdbcDataAdapter(fieldsQuery, connection)) {
                    fields = new DataTable("Fields");
                    adapter.SelectCommand.Parameters.Add(
                        "@PROCEDURE_NAME", OdbcType.VarChar, 31).Value =
                            procedureName;
                    adapter.Fill(fields);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw ex;
            }
            return fields;
        }

        public static void LoadMetaData(
            VhayuSchema schema, Procedure procedure)
        {
            LoadDescription(schema, procedure);
            LoadFields(schema, procedure);
            schema.LastUpdated = DateTime.Now;
        }

        public static VhayuSchema Load(string dataSourceName)
        {
            DataTable procedures = LoadProceduresTable(dataSourceName);
            VhayuSchema model = FromProceduresTable(procedures);
            model.DataSourceName = dataSourceName;
            model.LastUpdated = DateTime.Now;
            return model;
        }

        private static DataTable LoadProceduresTable(string dsn)
        {
            DataTable procedures = null;
            string connectionString = BuildConnectionString(dsn);
            string proceduresQuery = GetProceduresQuery();
            try {
                using (OdbcConnection connection =
                    new OdbcConnection(connectionString))
                using (OdbcDataAdapter adapter =
                    new OdbcDataAdapter(proceduresQuery, connection)) {
                    procedures = new DataTable("Procedures");
                    adapter.Fill(procedures);
                }
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw ex;
            }
            return procedures;
        }
    }
}
