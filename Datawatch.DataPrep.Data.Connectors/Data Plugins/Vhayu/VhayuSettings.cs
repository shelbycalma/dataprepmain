﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.VhayuPlugin
{
    public class VhayuSettings : ConnectionSettings
    {
        private const string DataSourceNameProperty = "DataSourceName";
        
        private const string ProcedureNameProperty = "ProcedureName";
        
        private const string ParameterInputDirection = "Input";
        private const string ParameterOutputDirection = "Output";
        private const string ParameterCountProperty = "{0}ParameterCount";
        private const string ParameterNameProperty = "{0}Parameter_{1}_Name";
        private const string ParameterTypeProperty = "{0}Parameter_{1}_Type";
        private const string ParameterVhayuTypeProperty = "{0}Parameter_{1}_VhayuType";
        private const string ParameterValueProperty = "{0}Parameter_{1}_Value";

        private const string LastSelectedProcedureTypeProperty = "LastSelectedProcedureType";
        private const string LastSelectedProcedureProperty = "LastSelectedProcedure";

        private const string GuessTimeTypeProperty = "GuessTimeType";
        private const bool GuessTimeTypeDefault = true;

        private const string SqlProperty = "Sql";

        private const string TimeZoneProperty = "TimeZone";
        private const string TimeZoneDefaultValue = "null";
        
        public VhayuSettings()
        {
        }

        public VhayuSettings(PropertyBag properties)
            : base(properties)
        {
        }

        public string DataSourceName
        {
            get { return GetInternal(DataSourceNameProperty); }
            set { SetInternal(DataSourceNameProperty, value); }
        }

        public string LastSelectedProcedureType
        {
            get { return GetInternal(LastSelectedProcedureTypeProperty); }
            set { SetInternal(LastSelectedProcedureTypeProperty, value); }
        }

        public string LastSelectedProcedure
        {
            get { return GetInternal(LastSelectedProcedureProperty); }
            set { SetInternal(LastSelectedProcedureProperty, value); }
        }

        public string ProcedureName
        {
            get { return GetInternal(ProcedureNameProperty); }
            set { SetInternal(ProcedureNameProperty, value); }
        }

        public VhayuParameter[] InputParameters
        {
            get { return GetParameters(ParameterInputDirection); }
            set { SetParameters(ParameterInputDirection, value); }
        }

        public VhayuParameter[] OutputParameters
        {
            get { return GetParameters(ParameterOutputDirection); }
            set { SetParameters(ParameterOutputDirection, value); }
        }

        private VhayuParameter[] GetParameters(string direction)
        {
            int count = GetInternalInt(
                GetParameterCountProperty(direction), 0);
            VhayuParameter[] parameters = new VhayuParameter[count];
            for (int i = 0; i < count; i++) {
                parameters[i] = GetParameter(direction, i);
            }
            return parameters;
        }

        private void SetParameters(
            string direction, VhayuParameter[] parameters)
        {
            if (parameters == null) {
                throw Exceptions.ArgumentNull("parameters");
            }
            ClearParameters(direction);
            int count = parameters.Length;
            SetInternalInt(GetParameterCountProperty(direction), count);
            for (int i = 0; i < count; i++) {
                SetParameter(direction, i, parameters[i]);
            }
        }

        private int GetParameterCount(string direction)
        {
            return GetInternalInt(GetParameterCountProperty(direction), 0);
        }

        private void SetParameterCount(string direction, int count)
        {
            SetInternalInt(GetParameterCountProperty(direction), count);
        }

        private string GetParameterCountProperty(string direction)
        {
            return string.Format(ParameterCountProperty, direction);
        }

        private VhayuParameter GetParameter(string direction, int index)
        {
            string name = GetInternal(
                GetParameterNameProperty(direction, index));
            string typeText = GetInternal(
                GetParameterTypeProperty(direction, index));
            string vhayuType = GetInternal(
                GetParameterVhayuTypeProperty(direction, index));
            string value = GetInternal(
                GetParameterValueProperty(direction, index));

            VhayuParameterType type = (VhayuParameterType)
                Enum.Parse(typeof(VhayuParameterType), typeText);

            return new VhayuParameter(name, type, vhayuType) { Value = value };
        }

        protected void SetParameter(
            string direction, int index, VhayuParameter parameter)
        {
            string typeText = Enum.Format(
                typeof(VhayuParameterType), parameter.Type, "G");

            SetInternal(
                GetParameterNameProperty(direction, index), parameter.Name);
            SetInternal(
                GetParameterTypeProperty(direction, index), typeText);
            SetInternal(
                GetParameterVhayuTypeProperty(direction, index),
                parameter.VhayuType);
            SetInternal(
                GetParameterValueProperty(direction, index), parameter.Value);
        }

        private string GetParameterNameProperty(string direction, int index)
        {
            return string.Format(ParameterNameProperty, direction, index);
        }

        private string GetParameterTypeProperty(string direction, int index)
        {
            return string.Format(ParameterTypeProperty, direction, index);
        }

        private string GetParameterValueProperty(string direction, int index)
        {
            return string.Format(ParameterValueProperty, direction, index);
        }

        private string GetParameterVhayuTypeProperty(
            string direction, int index)
        {
            return string.Format(ParameterVhayuTypeProperty, direction, index);
        }

        private void ClearParameters(string direction)
        {
            int count = GetParameterCount(direction);
            for (int i = 0; i < count; i++) {
                SetInternal(GetParameterNameProperty(direction, i), null);
                SetInternal(GetParameterTypeProperty(direction, i), null);
                SetInternal(GetParameterVhayuTypeProperty(direction, i), null);
                SetInternal(GetParameterValueProperty(direction, i), null);
            }
            SetParameterCount(direction, 0);
        }

        public bool GuessTimeType
        {
            get {
                bool guess = GuessTimeTypeDefault;
                string guessText = GetInternal(GuessTimeTypeProperty);
                if (!string.IsNullOrEmpty(guessText)) {
                    try {
                        guess = Convert.ToBoolean(guessText);
                    } catch (Exception) {
                        Log.Warning(Properties.Resources.LogInvalidSettingsFor,
                            Plugin.PluginId, GuessTimeTypeProperty, guessText);
                    }
                }
                return guess;
            }
            set {
                string guessText = Convert.ToString(value);
                SetInternal(GuessTimeTypeProperty, guessText);
            }
        }

        public string Sql
        {
            get { return GetInternal(SqlProperty); }
            set { SetInternal(SqlProperty, value); }
        }

        public string TimeZone
        {
            get { return GetInternal(TimeZoneProperty, TimeZoneDefaultValue); }
            set 
            {
                if (value.Length <= 0)
                {
                    SetInternal(TimeZoneProperty, TimeZoneDefaultValue);
                }
                else
                {
                    SetInternal(TimeZoneProperty, value);
                }
            }
        }
    }
}
