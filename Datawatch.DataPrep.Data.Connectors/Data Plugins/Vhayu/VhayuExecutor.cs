﻿using System;
using System.Collections.Generic;
using System.Text;
using Panopticon.VhayuPlugin.Schema;
using System.Data;
using System.Data.Odbc;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Ado;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.VhayuPlugin.Properties;

namespace Panopticon.VhayuPlugin
{
    public class VhayuExecutor
    {
        private const string VhayuVtsfmtFormat = "%Y-%m-%d %H:%M:%S.%4N";
        private const string VhayuVtfmtFormat = "%Y-%m-%d %H:%M:%S.%3N";
        private const string dateTimeFormat = "yyyy-MM-dd HH:mm:ss.fff";

        private static string BuildInputParameter(VhayuParameter parameter,string timeZone)
        {
            string value = parameter.Value;
            if (value == null) {
                value = "?";
            }
            if (parameter.Type == VhayuParameterType.Numeric) {
                return value;
            }
            if (parameter.Type == VhayuParameterType.Text) {
                return string.Format("'{0}'", value);
            }
            if (!parameter.IsVhayuTime) {
                // Uh oh. Date, Time, or Timestamp.
                return value;
            }
            // VA_DTYPE_TIME
            //change the value to required format first
            DateTime dt;
            if (DateTime.TryParse(value, out dt))
            {
                value = dt.ToString(dateTimeFormat);
            }

            //if timezone is selected enclose it in single quotes else not
            if ("null".Equals(timeZone))
            {
                return string.Format("vtfmt('{0}', " +
                "'" + VhayuVtfmtFormat + "', null)", value);
            }
            else
            {
                return string.Format("vtfmt('{0}', " +
                "'" + VhayuVtfmtFormat + "','" + timeZone + "')", value);
            }
            
        }

        private static string BuildOutputParameter(VhayuParameter parameter, string timeZone)
        {
            string quoted = string.Format("\"{0}\"", parameter.Name);
            if (parameter.Type != VhayuParameterType.Time) {
                return quoted;
            }
            if (!parameter.IsVhayuTime) {
                return quoted;
            }

            //if timezone is selected enclose it in single quotes else not
            if ("null".Equals(timeZone))
            {
                return string.Format("iif({0} is null,null," +
                    "cast(vtsfmt({0}, " +
                "'" + VhayuVtsfmtFormat + "', null) " +
                "as timestamp)) as {0}", quoted);
            }
            else
            {
                return string.Format("iif({0} is null,null," +
                    "cast(vtsfmt({0}, " +
                "'" + VhayuVtsfmtFormat + "','" + timeZone + "') " +
                "as timestamp)) as {0}", quoted);
            }

        }

        public static string BuildQuery(VhayuSettings settings)
        {
            return BuildQuery(settings, -1);
        }

        public static string BuildQuery(VhayuSettings settings, int maxRows)
        {
            if (settings.Sql != null) {
                return settings.Sql;
            }
            if (string.IsNullOrEmpty(settings.ProcedureName)) {
                return null;
            }
            StringBuilder query = new StringBuilder();
            query.Append("select");
            if (maxRows > 0) {
                query.Append(" first ");
                query.Append(maxRows);
            }
            query.AppendLine();
            for (int i = 0; i < settings.OutputParameters.Length; i++) {
                VhayuParameter parameter = settings.OutputParameters[i];
                query.Append("  ");
                query.Append(BuildOutputParameter(parameter,settings.TimeZone));
                if (i < settings.OutputParameters.Length - 1) {
                    query.Append(',');
                }
                query.AppendLine();
            }
            query.Append("from ");
            query.Append(settings.ProcedureName);
            if (settings.InputParameters.Length > 0) {
                query.AppendLine("(");
                for (int i = 0; i < settings.InputParameters.Length; i++) {
                    VhayuParameter parameter = settings.InputParameters[i];
                    query.Append("  ");
                    query.Append(BuildInputParameter(parameter, settings.TimeZone));
                    if (i < settings.InputParameters.Length - 1) {
                        query.Append(',');
                    }
                    query.Append("\t-- ");
                    query.Append(parameter.Name);
                    query.AppendLine();
                }
                query.Append(')');
            }
            query.AppendLine();
            return query.ToString();
        }

        public static ITable Execute(VhayuSettings settings,
            IEnumerable<ParameterValue> parameters, int maxRows)
        {
            string parameterized = BuildQuery(settings, maxRows);
            string instantiated = ReplaceParameters(parameterized, parameters);
            DataTable ado = Execute(settings.DataSourceName, instantiated);
            ITable table = new AdoTable(ado);
            return table;
        }

        private static DataTable Execute(string dsn, string query)
        {
            Log.Info(Properties.Resources.LogPluginExecutingQuery, query);
            DateTime start = DateTime.Now;

            DataTable result = null;
            string connectionString =
                ServerSchemaProvider.BuildConnectionString(dsn);
            try {
                using (OdbcConnection connection =
                    new OdbcConnection(connectionString))
                using (OdbcDataAdapter adapter =
                    new OdbcDataAdapter(query, connection)) {
                    result = new DataTable();
                    adapter.Fill(result);
                }

                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryCompleted,
                    result.Rows.Count, result.Columns.Count, seconds);
            } catch (Exception ex) {
                Console.WriteLine(ex);
                throw ex;
            }
            return result;
        }

        private static string ReplaceParameters(
            string query, IEnumerable<ParameterValue> parameters)
        {
            ParameterEncoder encoder = new ParameterEncoder() {
                SourceString = query,
                Parameters = parameters
            };
            string encoded = encoder.Encoded();
            return encoded;
        }
    }
}
