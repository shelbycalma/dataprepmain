﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace Panopticon.VhayuPlugin
{
    public class VhayuParameter : INotifyPropertyChanged
    {
        private const string FieldSourceTime = "VA_DTYPE_TIME";
        private const string FieldSourceSymbol = "VA_DTYPE_SYMBOL";
        private const string ImplicitTimeFieldSuffix = "TIME";

        public event PropertyChangedEventHandler PropertyChanged;

        private string name;
        private VhayuParameterType type;
        private string value;

        private string vhayuType;

        public VhayuParameter()
        {
        }

        public VhayuParameter(string name,
            VhayuParameterType type, string vhayuType)
        {
            this.name = name;
            this.type = type;
            this.vhayuType = vhayuType;
        }

        public static VhayuParameter FromField(
            Schema.Field field, bool guessTimeType)
        {
            string name = field.ParameterName;
            VhayuParameterType type = GetFieldType(field, guessTimeType);
            string vhayuType = GetVhayuFieldType(field);

            return new VhayuParameter(name, type, vhayuType);
        }

        private static VhayuParameterType GetFieldType(
            Schema.Field field, bool guessTimeType)
        {
            if (FieldSourceTime.Equals(field.FieldSource)) {
                return VhayuParameterType.Time;
            }

            if (guessTimeType && IsImplicitTimeType(field)) {
                return VhayuParameterType.Time;
            }

            switch (field.FieldType)
            {
                case Schema.FieldType.SmallInt:
                case Schema.FieldType.Int:
                case Schema.FieldType.BigInt:
                case Schema.FieldType.DoublePrecision:
                    return VhayuParameterType.Numeric;

                case Schema.FieldType.Date:
                case Schema.FieldType.Time:
                case Schema.FieldType.Timestamp:
                    return VhayuParameterType.Time;

                default:
                    return VhayuParameterType.Text;
            }
        }

        private static string GetVhayuFieldType(Schema.Field field)
        {
            if (FieldSourceSymbol.Equals(field.FieldSource)) {
                return FieldSourceSymbol;
            }
            if (FieldSourceTime.Equals(field.FieldSource)) {
                return FieldSourceTime;
            }

            string type = Enum.Format(
                typeof(Schema.FieldType), field.FieldType, "G");

            return type;
        }

        private static bool IsImplicitTimeType(Schema.Field field)
        {
            if (field.FieldType == Schema.FieldType.BigInt &&
                field.ParameterName.EndsWith(ImplicitTimeFieldSuffix,
                    StringComparison.InvariantCultureIgnoreCase)) {
                return true;
            }
            return false;
        }

        public bool IsVhayuTime
        {
            get {
                return FieldSourceTime.Equals(this.VhayuType) ||
                    (this.Type == VhayuParameterType.Time &&
                     Schema.FieldType.BigInt == (Schema.FieldType)
                        Enum.Parse(typeof(Schema.FieldType), this.VhayuType));
            }
        }

        public string Name
        {
            get { return name; }
            set {
                if (value != name) {
                    name = value;
                    NotifyChanged("Name");
                }
            }
        }

        // The Datawatch data type (text/numeric/time) that the parameter is mapped
        // to. Usually straight-forward, except that in some cases you can't
        // tell from Vhayu's meta data that a time parameter is time. The
        // underlying data type for Vhayu's tick type is BigInt, so you can
        // tell the connector to treat any columns whos name ends in "time"
        // and is of BigInt type as a Vhayu time. See GetFieldType and also
        // IsImplicitTimeType and IsVahyuTime.
        public VhayuParameterType Type
        {
            get { return type; }
            set {
                if (value != type) {
                    type = value;
                    NotifyChanged("Type");
                }
            }
        }

        public string Value
        {
            get { return value; }
            set {
                if (value != this.value) {
                    this.value = value;
                    NotifyChanged("Value");
                }
            }
        }

        // The underlying field type as reported by Vhayu. This will be the
        // Schema.Field.FieldType except if Schema.Field.FieldSource is set
        // to VA_DTYPE_TIME or VA_DTYPE_SYMBOL, when it will be one of those
        // values instead (it's a type of extended types used in the Vhayu
        // schema). See GetVhayuFieldType.
        public string VhayuType
        {
            get { return vhayuType; }
            set {
                if (value != vhayuType) {
                    vhayuType = value;
                    NotifyChanged("VhayuType");
                }
            }
        }

        protected void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null) {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
