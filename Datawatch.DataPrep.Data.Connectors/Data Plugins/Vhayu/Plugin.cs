﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.VhayuPlugin.Properties;

// TODO: Change location of schema cache - Temp folder?
// TODO: Fallback if cache writing fails (e.g. on server).

namespace Panopticon.VhayuPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<VhayuSettings>
    {
        internal const string PluginId = "VhayuPlugin";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "Thomson Reuters Velocity Analytics";
        internal const string PluginType = DataPluginTypes.Database;

        protected override void Dispose(bool disposing)
        {
            if (disposing) {
            }
        }

        public override VhayuSettings CreateSettings(PropertyBag properties)
        {
            VhayuSettings settings = new VhayuSettings(properties);
            if (string.IsNullOrEmpty(settings.Title)) {
                settings.Title = Resources.UiDefaultConnectionTitle;
            }
            return settings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            VhayuSettings settings = CreateSettings(properties);

            return VhayuExecutor.Execute(settings, parameters, maxRowCount);
        }


        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
