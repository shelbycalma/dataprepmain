﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Salesforce;

namespace Panopticon.CData.UI.Salesforce.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Salesforce.Plugin, SalesforceConnectionSettings>
    {
        public PluginUI(CData.Salesforce.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(SalesforceConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new SalesforceConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            SalesforceConfigPanel panel = (SalesforceConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
