﻿using System;

namespace Panopticon.CData.UI.Salesforce.UI.UiHelpers
{
    public class FolderSelection : IComparable<FolderSelection>
    {
        public int CompareTo(FolderSelection other)
        {
            return String.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);
        }

        public override string ToString()
        {
            if (String.IsNullOrEmpty(Name))
            {
                return DeveloperName;
            }
            return Name;
        }

        public string DeveloperName { get; set; }
        public string Name { get; set; }
        public string ID { get; set; }
    }
}