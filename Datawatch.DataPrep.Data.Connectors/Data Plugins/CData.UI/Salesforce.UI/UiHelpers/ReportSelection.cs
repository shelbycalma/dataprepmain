﻿using System;

namespace Panopticon.CData.UI.Salesforce.UI.UiHelpers
{
    public class ReportSelection : IComparable<ReportSelection>
    {
        public int CompareTo(ReportSelection other)
        {
            return String.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);
        }

        public override string ToString()
        {
            return Name;
        }

        public string Name { get; set; }
        public string ID { get; set; }
        public string OwnerID { get; set; }
    }
}