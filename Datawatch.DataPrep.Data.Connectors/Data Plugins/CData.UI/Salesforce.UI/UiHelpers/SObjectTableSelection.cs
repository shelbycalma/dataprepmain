﻿using System;

namespace Panopticon.CData.UI.Salesforce.UI.UiHelpers
{
    public class SObjectTableSelection : IComparable<SObjectTableSelection>
    {
        public int CompareTo(SObjectTableSelection other)
        {
            return String.Compare(Name, other.Name, StringComparison.OrdinalIgnoreCase);
        }

        public override string ToString()
        {
            return Label;
        }

        public string Name { get; set; }
        public string Label { get; set; }

        public SObjectTableSelection(string name, string label)
        {
            Name = name;
            Label = label;
        }
    }
}