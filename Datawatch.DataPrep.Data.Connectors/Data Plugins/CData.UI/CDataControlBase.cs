﻿using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.CData.UI
{
    public class CDataControlBase : UserControl,
        IDataPluginConfigElement
    {
        protected DatabaseSettingsBase settings;

        public CDataControlBase(DatabaseSettingsBase settings)
        {
            this.settings = settings;
        }

        public DatabaseSettingsBase Settings
        {
            get { return settings; }
        }

        public void OnOk()
        {
            if (Settings == null)
            {
                return;
            }

            // This method is called when Ok button in DataSourceSettingsDialog is clicked.
            // Clear the column here, so that even if settings are not chagned, latest schema from 
            // DB is pulled.
            Settings.QuerySettings.SchemaColumnsSettings.ClearSchemaColumns();
        }

        public bool IsOk
        {
            get
            {
                if (Settings == null) return false;

                return Settings.IsOk;
            }
        }
    }
}
