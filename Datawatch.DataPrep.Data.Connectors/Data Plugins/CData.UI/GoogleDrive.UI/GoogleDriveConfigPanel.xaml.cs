﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.GoogleDrive;

namespace Panopticon.CData.UI.GoogleDrive.UI
{
    public partial class GoogleDriveConfigPanel
	{
        public GoogleDriveConfigPanel()
            : this(null)
        {
        }

        public GoogleDriveConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as GoogleDriveConnectionSettings;
        }

    }
}
