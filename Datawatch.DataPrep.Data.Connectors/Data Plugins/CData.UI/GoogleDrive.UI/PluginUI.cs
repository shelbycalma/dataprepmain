﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.GoogleDrive;

namespace Panopticon.CData.UI.GoogleDrive.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.GoogleDrive.Plugin, GoogleDriveConnectionSettings>
    {
        public PluginUI(CData.GoogleDrive.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(GoogleDriveConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new GoogleDriveConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			GoogleDriveConfigPanel panel = (GoogleDriveConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
