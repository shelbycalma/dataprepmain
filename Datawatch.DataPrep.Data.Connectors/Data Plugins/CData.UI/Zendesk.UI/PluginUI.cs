﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Zendesk;

namespace Panopticon.CData.UI.Zendesk.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Zendesk.Plugin, ZendeskConnectionSettings>
    {
        public PluginUI(CData.Zendesk.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override object GetConfigPanel(ZendeskConnectionSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new ZendeskConfigPanel(settings);
        }

        public override PropertyBag ToPropertyBag(object element)
        {
            ZendeskConfigPanel panel = (ZendeskConfigPanel)element;
            return panel.Settings.ToPropertyBag();
        }
    }
}
