﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.Hubspot;

namespace Panopticon.CData.UI.Hubspot.UI
{
    public partial class HubspotConfigPanel
	{
        public HubspotConfigPanel()
            : this(null)
        {
        }

        public HubspotConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as HubspotConnectionSettings;
        }

    }
}
