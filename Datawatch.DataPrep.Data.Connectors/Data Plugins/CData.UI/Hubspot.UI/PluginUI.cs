﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Hubspot;

namespace Panopticon.CData.UI.Hubspot.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Hubspot.Plugin, HubspotConnectionSettings>
    {
        public PluginUI(CData.Hubspot.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(HubspotConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new HubspotConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			HubspotConfigPanel panel = (HubspotConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
