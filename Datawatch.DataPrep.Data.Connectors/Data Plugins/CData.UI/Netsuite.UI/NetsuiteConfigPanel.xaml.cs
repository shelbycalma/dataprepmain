﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.Netsuite;

namespace Panopticon.CData.UI.Netsuite.UI
{
    public partial class NetsuiteConfigPanel
	{
        public NetsuiteConfigPanel()
            : this(null)
        {
        }

        public NetsuiteConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as NetsuiteConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
