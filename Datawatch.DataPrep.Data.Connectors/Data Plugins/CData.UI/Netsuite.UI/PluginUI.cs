﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Netsuite;

namespace Panopticon.CData.UI.Netsuite.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Netsuite.Plugin, NetsuiteConnectionSettings>
    {
        public PluginUI(CData.Netsuite.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(NetsuiteConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new NetsuiteConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			NetsuiteConfigPanel panel = (NetsuiteConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
