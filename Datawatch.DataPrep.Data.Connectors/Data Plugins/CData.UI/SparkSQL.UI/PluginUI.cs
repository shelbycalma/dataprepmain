﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.SparkSQL;

namespace Panopticon.CData.UI.SparkSQL.UI
{
	public class PluginUI : OdbcPluginUIBase<Plugin, SparkSQLConnectionSettings>
	{
		public PluginUI(Plugin plugin, Window owner)
			: base(plugin, owner)
		{
		}

		public override object GetConfigPanel(SparkSQLConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			// Uptill now we only need basic UI for Host/Port etc.
			// So DatabaseConfigPanel should work.
			return new CDataConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			CDataConfigPanel panel = (CDataConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}