﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Jira;

namespace Panopticon.CData.UI.Jira.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Jira.Plugin, JiraConnectionSettings>
    {
        public PluginUI(CData.Jira.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(JiraConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new JiraConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			JiraConfigPanel panel = (JiraConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
