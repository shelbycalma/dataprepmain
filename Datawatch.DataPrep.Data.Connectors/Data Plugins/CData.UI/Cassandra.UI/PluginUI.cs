﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Cassandra;

namespace Panopticon.CData.UI.Cassandra.UI
{
	public class PluginUI : OdbcPluginUIBase<Plugin, CassandraConnectionSettings>
	{
		public PluginUI(Plugin plugin, Window owner)
			: base(plugin, owner)
		{
		}

		public override object GetConfigPanel(CassandraConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new CDataConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			CDataConfigPanel panel = (CDataConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}