﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.ActiveDirectory;

namespace Panopticon.CData.UI.ActiveDirectory.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.ActiveDirectory.Plugin, ActiveDirectoryConnectionSettings>
    {
        public PluginUI(CData.ActiveDirectory.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(ActiveDirectoryConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new ActiveDirectoryConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			ActiveDirectoryConfigPanel panel = (ActiveDirectoryConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
