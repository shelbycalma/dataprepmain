﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.GoogleAdWords;

namespace Panopticon.CData.UI.GoogleAdWords.UI
{
    public partial class GoogleAdWordsConfigPanel
	{
        public GoogleAdWordsConfigPanel()
            : this(null)
        {
        }

        public GoogleAdWordsConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as GoogleAdWordsConnectionSettings;
            if (settings != null)
            {
//                PasswordBox.Password = settings.Password;
            }
        }
    }
}
