﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.GoogleAdWords;

namespace Panopticon.CData.UI.GoogleAdWords.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.GoogleAdWords.Plugin, GoogleAdWordsConnectionSettings>
    {
        public PluginUI(CData.GoogleAdWords.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(GoogleAdWordsConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new GoogleAdWordsConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			GoogleAdWordsConfigPanel panel = (GoogleAdWordsConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
