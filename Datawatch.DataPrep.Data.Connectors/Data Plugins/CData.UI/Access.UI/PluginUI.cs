﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Access;

namespace Panopticon.CData.UI.Access.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Access.Plugin, AccessConnectionSettings>
    {
        public PluginUI(CData.Access.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(AccessConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new AccessConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			AccessConfigPanel panel = (AccessConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
