﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Microsoft.Win32;
using Panopticon.CData.Access;

namespace Panopticon.CData.UI.Access.UI
{
    public partial class AccessConfigPanel
	{
        public AccessConfigPanel()
            : this(null)
        {
        }

        public AccessConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
        }

		private void OpenFileDialog_OnClick(object sender, RoutedEventArgs e)
		{
			var element = sender as FrameworkElement;
			settings = DataContext as AccessConnectionSettings;
			var dialog = new OpenFileDialog();
			dialog.Title = "Open Microsoft Access File";
			dialog.Filter = "Microsoft Access Files (*.accdb;*.mdb)|*.accdb;*.mdb|All Files (*.*)|*.*";
			if (dialog.ShowDialog() != true)
				return;
			
			settings.DatabaseName = dialog.FileName;
		}

	}
}
