﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.GoogleBigQuery;

namespace Panopticon.CData.UI.GoogleBigQuery.UI
{
    public partial class GoogleBigQueryConfigPanel
	{
        public GoogleBigQueryConfigPanel()
            : this(null)
        {
        }

        public GoogleBigQueryConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as GoogleBigQueryConnectionSettings;
        }
    }
}
