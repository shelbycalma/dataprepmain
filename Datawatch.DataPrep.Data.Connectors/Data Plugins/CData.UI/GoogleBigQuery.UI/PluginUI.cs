﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.GoogleBigQuery;

namespace Panopticon.CData.UI.GoogleBigQuery.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.GoogleBigQuery.Plugin, GoogleBigQueryConnectionSettings>
    {
        public PluginUI(CData.GoogleBigQuery.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(GoogleBigQueryConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new GoogleBigQueryConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			GoogleBigQueryConfigPanel panel = (GoogleBigQueryConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
