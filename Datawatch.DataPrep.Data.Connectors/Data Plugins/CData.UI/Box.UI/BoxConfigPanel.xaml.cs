﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.Box;

namespace Panopticon.CData.UI.Box.UI
{
    public partial class BoxConfigPanel
	{
        public BoxConfigPanel()
            : this(null)
        {
        }

        public BoxConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as BoxConnectionSettings;
        }

    }
}
