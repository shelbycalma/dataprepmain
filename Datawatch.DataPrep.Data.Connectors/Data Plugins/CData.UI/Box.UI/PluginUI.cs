﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Box;

namespace Panopticon.CData.UI.Box.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Box.Plugin, BoxConnectionSettings>
    {
        public PluginUI(CData.Box.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(BoxConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new BoxConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			BoxConfigPanel panel = (BoxConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
