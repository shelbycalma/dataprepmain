﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.GoogleSheets;

namespace Panopticon.CData.UI.GoogleSheets.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.GoogleSheets.Plugin, GoogleSheetsConnectionSettings>
    {
        public PluginUI(CData.GoogleSheets.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(GoogleSheetsConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new GoogleSheetsConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			GoogleSheetsConfigPanel panel = (GoogleSheetsConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
