﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.GoogleSheets;

namespace Panopticon.CData.UI.GoogleSheets.UI
{
    public partial class GoogleSheetsConfigPanel
	{
        public GoogleSheetsConfigPanel()
            : this(null)
        {
        }

        public GoogleSheetsConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as GoogleSheetsConnectionSettings;
        }

    }
}
