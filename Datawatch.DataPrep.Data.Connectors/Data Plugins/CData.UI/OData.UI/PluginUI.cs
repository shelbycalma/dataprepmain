﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.OData;

namespace Panopticon.CData.UI.OData.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.OData.Plugin, ODataConnectionSettings>
    {
        public PluginUI(CData.OData.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(ODataConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new ODataConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			ODataConfigPanel panel = (ODataConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
