﻿using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Panopticon.CData.Sharepoint;

namespace Panopticon.CData.UI.Sharepoint.UI
{
    public partial class SharepointConfigPanel
	{
        public SharepointConfigPanel()
            : this(null)
        {
        }

        public SharepointConfigPanel(DatabaseSettingsBase settings)
            : base(settings)
        {
            InitializeComponent();
            DataContext = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
            PasswordBox.Password = settings.Password;
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as SharepointConnectionSettings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            System.Windows.RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
