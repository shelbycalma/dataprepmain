﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.Sharepoint;

namespace Panopticon.CData.UI.Sharepoint.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.Sharepoint.Plugin, SharepointConnectionSettings>
    {
        public PluginUI(CData.Sharepoint.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(SharepointConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new SharepointConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			SharepointConfigPanel panel = (SharepointConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
