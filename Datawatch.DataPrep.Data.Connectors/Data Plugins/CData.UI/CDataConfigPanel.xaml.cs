﻿using System;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.CData.UI
{
	/// <summary>
	/// Interaction logic for ConfigPanel.xaml
	/// </summary>
	public partial class CDataConfigPanel : UserControl,
		IDataPluginConfigElement
	{
		private DatabaseSettingsBase settings;

		public CDataConfigPanel()
            : this(null)
        {
		}

		public CDataConfigPanel(DatabaseSettingsBase settings)
		{
			InitializeComponent();
			DataContext = this.settings = settings;
			PasswordBox.Password = settings.Password;
		}

		public DatabaseSettingsBase Settings
		{
			get { return settings; }
		}


		public void OnOk()
		{
			if (Settings == null)
			{
				return;
			}

			// This method is called when Ok button in DataSourceSettingsDialog is clicked.
			// Clear the column here, so that even if settings are not chagned, latest schema from 
			// DB is pulled.
			Settings.QuerySettings.SchemaColumnsSettings.ClearSchemaColumns();
		}

		public bool IsOk
		{
			get
			{
				if (Settings == null) return false;

				return Settings.IsOk;
			}
		}
		private void PasswordBox_PasswordChanged(object sender,	System.Windows.RoutedEventArgs e)
		{
			if (Settings != null)
			{
				Settings.Password = PasswordBox.Password;
			}
		}

	}
}
