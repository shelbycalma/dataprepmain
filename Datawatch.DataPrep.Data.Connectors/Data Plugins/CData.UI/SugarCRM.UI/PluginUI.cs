﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.CData.SugarCRM;

namespace Panopticon.CData.UI.SugarCRM.UI
{
    public class PluginUI : OdbcPluginUIBase<CData.SugarCRM.Plugin, SugarCRMConnectionSettings>
    {
        public PluginUI(CData.SugarCRM.Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

		public override object GetConfigPanel(SugarCRMConnectionSettings settings,
			IEnumerable<ParameterValue> parameters)
		{
			return new SugarCRMConfigPanel(settings);
		}

		public override PropertyBag ToPropertyBag(object element)
		{
			SugarCRMConfigPanel panel = (SugarCRMConfigPanel)element;
			return panel.Settings.ToPropertyBag();
		}
	}
}
