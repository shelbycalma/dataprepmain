﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    public class Filter
    {
        public Filter(string id, string name, string path)
        {
            if (id == null)
            {
                Exceptions.ArgumentNull("id");
            }

            if (name == null)
            {
                Exceptions.ArgumentNull("name");
            }

            if (path == null)
            {
                Exceptions.ArgumentNull("path");
            }

            Id = id;
            Name = name;
            Path = path;
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Path { get; private set; }
    }
}
