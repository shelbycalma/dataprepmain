﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    public class Item
    {
        public Item(string id, string name, string path, string displayName, string type,
            string parameterName, string @operator)
        {
            if (id == null)
            {
                Exceptions.ArgumentNull("id");
            }

            if (name == null)
            {
                Exceptions.ArgumentNull("name");
            }

            if (displayName == null)
            {
                Exceptions.ArgumentNull("displayName");
            }

            if (path == null)
            {
                Exceptions.ArgumentNull("path");
            }

            if (type == null)
            {
                Exceptions.ArgumentNull("type");
            }

            Id = id;
            Name = name;
            DisplayName = displayName;
            Path = path;
            Type = type;
            ParameterName = parameterName;
            Operator = @operator;
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string DisplayName { get; set; }
        public string Path { get; private set; }
        public string Type { get; private set; }

        public string ParameterName { get; set; }
        public string Operator { get; set; }
    }
}
