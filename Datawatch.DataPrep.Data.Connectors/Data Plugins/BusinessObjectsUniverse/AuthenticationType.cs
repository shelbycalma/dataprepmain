﻿namespace Panopticon.BusinessObjectsUniversePlugin
{
    public enum AuthenticationType
    {
        Enterprise,
        LDAP,
        WinAD,
        SAPR3
    }
}
