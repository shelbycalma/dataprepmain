﻿using System;
using System.ComponentModel;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public interface ISelectableChildrenTreeItem : ITreeItem, INotifyPropertyChanged
    {
        bool? ChildrenSelected { get; set; }
        bool CanSelect { get; set; }
        string UniverseId { get; }
        event EventHandler SettingChildrenSelected;
    }
}
