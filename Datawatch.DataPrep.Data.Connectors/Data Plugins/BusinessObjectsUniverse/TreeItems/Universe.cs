﻿using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public class Universe : SelectableChildrenTreeItemBase
    {
        public Universe(string id, string name, ITreeItem parent)
            : base (id, name, parent, id)
        {
            Children.Add(new DummyTreeItem(string.Empty, string.Empty, this));
        }

        public override string Image
        {
            get { return "/Panopticon.BusinessObjectsUniversePlugin;component/Images/universe.png"; }
        }

        public override string TypeName
        {
            get { return Resources.UiUniverse; }
        }
    }
}
