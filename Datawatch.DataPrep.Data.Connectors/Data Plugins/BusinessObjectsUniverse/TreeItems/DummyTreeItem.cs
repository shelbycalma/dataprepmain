﻿namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    class DummyTreeItem : TreeItemBase
    {
        public DummyTreeItem(string id, string name, ITreeItem parent) : base(id, name, parent)
        {
        }

        public override string Image
        {
            get { return null; }
        }

        public override string TypeName
        {
            get { return null; }
        }
    }
}
