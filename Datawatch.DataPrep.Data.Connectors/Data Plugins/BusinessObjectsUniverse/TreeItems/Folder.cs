﻿using System.ComponentModel;
using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public class Folder : TreeItemBase
    {
        private string image = "/Panopticon.BusinessObjectsUniversePlugin;component/Images/folder.png";

        public Folder(string id, string name, ITreeItem parent)
            : base(id, name, parent)
        {
            PropertyChanged += OnPropertyChanged;
            Children.Add(new DummyTreeItem(string.Empty, string.Empty, this));
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsExpanded") return;
            image = IsExpanded ?
                "/Panopticon.BusinessObjectsUniversePlugin;component/Images/folder_opened.png" :
                "/Panopticon.BusinessObjectsUniversePlugin;component/Images/folder.png";

            OnPropertyChanged("Image");
        }

        public override string Image
        {
            get { return image; }
        }

        public override string TypeName
        {
            get { return Resources.UiFolder; }
        }
    }
}
