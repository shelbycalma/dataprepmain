﻿using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    class Measure : SelectableTreeItemBase
    {
        public Measure(string id, string name, Universe universe, string path, ITreeItem parent)
            : base(id, name, universe, path, parent)
        {
        }

        public override string Image
        {
            get { return "/Panopticon.BusinessObjectsUniversePlugin;component/Images/measure.png"; }
        }

        public override string TypeName
        {
            get { return Resources.UiMeasure; }
        }
    }
}
