﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public abstract class SelectableChildrenTreeItemBase : TreeItemBase, ISelectableChildrenTreeItem
    {
        private bool? childrenSelected;
        private bool canSelect;

        public event EventHandler SettingChildrenSelected;

        protected SelectableChildrenTreeItemBase(string id, string name, ITreeItem parent, string universeId) : base(id, name, parent)
        {
            if (universeId == null)
            {
                Exceptions.ArgumentNull("universeId");
            }

            UniverseId = universeId;
            ChildrenSelected = false;
            CanSelect = true;
            Children.CollectionChanged += Children_CollectionChanged;
        }

        private void Children_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (ISelectableTreeItem selectableTreeItem in e.NewItems.OfType<ISelectableTreeItem>())
                    {
                        selectableTreeItem.PropertyChanged += SelectableTreeItem_PropertyChanged;
                    }

                    foreach (ISelectableChildrenTreeItem selectableChildrenTreeItem in e.NewItems.OfType<ISelectableChildrenTreeItem>())
                    {
                        selectableChildrenTreeItem.PropertyChanged += SelectableChildrenTreeItem_PropertyChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (ISelectableTreeItem selectableTreeItem in e.NewItems.OfType<ISelectableTreeItem>())
                    {
                        selectableTreeItem.PropertyChanged -= SelectableTreeItem_PropertyChanged;
                    }

                    foreach (ISelectableChildrenTreeItem selectableChildrenTreeItem in e.NewItems.OfType<ISelectableChildrenTreeItem>())
                    {
                        selectableChildrenTreeItem.PropertyChanged -= SelectableChildrenTreeItem_PropertyChanged;
                    }
                    break;
            }
        }

        private void SelectableChildrenTreeItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "ChildrenSelected") return;
            CheckChildrenIsSelectedStates();
        }

        private void SelectableTreeItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected") return;
            CheckChildrenIsSelectedStates();
        }

        private void CheckChildrenIsSelectedStates()
        {
            if (Children.OfType<ISelectableTreeItem>().All(i => i.IsSelected) &&
                Children.OfType<ISelectableChildrenTreeItem>().All(i => i.ChildrenSelected == true))
            {
                ChildrenSelected = true;
                return;
            }

            if (Children.OfType<ISelectableTreeItem>().Any(i => i.IsSelected) ||
                Children.OfType<ISelectableChildrenTreeItem>().Any(i => i.ChildrenSelected == true || i.ChildrenSelected == null))
            {
                ChildrenSelected = null;
                return;
            }

            ChildrenSelected = false;
        }

        public bool? ChildrenSelected
        {
            get
            {
                return childrenSelected;
            }
            set
            {
                if (childrenSelected == value) return;
                childrenSelected = value;

                OnSettingChildrenSelected();

                foreach (ISelectableChildrenTreeItem item in Children.OfType<ISelectableChildrenTreeItem>())
                {
                    item.PropertyChanged -= SelectableChildrenTreeItem_PropertyChanged;
                }

                if (childrenSelected.HasValue)
                {
                    foreach (ISelectableTreeItem item in Children.OfType<ISelectableTreeItem>())
                    {
                        item.PropertyChanged -= SelectableTreeItem_PropertyChanged;
                        item.IsSelected = childrenSelected == true;
                        foreach (ISelectableTreeItem child in item.Children.OfType<ISelectableTreeItem>())
                        {
                            child.PropertyChanged -= SelectableTreeItem_PropertyChanged;
                            child.IsSelected = childrenSelected == true;
                            child.PropertyChanged += SelectableTreeItem_PropertyChanged;
                        }
                        item.PropertyChanged += SelectableTreeItem_PropertyChanged;
                    }

                    foreach (ISelectableChildrenTreeItem item in Children.OfType<ISelectableChildrenTreeItem>())
                    {
                        item.ChildrenSelected = childrenSelected;
                    }
                }

                foreach (ISelectableChildrenTreeItem item in Children.OfType<ISelectableChildrenTreeItem>())
                {
                    item.PropertyChanged += SelectableChildrenTreeItem_PropertyChanged;
                }

                OnPropertyChanged("ChildrenSelected");
            }
        }

        public bool CanSelect
        {
            get { return canSelect; }
            set
            {
                if (value == canSelect) return;
                canSelect = value;
                OnPropertyChanged("CanSelect");
            }
        }

        public string UniverseId { get; private set; }
        
        public bool CanSelectChildren { get; set; }

        protected virtual void OnSettingChildrenSelected()
        {
            EventHandler handler = SettingChildrenSelected;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}
