﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public abstract class SelectableTreeItemBase : TreeItemBase, ISelectableTreeItem
    {
        private bool isSelected;
        private bool canSelect;

        protected SelectableTreeItemBase(string id, string name, Universe universe, string path, ITreeItem parent) : base(id, name, parent)
        {
            if (universe == null)
            {
                Exceptions.ArgumentNull("universe");
            }

            if (path == null)
            {
                Exceptions.ArgumentNull("path");
            }

            Universe = universe;
            Path = path;
            CanSelect = true;
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (value == isSelected) return;
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }

        public bool CanSelect
        {
            get { return canSelect; }
            set
            {
                if (value == canSelect) return;
                canSelect = value;
                OnPropertyChanged("CanSelect");
            }
        }

        public Universe Universe { get; private set; }
        public string Path { get; private set; }
    }
}
