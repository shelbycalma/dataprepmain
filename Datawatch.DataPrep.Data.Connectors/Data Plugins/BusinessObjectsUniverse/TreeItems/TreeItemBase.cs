﻿using System.Collections.ObjectModel;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public abstract class TreeItemBase : ViewModelBase, ITreeItem
    {
        private bool isExpanded;
        private bool isVisible;

        protected TreeItemBase(string id, string name, ITreeItem parent)
        {
            if (id == null)
            {
                Exceptions.ArgumentNull("id");
            }

            if (name == null)
            {
                Exceptions.ArgumentNull("name");
            }

            Id = id;
            Name = name;
            Children = new ObservableCollection<ITreeItem>();
            Parent = parent;
            IsVisible = true;
        }

        public ObservableCollection<ITreeItem> Children { get; private set; }
        public abstract string Image { get; }
        public string Id { get; private set; }
        public string Name { get; private set; }
        public ITreeItem Parent { get; private set; }

        public bool IsBusinessViewChild
        {
            get
            {
                ITreeItem parent = Parent;
                while (parent != null)
                {
                    if (parent is BusinessView) return true;
                    parent = parent.Parent;
                }

                return false;
            }
        }

        public virtual bool IsExpanded
        {
            get { return isExpanded; }
            set
            {
                if (value && Parent != null)
                {
                    Parent.IsExpanded = true;
                }

                if (value == isExpanded) return;
                isExpanded = value;
                OnPropertyChanged("IsExpanded");
            }
        }

        public bool IsVisible
        {
            get { return isVisible; }
            set
            {
                if (value == isVisible) return;
                isVisible = value;

                if (isVisible && Parent != null)
                {
                    Parent.IsVisible = true;
                }

                OnPropertyChanged("IsVisible");
            }
        }

        public abstract string TypeName { get; }

        public bool HasDummyChild
        {
            get { return Children.Count == 1 && Children.First() is DummyTreeItem; }
        }
    }
}
