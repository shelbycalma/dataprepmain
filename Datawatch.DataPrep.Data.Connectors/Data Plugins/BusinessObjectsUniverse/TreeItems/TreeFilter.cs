﻿using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public class TreeFilter : SelectableTreeItemBase
    {
        public TreeFilter(string id, string name, Universe universe, string path, ITreeItem parent)
            : base(id, name, universe, path, parent)
        {
        }

        public override string Image
        {
            get { return "/Panopticon.BusinessObjectsUniversePlugin;component/Images/filter.png"; }
        }

        public override string TypeName
        {
            get { return Resources.UiFilter; }
        }
    }
}
