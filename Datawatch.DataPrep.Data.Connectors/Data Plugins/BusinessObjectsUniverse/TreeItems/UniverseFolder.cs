﻿using System.ComponentModel;
using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    class UniverseFolder : SelectableChildrenTreeItemBase
    {
        private string image = "/Panopticon.BusinessObjectsUniversePlugin;component/Images/folder.png";

        public UniverseFolder(string id, string name, ITreeItem parent, string universeId)
            : base(id, name, parent, universeId)
        {
            PropertyChanged += OnPropertyChanged;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsExpanded") return;
            image = IsExpanded ?
                "/Panopticon.BusinessObjectsUniversePlugin;component/Images/folder_opened.png" :
                "/Panopticon.BusinessObjectsUniversePlugin;component/Images/folder.png";

            OnPropertyChanged("Image");
        }


        public override string Image
        {
            get { return image; }
        }

        public override string TypeName
        {
            get { return Resources.UiFolder; }
        }
    }
}
