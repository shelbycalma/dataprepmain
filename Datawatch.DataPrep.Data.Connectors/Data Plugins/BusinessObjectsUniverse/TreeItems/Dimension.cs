﻿using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    class Dimension : SelectableTreeItemBase
    {
        public Dimension(string id, string name, Universe universe, string path, ITreeItem parent)
            : base(id, name, universe, path, parent)
        {
        }

        public override string Image
        {
            get { return "/Panopticon.BusinessObjectsUniversePlugin;component/Images/dimension.png"; }
        }

        public override string TypeName
        {
            get { return Resources.UiDimension; }
        }
    }
}
