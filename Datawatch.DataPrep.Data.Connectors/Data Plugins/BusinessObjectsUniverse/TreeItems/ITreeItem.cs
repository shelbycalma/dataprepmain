﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public interface ITreeItem : INotifyPropertyChanged
    {
        ObservableCollection<ITreeItem> Children { get; }
        string Image { get; }
        string Id { get; }
        string Name { get; }
        ITreeItem Parent { get; }
        bool IsBusinessViewChild { get; }
        string TypeName { get; }
        bool IsExpanded { get; set; }
        bool HasDummyChild { get; }
        bool IsVisible { get; set; }
    }
}
