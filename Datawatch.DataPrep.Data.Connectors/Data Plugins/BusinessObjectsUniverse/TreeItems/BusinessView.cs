﻿using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    class BusinessView : SelectableChildrenTreeItemBase
    {
        public BusinessView(string id, string name, ITreeItem parent, string universeId)
            : base(id, name, parent, universeId)
        {
        }

        public override string Image
        {
            get { return "/Panopticon.BusinessObjectsUniversePlugin;component/Images/businessView.png"; }
        }

        public override string TypeName
        {
            get { return Resources.UiBusinessView; }
        }
    }
}
