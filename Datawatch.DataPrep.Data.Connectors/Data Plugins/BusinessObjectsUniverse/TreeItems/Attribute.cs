﻿using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    class Attribute : SelectableTreeItemBase
    {
        public Attribute(string id, string name, Universe universe, string path, ITreeItem parent)
            : base(id, name, universe, path, parent)
        {
        }

        public override string Image
        {
            get { return "/Panopticon.BusinessObjectsUniversePlugin;component/Images/attribute.png"; }
        }

        public override string TypeName
        {
            get { return Resources.UiAttribute; }
        }
    }
}
