﻿using System.ComponentModel;

namespace Panopticon.BusinessObjectsUniversePlugin.TreeItems
{
    public interface ISelectableTreeItem : ITreeItem, INotifyPropertyChanged
    {
        bool IsSelected { get; set; }
        bool CanSelect { get; set; }
        Universe Universe { get; }
        string Path { get; }
    }
}
