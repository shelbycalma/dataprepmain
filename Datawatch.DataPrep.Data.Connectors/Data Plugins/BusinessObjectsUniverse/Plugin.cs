﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.BusinessObjectsUniversePlugin.Properties;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<BusinessObjectsUniverseSettings>
    {
        internal const string PluginId = "BusinessObjectsUniversePlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Business Objects";
        internal const string PluginType = DataPluginTypes.Database;

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get
            {
                return LicenseCache.IsValid(GetType(), this);
            }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override BusinessObjectsUniverseSettings CreateSettings(PropertyBag bag)
        {
            BusinessObjectsUniverseSettings businessObjectsUniverseSettings =
                new BusinessObjectsUniverseSettings(bag);

            if (string.IsNullOrEmpty(businessObjectsUniverseSettings.Title))
            {
                businessObjectsUniverseSettings.Title = Resources.UiDefaultConnectionTitle;
            }

            return businessObjectsUniverseSettings;
        }
        public override ITable GetData(string workbookDir, string dataDir, PropertyBag propertyBag,
            IEnumerable<ParameterValue> parameters, int rowcount, bool applyRowFilteration)
        {
            CheckLicense();
            BusinessObjectsUniverseSettings businessObjectsUniverseSettings = CreateSettings(propertyBag);

            StandaloneTable table;
            using (BusinessObjectsUniverseClient client =
                new BusinessObjectsUniverseClient(businessObjectsUniverseSettings, parameters))
            {
                table = client.GetData(rowcount, parameters);    
            }

            // We're creating a new StandaloneTable on every request, so allow
            // EX to freeze it.
            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }
    }
}
