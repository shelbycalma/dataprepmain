﻿using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    class QueryMetaDataDefinition
    {
        public QueryMetaDataDefinition(string name, string type)
        {
            if (name == null)
            {
                Exceptions.ArgumentNull("name");
            }

            if (type == null)
            {
                Exceptions.ArgumentNull("type");
            }

            Name = name;
            Type = type;
        }

        public string Name { get; private set; }
        public string Type { get; private set; }
    }
}
