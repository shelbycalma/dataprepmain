﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using Panopticon.BusinessObjectsUniversePlugin.TreeItems;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    public static class Utils
    {
        public static IList<ITreeItem> FlattenTree(IEnumerable<ITreeItem> tree)
        {
            List<ITreeItem> result = new List<ITreeItem>(tree);
            foreach (ITreeItem item in tree.Where(item => item.Children != null))
            {
                result.AddRange(FlattenTree(item.Children));
            }

            return result;
        }

        public static IList<TreeViewItem> FlattenTree(IEnumerable<TreeViewItem> tree)
        {
            List<TreeViewItem> result = new List<TreeViewItem>(tree);
            foreach (TreeViewItem item in tree)
            {
                result.AddRange(FlattenTree(item.Items.OfType<TreeViewItem>().Where(i => i != null)));
            }

            return result;
        }

        public static string GetUniversePath(Universe universe)
        {
            string result = string.Empty;
            ITreeItem parent = universe.Parent;
            while (parent != null)
            {
                result = parent.Name + "\\" + result;
                parent = parent.Parent;
            }

            return result;
        }

        public static string GetUniverseChildPath(ITreeItem parent, string targetItemName)
        {
            string result = string.Empty;
            Universe universe = parent as Universe;
            while (universe == null)
            {
                result = parent.Name + "\\" + result;
                parent = parent.Parent;
                universe = parent as Universe;
            }

            return result + targetItemName;
        }
    }
}
