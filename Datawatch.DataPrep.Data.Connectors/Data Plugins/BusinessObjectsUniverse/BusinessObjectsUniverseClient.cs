﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Text;
using System.Xml.Linq;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Panopticon.BusinessObjectsUniversePlugin.Extentions;
using Panopticon.BusinessObjectsUniversePlugin.Properties;
using Panopticon.BusinessObjectsUniversePlugin.TreeItems;
using Attribute = Panopticon.BusinessObjectsUniversePlugin.TreeItems.Attribute;
using System.Xml;
using System.Security;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    public class BusinessObjectsUniverseClient : IDisposable
    {
        private const string LogonUrlPattern = "{0}/biprws/logon/long";
        private const string InfostoreUrlPattern = "{0}/biprws/infostore/{1}/children/?page=1&pageSize=1000000";
        private const string UniverseUrlPattern = "{0}/biprws/sl/v1/universes/{1}/?page=1&pageSize=1000000";
        private const string BusinessViewsUrlPattern = "{0}/biprws/sl/v1/universes/{1}/businessviews";
        private const string QueryUrlPattern = "{0}/biprws/sl/v1/queries";
        private const string DeleteQueryUrlPattern = "{0}/biprws/sl/v1/queries/{1}";
        private const string QueryDataUrlPattern = "{0}/biprws/sl/v1/queries/{1}/data.svc/Flows0";
        private const string QueryMetaDataUrlPattern = "{0}/biprws/sl/v1/queries/{1}/data.svc/$metadata";
        private const string LogOffUrlPattern = "{0}/biprws/logoff";

        private const string RootFolderCuid = "cuid_AWcPjwbDdBxPoXPBOUCsKkk";
        private const string LogonTokenHeader = "X-SAP-LogonToken";

        private readonly string url;
        private readonly string userName;
        private readonly SecureString password;
        private readonly string logonUrl;
        private readonly string queryUrl;
        private readonly string logOffUrl;
        private readonly string loginType;
        private readonly Item[] items;
        private readonly Filter[] filters;
        private readonly string activeUniverseId;
        private readonly int requestTimeout;
        private string logonToken;
        private bool disposed;

        private readonly XNamespace sapNamespace = XNamespace.Get("http://www.sap.com/rws/sl/universe");

        public BusinessObjectsUniverseClient(BusinessObjectsUniverseSettings settings, IEnumerable<ParameterValue> parameters)
        {
            url = DataUtils.ApplyParameters(settings.Url, parameters).TrimEnd('/');
            string port = DataUtils.ApplyParameters(settings.Port, parameters);

            if (!url.StartsWith("http://") && !url.StartsWith("https://"))
            {
                url = "http://" + url;
            }

            url += ":" + port;
            
            userName = DataUtils.ApplyParameters(settings.Username, parameters);
            password = SecurityUtils.ToSecureString(DataUtils.ApplyParameters(settings.Password, parameters));

            logonUrl = string.Format(LogonUrlPattern, url);
            queryUrl = string.Format(QueryUrlPattern, url);
            logOffUrl = string.Format(LogOffUrlPattern, url);

            requestTimeout = settings.RequestTimeout;

            items = settings.Items;

            if (items != null)
            {
                foreach (Item item in items)
                {
                    item.DisplayName = DataUtils.ApplyParameters(item.DisplayName, parameters);
                }    
            }

            filters = settings.Filters;
            
            activeUniverseId = settings.UniverseId;

            switch (settings.AuthenticationType)
            {
                case AuthenticationType.Enterprise:
                    loginType = "secEnterprise";
                    break;
                case AuthenticationType.LDAP:
                    loginType = "secLDAP";
                    break;
                case AuthenticationType.SAPR3:
                    loginType = "secSAPR3";
                    break;
                case AuthenticationType.WinAD:
                    loginType = "secWinAD";
                    break;
            }

            Login();
            Log.Info(Resources.LogLoginSuccessful, url);
        }

        private void Login()
        {
            string xml = new XDocument(
                new XElement("attrs", new object[]
                    {
                        new XElement("attr", new object[]{ userName, new XAttribute("name", "userName"), new XAttribute("type", "string")}),
                        new XElement("attr", new object[]{ SecurityUtils.ToInsecureString(password), new XAttribute("name", "password"), new XAttribute("type", "string")}),
                        new XElement("attr", new object[]{ loginType, new XAttribute("name", "auth"), new XAttribute("type", "string")})
                    })).ToString();

            HttpWebRequest request = CreatePostRequest(logonUrl, xml);
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                logonToken = response.Headers[LogonTokenHeader];
            }
        }

        private void LogOff()
        {
            try
            {
                HttpWebRequest request = CreateBasicRequest(logOffUrl);
                request.Method = "POST";
                (request.GetResponse()).Close();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        public StandaloneTable GetData(int rowcount, IEnumerable<ParameterValue> parameters)
        {
            if (items == null || !items.Any())
            {
                throw new ArgumentException(Resources.ExItemsCannotBeNull);
            }

            Log.Info(Resources.LogRecievingData, url);
            DateTime startTime = DateTime.Now;

            IEnumerable<XElement> resultObjects = items.Select(
                item => new XElement(sapNamespace + "resultObject", new XAttribute("id", item.Id)));
            XElement resultObjectsSection = new XElement(sapNamespace + "resultObjects", resultObjects);
            IList<XElement> queryDataSectionChildren = new List<XElement> { resultObjectsSection };
            XElement filterSection = CreateFilters(parameters);
            if (filterSection != null)
            {
                queryDataSectionChildren.Add(filterSection);
            }

            string xml = new XDocument(
                new XElement(sapNamespace + "query", new object[]
                    {
                        new XAttribute("dataSourceId", activeUniverseId),
                        new XAttribute("dataSourceType", "unx"),
                        new XElement(sapNamespace + "querySpecification", new object[]
                        {
                            new XAttribute("version", "1.0"),
                            new XElement(sapNamespace + "queryData", queryDataSectionChildren)
                        })
                    })).ToString();

            try
            {
                XDocument doc = GetResponseDocument(queryUrl, xml);
                string queryId = doc.ElementIgnoreNamespace("success").ElementIgnoreNamespace("id").Value;

                IList<QueryMetaDataDefinition> metaData = GetQueryMetaData(queryId);
                IList<IList<KeyValuePair<string, string>>> values = GetQueryData(queryId, metaData, rowcount);

                DeleteQuery(queryId);

                StandaloneTable table = BusinessObjectsUniverseTableBuilder.BuildTable(values, metaData, items);
                DateTime endTime = DateTime.Now;
                Log.Info(Resources.LogRecievedData, table.RowCount, table.ColumnCount, (endTime - startTime).TotalSeconds, url);
                return table;
            }
            catch (WebException ex)
            {
                string message = string.Format(Resources.ExFailedToCreateQuery, GetErrorMessage(ex));
                Log.Error(message);
                throw new Exception(message);
            }
        }

        private XElement CreateFilters(IEnumerable<ParameterValue> parameters)
        {
            List<XElement> filterElements = new List<XElement>();
            if (filters != null)
            {
                filterElements.AddRange(filters.Select(
                    f => new XElement(sapNamespace + "predefinedFilter", new XAttribute("id", f.Id))));
            }

            foreach (Item item in items.Where(
                i => !string.IsNullOrEmpty(i.ParameterName) && !string.IsNullOrEmpty(i.Operator)))
            {
                ParameterValue parameter = parameters.FirstOrDefault(p => p.Name == item.ParameterName);
                if(parameter == null) continue;
                StringParameterValue spv = parameter.TypedValue as StringParameterValue;
                if(spv == null) continue;

                filterElements.Add(new XElement(sapNamespace + "comparisonFilter", new object[]
                {
                    new XAttribute("id", item.Id),
                    new XAttribute("operator", item.Operator),
                    new XElement(sapNamespace + "constantOperand", new XElement(sapNamespace + "answerValue", spv.Value))
                }));
            }

            if (!filterElements.Any()) return null;

            if (filterElements.Count == 1)
            {
                return new XElement(sapNamespace + "filterPart", filterElements);
            }

            return new XElement(sapNamespace + "filterPart", new XElement(sapNamespace + "and", filterElements));
        }

        private IList<QueryMetaDataDefinition> GetQueryMetaData(string queryId)
        {
            IList<QueryMetaDataDefinition> metaData = new List<QueryMetaDataDefinition>();

            XDocument doc = GetResponseDocument(string.Format(QueryMetaDataUrlPattern, url, queryId));
            IEnumerable<XElement> properties =
                doc.ElementIgnoreNamespace("Edmx")
                    .ElementIgnoreNamespace("DataServices")
                    .ElementIgnoreNamespace("Schema")
                    .ElementIgnoreNamespace("EntityType", "Name", "Flow0")
                    .ElementsIgnoreNamespace("Property").Skip(1); //the first entry is always "Id", we skip it

            foreach (XElement property in properties)
            {
                metaData.Add(
                    new QueryMetaDataDefinition(
                        property.Attribute("Name").Value,
                        property.Attribute("Type").Value));
            }

            return metaData;
        }

        private IList<IList<KeyValuePair<string, string>>> GetQueryData(string queryId, IList<QueryMetaDataDefinition> metaData, int rowcount)
        {
            IList<IList<KeyValuePair<string, string>>> result = new List<IList<KeyValuePair<string, string>>>();

            string queryDataUrl = string.Format(QueryDataUrlPattern, url, queryId);
            if (rowcount > 0)
            {
                queryDataUrl += "?$top=" + (rowcount - 1);
            }

            XDocument doc = GetResponseDocument(queryDataUrl);
            foreach (XElement entry in doc.ElementIgnoreNamespace("feed").ElementsIgnoreNamespace("entry"))
            {
                //The first entry is always "Id", skip it
                IList<XElement> cells = entry.ElementIgnoreNamespace("content").ElementIgnoreNamespace("properties").Elements().Skip(1).ToList();
                IList<KeyValuePair<string, string>> row = new List<KeyValuePair<string, string>>();
                
                for (int i = 0; i < metaData.Count(); i++)
                {
                    string value;
                    XAttribute nullAttribute = cells[i].AttributeIgnoreNamespace("null");
                    if (nullAttribute != null && nullAttribute.Value == "true")
                    {
                        value = null;
                    }
                    else
                    {
                        value = cells[i].Value;
                    }

                    row.Add(new KeyValuePair<string, string>(metaData[i].Name, value));
                }

                result.Add(row);
            }

            return result;
        }

        private void DeleteQuery(string queryId)
        {
            HttpWebRequest request = CreateBasicRequest(string.Format(DeleteQueryUrlPattern, url, queryId));
            request.Method = "DELETE";
            (request.GetResponse()).Close();
        }

        public IList<ITreeItem> CreateRootTreeItems()
        {
            return CreateFolderChildren(null);
        }

        public IList<ITreeItem> CreateFolderChildren(Folder parentFolder)
        {
            try
            {
                IList<ITreeItem> result = new List<ITreeItem>();
                XDocument doc = GetResponseDocument(string.Format(InfostoreUrlPattern, url,
                    parentFolder == null ? RootFolderCuid : parentFolder.Id));

                foreach (XElement entry in doc.ElementIgnoreNamespace("feed").ElementsIgnoreNamespace("entry"))
                {
                    XElement attrs = entry.ElementIgnoreNamespace("content").ElementIgnoreNamespace("attrs");
                    string type = attrs.ElementIgnoreNamespace("attr", "name", "type").Value;
                    if (type == "Folder")
                    {
                        Folder folder = new Folder(
                            attrs.ElementIgnoreNamespace("attr", "name", "id").Value,
                            attrs.ElementIgnoreNamespace("attr", "name", "name").Value,
                            parentFolder);
                        result.Add(folder);
                    }
                    else if (type == "DSL.Universe")
                    {
                        Universe universe = new Universe(
                            attrs.ElementIgnoreNamespace("attr", "name", "id").Value,
                            attrs.ElementIgnoreNamespace("attr", "name", "name").Value,
                            parentFolder);
                        result.Add(universe);
                    }
                }

                return result;
            }
            catch (WebException ex)
            {
                string message = string.Format(Resources.ExFailedToGetFolderChildren, GetErrorMessage(ex));
                Log.Error(message);
                throw new Exception(message);
            }
        }

        public IList<ITreeItem> CreateUniverseChildren(Universe universe)
        {
            try
            {
                XDocument doc = GetResponseDocument(string.Format(UniverseUrlPattern, url, universe.Id));

                IEnumerable<ITreeItem> children = CreateUniverseChildren(
                    doc.ElementIgnoreNamespace("universe").ElementIgnoreNamespace("outline").Elements(),
                    universe, universe);

                IEnumerable<BusinessView> businessViews = CreateBusinessViews(universe, children);

                List<ITreeItem> result = new List<ITreeItem>();
                if (businessViews.Any())
                {
                    UniverseFolder businessViewsFolder = new UniverseFolder(string.Empty, Resources.UiBusinessViews, universe, universe.Id);
                    businessViewsFolder.Children.AddRange(businessViews);
                    result.Add(businessViewsFolder);
                }

                result.AddRange(children);

                return result;
            }
            catch (WebException ex)
            {
                string message = string.Format(Resources.ExFailedToGetUniverseChildren, GetErrorMessage(ex));
                Log.Error(message);
                throw new Exception(message);
            }
        }

        private static IEnumerable<ITreeItem> CreateUniverseChildren(IEnumerable<XElement> elements, Universe universe, ITreeItem parent)
        {
            IList<ITreeItem> result = new List<ITreeItem>();
            foreach (XElement element in elements)
            {
                if (element.Name.LocalName == "folder")
                {
                    string id = element.ElementIgnoreNamespace("id").Value;
                    string name = element.ElementIgnoreNamespace("name").Value;

                    UniverseFolder folder = new UniverseFolder(id, name, parent, universe.Id);
                    folder.Children.AddRange(CreateUniverseChildren(element.Elements(), universe, folder));

                    result.Add(folder);
                }
                else if(element.Name.LocalName == "item")
                {
                    string id = element.ElementIgnoreNamespace("id").Value;
                    string name = element.ElementIgnoreNamespace("name").Value;
                    string type = element.Attributes().Single(a => a.Name.LocalName == "type").Value;
                    ITreeItem item = null;
                    switch (type)
                    {
                        case "Dimension":
                            item = new Dimension(id, name, universe, Utils.GetUniverseChildPath(parent, name), parent);
                            break;
                        case "Measure":
                            item = new Measure(id, name, universe, Utils.GetUniverseChildPath(parent, name), parent);
                            break;
                        case "Filter":
                            item = new TreeFilter(id, name, universe, Utils.GetUniverseChildPath(parent, name), parent);
                            break;
                    }

                    if (item == null) continue;

                    foreach (XElement child in element.ElementsIgnoreNamespace("item"))
                    {
                        XAttribute typeAttribute = child.Attributes().SingleOrDefault(a => a.Name.LocalName == "type");
                        if (typeAttribute == null || typeAttribute.Value != "Attribute") continue;

                        string attributeName = child.ElementIgnoreNamespace("name").Value;
                        item.Children.Add(new Attribute(
                            child.ElementIgnoreNamespace("id").Value,
                            attributeName,
                            universe,
                            Utils.GetUniverseChildPath(item, attributeName),
                            item));
                    }

                    result.Add(item);
                }
            }

            return result;
        }

        private IEnumerable<BusinessView> CreateBusinessViews(Universe universe, IEnumerable<ITreeItem> universeChildren)
        {
            XDocument doc = GetResponseDocument(string.Format(BusinessViewsUrlPattern, url, universe.Id));

            IList<BusinessView> result = new List<BusinessView>();

            IEnumerable<XElement> elements =
                doc.ElementIgnoreNamespace("businessViews")
                    .ElementsIgnoreNamespace("businessView", "masterView", "false");

            if (!elements.Any()) return result;

            IEnumerable<ITreeItem> flattenedUniverseChildren = Utils.FlattenTree(universeChildren);

            foreach (XElement element in elements)
            {
                string id = element.ElementIgnoreNamespace("id").Value;
                string name = element.ElementIgnoreNamespace("name").Value;
                BusinessView businessView = new BusinessView(id, name, universe, universe.Id);
                businessView.Children.AddRange(CreateBusinessViewChildren(element.Elements(), flattenedUniverseChildren, universe, businessView));
                result.Add(businessView);
            }

            return result;
        }

        private static IEnumerable<ITreeItem> CreateBusinessViewChildren(
            IEnumerable<XElement> elements, IEnumerable<ITreeItem> flattenedUniverseChildren, Universe universe, ITreeItem parent)
        {
            IList<ITreeItem> result = new List<ITreeItem>();
            foreach (XElement element in elements)
            {
                if (element.Name.LocalName != "itemRef" && element.Name.LocalName != "folderRef") continue;
                string id = element.AttributeIgnoreNamespace("id").Value;
                ITreeItem existingItem = flattenedUniverseChildren.Single(i => i.Id == id);

                if (existingItem is UniverseFolder)
                {
                    UniverseFolder folder = new UniverseFolder(existingItem.Id, existingItem.Name, parent, universe.Id);
                    folder.Children.AddRange(CreateBusinessViewChildren(element.Elements(), flattenedUniverseChildren, universe, folder));
                    result.Add(folder);
                    continue;
                }

                ISelectableTreeItem existingSelectableItem = (ISelectableTreeItem) existingItem;
                if (existingItem is Dimension)
                {
                    Dimension dimension = new Dimension(
                        existingSelectableItem.Id, existingSelectableItem.Name,
                        universe, existingSelectableItem.Path, parent);
                    dimension.Children.AddRange(CreateBusinessViewChildren(element.Elements(), flattenedUniverseChildren, universe, dimension));
                    result.Add(dimension);
                }
                else if (existingItem is Measure)
                {

                    Measure measure = new Measure(
                        existingSelectableItem.Id, existingSelectableItem.Name,
                        universe, existingSelectableItem.Path, parent);
                    measure.Children.AddRange(CreateBusinessViewChildren(element.Elements(), flattenedUniverseChildren, universe, measure));
                    result.Add(measure);
                }
                else if (existingItem is Attribute)
                {

                    Attribute attribute = new Attribute(
                        existingSelectableItem.Id, existingSelectableItem.Name,
                        universe, existingSelectableItem.Path, parent);
                    result.Add(attribute);
                }
                else if (existingItem is TreeFilter)
                {

                    TreeFilter filter = new TreeFilter(
                        existingSelectableItem.Id, existingSelectableItem.Name,
                        universe, existingSelectableItem.Path, parent);
                    result.Add(filter);
                }
            }

            return result;
        }

        private XDocument GetResponseDocument(string requestUrl)
        {
            HttpWebRequest request = CreateBasicRequest(requestUrl);
            return GetResponseDocument(request);
        }

        private XDocument GetResponseDocument(string requestUrl, string xml)
        {
            HttpWebRequest request = CreatePostRequest(requestUrl, xml);
            return GetResponseDocument(request);
        }

        private static XDocument GetResponseDocument(WebRequest request)
        {
			using (HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse())
            using (StreamReader xmlStream = new StreamReader(webResponse.GetResponseStream(), Encoding.Default))
            {
				XmlReaderSettings settings = new XmlReaderSettings();
				settings.XmlResolver = null;

				XmlReader reader = XmlTextReader.Create(xmlStream, settings);
				return XDocument.Load(reader);
            }
        }

        private HttpWebRequest CreateBasicRequest(string requestUrl)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUrl);
            request.Accept = "application/xml";
            request.ContentType = "application/xml";
            request.Timeout = requestTimeout * 1000;

            HttpRequestCachePolicy noCachePolicy =
                new HttpRequestCachePolicy(
                    HttpRequestCacheLevel.NoCacheNoStore);
            request.CachePolicy = noCachePolicy;

            if (logonToken != null)
            {
                request.Headers.Add(LogonTokenHeader, logonToken);
            }

            return request;
        }

        private HttpWebRequest CreatePostRequest(string requestUrl, string xml)
        {
            HttpWebRequest request = CreateBasicRequest(requestUrl);
            request.Method = "POST";

            byte[] bytes = Encoding.ASCII.GetBytes(xml);
            request.ContentLength = bytes.Length;
            Stream outputStream = request.GetRequestStream();
            outputStream.Write(bytes, 0, bytes.Length);
            outputStream.Close();

            return request;
        }

        private string GetErrorMessage(WebException ex)
        {
            using (StreamReader xmlStream = new StreamReader(ex.Response.GetResponseStream()))
            {
				XmlReaderSettings settings = new XmlReaderSettings();
				settings.XmlResolver = null;

				XmlReader reader = XmlTextReader.Create(xmlStream, settings);
				XDocument doc = XDocument.Load(reader);
                return doc.ElementIgnoreNamespace("error").ElementIgnoreNamespace("message").Value;
            }
        }

        ~BusinessObjectsUniverseClient()
        {
            Dispose(false);
        }

        void IDisposable.Dispose()
        {
            Close();
        }

        public void Close()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;
            if (disposing)
            {
                LogOff();
            }
            disposed = true;
        }
    }
}
