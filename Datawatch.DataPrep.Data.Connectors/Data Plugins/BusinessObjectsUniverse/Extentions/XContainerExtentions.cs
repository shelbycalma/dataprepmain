﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Panopticon.BusinessObjectsUniversePlugin.Extentions
{
    static class XContainerExtentions
    {
        public static XElement ElementIgnoreNamespace(this XContainer container, string localName)
        {
            return container.Elements().FirstOrDefault(e => e.Name.LocalName == localName);
        }

        public static XElement ElementIgnoreNamespace(this XContainer container, string localName, string attributeName, string attributeValue)
        {
            IEnumerable<XElement> elements = container.Elements().Where(e => e.Name.LocalName == localName);

            foreach (XElement element in elements)
            {
                XAttribute attribute = element.Attributes().FirstOrDefault(a => a.Name.LocalName == attributeName);
                if (attribute == null || attribute.Value != attributeValue) continue;
                return element;
            }

            return null;
        }

        public static IEnumerable<XElement> ElementsIgnoreNamespace(this XContainer container, string localName)
        {
            return container.Elements().Where(e => e.Name.LocalName == localName);
        }

        public static IEnumerable<XElement> ElementsIgnoreNamespace(this XContainer container, string localName, string attributeName, string attributeValue)
        {
            IEnumerable<XElement> elements = container.Elements().Where(e => e.Name.LocalName == localName);

            foreach (XElement element in elements)
            {
                XAttribute attribute = element.Attributes().FirstOrDefault(a => a.Name.LocalName == attributeName);
                if (attribute == null || attribute.Value != attributeValue) continue;
                yield return element;
            }
        }
    }
}