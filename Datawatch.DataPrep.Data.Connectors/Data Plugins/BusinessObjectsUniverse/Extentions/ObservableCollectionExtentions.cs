﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Panopticon.BusinessObjectsUniversePlugin.Extentions
{
    public static class ObservableCollectionExtentions
    {
        public static void AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                collection.Add(item);
            }
        }
    }
}
