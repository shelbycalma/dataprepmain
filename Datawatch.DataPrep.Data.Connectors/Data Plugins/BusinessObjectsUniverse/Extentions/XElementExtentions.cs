﻿
using System.Linq;
using System.Xml.Linq;

namespace Panopticon.BusinessObjectsUniversePlugin.Extentions
{
    static class XElementExtentions
    {
        public static XAttribute AttributeIgnoreNamespace(this XElement element, string attributeName)
        {
            return element.Attributes().SingleOrDefault(a => a.Name.LocalName == attributeName);
        }
    }
}
