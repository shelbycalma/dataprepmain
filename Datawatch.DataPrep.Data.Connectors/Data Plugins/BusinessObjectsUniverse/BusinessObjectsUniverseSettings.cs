﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    public class BusinessObjectsUniverseSettings : ConnectionSettings
    {
        private const string ItemsCountKey = "ItemsCount";
        private const string FiltersCountKey = "FiltersCount";
        private const string ItemsKey = "Items_{0}";
        private const string FiltersKey = "Filters_{0}";
        private const string IdKey = "Id";
        private const string NameKey = "Name";
        private const string PathKey = "Path";
        private const string DisplayNameKey = "DisplayName";
        private const string TypeKey = "Type";
        private const string OperatorKey = "Operator";
        private const string ParameterNameKey = "ParameterName";

        public BusinessObjectsUniverseSettings(): this(new PropertyBag())
        {}

        public BusinessObjectsUniverseSettings(PropertyBag bag) : base(bag)
        {
            //set default Port number
            Port = "6405";
		}

        public string Url
        {
            get { return GetInternal("Url", "localhost"); }
            set { SetInternal("Url", value); }
        }

        public string Port
        {
            get { return GetInternal("Port", "6405"); }
            set { SetInternal("Port", value); }
        }

        public string Username
        {
            get { return GetInternal("Username"); }
            set { SetInternal("Username", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string UniverseId
        {
            get { return GetInternal("UniverseId"); }
            set { SetInternal("UniverseId", value); }
        }

        public string UniversePath
        {
            get { return GetInternal("UniversePath"); }
            set { SetInternal("UniversePath", value); }
        }

        public string TableName
        {
            get { return GetInternal("TableName"); }
            set
            {
                SetInternal("TableName", value);
                Title = value;
            }
        }

        public int RequestTimeout
        {
            get { return GetInternalInt("RequestTimeout", 30); }
            set { SetInternalInt("RequestTimeout", value); }
        }

        public AuthenticationType AuthenticationType 
        {
            get
            {
                string authenticationType = GetInternal("AuthenticationType");
                if (authenticationType != null)
                {
                    return (AuthenticationType)Enum.Parse(
                        typeof(AuthenticationType), authenticationType);
                }

                return AuthenticationType.Enterprise;
            }
            set { SetInternal("AuthenticationType", value.ToString()); }
        }

        public Item[] Items
        {
            get
            {
                int itemsCount = GetInternalInt(ItemsCountKey, 0);
                Item[] resultItems = new Item[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    PropertyBag counterBag =
                        GetInternalSubGroup(string.Format(ItemsKey, i));

                    resultItems[i] =
                        new Item(
                            counterBag.Values[IdKey],
                            counterBag.Values[NameKey],
                            counterBag.Values[PathKey],
                            counterBag.Values[DisplayNameKey],
                            counterBag.Values[TypeKey],
                            counterBag.Values[ParameterNameKey],
                            counterBag.Values[OperatorKey]);
                }

                return resultItems;
            }
            set
            {
                if (value == null)
                {
                    SetInternalInt(ItemsCountKey, 0);
                    return;
                }

                SetInternalInt(ItemsCountKey, value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    PropertyBag itemBag = new PropertyBag();
                    itemBag.Values[DisplayNameKey] = value[i].DisplayName;
                    itemBag.Values[IdKey] = value[i].Id;
                    itemBag.Values[NameKey] = value[i].Name;
                    itemBag.Values[PathKey] = value[i].Path;
                    itemBag.Values[TypeKey] = value[i].Type;

                    if (!string.IsNullOrEmpty(value[i].ParameterName) &&
                        !string.IsNullOrEmpty(value[i].Operator))
                    {
                        itemBag.Values[ParameterNameKey] = value[i].ParameterName;
                        itemBag.Values[OperatorKey] = value[i].Operator;
                    }
                    
                    SetInternalSubGroup(string.Format(ItemsKey, i), itemBag);
                }
            }
        }

        public Filter[] Filters
        {
            get
            {
                int filtersCount = GetInternalInt(FiltersCountKey, 0);
                Filter[] resultFilters = new Filter[filtersCount];

                for (int i = 0; i < filtersCount; i++)
                {
                    PropertyBag counterBag =
                        GetInternalSubGroup(string.Format(FiltersKey, i));

                    resultFilters[i] =
                        new Filter(
                            counterBag.Values[IdKey],
                            counterBag.Values[NameKey],
                            counterBag.Values[PathKey]);
                }

                return resultFilters;
            }
            set
            {
                if (value == null)
                {
                    SetInternalInt(FiltersCountKey, 0);
                    return;
                }

                SetInternalInt(FiltersCountKey, value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    PropertyBag filterBag = new PropertyBag();
                    filterBag.Values[IdKey] = value[i].Id;
                    filterBag.Values[NameKey] = value[i].Name;
                    filterBag.Values[PathKey] = value[i].Path;

                    SetInternalSubGroup(string.Format(FiltersKey, i), filterBag);
                }
            }
        }
    }
}
