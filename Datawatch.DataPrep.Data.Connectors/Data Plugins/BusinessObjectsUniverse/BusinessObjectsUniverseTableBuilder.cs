﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.BusinessObjectsUniversePlugin.Properties;

namespace Panopticon.BusinessObjectsUniversePlugin
{
    static class BusinessObjectsUniverseTableBuilder
    {
        public static StandaloneTable BuildTable(
            IList<IList<KeyValuePair<string, string>>> rows,
            IList<QueryMetaDataDefinition> metaData,
            IList<Item> items)
        {
            if (items.Count != metaData.Count) throw new Exception(Resources.ExIncorrectMetaData);
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();
            try
            {
                foreach (Column column in CreateColumns(items, metaData))
                {
                    table.AddColumn(column);
                }
                
                foreach (IList<KeyValuePair<string, string>> row in rows)
                {
                    object[] values = new object[table.ColumnCount];

                    for (int i = 0; i < metaData.Count; i++)
                    {
                        values[i] = GetValue(row[i].Value, table.GetColumn(i).MetaData.DataType);
                    }

                    table.AddRow(values);
                }
                
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        private static IEnumerable<Column> CreateColumns(IList<Item> items,
            IList<QueryMetaDataDefinition> metaData)
        {
            IList<Column> result = new List<Column>();

            for (int i = 0; i < items.Count(); i++)
            {
                ColumnType columnType = GetColumnType(metaData[i].Type);

                switch (columnType)
                {
                    case ColumnType.Text:
                        result.Add(new TextColumn(items[i].DisplayName));
                        break;
                    case ColumnType.Numeric:
                        result.Add(new NumericColumn(items[i].DisplayName));
                        break;
                    case ColumnType.Time:
                        result.Add(new TimeColumn(items[i].DisplayName));
                        break;
                }
            }

            return result;
        }

        private static ColumnType GetColumnType(string sapType)
        {
            switch (sapType)
            {
                case "Edm.DateTime":
                case "Edm.DateTimeOffset":
                case "Edm.Time":
                    return ColumnType.Time;
                case "Edm.Byte":
                case "Edm.Decimal":
                case "Edm.Double":
                case "Edm.Float":
                case "Edm.Int16":
                case "Edm.Int32":
                case "Edm.Int64":
                case "Edm.SByte":
                case "Edm.Single":
                    return ColumnType.Numeric;
                default:
                    return ColumnType.Text;
            }
        }

        private static object GetValue(string value, Type dataType)
        {
            if (dataType == typeof (string))
            {
                return value;
            }
            
            if(dataType == typeof(double))
            {
                return value == null ? (double?)null : double.Parse(value, CultureInfo.InvariantCulture);
            }

            if (dataType == typeof (DateTime))
            {
                return value == null ? (DateTime?)null : DateTime.Parse(value, CultureInfo.InvariantCulture);
            }

            throw new ArgumentException(Resources.ExInvalidDataType);
        }
    }
}
