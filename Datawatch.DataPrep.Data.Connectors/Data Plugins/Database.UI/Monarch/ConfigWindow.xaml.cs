﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.DatabasePlugin.UI.Monarch
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    internal partial class ConfigWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(DatabaseConnectionSettings));

        private DatabaseConnectionSettings connectionSettings;

        public ConfigWindow() : this(null, null)
        {
        }

        public ConfigWindow(DatabaseConnectionSettings settings, 
            PropertyBag globalSettings)            
        {
            ConnectionSettings = settings;
            InitializeComponent();
            configPanel.GlobalSettings = globalSettings;
        }

        public DatabaseConnectionSettings ConnectionSettings
        {
            get { return connectionSettings; }
            set
            {
                if (connectionSettings != value)
                {
                    connectionSettings = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, 
                            new PropertyChangedEventArgs("ConnectionSettings"));
                    }
                }
            }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = configPanel.IsOkey;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
