﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;
using Panopticon.DatabasePlugin.Monarch;

namespace Panopticon.DatabasePlugin.UI.Monarch
{
    public class PluginUI : PluginUIBase<DatabasePlugin.Monarch.Plugin, ConnectionSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.DatabasePlugin;component" +
            "/Monarch/monarch-small.png";

        private IEnumerable<ParameterValue> parametersForTitle;

        public PluginUI(DatabasePlugin.Monarch.Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(bag, parameters);
            return new ConfigPanel(connectionSettings, owner);
        }

        public override PropertyBag GetSetting(object obj)
        {
            return ((ConfigPanel)obj).ConnectionSettings.ToPropertyBag();
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            parametersForTitle = parameters;
            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(new PropertyBag(), parameters);
            connectionSettings.QuerySettings.QueryMode = QueryMode.Table;
            connectionSettings.QuerySettings.SqlDialect = SqlDialectFactory.AccessExcel;

            ConfigWindow win =
                new ConfigWindow(connectionSettings, Plugin.GlobalSettings);
            
            return DataPluginUtils.ShowDialog(owner, win, win.ConnectionSettings);
        }

        public override string GetTitle(PropertyBag settings)
        {
            MonarchConnectionSettings connectionSettings =
                new MonarchConnectionSettings(settings, null);

            if (connectionSettings.QuerySettings.QueryMode == QueryMode.Table)
            {
                return connectionSettings.QuerySettings.SelectedTable.ToString();
            }

            if (!string.IsNullOrEmpty(connectionSettings.ConnectionStringSettingsName))
            {
                return connectionSettings.ConnectionStringSettingsName;
            }

            string connStr = connectionSettings.QuerySettings.ConnectionString;
            connStr = Utils.ApplyParameters(connStr, parametersForTitle, false);
            connStr = Utils.GetCorrectConnectionString(connStr);
            DbConnectionStringBuilder strBuilder = new DbConnectionStringBuilder();
            string[] keys = new string[] {
                    "Initial Catalog", "Data Source", "dsn", "eng",
                    "Database"
                };
            strBuilder.ConnectionString = connStr;
            foreach (string key in keys)
            {
                if (strBuilder.ContainsKey(key))
                {
                    string s = strBuilder[key].ToString();
                    try
                    {
                        return System.IO.Path.GetFileName(s);
                    }
                    catch (ArgumentException)
                    {
                        return s;
                    }
                }
            }

            return "No title";
        }
    }
}
