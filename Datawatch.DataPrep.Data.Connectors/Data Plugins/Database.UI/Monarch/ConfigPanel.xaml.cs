﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Microsoft.Win32;

namespace Panopticon.DatabasePlugin.UI.Monarch
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    internal partial class ConfigPanel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static DependencyProperty ConnectionSettingsProperty =
            DependencyProperty.Register("ConnectionSettings",
                typeof(DatabaseConnectionSettings), typeof(ConfigPanel),
                new PropertyMetadata(new PropertyChangedCallback(
                    ConnectionSettings_Changed)));
        public static RoutedCommand BrowseExportsCommand =
            new RoutedCommand("BrowseExports", typeof(ConfigPanel));
        public static RoutedCommand NewExportCommand =
            new RoutedCommand("NewExport", typeof(ConfigPanel));
        
        private static readonly string FileFilter = 
            Datawatch.DataPrep.Data.Core.Utils.CreateFilterString(
            Properties.Resources.UiDialogDatawatchFilesFilter, "dwx");

        private Window owner;

        public PropertyBag GlobalSettings { get; set; }

        public ConfigPanel(DatabaseConnectionSettings settings, Window owner)
            : this()
        {
            ConnectionSettings = settings;
            this.owner = owner;
        }

        public ConfigPanel()
        {
            InitializeComponent();
            DataContext = this;

            queryPanel.ConnectionSettingsChanged += QueryPanel_ConnectionSettingsChanged;
        }

        private void QueryPanel_ConnectionSettingsChanged(
            object sender, EventArgs args)
        {
            if (queryPanel.ConnectionSettings != null &&
                queryPanel.ConnectionSettings.ConnectionString != null &&
                queryPanel.ConnectionSettings.QuerySettings.QueryMode == QueryMode.Table)
            {
                queryPanel.ConnectionSettings.QueryBuilderViewModel.LoadTables();
            }
        }

        public DatabaseConnectionSettings ConnectionSettings
        {
            get { return (DatabaseConnectionSettings)GetValue(ConnectionSettingsProperty); }
            set { SetValue(ConnectionSettingsProperty, value); }
        }

        private static void ConnectionSettings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnConnectionSettingsChanged(
                (DatabaseConnectionSettings)args.OldValue,
                (DatabaseConnectionSettings)args.NewValue);
        }

        private void OnConnectionSettingsChanged(DatabaseConnectionSettings oldSettings,
            DatabaseConnectionSettings newSettings)
        {
            if (oldSettings != null)
            {
                newSettings.PropertyChanged -=
                    new PropertyChangedEventHandler(
                        ConnectionSettings_PropertyChanged);
            }
            if (newSettings != null)
            {
                newSettings.PropertyChanged +=
                    new PropertyChangedEventHandler(
                        ConnectionSettings_PropertyChanged);
            }
        }

        private void ConnectionSettings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
        }

        public bool IsOkey
        {
            get
            {
                if (ConnectionSettings == null)
                {
                    return false;
                }
                return queryPanel.IsOkey;
            }
        }

        protected void FireChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        private void BrowseExports_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            string path = DataPluginUtils.BrowseForFile(owner,
                Utils.ApplyParameters(ConnectionSettings.SelectedDatabase,
                                      ConnectionSettings.Parameters, false),
                FileFilter, GlobalSettings);

            if (path == null)
            {
                return;
            }
            ConnectionSettings.SelectedDatabase = path;
            this.ConnectionSettings.ConnectionString =
                Utils.GetMonarchConnectionString(path);

            if (ConnectionSettings.QuerySettings.QueryMode == QueryMode.Table)
            {
                queryPanel.ConnectionSettings.QueryBuilderViewModel.LoadTables();
            }                            
        }

        private void NewExport_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            Process[] monarchProcesses = Process.GetProcessesByName("Monarch");
            if (monarchProcesses.Length == 0)
            {
                Process.Start("Monarch.exe");
            }
            else
            {
                AutomationElement element = AutomationElement.FromHandle(
                    monarchProcesses[0].MainWindowHandle);
                if (element != null)
                {
                    try
                    {
                        element.SetFocus();
                    }
                    // Catch potential "Target Element cannot receive focus".
                    catch (InvalidOperationException ioe)
                    {
                        Log.Error(ioe.Message);
                    }
                }
            }
        }

        private void NewExport_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //32-bit Monarch on 64-bit Windows
            using (RegistryKey Key = Registry.LocalMachine.OpenSubKey(
                    @"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\App Paths\Monarch.exe\"))
            {
                if (Key != null)
                {
                    string val = Key.GetValue("").ToString();
                    if (!string.IsNullOrEmpty(val))
                    {
                        e.CanExecute = true;
                        return;
                    }
                }
            }

            //32-bit Windows/32-bit Monarch and 64-bit Monarch
            using (RegistryKey Key = Registry.LocalMachine.OpenSubKey(
                    @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\Monarch.exe\"))
            {
                if (Key != null)
                {
                    string val = Key.GetValue("").ToString();
                    if (!string.IsNullOrEmpty(val))
                    {
                        e.CanExecute = true;
                    }
                }
            }
        }
    }
}
