﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.DatabasePlugin;

namespace Panopticon.DatabasePlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, DatabaseConnectionSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.DatabasePlugin.UI;component" +
            "/database-small.png";

        protected IEnumerable<ParameterValue> parametersForTitle;

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            parametersForTitle = parameters;
            string connectionString = ConnectionDialogHelper.ShowConnectionDialog();
            if (string.IsNullOrEmpty(connectionString))
            {
                return null;
            }
            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(new PropertyBag(), parameters);
            connectionSettings.QuerySettings.PluginHostSettings =
                new PluginHostSettingsViewModel(Plugin.GlobalSettings);
            connectionSettings.QuerySettings.QueryMode = QueryMode.Table;

            connectionSettings.InitConnectionString(connectionString);
            QueryWindow win =
                new QueryWindow(connectionSettings);
            win.Owner = owner;

            return DataPluginUtils.ShowDialog(owner, win,
                win.ConnectionSettings);
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(bag, parameters);
            connectionSettings.QuerySettings.PluginHostSettings =
                new PluginHostSettingsViewModel(Plugin.GlobalSettings);

            return new ConfigPanel(connectionSettings);
        }

        public override PropertyBag GetSetting(object obj)
        {
            return ((ConfigPanel)obj).ConnectionSettings.ToPropertyBag();
        }
        
        public override string GetTitle(PropertyBag settings)
        {
            DatabaseConnectionSettings connectionSettings =
                new DatabaseConnectionSettings(settings, null, false);

            if (connectionSettings.QuerySettings.QueryMode == QueryMode.Table)
            {
                return connectionSettings.QuerySettings.SelectedTable.ToString();
            }

            if (!string.IsNullOrEmpty(connectionSettings.ConnectionStringSettingsName))
            {
                return connectionSettings.ConnectionStringSettingsName;
            }

            string connStr = connectionSettings.DatabaseConnectionString;
            connStr = Utils.ApplyParameters(connStr, parametersForTitle, false);
            connStr = Utils.GetCorrectConnectionString(connStr);
            DbConnectionStringBuilder strBuilder = new DbConnectionStringBuilder();
            string[] keys = new string[] {
                    "Initial Catalog", "Data Source", "dsn", "eng",
                    "Database"
                };
            strBuilder.ConnectionString = connStr;
            foreach (string key in keys)
            {
                if (strBuilder.ContainsKey(key))
                {
                    string s = strBuilder[key].ToString();
                    try
                    {
                        return System.IO.Path.GetFileName(s);
                    }
                    catch (ArgumentException)
                    {
                        return s;
                    }
                }
            }

            return "<No title>";
        }
    }
}
