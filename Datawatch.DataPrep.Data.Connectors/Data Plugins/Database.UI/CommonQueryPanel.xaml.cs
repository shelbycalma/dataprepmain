﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Microsoft.Win32;

namespace Panopticon.DatabasePlugin.UI
{
    /// <summary>
    /// Interaction logic for CommonQueryPanel.xaml
    /// </summary>
    public partial class CommonQueryPanel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler ConnectionSettingsChanged;

        public static DependencyProperty ConnectionSettingsProperty =
            DependencyProperty.Register("ConnectionSettings",
                typeof(DatabaseConnectionSettings), typeof(CommonQueryPanel),
                new PropertyMetadata(new PropertyChangedCallback(
                    ConnectionSettings_Changed)));

        private JavaConnectionMode javaConnectionMode = JavaConnectionMode.JndiName;

        public CommonQueryPanel(DatabaseConnectionSettings settings)
            : this()
        {
            ConnectionSettings = settings;
        }

        public CommonQueryPanel()
        {
            InitializeComponent();
            DataContext = this;
        }


        public DatabaseConnectionSettings ConnectionSettings
        {
            get { return (DatabaseConnectionSettings)GetValue(ConnectionSettingsProperty); }
            set { SetValue(ConnectionSettingsProperty, value); }
        }

        private static void ConnectionSettings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((CommonQueryPanel)obj).OnConnectionSettingsChanged(
                (DatabaseConnectionSettings)args.OldValue,
                (DatabaseConnectionSettings)args.NewValue);
        }

        private void OnConnectionSettingsChanged(DatabaseConnectionSettings oldSettings,
            DatabaseConnectionSettings newSettings)
        {

            if (oldSettings != null)
            {
                newSettings.PropertyChanged -=
                    new PropertyChangedEventHandler(
                        ConnectionSettings_PropertyChanged);
            }

            FireChanged("Tables");
            FireChanged("Columns");

            if (newSettings != null)
            {
                newSettings.PropertyChanged +=
                    new PropertyChangedEventHandler(
                        ConnectionSettings_PropertyChanged);

                newSettings.QuerySettings.LoadTablesCaption = Properties.Resources.UiLoadTablesLabel;

                newSettings.QuerySettings.IsQueryMode =
                    newSettings.QuerySettings.QueryMode == QueryMode.Query;

                JavaConnectionMode = string.IsNullOrEmpty(newSettings.JavaUrl)
                    ? JavaConnectionMode.JndiName : JavaConnectionMode.Url;
            }

            FireConnectionSettingsChanged();
        }

        protected void FireConnectionSettingsChanged()
        {
            if (ConnectionSettingsChanged != null)
            {
                ConnectionSettingsChanged(this, EventArgs.Empty);
            }
        }

        private void ConnectionSettings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {

            if (e.PropertyName == "QueryMode")
            {
                if (ConnectionSettings.QuerySettings.IsQueryMode == false &&
                    ConnectionSettings.IsDatabasePlugin == false &&
                   !string.IsNullOrEmpty(ConnectionSettings.SelectedDatabase))
                {
                    ConnectionSettings.QueryBuilderViewModel.LoadTables();
                }
            }
            if (e.PropertyName == "SelectedTable")
            {
                FireChanged("AvailableColumns");
            }
        }

        private void editConnectionButton_Click(object sender, RoutedEventArgs e)
        {
            string newConnectionString =
                ConnectionDialogHelper.EditConnectionString(ConnectionSettings.ConnectionString);

            if (newConnectionString != null)
            {
                ConnectionSettings.ConnectionString = newConnectionString;
            }
        }

        protected void FireChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
		
        public bool IsOkey
        {
            get
            {
                if (ConnectionSettings == null)
                {
                    return false;
                }
                return ConnectionSettings.QuerySettings.IsQueryMode
                    ? !string.IsNullOrEmpty(ConnectionSettings.QuerySettings.Query)
                    : ConnectionSettings.QuerySettings.SelectedTable != null;
            }
        }

        public JavaConnectionMode JavaConnectionMode
        {
            get { return javaConnectionMode; }
            set
            {
                if (value == javaConnectionMode) return;
                JavaConnectionText = null;
                javaConnectionMode = value;
                FireChanged("JavaConnectionMode");
                FireChanged("JavaConnectionText");
            }
        }

        public string JavaConnectionText
        {
            get
            {
                if (ConnectionSettings == null)
                {
                    return null;
                }
                return javaConnectionMode == JavaConnectionMode.Url
                    ? ConnectionSettings.JavaUrl
                    : ConnectionSettings.JavaJndiResourceName;
            }
            set
            {
                if (javaConnectionMode == JavaConnectionMode.Url)
                {
                    ConnectionSettings.JavaUrl = value;
                }
                else
                {
                    ConnectionSettings.JavaJndiResourceName = value;
                }
            }
        }
		
		private void PreviewData_OnClick(object sender, RoutedEventArgs e)
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
				if (ConnectionSettings.QuerySettings.SelectedColumns.Count == 0 && !ConnectionSettings.QuerySettings.IsQueryMode)
				{
					MessageBox.Show(Properties.Resources.UiErrorNoColumnsSelected, ConnectionSettings.Title);
				}
				else
				{
					int previewRowLimit = 10;
					RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

					if (previewRowLimit == 0)
					{
						previewRowLimit = 10;
					}

					string connstring = Utils.GetCorrectConnectionString(ConnectionSettings.QuerySettings.ConnectionString);
					DbDataReader reader;
					DataTable dt = new DataTable();
					string query = "";
					query = ConnectionSettings.QuerySettings.Query;
					DatabasePlugin.Executor executor = new Executor();
					DbDataAdapter adapter = executor.GetAdapter(query, connstring);
					adapter.SelectCommand.Connection.Open();
					reader = adapter.SelectCommand.ExecuteReader(
							CommandBehavior.CloseConnection |
							CommandBehavior.SingleResult |
							CommandBehavior.KeyInfo);

					using (DataSet ds = new DataSet() { EnforceConstraints = false })
					{
						ds.Tables.Add(dt);
						dt.Load(reader);
						ds.Tables.Remove(dt);
					}
					adapter.Dispose();
					var dtlimit = dt.AsEnumerable().Take(previewRowLimit).CopyToDataTable();
					PreviewGrid.ItemsSource = dtlimit.AsDataView();
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				MessageBox.Show(ex.Message, "", MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}
