﻿using System;
using System.Windows;
using System.Windows.Interop;


namespace Panopticon.DatabasePlugin.UI
{
    public class ConnectionDialogHelper
    {
        public static string ShowConnectionDialog()
        {
            MSDASC.DataLinks dataLinks = new MSDASC.DataLinksClass();
            IntPtr windowHandle = new WindowInteropHelper(
                Application.Current.MainWindow).Handle;
            dataLinks.hWnd = (int)windowHandle;
            ADODB.Connection conn = dataLinks.PromptNew() as ADODB.Connection;

            if (conn != null)
            {
                return conn.ConnectionString;
            }

            return null;
        }

        public static string EditConnectionString(string connectionString)
        {
            MSDASC.DataLinks dataLinks = new MSDASC.DataLinksClass();
            IntPtr windowHandle = new WindowInteropHelper(
                Application.Current.MainWindow).Handle;
            dataLinks.hWnd = (int)windowHandle;

            try
            {
                ADODB.Connection conn = new ADODB.Connection();
                conn.ConnectionString = connectionString;
                object temp = conn;
                bool connChanged = dataLinks.PromptEdit(ref temp);

                if (connChanged)
                {
                    return conn.ConnectionString;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Database Plugin",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            return null;
        }

    }
}
