﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.DatabasePlugin.UI
{
    class AggregationTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is AggregateType)) return DependencyProperty.UnsetValue;

            AggregateType aggType = (AggregateType)value;

            if (aggType == AggregateType.None)
            {
                return Properties.Resources.UiAggregationTypeGroupBy;
            }
            else
            {
                return aggType.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Datawatch.DataPrep.Data.Core.Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
