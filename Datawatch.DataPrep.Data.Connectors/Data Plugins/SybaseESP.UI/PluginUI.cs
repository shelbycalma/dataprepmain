﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.SybaseESPPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, SybaseESPSettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.SybaseESPPlugin.UI;component" +
            "/sybase.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            SybaseESPConfigPanel cp = new SybaseESPConfigPanel();

            cp.Settings = new SybaseESPSettings(bag, parameters);

            return cp;
        }

        public override PropertyBag GetSetting(object obj)
        {
            SybaseESPConfigPanel cp = (SybaseESPConfigPanel)obj;

            return cp.Settings.ToPropertyBag();
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            SybaseESPSettings con = new SybaseESPSettings(
                new PropertyBag(), parameters);
            con.Parameters = parameters;
            SybaseESPConfigWindow win = new SybaseESPConfigWindow(con);

            win.Owner = owner;

            if (win.ShowDialog() == true)
            {
                PropertyBag bag = con.ToPropertyBag();

                return bag;
            }

            return null;
        }
    }
}
