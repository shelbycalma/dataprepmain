﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.SybaseESPPlugin.UI
{
    /// <summary>
    /// Interaction logic for SybaseESPConfigPanel.xaml
    /// </summary>
    internal partial class SybaseESPConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
              DependencyProperty.Register("Settings",
                  typeof(SybaseESPSettings), typeof(SybaseESPConfigPanel),
                      new PropertyMetadata(new PropertyChangedCallback(
                          Settings_Changed)));

        public SybaseESPConfigPanel()
        {
            InitializeComponent();
        }

        public SybaseESPSettings Settings
        {
            get { return (SybaseESPSettings)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((SybaseESPConfigPanel)obj).OnSettingsChanged(
                (SybaseESPSettings)args.OldValue,
                (SybaseESPSettings)args.NewValue);
        }

        protected void OnSettingsChanged(SybaseESPSettings oldSettings,
            SybaseESPSettings newSettings)
        {
            DataContext = newSettings;

            if (newSettings != null)
            {
                PasswordBox.Password = newSettings.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
