﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.SybaseESPPlugin.UI
{
    /// <summary>
    /// Interaction logic for SybaseESPConfigWindow.xaml
    /// </summary>
    internal partial class SybaseESPConfigWindow : Window
    {
        private SybaseESPSettings settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(SybaseESPConfigWindow));

        public SybaseESPConfigWindow(SybaseESPSettings settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public SybaseESPSettings Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOK;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
