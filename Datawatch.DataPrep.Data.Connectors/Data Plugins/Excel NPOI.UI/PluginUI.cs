﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.ExcelNpoiPlugin.UI.Properties;
using Exceptions = Datawatch.DataPrep.Data.Framework.Exceptions;

namespace Panopticon.ExcelNpoiPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, ExcelSettings>, IFileOpenDataPluginUI<Plugin>
    {
        private const string Imageuri =
            "pack://application:,,,/Panopticon.ExcelNpoiPlugin.UI;component" +
            "/connect-excel.gif";
        
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(Imageuri,
                                                       UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            string workbookDirectory =
                SettingsHelper.GetDefaultWorkbookDirectory(Plugin.GlobalSettings);

            ConfigPanel cp = new ConfigPanel(this, workbookDirectory);

            cp.Path = bag.Values[ExcelNpoiPlugin.Plugin.PathKey];
            cp.Range = bag.Values[ExcelNpoiPlugin.Plugin.RangeKey];
            cp.Unpivot = Convert.ToBoolean(bag.Values[ExcelNpoiPlugin.Plugin.UnpivotKey]);
            cp.IsRealtimeEnabled = false;

            return cp;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            PropertyBag bag = new PropertyBag();
            bag.Values[ExcelNpoiPlugin.Plugin.PathKey] = configPanel.Path;
            bag.Values[ExcelNpoiPlugin.Plugin.RangeKey] = configPanel.Range;
            bag.Values[ExcelNpoiPlugin.Plugin.UnpivotKey] = Convert.ToString(configPanel.Unpivot);
            return bag;
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            string path = BrowseForFile();
            if (path == null)
            {
                return null;
            }

            return ShowRangeSelection(path);
        }

        private PropertyBag ShowRangeSelection(string path)
        {
            string[] ranges = Plugin.GetRanges(path);

            if (ranges == null) return null;

            string[] titles = new string[ranges.Length];

            for (int i = 0; i < ranges.Length; i++)
            {
                titles[i] = ExcelNpoiPlugin.Plugin.GetTitle(null, ranges[i]);
            }

            TableSelectionWindow dlg = new TableSelectionWindow(titles)
            {
                IsRealtimeEnabled = false,
                Owner = owner
            };

            if (dlg.ShowDialog() != true)
            {
                return null;
            }

            int selectedSheet = dlg.SelectedSheet;
            if (selectedSheet != -1)
            {
                PropertyBag bag = CreatePropertyBag(
                    path, ranges[selectedSheet]);
                return bag;
            }
            return null;
        }

        public static PropertyBag CreatePropertyBag(string path, string range)
        {
            PropertyBag settings = new PropertyBag();
            settings.Values[ExcelNpoiPlugin.Plugin.PathKey] = path;
            settings.Values[ExcelNpoiPlugin.Plugin.RangeKey] = range;
            return settings;
        }

        internal string BrowseForFile()
        {
            return BrowseForFile(null);
        }

        internal string BrowseForFile(string oldFilename)
        {
            return DataPluginUtils.BrowseForFile(owner, oldFilename,
                string.Concat(Resources.UiDialogExcelFilesFilter, "|*.xls;*.xlsx;*.xlsm"), Plugin.GlobalSettings);
        }

        public bool CanOpen(string filePath)
        {
            return DataUtils.CanOpen(filePath, fileExtensions);
        }

        public PropertyBag DoOpenFile(string filePath)
        {
            return ShowRangeSelection(filePath);
        }

        public PropertyBag DoOpenFile(string filePath, IDictionary<string, string> connectionPropertiesMap)
        {
            if (filePath == null) throw Exceptions.ArgumentNull("filePath");
            if (connectionPropertiesMap == null) throw Exceptions.ArgumentNull("connectionPropertiesMap");
            string range;
            if (!connectionPropertiesMap.TryGetValue(ExcelNpoiPlugin.Plugin.RangeKey, out range)) return null;
            if (string.IsNullOrEmpty(range)) return null;
            
            return CreatePropertyBag(filePath, range);
        }

        public override string GetTitle(PropertyBag settings)
        {
            return Plugin.GetTitle(settings);
        }

        private static readonly string[] fileExtensions =
            new[] { "xls", "xlsx", "xlsm" };

        public string[] FileExtensions
        {
            get
            {
                return fileExtensions;
            }
        }

        public string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Properties.Resources.UiExcelFiles, fileExtensions);
            }
        }
    }
}
