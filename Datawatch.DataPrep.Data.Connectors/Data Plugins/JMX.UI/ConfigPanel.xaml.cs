﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Panopticon.JMXPlugin.UI.ViewModels;

namespace Panopticon.JMXPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel: UserControl
    {
        private ConfigPanelViewModel configPanelViewModel;

        public JmxSettings JMXSettings
        {
            get { return (JmxSettings) GetValue(JMXSettingsProperty); }
            set { SetValue(JMXSettingsProperty, value);
                configPanelViewModel = new ConfigPanelViewModel(value);
                DataContext = configPanelViewModel;
            }
        }

        public static readonly DependencyProperty JMXSettingsProperty =
            DependencyProperty.Register("JMXSettings", typeof (JmxSettings), 
            typeof (ConfigPanel));

        public ConfigPanel()
        {
            InitializeComponent();
        }

        public ConfigPanel(JmxSettings settings)
        {
            JMXSettings = settings;
            InitializeComponent();
        }

        private void Attribute_Updated(object sender, DataTransferEventArgs dataTransferEventArgs)
        {
            //Trigger collection changed to properly update values and validate
            configPanelViewModel.JmxAttributeNames.Move(0,0);
        }
    }
}
