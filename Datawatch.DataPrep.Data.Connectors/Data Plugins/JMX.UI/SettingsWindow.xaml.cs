﻿using System.Linq;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.JMXPlugin.UI
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        private ICommand saveSettings;
        private ICommand cancel;
        private JmxSettings settings;

        private PluginUI jmxPluginUI;

        public SettingsWindow(PluginUI jmxPluginUI, JmxSettings settings)
        {
            this.jmxPluginUI = jmxPluginUI;
            this.settings = settings;
            saveSettings = new DelegateCommand(SaveSettingsPerform, CanSaveSettings);
            cancel = new DelegateCommand(CancelPerform);
            DataContext = this;
            InitializeComponent();
            ConfigPanel.JMXSettings = settings;
        }

        private bool CanSaveSettings()
        {
            return !string.IsNullOrEmpty(settings.JmxServiceUrl)
                   && !string.IsNullOrEmpty(settings.MBeanName)
                   && settings.JmxAttributes.All(
                    attribute => !string.IsNullOrEmpty(attribute.Name));
        }

        private void SaveSettingsPerform()
        {
            jmxPluginUI.Plugin.Settings.Merge(settings.ToPropertyBag());
            DialogResult = true;
        }

        private void CancelPerform()
        {
            DialogResult = false;
        }

        public ICommand SaveSettings
        {
            get { return saveSettings; }
        }

        public ICommand Cancel
        {
            get { return cancel; }
        }

        public JmxSettings Settings
        {
            get { return settings; }
        }
    }
}
