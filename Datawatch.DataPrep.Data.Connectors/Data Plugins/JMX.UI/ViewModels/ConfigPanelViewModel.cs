﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.JMXPlugin.UI.ViewModels
{
    public class ConfigPanelViewModel:ViewModelBase
    {
        private JmxSettings settings;
        private DelegateCommand addCommand;
        private DelegateCommand<JmxAttributeViewModel> deleteCommand;
        private ObservableCollection<JmxAttributeViewModel> jmxAttributeNames;

        public ConfigPanelViewModel(JmxSettings settings)
        {
            AddCommand = new DelegateCommand(() => 
                JmxAttributeNames.Add(new JmxAttributeViewModel
            {
                Name = string.Empty,
                DataType = new DataType(typeof(double))
            }));
            DeleteCommand = new DelegateCommand<JmxAttributeViewModel>(model =>
            {
                if (model != null)
                {
                    JmxAttributeNames.Remove(model);
                }
            });
            this.settings = settings;

            if (this.settings.JmxAttributes == null)
                {
                    this.settings.JmxAttributes = new List<JmxAttribute>();
                }
            JmxAttributeNames = new ObservableCollection<JmxAttributeViewModel>(
                this.settings.JmxAttributes
                    .Select(attribute => new JmxAttributeViewModel(attribute)));
            JmxAttributeNames.CollectionChanged += JmxAttributeNamesOnCollectionChanged;
        }

        private void JmxAttributeNamesOnCollectionChanged(object sender, 
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArgs)
        {
            this.settings.JmxAttributes = new List<JmxAttribute>(
                    JmxAttributeNames.Select(attribute => attribute.Attribute));
        }

        public string JmxServiceUrl
        {
            get { return settings.JmxServiceUrl; }
            set
            {
                settings.JmxServiceUrl = value;
                OnPropertyChanged();
            }
        }

        public string MBeanName
        {
            get { return settings.MBeanName; }
            set
            {
                settings.MBeanName = value; 
                OnPropertyChanged();
            }
        }

        public string IdColumnName
        {
            get { return settings.IdColumn; }
            set
            {
                settings.IdColumn = value; 
                OnPropertyChanged();
            }
        }

        public string TimeIdColumnName
        {
            get { return settings.TimeIdColumn; }
            set
            {
                settings.TimeIdColumn = value;
                OnPropertyChanged();
            }
        }

        public int TimeWindowSeconds
        {
            get { return settings.TimeWindowSeconds; }
            set
            {
                settings.TimeWindowSeconds = value;
                OnPropertyChanged();
            }
        }

        public int Limit
        {
            get { return settings.Limit; }
            set
            {
                settings.Limit = value; 
                OnPropertyChanged();
            }
        }

        public JmxSettings Settings
        {
            get { return settings; }
            set
            {
                settings = value;
                OnPropertyChanged("Settings");
            }
        }

        public DelegateCommand AddCommand
        {
            get { return addCommand; }
            set
            {
                addCommand = value;
                OnPropertyChanged("AddCommand");
            }
        }

        public DelegateCommand<JmxAttributeViewModel> DeleteCommand
        {
            get
            {
                return deleteCommand;
            }
            set
            {
                deleteCommand = value;
                OnPropertyChanged("DeleteCommand");
            }
        }

        public ObservableCollection<JmxAttributeViewModel> JmxAttributeNames
        {
            get
            {
                return jmxAttributeNames;
            }
            set
            {
                jmxAttributeNames = value;
                OnPropertyChanged("JmxAttributeNames");
            }
        }
    }
}
