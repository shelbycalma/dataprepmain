﻿using System;

namespace Panopticon.JMXPlugin.UI.ViewModels
{
    public class JmxAttributeViewModel: ViewModelBase
    {
        private readonly JmxAttribute attribute;

        public JmxAttributeViewModel(JmxAttribute attribute = null)
        {
            if (attribute == null)
            {
                attribute = new JmxAttribute();
            }
            this.attribute = attribute;
        }

        public string Name
        {
            get { return attribute.Name; }
            set 
            { 
                attribute.Name = value; 
                OnPropertyChanged(); 
            }
        }

        public DataType DataType
        {
            get { return attribute.DataType; }
            set
            {
                attribute.DataType = value; 
                OnPropertyChanged();
            }
        }

        public JmxAttribute Attribute
        {
            get { return attribute; }
        }

        public DataType[] AvailableDataTypes
        {
            get
            {
                return new[] {
                    new DataType(typeof(String)), 
                    new DataType(typeof(Double))
                };
            }
        }
    }
}
