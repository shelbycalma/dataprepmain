﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Panopticon.JMXPlugin.UI.ViewModels
{
    public class ViewModelBase: INotifyPropertyChanged
    {
        private readonly Dictionary<string, object> 
            _propertyBackingDictionary = new Dictionary<string, object>();

        protected T GetProperty<T>([CallerMemberName]string propertyName = null)
        {
            if (propertyName == null)
                throw new ArgumentNullException("propertyName");

            object value;
            if (_propertyBackingDictionary.TryGetValue(propertyName, out value))
            {
                return (T)value;
            }

            return default(T);
        }

        protected bool SetProperty<T>(T newValue, [CallerMemberName]string propertyName = null)
        {
            if (propertyName == null) 
                throw new ArgumentNullException("propertyName");

            if (EqualityComparer<T>
                .Default.Equals(newValue, 
                GetProperty<T>(propertyName))) return false;

            _propertyBackingDictionary[propertyName] = newValue;
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
