﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.JMXPlugin.UI
{
    public class PluginUI: PluginUIBase<Plugin, JmxSettings>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(
               IEnumerable<ParameterValue> parameters)
        {
            this.CheckLicense();

            JmxSettings newSettings = new JmxSettings(new PropertyBag())
            {
                Title = Plugin.Title
            };

            SettingsWindow window = new SettingsWindow(this, newSettings);

            if (window.ShowDialog() == true)
            {
                return newSettings.ToPropertyBag();
            }
            else
            {
                return null;
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ConfigPanel cp = new ConfigPanel(new JmxSettings(bag));
            return cp;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            return configPanel.JMXSettings.ToPropertyBag();
        }

    }
}
