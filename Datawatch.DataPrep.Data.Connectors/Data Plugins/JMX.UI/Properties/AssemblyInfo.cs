﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Datawatch.DataPrep.Data.Framework.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.JMXPlugin.UI.dll")]
[assembly: AssemblyDescription("Datawatch JMX UI Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription("JMX", "Streaming")]
