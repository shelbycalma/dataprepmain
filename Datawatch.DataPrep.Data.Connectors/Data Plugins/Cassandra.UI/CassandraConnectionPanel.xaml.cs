﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.UI;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Utils = Datawatch.DataPrep.Data.Framework.Utils;

namespace Panopticon.CassandraPlugin.UI
{
    /// <summary>
    /// Interaction logic for CassandraConnectionPanel.xaml
    /// </summary>
    public partial class CassandraConnectionPanel : UserControl
    {
        private ICommand testConnectionCommand;

        public static DependencyProperty SettingsProperty =
           DependencyProperty.Register("Settings", typeof(CassandraSettingsViewModel), typeof(CassandraConnectionPanel), new PropertyMetadata(Settings_Changed));

        public CassandraConnectionPanel()
        {
            InitializeComponent();
        }

        public CassandraSettingsViewModel Settings
        {
            get { return (CassandraSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        public ICommand TestConnectionCommand
        {
            get
            {
                if (testConnectionCommand == null)
                {
                    testConnectionCommand = new DelegateCommand(TestConnection, CanTestConnection);
                }

                return testConnectionCommand;
            }
        }

        private void TestConnection()
        {
            try
            {
                Settings.TestConnection();
                MessageBoxEx.Show(Datawatch.DataPrep.Data.Core.UI.Utils.GetOwnerWindow(this), Properties.Resources.UiTestConnectionSuccess);
            }
            catch(Exception ex)
            {
                Log.Exception(ex);
                MessageBoxEx.Show(Datawatch.DataPrep.Data.Core.UI.Utils.GetOwnerWindow(this), ex.Message, null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanTestConnection()
        {
            return Settings != null && Settings.CanTestConnection();
        }

        private static void Settings_Changed(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((CassandraConnectionPanel)obj).OnSettingsChanged(
                (CassandraSettingsViewModel)args.OldValue,
                (CassandraSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(CassandraSettingsViewModel oldSettings, CassandraSettingsViewModel newSettings)
        {
            DataContext = newSettings;

            if (newSettings != null)
            {
                PasswordBox.Password = newSettings.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}
