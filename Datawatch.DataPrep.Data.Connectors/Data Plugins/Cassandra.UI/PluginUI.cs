﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.CassandraPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, CassandraSettings>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            CassandraSettings cassandraSettings = new CassandraSettings();
            CassandraSettingsViewModel settingsView = new CassandraSettingsViewModel(cassandraSettings, parameters);
            CassandraConnectionWindow window = new CassandraConnectionWindow(settingsView);
            window.Owner = owner;

            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            PropertyBag properties = cassandraSettings.ToPropertyBag();
            return properties;
        }

        public override PropertyBag GetSetting(object element)
        {
            CassandraConnectionPanel panel = (CassandraConnectionPanel)element;
            return panel.Settings.Model.ToPropertyBag();
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            CassandraSettings cassandraSettings = Plugin.CreateSettings(bag);

            return new CassandraConnectionPanel()
            {
                Settings = new CassandraSettingsViewModel(cassandraSettings, parameters)
            };
        }
    }
}
