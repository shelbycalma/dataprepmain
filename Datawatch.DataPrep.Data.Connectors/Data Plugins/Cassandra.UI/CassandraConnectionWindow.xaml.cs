﻿using System;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.UI;
using Datawatch.DataPrep.Data.Framework;
using Utils = Datawatch.DataPrep.Data.Framework.Utils;

namespace Panopticon.CassandraPlugin.UI
{
    /// <summary>
    /// Interaction logic for CassandraConnectionWindow.xaml
    /// </summary>
    internal partial class CassandraConnectionWindow : Window
    {
        private readonly CassandraSettingsViewModel settings;

        public static RoutedCommand OkCommand = new RoutedCommand("Ok", typeof(CassandraConnectionWindow));

        public CassandraConnectionWindow(CassandraSettingsViewModel settings)
        {
            this.settings = settings;

            InitializeComponent();
            cancelButton.Click += cancelButton_Click;
            connectionPanel.Settings = settings;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Close();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                settings.SetTableName();
                DialogResult = true;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBoxEx.Show(Datawatch.DataPrep.Data.Core.UI.Utils.GetOwnerWindow(this), ex.Message, null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
