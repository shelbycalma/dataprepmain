﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.CassandraPlugin.UI
{
    public class CassandraSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public CassandraSettings Model { get; private set; }
        public IEnumerable<ParameterValue> Parameters { get; private set; }

        public CassandraSettingsViewModel(CassandraSettings model, IEnumerable<ParameterValue> parameters)
        {
            Model = model;
            Parameters = parameters;
        }

        public string ConnectionUrl
        {
            get { return this.Model.ConnectionUrl; }
            set
            {
                if (value != this.Model.ConnectionUrl)
                {
                    this.Model.ConnectionUrl = value;
                    OnPropertyChanged("ConnectionUrl");
                }
            }
        }

        public string Port
        {
            get { return this.Model.Port; }
            set
            {
                if (value != this.Model.Port)
                {
                    this.Model.Port = value;
                    OnPropertyChanged("Port");
                }
            }
        }

        public string KeySpace
        {
            get { return this.Model.KeySpace; }
            set
            {
                if (value != this.Model.KeySpace)
                {
                    this.Model.KeySpace = value;
                    OnPropertyChanged("KeySpace");
                }
            }
        }

        public string UserId
        {
            get { return this.Model.UserId; }
            set
            {
                if (value != this.Model.UserId)
                {
                    this.Model.UserId = value;
                    OnPropertyChanged("UserId");
                }
            }
        }

        public string Password
        {
            get { return this.Model.Password; }
            set
            {
                if (value != this.Model.Password)
                {
                    this.Model.Password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public bool EncloseParameterInQuote
        {
            get { return this.Model.EncloseParameterInQuote; }
            set
            {
                if (value != this.Model.EncloseParameterInQuote)
                {
                    this.Model.EncloseParameterInQuote = value;
                    OnPropertyChanged("EncloseParameterInQuote");
                }
            }
        }

        public string CassandraQuery
        {
            get { return this.Model.CassandraQuery; }
            set
            {
                if (value != this.Model.CassandraQuery)
                {
                    this.Model.CassandraQuery = value;
                    OnPropertyChanged("CassandraQuery");
                }
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return this.Model.TimeZoneHelper; }
        }

        public string TableName
        {
            get { return this.Model.Title; }
            set
            {
                if (value != this.Model.Title)
                {
                    this.Model.Title = value;
                }
            }
        }

        public bool IsOk
        {
            get
            {
                return !(string.IsNullOrWhiteSpace(CassandraQuery)
                         || string.IsNullOrWhiteSpace(ConnectionUrl)
                         || string.IsNullOrWhiteSpace(KeySpace)
                         || string.IsNullOrWhiteSpace(Port));

            }
        }

        public void TestConnection()
        {
            (new CassandraClient(this.Model, Parameters)).TestConnection();
        }

        public bool CanTestConnection()
        {
            return !(string.IsNullOrWhiteSpace(ConnectionUrl) || string.IsNullOrWhiteSpace(KeySpace) || string.IsNullOrWhiteSpace(Port));
        }
        
        public void SetTableName()
        {
            CassandraClient client = new CassandraClient(this.Model, Parameters);

            Model.Title = client.GetTableName();
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
