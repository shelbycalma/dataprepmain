﻿namespace Panopticon.MongoDBPlugin.Enums
{
    public enum ArrayExpansionType
    {
        RowWise = 0,
        ColumnWise = 1,
        Hierarchy = 2
    }
}