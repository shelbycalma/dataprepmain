﻿namespace Panopticon.MongoDBPlugin.Enums
{
    public enum ColumnStructure
    {
        Value = 0,
        RowArray = 1,
        ColumnArray = 2,
        HierarchyStructure = 3,
        HierarchyLabel = 4
    }
}