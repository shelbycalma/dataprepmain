﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.MongoDBPlugin.Enums;

namespace Panopticon.MongoDBPlugin.Managers

{
    public class ValueManager
    {
        #region Private Variables

        private MongoSettings settings;

        #endregion

        #region Constructors

        public ValueManager(
            MongoSettings settings,
            Dictionary<string, object> columnDictionary)
        {
            this.settings = settings;

            ValueDictionary = new Dictionary<string, object>();

            var mongoParseSettings =
                settings.ParserSettings as MongoParserSettings;

            var colDefs = mongoParseSettings.Columns;
            string coldefname;

            if (colDefs != null)
            {
                int colIndex = 0;
                foreach (MongoDBColumnDefinition colDef in colDefs)
                {
                    if (colDef.Structure == ColumnStructure.Value)
                    {
                        coldefname = colDef.Name;
                        if (columnDictionary.ContainsKey(coldefname))
                        {
                            try
                            {
                                AddValue(coldefname,
                                    columnDictionary[colDef.Name]);
                            }
                            catch (KeyNotFoundException e)
                            {
                                    Log.Error(Properties.Resources.LogJsonPathNotFound);
                                    Log.Error(e.ToString());
                            }
                        }
                        
                    }
                    colIndex++;
                }
            }
        }

        #endregion

        #region Properties

        public Dictionary<string, object> ValueDictionary { get; set; }

        public bool HasValues
        {
            get { return ValueDictionary.Any(); }
        }

        #endregion

        #region General Methods

        public void AddValue(string name, object value)
        {
            if (ValueDictionary.ContainsKey(name))
            {
                return;
            }
            ValueDictionary.Add(name, value);
        }

        public object GetValue(string name)
        {
            try
            {
                object retVal = ValueDictionary[name];
                ValueDictionary.Remove(name);
                return retVal;
            }
            catch (KeyNotFoundException e)
            {
                Log.Error(Properties.Resources.LogMissingValueColumn);
                Log.Error(e.ToString());
                return null;
            }
        }

        public KeyValuePair<string, object> GetFirstValue()
        {
            KeyValuePair<string, object> retVal =
                ValueDictionary.First();

            ValueDictionary.Remove(retVal.Key);

            return retVal;
        }

        #endregion
    }
}