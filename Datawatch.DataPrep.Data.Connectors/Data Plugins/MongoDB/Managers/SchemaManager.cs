﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.MongoDBPlugin.Managers
{
    public class SchemaManager
    {
        #region Private Variables

        #endregion

        #region Constructors

        public SchemaManager(IEnumerable<Column> suppliedSchema = null)
        {
            SchemaList = new List<Column>();
            SchemaDictionary = new Dictionary<string, ColumnIndexPair>();

            if (suppliedSchema != null)           
            {
                SchemaList = new List<Column>();
                int i = 0;
                foreach (Column col in suppliedSchema)
                {
                    SchemaList.Add(col);
                    SchemaDictionary.Add(col.Name, new ColumnIndexPair(i, col));
                    i++;
                }
            }
        }

        #endregion

        #region Properties
        //This is a ORDERED list for Index -> Col/Name lookups
        private List<Column> SchemaList { get; set; }

        //This is an UNORDERED dictionary for Name -> Col/Index lookups
        private Dictionary<string, ColumnIndexPair> SchemaDictionary { get; set; } 

        #endregion

        #region General Methods

        public void AddColumn(Column col)
        {
            if (col != null && !SchemaDictionary.ContainsKey(col.Name))
            {
                //List Update
                SchemaList.Add(col);

                //Dictionary Update
                SchemaDictionary.Add(
                    col.Name, new ColumnIndexPair(SchemaDictionary.Count, col));
            }
        }

        public void InsertColumn(int index, Column col)
        {
            if (col != null && !SchemaDictionary.ContainsKey(col.Name))
            {
                //List Update
                SchemaList.Insert(index, col);

                //Dictionary Update
                foreach (var kvp in SchemaDictionary)
                {
                    if (kvp.Value.Index >= index)
                    {
                        kvp.Value.Index += 1;
                    }
                }
                SchemaDictionary.Add(col.Name, new ColumnIndexPair(index, col));
            }
        }

        public Column GetColumn(string name)
        {
            try
            {
                return SchemaDictionary[name].Column;
            }
            catch (KeyNotFoundException e)
            {
                Log.Error(Properties.Resources.LogColumnNotFound);
                Log.Exception(e);
                return null;
            }
        }

        public Column GetColumn(int index)
        {
            try
            {
                return SchemaList[index];
            }
            catch (ArgumentOutOfRangeException e)
            {
                Log.Error(Properties.Resources.LogIndexNotFound + index);
                Log.Exception(e);
                return null;
            }
        }

        public string GetColumnName(int index)
        {
            try
            {
                return GetColumn(index).Name;
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.LogColumnNameFailure);
                Log.Exception(e);
                return String.Empty;
            }
        }

        public int GetColumnIndex(string name)
        {
            try
            {
                return SchemaDictionary[name].Index;
            }
            catch (KeyNotFoundException e)
            {
                Log.Error(Properties.Resources.LogColumnNotFound);
                Log.Exception(e);
                return -1;
            }
        }

        public void RemoveColumn(int index)
        {
            var col = GetColumn(index);

            if (col != null)
            {
                //List Update
                SchemaList.Remove(col);

                //Dictionary Update
                foreach (var kvp in SchemaDictionary)
                {
                    if (kvp.Value.Index > index)
                    {
                        kvp.Value.Index -= 1;
                    }
                }
                SchemaDictionary.Remove(col.Name);
            }

        }

        public void RemoveColumn(string name)
        {
            var col = GetColumn(name);
            int index = GetColumnIndex(name);

            if (col != null)
            {
                //List Update
                SchemaList.Remove(col);
                
                //Dictionary Update
                foreach (var kvp in SchemaDictionary)
                {
                    if (kvp.Value.Index > index)
                    {
                        kvp.Value.Index -= 1;
                    }
                }
                SchemaDictionary.Remove(col.Name);
            }
        }

        #endregion
    }

    public class ColumnIndexPair
    {
        public Column Column { get; set; }
        public int Index { get; set; }
        
        public ColumnIndexPair(int index, Column col)
        {
            Column = col;
            Index = index;
        }
    } 
}