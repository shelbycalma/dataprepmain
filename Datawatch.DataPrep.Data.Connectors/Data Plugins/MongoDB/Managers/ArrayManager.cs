﻿using System.Collections.Generic;
using System.Linq;
using Panopticon.MongoDBPlugin.Enums;

namespace Panopticon.MongoDBPlugin.Managers

{
    public class ArrayManager
    {
        // Properties
        public Dictionary<string, Queue<object>> Queues { get; set; }

        public Dictionary<string, ArrayExpansionType> QueueExpansionTypes { get;
            set; }

        public int MaxCount { get; set; }

        public IEnumerable<string> ArrayNames
        {
            get { return new List<string>(Queues.Keys); }
        } 

        public int NumberOfArrays
        {
            get { return Queues.Count; }
        }

        public bool HasArrays
        {
            get { return Queues.Count > 0; }
        }

        // Constructor
        public ArrayManager()
        {
            Queues = new Dictionary<string, Queue<object>>();
            QueueExpansionTypes = new Dictionary<string, ArrayExpansionType>();
        }

        // Methods
        public void AddArray(
            string name, IEnumerable<object> bsonArray,
            ArrayExpansionType expType)
        {
            if (Queues.ContainsKey(name))
                return;

            Queues.Add(name, new Queue<object>(bsonArray));
            QueueExpansionTypes.Add(name, expType);

            if (bsonArray.Count() > MaxCount)
                MaxCount = bsonArray.Count();
        }

        public object GetNextElement(string arrayName)
        {
            if (!Queues.ContainsKey(arrayName))
                return null;

            Queue<object> queue = Queues[arrayName];
            if (queue.Count == 0)
            {
                Queues.Remove(arrayName);
                QueueExpansionTypes.Remove(arrayName);
                return null;
            }

            return queue.Dequeue();
        }

        public void DivideIntoColumnAndRowManagers(
            out ArrayManager rowManager,
            out ArrayManager colManager)
        {
            rowManager = new ArrayManager();
            colManager = new ArrayManager();

            int index = 0;

            foreach (var kvp in QueueExpansionTypes)
            {
                if (kvp.Value == ArrayExpansionType.RowWise)
                {
                    rowManager.AddArray(kvp.Key, Queues[kvp.Key],
                        ArrayExpansionType.RowWise);
                }
                else if (kvp.Value == ArrayExpansionType.ColumnWise)
                {
                    colManager.AddArray(kvp.Key, Queues[kvp.Key],
                        ArrayExpansionType.ColumnWise);
                }
                index++;
            }
        }
    }
}