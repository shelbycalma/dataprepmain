﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.MongoDBPlugin.Properties;

namespace Panopticon.MongoDBPlugin
{
    public class MongoExceptions : Exceptions
    {
        public static Exception UnableToConnectToServer(
            Exception exception, string server)
        {
            return
                CreateInvalidOperation(String.Format(
                    Resources.ExUnableToConnect, server));
        }

        public static Exception FailedToGenerateColumns(Exception exception)
        {
            return CreateInvalidOperation(String.Format(
                Resources.ExFailedToGenerateColumns, exception.Message));
        }

        public static Exception InvalidId(Exception exception, string id)
        {
            return
                CreateInvalidOperation(String.Format(Resources.ExInvalidId, id));
        }

        public static Exception BsonParseFailure(Exception exception, string r)
        {
            return CreateInvalidOperation(String.Format("{0} {1}",
                Resources.ExBsonParseFailure, r));
        }

        public static Exception NoHierarchyLabel(Exception exception)
        {
            return
                CreateInvalidOperation(String.Format(Resources.ExNoHierarchyLabel));
        }
    }
}