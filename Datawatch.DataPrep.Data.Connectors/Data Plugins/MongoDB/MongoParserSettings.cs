﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Panopticon.MongoDBPlugin.Enums;

namespace Panopticon.MongoDBPlugin
{
    public class MongoParserSettings : ParserSettings
    {
        public MongoParserSettings(PropertyBag bag) : base(bag)
        {
        }

        public override string GetDescription()
        {
            return string.Empty;
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new MongoDBColumnDefinition(bag);
        }

        public override IParser CreateParser()
        {
            return new MongoParser(this);
        }

        public MongoDBColumnDefinition GetColumnDefinition(string path)
        {
            foreach (MongoDBColumnDefinition col in Columns)
            {
                if (col.JsonPath == path)
                {
                    return col;
                }
            }

            return null;
        }

        public bool HasArrays()
        {
            foreach (MongoDBColumnDefinition col in base.Columns)
            {
                if (col.Structure == ColumnStructure.RowArray ||
                    col.Structure == ColumnStructure.ColumnArray)
                {
                    return true;
                }
            }

            return false;
        }
    }
}