﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.MongoDBPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<MongoSettings>
    {
        internal const string PluginId = "MongoDBPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "MongoDB";
        internal const string PluginType = DataPluginTypes.Database;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override MongoSettings CreateSettings(PropertyBag bag)
        {
            return new MongoSettings(bag, pluginManager);
        }

        public override ITable GetData(
            string workbookDir, string dataDir, PropertyBag bag,
            IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            CheckLicense();
            var setts = new MongoSettings(bag, pluginManager, parameters);
            MongoHelper helper = new MongoHelper(setts, this.errorReporter);
            StandaloneTable table = helper.BuildTable(rowcount, setts.Parameters);

            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }
    }
}