﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using MongoDB.Bson;
using MongoDB.Driver;
//using MongoDB.Driver.Builders;
using Panopticon.MongoDBPlugin.Enums;
using Panopticon.MongoDBPlugin.Managers;

namespace Panopticon.MongoDBPlugin
{
    public class MongoHelper
    {
        #region Private Variables

        // Fields
        private readonly MongoSettings _settings;
        private MongoClient _client;
        //private MongoServer _server;
        private IMongoDatabase _database;
        private IPluginErrorReportingService errorReporter;

        #endregion

        #region Constructors

        // Constructors
        public MongoHelper(MongoSettings settings, 
            IPluginErrorReportingService errorReporter)
        {
            _settings = settings;
            this.errorReporter = errorReporter;
            EstablishConnection();
        }

        #endregion

        #region Properties

        public IMongoDatabase Database
        {
            get { return _database; }
        }

        #endregion

        #region Array Methods

        private void DiscoverBsonArrayType(
            BsonArray array, MongoDBColumnDefinition col)
        {
            if (array.Count > 0)
            {
                switch (array[0].BsonType)
                {
                    case BsonType.Boolean:
                    case BsonType.String:
                        col.Type = ColumnType.Text;
                        break;
                    case BsonType.DateTime:
                    case BsonType.Timestamp:
                        col.Type = ColumnType.Time;
                        break;
                    case BsonType.Double:
                    case BsonType.Int32:
                    case BsonType.Int64:
                        col.Type = ColumnType.Numeric;
                        break;
                    default:
                        col.Type = ColumnType.Text;
                        break;
                }
            }
            else
            {
                col.Type = ColumnType.Text;
            }
        }

        #endregion

        #region Array Row Expansion

        private void AddArrayRowWise(
            StandaloneTable table, object[] row,
            ArrayManager arrayManager, SchemaManager schemaManager,
            ValueManager valueManager)
        {
            // Iterate to the max count of any of the arrays
            for (int i = 0; i < arrayManager.MaxCount; i++)
            {
                // Set the row value for each of the arrays
                foreach (string arrName in arrayManager.ArrayNames)
                {
                    object obj = arrayManager.GetNextElement(arrName);
                    if (obj != null)
                    {
                        row[schemaManager.GetColumnIndex(arrName)]
                            = obj.ToString();
                    }
                    else
                    {
                        row[schemaManager.GetColumnIndex(arrName)]
                            = null;
                    }
                }

                //fill in the non-array fields in a row
                FillNonArrayFields(row, schemaManager, valueManager);

                row = AddAndPopulateTimeColumn(table, row, i, schemaManager);

                table.AddRow(row);
            }
        }

        #endregion

        #region Array Column Expansion

        private object[] AddArrayColumnWise(
            StandaloneTable table, object[] row,
            ArrayManager colArrayManager, ArrayManager rowArrayManager,
            SchemaManager schemaManager, ValueManager valueManager)
        {
            foreach (string arrName in colArrayManager.ArrayNames)
            {
                var parserSettings =
                    (MongoParserSettings) _settings.ParserSettings;

                var colDef = parserSettings.GetColumnDefinition(arrName);


                for (int i = 0; i < colDef.MaxArrayColumns; i++)
                {
                    //get object in array to insert
                    object obj = colArrayManager.GetNextElement(arrName);

                    int insertIndex =
                        schemaManager.GetColumnIndex(arrName + "[" + i + "]");

                    row[insertIndex] = obj;
                }
            }

            //if there's no further expansion to be done, add to the table.
            if (!rowArrayManager.HasArrays)
            {
                //fill in the non-array fields in a row
                FillNonArrayFields(row, schemaManager, valueManager);

                table.AddRow(row);
            }

            return row;
        }

        #endregion

        #region Array Hierarchy Expansion

        private void AddHierarchyColumns(
            StandaloneTable table, MongoDBColumnDefinition childCol,
            MongoDBColumnDefinition labelCol)
        {
            int deepestLevelNumber = 1;
            string hierachyName = "";
            try
            {
                hierachyName = labelCol.Name;
            }
            catch (NullReferenceException e)
            {
                throw MongoExceptions.NoHierarchyLabel(e);
            }

            //get the field that contains the children
            string childColPath = childCol.JsonPath;

            //get the values held in each child field
            IAsyncCursor<BsonDocument> documents = this.GetMongoCursor(-1, null);

            while (documents.MoveNext())
            {
                IEnumerable<BsonDocument> batch = documents.Current;
                foreach (BsonDocument doc in batch)
                {
                    BsonArray children =
                        (BsonArray)(doc.GetElement(childColPath).Value);
                    deepestLevelNumber = this.DiscoverHierarchyDepth(
                        children, childColPath, deepestLevelNumber);
                }
            }

            //create the proper number of hierarchy columns
            for (int i = 0; i < deepestLevelNumber; i++)
            {
                if (i == 0)
                {
                    table.AddColumn(new TextColumn(hierachyName));
                }
                else
                {
                    table.AddColumn(
                        new TextColumn(hierachyName + "(Level " + (i) + ")"));
                }
            }

            //save in settings for later use
            _settings.NumberOfHierarchyColumns = deepestLevelNumber;
        }

        private int DiscoverHierarchyDepth(
            BsonArray childrenArr,
            string childPathField, int currentLevel)
        {
            ArrayManager arrayMan = new ArrayManager();
            int currentLevelNumber = currentLevel;
            int deepestLevelNumber = 0;
            string arrayName = childrenArr.GetHashCode().ToString();

            arrayMan.AddArray(arrayName, childrenArr,
                ArrayExpansionType.Hierarchy);

            BsonDocument doc =
                (BsonDocument) (arrayMan.GetNextElement(arrayName));

            bool markedThisLevel = false;
            while (doc != null)
            {
                if (!markedThisLevel)
                {
                    currentLevelNumber++;
                    if (currentLevelNumber > deepestLevelNumber)
                    {
                        deepestLevelNumber = currentLevelNumber;
                    }
                    markedThisLevel = true;
                }
                BsonArray newChildrenArr =
                    (BsonArray) (doc.GetElement(childPathField).Value);
                int returnNumber = this.DiscoverHierarchyDepth(
                    newChildrenArr, childPathField, currentLevelNumber);

                if (returnNumber > deepestLevelNumber)
                {
                    deepestLevelNumber = returnNumber;
                }

                doc = (BsonDocument) (arrayMan.GetNextElement(arrayName));
            }

            return deepestLevelNumber;
        }

        private void AddHierarchyRows(
            StandaloneTable table, BsonDocument doc,
            int rowLength, SchemaManager schemaManager,
            List<string> path = null)
        {
            MongoParser parser =
                (MongoParser) _settings.ParserSettings.CreateParser();
            string label = _settings.HierarchyLabelColumnName;
            string structure = _settings.HierarchyStructureColumnName;

            object[] row = new object[rowLength];

            ArrayManager arrayManager = new ArrayManager();
            Dictionary<string, object> columnDictionary =
                parser.Parse(doc, _settings.AutomaticTimeColumn, arrayManager);
            ValueManager valueManager = new ValueManager(_settings,
                columnDictionary);

            if (path == null)
            {
                row[0] = doc.GetElement(_settings.HierarchyLabelColumnName)
                    .Value.ToString();

                path = new List<string>(
                    new[] {doc.GetElement(label).Value.ToString()});
            }

            //retrieve the appropriate array manager queue for children
            BsonDocument currDoc =
                (BsonDocument) (arrayManager.GetNextElement(structure));
            while (currDoc != null)
            {
                var newPath = new List<string>(path)
                {
                    currDoc.GetElement(
                        _settings.HierarchyLabelColumnName).Value.ToString()
                };

                //begin recursing on the queue of the array manager
                AddHierarchyRows(table, currDoc, row.Length,
                    schemaManager, newPath);

                currDoc =
                    (BsonDocument) (arrayManager.GetNextElement(structure));
            }

            foreach (var kvp in columnDictionary)
            {
                if (valueManager.ValueDictionary.ContainsKey(kvp.Key))
                {
                    valueManager.ValueDictionary[kvp.Key] = kvp.Value;
                }
            }

            PopulateHierarchyRow(table, doc, path, row, arrayManager,
                structure, schemaManager, valueManager);
        }

        private void PopulateHierarchyRow(
            StandaloneTable table, BsonDocument doc,
            List<string> path, object[] row, ArrayManager arrayManager,
            string structure, SchemaManager schemaManager,
            ValueManager valueManager)
        {
            //filling ragged hierarchies
            FillRowToDeepestLevelIfLeafNode(doc, structure, path);

            //populate hierarchy columns in row
            InsertPathIntoHierarchyColumns(path, row);

            //Remove the structure array from the array manager before
            //processing any data array expansion.
            RemoveStructureFromArrayManager(arrayManager, structure);

            //Add the data arrays, if any others exist.
            if (arrayManager.HasArrays)
            {
                AddDataArrays(table, row, arrayManager, schemaManager,
                    valueManager);
            }
                //if no other arrays are found, add to table.
            else
            {
                //fill in the non-array fields in a row
                FillNonArrayFields(row, schemaManager, valueManager);

                table.AddRow(row);
            }
        }

        private void FillRowToDeepestLevelIfLeafNode(
            BsonDocument doc,
            string structure, List<string> path)
        {
            bool leafNode = doc.GetElement(structure)
                .Value.AsBsonArray.Count == 0;
            if (leafNode && path.Count < _settings.NumberOfHierarchyColumns)
            {
                for (int i = path.Count;
                    i < _settings.NumberOfHierarchyColumns;
                    i++)
                {
                    path.Add(path[i - 1]);
                }
            }
        }

        private void InsertPathIntoHierarchyColumns(
            List<string> path, object[] row)
        {
            for (int i = 0;
                i < path.Count &&
                i < _settings.NumberOfHierarchyColumns;
                i++)
            {
                row[i] = path[i];
            }
        }

        private void FillNonArrayFields(
            object[] row,
            SchemaManager schemaManger, ValueManager valManager)
        {
            while (valManager.HasValues)
            {
                KeyValuePair<string, object> kvp = valManager.GetFirstValue();

                int placementIndex = schemaManger.GetColumnIndex(kvp.Key);

                //CONN-33 MongoDB is not Parsing numbers itself before storing it into the numeric column
                if (schemaManger.GetColumn(placementIndex).GetType().Name == "NumericColumn")
                {
                    row[placementIndex] = Convert.ToDouble(kvp.Value, CultureInfo.InvariantCulture);
                }
                else
                {
                    row[placementIndex] = kvp.Value;
                }
            }
        }

        private void RemoveStructureFromArrayManager(
            ArrayManager arrayManager,
            string structure)
        {
            arrayManager.Queues.Remove(structure);
        }

        #endregion
        
        #region String Filter Methods

        private string ReplaceQueryParametersAndKeywords()
        {
            string query = _settings.CustomFindQuery;

            //remove whitespace characters before processing
            query = query.Replace("\n", "");
            query = query.Replace("\r", "");
            query = query.Replace("\t", "");

            //stack to track indices of "open" braces.
            var stack = new Stack<int>();

            for (int i = 0; i < query.Length; i++)
            {
                if (query[i] == '{')
                {
                    stack.Push(i);
                }
                else if (query[i] == '}')
                {
                    if (stack.Count > 0)
                    {
                        //measure how long the braces span
                        var startIndex = stack.Pop();
                        var endLength = ((i + 1) - startIndex);

                        var param = query.Substring(startIndex, endLength);

                        //regardless of value, pass as a parameter
                        var paramValue =
                            _settings.ParameterizeValue(param);

                        //if it has a value, replace it and keep searching
                        if (param != paramValue)
                        {
                            query = query.Remove(startIndex, endLength);
                            query = query.Insert(startIndex, paramValue);
                            i = startIndex + paramValue.Length - 1;
                        }
                    }
                    else
                    {
                        //this would only happen if you wrote a closing brace
                        //without an open one.
                        var reStr =
                            Properties.Resources.LogInvalidQueryOpenBrace;
                        Log.Error(reStr);
                        return reStr;
                    }
                }
                else if (query[i] == 'N')
                {
                    try
                    {
                        //Do not change this evaluation order.
                        if (query.Substring(i, 9) == "NOWPOSIX(")
                        {
                            int iAfterReplace;
                            query = this.ReplaceNowKeyword(
                                query, i, "POSIX", out iAfterReplace);
                            i = iAfterReplace;
                        }
                        else if (query.Substring(i, 11) == "NOWISODATE(")
                        {
                            int iAfterReplace;
                            query = this.ReplaceNowKeyword(
                                query, i, "ISODATE", out iAfterReplace);
                            i = iAfterReplace;
                        }
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        Log.Error(Properties.Resources.LogCheckForNOW);
                    }
                }
            }

            if (stack.Count > 0)
            {
                //this would only happen if you wrote a open brace
                //without an closing one.
                var reStr =
                    Properties.Resources.LogInvalidQueryCloseBrace;
                Log.Error(reStr);
                return reStr;
            }
            else if (!query.StartsWith("{") && !query.EndsWith("}"))
            {
                var reStr =
                    Properties.Resources.LogStartEndBraceMissing;
                Log.Error(reStr);
                return reStr;
            }

            return query;
        }

        private string ReplaceNowKeyword(
            string query, int nowIndex,
            string dateType, out int newParsingIndex)
        {
            //get the length of all non-numeric characters
            int textLength = "NOW".Length
                             + "(".Length + dateType.Length + ")".Length;

            //get the index where the number parameter begins
            int beginNumberIndex = nowIndex + "NOW".Length
                                   + dateType.Length + "(".Length;

            //get the number parameter from the string
            string secondsString = "";
            while (query.Substring(beginNumberIndex
                                   + secondsString.Length, 1) != ")")
            {
                secondsString += query.Substring(beginNumberIndex
                                                 + secondsString.Length, 1);
            }

            //Input checking on the number within parentheses
            int secondsOff;
            if (string.IsNullOrEmpty(secondsString))
            {
                secondsOff = 0;
            }
            else
            {
                try
                {
                    secondsOff = Int32.Parse(secondsString);
                }
                catch (Exception)
                {
                    Log.Error(Properties.Resources.LogParseNumberFailure);
                    throw;
                }
            }

            //create the appropriate replacement date
            string timeToInsert = "";
            if (dateType == "POSIX")
            {
                timeToInsert = CreatePosixDateReplacement(secondsOff);
            }
            else if (dateType == "ISODATE")
            {
                timeToInsert = CreateIsoDateReplacement(secondsOff);
            }

            //make the replacement
            query = query.Remove(nowIndex, secondsString.Length + textLength);
            query = query.Insert(nowIndex, timeToInsert);

            //also return a new index for the method to continue parsing at
            newParsingIndex = nowIndex + timeToInsert.Length - 1;

            return query;
        }

        private string CreatePosixDateReplacement(int secondsOff)
        {
            //get UNIX time and perform any needed adjustments
            var unixTime =
                (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0))
                    .TotalSeconds;

            //determine if we need to add or subtract time
            bool negative = secondsOff < 0;
            if (negative)
            {
                secondsOff *= -1;
            }

            //make the calculation
            double timeToInsert;
            if (negative)
            {
                timeToInsert = unixTime - secondsOff;
            }
            else
            {
                timeToInsert = unixTime + secondsOff;
            }

            return timeToInsert.ToString();
        }

        private string CreateIsoDateReplacement(int secondsOff)
        {
            var dt = DateTime.Now;

            //determine if we need to add or subtract time
            bool negative = secondsOff < 0;
            if (negative)
            {
                secondsOff *= -1;
            }

            TimeSpan offset = new TimeSpan(0, 0, secondsOff);

            //make the calculation
            DateTime timeToInsert;
            if (negative)
            {
                timeToInsert = dt.Subtract(offset);
            }
            else
            {
                timeToInsert = dt.Add(offset);
            }

            return timeToInsert.ToJson();
        }

        #endregion

        #region Table Building Methods

        public StandaloneTable BuildTable(
            int requestedRowCount,
            IEnumerable<ParameterValue> parameters)
        {
            StandaloneTable table = new StandaloneTable();
            try
            {
                table.BeginUpdate();

                // Add columns to the table
                AddColumns(table);

                // Get an iterator for the BsonDocuments in the collection
                var cursor = GetMongoCursor(requestedRowCount, parameters);

                if (cursor != null)
                {
                    // Fill the rows
                    PopulateRows(table, cursor);
                }

                return table;
            }
            finally
            {
                table.EndUpdate();
            }
        }

        private void AddColumns(StandaloneTable table)
        {
            _settings.UsingHierarchy = false;
            var hierStructureCol = FindHierarchyColumnIfExists();
            var hierLabelCol = FindHierarchyLabelColumnIfExists();

            if (hierStructureCol != null && hierLabelCol != null)
            {
                //save in settings for later use
                _settings.UsingHierarchy = true;
                _settings.HierarchyStructureColumnName = hierStructureCol.Name;
                _settings.HierarchyLabelColumnName = hierLabelCol.Name;

                this.AddHierarchyColumns(table, hierStructureCol,
                    hierLabelCol);

                // Add the user defined non-hierarchy columns
                var nonHierarchyColumns = new List<MongoDBColumnDefinition>();
                foreach (
                    MongoDBColumnDefinition colDef in
                        _settings.ParserSettings.Columns)
                {
                    if (!colDef.Equals(hierStructureCol) &&
                        !colDef.Equals(hierLabelCol) &&
                        colDef.Structure != ColumnStructure.ColumnArray)
                    {
                        nonHierarchyColumns.Add(colDef);
                    }
                }
                DataPluginUtils.AddColumns(table, nonHierarchyColumns);

                //add Column Array Columns if exist
                AddArrayColumnsIfNeeded(table);
            }
                // no hierarchy, do normal
            else
            {
                // Add the user defined non-column Array columns
                var nonColumnArrays = new List<MongoDBColumnDefinition>();
                foreach (
                    MongoDBColumnDefinition colDef in
                        _settings.ParserSettings.Columns)
                {
                    if (colDef.Structure != ColumnStructure.ColumnArray)
                    {
                        nonColumnArrays.Add(colDef);
                    }
                }
                DataPluginUtils.AddColumns(table, nonColumnArrays);

                //add Column Array Columns if exist
                AddArrayColumnsIfNeeded(table);
            }
        }

        public void GenerateColumns()
        {
            try
            {
                // If these are null or empty we will definitely fail
                if (String.IsNullOrWhiteSpace(_settings.Url) ||
                    String.IsNullOrWhiteSpace(_settings.Database) ||
                    String.IsNullOrWhiteSpace(_settings.Collection))
                    return;

                // Get the top 10 BsonDocuments from the collection and attempt to
                // build column definitions out of the data found
                Dictionary<string, ColumnDefinition> generatedColumns =
                    new Dictionary<string, ColumnDefinition>();

                var collectionName =
                    _settings.ParameterizeValue(_settings.Collection);

                IMongoCollection<BsonDocument> collection = null;

                if (_database.ListCollections().ToList().Select(col => col.GetValue("name").AsString).Contains(collectionName))
                {
                    collection = _database.GetCollection<BsonDocument>(collectionName);
                }

                //if (_database.CollectionExists(collectionName))
                //{
                //    collection =
                //        _database.GetCollection<BsonDocument>(collectionName);
                //}

                if (collection != null)
                {
                    _settings.ParserSettings.ClearColumnDefinitions();
                    //var iter = collection.FindAll().SetLimit(10);
                    var iter = collection.Find(new BsonDocument()).Limit(10).ToCursor();
                    while (iter.MoveNext())
                    {
                        IEnumerable<BsonDocument> batch = iter.Current;
                        foreach (BsonDocument doc in batch)
                        {
                            DiscoverBsonDocumentSchema(doc, string.Empty,
                                generatedColumns);
                        }

                        foreach (var col in generatedColumns)
                        {
                            _settings.ParserSettings.AddColumnDefinition(col.Value);
                        }
                    }
                   
                }
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.UiFailedGenerationMessage);
                this.errorReporter.Report(
                    Properties.Resources.UiErrorLabel,
                    Properties.Resources.UiFailedGenerationMessage);
            }
        }

        private void AddArrayColumnsIfNeeded(StandaloneTable table)
        {
            var arrDict = FindColumnwiseRowExpansions();
            var expandedColList = new List<MongoDBColumnDefinition>();

            foreach (var kvp in arrDict)
            {
                PropertyBag colBag = kvp.Key.PropertyBag;
                for (int i = 0; i < kvp.Value; i++)
                {
                    var newPropBag = new PropertyBag(colBag);
                    var newDef = new MongoDBColumnDefinition(newPropBag);
                    newDef.Name = kvp.Key.Name + "[" + i + "]";
                    newDef.JsonPath = kvp.Key.JsonPath + "[" + i + "]";
                    expandedColList.Add(newDef);
                }
            }

            DataPluginUtils.AddColumns(table, expandedColList);
        }

        private void DiscoverBsonDocumentSchema(
            BsonDocument doc, string root,
            Dictionary<string, ColumnDefinition> columnsToAdd)
        {
            bool rootIsEmpty = String.IsNullOrWhiteSpace(root);
            foreach (BsonElement element in doc)
            {
                if (!columnsToAdd.ContainsKey(element.Name))
                {
                    MongoDBColumnDefinition col =
                        (MongoDBColumnDefinition) _settings.ParserSettings.
                            CreateColumnDefinition(new PropertyBag());
                    col.Name = element.Name;
                    switch (element.Value.BsonType)
                    {
                        case BsonType.Document:
                            if (rootIsEmpty)
                                DiscoverBsonDocumentSchema(
                                    (BsonDocument) element.Value,
                                    element.Name, columnsToAdd);
                            else
                                DiscoverBsonDocumentSchema(
                                    (BsonDocument) element.Value,
                                    root + "." + element.Name, columnsToAdd);
                            continue;
                        case BsonType.Boolean:
                        case BsonType.String:
                            col.Type = ColumnType.Text;
                            break;
                        case BsonType.DateTime:
                        case BsonType.Timestamp:
                            col.Type = ColumnType.Time;
                            break;
                        case BsonType.Double:
                        case BsonType.Int32:
                        case BsonType.Int64:
                            col.Type = ColumnType.Numeric;
                            break;
                        case BsonType.Array:
                            DiscoverBsonArrayType((BsonArray) element.Value, col);
                            break;
                        default:
                            col.Type = ColumnType.Text;
                            break;
                    }

                    if (rootIsEmpty)
                    {
                        col.JsonPath = col.Name;
                    }
                    else
                    {
                        col.JsonPath = root + "." + col.Name;
                    }

                    columnsToAdd.Add(col.Name, col);
                }
                else
                {
                    MongoDBColumnDefinition col = (MongoDBColumnDefinition) columnsToAdd[element.Name];
                    if(element.Value.BsonType == BsonType.String)
                    {
                        col.Type = ColumnType.Text;
                    }
                }
            }
        }

        private bool ShouldAddAutoArrayAxisColumn()
        {
            foreach (
                MongoDBColumnDefinition col in _settings.ParserSettings.Columns)
            {
                if (col.Structure == ColumnStructure.RowArray)
                    return true;
            }
            return false;
        }

        private MongoDBColumnDefinition FindHierarchyColumnIfExists()
        {
            foreach (MongoDBColumnDefinition col in
                _settings.ParserSettings.Columns)
            {
                if (col.Structure == ColumnStructure.HierarchyStructure)
                {
                    return col;
                }
            }

            return null;
        }

        private MongoDBColumnDefinition FindHierarchyLabelColumnIfExists()
        {
            var labelColumns = new List<MongoDBColumnDefinition>();
            foreach (MongoDBColumnDefinition col in
                _settings.ParserSettings.Columns)
            {
                if (col.Structure == ColumnStructure.HierarchyLabel)
                {
                    labelColumns.Add(col);
                }
            }

            if (labelColumns.Count == 1)
            {
                return labelColumns[0];
            }
            else
            {
                return null;
            }
        }

        private Dictionary<MongoDBColumnDefinition, int>
            FindColumnwiseRowExpansions()
        {
            var retDictionary = new Dictionary<MongoDBColumnDefinition, int>();
            foreach (MongoDBColumnDefinition col in
                _settings.ParserSettings.Columns)
            {
                if (col.Structure == ColumnStructure.ColumnArray)
                {
                    retDictionary.Add(col, col.MaxArrayColumns);
                }
            }

            return retDictionary;
        }

        private object[] AddAndPopulateTimeColumn(
            StandaloneTable table,
            object[] row, int xIndex, SchemaManager schemaManager)
        {
            List<object> newRow = row.ToList();

            // optionally add the array auto x-axis column
            if (ShouldAddAutoArrayAxisColumn())
            {
                if (!table.ContainsColumn(Properties.Resources.UiAutoXAxisLabel))
                {
                    var timeCol = new NumericColumn(
                        Properties.Resources.UiAutoXAxisLabel);
                    table.AddColumn(timeCol);
                    newRow.Add(xIndex);
                    schemaManager.AddColumn(timeCol);
                }
                else if (xIndex == 0)
                {
                    newRow.Add(xIndex);
                }
                else
                {
                    newRow[schemaManager.GetColumnIndex(
                        Properties.Resources.UiAutoXAxisLabel)]
                        = xIndex;
                }
            }

            return newRow.ToArray();
        }

        #endregion

        #region Table Population Methods

        private void PopulateRows(StandaloneTable table, IAsyncCursor<BsonDocument> cursor)
        {
            // Create a parser to evaluate the user defined columns
            MongoParser parser =
                (MongoParser) _settings.ParserSettings.CreateParser();

            //original row template - DO NOT CHANGE!!
            object[] row = new object[table.ColumnCount];

            while (cursor.MoveNext())
            {
                IEnumerable<BsonDocument> batch = cursor.Current;
                foreach (BsonDocument doc in batch)
                {
                    //use schema manager to keep track on column movement, if any
                    var colList = new List<Column>();
                    for (int i = 0; i < table.ColumnCount; i++)
                    {
                        colList.Add(table.GetColumn(i));
                    }
                    SchemaManager schemaManager = new SchemaManager(colList);
                    ArrayManager arrayManager = new ArrayManager();

                    // Use the jsonpath values of the defined columns to parse out 
                    // values for a single row
                    Dictionary<string, object> columnDictionary =
                        parser.Parse(doc, _settings.AutomaticTimeColumn, arrayManager);

                    ValueManager valueManager = new ValueManager(_settings,
                        columnDictionary);

                    //if a hierarchy exists we have to render it in the table/rows 
                    //before expanding the data arrays into rows or columns.
                    if (_settings.UsingHierarchy)
                    {
                        //add the top level node
                        AddHierarchyRows(table, doc, row.Length, schemaManager);
                    }
                    //normal parsing/expansion - no hierarchy
                    else
                    {
                        //Add the data arrays, if any others exist.
                        if (arrayManager.HasArrays)
                        {
                            AddDataArrays(table, row, arrayManager, schemaManager,
                                valueManager);
                        }
                        //if no other arrays are found, add to table.
                        else
                        {
                            FillNonArrayFields(row, schemaManager, valueManager);

                            table.AddRow(row);
                        }
                    }
                }
            }           

            //foreach (BsonDocument doc in cursor)
            //{
            //    //use schema manager to keep track on column movement, if any
            //    var colList = new List<Column>();
            //    for (int i = 0; i < table.ColumnCount; i++)
            //    {
            //        colList.Add(table.GetColumn(i));
            //    }
            //    SchemaManager schemaManager = new SchemaManager(colList);
            //    ArrayManager arrayManager = new ArrayManager();

            //    // Use the jsonpath values of the defined columns to parse out 
            //    // values for a single row
            //    Dictionary<string, object> columnDictionary =
            //        parser.Parse(doc, _settings.AutomaticTimeColumn, arrayManager);

            //    ValueManager valueManager = new ValueManager(_settings,
            //        columnDictionary);

            //    //if a hierarchy exists we have to render it in the table/rows 
            //    //before expanding the data arrays into rows or columns.
            //    if (_settings.UsingHierarchy)
            //    {
            //        //add the top level node
            //        AddHierarchyRows(table, doc, row.Length, schemaManager);
            //    }
            //        //normal parsing/expansion - no hierarchy
            //    else
            //    {
            //        //Add the data arrays, if any others exist.
            //        if (arrayManager.HasArrays)
            //        {
            //            AddDataArrays(table, row, arrayManager, schemaManager,
            //                valueManager);
            //        }
            //            //if no other arrays are found, add to table.
            //        else
            //        {
            //            FillNonArrayFields(row, schemaManager, valueManager);

            //            table.AddRow(row);
            //        }
            //    }
            //}
        }

        private void AddDataArrays(
            StandaloneTable table, object[] row,
            ArrayManager arrayManager, SchemaManager schemaManager,
            ValueManager valueManager)
        {
            ArrayManager rowManager;
            ArrayManager colManager;

            arrayManager.DivideIntoColumnAndRowManagers(out rowManager,
                out colManager);

            //remove all content from the old ArrayManager
            arrayManager.Queues.Clear();
            arrayManager.QueueExpansionTypes.Clear();

            object[] expandedColumnRow =
                AddArrayColumnWise(table, row, colManager, rowManager,
                    schemaManager, valueManager);

            AddArrayRowWise(table, expandedColumnRow,
                rowManager, schemaManager, valueManager);
        }

        #endregion

        #region Connection Methods

        public ObservableCollection<string> GetDatabaseNames()
        {
            ObservableCollection<string> dblist = new ObservableCollection<string>();
            try
            {
                //return
                //    new ObservableCollection<string>(_server.GetDatabaseNames());
                
                var dbs = _client.ListDatabases().ToList();
                dbs.Sort();
                foreach (var db in dbs)
                {
                    dblist.Add(db.GetElement("name").Value.ToString());
                }

                return dblist;
            }
            catch (MongoConnectionException e)
            {
                throw MongoExceptions.UnableToConnectToServer(e,
                    _settings.ParameterizeValue(_settings.Url));
            }
        }

        public ObservableCollection<string> GetCollectionNames()
        {
            ObservableCollection<string> collection = new ObservableCollection<string>();
            try
            {
                if (!SetupDatabase())
                    return new ObservableCollection<string>();

                //return
                //    new ObservableCollection<string>(_database.GetCollectionNames());
                var dbs = _database.ListCollections().ToList();
                dbs.Sort();
                foreach (var db in dbs)
                {
                    collection.Add(db.GetElement("name").Value.ToString());
                }

                return collection;
            }
            catch (MongoConnectionException e)
            {
                throw MongoExceptions.UnableToConnectToServer(e,
                    _settings.ParameterizeValue(_settings.Url));
            }
        }

        private void EstablishConnection()
        {
            try
            {
                string authenticatedString = CreateConnectionString();
                _client = new MongoClient(authenticatedString);
                //_server = _client.GetServer();

                if (!String.IsNullOrWhiteSpace(_settings.Database))
                {
                    var selectedDb = _settings.ParameterizeValue(_settings.Database);
                    if (_client.ListDatabases().ToList().Select(db => db.GetValue("name").AsString).Contains(selectedDb))
                    {
                        _database = _client.GetDatabase(selectedDb);
                    }


                    //if (_server.DatabaseExists(selectedDb))
                    //{
                    //    _database = _server.GetDatabase(selectedDb);
                    //}
                }
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.ExUnableToConnect);
                this.errorReporter.Report(
                    Properties.Resources.UiErrorLabel,
                    Properties.Resources.ExUnableToConnect, _settings.ParameterizeValue(_settings.Url));
            }
        }

        private string CreateConnectionString()
        {
            if (!String.IsNullOrWhiteSpace(_settings.User) &&
                !String.IsNullOrWhiteSpace(_settings.Password))
            {
                string user = _settings.ParameterizeValue(_settings.User);
                string password = _settings.ParameterizeValue(_settings.Password);
                string url = _settings.ParameterizeValue(_settings.Url);

                return String.Format("mongodb://{0}:{1}@{2}",
                    user, password, url);
            }

            return String.Format("mongodb://{0}",
                _settings.ParameterizeValue(_settings.Url));
        }

        private bool SetupDatabase()
        {
            if (!String.IsNullOrWhiteSpace(_settings.Database))
            {
                var selectedDb = _settings.ParameterizeValue(_settings.Database);
                
                if (_client.ListDatabases().ToList().Select(db => db.GetValue("name").AsString).Contains(selectedDb))
                {
                    _database = _client.GetDatabase(selectedDb);
                }

                //if (_server.DatabaseExists(selectedDb))
                //{
                //    _database = _server.GetDatabase(selectedDb);
                //}
            }

            return _database != null;
        }

        #endregion

        #region Data Retrieval Methods

        private IAsyncCursor<BsonDocument> GetMongoCursor(
            int requestedRowCount,
            IEnumerable<ParameterValue> parameters)
        {
            // Get the collection
            var collectionName =
                _settings.ParameterizeValue(_settings.Collection);

            if (collectionName == null)
            {
                return null;
            }

            IMongoCollection<BsonDocument> collection = null;

            if (_database.ListCollections().ToList().Select(col => col.GetValue("name").AsString).Contains(collectionName))
            {
                collection =
                    _database.GetCollection<BsonDocument>(collectionName);
            }

            //if (_database.CollectionExists(collectionName))
            //{
            //    collection =
            //        _database.GetCollection<BsonDocument>(collectionName);
            //}

            if (collection == null)
            {
                return null;
            }

            ParameterValue[] parameterValues = parameters == null
                ? new ParameterValue[0]
                : parameters.ToArray();

            if (!_settings.UseCustomFindQuery)
            {
                // No parameters
                if (!_settings.Parameterize || !parameterValues.Any())
                {
                    return GetCursor(collection, requestedRowCount);
                }

                // Parameterize
                List<BsonValue> queryValues = new List<BsonValue>();
                foreach (ParameterValue param in parameterValues)
                {
                    if (!param.Name.Equals(_settings.FilterParameter))
                        continue;

                    var paramValue = param.TypedValue as ArrayParameterValue;
                    if (paramValue != null)
                    {
                        foreach (TypedParameterValue val in paramValue.Values)
                        {
                            AddBsonValue(queryValues, val);
                        }
                    }
                    else
                    {
                        AddBsonValue(queryValues, param.TypedValue);
                    }
                }

                if (!String.IsNullOrWhiteSpace(_settings.FilterColumn) &&
                    queryValues.Any())
                {
                    //IMongoQuery query = Query.In(_settings.FilterColumn, queryValues);
                    var builder = Builders<BsonDocument>.Filter;
                    FilterDefinition<BsonDocument> filter = null;
                    filter = builder.Eq(_settings.FilterColumn, queryValues);

                    //return GetCursor(collection, requestedRowCount, query);
                    return GetCursor(collection, requestedRowCount, filter);
                }
                else
                {
                    return GetCursor(collection, requestedRowCount);
                }
            }
            else
            {
                string queryString = "";
                try
                {
                    queryString = ReplaceQueryParametersAndKeywords();

                    BsonDocument bDoc =
                        BsonDocument.Parse(queryString);

                    //var qDoc = new QueryDocument(bDoc);
                    // var qDoc = new BsonDocument(bDoc);
                    //return GetCursor(collection, requestedRowCount, qDoc);
                    return GetCursor(collection, requestedRowCount, bDoc);
                }
                catch (Exception ex)
                {
                    throw MongoExceptions.BsonParseFailure(ex, queryString);
                }
            }
        }

        private IAsyncCursor<BsonDocument> GetCursor(
            IMongoCollection<BsonDocument> collection,
            int requestedRowCount, FilterDefinition<BsonDocument> query = null)
        {
            // Not parameterized
            if (query == null)
            {
                if (requestedRowCount > 0)
                {
                    //return collection.FindAll().SetLimit(requestedRowCount);
                    return collection.Find(new BsonDocument()).Limit(requestedRowCount).ToCursor();
                }

                //return collection.FindAll();
                return collection.Find(new BsonDocument()).ToCursor();
            }
            else // Parameterized
            {
                if (requestedRowCount > 0)
                {
                    //return collection.Find(query).SetLimit(requestedRowCount);
                    return collection.Find(query).Limit(requestedRowCount).ToCursor();
                }

                //return collection.Find(query);
                return collection.Find(query).ToCursor();
            }
        }

        private void AddBsonValue(
            List<BsonValue> queryValues, TypedParameterValue valueToAdd)
        {
            string valueAsString = valueToAdd.ToString();

            // _id is an internal MongoDB document property that must be of type
            // BsonObjectId for queries to work properly
            if (_settings.FilterColumn == "_id")
            {
                try
                {
                    var objId = new ObjectId();
                    if (ObjectId.TryParse(valueAsString, out objId))
                    {
                        queryValues.Add(new BsonObjectId(objId));
                    }
                }
                catch (Exception e)
                {
                    throw MongoExceptions.InvalidId(e, valueAsString);
                }
            }
            else
            {
                MongoDBColumnDefinition col = (MongoDBColumnDefinition)_settings.
                    ParserSettings.Columns.
                    FirstOrDefault(c => c.Name == _settings.FilterColumn);

                // Try to add as a number
                if (valueToAdd is NumberParameterValue)
                {
                    queryValues.Add((valueToAdd as NumberParameterValue).Value);
                    return;
                }

                // Try to add as a time
                if (valueToAdd is DateTimeParameterValue)
                {
                    queryValues.Add((valueToAdd as DateTimeParameterValue).Value);
                    return;
                }

                double doubleValue;
                if (col != null &&
                    col.Type == ColumnType.Numeric &&
                    Double.TryParse(valueAsString, NumberStyles.Any, CultureInfo.InvariantCulture, out doubleValue))
                {
                    queryValues.Add(doubleValue);
                    return;
                }

                // Default to adding as a string
                queryValues.Add(valueAsString);
            }
        }

        #endregion
    }
}