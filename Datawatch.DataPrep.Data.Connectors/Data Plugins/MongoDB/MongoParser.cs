﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using MongoDB.Bson;
using Panopticon.MongoDBPlugin.Enums;
using Panopticon.MongoDBPlugin.Managers;

namespace Panopticon.MongoDBPlugin
{
    public class MongoParser : IParser
    {
        private readonly IList<string> columnNameList = new List<string>();
        private readonly IList<string> jsonPathList = new List<string>();
        private readonly IList<bool> isTimeColumn = new List<bool>();
        public readonly IList<bool> IsArrayColumn = new List<bool>();
        private readonly IList<bool> isRowArrayColumn = new List<bool>();
        private readonly IList<bool> isColumnArrayColumn = new List<bool>();
        private MongoParserSettings parserSettings;

        public MongoParser(MongoParserSettings settings)
        {
            parserSettings = settings;
            foreach (MongoDBColumnDefinition columnDefinition in settings.Columns
                )
            {
                if (!columnDefinition.IsEnabled) continue;

                jsonPathList.Add(columnDefinition.JsonPath);
                columnNameList.Add(columnDefinition.Name);
                isTimeColumn.Add(columnDefinition.Type == ColumnType.Time);
                IsArrayColumn.Add(columnDefinition.Structure ==
                                  ColumnStructure.RowArray ||
                                  columnDefinition.Structure ==
                                  ColumnStructure.ColumnArray ||
                                  columnDefinition.Structure ==
                                  ColumnStructure.HierarchyStructure);
                isRowArrayColumn.Add(columnDefinition.Structure ==
                                     ColumnStructure.RowArray);
                isColumnArrayColumn.Add(columnDefinition.Structure ==
                                        ColumnStructure.ColumnArray);
            }
        }

        public Dictionary<string, object> Parse(string message)
        {
            return null;
        }

        public Dictionary<string, object> Parse(
            BsonDocument document, string timeStampColumn,
            ArrayManager arrayManager)
        {
            Dictionary<string, object> dictionary =
                new Dictionary<string, object>();
            if (document == null) return dictionary;

            // Iterate through the columns from the MongoParserSettings
            for (int i = 0; i < jsonPathList.Count; i++)
            {
                if (jsonPathList[i] == null || columnNameList[i] == null)
                {
                    break;
                }

                object value = null;
                try
                {
                    // Attempt to pull out the BsonValue at each column's jsonpath
                    BsonValue val = IndexInto(jsonPathList[i], document);
                    if (val != null && !val.IsBsonNull)
                    {
                        if (val.BsonType == BsonType.Array)
                        {
                            // Store the whole array so we can expand it later
                            if (IsArrayColumn[i])
                            {
                                if (isRowArrayColumn[i])
                                {
                                    arrayManager.AddArray(columnNameList[i],
                                        (BsonArray) val,
                                        ArrayExpansionType.RowWise);
                                }
                                else if (isColumnArrayColumn[i])
                                {
                                    arrayManager.AddArray(columnNameList[i],
                                        (BsonArray) val,
                                        ArrayExpansionType.ColumnWise);
                                }
                                else
                                {
                                    arrayManager.AddArray(columnNameList[i],
                                        (BsonArray) val,
                                        ArrayExpansionType.Hierarchy);
                                }
                            }
                                // Otherwise use the first value in the array if the user
                                // set this to a value column instead of array
                            else if (((BsonArray) val).Count > 0)
                            {
                                value = val[0].ToString();
                            }
                        }
                        else if (isTimeColumn[i])
                        {
                            MongoDBColumnDefinition mongoCol =
                                (MongoDBColumnDefinition) parserSettings.Columns.
                                    First(col => col.Name == columnNameList[i]);

                            value = mongoCol.DateParser.Parse(val.ToString());
                        }
                        else
                        {
                            value = val.ToString();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                    throw new Exception(string.Format("Invalid JsonPath: {0}",
                        columnNameList[i]));
                }

                dictionary[columnNameList[i]] = value;
            }
            return dictionary;
        }

        private BsonValue IndexInto(string jsonpath, BsonDocument doc)
        {
            // If the jsonpath looks like, "address.street"
            // trim it to just, "address" and see if it exists in the BsonDocument
            string name = jsonpath.Split('.')[0];
            if (!doc.Contains(name))
                return null;

            BsonValue val = doc[name];
            if (val.IsBsonDocument)
            {
                // If the jsonpath looks like, "address.street"
                // trim it to just, "street" and call the function again
                string path = jsonpath.Remove(0, name.Count() + 1);
                return IndexInto(path, val.AsBsonDocument);
            }

            return val;
        }
    }

    public class DateParser
    {
        public const string DefaultDateFormat = "MM/dd/yyyy";
        public const string Posix = "POSIX";

        public static readonly DateTime UnixEpochStart = new DateTime(1970, 1, 1,
            0, 0, 0, DateTimeKind.Utc);

        private string dateFormat;
        private bool isUnixEpoch;
        private CultureInfo cultureInfo;
        private IFormatProvider formatProvider;

        public DateParser(string format)
        {
            DateFormat = format;
            cultureInfo = CultureInfo.InvariantCulture;
        }

        public CultureInfo CultureInfo
        {
            get { return cultureInfo; }
            set
            {
                if (cultureInfo != value)
                {
                    cultureInfo = value;
                }
            }
        }

        public IFormatProvider FormatProvider
        {
            get { return formatProvider; }
            set { formatProvider = value; }
        }

        public string DateFormat
        {
            set
            {
                isUnixEpoch = Posix == value;

                this.dateFormat = value;
            }

            get { return dateFormat; }
        }

        public DateTime Parse(string date)
        {
            DateTime newDate;
            // Parser versus string format
            if (string.IsNullOrEmpty(dateFormat))
            {
                // No format is set for the ColumnDefinition so do the standard Date parsing
                if (DateTime.TryParse(date, out newDate))
                {
                    return newDate;
                }
                return DateTime.MinValue;
            }
            // Format is set for the ColumnDefinition
            try
            {
                if (isUnixEpoch)
                {
                    double unixEpochDouble = Convert.ToDouble(date);
                    return UnixEpochStart.AddSeconds(unixEpochDouble);
                }
                newDate = DateTime.ParseExact(date, dateFormat, cultureInfo);
                return newDate;
            }
            catch
            {
            }
            return DateTime.MinValue;
        }

        public string ToString(DateTime date)
        {
            try
            {
                if (isUnixEpoch)
                {
                    return
                        Convert.ToString(
                            date.Subtract(UnixEpochStart).TotalSeconds);
                }
                if (formatProvider != null)
                {
                    return date.ToString(dateFormat, formatProvider);
                }
                return date.ToString(dateFormat);
            }
            catch
            {
            }
            return Properties.Resources.UiTimeValueEmptyText;
        }
    }
}