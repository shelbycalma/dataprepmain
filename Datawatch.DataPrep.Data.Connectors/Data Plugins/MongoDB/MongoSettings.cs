﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.MongoDBPlugin
{
    public class MongoSettings : TextSettingsBase
    {
        public MongoSettings(PropertyBag bag, IPluginManager pluginManager)
            : this(bag, pluginManager, null)
        {
        }

        public MongoSettings(
            PropertyBag bag, IPluginManager pluginManager,
            IEnumerable<ParameterValue> parameters) 
            : base(pluginManager, bag, parameters)
        {
        }

        public string User
        {
            get { return GetInternal("User"); }
            set { SetInternal("User", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string Url
        {
            get { return GetInternal("Url", "localhost"); }
            set { SetInternal("Url", value); }
        }

        public string Database
        {
            get { return GetInternal("Database"); }
            set { SetInternal("Database", value); }
        }

        public string Collection
        {
            get { return GetInternal("Collection"); }
            set { SetInternal("Collection", value); }
        }

        public int TimeIncrement // ms
        {
            get { return GetInternalInt("TimeIncrement", 1); }
            set { SetInternalInt("TimeIncrement", value); }
        }

        public string StartingTimeColumn
        {
            get { return GetInternal("StartingTimeColumn"); }
            set { SetInternal("StartingTimeColumn", value); }
        }

        public string AutomaticTimeColumn
        {
            get
            {
                return GetInternal("AutomaticTimeColumn", "Automatic Timestamp");
            }
            set { SetInternal("AutomaticTimeColumn", value); }
        }

        public bool IncludeTimeSeriesArray
        {
            get
            {
                string s = GetInternal("IncludeTimeSeriesArray", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IncludeTimeSeriesArray", Convert.ToString(value));
            }
        }

        public bool ExpandArraysByColumn
        {
            get
            {
                string s = GetInternal("ExpandArraysByColumn", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("ExpandArraysByColumn", Convert.ToString(value)); }
        }

        public bool UsingHierarchy
        {
            get
            {
                string s = GetInternal("UsingHierarchy", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("UsingHierarchy", Convert.ToString(value)); }
        }

        public string HierarchyStructureColumnName
        {
            get
            {
                return GetInternal("HierarchyStructureColumnName", String.Empty);
            }
            set { SetInternal("HierarchyStructureColumnName", value); }
        }

        public string HierarchyLabelColumnName
        {
            get { return GetInternal("HierarchyLabelColumnName", String.Empty); }
            set { SetInternal("HierarchyLabelColumnName", value); }
        }

        public int NumberOfHierarchyColumns
        {
            get { return GetInternalInt("NumberOfHierarchyColumns", -1); }
            set { SetInternalInt("NumberOfHierarchyColumns", value); }
        }

        public bool ExpandArraysByRow
        {
            get
            {
                string s = GetInternal("ExpandArraysByRow", true.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("ExpandArraysByRow", Convert.ToString(value)); }
        }

        public bool Parameterize
        {
            get
            {
                string s = GetInternal("Parameterize", false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("Parameterize", Convert.ToString(value));
                if (!value) // wipe out FilterColumn and FilterParameter
                {
                    FilterColumn = null;
                    FilterParameter = null;
                }
            }
        }

        public string FilterColumn
        {
            get { return GetInternal("FilterColumn"); }
            set { SetInternal("FilterColumn", value); }
        }

        public string FilterParameter
        {
            get { return GetInternal("FilterParameter"); }
            set { SetInternal("FilterParameter", value); }
        }

        public override string Title
        {
            get { return GetInternal("Title", GetDefaultTitle()); }
            set { SetInternal("Title", value); }
        }

        private string GetDefaultTitle()
        {
            string selectedCol = ParameterizeValue(Collection);
            return Properties.Resources.UiDefaultTitle + " - " + selectedCol;
        }

        public override PathType FilePathType { get; set; }

        public override ParserSettings ParserSettings
        {
            get
            {
                if (parserSettings != null)
                    return parserSettings;
                parserSettings = new MongoParserSettings(parserSettingsBag);
                return parserSettings;
            }
        }

        public string CustomFindQuery
        {
            get { return GetInternal("CustomFindQuery", string.Empty); }
            set { SetInternal("CustomFindQuery", value); }
        }

        public bool UseCustomFindQuery
        {
            get
            {
                return Convert.ToBoolean(GetInternal("UseCustomFindQuery",
                    "false"));
            }
            set { SetInternal("UseCustomFindQuery", value.ToString()); }
        }

        public bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        public override bool CanGenerateColumns()
        {
            return !String.IsNullOrWhiteSpace(Url) &&
                   !String.IsNullOrWhiteSpace(Database) &&
                   !String.IsNullOrWhiteSpace(Collection);
        }

        public override void DoGenerateColumns()
        {
            MongoHelper helper = new MongoHelper(this, this.errorReporter);
            if (helper.Database != null)
            {
                helper.GenerateColumns();
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return "Json"; }
        }

        public string ParameterizeValue(
            string value, string arraySeparator = "\n")
        {
            var parameters = base.Parameters;

            if (parameters == null || String.IsNullOrEmpty(value))
                return value;
            else
            {
                var encoder = new ParameterEncoder
                {
                    Parameters = parameters,
                    DefaultArraySeparator = arraySeparator,
                    SourceString = value
                };
                return encoder.Encoded();
            }
        }
    }
}