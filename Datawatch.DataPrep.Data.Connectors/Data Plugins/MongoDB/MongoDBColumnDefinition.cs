﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Panopticon.MongoDBPlugin.Enums;

namespace Panopticon.MongoDBPlugin
{
    public class MongoDBColumnDefinition : ColumnDefinition
    {
        public MongoDBColumnDefinition(PropertyBag bag)
            : base(bag)
        {
            dateParser = new DateParser(DateFormat);
        }

        private DateParser dateParser;

        public DateParser DateParser
        {
            get { return dateParser; }
        }

        public string DateFormat
        {
            get { return GetInternal("DateFormat"); }
            set
            {
                SetInternal("DateFormat", value);
                dateParser.DateFormat = value;
            }
        }

        public int MaxArrayColumns
        {
            get { return GetInternalInt("MaxArrayColumns", 0); }
            set { SetInternalInt("MaxArrayColumns", value); }
        }

        public string JsonPath
        {
            get { return GetInternal("JsonPath"); }
            set { SetInternal("JsonPath", value); }
        }

        public bool IsColArray
        {
            get { return Structure == ColumnStructure.ColumnArray; }
        }

        public ColumnStructure Structure
        {
            get { return GetInternalEnum("Structure", ColumnStructure.Value); }
            set
            {
                SetInternalEnum("Structure", value);
                OnPropertyChanged("IsColArray");
            }
        }

        public ColumnStructure[] Structures
        {
            get
            {
                return
                    (ColumnStructure[]) Enum.GetValues(typeof (ColumnStructure));
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!base.Equals(obj)) return false;
            if (!(obj is MongoDBColumnDefinition)) return false;

            MongoDBColumnDefinition xpathColumnDefinition =
                (MongoDBColumnDefinition) obj;

            return string.Equals(JsonPath, xpathColumnDefinition.JsonPath);
        }

        public override string ToString()
        {
            return Name;
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (!string.IsNullOrEmpty(JsonPath))
            {
                code += code*23 + JsonPath.GetHashCode();
            }
            return code;
        }

        // Check so column name and path have been entered.
        public override bool IsValid
        {
            get
            {
                return !string.IsNullOrEmpty(Name)
                       && !string.IsNullOrEmpty(JsonPath);
            }
        }
    }
}