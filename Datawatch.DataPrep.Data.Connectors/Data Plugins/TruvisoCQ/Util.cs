﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.TruvisoCQPlugin.Properties;
using Exceptions = Datawatch.DataPrep.Data.Core.Exceptions;

namespace Panopticon.TruvisoCQPlugin
{
    public class Util
    {
        internal static string cursorName = "DatawatchTruCQCursor";
        private static string declareCursorStatement = "declare {0} cursor for {1};";
        private static string fetchCursorStatement = "fetch 0 from {0};";

        public static string ApplyParameters(string sourceString,
            IEnumerable<ParameterValue> parameters)
        {
            try
            {
                if (parameters == null) return sourceString;
                return new ParameterEncoder
                {
                    SourceString = sourceString,
                    Parameters = parameters,
                }.Encoded();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
            return sourceString;
        }

        /// <summary>
        /// Returns the best SDK column type "fit" for an ADO column type.
        /// </summary>
        private static Column CreateColumn(string name, Type type)
        {
            if (type == typeof(Byte) || type == typeof(SByte) ||
                type == typeof(Single) ||
                type == typeof(Double) ||
                type == typeof(Int16) || type == typeof(UInt16) ||
                type == typeof(Int32) || type == typeof(UInt32) ||
                type == typeof(Int64) || type == typeof(UInt64) ||
                type == typeof(Decimal))
            {
                return new NumericColumn(name);
            }
            if (type == typeof(string) ||
                type == typeof(char) ||
                type == typeof(Boolean) ||
                type == typeof(Guid))
            {
                return new TextColumn(name);
            }
            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan))
            {
                return new TimeColumn(name);
            }

            throw Exceptions.BadColumnType(type);
        }

        private static ParameterTable CreateTable(
            List<TruvisoColumn> columns, IEnumerable<ParameterValue> parameters)
        {
            ParameterTable table = new ParameterTable(parameters);
            table.BeginUpdate();
            try {
                foreach(TruvisoColumn column in columns)
                {
                    string name = column.Name;
                    Type type = column.Type;
                    table.AddColumn(CreateColumn(name, type));
                }
            } finally {
                table.EndUpdate();
            }
            return table;
        }

        internal static string GetCommandText(string query)
        {
            //Joining both declare and fetch statements to avoid roundtrip.
            return String.Format(declareCursorStatement, cursorName, query) +
                String.Format(fetchCursorStatement, cursorName);
        }

        public static List<TruvisoColumn> GetColumns(string DSNName, string query)
        {
            string commandText = GetCommandText(query);

            Log.Info(Properties.Resources.LogQueryInfo, commandText);

            List<TruvisoColumn> columns = new List<TruvisoColumn>();
            DbTransaction transaction = null;
            OdbcConnection connection = null;
            try
            {
                DateTime start = DateTime.Now;
                connection = GetConnection(DSNName);
                transaction = connection.BeginTransaction();

                using (DbCommand statement = connection.CreateCommand())
                {
                    statement.Transaction = transaction;
                    statement.CommandText = commandText;
                        
                    using (DbDataReader reader = statement.ExecuteReader())
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            string name = reader.GetName(i);
                            Type type = reader.GetFieldType(i);
                            columns.Add(new TruvisoColumn(name, type));
                        }
                    }
                }

                transaction.Commit();
                                
            }
            catch (Exception ex)
            {
                if (transaction != null)
                {
                    transaction.Rollback();
                }
                throw ex;
            }
            finally
            {
                if (connection != null  && 
                    connection.State != ConnectionState.Broken &&
                    connection.State != ConnectionState.Closed)
                {                    
                    connection.Close();
                }
            }

            return columns;
        }

        public static OdbcConnection GetConnection(string DSNName)
        {
            try
            {
                OdbcConnection connection = new OdbcConnection("DSN=" + DSNName);
                connection.Open();
                return connection;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                throw ex;
            }
        }

        public static ColumnType GetColumnType(Type type)
        {
            if (type == typeof(Byte) || type == typeof(SByte) ||
                type == typeof(Single) ||
                type == typeof(Double) ||
                type == typeof(Int16) || type == typeof(UInt16) ||
                type == typeof(Int32) || type == typeof(UInt32) ||
                type == typeof(Int64) || type == typeof(UInt64) ||
                type == typeof(Decimal))
            {
                return ColumnType.Numeric;
            }
            if (type == typeof(DateTime) ||
                type == typeof(TimeSpan))
            {            
                return ColumnType.Time;
            }
            else
            {
                return ColumnType.Text;
            }
        }

        public static ITable GetTable(string DSNName, string query,
            IEnumerable<ParameterValue> parameters)
        {
            DateTime start = DateTime.Now;
            List<TruvisoColumn> columns = GetColumns(DSNName, query);
            ParameterTable table = CreateTable(columns, parameters);
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);
            return table;
        }
    }
}
