﻿using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.TruvisoCQPlugin
{
    public class TruvisoCQSettings : RealtimeConnectionSettings
    {
        public TruvisoCQSettings()
            : this(new PropertyBag())
        {
        }

        public TruvisoCQSettings(PropertyBag bag)
            : base(bag, true)
        {            
        }

        public string DSN
        {
            get { return GetInternal("DSN"); }
            set { SetInternal("DSN", value); }
        }
        
        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public override string Title
        {
            get
            {
                return DSN;
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is TruvisoCQSettings)) return false;

            if (!base.Equals(obj)) return false;

            TruvisoCQSettings cs = (TruvisoCQSettings)obj;

            if (this.DSN != cs.DSN ||
                 this.Query != cs.Query)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = 0;

            object[] fields =
                {
                    DSN, Query
                };

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }
        
    }
}
