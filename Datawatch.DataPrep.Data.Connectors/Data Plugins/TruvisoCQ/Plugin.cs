﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.TruvisoCQPlugin
{
    /// <summary>
    /// DataPlugin for Truviso CQ
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin<ParameterTable, TruvisoCQSettings,
        TruvisoRecord, TruvisoCQDataAdapter>
    {
        // The uri of the image displayed in the "Connect to data" dialog as
        // well as the data plug-in drop down.
        

        private readonly ImageSource connectImage;
        private bool disposed;

        // The id of the data plug-in, this string must not be changed after
        // the plug-in has been used in workbooks or the workbooks created 
        // using the plug-in will loose their data connections.
        internal const string PluginId = "TruvisoCQPlugin";
        internal const bool PluginIsObsolete = false;

        // The title displayed in the "Connect to data" dialog and data 
        // plug-in dropdown.
        internal const string PluginTitle = "Truviso CQ";
        internal const string PluginType = DataPluginTypes.Streaming;
                
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }
        }

        public override TruvisoCQSettings CreateSettings(PropertyBag bag)
        {
            return new TruvisoCQSettings(bag);
        }

        protected override ITable CreateTable(TruvisoCQSettings settings,
             IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            // Check that the plug-in has a valid license.
            CheckLicense();

            string query = Util.ApplyParameters(settings.Query, parameters);
            return Util.GetTable(settings.DSN, query, parameters);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        /// <summary>
        /// Gets the plug-in id.
        /// </summary>
        public override string Id
        {
            get { return PluginId; }
        }

        /// <summary>
        /// Gets a value indicating whether the plug-in has a valid license.
        /// </summary>
        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        /// <summary>
        /// Gets a value indicating the title of the plug-in.
        /// </summary>
        public override string Title
        {
            get { return PluginTitle; }
        }
    }
}
