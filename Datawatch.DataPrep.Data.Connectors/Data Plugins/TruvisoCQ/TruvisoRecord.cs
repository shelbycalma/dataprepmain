﻿using System;
using System.Collections.Generic;

namespace Panopticon.TruvisoCQPlugin
{
    public class TruvisoRecord
    {
        private List<TruvisoColumn> columns;
        private List<object> values;
        private Dictionary<string, int> columnNameToIndexMap;
        
        public TruvisoRecord(List<TruvisoColumn> columns, List<object> values,
            Dictionary<string, int> columnNameToIndexMap)
        {           
            this.columns = columns;
            this.values = values;
            this.columnNameToIndexMap = columnNameToIndexMap;
        }
        
        public object GetValue(string columnName)
        {
            if (!string.IsNullOrEmpty(columnName) && 
                columnNameToIndexMap.ContainsKey(columnName))
            {
                return values[columnNameToIndexMap[columnName]];
            }
            else
            {
                string msg = String.Format(
                    Properties.Resources.ExInvalidColumnName, columnName);
                throw new ArgumentException(msg);
            }
        }

        public IList<TruvisoColumn> Columns
        {
            get { return columns.AsReadOnly(); }
        }

        public List<object> Values
        {
            get { return values; }
        }

    }
}
