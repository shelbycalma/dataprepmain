﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.TruvisoCQPlugin
{
    public class TruvisoColumn
    {
        private string name;
        private Type type;
        private ColumnType columnType;

        public TruvisoColumn(string columnName, Type type)
        {            
            this.name = columnName;
            this.type = type;
            this.columnType = Util.GetColumnType(type);
        }

        public string Name
        {
            get { return name; }
        }

        public Type Type
        {
            get { return type; }
        }

        public ColumnType ColumnType
        {
            get { return columnType; }
        }
        
    }
}
