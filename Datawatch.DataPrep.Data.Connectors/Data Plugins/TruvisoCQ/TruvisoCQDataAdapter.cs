﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using System.Threading;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.TruvisoCQPlugin
{
    public class TruvisoCQDataAdapter :
        RealtimeDataAdapter<ParameterTable, TruvisoCQSettings, TruvisoRecord>
    {
        private Thread thread;

        DbTransaction transaction = null;
        OdbcConnection connection = null;
        DbCommand command = null;

        List<TruvisoColumn> columns = new List<TruvisoColumn>();
        Dictionary<string, int> columnNameToIndexMap = new Dictionary<string, int>();
        bool loadSchema = true;

        private static string declareCursorStatement = "declare {0} window cursor for {1};";
        private static string fetchCursorStatement = "fetch window from {0};";
        
        public override void Start()
        {
            try
            {
                string query = Util.ApplyParameters(settings.Query,
                    table.Parameters);

                connection = Util.GetConnection(settings.DSN);
                transaction = connection.BeginTransaction();

                command = connection.CreateCommand();
                command.CommandText = string.Format(declareCursorStatement,
                    Util.cursorName, query);
                command.Transaction = transaction;
                command.ExecuteNonQuery();

                thread = new Thread(new ThreadStart(ReadSubscription));
                thread.Name = "Truviso subscription reader";
                thread.Start();

            }
            catch (Exception ex)
            {
                this.ErrorReporter.Report("Start failure", ex.Message);
                Log.Exception(ex);
            }
        }

        private void ReadSubscription()
        {            
            try
            {
                command.CommandText = String.Format(fetchCursorStatement,
                    Util.cursorName);                

                command.Prepare();
                            
                while (thread != null)
                {
                    using (DbDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            List<object> values = new List<object>();
                            if (thread == null)
                            {
                                break;
                            }
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                if (loadSchema)
                                {
                                    string columnName = reader.GetName(i);

                                    TruvisoColumn column = new TruvisoColumn(
                                        columnName, reader.GetFieldType(i));
                                    columns.Add(column);
                                    
                                    columnNameToIndexMap[columnName] = i;
                                }

                                values.Add(reader[i]);
                            }
                            loadSchema = false;

                            TruvisoRecord record = new TruvisoRecord(columns,
                                values, columnNameToIndexMap);
                            
                            Enqueue(record);
                        }
                    } 
                }
            }
            catch (Exception e)
            {
                Log.Exception(e);
                thread = null;
                if (transaction != null)
                {
                    transaction.Rollback();
                    transaction = null;
                }
            }
            finally
            {
                if (command != null)
                {
                    command.Dispose();
                }
                if (transaction != null)
                {
                    transaction.Commit();
                }
                if (connection != null &&
                    connection.State != ConnectionState.Broken &&
                    connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }                
            }
        }

        private void Enqueue(TruvisoRecord tuple)
        {
            string id = tuple.GetValue(settings.IdColumn).ToString();

            updater.EnqueueUpdate(id, tuple);
        }

        public override void Stop()
        {
            thread = null;                        
        }

        public override void UpdateColumns(Row row, TruvisoRecord evt)
        {
            object newValue;
            foreach (TruvisoColumn column in evt.Columns)
            {
                if (column.Name == settings.IdColumn) continue;

                ColumnType type = column.ColumnType;
                switch (type)
                {
                    case ColumnType.Text:
                        newValue = evt.GetValue(column.Name);
                        ProcessTextValue(row, newValue, column.Name);
                        break;
                    case ColumnType.Numeric:
                        newValue = evt.GetValue(column.Name);
                        ProcessNumericValue(row, newValue, column.Name);
                        break;
                    case ColumnType.Time:
                        newValue = evt.GetValue(column.Name);
                        ProcessDateValue(row, newValue, column.Name);
                        break;
                }
            }
        }

        private void ProcessTextValue(Row row, object newValue, string colName)
        {
            TextColumn textCol = (TextColumn) table.GetColumn(colName);
            string oldTextValue = textCol.GetTextValue(row);
            string newTextValue = newValue != null ?
                newValue.ToString() : null;

            if (newTextValue != oldTextValue)
            {
                textCol.SetTextValue(row, newTextValue);
            }
        }

        private void ProcessNumericValue(Row row, object newValue, string colName)
        {
            NumericColumn numericCol = (NumericColumn) table.GetColumn(colName);
            double oldNumericValue = numericCol.GetNumericValue(row);
            double newNumericValue = newValue != null ?
                Convert.ToDouble(newValue) : NumericValue.Empty;

            if (newNumericValue != oldNumericValue)
            {
                numericCol.SetNumericValue(row, newNumericValue);
            }
        }

        private void ProcessDateValue(Row row, object newValue, string colName)
        {
            TimeColumn timeColumn = (TimeColumn) table.GetColumn(colName);
            DateTime oldTimeValue = timeColumn.GetTimeValue(row);
            DateTime newTimeValue = newValue != null ?
                Convert.ToDateTime(newValue) : TimeValue.Empty;

            if (newTimeValue != oldTimeValue)
            {
                timeColumn.SetTimeValue(row, newTimeValue);
            }
        }

        public override DateTime GetTimestamp(TruvisoRecord row)
        {
            if (settings.IsTimeIdColumnGenerated ||
                settings.TimeIdColumn == null ||
                settings.TimeIdColumn.Length == 0)
            {
                return TimeValue.Empty;
            }

            object value = row.GetValue(settings.IdColumn);
            return value != null ?
                Convert.ToDateTime(value) : TimeValue.Empty;
            
        }
        
    }
}
