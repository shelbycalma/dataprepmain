﻿using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Python;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.PythonPlugin
{
    public class PythonPluginSettings : ConnectionSettings
    {

        public PythonPluginSettings() :
            this(new PropertyBag())
        {}

        public PythonPluginSettings(PropertyBag properties)
            : base(properties)
        {}

        public string Host
        {
            get { return GetInternal("host", "localhost"); }
            set { SetInternal("host", value); }
        }

        public string Port
        {
            get { return GetInternal("port", "9090"); }
            set { SetInternal("port", value); }
        }

        public string Password
        {
            get { return GetInternal("password"); }
            set { SetInternal("password", value); }
        }

        public string ParameterName
        {
            get { return GetInternal("parameterName", "table"); }
            set { SetInternal("parameterName", value); }
        }

        public string Script
        {
            get { return GetInternal("script"); }
            set { SetInternal("script", value); }
        }

        public bool EncloseParametersInQuotes
        {
            get
            {
                string value = GetInternal("encloseParametersInQuotes", false.ToString());
                return bool.Parse(value);
            }
            set { SetInternal("encloseParametersInQuotes", value.ToString()); }
        }

        public int Timeout
        {
            get { return GetInternalInt("timeout", 10); }
            set { SetInternalInt("timeout", value); }
        }

        public PythonSettings ToPythonSettings()
        {
            return new PythonSettings
                {
                    Host = Host,
                    Port = Port,
                    Password = Password,
                    EncloseParametersInQuotes = EncloseParametersInQuotes,
                    Script = Script,
                    Timeout = Timeout,
                    ParameterName = ParameterName
                };
        }
    }
}
