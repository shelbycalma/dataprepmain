﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Python;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.PythonPlugin.Properties;

namespace Panopticon.PythonPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<PythonPluginSettings>
    {
        internal const string PluginId = "PythonPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Python";
        internal const string PluginType = DataPluginTypes.Database;

        public override PythonPluginSettings CreateSettings(PropertyBag bag)
        {
            PythonPluginSettings pythonPluginSettings = new PythonPluginSettings(bag);
            if (string.IsNullOrEmpty(pythonPluginSettings.Title))
            {
                pythonPluginSettings.Title = Resources.UiDefaultConnectionTitle;
            }

            return pythonPluginSettings;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            CheckLicense();
            PythonPluginSettings pythonPluginSettings = CreateSettings(bag);
            PythonClient pythonClient =
                new PythonClient(pythonPluginSettings.ToPythonSettings(), parameters);

            StandaloneTable table = pythonClient.ExcuteScript(rowcount);
            
            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }
    }
}
