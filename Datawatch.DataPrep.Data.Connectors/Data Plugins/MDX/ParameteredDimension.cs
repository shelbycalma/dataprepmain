﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.MDXPlugin
{
    public class ParameteredDimension : ParameteredMetadataBase<Dimension>
    {
        public ParameteredDimension()
        {
            ParameteredHierarchyInfos = new List<ParameteredHierarchy>();
        }

        public List<ParameteredHierarchy> ParameteredHierarchyInfos { get; set; }
        
    }

    public class ParameteredHierarchy : ParameteredMetadataBase<Hierarchy>
    {
        public ParameteredHierarchy()
        {
            ParameteredLevelInfos = new List<ParameteredLevel>();
        }

        public List<ParameteredLevel> ParameteredLevelInfos { get; set; }
    }

    public class ParameteredLevel : ParameteredMetadataBase<Level> {}

    public class ParameteredMeasure : ParameteredMetadataBase<Measure> { }

    
    public class ParameteredMetadataBase<T> : IParameterizedMetadata, INotifyPropertyChanged where T : MetadataElementBase
    {
        public MetadataElementBase Element { get; set; }

        private bool Equals(IParameterizedMetadata other)
        {
            return Element.Equals(other.Element);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!(obj is IParameterizedMetadata)) return false;
            return Equals((IParameterizedMetadata)obj);
        }

        public override int GetHashCode()
        {
            return this.Element != null ? this.Element.GetHashCode() : 0;
        }

        
        private ParameterValue _parameter;
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Name
        {
            get { return Element.Name; }
            set
            {
                Element.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string UniqueName
        {
            get { return Element.UniqueName; }
            set
            {
                Element.UniqueName = value;
                OnPropertyChanged("Name");
            }
        }

        public ParameterValue Parameter
        {
            get { return _parameter; }
            set
            {
                _parameter = value;
                OnPropertyChanged("Parameter");
            }
        }
    }

    public interface IParameterizedMetadata
    {
        MetadataElementBase Element { get; set; }
        ParameterValue Parameter { get; set; }
        string Name { get; set; }
        string UniqueName { get; set; }
    }
}