﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.MDXPlugin.Properties;
using Exceptions = Datawatch.DataPrep.Data.Framework.Exceptions;

namespace Panopticon.MDXPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<MDXSettings>
    {
        internal const string PluginId = "MDXPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "MDX";
        internal const string PluginType = DataPluginTypes.Database;

        public Plugin()
        {
            // prevent plugin load if we dont have AdomdClient library
            try
            {
                Log.Info(Resources.LogLibraryLoaded,
                    typeof (AdomdCommand).Assembly.FullName);
            }
            catch (Exception ex)
            {
                throw Datawatch.DataPrep.Data.Core.Connectors.Exceptions.PluginDependenciesAreNotFound(PluginId, ex);
            }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override MDXSettings CreateSettings(PropertyBag properties)
        {
            MDXSettings mdxSettings = new MDXSettings(properties);
            return mdxSettings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag properties, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            MDXSettings mdxSettings = CreateSettings(properties);

            MDXClientUtil mdxClientUtil = new MDXClientUtil(mdxSettings);

            return mdxClientUtil.Execute(mdxSettings, parameters, maxRowCount);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
    }
}