﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.MDXPlugin.MetadataProviders.AdomdMetadataProvider;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;
using Level = Microsoft.AnalysisServices.AdomdClient.Level;
using Tuple = Microsoft.AnalysisServices.AdomdClient.Tuple;

namespace Panopticon.MDXPlugin
{
    public class MDXClientUtil
    {
        private Dictionary<string, string> encodedParametersMap;
        private readonly IMetadataProvider metadataProvider;

        public MDXClientUtil(MDXSettings mdxSettings)
        {
            metadataProvider = new AdomdMetadataProvider(mdxSettings);
            metadataProvider.SetCatalogName(mdxSettings.Catalog);
        }

        private string ApplyParameters(string source,
            IEnumerable<ParameterValue> parameters)
        {
            if (string.IsNullOrEmpty(source)) return null;
            if (parameters == null) return source;

            //var mdxParameteres = parameters.Select(p => new ParameterValue(p.Name, new StringParameterValue()
            //    {
            //        Value = string.Format("@{0}", p.Name)
            //    }));

            //return new ParameterEncoder
            //{
            //    SourceString = source,
            //    Parameters = mdxParameteres,
            //    NullOrEmptyString = "NULL"
            //}.Encoded();

            List<ParameterValue> paramsList = parameters.ToList();
            if (paramsList.Any())
            {
                encodedParametersMap = new Dictionary<string, string>();
            }

            for (int i = 0; i < paramsList.Count; i++)
            {
                ParameterValue parameter = paramsList[i];
                string newValue = string.Format("@p{0}", i);
                string paramName = string.Format("@{0}", parameter.Name);
                source = source.Replace(paramName, newValue);
                encodedParametersMap.Add(parameter.Name, "p" + i);
            }

            return source;
        }

        public ITable Execute(MDXSettings settings,
            IEnumerable<ParameterValue> parameters, int maxRows)
        {
            string query = ApplyParameters(settings.Query, parameters);
            StandaloneTable table;
            DateTime start = DateTime.Now;

            AdomdCommand cmd = new AdomdCommand(query, (AdomdConnection) metadataProvider.Connection);

            if (parameters != null)
            {
                foreach (ParameterValue parameterValue in parameters)
                {
                    cmd.Parameters.Add(new AdomdParameter(encodedParametersMap[parameterValue.Name],
                        GetParameterValue(parameterValue.TypedValue)));
                }
            }

            Log.Info(Properties.Resources.LogMDXExecutingQuery,
                cmd.CommandText);

            CellSet cellSet = cmd.ExecuteCellSet();
            table = CreateTable(cellSet, settings, maxRows);
            
            // We're creating a new StandaloneTable on every request, so allow
            // EX to freeze it.
            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }
            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Properties.Resources.LogMDXQueryCompleted,
                table.RowCount, table.ColumnCount, seconds);
            return table;
        }

        private StandaloneTable CreateTable(CellSet cellSet, MDXSettings settings, int maxRows)
        {
            if (cellSet.Axes.Count == 0)
            {
                throw new Exception(Properties.Resources.ExNoData);
            }

            //current implementation only supports two exes
            if (cellSet.Axes.Count != 2)
            {
                throw new Exception(Properties.Resources.ExInvalidAxes);
            }

            if (!(cellSet.Axes[0].Positions.Count > 0) &&
                !(cellSet.Axes[1].Positions.Count > 0))
            {
                throw new Exception(Properties.Resources.ExNoData);
            }

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            // Go through the row axis.
            // Create text columns for each level of each hierarchy.
            HierarchyCollection rowHierarchies = cellSet.Axes[1].Set.Hierarchies;
            int[] hierarchyStartIndex = new int[rowHierarchies.Count];
            for (int i = 0; i < rowHierarchies.Count; i++)
            {
                Hierarchy hierarchy = rowHierarchies[i];
                hierarchyStartIndex[i] = table.ColumnCount;
                foreach (Level level in hierarchy.Levels)
                {
                    string columnName = hierarchy.Caption + " " + level.Caption;
                    TextColumn column = new TextColumn(columnName);
                    table.AddColumn(column);
                }
            }
            string[] textValues = new string[table.ColumnCount];
            string[] raggedTextValues = new string[table.ColumnCount];

            // Go through the column axis.
            // Create one numeric column per tuple.
            TupleCollection columnTuples = cellSet.Axes[0].Set.Tuples;
            foreach (Tuple tuple in columnTuples)
            {
                StringBuilder columnName = new StringBuilder();
                foreach (Member member in tuple.Members)
                {
                    if (columnName.Length > 0)
                    {
                        columnName.Append(" ");
                    }
                    columnName.Append(member.Caption);
                }
                NumericColumn column = new NumericColumn(columnName.ToString());
                table.AddColumn(column);
            }

            // Pull out the data
            TupleCollection rowTuples = cellSet.Axes[1].Set.Tuples;
            for (int rowTupleIndex = 0; rowTupleIndex < rowTuples.Count; rowTupleIndex++)
            {
                if (maxRows > -1 && table.RowCount >= maxRows)
                {
                    break;
                }

                Tuple rowTuple = rowTuples[rowTupleIndex];
                IsRaggedToAdd isRaggedToAdd = IsRaggedToAdd.Undefined;
                bool[] raggedHierarchies = new bool[rowTuple.Members.Count];
                if (settings.FillRaggedHierarchies)
                {
                    for (int i = 0; i < rowTuple.Members.Count; i++)
                    {
                        Member member = rowTuple.Members[i];
                        if (member.GetChildren(0, 1).Count == 0)
                        {
                            if (member.LevelDepth + 1 != member.ParentLevel.ParentHierarchy.Levels.Count)
                            {
                                isRaggedToAdd = IsRaggedToAdd.Yes;
                                raggedHierarchies[i] = true;
                            }
                        }
                        else if (isRaggedToAdd == IsRaggedToAdd.Undefined)
                        {
                            isRaggedToAdd = IsRaggedToAdd.No;
                        }
                    }
                }

                bool isAggregateRow = false;
                for (int i = 0; i < rowTuple.Members.Count; i++)
                {
                    Member member = rowTuple.Members[i];
                    if (settings.FillRaggedHierarchies)
                    {
                        isAggregateRow = isAggregateRow || member.ChildCount > 0;
                        if (!raggedHierarchies[i] && settings.SkipAggregatedRows && isAggregateRow)
                        {
                            isRaggedToAdd = IsRaggedToAdd.No;
                        }
                    }
                    else
                    {
                        isAggregateRow = isAggregateRow || (member.ChildCount > 0 && !raggedHierarchies[i]);
                    }
                    int levelDepth = member.LevelDepth;
                    int textValueIndex = hierarchyStartIndex[i] + levelDepth;
                    string value = member.Caption;
                    textValues[textValueIndex] = value;
                    raggedTextValues[textValueIndex] = value;


                    // blank out row axis values to the right of this level
                    if (!IsParentSameAsPrevious(rowTuples, i, rowTupleIndex) || isRaggedToAdd == IsRaggedToAdd.Yes)
                    {
                        int lastIndex = member.ParentLevel.ParentHierarchy.Levels.Count
                                        - member.LevelDepth
                                        + textValueIndex;
                        for (int j = textValueIndex + 1; j < lastIndex; j++)
                        {
                            if (!IsParentSameAsPrevious(rowTuples, i, rowTupleIndex))
                            {
                                textValues[j] = null;
                            }
                            if (isRaggedToAdd == IsRaggedToAdd.Yes)
                            {
                                switch (settings.FillRaggedHierarchyType)
                                {
                                    case FillRaggedHierarchyTypes.Left:
                                        raggedTextValues[j] = "-";
                                        break;
                                    case FillRaggedHierarchyTypes.Right:
                                        raggedTextValues[j] = raggedTextValues[j - 1];
                                        break;
                                }
                            }
                        }
                    }
                }

                if (!settings.SkipAggregatedRows || !isAggregateRow)
                {
                    SetRow(table, textValues, columnTuples, cellSet, rowTupleIndex);
                }
                if (isRaggedToAdd == IsRaggedToAdd.Yes)
                {
                    switch (settings.FillRaggedHierarchyType)
                    {
                        case FillRaggedHierarchyTypes.Right:
                            SetRow(table, raggedTextValues, columnTuples, cellSet, rowTupleIndex);
                            break;
                        case FillRaggedHierarchyTypes.Left:
                            table.AddRow(raggedTextValues);
                            break;
                    }
                }
            }

            table.EndUpdate();

            return table;
        }

        private bool IsParentSameAsPrevious(TupleCollection rowTuples, int memberIndex, int rowTupleIndex)
        {
            Tuple currentRowTuple = rowTuples[rowTupleIndex];
            Member currentMember = currentRowTuple.Members[memberIndex];

            bool result = currentMember.ParentSameAsPrevious;
            /*bool result = false;
            if (rowTupleIndex != 0)
            {
                Tuple previousRowTuple = rowTuples[rowTupleIndex - 1];
                Member previousMember = previousRowTuple.Members[memberIndex];
                if (currentMember.Parent == null)
                {
                    if (previousMember.Parent == null)
                    {
                        result = currentMember.UniqueName.Equals(previousMember.UniqueName);
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    if (previousMember.Parent != null)
                        result = currentMember.Parent.UniqueName.Equals(previousMember.Parent.UniqueName);
                }
            }*/
            //if (result != check)
            //{
            //    throw new ApplicationException("Members check code is wrong");
            //}
            return result;
        }

        private void SetRow(StandaloneTable table, string[] textValues,
            TupleCollection columnTuples, CellSet cellSet, int rowTupleIndex)
        {
            Row row = table.AddRow(textValues);

            for (int colTupleIndex = 0; colTupleIndex < columnTuples.Count; colTupleIndex++)
            {
                int columnIndex = textValues.Length + colTupleIndex;
                NumericColumn column = (NumericColumn) table.GetColumn(columnIndex);
                Cell cell = cellSet.Cells[colTupleIndex, rowTupleIndex];
                object value = cell.Value;

                double numericValue;
                if (value == null)
                {
                    numericValue = NumericValue.Empty;
                }
                else
                {
                    if (value is double)
                    {
                        numericValue = (double) value;
                    }
                    else
                    {
                        numericValue = Convert.ToDouble(value);
                    }
                }

                column.SetNumericValue(row, numericValue);
            }
        }

        public string[] GetCatalogsList()
        {
            return metadataProvider.GetAllCatalogs().Select(c => c.Name).ToArray();
        }

        public string[] GetCubesList(string catalogName)
        {
            metadataProvider.SetCatalogName(catalogName);
            return metadataProvider.GetAllCubes().ToArray();
        }

        public Cube GetCubeMetadata(string catalogName, string cubeName)
        {
            metadataProvider.SetCatalogName(catalogName);
            return metadataProvider.GetCubeMetaData(cubeName);
        }

        private object GetParameterValue(TypedParameterValue typedParameterValue)
        {
            if (typedParameterValue is StringParameterValue)
            {
                return ((StringParameterValue) typedParameterValue).Value;
            }
            if (typedParameterValue is DateTimeParameterValue)
            {
                return ((DateTimeParameterValue) typedParameterValue).Value;
            }
            if (typedParameterValue is NumberParameterValue)
            {
                return ((NumberParameterValue) typedParameterValue).Value;
            }

            throw new ArgumentException(string.Format(Properties.Resources.ExInvalidParameterType,
                typedParameterValue.GetType()));
        }

        private enum IsRaggedToAdd
        {
            Undefined,
            Yes,
            No
        }
    }
}