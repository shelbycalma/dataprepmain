﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.MetadataProviders;
using Microsoft.AnalysisServices.AdomdClient;
using CubeType = Microsoft.AnalysisServices.AdomdClient.CubeType;
using Dimension = Microsoft.AnalysisServices.AdomdClient.Dimension;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;
using Level = Microsoft.AnalysisServices.AdomdClient.Level;
using Measure = Microsoft.AnalysisServices.AdomdClient.Measure;

namespace Panopticon.MDXPlugin.MetadataProviders.AdomdMetadataProvider
{
    public class AdomdMetadataProvider : IMetadataProvider
    {
        private AdomdConnection connection;
        public IDbConnection Connection
        {
            get { return connection; }
            private set { connection = (AdomdConnection)value; }
        }

        public AdomdConnection AdomdConnection
        {
            get { return connection; }
            private set { connection = value; }
        }

        public AdomdMetadataProvider(MDXSettings mdxSettings)
        {
            string connectionString = BuildConnectionString(mdxSettings);
            Connection = AdomdConnectionHelper
                .GetConnection(connectionString);
        }

        public Cube GetCubeMetaData(string cubeName)
        {
            Cube cubeMetadataInfo = null;
            var cube = FindCubeByName(cubeName);
            if (cube != null)
            {
                cubeMetadataInfo = AdomdInfoHelper.CreateCubeMetadataInfo(cube);

                foreach (Dimension dimension in cube.Dimensions)
                {
                    if (dimension.DimensionType == DimensionTypeEnum.Measure) continue;

                    var dimensionMetadataInfo = AdomdInfoHelper.CreateDimensionMetadataInfo(dimension);
                    cubeMetadataInfo.Dimensions.Add(dimensionMetadataInfo);

                    foreach (Hierarchy hierarchy in dimension.Hierarchies)
                    {
                        Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Hierarchy hierarchyMetadata = AdomdInfoHelper.CreateHierarchyMetadataInfo(hierarchy);
                        dimensionMetadataInfo.HierarchyInfos.Add(hierarchyMetadata);

                        foreach (Level level in hierarchy.Levels)
                        {
                            Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata.Level levelMetadata = AdomdInfoHelper.CreateLevelMetadataInfo(level);
                            hierarchyMetadata.LevelInfos.Add(levelMetadata);
                        }
                    }
                }

                cubeMetadataInfo.Measures = cubeMetadataInfo.Measures.Concat(cube.Measures
                                              .Cast<Measure>()
                                              .Select(AdomdInfoHelper.CreateMeasureMetadataInfo))
                                              .ToList();
            }
            return cubeMetadataInfo;
        }

        public IList<MeasureGroup> GetMeasureGroups(string cubeName)
        {
            var list = new Dictionary<String, MeasureGroup>();
            var restrictions = new AdomdRestrictionCollection {{"CATALOG_NAME", Connection.Database}};
            if (!String.IsNullOrEmpty(cubeName))
            {
                restrictions.Add("CUBE_NAME", AdomdInfoHelper.ConvertToNormalStyle(cubeName));
            }

            DataSet ds = AdomdConnection.GetSchemaDataSet("MDSCHEMA_MEASUREGROUPS", restrictions);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable table = ds.Tables[0];

                if (ds.Tables[0].Columns.Count > 0)
                {
                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                    {
                        var measureGroupMetadataInfo = new MeasureGroup();

                        if (table.Columns.Contains("CATALOG_NAME"))
                        {
                            if (dataRow["CATALOG_NAME"] != null)
                            {
                                measureGroupMetadataInfo.CatalogName = dataRow["CATALOG_NAME"].ToString();
                            }
                        }

                        if (table.Columns.Contains("CUBE_NAME"))
                        {
                            if (dataRow["CUBE_NAME"] != null)
                            {
                                measureGroupMetadataInfo.CubeName = dataRow["CUBE_NAME"].ToString();
                            }
                        }

                        if (table.Columns.Contains("MEASUREGROUP_NAME"))
                        {
                            if (dataRow["MEASUREGROUP_NAME"] != null)
                            {
                                measureGroupMetadataInfo.Name = dataRow["MEASUREGROUP_NAME"].ToString();
                            }
                        }

                        if (table.Columns.Contains("DESCRIPTION"))
                        {
                            if (dataRow["DESCRIPTION"] != null)
                            {
                                measureGroupMetadataInfo.Description = dataRow["DESCRIPTION"].ToString();
                            }
                        }

                        if (table.Columns.Contains("MEASUREGROUP_CAPTION"))
                        {
                            if (dataRow["MEASUREGROUP_CAPTION"] != null)
                            {
                                measureGroupMetadataInfo.Caption = dataRow["MEASUREGROUP_CAPTION"].ToString();
                            }
                        }

                        if (!list.ContainsKey(measureGroupMetadataInfo.Name))
                        {
                            list.Add(measureGroupMetadataInfo.Name, measureGroupMetadataInfo);
                        }
                    }
                }
            }

            ds = AdomdConnection.GetSchemaDataSet("MDSCHEMA_MEASURES", restrictions);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataTable table = ds.Tables[0];

                if (ds.Tables[0].Columns.Count > 0)
                {
                    foreach (DataRow dataRow in ds.Tables[0].Rows)
                    {
                        String measuresGroupName = string.Empty;
                        if (table.Columns.Contains("MEASUREGROUP_NAME"))
                        {
                            if (dataRow["MEASUREGROUP_NAME"] != null)
                            {
                                measuresGroupName = dataRow["MEASUREGROUP_NAME"].ToString();
                            }
                        }

                        String measureUniqueName = String.Empty;
                        if (table.Columns.Contains("MEASURE_UNIQUE_NAME"))
                        {
                            if (dataRow["MEASURE_UNIQUE_NAME"] != null)
                            {
                                measureUniqueName = dataRow["MEASURE_UNIQUE_NAME"].ToString();
                            }
                        }

                        if (!String.IsNullOrEmpty(measuresGroupName) &&
                            !String.IsNullOrEmpty(measureUniqueName))
                        {
                            if (list.ContainsKey(measuresGroupName))
                            {
                                if (!list[measuresGroupName].Measures.Contains(measureUniqueName))
                                    list[measuresGroupName].Measures.Add(measureUniqueName);
                            }
                        }
                    }
                }
            }

            foreach (MeasureGroup measureGroupMetadataInfo in list.Values)
            {
                var restrictionCollection = new AdomdRestrictionCollection
                    {
                        {"CATALOG_NAME", Connection.Database},
                        {"CUBE_NAME", AdomdInfoHelper.ConvertToNormalStyle(cubeName)},
                        {"MEASUREGROUP_NAME", measureGroupMetadataInfo.Name}
                    };
                ds = AdomdConnection.GetSchemaDataSet("MDSCHEMA_MEASUREGROUP_DIMENSIONS", restrictionCollection);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable table = ds.Tables[0];

                    if (ds.Tables[0].Columns.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            if (table.Columns.Contains("DIMENSION_UNIQUE_NAME"))
                            {
                                if (row["DIMENSION_UNIQUE_NAME"] != null)
                                {
                                    String dimensionUniqueName = row["DIMENSION_UNIQUE_NAME"].ToString();
                                    if (!String.IsNullOrEmpty(dimensionUniqueName))
                                    {
                                        if (!measureGroupMetadataInfo.Dimensions.Contains(dimensionUniqueName))
                                            measureGroupMetadataInfo.Dimensions.Add(dimensionUniqueName);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return list.Values.ToList();
        }

        public CubeDef FindCubeByName(string cubeName)
        {
            if (string.IsNullOrEmpty(cubeName))
            {
                return null;
            }
            return
                AdomdConnection.Cubes.Cast<CubeDef>()
                           .FirstOrDefault(cubeDef => cubeDef.Name.ToLower() == cubeName.ToLower());
        }

        public IList<string> GetAllCubes()
        {
            return
                AdomdConnection.Cubes.Cast<CubeDef>()
                    .Where(cubeDef => cubeDef.Type == CubeType.Cube)
                    .Select(cubeDef => cubeDef.Name)
                    .ToList();
        }

        public IList<Catalog> GetAllCatalogs()
        {
            var result = new DataTable();

            using (var adapter =
                new AdomdDataAdapter(
                    "SELECT [CATALOG_NAME] FROM $system.dbschema_catalogs",
                    AdomdConnection))
            {
                adapter.Fill(result);
            }

            var databases = new List<Catalog>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                DataRow dataRow = result.Rows[i];
                databases.Add(new Catalog
                    {
                        Name = dataRow["CATALOG_NAME"].ToString()
                    });
            }
            return databases;
        }

        public void SetCatalogName(string catalogName)
        {
            if (!string.IsNullOrEmpty(catalogName)
                   && Connection.Database != catalogName)
            {
                Connection.ChangeDatabase(catalogName);
            }
        }

        private string BuildConnectionString(MDXSettings settings)
        {
            if (settings == null || string.IsNullOrEmpty(settings.ServerName))
            {
                return string.Empty;
            }

            StringBuilder connectionString = new StringBuilder(@"Provider=MSOLAP");

            if (settings.AuthType == AuthenticationType.Windows || settings.ServerName.EndsWith("msmdpump.dll"))
            {
                connectionString.Append("; Data Source=" + settings.ServerName);
            }
            else
            {
                string protocol = settings.UseSSL ? "https://" : "http://";
                connectionString.Append("; Data Source=" + protocol + settings.ServerName + "/olap/msmdpump.dll");
            }

//            if (!string.IsNullOrEmpty(settings.Catalog))
//            {
//                connectionString.Append("; Catalog=" + settings.Catalog);
//            }

            if (settings.AuthType != AuthenticationType.Windows)
            {
                if (string.IsNullOrEmpty(settings.Password) || string.IsNullOrEmpty(settings.UserName))
                {
                    throw new ApplicationException("Username and password are requied");
                }
                if (!string.IsNullOrEmpty(settings.UserName))
                {
                    connectionString.Append(";User ID=" + settings.UserName);
                }
                if (!string.IsNullOrEmpty(settings.Password))
                {
                    connectionString.Append(";Password=" + settings.Password);
                }
            }

            return connectionString.ToString();
        }

        public void Dispose()
        {
            Connection.Close();
        }
    }
}