﻿using Microsoft.AnalysisServices.AdomdClient;

namespace Panopticon.MDXPlugin.MetadataProviders.AdomdMetadataProvider
{
    public class AdomdConnectionHelper
    {
        public static AdomdConnection GetConnection(string connectionString)
        {
            var adomdConnection = new AdomdConnection(connectionString);
            adomdConnection.Open();
            return adomdConnection;
        }
    }
}