﻿using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.MDXPlugin
{
    public class LoginInfo : ViewModelBase
    {
        private AuthenticationType authType;
        private string password;
        private string serverName;
        private bool useSSL;
        private string userName;

        public LoginInfo(MDXSettings mdxSettings)
        {
            this.ServerName = mdxSettings.ServerName;
            this.UseSSL = mdxSettings.UseSSL;
            this.UserName = mdxSettings.UserName;
            this.Password = mdxSettings.Password;
            this.AuthType = mdxSettings.AuthType;
        }

        public string ServerName
        {
            get
            {
                return string.IsNullOrEmpty(serverName)
                    ? "localhost"
                    : serverName;
            }
            set
            {
                serverName = value;
                OnPropertyChanged("ServerName");
            }
        }

        public bool UseSSL
        {
            get { return useSSL; }
            set
            {
                useSSL = value;
                OnPropertyChanged("UseSSL");
            }
        }

        public string UserName
        {
            get { return userName ?? string.Empty; }
            set
            {
                userName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get { return password ?? string.Empty; }
            set
            {
                password = value;
                OnPropertyChanged("Password");
            }
        }

        public AuthenticationType AuthType
        {
            get { return authType; }
            set
            {
                authType = value;
                OnPropertyChanged("AuthType");
            }
        }

        public void SaveToMdxSettings(MDXSettings mdxSettings)
        {
            mdxSettings.ServerName = this.ServerName;
            mdxSettings.UseSSL = this.UseSSL;
            mdxSettings.UserName = this.UserName;
            mdxSettings.Password = this.Password;
            mdxSettings.AuthType = this.AuthType;
        }
    }
}