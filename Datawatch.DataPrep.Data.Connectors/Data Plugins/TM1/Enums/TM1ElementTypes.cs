﻿namespace Panopticon.TM1Plugin
{
    public enum TM1ElementTypes
    {
        Simple = 21,
        Consolidated = 22,
        String = 23
    }
}