﻿namespace Panopticon.TM1Plugin
{
    public enum ObjectType
    {
        Unknown,
        Cubeview,
        DimensionSubset,
        MDX,
        SubsetElement
    }
}