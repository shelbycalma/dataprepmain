﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.TM1Plugin
{
    public class DbTitle : PropertyBagViewModel
    {
        private string appliedParameterName;
        private string titleName;

        public DbTitle(PropertyBag bag)
            : base(bag)
        {
        }

        public string TitleName
        {
            get { return titleName; }
            set
            {
                if (titleName == value)
                    return;
                titleName = value;
                appliedParameterName = SubgroupGetInternal(TitleName);
                OnPropertyChanged("TitleName");
            }
        }

        public string AppliedParameterName
        {
            get { return appliedParameterName; }
            set
            {
                if (appliedParameterName == value)
                    return;
                appliedParameterName = value;
                SubgroupSetInternal(TitleName, AppliedParameterName);
                OnPropertyChanged("AppliedParameterName");
            }
        }

        private string SubgroupGetInternal(string name)
        {
            PropertyBag subGroup = propertyBag.SubGroups[this.GetType().Name];
            return subGroup != null ? subGroup.Values[name] : null;
        }

        private void SubgroupSetInternal(string name, string value)
        {
            PropertyBag subGroup = GetInternalSubGroup(this.GetType().Name);
            if (subGroup == null)
            {
                PropertyBag subGroupPropertyBag = new PropertyBag();
                subGroupPropertyBag.Values[name] = value;
                SetInternalSubGroup(this.GetType().Name, subGroupPropertyBag);
            }
            else
            {
                subGroup.Values[name] = value;
            }
        }
    }
}