﻿using System;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.TM1Plugin.Properties;

namespace Panopticon.TM1Plugin
{
    public class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        public static Exception UnableToLoadTable(string message)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExUnableToLoadData, message));
        }

        public static Exception UnableToConnectToServer(Exception exception)
        {
            Log.Exception(exception);
            return CreateInvalidOperation(
                string.Format(Resources.ExUnableToConnectToServer, string.Empty));
        }

        public static Exception UnableToConnectToServer(string message)
        {
            return CreateInvalidOperation(
                string.Format(Resources.ExUnableToConnectToServer, message));
        }

        public static Exception RowsOrColumnsAreMissing()
        {
            return CreateInvalidOperation(Resources.ExRowsOrColumnsAreMissing);
        }

        public static Exception NotSupportedSourceType()
        {
            return CreateInvalidOperation(Resources.ExNotSupportedSourceType);
        }

        public static Exception CubeOrViewIsMissing(Exception exception)
        {
            Log.Exception(exception);
            return CreateInvalidOperation(Resources.ExCubeOrViewIsMissing);
        }

        public static Exception DimensionOrSubsetIsMissing()
        {
            return CreateInvalidOperation(Resources.ExDimensionOrSubsetIsMissing);
        }

        public static Exception ErrorExecutingMdxQuery(Exception exception)
        {
            Log.Exception(exception);
            return CreateInvalidOperation(Resources.ExErrorExecutingMdxQuery);
        }
    }
}