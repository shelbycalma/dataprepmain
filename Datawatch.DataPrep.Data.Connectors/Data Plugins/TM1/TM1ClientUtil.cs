﻿using System;
using System.Collections.Generic;
using Applix.TM1.API.Internal;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.TM1Plugin.Properties;

namespace Panopticon.TM1Plugin
{
    internal class TM1ClientUtil
    {
        private readonly _TM1Main adminServer;
        private readonly IEnumerable<ParameterValue> parameters;
        private readonly PropertyBag propertyBag;
        private readonly int rowcount;
        private readonly TM1Settings tm1Settings;
        private _TM1Server tm1Server;

        public TM1ClientUtil(PropertyBag bag,
            IEnumerable<ParameterValue> parameters, int rowcount)
        {
            this.tm1Settings = new TM1Settings(bag);
            adminServer = TM1Helper
                .GetMainServer(tm1Settings.AdminHost,
                    tm1Settings.SSLCertificateId);
            this.parameters = parameters;
            this.rowcount = rowcount;
            this.propertyBag = bag;
        }

        public StandaloneTable GetTable()
        {
            OpenConnection();
            if (tm1Server == null)
            {
                return null;
            }

            StandaloneTable result = null;
            try
            {
                switch (tm1Settings.ObjectType)
                {
                    case ObjectType.Cubeview:
                        result = GetTableCubeview();
                        break;
                    case ObjectType.DimensionSubset:
                        result = GetTableDimensionSubset();
                        break;
                    case ObjectType.MDX:
                        result = GetTableMDX();
                        break;
                    case ObjectType.SubsetElement:
                        result = GetTableSubset();
                        break;
                    default:
                        throw Exceptions.NotSupportedSourceType();
                }
            }
            finally
            {
                CloseConnection();
            }

            if (result == null)
            {
                throw Exceptions.UnableToLoadTable(string.Empty);
            }

            return result;
        }

        private void OpenConnection()
        {
            try
            {
                tm1Server = adminServer
                    .openConnection(tm1Settings.Server,
                        tm1Settings.User, tm1Settings.Password);
            }
            catch (Exception ex)
            {
                throw Exceptions.UnableToConnectToServer(ex);
            }
            if (tm1Server == null || tm1Server.Error)
            {
                string message
                    = tm1Server == null ? string.Empty : tm1Server.ErrorMessage;
                throw Exceptions.UnableToConnectToServer(message);
            }
        }

        private void CloseConnection()
        {
            if (tm1Server != null)
            {
                tm1Server.Disconnect();
            }
        }

        private StandaloneTable GetTableCubeview()
        {
            _TM1View view;
            try
            {
                _TM1Cube cube = tm1Server.Cubes[tm1Settings.Cube];
                view = cube.PublicViews[tm1Settings.View];
            }
            catch (Exception ex)
            {
                throw Exceptions.CubeOrViewIsMissing(ex);
            }

            CubeviewDataHelper cubeviewDataHelper
                = new CubeviewDataHelper(view, parameters,
                    rowcount, propertyBag);
            return cubeviewDataHelper.BuildTable();
        }

        private StandaloneTable GetTableDimensionSubset()
        {
            string queryDimension = tm1Settings.DimensionSubsetLister;

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            TextColumn textColumn = new TextColumn(queryDimension);
            table.AddColumn(textColumn);
            object[] newRow = new object[table.ColumnCount];
            _TM1Dimension dim = tm1Server.getDimension(queryDimension);
            if (dim == null || dim.Error)
            {
                throw Exceptions.DimensionOrSubsetIsMissing();
            }
            int rowsAdded = 0;
            for (int publicSubsetIndex = 0;
                publicSubsetIndex < dim.PublicSubsetsCount;
                publicSubsetIndex++)
            {
                _TM1Subset subset = dim.PublicSubsets[publicSubsetIndex];
                //
                //  if requested show "control" subsets
                //  (subsets whose name begins with the '}' character
                //  (normally hidden)
                //
                if (!TM1Helper.IsControlObject(subset.Name)
                    || tm1Settings.ShowControlObjects)
                {
                    newRow = new object[table.ColumnCount];
                    newRow[0] = subset.Name;
                    table.AddRow(newRow);
                    rowsAdded++;
                    if (rowsAdded >= rowcount && rowcount >= 0)
                    {
                        break;
                    }
                }
            }
            table.EndUpdate();
            return table;
        }

        private StandaloneTable GetTableMDX()
        {
            //  swap out parameters in the expression
            ParameterEncoder encoder = new ParameterEncoder();
            encoder.SourceString = tm1Settings.MDX;
            encoder.Parameters = parameters;
            string mdxQuery = encoder.Encoded();

            _MDXView mdxView;
            try
            {
                mdxView = new _MDXView(tm1Server, mdxQuery);
            }
            catch (Exception ex)
            {
                throw Exceptions.ErrorExecutingMdxQuery(ex);
            }

            if (mdxView.RowsAxisIndex == -1
                || mdxView.ColumnsAxisIndex == -1)
            {
                throw Exceptions.RowsOrColumnsAreMissing();
            }

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            bool isMixedTypes = false;

            // add table columns from row axis dimension names
            for (int dimensionIndex = 0;
                dimensionIndex < mdxView.GetAxis(mdxView.RowsAxisIndex)
                    .DimensionCount;
                dimensionIndex++)
            {
                TextColumn tCol = new TextColumn(
                    mdxView.GetAxis(mdxView.RowsAxisIndex)
                        .GetDimensionName(dimensionIndex));
                table.AddColumn(tCol);

                int checkType = -1;
                for (int i = 0; i < mdxView.GetTupleCount(mdxView.RowsAxisIndex); i++)
                {
                    int cellType = mdxView.GetCellValue(i, 0).Type;
                    if (cellType != checkType)
                    {
                        if (checkType != -1)
                        {
                            isMixedTypes = true;
                            break;
                        }
                        checkType = cellType;
                    }
                }
                
            }

            // add table columns for each column axis member
            for (int iCol = 0;
                iCol < mdxView.GetAxis(mdxView.ColumnsAxisIndex).TupleCount;
                iCol++)
            {
                if (!isMixedTypes
                    && mdxView.GetCellValue(0, iCol).Type
                    == (int) TM1CellType.Numeric)
                {
                    NumericColumn numericColumn =
                        new NumericColumn(
                            mdxView.GetAxis(mdxView.ColumnsAxisIndex)
                                .GetAxisValue(iCol, 0, 1).String);
                    table.AddColumn(numericColumn);
                }
                else
                {
                    TextColumn textColumn =
                        new TextColumn(
                            mdxView.GetAxis(mdxView.ColumnsAxisIndex)
                                .GetAxisValue(iCol, 0, 1).String);
                    table.AddColumn(textColumn);
                }
            }

            // loop through value rows 
            for (int iRow = 0;
                iRow < mdxView.GetAxis(mdxView.RowsAxisIndex).TupleCount;
                iRow++)
            {
                object[] newRow = new object[table.ColumnCount];

                // add row dimension members
                for (int iCol = 0;
                    iCol < mdxView.GetAxis(mdxView.RowsAxisIndex).DimensionCount;
                    iCol++)
                {
                    newRow[iCol] = mdxView.GetAxis(mdxView.RowsAxisIndex)
                        .GetAxisValue(iRow, iCol, 1).String;
                }

                // add column values
                for (int iCol = 0;
                    iCol < mdxView.GetAxis(mdxView.ColumnsAxisIndex).TupleCount;
                    iCol++)
                {
                    if (mdxView.GetCellValue(0, iCol).Type == 1)
                    {
                        newRow[iCol + mdxView
                            .GetAxis(mdxView.RowsAxisIndex).DimensionCount] =
                            mdxView.GetCellValue(iRow, iCol).Real;
                    }
                    else
                    {
                        newRow[iCol + mdxView
                            .GetAxis(mdxView.RowsAxisIndex).DimensionCount] =
                            mdxView.GetCellValue(iRow, iCol).String;
                    }
                }
                table.AddRow(newRow);
                if (iRow >= rowcount && rowcount >= 0)
                    break;
            }

            table.EndUpdate();
            mdxView.Destroy();
            return table;
        }

        private StandaloneTable GetTableSubset()
        {
            string queryDimension = tm1Settings.Dimension;
            string querySubset = tm1Settings.Subset;

            _TM1Dimension dimension = tm1Server.Dimensions[queryDimension];
            if (dimension == null || dimension.Error)
            {
                throw Exceptions.DimensionOrSubsetIsMissing();
            }

            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            if (tm1Settings.IncludeAllSubsets)
            {
                string caption = dimension.Name + "- " + Resources.UiAllMembers;
                table.AddColumn(new TextColumn(caption));

                int rowsAdded = 0;

                for (int elementIndex = 0;
                    elementIndex < dimension.ElementCount;
                    elementIndex++)
                {
                    //
                    //  if dimension attributes are requested
                    //  loop through associated attributes and create additional 
                    //  table columns
                    //
                    _TM1Element element = dimension.Elements[elementIndex];
                    if (tm1Settings.IncludedElementAttributes
                        && dimension.AttributeCount.Int > 0
                        && elementIndex == 0)
                    {
                        for (int attributeIndex = 1;
                            attributeIndex <= element.AttributeCount.Int;
                            attributeIndex++)
                        {
                            string sAttr = element
                                .getAttribute(attributeIndex).Name;
                            int attributeType = element
                                .getAttribute(attributeIndex).AttributeType;
                            if (attributeType
                                == (decimal) TM1AttributeTypes.Numeric)
                            {
                                NumericColumn numericColumn
                                    = new NumericColumn(sAttr);
                                table.AddColumn(numericColumn);
                            }
                            else
                            {
                                TextColumn textColumn = new TextColumn(sAttr);
                                table.AddColumn(textColumn);
                            }
                        }
                    }
                    //  add one row for each element in the subset
                    object[] newRow = new object[table.ColumnCount];
                    newRow[0] = element.Name;
                    if (tm1Settings.IncludedElementAttributes
                        && dimension.AttributeCount.Int > 0)
                    {
                        //
                        //  if requested lookup attribute data
                        //  for each subset element as well
                        //
                        for (int attributeIndex = 1;
                            attributeIndex <= element.AttributeCount.Int;
                            attributeIndex++)
                        {
                            int attributeType = element
                                .getAttribute(attributeIndex).AttributeType;
                            if (attributeType
                                == (decimal) TM1AttributeTypes.Numeric)
                            {
                                newRow[attributeIndex] =
                                    element.getAttributeValue(element
                                        .getAttribute(attributeIndex)).Real;
                            }
                            else
                            {
                                newRow[attributeIndex] =
                                    element.getAttributeValue(
                                        element.getAttribute(attributeIndex))
                                        .String;
                            }
                        }
                    }
                    table.AddRow(newRow);
                    rowsAdded++;
                    if (rowsAdded >= rowcount && rowcount >= 0)
                        break;
                }
            }
            else
            {
                //
                //  open the named subset 
                //
                _TM1Subset subset = dimension.PublicSubsets[querySubset];
                if (subset == null || subset.Error)
                {
                    throw Exceptions.DimensionOrSubsetIsMissing();
                }

                string caption = dimension.Name + "-" + subset.Name;
                //
                //  if the subset "alias" property is not blank
                //  include it in the element column caption
                //
                if (!string.IsNullOrEmpty(subset.Alias))
                {
                    caption = caption + "-" + subset.Alias;
                }
                table.AddColumn(new TextColumn(caption));
                int rowsAdded = 0;

                for (int elementIndex = 0;
                    elementIndex < subset.ElementCount;
                    elementIndex++)
                {
                    _TM1SubsetElement subsetElement
                        = subset.Elements[elementIndex];
                    //
                    //  if requested add columns
                    //  for all attribute in the subset dimension
                    //
                    if (tm1Settings.IncludedElementAttributes
                        && dimension.AttributeCount.Int > 0
                        && elementIndex == 0)
                    {
                        for (int attributeIndex = 1;
                            attributeIndex <= subsetElement
                                .DimensionElement.AttributeCount.Int;
                            attributeIndex++)
                        {
                            string sAttr = subsetElement.DimensionElement
                                .getAttribute(attributeIndex).Name;
                            int attributeType = subsetElement.DimensionElement
                                .getAttribute(attributeIndex).AttributeType;
                            if (attributeType
                                == (decimal) TM1AttributeTypes.Numeric)
                            {
                                NumericColumn numericColumn
                                    = new NumericColumn(sAttr);
                                table.AddColumn(numericColumn);
                            }
                            else
                            {
                                TextColumn textColumn = new TextColumn(sAttr);
                                table.AddColumn(textColumn);
                            }
                        }
                    }
                    //
                    //  add one row for each subset member
                    //  (using the alias value associated with the subset)
                    //
                    object[] newRow = new object[table.ColumnCount];
                    newRow[0] = subsetElement.AliasName;
                    if (tm1Settings.IncludedElementAttributes
                        && dimension.AttributeCount.Int > 0)
                    {
                        //
                        //  if requested populate the attribute columns
                        //  with values associated with this element
                        //
                        for (int attributeIndex = 1;
                            attributeIndex <= subsetElement
                                .DimensionElement.AttributeCount.Int;
                            attributeIndex++)
                        {
                            int attributeType =
                                subsetElement.DimensionElement
                                    .getAttribute(attributeIndex).AttributeType;
                            if (attributeType
                                == (decimal) TM1AttributeTypes.Numeric)
                            {
                                newRow[attributeIndex] =
                                    subsetElement.DimensionElement
                                        .getAttributeValue(subsetElement
                                            .DimensionElement
                                            .getAttribute(attributeIndex)).Real;
                            }
                            else
                            {
                                newRow[attributeIndex] =
                                    subsetElement.DimensionElement
                                        .getAttributeValue(subsetElement
                                            .DimensionElement
                                            .getAttribute(attributeIndex)).String;
                            }
                        }
                    }
                    table.AddRow(newRow);
                    rowsAdded++;
                    if (rowsAdded >= rowcount && rowcount >= 0)
                        break;
                }
            }
            table.EndUpdate();
            return table;
        }
    }
}