﻿using System;
using System.Collections.Generic;
using Applix.TM1.API.Internal;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.TM1Plugin.Properties;

namespace Panopticon.TM1Plugin
{
    internal class CubeviewDataHelper
    {
        private readonly IEnumerable<ParameterValue> parameters;
        private readonly PropertyBag propertyBag;
        private readonly int rowcount;
        private readonly TM1Settings tm1Settings;
        private readonly _TM1View view;
        private int dataStartColumn;

        public CubeviewDataHelper(_TM1View view, IEnumerable<ParameterValue> parameters,
            int rowcount, PropertyBag propertyBag)
        {
            if (view.ColumnSubsetCount < 1
                || view.RowSubsetCount < 1)
            {
                throw Exceptions.RowsOrColumnsAreMissing();
            }

            this.view = view;
            this.parameters = parameters;
            this.tm1Settings = new TM1Settings(propertyBag);
            this.rowcount = rowcount;
            this.propertyBag = propertyBag;
        }

        public StandaloneTable BuildTable()
        {
            // apply parameters
            if (parameters != null)
            {
                ApplyParameters();
            }

            // set null suppression according to settings
            view.setSuppressZerosOnColumns(false);
            bool bPreviousSuppressSetting = view.getSuppressZeroes().Boolean;
            view.setSuppressZeroes(false);
            if (tm1Settings.SuppressEmptyRows || bPreviousSuppressSetting)
            {
                view.setSuppressZerosOnRows(true);
            }

            // reset the view array
            view.destroyArray();
            // execute the view
            view.constructArray();

            // start populating table
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();

            CreateColumns(table);
            PopulateRows(table);

            table.EndUpdate();
            return table;
        }

        private void ApplyParameters()
        {
            if (!tm1Settings.Parameterize)
                return;

            for (int titleIndex = 0; titleIndex < view.TitleSubsetCount; titleIndex++)
            {
                _TM1Subset titleSubset = view.getTitleSubset(titleIndex + 1);

                // if parameter for this title was not applied then continue
                string titleName = titleSubset.Parent.Name;
                DbTitle dbTitle = new DbTitle(propertyBag);
                dbTitle.TitleName = titleName;
                if (string.IsNullOrEmpty(dbTitle.AppliedParameterName))
                    continue;

                string parameterValue = TM1Helper
                    .GetParameterValue(parameters, dbTitle.AppliedParameterName);

                // if it was already properely set then continue
                _TM1Element ele = view.getTitleElement(titleIndex + 1);
                if (titleSubset.Elements[ele.Name].AliasName == parameterValue)
                    continue;

                // if there is no option like it was specified in parameter then continue
                if (titleSubset.Elements[parameterValue] == null)
                    continue;

                _TM1Val newTitles = view.getTitleElements();

                int newTitleIndex = 0;

                for (int i = 0; i < titleSubset.Elements.Count; i++)
                {
                    if (titleSubset.Elements[i].AliasName == parameterValue)
                    {
                        newTitleIndex = i;
                    }
                }

                newTitles.setArrayVal(titleIndex, new _TM1Val(newTitleIndex + 1));
                view.setTitleElements(newTitles);
            }
        }

        private void CreateColumns(StandaloneTable table)
        {
            bool isMixedTypes = AddColumnsByRowsDimensions(table);
            AddColumnsByColumnsDimensions(table, isMixedTypes);
        }

        private bool AddColumnsByRowsDimensions(StandaloneTable table)
        {
            bool isMixedTypes = false;
            _TM1Val checkType = null;
            for (int rowSubsetIndex = 0; rowSubsetIndex < view.RowSubsetCount; rowSubsetIndex++)
            {
                _TM1Subset rowSubset
                    = (_TM1Subset) view.RowSubsets.getArrayVal(rowSubsetIndex);
                _TM1Dimension parentDimension = rowSubset.Parent;

                _TM1Element element = parentDimension.Elements[0];
                int attributesCount = element.AttributeCount.Int;

                if (tm1Settings.IncludeRowAttributes)
                {
                    dataStartColumn
                        += parentDimension.NumberOfLevels*attributesCount;
                }
                else
                {
                    dataStartColumn += parentDimension.NumberOfLevels;
                }
                for (int levelIndex = 0;
                    levelIndex < parentDimension.NumberOfLevels;
                    levelIndex++)
                {
                    if (tm1Settings.IncludeRowAttributes)
                    {
                        // add all attributes for this element
                        for (int attributeIndex = 0;
                            attributeIndex < attributesCount;
                            attributeIndex++)
                        {
                            string caption
                                = element.getAttribute(attributeIndex + 1).Name;
                            if (parentDimension.NumberOfLevels != 1)
                            {
                                caption= string.Format(Resources.UiLevel,
                                    caption, levelIndex + 1);
                            }
                            if (table.ContainsColumn(caption))
                            {
                                caption += " - " + element.Name;
                            }
                            TM1Helper.AddTextColumnToTable(table, caption);
                        }
                    }
                    else
                    {
                        string caption = parentDimension.Name;
                        if (parentDimension.NumberOfLevels != 1)
                        {
                            caption= string.Format(Resources.UiLevel,
                                caption, levelIndex + 1);
                        }
                        if (table.ContainsColumn(caption))
                        {
                            caption += " - " + element.Name;
                        }
                        TM1Helper.AddTextColumnToTable(table, caption);
                    }
                }

                // check if row subsets have mixed types
                // if they are then make all columns string-typed
                for (int elementIndex = 0;
                    elementIndex < rowSubset.ElementCount;
                    elementIndex++)
                {
                    _TM1SubsetElement ele = rowSubset.Elements[elementIndex];
                    _TM1Val eType = new _TM1Val(ele.ElementType);
                    if (checkType == null
                        && eType.Int != (int) TM1ElementTypes.Consolidated)
                    {
                        checkType = eType;
                    }
                    if (checkType != null
                        && eType.Int != checkType.Int
                        && eType.Int != (int) TM1ElementTypes.Consolidated)
                    {
                        isMixedTypes = true;
                        break;
                    }
                }
            }

            return isMixedTypes;
        }

        private void AddColumnsByColumnsDimensions(StandaloneTable table,
            bool isMixedTypes)
        {
            // populate arrays of elements for each column dimension
            // concatenate the members of the different
            // column dimension subsets together separated by "--" 
            // use the "bottom" column dimension to determine data types
            // for the table columns

            // build arrays for each column dimension subset members
            ViewColumnDimension[] viewColumnDimensions
                = new ViewColumnDimension[view.ColumnSubsetCount];

            for (int columnSubsetIndex = 0;
                columnSubsetIndex < view.ColumnSubsetCount;
                columnSubsetIndex++)
            {
                _TM1Subset subset= (_TM1Subset) view
                    .ColumnSubsets.getArrayVal(columnSubsetIndex);
                viewColumnDimensions[columnSubsetIndex].Elements
                    = new List<string>(subset.ElementCount);
                viewColumnDimensions[columnSubsetIndex].ElementTypes
                    = new List<int>(subset.ElementCount);

                for (int elementIndex = 0;
                    elementIndex < subset.ElementCount;
                    elementIndex++)
                {
                    _TM1SubsetElement ele = subset.Elements[elementIndex];
                    _TM1Val eType = new _TM1Val(ele.ElementType);
                    viewColumnDimensions[columnSubsetIndex]
                        .Elements.Add(ele.AliasName);
                    if (isMixedTypes)
                    {
                        viewColumnDimensions[columnSubsetIndex]
                            .ElementTypes.Add((int) TM1ElementTypes.String);
                    }
                    else
                    {
                        viewColumnDimensions[columnSubsetIndex]
                            .ElementTypes.Add(eType.Int);
                    }
                }
            }

            int stackedColumnCount = 1;
            for (int columnSubsetIndex = 0;
                columnSubsetIndex < view.ColumnSubsetCount - 1;
                columnSubsetIndex++)
            {
                stackedColumnCount
                    = stackedColumnCount*viewColumnDimensions[columnSubsetIndex]
                    .Elements.Count;
            }
            List<string> stackedColumns = new List<string>(stackedColumnCount);
            if (view.ColumnSubsetCount > 1)
            {
                ConcatenateStackedColumns(viewColumnDimensions, 0,
                    view.ColumnSubsetCount - 2, "", stackedColumns);
            }
            else
            {
                stackedColumns.Add("");
            }
            foreach (string stackedColumn in stackedColumns)
            {
                for (int elementIndex = 0;
                    elementIndex < viewColumnDimensions[view.ColumnSubsetCount - 1]
                    .Elements.Count;
                    elementIndex++)
                {
                    int attributeType
                        = viewColumnDimensions[view.ColumnSubsetCount - 1]
                    .ElementTypes[elementIndex];
                    string columnName
                        = viewColumnDimensions[view.ColumnSubsetCount - 1]
                        .Elements[elementIndex];
                    TM1Helper.AddColumnToTable(table, attributeType,
                        columnName, stackedColumn);
                }
            }
        }

        private void PopulateRows(StandaloneTable table)
        {
            object[] newRow = new object[table.ColumnCount];
            object[] newRaggedHierarchyRow = new object[table.ColumnCount];
            int rowsAdded = 0;

            for (int rowIndex = view.ColumnSubsetCount;
                rowIndex < view.NumberOfArrayRows;
                rowIndex++)
            {
                bool skipAggregated;
                bool isRaggedToAdd;

                FillRowHierarchiesData(newRow, newRaggedHierarchyRow, rowIndex,
                    out skipAggregated, out isRaggedToAdd);

                if (skipAggregated && !tm1Settings.FillRaggedHierarchies)
                    continue;

                FillColumnHierarchiesData(table, newRow,
                    newRaggedHierarchyRow, rowIndex);

                if (!skipAggregated)
                {
                    rowsAdded++;
                    table.AddRow(newRow);
                }

                if (tm1Settings.FillRaggedHierarchies
                    && isRaggedToAdd)
                {
                    rowsAdded++;
                    table.AddRow(newRaggedHierarchyRow);
                }

                if (rowsAdded >= rowcount && rowcount >= 0)
                {
                    break;
                }
            }
        }

        private void FillRowHierarchiesData(object[] newRow,
            object[] newRaggedHierarchyRow, int rowIndex,
            out bool skipAggregated, out bool isRaggedToAdd)
        {
            // fill row hierarchies
            int newSubsetColumnStartIndex = 0;
            skipAggregated = false;
            isRaggedToAdd = false;

            for (int rowSubsetIndex = 0;
                rowSubsetIndex < view.RowSubsetCount;
                rowSubsetIndex++)
            {
                _TM1Val cell = view
                    .getArrayValue(rowSubsetIndex + 1, rowIndex + 1);
                _TM1Subset subset = (_TM1Subset) view.RowSubsets
                    .getArrayVal(rowSubsetIndex);
                _TM1SubsetElement subsetElement = subset.Elements[cell.Int - 1];

                _TM1Element element = subset.Parent.Elements[0];
                int attributesCount = element.AttributeCount.Int;

                // if has no childs and not last hierarchy level
                if (subsetElement.ComponentCount == 0
                    && subset.LevelsCount != subsetElement.DisplayLevel + 1)
                {
                    isRaggedToAdd = true;
                }

                // if this leaf is not of the last hierarchy level
                // and skip aggregated rows is checked then skip it
                if (tm1Settings.SkipAggregatedRows
                    && subsetElement.DisplayLevel != subset.LevelsCount - 1)
                {
                    skipAggregated = true;
                }

                // indicated column, from which we need
                // to start to fill current subset
                int columnIndex;
                if (tm1Settings.IncludeRowAttributes)
                {
                    columnIndex = newSubsetColumnStartIndex
                                  + subsetElement.DisplayLevel*attributesCount;
                }
                else
                {
                    columnIndex = newSubsetColumnStartIndex
                                  + subsetElement.DisplayLevel;
                }

                // lets clear out cells if we have a new subset
                if (IsNewBranch(rowSubsetIndex, rowIndex))
                {
                    int endColumnIndex;
                    if (tm1Settings.IncludeRowAttributes)
                    {
                        endColumnIndex = newSubsetColumnStartIndex
                                         + subset.LevelsCount*attributesCount;
                    }
                    else
                    {
                        endColumnIndex = newSubsetColumnStartIndex
                                         + subset.LevelsCount;
                    }
                    for (int i = columnIndex + 1; i < endColumnIndex; i++)
                    {
                        newRow[i] = null;
                        switch (tm1Settings.RaggedHierarchyFillMethod)
                        {
                            case RaggedHierarchyFillMethods.Left:
                                newRaggedHierarchyRow[i] = "-";
                                break;
                            case RaggedHierarchyFillMethods.Right:
                                newRaggedHierarchyRow[i] = null;
                                break;
                        }
                    }
                }

                if (tm1Settings.IncludeRowAttributes)
                {
                    for (int i = 0; i < attributesCount; i++)
                    {
                        _TM1Attribute attribute = subsetElement
                            .DimensionElement.getAttribute(i + 1);
                        _TM1Val attributeValue = subsetElement
                            .DimensionElement.getAttributeValue(attribute);
                        newRow[columnIndex + i] = attributeValue.String;
                        newRaggedHierarchyRow[columnIndex + i]
                            = attributeValue.String;
                    }
                }
                else
                {
                    newRow[columnIndex] = subsetElement.AliasName;
                    newRaggedHierarchyRow[columnIndex] = subsetElement.AliasName;
                }

                if (tm1Settings.RaggedHierarchyFillMethod
                    == RaggedHierarchyFillMethods.Right)
                {
                    int endColumnIndex;
                    int startColumnIndex;
                    if (tm1Settings.IncludeRowAttributes)
                    {
                        endColumnIndex = newSubsetColumnStartIndex
                                         + subset.LevelsCount*attributesCount;
                        startColumnIndex = columnIndex + attributesCount - 1;
                    }
                    else
                    {
                        endColumnIndex = newSubsetColumnStartIndex
                                         + subset.LevelsCount;
                        startColumnIndex = columnIndex;
                    }
                    for (int i = startColumnIndex; i < endColumnIndex - 1; i++)
                    {
                        newRaggedHierarchyRow[i + 1] = subsetElement.AliasName;
                    }
                }

                if (tm1Settings.IncludeRowAttributes)
                {
                    newSubsetColumnStartIndex
                        += subset.LevelsCount*attributesCount;
                }
                else
                {
                    newSubsetColumnStartIndex += subset.LevelsCount;
                }
            }
        }

        private void FillColumnHierarchiesData(StandaloneTable table,
            object[] newRow, object[] newRaggedHierarchyRow, int rowIndex)
        {
            // lets add data under columns hierarchies
            int lastDataColumnIndex = view.RowSubsetCount
                                      + (table.ColumnCount - dataStartColumn);
            for (int columnIndex = view.RowSubsetCount;
                columnIndex < lastDataColumnIndex;
                columnIndex++)
            {
                object cellValue = GetCellValue(table, columnIndex, rowIndex);

                int cellColumnIndex = dataStartColumn
                                      + (columnIndex - view.RowSubsetCount);
                newRow[cellColumnIndex] = cellValue;
                switch (tm1Settings.RaggedHierarchyFillMethod)
                {
                    case RaggedHierarchyFillMethods.Left:
                        newRaggedHierarchyRow[cellColumnIndex] = null;
                        break;
                    case RaggedHierarchyFillMethods.Right:
                        newRaggedHierarchyRow[cellColumnIndex] = cellValue;
                        break;
                }
            }
        }

        private object GetCellValue(StandaloneTable table,
            int columnIndex, int rowIndex)
        {
            _TM1Val cell = view.getArrayValue(columnIndex + 1, rowIndex + 1);
            Column tableColumn = table.GetColumn(columnIndex);
            if (tableColumn is TextColumn)
            {
                return cell.String;
            }
            if (tableColumn is TimeColumn)
            {
                return Convert.ToDateTime(cell.String);
            }
            if (tableColumn is NumericColumn)
            {
                return cell.Real;
            }
            return null;
        }

        private bool IsNewBranch(int rowSubsetIndex, int rowIndex)
        {
            _TM1Val cell = view.getArrayValue(rowSubsetIndex + 1, rowIndex + 1);
            _TM1Subset subset = (_TM1Subset) view
                .RowSubsets.getArrayVal(rowSubsetIndex);
            _TM1SubsetElement subsetElement = subset.Elements[cell.Int - 1];

            if (subsetElement.DisplayLevel == 0)
            {
                return true;
            }

            _TM1Val previousCell
                = view.getArrayValue(rowSubsetIndex + 1, rowIndex);
            _TM1Subset previousSubset
                = (_TM1Subset) view.RowSubsets.getArrayVal(rowSubsetIndex);
            _TM1SubsetElement previousSubsetElement
                = previousSubset.Elements[previousCell.Int - 1];

            if (subsetElement.DisplayLevel < previousSubsetElement.DisplayLevel)
            {
                return true;
            }
            return false;
        }

        private static void ConcatenateStackedColumns(ViewColumnDimension[] viewColumnDimensions,
            int levelIndex, int lastLevelIndex,
            string sCaptionRoot, List<string> stackedColumns)
        {
            foreach (string element in viewColumnDimensions[levelIndex].Elements)
            {
                string sCaption = sCaptionRoot + element + "--";
                if (levelIndex == lastLevelIndex)
                {
                    stackedColumns.Add(sCaption);
                }
                if (levelIndex < lastLevelIndex)
                {
                    ConcatenateStackedColumns(viewColumnDimensions,
                        levelIndex + 1, lastLevelIndex, sCaption, stackedColumns);
                }
            }
        }

        private struct ViewColumnDimension
        {
            public List<int> ElementTypes;
            public List<string> Elements;
        }
    }
}