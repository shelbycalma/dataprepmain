﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Applix.TM1.API.Internal;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.TM1Plugin
{
    public class TM1Helper
    {
        public static Task<_TM1Main> GetMainServerAsync(string adminHost,
            string SSLCertId)
        {
            return Task.Factory
                .StartNew(() => GetMainServer(adminHost, SSLCertId));
        }

        public static _TM1Main GetMainServer(string adminHost, string SSLCertId)
        {
            _TM1Main tm1Main = new _TM1Main
            {
                AdminHost = adminHost,
                AdminHostSSLCertID = SSLCertId
            };
            tm1Main.RefreshServers();
            return tm1Main;
        }

        public static void AddTextColumnToTable(StandaloneTable table,
            string columnName)
        {
            TextColumn column = new TextColumn(columnName);
            table.AddColumn(column);
        }

        public static void AddColumnToTable(StandaloneTable table,
            int attributeType, string columnName,
            string stackedColumnName)
        {
            if (attributeType == (int) TM1ElementTypes.String)
            {
                TextColumn tCol
                    = new TextColumn(stackedColumnName + columnName);
                table.AddColumn(tCol);
            }
            else
            {
                NumericColumn tCol
                    = new NumericColumn(stackedColumnName + columnName);
                table.AddColumn(tCol);
            }
        }

        public static string GetParameterValue(
            IEnumerable<ParameterValue> parameters, string parameterName)
        {
            foreach (ParameterValue parameter in parameters)
            {
                if (parameter.Name.Equals(parameterName))
                {
                    ParameterEncoder encoder = new ParameterEncoder();
                    encoder.Parameters = parameters;
                    return encoder.EncodedValue(parameter);
                }
            }
            return null;
        }

        public static bool IsControlObject(string name)
        {
            return name.StartsWith("}");
        }
    }
}