﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Applix.TM1.API.Internal;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.TM1Plugin.Properties;

namespace Panopticon.TM1Plugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<TM1Settings>
    {
        internal const string PluginId = "TM1Plugin";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "IBM Cognos TM1";
        internal const string PluginType = DataPluginTypes.Database;

        public Plugin()
        {
            // prevent plugin load if we dont have tm1 libraries
            try
            {
                Log.Info(Resources.LogLibraryLoaded,
                    typeof (_TM1Val).Assembly.FullName);
            }
            catch (Exception ex)
            {
                throw Datawatch.DataPrep.Data.Core.Connectors.Exceptions
                    .PluginDependenciesAreNotFound(PluginId, ex);
            }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override TM1Settings CreateSettings(PropertyBag bag)
        {
            return new TM1Settings(bag);
        }

        public override ITable GetData(
            string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingQuery);
            DateTime start = DateTime.Now;

            TM1ClientUtil tm1ClientUtil
                = new TM1ClientUtil(bag, parameters, rowcount);
            StandaloneTable result = null;
            result = tm1ClientUtil.GetTable();
            if (result != null)
            {
                // Done, log out how long it took and how much we got.
                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime,
                    result.RowCount, result.ColumnCount, seconds);
                return result;
            }
            throw Exceptions.UnableToLoadTable(string.Empty);
        }
    }
}