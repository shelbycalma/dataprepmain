﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.TM1Plugin.Properties;

namespace Panopticon.TM1Plugin
{
    public class TM1Settings : ConnectionSettings
    {
        public TM1Settings(PropertyBag bag)
            : base(bag)
        {
        }

        public string AdminHost
        {
            get { return GetInternal("AdminHost", "localhost"); }
            set { SetInternal("AdminHost", value); }
        }

        public string SSLCertificateId
        {
            get { return GetInternal("SSLCertificateId", "tm1adminserver"); }
            set { SetInternal("SSLCertificateId", value); }
        }

        public string Server
        {
            get { return GetInternal("Server"); }
            set { SetInternal("Server", value); }
        }

        public string User
        {
            get { return GetInternal("User"); }
            set { SetInternal("User", value); }
        }

        public string Password
        {
            get { return GetInternal("Password"); }
            set { SetInternal("Password", value); }
        }

        public string Cube
        {
            get { return GetInternal("Cube"); }
            set
            {
                SetInternal("Cube", value);
                SetTitle();
            }
        }

        public string Dimension
        {
            get { return GetInternal("Dimension"); }
            set
            {
                SetInternal("Dimension", value);
                SetTitle();
            }
        }

        public string Subset
        {
            get { return GetInternal("Subset"); }
            set
            {
                SetInternal("Subset", value);
                SetTitle();
            }
        }

        public string DimensionSubsetLister
        {
            get { return GetInternal("DimensionSubsetLister"); }
            set
            {
                SetInternal("DimensionSubsetLister", value);
                SetTitle();
            }
        }

        public ObjectType ObjectType
        {
            get
            {
                string s = GetInternal("ObjectType",
                    Enum.GetName(typeof (ObjectType), ObjectType.Unknown));
                return (ObjectType) Enum.Parse(typeof (ObjectType), s, true);
            }
            set
            {
                SetInternal("ObjectType",
                    Enum.GetName(typeof (ObjectType), value));
                SetTitle();
            }
        }

        public bool FillRaggedHierarchies
        {
            get
            {
                string s = GetInternal("FillRaggedHierarchies",
                    false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("FillRaggedHierarchies",
                    Convert.ToString(value));
            }
        }

        public string View
        {
            get { return GetInternal("View"); }
            set
            {
                SetInternal("View", value);
                SetTitle();
            }
        }

        public bool SkipAggregatedRows
        {
            get
            {
                string s = GetInternal("SkipAggregatedRows", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("SkipAggregatedRows", Convert.ToString(value)); }
        }

        public bool SuppressEmptyRows
        {
            get
            {
                string s = GetInternal("SuppressEmptyRows", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("SuppressEmptyRows", Convert.ToString(value)); }
        }

        public string MDX
        {
            get { return GetInternal("MDX"); }
            set
            {
                SetInternal("MDX", value);
                SetTitle();
            }
        }

        public bool ShowControlObjects
        {
            get
            {
                string s = GetInternal("ShowControlObjects", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("ShowControlObjects", Convert.ToString(value)); }
        }

        public bool IncludeRowAttributes
        {
            get
            {
                string s = GetInternal("IncludeRowAttributes", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("IncludeRowAttributes", Convert.ToString(value)); }
        }

        public bool IncludedElementAttributes
        {
            get
            {
                string s = GetInternal("IncludedElementAttributes",
                    false.ToString());
                return Convert.ToBoolean(s);
            }
            set
            {
                SetInternal("IncludedElementAttributes",
                    Convert.ToString(value));
            }
        }

        public bool IncludeAllSubsets
        {
            get
            {
                string s = GetInternal("IncludeAllSubsets", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("IncludeAllSubsets", Convert.ToString(value)); }
        }

        public RaggedHierarchyFillMethods RaggedHierarchyFillMethod
        {
            get
            {
                string s = GetInternal("RaggedHierarchyFillMethod",
                    Enum.GetName(typeof (RaggedHierarchyFillMethods),
                        RaggedHierarchyFillMethods.Left));
                return (RaggedHierarchyFillMethods) Enum
                    .Parse(typeof (RaggedHierarchyFillMethods), s, true);
            }
            set
            {
                SetInternal("RaggedHierarchyFillMethod",
                    Enum.GetName(typeof (RaggedHierarchyFillMethods), value));
            }
        }

        public bool Parameterize
        {
            get
            {
                string s = GetInternal("Parameterize", false.ToString());
                return Convert.ToBoolean(s);
            }
            set { SetInternal("Parameterize", Convert.ToString(value)); }
        }

        private void SetTitle()
        {
            if (ObjectType == ObjectType.Cubeview)
            {
                Title = Resources.UiTitleCubeview
                        + Server + "-" + Cube + "-" + View;
            }
            else if (ObjectType == ObjectType.SubsetElement)
            {
                Title = Resources.UiTitleSubset
                        + Server + "-" + Dimension + "-" + Subset;
            }
            else if (ObjectType == ObjectType.DimensionSubset)
            {
                Title = Resources.UiTitleDimension
                        + Server + "-" + DimensionSubsetLister;
            }
            else
            {
                Title = Resources.UiTitleMdxQuery + Server;
            }
        }

        public void RestoreDefaults()
        {
            Server = null;
            Cube = null;
            Dimension = null;
            Subset = null;
            DimensionSubsetLister = null;
            ObjectType = ObjectType.Unknown;
            FillRaggedHierarchies = false;
            View = null;
            SkipAggregatedRows = false;
            SuppressEmptyRows = false;
            MDX = null;
            ShowControlObjects = false;
            IncludeRowAttributes = false;
            IncludedElementAttributes = false;
            IncludeAllSubsets = false;
            RaggedHierarchyFillMethod = RaggedHierarchyFillMethods.Left;
            Parameterize = false;
            Title = null;
        }
    }
}