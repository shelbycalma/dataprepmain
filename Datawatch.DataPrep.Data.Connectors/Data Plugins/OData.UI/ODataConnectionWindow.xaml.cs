﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.ODataPlugin.UI
{
    /// <summary>
    /// Interaction logic for ODataConnectionWindow.xaml
    /// </summary>
    internal partial class ODataConnectionWindow : Window
    {
        private ODataSettingsViewModel settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ODataConnectionWindow));

        public ODataConnectionWindow(ODataSettingsViewModel settings)
        {
            this.settings = settings;

            InitializeComponent();
            cancelButton.Click += cancelButton_Click;
            connectionPanel.Settings = settings;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
