﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.ODataPlugin.UI
{
    public class ODataSettingsViewModel : INotifyPropertyChanged
    {
        private ICommand fetchCollectionsCommand;
        private ICommand addODataParameter;
		private ICommand _previewCommand;
		private DataTable _previewTable;
		public event PropertyChangedEventHandler PropertyChanged;

        ObservableCollection<ODataParameter> oDataParameters = new
         ObservableCollection<ODataParameter>();

        List<ODataField> odataFieldList = new
         List<ODataField>();

        List<string> navigationProperties = new List<string>();

        List<string> collectionsList = new List<string>();


        public string CollectionListURL
        {
            get { return this.Model.CollectionListURL; }
            set
            {
                if (value != this.Model.CollectionListURL)
                {
                    this.Model.CollectionListURL = value;
                    NotifyChanged("CollectionListURL");
                }
            }
        }

        public string UserId
        {
            get { return this.Model.UserId; }
            set
            {
                if (value != this.Model.UserId)
                {
                    this.Model.UserId = value;
                    NotifyChanged("UserId");
                }
            }
        }

        public string Password
        {
            get { return this.Model.Password; }
            set
            {
                if (value != this.Model.Password)
                {
                    this.Model.Password = value;
                    NotifyChanged("Password");
                }
            }
        }

        public string Collection
        {
            get { return this.Model.Collection; }
            set
            {
                try
                {
                    if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                    {
                        Mouse.OverrideCursor = Cursors.Wait;
                    }
                    if (value != this.Model.Collection)
                    {
                        this.Model.Collection = value;
                        NotifyChanged("Collection");
                    }
                }
                finally
                {
                    if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                    {
                        Mouse.OverrideCursor = null;
                    }
                }
            }
        }

        public int RequestTimeout
        {
            get { return this.Model.RequestTimeout; }
            set
            {
                if (value != this.Model.RequestTimeout)
                {
                    this.Model.RequestTimeout = value;
                    NotifyChanged("RequestTimeout");
                }
            }
        }

        public string ODataQuery
        {
            get { return this.Model.ODataQuery; }
            set
            {
                if (value != this.Model.ODataQuery)
                {
                    this.Model.ODataQuery = value;
                    NotifyChanged("ODataQuery");
                }
            }
        }

        public string BaseCollectionListURL
        {
            get { return this.Model.BaseCollectionListURL; }
            set
            {
                if (value != this.Model.BaseCollectionListURL)
                {
                    this.Model.BaseCollectionListURL = value;
                    NotifyChanged("BaseCollectionListURL");
                }
            }
        }

        public NumericDataHelper NumericDataHelper
        {
            get
            {
                return this.Model.NumericDataHelper;
            }
        }

        public bool IsMetadataSupported
        {
            get { return this.Model.IsMetadataSupported; }
            set
            {
                if (value != this.Model.IsMetadataSupported)
                {
                    this.Model.IsMetadataSupported = value;
                    NotifyChanged("IsMetadataSupported");
                }
            }
        }

        public ODataSettingsViewModel(ODataSettings model)
        {
            this.Model = model;
            this.PropertyChanged += this_PropertyChanged;
        }

        public ODataSettings Model { get; private set; }

        private void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        public int[] TimeoutSeconds
        {
            get
            {
                return new int[]{
                    10,
                    20,
                    30,
                    60
                };
            }
        }

        public List<string> AvailableCollections
        {
            get
            {
                if (!string.IsNullOrEmpty(Collection))
                {
                    LoadCollections();
                }
                return collectionsList;
            }
        }

        private void LoadCollections()
        {
            try
            {
                if (!CanFetchCollections())
                {
                    return;
                }
                string baseURL;

                ODataTableBuilder util = new ODataTableBuilder(this.Model);
                collectionsList = util.GetCollections(out baseURL);

                if (!string.IsNullOrEmpty(baseURL))
                {
                    BaseCollectionListURL = baseURL;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                       Properties.Resources.UiWindowTitle,
                       MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

		public ICommand PreviewCommand
		{
			get
			{
				return _previewCommand ??
					   (_previewCommand = new DelegateCommand(PreviewData, CanPreview));
			}
		}

		public ICommand FetchCollectionsCommand
        {
            get
            {
                if (fetchCollectionsCommand == null)
                {
                    fetchCollectionsCommand = new DelegateCommand(FetchCollections,
                        CanFetchCollections);
                }
                return fetchCollectionsCommand;
            }
        }

        public bool CanPreview()
        {
            return !(string.IsNullOrEmpty(ODataQuery) ||
                    string.IsNullOrEmpty(Collection));
        }
        private void FetchCollections()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                if (string.IsNullOrEmpty(Collection))
                {
                    LoadCollections();
                }
                NotifyChanged("AvailableCollections");
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private bool CanFetchCollections()
        {
            return (!(string.IsNullOrEmpty(CollectionListURL)) &&
              !(string.Equals(CollectionListURL,
              this.Model.DefaultCollectionListURL,
              StringComparison.CurrentCultureIgnoreCase)));
        }

        public ObservableCollection<ODataParameter> ODataParameters
        {
            get
            {
                if (string.IsNullOrEmpty(Collection) == false &&
                    this.Model.GetParameterCount() > 0 && odataFieldList.Count == 0)
                {
                    GetFields();

                }

                if (odataFieldList.Count > 0)
                {

                    GetODataParametersFromModel();
                }

                return oDataParameters;
            }
        }

        public List<ODataField> ODataFieldList
        {
            get
            {
                return odataFieldList.Where(field => field.IsNumericField ==
                    true || field.FieldType == ODataFieldType.String ||
                    field.FieldType == ODataFieldType.DateTime).ToList();
            }
        }

        public List<string> NavigationProperties
        {
            get
            {
                return navigationProperties;
            }
        }

        public bool IsOk
        {
            get
            {
                return !(string.IsNullOrEmpty(ODataQuery) ||
                    string.IsNullOrEmpty(Collection));

            }
        }

        public ICommand AddODataParameter
        {
            get
            {
                if (addODataParameter == null)
                {
                    addODataParameter = new DelegateCommand(AddODataParam,
                        CanAddODataParam);
                }
                return addODataParameter;
            }
        }

        private void AddODataParam()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                ODataParameter odataParameter = new ODataParameter();
                odataParameter.PropertyChanged += new
                    PropertyChangedEventHandler(
                    odataParameter_PropertyChanged);
                oDataParameters.Add(odataParameter);
                SaveParameters();
                NotifyChanged("ODataParameters");
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private bool CanAddODataParam()
        {
            return !(string.IsNullOrEmpty(BaseCollectionListURL) ||
                string.IsNullOrEmpty(Collection));
        }

        void odataParameter_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            ODataParameter parameter = (ODataParameter)sender;

            if (e.PropertyName == "SelectedFieldName" ||
                e.PropertyName == "SelectedQueryOperatorId" ||
                e.PropertyName == "ParameterValue")
            {
                SaveParameters();
                BuildQuery();
            }
        }

        internal void DeleteODataParameter(ODataParameter parameter)
        {
            oDataParameters.Remove(parameter);
            SaveParameters();
            BuildQuery();
            NotifyChanged("ODataParameters");
        }

        private void SaveParameters()
        {
            this.Model.SavedODataParameters = oDataParameters.ToArray();
        }

        private void GetODataParametersFromModel()
        {
            oDataParameters.Clear();

            foreach (ODataParameter parameter in
                this.Model.SavedODataParameters.ToList())
            {
                parameter.PropertyChanged += new
                PropertyChangedEventHandler(
                odataParameter_PropertyChanged);

                oDataParameters.Add(parameter);

                if (!string.IsNullOrEmpty(parameter.SelectedFieldName))
                {
                    parameter.Field = odataFieldList.Where(o => o.FieldName ==
                        parameter.SelectedFieldName).FirstOrDefault();
                }
                if (parameter.Field != null &&
                    parameter.SelectedQueryOperatorId > 0)
                {
                    parameter.QueryOperator =
                        ODataQueryOperator.GetQueryOperators(
                        parameter.Field).Where(o => o.QueryOperatorId ==
                        parameter.SelectedQueryOperatorId).FirstOrDefault();
                }
            }
        }

        public void this_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Collection")
            {
                GetFields();
                this.Model.Title = this.Model.Collection;
                NotifyChanged("ODataParameters");
                BuildQuery();

            }
            else if (e.PropertyName == "CollectionListURL")
            {
                collectionsList = new List<string>();
                NotifyChanged("AvailableCollections");
                odataFieldList = new List<ODataField>();
                NotifyChanged("ODataFieldList");

            }
        }

        private void BuildQuery()
        {
            ODataQuery = ODataQueryBuilder.GetQuery(BaseCollectionListURL, Collection,
                oDataParameters.ToList());

        }

        private void GetFields()
        {
            try
            {
                if (String.IsNullOrEmpty(BaseCollectionListURL) ||
                String.IsNullOrEmpty(Collection))
                {
                    return;
                }

                ODataTableBuilder tableBuilder = new ODataTableBuilder(this.Model);
                if (this.Model.IsMetadataSupported == true)
                {
                    odataFieldList = new List<ODataField>();
                    navigationProperties = new List<string>();
                    tableBuilder.GetFields(odataFieldList, navigationProperties);
                }
                else
                {
                    odataFieldList = new List<ODataField>();
                    tableBuilder.GetFieldsWithoutMetadata(odataFieldList);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                       Properties.Resources.UiWindowTitle,
                       MessageBoxButton.OK, MessageBoxImage.Error);
            }
            NotifyChanged("ODataFieldList");
        }
		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				NotifyChanged("PreviewTable");
			}
		}
		
		private PropertyBag CreatePropertyBag()
		{
			PropertyBag pb = new PropertyBag();
			PropertyValue pv = new PropertyValue("UserId", Model.UserId);
			pb.Values.Add(pv);
			pv = new PropertyValue("Password", Model.Password);
			pb.Values.Add(pv);
			pv = new PropertyValue("RequestTimeout", Model.RequestTimeout.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("ODataQuery", Model.ODataQuery);
			pb.Values.Add(pv);
			pv = new PropertyValue("Collection", Model.Collection);
			pb.Values.Add(pv);
			pv = new PropertyValue("BaseCollectionListURL", Model.BaseCollectionListURL);
			pb.Values.Add(pv);
			pv = new PropertyValue("DefaultCollectionListURL", Model.DefaultCollectionListURL);
			pb.Values.Add(pv);
			pv = new PropertyValue("CollectionListURL", Model.CollectionListURL);
			pb.Values.Add(pv);
			pv = new PropertyValue("Title", Model.Title);
			pb.Values.Add(pv);

			NumericDataHelper ndh = Model.NumericDataHelper;
			PropertyBag numericDataHelperBag = new PropertyBag();
			numericDataHelperBag.Name = "NumericDataHelper";
			PropertyValue value = new PropertyValue("DecimalSeparator", ndh.DecimalSeparator.ToString());
			numericDataHelperBag.Values.Add(value);
			pb.SubGroups.Add(numericDataHelperBag);

			return pb;
		}

		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}

				Panopticon.ODataPlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);

				if (t != null)
				{
					_previewTable = PreviewDataTable.GetPreviewTable(t);
					NotifyChanged("PreviewTable");
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				MessageBox.Show(ex.Message, null,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

	}
}
