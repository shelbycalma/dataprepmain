﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Panopticon.ODataPlugin.UI
{
    /// <summary>
    /// Interaction logic for ODataConnectionPanel.xaml
    /// </summary>
    public partial class ODataConnectionPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
               DependencyProperty.Register("Settings",
                   typeof(ODataSettingsViewModel), typeof(ODataConnectionPanel),
                       new PropertyMetadata(new PropertyChangedCallback(
                           Settings_Changed)));

        public ODataConnectionPanel()
        {
            InitializeComponent();
        }

        public ODataSettingsViewModel Settings
        {
            get { return (ODataSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ODataConnectionPanel)obj).OnSettingsChanged(
                (ODataSettingsViewModel)args.OldValue,
                (ODataSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(ODataSettingsViewModel oldSettings,
            ODataSettingsViewModel newSettings)
        {
            DataContext = newSettings;

            if (newSettings != null)
            {
                PasswordBox.Password = newSettings.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (Settings != null)
            {

                ODataParameter parameter = (ODataParameter)e.Parameter;
                if (parameter == null)
                {
                    return;
                }

                string fieldName = "";
                if (parameter.Field != null && parameter.Field.FieldName != null)
                {
                    fieldName = parameter.Field.FieldName;
                }
                string message = string.Format(
                    Properties.Resources.ConfirmDeleteParameter,
                    fieldName);
                if (MessageBox.Show(message,
                    Properties.Resources.ConfirmDeleteTitle,
                    MessageBoxButton.OKCancel,
                    MessageBoxImage.Question) == MessageBoxResult.OK)
                {
                    Settings.DeleteODataParameter(parameter);
                }
            }
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}
