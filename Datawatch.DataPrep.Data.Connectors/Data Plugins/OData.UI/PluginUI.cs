﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.ODataPlugin.Properties;

namespace Panopticon.ODataPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, ODataSettings>
    {
        private const string imageUri = "pack://application:,,,/" +
            "Panopticon.ODataPlugin.UI;component/OData-plugin.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(imageUri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            ODataSettings settings = new ODataSettings();
            ODataSettingsViewModel settingsView =
                 new ODataSettingsViewModel(settings);
            ODataConnectionWindow window =
                new ODataConnectionWindow(settingsView);

            window.Owner = owner;

            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            string title = Resources.UiDefaultConnectionTitle;
            if (settings.Collection != null)
            {
                title = settings.Collection;

            }
            settings.Title = title;

            PropertyBag properties = settings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ODataSettings settings = Plugin.CreateSettings(bag);

            return new ODataConnectionPanel()
            {
                Settings = new ODataSettingsViewModel(settings)
            };
        }

        public override PropertyBag GetSetting(object element)
        {
            ODataConnectionPanel panel = (ODataConnectionPanel)element;

            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
