﻿using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Microsoft.Win32;

namespace Panopticon.RservePlugin.UI
{
    internal partial class ConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                                        typeof(RservePluginSettingsViewModel),
                                        typeof(ConfigPanel),
                                        new PropertyMetadata(Settings_Changed));

        private readonly Window owner;
        public ConfigPanel()
        {
            InitializeComponent();
            owner = Window.GetWindow(this) ?? Application.Current.MainWindow;
        }

        public RservePluginSettingsViewModel Settings
        {
            get { return (RservePluginSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (RservePluginSettingsViewModel)args.OldValue,
                (RservePluginSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(RservePluginSettingsViewModel oldSettings, RservePluginSettingsViewModel newSettings)
        {
            DataContext = newSettings;

            PasswordBox.Password = newSettings != null ? newSettings.Password : null;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            bool? result = dialog.ShowDialog(owner);
            if (!result.Value) return;

            string fileName = dialog.FileName;
            if (File.Exists(fileName))
            {
                Settings.Script = File.ReadAllText(fileName);
            }
        }
    }
}
