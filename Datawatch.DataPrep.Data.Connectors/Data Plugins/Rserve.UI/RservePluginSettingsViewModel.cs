﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core.RScriptsHelpers;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.RservePlugin.UI
{
    internal class RservePluginSettingsViewModel : ViewModelBase
    {
        public RservePluginSettings Model { get; set; }
        public IEnumerable<ParameterValue> Parameters { get; private set; }

        public RservePluginSettingsViewModel(RservePluginSettings settings, IEnumerable<ParameterValue> parameters)
        {
            Model = settings;
            Parameters = parameters;
        }

        #region Properties

        public string Host
        {
            get { return Model.Host; }
            set
            {
                Model.Host = value;
                OnPropertyChanged("Host");
            }
        }

        public string Port
        {
            get { return Model.Port; }
            set
            {
                Model.Port = value;
                OnPropertyChanged("Port");
            }
        }

        public string Username
        {
            get { return Model.Username; }
            set
            {
                Model.Username = value;
                OnPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return Model.Password; }
            set
            {
                Model.Password = value;
                OnPropertyChanged("Password");
            }
        }

        public string Script
        {
            get { return Model.Script; }
            set
            {
                Model.Script = value;
                OnPropertyChanged("Script");
            }
        }

        public bool EncloseParametersInQuotes
        {
            get { return Model.EncloseParametersInQuotes; }
            set
            {
                Model.EncloseParametersInQuotes = value;
                OnPropertyChanged("EncloseParametersInQuotes");
            }
        }

        public int Timeout
        {
            get { return Model.Timeout; }
            set
            {
                Model.Timeout = value;
                OnPropertyChanged("Timeout");
            }
        }

        #endregion

        public bool IsOk()
        {
            return !string.IsNullOrEmpty(Host) &&
                   !string.IsNullOrEmpty(Port) &&
                   !string.IsNullOrEmpty(Script);
        }

        private ICommand testConnectionCommand;
        public ICommand TestConnectionCommand
        {
            get
            {
                return testConnectionCommand ??
                   (testConnectionCommand = new DelegateCommand(TestConnection, CanTestConnection));
            }
        }

        private void TestConnection()
        {
            bool connectionResult = ExecutionHelper.TestConnection(Model.ToRSettings(), Parameters);
            if (connectionResult)
            {
                MessageBox.Show(Datawatch.DataPrep.Data.Core.Properties.Resources.MessageRTestConnectionSuccess,
                                Datawatch.DataPrep.Data.Core.Properties.Resources.UiRTestConnectionTitle,
                                MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show(Datawatch.DataPrep.Data.Core.Properties.Resources.MessageRTestConnectionFailed,
                                Datawatch.DataPrep.Data.Core.Properties.Resources.UiRTestConnectionTitle,
                                MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CanTestConnection()
        {
            return !string.IsNullOrEmpty(Host) && !string.IsNullOrEmpty(Port);
        }
    }
}
