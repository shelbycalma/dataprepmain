﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.RservePlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, RservePluginSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            RservePluginSettings rservePluginSettings = new RservePluginSettings();
            RservePluginSettingsViewModel rservePluginSettingsViewModel =
                new RservePluginSettingsViewModel(rservePluginSettings, parameters);
            ConfigWindow configWindow = new ConfigWindow(rservePluginSettingsViewModel);
            configWindow.Owner = owner;

            bool? result = configWindow.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            PropertyBag properties = rservePluginSettings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            return new ConfigPanel()
            {
                Settings = new RservePluginSettingsViewModel(Plugin.CreateSettings(bag), parameters)
            };
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel panel = (ConfigPanel)obj;
            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
