﻿using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.UI.DragDrop;

namespace Panopticon.BusinessObjectsUniversePlugin.UI.ViewModels
{
    public class AddedItemViewModel : ViewModelBase
    {
        private string displayName;
        private string parameterName;
        private string @operator;

        public AddedItemViewModel(string id, string name, string path,
            string dispalyName, string type, string parameterName, string @operator)
        {
            if (id == null)
            {
                Exceptions.ArgumentNull("id");
            }

            if (name == null)
            {
                Exceptions.ArgumentNull("name");
            }

            if (path == null)
            {
                Exceptions.ArgumentNull("path");
            }

            if (dispalyName == null)
            {
                Exceptions.ArgumentNull("dispalyName");
            }

            if (type == null)
            {
                Exceptions.ArgumentNull("type");
            }

            Id = id;
            Name = name;
            DisplayName = dispalyName;
            Path = path;
            Type = type;
            ParameterName = parameterName;
            Operator = @operator;

            if (Type == BusinessObjectsUniversePlugin.Properties.Resources.UiDimension)
            {
                Image = "/Panopticon.BusinessObjectsUniversePlugin.UI;component/Images/dimension.png";
            }
            else if (Type == BusinessObjectsUniversePlugin.Properties.Resources.UiMeasure)
            {
                Image = "/Panopticon.BusinessObjectsUniversePlugin.UI;component/Images/measure.png";
            }
            else if (Type == BusinessObjectsUniversePlugin.Properties.Resources.UiAttribute)
            {
                Image = "/Panopticon.BusinessObjectsUniversePlugin.UI;component/Images/attribute.png";
            }
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Path { get; private set; }
        public string Type { get; private set; }

        public new string DisplayName
        {
            get { return displayName; }
            set
            {
                if (displayName == value) return;
                displayName = value;
                OnPropertyChanged("DisplayName");
            }
        }

        public string ParameterName
        {
            get { return parameterName; }
            set
            {
                if (parameterName == value) return;
                parameterName = value;
                OnPropertyChanged("ParameterName");
            }
        }

        public string Operator
        {
            get { return @operator; }
            set
            {
                if (@operator == value) return;
                @operator = value;
                OnPropertyChanged("Operator");
            }
        }

        public string Image { get; private set; }
    }

    internal class AddedItemViewModelDragSourceAdvisor
        : ListBoxDragSourceAdvisor<AddedItemViewModel>
    {
    }

    internal class AddedItemViewModelDropTargetAdvisor
        : ListBoxDropTargetAdvisor<AddedItemViewModel>
    {
    }
}
