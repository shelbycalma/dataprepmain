﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.BusinessObjectsUniversePlugin.TreeItems;
using Panopticon.BusinessObjectsUniversePlugin.Extentions;
using Panopticon.BusinessObjectsUniversePlugin.UI.Properties;
using System.Data;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.BusinessObjectsUniversePlugin.UI.ViewModels
{
    public class BusinessObjectsUniverseSettingsViewModel : ViewModelBase
	{
        private readonly IEnumerable<ParameterValue> parameters;

        private readonly ObservableCollection<ITreeItem> treeItems = new ObservableCollection<ITreeItem>();
        private readonly List<ITreeItem> flattenedTreeItems = new List<ITreeItem>();
        private readonly ObservableCollection<AddedItemViewModel> addedItems = new ObservableCollection<AddedItemViewModel>();
        private readonly ObservableCollection<AddedFilterViewModel> addedFilters = new ObservableCollection<AddedFilterViewModel>();

        private ICommand deleteAllItemsCommand;
        private ICommand deleteAllFiltersCommand;
        private ICommand validateQueryCommand;
		private ICommand _previewCommand;
		private DataTable _previewTable;

		private BusinessObjectsUniverseClient client;

        private string searchText;

        private readonly IEnumerable<KeyValuePair<string, string>> filterOperators = new List<KeyValuePair<string, string>>
        {
            new KeyValuePair<string, string>("EqualTo", "Equal To (=)"),
            new KeyValuePair<string, string>("NotEqualTo", "Not Equal To (<>)"),
            new KeyValuePair<string, string>("LessThan", "Less Than (<)"),
            new KeyValuePair<string, string>("GreaterThan", "Greater Than (>)"),
            new KeyValuePair<string, string>("LessThanOrEqualTo", "Less Than or Equal To(<=)"),
            new KeyValuePair<string, string>("GreaterThanOrEqualTo", "Greater Than or Equal to (>=) "),
            new KeyValuePair<string, string>("Like", "Like"),
            new KeyValuePair<string, string>("NotLike", "Not Like")
        };

        public BusinessObjectsUniverseSettingsViewModel(
            BusinessObjectsUniverseSettings settigns, IEnumerable<ParameterValue> parameters)
        {
            Model = settigns;
            this.parameters = parameters;

            AddedItems.CollectionChanged += AddedItems_CollectionChanged;
            foreach (Item item in Model.Items)
            {
                AddedItems.Add(new AddedItemViewModel(item.Id, item.Name, item.Path, item.DisplayName, item.Type,
                    item.ParameterName, item.Operator));
            }

            foreach (Filter filter in Model.Filters)
            {
                AddedFilters.Add(new AddedFilterViewModel(filter.Id, filter.Name, filter.Path));
            }
        }

        public BusinessObjectsUniverseSettings Model { get; private set; }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public IEnumerable<KeyValuePair<string, string>> FilterOperators
        {
            get { return filterOperators; }
        }

        public string Url
        {
            get { return Model.Url; }
            set
            {
                if (value == Model.Url) return;
                Model.Url = value;
                OnPropertyChanged("Url");
                ClearTreeViewAndAddedItems();
            }
        }

        public string Port
        {
            get { return Model.Port; }
            set
            {
                if (value == Model.Port) return;
                Model.Port = value;
                OnPropertyChanged("Port");
                ClearTreeViewAndAddedItems();
            }
        }

		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				OnPropertyChanged("PreviewTable");
			}
		}

		public string Username
        {
            get { return Model.Username; }
            set
            {
                if (value == Model.Username) return;
                Model.Username = value;
                OnPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return Model.Password; }
            set
            {
                if (value == Model.Password) return;
                Model.Password = value;
                OnPropertyChanged("Password");
            }
        }

        public string UniversePath
        {
            get { return Model.UniversePath; }
            set
            {
                if (value == Model.UniversePath) return;
                Model.UniversePath = value;
                OnPropertyChanged("UniversePath");
                OnPropertyChanged("DisplayUniversePath");
            }
        }

        public string TableName
        {
            get { return Model.TableName; }
            set
            {
                if (value == Model.TableName) return;
                Model.TableName = value;
                //OnPropertyChanged("UniversePath");
                //OnPropertyChanged("DisplayUniversePath");
            }
        }

        public string DisplayUniversePath { get { return string.Format(Resources.UiSelectedUniverse, UniversePath); } }

        public AuthenticationType AuthenticationType
        {
            get { return Model.AuthenticationType; }
            set
            {
                if (value == Model.AuthenticationType) return;
                Model.AuthenticationType = value;
                OnPropertyChanged("AuthenticationType");
            }
        }

        public int RequestTimeout
        {
            get { return Model.RequestTimeout; }
            set
            {
                if (value == Model.RequestTimeout) return;
                Model.RequestTimeout = value;
                OnPropertyChanged("RequestTimeout");
            }
        }

        public int[] Timeouts
        {
            get
            {
                return new[]{
                    10,
                    20,
                    30,
                    60,
                    90,
                    120,
                    180
                };
            }
        }

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                string text = searchText.ToLower().Trim();

                foreach (ITreeItem treeItem in flattenedTreeItems)
                {
                    treeItem.IsVisible = string.IsNullOrWhiteSpace(text);
                }

                foreach (ITreeItem treeItem in flattenedTreeItems)
                {
                    if (!treeItem.Name.ToLower().Contains(text)) continue;
                    treeItem.IsVisible = true;
                    if (treeItem.Parent == null) continue;
                    treeItem.Parent.IsExpanded = true;
                    treeItem.Parent.IsVisible = true;
                }
            }
        }

        public IEnumerable<AuthenticationType> AuthenticationTypes
        {
            get { return Enum.GetValues(typeof(AuthenticationType)).Cast<AuthenticationType>(); }
        }

        private void AddedItems_CollectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (AddedItemViewModel c in e.NewItems)
                    {
                        c.PropertyChanged += AddedItem_PropertyChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (AddedItemViewModel c in e.OldItems)
                    {
                        c.PropertyChanged -= AddedItem_PropertyChanged;
                    }
                    break;
            }
        }

        private void AddedItem_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            SetItemsToModel();
        }

        public ObservableCollection<ITreeItem> TreeItems { get { return treeItems; } }

        public IEnumerable<ITreeItem> FlattenedTreeItems { get { return flattenedTreeItems; } }

        public ObservableCollection<AddedItemViewModel> AddedItems { get { return addedItems; } }

        public ObservableCollection<AddedFilterViewModel> AddedFilters { get { return addedFilters; } }

		public ICommand PreviewCommand
		{
			get
			{
				return _previewCommand ??
					   (_previewCommand = new DelegateCommand(PreviewData, CanPreview));
			}
		}

		public ICommand DeleteAllItemsCommand
        {
            get
            {
                return deleteAllItemsCommand ??
                       (deleteAllItemsCommand = new DelegateCommand(
                           DeleteAllItems));
            }
        }

        public ICommand DeleteAllFiltersCommand
        {
            get
            {
                return deleteAllFiltersCommand ??
                       (deleteAllFiltersCommand = new DelegateCommand(
                           DeleteAllFilters));
            }
        }

        public ICommand ValidateQueryCommand
        {
            get
            {
                return validateQueryCommand ??
                       (validateQueryCommand = new DelegateCommand(
                           ValidateQuery, () => IsOk));
            }
        }

        public bool CanPreview()
        {
            return AddedItems.Any() &&
                       AddedItems.Select(ai => ai.DisplayName).Distinct().Count() == AddedItems.Count;
        }
        private void ValidateQuery()
        {
            CloseConnection();
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                client = new BusinessObjectsUniverseClient(Model, parameters);
                client.GetData(1, parameters);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(
                //    string.Format(Resources.UiQueryValidationFailed, ex.Message),
                //    null, MessageBoxButton.OK, MessageBoxImage.Error);
                MessageBox.Show(
                    string.Format(Resources.UiQueryValidationFailed, ErrorMessages.GetDisplayMessage(ex.Message)),
                    null, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }

            MessageBox.Show(Resources.UiQueryValidatedSuccessfully, Resources.UiSuccess, MessageBoxButton.OK,
                MessageBoxImage.Information);
        }

        public void DeleteAllItems()
        {
            foreach (AddedItemViewModel addedItem in AddedItems.ToList())
            {
                AddedItemViewModel item = addedItem;
                foreach (ISelectableTreeItem selectableTreeItem in
                    flattenedTreeItems.OfType<ISelectableTreeItem>().Where(i => i.Id == item.Id))
                {
                    selectableTreeItem.IsSelected = false;
                }

                AddedItems.Remove(addedItem);
            }

            CheckExistanceOfSelectedItemsAndFilters();
            SetItemsToModel();
        }

        public void DeleteAllFilters()
        {
            foreach (AddedFilterViewModel addedFilter in AddedFilters.ToList())
            {
                AddedFilterViewModel filter = addedFilter;
                foreach (ISelectableTreeItem selectableTreeItem in
                    flattenedTreeItems.OfType<ISelectableTreeItem>().Where(i => i.Id == filter.Id))
                {
                    selectableTreeItem.IsSelected = false;
                }

                AddedFilters.Remove(addedFilter);
            }

            CheckExistanceOfSelectedItemsAndFilters();
            SetFiltersToModel();
        }

        public void DeleteItem(AddedItemViewModel addedItemViewModel)
        {
            foreach (ISelectableTreeItem selectableTreeItem in
                    flattenedTreeItems.OfType<ISelectableTreeItem>().Where(i => i.Id == addedItemViewModel.Id))
            {
                selectableTreeItem.IsSelected = false;
            }

            AddedItems.Remove(addedItemViewModel);

            CheckExistanceOfSelectedItemsAndFilters();
            SetItemsToModel();
        }

        public void DeleteFilter(AddedFilterViewModel addedFilterViewModel)
        {
            foreach (ISelectableTreeItem selectableTreeItem in
                    flattenedTreeItems.OfType<ISelectableTreeItem>().Where(i => i.Id == addedFilterViewModel.Id))
            {
                selectableTreeItem.IsSelected = false;
            }

            AddedFilters.Remove(addedFilterViewModel);

            CheckExistanceOfSelectedItemsAndFilters();
            SetFiltersToModel();
        }

        public bool IsOk
        {
            get
            {
                return AddedItems.Any() &&
                       AddedItems.Select(ai => ai.DisplayName).Distinct().Count() == AddedItems.Count;
            }
        }

        private void ClearTreeViewAndAddedItems()
        {
            UniversePath = null;
            Model.UniverseId = null;
            AddedItems.Clear();
            AddedFilters.Clear();

            foreach (ISelectableTreeItem selectableTreeItem in flattenedTreeItems.OfType<ISelectableTreeItem>())
            {
                selectableTreeItem.PropertyChanged -= SelectableTreeItem_PropertyChanged;
            }

            foreach (ISelectableChildrenTreeItem selectableChildrenTreeItem in
                flattenedTreeItems.OfType<ISelectableChildrenTreeItem>())
            {
                selectableChildrenTreeItem.SettingChildrenSelected -= SelectableChildrenTreeItem_PropertyChanged;
            }

            foreach (ITreeItem treeItem in flattenedTreeItems)
            {
                treeItem.PropertyChanged -= TreeItem_PropertyChanged;
            }

            flattenedTreeItems.Clear();
            TreeItems.Clear();

            SetItemsToModel();
            SetFiltersToModel();
        }

        public void Connect()
        {
            CloseConnection();
            client = new BusinessObjectsUniverseClient(Model, parameters);

            TreeItems.Clear();

            foreach (ISelectableTreeItem selectableTreeItem in flattenedTreeItems.OfType<ISelectableTreeItem>())
            {
                selectableTreeItem.PropertyChanged -= SelectableTreeItem_PropertyChanged;
            }

            foreach (ISelectableChildrenTreeItem selectableChildrenTreeItem in
                flattenedTreeItems.OfType<ISelectableChildrenTreeItem>())
            {
                selectableChildrenTreeItem.SettingChildrenSelected -= SelectableChildrenTreeItem_PropertyChanged;
            }

            foreach (ITreeItem treeItem in flattenedTreeItems)
            {
                treeItem.PropertyChanged -= TreeItem_PropertyChanged;
            }

            flattenedTreeItems.Clear();

            foreach (ITreeItem treeItem in client.CreateRootTreeItems())
            {
                TreeItems.Add(treeItem);
                treeItem.PropertyChanged += TreeItem_PropertyChanged;
            }

            flattenedTreeItems.AddRange(TreeItems);

            foreach (ISelectableChildrenTreeItem selectableChildrenTreeItem in
                flattenedTreeItems.OfType<ISelectableChildrenTreeItem>())
            {
                selectableChildrenTreeItem.SettingChildrenSelected += SelectableChildrenTreeItem_PropertyChanged;
            }

            if (Model.UniverseId != null)
            {
                Universe selectedUniverse = TreeItems.OfType<Universe>().SingleOrDefault(u => u.Id == Model.UniverseId);
                if (selectedUniverse != null)
                {
                    LoadChildren(selectedUniverse);
                }

                foreach (ISelectableTreeItem item in flattenedTreeItems.OfType<ISelectableTreeItem>())
                {
                    item.CanSelect = item.Universe.Id == Model.UniverseId;
                }

                foreach (ISelectableChildrenTreeItem item in flattenedTreeItems.OfType<ISelectableChildrenTreeItem>())
                {
                    item.CanSelect = item.UniverseId == Model.UniverseId;
                }
            }
        }

        private void TreeItem_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsExpanded") return;
            ITreeItem treeItem = (ITreeItem)sender;
            try
            {
                LoadChildren(treeItem);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    string.Format(Resources.UiConnectionFailed, ex.Message),
                    null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SelectableChildrenTreeItem_PropertyChanged(object sender, EventArgs e)
        {
            ITreeItem treeItem = (ITreeItem)sender;
            LoadChildren(treeItem);
        }

        public void CloseConnection()
        {
            if (client != null)
            {
                client.Close();
            }
        }

        private void LoadChildren(ITreeItem treeItem)
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                if (!treeItem.HasDummyChild) return;

                IList<ITreeItem> newItems = null;

                Folder folder = treeItem as Folder;
                if (folder != null)
                {
                    newItems = client.CreateFolderChildren(folder);
                }

                Universe universe = treeItem as Universe;
                if (universe != null)
                {
                    newItems = client.CreateUniverseChildren(universe);
                }

                if (newItems == null) return;

                treeItem.Children.Clear();
                treeItem.Children.AddRange(newItems);
                IList<ITreeItem> flattenedNewItems = Utils.FlattenTree(newItems);

                foreach (ITreeItem child in flattenedNewItems)
                {
                    child.PropertyChanged += TreeItem_PropertyChanged;
                }

                foreach (ISelectableTreeItem child in flattenedNewItems.OfType<ISelectableTreeItem>())
                {
                    child.PropertyChanged += SelectableTreeItem_PropertyChanged;
                    if (Model.UniverseId == null) continue;

                    child.CanSelect = Model.UniverseId == child.Universe.Id;
                    child.IsSelected = Model.UniverseId == child.Universe.Id &&
                                       (AddedItems.Any(ai => ai.Id == child.Id) ||
                                        AddedFilters.Any(af => af.Id == child.Id));
                }

                foreach (ISelectableChildrenTreeItem child in flattenedNewItems.OfType<ISelectableChildrenTreeItem>())
                {
                    child.SettingChildrenSelected += SelectableChildrenTreeItem_PropertyChanged;
                    if (Model.UniverseId != null)
                    {
                        child.CanSelect = Model.UniverseId == child.UniverseId;
                    }
                }

                flattenedTreeItems.AddRange(flattenedNewItems);

                if (Model.UniverseId != null)
                {
                    Universe selectedUniverse = flattenedNewItems.OfType<Universe>().SingleOrDefault(u => u.Id == Model.UniverseId);
                    if (selectedUniverse != null)
                    {
                        LoadChildren(selectedUniverse);
                    }
                }
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }
        
        private void SelectableTreeItem_PropertyChanged(object o, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected") return;
            ISelectableTreeItem selectableTreeItem = (ISelectableTreeItem)o;

            if (selectableTreeItem.IsSelected)
            {
                if (selectableTreeItem is TreeFilter)
                {
                    if (AddedFilters.All(f => f.Id != selectableTreeItem.Id))
                    {
                        AddedFilters.Add(new AddedFilterViewModel(selectableTreeItem.Id, selectableTreeItem.Name, selectableTreeItem.Path));
                    }
                }
                else
                {
                    if (AddedItems.All(f => f.Id != selectableTreeItem.Id))
                    {
                        string originalDisplayName = selectableTreeItem.Name;
                        string displayName = originalDisplayName;
                        int index = 1;
                        while (AddedItems.Any(ai => ai.DisplayName == displayName))
                        {
                            displayName = string.Format("{0}{1}", originalDisplayName, index);
                            index++;
                        }

                        AddedItems.Add(new AddedItemViewModel(selectableTreeItem.Id,
                            selectableTreeItem.Name, selectableTreeItem.Path, displayName, selectableTreeItem.TypeName,
                            null, null));
                    }
                }

                if (Model.UniverseId == null)
                { 
                    UniversePath = Utils.GetUniversePath(selectableTreeItem.Universe) + selectableTreeItem.Universe.Name;
                    TableName = selectableTreeItem.Parent.Name;
                    foreach (ISelectableTreeItem item in flattenedTreeItems.OfType<ISelectableTreeItem>())
                    {
                        item.CanSelect = item.Universe.Id == selectableTreeItem.Universe.Id;
                    }

                    foreach (ISelectableChildrenTreeItem item in flattenedTreeItems.OfType<ISelectableChildrenTreeItem>())
                    {
                        item.CanSelect = item.UniverseId == selectableTreeItem.Universe.Id;
                    }
                }

                Model.UniverseId = selectableTreeItem.Universe.Id;
            }
            else
            {
                if (selectableTreeItem is TreeFilter)
                {
                    AddedFilterViewModel addedFilter = AddedFilters.SingleOrDefault(af => af.Id == selectableTreeItem.Id);
                    if (addedFilter != null)
                    {
                        AddedFilters.Remove(addedFilter);
                    }
                }
                else
                {
                    AddedItemViewModel addedItem = AddedItems.SingleOrDefault(ai => ai.Id == selectableTreeItem.Id);
                    if (addedItem != null)
                    {
                        AddedItems.Remove(addedItem);
                    }
                }

                CheckExistanceOfSelectedItemsAndFilters();
            }

            foreach (ISelectableTreeItem item in flattenedTreeItems.OfType<ISelectableTreeItem>()
                .Where(item => item.Id == selectableTreeItem.Id && item.Universe.Id == selectableTreeItem.Universe.Id))
            {
                item.IsSelected = selectableTreeItem.IsSelected;
            }

            SetItemsToModel();
            SetFiltersToModel();
        }

        private void CheckExistanceOfSelectedItemsAndFilters()
        {
            if (AddedItems.Any() || AddedFilters.Any()) return;
            UniversePath = null;
            Model.UniverseId = null;
            foreach (ISelectableTreeItem item in flattenedTreeItems.OfType<ISelectableTreeItem>())
            {
                item.CanSelect = true;
            }
            foreach (ISelectableChildrenTreeItem item in flattenedTreeItems.OfType<ISelectableChildrenTreeItem>())
            {
                item.CanSelect = true;
            }
        }

        public bool CanConnect
        {
            get
            {
                return
                    !string.IsNullOrEmpty(Url) &&
                    !string.IsNullOrEmpty(Port) &&
                    !string.IsNullOrEmpty(Username) &&
                    !string.IsNullOrEmpty(Password);
            }
        }

        private void SetItemsToModel()
        {
            Model.Items = AddedItems.Select(ai =>
                new Item(ai.Id,
                         ai.Name,
                         ai.Path,
                         ai.DisplayName,
                         ai.Type,
                         ai.ParameterName,
                         ai.Operator)).ToArray();
        }

        private void SetFiltersToModel()
        {
            Model.Filters = AddedFilters.Select(af =>
                new Filter(af.Id,
                         af.Name,
                         af.Path)).ToArray();
        }

		private PropertyBag CreatePropertyBag()
		{
			const string ItemsKey = "Items_{0}";
			const string FiltersKey = "Filters_{0}";
			const string IdKey = "Id";
			const string NameKey = "Name";
			const string PathKey = "Path";
			const string DisplayNameKey = "DisplayName";
			const string TypeKey = "Type";
			const string OperatorKey = "Operator";
			const string ParameterNameKey = "ParameterName";

			PropertyBag pb = new PropertyBag();
			PropertyValue pv = new PropertyValue("Port", Model.Port);
			pb.Values.Add(pv);
			pv = new PropertyValue("Url", Model.Url);
			pb.Values.Add(pv);
			pv = new PropertyValue("Username", Model.Username);
			pb.Values.Add(pv);
			pv = new PropertyValue("Password", Model.Password);
			pb.Values.Add(pv);
			pv = new PropertyValue("UniverseId", Model.UniverseId);
			pb.Values.Add(pv);
			pv = new PropertyValue("UniversePath", Model.UniversePath);
			pb.Values.Add(pv);
			pv = new PropertyValue("TableName", Model.TableName);
			pb.Values.Add(pv);
			pv = new PropertyValue("RequestTimeout", Model.RequestTimeout.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("AuthenticationType", Model.AuthenticationType.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("Title", Model.Title);
			pb.Values.Add(pv);
			pv = new PropertyValue("ItemsCount", Model.Items.Count().ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("FiltersCount", Model.Filters.Count().ToString());
			pb.Values.Add(pv);

			for (int i = 0; i < Model.Items.Count(); i++)
			{
				var value = Model.Items[i];
				PropertyBag itemBag = new PropertyBag();
				itemBag.Values[DisplayNameKey] = value.DisplayName;
				itemBag.Values[IdKey] = value.Id;
				itemBag.Values[NameKey] = value.Name;
				itemBag.Values[PathKey] = value.Path;
				itemBag.Values[TypeKey] = value.Type;

				if (!string.IsNullOrEmpty(value.ParameterName) &&
					!string.IsNullOrEmpty(value.Operator))
				{
					itemBag.Values[ParameterNameKey] = value.ParameterName;
					itemBag.Values[OperatorKey] = value.Operator;
				}
				itemBag.Name = String.Format(ItemsKey, i);

				pb.SubGroups.Add(itemBag);
			}

			for (int i = 0; i < Model.Filters.Count(); i++)
			{
				var value = Model.Filters[i];
				PropertyBag filterBag = new PropertyBag();
				filterBag.Values[IdKey] = value.Id;
				filterBag.Values[NameKey] = value.Name;
				filterBag.Values[PathKey] = value.Path;
				filterBag.Name = String.Format(FiltersKey, i);

				pb.SubGroups.Add(filterBag);
			}
			return pb;
		}
		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}
				
				Panopticon.BusinessObjectsUniversePlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);

				if (t != null)
				{
					
					_previewTable = PreviewDataTable.GetPreviewTable(t);
					OnPropertyChanged("PreviewTable");
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				//MessageBox.Show(ex.Message, null,
				//	MessageBoxButton.OK, MessageBoxImage.Error);
                MessageBox.Show(ErrorMessages.GetDisplayMessage(ex.Message), null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

	}
}
