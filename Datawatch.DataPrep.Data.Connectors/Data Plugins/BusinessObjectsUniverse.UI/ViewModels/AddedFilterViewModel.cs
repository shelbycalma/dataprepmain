﻿using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.UI.DragDrop;

namespace Panopticon.BusinessObjectsUniversePlugin.UI.ViewModels
{
    public class AddedFilterViewModel : ViewModelBase
    {
        public AddedFilterViewModel(string id, string name, string path)
        {
            if (id == null)
            {
                Exceptions.ArgumentNull("id");
            }

            if (name == null)
            {
                Exceptions.ArgumentNull("name");
            }

            if (path == null)
            {
                Exceptions.ArgumentNull("path");
            }

            Id = id;
            Name = name;
            Path = path;
        }

        public string Id { get; private set; }
        public string Name { get; private set; }
        public string Path { get; private set; }

        public string Type { get { return BusinessObjectsUniversePlugin.Properties.Resources.UiFilter; } }
        public string Image { get { return "/Panopticon.BusinessObjectsUniversePlugin.UI;component/Images/filter.png"; } }
    }

    internal class AddedFilterViewModelDragSourceAdvisor
    : ListBoxDragSourceAdvisor<AddedFilterViewModel>
    {
    }

    internal class AddedFilterViewModelDropTargetAdvisor
        : ListBoxDropTargetAdvisor<AddedFilterViewModel>
    {
    }
}
