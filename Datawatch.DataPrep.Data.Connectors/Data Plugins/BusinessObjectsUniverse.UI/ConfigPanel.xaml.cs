﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.BusinessObjectsUniversePlugin.TreeItems;
using Panopticon.BusinessObjectsUniversePlugin.UI.ViewModels;

namespace Panopticon.BusinessObjectsUniversePlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
        private int addedItemsLastSelectedIndex = -1;
        private int addedFiltersLastSelectedIndex = -1;

        public static DependencyProperty SettingsProperty =
               DependencyProperty.Register("Settings",
                   typeof(BusinessObjectsUniverseSettingsViewModel), typeof(ConfigPanel),
                       new PropertyMetadata(Settings_Changed));

        public static RoutedCommand ConnectCommand
            = new RoutedCommand("Connect", typeof(ConfigPanel));

        public ConfigPanel()
        {
            InitializeComponent();
            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
        }

        private void OnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Settings != null && Settings.CanConnect)
            {
                Connect_Executed(this, null);
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            if (Settings != null)
            {
                Settings.CloseConnection();
            }
        }

        public BusinessObjectsUniverseSettingsViewModel Settings
        {
            get { return (BusinessObjectsUniverseSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (BusinessObjectsUniverseSettingsViewModel)args.OldValue,
                (BusinessObjectsUniverseSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(
            BusinessObjectsUniverseSettingsViewModel oldSettings,
            BusinessObjectsUniverseSettingsViewModel newSettings)
        {
            DataContext = newSettings;
            PasswordBox.Password = newSettings != null ? newSettings.Password : null;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void Connect_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                Settings.Connect();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show(
                    Datawatch.DataPrep.Data.Core.UI.Utils.GetOwnerWindow(this),
                    string.Format(Properties.Resources.UiConnectionFailed, ex.Message),
                    null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private void Connect_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Settings != null && Settings.CanConnect;
        }
        
        private void DeleteItem_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Parameter == null) return;
            Settings.DeleteItem((AddedItemViewModel)e.Parameter);
        }

        private void DeleteFilter_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Parameter == null) return;
            Settings.DeleteFilter((AddedFilterViewModel)e.Parameter);
        }

        private void NavigateToTreeViewItem(string id, IInputElement listView)
        {
            ISelectableTreeItem selectableTreeItem = Settings.FlattenedTreeItems.OfType<ISelectableTreeItem>()
                .FirstOrDefault(i => i.Id == id && Settings.Model.UniverseId == i.Universe.Id && !i.IsBusinessViewChild);
            if (selectableTreeItem == null || !selectableTreeItem.IsVisible) return;
            TreeViewItem treeViewItem = Datawatch.DataPrep.Data.Core.UI.Utils.FindTreeViewItem(TreeView, selectableTreeItem);
            if (treeViewItem == null) return;
            selectableTreeItem.IsExpanded = true;
            treeViewItem.IsSelected = true;
            treeViewItem.Focus();
            listView.Focus();
        }

        private void ItemsListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (addedItemsLastSelectedIndex == ItemsListView.SelectedIndex) return;
            AddedItemViewModel addedItemViewModel = ItemsListView.SelectedItem as AddedItemViewModel;
            if (addedItemViewModel == null) return;
            NavigateToTreeViewItem(addedItemViewModel.Id, ItemsListView);
            addedItemsLastSelectedIndex = ItemsListView.SelectedIndex;
        }

        private void FiltersListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (addedFiltersLastSelectedIndex == FiltersListView.SelectedIndex) return;
            AddedFilterViewModel addedFilterViewModel = FiltersListView.SelectedItem as AddedFilterViewModel;
            if (addedFilterViewModel == null) return;
            NavigateToTreeViewItem(addedFilterViewModel.Id, FiltersListView);
            addedFiltersLastSelectedIndex = FiltersListView.SelectedIndex;
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }

        private void PreviewData_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
