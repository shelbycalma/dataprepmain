﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Panopticon.BusinessObjectsUniversePlugin.UI.ViewModels;

namespace Panopticon.BusinessObjectsUniversePlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, BusinessObjectsUniverseSettings>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            BusinessObjectsUniverseSettings businessObjectsUniverseSettings =
                new BusinessObjectsUniverseSettings();
            BusinessObjectsUniverseSettingsViewModel settingsView =
                new BusinessObjectsUniverseSettingsViewModel(businessObjectsUniverseSettings, parameters);
            ConfigWindow window = new ConfigWindow(settingsView);

            window.Owner = owner;

            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            PropertyBag properties = businessObjectsUniverseSettings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            BusinessObjectsUniverseSettings businessObjectsUniverseSettings = Plugin.CreateSettings(bag);

            return new ConfigPanel
            {
                Settings = new BusinessObjectsUniverseSettingsViewModel(
                    businessObjectsUniverseSettings, parameters)
            };
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel panel = (ConfigPanel)obj;
            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
