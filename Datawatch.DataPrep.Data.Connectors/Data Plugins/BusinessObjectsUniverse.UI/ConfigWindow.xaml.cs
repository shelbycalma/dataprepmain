﻿using System;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.BusinessObjectsUniversePlugin.UI.ViewModels;

namespace Panopticon.BusinessObjectsUniversePlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        private readonly BusinessObjectsUniverseSettingsViewModel viewModel;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindow));

        public ConfigWindow(BusinessObjectsUniverseSettingsViewModel viewModel)
        {
            InitializeComponent();
            this.viewModel = viewModel;
            ConfigPanel.Settings = viewModel;
        }

        public BusinessObjectsUniverseSettingsViewModel ViewModel
        {
            get { return viewModel; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = viewModel != null && viewModel.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            using (
                BusinessObjectsUniverseClient client =
                    new BusinessObjectsUniverseClient(ViewModel.Model, viewModel.Parameters))
            {
                try
                {
                    client.GetData(1, viewModel.Parameters);
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    MessageBox.Show(string.Format(Properties.Resources.UiQueryValidationFailed, ex.Message),
                        null, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
            }

            this.DialogResult = true;
        }
    }
}
