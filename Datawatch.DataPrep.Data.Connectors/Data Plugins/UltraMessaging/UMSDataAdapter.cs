﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.UMSPlugin
{
    public class UMSDataAdapter : MessageQueueAdapterBase<ParameterTable,
        UMSSettings, Dictionary<string, object>>
    {

        private UMSListener listener;

        public override void Start()
        {
            settings.Parameters = table.Parameters;
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();

            try
            {
                listener = new UMSListener(this);

                // Connect
                listener.Connect(settings);
                // Subscribe
                string topicName = ParseTopic(settings.Topic);
                topicName = DataUtils.ApplyParameters(topicName,
                    table.Parameters);
                listener.ListenTo(topicName, settings);
            }
            catch (InvalidOperationException e)
            {
                string innerMessage = "";
                if (e.InnerException != null)
                {
                    innerMessage = e.InnerException.Message;
                }

                Log.Error(Properties.Resources.LogFailedToStartListener,
                    e.Message, innerMessage);
                throw e;
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.LogFailedToStartListener + "\n"
                          + e.Message);
            }
        }

        public override void Stop()
        {
            base.Stop();
            try
            {
                listener.Disconnect(settings);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }
            listener = null;
        }

        public static string ParseTopic(string source)
        {
            // Some hardcoded cleaning of the string here, seems easier than
            // trying to communicate in the GUI that the user shouldnt enter
            // the topic part.
            string topicName = source.StartsWith("topic://")
                ? source.Substring("topic://".Length)
                : source;
            return topicName;
        }

        public void MessageReceived(string message)
        {
            DataPluginUtils.EnqueueMessage(message, parser, idColumn, updater);
        }
    }
}