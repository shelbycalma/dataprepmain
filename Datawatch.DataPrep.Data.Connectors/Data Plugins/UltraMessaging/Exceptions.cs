﻿using System;

namespace Panopticon.UMSPlugin
{
    internal class Exceptions : Datawatch.DataPrep.Data.Core.Connectors.Exceptions
    {
        internal static Exception InvalidSetting(string attribute, 
            string setting, Exception innerException)
        {
            return LogException(new InvalidOperationException(
                string.Format("{0}\n{1}: {2}", 
                Properties.Resources.ExSettingInvalid, attribute, setting),
                innerException));
        }

        internal static Exception NoColumns()
        {
            return CreateInvalidOperation("Resources.NoColumns");
        }

        internal static Exception NoIdColumn()
        {
            return CreateInvalidOperation("Resources.NoIdColumn");
        }
    }
}