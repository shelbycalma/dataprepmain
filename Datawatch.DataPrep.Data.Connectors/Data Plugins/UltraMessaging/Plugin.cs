﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.UMSPlugin
{
    /// <summary>
    /// DataPlugin for UMS.
    /// </summary>
    [LicenseProvider(typeof (DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : MessageQueuePluginBase<ParameterTable, UMSSettings,
        Dictionary<string, object>, UMSDataAdapter>
    {
        internal const string PluginId = "UMSPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Ultra Messaging Streams";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override UMSSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new UMSSettings(pluginManager, parameters);
        }

        public override UMSSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new UMSSettings(pluginManager, bag, null);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            UMSSettings setting = new UMSSettings(pluginManager, settings, null);
            if (setting.ConfigurationType == ConfigurationType.ConfigurationFile)
            {
                if (setting.ConfigFile.Length > 0)
                {
                    return new string[] { setting.ConfigFile };
                }
            }
            return null;
        }

        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();
            UMSSettings setting = new UMSSettings(pluginManager, settings, null);

            setting.ConfigFile =
                Utils.ComputeRelativePath(workbookPath, newPath);
        }
    }
}