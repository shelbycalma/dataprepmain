﻿using System.ComponentModel;

namespace Panopticon.UMSPlugin
{

    public enum ReceptionMode
    {
        [Description("Streaming")]
        Streaming,
        [Description("Persistent")]
        Persistent,
        [Description("Ultra Load Balancing")]
        UltraLoadBalancing
    }
}
