﻿using System.ComponentModel;

namespace Panopticon.UMSPlugin
{
    public enum ConfigurationType
    {
        [Description("Configuration File")]
        ConfigurationFile,
        [Description("Manual Configuration")]
        ManualConfiguration
    }
}
