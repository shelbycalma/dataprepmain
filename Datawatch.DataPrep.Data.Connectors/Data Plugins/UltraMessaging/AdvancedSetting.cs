﻿namespace Panopticon.UMSPlugin
{
    public class AdvancedSetting
    {
        public static readonly string Delimiter = ";;;";
        public string Scope { get; set; }
        public string Attribute { get; set; }
        public string Value { get; set; }

        public AdvancedSetting(string scope, string attribute, string value)
        {
            Scope = scope;
            Attribute = attribute;
            Value = value;
        }

        public string CreatePropertyString()
        {
            return string.Format("{0}{3}{1}{3}{2}", Scope, Attribute, Value,
                Delimiter);
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", Scope, Attribute, Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(AdvancedSetting)) return false;
            return Equals((AdvancedSetting)obj);
        }

        public bool Equals(AdvancedSetting other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return
                Equals(other.Scope, Scope) &&
                Equals(other.Value, Value) &&
                Equals(other.Attribute, Attribute);
        }

        public override int GetHashCode()
        {
            int result = 17;
            if (Scope != null)
            {
                result = result * 23 + Scope.GetHashCode();
            }
            if (Value != null)
            {
                result = result * 23 + Value.GetHashCode();
            }
            if (Attribute != null)
            {
                result = result * 23 + Attribute.GetHashCode();
            }
            return result;
        }
    }
}