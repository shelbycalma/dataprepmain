﻿namespace Panopticon.UMSPlugin
{
    public class UnicastResolverDaemon
    {
        public string IpAddress { get; set; }
        public string Port { get; set; }

        public UnicastResolverDaemon(string ipAddress, string port)
        {
            IpAddress = ipAddress;
            Port = port;
        }
    }
}