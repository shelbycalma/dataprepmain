﻿//using UMSPlugin.Properties;
using System;
using com.latencybusters.lbm;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.UMSPlugin
{
    internal class UMSListener
    {

        private LBMContextAttributes contextAttributes;
        private LBMReceiverAttributes receiverAttributes;
        private LBMContext context;
        private LBMReceiver receiver;
        private LBMWildcardReceiver wildcardReceiver;
        private LBMTopic topic;
        private UMSSettings settings;
        private readonly IMessageHandler messageHandler;

        public UMSListener(IMessageHandler messageHandler)
        {
            if (messageHandler == null)
            {
                throw Exceptions.DataAdaptorWasNull();
            }
            this.messageHandler = messageHandler;
        }

        public void Connect(UMSSettings settings)
        {
            if (settings == null) return;

            this.settings = settings;

            if (settings.ConfigurationType ==
                ConfigurationType.ConfigurationFile)
            {
                var parameterizedConfigFile =
                    settings.ParameterizeValue(settings.ConfigFile);
                LBM.setConfiguration(parameterizedConfigFile);
            }

            contextAttributes = new LBMContextAttributes();
            receiverAttributes = new LBMReceiverAttributes();

            if (settings.ConfigurationType ==
                ConfigurationType.ManualConfiguration)
            {
                ApplyContextConfigurationManually();
                ApplyReceiverConfigurationManually();
                ApplyAdvancedConfigurationSettings();
            }

            context = new LBMContext(contextAttributes);
        }


        public void Disconnect(UMSSettings settings)
        {
            if (settings == null) return;
            try
            {
                if (settings.IsTopicWildcard)
                    wildcardReceiver.close();
                else
                    receiver.close();

                context.close();
            }
            catch (Exception ex)
            {
                // I dont really know what could go wrong here, catch the
                // internal base exception and log everything.
                //Log.Error(string.Format(Resources.InternalError,
                //  ex.Message));
                Log.Exception(ex);
            }

            //topic = null;
            Log.Info("Disconnected");
        }

        public void ListenTo(string topicString, UMSSettings settings)
        {
            bool isTopicWildcard = settings == null? false:settings.IsTopicWildcard;

            if (isTopicWildcard)
            {
                wildcardReceiver = new LBMWildcardReceiver(context, topicString,
                    receiverAttributes, null, OnReceive, null);
            }
            else
            {
                topic = new LBMTopic(context, topicString, receiverAttributes);
                receiver = new LBMReceiver(context, topic, OnReceive, null,
                    null);
            }

            Log.Info("Listening to " + topicString);
        }

        public void ConnectionInterruptedListener()
        {
            // TODO do we need to call updater.ConnectionLost(true); ?
            Log.Warning("Connection Interrupted");
        }

        public void ConnectionResumedListener()
        {
            Log.Warning("Connection Resumed");
        }

        public void ExceptionListener(Exception ex)
        {
            // TODO do we need to call updater.ConnectionLost(true); ?
            //Log.Error(string.Format(Resources.InternalError,
            //  ex.Message));
            Log.Exception(ex);
        }

        public int OnReceive(Object cbArgs, LBMMessage theMessage)
        {
            /* There are several different events that can cause the
             *  receiver callbackto be called.  Decode the event that
             * caused this.  */
            switch (theMessage.type())
            {
                case LBM.MSG_DATA:
                    string xml = System.Text.Encoding.
                        GetEncoding("ISO-8859-1").GetString(
                            theMessage.data());
                    messageHandler.MessageReceived(xml);
                    break;

                case LBM.MSG_BOS:
                    Log.Info(Properties.Resources.LogBeginningOfTransportSession,
                        theMessage.topicName(), theMessage.source());

                    break;

                case LBM.MSG_EOS:
                    Log.Info(Properties.Resources.LogEndOfTransportSession,
                        theMessage.topicName(), theMessage.source());
                    break;

                default:
                    Log.Error(Properties.Resources.LogUnexpected, theMessage.type());
                    System.Environment.Exit(1);
                    break;
            }
            theMessage.dispose();
            /*
             * Return 0 if there were no errors. 
             * Returning a non-zero value will
             * cause LBM to log a generic error message.
             */
            return 0;
        }

        private void ApplyContextConfigurationManually()
        {
            var type = typeof (LBMContextAttributes);

            if (settings.TopicResolutionMode == TopicResolutionMode.Multicast)
            {
                SetAttribute(type, "resolver_multicast_address",
                    settings.MulticastGroup);
                SetAttribute(type, "resolver_multicast_port",
                    settings.MulticastDestinationPort);
            }
            else
            {
                var resolverDaemons = settings.LoadResolverDaemons();
                foreach (var daemon in resolverDaemons)
                {
                    var daemonAddress = string.Format("{0}:{1}",
                        daemon.IpAddress, daemon.Port);
                    SetAttribute(type, "resolver_unicast_daemon",
                        daemonAddress);
                }
            }

            SetAttribute(type, "resolver_multicast_interface",
                settings.LocalNetworkInterface);
            SetAttribute(type, "resolver_unicast_interface",
                settings.LocalNetworkInterface);
            SetAttribute(type, "response_tcp_interface",
                settings.LocalNetworkInterface);
            SetAttribute(type, "request_tcp_interface",
                settings.LocalNetworkInterface);

            if (settings.ReceptionMode ==
                ReceptionMode.Persistent)
            {
                SetAttribute(type, "ume_session_id",
                    settings.PersistentSessionId);
            }
        }

        private void ApplyReceiverConfigurationManually()
        {
            var type = typeof (LBMReceiverAttributes);

            SetAttribute(type, "transport_tcp_interface",
                settings.LocalNetworkInterface);
            SetAttribute(type, "transport_lbtru_interface",
                settings.LocalNetworkInterface);

            if (settings.ReceptionMode ==
                ReceptionMode.UltraLoadBalancing)
            {
                SetAttribute(type, "umq_receiver_type_id",
                    settings.UlbReceiverTypeId);
            }
        }

        private void ApplyAdvancedConfigurationSettings()
        {
            var advancedSettings = settings.LoadAdvancedSettings();

            foreach (var setting in advancedSettings)
            {
                switch (setting.Scope.ToLower())
                {
                    case "context":
                        SetAttribute(typeof (LBMContextAttributes),
                            setting.Attribute, setting.Value);
                        break;
                    case "receiver":
                        SetAttribute(typeof (LBMReceiverAttributes),
                            setting.Attribute, setting.Value);
                        break;
                }
            }
        }

        private void SetAttribute(
            Type attributesType, string attribute,
            string setting)
        {
            try
            {
                var parameterizedSetting = settings.ParameterizeValue(setting);

                if (!string.IsNullOrEmpty(parameterizedSetting))
                {
                    if (attributesType == typeof(LBMContextAttributes))
                    {
                        contextAttributes.setValue(attribute, 
                            parameterizedSetting);
                    }
                    else if (attributesType == typeof(LBMReceiverAttributes))
                    {
                        receiverAttributes.setValue(attribute,
                            parameterizedSetting);
                    }
                }
            } 
            catch (Exception e)
            {
                throw 
                    UMSPlugin.Exceptions.InvalidSetting(attribute, setting, e);
            }
        }
    }
}