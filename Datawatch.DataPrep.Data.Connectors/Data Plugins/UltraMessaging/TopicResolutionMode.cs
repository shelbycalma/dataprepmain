﻿using System.ComponentModel;

namespace Panopticon.UMSPlugin
{
    public enum TopicResolutionMode
    {
        [Description("Multicast")]
        Multicast,
        [Description("Unicast")]
        Unicast
    }
}
