﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.UMSPlugin
{
    public class UMSSettings : MessageQueueSettingsBase
    {
        public UMSSettings(IPluginManager pluginManager) :
            this(pluginManager, new PropertyBag(), null)
        {
        }

        public UMSSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
        }

        public UMSSettings(IPluginManager pluginManager, PropertyBag bag, IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            if (bag.SubGroups["AdvancedSettings"] == null)
            {
                bag.SubGroups["AdvancedSettings"] = new PropertyBag();
            }
            AdvancedSettingsBag = bag.SubGroups["AdvancedSettings"];
        }

        protected override string DefaultParserPluginId
        {
            get { return "Xml"; }
        }

        public override bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(Topic) &&
                       ((ConfigurationType == ConfigurationType.ConfigurationFile
                         && !string.IsNullOrEmpty(ConfigFile)) ||
                        ConfigurationType ==
                        ConfigurationType.ManualConfiguration);
            }
        }

        public ConfigurationType ConfigurationType
        {
            get
            {
                string val = GetInternal("ConfigurationType");
                UMSPlugin.ConfigurationType result;
                UMSPlugin.ConfigurationType.TryParse(val, out result);

                return result;
            }
            set { SetInternal("ConfigurationType", value.ToString()); }
        }

        public TopicResolutionMode TopicResolutionMode
        {
            get
            {
                string val = GetInternal("TopicResolutionMode");
                UMSPlugin.TopicResolutionMode result;
                UMSPlugin.TopicResolutionMode.TryParse(val, out result);

                return result;
            }
            set { SetInternal("TopicResolutionMode", value.ToString()); }
        }

        public ReceptionMode ReceptionMode
        {
            get
            {
                string val = GetInternal("ReceptionMode");
                UMSPlugin.ReceptionMode result;
                UMSPlugin.ReceptionMode.TryParse(val, out result);

                return result;
            }
            set { SetInternal("ReceptionMode", value.ToString()); }
        }

        public string ConfigFile
        {
            get { return GetInternal("ConfigFile"); }
            set { SetInternal("ConfigFile", value); }
        }

        public string MulticastGroup
        {
            get { return GetInternal("MulticastGroup"); }
            set { SetInternal("MulticastGroup", value); }
        }

        public string MulticastDestinationPort
        {
            get { return GetInternal("DestinationPort"); }
            set { SetInternal("DestinationPort", value); }
        }

        public bool IsTopicWildcard
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsTopicWildcard",
                    false.ToString()));
            }
            set { SetInternal("IsTopicWildcard", value.ToString()); }
        }

        public string LocalNetworkInterface
        {
            get { return GetInternal("LocalNetworkInterface"); }
            set { SetInternal("LocalNetworkInterface", value); }
        }
        
        public string PersistentSessionId
        {
            get { return GetInternal("PersistentSessionId"); }
            set { SetInternal("PersistentSessionId", value); }
        }

        public string UlbReceiverTypeId
        {
            get { return GetInternal("UlbReceiverTypeId"); }
            set { SetInternal("UlbReceiverTypeId", value); }
        }

        public string UnicastResolverDaemons
        {
            get { return GetInternal("UnicastResolverDaemons"); }
            set { SetInternal("UnicastResolverDaemons", value); }
        }

        public PropertyBag AdvancedSettingsBag { get; set; }

        public List<UnicastResolverDaemon> LoadResolverDaemons()
        {
            var resolverDaemons = new List<UnicastResolverDaemon>();

            if (!string.IsNullOrEmpty(UnicastResolverDaemons))
            {
                var resolverDaemonStrings =
                    UnicastResolverDaemons.Split(';');
                foreach (var daemon in resolverDaemonStrings)
                {
                    var daemonParts = daemon.Split(':');
                    var ip = daemonParts[0];
                    var port = daemonParts[1];

                    resolverDaemons.Add(new UnicastResolverDaemon(ip, port));
                }
            }
            return resolverDaemons;
        }

        public List<AdvancedSetting> LoadAdvancedSettings()
        {
            var advancedSettings = new List<AdvancedSetting>();

            foreach (var setting in AdvancedSettingsBag.Values)
            {
                var settingParts =
                    setting.Value.Split(new string[] { AdvancedSetting.Delimiter },
                        StringSplitOptions.None);
                var advancedSEtting = new AdvancedSetting(settingParts[0],
                    settingParts[1], settingParts[2]);

                advancedSettings.Add(advancedSEtting);
            }

            return advancedSettings;
        }
        
        public string ParameterizeValue(string value)
        {
            if (Parameters == null || String.IsNullOrEmpty(value))
            {
                return value;
            }
            return DataUtils.ApplyParameters(value, Parameters);
        }

        public override void DoGenerateColumns()
        {
            StreamingColumnGenerator columnGenerator = 
                new StreamingColumnGenerator(this, this.errorReporter);

            // Set up connection to ActiveMQ
            UMSListener listener = new UMSListener(columnGenerator);
            listener.Connect(this);
            // Subscribe
            string topicName = UMSDataAdapter.ParseTopic(Topic);
            topicName = DataUtils.ApplyParameters(topicName,
                Parameters);
            listener.ListenTo(topicName, this);

            // Start receiving data to generate columns
            columnGenerator.Generate();

            try
            {
                listener.Disconnect(this);
            }
            catch (Exception e)
            {
                Log.Error(e.Message);
            }
        }


        public override bool CanGenerateColumns()
        {
            return !string.IsNullOrEmpty(Topic);
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }
    }
}