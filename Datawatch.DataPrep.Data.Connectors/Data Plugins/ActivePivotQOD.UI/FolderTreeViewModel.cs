﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    public class FolderTreeViewModel : ActivePivotMetaDataTreeViewModel
    {
        public FolderTreeViewModel(ActivePivotMetaDataTreeViewModel parent, 
            string title, PropertyBag selectedItems) 
            : base(parent, title, selectedItems)
        {
        }

        public override string Image 
        {
            get
            {
                return IsExpanded ?
                    "/Panopticon.ActivePivot.QODPlugin.UI;component/Images/Folder_open.png" :
                    "/Panopticon.ActivePivot.QODPlugin.UI;component/Images/Folder_closed.png";
            }
            set
            {
                
            } 
        }

        protected override string UniqueName
        {
            get { return null; }
        }

        public override bool IsExpanded
        {
            get { return base.IsExpanded; }
            set
            {
                base.IsExpanded = value;
                OnPropertyChanged("Image");
            }
        }
    }
}
