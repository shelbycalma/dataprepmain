﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, ActivePivotConnectionSettings>
    {
        private const string ImageUri = "pack://application:,,,/" +
            "Panopticon.AnalysisServicesPlugin.UI;component/mdx-plugin.gif";

        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(ImageUri, UriKind.Absolute));
            }
            catch (Exception) { }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            ActivePivotConnectionSettings connection = new ActivePivotConnectionSettings();
            ActivePivotConnectionSettingsViewModel connectionViewModel =
                new ActivePivotConnectionSettingsViewModel(connection, parameters);
            MDXConnectionWindow connectionWindow =
                new MDXConnectionWindow(connectionViewModel);
            connectionWindow.Owner = owner;

            bool? result = connectionWindow.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            return connection.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag connectionBag, IEnumerable<ParameterValue> parameters)
        {
            ActivePivotConnectionSettings connection =
                new ActivePivotConnectionSettings(connectionBag);
            ActivePivotConnectionSettingsViewModel connectionViewModel =
                new ActivePivotConnectionSettingsViewModel(connection, parameters);

            ActivePivotConnectionPanel connectionPanel = new ActivePivotConnectionPanel();
            connectionPanel.Settings = connectionViewModel;

            return connectionPanel;
        }

        public override PropertyBag GetSetting(object element)
        {
            ActivePivotConnectionPanel connectionPanel = (ActivePivotConnectionPanel)element;

            ActivePivotConnectionSettingsViewModel connectionViewModel =
                connectionPanel.Settings;
            ActivePivotConnectionSettings connection = connectionViewModel.Model;

            return connection.ToPropertyBag();
        }
    }
}
