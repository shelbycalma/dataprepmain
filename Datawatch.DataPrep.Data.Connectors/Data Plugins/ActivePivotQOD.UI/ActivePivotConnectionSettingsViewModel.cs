﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.ActivePivot.QODPlugin.XMLA;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    public class ActivePivotConnectionSettingsViewModel : ViewModelBase
    {
        private readonly ActivePivotConnectionSettings model;
        private readonly ObservableCollection<ParameterValue> parameters;
        private ObservableCollection<string> _catalogNames;
        private ObservableCollection<string> _cubes;
        private ActivePivotMetaDataTreeViewModel metaDataTree;
        private bool isConnecting;
        private DelegateCommand connectCommand;
        private string _connectionStatus;

        public ActivePivotConnectionSettingsViewModel(
            ActivePivotConnectionSettings model,
            IEnumerable<ParameterValue> parameters) 
        {
            if (model == null) {
                throw Exceptions.ArgumentNull("model");
            }

            this.model = model;
            this.parameters = new ObservableCollection<ParameterValue>();
            if (parameters != null) {
                foreach (ParameterValue parameter in parameters) {
                    this.parameters.Add(parameter);
                }
            }
            
            connectCommand = new DelegateCommand(async () => await UpdateCatalogsBackground().ConfigureAwait(false));

            if (CatalogName != null)
            {
                UpdateCatalogsBackground(false);
            }
        }

        #region Properties
        public ObservableCollection<string> CatalogNames
        {
            get { return _catalogNames; }
            private set
            {
                _catalogNames = value;
                OnPropertyChanged("CatalogNames");
            }
        }

        public string CatalogName
        {
            get { return model.CatalogName; }
            private set
            {
                if (model.CatalogName == value) return;
                model.CatalogName = value;
                OnPropertyChanged("CatalogName");
            }
        }

        

        public bool IsUsernamePasswordVisible
        {
            get { return true;}
        }

        public string UserName
        {
            get { return model.UserName; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                model.UserName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get { return model.Password; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                model.Password = value; 
                OnPropertyChanged("Password");
            }
        }

        public bool IsAggregated
        {
            get { return model.IsAggregated; }
            set {
                if (value != model.IsAggregated) {
                    model.IsAggregated = value;
                    OnPropertyChanged("IsAggregated");
                }
            }
        }

        public bool IsOk
        {
            get
            {
                return !string.IsNullOrWhiteSpace(model.CatalogName) &&
                       !string.IsNullOrWhiteSpace(model.CubeName) &&
                       !model.SelectedDimensionsAndMeasures.IsEmpty();
            }
        }

        public bool IsOnDemand
        {
            get { return model.IsOnDemand; }
            set {
                if (value != model.IsOnDemand) {
                    model.IsOnDemand = value;
                    OnPropertyChanged("IsOnDemand");
                }
            }
        }

        public bool IsUseSsl
        {
            get { return model.UseSsl; }
            set
            {
                if (value != model.UseSsl)
                {
                    model.UseSsl = value;
                    OnPropertyChanged("IsUseSsl");
                }
            }
        }

        public ActivePivotConnectionSettings Model
        {
            get { return model; }
        }

        public ObservableCollection<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public ObservableCollection<string> CubeNames
        {
            get { return _cubes; }
            set 
            {
                _cubes = value;
                
                OnPropertyChanged("CubeNames");
            }
        }

        public string CubeName
        {
            get { return model.CubeName; }
            set
            {
                if (model.CubeName != value)
                {
                    model.CubeName = value;
                    OnPropertyChanged("CubeName");
                }
            }
        }

        public string ServerName
        {
            get { return model.ServerName; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                if (model.ServerName != value)
                {
                    model.ServerName = value;
                }
            }
        }

        public bool IsConnecting
        {
            get { return isConnecting; }
            set
            {
                isConnecting = value;
                OnPropertyChanged("IsConnecting");
            }
        }

        public ActivePivotMetaDataTreeViewModel MetaDataTree
        {
            get { return metaDataTree; }
            set
            {
                metaDataTree = value;
                OnPropertyChanged("MetaDataTree");
            }
        }

        public DelegateCommand ConnectCommand
        {
            get { return connectCommand; }
        }

        public string ConnectionStatus
        {
            get
            {
                return _connectionStatus;
            }

            set
            {
                _connectionStatus = value;
                OnPropertyChanged("ConnectionStatus");
            }
        }

        public async Task UpdateCatalogNames(IEnumerable<string> catalogNames)
        {
            CatalogNames = new ObservableCollection<string>(catalogNames);
            if (CatalogNames != null && CatalogNames.Count > 0 &&
                    string.IsNullOrEmpty(CatalogName))
            {
                await UpdateCatalogName(_catalogNames[0]).ConfigureAwait(false);
            }
            else
            {
                await UpdateCubesBackground().ConfigureAwait(false);
            }
        }

        public async Task UpdateCatalogName(string name)
        {
            CatalogName = name;
            if (CatalogName != null)
            {
                await UpdateCubesBackground().ConfigureAwait(false);
            }
        }
        #endregion

        #region Actions
        public async Task UpdateCubeNames(IEnumerable<string> value)
        {
            CubeNames = new ObservableCollection<string>(value);
            if (CubeNames != null && CubeNames.Count > 0 &&
                    (string.IsNullOrEmpty(CubeName)
                    || !CubeNames.Contains(CubeName)))
            {
                await UpdateCubeName(_cubes[0]).ConfigureAwait(false);
            }
            else
            {
                await UpdateDimensionsAndMeasures().ConfigureAwait(false);
            }
        }

        public async Task UpdateCubeName(string value)
        {
            CubeName = value;
            ResetDimentionsAndMeasures();
            await UpdateDimensionsAndMeasures().ConfigureAwait(false);
        }

        public Task UpdateCatalogsBackground(bool reset = true)
        {
            if (reset)
            {
                Reset();
            }
            return DoUpdateCatalogs();
        }

        private async Task DoUpdateCatalogs()
        {
            try
            {
                IsConnecting = true;
                ConnectionStatus = Properties.Resources.LogUpdatingCatalogs;
                Log.Info(Properties.Resources.LogUpdatingCatalogs);
                XMLAProvider provider = new XMLAProvider(this.Parameters);
                Log.Info(Properties.Resources.LogUpdatingCatalogs);
                var catalogNames = await Task.Run(() => provider.GetCatalogs(model)).ConfigureAwait(false);
                await UpdateCatalogNames(catalogNames).ConfigureAwait(false);
                ConnectionStatus = Properties.Resources.UiStatusReady;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                ConnectionStatus = Properties.Resources.UiStatusFailed;
                MessageBox.Show(string.Format(Properties.Resources.ExUpdatingCatalogs,
                    e.Message), Properties.Resources.ExError);
            }
            finally
            {
                IsConnecting = false;
            }
        }

        public async Task UpdateCubesBackground()
        {
            IsConnecting = true;
            try
            {
                ConnectionStatus = Properties.Resources.LogFetchingCubes;
                var cubes = await DoUpdateCubes().ConfigureAwait(false);
                await UpdateCubeNames(cubes).ConfigureAwait(false);
                ConnectionStatus = Properties.Resources.UiStatusReady;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                ConnectionStatus = Properties.Resources.UiStatusFailed;
                MessageBox.Show(string.Format(Properties.Resources.ExFailedToUpdateCubes,
                    e.Message), Properties.Resources.ExError);
            }
            finally
            {

                IsConnecting = false;
            }
        }

        private async Task<IList<string>> DoUpdateCubes()
        {
            XMLAProvider provider = new XMLAProvider(Parameters);
            List<string> cubes = new List<string>();
            await Task.Run(
                () =>
                {
                    using (AdomdConnection connection =
                       (AdomdConnection)provider.Connect(model))
                    {

                        Log.Info(Properties.Resources.LogFetchingCubes);
                        foreach (CubeDef cube in connection.Cubes)
                        {
                            if (cube.Type == CubeType.Cube)
                            {
                                cubes.Add(cube.Name);
                            }
                        }
                        
                    }
                }).ConfigureAwait(false);
            return cubes;
        }

        private async Task UpdateDimensionsAndMeasures()
        {
            ActivePivotMetaDataTreeViewModel meta = null;
            IsConnecting = true;
            if (string.IsNullOrEmpty(CubeName))
            {
                return;
            }
            try
            {
                meta = await Task.Run(() => DoUpdateDimensionsAndMeasures()).ConfigureAwait(false);
                ConnectionStatus = Properties.Resources
                        .UiStatusReady;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                MessageBox.Show(
                    string.Format(Properties.
                    Resources.ExFailedToUpdateDimentions, e.Message)
                    , Properties.Resources.ExError);
                ConnectionStatus = Properties.
                    Resources.UiStatusFailed;
            }
            finally
            {
                IsConnecting = false;
                MetaDataTree = meta;
            }
        }

        private ActivePivotMetaDataTreeViewModel DoUpdateDimensionsAndMeasures()
        {
            ActivePivotMetaDataTreeViewModel meta = null;

            XMLAProvider provider = new XMLAProvider(Parameters);
            using (AdomdConnection connection =
                (AdomdConnection)provider.Connect(model))
            {
                Log.Info(Properties.Resources
                    .LogUpdatingDimensionsAndMeasures);
                ConnectionStatus = Properties.Resources
                    .LogUpdatingDimensionsAndMeasures;
                CubeDef cube = null;
                foreach (CubeDef c in connection.Cubes)
                {
                    if (c.Name != CubeName) continue;
                    cube = c;
                    break;
                }
                if (cube == null)
                {
                    return null;
                }

                meta = new ActivePivotMetaDataTreeViewModel(cube,
                    model.SelectedDimensionsAndMeasures);
                return meta;
            }
        }

        private void Reset()
        {
            if (CatalogNames != null)
            {
                CatalogNames.Clear();
            }
            if (CubeNames != null)
            {
                CubeNames.Clear();
            }
            ResetDimentionsAndMeasures();
            CatalogName = null;
            CubeName = null;
        }

        private void ResetDimentionsAndMeasures()
        {
            MetaDataTree = null;
            Model.SelectedDimensionsAndMeasures = null;
        }
        #endregion
    }
}
