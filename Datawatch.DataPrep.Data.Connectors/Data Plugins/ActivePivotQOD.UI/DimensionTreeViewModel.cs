﻿using Datawatch.DataPrep.Data.Framework.Model;
using Dimension = Microsoft.AnalysisServices.AdomdClient.Dimension;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    public class DimensionTreeViewModel : ActivePivotMetaDataTreeViewModel
    {
        private readonly Hierarchy hierarchy;

        public DimensionTreeViewModel(ActivePivotMetaDataTreeViewModel parent, 
            Dimension dimension, PropertyBag selectedItems) 
            : this(parent, dimension.Caption, selectedItems)
        {
            foreach (Hierarchy h in dimension.Hierarchies)
            {
                ActivePivotMetaDataTreeViewModel node = 
                    string.IsNullOrEmpty(h.DisplayFolder)
                    ? this
                    : GetFolder(h.DisplayFolder);
                node.Children.Add(new DimensionTreeViewModel(this, h, 
                    selectedItems));
            }
        }

        private DimensionTreeViewModel(ActivePivotMetaDataTreeViewModel parent, 
            Hierarchy hierarchy, PropertyBag selectedItems) 
            : this(parent, hierarchy.Caption, selectedItems)
        {
            this.hierarchy = hierarchy;
        }

        private DimensionTreeViewModel(ActivePivotMetaDataTreeViewModel parent,
            string title, PropertyBag selectedItems) 
            : base(parent, title, selectedItems)
        {
            Image = "/Panopticon.ActivePivot.QODPlugin.UI;component/Images/Dimension.png";
        }

        protected override string UniqueName
        {
            get
            {
                if (hierarchy == null)
                {
                    return null;
                }
                return hierarchy.UniqueName;
            }
        }
    }
}
