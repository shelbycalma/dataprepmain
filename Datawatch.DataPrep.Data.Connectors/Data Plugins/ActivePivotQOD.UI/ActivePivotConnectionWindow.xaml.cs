﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    partial class MDXConnectionWindow : Window
    {
        public static RoutedCommand OkCommand =
            new RoutedCommand("Ok", typeof(MDXConnectionWindow));

        private readonly ActivePivotConnectionSettingsViewModel settings;

        public MDXConnectionWindow(ActivePivotConnectionSettingsViewModel settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public ActivePivotConnectionSettingsViewModel Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings != null && settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }

   
}
