﻿<UserControl
    x:Class="Panopticon.ActivePivot.QODPlugin.UI.ActivePivotConnectionPanel"
    x:ClassModifier="internal"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    xmlns:properties="clr-namespace:Panopticon.ActivePivot.QODPlugin.UI.Properties"
    xmlns:help="clr-namespace:Datawatch.DataPrep.Data.Core.Help;assembly=Datawatch.DataPrep.Data.Core"
    xmlns:converters="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Converters;assembly=Datawatch.DataPrep.Data.Core.UI"
    xmlns:local="clr-namespace:Panopticon.ActivePivot.QODPlugin.UI"
    xmlns:System="clr-namespace:System;assembly=mscorlib"
    help:DataHelpProvider.HelpKey="MDX"
    mc:Ignorable="d"
    d:DesignWidth="350" d:DesignHeight="400"
    DataContext="{Binding Settings, RelativeSource={x:Static RelativeSource.Self}}">

    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/ResourceDictionaries/CommonStyles.xaml"/>
            </ResourceDictionary.MergedDictionaries>
            <HierarchicalDataTemplate 
                x:Key="MetaDataTemplate" 
                DataType="{x:Type local:ActivePivotMetaDataTreeViewModel}"
                ItemsSource="{Binding Children}" >
                <StackPanel Orientation="Horizontal" UseLayoutRounding="True">
                    <Image Source="{Binding Image}" Stretch="None" SnapsToDevicePixels="True"/>
                    <CheckBox 
                        Content="{Binding Title}" 
                        IsChecked="{Binding IsSelected}"/>
                </StackPanel>
            </HierarchicalDataTemplate>

            <BooleanToVisibilityConverter x:Key="BooleanToVisibilityConverter" />
            <converters:EnumToLocalizedConverter 
                x:Key="EnumToLocalizedConverter"
                ResourceManager="{x:Static properties:Resources.ResourceManager}"/>
            <converters:VisibilityToBoolConverter x:Key="HiddenConverter"
                Inverted="True" 
                InvisibleValue="Hidden"/>
            <converters:InverseBooleanConverter x:Key="InverseBooleanConverter"/>
            <Style TargetType="{x:Type TreeViewItem}">
                <Setter Property="IsExpanded" Value="{Binding IsExpanded, Mode=TwoWay}" />
                <Setter Property="IsSelected" Value="{Binding IsSelectedDummy, Mode=TwoWay}" />
                <Setter Property="Padding" Value="0"/>
                <Setter Property="Margin" Value="0"/>
            </Style>
            <Style TargetType="{x:Type Label}" BasedOn="{StaticResource CommonLabelStyle}"/>
            <Style TargetType="{x:Type TextBox}" BasedOn="{StaticResource CommonTextBoxStyle}"/>
            <Style TargetType="{x:Type Button}" BasedOn="{StaticResource CommonButtonStyle}" />
            <Style TargetType="{x:Type ComboBox}" BasedOn="{StaticResource CommonComboBoxStyle}" />
            <Style TargetType="{x:Type PasswordBox}" BasedOn="{StaticResource CommonPasswordBoxStyle}" />
        </ResourceDictionary>
    </UserControl.Resources>

    <Grid IsEnabled="{Binding IsConnecting, Converter={StaticResource InverseBooleanConverter}}">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="*"/>
            <RowDefinition Height="Auto"/>
        </Grid.RowDefinitions>
        <Label Content="{x:Static properties:Resources.UiHost}"/>
        <TextBox Grid.Column="3" Text="{Binding ServerName}"/>

        <Label Grid.Row="3" 
               Visibility="{Binding IsUsernamePasswordVisible, Converter={StaticResource BooleanToVisibilityConverter}}" 
               Content="{x:Static properties:Resources.UiUsername}" />
        <TextBox Grid.Row="3" Visibility="{Binding IsUsernamePasswordVisible, Converter={StaticResource BooleanToVisibilityConverter}}"
                 Grid.Column="1" Text="{Binding UserName}"/>

        <Label Grid.Row="4" 
               Visibility="{Binding IsUsernamePasswordVisible, Converter={StaticResource BooleanToVisibilityConverter}}" 
               Content="{x:Static properties:Resources.UiPassword}" />
        <PasswordBox x:Name="Password" Grid.Row="4" 
                Visibility="{Binding IsUsernamePasswordVisible, Converter={StaticResource BooleanToVisibilityConverter}}" 
                Grid.Column="1" LostFocus="PasswordBox_OnPasswordChanged" VerticalAlignment="Center" VerticalContentAlignment="Center"/>

        <Button Grid.Row="5" Content="{x:Static properties:Resources.UiConnect}" 
                Grid.ColumnSpan="2" HorizontalAlignment="Right"
                Command="{Binding ConnectCommand}"/>

        <Label Grid.Row="6" Content="{x:Static properties:Resources.UiCatalog}"/>
        <ComboBox 
            Grid.Column="1" 
            Grid.Row="6"
            SelectedItem="{Binding CatalogName, Mode=OneWay}"
            ItemsSource="{Binding CatalogNames}"
            SelectionChanged="CatalogName_SelectionChanged"/>

        <Label Grid.Row="7" Content="{x:Static properties:Resources.UiCube}"/>

        <ComboBox 
            Grid.Row="7" 
            Grid.Column="1" 
            SelectedItem="{Binding CubeName, Mode=OneWay}" 
            ItemsSource="{Binding CubeNames}"
            SelectionChanged="CubeName_SelectionChanged"/>
        <ScrollViewer
            Grid.Row="8" 
            Grid.ColumnSpan="2"
            Margin="5"
            HorizontalScrollBarVisibility="Hidden"
            VerticalScrollBarVisibility="Auto">
            <TreeView 
                ItemsSource="{Binding MetaDataTree.Children}"
                ItemTemplate="{StaticResource MetaDataTemplate}" />
        </ScrollViewer>
        <StatusBar Grid.Row="9" Grid.ColumnSpan="2" Margin="5,0,5,0" DockPanel.Dock="Bottom">
            <StatusBar.ItemsPanel>
                <ItemsPanelTemplate>
                    <Grid>
                        <Grid.RowDefinitions>
                            <RowDefinition Height="*"/>
                        </Grid.RowDefinitions>
                        <Grid.ColumnDefinitions>
                            <ColumnDefinition Width="Auto"/>
                            <ColumnDefinition Width="*"/>
                        </Grid.ColumnDefinitions>
                    </Grid>
                </ItemsPanelTemplate>
            </StatusBar.ItemsPanel>
            <StatusBarItem Grid.Column="0">
                <TextBlock x:Name="ConnectionStatus" Text="{Binding ConnectionStatus}"/>
            </StatusBarItem>
            <StatusBarItem Grid.Column="1" HorizontalAlignment="Right">
                <ProgressBar IsIndeterminate="{Binding IsConnecting}" Width="72" Height="16"/>
            </StatusBarItem>
        </StatusBar>
    </Grid>

</UserControl>
