﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    internal partial class ActivePivotConnectionPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(ActivePivotConnectionSettingsViewModel),
                typeof(ActivePivotConnectionPanel),
                new PropertyMetadata(Settings_Changed));

        public ActivePivotConnectionPanel()
        {
            InitializeComponent();
        }

        public ActivePivotConnectionSettingsViewModel Settings
        {
            get {
                return (ActivePivotConnectionSettingsViewModel)
                    GetValue(SettingsProperty);
            }
            set {
                SetValue(SettingsProperty, value);
            }
        }

        private static void Settings_Changed(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((ActivePivotConnectionPanel) obj).OnSettingsChanged(
                (ActivePivotConnectionSettingsViewModel) e.OldValue,
                (ActivePivotConnectionSettingsViewModel) e.NewValue);
        }

        protected virtual void OnSettingsChanged(
            ActivePivotConnectionSettingsViewModel oldSettings,
            ActivePivotConnectionSettingsViewModel newSettings)
        {
            this.DataContext = newSettings;
            Password.Password = newSettings.Password;
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox senderPasswordBox = sender as PasswordBox;
            if (senderPasswordBox != null)
            {
                Settings.Password = senderPasswordBox.Password;
            }
        }

        private async void CubeName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var box = (ComboBox)sender;
            if (!Settings.IsConnecting)
            {
                await Settings.UpdateCubeName(box.SelectedItem as string).ConfigureAwait(false);
            }
        }

        private async void CatalogName_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var box = (ComboBox)sender;
            if (!Settings.IsConnecting)
            {
                await Settings.UpdateCatalogName(box.SelectedItem as string).ConfigureAwait(false);
            }
        }
    }
}
