﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Microsoft.AnalysisServices.AdomdClient;

namespace Panopticon.ActivePivot.QODPlugin.UI
{
    public class MeasureTreeViewModel : ActivePivotMetaDataTreeViewModel
    {
        private readonly Measure measure;

        public MeasureTreeViewModel(ActivePivotMetaDataTreeViewModel parent,
            CubeDef cube, PropertyBag selectedItems)
            : this(parent, Properties.Resources.UiMeasures, selectedItems)
        {
            foreach (Measure measure in cube.Measures)
            {
                string group = Convert.ToString(measure.Properties["MEASUREGROUP_NAME"].
                        Value);
                if (!string.IsNullOrEmpty(group))
                {
                    GetFolder(group).Children.Add(
                        new MeasureTreeViewModel(this, measure, selectedItems));
                }
                else
                {
                    Children.Add(new MeasureTreeViewModel(this, measure,
                        selectedItems));
                }
            }
        }

        private MeasureTreeViewModel(MeasureTreeViewModel parent, Measure measure, PropertyBag selectedItems) : 
            this(parent, measure.Caption, selectedItems)
        {
            this.measure = measure;
        }

        private MeasureTreeViewModel(ActivePivotMetaDataTreeViewModel parent, 
            string folderName, PropertyBag selectedItems) : 
            base(parent, folderName, selectedItems)
        {
            Image = "/Panopticon.ActivePivot.QODPlugin.UI;component/Images/Measure.png";
        }

        public override string Title
        {
            get { return title; }
        }

        protected override string UniqueName
        {
            get
            {
                if (measure == null)
                {
                    return null;
                }
                return measure.UniqueName;
            }
        }
    }
}
