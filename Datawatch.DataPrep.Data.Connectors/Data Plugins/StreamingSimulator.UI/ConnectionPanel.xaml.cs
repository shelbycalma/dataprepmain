﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.StreamingSimulatorPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private static readonly string FileFilter = 
            string.Concat(Properties.Resources.UiDialogCsvFilesFilter, "|*.csv|", 
            Properties.Resources.UiDialogTextFilesFilter, "|*.txt");
        private StreamingSimulatorSettings settings;
        private readonly Window owner;
        public PropertyBag GlobalSettings { get; set; }

        public ConnectionPanel()
            : this(null, null)
        {
        }

        public ConnectionPanel(StreamingSimulatorSettings settings, Window owner)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            this.owner = owner;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        public StreamingSimulatorSettings Settings
        {
            get { return settings; }
        }

        protected void OnConnectionChanged(StreamingSimulatorSettings newConnection)
        {
            DataContext = newConnection;

            if (newConnection != null)
            {
                PasswordBox.Password = newConnection.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as StreamingSimulatorSettings;
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = DataPluginUtils.BrowseForFile(owner, Settings.FilePath,
                FileFilter, GlobalSettings);
            if (path != null)
            {
                Settings.FilePath = path;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }



    }
}
