﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.StreamingSimulatorPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<StreamingSimulatorTable, StreamingSimulatorSettings,
        Dictionary<string, object>, StreamingSimulatorAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner,
            "pack://application:,,,/Panopticon.StreamingSimulator.UI;component/blank16.png")
        {
        }

        public override Window CreateConnectionWindow(StreamingSimulatorSettings settings)
        {
            return new ConnectionWindow(settings);
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new StreamingSimulatorSettings(Plugin.PluginManager, bag, parameters), owner);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }
    }
}
