﻿using System;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.SplunkPlugin
{
    public class SplunkSettings : ConnectionSettings
    {
        private readonly TimeZoneHelper timeZoneHelper;

        public SplunkSettings()
            : this(new PropertyBag())
        {}

        public SplunkSettings(PropertyBag bag)
            : base(bag)
        {
            PropertyBag timeZoneHelperBag = bag.SubGroups["TimeZone"];
            if (timeZoneHelperBag == null)
            {
                timeZoneHelperBag = new PropertyBag();
                bag.SubGroups["TimeZone"] = timeZoneHelperBag;
            }

            timeZoneHelper = new TimeZoneHelper(timeZoneHelperBag);

            //set default Port number
            Port = "8089";
        }

        public string Host
        {
            get { return GetInternal("host", "localhost"); }
            set { SetInternal("host", value); }
        }

        public string Port
        {
            get { return GetInternal("port", "8089"); }
            set { SetInternal("port", value); }
        }

        public string App
        {
            get { return GetInternal("app"); }
            set { SetInternal("app", value); }
        }

        public string Username
        {
            get { return GetInternal("username"); }
            set { SetInternal("username", value); }
        }

        public string Password
        {
            get { return GetInternal("password"); }
            set { SetInternal("password", value); }
        }

        public string SearchQuery
        {
            get { return GetInternal("searchQuery"); }
            set { SetInternal("searchQuery", value); }
        }

        public string SavedSearch
        {
            get { return GetInternal("savedsearch"); }
            set {SetInternal("savedsearch", value); }
        }

        public SearchType SearchType
        {
            get
            {
                string value = GetInternal("searchtype");
                if (value != null)
                {
                    try
                    {
                        return (SearchType)Enum.Parse(
                            typeof(SearchType), value);
                    } catch {}
                }

                return SearchType.SavedSearch;
            }
            set { SetInternal("searchtype", value.ToString()); }
        }

        public bool EncloseParameterInQuote
        {
            get
            {
                string value = GetInternal("encloseParameterInQuote", false.ToString());
                return !string.IsNullOrEmpty(value) && Convert.ToBoolean(value);
            }
            set { SetInternal("encloseParameterInQuote", Convert.ToString(value)); }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return timeZoneHelper; }
        }
    }
}
