﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Splunk;

namespace Panopticon.SplunkPlugin
{
    internal static class SplunkTableBuilder
    {
        public static StandaloneTable CreateTable(Stream stream, TimeZoneHelper timeZoneHelper)
        {
            StandaloneTable table = new StandaloneTable();

            if (stream == null)
            {
                return table;
            }

            table.BeginUpdate();
            try
            {
                using (ResultsReaderXml resultsReader = new ResultsReaderXml(stream))
                {
                    IList<Event> rows = resultsReader.ToList();
                    IDictionary<string,ColumnType> columns = CreateColunms(table, resultsReader.Fields, rows);
                    foreach (Event row in rows)
                    {
                        object[] values = new object[table.ColumnCount];

                        for (int i = 0; i < table.ColumnCount; i++ )
                        {
                            string columnName = table.GetColumn(i).Name;
                            values[i] = row.ContainsKey(columnName) ?
                                GetValue(row[columnName], columns[columnName], timeZoneHelper)
                                : null;
                        }

                        table.AddRow(values);
                    }
                }

                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }

        private static IDictionary<string,ColumnType> CreateColunms(StandaloneTable table, IEnumerable<string> columnNames, IList<Event> rows)
        {
            IDictionary<string, ColumnType> result = new Dictionary<string, ColumnType>();

            foreach (var columnName in columnNames)
            {
                IList<string> columnValues = new List<string>();
                foreach (Event row in rows)
                {
                    if (row.ContainsKey(columnName))
                    {
                        columnValues.Add(row[columnName]);
                    }
                }

                ColumnType columnType = GetColumnType(columnValues);

                switch (columnType)
                {
                    case ColumnType.Text:
                        table.AddColumn(new TextColumn(columnName));
                        break;
                    case ColumnType.Numeric:
                        table.AddColumn(new NumericColumn(columnName));
                        break;
                    case ColumnType.Time:
                        table.AddColumn(new TimeColumn(columnName));
                        break;
                }

                result.Add(columnName, columnType);
            }
            
            return result;
        }

        private static ColumnType GetColumnType(IEnumerable<string> columnValues)
        {
            ColumnType previousCellType = ColumnType.Text;
            bool isFirstRow = true;
            foreach (string value in columnValues)
            {
                ColumnType currentCellType = GetCellType(value);
                if (currentCellType == ColumnType.Text)
                {
                    return ColumnType.Text;    
                }

                if (isFirstRow)
                {
                    isFirstRow = false;
                }
                else if (currentCellType != previousCellType)
                {
                    return ColumnType.Text;
                }

                previousCellType = currentCellType;
            }

            return previousCellType;
        }

        private static ColumnType GetCellType(string value)
        {
            double doubleValue;
            if (double.TryParse(value, NumberStyles.Any, CultureInfo.InvariantCulture, out doubleValue))
            {
                return ColumnType.Numeric;
            }

            DateTime dateTimeValue;
            if (DateTime.TryParse(value, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out dateTimeValue))
            {
                return ColumnType.Time;
            }

            return ColumnType.Text;
        }

        private static object GetValue(string value, ColumnType columnType, TimeZoneHelper timeZoneHelper)
        {
            switch (columnType)
            {
                case ColumnType.Numeric:
                    return double.Parse(value, CultureInfo.InvariantCulture);
                case ColumnType.Time:
                    DateTime dateTimeValue = DateTime.Parse(value, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
                    return timeZoneHelper != null && timeZoneHelper.TimeZoneSelected ?
                        timeZoneHelper.ConvertFromUTC(dateTimeValue) : dateTimeValue;
                default:
                    return value;
            }
        }
    }
}
