﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Splunk;

namespace Panopticon.SplunkPlugin
{
    public class SplunkClient
    {
        private readonly Service service;
        private readonly SearchType searchType;
        private readonly string savedSearch;
        private readonly string searchQuery;
        private readonly TimeZoneHelper timeZoneHelper;
        private readonly string app;

        public SplunkClient(SplunkSettings settings, IEnumerable<ParameterValue> parameters)
        {
            string host = ApplyParameters(settings.Host, parameters, false);
            string username = ApplyParameters(settings.Username, parameters, false);
            string password = ApplyParameters(settings.Password, parameters, false);
            searchType = settings.SearchType;
            savedSearch = settings.SavedSearch;
            searchQuery = ApplyParameters(settings.SearchQuery, parameters, settings.EncloseParameterInQuote);
            app = settings.App;
            
            string portString = ApplyParameters(settings.Port, parameters, false);
            int port;
            if (!int.TryParse(portString, out port))
                throw new ArgumentException(Properties.Resources.ExInvalidPort);
            if (string.IsNullOrWhiteSpace(host))
                throw new ArgumentException(Properties.Resources.ExEmptyHost);
            if (string.IsNullOrWhiteSpace(username))
                throw new ArgumentException(Properties.Resources.ExEmptyUsername);
            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentException(Properties.Resources.ExEmptyPassword);

            timeZoneHelper = settings.TimeZoneHelper;

            ServiceArgs serviceArgs = new ServiceArgs
            {
                Host = host,
                Port = port,
                App = string.IsNullOrWhiteSpace(app) ? null : app
            };

            service = new Service(serviceArgs);
            service.Login(username, password);
        }

        public IList<string> GetApplicationList()
        {
            IList<string> result = new List<string>();
            ICollection<Application> applications = service.GetApplications().Values;
            foreach (Application application in applications)
            {
                if (!application.IsVisible) continue;
                result.Add(application.Name);
            }

            return result;
        }

        public IDictionary<string, string> GetSavedSearches()
        {
            IDictionary<string, string> result = new Dictionary<string, string>();
            ICollection<SavedSearch> savedSearches = service.GetSavedSearches().Values;
            foreach (SavedSearch savedSearchObject in savedSearches)
            {
                result.Add(savedSearchObject.Name, savedSearchObject.Search);
            }

            return result;
        }

        public StandaloneTable GetTable(int maxRows)
        {
            Log.Info(Properties.Resources.LogRecievingData, service.Host, service.Port, app, searchQuery);
            DateTime startTime = DateTime.Now;
            StandaloneTable table;
            using (Stream stream = GetSearchStream(maxRows))
            {
                table = SplunkTableBuilder.CreateTable(stream, timeZoneHelper);
            }
            DateTime endTime = DateTime.Now;

            Log.Info(Properties.Resources.LogRecievedData,
                table.RowCount,
                table.ColumnCount,
                (endTime - startTime).TotalSeconds,
                service.Host,
                service.Port,
                app,
                searchQuery);

            return table;
        }
        
        private Stream GetSearchStream(int maxRows)
        {
            if (maxRows == 0)
            {
                return null;
            }

            int count = maxRows;
            if (count == -1)
            {
                count = 0;
            }

            JobResultsArgs outArgs = new JobResultsArgs
            {
                OutputMode = JobResultsArgs.OutputModeEnum.Xml,
                Count = count
            };

            switch (searchType)
            {
                case SearchType.SavedSearch:
                    if(string.IsNullOrWhiteSpace(savedSearch))
                        throw new ArgumentException(Properties.Resources.ExEmptySavedSearch);
                    
                    SavedSearch savedSearchObject = service.GetSavedSearches().Get(savedSearch);
                    Job job = savedSearchObject.Dispatch();

                    //This comes from official documentation: http://dev.splunk.com/view/csharp-sdk/SP-CAAAEQF
                    while (!job.IsDone)
                    {
                        Thread.Sleep(100);
                    }
                   
                    return job.Results(outArgs);

                case SearchType.Manual:
                    return service.Oneshot(string.Format("search {0}", searchQuery), outArgs);
                default:
                    throw new NotSupportedException(string.Format(Properties.Resources.ExUnsupportedSearchType, searchType));
            }
        }

        private static string ApplyParameters(string source, IEnumerable<ParameterValue> parameters, bool encloseParametersInQuotes)
        {
            if (source == null)
                return null;

            if (parameters == null)
                return source;

            return new ParameterEncoder
            {
                SourceString = source,
                Parameters = parameters,
                NullOrEmptyString = "NULL",
                EnforceSingleQuoteEnclosure = encloseParametersInQuotes,
            }.Encoded();
        }
    }
}
