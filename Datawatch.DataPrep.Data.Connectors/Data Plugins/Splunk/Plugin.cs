﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.SplunkPlugin.Properties;

namespace Panopticon.SplunkPlugin
{
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DataPluginBase<SplunkSettings>
    {
        internal const string PluginId = "SplunkPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Splunk";
        internal const string PluginType = DataPluginTypes.Database;

        public override SplunkSettings CreateSettings(PropertyBag bag)
        {
            SplunkSettings splunkSettings = new SplunkSettings(bag);
            if (string.IsNullOrEmpty(splunkSettings.Title))
            {
                splunkSettings.Title = Resources.UiDefaultConnectionTitle;
            }

            return splunkSettings;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag propertyBag, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            CheckLicense();
            SplunkSettings splunkSettings = CreateSettings(propertyBag);

            SplunkClient client = new SplunkClient(splunkSettings, parameters);
            StandaloneTable table = client.GetTable(rowcount);

            return table;
        }
    }
}
