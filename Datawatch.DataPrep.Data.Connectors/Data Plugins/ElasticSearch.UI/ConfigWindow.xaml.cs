﻿using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.ElasticSearchPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        private readonly ElasticSearchSettings settings;

        /// <summary>
        /// Defines the Ok command, used by the Ok button.
        /// </summary>
        public static RoutedCommand OkCommand
           = new RoutedCommand("Ok", typeof(ConfigWindow));

        public ConfigWindow() : this(null, null)
        {
            
        }

        /// <summary>
        /// Creates a new ConfigWindow instance for a specific 
        /// ElasticSearchSettings instance.
        /// </summary>
        /// <param name="settings">The ElasticSearchSettings.</param>
        public ConfigWindow(ElasticSearchSettings settings, PropertyBag globalSettings)
        {
            this.settings = settings;
            InitializeComponent();
            ConfigPanel.GlobalSettings = globalSettings;
        }

        /// <summary>
        /// Gets the current AMPSSettings instance.
        /// </summary>
        public ElasticSearchSettings Settings
        {
            get
            {
                return settings;
            }

        }

        /// <summary>
        /// Checks if the Ok command can be executed. 
        /// If the ConfigPanel exists and it's IsOK property is true, the
        /// command can be executed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true; //settings.IsOK;
        }

        /// <summary>
        /// Executes the Ok command, setting the DialogResult to true.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            settings.ParserSettings.ClearColumnDefinitions();
            this.DialogResult = true;
        }
    }
}
