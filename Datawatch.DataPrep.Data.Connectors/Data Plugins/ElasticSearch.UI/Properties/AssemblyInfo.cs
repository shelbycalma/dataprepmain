﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.ElasticSearch.UI.dll")]
[assembly: AssemblyDescription("Datawatch ElasticSearch Plug-in UI")]

[assembly: PluginDescription("ElasticSearch", "Database")]
