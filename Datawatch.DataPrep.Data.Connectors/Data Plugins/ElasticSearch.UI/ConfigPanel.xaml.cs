﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.ElasticSearchPlugin.UI
{
    /// <summary>
    /// The ConfigPanel implementa a WPF UserControl allowing editing of data 
    /// connections for the streaming DataPlugin. The ConfigPanel is used  both 
    /// when a connection is created and when edited in the data source settings
    /// dialog.
    /// </summary>

    internal partial class ConfigPanel : UserControl
    {
        private ElasticSearchSettings settings;
        private Window owner;
        public PropertyBag GlobalSettings { get; set; }

        /// <summary>
        /// Creates a new instance of the ConfigPanel.
        /// </summary>
        public ConfigPanel() : this(null, null)
        {

        }

        public ConfigPanel(ElasticSearchSettings settings, Window owner)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            this.owner = owner;
            DataContextChanged +=
                UserControl_DataContextChanged;
            if (settings != null)
            {
                PasswordBox.Password = settings.WebPassword;
            }
        }

        /// <summary>
        /// Gets or sets the current JsonSettings instance.
        /// </summary>
        public ElasticSearchSettings Settings
        {
            get { return settings; }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as ElasticSearchSettings;
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.WebPassword = PasswordBox.Password;
            }
        }
    }

}
