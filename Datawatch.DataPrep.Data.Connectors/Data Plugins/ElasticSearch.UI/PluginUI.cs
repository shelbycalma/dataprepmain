﻿using System;
using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.ElasticSearchPlugin.UI
{
    public class PluginUI : TextPluginUIBase<ElasticSearchSettings, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner,
            "pack://application:,,,/Panopticon.ElasticSearchPlugin.UI;component/elasticsearch.png")
        {
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ElasticSearchSettings settings =
                new ElasticSearchSettings(Plugin.PluginManager, bag, parameters);
            return new ConfigPanel(settings, owner);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel conWindow = (ConfigPanel)obj;
            return conWindow.Settings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(ElasticSearchSettings settings)
        {
            return new ConfigWindow(settings, Plugin.GlobalSettings);
        }
        public override string[] FileExtensions
        {
            get { return null; }
        }

        public override string FileFilter
        {
            get
            {
                return null;
            }
        }

        public override string GetTitle(PropertyBag settings)
        {
            ElasticSearchSettings connectionSettings =
                new ElasticSearchSettings(Plugin.PluginManager, settings, null);

            if(!string.IsNullOrEmpty(connectionSettings.IndexName))
            {
                return connectionSettings.IndexName;
            }

            if (!string.IsNullOrEmpty(connectionSettings.Title))
            {
                return connectionSettings.Title;
            }

            return "<No title>";
        }
    }
}
