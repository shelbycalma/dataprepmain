﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.KDBPlugin.UI.Realtime
{
    public partial class ConnectionWindow : Window
    {
        public static RoutedCommand OkCommand =
            new RoutedCommand("OK", typeof(ConnectionWindow));

        private RealtimeKdbSettingsViewModel settings;

        public ConnectionWindow(RealtimeKdbSettingsViewModel settings)
        {
            this.settings = settings;

            InitializeComponent();

            cancelButton.Click += cancelButton_Click;
            ConfigPanel.Settings = settings;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
        
    }
}
