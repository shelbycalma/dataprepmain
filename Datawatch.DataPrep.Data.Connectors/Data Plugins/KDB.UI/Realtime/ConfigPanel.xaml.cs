﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.KDBPlugin.UI.Realtime
{
    public partial class ConfigPanel : UserControl, 
        IDataPluginConfigElement
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(RealtimeKdbSettingsViewModel), typeof(ConfigPanel),
                    new PropertyMetadata(new PropertyChangedCallback(
                        Settings_Changed)));
        
        public ConfigPanel()
        {
            InitializeComponent();
        }

        public RealtimeKdbSettingsViewModel Settings
        {
            get { return (RealtimeKdbSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
           DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (RealtimeKdbSettingsViewModel)args.OldValue,
                (RealtimeKdbSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(RealtimeKdbSettingsViewModel oldSettings,
            RealtimeKdbSettingsViewModel newSettings)
        {
            DataContext = newSettings;
        }

        public void OnOk()
        {
            if (Settings == null)
            {
                return;
            }

            // This method is called when Ok button in DataSourceSettingsDialog is clicked.
            // Clear the column here, so that even if settings are not chagned, latest schema from 
            // DB is pulled.
            Settings.Model.SchemaColumnsSettings.ClearSchemaColumns();
        }

        public bool IsOk
        {
            get
            {
                if (Settings == null) return false;

                return Settings.IsOk;
            }
        }
    }
}
