﻿using System.Windows;
using System.Windows.Controls;
using Panopticon.KDBPlugin.Realtime;

namespace Panopticon.KDBPlugin.UI.Realtime
{
    /// <summary>
    /// Interaction logic for HistoricConnectionPanel.xaml
    /// </summary>
    public partial class HistoricConnectionPanel : UserControl
    {
        public HistoricConnectionPanel(KdbHistoricSetting settings)
        {
            InitializeComponent();
            DataContext = settings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        public KdbHistoricSetting Settings
        {
            get { return DataContext as KdbHistoricSetting; }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}