﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.KDBPlugin.Realtime;

namespace Panopticon.KDBPlugin.UI.Realtime
{
    public class PluginUI : PluginUIBase<Plugin, RealtimeKdbSettings>
    {
        // The uri of the image displayed in the "Connect to data" dialog as
        // well as the data plug-in drop down.
        private const string imageuri =
            "pack://application:,,,/Panopticon.KDBPlugin.UI;component/ConnectImage.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
                //Set unicode encoding as default
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            RealtimeKdbSettings settings = Plugin.CreateConnectionSettings(bag);
            return new ConfigPanel()
            {
                Settings = CreateViewModel(settings, parameters)
            };
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            return cp.Settings.Model.ToPropertyBag();
        }

        // Called when the user selects this plug-in in the "Connect to data" 
        // dialog or picks it from the data plug-in dropdown. The DoConnect 
        // method is resposible for allowing the user to create a new data 
        // source connection.
        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            RealtimeKdbSettings settings =
                Plugin.CreateConnectionSettings(new PropertyBag());
            RealtimeKdbSettingsViewModel settingsView =
                CreateViewModel(settings, parameters);
            ConnectionWindow window = new ConnectionWindow(settingsView);
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return settings.ToPropertyBag();
            }
            return null;
        }

        public virtual RealtimeKdbSettingsViewModel CreateViewModel(RealtimeKdbSettings settings, 
            IEnumerable<ParameterValue> parameters)
        {
            return new RealtimeKdbSettingsViewModel(settings, parameters);
        }        
    }
}
