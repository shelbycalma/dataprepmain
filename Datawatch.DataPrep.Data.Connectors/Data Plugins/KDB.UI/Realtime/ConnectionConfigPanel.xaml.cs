﻿using System.Windows;
using System.Windows.Controls;
using Panopticon.KDBPlugin.Realtime;

namespace Panopticon.KDBPlugin.UI.Realtime
{
    /// <summary>
    /// Interaction logic for ConnectionConfigPanel.xaml
    /// </summary>
    public partial class ConnectionConfigPanel : UserControl
    {
        public ConnectionConfigPanel(RealtimeKdbSettings settings)
        {
            InitializeComponent();
            DataContext = settings;
            if (settings != null)
            {
                PasswordBox.Password = settings.Password;
            }
        }

        public RealtimeKdbSettings Settings
        {
            get { return DataContext as RealtimeKdbSettings; }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}