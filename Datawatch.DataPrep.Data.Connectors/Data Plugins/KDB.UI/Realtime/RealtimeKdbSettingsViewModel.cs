﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.KDBPlugin.Realtime;

namespace Panopticon.KDBPlugin.UI.Realtime
{
    public class RealtimeKdbSettingsViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private ICommand fetchSchemaCommand;

        public RealtimeKdbSettings Model { get; private set; }

        private IEnumerable<ParameterValue> parameters;
        private readonly ObservableCollection<string> symbolColumns = 
            new ObservableCollection<string>();

        private string[] symbolColumnCandidates;

        public RealtimeKdbSettingsViewModel(RealtimeKdbSettings model,
            IEnumerable<ParameterValue> parameters)
        {
            this.Model = model;
            this.parameters = parameters;
            symbolColumns.Add(RealtimeKdbSettings.IdSymbolColumn);
            if (Model.SymbolColumn != null)
            {
                SymbolColumnCandidates = new string[] { Model.SymbolColumn };
            }
        }
        
        private string[] SymbolColumnCandidates
        {
            get { return symbolColumnCandidates; }
            set
            {
                if (symbolColumnCandidates != value)
                {
                    symbolColumnCandidates = value;
                    UpdateSymbolColumns();
                }
            }
        }

        public ObservableCollection<string> SymbolColumns
        {
            get { return symbolColumns; }
        }
        
        public bool IsOk
        {
            get
            {
                return !(string.IsNullOrEmpty(Model.Host) ||
                    string.IsNullOrEmpty(Model.Port)
                    || string.IsNullOrEmpty(Model.Service) ||
                    string.IsNullOrEmpty(this.Model.IdColumn) ||
                    (this.Model.IsTimeIdColumnGenerated &&
                    string.IsNullOrEmpty(this.Model.TimeIdColumn))) &&
                    (Model.FunctionalSubscription || 
                        !string.IsNullOrEmpty(Model.Table));

            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get
            {
                return this.Model.TimeZoneHelper;
            }
        }

        public ICommand FetchSchemaCommand
        {
            get
            {
                if (fetchSchemaCommand == null)
                {
                    fetchSchemaCommand = new DelegateCommand(FetchSchema,
                        CanFetchSchema);
                }
                return fetchSchemaCommand;
            }
        }

        private void FetchSchema()
        {
            UpdateIdCandidates();
        }

        private bool CanFetchSchema()
        {
            return !(string.IsNullOrEmpty(Model.Host) ||
                    string.IsNullOrEmpty(Model.Port) ||
                    string.IsNullOrEmpty(Model.Service))&&
                    (Model.FunctionalSubscription || 
                      !string.IsNullOrEmpty(Model.Table));
        }

        private void NotifyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        internal void UpdateIdCandidates()
        {
            try
            {
                IQueryResult result = RealtimeKdbUtil.GetTickSchema(Model,
                    parameters);

                List<string> idCandidates = new List<string>();
                List<string> timeIdCandidates = new List<string>();

                string[] names = result.Names;

                for (int i = 0; i < names.Length; i++)
                {
                    string name = names[i];                    
                    Type type = result.GetFieldType(i);
                
                    if (type == typeof(string))
                    {
                        idCandidates.Add(name);
                    }
                    else if (type == typeof(DateTime) ||
                    type == typeof(c.Date) ||
                    type == typeof(TimeSpan) ||
                    type == typeof(c.KTimespan))
                    {
                        timeIdCandidates.Add(name);
                    }
                }

                this.Model.IdColumnCandidates =
                        idCandidates.Count > 0 ? idCandidates.ToArray() : null;
                this.Model.TimeIdColumnCandidates =
                    timeIdCandidates.Count > 0 ? timeIdCandidates.ToArray() : null;
                SymbolColumnCandidates = 
                    idCandidates.Count > 0 ? idCandidates.ToArray() : null;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,
                           Properties.Resources.UiWindowTitleRealtime,
                           MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        

        private void UpdateSymbolColumns()
        {
            string oldSymbolColumn = Model.SymbolColumn;

            while (symbolColumns.Count > 1)
            {
                symbolColumns.RemoveAt(symbolColumns.Count - 1);
            }

            bool found = false;

            if (SymbolColumnCandidates != null)
            {
                foreach (string s in SymbolColumnCandidates)
                {
                    if (symbolColumns.Contains(s)) continue;
                    found = found || s == oldSymbolColumn;
                    symbolColumns.Add(s);
                }
            }

            if (found)
            {
                Model.SymbolColumn = oldSymbolColumn;
            }
            else if (symbolColumns.Count > 0)
            {
                Model.SymbolColumn = symbolColumns[0];
            }
            else
            {
                Model.SymbolColumn = null;
            }
        }

        public virtual object ConnectionConfigElement
        {
            get
            {
                return new ConnectionConfigPanel(Model);
            }
        }

        public virtual object HistoricConnectionConfigElement
        {
            get
            {
                return new HistoricConnectionPanel(Model.HistoricSettings);
            }
        }
    }
}
