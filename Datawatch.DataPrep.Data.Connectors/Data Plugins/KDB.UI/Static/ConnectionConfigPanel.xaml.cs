﻿using System.Windows;
using System.Windows.Controls;
using Panopticon.KDBPlugin.Static;

namespace Panopticon.KDBPlugin.UI.Static
{
    /// <summary>
    /// Interaction logic for ConnectionConfigPanel.xaml
    /// </summary>
    public partial class ConnectionConfigPanel : UserControl
    {
        public ConnectionConfigPanel(KdbSettings settings)
        {
            InitializeComponent();
            DataContext = settings;
            if (settings != null)
            {
                PasswordBox.Password = settings.PasswordConfigurator;
            }
        }

        public KdbSettings Settings
        {
            get { return DataContext as KdbSettings; }
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }
    }
}