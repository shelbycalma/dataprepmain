﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;


namespace Panopticon.KDBPlugin.UI.Static
{
    public class AggregationPeriodTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (!(value is AggregationPeriodType))
            {
                return DependencyProperty.UnsetValue;
            }
            AggregationPeriodType authType = (AggregationPeriodType)value;

            switch (authType)
            {
                default:
                case AggregationPeriodType.second:
                    return Properties.Resources.UiAggregationPeriodSecond;
                case AggregationPeriodType.minute:
                    return Properties.Resources.UiAggregationPeriodMinute;
                case AggregationPeriodType.hh:
                    return Properties.Resources.UiAggregationPeriodHour;
                case AggregationPeriodType.date:
                    return Properties.Resources.UiAggregationPeriodDate;
                case AggregationPeriodType.week:
                    return Properties.Resources.UiAggregationPeriodWeek;
                case AggregationPeriodType.month:
                    return Properties.Resources.UiAggregationPeriodMonth;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
