﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors.Kdb;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.KDBPlugin.Static;

namespace Panopticon.KDBPlugin.UI.Static
{
    public partial class ConfigPanel : UserControl, INotifyPropertyChanged,
        IDataPluginConfigElement
    {
        // TODO: Remove attached properties,
        // most of the properties can be moved to settings. 
        public event PropertyChangedEventHandler PropertyChanged;

        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(KdbSettings), typeof(ConfigPanel),
                    new PropertyMetadata(new PropertyChangedCallback(
                        Settings_Changed)));

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel",
                typeof(StaticKdbSettingsViewModel), typeof(ConfigPanel),
                    new PropertyMetadata(new PropertyChangedCallback(
                        ViewModel_Changed)));

        public static DependencyProperty ParametersProperty =
            DependencyProperty.Register("Parameters",
                typeof(IEnumerable<ParameterValue>), typeof(ConfigPanel),
                    new PropertyMetadata());

        /// <summary>
        /// Creates a new instance of the ConfigPanel.
        /// </summary>
        public ConfigPanel()
            :this(null)
        {

        }

        //TODO:- cleanup, move methods to viewModel
        public ConfigPanel(StaticKdbSettingsViewModel viewModel)
        {
            ViewModel = viewModel;
            
            InitializeComponent();
        }

        private void Init(StaticKdbSettingsViewModel viewModel)
        {
            if (viewModel == null) return;
            DataContext = this.Settings = viewModel.Settings;
            DataContextChanged +=
                UserControl_DataContextChanged;
            this.Parameters = viewModel.Parameters;
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (KdbSettings)args.OldValue,
                (KdbSettings)args.NewValue);
        }

        protected void OnSettingsChanged(KdbSettings oldSettings,
            KdbSettings newSettings)
        {
            if (oldSettings != null)
            {
                oldSettings.PropertyChanged -=
                    new PropertyChangedEventHandler(Settings_PropertyChanged);
                oldSettings.QuerySettings.PropertyChanged -=
                    QuerySettings_PropertyChanged;
            }

            if (newSettings != null)
            {
                newSettings.PropertyChanged +=
                    new PropertyChangedEventHandler(Settings_PropertyChanged);
                newSettings.QuerySettings.PropertyChanged +=
                    QuerySettings_PropertyChanged;
            }
        }

        private static void ViewModel_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnViewModelChanged(
                (StaticKdbSettingsViewModel)args.OldValue,
                (StaticKdbSettingsViewModel)args.NewValue);
        }

        protected void OnViewModelChanged(StaticKdbSettingsViewModel oldViewModel,
            StaticKdbSettingsViewModel newViewModel)
        {
            Init(newViewModel);
        }

        void QuerySettings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectedTable")
            {
                FirePropertyChanged("Columns");
                FirePropertyChanged("DatetimeZColumns");
                FirePropertyChanged("DateDColumns");
                FirePropertyChanged("TimeTColumns");
                FirePropertyChanged("SymbolSColumns");
            }

            Settings.UpdateQuery();
        }

        private void Settings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            //regenerate the query as settings changed
            Settings.UpdateQuery();
        }

        public KdbSettings Settings
        {
            get { return (KdbSettings)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        public StaticKdbSettingsViewModel ViewModel
        {
            get { return (StaticKdbSettingsViewModel)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            private get
            {
                return (IEnumerable<ParameterValue>)
                GetValue(ParametersProperty);
            }
            set { SetValue(ParametersProperty, value); }
        }

        public bool IsOk
        {
            get
            {
                if (Settings == null) return false;

                if (string.IsNullOrEmpty(Settings.Host)) return false;

                return Settings.QuerySettings.IsQueryMode
                    ? !string.IsNullOrEmpty(Settings.QuerySettings.Query)
                    : Settings.QuerySettings.SelectedTable != null;
            }
        }

        public void OnOk()
        {
            if (Settings == null)
            {
                return;
            }

            // This method is called when Ok button in DataSourceSettingsDialog is clicked.
            // Clear the column here, so that even if settings are not chagned, latest schema from 
            // DB is pulled.
            Settings.QuerySettings.SchemaColumnsSettings.ClearSchemaColumns();
        }

        public List<string> DatetimeZColumns
        {
            get
            {
                return GetFilteredColumns(new char[] { 'z', 'p' });
            }
        }

        public List<string> DateDColumns
        {
            get
            {
                List<string> dColumns = new List<string>();
                dColumns.Add(KdbUtil.StaticDateColumnName);
                var t = GetFilteredColumns(new char[] { 'd' });
                if (t != null)
                {
                    dColumns.AddRange(t);
                }
                return dColumns;
            }
        }

        public List<string> TimeTColumns
        {
            get
            {
                return GetFilteredColumns(new char[] { 't', 'n' });
            }
        }

        public List<string> SymbolSColumns
        {
            get
            {
                return GetFilteredColumns(new char[] { 's' });
            }
        }

        private void FirePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this,
                    new PropertyChangedEventArgs(propertyName));
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            Settings = DataContext as KdbSettings;
        }

        private List<string> GetFilteredColumns(char[] columnTypes)
        {
            if (Settings == null || Settings.QuerySettings.SelectedTable == null
                || Settings.Columns == null)
            {
                return null;
            }

            return KdbUtil.FilterColumnsList(Settings.Columns, columnTypes);
        }

        private void Expander_OnExpanded(object sender, RoutedEventArgs e)
        {
            Expander expander = sender as Expander;

            if (expander == null) return;

            var rowIndex = Grid.GetRow(expander);
            var row = TableAndQueryGrid.RowDefinitions[rowIndex];
            if (expander.IsExpanded)
            {
                row.Height = new GridLength(1, GridUnitType.Star); //starHeight[rowIndex];
                row.MinHeight = 50;
            }
            else
            {
                //starHeight[rowIndex] = row.Height;
                row.Height = GridLength.Auto;
                row.MinHeight = 0;
            }

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //TODO:- fix for DDTV-6369 need to improvise
            TableExpander.IsExpanded = false;
            QueryExpander.IsExpanded = false;
            if (ViewModel.Settings.QuerySettings.IsQueryMode)
            {
                TableExpander.IsExpanded = false;
                QueryExpander.IsExpanded = true;
            }
            else
            {
                TableExpander.IsExpanded = true;
                QueryExpander.IsExpanded = false;
            }
        }
    }
}
