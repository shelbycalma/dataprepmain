﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.KDBPlugin.Static;

namespace Panopticon.KDBPlugin.UI.Static
{
    public class PluginUI : PluginUIBase<Plugin, KdbSettings>
    {
        // The uri of the image displayed in the "Connect to data" dialog as
        // well as the data plug-in drop down.
        private const string imageuri =
            "pack://application:,,,/Panopticon.KDBPlugin.UI;component/ConnectImage.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        // Called when the user selects this plug-in in the "Connect to data" 
        // dialog or picks it from the data plug-in dropdown. The DoConnect 
        // method is resposible for allowing the user to create a new data 
        // source connection.
        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            KdbSettings con = Plugin.CreateConnectionSettings(new PropertyBag(),
                parameters);

            ConnectionWindow window = new ConnectionWindow(
                CreateViewModel(con, parameters));

            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return window.PropertyBag;
            }
            return null;
        }

        /// <summary>
        /// GetConfigElement returns a UI element that can be used to configure 
        /// a data connection, for this specific plug-in, in Datawatch Designer.
        /// </summary>
        /// <param name="bag">A <see cref="PropertyBag"/> containing connection 
        /// settings.</param>
        /// <param name="parameters">An enumerable of parameter values.</param>
        /// <returns>A configuration UI element.</returns>
        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            // Create a new ConfigPanel instance.

            KdbSettings settings = Plugin.CreateConnectionSettings(bag, parameters);
            return new ConfigPanel(CreateViewModel(settings, parameters));
        }

        /// <summary>
        /// Returns a <see cref="PropertyBag"/> containing settings for a 
        /// connection, given a UI configuration element. This method is used to
        /// store the modified connection settings after editing the data plug-in
        /// settings for a data source in Datawatch designer.
        /// </summary>
        /// <param name="obj">The configuration UI element instance.</param>
        /// <returns>A <see cref="PropertyBag"/> containing connection settings.
        /// </returns>
        public override PropertyBag GetSetting(object obj)
        {
            // Cast the object to a ConfigPanel, since we know that all 
            // configuration elements for this plug-in will be of this class.
            ConfigPanel configPanel = (ConfigPanel)obj;

            // Create a new PropertyBag for the connection.            
            return configPanel.Settings.ToPropertyBag();
        }

        public virtual StaticKdbSettingsViewModel CreateViewModel(KdbSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            return new StaticKdbSettingsViewModel(settings, parameters);
        }
    }
}
