﻿using System;
using System.Windows;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Framework.Model;
using Exceptions = Datawatch.DataPrep.Data.Core.Exceptions;

namespace Panopticon.KDBPlugin.UI.Static
{
    class AggregationMethodTypeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (!(value is AggregateType)) return DependencyProperty.UnsetValue;

            AggregateType aggType = (AggregateType)value;

            if (aggType == AggregateType.None)
            {
                return Properties.Resources.UiAggregationTypeGroupBy;
            }

            return aggType.ToString();
            
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
