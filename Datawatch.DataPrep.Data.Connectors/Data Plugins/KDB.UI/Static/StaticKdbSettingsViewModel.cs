﻿using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.KDBPlugin.Static;
using System.Collections.Generic;
using System.ComponentModel;

namespace Panopticon.KDBPlugin.UI.Static
{
    public class StaticKdbSettingsViewModel : INotifyPropertyChanged
    {
        private IEnumerable<ParameterValue> parameters;
        public KdbSettings Settings { get; private set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public StaticKdbSettingsViewModel(KdbSettings settings, 
            IEnumerable<ParameterValue> parameters) 
        {
            this.Settings = settings;
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public virtual object ConnectionConfigElement
        {
            get
            {
                return new ConnectionConfigPanel(Settings);
            }
        }
    }
}
