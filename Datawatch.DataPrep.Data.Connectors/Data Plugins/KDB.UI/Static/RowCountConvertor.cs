﻿using System;
using System.Windows.Data;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.KDBPlugin.UI.Static
{
    class RowCountConvertor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (value == null)
            {
                value = 0;
            }
            return value.ToString() + " " + "Rows";
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw Exceptions.ValueConverterCantConvertBack(this.GetType());
        }
    }
}
