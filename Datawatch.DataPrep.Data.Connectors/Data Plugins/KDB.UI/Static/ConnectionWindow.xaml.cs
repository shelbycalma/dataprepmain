﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.KDBPlugin.Static;

namespace Panopticon.KDBPlugin.UI.Static
{
    public partial class ConnectionWindow : Window
    {
        public static RoutedCommand OkCommand =
            new RoutedCommand("OK", typeof(ConnectionWindow));
        public static RoutedCommand CancelCommand =
            new RoutedCommand("Cancel", typeof(ConnectionWindow));
        private KdbSettings settings;

        private readonly IEnumerable<ParameterValue> parameters;
        private readonly StaticKdbSettingsViewModel viewModel;

        public ConnectionWindow(StaticKdbSettingsViewModel viewModel)
        {
            this.viewModel = viewModel;
            this.settings = viewModel.Settings;
            this.parameters = viewModel.Parameters;

            InitializeComponent();
        }

        public StaticKdbSettingsViewModel ViewModel
        {
            get { return viewModel; }
        }

        public KdbSettings Settings
        {
            get { return settings; }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public PropertyBag PropertyBag
        {
            get { return ConfigPanel.Settings.ToPropertyBag(); }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ConfigPanel.IsOk;
        }

        private void Ok_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void Cancel_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }
        
    }
}
