﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.HtmlPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof (DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<HtmlSettings>
    {
        internal const string PluginId = "HtmlPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Html";
        internal const string PluginType = DataPluginTypes.File;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override HtmlSettings CreateSettings(PropertyBag bag)
        {
            return new HtmlSettings(pluginManager, bag);
        }

        //Data Methods
        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            return GetData(workbookDir, dataDir, bag, parameters, rowcount, true);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters,
            int rowcount, bool applyRowFilteration)
        {
            HtmlSettings hSettings
                = new HtmlSettings(pluginManager, bag, parameters);

            HtmlHelper hHelper = new HtmlHelper(hSettings);

            return hHelper.DeserializeTableData();
        }
    }
}