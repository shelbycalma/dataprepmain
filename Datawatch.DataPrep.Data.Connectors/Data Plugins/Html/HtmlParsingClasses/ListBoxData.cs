﻿using System.Data;

namespace Panopticon.HtmlPlugin.HtmlParsingClasses
{
    public class ListBoxData
    {
        public object Source { get; set; }
        public TableData TableData { get; set; }
        public string TableName { get; set; }
        public DataView DataView { get; set; }
        public ListBoxData(object source, TableData tableData)
        {
            Source = source;
            TableData = tableData;
            TableName = TableData.Name;
            DataView = TableData.DataTable.AsDataView();
        }
    }
}
