﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Panopticon.HtmlPlugin.HtmlParsingClasses
{
    /// <summary>
    /// This class is intended to take the DataTables produced from the HtmlConverter class and return an IDataReader that conforms to ScratchpadColumns.
    /// </summary>
    public class HtmlDataTableReader : IDataReader
    {
        #region Fields
        private readonly DataTable dataTable;
        private readonly List<DataColumn> columns;
        private bool isOpen;
        private int rowIndex;
        #endregion Fields

        #region Properties
        #endregion Properties

        #region Constructors
        public HtmlDataTableReader(DataTable dataTable)
        {
            this.dataTable = dataTable;
            columns = new List<DataColumn>();
            foreach (DataColumn column in dataTable.Columns)
            {
                columns.Add(column);
            }

            isOpen = false;
            rowIndex = -1;
        }
        #endregion Constructors

        #region Methods
        /// <summary>
        /// Creates a Schema Table in the form on a DataTable for the IDataReader interface.
        /// Row will NOT be adde.
        /// </summary>
        /// <param name="table">ITable instance to convert.</param>
        /// <returns>DataTable instance for schema use.</returns>
        private static DataTable MakeSchemaTable(DataTable table)
        {
            /**
             **********************ITable SchemaTable ***********************
             * | ColumnName | ColumnOrdinal | DbType | Size | IsLong |
             */
            DataTable dataTable = new DataTable();

            // Create the columns
            DataColumn[] cols =
            {
                new DataColumn("ColumnName", typeof (String)),
                new DataColumn("ColumnOrdinal", typeof (int)),
                new DataColumn("DbType", typeof (int)),
                new DataColumn("DataType", typeof (String)),
                new DataColumn("Size", typeof(int)),
                new DataColumn("IsLong", typeof(bool)),
            };
            dataTable.Columns.AddRange(cols);

            // Add the row data for each column.
            IEnumerable<DataColumn> columns =
                Enumerable.Range(0, (table.Columns.Count)).Select(idx => table.Columns[idx]);

            int i = 0;
            foreach (DataColumn column in columns)
            {
                DataRow row = dataTable.NewRow();
                row["ColumnName"] = column.ColumnName;
                row["ColumnOrdinal"] = i++;
                row["DbType"] = DatabaseUtility.TypeToDbTypeMap[typeof(String)]; // There are only strings here.
                row["DataType"] = DatabaseUtility.DbTypeToTypeStringMap[DbType.String]; // There are only strings here.
                row["IsLong"] = true; // strings are always long, because we don't want to discover length yet.
                row["Size"] = 0;
                dataTable.Rows.Add(row);
            }

            return dataTable;
        }
        #endregion Methods

        #region IDataReader
        /// <summary>
        /// Column Count for the Table being read.
        /// </summary>
        public int FieldCount { get { return columns.Count; } }

        /// <summary>
        /// Depth returns 0 because we do not have nested tables.
        /// </summary>
        public int Depth { get { return 0; } }

        /// <summary>
        /// Is the reader closed?
        /// </summary>
        public bool IsClosed { get { return !isOpen; } }

        /// <summary>
        /// This Interface will not allow writing, so we will return -1 always
        /// </summary>
        public int RecordsAffected { get { return -1; } }

        /// <summary>
        /// Get the column at i.
        /// </summary>
        object IDataRecord.this[int i]
        {
            get { return columns[i]; }
        }

        /// <summary>
        /// Get the column from the column name.
        /// </summary>
        object IDataRecord.this[string name]
        {
            get { return columns.FirstOrDefault(c => c.ColumnName == name); }
        }

        public void Dispose() { }

        /// <summary>
        /// Gets the column name at the given index.
        /// </summary>
        /// <param name="i">Column index.</param>
        /// <returns>Column name.</returns>
        public string GetName(int i)
        {
            return columns[i].ColumnName;
        }

        /// <summary>
        /// These columns are always strings.
        /// </summary>
        /// <param name="i">column index</param>
        /// <returns>String.</returns>
        public string GetDataTypeName(int i)
        {
            return DbType.String.ToString();
        }

        /// <summary>
        /// These columns are always strings.
        /// </summary>
        /// <param name="i">Column index</param>
        /// <returns>String</returns>
        public Type GetFieldType(int i)
        {
            return typeof(String);
        }

        /// <summary>
        /// Get the Column Object value at the Column Ordinal i.
        /// </summary>
        /// <param name="i">Column Ordinal</param>
        /// <returns>Object value</returns>
        public object GetValue(int i)
        {
            return dataTable.Rows[rowIndex][i];
        }

        /// <summary>
        /// Get all the values in a row and load them into the passed values array.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public int GetValues(object[] values)
        {
            // need to make sure that the values array or columns array do not through null, so indexers for each.
            int i, j;
            for (i = 0, j = 0; i < values.Length && j < columns.Count; i++, j++)
            {
                values[i] = dataTable.Rows[rowIndex][j];
            }

            return i;
        }

        /// <summary>
        /// Get the Column Ordinal from the column name.
        /// </summary>
        /// <param name="name">Column Name</param>
        /// <returns>Column Ordinal</returns>
        public int GetOrdinal(string name)
        {
            for (var i = 0; i < columns.Count; i++)
            {
                if (columns[i].ColumnName == name)
                {
                    return i;
                }
            }

            throw new IndexOutOfRangeException(
                Properties.Resources.ExCannotFindSpecifiedColumn);
        }

        /// <summary>
        /// Get a String Value at the Column Ordinal i.
        /// </summary>
        /// <param name="i">Column Ordinal</param>
        /// <returns>String value</returns>
        public string GetString(int i)
        {
            return dataTable.Rows[rowIndex][i].ToString();
        }

        public IDataReader GetData(int i)
        {
            throw new NotSupportedException("GetData is not supported.");
        }

        /// <summary>
        /// Tests if column value is DBNull.
        /// </summary>
        /// <param name="i">Column Ordinal</param>
        /// <returns>Bool</returns>
        public bool IsDBNull(int i)
        {
            return dataTable.Rows[rowIndex].IsNull(i);
        }


        public void Close()
        {
            isOpen = false;
        }

        /// <summary>
        /// Returns a DataTable that defines the schema of the underlying ITable.
        /// </summary>
        /// <returns></returns>
        public DataTable GetSchemaTable()
        {
            return MakeSchemaTable(dataTable);
        }

        public bool Read()
        {
            //if (++_rowIndex < _dataTable.Rows.Count)
            //    _row = _dataTable.Rows[_rowIndex];
            return (++rowIndex < dataTable.Rows.Count);
        }

        #region NotImplemented
        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public bool GetBoolean(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public byte GetByte(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public long GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public char GetChar(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public long GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public Guid GetGuid(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public short GetInt16(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public int GetInt32(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public long GetInt64(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public float GetFloat(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public double GetDouble(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public decimal GetDecimal(int i)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not Inplemented since all values are strings.
        /// </summary>
        public DateTime GetDateTime(int i)
        {
            throw new NotImplementedException();
        }

        public bool NextResult()
        {
            throw new NotImplementedException();
        }
        #endregion NotImplemented

        #endregion IDataReader
    }
}
