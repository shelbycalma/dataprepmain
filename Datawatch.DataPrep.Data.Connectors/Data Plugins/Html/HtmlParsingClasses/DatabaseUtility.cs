﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Panopticon.HtmlPlugin.HtmlParsingClasses
{
    public static class DatabaseUtility
    {
        //Deleted code from this class in revision 462905709eeb

        #region Fields
        // 2**30-1 is the maximum size of an nvarchar column.
        public const int MaxTextSize = (Int32.MaxValue - 1) / 2;		
        #endregion Fields

        #region Properties
        public static Dictionary<Type, DbType> TypeToDbTypeMap
        {
            get
            {
                return new Dictionary<Type, DbType>()
                {
                    {typeof (byte), DbType.Byte},
                    {typeof (sbyte), DbType.SByte},
                    {typeof (short), DbType.Int16},
                    {typeof (ushort), DbType.UInt16},
                    {typeof (int), DbType.Int32},
                    {typeof (uint), DbType.UInt32},
                    {typeof (long), DbType.Int64},
                    {typeof (ulong), DbType.UInt64},
                    {typeof (float), DbType.Single},
                    {typeof (double), DbType.Double},
                    {typeof (decimal), DbType.Decimal},
                    {typeof (bool), DbType.Boolean},
                    {typeof (string), DbType.String},
                    {typeof (char), DbType.StringFixedLength},
                    {typeof (Guid), DbType.Guid},
                    {typeof (DateTime), DbType.DateTime},
                    {typeof (DateTimeOffset), DbType.DateTimeOffset},
                    {typeof (byte[]), DbType.Binary},
                    {typeof (byte?), DbType.Byte},
                    {typeof (sbyte?), DbType.SByte},
                    {typeof (short?), DbType.Int16},
                    {typeof (ushort?), DbType.UInt16},
                    {typeof (int?), DbType.Int32},
                    {typeof (uint?), DbType.UInt32},
                    {typeof (long?), DbType.Int64},
                    {typeof (ulong?), DbType.UInt64},
                    {typeof (float?), DbType.Single},
                    {typeof (double?), DbType.Double},
                    {typeof (decimal?), DbType.Decimal},
                    {typeof (bool?), DbType.Boolean},
                    {typeof (char?), DbType.StringFixedLength},
                    {typeof (Guid?), DbType.Guid},
                    {typeof (DateTime?), DbType.DateTime},
                    {typeof (DateTimeOffset?), DbType.DateTimeOffset},
                };
            }
        }

        public static Dictionary<DbType, String> DbTypeToTypeStringMap
        {
            get
            {
                return new Dictionary<DbType, String>()
                {
                    {DbType.Byte, "System.Byte"},
                    {DbType.SByte, "System.SByte"},
                    {DbType.Int16, "System.Int16"},
                    {DbType.UInt16, "System.UInt16"},
                    {DbType.Int32, "System.Int32"},
                    {DbType.UInt32, "System.UInt32"},
                    {DbType.Int64, "System.Int64"},
                    {DbType.UInt64, "System.UInt64"},
                    {DbType.Single, "System.Single"},
                    {DbType.Double, "System.Double"},
                    {DbType.Decimal, "System.Decimal"},
                    {DbType.Boolean, "System.Boolean"},
                    {DbType.String, "System.String"},
                    {DbType.StringFixedLength, "System.String"},
                    {DbType.Guid, "System.Guid"},
                    {DbType.DateTime, "System.DateTime"},
                    {DbType.DateTimeOffset, "System.DateTimeOffest"},
                    {DbType.Binary, "System.Byte[]"},
                };
            }
        }
        #endregion Properties

    }
}
