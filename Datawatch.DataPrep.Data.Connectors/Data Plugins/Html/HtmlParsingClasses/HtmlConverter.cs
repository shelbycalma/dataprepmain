﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Windows;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Dom.Html;
using AngleSharp.Parser.Html;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.HtmlPlugin.HtmlParsingClasses
{
    public class HtmlConverter : TextConverterBase
    {
        //Deleted code from this class in revision 462905709eeb

        private List<TableData> tables;
        private List<TableData> lists;
        private Dictionary<int, int> rowSpan;
        private bool hasRowSpan;
        private int tableCount;
        private int listCount;
        private bool _includeLists;

        public int CreateTables(IDataObject data, bool includeLists = false)
        {
            IDocument document = null;
            Tables = new List<DataTable>();
            Data = new List<TableData>();
            TableMap = new Dictionary<string, TableData>();
            tables = new List<TableData>();
            lists = new List<TableData>();
            tableCount = 0;
            listCount = 0;
            _includeLists = false;

            try
            {
                document = CreateDocumentFromClipboard(data);

                if (document == null)
                {
                    document = CreateDocumentFromWebURL(data);
                }
                if (document == null)
                {
                    return 0;
                }

                //WalkTree(document, 0);
                foreach (INode node in document.ChildNodes)
                {
                    ProcessNode(node);
                }

                foreach (TableData table in tables)
                {
                    Data.Add(table);
                }

                if (_includeLists)
                {
                    foreach (TableData table in lists)
                    {
                        Data.Add(table);
                    }
                }

                foreach (TableData tableData in Data)
                {
                    Tables.Add(tableData.DataTable);
                }

                foreach (TableData table in Data)
                {
                    TableMap[table.Name] = table;
                }

                return Tables.Count;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return Tables.Count;
            }
        }

        private static IDocument CreateDocumentFromWebURL(IDataObject data)
        {
            IDocument document = null;
            string html = data.GetData(DataFormats.Html) as string;
            if (!string.IsNullOrWhiteSpace(html))
            {
                document = DocumentBuilder.Html(html);
            }
            return document;
        }

        private static IDocument CreateDocumentFromClipboard(IDataObject data)
        {
            IDocument document = null;
            string html = GetHtmlFromClipboard(data);
            if (!string.IsNullOrWhiteSpace(html))
            {
                document = DocumentBuilder.Html(html);
            }
            else
            {
                Url url = GetUrlFromClipboard(data);
                if (url != null)
                {
                    document = DocumentBuilder.Html(url);
                }
            }
            return document;
        }

        public int CreateTablesFromUrl(string path)
        {

            IDocument document = null;
            Tables = new List<DataTable>();
            Data = new List<TableData>();
            TableMap = new Dictionary<string, TableData>();
            tables = new List<TableData>();
            lists = new List<TableData>();
            tableCount = 0;
            listCount = 0;
            try
            {
                WebClient client = new WebClient
                {
                    CachePolicy = new System.Net.Cache.RequestCachePolicy(
                        System.Net.Cache.RequestCacheLevel.BypassCache),
                };
                Uri uri = GetUri(path);
                byte[] html = client.DownloadData(uri);
                Stream stream = new MemoryStream(html);
                document = DocumentBuilder.Html(stream);
                if (document == null)
                {
                    return 0;
                }

                //WalkTree(document, 0);
                foreach (INode node in document.ChildNodes)
                {
                    ProcessNode(node);
                }

                foreach (TableData table in tables)
                {
                    Data.Add(table);
                }

                foreach (TableData table in lists)
                {
                    Data.Add(table);
                }

                foreach (TableData tableData in Data)
                {
                    Tables.Add(tableData.DataTable);
                }

                foreach (TableData table in Data)
                {
                    TableMap[table.Name] = table;
                }

                return Tables.Count;
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return Tables.Count;
            }
        }

        public int CreateTablesFromFile(string path)
        {
            IDocument document = null;
            Tables = new List<DataTable>();
            Data = new List<TableData>();
            try
            {
                using (FileStream stream = File.OpenRead(path))
                {
                    document = DocumentBuilder.Html(stream);
                    foreach (INode node in document.ChildNodes)
                    {
                        ProcessNode(node);
                    }
                    return Data.Count;
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                return Data.Count;
            }
        }

        void ProcessNode(INode node)
        {
            if (node.NodeName.ToLower() == "table")
            {
                ProcessTable(node as IHtmlTableElement);
            }
            else if (node.NodeName.ToLower() == "ul")
            {
                ProcessList(node);
            }
            else
            {
                ProcessNodeList(node.ChildNodes);
            }
        }

        void ProcessTable(IHtmlTableElement table)
        {
            try
            {
                if (table == null || table.Bodies == null)
                {
                    return;
                }

                tableCount++;
                TableData data = new TableData(string.Format("Table {0}", tableCount));
                rowSpan = new Dictionary<int, int>();
                hasRowSpan = false;

                if (table.Head != null)
                {
                    foreach (IHtmlTableRowElement row in table.Head.Rows)
                    {
                        foreach (IHtmlTableHeaderCellElement cell in row.Cells)
                        {
                            string text = ReplaceWhitespace(cell.TextContent);
                            string name = MakeColumnName(text, data.Columns);
                            data.Columns.Add(name);
                        }
                    }
                }

                foreach (IHtmlTableSectionElement section in table.Bodies)
                {
                    if (section.TagName.ToLower() != "tbody")
                    {
                        continue;
                    }

                    foreach (INode node in section.ChildNodes)
                    {
                        if (node.NodeName.ToLower() != "tr")
                        {
                            continue;
                        }

                        List<string> row = new List<string>();
                        foreach (INode cell in node.ChildNodes)
                        {
                            if (cell.NodeName.ToLower() == "th")
                            {
                                string columnName = cell.TextContent;
                                data.Columns.Add(columnName);
                                continue;
                            }
                            if (cell.NodeName.ToLower() != "td")
                            {
                                continue;
                            }

                            CheckForAnonymousHeader(row, data);
                            CheckForRowSpan(cell, row);
                            string text = GetNodeText(cell);
                            row.Add(text);
                        }

                        if (row.Count > 0)
                        {
                            data.Rows.Add(row);
                        }
                    }
                }

                if (HasData(data))
                {
                    tables.Add(data);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        void ProcessList(INode node)
        {
            try
            {
                listCount++;
                TableData data = new TableData(string.Format("List {0}", listCount));
                data.Columns.Add("F1");
                foreach (INode child in node.ChildNodes)
                {
                    if (child.NodeName.ToLower() == "li")
                    {
                        data.Rows.Add(new List<string>(new string[1] { child.TextContent }));
                    }
                }
                if (HasData(data))
                {
                    lists.Add(data);
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        bool HasData(TableData data)
        {
            foreach (List<string> row in data.Rows)
            {
                foreach (string cell in row)
                {
                    if (!string.IsNullOrWhiteSpace(cell))
                    {
                        return true;
                    }
                }
            }

            return true;
        }

        void CheckForRowSpan(INode node, List<string> row)
        {
            try
            {
                IHtmlTableCellElement tableCell = node as IHtmlTableCellElement;
                if (tableCell == null)
                {
                    return;
                }

                if (!hasRowSpan || !this.rowSpan.ContainsKey(row.Count))
                {
                    if (tableCell.RowSpan > 0)
                    {
                        hasRowSpan = true;
                        this.rowSpan[row.Count] = tableCell.RowSpan;
                    }

                    return;
                }

                int rowSpan = this.rowSpan[row.Count] - 1;
                if (rowSpan > 0)
                {
                    this.rowSpan[row.Count] = rowSpan;
                }
                else
                {
                    this.rowSpan.Remove(row.Count);
                    if (this.rowSpan.Count == 0)
                    {
                        hasRowSpan = false;
                    }
                }
                row.Add("");
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
            }
        }

        void CheckForAnonymousHeader(List<string> row, TableData data)
        {
            if (row.Count >= data.Columns.Count)
            {
                for (int n = data.Columns.Count; n <= row.Count; n++)
                {
                    string columnName = string.Format("F{0}", n + 1);
                    data.Columns.Add(columnName);
                }
            }
        }

        void ProcessNodeList(INodeList nodeList)
        {
            foreach (INode node in nodeList)
            {
                ProcessNode(node);
            }
        }

        public static string GetNodeText(INode node)
        {
            return ReplaceWhitespace(node.TextContent);
        }

        public static Url GetUrl(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                return null;
            }

            string scheme = path.Split(':')[0].ToLower();

            if (scheme == "http" || scheme == "https")
            {
                try
                {
                    return new Url(path);
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        public static Url GetUrlFromClipboard(IDataObject data)
        {
            return GetUrl(data.GetData("Text") as string);
        }
    }
}
