﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.HtmlPlugin.HtmlParsingClasses
{
    public class TableData
    {
        public string Name { get; set; }
        public List<string> Columns { get; set; }
        public List<List<string>> Rows { get; set; }
        
        private DataTable dataTable;
        public DataTable DataTable
        {
            get { return dataTable ?? (dataTable = CreateDataTable()); }
        }
        
        public TableData(string name)
        {
            Name = name;
            Columns = new List<string>();
            Rows = new List<List<string>>();
        }

        public DataTable CreateDataTable()
        {
            DataTable table = new DataTable(Name);
            foreach (var name in Columns)
            {
                table.Columns.Add(name);
            }

            foreach (var row in Rows)
            {
                table.Rows.Add(row.ToArray());
            }

            return table;
        }

        public void WriteCsvFile(string path)
        {
            try
            {
                using (var writer = new StreamWriter(path, false, Encoding.UTF8))
                {
                    for (int i = 0; i < Columns.Count; i++)
                    {
                        if (i > 0)
                        {
                            writer.Write(",");
                        }

                        writer.Write("\"" + Columns[i] + "\"");
                    }

                    writer.WriteLine();

                    for (int r = 0; r < Rows.Count; r++)
                    {
                        for (int c = 0; c < Rows[r].Count; c++)
                        {
                            if (c > 0)
                            {
                                writer.Write(",");
                            }

                            writer.Write("\"" + Rows[r][c] + "\"");
                        }

                        writer.WriteLine();
                    }
                }
            }
            catch(Exception ex)
            {
                Log.Error(Properties.Resources.ExErrorWritingCsvFile, ex.Message);
            }
        }
    }
}
