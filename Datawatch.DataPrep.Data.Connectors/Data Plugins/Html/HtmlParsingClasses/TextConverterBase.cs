﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.HtmlPlugin.HtmlParsingClasses
{
    public class TextConverterBase
    {
        //Deleted code from this class in revision 462905709eeb

        public List<DataTable> Tables { get; set; }
        public List<TableData> Data { get; set; }
        public Dictionary<string, TableData> TableMap { get; set; }

        /// <summary>
        /// Return an IDataReader based on the tableName Provided.
        /// </summary>
        /// <param name="tableName">The name of the table to create an IDataReader for.</param>
        /// <returns>An IDataReader.</returns>
        public IDataReader GetReader(string tableName)
        {
            if (!TableMap.ContainsKey(tableName))
            {
                throw new ArgumentException(Properties.Resources.ExTableDoesNotExist);
            }

            DataTable table = Tables.First(t => t.TableName == tableName);
            return new HtmlDataTableReader(table);
        }

        public DataTable GetDataTable(string tableName)
        {
            return CreateDataTable(TableMap[tableName]);
        }

        public static bool IsHtmlFile(IDataObject data)
        {
            string[] files = data.GetData(DataFormats.FileDrop) as string[];
            if (files == null || files.Length == 0)
            {
                return false;
            }

            string ext = Path.GetExtension(files[0]);
            return ".htm".Equals(ext) || ".html".Equals(ext);
        }

        public static bool IsHtml(IDataObject data)
        {
            if (data.GetDataPresent(DataFormats.FileDrop))
            {
                return IsHtmlFile(data);
            }

            if (data.GetDataPresent(DataFormats.Html))
            {
                return true;
            }

            Uri uri = GetUriFromClipboard(data);
            if (uri == null)
            {
                return false;
            }

            string path = uri.AbsolutePath.Split('#')[0];
            string[] parts = path.Split('.');
            return !(parts[parts.Length - 1].Equals(".xml",
                StringComparison.InvariantCultureIgnoreCase));
        }

        public static string ReplaceWhitespace(string text)
        {
            StringBuilder output = new StringBuilder();
            text = Regex.Replace(text, @"[^\u0000-\u007F]", string.Empty);
            foreach (char t in text)
            {
                output.Append(char.IsWhiteSpace(t) ? ' ' : t);
            }
            return output.ToString();
        }

        public static string MakeColumnName(string text, List<string> columns)
        {
            if (string.IsNullOrWhiteSpace(text))
            {
                return string.Format("F{0}", columns.Count);
            }

            text = text.Trim();

            StringBuilder name = new StringBuilder();
            text = Regex.Replace(text, @"[^\u0000-\u007F]", string.Empty);
            int spaces = 0;
            foreach (char t in text)
            {
                if (char.IsWhiteSpace(t) || char.IsControl(t))
                {
                    spaces++;
                    if (spaces == 1)
                        name.Append(' ');
                    continue;
                }
                if(!Char.IsLetter(t) || !Char.IsLetterOrDigit(t)) continue;
                name.Append(t);
            }

            if (name.Length == 0)
            {
                return string.Format("F{0}", columns.Count);
            }

            return MakeUniqueColumnName(name.ToString(), columns);
        }

        public static string GetHtmlFromClipboard(IDataObject data)
        {
            try
            {
                string html = data.GetData(DataFormats.Html) as string;
                if (string.IsNullOrWhiteSpace(html))
                {
                    return string.Empty;
                }

                StringReader reader = new StringReader(html);
                if (!reader.ReadLine().StartsWith("Version"))
                {
                    return string.Empty;
                }

                string[] startPos = reader.ReadLine().Split(':');
                if (startPos.Length != 2 && startPos[0].Equals("StartHML", StringComparison.InvariantCultureIgnoreCase))
                {
                    return string.Empty;
                }

                string[] endPos = reader.ReadLine().Split(':');
                if (endPos.Length != 2 && endPos[0].Equals("EndHML", StringComparison.InvariantCultureIgnoreCase))
                {
                    return string.Empty;
                }

                int start = int.Parse(startPos[1]);
                int end = int.Parse(endPos[1]);
                return html.Substring(start);
            }
            catch(Exception ex)
            {
                Log.Exception(ex);
                return string.Empty;
            }
        }

        public static string MakeUniqueColumnName(string name, List<string> columns)
        {
            if (!columns.Contains(name))
            {
                return name;
            }

            int index = 1;
            string origName = name;

            while (true)
            {
                name = string.Format("{0}{1}", origName, index);
                if (!columns.Contains(name))
                {
                    return name;
                }

                index++;
            }
        }

        public static string GetXmlFilePath(IDataObject data)
        {
            string[] files = data.GetData(DataFormats.FileDrop) as string[];
            if (files == null || files.Length == 0)
            {
                return null;
            }

            string ext = Path.GetExtension(files[0]);
            if (".xml".Equals(ext, StringComparison.InvariantCultureIgnoreCase))
            {
                return files[0];
            }

            return null;
        }

        public static bool IsXmlFile(IDataObject data)
        {
            return GetXmlFilePath(data) != null;
        }

        public static DataTable CreateDataTable(TableData data)
        {
            if (data == null)
            {
                return null;
            }

            DataTable table = new DataTable(data.Name);

            foreach (string name in data.Columns)
            {
                table.Columns.Add(name, typeof(string));
            }

            foreach (IEnumerable<string> row in data.Rows)
            {
                table.Rows.Add(row.ToArray());
            }

            return table;
        }

        public static Uri GetUri(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || !path.StartsWith("http"))
            {
                return null;
            }

            string scheme = path.Split(':')[0].Trim().ToLower();

            if (scheme == "http" || scheme == "https")
            {
                try
                {
                    return new Uri(path);
                }
                catch
                {
                    return null;
                }
            }

            return null;
        }

        public static Uri GetUriFromClipboard(IDataObject data)
        {
            return GetUri(data.GetData(DataFormats.Text) as string);
        }

    }
}
