﻿using System;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.HtmlPlugin
{
    public class HtmlSettings : TextSettingsBase
    {
        #region Private Variables
        private readonly IEnumerable<ParameterValue> parameters;
        #endregion

        #region Constructors
        public HtmlSettings(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public HtmlSettings(IPluginManager pluginManager, PropertyBag bag)
            : this(pluginManager, bag, null)
        {
        }

        public HtmlSettings(
            IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            this.parameters = parameters;
        }

        #endregion

        #region Properties
        public string HtmlString
        {
            get
            {
                return GetInternal("HtmlString", String.Empty);
            }
            set
            {
                SetInternal("HtmlString", value);
            }
        }

        public int TableIndex
        {
            get
            {
                return GetInternalInt("TableIndex", -1);
            }
            set
            {
                SetInternalInt("TableIndex", value);
            }
        }

        public bool IsDropable
        {
            get
            {
                if (this.FilePathType == PathType.Text) return true;
                return false;
            }
        }

        private PropertyBag _selectedTable = null;
        public PropertyBag SelectedTable
        {
            get
            {
                return this.FilePathType == PathType.Text ? GetInternalSubGroup("SelectedTable") : _selectedTable;
            }
            set
            {
                if (this.FilePathType == PathType.Text) { SetInternalSubGroup("SelectedTable", value); } else { _selectedTable = value; }
            }        
        }

        public bool UseFirstRowHeaders
        {
            get
            {
                return Convert.ToBoolean(
                    GetInternal("UseFirstRowHeaders", "false"));
            }
            set
            {
                SetInternal("UseFirstRowHeaders", Convert.ToString(value));
            }
        }

        public override string Title
        {
            get
            {
                return GetInternal("Title", Properties.Resources.UiPluginTitle +
                    " " + Convert.ToString(TableIndex + 1));
            }
            set { SetInternal("Title", value); }
        }

        public IEnumerable<ParameterValue> Parameters
        {
            get
            {
                return parameters;
            }
        }

        #endregion

        #region General Methods

        public override PathType FilePathType
        {
            get
            {
                string s = GetInternal("HtmlFilePathType");
                if (s != null)
                {
                    try
                    {
                        return (PathType)Enum.Parse(
                            typeof(PathType), s);
                    }
                    catch
                    {
                    }
                }
                return PathType.Text;
            }
            set
            {
                SetInternal("HtmlFilePathType", value.ToString());
                FirePropertyChanged("IsDropable");
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return "Json"; }
        }

        public string ParameterizeValue(
            string value, string arraySeparator = "\n")
        {
            if (parameters == null || string.IsNullOrEmpty(value))
            {
                return value;
            }

            ParameterEncoder encoder = new ParameterEncoder
            {
                Parameters = parameters,
                DefaultArraySeparator = arraySeparator,
                SourceString = value
            };
            return encoder.Encoded();
        }

        public string GetHtmlStreamData()
        {
            StreamReader stream = null;
            try
            {
                stream = DataPluginUtils.GetStream(this, parameters);
            }
            catch (Exception e)
            {
                this.errorReporter.Report("Creating stream", e.Message);
            }

            if (stream == null) return string.Empty;

            string html = stream.ReadToEnd();
            stream.Close();

            return html;
        }

        #endregion
    }
}