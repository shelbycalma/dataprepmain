﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.HtmlPlugin.HtmlParsingClasses;

namespace Panopticon.HtmlPlugin
{
    public class HtmlHelper
    {
        #region Constructors

        public HtmlHelper(HtmlSettings settings)
        {
            HSettings = settings;
        }

        #endregion

        #region Properties

        public HtmlSettings HSettings { get; set; }

        #endregion

        public List<ListBoxData> ParseAndCreateTables(DragEventArgs e)
        {
            return ParseAndCreateTablesInternal(e.Data, e.Data.GetData(DataFormats.Html) as string);
        }

        public List<ListBoxData> ParseAndCreateTables(string htmlString)
        {
            DataObject dataObj = new DataObject(DataFormats.Html, htmlString);
            return ParseAndCreateTablesInternal(dataObj, htmlString);
        }

        private List<ListBoxData> ParseAndCreateTablesInternal(IDataObject dataObject, string html)
        {
            List<ListBoxData> listBoxData = new List<ListBoxData>();
            List<TableData> tables = null;
            object source = null;

            // Convert the clipbard HTML to tables
            if (HtmlConverter.IsHtml(dataObject))
            {
                HtmlConverter tableCreator = new HtmlConverter();
                HSettings.HtmlString =
                    HSettings.FilePathType == PathType.Text ? html
                                                : string.Empty;
                tableCreator.CreateTables(dataObject);
                tables = tableCreator.Data;
                source = tableCreator;
            }

            if (tables == null || tables.Count == 0)
            {
                return listBoxData;
            }

            // Populate the table selection list box
            foreach (TableData tableData in tables)
            {
                ListBoxData data = new ListBoxData(source, tableData);
                listBoxData.Add(data);
            }

            return listBoxData;
        }

        public StandaloneTable BuildAndPopulateTable()
        {
            StandaloneTable table = new StandaloneTable();
            List<ListBoxData> data = ParseAndCreateTables(HSettings.HtmlString);

            if (HSettings.TableIndex == -1
                && HSettings.TableIndex < data.Count)
                return table;

            ListBoxData selectedData = data[HSettings.TableIndex];

            table = BuildTable(table, selectedData);
            table = PopulateTable(table, selectedData);

            return table;
        }

        public PropertyBag SerializeTableData(DataTable dataTable)
        {
            PropertyBag tableBag = new PropertyBag();
            int bagEntryNumber = 0;

            StringBuilder sb = new StringBuilder();

            //write cols to '0' entry
            if (!HSettings.UseFirstRowHeaders)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    if (i == dataTable.Columns.Count - 1)
                    {
                        sb.Append(dataTable.Columns[i].ColumnName.Trim());
                    }
                    else
                    {
                        sb.Append(dataTable.Columns[i].ColumnName.Trim() + "\n");
                    }
                }
                tableBag.Values.Add(
                    new PropertyValue(
                    Convert.ToString(bagEntryNumber), sb.ToString()));
                bagEntryNumber++;
            }

            //write rows until finished
            foreach (DataRow row in dataTable.Rows)
            {
                sb.Clear();
                object[] arr = row.ItemArray;
                for (int i = 0; i < arr.Length; i++)
                {
                    //no new line for the last because we can't stringsplit and
                    //ignore blank entries
                    if (i == arr.Length - 1)
                    {
                        sb.Append(Convert.ToString(arr[i]).Trim());
                    }
                    else
                    {
                        sb.Append(Convert.ToString(arr[i]).Trim() + "\n");
                    }
                }

                tableBag.Values.Add(
                    new PropertyValue(
                        Convert.ToString(bagEntryNumber), sb.ToString()));
                bagEntryNumber++;
            }

            return tableBag;
        }

        public ITable DeserializeTableData()
        {
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            if (HSettings.FilePathType == PathType.WebURL)
            {
                HSettings.SelectedTable = GetSelectedTableForWebUrlMode();
            }

            if (HSettings == null
                || HSettings.SelectedTable == null)
                throw new InvalidOperationException(
                    Properties.Resources.ExNullSelectedTable);

            PropertyValueCollection data = HSettings.SelectedTable.Values;
            int bagEntryIndex = 0;

            //build columns from first row
            string columns = data[Convert.ToString(bagEntryIndex)];
            string[] colNames = columns.Split(new[] { '\n' });
            int index = 1;
            foreach (string colName in colNames)
            {
                //if somehow the column name is null, sub in the default name
                Column col = !table.ContainsColumn(colName)
                    && !String.IsNullOrWhiteSpace(colName)
                    ? new TextColumn(colName)
                    : new TextColumn(String.Format("F{0}", index));

                table.AddColumn(col);
                index++;
            }
            ParameterFilter paramFilter =
                    ParameterFilter.CreateFilter(table, HSettings.Parameters);

            //build the rows from the rest of the entries
            bagEntryIndex++;
            string rowText = data[Convert.ToString(bagEntryIndex)];
            while (bagEntryIndex < data.Count)
            {
                if (!String.IsNullOrWhiteSpace(rowText))
                {
                    object[] row = new object[table.ColumnCount];
                    string[] rowVals = rowText.Split(new[] { '\n' });

                    for (int i = 0; i < rowVals.Length; i++)
                    {
                        row[i] = rowVals[i];
                    }

                    if (paramFilter == null || paramFilter.Contains(row))
                    {
                        table.AddRow(row);
                    }
                }

                bagEntryIndex++;
                rowText = data[Convert.ToString(bagEntryIndex)];
            }

            table.EndUpdate();

            return table;
        }

        public TableData TableBagToTableData(int tableIndex)
        {
            PropertyBag bag = HSettings.SelectedTable;
            if (bag == null) return null;

            TableData table
                = new TableData("Table " + Convert.ToString(tableIndex));

            int index = 0;
            if (!HSettings.UseFirstRowHeaders)
            {
                table.Columns = new List<string>(bag.Values[
                    Convert.ToString(index)].Split('\n'));
                index++;
            }
            else
            {
                int colCount = bag.Values[
                    Convert.ToString(index)].Split('\n').Length;
                for (int i = 0; i < colCount; i++)
                {
                    table.Columns.Add(String.Format("F{0}", i + 1));
                }
            }

            for (int i = index; i < bag.Values.Count; i++)
            {
                table.Rows.Add(new List<string>(
                    bag.Values[Convert.ToString(i)].Split('\n')));
            }

            return table;
        }

        private StandaloneTable BuildTable(StandaloneTable table,
            ListBoxData data)
        {
            int repeatedCols = 0;
            foreach (string col in data.TableData.Columns)
            {
                if (table.ContainsColumn(col))
                {
                    repeatedCols++;
                    table.AddColumn(new TextColumn("Column" + repeatedCols));
                }
                else
                {
                    table.AddColumn(new TextColumn(col));
                }
            }

            return table;
        }

        private StandaloneTable PopulateTable(StandaloneTable table,
            ListBoxData data)
        {
            foreach (List<string> row in data.TableData.Rows)
            {
                //don't add blank rows
                if (row.Count <= 0) continue;

                object[] newRow = row.ToArray();
                table.AddRow(newRow);
            }

            return table;
        }

        private PropertyBag GetSelectedTableForWebUrlMode()
        {
            string htmlString = HSettings.GetHtmlStreamData();
            List<ListBoxData> listBoxData = ParseAndCreateTables(htmlString);
            return SerializeTableData(
                listBoxData[HSettings.TableIndex].TableData.DataTable);
        }
    }
}