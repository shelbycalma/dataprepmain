﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.UMSPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    public partial class ConnectionPanel : UserControl
    {

        public static readonly GridLength LeftColumnWidth = new GridLength(150);

        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (UMSViewModel),
                typeof (ConnectionPanel),
                new PropertyMetadata(SettingsChanged));

        private Window owner;

        public PropertyBag GlobalSettings { get; set; }
        private static readonly string FileFilter = string.Concat(Properties.Resources.UiDialogConfigFilesFilter, "|*.*");
        public UMSViewModel ViewModel
        {
            get { return (UMSViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public ConnectionPanel():this(null, null)
        {
            InitializeComponent();
        }

        public ConnectionPanel(UMSViewModel viewModel, Window owner)
        {
            InitializeComponent();
            this.ViewModel = viewModel;
            this.owner = owner;
        }
        private void BrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            string path = DataPluginUtils.BrowseForFile(owner, ViewModel.Settings.ConfigFile,
                FileFilter, GlobalSettings);
            if (path != null)
            {
                ViewModel.Settings.ConfigFile = path;
            }
        }

        private void OnSettingsChanged(UMSViewModel newSettings)
        {
            DataContext = newSettings;
            UnicastControl.ViewModel = newSettings;
            AdvancedSettingsControl.ViewModel = newSettings;
        }

        private static void SettingsChanged(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConnectionPanel) obj).OnSettingsChanged(
                (UMSViewModel) args.NewValue);
        }

        private void UserControl_DataContextChanged(
            object sender,
            DependencyPropertyChangedEventArgs e)
        {
            ViewModel = DataContext as UMSViewModel;
        }
    }
}