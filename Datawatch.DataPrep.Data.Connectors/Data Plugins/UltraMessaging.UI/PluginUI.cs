﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.UMSPlugin.UI.Properties;

namespace Panopticon.UMSPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<
        ParameterTable, UMSSettings, Dictionary<string, object>, UMSDataAdapter, Plugin>//, IFileOpenDataPluginUI<Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner,
            "pack://application:,,,/UMSPlugin.UI;component/ums-small.png")
        {
        }

        public override object GetConfigElement(
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            var viewModel = new UMSViewModel(new UMSSettings(Plugin.PluginManager, bag, parameters));
            var panel = new ConnectionPanel();
            panel.ViewModel = viewModel;
            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel panel = (ConnectionPanel)obj;
            return panel.ViewModel.Settings.ToPropertyBag();
        }

        public override Window CreateConnectionWindow(UMSSettings settings)
        {
            var viewModel = new UMSViewModel(settings);
            return new ConnectionWindow(viewModel);
        }

        public bool CanOpen(string filePath)
        {
            return DataUtils.CanOpen(filePath, FileExtensions);
        }

        public PropertyBag DoOpenFile(string filePath)
        {
            CheckLicense();

            UMSSettings con = Plugin.CreateConnectionSettings(new ParameterValue[0]);
            con.ConfigFile = filePath;
            Window win = CreateConnectionWindow(con);

            return DataPluginUtils.ShowDialog(owner, win, con);
        }

        public PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new System.NotImplementedException();
        }

        private static readonly string[] fileExtensions = new[] { "*" };

        public string[] FileExtensions
        {
            get { return fileExtensions; }
        }

        public string FileFilter
        {
            get
            {
                return Utils.CreateFilterString(
                    Resources.UiConfigurationFiles, fileExtensions);
            }
        }
    }
}
