﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.UMSPlugin.UI
{
    public class UMSViewModel : ViewModelBase
    {
        private ICommand addCommand;

        private ICommand removeCommnad;

        public static readonly string[] TopicResolutionModes =
            new string[]{"Multicast",
                "Unicast"};

        public static readonly string[] ConfigurationTypes =
            new string[]{"Configuration File",
                "Manual Configuration"};

        public static readonly string[] ReceptionModes =
            new string[]{"Streaming",
                "Persistent",
                "Ultra Load Balancing"};

        private ObservableCollection<UnicastResolverDaemon>
            unicastResolverDaemons;

        private ObservableCollection<AdvancedSetting>
            advancedSettings;

        public UMSSettings Settings { get; set; }

        public ObservableCollection<UnicastResolverDaemon>
            UnicastResolverDaemons
        {
            get
            {
                if (unicastResolverDaemons == null)
                {
                    unicastResolverDaemons = new
                        ObservableCollection<UnicastResolverDaemon>(
                        LoadResolverDaemonsFromSettings());
                    OnPropertyChanged("UnicastResolverDaemons");
                }

                return unicastResolverDaemons;
            }
        }

        public ObservableCollection<AdvancedSetting>
            AdvancedSettings
        {
            get
            {
                if (advancedSettings == null)
                {
                    advancedSettings = new
                        ObservableCollection<AdvancedSetting>(
                        LoadAdvancedSettingsFromSettings());
                    OnPropertyChanged("AdvancedSettings");
                }
                return advancedSettings;
            }
        }

        public UMSViewModel(UMSSettings settings)
        {
            Settings = settings;
        }

        public ICommand AddCommand
        {
            get
            {
                if (addCommand == null)
                {
                    addCommand =
                        new DelegateCommand(AddExecuted,
                            AddCanExecuted);
                }
                return addCommand;
            }
        }

        private bool AddCanExecuted()
        {
            return true;
        }

        private void AddExecuted()
        {
            advancedSettings.Add(new AdvancedSetting("", "", ""));
        }

        public ICommand RemoveCommand
        {
            get
            {
                if(removeCommnad == null)
                {
                    removeCommnad =
                        new DelegateCommand<int>(RemoveExecuted,
                            RemoveCanExecuted);
                }
                return removeCommnad;
            }
        }

        private bool RemoveCanExecuted(int index)
        {
            if(index >= 0)
            {
                return true;
            }
            return false;
        }

        private void RemoveExecuted(int index)
        {
            advancedSettings.RemoveAt(index);
        }

        public void AddUnicastResolverDaemon()
        {
            unicastResolverDaemons.Add(new UnicastResolverDaemon("", ""));
        }

        public void RemoveUnicastResolverDaemon(int index)
        {
            unicastResolverDaemons.RemoveAt(index);
        }

        public List<UnicastResolverDaemon> LoadResolverDaemonsFromSettings()
        {
            return Settings.LoadResolverDaemons();
        }

        public void WriteResolverDaemonsToSettings()
        {
            var daemonBuilder = new StringBuilder();

            bool first = true;
            foreach (var daemon in UnicastResolverDaemons)
            {
                if (!first)
                {
                    daemonBuilder.Append(";");
                }
                daemonBuilder.Append(string.Format("{0}:{1}", daemon.IpAddress,
                    daemon.Port));

                first = false;
            }

            Settings.UnicastResolverDaemons = daemonBuilder.ToString();
        }

        public void RemoveAdvancedSetting(int index)
        {
            advancedSettings.RemoveAt(index);
        }

        public List<AdvancedSetting>
            LoadAdvancedSettingsFromSettings()
        {
            return Settings.LoadAdvancedSettings();
        }

        public void WriteAdvancedSettingsToSettings()
        {
            Settings.AdvancedSettingsBag.Clear();
            foreach (var setting in AdvancedSettings)
            {
                Settings.AdvancedSettingsBag.Values[setting.Attribute] =
                    setting.CreatePropertyString();
            }
        }
    }
}