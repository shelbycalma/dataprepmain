﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.UMSPlugin.UI
{
    /// <summary>
    /// Interaction logic for AdvancedSettingsControl.xaml
    /// </summary>
    public partial class AdvancedSettingsControl : UserControl
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (UMSViewModel),
                typeof (AdvancedSettingsControl),
                new PropertyMetadata(SettingsChanged));

        public UMSViewModel ViewModel
        {
            get { return (UMSViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public AdvancedSettingsControl()
        {
            InitializeComponent();
        }

        private void OnSettingsChanged(UMSViewModel newSettings)
        {
            DataContext = newSettings;
        }

        private static void SettingsChanged(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((AdvancedSettingsControl) obj).OnSettingsChanged(
                (UMSViewModel) args.NewValue);
        }

        private void AdvancedSettingsControl_OnLostFocus(
            object sender,
            RoutedEventArgs e)
        {
            ViewModel.WriteAdvancedSettingsToSettings();
        }
    }
}