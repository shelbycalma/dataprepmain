﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Panopticon.UMSPlugin.UI
{
    [ValueConversion(typeof (IConvertible), typeof (Visibility))]
    public class EnumToVisibilityConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            if (value is ConfigurationType)
            {
                var val1 = (ConfigurationType) value;
                var val2 = (ConfigurationType) parameter;

                return (val1 == val2
                    ? Visibility.Visible
                    : Visibility.Collapsed);
            }
            else if (value is TopicResolutionMode)
            {
                var val1 = (TopicResolutionMode) value;
                var val2 = (TopicResolutionMode) parameter;

                return (val1 == val2
                    ? Visibility.Visible
                    : Visibility.Collapsed);
            }
            else
            {
                var val1 = (ReceptionMode) value;
                var val2 = (ReceptionMode) parameter;

                return (val1 == val2
                    ? Visibility.Visible
                    : Visibility.Collapsed);
            }
        }

        public object ConvertBack(
            object value, Type targetType,
            object parameter,
            CultureInfo culture)
        {
            return null;
        }
    }
}