﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.UMSPlugin.UI
{
    [ValueConversion(typeof (ConfigurationType), typeof (string))]
    public class ConfigurationTypeToStringConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            switch ((ConfigurationType) value)
            {
                case ConfigurationType.ConfigurationFile:
                    return "Configuration File";
                    break;
                case ConfigurationType.ManualConfiguration:
                    return "Manual Configuration";
                    break;
                default:
                    return null;
            }
        }

        public object ConvertBack(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            switch ((string) value)
            {
                case "Configuration File":
                    return ConfigurationType.ConfigurationFile;
                    break;
                case "Manual Configuration":
                    return ConfigurationType.ManualConfiguration;
                    break;
                default:
                    return null;
            }
        }
    }
}