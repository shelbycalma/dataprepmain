﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.UMSPlugin.UI
{
    [ValueConversion(typeof (ReceptionMode), typeof (string))]
    public class ReceptionModeToStringConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            switch ((ReceptionMode) value)
            {
                case ReceptionMode.Streaming:
                    return "Streaming";
                    break;
                case ReceptionMode.Persistent:
                    return "Persistent";
                    break;
                case ReceptionMode.UltraLoadBalancing:
                    return "Ultra Load Balancing";
                    break;
                default:
                    return null;
            }
        }

        public object ConvertBack(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            switch ((string) value)
            {
                case "Streaming":
                    return ReceptionMode.Streaming;
                    break;
                case "Persistent":
                    return ReceptionMode.Persistent;
                    break;
                case "Ultra Load Balancing":
                    return ReceptionMode.UltraLoadBalancing;
                default:
                    return null;
            }
        }
    }
}