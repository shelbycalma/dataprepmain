﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.UMSPlugin.UI
{
    [ValueConversion(typeof (string), typeof (bool))]
    internal class StringToBoolConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            string val1 = (string) parameter;
            string val2 = (string) value;
            return val1 == val2;
        }

        public object ConvertBack(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            return (string) parameter;
        }
    }
}