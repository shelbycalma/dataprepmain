﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.UMSPlugin.UI
{
    [ValueConversion(typeof (string), typeof (TopicResolutionMode))]
    public class TopicResolutionModeToStringConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            switch ((TopicResolutionMode) value)
            {
                case TopicResolutionMode.Multicast:
                    return "Multicast";
                    break;
                case TopicResolutionMode.Unicast:
                    return "Unicast";
                    break;
                default:
                    return null;
            }
        }

        public object ConvertBack(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            switch ((string) value)
            {
                case "Multicast":
                    return TopicResolutionMode.Multicast;
                    break;
                case "Unicast":
                    return TopicResolutionMode.Unicast;
                    break;
                default:
                    return null;
            }
        }
    }
}