﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.UMSPlugin.UI
{
    /// <summary>
    /// Interaction logic for UnicastAddRemoveControl.xaml
    /// </summary>
    public partial class UnicastAddRemoveControl : UserControl
    {
        public static readonly DependencyProperty ViewModelProperty =
            DependencyProperty.Register("ViewModel", typeof (UMSViewModel),
                typeof (UnicastAddRemoveControl),
                new PropertyMetadata(SettingsChanged));

        public UMSViewModel ViewModel
        {
            get { return (UMSViewModel) GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public UnicastAddRemoveControl()
        {
            InitializeComponent();
        }

        private void OnSettingsChanged(UMSViewModel newSettings)
        {
            DataContext = newSettings;
        }

        private static void SettingsChanged(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((UnicastAddRemoveControl) obj).OnSettingsChanged(
                (UMSViewModel) args.NewValue);
        }

        private void AddButton_OnClick(object sender, RoutedEventArgs e)
        {
            ViewModel.AddUnicastResolverDaemon();
            e.Handled = true;
        }

        private void RemoveButton_OnClick(object sender, RoutedEventArgs e)
        {
            int index = ResolverDaemonGrid.SelectedIndex;
            if (index >= 0)
            {
                ViewModel.RemoveUnicastResolverDaemon(index);
            }
            e.Handled = true;
        }

        private void UnicastAddRemoveControl_OnLostFocus(
            object sender,
            RoutedEventArgs e)
        {
            ViewModel.WriteResolverDaemonsToSettings();
        }
    }
}