﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.UMSPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        public UMSViewModel ViewModel { get; set; }

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof (ConnectionWindow));

        public ConnectionWindow(UMSViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ViewModel.Settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void ClickOK(object sender, RoutedEventArgs e)
        {
            new RoutedCommand("Ok", typeof (ConnectionWindow));
        }
    }
}