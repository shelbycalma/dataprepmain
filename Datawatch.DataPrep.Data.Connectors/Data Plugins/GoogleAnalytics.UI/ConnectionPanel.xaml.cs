﻿using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.GoogleAnalyticsPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl, IDataPluginConfigElement
    {
        private SettingsViewModel viewModel;

        public static readonly DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings", typeof(GASettings),
            typeof(ConnectionPanel));
        private static readonly string FileFilter =
            string.Concat(Properties.Resources.UiP12FileFilter, "|*.p12|",
                Properties.Resources.UiJSONFileFilter, "|*.json|",
                Properties.Resources.UiAllFileFilter, "|*.*");
        private const int MAXDIMENSIONS = 7;
        private const int MAXMETRICS = 10;
        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(GASettings settings)
        {
            InitializeComponent();
            if(settings != null)
            {
                DataContext = viewModel =  new SettingsViewModel(settings);
                Settings = settings;
            }
        }

        public GASettings Settings
        {
            get { return (GASettings)GetValue(SettingsProperty); }
            set
            {
                SetValue(SettingsProperty, value);
                viewModel = new SettingsViewModel(value);
                DataContext = viewModel;
            }
        }

        public bool IsOk
        {
            get
            {
                return Settings.IsOk;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (viewModel.Settings != null)
            {
                viewModel.Settings.ClientSecret = PasswordBox.Password;
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Should pass GlobalSettingsBag, so that InitialDirectory logic
            // inside BrowseForFile works.
            string path = DataPluginUtils.BrowseForFile(Window.GetWindow(this),
                Settings.KeyFilePath, FileFilter, new PropertyBag());
            if (!string.IsNullOrEmpty(path))
            {
                Settings.KeyFilePath = path;
            }
        }

        private void DimensionListBox_MouseDoubleClick(
            object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (DimensionListBox.SelectedItem == null) return;
            if(Settings.SelectedDimensions.Count == MAXDIMENSIONS)
            {
                MessageBox.Show(Properties.Resources.ExLimitMessage,
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            viewModel.AddSelectedDimenstons(
                (ColumnViewModel)DimensionListBox.SelectedItem);
            Settings.validated = false;
        }

        private void MetricsListBox_MouseDoubleClick(
            object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (MetricsListBox.SelectedItem == null) return;
            if (Settings.SelectedMetrics.Count == MAXMETRICS)
            {
                MessageBox.Show(Properties.Resources.ExLimitMessage,
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            viewModel.AddSelectedMetrics(
                (ColumnViewModel)MetricsListBox.SelectedItem);
            Settings.validated = false;
        }

        public void OnOk()
        {            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Window w = Window.GetWindow(this);
            if (w == null)
            {
                return;
            }
            w.SizeToContent = SizeToContent.Manual;
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}
