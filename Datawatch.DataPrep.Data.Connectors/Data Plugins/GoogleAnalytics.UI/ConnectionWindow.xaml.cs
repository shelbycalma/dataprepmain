﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.GoogleAnalyticsPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionWindow.xaml
    /// </summary>
    internal partial class ConnectionWindow : Window
    {
        private GASettings settings;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConnectionWindow));

        public ConnectionWindow(GASettings settings)
        {
            this.settings = settings;
            InitializeComponent();

            ConnectionPanel.Settings = settings;
        }

        public GASettings Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            //e.CanExecute = settings.IsOk;
            e.CanExecute = settings.validated;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
