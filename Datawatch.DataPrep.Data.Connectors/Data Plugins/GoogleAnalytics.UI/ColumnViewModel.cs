﻿using Google.Apis.Analytics.v3.Data;

namespace Panopticon.GoogleAnalyticsPlugin.UI
{
    public class ColumnViewModel
    {
        private Column column;
        public ColumnViewModel(Column column)
        {
            this.column = column;
        }

        public Column Column
        {
            get
            {
                return column;
            }
        }

        public string Id
        {
            get { return column.Id; }
        }

        public string Name
        {
            get { return column.Attributes["uiName"]; }
        }

        public string Description
        {
            get { return column.Attributes["description"]; }
        }

        public string Type
        {
            get { return column.Attributes["type"]; }
        }

        public string DataType
        {
            get { return column.Attributes["dataType"]; }
        }

        public bool IsSelected { get; set; }
    }
}
