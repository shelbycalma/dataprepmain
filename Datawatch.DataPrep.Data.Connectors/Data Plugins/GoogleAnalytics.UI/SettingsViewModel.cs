﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Data;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using System.Globalization;
using System.IO;

using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Google.Apis.Analytics.v3.Data;
using Panopticon.GoogleAnalyticsPlugin.Helper;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Plugin;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;
using System.Reflection;
using System.Web.Mvc;
using Datawatch.DataPrep.Data.Core;

namespace Panopticon.GoogleAnalyticsPlugin.UI
{
    public class SettingsViewModel : ViewModelBase
    {
        private const string DimensionString = "DIMENSION";
        private const string MetricsString = "METRIC";
        private const string StatusString = "DEPRECATED";
        private Columns gaColumns;
        private Goals gaGoals;
        private string selectedCategory;
        private List<string> categories;
        private List<GADatabaseColumn> selectedColumns =
            new List<GADatabaseColumn>();
        private List<GADatabaseColumn> orderedColumns =
            new List<GADatabaseColumn>();
        private List<ColumnViewModel> dimensions;
        private List<ColumnViewModel> metrics;
        private ICollectionView categoriesCollectionView;
        private ICollectionView dimensionsCollectionView;
        private ICollectionView metricsCollectionView;

        private ICommand _previewCommand;
        private DataTable _previewTable;

        protected static readonly StringComparison filterComparison =
            StringComparison.InvariantCultureIgnoreCase;

        public event PropertyChangedEventHandler PropertyChanged;
        public GASettings Settings { get; private set; }

        private ICommand loadAllCommand;
        private ICommand deleteColumnsCommand;
        private ICommand loadGAAccounts;
        private ICommand validateColumnsCommand;
        private ICommand clearColumnsCommand;

        public SettingsViewModel(GASettings settings)
        {
            Settings = settings;
            Settings.PropertyChanged += Settings_PropertyChanged;

            if (Settings.CanFetchDimensionsAndMetrics())
            {
                LoadAll();
                getAccounts();
            }

            if ((Settings.OrderedColumns.Count == 0) && ((Settings.SelectedMetrics.Count > 0) || (Settings.SelectedDimensions.Count > 0)))
            {
                Settings.OrderedColumns.AddRange(Settings.SelectedMetrics);
                Settings.OrderedColumns.AddRange(Settings.SelectedDimensions);
            }
        }

        private void Settings_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("CategoriesFilter")
                && categoriesCollectionView != null)
            {
                categoriesCollectionView.Refresh();
            }
            if (e.PropertyName.Equals("DimensionsFilter") &&
                dimensionsCollectionView != null)
            {
                dimensionsCollectionView.Refresh();
            }
            if (e.PropertyName.Equals("MetricsFilter") &&
                metricsCollectionView != null)
            {
                metricsCollectionView.Refresh();
            }
        }

        public ICommand PreviewCommand
        {
            get
            {
                return _previewCommand ??
                       (_previewCommand = new DelegateCommand(PreviewData, CanPreview));
            }
        }

        public ICommand LoadAllCommand
        {
            get
            {
                if (loadAllCommand == null)
                {
                    loadAllCommand = new DelegateCommand(LoadAll,
                        CanLoadAll);
                }
                return loadAllCommand;
            }
        }

        public ICommand LoadGAAccounts
        {
            get
            {
                if (loadGAAccounts == null)
                {
                    loadGAAccounts = new DelegateCommand(reLoadAccounts,
                        CanGetAccounts);
                }
                return loadGAAccounts;
            }
        }

        public ICommand DeleteColumnsCommand
        {
            get
            {
                if (deleteColumnsCommand == null)
                {
                    deleteColumnsCommand = new DelegateCommand<object>
                        (DeleteColumnExecute, CanDeleteColumn);
                }
                return deleteColumnsCommand;
            }
        }

        public ICommand ClearColumnsCommand
        {
            get
            {
                if (clearColumnsCommand == null)
                {
                    clearColumnsCommand = new DelegateCommand<object>
                        (ClearColumnsExecute, CanClearColumns);
                }
                return clearColumnsCommand;
            }
        }

        public ICommand ValidateColumnsCommand
        {
            get
            {
                return validateColumnsCommand ??
                       (validateColumnsCommand = new DelegateCommand(ValidateColumn));
            }
        }

        public Columns GAColumns
        {
            get { return gaColumns; }
            set
            {
                gaColumns = value;
                OnPropertyChanged("Categories");

                OnPropertyChanged("Dimensions");
                OnPropertyChanged("Metrics");
            }
        }

        public string SelectedCategory
        {
            get { return selectedCategory; }
            set
            {
                selectedCategory = value;

                dimensions = GetCategoryColumns(DimensionString);
                dimensionsCollectionView =
                    CollectionViewSource.GetDefaultView(dimensions);
                dimensionsCollectionView.Filter = Dimensions_Filter;

                metrics = GetCategoryColumns(MetricsString);
                metricsCollectionView =
                    CollectionViewSource.GetDefaultView(metrics);
                metricsCollectionView.Filter = Metrics_Filter;

                OnPropertyChanged("Dimensions");
                OnPropertyChanged("Metrics");
            }
        }

        public List<string> Categories
        {
            get
            {
                return categories;
            }
        }

        public List<ColumnViewModel> Dimensions
        {
            get
            {
                return dimensions;
            }
        }

        public List<ColumnViewModel> Metrics
        {
            get
            {
                return metrics;
            }
        }


        public List<GADatabaseColumn> SelectedColumns
        {
            get
            {
                //selectedColumns = Settings.SelectedDimensions
                //    .Concat(Settings.SelectedMetrics).ToList();
                //return selectedColumns;
                orderedColumns = Settings.OrderedColumns.ToList();
                return orderedColumns;
            }
        }

        private List<ColumnViewModel> GetCategoryColumns(string type)
        {
            if (gaColumns == null) return null;
            List<ColumnViewModel> columns = new List<ColumnViewModel>();

            foreach (Google.Apis.Analytics.v3.Data.Column column in GAColumns.Items)
            {
                if ((column.Attributes["group"].Equals(SelectedCategory) ||
                    SelectedCategory.Equals(Properties.Resources.UiAllCategories)) &&
                   column.Attributes["type"].Equals(type) &&
                   !column.Attributes["status"].Equals(StatusString))
                {
                    if (column.Id.Contains("goalXX") || column.Id.Contains("GoalXX"))
                    {
                        if (gaGoals != null)
                        {
                            foreach (Google.Apis.Analytics.v3.Data.Goal goal in gaGoals.Items)
                            {
                                Google.Apis.Analytics.v3.Data.Column goalColumn = new Google.Apis.Analytics.v3.Data.Column();
                                TagBuilder tag = new TagBuilder("test");
                                foreach (var attr in column.Attributes)
                                {
                                    string aKey = attr.Key;
                                    string aValue = attr.Value;

                                    if (attr.Key.Equals("uiName"))
                                        tag.Attributes.Add(aKey, goal.Name + "  (" + column.Id.Replace("XX", goal.Id) + ")");
                                    else
                                        tag.Attributes.Add(aKey, aValue);
                                }
                                goalColumn.Attributes = tag.Attributes;
                                goalColumn.Id = column.Id.Replace("XX", goal.Id);
                                columns.Add(new ColumnViewModel(goalColumn));
                            }
                        }
                    }
                    else
                    {
                        columns.Add(new ColumnViewModel(column));
                    }
                }
            }
            return columns;
        }

        public IList<GAAccount> GAAccounts
        {
            get;
            private set;
        }

        public ICollection<GAProperty> GAProperties
        {
            get;
            private set;
        }

        public ICollection<GAView> GAViews
        {
            get;
            private set;
        }

        private GAAccount selectedGAAccount;
        public GAAccount SelectedGAAccount
        {
            get { return selectedGAAccount; }
            set
            {
                selectedGAAccount = value;
                if (selectedGAAccount != null)
                {
                    Settings.AccountID = SelectedGAAccount.ID;
                    OnPropertyChanged("SelectedGAAccount");
                    this.GAProperties = selectedGAAccount.Properties;
                    OnPropertyChanged("GAProperties");
                    this.GAViews = null;
                    OnPropertyChanged("GAViews");
                }
            }
        }

        private GAProperty selectedProperty;
        public GAProperty SelectedGAProperty
        {
            get { return selectedProperty; }
            set
            {
                selectedProperty = value;
                OnPropertyChanged("SelectedGAProperty");
                if (selectedProperty != null)
                {
                    Settings.PropertyID = SelectedGAProperty.ID;
                    this.GAViews = SelectedGAProperty.GAViews;
                    OnPropertyChanged("GAViews");
                }

            }
        }

        private GAView selectedGAView;
        public GAView SelectedGAView
        {
            get { return selectedGAView; }
            set
            {
                selectedGAView = value;
                OnPropertyChanged("SelectedGAView");
                if (SelectedGAView != null)
                    Settings.ProfileID = SelectedGAView.ID;
                OnPropertyChanged("ProfileID");
            }

        }
        public bool CanLoadAll()
        {
            return Settings.CanFetchDimensionsAndMetrics();
        }

        public bool CanPreview()
        {
            if (((Settings.SelectedDimensions == null || Settings.SelectedDimensions.Count <= 0)
                 && (Settings.SelectedMetrics == null || Settings.SelectedMetrics.Count <= 0)))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool CanGetAccounts()
        {
            return Settings.CanGetAccounts();
        }

        private bool IsValidDate(string t)
        {
            DateTime parsed;
            return DateTime.TryParseExact(t, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsed);
        }

        public void LoadAll()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                Settings.Title = Path.GetFileNameWithoutExtension(Settings.KeyFilePath);
                Settings.StartDate = Convert.ToDateTime(Settings.StartDate).ToString("yyyy-MM-dd");
                Settings.EndDate = Convert.ToDateTime(Settings.EndDate).ToString("yyyy-MM-dd");
                if (!IsValidDate(Settings.StartDate))
                {
                    MessageBox.Show(String.Format(Properties.Resources.UiInvalidDatatimeValue, Settings.StartDate),
                        Properties.Resources.UiConnectionWindowTitle,
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                if (!IsValidDate(Settings.EndDate))
                {
                    MessageBox.Show(String.Format(Properties.Resources.UiInvalidDatatimeValue, Settings.EndDate),
                        Properties.Resources.UiConnectionWindowTitle,
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                GAColumns = GAHelper.LoadColumns(Settings);
                gaGoals = GAHelper.GetGoals(Settings);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(
                //    string.Format(Properties.Resources.ExLoadColumn, ex.Message),
                //    Properties.Resources.UiConnectionWindowTitle,
                //    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }

            //populate categories
            if (GAColumns == null) return;
            categories = GAColumns.Items.Select(c => c.Attributes["group"])
                .Distinct().ToList();
            categories.Insert(0, Properties.Resources.UiAllCategories);

            categoriesCollectionView =
               CollectionViewSource.GetDefaultView(categories);
            categoriesCollectionView.Filter = Categories_Filter;

            OnPropertyChanged("Categories");

            if (string.IsNullOrEmpty(SelectedCategory))
            {
                SelectedCategory = Properties.Resources.UiAllCategories;
            }
        }

        public void getAccounts()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                this.GAAccounts = null;
                this.GAProperties = null;
                this.GAViews = null;
                this.SelectedGAView = null;
                this.SelectedGAProperty = null;
                this.SelectedGAAccount = null;
                this.GAAccounts = GAHelper.LoadGAAccounts(Settings);
                OnPropertyChanged("GAAccounts");
                if (Settings.ProfileID != null)
                {
                    SelectedGAAccount = this.GAAccounts.FirstOrDefault(x => x.ID == Settings.AccountID);
                    OnPropertyChanged("SelectedGAAccount");
                    SelectedGAProperty = this.GAProperties.FirstOrDefault(x => x.ID == Settings.PropertyID);
                    OnPropertyChanged("SelectedProperty");
                    SelectedGAView = this.GAViews.FirstOrDefault(x => x.ID == Settings.ProfileID);
                    OnPropertyChanged("SelectedGAView");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show((ErrorMessages.GetDisplayMessage(ex.Message).Length > 0) ? ErrorMessages.GetDisplayMessage(ex.Message) : ex.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        public void reLoadAccounts()
        {
            Settings.ProfileID = null;
            getAccounts();
        }

        public void DeleteColumnExecute(object parameter)
        {
            GADatabaseColumn databaseColumn = parameter as GADatabaseColumn;
            if (MessageBox.Show(
                string.Format(
                Properties.Resources.UiConfirmDeleteColumn,
                    databaseColumn.DisplayName),
                Properties.Resources.UiErrorMessageBoxTitle,
                MessageBoxButton.OKCancel, MessageBoxImage.Question) ==
                MessageBoxResult.OK)
            {
                Settings.DeleteColumn(databaseColumn);
                OnPropertyChanged("SelectedColumns");
                Settings.validated = false;
            }
        }

        public void ClearColumnsExecute(object parameter)
        {
            if (MessageBox.Show(
                string.Format(
                Properties.Resources.UiConfirmDeleteAll),
                Properties.Resources.UiDeleteTitle,
                MessageBoxButton.OKCancel, MessageBoxImage.Question) ==
                MessageBoxResult.OK)
            {
                    Settings.ClearColumns();
                    OnPropertyChanged("SelectedColumns");
                    Settings.validated = false;
            }
        }

        public bool CanDeleteColumn(object parameter)
        {
            if (parameter is GADatabaseColumn)
            {
                return true;
            }
            return false;
        }

        public bool CanClearColumns(object parameter)
        {
            if (Settings.SelectedDimensions.Count > 0 || Settings.SelectedMetrics.Count() > 0)
            {
                return true;
            }
            return false;
        }

        public void AddSelectedDimenstons(ColumnViewModel column)
        {
            AddSelectedColumn(Settings.SelectedDimensions, column);
        }

        public void AddSelectedMetrics(ColumnViewModel column)
        {
            AddSelectedColumn(Settings.SelectedMetrics, column);
        }

         private void AddSelectedColumn(List<GADatabaseColumn> columns,
            ColumnViewModel selectedColumn)
        {
            var cols = columns.Where(c => c.ColumnName.Equals(
                selectedColumn.Id));
            if (cols.Count() > 0)
            {
                return;
            }
            columns.Add(new GADatabaseColumn()
            {
                Type = selectedColumn.Type,
                ColumnName = selectedColumn.Id,
                ColumnType = GAHelper.GetDataType(selectedColumn.DataType),
                DisplayName = selectedColumn.Name
            });
            Settings.OrderedColumns.Add(new GADatabaseColumn()
            {
                Type = selectedColumn.Type,
                ColumnName = selectedColumn.Id,
                ColumnType = GAHelper.GetDataType(selectedColumn.DataType),
                DisplayName = selectedColumn.Name
            });
            OnPropertyChanged("SelectedColumns");
        }

        private void ValidateColumn()
        {
            Settings.StartDate = Convert.ToDateTime(Settings.StartDate).ToString("yyyy-MM-dd");
            Settings.EndDate = Convert.ToDateTime(Settings.EndDate).ToString("yyyy-MM-dd");
            if (!IsValidDate(Settings.StartDate))
            {
                MessageBox.Show(String.Format(Properties.Resources.UiInvalidDatatimeValue, Settings.StartDate),
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (!IsValidDate(Settings.EndDate))
            {
                MessageBox.Show(String.Format(Properties.Resources.UiInvalidDatatimeValue, Settings.EndDate),
                    Properties.Resources.UiConnectionWindowTitle,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (Settings.SelectedDimensions.Count > 0 || Settings.SelectedMetrics.Count > 0)
                Settings.validated = Settings.IsParamOk;
            else
                MessageBox.Show(Properties.Resources.UiNoData, Properties.Resources.UiTitle,
                    MessageBoxButton.OK,MessageBoxImage.Exclamation);
            if (Settings.IsParamOk)
                MessageBox.Show(Properties.Resources.UiValidationSuccessful, Properties.Resources.UiPluginTitle);
        }

        private bool Categories_Filter(object item)
        {
            string column = item as string;
            if (column == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(Settings.CategoriesFilter))
            {
                return true;
            }
            bool visible = column.IndexOf(
                Settings.CategoriesFilter, filterComparison) >= 0;
            return visible;
        }

        private bool Dimensions_Filter(object item)
        {
            ColumnViewModel column = item as ColumnViewModel;
            if (column == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(Settings.DimensionsFilter))
            {
                return true;
            }
            bool visible = column.Name.IndexOf(
                Settings.DimensionsFilter, filterComparison) >= 0;
            return visible;
        }

        private bool Metrics_Filter(object item)
        {
            ColumnViewModel column = item as ColumnViewModel;
            if (column == null)
            {
                return false;
            }

            if (string.IsNullOrEmpty(Settings.MetricsFilter))
            {
                return true;
            }
            bool visible = column.Name.IndexOf(
                Settings.MetricsFilter, filterComparison) >= 0;
            return visible;
        }

		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				OnPropertyChanged("PreviewTable");
			}
		}
				
		private PropertyBag CreatePropertyBag()
		{
			const string ColumnNameKey = "ColumnName";
			const string ColumnTypeKey = "ColumnType";
			const string DisplayNameKey = "DisplayName";
			const string TypeKey = "Type";
			const string AggregateTypeKey = "AggregateType";
			const string SelectedMetricsKey = "SelectedMetrics";
			const string SelectedDimensionsKey = "SelectedDimensions";
            const string SelectedColumnKey = "SelectedColumns";

        PropertyBag pb = new PropertyBag();
			//PropertyValue pv = new PropertyValue("UserName", Settings.UserName);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("ClientID", Settings.ClientID);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("AuthenticationType", Settings.AuthenticationType.ToString());
			//pb.Values.Add(pv);
			//pv = new PropertyValue("ClientSecret", Settings.ClientSecret);
			//pb.Values.Add(pv);
			PropertyValue pv = new PropertyValue("ServiceAccountEmail", Settings.ServiceAccountEmail);
			pb.Values.Add(pv);
			pv = new PropertyValue("KeyFilePath", Settings.KeyFilePath);
			pb.Values.Add(pv);
			pv = new PropertyValue("ProfileID", Settings.ProfileID);
			pb.Values.Add(pv);
			//pv = new PropertyValue("AccountID", Settings.AccountID);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("PropertyID", Settings.PropertyID);
			//pb.Values.Add(pv);
			pv = new PropertyValue("StartDate", Settings.StartDate);
			pb.Values.Add(pv);
			pv = new PropertyValue("EndDate", Settings.EndDate);
			pb.Values.Add(pv);
			//pv = new PropertyValue("Filters", Settings.Filters);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("CategoriesFilter", Settings.CategoriesFilter);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("DimensionsFilter", Settings.DimensionsFilter);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("MetricsFilter", Settings.MetricsFilter);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("MaxResults", Settings.MaxResults.ToString());
			//pb.Values.Add(pv);
			//pv = new PropertyValue("Fields", Settings.Fields);
			//pb.Values.Add(pv);
			//pv = new PropertyValue("ShowAllowedInSegmentOnly", Settings.ShowAllowedInSegmentOnly.ToString());
			//pb.Values.Add(pv);
			//pv = new PropertyValue("CanGetAccounts", Settings.CanGetAccounts().ToString());
			//pb.Values.Add(pv);
			//pv = new PropertyValue("CanFetchDimensionsAndMetrics", Settings.CanFetchDimensionsAndMetrics().ToString());
			//pb.Values.Add(pv);
			pv = new PropertyValue("Title", Settings.Title);
			pb.Values.Add(pv);

			PropertyBag MetricsBag = new PropertyBag();
			MetricsBag.Name = SelectedMetricsKey;
			for (int i = 0; i < Settings.SelectedMetrics.Count(); i++)
			{
				var value = Settings.SelectedMetrics[i];
				PropertyBag itemBag = new PropertyBag();
				itemBag.Values[DisplayNameKey] = value.DisplayName;
				itemBag.Values[ColumnNameKey] = value.ColumnName;
				itemBag.Values[ColumnTypeKey] = value.ColumnType.ToString();
				itemBag.Values[AggregateTypeKey] = value.AggregateType.ToString();
				itemBag.Values[TypeKey] = value.Type;

				itemBag.Name = String.Format("Column_{0}", i);

				MetricsBag.SubGroups.Add(itemBag);
			}
			pb.SubGroups.Add(MetricsBag);

			PropertyBag DimensionsBag = new PropertyBag();
			DimensionsBag.Name = SelectedDimensionsKey;
			for (int i = 0; i < Settings.SelectedDimensions.Count(); i++)
			{
				var value = Settings.SelectedDimensions[i];
				PropertyBag itemBag = new PropertyBag();
				itemBag.Values[DisplayNameKey] = value.DisplayName;
				itemBag.Values[ColumnNameKey] = value.ColumnName;
				itemBag.Values[ColumnTypeKey] = value.ColumnType.ToString();
				itemBag.Values[AggregateTypeKey] = value.AggregateType.ToString();
				itemBag.Values[TypeKey] = value.Type;

				itemBag.Name = String.Format("Column_{0}", i);

				DimensionsBag.SubGroups.Add(itemBag);
			}
			pb.SubGroups.Add(DimensionsBag);

            PropertyBag ColumnsBag = new PropertyBag();
            ColumnsBag.Name = SelectedColumnKey;
            for (int i = 0; i < Settings.OrderedColumns.Count(); i++)
            {
                var value = Settings.OrderedColumns[i];
                PropertyBag itemBag = new PropertyBag();
                itemBag.Values[DisplayNameKey] = value.DisplayName;
                itemBag.Values[ColumnNameKey] = value.ColumnName;
                itemBag.Values[ColumnTypeKey] = value.ColumnType.ToString();
                itemBag.Values[AggregateTypeKey] = value.AggregateType.ToString();
                itemBag.Values[TypeKey] = value.Type;

                itemBag.Name = String.Format("Column_{0}", i);

                ColumnsBag.SubGroups.Add(itemBag);
            }
            pb.SubGroups.Add(ColumnsBag);

            return pb;
		}

		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}

                Settings.StartDate = Convert.ToDateTime(Settings.StartDate).ToString("yyyy-MM-dd");
                Settings.EndDate = Convert.ToDateTime(Settings.EndDate).ToString("yyyy-MM-dd");
                if (!IsValidDate(Settings.StartDate))
                {
                    MessageBox.Show(String.Format(Properties.Resources.UiInvalidDatatimeValue, Settings.StartDate),
                        Properties.Resources.UiConnectionWindowTitle,
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (!IsValidDate(Settings.EndDate))
                {
                    MessageBox.Show(String.Format(Properties.Resources.UiInvalidDatatimeValue, Settings.EndDate),
                        Properties.Resources.UiConnectionWindowTitle,
                        MessageBoxButton.OK, MessageBoxImage.Error);
                }

                Panopticon.GoogleAnalyticsPlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				ITable t = p.GetData("", "", bag, null, previewRowLimit, true);

				if (t != null)
				{
					_previewTable = PreviewDataTable.GetPreviewTable(t);
					OnPropertyChanged("PreviewTable");
                    Settings.validated = true;
                }
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
                MessageBox.Show((ErrorMessages.GetDisplayMessage(ex.Message).Length > 0) ? ErrorMessages.GetDisplayMessage(ex.Message) : ex.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                Settings.validated = false;
            }
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}
	}
}
