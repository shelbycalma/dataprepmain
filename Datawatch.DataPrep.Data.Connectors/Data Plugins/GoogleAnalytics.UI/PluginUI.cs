﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.GoogleAnalyticsPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, GASettings>
    {
        private const string imageuri =
            "pack://application:,,,/Panopticon.GoogleAnalyticsPlugin.UI;component/Google-Analytics-Logo.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(new Uri(imageuri, UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            GASettings con = new GASettings(new PropertyBag(), parameters);

            ConnectionWindow window = new ConnectionWindow(con);

            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return window.Settings.ToPropertyBag();
            }
            return null;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            GASettings connectionSettings =
                new GASettings(bag, parameters);
            return new ConnectionPanel(connectionSettings);
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }
    }
}
