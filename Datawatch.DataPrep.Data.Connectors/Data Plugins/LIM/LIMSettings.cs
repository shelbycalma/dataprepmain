﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.LIMPlugin
{
    public enum LIMQueryType
    {
        Columns = 0,
        Series = 1,
        Curve = 2
    }

    public enum CurveOutPutType
    {
        Vertical = 0,
        Horizontal = 1
    }

    public class LIMSettings : ConnectionSettings
    {
        public LIMSettings()
        {
        }

        public LIMSettings(PropertyBag bag)
            : base(bag)
        {
        }

        public string ConnectionString
        {
            get { return GetInternal("ConnectionString"); }
            set { SetInternal("ConnectionString", value); }
        }

        public string Description
        {
            get { return GetInternal("Description"); }
            set { SetInternal("Description", value); }
        }

        public string ConnectionUserName
        {
            get { return GetInternal("ConnectionUserName"); }
            set { SetInternal("ConnectionUserName", value); }
        }

        public string ConnectionPassword
        {
            get { return GetInternal("ConnectionPassword"); }
            set { SetInternal("ConnectionPassword", value); }
        }

        public string Query
        {
            get { return GetInternal("Query"); }
            set { SetInternal("Query", value); }
        }

        public bool isQueryTypeCurve
        {
            get
            {
                if (QueryType == LIMQueryType.Curve)
                {
                    return true;
                }

                return false;

            }

        }

        public CurveOutPutType CurveOutPutType
        {
            get
            {
                string s = GetInternal("CurveOutPutType");
                if (s != null)
                {
                    try
                    {
                        return (CurveOutPutType)Enum.Parse(
                            typeof(CurveOutPutType), s);
                    }
                    catch
                    {
                    }
                }
                return CurveOutPutType.Vertical;
            }

            set
            {
                SetInternal("CurveOutPutType", value.ToString());
            }
        }

        public LIMQueryType QueryType
        {
            get
            {
                string s = GetInternal("QueryType");
                if (s != null)
                {
                    try
                    {
                        return (LIMQueryType)Enum.Parse(typeof(LIMQueryType), s);
                    }
                    catch
                    {
                    }
                }
                return LIMQueryType.Columns;
            }
            set
            {
                SetInternal("QueryType", value.ToString());

                FirePropertyChanged("isQueryTypeCurve");
            }
        }

        public LIMQueryType[] QueryTypes
        {
            get
            {
                return new LIMQueryType[] 
                    { 
                        LIMQueryType.Columns, 
                        LIMQueryType.Series,
                        LIMQueryType.Curve
                    };
            }
        }
    }
}
