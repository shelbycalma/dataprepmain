﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;
using System.Xml.Serialization;
using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.LIMPlugin
{
    class Client
    {
        private static readonly Encoding LIMEncoding = Encoding.UTF8;
        private static readonly string LIMNamespace = string.Empty;
        private static readonly string LIMPrefix = string.Empty;

        public Client(Uri webContext, string username, string password)
        {
            this.WebContext = webContext;
            this.Username = username;
            this.Password = password;
        }

        private Schema.dataRequest BuildQueryDataRequest(
            string text, int? scale)
        {
            Schema.query query = new Schema.query() { Text = text };
            XmlElement xmlQuery = ToElement(query);
            Schema.dataRequest dataRequest = new Schema.dataRequest() {
                Any = new XmlElement[] { xmlQuery},
            };
           
            return dataRequest;
        }

        private T FromStream<T>(Stream stream)
        {
            XmlTextReader reader = new XmlTextReader(stream);
            XmlSerializer serializer =
                new XmlSerializer(typeof(T), LIMNamespace);
            object value = serializer.Deserialize(reader);
            return (T) value;
        }

        public Schema.dataRequestResponse Query(string text, int? scale)
        {
            Log.Info(Properties.Resources.LogExecutingQueryInfo, text);

            Schema.dataRequest request = BuildQueryDataRequest(text, scale);
            
            Schema.dataRequestResponse response =
                PostMessage<Schema.dataRequest, Schema.dataRequestResponse>(
                    "datarequests", request);
            
            if (response.status == 200) {
                long id = response.id;
                Log.Info(Properties.Resources.LogLongRunningQueryInfo, id);
                while (response.status == 200) {
                    Thread.Sleep(300);
                    string path = string.Format("datarequests/{0}", id);
                    response = GetMessage<Schema.dataRequestResponse>(path);
                }
            }

            Log.Info(Properties.Resources.LogQueryCompletedInfo);

            if (response.status != 100) {
                throw new Exception(response.statusMsg);
            }

            

            return response;
        }

        private HttpWebRequest CreateRequest(string requestPath)
        {
            Uri requestUri = new Uri(this.WebContext, requestPath);
            HttpWebRequest httpRequest =
                (HttpWebRequest) WebRequest.Create(requestUri);

            httpRequest.ProtocolVersion = new Version(1, 0);
            httpRequest.ContentType = "text/xml";
            
            NetworkCredential credential =
                new NetworkCredential(this.Username, this.Password);
            CredentialCache cache = new CredentialCache();
            cache.Add(this.WebContext, "Basic", credential);

            httpRequest.Credentials = cache;
            httpRequest.PreAuthenticate = true;

            return httpRequest;
        }

        public string Username { get; set; }

        public string Password { get; set; }

        private TResponse PostMessage<TRequest, TResponse>(
            string path, TRequest request)
        {
            HttpWebRequest httpRequest = CreateRequest(path);
            httpRequest.Method = "POST";

            byte[] bytes = ToBinary(request);
            httpRequest.ContentLength = bytes.Length;
            Stream requestStream = httpRequest.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            HttpWebResponse httpResponse =
                (HttpWebResponse) httpRequest.GetResponse();
            Stream responseStream = httpResponse.GetResponseStream();
            TResponse response = FromStream<TResponse>(responseStream);
            responseStream.Close();

            return response;
        }

        private TResponse GetMessage<TResponse>(string path)
        {
            HttpWebRequest httpRequest = CreateRequest(path);
            httpRequest.Method = "GET";

            HttpWebResponse httpResponse =
                (HttpWebResponse) httpRequest.GetResponse();
            Stream responseStream = httpResponse.GetResponseStream();
            TResponse response = FromStream<TResponse>(responseStream);
            responseStream.Close();

            return response;
        }

        private byte[] ToBinary<T>(T value)
        {
            MemoryStream stream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(stream, LIMEncoding);
            XmlSerializer serializer =
                new XmlSerializer(typeof(T), LIMNamespace);
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add(LIMPrefix, LIMNamespace);
            serializer.Serialize(writer, value, namespaces);
            writer.Close();
            return stream.ToArray();
        }

        private XmlElement ToElement<T>(T value)
        {
            byte[] bytes = ToBinary<T>(value);
            char[] chars = LIMEncoding.GetChars(bytes);
            string xml = new string(chars);
            MemoryStream stream = new MemoryStream(bytes);
            StreamReader reader = new StreamReader(stream, LIMEncoding);
            XmlReader xmlReader = XmlTextReader.Create(reader);
            XmlDocument document = new XmlDocument();
            document.Load(xmlReader);
            XmlElement element = document.DocumentElement;
            return element;
        }

        public Uri WebContext { get; private set; }
    }
}
