﻿using System;
using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.LIMPlugin
{
    internal class TableBuilder
    {

        internal static StandaloneTable GetTable(Schema.reportBlock block,
            int maxRows,string timeColFormat)
        {
            
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try
            {
                //add columns

                //1st column would always be time column
                table.AddColumn(new TimeColumn("Time"));

                if (!string.IsNullOrEmpty(timeColFormat))
                {
                    ((IMutableTimeColumnMetaData) 
                        table.GetColumn("Time").MetaData).Format = timeColFormat;
                }

                for (int i = 0; i < block.ColumnHeadingIndices.Count(); i++)
                {
                    string colName = block.ColumnHeadings[
                        block.ColumnHeadingIndices[i]];
                    if (!table.ContainsColumn(colName))
                    {
                        table.AddColumn(new NumericColumn(colName));
                    }
                    else
                    {
                        table.AddColumn(new NumericColumn(
                            "Field" + (table.ColumnCount + 1)));
                    }
                }

                //add rows
                for (int i = 0; i < block.numRows; i++)
                {
                    if (maxRows > -1 && table.RowCount >= maxRows) break;

                    Row r = table.AddRow();

                    DateTime time = block.RowDates[i];
                    if (block.RowTimes != null)
                    {
                        DateTime t = block.RowTimes[i];
                        time.Add(t.TimeOfDay);
                    }
                    //add time value to the 1st column
                    ((TimeColumn) table.GetColumn("Time")).SetTimeValue(r, time);

                    for (int j = 0; j < block.numCols; j++)
                    {
                        int k = i * block.numCols + j;
                        ((NumericColumn) table.GetColumn(j + 1)).SetNumericValue(r,
                                    block.Values[k]);
                    }
                }
            }
            finally {
                table.EndUpdate();
            }
            return table;
        }

        internal static StandaloneTable GetTableForHorizonCurve(
            Schema.reportBlock block,
            int maxRows)
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try
            {
                table.AddColumn(new TimeColumn("Time"));

                for (int i = 0; i < block.numRows; i++)
                {
                    DateTime time = block.RowDates[i];
                    if (block.RowTimes != null)
                    {
                        DateTime t = block.RowTimes[i];
                        time.Add(t.TimeOfDay);
                    }

                    table.AddColumn(new NumericColumn(
                           time.ToString("MMM-yy")));

                }

                for (int i = 0; i < block.numCols; i++)
                {
                    if (maxRows > -1 && table.RowCount >= maxRows) break;

                    Row r = table.AddRow();
                    DateTime time;
                    if (DateTime.TryParse(block.ColumnHeadings[i], out time))
                    {
                        //add time value to the 1st column
                        ((TimeColumn) table.GetColumn("Time")).SetTimeValue(r, time);
                        for (int j = 0; j < block.numRows; j++)
                        {
                            int k = i * block.numCols + j;
                            ((NumericColumn) table.GetColumn(j + 1)).SetNumericValue(r,
                                    block.Values[k]);
                        }
                    }
                }
            }
            finally {
                table.EndUpdate();
            }

            return table;
        }

        internal static StandaloneTable GetTableForSeries(
            Schema.reportBlock block,
            int maxRows)
        {
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try
            {
                //add three columns
                table.AddColumn(new TimeColumn("Time"));
                table.AddColumn(new TextColumn("SeriesTitle"));
                table.AddColumn(new NumericColumn("SeriesValue"));
                       
                int RowDateCount = 0;
            
                for (int i = 0; i < block.numRows; i++)
                {
                    List<string> tempseriesList = new List<string>();

                    for (int j = 0; j < block.ColumnHeadingIndices.Count(); j++)
                    {
                        if (maxRows > -1 && table.RowCount >= maxRows)
                        {
                            //break the outer loop
                            i = block.numRows;

                            break;
                        }
                        Row r = table.AddRow();

                        DateTime time = block.RowDates[RowDateCount];
                        if (block.RowTimes != null)
                        {
                            DateTime t = block.RowTimes[RowDateCount];
                            time.Add(t.TimeOfDay);
                        }
                        //time
                        ((TimeColumn) table.GetColumn("Time")).SetTimeValue(r, time);

                        //seriestitle
                        string seriesTitle = block.ColumnHeadings[
                        block.ColumnHeadingIndices[j]];

                        if (tempseriesList.Contains(seriesTitle))
                        {
                            seriesTitle = "Feild" + (j + 1);
                        }

                        tempseriesList.Add(seriesTitle);
                    
                        ((TextColumn) table.GetColumn("SeriesTitle")).SetValue(r,
                            seriesTitle);

                        //series value
                        int k = i * block.numCols + j;
                        ((NumericColumn) table.GetColumn("SeriesValue")).SetNumericValue(r,
                                    block.Values[k]);

                    }
             
                    RowDateCount++;
                }
            }
            finally {
                table.EndUpdate();
            }

            return table;
        }
    }
}
