﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.LIMPlugin.Properties;

namespace Panopticon.LIMPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<LIMSettings>
    {
        internal const string PluginId = "LIM";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "LIM";
        internal const string PluginType = DataPluginTypes.Database;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override LIMSettings CreateSettings(PropertyBag
            properties)
        {
            LIMSettings settings = new LIMSettings(properties);
            if (string.IsNullOrEmpty(settings.Title))
            {
                settings.Title = Properties.Resources.UiDefaultConnectionTitle;
            }
            return settings;
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag propertyBagSettings, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            CheckLicense();

            DateTime start = DateTime.Now;

            LIMSettings settings = new LIMSettings(propertyBagSettings);

            Client client = new Client(new Uri(settings.ConnectionString),
               settings.ConnectionUserName, settings.ConnectionPassword);
            StandaloneTable table = null;

            string query = ReplaceParameters(settings.Query, parameters);
            try
            {
                Schema.dataRequestResponse response =
                    client.Query(query, null);
                if (response.Reports.Length == 0)
                {
                    throw new Exception(Properties.Resources.ExNoReports);
                }
                if (response.Reports.Length > 1)
                {
                    throw new Exception(string.Format(
                        Properties.Resources.ExMultipleReports,
                        response.Reports.Length));
                }

                if (settings.QueryType == LIMQueryType.Series)
                {
                    table = TableBuilder.GetTableForSeries(
                   response.Reports[0].ReportBlocks[0], maxRowCount);
                }
                else if (settings.QueryType == LIMQueryType.Curve &&
                    settings.CurveOutPutType == CurveOutPutType.Horizontal)
                {
                    table = TableBuilder.GetTableForHorizonCurve(
                    response.Reports[0].ReportBlocks[0], maxRowCount);
                }
                else if (settings.QueryType == LIMQueryType.Curve &&
                    settings.CurveOutPutType == CurveOutPutType.Vertical)
                {
                    table = TableBuilder.GetTable(
                    response.Reports[0].ReportBlocks[0], maxRowCount, "MMM-yy");
                }
                else
                {
                    table = TableBuilder.GetTable(
                    response.Reports[0].ReportBlocks[0], maxRowCount, "");
                }
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                throw ex;

            }

            // We're creating a new StandaloneTable on every request, so allow
            // EX to freeze it.
            table.BeginUpdate();
            try
            {
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
            }

            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Resources.LogQueryResultSizeAndTime,
                table.RowCount, table.ColumnCount, seconds);

            return table;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
        private static string ReplaceParameters(string query,
            IEnumerable<ParameterValue> parameters)
        {
            if (parameters == null) return query;
            return new ParameterEncoder
            {
                SourceString = query,
                Parameters = parameters,
            }.Encoded();
        }
    }
}

