﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.ActiveMQPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConnectionPanel.xaml
    /// </summary>
    internal partial class ConnectionPanel : UserControl
    {
        private ActiveMQSettings settings;

        public ConnectionPanel()
            : this(null)
        {
        }

        public ConnectionPanel(ActiveMQSettings settings)
        {
            InitializeComponent();
            DataContext = this.settings = settings;
            DataContextChanged += 
                UserControl_DataContextChanged;
        }

        public ActiveMQSettings Settings
        {
            get { return settings; }
        }

        protected void OnConnectionChanged(ActiveMQSettings newConnection)
        {
            DataContext = newConnection;

            if (newConnection != null)
            {
                PasswordBox.Password = newConnection.Password;
            }
            else
            {
                PasswordBox.Password = null;
            }
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void UserControl_DataContextChanged(object sender,
            DependencyPropertyChangedEventArgs e)
        {
            settings = DataContext as ActiveMQSettings;
        }

    }
}
