﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.ActiveMQPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, ActiveMQSettings,
                       Dictionary<string, object>, ActiveMQDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner,
                  "pack://application:,,,/Panopticon.ActiveMQPlugin.UI;component/activemq-small.png")
        {
        }

        public override Window CreateConnectionWindow(ActiveMQSettings settings)
        {
            return new ConnectionWindow(settings);
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return new ConnectionPanel(new ActiveMQSettings(Plugin.PluginManager, bag, parameters));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConnectionPanel cp = (ConnectionPanel)obj;
            return cp.Settings.ToPropertyBag();
        }
    }
}
