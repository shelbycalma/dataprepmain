using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.XmlFilePlugin
{
    public class XmlFileSettings : TextSettingsBase
    {
        private const string Xml = "Xml";

        public XmlFileSettings(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public XmlFileSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        public override PathType FilePathType
        {
            get
            {
                string s = GetInternal("XmlFilePathType");
                if (s != null)
                {
                    try
                    {
                        return (PathType)Enum.Parse(
                            typeof(PathType), s);
                    }
                    catch
                    {
                    }
                }
                return PathType.File;
            }
            set
            {

                SetInternal("XmlFilePathType", value.ToString());
            }
        }

        protected override string DefaultParserPluginId
        {
            get { return Xml; }
        }

        public string RecordsPath
        {
            get
            {
                return GetInternal("RecordsXpath");

            }
            set
            {
                SetInternal("RecordsXpath", value);
            }
        }
        
        public bool IsOK
        {
            get
            {
                if (!IsParserSettingsOk)
                {
                    return false;
                }
                return IsPathOk();
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is XmlFileSettings)) return false;

            if (!string.Equals(RecordsPath, ((XmlFileSettings)obj).RecordsPath))
            {
                return false;
            }

            if (!base.Equals(obj)) return false;

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            hashCode ^= RecordsPath.GetHashCode();

            return hashCode;
        }

        public override bool CanGenerateColumns()
        {
            return IsPathOk();
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        public override void DoGenerateColumns()
        {
            try
            {
                StreamReader streamReader
                    = DataPluginUtils.GetStream(this, Parameters, this.FilePath);
                XPathParser parser = (XPathParser) ParserSettings.
                    CreateParser();
                XmlNodeList xmlRows = parser.GetRecords(streamReader, RecordsPath);
                if (xmlRows.Count == 0)
                {
                    throw new Exception(string.Format(
                        Properties.Resources.UiColumnGenerationWrongRecordPath, RecordsPath));
                }
                ColumnGenerator cg = new ColumnGenerator(parserSettings, NumericDataHelper);

                int i = 0;
                foreach (XmlElement element in xmlRows)
                {
                    if (i < 10)
                        cg.Generate(element.OuterXml);
                    else
                        break;
                    i++;
                }
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.ExFailedColumnGeneration);
                Log.Exception(e);

                this.errorReporter.Report(
                    Properties.Resources.ExError,
                    Properties.Resources.ExFailedColumnGeneration +
                        "\nException: "+ e.Message);
            }
        }

        public override bool ShowFilter
        {
            get
            {
                return false;
            }
        }

    }
}
