﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Xml;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.XmlFilePlugin.Properties;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.XmlFilePlugin
{
    /// <summary>
    /// DataPlugin for XML.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : TextPluginBase<XmlFileSettings>
    {
        internal const string PluginId = "XML";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "XML";
        internal const string PluginType = DataPluginTypes.File;

        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }
        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag propertyBagSettings,
            IEnumerable<ParameterValue> parameters, int maxRowCount,
            bool applyRowFilteration)
        {
            CheckLicense();

            XmlFileSettings settings = 
                new XmlFileSettings(pluginManager, propertyBagSettings, parameters);

            //Handle File Path
            string fixedFilePath = null;
            if (settings.FilePathType == PathType.File)
            {
                fixedFilePath = DataUtils.FixFilePath(workbookDir, dataDir,
                    settings.FilePath, parameters);
                if (!settings.IsFilePathParameterized)
                {
                    settings.FilePath = fixedFilePath;
                }
            }

            StreamReader streamReader = null;
            StandaloneTable table = new StandaloneTable();
            table.BeginUpdate();
            try
            {
                DataPluginUtils.AddColumns(table, settings.ParserSettings.Columns);
                
                XPathParser parser = (XPathParser)settings.ParserSettings.
                        CreateParser();
                DateTime start = DateTime.Now;

                streamReader = DataPluginUtils.GetStream(settings,
                    parameters, fixedFilePath);
                // Read up all the rows
                XmlNodeList xmlRows = parser.GetRecords(streamReader, 
                    settings.RecordsPath);
                ParameterFilter parameterFilter = null;
                if (applyRowFilteration)
                {
                    parameterFilter =
                        ParameterFilter.CreateFilter(table, parameters);
                }
                // Add the rows
                foreach (XmlElement element in xmlRows)
                {
                    if (maxRowCount > -1 && table.RowCount >= maxRowCount) break;
                    Dictionary<string, object> dict =
                        parser.Parse(element);
                    DataPluginUtils.AddRow(dict, table, settings.NumericDataHelper,
                        parameterFilter, settings.ParserSettings);
                }

                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime,
                    table.RowCount, table.ColumnCount, seconds);
                // We're creating a new StandaloneTable on every call, so allow
                // EX to freeze it.
                table.CanFreeze = true;
            }
            finally
            {
                if (streamReader != null)
                {
                    try
                    {
                        streamReader.Close();
                    }
                    catch (Exception e)
                    {
                        // Couldn't close the stream! Write warning in log.
                    }
                }
                table.EndUpdate();
            }
            return table;
        }

        public override XmlFileSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new XmlFileSettings(pluginManager, new PropertyBag(), parameters);
        }

        public override XmlFileSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new XmlFileSettings(pluginManager, bag, null);
        }
    }
}
