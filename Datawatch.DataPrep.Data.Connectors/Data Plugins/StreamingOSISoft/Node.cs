﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;

namespace Panopticon.StreamingOSISoftPlugin
{
    public class Node : INotifyPropertyChanged
    {
        #region constructor

        public Node(string name, Action selectionChangedAction = null)
        {
            Path = Name = name;
            Children = new ObservableCollection<Node>();
            if (selectionChangedAction != null)
                SelectionChangedAction = selectionChangedAction;
            SelectionChanged += OnSelectionChanged;
        }

        ~Node()
        {
            if (SelectionChanged != null)
                SelectionChanged -= OnSelectionChanged;
        }

        #endregion

        #region Properties

        public delegate void OnSelectionChangedHandler();

        public event OnSelectionChangedHandler SelectionChanged;

        public void OnSelectionChanged()
        {
            if (SelectionChangedAction != null)
                SelectionChangedAction();
        }

        public List<string> SelectedPaths
        {
            get { return GetPathList(); }
        }

        public bool? IsChecked
        {
            get { return _isChecked; }
            set
            {
                this.SetIsChecked(value, true, true);
                ThrowSelectionChangedEvent();
            }
        }

        private void ThrowSelectionChangedEvent()
        {
            var root = GetRoot();
            if (root != null && root.SelectionChanged != null)
                root.SelectionChanged();
        }

        public Action SelectionChangedAction { get; private set; }
        public string Name { get; private set; }
        public ObservableCollection<Node> Children { get; private set; }
        public Node Parent { get; private set; }
        public string Path { get; private set; }

        public string Id
        {
            get { return GetHashCode().ToString(CultureInfo.InvariantCulture); }
        }

        private bool? _isChecked = false;

        #endregion

        #region Methods

        private void OnPropertyChanged(string prop)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void AddNode(Node child)
        {
            child.Parent = this;
            child.Path = String.Format(@"{0}\{1}", Path, child.Name);
            Children.Add(child);
        }

        private static void WalkTree(
            Node node, string nodePath, ref List<string> paths)
        {
            if (node.IsChecked == true)
            {
                paths.Add(nodePath + (node.Children.Count > 0 ? "\\" : ""));
                return;
            }
            if (node.IsChecked == false)
                return;
            foreach (var child in node.Children)
                WalkTree(child, nodePath + "\\" + child.Name, ref paths);
        }

        public Node GetRoot()
        {
            Node root = this;
            while (root.Parent != null)
                root = root.Parent;
            return root;
        }

        public List<string> GetPathList()
        {
            Node root = this;
            while (root.Parent != null)
                root = root.Parent;
            var paths = new List<string>();
            WalkTree(root, root.Name, ref paths);
            return paths;
        }

        #region IsChecked

        private void SetIsChecked(
            bool? value, bool updateChildren, bool updateParent)
        {
            if (value == _isChecked)
                return;

            _isChecked = value;

            if (updateChildren && _isChecked.HasValue)
            {
                foreach (var child in this.Children)
                {
                    child.SetIsChecked(_isChecked, true, false);
                }
            }

            if (updateParent && Parent != null)
                Parent.VerifyCheckState();

            OnPropertyChanged("IsChecked");
        }

        private void VerifyCheckState()
        {
            bool? state = null;
            for (int i = 0; i < this.Children.Count; ++i)
            {
                bool? current = this.Children[i].IsChecked;
                if (i == 0)
                {
                    state = current;
                }
                else if (state != current)
                {
                    state = null;
                    break;
                }
            }
            this.SetIsChecked(state, false, true);
        }

        #endregion

        #endregion
    }
}