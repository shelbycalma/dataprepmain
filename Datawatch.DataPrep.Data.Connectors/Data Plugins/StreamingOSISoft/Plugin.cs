﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.StreamingOSISoftPlugin
{
    [LicenseProvider(typeof (DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin :
        MessageQueuePluginBase<ParameterTable, StreamingOSISoftSettings,
            Dictionary<string, object>, StreamingOSISoftDataAdapter>
    {
        internal const string PluginId = "OSISoftStreaming";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "OSISoft - Streaming";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        #region General Methods

        public StreamingOSISoftSettings CreateConnectionSettings()
        {
            return new StreamingOSISoftSettings(pluginManager);
        }

        public override StreamingOSISoftSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new StreamingOSISoftSettings(pluginManager, new PropertyBag(),
                parameters);
        }

        public override StreamingOSISoftSettings CreateConnectionSettings(
            PropertyBag bag)
        {
            StreamingOSISoftSettings settings
                = new StreamingOSISoftSettings(pluginManager, bag);

            StreamingOSISoftHelper helper = 
                new StreamingOSISoftHelper(settings, this.errorReporter);
            helper.Connect();
            helper.CreateColumns();

            return settings;
        }

        #endregion
    }
}