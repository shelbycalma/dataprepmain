﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Data;

namespace Panopticon.StreamingOSISoftPlugin
{
    public class StreamingOSISoftDataAdapter :
        MessageQueueAdapterBase<ParameterTable,
            StreamingOSISoftSettings, Dictionary<string, object>>
    {
        #region PrivateVariables

        private CancellationTokenSource cancelToken;
        private Task task;
        private StreamingOSISoftHelper osiHelper;
        private int messageCounter;

        #endregion

        #region Properties

        #endregion

        #region Constructors

        public StreamingOSISoftDataAdapter()
        {
        }

        #endregion

        #region GeneralMethods

        public override void Start()
        {
            base.Start();

            osiHelper = new StreamingOSISoftHelper(settings, this.ErrorReporter);
            osiHelper.Connect();

            cancelToken = new CancellationTokenSource();
            task = new Task(StartListening);
            task.Start();
        }

        public override void Stop()
        {
            base.Stop();
            cancelToken.Cancel();
            task = null;
        }

        private void StartListening()
        {
            AFDataPipe pipe = new AFDataPipe();

            AFAttributeList attObjList =
                (AFAttributeList) osiHelper
                    .GetAllSelectedAttributes(table.Parameters);
            pipe.AddSignups(attObjList);

            while (!cancelToken.IsCancellationRequested)
            {
                //pickup events and create dictionaries to enqueue inserts
                AFListResults<AFAttribute, AFDataPipeEvent> results
                    = pipe.GetUpdateEvents();

                foreach (AFDataPipeEvent pipeEvent in results)
                {
                    AFAttribute eventAttr = pipeEvent.Value.Attribute;

                    Dictionary<string, object> rowDict
                        = new Dictionary<string, object>
                        {
                            {"Database", settings.Database},
                            {"PI System", settings.SystemName},
                            {"Attribute", pipeEvent.Value.Attribute.Name},
                            {"TimeStamp", pipeEvent.Value.Timestamp.LocalTime},
                            {"Event Path", eventAttr.GetPath()},
                            {"Element", eventAttr.Element.Name}
                        };

                    rowDict.Add(eventAttr.Name,
                        pipeEvent.Value.Value.ToString());

                    messageCounter++;
                    updater.EnqueueUpdate(messageCounter.ToString(),
                        new Dictionary<string, object>(rowDict));
                }

                Thread.Sleep(500);
            }
        }

        #endregion
    }
}