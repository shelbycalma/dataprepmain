﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using OSIsoft.AF;
using OSIsoft.AF.Asset;

namespace Panopticon.StreamingOSISoftPlugin
{
    public class StreamingOSISoftHelper
    {
        private IPluginErrorReportingService errorReporter;

        #region Constructors

        public StreamingOSISoftHelper(StreamingOSISoftSettings settings, 
            IPluginErrorReportingService errorReporter)
        {
            OsiSettings = settings;
            Systems = new PISystems(true);
            this.errorReporter = errorReporter;
        }

        #endregion

        #region Properties

        public bool IsConnected
        {
            get { return System != null && System.ConnectionInfo.IsConnected; }
        }

        public StreamingOSISoftSettings OsiSettings { get; set; }

        public AFDatabase Database
        {
            get
            {
                try
                {
                    return System.Databases[OsiSettings.Database];
                }
                catch
                {
                    return null;
                }
            }
        }

        public PISystems Systems { get; set; }

        public PISystem System { get; set; }

        #endregion

        //General Methods
        public bool Connect()
        {
            if (String.IsNullOrWhiteSpace(OsiSettings.SystemName)) return false;

            System = Systems[OsiSettings.SystemName];

            if (System == null) return false;
            if (System.ConnectionInfo.IsConnected) return true;

            try
            {
                if (OsiSettings.UseWindowsAuthentication)
                {
                    System.Connect();
                }
                else
                {
                    string paramUser =
                        OsiSettings.ParameterizeValue(OsiSettings.UserName);
                    string paramPass =
                        OsiSettings.ParameterizeValue(OsiSettings.Password);

                    System.Connect(new NetworkCredential(paramUser, paramPass));
                }
            }
            catch (Exception e)
            {
                Log.Error(Properties.Resources.ConnectionError);
                Log.Exception(e);
                this.errorReporter.Report("Error",
                    Properties.Resources.ConnectionError + ".\n"
                        + "Exception: " + e.Message);
            }

            return IsConnected;
        }

        public void Disconnect()
        {
            if (System != null && System.ConnectionInfo.IsConnected)
                System.Disconnect();
        }

        public void CreateColumns()
        {
            ParserSettings parserSettings
                = OsiSettings.ParserSettings;

            List<ColumnDefinition> colDefs = new List<ColumnDefinition>();

            string[] textColNames =
            {
                "Element", "Attribute",
                "PI System", "Database", "Event Path"
            };

            foreach (string colName in textColNames)
            {
                ColumnDefinition colDef
                    = parserSettings.CreateColumnDefinition(new PropertyBag());
                colDef.Name = colName;
                colDef.Type = ColumnType.Text;
                colDefs.Add(colDef);
            }

            ColumnDefinition timeColDef
                = parserSettings.CreateColumnDefinition(new PropertyBag());
            timeColDef.Name = "TimeStamp";
            timeColDef.Type = ColumnType.Time;
            colDefs.Add(timeColDef);

            string[] pathList = GetPathList();
            string[] attrNameList = GetAttributeList();
            List<string> addedDefNames = new List<string>();

            AFElementTemplate lastSeen = new AFElementTemplate();

            foreach (KeyValuePair<string, AFElement> ele
                in GetElementByPaths(pathList))
            {
                if (ele.Value.Template == lastSeen) continue;

                foreach (AFAttribute attr in ele.Value.Attributes)
                {
                    if (!attrNameList.Contains(attr.Name)) continue;

                    if (addedDefNames.Contains(attr.Name)) continue;

                    ColumnDefinition valColDef = parserSettings.CreateColumnDefinition(
                        new PropertyBag());
                    valColDef.Name = attr.Name;

                    if (attr.Type == typeof (DateTime))
                        valColDef.Type = ColumnType.Time;

                    else if (attr.Type == typeof(int) ||
                        attr.Type == typeof(double) ||
                        attr.Type == typeof(float))
                        valColDef.Type = ColumnType.Numeric;

                    else                   
                        valColDef.Type = ColumnType.Text;
                    
                    addedDefNames.Add(attr.Name);
                    colDefs.Add(valColDef);
                }
            }

            foreach (ColumnDefinition col in colDefs)
            {
                if (parserSettings.HasColumn(col.Name)) continue;

                parserSettings.AddColumnDefinition(col);
            }
        }

        //Node Methods
        public IDictionary<string, AFElement> GetElementByPaths(
            IEnumerable<string> elementPaths)
        {
            HashSet<AFElement> encounteredElements = new HashSet<AFElement>();
            IDictionary<string, AFElement> elementDictionary =
                new Dictionary<string, AFElement>();

            List<string> simplePaths = new List<string>();
            List<string> complexPaths = new List<string>();
            StringBuilder errorStringBuilder = new StringBuilder();

            if (elementPaths != null)
            {
                //sort the given paths
                foreach (string path in elementPaths)
                {
                    if (path.EndsWith(@"\"))
                    {
                        complexPaths.Add(path.TrimEnd('\\'));
                    }
                    else
                    {
                        simplePaths.Add(path);
                    }
                }

                //retrive the simple queries
                try
                {
                    if (simplePaths.Count > 0)
                    {
                        AFKeyedResults<string, AFElement> queryReturn
                            = AFElement.FindElementsByPath(simplePaths, Database);
                        IDictionary<string, AFElement> simpleElements
                            = queryReturn.Results;
                        IDictionary<string, Exception> errors
                            = queryReturn.Errors;

                        foreach (string err in errors.Keys)
                        {
                            errorStringBuilder.Append(err + ",");
                        }
                        if (simpleElements != null)
                        {
                            foreach (KeyValuePair<string, AFElement> elementPair 
                                in simpleElements)
                            {
                                string elementPath = elementPair.Key;
                                AFElement elementObj = elementPair.Value;
                                if (!encounteredElements.Contains(elementObj))
                                {
                                    encounteredElements.Add(elementObj);
                                    elementDictionary.Add(elementPath, elementObj);
                                }
                            }
                        }
                        else
                        {
                            Log.Error(Properties.Resources.LogSimplePathError);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }

                //expand each of the complex paths and query for them
                try
                {
                    AFKeyedResults<string, AFElement> queryReturn
                        = AFElement.FindElementsByPath(complexPaths, Database);
                    IDictionary<string, AFElement> complexPathRootElements
                        = queryReturn.Results;
                    IDictionary<string, Exception> errors
                        = queryReturn.Errors;

                    foreach (string err in errors.Keys)
                    {
                        errorStringBuilder.Append(err + ", ");
                    }

                    if (complexPathRootElements == null)
                    {
                        Log.Error(Properties.Resources.LogComplexPathError);
                        complexPathRootElements =
                            new Dictionary<string, AFElement>();
                    }
                    foreach (
                        KeyValuePair<string, AFElement> kvp in
                            complexPathRootElements)
                    {
                        foreach (KeyValuePair<string, AFElement> childElement 
                            in GetAfElementChildren(kvp.Key, kvp.Value))
                        {
                            string elementPath = childElement.Key;
                            AFElement elementObj = childElement.Value;

                            if (encounteredElements.Contains(elementObj))
                                continue;

                            encounteredElements.Add(elementObj);
                            elementDictionary.Add(elementPath, elementObj);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Exception(ex);
                }
            }

            if (string.IsNullOrWhiteSpace(errorStringBuilder.ToString()))
                return elementDictionary;

            errorStringBuilder.Remove(errorStringBuilder.Length - 2, 2);
            Log.Error(String.Format(Properties.Resources.LogPathError,
                errorStringBuilder));

            return elementDictionary;
        }

        public IList<AFAttribute> GetAllSelectedAttributes(
            IEnumerable<ParameterValue> parameters = null)
        {
            AFAttributeList attrObjList = new AFAttributeList();

            ParameterValue[] paramArr = parameters == null
                ? null
                : parameters.ToArray();

            string[] pathList = GetPathList(paramArr);
            string[] attrNameList = GetAttributeList(paramArr);

            var selectedElements = GetElementByPaths(pathList);

            foreach (AFElement ele in selectedElements.Values)
            {
                foreach (string attrName in attrNameList)
                {
                    try
                    {
                        attrObjList.Add(ele.Attributes[attrName]);
                    }
                    catch
                    {
                        Log.Info(String.Format(
                            Properties.Resources.LogElementAttributeError,
                            attrName, ele.Name));
                    }
                }
            }

            return attrObjList;
        }

        private Dictionary<string, AFElement> GetAfElementChildren(
            string elementPath, AFElement element,
            Dictionary<string, AFElement> elements = null,
            HashSet<AFElement> elementsEncountered = null)
        {
            if (elements == null)
                elements = new Dictionary<string, AFElement>();
            if (elementsEncountered == null)
                elementsEncountered = new HashSet<AFElement>();

            if (!elementsEncountered.Contains(element))
            {
                if (element.Elements.Any())
                {
                    elementsEncountered.Add(element);
                    foreach (AFElement child in element.Elements)
                    {
                        string childPath = String.Format("{0}\\{1}",
                            elementPath.TrimEnd('\\'), child.Name);
                        GetAfElementChildren(childPath, child, elements,
                            elementsEncountered);
                    }
                }
                else
                {
                    elements.Add(elementPath, element);
                    elementsEncountered.Add(element);
                }
            }
            return elements;
        }

        public string[] GetPathList(
            IEnumerable<ParameterValue> parameters = null)
        {
            string[] pathList;
            if (OsiSettings.IsTreeSelectionMode)
            {
                pathList = OsiSettings.ElementPaths.Split(
                    new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                string test = OsiSettings.ParameterizeValue(
                    OsiSettings.ManualElementPaths, parameters);
                pathList = test.Split(
                    new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            }

            return pathList;
        }

        public string[] GetAttributeList(
            IEnumerable<ParameterValue> parameters = null)
        {
            string[] attrNameList;
            if (OsiSettings.IsTreeSelectionMode)
            {
                attrNameList = OsiSettings.Attributes.Split(
                    new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            {
                attrNameList = OsiSettings.ParameterizeValue(
                    OsiSettings.ManualAttributes, parameters).Split(
                        new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);
            }
            return attrNameList;
        }
    }
}