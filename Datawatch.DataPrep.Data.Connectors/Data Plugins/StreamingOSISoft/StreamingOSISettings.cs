﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.StreamingOSISoftPlugin
{
    public class StreamingOSISoftSettings : MessageQueueSettingsBase
    {
        #region Constructors

        public StreamingOSISoftSettings(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public StreamingOSISoftSettings(IPluginManager pluginManager, PropertyBag bag)
            : base(pluginManager, bag)
        {
        }

        public StreamingOSISoftSettings(
            IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
        }

        #endregion

        #region Properties
        
        public string SystemName
        {
            get { return GetInternal("SystemName"); }
            set { SetInternal("SystemName", value); }
        }

        public string Database
        {
            get { return GetInternal("Database"); }
            set { SetInternal("Database", value); }
        }

        public string ElementSelectionMode
        {
            get { return GetInternal("ElementSelectionMode"); }
            set { SetInternal("ElementSelectionMode", value); }
        }

        public string Attributes
        {
            get { return GetInternal("Attributes", String.Empty); }
            set { SetInternal("Attributes", value); }
        }

        public string ElementPaths
        {
            get { return GetInternal("ElementPaths", String.Empty); }
            set { SetInternal("ElementPaths", value); }
        }

        public string ManualAttributes
        {
            get { return GetInternal("ManualAttributes", String.Empty); }
            set { SetInternal("ManualAttributes", value); }
        }

        public string ManualElementPaths
        {
            get { return GetInternal("ManualElementPaths", String.Empty); }
            set { SetInternal("ManualElementPaths", value); }
        }

        public bool IsManualSelectionMode
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsManualSelectionMode",
                        "false"));
            }
            set { SetInternal("IsManualSelectionMode", value.ToString()); }
        }

        public bool IsTreeSelectionMode
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("IsTreeSelectionMode", "true"));
            }
            set { SetInternal("IsTreeSelectionMode", value.ToString()); }
        }

        public bool UseWindowsAuthentication
        {
            get
            {
                return
                    Convert.ToBoolean(GetInternal("UseWindowsAuthentication",
                        "true"));
            }
            set { SetInternal("UseWindowsAuthentication", value.ToString()); }
        }

        public bool IsConnected
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsConnected", "false"));
            }
            set { SetInternal("IsConnected", value.ToString()); }
        }

        #endregion

        //General Methods
        protected override string DefaultParserPluginId
        {
            get { return "Text"; }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StreamingOSISoftSettings)) return false;

            if (!base.Equals(obj)) return false;

            StreamingOSISoftSettings s = (StreamingOSISoftSettings) obj;

            if (SystemName != s.SystemName || Database != s.Database 
                || ElementSelectionMode != s.ElementSelectionMode) 
                return false;

            if (IsTreeSelectionMode && s.IsTreeSelectionMode)
            {
                if (Attributes == s.Attributes
                    && ElementPaths == s.ElementPaths)
                {
                    return true;
                }
            }
            else if (IsManualSelectionMode && s.IsManualSelectionMode)
            {
                if (ParameterizeValue(ManualAttributes)
                    == s.ParameterizeValue(s.ManualAttributes)
                    && ParameterizeValue(ManualElementPaths)
                    == s.ParameterizeValue(s.ManualElementPaths))
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            hashCode ^= SystemName.GetHashCode() + Database.GetHashCode();

            if (IsTreeSelectionMode)
            {
                hashCode ^= Attributes.GetHashCode() 
                    + ElementPaths.GetHashCode();
            }
            //IsManualSelectionMode
            else
            {
                hashCode ^= ParameterizeValue(ManualAttributes).GetHashCode()
                    + ParameterizeValue(ManualElementPaths).GetHashCode();
            }

            return hashCode;
        }

        public string ParameterizeValue(
            string value, IEnumerable<ParameterValue> paramsArg = null,
            string arraySeparator = "\n")
        {
            if (String.IsNullOrWhiteSpace(value)) return value;

            if (paramsArg != null)
            {
                var encoder = new ParameterEncoder
                {
                    Parameters = paramsArg,
                    DefaultArraySeparator =
                        '\n'.ToString(CultureInfo.InvariantCulture),
                    SourceString = value
                };
                return encoder.Encoded();
            }

            if (this.Parameters != null)
            {
                var encoder = new ParameterEncoder
                {
                    Parameters = Parameters,
                    DefaultArraySeparator =
                        '\n'.ToString(CultureInfo.InvariantCulture),
                    SourceString = value
                };
                return encoder.Encoded();
            }

            return value;
        }
    }
}