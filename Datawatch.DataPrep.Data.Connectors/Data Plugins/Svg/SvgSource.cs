﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Datawatch.DataPrep.Data.Core.Svg;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.Dashboards.Common.Svg
{
    public static class SvgSource
    {
        public static StandaloneTable LoadFromContent(string svgFileContent)
        {
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            doc.LoadXml(svgFileContent);
            return LoadFromXmlDocument(doc);
        }

        public static StandaloneTable LoadFromFile(string path)
        {
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            doc.Load(path);
            return LoadFromXmlDocument(doc);
        }

        public static StandaloneTable LoadFromXmlDocument(XmlDocument document)
        {
            IList<SvgLeaf> leaves = new List<SvgLeaf>();
            XmlElement root = GetRoot(document);
            if (root != null)
            {
                Stack<string> nodeKeys = new Stack<string>();
                Stack<string> transforms = new Stack<string>();
                Stack<bool> fill = new Stack<bool>();
                Stack<bool> transparent = new Stack<bool>();
                GetLeaves(root, leaves, transforms, nodeKeys, fill, transparent);
            }
            return LoadTable(leaves);
        }

        private static string GetAttribute(XmlNode node, string name)
        {
            if (node.Attributes != null)
            {
                XmlNode attr = node.Attributes.GetNamedItem(name);
                return attr == null ? null : attr.Value;
            }
            return null;
        }

        private static void GetLeaves(XmlNode node, IList<SvgLeaf> leaves,
            Stack<string> transforms, Stack<string> nodeKeys,
            Stack<bool> fill, Stack<bool> transparent)
        {
            // Start by adding root info
            string key = GetAttribute(node, "id");
            if (key != null)
            {
                nodeKeys.Push(key);
            }
            string transform = GetAttribute(node, "transform");
            if (transform != null)
            {
                transform = SvgHelper.PrepareTransformData(transform);
                transforms.Push(transform);
            }

            bool fillPushed = false;
            bool transparentPushed = false;

            string style = GetAttribute(node, "style");
            if (style != null)
            {
                if (style.IndexOf("fill:") != -1)
                {
                    fillPushed = true;
                    fill.Push(style.IndexOf("fill:none") == -1);
                }
                if (style.IndexOf("fill-opacity:") != -1)
                {
                    transparentPushed = true;
                    transparent.Push(style.IndexOf("fill-opacity:0") != -1);
                }
            }

            string fillAttribute = GetAttribute(node, "fill");
            if (!fillPushed && fillAttribute != null)
            {
                fillPushed = true;
                fill.Push(fillAttribute != "none");
            }

            string transparentAttribute = GetAttribute(node, "fill-opacity");
            if (!transparentPushed && transparentAttribute != null)
            {
                transparentPushed = true;
                transparent.Push(transparentAttribute == "0");
            }

            // Weve found a shape
            if (node.Name == "path" && (fill.Count == 0 || fill.Peek()) &&
                (transparent.Count == 0 || !transparent.Peek()))
            {
                SvgLeaf leaf = new SvgLeaf();
                leaf.NodeKeys = nodeKeys.ToArray();
                leaf.Transforms = transforms.ToArray();
                leaf.PathData = GetAttribute(node, "d");
                leaves.Add(leaf);
            }

            // Process each child
            foreach (XmlNode childNode in node.ChildNodes)
            {
                if (childNode.Name == "g" || childNode.Name == "path")
                {
                    GetLeaves(childNode, leaves, transforms, nodeKeys,
                        fill, transparent);
                }
            }

            // Pop keys of stack
            if (key != null)
            {
                nodeKeys.Pop();
            }
            if (fillPushed)
            {
                fill.Pop();
            }
            if (transparentPushed)
            {
                transparent.Pop();
            }
            if (transform != null)
            {
                transforms.Pop();
            }
        }

        private static XmlElement GetRoot(XmlDocument document)
        {
            foreach (XmlNode child in document.ChildNodes)
            {
                if (child is XmlElement && child.Name == "svg")
                {
                    return (XmlElement) child;
                }
            }
            return null;
        }

        private static StandaloneTable LoadTable(IList<SvgLeaf> leaves)
        {
            int maxDepth = 0;
            foreach (SvgLeaf leaf in leaves)
            {
                maxDepth = Math.Max(leaf.NodeKeys.Length, maxDepth);
            }
            // Generate decent column names
            string[] names = new string[maxDepth];
            for (int i = 0; i < names.Length; i++)
            {
                names[i] = "NodeKey" + (i + 1);
            }
            return LoadTable(leaves, names);
        }

        private static StandaloneTable LoadTable(IList<SvgLeaf> leaves,
            string[] nodeKeyColumnNames)
        {
            // Create the table
            StandaloneTable table = new StandaloneTable();

            table.BeginUpdate();

            try
            {
                // Create all the columns
                int breakDownColumns = nodeKeyColumnNames.Length;
                table.AddColumn(new TextColumn("ShapeData"));
                foreach (string nodeKeyColumnName in nodeKeyColumnNames)
                {
                    table.AddColumn(new TextColumn(nodeKeyColumnName));
                }

                // Enter all the info
                foreach (SvgLeaf leaf in leaves)
                {
                    string[] values = new string[breakDownColumns + 1];

                    StringBuilder builder = new StringBuilder();

                    // All the transforms concatenated into the same column
                    string[] transforms = leaf.Transforms;
                    for (int i = 0; i < transforms.Length; i++ )
                    {
                        builder.Append(transforms[i]);
                        if (i < transforms.Length - 1)
                        {
                            builder.Append(" ");
                        }
                    }

                    // Add a delimiter
                    builder.Append("|");

                    // Add the actual path data
                    string pathData = SvgHelper.PreparePathData(leaf.PathData);
                    builder.Append(pathData);

                    values[0] = builder.ToString();

                    // One id part in each column
                    for (int i = 0; i < leaf.NodeKeys.Length; i++)
                    {
                        values[breakDownColumns - i] = leaf.NodeKeys[i];
                    }

                    table.AddRow(values);
                }
            }
            finally {
                table.EndUpdate();
            }

            return table;
        }

        private class SvgLeaf
        {
            // An array of all parents id's (including our own) so far.
            public string[] NodeKeys;

            // Each transform-attribute value. For example:
            // {"scale(1,-1)", "translate(5,6)"}
            public string[] Transforms;

            // The value of the d-attribute. For example:
            // "M 7,8 L 9,10 ..."
            public string PathData;
        }
    }
}
