﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.Dashboards.Common.Svg;
using Panopticon.SvgPlugin.Properties;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.SvgPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : IDataPlugin
    {
        internal const string PluginId = "SVG";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "SVG";
        internal const string PluginType = DataPluginTypes.File;

        private bool isHosted;
        private const string pathKey = "FilePath";

        public string PathKey { get { return pathKey; } }

        private void CheckLicense()
        {
            if (!this.IsLicensed)
            {
                throw Exceptions.PluginNotLicensed(GetType());
            }
        }

        #region Implementation of IDisposable

        public void Dispose()
        {
        }

        #endregion

        #region Implementation of IPlugin

        public string Id
        {
            get { return PluginId; }
        }

        public bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public PropertyBag Settings
        {
            get;
            private set;
        }

        public PropertyBag GlobalSettings
        {
            get;
            private set;
        }

        public string Title
        {
            get { return PluginTitle; }
        }

        #endregion

        #region Implementation of IDataPlugin

        public void Initialize(bool isHosted, IPluginManager pluginManager, PropertyBag settings, PropertyBag globalSettings)
        {
            this.isHosted = isHosted;
            this.GlobalSettings = globalSettings;
            this.Settings = settings;
            this.PluginManager = pluginManager;
        }

        public ITable GetAllData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters, -1);
        }

        public ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            CheckLicense();

            string path = settings.Values[pathKey];

            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(dataDir, path);
            }

            if (!File.Exists(path))
            {
                string fileName = Path.GetFileName(path);
                string testPath = Path.Combine(dataDir, fileName);
                if (File.Exists(testPath))
                {
                    path = testPath;
                    settings.Values[pathKey] = path;
                }
            }

            return string.IsNullOrEmpty(path) ?
                null : SvgSource.LoadFromFile(path);
        }

        public string[] GetDataFiles(PropertyBag settings)
        {
            if (settings.Values.ContainsKey(pathKey))
            {
                string path = settings.Values[pathKey];
                return new[] { path };
            }
            return null;
        }

        public string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public IPluginManager PluginManager { get; private set; }

        public void UpdateFileLocation(PropertyBag settings, string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();

            if (Path.GetFileName(settings.Values[pathKey]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values[pathKey] =
                    Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }

        #endregion

        public PropertyBag DoOpenFile(string filePaths, IDictionary<string, string> connectionPropertiesMap)
        {
            throw new NotImplementedException();
        }
    }
}