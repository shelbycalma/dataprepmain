﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Panopticon.ActivePivotMDX.UI;

namespace Panopticon.ActivePivot
{
    /// <summary>
    /// Interaction logic for ConfigPanelStatic.xaml
    /// </summary>
    internal partial class ConfigPanelStatic : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                                        typeof (ActivePivotSettingsViewModel), typeof (ConfigPanelStatic),
                                        new PropertyMetadata(Settings_Changed));

        
        
        

        public ConfigPanelStatic()
        {
            InitializeComponent();
        }

        public ActivePivotSettingsViewModel Settings
        {
            get { return (ActivePivotSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }





        private static void Settings_Changed(DependencyObject obj,
                                             DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanelStatic)obj).OnSettingsChanged(
                (ActivePivotSettingsViewModel)args.OldValue,
                (ActivePivotSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(ActivePivotSettingsViewModel oldSettings,
                                         ActivePivotSettingsViewModel newSettings)
        {
            DataContext = newSettings;

            this.UiPasswordBox.Password = newSettings != null ? newSettings.Model.Password : null;
        }



        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.LoginInfo.Password = UiPasswordBox.Password;
            }
        }


        private void ManualEditChecked(object sender, RoutedEventArgs e)
        {
            var checkboxValue = (bool)((CheckBox)sender).IsChecked;
            MeasuresDimensionsExpander.IsExpanded = !checkboxValue;
            Settings.Model.Query
                = checkboxValue ? Settings.ManualQuery : Settings.GeneratedQuery;
        }

        private void ParameterChanged(object sender, RoutedEventArgs e)
        {
            Settings.GenerateMdxQuery();
        }
    }
}
