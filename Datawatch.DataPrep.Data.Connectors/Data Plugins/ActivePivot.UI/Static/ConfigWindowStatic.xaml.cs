﻿using System.Windows;
using System.Windows.Input;
using Panopticon.ActivePivot;
using Panopticon.ActivePivotMDX.UI;

namespace Panopticon.ActivePivotMDX.UI.Static
{
    /// <summary>
    /// Interaction logic for ConfigWindowStatic.xaml
    /// </summary>
    public partial class ConfigWindowStatic : Window
    {
        
         public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindowStatic));

        private readonly ActivePivotSettingsViewModel settings;

        public ConfigWindowStatic(ActivePivotSettingsViewModel settings)
        {
            this.settings = settings;
            InitializeComponent();
        }

        public ActivePivotSettingsViewModel Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings != null && settings.IsOK;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
