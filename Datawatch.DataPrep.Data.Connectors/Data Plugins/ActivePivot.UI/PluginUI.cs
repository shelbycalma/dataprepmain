﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.ActivePivotMDX.UI.Static;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.ActivePivot;
using Panopticon.ActivePivot.MDXPlugin;
using Panopticon.ActivePivot.Static;
using Panopticon.ActivePivotMDX.UI;

namespace Panopticon.ActivePivotMDX.UI
{
    public class PluginUI: PluginUIBase<Plugin, ActivePivotSettings>
    {
        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ActivePivotSettings mdxSettings = Plugin.CreateSettings(bag);

            ActivePivotSettingsViewModel activePivotSettingsViewModel =
                new ActivePivotSettingsViewModel(mdxSettings, parameters);
            ConfigPanelStatic panel = new ConfigPanelStatic();
            panel.Settings = activePivotSettingsViewModel;
            activePivotSettingsViewModel.LoadAllDataBySettings();
            return panel;
        }


        public override PropertyBag GetSetting(object element)
        {
            ConfigPanelStatic panel = (ConfigPanelStatic)element;
            panel.Settings.SaveParameters();
            return panel.Settings.Model.ToPropertyBag();
        }

        public override PropertyBag DoConnect(
           IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            ActivePivotSettings mdxSettings = new ActivePivotSettings(new PropertyBag(), true);
            ActivePivotSettingsViewModel settingsView =
                new ActivePivotSettingsViewModel(mdxSettings, parameters);
            ConfigWindowStatic window =
                new ConfigWindowStatic(settingsView);

            window.Owner = owner;

            bool? result = window.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            mdxSettings.Title = mdxSettings.CubeName;
            settingsView.SaveParameters();
            PropertyBag properties = mdxSettings.ToPropertyBag();
            return properties;
        }

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }
    }
}
