﻿
using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.ActivePivot.MDXPlugin.UI.Converters
{
    public class IsButtonEnabledConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            int count = 0;
            foreach (var value in values)
            {
                if (System.Convert.ToInt32(value) > 0)
                {
                    count++;
                }
            }
            return count != values.Length;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
