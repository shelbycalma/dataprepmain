﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.ExcelPlugin.UI.Properties;

namespace Panopticon.ExcelPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, ConnectionSettings>
    {
        public const string RangeKey = "range";
        public const string PathKey = "path";
        public const string UnpivotKey = "unpivot";
        public const string RealtimeKey = "realtime";

        private const string Imageuri =
            "pack://application:,,,/Panopticon.ExcelPlugin;component" +
                "/connect-excel.gif";
        
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(Imageuri,
                    UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        internal string BrowseForFile()
        {
            return BrowseForFile(null);
        }

        public override string GetTitle(PropertyBag settings)
        {
            return ExcelPlugin.Plugin.GetTitle(settings.Values[PathKey], settings.Values[RangeKey]);
        }

        //TODO: Should use DataPluginUtils.BrowseForFile
        internal string BrowseForFile(string oldFilename)
        {
            return DataPluginUtils.BrowseForFile(owner, oldFilename,
                string.Concat(Resources.UiDialogExcelFilesFilter, "|*.xls;*.xlsx;*.xlsm"), Plugin.GlobalSettings);
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            string path = BrowseForFile();
            if (path == null)
            {
                return null;
            }

            string[] ranges = Plugin.RealtimeExcelConnector.IsLicensed
                ? Plugin.RealtimeExcelConnector.GetRanges(path)
                : Plugin.StaticExcelConnector.GetRanges(path);

            if (ranges != null)
            {
                string[] titles = new string[ranges.Length];

                for (int i = 0; i < ranges.Length; i++)
                {
                    titles[i] = ExcelPlugin.Plugin.GetTitle(null, ranges[i]);
                }

                TableSelectionWindow dlg = new TableSelectionWindow(titles)
                {
                    IsRealtimeEnabled = Plugin.RealtimeExcelConnector.IsLicensed
                };
                
                if (dlg.ShowDialog().Value != true)
                {
                    return null;
                }

                int selectedSheet = dlg.SelectedSheet;
                if (selectedSheet != -1)
                {
                    PropertyBag bag = CreatePropertyBag(
                        path, ranges[selectedSheet], dlg.Realtime);
                    return bag;
                }
            }
            return null;
        }

        public static PropertyBag CreatePropertyBag(
            string path, string range, bool realtime)
        {
            PropertyBag settings = new PropertyBag();
            settings.Values[PathKey] = path;
            settings.Values[RangeKey] = range;
            settings.Values[RealtimeKey] = Convert.ToString(realtime);
            return settings;
        }


        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            PropertyBag bag = new PropertyBag();
            bag.Values[PathKey] = configPanel.Path;
            bag.Values[RangeKey] = configPanel.Range;
            bag.Values[UnpivotKey] = Convert.ToString(configPanel.Unpivot);
            bag.Values[RealtimeKey] = Convert.ToString(configPanel.Realtime);
            return bag;
        }

        public override object GetConfigElement(PropertyBag bag,
           IEnumerable<ParameterValue> parameters)
        {
            string workbookDirectory =
               SettingsHelper.GetDefaultWorkbookDirectory(Plugin.GlobalSettings);

            ConfigPanel cp = new ConfigPanel(this, workbookDirectory);

            cp.Path = bag.Values[PathKey];
            cp.Range = bag.Values[RangeKey];
            cp.Unpivot = Convert.ToBoolean(bag.Values[UnpivotKey]);
            cp.Realtime = Convert.ToBoolean(bag.Values[RealtimeKey]);
            cp.IsRealtimeEnabled = Plugin.RealtimeExcelConnector.IsLicensed;

            return cp;
        }
    }
}
