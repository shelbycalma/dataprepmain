﻿using System;
using System.ComponentModel;
using System.Data.OleDb;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using Datawatch.DataPrep.Data.Core.Tables.Real_Time_Excel;


namespace Panopticon.ExcelPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string path;
        private string range;
        private bool unpivot = false;
        private bool realtime;
        private bool realtimeEnabled;
        private PluginUI pluginUI;
        private readonly string workbookDir;

        public ConfigPanel(PluginUI pluginUI, string workbookDir)
        {
            this.pluginUI = pluginUI;
            this.workbookDir = workbookDir;
            InitializeComponent();
            DataContext = this;
        }

        public bool Unpivot
        {
            get { return unpivot; }
            set
            {
                unpivot = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Unpivot"));
                }
            }
        }

        public bool IsRealtimeEnabled
        {
            get { return realtimeEnabled; }
            set
            {
                realtimeEnabled = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsRealtimeEnabled"));
                }
            }
        }

        public bool Realtime
        {
            get { return realtime; }
            set
            {
                realtime = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Realtime"));
                }
            }
        }

        public string Path
        {
            get { return path; }
            set 
            { 
                path = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Path"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Ranges"));
                }
            }
        }

        public string Range
        {
            get { return range; }
            set
            {
                range = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Range"));
                }
            }
        }

        public string[] Ranges
        {
            get
            {
                // TODO: if there is something to root with, do it here
                if (!System.IO.Path.IsPathRooted(path))
                {
                    return RealtimeExcelTable.GetRanges(System.IO.Path.Combine(workbookDir, path));
                }
                return RealtimeExcelTable.GetRanges(path);
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = pluginUI.BrowseForFile(Path);
            if (path != null)
            {
                Path = path;
            }
        }
    }
}
