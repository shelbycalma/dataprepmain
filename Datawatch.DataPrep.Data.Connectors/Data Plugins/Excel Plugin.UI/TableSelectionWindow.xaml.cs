﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace Panopticon.ExcelPlugin.UI
{
    /// <summary>
    /// Interaction logic for TableSelectionWindow.xaml
    /// </summary>
    public partial class TableSelectionWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool realtime;
        private string[] sheets;
        private int selectedSheet;
        private bool realtimeEnabled;

        public static RoutedCommand OK = 
            new RoutedCommand("OKCommand", typeof(TableSelectionWindow));

        public TableSelectionWindow(string[] sheets)
        {
            this.sheets = sheets;
            
            InitializeComponent();
            DataContext = this;
            
            if (sheets.Length > 0)
            {
                SelectedSheet = 0;
            }
        }

        public bool IsRealtimeEnabled
        {
            get { return realtimeEnabled; }
            set
            {
                realtimeEnabled = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("IsRealtimeEnabled"));
                }
            }
        }

        public int SelectedSheet
        {
            get
            {
                return selectedSheet;
            }
            set
            {
                selectedSheet = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("SelectedSheet"));
                }
            }
        }

        public string[] Sheets
        {
            get { return sheets; }
        }

        public bool Realtime
        {
            get { return realtime; }
            set { realtime = value; }
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void OK_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = SelectedSheet != -1;
        }

        private void OK_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
