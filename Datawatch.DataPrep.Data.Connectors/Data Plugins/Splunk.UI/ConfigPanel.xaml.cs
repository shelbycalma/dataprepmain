﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.SplunkPlugin.UI
{
    internal partial class ConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(SplunkSettingsViewModel),
                typeof(ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        public ConfigPanel()
        {
            InitializeComponent();
        }

        public SplunkSettingsViewModel Settings
        {
            get { return (SplunkSettingsViewModel)GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (SplunkSettingsViewModel)args.OldValue,
                (SplunkSettingsViewModel)args.NewValue);
        }

        protected void OnSettingsChanged(SplunkSettingsViewModel oldSettings,
            SplunkSettingsViewModel newSettings)
        {
            DataContext = newSettings;

            PasswordBox.Password = newSettings != null ?
                newSettings.Password : null;
        }

        private void PasswordBox_PasswordChanged(object sender,
            RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Password = PasswordBox.Password;
            }
        }

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}
