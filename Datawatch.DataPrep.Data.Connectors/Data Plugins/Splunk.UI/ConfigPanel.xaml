﻿<UserControl
    x:Class="Panopticon.SplunkPlugin.UI.ConfigPanel"
    x:ClassModifier="internal"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" 
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008" 
    help:DataHelpProvider.HelpKey="Splunk"
    mc:Ignorable="d"
    d:DataContext="{d:DesignInstance ui:SplunkSettingsViewModel}"
    xmlns:properties="clr-namespace:Panopticon.SplunkPlugin.UI.Properties"
    d:DesignHeight="300" d:DesignWidth="450"
    xmlns:behaviors="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Behaviors;assembly=Datawatch.DataPrep.Data.Core.UI"
    xmlns:help="clr-namespace:Datawatch.DataPrep.Data.Core.Help;assembly=Datawatch.DataPrep.Data.Core"
    xmlns:ui="clr-namespace:Panopticon.SplunkPlugin.UI"
    xmlns:converters="clr-namespace:Datawatch.DataPrep.Data.Core.UI.Converters;assembly=Datawatch.DataPrep.Data.Core.UI"
    xmlns:splunkPlugin="clr-namespace:Panopticon.SplunkPlugin;assembly=Panopticon.SplunkPlugin">
    <UserControl.Resources>
        <ResourceDictionary>
            <ResourceDictionary.MergedDictionaries>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/Converters/Converters.xaml"/>
                <ResourceDictionary Source="/Datawatch.DataPrep.Data.Core.UI;component/ResourceDictionaries/CommonStyles.xaml"/>
            </ResourceDictionary.MergedDictionaries>
            <converters:TimeZoneConverter x:Key="TimeZoneConverter" />
            <Style BasedOn="{StaticResource CommonPasswordBoxStyle}" TargetType="{x:Type PasswordBox}"/>
            <Style BasedOn="{StaticResource CommonCheckBoxStyle}" TargetType="{x:Type CheckBox}" />
            <Style BasedOn="{StaticResource CommonComboBoxStyle}" TargetType="{x:Type ComboBox}"/>
            <Style BasedOn="{StaticResource CommonLabelStyle}" TargetType="{x:Type Label}" />
            <Style BasedOn="{StaticResource CommonTextBoxStyle}" TargetType="{x:Type TextBox}"/>
            <Style BasedOn="{StaticResource CommonRadioButtonStyle}" TargetType="{x:Type RadioButton}" />
        </ResourceDictionary>
    </UserControl.Resources>
    
    <Grid behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*"/>
            <ColumnDefinition Width="Auto"/>
            <ColumnDefinition Width="*"/>
        </Grid.ColumnDefinitions>

        <Grid.RowDefinitions>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
            <RowDefinition Height="Auto"/>
			<RowDefinition Height="Auto"/>
		</Grid.RowDefinitions>

        <Label
            Grid.Row="0"
            Grid.Column="0"
            Content="{x:Static properties:Resources.UiHost}" />
        <TextBox
            Grid.Row="0"
            Grid.Column="1"
            Text="{Binding Host}" />

        <Label
            Grid.Row="0"
            Grid.Column="2"
            Content="{x:Static properties:Resources.UiPort}" />
        <TextBox
            Grid.Row="0"
            Grid.Column="3"
            Text="{Binding Port}" />

        <Label
            Grid.Row="1"
            Grid.Column="0"
            Content="{x:Static properties:Resources.UiUsername}" />
        <TextBox
            Grid.Row="1"
            Grid.Column="1"
            Text="{Binding Username, UpdateSourceTrigger=PropertyChanged}" />

        <Label
            Grid.Row="1"
            Grid.Column="2"
            Content="{x:Static properties:Resources.UiPassword}" />
        <PasswordBox
            Grid.Row="1"
            Grid.Column="3"
            Name="PasswordBox"
            PasswordChanged="PasswordBox_PasswordChanged" />

        <Button
            Grid.Row="2"
            Grid.Column="3"
            Content="{x:Static properties:Resources.UiConnectButtonText}"
            Command="{Binding ConnectCommand}" />

        <Label
            Grid.Row="3"
            Grid.Column="0"
            Content="{x:Static properties:Resources.UiApplication}" />
        <ComboBox
            Grid.ColumnSpan ="3"
            Grid.Row="3"
            Grid.Column="1"
            ItemsSource="{Binding Applications}"
            SelectedItem="{Binding App}" />

        <RadioButton
            Grid.Row="4"
            Grid.Column="0"
            GroupName="SearchType"
            IsChecked="{Binding SearchType,
                Converter={StaticResource EnumToBoolConverter},
                ConverterParameter={x:Static splunkPlugin:SearchType.SavedSearch},
                Mode=TwoWay}"
            Content="{x:Static properties:Resources.UiSavedSearch}"
            Visibility="{Binding PluginHostSettings.IsFreeformSQLSupported,
                Converter={StaticResource VisibleIfTrue}}"/>
        <Label
            Grid.Row="4"
            Grid.Column="0"
            Content="{x:Static properties:Resources.UiSavedSearch}"
            Visibility="{Binding PluginHostSettings.IsFreeformSQLSupported,
                Converter={StaticResource VisibleIfNotTrue}}"/>

        <ComboBox
            Grid.Row="4"
            Grid.Column="1"
            Grid.ColumnSpan="3"
            ItemsSource="{Binding SavedSearchesNames}"
            SelectedItem="{Binding SavedSearch}"
            IsEnabled="{Binding SearchType,
                Converter={StaticResource EnumToBoolConverter},
                ConverterParameter={x:Static splunkPlugin:SearchType.SavedSearch}}" />

        <RadioButton
            Grid.Row="5"
            Grid.Column="0"
            GroupName="SearchType"
            Margin="5 5 0 0"
            IsChecked="{Binding SearchType,
                Converter={StaticResource EnumToBoolConverter},
                ConverterParameter={x:Static splunkPlugin:SearchType.Manual},
                Mode=TwoWay}"
            Content="{x:Static properties:Resources.UiManual}"
            Visibility="{Binding PluginHostSettings.IsFreeformSQLSupported,
                Converter={StaticResource VisibleIfTrue}}" />

        <CheckBox
            Grid.Row="6"
            Grid.Column="1"
            Grid.ColumnSpan="3"
            Margin="5 0 0 0"
            IsChecked="{Binding EncloseParameterInQuote}"
            Content="{x:Static properties:Resources.UiEncloseParametersInQuotes}"
            Visibility="Hidden" />

        <Label
            Grid.Row="7"
            Grid.Column="0"
            VerticalAlignment="Top" Margin="5 0 0 0" Padding="0"
            Content="{x:Static properties:Resources.UiSearchQuery}"
            Visibility="{Binding PluginHostSettings.IsFreeformSQLSupported,
                Converter={StaticResource VisibleIfTrue}}" />
        <Canvas Name="QueryCanvas" Margin="0 0 10 5" Grid.Row="7" Grid.Column="1" Grid.ColumnSpan="3" MinHeight="190">
        <TextBox
            Width="{Binding ActualWidth, ElementName=QueryCanvas}"
            Height="{Binding ActualHeight, ElementName=QueryCanvas}"
            IsReadOnly="{Binding QuerySettings.IsQueryMode,
            Converter={StaticResource boolInverterConverter}}"
            HorizontalAlignment="Stretch"
            VerticalAlignment="Stretch"
            AcceptsReturn="True"
            TextWrapping="Wrap"
            VerticalScrollBarVisibility="Auto"
            VerticalContentAlignment="Top"
            Text="{Binding SearchQuery, UpdateSourceTrigger=PropertyChanged}"
            IsEnabled="{Binding SearchType,
                Converter={StaticResource EnumToBoolConverter},
                ConverterParameter={x:Static splunkPlugin:SearchType.Manual}}"
            Visibility="{Binding PluginHostSettings.IsFreeformSQLSupported,
                Converter={StaticResource VisibleIfTrue}}"/>
        </Canvas>

        <Label
            Grid.Row="8"
            Grid.Column="0"
            Content="{x:Static properties:Resources.UiTimezone}"
            Visibility="{Binding PluginHostSettings.IsTimezoneSupported,
                Converter={StaticResource VisibleIfTrue}}" />
        <ComboBox
            Grid.Row="8"
            Grid.Column="1"
            Grid.ColumnSpan="3"
            ItemsSource="{Binding TimeZoneHelper.TimeZones}"
            SelectedItem="{Binding TimeZoneHelper.SelectedTimeZone}"
            IsSynchronizedWithCurrentItem="True"
            Visibility="{Binding PluginHostSettings.IsTimezoneSupported,
                Converter={StaticResource VisibleIfTrue}}" />
		<Grid Grid.Row="9" Grid.Column="0" Grid.ColumnSpan="4">
			<Grid.RowDefinitions>
				<RowDefinition Height="*" />
				<RowDefinition Height="Auto" />
			</Grid.RowDefinitions>
			<Button Content="{x:Static properties:Resources.UiPreviewData}" Grid.Row="0" MinWidth="100" MaxWidth="150" Height="20" HorizontalAlignment="Left" x:Name="PreviewData"
                                Command="{Binding PreviewCommand}" />
			<DataGrid behaviors:SharedResources.MergedDictionaries="CommonStyles;ConnectorTheme"
                            Grid.Row="1" Height="120" Width="Auto" Name="PreviewGrid"
                            AutoGenerateColumns="True"
                            AutoGeneratingColumn="PreviewGrid_AutoGeneratingColumn"
                            ScrollViewer.CanContentScroll="True"
                            ScrollViewer.HorizontalScrollBarVisibility="Auto"
                            ScrollViewer.VerticalScrollBarVisibility="Auto"
                            CanUserAddRows="false" IsReadOnly="True"
                            ItemsSource="{Binding PreviewTable, Mode=TwoWay,
                                UpdateSourceTrigger=PropertyChanged}">
			</DataGrid>
		</Grid>
	</Grid>
</UserControl>
