﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.SplunkPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, SplunkSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            SplunkSettings splunkSettings = new SplunkSettings();
            PluginHostSettingsViewModel pluginHostSettings =
                new PluginHostSettingsViewModel(Plugin.GlobalSettings);
            SplunkSettingsViewModel splunkSettingsViewModel =
                new SplunkSettingsViewModel(splunkSettings, parameters,
                        pluginHostSettings);

            ConfigWindow configWindow = new ConfigWindow(
                splunkSettingsViewModel);
            configWindow.Owner = owner;

            bool? result = configWindow.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            PropertyBag properties = splunkSettings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            SplunkSettings splunkSettings = Plugin.CreateSettings(bag);
            PluginHostSettingsViewModel pluginHostSettings =
                new PluginHostSettingsViewModel(Plugin.GlobalSettings);
            SplunkSettingsViewModel splunkSettingsViewModel =
                new SplunkSettingsViewModel(splunkSettings, parameters,
                        pluginHostSettings);

            ConfigPanel panel = new ConfigPanel();
            panel.Settings = splunkSettingsViewModel;
            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel panel = (ConfigPanel)obj;
            return panel.Settings.ToPropertyBag();
        }
    }
}
