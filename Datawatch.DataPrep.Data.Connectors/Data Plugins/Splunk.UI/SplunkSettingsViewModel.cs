﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.SplunkPlugin.UI.Properties;
using System.Linq;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;

namespace Panopticon.SplunkPlugin
{
    internal class SplunkSettingsViewModel : ViewModelBase
    {
		private ICommand _previewCommand;
		private DataTable _previewTable;

		private ObservableCollection<string> applications;
        private ObservableCollection<string> savedSearchesNames;
        private IDictionary<string, string> savedSearches;

        public SplunkSettings Model { get; private set; }
        public IEnumerable<ParameterValue> Parameters { get; private set; }
        private readonly PluginHostSettingsViewModel pluginHostSettings;

        public SplunkSettingsViewModel(SplunkSettings model,
            IEnumerable<ParameterValue> parameters,
            PluginHostSettingsViewModel pluginHostSettings)
        {
            Model = model;
            Parameters = parameters;
            this.pluginHostSettings = pluginHostSettings;

            if (CanConnect())
            {
                Connect();
            }
        }

		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				OnPropertyChanged("PreviewTable");
			}
		}

		public string Host
        {
            get { return Model.Host; }
            set
            {
                Model.Host = value;
                OnPropertyChanged("Host");
            }
        }

        public string App
        {
            get { return Model.App; }
            set
            {
                string oldValue = Model.App;
                Model.App = value;
                OnPropertyChanged("App");

                try
                {
                    SplunkClient client = new SplunkClient(Model, Parameters);
                    UpdateSavedSearches(client);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Resources.UiError,
                        MessageBoxButton.OK, MessageBoxImage.Error);

                    //This is needed to reset application to previously
                    //selected if an error occured.
                    //Without Dispatcher application combobox doesn't update
                    //because OnPropertyChanged has already been called.
                    Dispatcher.CurrentDispatcher.BeginInvoke(new System.Action(() =>
                    {
                        Model.App = oldValue;
                        OnPropertyChanged("App");
                    }));
                    Log.Exception(ex);
                }
            }
        }

        public string Username
        {
            get { return Model.Username; }
            set
            {
                Model.Username = value;
                OnPropertyChanged("Username");
            }
        }

        public string Password
        {
            get { return Model.Password; }
            set
            {
                Model.Password = value;
                OnPropertyChanged("Password");
            }
        }

        public string Port
        {
            get { return Model.Port; }
            set
            {
                Model.Port = value;
                OnPropertyChanged("Port");
            }
        }

        public string SearchQuery
        {
            get { return Model.SearchQuery; }
            set
            {
                Model.SearchQuery = value;
                OnPropertyChanged("SearchQuery");
            }
        }

        public string SavedSearch
        {
            get { return Model.SavedSearch; }
            set
            {
                Model.SavedSearch = value;
                OnPropertyChanged("SavedSearch");
                SearchQuery = !string.IsNullOrWhiteSpace(SavedSearch) ?
                    savedSearches[SavedSearch] : null;
                Model.Title = string.Format(
                    Resources.UiSavedSearchTitle, Model.SavedSearch);
            }
        }

        public SearchType SearchType
        {
            get { return Model.SearchType; }
            set
            {
                Model.SearchType = value;
                OnPropertyChanged("SearchType");
                switch (value)
                {
                    case SearchType.Manual:
                        Model.Title = Resources.UiManualSearchTitle;
                        break;
                    case SearchType.SavedSearch:
                        SearchQuery = !string.IsNullOrWhiteSpace(SavedSearch) ?
                            savedSearches[SavedSearch] : null;
                        Model.Title = string.Format(
                            Resources.UiSavedSearchTitle,
                            Model.SavedSearch);
                        break;
                }
            }
        }

        public bool EncloseParameterInQuote
        {
            get { return Model.EncloseParameterInQuote; }
            set
            {
                if (value == Model.EncloseParameterInQuote) return;
                Model.EncloseParameterInQuote = value;
                OnPropertyChanged("EncloseParameterInQuote");
            }
        }

        public TimeZoneHelper TimeZoneHelper
        {
            get { return Model.TimeZoneHelper; }
        }

        public ObservableCollection<string> Applications
        {
            get
            {
                    return applications ??
                           (applications = new ObservableCollection<string>());
            }
            set
            {
                if (applications == value) return;
                applications = value;
                OnPropertyChanged("Applications");
            }
        }

        public ObservableCollection<string> SavedSearchesNames
        {
            get
            {
                    return savedSearchesNames ??
                           (savedSearchesNames = new ObservableCollection<string>());
            }
            set
            {
                if (savedSearchesNames == value) return;
                savedSearchesNames = value;
                OnPropertyChanged("SavedSearchesNames");
            }
        }

        public PluginHostSettingsViewModel PluginHostSettings
        {
            get { return pluginHostSettings; }
        }

        public bool ShowEncloseParameterInQuote
        {
            get
            {
                return pluginHostSettings.IsFreeformSQLSupported &&
                    pluginHostSettings.IsParameterSupported;
            }
        }

		public bool IsOk
		{
			get
			{
				return CanConnect() &&
                   ((!string.IsNullOrWhiteSpace(Model.SavedSearch) && Model.SearchType == SearchType.SavedSearch) ||
                    (!string.IsNullOrWhiteSpace(Model.SearchQuery) && Model.SearchType == SearchType.Manual));
            }
        }

        public PropertyBag ToPropertyBag()
        {
            return Model.ToPropertyBag();
        }

		public ICommand PreviewCommand
		{
			get
			{
				return _previewCommand ??
					   (_previewCommand = new DelegateCommand(PreviewData, CanPreview));
			}
		}

		private ICommand connectCommand;
        public ICommand ConnectCommand
        {
            get
            {
                return connectCommand ??
                   (connectCommand = new DelegateCommand(Connect, CanConnect));
            }
        }

        private void Connect()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                string oldApp = Model.App;
                // Obtain all apps and check each of them if they are enabled.
                // Only enabled applications will be listed.
                SplunkClient client = new SplunkClient(Model, Parameters);
                List<string> applicationList = new List<string> { string.Empty };
                applicationList.AddRange(client.GetApplicationList());
                for (int i = applicationList.Count - 1; i >= 1; i--)
                {
                    // Use only those applications that are available
                    try
                    {
                        Model.App = applicationList[i];
                        client = new SplunkClient(Model, Parameters);

                    }
                    catch (Exception e)
                    {
                        applicationList.RemoveAt(i);
                    }
                }
                Applications = new ObservableCollection<string>(applicationList);

                Model.App = oldApp;
                if (string.IsNullOrEmpty(Model.App) &&
                    Applications.Count > 1)
                {
                    // Can't set this.App, which tries to reload SavedSearches
                    // and displays messagebox on error.
                    // Splunk throws 404 error if no SavedSearches is available.

                    // Applications[0] is empty string.
                    Model.App = Applications[1];
                    OnPropertyChanged("App");

                    try
                    {
                        // First App from list is selected
                        // try to get saved search for this App.
                        client = new SplunkClient(Model, Parameters);
                        UpdateSavedSearches(client);
                    }
                    catch (Exception ex)
                    {
                        Log.Exception(ex);
                    }
                }
                else
                {
                    UpdateSavedSearches(client);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, Resources.UiError,
                    MessageBoxButton.OK, MessageBoxImage.Error);
                Log.Exception(ex);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private bool CanConnect()
        {
            return !string.IsNullOrWhiteSpace(Model.Host) &&
                   !string.IsNullOrWhiteSpace(Model.Port) &&
                   !string.IsNullOrWhiteSpace(Model.Username) &&
                   !string.IsNullOrWhiteSpace(Model.Password);
        }

        private bool CanPreview()
        {
            return CanConnect() &&
                   ((!string.IsNullOrWhiteSpace(Model.SavedSearch) && Model.SearchType == SearchType.SavedSearch) ||
                    (!string.IsNullOrWhiteSpace(Model.SearchQuery) && Model.SearchType == SearchType.Manual));
        }

        private void UpdateSavedSearches(SplunkClient client)
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                savedSearches = client.GetSavedSearches();
                SavedSearchesNames =
                    new ObservableCollection<string>(savedSearches.Keys);
                if (string.IsNullOrEmpty(Model.SavedSearch) &&
                    SavedSearchesNames.Count > 0)
                {
                    SavedSearch = SavedSearchesNames[0];
                }
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }
		
		private PropertyBag CreatePropertyBag()
		{
			PropertyBag pb = new PropertyBag();
			PropertyValue pv = new PropertyValue("host", Model.Host);
			pb.Values.Add(pv);
			pv = new PropertyValue("port", Model.Port);
			pb.Values.Add(pv);
			pv = new PropertyValue("username", Model.Username);
			pb.Values.Add(pv);
			pv = new PropertyValue("password", Model.Password);
			pb.Values.Add(pv);
			pv = new PropertyValue("searchQuery", Model.SearchQuery);
			pb.Values.Add(pv);
			pv = new PropertyValue("savedsearch", Model.SavedSearch);
			pb.Values.Add(pv);
			pv = new PropertyValue("app", Model.App);
			pb.Values.Add(pv);
			pv = new PropertyValue("encloseParameterInQuote", Model.EncloseParameterInQuote.ToString());
			pb.Values.Add(pv);
			pv = new PropertyValue("searchtype", Model.SearchType.ToString());
			pb.Values.Add(pv);

			TimeZoneHelper tzh = Model.TimeZoneHelper;
			PropertyBag timeZoneHelperBag = new PropertyBag();
			timeZoneHelperBag.Name = "TimeZone";
			pb.SubGroups.Add(timeZoneHelperBag);

			return pb;
		}

		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}

				Panopticon.SplunkPlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);
				if (t != null)
				{
					_previewTable = PreviewDataTable.GetPreviewTable(t);
					OnPropertyChanged("PreviewTable");
				}
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				MessageBox.Show(ex.Message, null,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

	}
}
