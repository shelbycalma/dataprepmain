﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.PythonPlugin.UI
{
    internal partial class ConfigWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindow));

        private readonly PythonPluginSettingsViewModel viewModel;

        public ConfigWindow(PythonPluginSettingsViewModel viewModel)
        {
            this.viewModel = viewModel;
            InitializeComponent();
        }

        public PythonPluginSettingsViewModel ViewModel
        {
            get { return viewModel; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = viewModel != null && viewModel.IsOk();
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }
}
