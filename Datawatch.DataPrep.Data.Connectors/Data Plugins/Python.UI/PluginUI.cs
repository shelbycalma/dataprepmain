﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.PythonPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, PythonPluginSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            PythonPluginSettings pythonPluginSettings = new PythonPluginSettings();
            PythonPluginSettingsViewModel pythonPluginSettingsViewModel =
                new PythonPluginSettingsViewModel(pythonPluginSettings, parameters);
            ConfigWindow configWindow = new ConfigWindow(pythonPluginSettingsViewModel);
            configWindow.Owner = owner;
            
            bool? result = configWindow.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            PropertyBag properties = pythonPluginSettings.ToPropertyBag();
            return properties;
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            return new ConfigPanel()
            {
                Settings = new PythonPluginSettingsViewModel(Plugin.CreateSettings(bag), parameters)
            };
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel panel = (ConfigPanel)obj;
            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
