﻿using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Panopticon.Python.UI.dll")]
[assembly: AssemblyDescription("Datawatch Python Plug-in UI")]
[assembly: AssemblyConfiguration("")]
