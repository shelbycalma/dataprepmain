﻿using System;
using System.Collections.Generic;

namespace Panopticon.StreamingSimulatorPlugin
{
    class TextComparer : IComparer<object>
    {
        private readonly int sortIndex;

        public TextComparer(int sortIndex)
        {
            this.sortIndex = sortIndex;
        }

        public int Compare(object o1, object o2)
        {
            object[] arr1 = (object[])o1;
            object[] arr2 = (object[])o2;
            return String.Compare((string)arr1[sortIndex], (string)arr2[sortIndex], StringComparison.Ordinal);
        }
    }
}
