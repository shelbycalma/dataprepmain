﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.StreamingSimulatorPlugin
{
    public class StreamingSimulatorTable : ParameterTable
    {
        private IList<object[]> objRows;
        private int objRowIndex = 0;
        private object indexLock = new object();
        private ParameterFilter parameterFilter;

        public StreamingSimulatorTable(IEnumerable<ParameterValue> parameters) : base(parameters)
        {
        }

        public object[] NextRow
        {
            get
            {
                object[] row;
                lock (indexLock)
                {
                    row = objRows[objRowIndex++];
                }
                return row;
            }
        } 

        public IList<object[]> ObjectRows
        {
            get { return objRows; }
            set { objRows = value; }
        }

        public int ObjectRowsCount
        {
            get { return objRows.Count; }
        }

        public bool IsMoreRowsToSend
        {
            get
            {
                lock (indexLock)
                {
                    return objRowIndex < objRows.Count;
                }
            }
        }

        public int ObjectRowIndex
        {
            get
            {
                lock (indexLock)
                {
                    return objRowIndex;
                }
            }
            set
            {
                lock (indexLock)
                {
                    objRowIndex = value;
                }
            }
        }

        public ParameterFilter ParameterFilter
        {
            get { return parameterFilter;}
            set
            {
                parameterFilter = value;
            }
        }
    }
}
