﻿using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.StreamingSimulatorPlugin
{
    class Utils
    {
        public static object ConvertSortColumnValueToRightType(ColumnDefinition sortColumn,  object value, 
            NumericDataHelper numericDataHelper)
        {
            switch (sortColumn.Type)
            {
                case ColumnType.Numeric:
                    double newDouble = DataPluginUtils.ObjectToDouble(value, numericDataHelper);
                    // Will also insert NaN values
                    return newDouble;
                case ColumnType.Time:
                    DateParser dateParser = sortColumn.DateParser;
                    return DataPluginUtils.ObjectToDateTime(value, dateParser);
                case ColumnType.Text:
                    return DataPluginUtils.ObjectToString(value);
            }
            return null;
        }

        internal static StreamReader GetStreamAndSkipRows(
            StreamingSimulatorSettings settings, IEnumerable<ParameterValue> parameters)
        {
            StreamReader stream = DataPluginUtils.GetStream(settings,
                parameters, settings.FilePath);

            for (int i = 0; i < settings.SkipRows; i++)
            {
                stream.ReadLine();
            }
            return stream;
        }

    }
}
