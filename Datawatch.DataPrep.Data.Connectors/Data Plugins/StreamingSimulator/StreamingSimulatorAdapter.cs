﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Timers;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Timer = System.Timers.Timer;

namespace Panopticon.StreamingSimulatorPlugin
{
    public class StreamingSimulatorAdapter : MessageQueueAdapterBase<StreamingSimulatorTable,
        StreamingSimulatorSettings, Dictionary<string, object>>
    {
        private Thread rowLoaderThread;
        private LoadAllRowsWorker loadAllRowsWorker;
        private RowSenderTimer rowSenderTimer;

        private enum State
        {
            LoadingRows,
            SendingRows
        }

        private State state;
        private readonly Object stateLock = new Object();

        public override void Start()
        {
            settings.Parameters = table.Parameters;
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();
            lock (stateLock)
            {
                loadAllRowsWorker = new LoadAllRowsWorker(this);
                state = State.LoadingRows;
            }
            rowLoaderThread = new Thread(loadAllRowsWorker.LoadRowsAndInitiateTimer);
            rowLoaderThread.Name = "Simulated Stream";
            rowLoaderThread.Start();
        }

        public override void Stop()
        {
            base.Stop();
            lock (stateLock)
            {
                switch (state)
                {
                    case State.LoadingRows:
                        loadAllRowsWorker.StopLoading();
                        break;
                    case State.SendingRows:
                        rowSenderTimer.StopSending();
                        break;
                }
            }
            rowLoaderThread = null;
            rowSenderTimer = null;
        }

        class LoadAllRowsWorker
        {
            private readonly StreamingSimulatorAdapter owner;
            private bool stop = false;

            public LoadAllRowsWorker(StreamingSimulatorAdapter owner)
            {
                this.owner = owner;
            }

            public void LoadRowsAndInitiateTimer()
            {
                StreamingSimulatorTable table = owner.table;
                StreamingSimulatorSettings settings = owner.settings;

                if (table.ObjectRows == null)
                {
                    // We need to read up all rows from the file
                    table.ObjectRows = LoadRowsAndSort(settings, owner.table);
                }

                // Fix: DDTV-1220 - Designer freezes if updater.flush() to close to start()
                Thread.Sleep(settings.Limit);

                // Create a timer that will send the rows with intervals.
                RowSenderTimer timer = null;
                lock (owner.stateLock)
                {
                    if (stop)
                    {
                        return;
                    }

                    timer = new RowSenderTimer(owner);
                    timer.Enabled = false;
                    owner.state = State.SendingRows;
                    owner.rowSenderTimer = timer;
                    owner.updater.UseInclusiveTimeWindow = false;
                }
                if (table.ObjectRowIndex < settings.StartupSetSize)
                {
                    // We need to send all initial rows. Do that manually.
                    timer.SetSizeToSend = settings.StartupSetSize;
                    timer.sendRowsTimer_Elapsed(null, null);
                }
                // Send the rest of the rows to the table updater in chunks of UpdateSetSize, 
                // using the Timer.
                timer.SetSizeToSend = settings.UpdateSetSize;
                timer.Enabled = true;
            }

            private List<object[]> LoadRowsAndSort(StreamingSimulatorSettings settings, ParameterTable table)
            {
                // First: Load all rows
                List<object[]> rows = new List<object[]>();
                StreamReader stream = null;
                try
                {
                    try
                    {
                        stream = Utils.GetStreamAndSkipRows(settings, table.Parameters);
                    }
                    catch (Exception ex)
                    {
                        Log.Error(Properties.Resources.LogStreamException, ex.Message);
                        return rows;
                    }

                    TextFileParserSettings parserSettings = (TextFileParserSettings) settings.ParserSettings;

                    if (parserSettings.MaxIndex < 0) return rows;

                    StaticTextAnalyser analyser = new StaticTextAnalyser(new TextAnalyser(parserSettings));
                    string line = stream.ReadLine();
                    if (parserSettings.IsFirstRowHeading)
                    {
                        // First row was the header row => read a new line
                        line = stream.ReadLine();
                    }
                    while (line != null && !stop)
                    {
                        object[] row = new object[parserSettings.MaxIndex + 1];

                        int numRead = analyser.GetColumnValues(line, row);

                        // Throw away rows that are too short so we cannot fill row.
                        if (numRead == row.Length)
                        {
                            // Check if parameters also allow the row to be sent
                            if (owner.table.ParameterFilter == null || owner.table.ParameterFilter.Contains(row))
                            {
                                rows.Add(row);
                            }
                        }
                        line = stream.ReadLine();
                    }
                }
                finally
                {
                    try
                    {
                        if (stream != null)
                        {
                            stream.Close();
                        }
                    }
                    catch (Exception e)
                    {
                        // Couldn't close the stream! Write warning in log.
                    }
                }
                if (stop)
                {
                    return null;
                }

                return SortRows(rows, settings);
            }

            private List<object[]> SortRows(List<object[]> rows,
                StreamingSimulatorSettings settings)
            {
                if (SortOrder.None.Equals(settings.SortOrder))
                {
                    // We do not want to sort the rows => just return them as they are
                    return rows;
                }
                // Second: Sort the rows
                // We need to convert the values in the column that shall be 
                // sorted into correct type.

                TextFileColumnDefinition sortColumn = (TextFileColumnDefinition)settings.SelectedColumnToSort;
                int sortIndex = sortColumn.Index;
                foreach (object[] row in rows)
                {
                    if (stop)
                    {
                        return null;
                    }
                    // Prepare sorting by converting the sorted column value into 
                    // target type.
                    object value = row[sortIndex];
                    row[sortIndex] = Utils.ConvertSortColumnValueToRightType(sortColumn, value,
                        settings.NumericDataHelper);
                }
                IComparer<object> comparer = null;
                switch (sortColumn.Type)
                {
                    case ColumnType.Numeric:
                        comparer = new NumericComparer(sortIndex);
                        break;
                    case ColumnType.Time:
                        comparer = new TimeComparer(sortIndex);
                        break;
                    case ColumnType.Text:
                        comparer = new TextComparer(sortIndex);
                        break;
                }
                if (SortOrder.Descending.Equals(settings.SortOrder))
                {
                    comparer = new DescendingComparer(comparer);
                }
                rows.Sort(comparer);

                return rows;
            }

            public void StopLoading()
            {
                stop = true;
            }
        }

        class RowSenderTimer : Timer
        {
            private readonly TableUpdater<StreamingSimulatorTable, StreamingSimulatorSettings,
                Dictionary<string, object>> updater;
            private readonly StreamingSimulatorTable table;
            readonly List<string> columnNames = new List<string>();
            readonly List<int> columnIndexes = new List<int>();
            private readonly bool loop = false;
            private readonly string idColumn;
            private int setSizeToSend;
            private bool stop = false;

            public RowSenderTimer(StreamingSimulatorAdapter owner)
                : base(owner.settings.Limit)
            {
                Elapsed +=
                    new ElapsedEventHandler(sendRowsTimer_Elapsed);
                StreamingSimulatorSettings settings = owner.settings;
                this.updater = owner.updater;
                this.table = owner.table;
                this.loop = settings.CanLoop && settings.Loop;
                idColumn = settings.IdColumn;

                foreach (TextFileColumnDefinition columnDefinition in settings.ParserSettings.Columns)
                {
                    if (!columnDefinition.IsEnabled) continue;

                    columnNames.Add(columnDefinition.Name);
                    columnIndexes.Add(columnDefinition.Index);
                }
            }

            public void sendRowsTimer_Elapsed(object sender, ElapsedEventArgs e)
            {
                // Lock because if the Real-Time refresh rate is to fast the Timer will 
                // call this method before the previous call is done.
                lock (updater)
                {
                    Enabled = false;
                    int sentThisRound = 0;
                    while (table.IsMoreRowsToSend && sentThisRound < setSizeToSend)
                    {
                        object[] row = table.NextRow;
                        // Create the message dictionary
                        Dictionary<string, object> dict = new Dictionary<string, object>();
                        for (int j = 0; j < columnNames.Count; j++)
                        {
                            int columnIndex = columnIndexes[j];

                            if (row[columnIndex] != null)
                            {
                                dict[columnNames[j]] = row[columnIndex];
                            }
                        }
                        // Find the idColumnString
                        object id;

                        if (!dict.TryGetValue(idColumn, out id) || id == null)
                        {
                            continue;
                        }
                        string idString = id.ToString();
                        if (string.IsNullOrEmpty(idString))
                        {
                            continue;
                        }
                        if (stop)
                        {
                            return;
                        }
                        // Send row to the table updater
                        updater.EnqueueUpdate(idString, dict);
                        sentThisRound++;
                    }
                    if (stop)
                    {
                        return;
                    }
                    if (sentThisRound > 0)
                    {
                        updater.Flush();
                    }
                    if (stop)
                    {
                        return;
                    }

                    if (!table.IsMoreRowsToSend)
                    {
                        // The last row have been sent => check if we are going to 
                        // resend all again or not.
                        if (loop)
                        {
                            table.ObjectRowIndex = 0;
                        }
                        else
                        {
                            Enabled = false;
                            return;
                        }
                    }
                    Enabled = true;
                }
            }

            public int SetSizeToSend
            {
                set { setSizeToSend = value; }
            }

            public void StopSending()
            {
                stop = true;
                Enabled = false;
            }
        }

    }
}
