﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.StreamingSimulatorPlugin
{
    /// <summary>
    /// Plugin for Stream Simulator.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : MessageQueuePluginBase<StreamingSimulatorTable, StreamingSimulatorSettings,
        Dictionary<string, object>, StreamingSimulatorAdapter>
    {
        internal const string PluginId = "StreamSimulator";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Stream Simulator";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override StreamingSimulatorSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            StreamingSimulatorSettings streamingSimulatorSettings
                = new StreamingSimulatorSettings(pluginManager, parameters);

            ((TextFileParserSettings) streamingSimulatorSettings.ParserSettings).IsFirstRowHeading = true;


            return streamingSimulatorSettings;
        }

        public override StreamingSimulatorSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new StreamingSimulatorSettings(pluginManager, bag, null);
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag bag, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            StreamingSimulatorSettings settings =
                new StreamingSimulatorSettings(pluginManager, bag, parameters);

            if (settings.FilePathType == PathType.File)
            {
                string fixedFilePath = DataUtils.FixFilePath(workbookDir, dataDir,
                    settings.FilePath, parameters);
                if (!settings.IsFilePathParameterized)
                {
                    settings.FilePath = fixedFilePath;
                }
            }
            return base.GetData(workbookDir, dataDir, bag, parameters, rowcount);
        }

        protected override ITable CreateTable(StreamingSimulatorSettings connectionSettings,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;
            StreamingSimulatorTable table = new StreamingSimulatorTable(parameters);
            table.BeginUpdate();
            try
            {
                DataPluginUtils.AddColumns(table,
                    connectionSettings.ParserSettings.Columns);
                table.ParameterFilter = ParameterFilter.CreateFilter(connectionSettings.ParserSettings.Columns.ToArray(), parameters);

            }
            finally
            {
                table.EndUpdate();
            }
            Log.Info(Datawatch.DataPrep.Data.Core.Connectors.Properties.Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);
            return table;
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            return DataPluginUtils.GetDataFiles(
                new StreamingSimulatorSettings(pluginManager, settings, null));
        }

        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();

            DataPluginUtils.UpdateFileLocation(
                new StreamingSimulatorSettings(pluginManager, settings, null),
                oldPath, newPath, workbookPath);
        }
    }
}
