﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.StreamingSimulatorPlugin
{
    public class StreamingSimulatorSettings : MessageQueueSettingsBase,
        IFilePathSettings, ITextFileSettings
    {
        private ChunkedStringHelper textHelper;

        public StreamingSimulatorSettings(IPluginManager pluginManager)
            : this(pluginManager, new PropertyBag(), null)
        {
        }

        public StreamingSimulatorSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
        }

        public StreamingSimulatorSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            InitTextHelper();
            ManualFlush = true;
        }

        private void InitTextHelper()
        {
            PropertyBag textSubGroup = GetInternalSubGroup("Text");
            if (textSubGroup == null)
            {
                textSubGroup = new PropertyBag();
            }
            textHelper = new ChunkedStringHelper(textSubGroup);
        }

        protected override string DefaultParserPluginId
        {
            get { return "Text"; }
        }

        public PathType FilePathType
        {
            get
            {
                string s = GetInternal("TextFilePathType");
                if (s != null)
                {
                    try
                    {
                        return (PathType)Enum.Parse(
                            typeof(PathType), s);
                    }
                    catch
                    {
                    }
                }
                return PathType.File;
            }

            set { SetInternal("TextFilePathType", value.ToString()); }
        }

        public string FilePath
        {
            get { return GetInternal("FilePath"); }
            set { SetInternal("FilePath", value); }
        }

        public bool IsFilePathParameterized
        {
            get
            {
                if (this.Parameters == null)
                {
                    return false;
                }

                foreach (ParameterValue parameter in this.Parameters)
                {
                    if (this.FilePath.Contains("{" + parameter.Name + "}") ||
                        this.FilePath.Contains("$" + parameter.Name))
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public string WebPath
        {
            get { return GetInternal("WebPath", "http://"); }
            set { SetInternal("WebPath", value); }
        }
        
        public string WebUserID
        {
            get { return GetInternal("WebUserID"); }
            set { SetInternal("WebUserID", value); }
        }

        public string WebPassword
        {
            get { return GetInternal("WebPassword"); }
            set { SetInternal("WebPassword", value); }
        }

        public string RequestBody
        {
            get { return GetInternal("RequestBody"); }
            set { SetInternal("RequestBody", value); }
        }

        public int RequestTimeout
        {
            get { return GetInternalInt("RequestTimeout", 10); }
            set { SetInternalInt("RequestTimeout", value); }
        }

        public WebHeaderCollection WebHeaderCollection
        {
            get { return null; }
        }

        public int[] RequestTimeouts
        {
            get
            {
                return new int[]
                {
                    10,
                    20,
                    30,
                    60
                };
            }
        }

        public string Text
        {
            get { return textHelper.Text; }
            set
            {
                textHelper.Text = value;
                SetInternalSubGroup("Text", textHelper.ToPropertyBag());
            }
        }

        public int DataDiscoveryRowCount
        {
            get { return GetInternalInt("DataDiscoveryRowCount", 10); }
            set { SetInternalInt("DataDiscoveryRowCount", value); }
        }

        public int[] DataTypeDiscoveryRows
        {
            get { return DropDownDataProvider.DataTypeDiscoveryRows; }
        }

        public int SkipRows
        {
            get { return GetInternalInt("SkipRows", 0); }
            set { SetInternalInt("SkipRows", value); }
        }

        public int[] AvailableSkipRows
        {
            get
            {
                int[] availableSkipRows = new int[11];
                for (int i = 0; i < availableSkipRows.Length; i++)
                {
                    availableSkipRows[i] = i;
                }
                return availableSkipRows;
            }
        }

        public override bool ShowParserSelector
        {
            get { return false; }
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        public override bool CanGenerateColumns()
        {
            return IsPathOk() && IsColumnDelimiterOk;
        }

        public bool IsPathOk()
        {
            if (FilePathType == PathType.File &&
                FilePath != null)
            {
                return FilePath.Length > 0;
            }
            if (FilePathType == PathType.WebURL &&
                WebPath != null)
            {
                return WebPath.StartsWith("http://", true, CultureInfo.InvariantCulture) ||
                       WebPath.StartsWith("https://", true, CultureInfo.InvariantCulture);
            }
            if (FilePathType == PathType.Text &&
                Text != null)
            {
                return Text.Length > 0;
            }

            return false;
        }

        private bool IsColumnDelimiterOk
        {
            get
            {
                TextFileParserSettings textFileParserSettings =
                    (TextFileParserSettings)ParserSettings;

                if (textFileParserSettings.ColumnDelimiter ==
                        DropDownDataProvider.OtherColumnDelimiterValue &&
                    string.IsNullOrEmpty(textFileParserSettings.CustomColumnDelimiter))
                {
                    return false;
                }
                return true;
            }
        }

        public override void DoGenerateColumns()
        {
            StreamReader stream = null;
            try
            {
                stream = Utils.GetStreamAndSkipRows(this, Parameters);
            }
            catch (Exception e)
            {
                this.errorReporter.Report("Creating stream", e.Message);
            }
            if (stream == null) return;

            ColumnGenerator cg = new ColumnGenerator(ParserSettings, NumericDataHelper);

            int rowsToDiscover = DataDiscoveryRowCount;
            if (((TextFileParserSettings)ParserSettings).IsFirstRowHeading)
            {
                rowsToDiscover++;
            }
            while (!stream.EndOfStream)
            {
                string row = stream.ReadLine();

                if (string.IsNullOrEmpty(row)) continue;

                cg.Generate(row);
                rowsToDiscover--;
                if (rowsToDiscover <= 0)
                {
                    break;
                }
            }
            stream.Close();

            //UpdateIdCandidates(null, null);
            if (SortColumns.Any())
            {
                SelectedColumnToSort = SortColumns.First();
            }
            FirePropertyChanged("SortColumns");
        }

        /// <summary>
        /// Gets a value indicating wether the current StreamingSimulationSettings 
        /// instance can be okeyed.
        /// </summary>
        public override bool IsOk
        {
            get
            {
                if (!IsParserSettingsOk)
                {
                    return false;
                }
                if (!IsColumnDelimiterOk)
                {
                    return false;
                }
                if (!IsAllColumnIndexOk)
                {
                    return false;
                }
                return IsPathOk();
            }
        }

        private bool IsAllColumnIndexOk
        {
            get
            {
                HashSet<int> indexes = new HashSet<int>();
                foreach (TextFileColumnDefinition colDef in ParserSettings.Columns)
                {
                    if (indexes.Contains(colDef.Index))
                    {
                        return false;
                    }
                    indexes.Add(colDef.Index);
                }
                return true;
            }
        }

        public bool CanLoop
        {
            get
            {
                return (TimeIdColumn == null || IsTimeIdColumnGenerated);
            }
        }

        public bool Loop
        {
            get
            {
                string looping = GetInternal("Loop",
                  Convert.ToString(false));
                return Convert.ToBoolean(looping);
            }
            set { SetInternal("Loop", Convert.ToString(value)); }
        }

        public ColumnDefinition SelectedColumnToSort
        {
            get
            {
                string columnName = GetInternal("SelectedColumnToSort");
                if (columnName == null) return null;
                return ParserSettings.GetColumn(columnName);
            }
            set
            {
                SetInternal("SelectedColumnToSort", value.Name);
            }
        }

        public IEnumerable<ColumnDefinition> SortColumns
        {
            get
            {
                return ParserSettings.Columns;
            }
        }

        public SortOrder SortOrder
        {
            get
            {
                String sortOrder = GetInternal("SortOrder", "None");
                SortOrder sortOrderEnum;
                if (Enum.TryParse(sortOrder, out sortOrderEnum))
                {
                    return sortOrderEnum;
                }
                return SortOrder.None;
            }
            set
            {
                SetInternal("SortOrder", value.ToString());
                FirePropertyChanged("SortOrder");
                FirePropertyChanged("SortEnabled");
            }
        }

        public SortOrder[] SortOrders
        {
            get
            {
                return Enum.GetValues(typeof(SortOrder)).Cast<SortOrder>().ToArray();
            }
        }

        public bool SortEnabled
        {
            get { return !SortOrder.None.Equals(SortOrder); }
        }

        public int UpdateSetSize
        {
            get { return GetInternalInt("UpdateSetSize", 1); }
            set { SetInternalInt("UpdateSetSize", value); }
        }

        public int StartupSetSize
        {
            get { return GetInternalInt("StartupSetSize", 0); }
            set { SetInternalInt("StartupSetSize", value); }
        }

        public override TimeIdColumnViewModel SelectedTimeIdColumnViewModel
        {
            get
            {
                return base.SelectedTimeIdColumnViewModel;
            }
            set
            {
                base.SelectedTimeIdColumnViewModel = value;
                FirePropertyChanged("CanLoop");
            }
        }

        public override bool Equals(object obj)
        {
            if (!(obj is StreamingSimulatorSettings)) return false;

            if (!base.Equals(obj)) return false;

            StreamingSimulatorSettings cs = (StreamingSimulatorSettings)obj;

            if (!FilePathType.Equals(cs.FilePathType) ||
                !string.Equals(FilePath, cs.FilePath) ||
                !string.Equals(WebPath, cs.WebPath) ||
                !string.Equals(WebUserID, cs.WebUserID) ||
                !string.Equals(WebPassword, cs.WebPassword) ||
                !string.Equals(RequestBody, cs.RequestBody) ||
                RequestTimeout != cs.RequestTimeout ||
                !string.Equals(Text, cs.Text) ||
                DataDiscoveryRowCount != cs.DataDiscoveryRowCount ||
                SkipRows != cs.SkipRows ||
                Loop != cs.Loop ||
                !Equals(SelectedColumnToSort, cs.SelectedColumnToSort) ||
                !SortOrder.Equals(cs.SortOrder) ||
                UpdateSetSize != cs.UpdateSetSize ||
                StartupSetSize != cs.StartupSetSize)
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            int hashCode = base.GetHashCode();

            object[] fields = { FilePathType, FilePath, WebPath, WebUserID, WebPassword,
                                  RequestBody, RequestTimeout, Text, DataDiscoveryRowCount,
                                  SkipRows, Loop, SelectedColumnToSort, SortOrder,
                                  UpdateSetSize, StartupSetSize};

            foreach (object field in fields)
            {
                if (field != null)
                {
                    hashCode ^= field.GetHashCode();
                }
            }

            return hashCode;
        }
    }
}
