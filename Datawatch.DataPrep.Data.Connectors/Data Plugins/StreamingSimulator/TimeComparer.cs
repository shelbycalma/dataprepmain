﻿using System;
using System.Collections.Generic;

namespace Panopticon.StreamingSimulatorPlugin
{
    class TimeComparer : IComparer<object>
    {
        private readonly int sortIndex;

        public TimeComparer(int sortIndex)
        {
            this.sortIndex = sortIndex;
        }

        public int Compare(object o1, object o2)
        {
            object[] arr1 = (object[])o1;
            object[] arr2 = (object[])o2;
            return ((DateTime)arr1[sortIndex]).CompareTo((DateTime)arr2[sortIndex]);
        }
    }
}
