﻿using System.Collections.Generic;

namespace Panopticon.StreamingSimulatorPlugin
{
    class NumericComparer : IComparer<object>
    {
        private readonly int sortIndex;

        public NumericComparer(int sortIndex)
        {
            this.sortIndex = sortIndex;
        }

        public int Compare(object o1, object o2)
        {
            object[] arr1 = (object[])o1;
            object[] arr2 = (object[])o2;
            return ((double)arr1[sortIndex]).CompareTo((object)arr2[sortIndex]);
        }
    }
}
