﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.StreamingSimulatorPlugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Panopticon.StreamingSimulator.dll")]
[assembly: AssemblyDescription("Datawatch StreamingSimulator Plug-in")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]