﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.SalesForcePlugin.Enums;
using Panopticon.SalesForcePlugin.UI.UiHelpers;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;

namespace Panopticon.SalesForcePlugin.UI
{
    public class SalesForceSettingsViewModel : ViewModelBase
    {
        private readonly IEnumerable<ParameterValue> _parameters;

        #region Fields

        private ICommand _loginCommand;
        private ICommand _previewCommand;
        private DataTable _previewTable;

        private SalesForceAnalyticsClient salesForceClient =
            new SalesForceAnalyticsClient();

        private List<ReportSelection> _availableReports;
        private List<SObjectTableSelection> _availableTables;
        private SObjectTableSelection _selectedTable;
        private bool _isConnectEnabled;
        private bool _isConnected;
        private List<FolderSelection> _availableReportFolders;
        private FolderSelection _selectedReportFolder;

        private List<ReportSelection> _reports;
        private ReportSelection _selectedReport;

		//private List<string> _columns;
		private ObservableCollection<DatabaseColumn> columns = new ObservableCollection<DatabaseColumn>();
		private ObservableCollection<DatabaseColumn> availableColumns;
		private string _selectedColumns;

		private List<string> _servers;

        public Dictionary<string, List<ReportSelection>> ReportsByFolder =
            new Dictionary<string, List<ReportSelection>>();

        public event EventHandler SettingsLoaded;

        protected virtual void OnSettingsLoaded()
        {
            EventHandler handler = SettingsLoaded;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        #endregion

        #region Properties

        public SalesForceSettings Model { get; private set; }
        public bool ConnectionActive { get; set; }

        public ICommand LoginCommand
        {
            get
            {
                return _loginCommand ??
                       (_loginCommand = new DelegateCommand(Login));
            }
        }

        public ICommand PreviewCommand
        {
            get
            {
                return _previewCommand ??
                       (_previewCommand = new DelegateCommand(PreviewData, CanPreview));
            }
        }

        public bool CanPreview()
        {
            return !string.IsNullOrEmpty(Model.User) &&
                ((Model.ObjectType == ObjectType.Report &&
                !string.IsNullOrEmpty(Model.ReportName)) ||
                (Model.ObjectType == ObjectType.Table &&
                !string.IsNullOrEmpty(Model.TableName) && (!string.IsNullOrEmpty(Model.ColumnNames) ||
                Model.SelectedColumns.Count > 0)));
        }

        public bool IsConnected
        {
            get { return _isConnected; }
            private set
            {
                _isConnected = value;
                OnPropertyChanged("IsConnected");
            }
        }

        public bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(Model.User) &&
                       ((Model.ObjectType == ObjectType.Report &&
                         !string.IsNullOrEmpty(Model.ReportName)) ||
                        (Model.ObjectType == ObjectType.Table &&
                         !string.IsNullOrEmpty(Model.TableName) && (!string.IsNullOrEmpty(Model.ColumnNames) || 
						 Model.SelectedColumns.Count > 0)));
            }
        }

        public string LoginServer
        {
            get { return Model.LoginServer; }
            set
            {
                if (value != Model.LoginServer)
                {
                    Model.LoginServer = value;
                    OnPropertyChanged("LoginServer");
                }
            }
        }

        public bool ReportCsvExport
        {
            get { return Model.ReportCsvExport; }
            set
            {
                if (value != Model.ReportCsvExport)
                {
                    Model.ReportCsvExport = value;
                    OnPropertyChanged("ReportCsvExport");
                }
            }
        }

        public string ReportCsvExportFileName
        {
            get { return Model.ReportCsvExportFileName; }
            set
            {
                if (value != Model.ReportCsvExportFileName)
                {
                    Model.ReportCsvExportFileName = value;
                    OnPropertyChanged("ReportCsvExportFileName");
                }
            }
        }

        public string User
        {
            get { return Model.User; }
            set
            {
                if (value != Model.User)
                {
                    Model.User = value;
                    OnPropertyChanged("User");
                }
            }
        }

        public string Password
        {
            get { return Model.Password; }
            set
            {
                if (value != Model.Password)
                {
                    Model.Password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public bool IsConnectEnabled
        {
            get { return _isConnectEnabled; }
            private set
            {
                _isConnectEnabled = value;
                OnPropertyChanged("IsConnectEnabled");
            }
        }

        public List<ReportSelection> AvailableReports
        {
            get
            {
                if (_availableReports == null)
                    _availableReports = new List<ReportSelection>();
                return _availableReports;
            }
            set
            {
                if (_availableReports == value)
                    return;
                _availableReports = value;
                OnPropertyChanged("AvailableReports");
            }
        }

        public List<ReportSelection> Reports
        {
            get
            {
                if (_reports == null)
                    _reports = new List<ReportSelection>();
                return _reports;
            }
            set
            {
                if (_reports.Equals(value))
                    return;
                _reports = value;
                OnPropertyChanged("Reports");
            }
        }

		public ObservableCollection<DatabaseColumn> AvailableColumns
		{
			get
			{
				if (availableColumns != null)
				{
					return availableColumns;
				}

				//availableColumns = PopulateColumns();

				if (availableColumns == null)
				{
					return null;
				}

				return availableColumns;
			}
		}

		public virtual DatabaseColumn CreateColumn(object v)
		{
			return new DatabaseColumn((DatabaseColumn)v);
		}

		public DatabaseColumn NewColumn
		{
			get
			{
				return null;
			}
			set
			{
				DatabaseColumn newCol = CreateColumn(value);
				newCol.UserDefined = true;
				newCol.AppliedParameterName = "";

				newCol.AggregateType = AggregateType.None;

				newCol.PropertyChanged += AvailableColumns_PropertyChanged;
				columns.Add(newCol);
			}
		}

		public ObservableCollection<DatabaseColumn> Columns
		{
			get
			{
				columns = new ObservableCollection<DatabaseColumn>();

				if (AvailableColumns == null)
				{
					return columns;
				}
				foreach (DatabaseColumn availableColumn in AvailableColumns)
				{
					DatabaseColumn column = CreateColumn(availableColumn);
					foreach (DatabaseColumn selectedCol in Model.SelectedColumns)
					{
						if (selectedCol.ColumnName == column.ColumnName)
						{
							column.Selected = selectedCol.Selected;
						}
					}
					column.PropertyChanged += AvailableColumns_PropertyChanged;
					columns.Add(column);
				}
				return columns;
			}
		}

		public string SelectedColumns
		{
			get
			{
				return _selectedColumns;
			}
			set
			{
				if (Object.Equals(_selectedColumns, value))
					return;
				_selectedColumns = value;
				ColumnsBox_OnSelectionChanged();
				OnPropertyChanged("SelectedColumns");
			}
		}

		public ReportSelection SelectedReport
        {
            get
            {
                return _selectedReport;
            }
            set
            {
                if (Object.Equals(_selectedReport, value))
                    return;
                _selectedReport = value;
                ReportsBox_OnSelectionChanged();
                OnPropertyChanged("SelectedReport");
            }
        }

        public List<SObjectTableSelection> AvailableTables
        {
            get
            {
                if (_availableTables == null)
                    _availableTables = new List<SObjectTableSelection>();
                _availableTables = _availableTables.OrderBy(x => x.Label).ToList();
                return _availableTables;
            }
            set
            {
                if (_availableTables == value)
                    return;
                _availableTables = value;
                OnPropertyChanged("AvailableTables");
            }
        }

        public SObjectTableSelection SelectedTable
        {
            get
            {
                return _selectedTable;
            }
            set
            {
                if (Object.Equals(_selectedTable, value))
                    return;
                _selectedTable = value;
                TablesListBox_OnSelectionChanged();
                OnPropertyChanged("SelectedTable");
            }
        }

        public List<FolderSelection> AvailableReportFolders
        {
            get
            {
                if (_availableReportFolders == null)
                    _availableReportFolders = new List<FolderSelection>();
                _availableReportFolders = _availableReportFolders.OrderBy(x => x.Name).ToList();
                return _availableReportFolders;
            }
            set
            {
                if (_availableReportFolders == value)
                    return;
                _availableReportFolders = value;
                OnPropertyChanged("AvailableReportFolders");
            }
        }

        public FolderSelection SelectedReportFolder
        {
            get
            {
                return _selectedReportFolder;
            }
            set
            {
                if (Object.Equals(_selectedReportFolder, value))
                    return;
                _selectedReportFolder = value;
                FolderSelector_OnSelectionChanged();
                OnPropertyChanged("SelectedReportFolder");
            }
        }

        public List<string> Servers
        {
            get
            {
                if (_servers == null)
                {
                    _servers = new List<string>()
                    {
                        "login.salesforce.com",
                        "test.salesforce.com"
                    };
                }
                return _servers;
            }
            set
            {
                if (_servers.Equals(value))
                {
                    return;
                }

                _servers = value;
                OnPropertyChanged("Servers");
            }
        }

        #endregion

        #region Constructors

        public SalesForceSettingsViewModel(
            SalesForceSettings model, IEnumerable<ParameterValue> parameters)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }
            _parameters = parameters;
            salesForceClient.ApiVersion = "v30.0";
            Model = model;
            Model.PropertyChanged += OnModelChanged;
        }

        #endregion

        #region Methods

        private void OnModelChanged(object sender, PropertyChangedEventArgs e)
        {
            // todo refresh ui when specific connection params changed
            switch (e.PropertyName)
            {
                default:
                    OnPropertyChanged();
                    break;
            }
        }

        public DataTable PreviewTable
        {
            get { return _previewTable; }
            set
            {
                _previewTable = value;
                OnPropertyChanged("PreviewTable");
            }
        }
		        
		private PropertyBag CreatePropertyBag()
		{
			PropertyBag pb = new PropertyBag();
			PropertyValue pv = new PropertyValue("LoginServer", LoginServer);
			pb.Values.Add(pv);
			pv = new PropertyValue("User", Model.User);
			pb.Values.Add(pv);
			pv = new PropertyValue("Password", Model.Password);
			pb.Values.Add(pv);
			pv = new PropertyValue("SecurityToken", Model.SecurityToken);
			pb.Values.Add(pv);
			pv = new PropertyValue("ObjectType", (Model.ObjectType == ObjectType.Table) ? "Table" : "Report");
			pb.Values.Add(pv);
			if (Model.ObjectType == ObjectType.Table)
			{
				pv = new PropertyValue("TableName", Model.TableName);
				pb.Values.Add(pv);
				GetSelectedColumns();
				if (Model.ColumnNames.Length == 0)
				{
					MessageBox.Show(Properties.Resources.UiErrorNoColumnsSelected, Properties.Resources.UiErrorHeader);
					return null;
				}
				else
				{
					pv = new PropertyValue("ColumnNames", Model.ColumnNames);
					pb.Values.Add(pv);
				}
			}
			else
			{
				if (Model.ReportId == null || Model.ReportName == null)
				{
					MessageBox.Show(Properties.Resources.UiErrorNoReportSelected, Properties.Resources.UiErrorHeader);
					return null;
				}
				else
				{
					pv = new PropertyValue("ReportId", Model.ReportId);
					pb.Values.Add(pv);
					pv = new PropertyValue("ReportName", Model.ReportName);
					pb.Values.Add(pv);
					pv = new PropertyValue("ReportFolderId", Model.ReportFolderId);
					pb.Values.Add(pv);
					pv = new PropertyValue("ReportCsvExport", Model.ReportCsvExportFileName);
					pb.Values.Add(pv);
				}
			}
			pv = new PropertyValue("Title", Model.Title);
			pb.Values.Add(pv);

			return pb;
		}

		internal void PreviewData()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

		int previewRowLimit;
		RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

		if (previewRowLimit == 0)
		{
			previewRowLimit = 10;
		}

                Panopticon.SalesForcePlugin.Plugin p = new Plugin();
				PropertyBag bag = CreatePropertyBag();

				if (bag != null)
				{
					ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);

					if (t != null)
					{
						_previewTable = PreviewDataTable.GetPreviewTable(t);
						OnPropertyChanged("PreviewTable");
					}
				}
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show((ErrorMessages.GetDisplayMessage(ex.Message).Length > 0) ? ErrorMessages.GetDisplayMessage(ex.Message) : ex.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        internal void Login()
        {
            IsConnectEnabled = false;
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                if (!DoLogin())
                {
                    return;
                }
                PopulateAll();
                ConnectionActive = true;
                OnPropertyChanged("ConnectionActive");
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show((ErrorMessages.GetDisplayMessage(ex.Message).Length > 0) ? ErrorMessages.GetDisplayMessage(ex.Message) : ex.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                IsConnectEnabled = true;
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }

            if (Model.ObjectType == ObjectType.Unknown)
            {
                Model.ObjectType = ObjectType.Table;
            }
        }

        private void PopulateAll()
        {
            try
            {
                PopulateReportFolders();
                PopulateReports();
                OrganizeReports();
				PopulateTables();
				SelectItemsFromSettings();
				OnSettingsLoaded();
			}
			catch (Exception ex)
            {
                Log.Info(ex.Message);
            }
        }

        private void SelectItemsFromSettings()
        {
            if (Model == null ||
                Model.ObjectType == ObjectType.Unknown)
            {
                return;
            }

            if (Model.ObjectType == ObjectType.Table &&
                AvailableTables != null)
            {
                SObjectTableSelection prevTable = AvailableTables
                    .Find(selection => selection.Name == Model.TableName);

                SelectedTable = prevTable;
				SelectedColumns = (Model.ColumnNames != null ? Model.ColumnNames : "");
				LoadSelectedColumns();
				return;
            }

            if (Model.ObjectType == ObjectType.Report &&
                Reports != null &&
                AvailableReportFolders != null)
            {
                FolderSelection prevFolder = AvailableReportFolders
                    .Find(selection => selection.ID == Model.ReportFolderId);

                SelectedReportFolder = prevFolder;


                ReportSelection prevReport = Reports
                    .Find(selection => selection.ID == Model.ReportId
                        && selection.Name == Model.ReportName);

                SelectedReport = prevReport;
                
                return;
            }
        }

        private bool DoLogin()
        {
            if (string.IsNullOrEmpty(Model.LoginServer))
            {
                MessageBox.Show(Properties.Resources.UiErrorNoLoginServerSpecified,
                    Properties.Resources.UiErrorHeader);
                return false;
            }
            if (string.IsNullOrEmpty(Model.User))
            {
                MessageBox.Show(Properties.Resources.UiErrorNoUsernameSpecified, 
                    Properties.Resources.UiErrorHeader);
                return false;
            }
            if (string.IsNullOrEmpty(Model.Password))
            {
                MessageBox.Show(Properties.Resources.UiErrorNoPasswordSpecified, 
                   Properties.Resources.UiErrorHeader);
                return false;
            }
            if (salesForceClient == null || string.IsNullOrEmpty(Model.User))
            {
                IsConnected = false;
                return false;
            }

            salesForceClient.Authenticate(_parameters, Model.LoginServer, Model.User,
                Model.GetFullPassword(_parameters));
            IsConnected = salesForceClient.IsAuthenticated;
            return IsConnected;
        }

		private bool IsColumnSelected(string colname)
		{
			if (Model.ColumnNames != null)
			{
				if (Model.ColumnNames.Length > 0)
				{
					string[] col = Model.ColumnNames.Split(',');
					if (col.Contains(colname))
						return true;
				}
			}
			return false;
		}

		private ObservableCollection<DatabaseColumn> PopulateColumns()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				ObservableCollection<DatabaseColumn> tableColumns = new ObservableCollection<DatabaseColumn>();
				string columnNames = salesForceClient.GetTableColumns(SelectedTable.Name);
                //string[] col = columnNames.Split(',');
                string[] col = columnNames.Split('~');
                foreach (string s in col)
				{
					string[] collabel = s.Split('^');
					DatabaseColumn newcol = new DatabaseColumn();
					newcol.ColumnName = collabel[0];
					newcol.ColumnLabel = collabel[1];
					newcol.ColumnType = ColumnType.Text;
					newcol.Selected = IsColumnSelected(collabel[0]);
					newcol.AggregateType = AggregateType.None;
					newcol.PropertyChanged += AvailableColumns_PropertyChanged;
					tableColumns.Add(newcol);
				}
				return tableColumns;
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

		private void PopulateTables()
        {
            List<KeyValuePair<string, string>> tables = salesForceClient.GetQueryableTables();
            List<SObjectTableSelection> tableSelections = new List<SObjectTableSelection>();
            foreach (KeyValuePair<string, string> tbl in tables)
            {
//                tableSelections.Add(new SObjectTableSelection(tbl.Key, tbl.Value));
				tableSelections.Add(new SObjectTableSelection(tbl.Key, tbl.Key));
			}
			tableSelections.Sort();
            AvailableTables = tableSelections;
        }

        private void PopulateReports()
        {
            IList<ReportSelection> queryList =
                salesForceClient.Query<ReportSelection>(
                    "Select Name, ID, OwnerID FROM Report");

            List<ReportSelection> reportList
                = new List<ReportSelection>(queryList);
            reportList.Sort();

            AvailableReports = reportList;
        }

        private void PopulateReportFolders()
        {
            List<FolderSelection> allFolders = new List<FolderSelection>()
            {
                new FolderSelection()
                {
                    DeveloperName = "Unfiled Reports",
                    ID = "__UNFILED__",
                    Name = "Unfiled Reports"
                }
            };
            allFolders.Add(new FolderSelection()
            {
                DeveloperName = "My Personal Custom Reports",
                ID = "__PERSONAL__",
                Name = "My Personal Custom Reports"
            });

            IList<FolderSelection> queryList = salesForceClient
                .Query<FolderSelection>(
                    "Select DeveloperName, Name, ID FROM Folder WHERE Type = 'Report' AND DeveloperName != NULL");

            List<FolderSelection> folderList
                = new List<FolderSelection>(queryList);
            folderList.Sort();

            allFolders.AddRange(folderList);

            AvailableReportFolders = allFolders;
        }

        private void OrganizeReports()
        {
            foreach (FolderSelection folder in AvailableReportFolders)
            {
                ReportsByFolder[folder.ID] = new List<ReportSelection>();
            }
            ReportsByFolder["__UNFILED__"] = new List<ReportSelection>();
            List<ReportSelection> unfiled = new List<ReportSelection>();
            foreach (ReportSelection availableReport in AvailableReports)
            {
                if (availableReport.OwnerID != null)
                {
                    if (ReportsByFolder.ContainsKey(availableReport.OwnerID))
                    {
                        ReportsByFolder[availableReport.OwnerID].Add(
                            availableReport);
                    }
                    else
                    {
                        if (availableReport.OwnerID ==
                            salesForceClient.CurrentUid)
                            ReportsByFolder["__PERSONAL__"].Add(availableReport);
                        else
                            ReportsByFolder["__UNFILED__"].Add(availableReport);
                    }
                }
                else
                {
                    ReportsByFolder["__UNFILED__"].Add(availableReport);
                }
            }
        }

        private void FolderSelector_OnSelectionChanged()
        {
            if (SelectedReportFolder == null)
            {
                Model.ReportFolderId = null;
            }
            else
            {
                Model.ReportFolderId = SelectedReportFolder.ID;
            }

            List<ReportSelection> reports = new List<ReportSelection>();

            if (SelectedReportFolder == null || SelectedReportFolder.ID == null ||
                !ReportsByFolder.ContainsKey(SelectedReportFolder.ID))
            {
                return;
            }

            foreach (ReportSelection report in ReportsByFolder[SelectedReportFolder.ID])
            {
                reports.Add(report);
            }

            Reports = reports;
        }

		private void AvailableColumns_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			UpdateSelectedColumns();
		}

		private void GetSelectedColumns()
		{
			string columnNames = "";
			int i = 0;
			foreach (DatabaseColumn col in Model.SelectedColumns)
			{
				if (i > 0)
					columnNames += ",";
				columnNames += col.ColumnName;
				i++;
			}
			SelectedColumns = columnNames;
		}

		private void LoadSelectedColumns()
		{
			Model.SelectedColumns.Clear();
			string[] selectedcols = Model.ColumnNames.Split(',');
			foreach (DatabaseColumn col in availableColumns)
			{
				if (selectedcols.Contains(col.ColumnName))
				{
					Model.SelectedColumns.Add(col);
				}
			}
		}

		private void UpdateSelectedColumns()
		{
			Model.SelectedColumns.Clear();
			foreach (DatabaseColumn col in columns)
			{
				if (col.Selected)
				{
					Model.SelectedColumns.Add(col);
				}
			}
		}

		private void ColumnsBox_OnSelectionChanged()
		{
			if (SelectedColumns == null)
			{
				Model.ColumnNames = null;
				return;
			}
			Model.ColumnNames = SelectedColumns;
		}

		private void ReportsBox_OnSelectionChanged()
        {
            if (SelectedReport == null)
            {
                Model.ReportId = null;
                Model.ReportName = null;
                return;
            }
            Model.ReportId = SelectedReport.ID;
            Model.ReportName = SelectedReport.Name;
        }

        private void TablesListBox_OnSelectionChanged()
        {
            if (SelectedTable == null)
            {
                Model.TableName = null;
                return;
            }
			Model.SelectedColumns.Clear();
			if (Model.TableName != SelectedTable.Name)
				Model.ColumnNames = "";

			availableColumns = PopulateColumns();
			Model.TableName = SelectedTable.Name;
        }

        #endregion
    }
}