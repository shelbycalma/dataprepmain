﻿using System;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.SalesForcePlugin.Enums;

namespace Panopticon.SalesForcePlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigPanel
    {
		private bool IsSelectedAllUnchecked = true;

		public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof (SalesForceSettingsViewModel), typeof (ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        internal BitmapSource FolderImage { get; set; }

        public SalesForceSettingsViewModel Settings
        {
            get
            {
                return (SalesForceSettingsViewModel) GetValue(SettingsProperty);
            }
            set
            {
                var oldValue = (SalesForceSettingsViewModel) GetValue(SettingsProperty);
                if (oldValue != null)
                {
                    oldValue.SettingsLoaded -= ScrollToAppropriateItems;
                }

                value.SettingsLoaded += ScrollToAppropriateItems;
                SetValue(SettingsProperty, value);
            }
        }

        public ConfigPanel()
        {
            InitializeComponent();
			((INotifyCollectionChanged)ColumnsControl.Items).CollectionChanged +=
				ColumnsList_CollectionChanged;
		}

		private void ColumnsList_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			if (Settings != null)
			{
				if (Settings.Model.SelectedColumns.Count == 0)
				{
					ScrollViewer sv = (ScrollViewer)ColumnsControl.Template.
						FindName("ColumnsScrollBar", ColumnsControl);
					if (sv == null)
					{
						return;
					}
					sv.ScrollToTop();
				}
			}

			if (e.Action == NotifyCollectionChangedAction.Reset)
			{
				bool isEdit = false;
				foreach (DatabaseColumn d in ColumnsControl.ItemsSource)
				{
					if (d.Selected)
					{
						isEdit = true;
						break;
					}
				}
				if (!isEdit)
				{
					bool hasColumns = false;
					foreach (DatabaseColumn d in ColumnsControl.ItemsSource)
					{
						d.Selected = true;
						hasColumns = true;
					}
					if (hasColumns)
					{
						ControlTemplate template = ColumnsControl.Template;
						CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
						if (c != null) c.IsChecked = true;
					}
				}
				else
				{
					bool isCheckedAll = false;
					foreach (DatabaseColumn d in ColumnsControl.ItemsSource)
					{
						if (d.Selected)
						{
							isCheckedAll = true;
						}
						else
						{
							isCheckedAll = false;
							break;
						}
					}
					ControlTemplate template = ColumnsControl.Template;
					CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
					if (c != null) c.IsChecked = isCheckedAll;
				}
			}

			if (e.Action == NotifyCollectionChangedAction.Add)
			{
				ControlTemplate template = ColumnsControl.Template;
				CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
				IsSelectedAllUnchecked = false;
				if (c != null) c.IsChecked = false;
				IsSelectedAllUnchecked = true;
			}
		}

		private void ScrollToAppropriateItems(object sender, EventArgs e)
        {
            if (Settings.Model.ObjectType == ObjectType.Unknown)
            {
                return;
            }

            if (Settings.Model.ObjectType == ObjectType.Table)
            {
                TablesListBox.ScrollIntoView(TablesListBox.SelectedItem);
				return;
            }

            if (Settings.Model.ObjectType == ObjectType.Report)
            {
				ReportFoldersBox.ScrollIntoView(ReportFoldersBox.SelectedItem);
				ReportsBox.ScrollIntoView(ReportsBox.SelectedItem);
				return;
            }
        }

        private static void Settings_Changed(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (SalesForceSettingsViewModel) args.OldValue,
                (SalesForceSettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(
            SalesForceSettingsViewModel oldSettings,
            SalesForceSettingsViewModel newSettings)
        {
            DataContext = newSettings;
            this.PasswordBox.Password
                = newSettings != null ? newSettings.Model.Password : null;
            this.SecurityTokenBox.Password
                = newSettings != null ? newSettings.Model.SecurityToken : null;
        }

        private void PasswordBox_Changed(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Model.Password = PasswordBox.Password;
            }
        }

        private void SecurityTokenBox_Changed(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
            {
                Settings.Model.SecurityToken = SecurityTokenBox.Password;
            }
        }

		private void Select_Checked(object sender, System.Windows.RoutedEventArgs e)
		{
			bool isCheckedAll = false;
			foreach (DatabaseColumn c in ColumnsControl.ItemsSource)
			{
				if (c.Selected)
				{
					isCheckedAll = true;
				}
				else
				{
					isCheckedAll = false;
					break;
				}
			}
			if (isCheckedAll)
			{
				ControlTemplate template = ColumnsControl.Template;
				CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;
				if (c != null) c.IsChecked = true;
			}
		}

		private void Select_Unchecked(object sender, System.Windows.RoutedEventArgs e)
		{
			ControlTemplate template = ColumnsControl.Template;
			CheckBox c = template.FindName("SelectAll", ColumnsControl) as CheckBox;

			IsSelectedAllUnchecked = false;
			if (c != null) c.IsChecked = false;
			IsSelectedAllUnchecked = true;
		}

		private void SelectAll_Checked(object sender, System.Windows.RoutedEventArgs e)
		{
			foreach (DatabaseColumn c in ColumnsControl.ItemsSource)
			{
				c.Selected = true;
			}
		}

		private void SelectAll_Unchecked(object sender, System.Windows.RoutedEventArgs e)
		{
			if (IsSelectedAllUnchecked)
			{
				foreach (DatabaseColumn c in ColumnsControl.ItemsSource)
				{
					c.Selected = false;
				}
			}
		}

		private void TablesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ListBox selector = sender as ListBox;
			if (selector == null) return;
			selector.ScrollIntoView(selector.SelectedItem);
		}

		private void ReportsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ListBox selector = sender as ListBox;
			if (selector == null) return;
			selector.ScrollIntoView(selector.SelectedItem);
		}

		private void ReportFoldersBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			ListBox selector = sender as ListBox;
			if (selector == null) return;
			selector.ScrollIntoView(selector.SelectedItem);
		}

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}