﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.SalesForcePlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, SalesForceSettings>
    {
        private const string ImageUri =
            "pack://application:,,,/Panopticon.SalesForcePlugin.UI;component/ConnectImage.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(ImageUri, UriKind.Absolute));
            }
            catch (Exception)
            {   //Fix for DPS-1713 and DPS-1554
                //Trace.WriteLine(String.Format("Failed to load image: {0}", ImageUri));
            }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();
            SalesForceSettingsViewModel settingsViewModel =
                new SalesForceSettingsViewModel(
                    Plugin.CreateSettings(new PropertyBag()), parameters);
            ConfigWindow window = new ConfigWindow(settingsViewModel);

            window.Owner = owner;

            bool? dialogResult = window.ShowDialog();

            if (dialogResult != true)
            {
                return null;
            }

			settingsViewModel.Model.ColumnNames = GetSelectedColumns(settingsViewModel.Model);

			return settingsViewModel.Model.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            SalesForceSettingsViewModel settingsViewModel =
                new SalesForceSettingsViewModel(Plugin.CreateSettings(bag), parameters);
            ConfigPanel panel = new ConfigPanel { Settings = settingsViewModel };
            settingsViewModel.Login();
            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
			configPanel.Settings.Model.ColumnNames = GetSelectedColumns(configPanel.Settings.Model);
			return configPanel.Settings.Model.ToPropertyBag();
        }

		private string GetSelectedColumns(SalesForceSettings settings)
		{
			string columnNames = "";
			int i = 0;
			foreach (DatabaseColumn col in settings.SelectedColumns)
			{
				if (i > 0)
					columnNames += ",";
				columnNames += col.ColumnName;
				i++;
			}
			return columnNames;
		}

	}
}
