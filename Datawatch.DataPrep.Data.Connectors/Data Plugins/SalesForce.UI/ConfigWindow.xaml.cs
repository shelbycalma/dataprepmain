﻿using System.Windows.Input;

namespace Panopticon.SalesForcePlugin.UI
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ConfigWindow
    {
        public static RoutedCommand OkCommand = new RoutedCommand("Ok",
            typeof (ConfigWindow));

        public SalesForceSettingsViewModel Settings { get; private set; }

        public ConfigWindow(SalesForceSettingsViewModel settings)
        {
            Settings = settings;
            Settings.LoginServer = "login.salesforce.com";
            InitializeComponent();
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Settings != null && Settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}