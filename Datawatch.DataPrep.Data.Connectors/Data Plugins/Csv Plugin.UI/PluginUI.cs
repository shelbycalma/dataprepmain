﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;

namespace Panopticon.CsvPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, ConnectionSettings>
    {
        private const string Imageuri =
           "pack://application:,,,/Panopticon.CsvPlugin.UI;component" +
           "/connect-csv.gif";

        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(Imageuri,
                                                       UriKind.Absolute));
            }
            catch (Exception)
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ConfigPanel configPanel = new ConfigPanel(this);
            configPanel.Path = bag.Values[CsvPlugin.Plugin.PathKey];
            return configPanel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            PropertyBag bag = new PropertyBag();
            bag.Values[CsvPlugin.Plugin.PathKey] = configPanel.Path;
            return bag;
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            string file = BrowseForFile();
            if (file == null || !file.ToLower().EndsWith(".csv"))
            {
                return null;
            }

            PropertyBag settings = new PropertyBag();
            settings.Values[CsvPlugin.Plugin.PathKey] = file;
            return settings;
        }

        private string BrowseForFile()
        {
            return BrowseForFile(null);
        }

        internal string BrowseForFile(string oldFileName)
        {
            return DataPluginUtils.BrowseForFile(owner, oldFileName, 
                "CSV Files (*.csv)|*.csv", Plugin.GlobalSettings);
        }

        public override string GetTitle(PropertyBag settings)
        {
            string path = settings.Values[CsvPlugin.Plugin.PathKey];
            return path.Substring(
                path.LastIndexOf(Path.DirectorySeparatorChar) + 1);
        }
    }
}
