﻿using System.Windows;
using System.Windows.Controls;
using System.ComponentModel;

namespace Panopticon.CsvPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string path;
        private PluginUI pluginUI;

        public ConfigPanel(PluginUI pluginUI)
        {
            this.pluginUI = pluginUI;

            InitializeComponent();
            DataContext = this;
        }

        public string Path
        {
            get { return path; }
            set
            {
                path = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Path"));
                    PropertyChanged(this, new PropertyChangedEventArgs("Ranges"));
                }
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            string path = pluginUI.BrowseForFile(Path);
            if (path != null)
            {
                Path = path;
            }
        }
    }
}
