﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.TM1Plugin.UI
{
    public partial class ConfigWindow : Window
    {
        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(ConfigWindow));

        private readonly TM1SettingsViewModel settings;

        internal ConfigWindow(TM1SettingsViewModel settings)
        {
            this.settings = settings;
            InitializeComponent();
        }

        public TM1SettingsViewModel Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings != null && settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
