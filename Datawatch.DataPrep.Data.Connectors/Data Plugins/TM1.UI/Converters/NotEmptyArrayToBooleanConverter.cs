﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Panopticon.TM1Plugin.UI
{
    public class NotEmptyArrayToBooleanConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return false;
            string[] array = value as string[];
            if (array != null)
            {
                if (array.Length == 0)
                    return false;
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}