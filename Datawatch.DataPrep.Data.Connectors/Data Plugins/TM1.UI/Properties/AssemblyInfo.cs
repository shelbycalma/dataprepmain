﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.TM1Plugin.UI.dll")]
[assembly: AssemblyDescription("Datawatch TM1 Plug-in UI")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription("IBM Cognos TM1", "Database", "TM1Plugin", true)]
