﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Applix.TM1.API.Internal;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.TM1Plugin.Properties;

namespace Panopticon.TM1Plugin.UI
{
    public class TM1SettingsViewModel : ViewModelBase
    {
        private _TM1Main adminServer;
        private _TM1Server tm1Server;

        public TM1SettingsViewModel(TM1Settings model,
            IEnumerable<ParameterValue> parameters)
        {
            if (model == null)
            {
                throw Exceptions.ArgumentNull("model");
            }
            this.model = model;

            // add combobox selected items to collections
            // that is used as a source to make them visible
            AddItemsToCollecions();

            this.model.PropertyChanged += OnModelChanged;

            if (parameters != null)
            {
                foreach (ParameterValue parameter in parameters)
                {
                    ParametersCollection.Add(parameter);
                }
            }

            IsConnectEnabled = true;
            ListServersButtonText = Properties.Resources.UiListServers;
        }

        private void AddItemsToCollecions()
        {
            AvailableCubes = new[] {Model.Cube};
            AvailableDimensions = new[] {Model.Dimension};
            AvailableServers = new[] {Model.Server};
            AvailableSubsets = new[] {Model.Subset};
            AvailableViews = new[] {Model.View};
        }

        ~TM1SettingsViewModel()
        {
            CloseConnection();
        }

        public void CloseConnection()
        {
            if (tm1Server != null && adminServer != null)
            {
                adminServer.CloseConnection(tm1Server);
            }
        }

        private void OnModelChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Cube":
                    PopulateViews();
                    break;
                case "Dimension":
                    PopulateSubsets();
                    break;
                case "View":
                    PopulateDbTitles();
                    break;
                case "User":
                case "Server":
                case "Parameterize":
                    OnPropertyChanged();
                    break;
            }
        }

        public void LoadAllDataBySettings()
        {
            PopulateServers().ContinueWith(t =>
            {
                Login();
                OnPropertyChanged();
            });
        }

        #region props

        private readonly TM1Settings model;
        private string[] availableCubes;
        private string[] availableDimensions;
        private object[] availableRaggedHierarchyAttributes;
        private string[] availableServers;
        private string[] availableSubsets;
        private string[] availableViews;
        private ObservableCollection<DbTitle> dbTitles;
        private bool isConnectEnabled;
        private bool isLogged;
        private string listServersButtonText;
        private ObservableCollection<ParameterValue> parametersCollection;

        public TM1Settings Model
        {
            get { return model; }
        }

        public string[] AvailableServers
        {
            get
            {
                return availableServers
                       ?? (availableServers = new string[] {});
            }
            set
            {
                if (availableServers == value) return;
                availableServers = value;
                OnPropertyChanged("AvailableServers");
            }
        }

        public string[] AvailableCubes
        {
            get
            {
                return availableCubes
                       ?? (availableCubes = new string[] {});
            }
            set
            {
                if (availableCubes == value) return;
                availableCubes = value;
                OnPropertyChanged("AvailableCubes");
            }
        }

        public string[] AvailableDimensions
        {
            get
            {
                return availableDimensions
                       ?? (availableDimensions = new string[] {});
            }
            set
            {
                if (availableDimensions == value) return;
                availableDimensions = value;
                OnPropertyChanged("AvailableDimensions");
            }
        }

        public string[] AvailableSubsets
        {
            get
            {
                return availableSubsets
                       ?? (availableSubsets = new string[] {});
            }
            set
            {
                if (availableSubsets == value) return;
                availableSubsets = value;
                OnPropertyChanged("AvailableSubsets");
            }
        }

        public string[] AvailableViews
        {
            get
            {
                return availableViews
                       ?? (availableViews = new string[] {});
            }
            set
            {
                if (availableViews == value) return;
                availableViews = value;
                OnPropertyChanged("AvailableViews");
            }
        }

        public string ListServersButtonText
        {
            get { return listServersButtonText; }
            set
            {
                listServersButtonText = value;
                OnPropertyChanged("ListServersButtonText");
            }
        }

        public ObservableCollection<DbTitle> DbTitles
        {
            get
            {
                return dbTitles
                       ?? (dbTitles = new ObservableCollection<DbTitle>());
            }
            set
            {
                if (dbTitles == value) return;
                dbTitles = value;
                OnPropertyChanged("DbTitles");
            }
        }

        public ObservableCollection<ParameterValue> ParametersCollection
        {
            get
            {
                return parametersCollection ?? (parametersCollection
                    = new ObservableCollection<ParameterValue>
                    {
                        new ParameterValue
                        {
                            Name = ""
                        }
                    });
            }
            set
            {
                if (parametersCollection == value) return;
                parametersCollection = value;
                OnPropertyChanged("ParametersCollection");
            }
        }

        public bool IsOk
        {
            get
            {
                return !string.IsNullOrEmpty(model.Server) &&
                       !string.IsNullOrEmpty(model.User) &&
                       (
                           (model.ObjectType == ObjectType.Cubeview
                            && !string.IsNullOrEmpty(model.View)
                            && !string.IsNullOrEmpty(model.Cube))
                           || (model.ObjectType == ObjectType.SubsetElement
                               && !string.IsNullOrEmpty(model.Dimension)
                               && (!string.IsNullOrEmpty(model.Subset)
                                   || model.IncludeAllSubsets))
                           || (model.ObjectType == ObjectType.MDX
                               && !string.IsNullOrEmpty(model.MDX))
                           || (model.ObjectType == ObjectType.DimensionSubset
                               && !string.IsNullOrEmpty(model.DimensionSubsetLister))
                           );
            }
        }

        public bool IsLogged
        {
            get { return isLogged; }
            private set
            {
                isLogged = value;
                OnPropertyChanged("IsLogged");
            }
        }

        public bool CanLogin
        {
            get
            {
                return !String.IsNullOrEmpty(Model.User)
                       && !String.IsNullOrEmpty(Model.Server)
                       && isConnectEnabled;
            }
        }

        public bool IsConnectEnabled
        {
            get { return isConnectEnabled; }
            private set
            {
                isConnectEnabled = value;
                OnPropertyChanged("IsConnectEnabled");
            }
        }

        #endregion

        #region commands

        private ICommand listServersCommand;
        private ICommand loginCommand;
        private ICommand populateCubesDimensionsSubsetsCommand;

        public ICommand LoginCommand
        {
            get
            {
                return loginCommand ??
                       (loginCommand = new DelegateCommand(this.Login));
            }
        }

        public ICommand ListServersCommand
        {
            get
            {
                return listServersCommand ??
                       (listServersCommand
                           = new DelegateCommand(() => this.ListServers()));
            }
        }

        public ICommand PopulateCubesDimensionsSubsetsCommand
        {
            get
            {
                return populateCubesDimensionsSubsetsCommand ??
                       (populateCubesDimensionsSubsetsCommand
                           = new DelegateCommand(this.PopulateAll));
            }
        }

        #endregion

        #region private/internal methods

        private void Login()
        {
            IsConnectEnabled = false;
            try
            {
                if (DoLogin())
                    PopulateAll();
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show(ex.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                IsConnectEnabled = true;
            }
        }

        private Task ListServers()
        {
//            RestoreDefaults();
            return PopulateServers();
        }

        private void PopulateAll()
        {
            PopulateCubes();
            PopulateViews();
            PopulateDimensions();
            PopulateSubsets();
            PopulateDbTitles();
        }

        private bool DoLogin()
        {
            if (adminServer == null || string.IsNullOrEmpty(Model.Server)
                || string.IsNullOrEmpty(Model.User))
            {
                IsLogged = false;
                return false;
            }
            tm1Server = adminServer
                .openConnection(Model.Server, Model.User, Model.Password);
            IsLogged = tm1Server != null;
            return IsLogged;
        }

        private void RestoreDefaults()
        {
            AvailableServers = new string[] {};
            AvailableCubes = new string[] {};
            AvailableDimensions = new string[] {};
            AvailableSubsets = new string[] {};
            AvailableViews = new string[] {};
            IsLogged = false;
            Model.RestoreDefaults();
        }

        private Task PopulateServers()
        {
            IsConnectEnabled = false;
            ListServersButtonText = Properties.Resources.UiListServersInProgress;
            return TM1Helper
                .GetMainServerAsync(Model.AdminHost, Model.SSLCertificateId)
                .ContinueWith(t =>
                {
                    if (IsError(t))
                    {
                        return;
                    }
                    adminServer = t.Result;
                    string[] serverNames
                        = new string[adminServer.NumberOfServers];
                    for (int i = 0; i < adminServer.NumberOfServers; i++)
                    {
                        serverNames[i] = adminServer.GetServerName(i);
                    }
                    AvailableServers = serverNames;
                    IsConnectEnabled = true;
                    ListServersButtonText = Properties.Resources.UiListServers;
                });
        }

        private static bool IsError(Task task)
        {
            if (task.Exception != null)
            {
                AggregateException ex = task.Exception.Flatten();
                Log.Exception(ex);
                MessageBox.Show(ex.InnerException.Message,
                    null, MessageBoxButton.OK, MessageBoxImage.Error);
                return true;
            }
            return false;
        }

        private void PopulateViews()
        {
            if (tm1Server == null)
            {
                return;
            }
            try
            {
                _TM1Cube cube = tm1Server.Cubes[Model.Cube];
                List<string> views = new List<string>();
                for (int i = 0; i < cube.PublicViewsCount; i++)
                {
                    _TM1View view = cube.PublicViews[i];
                    if (!TM1Helper.IsControlObject(view.Name)
                        || Model.ShowControlObjects)
                    {
                        views.Add(view.Name);
                    }
                }
                AvailableViews = views.ToArray();
            }
            catch
            {
                AvailableViews = new string[] {};
            }
        }

        private void PopulateCubes()
        {
            if (tm1Server == null)
                return;
            List<string> cubes = new List<string>();
            for (int i = 0; i < tm1Server.CubesCount; i++)
            {
                _TM1Cube cube = tm1Server.Cubes[i];
                if (!TM1Helper.IsControlObject(cube.Name)
                    || Model.ShowControlObjects)
                {
                    cubes.Add(cube.Name);
                }
            }
            AvailableCubes = cubes.ToArray();
        }

        private void PopulateDimensions()
        {
            if (tm1Server == null)
                return;
            List<string> dimensions = new List<string>();
            for (int i = 0; i < tm1Server.DimensionsCount; i++)
            {
                _TM1Dimension dimension = tm1Server.Dimensions[i];
                if (!TM1Helper.IsControlObject(dimension.Name)
                    || Model.ShowControlObjects)
                {
                    dimensions.Add(dimension.Name);
                }
            }
            AvailableDimensions = dimensions.ToArray();
        }

        private void PopulateDbTitles()
        {
            if (tm1Server == null
                || string.IsNullOrEmpty(Model.Cube)
                || string.IsNullOrEmpty(Model.View))
                return;

            ObservableCollection<DbTitle> result
                = new ObservableCollection<DbTitle>();

            _TM1Cube cub = tm1Server.Cubes[Model.Cube];
            _TM1View view = cub.PublicViews[Model.View];

            for (int titleIndex = 0;
                titleIndex < view.TitleSubsetCount;
                titleIndex++)
            {
                string titleName
                    = view.getTitleSubset(titleIndex + 1).Parent.Name;
                DbTitle dbTitle = new DbTitle(model.ToPropertyBag());
                dbTitle.TitleName = titleName;
                result.Add(dbTitle);
            }

            DbTitles = result;
        }

        private void PopulateSubsets()
        {
            try
            {
                if (tm1Server == null
                    || string.IsNullOrEmpty(Model.Dimension))
                    throw new Exception();
                _TM1Dimension dimension
                    = tm1Server.Dimensions[Model.Dimension];
                List<string> subsets = new List<string>();
                for (int i = 0; i < dimension.PublicSubsetsCount; i++)
                {
                    _TM1Subset subset = dimension.PublicSubsets[i];
                    if (!TM1Helper.IsControlObject(subset.Name)
                        || Model.ShowControlObjects)
                    {
                        subsets.Add(subset.Name);
                    }
                }
                AvailableSubsets = subsets.ToArray();
            }
            catch
            {
                AvailableSubsets = new string[] {};
            }
        }

        #endregion
    }
}