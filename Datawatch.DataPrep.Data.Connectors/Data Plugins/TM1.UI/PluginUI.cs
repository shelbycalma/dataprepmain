﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.TM1Plugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, TM1Settings>
    {
        private const string ImageUri
            = "pack://application:,,,/"
              + "Panopticon.TM1Plugin.UI;component/ConnectImage.png";

        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
            try
            {
                image =
                    new BitmapImage(
                        new Uri(ImageUri, UriKind.Absolute));
            }
            catch
            {
            }
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            TM1Settings tm1Settings = Plugin.CreateSettings(bag);

            TM1SettingsViewModel tm1SettingsViewModel
                = new TM1SettingsViewModel(tm1Settings, parameters);
            ConfigPanel panel = new ConfigPanel();
            panel.Settings = tm1SettingsViewModel;
            tm1SettingsViewModel.LoadAllDataBySettings();
            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel configPanel = (ConfigPanel)obj;
            return configPanel.Settings.Model.ToPropertyBag();
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            TM1Settings tm1Settings = new TM1Settings(new PropertyBag());
            TM1SettingsViewModel settingsViewModel
                = new TM1SettingsViewModel(tm1Settings, parameters);

            ConfigWindow window = new ConfigWindow(settingsViewModel);
            window.Owner = owner;

            bool? dialogResult = window.ShowDialog();

            settingsViewModel.CloseConnection();

            if (dialogResult != true)
                return null;

            return settingsViewModel.Model.ToPropertyBag();
        }
    }
}
