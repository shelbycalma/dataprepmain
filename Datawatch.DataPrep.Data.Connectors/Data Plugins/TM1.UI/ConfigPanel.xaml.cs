﻿using System.Windows;

namespace Panopticon.TM1Plugin.UI
{
    public partial class ConfigPanel
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof (TM1SettingsViewModel), typeof (ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        public ConfigPanel()
        {
            InitializeComponent();
        }

        public TM1SettingsViewModel Settings
        {
            get { return (TM1SettingsViewModel) GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        private static void Settings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (TM1SettingsViewModel) args.OldValue,
                (TM1SettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(TM1SettingsViewModel oldSettings,
            TM1SettingsViewModel newSettings)
        {
            DataContext = newSettings;
            this.PasswordBox.Password
                = newSettings != null ? newSettings.Model.Password : null;
        }

        private void PasswordBox_Changed(object sender, RoutedEventArgs e)
        {
            if (Settings != null)
                Settings.Model.Password = PasswordBox.Password;
        }
    }
}