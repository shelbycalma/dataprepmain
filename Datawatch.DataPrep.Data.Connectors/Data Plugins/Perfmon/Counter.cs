﻿using Datawatch.DataPrep.Data.Framework;

namespace Panopticon.PerfmonPlugin
{
    public class Counter
    {
        public Counter(string displayName,
            string categoryName, string counterName,
            string instanceName, string machineName)
        {
            if (displayName == null)
            {
                throw Exceptions.ArgumentNull("displayName");
            }
            if (categoryName == null)
            {
                throw Exceptions.ArgumentNull("categoryName");
            }
            if (counterName == null)
            {
                throw Exceptions.ArgumentNull("counterName");
            }
            if (machineName == null)
            {
                throw Exceptions.ArgumentNull("machineName");
            }

            CategoryName = categoryName;
            CounterName = counterName;
            InstanceName = instanceName;
            MachineName = machineName;
            DisplayName = displayName;
        }

        public string CategoryName { get; private set; }
        public string CounterName { get; private set; }
        public string InstanceName { get; private set; }
        public string MachineName { get; private set; }
        public string DisplayName { get; private set; }
    }
}
