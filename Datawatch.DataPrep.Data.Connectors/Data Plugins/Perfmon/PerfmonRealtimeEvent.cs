﻿using System;

namespace Panopticon.PerfmonPlugin
{
    public class PerfmonRealtimeEvent
    {
        public string CategoryName { get; set; }
        public string CounterName { get; set; }
        public string InstanceName { get; set; }
        public string MachineName { get; set; }
        public DateTime EventTime { get; set; }
        public float Value { get; set; }
        public PerfmonRealtimeEventType EventType { get; set; }
    }

    public enum PerfmonRealtimeEventType
    {
        Insert,
        Update,
        Delete
    }
}
