﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.PerfmonPlugin.Properties;

namespace Panopticon.PerfmonPlugin
{
    public class PerfmonRealtimeAdapter :
        RealtimeDataAdapter<ParameterTable, PerfmonSettings, PerfmonRealtimeEvent>
    {
        private Thread thread;
        private readonly IDictionary<string, PerformanceCounter> counters
            = new Dictionary<string, PerformanceCounter>();

        public static readonly string DisplayNameColumnName = Resources.UiDisplayName;
        private static readonly string ValueColumnName = Resources.UiCounterValue;
        private static readonly string CategoryColumnName = Resources.UiCategory;
        private static readonly string CounterColumnName = Resources.UiCounter;
        private static readonly string InstanceColumnName = Resources.UiInstance;
        private static readonly string MachineNameColumnName = Resources.UiComputer;
        public static readonly string EventTimeColumnName = Resources.UiEventTime;

        private NumericColumn valueColumn;
        private TextColumn categoryColumn;
        private TextColumn counterColumn;
        private TextColumn instanceColumn;
        private TextColumn machineNameColumn;
        private TimeColumn eventTimeColumn;

        public override void Start()
        {
            thread = new Thread(ReadCounters);
            thread.Start();
        }

        private void ReadCounters()
        {
            ISet<string> countersSet = new HashSet<string>();

            valueColumn = (NumericColumn)table.GetColumn(ValueColumnName);
            categoryColumn = (TextColumn)table.GetColumn(CategoryColumnName);
            counterColumn = (TextColumn)table.GetColumn(CounterColumnName);
            instanceColumn = (TextColumn)table.GetColumn(InstanceColumnName);
            machineNameColumn = (TextColumn)table.GetColumn(MachineNameColumnName);
            eventTimeColumn = (TimeColumn)table.GetColumn(EventTimeColumnName);

            while (thread != null)
            {
                foreach (Counter counter in settings.Counters)
                {
                    string name = counter.DisplayName;

                    if (!counters.ContainsKey(name))
                    {
                        PerformanceCounter pc = CounterHelper.CreateCounter(
                            counter.CategoryName,
                            counter.CounterName,
                            counter.InstanceName,
                            DataUtils.ApplyParameters(
                                counter.MachineName, table.Parameters));

                        if (pc != null)
                        {
                            counters.Add(name, pc);
                        }
                        else
                        {
                            continue;
                        }
                    }

                    float? value = CounterHelper.GetCounterValue(counters[name]);

                    PerfmonRealtimeEvent perfmonRealtimeEvent = 
                        new PerfmonRealtimeEvent();
                    perfmonRealtimeEvent.EventTime = DateTime.Now;

                    if (value.HasValue)
                    {
                        perfmonRealtimeEvent.Value = value.Value;
                        perfmonRealtimeEvent.CategoryName = counter.CategoryName;
                        perfmonRealtimeEvent.CounterName = counter.CounterName;
                        perfmonRealtimeEvent.InstanceName = counter.InstanceName;
                        perfmonRealtimeEvent.MachineName = counter.MachineName;

                        if (!countersSet.Contains(name))
                        {
                            countersSet.Add(name);

                            perfmonRealtimeEvent.EventType =
                                PerfmonRealtimeEventType.Insert;
                            updater.EnqueueInsert(name, perfmonRealtimeEvent);
                        }
                        else
                        {
                            perfmonRealtimeEvent.EventType =
                                PerfmonRealtimeEventType.Update;
                            updater.EnqueueUpdate(name, perfmonRealtimeEvent);
                        }
                    }
                    else if (countersSet.Contains(name))
                    {
                        perfmonRealtimeEvent.EventType =
                            PerfmonRealtimeEventType.Delete;
                        countersSet.Remove(name);
                        updater.EnqueueDelete(name, perfmonRealtimeEvent);
                    }
                }

                updater.Flush();
                Thread.Sleep(settings.Limit);
            }
        }

        public override void Stop()
        {
            thread = null;
        }

        public override void UpdateColumns(Row row, PerfmonRealtimeEvent evt)
        {
            if (valueColumn.GetNumericValue(row) != evt.Value)
            {
                valueColumn.SetNumericValue(row, evt.Value);
            }

            eventTimeColumn.SetTimeValue(row, evt.EventTime);

            if (categoryColumn.GetTextValue(row) != evt.CategoryName)
            {
                categoryColumn.SetTextValue(row, evt.CategoryName);
            }

            if (counterColumn.GetTextValue(row) != evt.CounterName)
            {
                counterColumn.SetTextValue(row, evt.CounterName);
            }

            if (instanceColumn.GetTextValue(row) != evt.InstanceName)
            {
                instanceColumn.SetTextValue(row, evt.InstanceName);
            }

            if (machineNameColumn.GetTextValue(row) != evt.MachineName)
            {
                machineNameColumn.SetTextValue(row, evt.MachineName);
            }
        }

        public override DateTime GetTimestamp(PerfmonRealtimeEvent evt)
        {
            if (settings.IsTimeIdColumnGenerated ||
                string.IsNullOrEmpty(settings.TimeIdColumn))
            {
                return DateTime.MinValue;
            }

            return evt.EventTime;
        }

        public static ITable CreateTable(
            PerfmonSettings settings, IEnumerable<ParameterValue> parameters,
            string dataDirectory)
        {
            ParameterTable table = new ParameterTable(parameters);
            table.Parameters = parameters;

            table.BeginUpdate();
            try
            {
                table.AddColumn(new TextColumn(settings.IdColumn));
                table.AddColumn(new NumericColumn(ValueColumnName));
                table.AddColumn(new TextColumn(CategoryColumnName));
                table.AddColumn(new TextColumn(CounterColumnName));
                table.AddColumn(new TextColumn(InstanceColumnName));
                table.AddColumn(new TextColumn(MachineNameColumnName));
                table.AddColumn(new TimeColumn(EventTimeColumnName));
            }
            finally
            {
                table.EndUpdate();
            }

            return table;
        }
    }
}
