﻿using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.PerfmonPlugin
{
    public class PerfmonSettings : RealtimeConnectionSettings
    {
        private const string CountersCountKey = "CountersCount";
        private const string CategoryKey = "Category";
        private const string CounterKey = "Counter";
        private const string InstanceKey = "Instance";
        private const string MachineKey = "Machine";
        private const string DisplayNameKey = "DisplayName";
        private const string CountersKey = "Counters_{0}";

        public PerfmonSettings()
            : this(new PropertyBag())
        {}

        public PerfmonSettings(PropertyBag bag)
            : base(bag, false)
        {
            TimeIdColumnCandidates = new[] { PerfmonRealtimeAdapter.EventTimeColumnName };
            ManualFlush = true;
        }

        public string MachineName
        {
            get { return GetInternal("MachineName"); }
            set { SetInternal("MachineName", value);}
        }

        public override string IdColumn
        {
            get { return PerfmonRealtimeAdapter.DisplayNameColumnName; }
            set { }
        }

        public Counter[] Counters
        {
            get
            {
                int itemsCount = GetInternalInt(CountersCountKey, 0);
                Counter[] resultItems
                    = new Counter[itemsCount];

                for (int i = 0; i < itemsCount; i++)
                {
                    PropertyBag counterBag =
                        GetInternalSubGroup(string.Format(CountersKey, i));

                    resultItems[i] =
                        new Counter(
                            counterBag.Values[DisplayNameKey],
                            counterBag.Values[CategoryKey],
                            counterBag.Values[CounterKey],
                            counterBag.Values[InstanceKey],
                            counterBag.Values[MachineKey]);
                }

                return resultItems;
            }
            set
            {
                if (value == null)
                {
                    SetInternalInt(CountersCountKey, 0);
                    return;
                }

                SetInternalInt(CountersCountKey, value.Length);

                for (int i = 0; i < value.Length; i++)
                {
                    PropertyBag counterBag = new PropertyBag();
                    counterBag.Values[CategoryKey] = value[i].CategoryName;
                    counterBag.Values[CounterKey] = value[i].CounterName;
                    counterBag.Values[InstanceKey] = value[i].InstanceName;
                    counterBag.Values[MachineKey] = value[i].MachineName;
                    counterBag.Values[DisplayNameKey] = value[i].DisplayName;

                    SetInternalSubGroup(string.Format(CountersKey, i), counterBag);
                }
            }
        }
    }
}
