﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.PerfmonPlugin.Properties;

namespace Panopticon.PerfmonPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : RealtimeDataPlugin
        <ParameterTable, PerfmonSettings, PerfmonRealtimeEvent, PerfmonRealtimeAdapter>
    {
        internal const string PluginId = "PerfmonPlugin";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "Perfmon";
        internal const string PluginType = DataPluginTypes.Streaming;

        public override PerfmonSettings CreateSettings(PropertyBag bag)
        {
            PerfmonSettings perfmonSettings = new PerfmonSettings(bag);
            if (string.IsNullOrEmpty(perfmonSettings.Title))
            {
                perfmonSettings.Title = Resources.UiDefaultConnectionTitle;
            }

            if (perfmonSettings.Limit == 0)
            {
                perfmonSettings.Limit = 1000;
            }

            return perfmonSettings;
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        protected override ITable CreateTable(
            PerfmonSettings perfmonSettings, IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            return PerfmonRealtimeAdapter.CreateTable(perfmonSettings, parameters, dataDirectory);
        }
    }
}
