﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.PerfmonPlugin.Properties;

namespace Panopticon.PerfmonPlugin
{
    public static class CounterHelper
    {
        public static string GetCategoryDescription(PerformanceCounterCategory category)
        {
            try
            {
                return category.CategoryHelp;
            }
            catch(Exception e)
            {
                Log.Error(Resources.ExFailedToGetCategoryDescription,
                    category.CategoryName, category.MachineName, e.Message);
            }

            return null;
        }

        public static string GetCounterDescription(PerformanceCounter counter)
        {
            try
            {
                return counter.CounterHelp;
            }
            catch (Exception e)
            {
                Log.Error(Resources.ExFailedToGetCounterDescription,
                    counter.CounterName, counter.CategoryName, counter.MachineName, e.Message);
            }

            return null;
        }

        public static IList<string> GetCategoryInstances(PerformanceCounterCategory category)
        {
            try
            {
                return category.GetInstanceNames();
            }
            catch (Exception e)
            {
                Log.Error(Resources.ExFailedToGetInstancesForCategory,
                    category.CategoryName, category.MachineName, e.Message);
            }

            return null;
        }

        public static IList<PerformanceCounter> GetMultiInstanceCounters(
            PerformanceCounterCategory category, IEnumerable<string> instances)
        {
            foreach (string instance in instances)
            {
                try
                {
                    return category.GetCounters(instance);
                }
                catch(Exception e)
                {
                    Log.Error(Resources.ExFailedToGetCountersForCategoryInstance,
                        category.CategoryName, instance, category.MachineName, e.Message);
                }
            }

            return null;
        }

        public static IList<PerformanceCounter> GetSigleInstanceCounters(PerformanceCounterCategory category)
        {
            try
            {
                return category.GetCounters();
            }
            catch (Exception e)
            {
                Log.Error(Resources.ExFailedToGetCountersForCategoryInstance,
                    category.CategoryName, category.MachineName, e.Message);
            }

            return null;
        }

        public static PerformanceCounter CreateCounter(string categoryName,
            string counterName, string instanceName, string machineName)
        {
            try
            {
                PerformanceCounter pc = new PerformanceCounter(
                    categoryName,
                    counterName,
                    instanceName,
                    machineName);
                pc.NextValue();
                return pc;
            }
            catch (Exception e)
            {
                Log.Error(Resources.ExFailedToCreatePerformanceCounter,
                    counterName, categoryName, instanceName, machineName, e.Message);
            }

            return null;
        }

        public static float? GetCounterValue(PerformanceCounter counter)
        {
            if (counter == null)
            {
                return null;
            }

            try
            {
                return counter.NextValue();
            }
            catch (Exception e)
            {
                Log.Error(Resources.ExFailedToReadPerformanceCounterValue,
                    counter.CounterName, counter.CategoryName, counter.InstanceName,
                    counter.MachineName, e.Message);
            }

            return null;
        }

        public static PerformanceCounterCategoryType? GetCategoryType(
            PerformanceCounterCategory category)
        {
            try
            {
                return category.CategoryType;
            }
            catch (Exception e)
            {
                Log.Error(Resources.ExFailedToGetCategoryType,
                    category.CategoryName, category.MachineName, e.Message);
            }

            return null;
        }
    }
}
