﻿using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    public class FolderTreeViewModel : MDXMetaDataTreeViewModel
    {
        public FolderTreeViewModel(MDXMetaDataTreeViewModel parent, 
            string title, PropertyBag selectedItems) 
            : base(parent, title, selectedItems)
        {
        }

        public override string Image 
        {
            get
            {
                return IsExpanded ?
                    "/Panopticon.AnalysisServicesPlugin.UI;component/Images/Folder_open.png" :
                    "/Panopticon.AnalysisServicesPlugin.UI;component/Images/Folder_closed.png";
            }
            set
            {
                
            } 
        }

        protected override string UniqueName
        {
            get { return null; }
        }

        public override bool IsExpanded
        {
            get { return base.IsExpanded; }
            set
            {
                base.IsExpanded = value;
                OnPropertyChanged("Image");
            }
        }
    }
}
