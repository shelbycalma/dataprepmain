﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    partial class MDXConnectionWindow : Window
    {
        public static RoutedCommand OkCommand =
            new RoutedCommand("Ok", typeof(MDXConnectionWindow));

        private readonly MDXConnectionSettingsViewModel settings;

        public MDXConnectionWindow(MDXConnectionSettingsViewModel settings)
        {
            this.settings = settings;

            InitializeComponent();
        }

        public MDXConnectionSettingsViewModel Settings
        {
            get { return settings; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = settings != null && settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }
    }

   
}
