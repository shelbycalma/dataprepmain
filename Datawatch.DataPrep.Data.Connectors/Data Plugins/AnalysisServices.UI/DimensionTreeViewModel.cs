﻿using Datawatch.DataPrep.Data.Framework.Model;
using Dimension = Microsoft.AnalysisServices.AdomdClient.Dimension;
using Hierarchy = Microsoft.AnalysisServices.AdomdClient.Hierarchy;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    public class DimensionTreeViewModel : MDXMetaDataTreeViewModel
    {
        private readonly Hierarchy hierarchy;

        public DimensionTreeViewModel(MDXMetaDataTreeViewModel parent, 
            Dimension dimension, PropertyBag selectedItems) 
            : this(parent, dimension.Caption, selectedItems)
        {
            foreach (Hierarchy hierarchy in dimension.AttributeHierarchies)
            {
                MDXMetaDataTreeViewModel node = 
                    string.IsNullOrEmpty(hierarchy.DisplayFolder)
                    ? this
                    : GetFolder(hierarchy.DisplayFolder);
                node.Children.Add(new DimensionTreeViewModel(this, hierarchy, 
                    selectedItems));
            }
        }

        private DimensionTreeViewModel(MDXMetaDataTreeViewModel parent, 
            Hierarchy hierarchy, PropertyBag selectedItems) 
            : this(parent, hierarchy.Caption, selectedItems)
        {
            this.hierarchy = hierarchy;
        }

        private DimensionTreeViewModel(MDXMetaDataTreeViewModel parent,
            string title, PropertyBag selectedItems) 
            : base(parent, title, selectedItems)
        {
            Image = "/Panopticon.AnalysisServicesPlugin.UI;component/Images/Dimension.png";
        }

        protected override string UniqueName
        {
            get
            {
                if (hierarchy == null)
                {
                    return null;
                }
                return hierarchy.UniqueName;
            }
        }
    }
}
