﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, MDXConnectionSettings>
    {
        private const string ImageUri = "pack://application:,,,/" +
            "Panopticon.AnalysisServicesPlugin.UI;component/mdx-plugin.gif";

        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
            try
            {
                image = new BitmapImage(new Uri(ImageUri, UriKind.Absolute));
            }
            catch (Exception) { }
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            MDXConnectionSettings connection = new MDXConnectionSettings();
            MDXConnectionSettingsViewModel connectionViewModel =
                new MDXConnectionSettingsViewModel(connection, parameters);
            MDXConnectionWindow connectionWindow =
                new MDXConnectionWindow(connectionViewModel);
            connectionWindow.Owner = owner;

            bool? result = connectionWindow.ShowDialog();
            if (!result.HasValue || !result.Value)
            {
                return null;
            }

            return connection.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag connectionBag, IEnumerable<ParameterValue> parameters)
        {
            MDXConnectionSettings connection =
                new MDXConnectionSettings(connectionBag);
            MDXConnectionSettingsViewModel connectionViewModel =
                new MDXConnectionSettingsViewModel(connection, parameters);

            MDXConnectionPanel connectionPanel = new MDXConnectionPanel();
            connectionPanel.Settings = connectionViewModel;

            return connectionPanel;
        }

        public override PropertyBag GetSetting(object element)
        {
            MDXConnectionPanel connectionPanel = (MDXConnectionPanel)element;

            MDXConnectionSettingsViewModel connectionViewModel =
                connectionPanel.Settings;
            MDXConnectionSettings connection = connectionViewModel.Model;

            return connection.ToPropertyBag();
        }
    }
}
