﻿using System.Windows;
using System.Windows.Controls;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    internal partial class MDXConnectionPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(MDXConnectionSettingsViewModel),
                typeof(MDXConnectionPanel),
                new PropertyMetadata(Settings_Changed));

        public MDXConnectionPanel()
        {
            InitializeComponent();
        }

        public MDXConnectionSettingsViewModel Settings
        {
            get {
                return (MDXConnectionSettingsViewModel)
                    GetValue(SettingsProperty);
            }
            set {
                SetValue(SettingsProperty, value);
            }
        }

        private static void Settings_Changed(
            DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((MDXConnectionPanel) obj).OnSettingsChanged(
                (MDXConnectionSettingsViewModel) e.OldValue,
                (MDXConnectionSettingsViewModel) e.NewValue);
        }

        protected virtual void OnSettingsChanged(
            MDXConnectionSettingsViewModel oldSettings,
            MDXConnectionSettingsViewModel newSettings)
        {
            this.DataContext = newSettings;
            Password.Password = newSettings.Password;
        }

        private void PasswordBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            PasswordBox senderPasswordBox = sender as PasswordBox;
            if (senderPasswordBox != null)
            {
                Settings.Password = senderPasswordBox.Password;
            }
        }
    }
}
