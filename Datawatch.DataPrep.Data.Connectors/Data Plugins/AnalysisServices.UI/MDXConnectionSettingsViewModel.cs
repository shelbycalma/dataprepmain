﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Datawatch.DataPrep.Data.Framework;
using Microsoft.AnalysisServices.AdomdClient;
using Panopticon.AnalysisServicesPlugin.MSOLAP;
using Panopticon.AnalysisServicesPlugin.MSOLAP;
using Panopticon.AnalysisServicesPlugin.UI.Properties;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    public class MDXConnectionSettingsViewModel : ViewModelBase
    {
        private readonly MDXConnectionSettings model;
        private readonly ObservableCollection<ParameterValue> parameters;
        private ObservableCollection<string> _catalogNames;
        private ObservableCollection<string> _cubes;
        private MDXMetaDataTreeViewModel metaDataTree;
        private bool isConnecting;
        private DelegateCommand connectCommand;
        private string _connectionStatus;

        public MDXConnectionSettingsViewModel(
            MDXConnectionSettings model,
            IEnumerable<ParameterValue> parameters) 
        {
            if (model == null) {
                throw Exceptions.ArgumentNull("model");
            }

            this.model = model;
            this.parameters = new ObservableCollection<ParameterValue>();
            if (parameters != null) {
                foreach (ParameterValue parameter in parameters) {
                    this.parameters.Add(parameter);
                }
            }

            connectCommand = new DelegateCommand(async () => await UpdateCatalogsBackground().ConfigureAwait(false));

            if (CatalogName != null)
            {
                UpdateCatalogsBackground(false);
            }
        }

        public ObservableCollection<string> CatalogNames
        {
            get { return _catalogNames; }
            set
            {
                _catalogNames = value;
                if (_catalogNames!= null && _catalogNames.Count > 0 &&
                    string.IsNullOrEmpty(CatalogName))
                {
                    CatalogName = _catalogNames[0];
                }
                else
                {
                    UpdateCubesBackground();
                }
                OnPropertyChanged("CatalogNames");
            }
        }

        public string CatalogName
        {
            get { return model.CatalogName; }
            set
            {
                if (model.CatalogName == value) return;
                model.CatalogName = value;
                if (model.CatalogName != null)
                {
                    UpdateCubesBackground();
                }
                OnPropertyChanged("CatalogName");
            }
        }

        public AuthenticationType Authentication
        {
            get { return model.Authentication; }
            set
            {
                if (model.Authentication != value)
                {
                    model.Authentication = value;
                    OnPropertyChanged("Authentication");
                    OnPropertyChanged("IsUsernamePasswordVisible");
                }
            }
        }

        public bool IsUsernamePasswordVisible
        {
            get { return Authentication == AuthenticationType.UsernamePassword;}
        }

        public IList AuthenticationModes
        {
            get { return Enum.GetValues(typeof (AuthenticationType)); }
        }

        public string UserName
        {
            get { return model.UserName; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                model.UserName = value;
                OnPropertyChanged("UserName");
            }
        }

        public string Password
        {
            get { return model.Password; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                model.Password = value; 
                OnPropertyChanged("Password");
            }
        }

        public bool IsAggregated
        {
            get { return model.IsAggregated; }
            set {
                if (value != model.IsAggregated) {
                    model.IsAggregated = value;
                    OnPropertyChanged("IsAggregated");
                }
            }
        }

        public bool IsOk
        {
            get
            {
                return !string.IsNullOrWhiteSpace(model.CatalogName) &&
                       !string.IsNullOrWhiteSpace(model.CubeName) &&
                       !model.SelectedDimensionsAndMeasures.IsEmpty();
            }
        }

        public bool IsOnDemand
        {
            get { return model.IsOnDemand; }
            set {
                if (value != model.IsOnDemand) {
                    model.IsOnDemand = value;
                    OnPropertyChanged("IsOnDemand");
                }
            }
        }

        public bool IsUseSsl
        {
            get { return model.UseSsl; }
            set
            {
                if (value != model.UseSsl)
                {
                    model.UseSsl = value;
                    OnPropertyChanged("IsUseSsl");
                }
            }
        }

        public MDXConnectionSettings Model
        {
            get { return model; }
        }

        public ObservableCollection<ParameterValue> Parameters
        {
            get { return parameters; }
        }

        public ObservableCollection<string> CubeNames
        {
            get { return _cubes; }
            set 
            {
                _cubes = value;
                if (_cubes != null && _cubes.Count > 0 && 
                    (string.IsNullOrEmpty(CubeName) 
                    || !_cubes.Contains(CubeName)))
                {
                    CubeName = _cubes[0];
                }
                else
                {
                    UpdateDimensionsAndMeasures();
                }
                OnPropertyChanged("CubeNames");
            }
        }

        public string CubeName
        {
            get { return model.CubeName; }
            set
            {
                if (model.CubeName != value)
                {
                    model.CubeName = value;
                    ResetDimentionsAndMeasures();
                    UpdateDimensionsAndMeasures();
                    OnPropertyChanged("CubeName");
                }
            }
        }

        public string ServerName
        {
            get { return model.ServerName; }
            set
            {
                if (value != null)
                {
                    value = value.Trim();
                }
                if (model.ServerName != value)
                {
                    model.ServerName = value;
                }
            }
        }

        public MDXMetaDataTreeViewModel MetaDataTree
        {
            get { return metaDataTree; }
            set
            {
                metaDataTree = value;
                OnPropertyChanged("MetaDataTree");
            }
        }

        public Task UpdateCatalogsBackground(bool reset = true)
        {
            if (reset)
            {
                Reset();
            }
            return Task.Run(new Action(DoUpdateCatalogs));
        }

        private void DoUpdateCatalogs()
        {
            try
            {
                IsConnecting = true;
                ConnectionStatus = Resources.LogUpdatingCatalogs;
                Log.Info(Resources.LogUpdatingCatalogs);
                MSOLAPProvider provider = new MSOLAPProvider(this.Parameters);
                Log.Info(Resources.LogUpdatingCatalogs);
                CatalogNames = new ObservableCollection<string>(provider.GetCatalogs(model));
                ConnectionStatus = Resources.UiStatusReady;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                ConnectionStatus = Resources.UiStatusFailed;
                MessageBox.Show(string.Format(Resources.ExUpdatingCatalogs,
                    e.Message), Resources.ExError);
            }
            finally
            {
                IsConnecting = false;
            }
        }

        public bool IsConnecting
        {
            get { return isConnecting; }
            set { isConnecting = value; 
                OnPropertyChanged("IsConnecting"); }
        }

        public async Task UpdateCubesBackground()
        {
            IsConnecting = true;
            try
            {
                ConnectionStatus = Resources.LogFetchingCubes;
                var cubes = await DoUpdateCubes().ConfigureAwait(false);
                CubeNames = new ObservableCollection<string>(cubes);
                ConnectionStatus = Resources.UiStatusReady;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                ConnectionStatus = Resources.UiStatusFailed;
                MessageBox.Show(string.Format(Resources.ExFailedToUpdateCubes,
                    e.Message), Resources.ExError);
            }
            finally
            {

                IsConnecting = false;
            }
        }

        private async Task<IList<string>> DoUpdateCubes()
        {
            MSOLAPProvider provider = new MSOLAPProvider(Parameters);
            List<string> cubes = new List<string>();
            await Task.Run(
                () =>
                {
                    using (AdomdConnection connection =
                       (AdomdConnection)provider.Connect(model))
                    {

                        Log.Info(Resources.LogFetchingCubes);
                        foreach (CubeDef cube in connection.Cubes)
                        {
                            if (cube.Type == CubeType.Cube)
                            {
                                cubes.Add(cube.Name);
                            }
                        }
                        
                    }
                }).ConfigureAwait(false);
            return cubes;
        }

        private async Task UpdateDimensionsAndMeasures()
        {
            MDXMetaDataTreeViewModel meta = null;
            IsConnecting = true;
            if (string.IsNullOrEmpty(CubeName))
            {
                return;
            }
            try
            {
                meta = await Task.Run(() => DoUpdateDimensionsAndMeasures()).ConfigureAwait(false);
                ConnectionStatus = Resources
                        .UiStatusReady;
            }
            catch (Exception e)
            {
                Log.Exception(e);
                MessageBox.Show(
                    string.Format(
                    Resources.ExFailedToUpdateDimentions, e.Message)
                    , Resources.ExError);
                ConnectionStatus = 
                    Resources.UiStatusFailed;
            }
            finally
            {
                IsConnecting = false;
                MetaDataTree = meta;
            }
        }

        private MDXMetaDataTreeViewModel DoUpdateDimensionsAndMeasures()
        {
            MDXMetaDataTreeViewModel meta = null;

            MSOLAPProvider provider = new MSOLAPProvider(Parameters);
            using (AdomdConnection connection =
                (AdomdConnection)provider.Connect(model))
            {
                Log.Info(Resources
                    .LogUpdatingDimensionsAndMeasures);
                ConnectionStatus =Resources
                    .LogUpdatingDimensionsAndMeasures;
                CubeDef cube = null;
                foreach (CubeDef c in connection.Cubes)
                {
                    if (c.Name != CubeName) continue;
                    cube = c;
                    break;
                }
                if (cube == null)
                {
                    return null;
                }

                meta = new MDXMetaDataTreeViewModel(cube,
                    model.SelectedDimensionsAndMeasures);
                return meta;
            }
            return null;
        }

        public DelegateCommand ConnectCommand
        {
            get { return connectCommand; }
        }

        public string ConnectionStatus
        {
            get
            {
                return _connectionStatus;
            }

            set
            {
                _connectionStatus = value;
                OnPropertyChanged("ConnectionStatus");
            }
        }

        private void Reset()
        {
            if (CatalogNames != null)
            {
                CatalogNames.Clear();
            }
            if (CubeNames != null)
            {
                CubeNames.Clear();
            }
            ResetDimentionsAndMeasures();
            CatalogName = null;
            CubeName = null;
        }

        private void ResetDimentionsAndMeasures()
        {
            MetaDataTree = null;
            Model.SelectedDimensionsAndMeasures = null;
        }
    }
}
