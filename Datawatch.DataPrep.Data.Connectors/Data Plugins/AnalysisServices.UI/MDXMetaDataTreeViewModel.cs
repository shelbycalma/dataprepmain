﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.ViewModel;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Microsoft.AnalysisServices.AdomdClient;

namespace Panopticon.AnalysisServicesPlugin.UI
{
    public class MDXMetaDataTreeViewModel : TreeViewNodeViewModel
    {
        protected readonly string title;
        private string image;
        private bool? isSelected;
        private readonly Dictionary<string, MDXMetaDataTreeViewModel> folders =
            new Dictionary<string, MDXMetaDataTreeViewModel>();

        private readonly PropertyBag selectedItems;

        protected MDXMetaDataTreeViewModel(
            MDXMetaDataTreeViewModel parent,
            string title,
            PropertyBag selectedItems)
            : base(parent)
        {
            if (selectedItems == null) throw Exceptions.ArgumentNull("selectedItems");
            this.title = title;
            this.selectedItems = selectedItems;
        }

        public MDXMetaDataTreeViewModel(CubeDef cube, PropertyBag selectedItems) 
            : this(null, null, selectedItems)
        {
            Children.Add(new MeasureTreeViewModel(this, cube, selectedItems));

            foreach (Dimension dimension in cube.Dimensions)
            {
                if (dimension.DimensionType == DimensionTypeEnum.Measure) continue;

                Children.Add(new DimensionTreeViewModel(this, dimension, 
                    selectedItems));
            }
            VerifySelectedValues();
        }

        protected MDXMetaDataTreeViewModel GetFolder(string folderName)
        {
            if (folders.ContainsKey(folderName))
            {
                return folders[folderName];
            }
            MDXMetaDataTreeViewModel folder = 
                new FolderTreeViewModel(this, folderName, selectedItems);
            Children.Add(folder);
            folders.Add(folderName, folder);
            return folder;
        }

        public bool IsSelectedDummy
        {
            get { return false; }
            set
            {
                //TODO: Nothing
            }
        }

        public string Title
        {
            get { return title; }
        }

        public virtual string Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged("Image");
            }
        }

        protected virtual string UniqueName
        {
            get { return null; }
        }

        public override bool? IsSelected
        {
            get { return isSelected; }
            set
            {
                SetIsSelected(value, true, true);
                OnPropertyChanged("IsSelected");
            }
        }

        public void SetIsSelected(bool? value, bool updateChildren,
            bool updateParent)
        {
            if (value == isSelected)
            {
                return;
            }

            isSelected = value;

            if (updateChildren && isSelected.HasValue)
            {
                foreach (TreeViewNodeViewModel treeViewNodeViewModel in Children)
                {
                    MDXMetaDataTreeViewModel child 
                        = treeViewNodeViewModel as MDXMetaDataTreeViewModel;
                    if (child != null)
                    {
                        child.SetIsSelected(isSelected, true, false);
                    }
                    
                }
            }

            MDXMetaDataTreeViewModel parentMetaDataTreeViewModel 
                = Parent as MDXMetaDataTreeViewModel;
            if (updateParent && parentMetaDataTreeViewModel != null)
            {
                parentMetaDataTreeViewModel.VerifyCheckState();
                foreach (KeyValuePair<string, MDXMetaDataTreeViewModel> folder 
                    in parentMetaDataTreeViewModel.folders)
                {
                    folder.Value.VerifyCheckState();
                }
            }

            UpdateSelectedValues(value);
            OnPropertyChanged("IsSelected");
        }

        public void VerifyCheckState()
        {
            bool? state = null;
            for (int i = 0; i < this.Children.Count; ++i)
            {
                bool? current = this.Children[i].IsSelected;
                if (i == 0)
                {
                    state = current;
                }
                else if (state != current)
                {
                    state = null;
                    break;
                }
            }
            this.SetIsSelected(state, false, true);
        }

        public void VerifySelectedValues()
        {
            if (UniqueName == null)
            {
                isSelected = false;
            }
            else
            {
                isSelected = selectedItems.Values.ContainsKey(UniqueName);
            }
            foreach (TreeViewNodeViewModel child in Children)
            {
                MDXMetaDataTreeViewModel mdxChild 
                    = child as MDXMetaDataTreeViewModel;
                if (mdxChild != null)
                {
                    mdxChild.VerifySelectedValues();
                }
            }

            MDXMetaDataTreeViewModel parentMetaDataTreeViewModel
                = Parent as MDXMetaDataTreeViewModel;
            if (parentMetaDataTreeViewModel != null)
            {
                parentMetaDataTreeViewModel.VerifyCheckState();
                foreach (KeyValuePair<string, MDXMetaDataTreeViewModel> folder
                    in parentMetaDataTreeViewModel.folders)
                {
                    folder.Value.VerifyCheckState();
                }
            }
            this.OnPropertyChanged("IsSelected");
        }

        public void UpdateSelectedValues(bool? value)
        {
            if (UniqueName != null)
            {
                if (value == true)
                {
                    selectedItems.Values[UniqueName] = UniqueName;
                }
                else if (value == false)
                {
                    selectedItems.Values.Remove(UniqueName);
                }
            }
        }
    }
}
