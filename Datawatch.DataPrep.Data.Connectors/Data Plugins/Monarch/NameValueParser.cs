﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datawatch.Engine
{
    public class NameValueParser
    {
        private SortedDictionary<string, string> Table = new SortedDictionary<string, string>();
        public string DefaultName;
        public bool NameCanHaveSpaces { get; protected set; }
        public bool HasSeparator { get; protected set; }
        protected char _Separator = '\0';

        public char Separator
        {
            get { return _Separator; }
            set
            {
                _Separator = value;
                HasSeparator = true;
            }
        }

        public NameValueParser(string strText, string strDefaultName = "name")
        {
            DefaultName = strDefaultName;
            Parse(strText);
        }

        public NameValueParser(string strText, char separator, bool nameCanHaveSpaces = false,
                               string strDefaultName = "name")
        {
            DefaultName = strDefaultName;
            Separator = separator;
            NameCanHaveSpaces = nameCanHaveSpaces;
            Parse(strText);
        }

        public static bool GetNextNameValuePair(ref string strText, out string strName, out string strValue,
                                                string strDefaultName)
        {
            strName = string.Empty;
            strValue = string.Empty;
            if (string.IsNullOrWhiteSpace(strText))
                return false;
            strText = strText.TrimStart();
            int nIndex = 0;
            StringBuilder strToken = new StringBuilder();
            if (strText.StartsWith("\""))
                strName = strDefaultName;
            else
            {
                for (; nIndex < strText.Length; nIndex++)
                {
                    if (char.IsWhiteSpace(strText[nIndex]) || strText[nIndex] == '=')
                        break;
                    strToken.Append(strText[nIndex]);
                }
                if (strToken.Length == 0)
                    return false;
                strName = strToken.ToString();
                strToken.Clear();
                for (; nIndex < strText.Length; nIndex++)
                    if (!char.IsWhiteSpace(strText[nIndex]))
                        break;
                if (nIndex == strText.Length || strText[nIndex] != '=')
                    return false;
                nIndex++;
            }
            strText = strText.Substring(nIndex).TrimStart();
            bool bQuoted = false;
            if (strText[0] == '"')
            {
                strText = strText.Substring(1);
                bQuoted = true;
            }
            nIndex = 0;
            for (; nIndex < strText.Length; nIndex++)
            {
                if (bQuoted)
                {
                    if (strText[nIndex] == '"')
                    {
                        if (nIndex + 1 < strText.Length && strText[nIndex + 1] == '"')
                        {
                            strToken.Append('"');
                            continue;
                        }
                        nIndex++;
                        break;
                    }
                }
                else
                {
                    if (char.IsWhiteSpace(strText[nIndex]))
                        break;
                }
                strToken.Append(strText[nIndex]);
            }
            if (strToken.Length == 0)
                return false;
            strValue = strToken.ToString();
            strText = strText.Substring(nIndex).TrimStart();
            return true;
        }

        public static void SkipSpaces(string strText, ref int nIndex)
        {
            for (; nIndex < strText.Length; nIndex++)
                if (!char.IsWhiteSpace(strText[nIndex]))
                    break;
        }

        public static bool GetNextNameValuePair(ref string strText, out string strName, out string strValue,
                                                string strDefaultName, bool nameCanHaveSpaces = false,
                                                bool hasSeparator = false, char separator = '\0')
        {
            strName = string.Empty;
            strValue = string.Empty;
            if (string.IsNullOrWhiteSpace(strText))
                return false;
            strText = strText.TrimStart();
            int nIndex = 0;
            StringBuilder strToken = new StringBuilder();
            if (strText.StartsWith("\""))
                strName = strDefaultName;
            else
            {
                for (; nIndex < strText.Length; nIndex++)
                {
                    if ((!nameCanHaveSpaces && char.IsWhiteSpace(strText[nIndex])) || strText[nIndex] == '=' ||
                        (hasSeparator && strText[nIndex] == separator))
                        break;
                    strToken.Append(strText[nIndex]);
                }
                if (strToken.Length == 0)
                {
                    nIndex++;
                    return false;
                }
                strName = strToken.ToString().Trim();
                strToken.Clear();
                if (hasSeparator && nIndex < strText.Length && strText[nIndex] == separator)
                {
                    nIndex++;
                    strText = strText.Substring(nIndex).TrimStart();
                    return false;
                }
                SkipSpaces(strText, ref nIndex);
                if (nIndex == strText.Length || strText[nIndex] != '=')
                    return false;
                nIndex++;
            }
            strText = strText.Substring(nIndex).TrimStart();
            bool bQuoted = false;
            if (strText[0] == '"')
            {
                strText = strText.Substring(1);
                bQuoted = true;
            }
            nIndex = 0;
            for (; nIndex < strText.Length; nIndex++)
            {
                if (bQuoted)
                {
                    if (strText[nIndex] == '"')
                    {
                        if (nIndex + 1 < strText.Length && strText[nIndex + 1] == '"')
                        {
                            strToken.Append('"');
                            continue;
                        }
                        nIndex++;
                        break;
                    }
                }
                else
                {
                    if (hasSeparator)
                    {
                        if (strText[nIndex] == separator)
                            break;
                    }
                    else if (char.IsWhiteSpace(strText[nIndex]))
                        break;
                }
                strToken.Append(strText[nIndex]);
            }
            if (strToken.Length == 0)
                return false;
            strValue = strToken.ToString();
            if (hasSeparator && nIndex < strText.Length && strText[nIndex] == separator)
                nIndex++;
            strText = strText.Substring(nIndex).TrimStart();
            return true;
        }

        public void Parse(string strText, bool bCaseSensitive = false)
        {
            if (NameCanHaveSpaces || HasSeparator)
            {
                Parse2(strText, bCaseSensitive);
                return;
            }
            Table.Clear();
            string strName, strValue;
            while (GetNextNameValuePair(ref strText, out strName, out strValue, DefaultName))
                Table[strName.ToLower()] = strValue;
        }

        public void Parse2(string strText, bool bCaseSensitive = false)
        {
            Table.Clear();
            string strName, strValue;
            while (GetNextNameValuePair(ref strText, out strName, out strValue, DefaultName, NameCanHaveSpaces,
                                        HasSeparator, Separator))
                Table[strName.ToLower()] = strValue;
        }

        public bool TryGetValue(string strName, out string strValue)
        {
            strValue = null;
            string str = strName.ToLower();
            if (!Table.ContainsKey(str))
                return false;
            strValue = Table[str];
            return true;
        }

        public string GetValue(string strName, string strDefault = null)
        {
            string str = strName.ToLower();
            if (!Table.ContainsKey(str))
                return strDefault;
            return Table[str];
        }

        public bool HasName(string strName)
        {
            return Table.ContainsKey(strName.ToLower());
        }
    }
}

