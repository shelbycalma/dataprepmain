﻿namespace Panopticon.MonarchPlugin
{
    public static class MonarchUtils
    {
        private const string MonarchConnectionString =
            "Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Persist Security Info=False";

        public static string GetMonarchConnectionString(string databasePath)
        {
            return string.Format(MonarchConnectionString,
                databasePath);
        }
    }
}