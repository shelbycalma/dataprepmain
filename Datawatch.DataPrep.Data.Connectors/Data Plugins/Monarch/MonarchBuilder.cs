﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.Definitions;
using Datawatch.Engine;
using Datawatch.Engine.BuilderTaskManagement;
using Datawatch.Interfaces;
using Datawatch.SDK;

namespace Panopticon.MonarchPlugin
{
    internal class MonarchBuilder
    {
        private readonly string workbookDir;

        //default constructor required by ResolveAssemblies class
        public MonarchBuilder()
        {
        }

        public MonarchBuilder(MonarchSettings settings, int rowCount, string workbookDir)
        {
            Settings = settings;
            RowCount = rowCount;
            this.workbookDir = workbookDir;
        }

        public int RowCount { get; set; }

        public MonarchSettings Settings { get; set; }

        public static ModelDefinition.FieldDefinition CreateImportField(RegistryDefinition registryDefinition, IColumnMetaData columnMetaData, int sourceId = 0)
        {
            var newField = new ModelDefinition.FieldDefinition(registryDefinition)
            {
                Name = columnMetaData.ColumnName,
                Size = columnMetaData.Size,
                DecimalPrecision = columnMetaData.DecimalPrecision,
                DataType = Datawatch.Definitions.Converters.DataTypeConverter
                .MonarchDataTypeToString(columnMetaData.DataType),
            };
            newField.SetFieldType("database");
            newField.ImportSpecific.OriginalName = columnMetaData.ColumnName;
            newField.ImportSpecific.OriginalType = Datawatch.Definitions.Converters
                .DataTypeConverter.GetCharForMonarchType(columnMetaData.DataType);
            newField.ImportSpecific.SourceId = sourceId;

            return newField;
        }

        public StandaloneTable BuildTable()
        {
            //create Datawatch object Standalone table
            StandaloneTable table = new StandaloneTable();
            try
            {
                if (Settings.IsTable)
                    ReadFromMainTable(table);
                else
                    ReadFromSummary(table);
            }
            catch (Exception e)
            {
                Trace.WriteLine("BuildTable() exception " + e.Message);
            }

            //return our fully built table assuming there is no exception
            //if there is, the table will be empty
            return table;
        }

        private static object CheckAndFixValueType(object value, Column column)
        {
            if (!(column is TextColumn) && value is string && string.IsNullOrEmpty((string)value))
            {
                value = null;
            }

            return value;
        }

        private static TableImportDefinition CreateTableImportDefinition(string connectionString, string password,
                    RegistryDefinition registry)
        {
            var tableImportDefinition = new TableImportDefinition(registry) { DataSource = connectionString };
            //connect tableImport Definition to correct database
            //encrypt password in tableImportDefinition
            if (!string.IsNullOrEmpty(password))
            {
                tableImportDefinition.Password = Datawatch.Engine.Common.Utility.Security.Encrypt(password);
            }
            //set import type
            ImportType importTypeNum = TableImport.CalcInputFormatFromSourceString(connectionString);
            //string importTypeString = TableImport.ImportTypeToImportTypeString(importTypeNum);
            TableImport.ResolveImportType(tableImportDefinition);
            //tableImportDefinition.ImportType = importTypeString;

            return tableImportDefinition;
        }

        private static void PopulateFields(TableImportDefinition tableImport, ModelDefinition modelDefinition, RegistryDefinition registryDefinition)
        {
            var helper = new DatabaseMetaDataHelper(tableImport, modelDefinition, registryDefinition, null);
            var databaseMetaData = helper.CreateMetaData();
            var columnMetaDatas = databaseMetaData.GetColumnMetaData(tableImport.Table);
            var fieldDefinitions = columnMetaDatas.Select(columnMetaData => CreateImportField(registryDefinition, columnMetaData));
            modelDefinition.Fields.AddRange(fieldDefinitions);
        }

        private static object ReplaceLineEndings(object value)
        {
            string stringValue = value as string;
            return stringValue == null ? value : stringValue.Replace(Environment.NewLine, " ");
        }

        //method to add a column to the StandaloneTable
        private void AddTableColumn(
            StandaloneTable table, string columnName, MonarchDataType type)
        {
            //add a different column based on the dataType
            switch (type)
            {
                //if it isn't a numeric or DateTime, it is a Text type
                case MonarchDataType.Numeric:
                    table.AddColumn(new NumericColumn(columnName));
                    break;
                case MonarchDataType.DateTime:
                    table.AddColumn(new TimeColumn(columnName));
                    break;
                default:
                    table.AddColumn(new TextColumn(columnName));
                    break;
            }
        }

        //method to add a particular value to the StandaloneTable
        private void AddValueToTable(
            StandaloneTable table, int columnIndex, Row tableRow,
            ISequentialTableReader reader, IDictionary<string, object> runtimeFields)
        {
            //get the column name based on the passed in column index
            string columnName = reader.GetName(columnIndex);
            //get the column in question based on the name
            Column column = table.GetColumn(columnName);

            if (runtimeFields.ContainsKey(columnName))
            {
                column.SetValue(tableRow, runtimeFields[columnName]);
                return;
            }

            if (reader.IsDBNull(columnIndex))
                column.SetValue(tableRow, null);
            else
            {
                //use the reader to get the value at the column index
                object value = reader.GetValue(columnIndex);

                value = CheckAndFixValueType(value, column);

                //set the StandaloneTable column value at a specific row
                column.SetValue(tableRow, ReplaceLineEndings(value));
            }
        }

        private void AddValueToTable(StandaloneTable table, Row tableRow,
            object value, string columnName, IDictionary<string, object> runtimeFields)
        {
            //get the column object.
            Column column = table.GetColumn(columnName);

            if (runtimeFields.ContainsKey(columnName))
            {
                column.SetValue(tableRow, runtimeFields[columnName]);
                return;
            }

            value = CheckAndFixValueType(value, column);

            //set the StandaloneTable from the cell value.
            column.SetValue(tableRow, ReplaceLineEndings(value));
        }

        private IDictionary<string, object> ConvertRuntimeFields(
            IEnumerable<KeyValuePair<string, string>> stringRuntimeFields, StandaloneTable table)
        {
            IDictionary<string, object> result = new Dictionary<string, object>();

            foreach (KeyValuePair<string, string> entry in stringRuntimeFields)
            {
                Column column = table.GetColumn(entry.Key);
                if (column == null) continue;

                object value;
                if (column is NumericColumn)
                {
                    double doubleValue;
                    if (double.TryParse(entry.Value, NumberStyles.Any,
                        CultureInfo.InvariantCulture, out doubleValue))
                    {
                        value = doubleValue;
                    }
                    else
                    {
                        continue;
                    }
                }
                else if (column is TimeColumn)
                {
                    DateTime dateTimeValue;

                    if (DateTime.TryParse(entry.Value, CultureInfo.InvariantCulture,
                        DateTimeStyles.AdjustToUniversal, out dateTimeValue))
                    {
                        value = dateTimeValue;
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    value = entry.Value;
                }

                result.Add(entry.Key, value);
            }

            return result;
        }

        private void LoadProjectFiles(EngineApi engine)
        {
            string projectPath = Settings.ProjectPath;
            if (projectPath != null && workbookDir != null && !Path.IsPathRooted(projectPath))
            {
                projectPath = Path.Combine(workbookDir, projectPath);
            }

            if (!string.IsNullOrWhiteSpace(projectPath) && File.Exists(projectPath))
            {
                engine.LoadProject(projectPath);
                Log.Info("MonarchConnector loaded project " + projectPath);
            }
            else if (!string.IsNullOrWhiteSpace(Settings.ExportPath))
            {
                string exportPath = Settings.ExportPath;
                if (!Path.IsPathRooted(exportPath))
                {
                    exportPath = Path.Combine(workbookDir, Settings.ModelPath);
                }
                var connectionString =
                    MonarchUtils.GetMonarchConnectionString(exportPath);
                var tableImportDefinition = CreateTableImportDefinition
                    (connectionString, null, engine.RegistryDefinition);

                tableImportDefinition.DataSource = connectionString;
                tableImportDefinition.Table = Settings.ExportTableName;
                tableImportDefinition.Name = Settings.ExportTableName;

                PopulateFields(tableImportDefinition, engine.ModelDefinition,
                    engine.RegistryDefinition);

                engine.ProjectDefinition.Inputs.IsDatabaseInput = true;
                engine.ProjectDefinition.Inputs.TableImport =
                    tableImportDefinition;
            }
            else
            {
                string modelPath = Settings.ModelPath;
                if (!Path.IsPathRooted(modelPath))
                {
                    modelPath = Path.Combine(workbookDir, Settings.ModelPath);
                }

                engine.LoadModel(modelPath);
                Log.Info("ModelerConnector loaded model " + modelPath);
                foreach (string report in Settings.ReportPaths)
                {
                    string reportPath = report;
                    if (!Path.IsPathRooted(reportPath))
                    {
                        reportPath = Path.Combine(workbookDir, report);
                    }

                    engine.AddReport(reportPath);
                    Log.Info("ModelerConnector added report " + reportPath);
                }
            }
        }

        /// <summary>
        /// Fills table with data from main table.
        /// </summary>
        private void ReadFromMainTable(StandaloneTable table)
        {
            EngineApi engine = null;
            try
            {
                table.BeginUpdate();

                engine = new EngineApi(new RegistryDefinition());

                this.LoadProjectFiles(engine);
                engine.Open();

                ISequentialTableReader tableReader = engine.GetTableReader(Settings.CurrentSort, Settings.CurrentFilter);

                //if we do have data
                if (tableReader.Read())
                {
                    //load columns
                    int columnCount = tableReader.FieldCount;
                    //loop to set up the column metadata inside StandaloneTable
                    for (int i = 0; i < columnCount; i++)
                    {
                        //if the table doesnt already have the column in it
                        if (!table.ContainsColumn(tableReader.GetName(i)))
                            //add the table column using the name and the data type
                            AddTableColumn(table, tableReader.GetName(i),
                                Converters.DataTypeConverter
                                    .GetMonarchTypeFromChar(
                                        tableReader.GetMonarchDataType(i)));
                    }

                    IDictionary<string, object> runtimeFields =
                        ConvertRuntimeFields(Settings.RuntimeFields, table);

                    //loop to add each value to the table
                    int rowNum = 0;
                    //check to see if RowCount passed in is -1, which means NO LIMIT.
                    if (RowCount == -1)
                    {
                        RowCount = tableReader.TotalRowCountIsReliable
                            ? tableReader.TotalRows
                            : int.MaxValue;
                    }
                    do
                    {
                        //add a row to the table
                        Row tableRow = table.AddRow();
                        rowNum++;
                        //for each column in the table
                        for (int i = 0; i < columnCount; i++)
                        {
                            //add value to the table
                            AddValueToTable(table, i, tableRow, tableReader, runtimeFields);
                        }
                        //while the table has information to read 
                    } while (rowNum < RowCount && tableReader.Read());
                }
            }
            catch (Exception e)
            {
                Log.Error("MonarchConnector failed to read from main table: " + e.Message);
                throw;
            }
            finally
            {
                table.EndUpdate();
                // Disposing engine includes cache database cleanup 
                // which may take a significant amount of time (1 minute or more).
                if (engine != null)
                    System.Threading.Tasks.Task.Run(() => engine.Dispose());
            }
        }

        /// <summary>
        /// Fills table with data from selected summary.
        /// </summary>
        private void ReadFromSummary(StandaloneTable table)
        {
            EngineApi engine = null;

            try
            {
                //beginUpdate presumably allows the table to be edited
                table.BeginUpdate();

                engine = new EngineApi(new RegistryDefinition());

                this.LoadProjectFiles(engine);
                engine.Open();

                IGridReader reader = null;
                if (!string.IsNullOrWhiteSpace(Settings.ExportPath))
                {
                    reader = engine.GetSummaryReader(null);
                }
                else
                {
                    IBuilderObject summary = engine.Summaries
                        .FirstOrDefault(item =>
                        Settings.CurrentSummaryName.Equals(
                            item.Name, StringComparison.CurrentCultureIgnoreCase));

                    if (summary == null)
                        throw new Exception(
                            "Summary '" + (Settings.CurrentSummaryName ?? "<NULL>") + "' not found");

                    reader = engine.GetSummaryReader(
                        Settings.CurrentSummaryName,
                        Settings.CurrentSort, Settings.CurrentFilter);
                }


                //load columns
                int columnCount = reader.TotalColumns;
                //loop to set up the column metadata inside StandaloneTable
                for (int i = 0; i < columnCount; i++)
                {
                    if (reader.IsColumnHidden(i)) continue;

                    string name = reader.SingleRowColumnLabel(i);

                    //if the table doesnt already have the column in it                    
                    if (!table.ContainsColumn(name))
                    {
                        //add the table column using the name and the data type
                        AddTableColumn(table, name,
                            Datawatch.Definitions.Converters.DataTypeConverter
                                .GetMonarchTypeFromChar(reader.FieldDataType(i)));
                    }
                }

                IDictionary<string, object> runtimeFields =
                    ConvertRuntimeFields(Settings.RuntimeFields, table);

                //check to see if RowCount passed in is -1, which means NO LIMIT.
                int rowCount = RowCount == -1 
                    ? reader.TotalRows 
                    : Math.Min(RowCount, reader.TotalRows);

                // Add rows to the table.
                for (int row = 0; row < rowCount; row++)
                {
                    Row tableRow = null;
                    for (int col = 0; col < columnCount; col++)
                    {
                        string columnName = reader.SingleRowColumnLabel(col);
                        if (!table.ContainsColumn(columnName)) continue;

                        ICellInfo cellInfo = reader.GetCellInfo(row, col);

                        if (cellInfo.RowType == RowTypes.Blank ||
                            cellInfo.RowSubType == RowSubTypes.Subtotal ||
                            cellInfo.RowSubType == RowSubTypes.GrandTotal ||
                            cellInfo.RowSubType == RowSubTypes.Header)
                        {
                            break;
                        }

                        if (tableRow == null)
                        {
                            tableRow = table.AddRow();
                        }

                        AddValueToTable(
                            table,
                            tableRow,
                            cellInfo.ObjValue,
                            columnName,
                            runtimeFields);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("MonarchConnector failed to read from summary: " +
                    e.Message);
                throw;
            }
            finally
            {
                table.EndUpdate();
                // Disposing engine includes cache cleanup 
                // which may take a significant amount of time (1 minute or more).
                if (engine != null)
                    System.Threading.Tasks.Task.Run(() => engine.Dispose());
            }
        }
    }
}