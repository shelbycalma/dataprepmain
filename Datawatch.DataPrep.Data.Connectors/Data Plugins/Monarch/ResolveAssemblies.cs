﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Win32;

namespace Panopticon.MonarchPlugin
{
    //class used to load necessary Assemblies for use in Modeler project loading
    internal static class ResolveAssemblies
    {
        //hashSet to contain loaded assemblies
        public static Dictionary<string, Assembly> _assemblies =
            new Dictionary<string, Assembly>();

        public static string _modelerConnectorPluginDirectory;

        /// <summary>
        /// Resolves assemblies based on the needs of the calling class
        /// </summary>
        static ResolveAssemblies()
        {
            Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
            bool resolveAssemblies = true;

            foreach (Assembly assembly in assemblies)
            {
                if (assembly.FullName.StartsWith("Datawatch.") == true &&
                    assembly.FullName.StartsWith("Datawatch.DataPrep.Data.") == false)
                    resolveAssemblies = false;
            }

            if (resolveAssemblies)
            {
                string location = Assembly.GetExecutingAssembly().Location;
                _modelerConnectorPluginDirectory = Path.GetDirectoryName(location);
            }

            //set up event handler for necessary DLLs
            AppDomain.CurrentDomain.AssemblyResolve +=
                new ResolveEventHandler(ResolveAssemblyEventHandler);
        }

        /// <summary>
        /// Static method that needs to be called in the static constructor of the primary class
        /// </summary>
        public static void Initialize()
        {
        }

        //assembly resolveEventHandler, handles actual loading
        private static Assembly ResolveAssemblyEventHandler(
            object sender, ResolveEventArgs args)
        {
            //get name of dll to load
            string name = args.Name;
            //split on first comma
            string[] assemblyInformation = name.Split(',');
            //set up DLL name variable
            string dllName = null;
            //if we run across a .resources filename, return null. Otherwise 
            if (!assemblyInformation[0].Contains(".resources"))
            {
                //set up our proper dll filename by adding .dll to the end
                dllName = assemblyInformation[0] + ".dll";
            }
            else
            {
                //null is returned because we do not create different resouces files for localization data (languages)
                //we overwrite our english resources files with other language resource files, but keep the same naming
                //don't ask why
                return null;
            }

            //get the path to the required dll's
            string fullPath;
            // first look for the dll's under in the ModelConnector sub-directory under the plugins directory.
            fullPath = GetModelerSubDirectoryPath(dllName);

            // if not found, look for the dll's where Modeler is installed on this machine.
            if (string.IsNullOrWhiteSpace(fullPath))
                fullPath = GetInstalledFilePath(dllName);

            // and finally if we requested Datawatch.* assembly and request for any version of this assembly 
            // try to return already loaded version
            AssemblyName assemblyName = new AssemblyName(args.Name);
            if (assemblyName.Name.StartsWith("Datawatch.", StringComparison.InvariantCultureIgnoreCase) &&
                assemblyName.Version == null ||
                assemblyName.Name == "DataPrep.Common" ||
                assemblyName.Name == "DataPrep.Interactive")
            {
                Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assembly in assemblies)
                {
                    if (string.Compare(assembly.GetName().Name,
                        assemblyName.Name,
                        StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        return assembly;
                    }
                }
            }

            //check whether the file actually exists in the directory
            //if the returned path is null or empty
            if (String.IsNullOrEmpty(fullPath))
            {
                //return null and do nothing
                return null;
            }

            //We are keeping track of the dependent assemblies that we already have loaded
            //as to avoid reolading them
            //If the hashtable doesnt contain the name of the assembly we want to load
            if (!_assemblies.ContainsKey(name))
            {
                //load the assembly
                Assembly newAssembly = Assembly.LoadFrom(fullPath);
                //add it to the hashtable with key as it's name
                _assemblies.Add(name, newAssembly);
                //return it
                return newAssembly;
            }
            //otherwise, we have the assembly already loaded, simply return it
            return (Assembly) _assemblies[name];
        }

        //get the full path to the assembly
        private static string GetInstalledFilePath(string fileName)
        {
            string keyPath =
                @"SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\DWModeler.exe";
            using (
                RegistryKey key = Registry.LocalMachine.OpenSubKey(keyPath, false)
                )
            {
                if (key != null)
                {
                    string path = key.GetValue("Path") as string;
                    string[] realPath = path.Split(';');

                    string filePath = Path.Combine(realPath[1], fileName);
                    filePath = filePath.Trim();
                    if (File.Exists(filePath))
                        return filePath;

                    return String.Empty;
                }
                return String.Empty;
            }
        }

        private static string GetModelerSubDirectoryPath(string fileName)
        {
            if (string.IsNullOrWhiteSpace(_modelerConnectorPluginDirectory))
                return string.Empty;

            string filePath = Path.Combine(_modelerConnectorPluginDirectory,
                fileName);
            filePath = filePath.Trim();
            if (File.Exists(filePath))
                return filePath;
            return string.Empty;
        }
    }
}