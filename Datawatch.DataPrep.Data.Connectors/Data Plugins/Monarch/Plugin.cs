﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.MonarchPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<MonarchSettings>
    {
        //private readonly object lockObject = new object();

        internal const string PluginId = "ModelerConnector";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Datawatch Monarch";
        internal const string PluginType = DataPluginTypes.File;

        #region Properties

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        #endregion

        #region Constructor

        public Plugin()
        {
            ResolveAssemblies.Initialize();
        }

        #endregion

        #region Methods

        public override MonarchSettings CreateSettings(PropertyBag bag)
        {
            return new MonarchSettings(bag);
        }

        public override ITable GetData(
            string workbookDir, string dataDir, PropertyBag settings,
            IEnumerable<ParameterValue> parameters, int rowcount, bool applyRowFilteration)
        {
            //if (Datawatch.DataPrep.Engine.Engine.PluginManager == null)
            //{
            //    lock (lockObject)
            //    {
            //        if (Datawatch.DataPrep.Engine.Engine.PluginManager == null)
            //        {
            //            Datawatch.DataPrep.Engine.Engine.PluginManager = (PluginManager)host.PluginManager;
            //        }        
            //    }
            //}

            MonarchBuilder tableBuilder = new MonarchBuilder(new MonarchSettings(settings),
                rowcount, workbookDir);
            StandaloneTable result = tableBuilder.BuildTable();
            return result;
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            MonarchSettings modelerSettings = new MonarchSettings(settings);

            List<string> files = new List<string>();

            if (!string.IsNullOrWhiteSpace(modelerSettings.ProjectPath))
            {
                files.Add(modelerSettings.ProjectPath);
            }

            if (!string.IsNullOrWhiteSpace(modelerSettings.ModelPath))
            {
                files.Add(modelerSettings.ModelPath);
            }

            files.AddRange(modelerSettings.ReportPaths.Where(rp => !string.IsNullOrWhiteSpace(rp)));
            
            return files.Any() ? files.ToArray() : null;
        }

        public override void UpdateFileLocation(PropertyBag settings, string oldPath,
            string newPath, string workbookPath)
        {
            string newRelativePath = Utils.ComputeRelativePath(workbookPath, newPath);

            if (Path.GetFileName(settings.Values["ProjectPath"]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values["ProjectPath"] = newRelativePath;
                return;
            }

            if (Path.GetFileName(settings.Values["ModelPath"]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values["ModelPath"] = newRelativePath;
                return;
            }

            string reportsSetting = settings.Values["ReportPathList"];
            if (string.IsNullOrWhiteSpace(reportsSetting)) return;

            string[] reports = reportsSetting.Split(';');

            bool hasChangedReports = false;

            for (int i = 0; i < reports.Length; i++)
            {
                if (Path.GetFileName(reports[i]) ==
                    Path.GetFileName(oldPath))
                {
                    reports[i] = newRelativePath;
                    hasChangedReports = true;
                    break;
                }
            }

            if (hasChangedReports)
            {
                StringBuilder text = new StringBuilder();
                foreach (string path in reports)
                {
                    if (text.Length > 0)
                    {
                        text.Append(";");
                    }

                    text.Append(path);
                }
                settings.Values["ReportPathList"] = text.ToString();
            }
        }

        #endregion
    }
}