﻿namespace Panopticon.MonarchPlugin
{
    public enum ViewMode
    {
        Model,
        Project,
        Export
    }
}