﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Microsoft.VisualBasic.FileIO;

namespace Panopticon.MonarchPlugin
{
    public class MonarchSettings : ConnectionSettings
    {
        #region Properties

        private readonly IDictionary<string, string> runtimeFields;
        
        public ObservableCollection<string> ReportPaths { get; set; }

        public IDictionary<string, string> RuntimeFields
        {
            get { return runtimeFields; }
        }

        public bool IsTable
        {
            get { return Convert.ToBoolean(GetInternal("IsTable")); }
            set { SetInternal("IsTable", Convert.ToString(value)); }
        }

        public string CurrentSummaryName
        {
            get { return GetInternal("CurrentSummaryName", ""); }
            set { SetInternal("CurrentSummaryName", value); }
        }

        public string ExportTableName
        {
            get { return GetInternal("ExportTableName", ""); }
            set { SetInternal("ExportTableName", value); }
        }

        public string ProjectPath
        {
            get { return GetInternal("ProjectPath"); }
            set
            {
                SetInternal("ProjectPath", value);
                SetTitle();
            }
        }

        public string ModelPath
        {
            get { return GetInternal("ModelPath"); }
            set
            {
                SetInternal("ModelPath", value);
                SetTitle();
            }
        }

        public string ExportPath
        {
            get { return GetInternal("ExportPath"); }
            set
            {
                SetInternal("ExportPath", value);
                SetTitle();
            }
        }

        public string CurrentSort
        {
            get { return GetInternal("CurrentSort"); }
            set { SetInternal("CurrentSort", value); }
        }

        public string CurrentFilter
        {
            get { return GetInternal("CurrentFilter"); }
            set { SetInternal("CurrentFilter", value); }
        }

        #endregion

        #region Constructors

        public MonarchSettings(PropertyBag bag) : base(bag)
        {
            ReportPaths = new ObservableCollection<string>();
            LoadReportPaths();
            runtimeFields = CreateSettingDictionary("RuntimeFieldsNames", "RuntimeFieldsValues");
        }

        #endregion

        #region Methods

        private void SetTitle()
        {
            if (!string.IsNullOrWhiteSpace(ProjectPath))
                Title = Path.GetFileName(ProjectPath).Split('.').FirstOrDefault();
            else if (!string.IsNullOrWhiteSpace(ExportPath))
                Title = Path.GetFileName(ExportPath).Split('.').FirstOrDefault();
            else if (!string.IsNullOrWhiteSpace(ModelPath))
                Title = Path.GetFileName(ModelPath).Split('.').FirstOrDefault();
        }

        private void LoadReportPaths()
        {
            ReportPaths.Clear();
            string text = GetInternal("ReportPathList");
            if (string.IsNullOrWhiteSpace(text))
                return;
            string[] paths = text.Split(';');
            foreach (string path in paths)
                if (!string.IsNullOrWhiteSpace(path))
                    ReportPaths.Add(path);
        }

        public void SaveReportPaths()
        {
            StringBuilder text = new StringBuilder();
            foreach (string path in ReportPaths)
            {
                if (text.Length > 0)
                    text.Append(";");
                text.Append(path);
            }
            SetInternal("ReportPathList", text.ToString());
        }

        private IDictionary<string, string> CreateSettingDictionary(
            string keySettingName, string valueSettingName)
        {
            string namesString = GetInternal(keySettingName);
            string valuesString = GetInternal(valueSettingName);

            if (string.IsNullOrWhiteSpace(namesString) ||
                string.IsNullOrWhiteSpace(valuesString))
            {
                return new Dictionary<string, string>();
            }

            IList<string> names = SplitList(namesString, ";");
            IList<string> values = SplitList(valuesString, ";");
            IDictionary<string, string> result = new Dictionary<string, string>();

            for (int i = 0; i < names.Count; i++)
            {
                if (result.ContainsKey(names[i])) continue;
                if (values.Count < i + 1) break;

                result.Add(names[i], values[i]);
            }

            return result;
        }

        private static IList<string> SplitList(string input, string separator)
        {
            using (TextFieldParser parser = new TextFieldParser(new MemoryStream(Encoding.UTF8.GetBytes(input))))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.Delimiters = new[] { separator };
                parser.HasFieldsEnclosedInQuotes = true;
                return parser.ReadFields();
            }
        }

        #endregion
    }
}