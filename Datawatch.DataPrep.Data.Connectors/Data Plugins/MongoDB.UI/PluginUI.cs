﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.MongoDBPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, MongoSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(
            IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            MongoSettings mongoSettings = new MongoSettings(new PropertyBag(),
                Plugin.PluginManager, parameters);
            MongoSettingsViewModel settingsViewModel
                = new MongoSettingsViewModel(mongoSettings,
                    mongoSettings.Parameters);

            ConfigWindow window = new ConfigWindow(settingsViewModel);
            window.Owner = owner;

            bool? dialogResult = window.ShowDialog();

            if (dialogResult != true)
                return null;

            return settingsViewModel.Model.ToPropertyBag();
        }

        public override object GetConfigElement(
            PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            var setts = new MongoSettings(bag, Plugin.PluginManager, parameters);
            ConfigPanel panel = new ConfigPanel
            {
                Settings =
                    new MongoSettingsViewModel(setts, setts.Parameters)
            };

            return panel;
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel panel = (ConfigPanel)obj;
            return panel.Settings.Model.ToPropertyBag();
        }
    }
}
