﻿using System.Windows.Controls;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.MongoDBPlugin.UI
{
    /// <summary>
    /// Interaction logic for ColumnConfigurator.xaml
    /// </summary>
    public partial class ColumnConfigurator : UserControl
    {
        private ICommand testConnection;
        private ICommand generateColumns;

        public ColumnConfigurator()
        {
            InitializeComponent();
        }

        private void Delete_CanExecute(
            object sender,
            CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter is ColumnDefinition;
        }

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ((IMessageQueueConnectorSettings) DataContext).ParserSettings.
                RemoveColumnDefinition((Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing.ColumnDefinition) e.Parameter);
        }

        public ICommand GenerateColumns
        {
            get
            {
                if (generateColumns == null)
                {
                    generateColumns = new DelegateCommand(
                        DoGenerateColumns, CanGenerateColumns);
                }
                return generateColumns;
            }
        }

        private bool CanGenerateColumns()
        {
            if (DataContext == null)
            {
                return false;
            }
            return
                ((IMessageQueueConnectorSettings) DataContext).CanGenerateColumns
                    ();
        }

        private void DoGenerateColumns()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                ((IMessageQueueConnectorSettings) DataContext).DoGenerateColumns();
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        public ICommand TestConnection
        {
            get
            {
                if (testConnection == null)
                {
                    testConnection = new DelegateCommand(DoTestConnection,
                        CanTestConnection);
                }
                return testConnection;
            }
        }

        private bool CanTestConnection()
        {
            if (DataContext == null)
            {
                return false;
            }
            return
                ((IMessageQueueConnectorSettings) DataContext).CanTestConnection();
        }

        private void DoTestConnection()
        {
            ((IMessageQueueConnectorSettings) DataContext).DoTestConnection();
        }
    }
}