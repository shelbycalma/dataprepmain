﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Panopticon.MongoDBPlugin.UI.Converters
{
    internal class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            var boolValue = (bool) value;
            var param = parameter as string;
            if (param == "not")
                boolValue = !boolValue;
            if (boolValue)
                return Visibility.Visible;
            return Visibility.Collapsed;
        }

        public object ConvertBack(
            object value, Type targetType, object parameter,
            CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}