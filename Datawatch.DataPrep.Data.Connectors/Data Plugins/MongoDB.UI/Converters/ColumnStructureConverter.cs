﻿using System;
using System.Windows;
using System.Windows.Data;
using Panopticon.MongoDBPlugin.Enums;
using Panopticon.MongoDBPlugin.Properties;

namespace Panopticon.MongoDBPlugin.UI.Converters
{
    [ValueConversion(typeof (ColumnStructure), typeof (string))]
    public class ColumnStructureConverter : IValueConverter
    {
        public object Convert(
            object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (!(value is ColumnStructure))
                return null;

            ColumnStructure columnType = (ColumnStructure) value;

            switch (columnType)
            {
                case ColumnStructure.Value:
                    return Resources.UiColumnStructureValue;

                case ColumnStructure.RowArray:
                    return Resources.UiColumnStructureRowArray;

                case ColumnStructure.ColumnArray:
                    return Resources.UiColumnStructureColumnArray;

                case ColumnStructure.HierarchyStructure:
                    return Resources.UiColumnStructureHierarchyStructure;

                case ColumnStructure.HierarchyLabel:
                    return Resources.UiColumnStructureHierarchyLabel;

                default:
                    return null;
            }
        }

        public object ConvertBack(
            object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (Resources.UiColumnStructureValue.Equals(value))
            {
                return ColumnStructure.Value;
            }
            if (Resources.UiColumnStructureRowArray.Equals(value))
            {
                return ColumnStructure.RowArray;
            }
            if (Resources.UiColumnStructureColumnArray.Equals(value))
            {
                return ColumnStructure.ColumnArray;
            }
            if (Resources.UiColumnStructureHierarchyStructure.Equals(value))
            {
                return ColumnStructure.HierarchyStructure;
            }
            if (Resources.UiColumnStructureHierarchyLabel.Equals(value))
            {
                return ColumnStructure.HierarchyLabel;
            }
            return DependencyProperty.UnsetValue;

        }
    }
}