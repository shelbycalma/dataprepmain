﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Panopticon.MongoDBPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
		public static RoutedCommand PreviewCommand = new RoutedCommand("Preview",
			typeof(ConfigPanel));

		public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof (MongoSettingsViewModel),
                typeof (ConfigPanel),
                new PropertyMetadata(Settings_Changed));

        private static void Settings_Changed(
            DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel) obj).OnSettingsChanged(
                (MongoSettingsViewModel) args.OldValue,
                (MongoSettingsViewModel) args.NewValue);
        }

        protected void OnSettingsChanged(
            MongoSettingsViewModel oldSettings,
            MongoSettingsViewModel newSettings)
        {
            DataContext = newSettings;
            PassBox.Password = newSettings != null
                ? newSettings.Model.Password
                : null;
        }

        // Properties
        public MongoSettingsViewModel Settings
        {
            get { return (MongoSettingsViewModel) GetValue(SettingsProperty); }
            set { SetValue(SettingsProperty, value); }
        }

        // Constructors
        public ConfigPanel()
        {
            InitializeComponent();
        }

        private void LoginButton_OnClick(object sender, RoutedEventArgs e)
        {
            Settings.Login();
        }

        private void PassBox_OnPasswordChanged(object sender, RoutedEventArgs e)
        {
            if (Settings == null)
                return;

            Settings.Model.Password = PassBox.Password;
        }

		private void PreviewData_Click(object sender, RoutedEventArgs e)
        {
            Settings.PreviewData();
        }

		private void Preview_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			e.CanExecute = Settings != null && Settings.IsOk;
		}

        private void PreviewGrid_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            string header = e.Column.Header.ToString();

            // Replace all underscores with two underscores, to prevent AccessKey handling
            e.Column.Header = header.Replace("_", "__");
        }
    }
}