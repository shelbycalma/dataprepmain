﻿using System.Windows;
using System.Windows.Input;

namespace Panopticon.MongoDBPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigWindow.xaml
    /// </summary>
    public partial class ConfigWindow : Window
    {
        // Properties
        public MongoSettingsViewModel Settings { get; set; }

        // Commands
        public static RoutedCommand OkCommand = new RoutedCommand("Ok",
            typeof (ConfigWindow));

        // Constructors
        public ConfigWindow(MongoSettingsViewModel settings)
        {
            InitializeComponent();
            Settings = settings;
        }

        // Events
        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Settings != null && Settings.IsOk;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}