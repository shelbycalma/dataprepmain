﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Framework.Plugin;
using PreviewDataTable = Datawatch.DataPrep.Data.Core.Connectors.PreviewDataTable;

namespace Panopticon.MongoDBPlugin.UI
{
    public class MongoSettingsViewModel : INotifyPropertyChanged
    {
        //Private Variables
        private bool noAdvancedQuerySelection = true;
		private DataTable _previewTable;

		// Properties
		private readonly MongoSettings model;

        public MongoSettings Model
        {
            get { return model; }
        }

        private MongoHelper mongoHelper { get; set; }

        private ObservableCollection<ParameterValue> parametersCollection;
        private ObservableCollection<string> availableDatabases;
        private ObservableCollection<string> availableCollections;

		public DataTable PreviewTable
		{
			get { return _previewTable; }
			set
			{
				_previewTable = value;
				OnPropertyChanged("PreviewTable");
			}
		}

		public bool HaveParameters
        {
            get
            {
                if (parametersCollection != null && parametersCollection.Any())
                {
                    return true;
                }

                // if the user had previously checked Parameterize, but then
                // deleted all parameters we want to disable Parameterization
                if (Model.Parameterize)
                {
                    Model.Parameterize = false;
                }

                return false;
            }
        }

        public bool UseCustomFindQuery
        {
            get { return model.UseCustomFindQuery; }
            set
            {
                model.UseCustomFindQuery = value;
                OnPropertyChanged("UseCustomFindQuery");
            }
        }

        public bool NoAdvancedQuerySelection
        {
            get
            {
                if (!UseCustomFindQuery && !Model.Parameterize)
                {
                    return true;
                }

                return noAdvancedQuerySelection;
            }
            set
            {
                noAdvancedQuerySelection = value;
                OnPropertyChanged("NoAdvancedQuerySelection");
            }
        }

        public ObservableCollection<ParameterValue> ParametersCollection
        {
            get
            {
                return parametersCollection
                       ??
                       (parametersCollection =
                           new ObservableCollection<ParameterValue>());
            }
            set
            {
                if (parametersCollection == value) return;
                parametersCollection = value;
                OnPropertyChanged("ParametersCollection");
                OnPropertyChanged("HaveParameters");
            }
        }

        public ObservableCollection<string> AvailableDatabases
        {
            get
            {
                return availableDatabases
                       ??
                       (availableDatabases = new ObservableCollection<string>());
            }
            set
            {
                if (availableDatabases == value) return;
                availableDatabases = value;
                OnPropertyChanged("AvailableDatabases");
            }
        }

        public ObservableCollection<string> AvailableCollections
        {
            get
            {
                return availableCollections
                       ??
                       (availableCollections = new ObservableCollection<string>());
            }
            set
            {
                if (availableCollections == value) return;
                availableCollections = value;
                OnPropertyChanged("AvailableCollections");
            }
        }

        public bool CanLogin
        {
            get { return !String.IsNullOrWhiteSpace(model.Url); }
        }

        public bool ConnectedToServer
        {
            get { return availableDatabases.Count > 0; }
        }

        public bool DatabaseIsSelected
        {
            get
            {
                return availableDatabases.Count > 0 &&
                       !String.IsNullOrWhiteSpace(model.Database);
            }
        }

        public bool CollectionIsSelected
        {
            get
            {
                return availableCollections.Count > 0 &&
                       !String.IsNullOrWhiteSpace(model.Collection);
            }
        }

        public bool IsOk
        {
            get
            {
                if (String.IsNullOrWhiteSpace(model.Url))
                    return false;
                if (String.IsNullOrWhiteSpace(model.Database))
                    return false;
                if (String.IsNullOrWhiteSpace(model.Collection))
                    return false;
                if (model.ParserSettings.ColumnCount == 0)
                    return false;
                if (!model.IsParserSettingsOk)
                    return false;
                if (!IsParameterizationValid())
                    return false;

                return true;
            }
        }

        private bool IsParameterizationValid()
        {
            // No parameterization is valid
            if (!model.Parameterize)
                return true;

            if (!String.IsNullOrWhiteSpace(model.FilterColumn) &&
                !String.IsNullOrWhiteSpace(model.FilterParameter))
                return true;

            return false;
        }

        // Constructors
        public MongoSettingsViewModel(
            MongoSettings settings, IEnumerable<ParameterValue> parameters)
        {
            model = settings;
            model.PropertyChanged += OnModelChanged;
            if (parameters != null)
            {
                //model.Parameters = parameters.ToList();

                parameters.ToList()
                    .ForEach(p => ParametersCollection.Add(p));
            }
        }

        // Methods
        private void OnModelChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "Uri":
                    OnPropertyChanged("CanLogin");
                    break;
                case "Database":
                    PopulateCollections();
                    OnPropertyChanged("DatabaseIsSelected");
                    break;
                case "Collection":
                    OnPropertyChanged("CollectionIsSelected");
                    break;
            }
        }

        public void Login()
        {
            try
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }
                mongoHelper = new MongoHelper(Model, 
                    new MessageBoxBasedErrorReportingService());
                PopulateAll();
                OnPropertyChanged(null);
            }
            catch (Exception ex)
            {
                Log.Exception(ex);
                MessageBox.Show((ErrorMessages.GetDisplayMessage(ex.Message).Length > 0) ? ErrorMessages.GetDisplayMessage(ex.Message) : ex.Message, null,
                    MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
            }
        }

        private void PopulateAll()
        {
            PopulateDatabases();
            PopulateCollections();
        }

        private void PopulateDatabases()
        {
            AvailableDatabases = mongoHelper.GetDatabaseNames();
        }

        private void PopulateCollections()
        {
            AvailableCollections = mongoHelper.GetCollectionNames();
        }

        // INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;


        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

		
		internal void PreviewData()
		{
			try
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = Cursors.Wait;
                }

				int previewRowLimit;
				RegistryUtils.GetMonarchRegistryValue("Settings", "PreviewRowLimit", out previewRowLimit);

				if (previewRowLimit == 0)
				{
					previewRowLimit = 10;
				}

				Panopticon.MongoDBPlugin.Plugin p = new Plugin();
				PropertyBag bag = model.ToPropertyBag();
                
                //check if there are any values in the property bag
                if (bag.Values.Count > 0)
                {
                    ITable t = p.GetData(string.Empty, string.Empty, bag, null, previewRowLimit, true);
                    if (t != null)
                    {
                        _previewTable = PreviewDataTable.GetPreviewTable(t);
                        OnPropertyChanged("PreviewTable");
                    }
                }
			}
			catch (Exception ex)
			{
				Log.Exception(ex);
				MessageBox.Show(ex.Message, null,
					MessageBoxButton.OK, MessageBoxImage.Error);
			}
			finally
			{
                if (System.Threading.Thread.CurrentThread.GetApartmentState().Equals(System.Threading.ApartmentState.STA))
                {
                    Mouse.OverrideCursor = null;
                }
			}
		}

	}
}