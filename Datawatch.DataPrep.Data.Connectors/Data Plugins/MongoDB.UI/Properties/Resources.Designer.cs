﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Panopticon.MongoDBPlugin.UI.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Panopticon.MongoDBPlugin.UI.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add Column....
        /// </summary>
        public static string MessageParserConfiguratorAddColumn {
            get {
                return ResourceManager.GetString("MessageParserConfiguratorAddColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Generate Columns....
        /// </summary>
        public static string MessageParserConfiguratorGenerateColumns {
            get {
                return ResourceManager.GetString("MessageParserConfiguratorGenerateColumns", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Message Type:.
        /// </summary>
        public static string MessageParserConfiguratorMessageType {
            get {
                return ResourceManager.GetString("MessageParserConfiguratorMessageType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Array Options.
        /// </summary>
        public static string UiArrayOptionsLabel {
            get {
                return ResourceManager.GetString("UiArrayOptionsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Automatic time column:.
        /// </summary>
        public static string UiAutoTimeColumn {
            get {
                return ResourceManager.GetString("UiAutoTimeColumn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Collection.
        /// </summary>
        public static string UiCollections {
            get {
                return ResourceManager.GetString("UiCollections", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enabled.
        /// </summary>
        public static string UiColumnDefinitionEnabled {
            get {
                return ResourceManager.GetString("UiColumnDefinitionEnabled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to JsonPath.
        /// </summary>
        public static string UiColumnDefinitionJsonPath {
            get {
                return ResourceManager.GetString("UiColumnDefinitionJsonPath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string UiColumnDefinitionName {
            get {
                return ResourceManager.GetString("UiColumnDefinitionName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type.
        /// </summary>
        public static string UiColumnDefinitionType {
            get {
                return ResourceManager.GetString("UiColumnDefinitionType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to XPath.
        /// </summary>
        public static string UiColumnDefinitionXPath {
            get {
                return ResourceManager.GetString("UiColumnDefinitionXPath", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connect.
        /// </summary>
        public static string UiConnect {
            get {
                return ResourceManager.GetString("UiConnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Database.
        /// </summary>
        public static string UiDatabases {
            get {
                return ResourceManager.GetString("UiDatabases", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Date Format.
        /// </summary>
        public static string UiDateFormatLabel {
            get {
                return ResourceManager.GetString("UiDateFormatLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filter by.
        /// </summary>
        public static string UiFilterBy {
            get {
                return ResourceManager.GetString("UiFilterBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Initial timestamp.
        /// </summary>
        public static string UiInitialTimestamp {
            get {
                return ResourceManager.GetString("UiInitialTimestamp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Interval (ms).
        /// </summary>
        public static string UiInterval {
            get {
                return ResourceManager.GetString("UiInterval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Column Count.
        /// </summary>
        public static string UiMaxColumnLabel {
            get {
                return ResourceManager.GetString("UiMaxColumnLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No Advanced Query.
        /// </summary>
        public static string UiNoAdvancedQueryLabel {
            get {
                return ResourceManager.GetString("UiNoAdvancedQueryLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parameter.
        /// </summary>
        public static string UiParameter {
            get {
                return ResourceManager.GetString("UiParameter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parameterize.
        /// </summary>
        public static string UiParameterize {
            get {
                return ResourceManager.GetString("UiParameterize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Password.
        /// </summary>
        public static string UiPassword {
            get {
                return ResourceManager.GetString("UiPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Preview data.
        /// </summary>
        public static string UiPreviewData {
            get {
                return ResourceManager.GetString("UiPreviewData", resourceCulture);
            }
        }

        /// <summary>
        ///   Looks up a localized string similar to Query Options.
        /// </summary>
        public static string UiQueryOptionsLabel {
            get {
                return ResourceManager.GetString("UiQueryOptionsLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Query Document.
        /// </summary>
        public static string UiQueryStringLabel {
            get {
                return ResourceManager.GetString("UiQueryStringLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Structure.
        /// </summary>
        public static string UiStructureLabel {
            get {
                return ResourceManager.GetString("UiStructureLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to URL.
        /// </summary>
        public static string UiUrl {
            get {
                return ResourceManager.GetString("UiUrl", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Use Query Document.
        /// </summary>
        public static string UiUseCustomQueryLabel {
            get {
                return ResourceManager.GetString("UiUseCustomQueryLabel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Username.
        /// </summary>
        public static string UiUsername {
            get {
                return ResourceManager.GetString("UiUsername", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string UiWindowCancel {
            get {
                return ResourceManager.GetString("UiWindowCancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        public static string UiWindowOk {
            get {
                return ResourceManager.GetString("UiWindowOk", resourceCulture);
            }
        }
    }
}
