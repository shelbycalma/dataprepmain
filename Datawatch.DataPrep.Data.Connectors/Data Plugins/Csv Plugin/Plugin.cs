﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Ado;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.CsvPlugin
{
    using Panopticon.CsvPlugin.Properties;
    using Datawatch.DataPrep.Data.Framework;

    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ConnectionSettings>
    {
        public const string PathKey = "path";

        internal const string PluginId = "CSV";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "CSV";
        internal const string PluginType = DataPluginTypes.File;

        private static string ConnectionTemplate= 
            "Provider=Microsoft.Jet.OLEDB.4.0;" + 
            "Data Source={0};Extended Properties=" + 
            "'text;FMT=Delimited'";
    
        private static string QueryTemplate = "SELECT * FROM [{0}]";
        private static string QueryTemplateWithTop = "SELECT TOP {0} * FROM [{1}]";

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        // From version 6.3 the Csv plug-in is obsolete
        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override ConnectionSettings CreateSettings(PropertyBag bag)
        {
            throw new NotImplementedException();
        }

        public override ITable GetAllData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters)
        {
            return GetData(workbookDir, dataDir, settings, parameters, -1);
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount)
        {
            CheckLicense();

            string path = settings.Values[PathKey];

            //TODO: This should be removed, the csv:// prefix is no longer used.
            if (path.StartsWith("csv://"))
            {
                path = path.Substring("csv://".Length);
            }

            if (!Path.IsPathRooted(path))
            {
                path = Path.Combine(workbookDir, path);
            }

            string dir = path.Substring(0, 
                path.LastIndexOf(Path.DirectorySeparatorChar));
            string fileName = Path.GetFileName(path);

            if (!File.Exists(path))
            {
                string testPath = Path.Combine(dataDir, fileName);
                if (File.Exists(testPath))
                {
                    path = testPath;
                    settings.Values[PathKey] = path;
                }
            }

            string connectionString = string.Format(ConnectionTemplate, dir);
            string query;
            if (rowcount < 0)
            {
                query = string.Format(QueryTemplate, fileName);
            }
            else
            {
                query = string.Format(QueryTemplateWithTop, rowcount, fileName);
            }

            // Log the query and time the execution.
            Log.Info(Resources.LogExecutingQuery, query);
            DateTime start = DateTime.Now;

            OleDbConnection connection = null;
            OleDbCommand command = null;
            OleDbDataAdapter adapter = null;
            DataTable table = new DataTable();
            try 
            {
                connection = new OleDbConnection(connectionString);
                connection.Open();

                command = new OleDbCommand(query, connection);
                adapter = new OleDbDataAdapter(command);
                adapter.Fill(table);


                if (parameters != null)
                {
                    for (int i = table.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow row = table.Rows[i];
                        if (!Include(row, parameters))
                        {
                            table.Rows.Remove(row);
                        }
                    }
                }                
            } 
            finally 
            {
                if (adapter != null) 
                {
                    adapter.Dispose();
                }
                if (command != null) 
                {
                    command.Dispose();
                }
                if (connection != null) 
                {
                    connection.Close();
                    connection.Dispose();
                }
            }

            // Done, log out how long it took and how much we got.
            DateTime end = DateTime.Now;
            double seconds = (end - start).TotalSeconds;
            Log.Info(Resources.LogCsvQueryCompleted,
                table.Rows.Count, table.Columns.Count, seconds);

            return new AdoTable(table);
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            return GetData(workbookDir, dataDir, settings, parameters, rowcount);
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            if (settings.Values.ContainsKey(PathKey))
            {
                string path = settings.Values[PathKey];
                return new string[] { path };
            }
            return null;
        }

        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();

            if (Path.GetFileName(settings.Values[PathKey]) ==
                Path.GetFileName(oldPath))
            {
                settings.Values[PathKey] =
                    Datawatch.DataPrep.Data.Core.Utils.ComputeRelativePath(workbookPath, newPath);
            }
        }

        private bool Include(DataRow row,
                            IEnumerable<ParameterValue> parameters)
        {
            if (parameters == null)
            {
                return true;
            }

            // use an encoder to enforce single quotes
            ParameterEncoder encoder = new ParameterEncoder
            {
                EnforceSingleQuoteEnclosure = true
            };

            foreach (ParameterValue parameterValue in parameters)
            {
                // only string parameters need to be tested
                if (parameterValue == null
                    || !(parameterValue.TypedValue is StringParameterValue))
                    continue;

                DataColumn col = row.Table.Columns[parameterValue.Name];
                if (col == null) continue;

                object value = row[col];
                if (value == null || !(value is string)) continue;

                // format the value and determine if it should be included
                string str = (string)value;
                if (encoder.EncodedValue(parameterValue).IndexOf(str) == -1)
                    return false;
            }
            return true;
        }
    }
}
