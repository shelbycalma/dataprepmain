﻿using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.TextFilePlugin
{
    class Utils
    {
        internal static void ReadAndAddRows(StandaloneTable table, StreamReader stream,
            TextFileParser parser, NumericDataHelper numericDataHelper,
            ParameterFilter parameterFilter, TextFileParserSettings parserSettings,
            int maxRowCount)
        {
            Dictionary<string, object> rowDict = new Dictionary<string, object>();

            string[] row = new string[parserSettings.MaxIndex + 1];
            string line = stream.ReadLine();
            while (line != null)
            {
                if (maxRowCount > -1 && table.RowCount >= maxRowCount) break;

                rowDict = parser.Parse(line, rowDict, row);

                if (rowDict.Count > 0)
                {
                    DataPluginUtils.AddRow(rowDict, table,
                       numericDataHelper, parameterFilter, parserSettings);
                }
                line = stream.ReadLine();
            }
        }

        internal static StreamReader GetStreamAndSkipRows(
            TextFileSettings settings, IEnumerable<ParameterValue> parameters, string filePath)
        {
            StreamReader stream = DataPluginUtils.GetStream(settings,
                parameters, filePath);

            for (int i = 0; i < settings.SkipRows; i++)
            {
                stream.ReadLine();
            }
            return stream;
        }

        internal static List<string[]> DoColumnDiscovery(StreamReader stream,
            TextFileSettings settings, bool suggestDateFormat)
        {
            ColumnGenerator cg = new ColumnGenerator(settings.ParserSettings,
                settings.NumericDataHelper, false, suggestDateFormat);

            List<string[]> rowsRead = new List<string[]>();

            int rowsToDiscover = settings.DataDiscoveryRowCount;
            if (((TextFileParserSettings)settings.ParserSettings).IsFirstRowHeading)
            {
                rowsToDiscover++;
            }

            while (!stream.EndOfStream)
            {
                string row = stream.ReadLine();

                if (string.IsNullOrEmpty(row)) continue;

                string[] rowValues = cg.GenerateAndObtainValues(row);

                if (rowValues == null)
                {
                    continue;
                }

                rowsRead.Add(rowValues);

                rowsToDiscover--;
                if (rowsToDiscover <= 0)
                {
                    break;
                }
            }
            return rowsRead;
        }
    }
}
