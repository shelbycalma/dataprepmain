﻿using System;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.IntelligentCache;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.TextFilePlugin
{
    public class TextFileDataReader: DataReaderBase
    {
        private readonly TextFileSettings settings;
        private List<string[]> dataRows;
        private int _currentIndex = -1;
        private string[] row;
        private StreamReader sReader;
        private StaticTextReader staticReader;
        private int columnsRead;
        private TextFileParserSettings parserSettings;
        private Boolean eof = false;
        private Boolean isClosed = false;
        private readonly IEnumerable<ParameterValue> parameters;
        private readonly int maxRowCount;
        private int rowsRead;

        public TextFileDataReader(TextFileSettings settings,
            IEnumerable<ParameterValue> parameters, int maxRowCount)
        {
            if (settings == null)
            {
                throw new ArgumentNullException("parser");
            }

            this.settings = settings;
            this.parameters = parameters;
            this.maxRowCount = maxRowCount;

            Initialize();
        }

        private void Initialize()
        {
            // Discover Data Columns
            StandaloneTable table = new StandaloneTable();
            parserSettings =
                (TextFileParserSettings)settings.ParserSettings;

            table.BeginUpdate();
            dataRows = new List<string[]>();
            sReader = Utils.GetStreamAndSkipRows(settings, parameters, settings.FilePath);
            //if stream can't be initialized return
            if (sReader == null)
            {
                return;
            }
            TextAnalyser analyser = new TextAnalyser(parserSettings);
            if (settings.Version.Major < 2)
            {
                // Old workbook! => Create table doing data discovery and stuff.

                dataRows = Utils.DoColumnDiscovery(sReader, settings, false);
                row = new string[parserSettings.MaxIndex + 1];

                if (dataRows == null) return;

                settings.Version = TextFileSettings.Version_2_0_0_0;
                DataPluginUtils.AddColumns(table, parserSettings.Columns);
                staticReader =
                    new StaticTextReader(sReader, analyser);
            }
            else
            {
                // New Workbook => ColumnDefinition could be found
                DataPluginUtils.AddColumns(table, parserSettings.Columns);
                row = new string[parserSettings.MaxIndex + 1];
                staticReader =
                    new StaticTextReader(sReader, analyser);
                if (((TextFileParserSettings)settings.ParserSettings).IsFirstRowHeading)
                {
                    // Read the expected header row, so we wont get it in the data
                    staticReader.ReadNextLine(row);
                }
            }

            // We're creating a new StandaloneTable on every call, so allow
            // EX to freeze it.
            table.CanFreeze = true;
            table.EndUpdate();

            values = new object[parserSettings.MaxIndex + 1];

            SchemaTable = table;
            staticReader =
                    new StaticTextReader(sReader, analyser);

            rowsRead = 0;

            isClosed = false;
        }

        public override void Close()
        {
            if (sReader != null)
            {
                sReader.Close();
                isClosed = true;
            }
        }

        public override string GetName(int i)
        {
            return SchemaTable.GetColumn(i).Name;
        }

        public override bool IsClosed
        {
            get
            {
                return isClosed;
            }
        }

        public override bool IsDBNull(int i)
        {
            return base.GetValue(i) == null;
        }

        public override bool Read()
        {
            if (maxRowCount >= 0 && rowsRead >= maxRowCount) {
                return false;
            }

            Dictionary<string, object> rowDict = new Dictionary<string, object>();
            var parser = (TextFileParser)parserSettings.CreateParser();
            string line = sReader.ReadLine();

            if (string.IsNullOrEmpty(line)) { return false; }

            rowDict = parser.Parse(line, rowDict, row);

            Array.Clear(values, 0, values.Length);

            DataPluginUtils.ParseObjectArrayData((StandaloneTable)SchemaTable, rowDict, values,
                 settings.NumericDataHelper, parserSettings);

            rowsRead++;

            return true;
        }
    }
}
