using System;
using System.Collections.Generic;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.TextFilePlugin.Properties;


namespace Panopticon.TextFilePlugin
{
    public class TextFileSettings : TextSettingsBase, ITextFileSettings
    {
        public readonly static Version Version_2_0_0_0 = new Version("2.0.0.0");
        private readonly string Csv = Datawatch.DataPrep.Data.Core.MessageParsing.Properties.Resources.UiText;
        private const string VersionProperty = "Version";

        public TextFileSettings(IPluginManager pluginManager)
            : this(pluginManager, null)
        {
        }

        public TextFileSettings(IPluginManager pluginManager, IEnumerable<ParameterValue> parameters)
            : this(pluginManager, new PropertyBag(), parameters)
        {
            Version = Version_2_0_0_0;
        }

        public TextFileSettings(IPluginManager pluginManager, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginManager, bag, parameters)
        {
            PropertyBag parserSettingsBag = bag.SubGroups["ParserSettings"];

            string temp;
            if (bag.Values.ContainsKey("ColumnDelimiter"))
            {
                // OLD Workbook => Move ColumnDelimiter down to ParserSettings.
                temp = bag.Values["ColumnDelimiter"];
                bag.Values.Remove("ColumnDelimiter");
                parserSettingsBag.Values["ColumnDelimiter"] = temp;
            }
            if (bag.Values.ContainsKey("CustomColumnDelimiter"))
            {
                // OLD Workbook => Move CustomColumnDelimiter down to ParserSettings.
                temp = bag.Values["CustomColumnDelimiter"];
                bag.Values.Remove("CustomColumnDelimiter");
                parserSettingsBag.Values["CustomColumnDelimiter"] = temp;
            }
            if (bag.Values.ContainsKey("TextQualifier"))
            {
                // OLD Workbook => Move TextQualifier down to ParserSettings.
                temp = bag.Values["TextQualifier"];
                bag.Values.Remove("TextQualifier");
                parserSettingsBag.Values["TextQualifier"] = temp;
            }
            if (bag.Values.ContainsKey("IsFirstRowHeading"))
            {
                // OLD Workbook => Move IsFirstRowHeading down to ParserSettings.
                temp = bag.Values["IsFirstRowHeading"];
                bag.Values.Remove("IsFirstRowHeading");
                parserSettingsBag.Values["IsFirstRowHeading"] = temp;
            }
        }

        public override bool CanGenerateColumns()
        {
            return IsPathOk() && IsColumnDelimiterOk;
        }

        private bool IsColumnDelimiterOk
        {
            get
            {
                if (((TextFileParserSettings)ParserSettings).ColumnDelimiter == DropDownDataProvider.OtherColumnDelimiterValue)
                {
                    if (string.IsNullOrEmpty(((TextFileParserSettings)ParserSettings).CustomColumnDelimiter))
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        public override void DoGenerateColumns()
        {            
            StreamReader stream = null;
            try
            {
                stream = Utils.GetStreamAndSkipRows(this, Parameters, this.FilePath);
            }
            catch (Exception e)
            {
                this.errorReporter.Report("Creating stream", e.Message);
            }
            if (stream == null) return;

            Utils.DoColumnDiscovery(stream, this, true);
            stream.Close();
        }

        public override PathType FilePathType
        {
            get
            {
                string s = GetInternal("TextFilePathType");
                if (s != null)
                {
                    try
                    {
                        return (PathType)Enum.Parse(
                            typeof(PathType), s);
                    }
                    catch
                    {
                    }
                }
                return PathType.File;
            }

            set
            {
                SetInternal("TextFilePathType", value.ToString());
            }
        }

        public virtual bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        protected override string DefaultParserPluginId
        {
            get { return Csv; }
        }

        public int DataDiscoveryRowCount
        {
            get
            {
                return GetInternalInt("DataDiscoveryRowCount", 10);
            }

            set
            {
                SetInternalInt("DataDiscoveryRowCount", value);
            }
        }

        public int[] DataTypeDiscoveryRows
        {
            get
            {
                return DropDownDataProvider.DataTypeDiscoveryRows;
            }
        }

        public override string Title
        {
            get
            {
                string title = Resources.UiDefaultDataTableName;

                if (FilePathType == PathType.File)
                {
                    if (FilePath != null)
                    {
                        title = Path.GetFileNameWithoutExtension(FilePath);
                    }
                }
                else if (FilePathType == PathType.Text)
                {
                    title = Resources.UiDefaultDataTableNameText;
                }
                else 
                {
                    if (WebPath != null)
                    {
                        try
                        {
                            Uri uri = new Uri(WebPath);
                            title = Path.GetFileNameWithoutExtension(uri.LocalPath);
                        }
                        catch (Exception ex)
                        {
                            Log.Exception(ex);
                        }
                    }
                }
                return title;
            }

            set
            {
                base.Title = value;
            }
        }

        /// <summary>
        /// Gets a value indicating wether the current JsonSettings 
        /// instance can be okeyed.
        /// </summary>
        public bool IsOK
        {
            get
            {
                if (!IsParserSettingsOk)
                {
                    return false;
                }
                if (!IsColumnDelimiterOk)
                {
                    return false;
                }
                if (!IsAllColumnIndexOk)
                {
                    return false;
                }
                return IsPathOk();
            }
        }

        private bool IsAllColumnIndexOk
        {
            get
            {
                HashSet<int> indexes = new HashSet<int>();
                foreach (TextFileColumnDefinition colDef in ParserSettings.Columns)
                {
                    if (indexes.Contains(colDef.Index))
                    {
                        return false;
                    }
                    indexes.Add(colDef.Index);
                }
                return true;
            }
        }

        public Version Version
        {
            get
            {
                return new Version(GetInternal(VersionProperty, "1.0.0.0"));
            }
            //            private set
            set
            {
                SetInternal(VersionProperty, value.ToString());
            }
        }

        public int SkipRows
        {
            get { return GetInternalInt("SkipRows", 0); }
            set { SetInternalInt("SkipRows", value);}
        }

        public int[] AvailableSkipRows
        {
            get
            {
                int[] availableSkipRows = new int[11];
                for (int i = 0; i < availableSkipRows.Length; i++)
                {
                    availableSkipRows[i] = i;
                }
                return availableSkipRows;
            }
        }

        public override bool ShowFilter
        {
            get
            {
                return false;
            }
        }

    }
}