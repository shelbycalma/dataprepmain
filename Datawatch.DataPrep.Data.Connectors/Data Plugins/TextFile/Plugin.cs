using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.TextFilePlugin.Properties;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;

namespace Panopticon.TextFilePlugin
{
    /// <summary>
    /// DataPlugin for TextFile.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : TextPluginBase<TextFileSettings>,
        IDataReaderPlugin
    {
        internal const string PluginId = "TEXT";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Text";
        internal const string PluginType = DataPluginTypes.File;

        /// <summary>
        /// Creates a new DataPlugin instance.
        /// </summary>
        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override ITable GetData(string workbookDir, string dataDir,
            PropertyBag propertyBagSettings,
            IEnumerable<ParameterValue> parameters, int maxRowCount,
            bool applyRowFilteration)
        {
            CheckLicense();

            TextFileSettings settings =
                new TextFileSettings(pluginManager, propertyBagSettings, parameters);

            TextFileParserSettings parserSettings = (TextFileParserSettings)settings.ParserSettings;


            //Handle File Path
            string fixedFilePath = null;
            if (settings.FilePathType == PathType.File)
            {
                fixedFilePath = DataUtils.FixFilePath(workbookDir, dataDir,
                    settings.FilePath, parameters);
                if (!settings.IsFilePathParameterized)
                {
                    settings.FilePath = fixedFilePath;
                }
            }

            StandaloneTable table = new StandaloneTable();

            DateTime start = DateTime.Now;

            StreamReader stream = Utils.GetStreamAndSkipRows(settings,
                parameters, fixedFilePath);
            if (stream == null) return table;

            table.BeginUpdate();
            try
            {
                TextFileParser parser;
                ParameterFilter parameterFilter = null;
                if (settings.Version.Major < 2)
                {
                    // Old workbook! => Create table doing data discovery and stuff.
                    List<string[]> readRows = Utils.DoColumnDiscovery(stream, settings, false);
                    if (readRows == null) return table;

                    settings.Version = TextFileSettings.Version_2_0_0_0;
                    DataPluginUtils.AddColumns(table, parserSettings.Columns);
                    if (applyRowFilteration)
                    {
                        parameterFilter = ParameterFilter.CreateFilter(table,
                            parameters);
                    }
                    parser = (TextFileParser)parserSettings.CreateParser();

                    // Add the so far read rows
                    foreach (string[] row in readRows)
                    {
                        if (maxRowCount > -1 && table.RowCount >= maxRowCount) break;

                        Dictionary<string, object> rowDict =
                            CreateRowDict(parserSettings, row);
                        if (rowDict.Count > 0)
                        {
                            DataPluginUtils.AddRow(rowDict, table,
                                settings.NumericDataHelper, parameterFilter, 
                                settings.ParserSettings);
                        }
                    }

                }
                else
                {
                    // New Workbook => ColumnDefinition could be found
                    DataPluginUtils.AddColumns(table, parserSettings.Columns);
                    if (applyRowFilteration)
                    {
                        parameterFilter = ParameterFilter.CreateFilter(table, parameters);
                    }
                    parser = (TextFileParser)parserSettings.CreateParser();
                    if (((TextFileParserSettings)settings.ParserSettings).IsFirstRowHeading)
                    {
                        // Read the expected header row, so we wont get it in the data
                        stream.ReadLine();
                    }
                }
                // Read and add the rest of the rows
                Utils.ReadAndAddRows(table, stream, parser, settings.NumericDataHelper,
                    parameterFilter, parserSettings, maxRowCount);

                DateTime end = DateTime.Now;
                double seconds = (end - start).TotalSeconds;
                Log.Info(Resources.LogQueryResultSizeAndTime,
                    table.RowCount, table.ColumnCount, seconds);

                // We're creating a new StandaloneTable on every call, so allow
                // EX to freeze it.
                table.CanFreeze = true;
            }
            finally
            {
                table.EndUpdate();
                stream.Close();
            }
            return table;
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            return DataPluginUtils.GetDataFiles(
                new TextFileSettings(pluginManager, settings, null));
        }

        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            CheckLicense();

            DataPluginUtils.UpdateFileLocation(
                new TextFileSettings(pluginManager, settings, null),
                oldPath, newPath, workbookPath);
        }

        public override TextFileSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            TextFileSettings settings = 
                new TextFileSettings(pluginManager, parameters);
            return settings;
        }

        public override TextFileSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new TextFileSettings(pluginManager, bag, null);
        }

        public IDataReaderWithSchema GetDataReader(
            string workbookDir, string dataDir, PropertyBag settingsBag,
            IEnumerable<ParameterValue> parameters, int maxRowCount)
        {
            CheckLicense();

            TextFileSettings textFileSettings =
                new TextFileSettings(pluginManager, settingsBag, null);

            //Handle File Path
            string fixedFilePath = null;
            if (textFileSettings.FilePathType == PathType.File)
            {
                fixedFilePath = DataUtils.FixFilePath(workbookDir, dataDir,
                    textFileSettings.FilePath, parameters);
                if (!textFileSettings.IsFilePathParameterized)
                {
                    textFileSettings.FilePath = fixedFilePath;
                }
            }

            return new TextFileDataReader(
                textFileSettings, parameters, maxRowCount);
        }

        public static Dictionary<string, object> CreateRowDict(TextFileParserSettings parserSettings,
            string[] values)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();

            int index;
            foreach (ColumnDefinition def in parserSettings.Columns)
            {
                TextFileColumnDefinition textColDef = (TextFileColumnDefinition)def;

                index = textColDef.Index;

                if (index >= values.Length) break;

                if (!string.IsNullOrEmpty(values[index]))
                {
                    dict[textColDef.Name] = values[index];
                }
            }
            return dict;
        }
    }
}

