﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.OnDemandProcessor;
using Datawatch.DataPrep.Data.Core.OnDemand;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.ActivePivot.QODPlugin.MDX;
using Panopticon.ActivePivot.QODPlugin.XMLA;
using BinaryOperator = Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast.BinaryOperator;

namespace Panopticon.ActivePivot.QODPlugin
{
    public class ActivePivotExecutor
    {
        private OnDemandSchemaTableCache onDemandSchemaTableCache = new OnDemandSchemaTableCache();
        public ITable GetData(
            XMLAProvider provider,
            ActivePivotConnectionSettings settings,
            IEnumerable<ParameterValue> parameters,
            OnDemandParameters qod,
            int maxRowCount)
        {
            ITable table;
            using (IDbConnection connection = provider.Connect(settings)) {
                if (qod == null || qod.IsEmpty) {
                    table = provider.GetSchema(connection, settings, onDemandSchemaTableCache);
                } else {
                    string[] domains = qod.Domains;
                    qod.Domains = null;
                    if (!qod.IsEmpty) {
                        table = provider.Execute(connection, settings, qod, onDemandSchemaTableCache);
                    } else {
                        table = provider.GetDomain(
                            connection, settings, domains.Single());
                    }
                }
            }
            return table;
        }

        public static QueryBuilder BuildOnDemandQuery(
            ActivePivotConnectionSettings settings, 
            OnDemandParameters onDemandParameters)
        {
            QueryBuilder queryBuilder = new QueryBuilder(settings);

            queryBuilder.FromNode = new CubeNode(settings.CubeName);

            if (onDemandParameters == null || onDemandParameters.IsEmpty)
            {
                //queryBuilder.Where.Predicate = new Comparison(Operator.Equals,
                //    new ValueParameter(1), new ValueParameter(0));
                return queryBuilder;
            }


            queryBuilder.Axises.Add(new AxisNode(0, false, new SetNode(new List<TupleNode>())));

            CreateBreakdownAxis(queryBuilder, onDemandParameters);


            if (onDemandParameters.Aggregates != null &&
                onDemandParameters.Aggregates.Length > 0)
            {
                foreach (OnDemandAggregate aggregate 
                    in onDemandParameters.Aggregates)
                {
                    queryBuilder.AddAggregate(new AggregateItemNode(
                        aggregate.FunctionType,
                        aggregate.Alias ?? aggregate.Key,
                        aggregate.Columns));
                }
            }

            if (onDemandParameters.AuxiliaryColumns != null &&
                onDemandParameters.AuxiliaryColumns.Length > 0)
            {
                foreach (string column in
                    onDemandParameters.AuxiliaryColumns)
                {
                    if (!onDemandParameters.BreakdownColumns.Contains(column))
                    {
                        //DDTV-5730 only add when AuxiliaryColumn is not in breakdown
                        queryBuilder.AddColumnDimension(new Dimension
                        {
                            UniqueName = MDXUtils.GetHierarchyName(column)
                        });
                    }
                }
            }

            if (onDemandParameters.Bucketings != null &&
                onDemandParameters.Bucketings.Length > 0)
            {
                foreach (Bucketing bucketing in onDemandParameters.Bucketings)
                {
                    if (!(bucketing is TimeBucketing))
                    {
                        continue;
                    }
                    TimeBucketing timeBucketing = (TimeBucketing)bucketing;
                    queryBuilder.AddColumnDimension(new Dimension
                    {
                        UniqueName = MDXUtils.GetHierarchyName(timeBucketing.SourceColumnName)
                    });
                }
            }

            //if (onDemandParameters.Aggregates != null &&
            //    onDemandParameters.Aggregates.Length > 0)
            //{
            //    foreach (OnDemandAggregate aggregate in
            //        onDemandParameters.Aggregates)
            //    {
            //        queryBuilder.Select.AddAggregate(
            //            aggregate.Columns,
            //            aggregate.FunctionType,
            //            aggregate.Alias != null ?
            //            aggregate.Alias : aggregate.Key);
            //    }
            //}

            //if (onDemandParameters.AuxiliaryColumns != null &&
            //    onDemandParameters.AuxiliaryColumns.Length > 0)
            //{
            //    foreach (string column in
            //        onDemandParameters.AuxiliaryColumns)
            //    {
            //        queryBuilder.Select.AddColumn(column);
            //        queryBuilder.GroupBy.AddColumn(column);
            //    }
            //}

            //if (onDemandParameters.Bucketings != null &&
            //    onDemandParameters.Bucketings.Length > 0)
            //{
            //    foreach (Dashboards.Model.Bucketing bucketing in onDemandParameters.Bucketings)
            //    {
            //        if (!(bucketing is TimeBucketing)) continue;
            //        TimeBucketing timeBucketing = (TimeBucketing)bucketing;
            //        queryBuilder.Select.AddTimePart(
            //            timeBucketing.SourceColumnName,
            //            timeBucketing.TimePart,
            //            timeBucketing.Name);
            //        queryBuilder.GroupBy.AddTimePart(
            //            timeBucketing.SourceColumnName,
            //            timeBucketing.TimePart,
            //            timeBucketing.Name);
            //    }
            //}

            Predicate predicate = null;

            if (onDemandParameters.Predicates != null &&
                onDemandParameters.Predicates.Length > 0)
            {
                foreach (Predicate p in onDemandParameters.Predicates)
                {
                    predicate = new AndOperator(predicate, p);
                }
            }

            QueryOnDemandHelper queryOnDemandHelper = new QueryOnDemandHelper();
            IList<IColumnModel> includedColumns = queryOnDemandHelper
                .GetIncludedColumns(onDemandParameters);
            IList<IColumnModel> excludedColumns = queryOnDemandHelper
                .GetOnDemandExclusivePredicates(onDemandParameters);

            if (onDemandParameters.DrillPath != null &&
                onDemandParameters.DrillPath.Length > 0 &&
                onDemandParameters.BreakdownColumns != null &&
                onDemandParameters.DrillPath.Length <=
                    onDemandParameters.BreakdownColumns.Length)
            {
                for (int i = 0; i < onDemandParameters.DrillPath.Length; i++)
                {
                    includedColumns.Add(
                        new ColumnValuesModel(MDXUtils.GetHierarchyName(onDemandParameters.BreakdownColumns[i]),
                        new List<string> { onDemandParameters.DrillPath[i] }));
                }
            }
            
            
            foreach (IColumnModel columnModel in includedColumns)
            {
                if (columnModel is ColumnValuesModel)
                {
                    var columnValuesModel = (ColumnValuesModel)columnModel;
                    Hierarchy elementBase = new Hierarchy();
                    elementBase.UniqueName = columnValuesModel.ColumnName;
                    var allColumns = columnValuesModel.ColumnValues.Select(s =>
                        new ComparisonMemberNode(
                                new HierarchyMemberNode(columnValuesModel.ColumnName), s)).ToList();
                    for (int i = 1; i <= onDemandParameters.Depth; i++)
                    {
                        var parentColumns = columnValuesModel.ColumnValues.Select(s =>
                            new ComparisonMemberNode(
                                new HierarchyParentMemberNode(columnValuesModel.ColumnName, i), s));
                        allColumns.AddRange(parentColumns);
                        
                    }
                    queryBuilder.AddFilter(elementBase,
                            new ExpressionNodeCollection(allColumns, BinaryOperator.OrOperator));
                }
                else if (columnModel is ColumnRangeModel)
                {
                    var columnRangeModel = (ColumnRangeModel)columnModel;
                    Measure elementBase = new Measure();
                    elementBase.UniqueName = columnRangeModel.ColumnName;
                    queryBuilder.AddNumericFilter(new ExpressionNode[]
                    {
                        new ComparisonMemberNode(new HierarchyWithMeasureFilterMemberNode(columnRangeModel.ColumnName),
                        columnRangeModel.Value.ToString(), columnRangeModel.ComparisonOperator, false)
                    });
                    //queryBuilder.AddFilter(elementBase, , BinaryOperator.OrOperator);
                }

            }
            foreach (IColumnModel columnModel in excludedColumns)
            {
                if (columnModel is ColumnValuesModel)
                {
                    var columnValuesModel = (ColumnValuesModel)columnModel;
                    Hierarchy elementBase = new Hierarchy();
                    elementBase.UniqueName = columnValuesModel.ColumnName;
                    ColumnValuesModel model = columnValuesModel;
                    queryBuilder.AddFilter(elementBase,  new ExpressionNodeCollection( columnValuesModel.ColumnValues.Select(s =>
                        new ComparisonMemberNode(
                                new HierarchyMemberNode(model.ColumnName), s, ComparisonOperator.NotEqual))));
                }
            }            

            
            return queryBuilder;
        }

        private static void CreateBreakdownAxis(QueryBuilder queryBuilder, 
            OnDemandParameters onDemandParameters)
        {
            if (onDemandParameters.BreakdownColumns == null) return;

            List<AxisItemNode> breakdownAxisItems = new List<AxisItemNode>();
            var hierarchies = onDemandParameters.BreakdownColumns.Select(MDXUtils.GetHierarchyName).Distinct();
            foreach (var hierarchy in hierarchies)
            {
                breakdownAxisItems.Add(new HierarchizeFunctionNode(new HierarchyMemberNode(hierarchy)));
            }
            if (breakdownAxisItems.Count == 0) return;

            AxisNode axis = new AxisNode(1, false,
                breakdownAxisItems.Count == 1
                    ? breakdownAxisItems[0]
                    : new CrossJoinNode(breakdownAxisItems));

            queryBuilder.Axises.Add(axis);
        }
    }
}
