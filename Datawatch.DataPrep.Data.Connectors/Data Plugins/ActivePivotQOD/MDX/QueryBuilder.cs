﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.MDX;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Ast;
using Datawatch.DataPrep.Data.Core.Connectors.MDX.Metadata;

namespace Panopticon.ActivePivot.QODPlugin.MDX
{
    public class QueryBuilder : AbstractQueryBuilder
    {
        public QueryBuilder(IMdxSettings settings): base(settings)
        {
            Axises = new List<AxisNode>();
        }

        public override string ToExpression()
        {
            StringBuilder result = new StringBuilder();
            if (columnMeasures.Count > 0 || columnDimensions.Count > 0)
            {
                var columnsNode = GenerateAxisItemNode(this.columnMeasures,
                    this.columnDimensions, null);
                var columnsAxisNode = new AxisNode(0, false, columnsNode);
                //Remove all default axes
                Axises = Axises.Where(node => node.AxisIndex != 0).ToList();
                Axises.Add(columnsAxisNode);
            }
            if (rows.Count > 0 || filters.Count > 0 || numericFilters.Count > 0)
            {

                if (numericFilters.Count > 0 && rows.Count == 0)
                {
                    if (filters.Count == 0)
                    {
                        var rowNode = Axises.First(node => node.AxisIndex == 1).Node;
                        var rowHierarchy = rowNode as HierarchizeFunctionNode;
                        if (rowHierarchy != null)
                        {
                            AddFilter(new Hierarchy
                            {
                                UniqueName = ((MemberNode)rowHierarchy.Node).FullName
                            }, new ExpressionNodeCollection(numericFilters, BinaryOperator.AndOperator));
                            rows.Clear();
                        }
                        else
                        {
                            var crossJoinHierarhy = rowNode as FunctionAxisItemNode;
                            if (crossJoinHierarhy != null)
                            {
                                var query = crossJoinHierarhy.ToExpression();
                                AddFilter(new MemberNode(query),
                                    new ExpressionNodeCollection(numericFilters, BinaryOperator.AndOperator));
                                rows.Clear();
                            }
                        }
                    }
                    else
                    {
                        foreach (var filterFunctionNode in filters)
                        {
                            filterFunctionNode.Expressions
                                .Add(new ExpressionNodeCollection(numericFilters, BinaryOperator.AndOperator));
                        }
                    }
                }
                var rowsNode = GenerateAxisItemNode(null, rows, filters);
                if (this.rowsParameters.Count > 0)
                {
                    rowsNode = new FilterFunctionNode(rowsNode, new List<ExpressionNodeCollection>
                    {
                        new ExpressionNodeCollection(rowsParameters)
                    });
                }
                var rowsAxisNode = new AxisNode(1, false, rowsNode);
                //Remove all default axis
                Axises = Axises.Where(node => node.AxisIndex != 1).ToList();
                if (!string.IsNullOrEmpty(rowsNode.ToExpression()))
                {
                    Axises.Add(rowsAxisNode);
                }
            }

            if (aggregates.Count > 0)
            {
                result.Append(TokensHelper.WITH);
                result.Append(TokensHelper.SPACE);
                foreach (AggregateItemNode aggregate in aggregates)
                {
                    result.Append(aggregate.ToExpression());
                    result.Append(TokensHelper.SPACE);
                }
            }

            result.Append(base.ToExpression());
            var stringResult = result.ToString();
            return stringResult;
        }

        //TODO this must be watched closely
        //Probably could unify that all within one method
        private static AxisItemNode GenerateAxisItemNode(IList<AxisItemNode> measures,
            IList<AxisItemNode> dimensions, IList<FilterFunctionNode> filters)
        {
            if (measures != null && measures.Count > 0 && dimensions != null && dimensions.Count > 0)
            {
                return GenerateComplexAxisNode(measures, dimensions, filters);
            }
            if (measures != null && measures.Count > 0)
            {
                return GenerateMeasuresSet(measures);
            }
            if (filters != null && filters.Count > 0)
            {
                return GenerateFilterSet(filters);
            }
            return GenerateHierarchizeNode(dimensions);
        }

        private static AxisItemNode GenerateComplexAxisNode(IList<AxisItemNode> measures,
            IList<AxisItemNode> dimensions,
            IList<FilterFunctionNode> filters)
        {
            List<AxisItemNode> unions = measures
                .Select(m => new UnionNode(new[] { m }.Concat(dimensions)))
                .ToList<AxisItemNode>();
            if (filters != null && filters.Any())
            {
                unions.AddRange(filters);
            }
            if (unions.Count == 1) return unions[0];
            return new HierarchizeFunctionNode(new UnionNode(unions));
        }

        private static AxisItemNode GenerateMeasuresSet(IList<AxisItemNode> measures)
        {
            if (measures.Count == 0)
            {
                //TODO just return new empty item node
                //should solve previous issues
                return null;
            }
            if (measures.Count > 1)
            {
                return new SetNode(
                    measures.Select(m => new TupleNode(new[] { m })));
            }
            else
            {
                return measures.First();
            }

        }

        private static AxisItemNode GenerateHierarchizeNode(IList<AxisItemNode> dimensions)
        {
            return new HierarchizeFunctionNode(new TupleNode(dimensions.Select(d => new TupleNode(new[] { d }))));
        }

        private static AxisItemNode GenerateFilterSet(IList<FilterFunctionNode> filters)
        {
            if (filters.Count == 0)
            {
                //Similar to GenerateMeasuresSet
                return null;
            }
            if (filters.Count > 1)
            {
                return new CrossJoinNode(filters);
            }
            else
            {
                return filters.First();
            }
        }
    }
}