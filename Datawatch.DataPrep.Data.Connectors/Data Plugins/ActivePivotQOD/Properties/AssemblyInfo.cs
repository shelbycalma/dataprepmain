﻿using Datawatch.DataPrep.Data.Framework.Plugin;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Panopticon.ActivePivot.QODPlugin;

[assembly: AssemblyTitle("Panopticon.ActivePivot.QODPlugin.dll")]
[assembly: AssemblyDescription("ActivePivot QOD Plug-in")]
[assembly: AssemblyConfiguration("")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("042680f0-d0b8-431c-b75a-96f689fe9ca7")]
