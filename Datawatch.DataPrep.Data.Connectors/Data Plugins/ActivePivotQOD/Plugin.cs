﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;
using Panopticon.ActivePivot.QODPlugin.Properties;
using Panopticon.ActivePivot.QODPlugin.XMLA;

namespace Panopticon.ActivePivot.QODPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : DataPluginBase<ActivePivotConnectionSettings>,
        IAggregatingOnDemandPlugin
    {
        internal const string PluginId = "ActivePivotQODPlugin";
        internal const string PluginTitle = "ActivePivot QOD";
        internal const string PluginType = DataPluginTypes.Database;
        internal const bool PluginIsObsolete = false;

        private readonly ActivePivotExecutor executor = new ActivePivotExecutor();

        public override ITable GetData(
            string workbooksFolder, string dataFolder,
            PropertyBag connectionBag, IEnumerable<ParameterValue> parameters,
            int maxRowCount, bool applyRowFilteration)
        {
            throw new NotImplementedException();
        }

        public ITable GetOnDemandData(
            string workbooksFolder, string dataFolder,
            PropertyBag connectionBag, IEnumerable<ParameterValue> parameters,
            OnDemandParameters qod, int maxRowCount)
        {
            CheckLicense();

            ActivePivotConnectionSettings connection =
                new ActivePivotConnectionSettings(connectionBag);

            XMLAProvider provider = new XMLAProvider(parameters);
            return executor.GetData(provider,
                connection, parameters, qod, maxRowCount);
        }

        #region IAggregatingOnDemandPlugin Plumbing

        public string GetPartitionValue(PropertyBag connectionBag)
        {
            return ActivePivotConnectionSettings.GetPartitionValue(connectionBag);
        }

        public bool IncludesAggregateData(PropertyBag connectionBag)
        {
            return ActivePivotConnectionSettings.GetIsAggregated(connectionBag);
        }

        #endregion // IAggregatingOnDemandPlugin Plumbing

        #region IOnDemandPlugin Plumbing

        public bool IsOnDemand(PropertyBag connectionBag)
        {
            return ActivePivotConnectionSettings.GetIsOnDemand(connectionBag);
        }

        #endregion // IOnDemandPlugin Plumbing

        #region DataPluginBase Plumbing

        public override ActivePivotConnectionSettings CreateSettings(
            PropertyBag connectionBag)
        {
            return new ActivePivotConnectionSettings(connectionBag);
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }

        #endregion // DataPluginBase Plumbing
    }
}
