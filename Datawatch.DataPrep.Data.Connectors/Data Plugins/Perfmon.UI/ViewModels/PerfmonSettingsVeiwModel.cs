﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.ViewModel;
using Panopticon.PerfmonPlugin.UI.Properties;

namespace Panopticon.PerfmonPlugin.UI.ViewModels
{
    internal class PerfmonSettingsVeiwModel : ViewModelBase
    {
        private const int MaxCounters = 1024;

        private readonly ObservableCollection<InstanceViewModel> instances =
            new ObservableCollection<InstanceViewModel>();
        private string description;
        private ICommand removeCountersCommand;
        private readonly ObservableCollection<AddedCounterViewModel> addedCounters =
            new ObservableCollection<AddedCounterViewModel>();
        private readonly ObservableCollection<CategoryViewModel> allCategories =
            new ObservableCollection<CategoryViewModel>();

        public static RoutedCommand ConnectCommand =
            new RoutedCommand("Connect", typeof(PerfmonSettingsVeiwModel));

        public static RoutedCommand AddCountersCommand =
            new RoutedCommand("AddCounters", typeof(PerfmonSettingsVeiwModel));

        private readonly IEnumerable<ParameterValue> parameters;

        public PerfmonSettingsVeiwModel(PerfmonSettings settings,
            IEnumerable<ParameterValue> parameters)
        {
            Settings = settings;

            if (string.IsNullOrWhiteSpace(Settings.MachineName))
            {
                Settings.MachineName = Environment.MachineName;
            }

            this.parameters = parameters;

            AddedCounters.CollectionChanged += AddedCounters_CollectionChanged;
            foreach (Counter counter in Settings.Counters)
            {
                AddedCounters.Add(new AddedCounterViewModel(
                    counter.DisplayName,
                    counter.CategoryName,
                    counter.CounterName,
                    counter.InstanceName,
                    counter.MachineName));
            }
        }

        private void AddedCounters_CollectionChanged(object sender,
            NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (AddedCounterViewModel c in e.NewItems)
                    {
                        c.PropertyChanged += AddedCounter_PropertyChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (AddedCounterViewModel c in e.OldItems)
                    {
                        c.PropertyChanged -= AddedCounter_PropertyChanged;
                    }
                    break;
            }
        }

        private void AddedCounter_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "DisplayName")
            {
                SetCountersToModel();
            }
        }

        public PerfmonSettings Settings { get; private set; }

        public string MachineName
        {
            get
            {
                return Settings.MachineName;
            }
            set
            {
                if (Settings.MachineName == value) return;
                Settings.MachineName = value;
                AllCategories.Clear();
                Instances.Clear();
                Description = null;
                OnPropertyChanged("MachineName");
            }
        }

        public ObservableCollection<CategoryViewModel> AllCategories
        {
            get { return allCategories; }
        }

        public string Description
        {
            get { return description; }
            set
            {
                if (description == value) return;
                description = value;
                OnPropertyChanged("Description");
            }
        }

        public ObservableCollection<InstanceViewModel> Instances
        {
            get { return instances; }
        }

        public ObservableCollection<AddedCounterViewModel> AddedCounters
        {
            get { return addedCounters; }
        }

        public int TimeWindowSeconds
        {
            get
            {
                return Settings.TimeWindowSeconds;
            }
            set
            {
                if (Settings.TimeWindowSeconds == value) return;
                Settings.TimeWindowSeconds = value;
                OnPropertyChanged("TimeWindowSeconds");
            }
        }

        public int Limit
        {
            get
            {
                return Settings.Limit;
            }
            set
            {
                if (Settings.Limit == value) return;
                Settings.Limit = value;
                OnPropertyChanged("Limit");
            }
        }

        public bool IsValid
        {
            get
            {
                return AddedCounters.Count > 0 &&
                       Limit > 0 &&
                       AddedCounters.Select(c => c.DisplayName).Distinct().Count() ==
                       AddedCounters.Count;
            }
        }

        public void RefreshCategories()
        {
            AllCategories.Clear();

            IEnumerable<CategoryViewModel> newCategories;
            try
            {
                newCategories =
                    Task<IEnumerable<CategoryViewModel>>
                        .Factory.StartNew(CreateAllCounters).Result;
            }
            catch (AggregateException ae)
            {
                throw ae.Flatten().InnerException;
            }
            
            foreach (CategoryViewModel categoryViewModel in newCategories)
            {
                AllCategories.Add(categoryViewModel);
            }
        }

        public bool CanConnect
        {
            get
            {
                return !string.IsNullOrWhiteSpace(MachineName);
            }   
        }

        public void AddCounters()
        {
            List<AddedCounterViewModel> result = new List<AddedCounterViewModel>();

            IList<InstanceViewModel> selectedInstances =
                Instances.Where(i => i.IsSelected).ToList();

            IList<CategoryViewModel> selectedCategories = AllCategories.Where(
                category => category.Counters.Any(counter => counter.IsSelected))
                .ToList();

            foreach (CategoryViewModel categoryViewModel in selectedCategories)
            {
                foreach (CounterViewModel counterViewModel in
                    categoryViewModel.Counters
                    .Where(counter => counter.IsSelected))
                {
                    if (selectedCategories.Count > 1)
                    {
                        result.AddRange(
                            GetAddedCountersForMultipleCategorySelection(
                                categoryViewModel, counterViewModel));
                    }
                    else
                    {
                        result.AddRange(GetAddedCountersForSingleCategorySelection(
                            categoryViewModel, counterViewModel, selectedInstances));
                    }
                }
            }

            foreach (AddedCounterViewModel addedCounter in result)
            {
                if (AddedCounters.SingleOrDefault(c =>
                        c.CategoryName == addedCounter.CategoryName &&
                        c.CounterName == addedCounter.CounterName &&
                        c.InstanceName == addedCounter.InstanceName &&
                        c.MachineName == addedCounter.MachineName) == null)
                {
                    if (AddedCounters.Count == MaxCounters)
                    {
                        SetCountersToModel();
                        throw new InvalidOperationException(
                            string.Format(Resources.ExMaxCounterNumber, MaxCounters));
                    }
                    AddedCounters.Add(addedCounter);
                }
            }

            SetCountersToModel();
        }

        private void SetCountersToModel()
        {
            Settings.Counters = AddedCounters.Select(addedCounter =>
                new Counter(addedCounter.DisplayName,
                            addedCounter.CategoryName,
                            addedCounter.CounterName,
                            addedCounter.InstanceName,
                            addedCounter.MachineName)).ToArray();
        }

        public bool CanAddCounters
        {
            get
            {
                return AllCategories.Any(category => category.Counters
                    .Any(counter => counter.IsSelected));
            }
        }

        private IEnumerable<AddedCounterViewModel>
            GetAddedCountersForMultipleCategorySelection(
                CategoryViewModel categoryViewModel,
                CounterViewModel counterViewModel)
        {
            List<AddedCounterViewModel> result = new List<AddedCounterViewModel>();
            if (categoryViewModel.Instances.Any())
            {
                result.AddRange(GetAddedCountersForAllInstances(
                    categoryViewModel, counterViewModel));
            }
            else
            {
                result.Add(new AddedCounterViewModel(null,
                    categoryViewModel.CategoryName,counterViewModel.CounterName,
                    null, MachineName));
            }

            return result;
        }

        private IEnumerable<AddedCounterViewModel>
            GetAddedCountersForSingleCategorySelection(
                CategoryViewModel categoryViewModel,
                CounterViewModel counterViewModel,
                IList<InstanceViewModel> selectedInstances)
        {
            List<AddedCounterViewModel> result = new List<AddedCounterViewModel>();

            if (Instances.Any())
            {
                if (!selectedInstances.Any() ||
                    selectedInstances.SingleOrDefault(i =>
                        i.InstanceName == Resources.UiAllInstances) != null)
                {
                    result.AddRange(GetAddedCountersForAllInstances(
                        categoryViewModel, counterViewModel));
                }
                else
                {
                    result.AddRange(selectedInstances.Select(instance =>
                        new AddedCounterViewModel(null,
                            categoryViewModel.CategoryName,
                            counterViewModel.CounterName,
                            instance.InstanceName,
                            MachineName)));
                }
            }
            else
            {
                result.Add(new AddedCounterViewModel(null,
                    categoryViewModel.CategoryName,
                    counterViewModel.CounterName, null, MachineName));
            }

            return result;
        }

        private IEnumerable<AddedCounterViewModel> GetAddedCountersForAllInstances(
            CategoryViewModel categoryViewModel, CounterViewModel counterViewModel)
        {
            return categoryViewModel.Instances.Where(i =>
                i.InstanceName != Resources.UiAllInstances).Select(instance =>
                    new AddedCounterViewModel(null,
                                              categoryViewModel.CategoryName,
                                              counterViewModel.CounterName,
                                              instance.InstanceName,
                                              MachineName)).ToList();
        }

        public ICommand RemoveCountersCommand
        {
            get
            {
                return removeCountersCommand ??
                       (removeCountersCommand = new DelegateCommand(
                           RemoveCounters, CanRemoveCounters));
            }
        }

        public void RemoveCounters()
        {
            IList<AddedCounterViewModel> addedCounterViewModelsToDelete =
                AddedCounters.Where(addedCounterViewModel =>
                    addedCounterViewModel.IsSelected).ToList();

            foreach (AddedCounterViewModel addedCounterViewModel
                in addedCounterViewModelsToDelete)
            {
                AddedCounters.Remove(addedCounterViewModel);
            }

            SetCountersToModel();
        }

        private bool CanRemoveCounters()
        {
            return AddedCounters.Any(c => c.IsSelected);
        }

        public IEnumerable<CategoryViewModel> CreateAllCounters()
        {
            List<CategoryViewModel> result = new  List<CategoryViewModel>();

            IEnumerable<PerformanceCounterCategory> allNewCategories =
                PerformanceCounterCategory.GetCategories(
                    DataUtils.ApplyParameters(MachineName, parameters))
                .OrderBy(c => c.CategoryName);

            foreach (PerformanceCounterCategory category in allNewCategories)
            {
                string categoryDescription =
                    CounterHelper.GetCategoryDescription(category);
                CategoryViewModel categoryViewModel =
                    new CategoryViewModel(category.CategoryName, categoryDescription);

                IList<string> instanceNames =
                    CounterHelper.GetCategoryInstances(category);

                IList<PerformanceCounter> counters = null;

                if (instanceNames!= null && instanceNames.Any())
                {
                    counters = CounterHelper.GetMultiInstanceCounters(
                        category, instanceNames);

                    if (counters != null && counters.Any())
                    {
                        if (instanceNames.Count > 1)
                        {
                            categoryViewModel.Instances.Add(
                                new InstanceViewModel(Resources.UiAllInstances));
                        }
                        categoryViewModel.Instances.AddRange(
                            instanceNames.Select(i =>
                                new InstanceViewModel(i)).OrderBy(i => i.InstanceName));
                    }
                }
                else
                {
                    PerformanceCounterCategoryType? categoryType =
                        CounterHelper.GetCategoryType(category);
                    if (categoryType.HasValue && categoryType.Value !=
                            PerformanceCounterCategoryType.MultiInstance)
                    {
                        counters = CounterHelper.GetSigleInstanceCounters(category);
                    }
                }

                if (counters == null || !counters.Any()) continue;

                IList<CounterViewModel> counterViewModels = counters
                    .Select(c => new CounterViewModel(c.CounterName,
                        CounterHelper.GetCounterDescription(c), categoryViewModel))
                    .OrderBy(c => c.CounterName)
                    .ToList();

                categoryViewModel.PropertyChanged += SelectableItem_PropertyChanged;
                foreach (CounterViewModel counterViewModel in counterViewModels)
                {
                    counterViewModel.PropertyChanged += SelectableItem_PropertyChanged;
                    categoryViewModel.Counters.Add(counterViewModel);
                }

                result.Add(categoryViewModel);
            }

            return result;
        }

        private void SelectableItem_PropertyChanged(
            object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != "IsSelected") return;

            IEnumerable<CategoryViewModel> selectedCategories = AllCategories.Where(
                category => category.Counters.Any(counter => counter.IsSelected)).ToList();

            Instances.Clear();
            if (selectedCategories.Count() > 1 || !selectedCategories.Any())
            {
                Description = null;
                return;
            }

            CategoryViewModel selectedCategoryViewModel = selectedCategories.Single();

            foreach (InstanceViewModel instance in selectedCategoryViewModel.Instances)
            {
                Instances.Add(instance);
            }

            IList<CounterViewModel> selectedCounters =
                selectedCategoryViewModel.Counters
                    .Where(counter => counter.IsSelected).ToList();

            Description = selectedCounters.Count() == 1 ?
                selectedCounters.Single().CounterDescription :
                selectedCategoryViewModel.CategoryDescription;
        }
    }
}
