﻿using System.Text;
using Datawatch.DataPrep.Data.Core.UI.DragDrop;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.PerfmonPlugin.UI.ViewModels
{
    internal class AddedCounterViewModel : ViewModelBase
    {
        private string displayName;
        private bool isSelected;

        public AddedCounterViewModel(string displayName,
            string categoryName, string counterName, string instanceName, string machineName)
        {
            CategoryName = categoryName;
            CounterName = counterName;
            InstanceName = instanceName;
            MachineName = machineName;

            if (displayName == null)
            {
                StringBuilder name = new StringBuilder();
                name.Append(categoryName)
                    .Append("_")
                    .Append(counterName)
                    .Append("_");

                if (instanceName != null)
                {
                    name.Append(instanceName).Append("_");
                }

                name.Append(machineName);
                DisplayName = name.ToString();
            }
            else
            {
                DisplayName = displayName;
            }
        }

        public string CategoryName { get; private set; }
        public string CounterName { get; private set; }
        public string InstanceName { get; private set; }
        public string MachineName { get; private set; }

        public new string DisplayName
        {
            get { return displayName; }
            set
            {
                if (displayName == value) return;
                displayName = value;
                OnPropertyChanged("DisplayName");
            }
        }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
    }

    internal class AddedCounterViewModelDragSourceAdvisor
        : ListBoxDragSourceAdvisor<AddedCounterViewModel>
    {
    }

    internal class AddedCounterViewModelDropTargetAdvisor
        : ListBoxDropTargetAdvisor<AddedCounterViewModel>
    {
    }
}
