﻿using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.PerfmonPlugin.UI.ViewModels
{
    internal class CounterViewModel : ViewModelBase, ISelectableItem
    {
        private bool isSelected;

        public CounterViewModel(string counterName,
            string counterDescription, CategoryViewModel category)
        {
            CounterName = counterName;
            CounterDescription = counterDescription;
            Category = category;
        }

        public string CounterName { get; private set; }
        public string CounterDescription { get; private set; }
        public CategoryViewModel Category { get; private set; }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                OnPropertyChanged("IsSelected");
                Category.UpdateIsSelected();
            }
        }
    }
}
