﻿using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.PerfmonPlugin.UI.ViewModels
{
    internal class InstanceViewModel : ViewModelBase
    {
        private bool isSelected;

        public InstanceViewModel(string instanceName)
        {
            InstanceName = instanceName;
        }

        public string InstanceName { get; private set; }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
    }
}
