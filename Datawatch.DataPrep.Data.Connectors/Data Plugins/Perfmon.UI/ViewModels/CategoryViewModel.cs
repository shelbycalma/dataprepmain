﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Framework.ViewModel;

namespace Panopticon.PerfmonPlugin.UI.ViewModels
{
    internal class CategoryViewModel : ViewModelBase, ISelectableItem
    {
        private readonly List<CounterViewModel> counters =
            new List<CounterViewModel>();

        private readonly List<InstanceViewModel> instances =
            new List<InstanceViewModel>();

        private bool isSelected;

        public CategoryViewModel(string categoryName, string categoryDescription)
        {
            CategoryName = categoryName;
            CategoryDescription = categoryDescription;
        }

        public string CategoryName { get; private set; }
        public string CategoryDescription { get; private set; }

        public List<InstanceViewModel> Instances { get { return instances; } }
        public List<CounterViewModel> Counters { get { return counters; } }

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected == value) return;
                isSelected = value;

                if (isSelected)
                {
                    counters.ForEach(counter => counter.IsSelected = true);
                }
                else if (counters.All(c => c.IsSelected))
                {
                    counters.ForEach(counter => counter.IsSelected = false);
                }

                OnPropertyChanged("IsSelected");
            }
        }

        public void UpdateIsSelected()
        {
            IsSelected = Counters.All(counter => counter.IsSelected);
        }
    }
}
