﻿namespace Panopticon.PerfmonPlugin.UI.ViewModels
{
    internal interface ISelectableItem
    {
        bool IsSelected { get; set; }
    }
}
