﻿using System.Windows.Input;
using Panopticon.PerfmonPlugin.UI.ViewModels;

namespace Panopticon.PerfmonPlugin.UI
{
    internal partial class ConfigWindow
    {
        public static RoutedCommand OkCommand
           = new RoutedCommand("Ok", typeof(ConfigWindow));

        private readonly PerfmonSettingsVeiwModel viewModel;

        public ConfigWindow(PerfmonSettingsVeiwModel viewModel)
        {
            InitializeComponent();
            this.viewModel = viewModel;
        }

        public PerfmonSettingsVeiwModel ViewModel
        {
            get { return viewModel; }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = viewModel != null && viewModel.IsValid;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
