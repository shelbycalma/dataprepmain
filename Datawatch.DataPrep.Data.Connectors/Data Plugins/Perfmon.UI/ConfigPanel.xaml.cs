﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using Datawatch.DataPrep.Data.Framework;
using Panopticon.PerfmonPlugin.UI.ViewModels;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using TreeView = System.Windows.Controls.TreeView;
using Utils = Datawatch.DataPrep.Data.Core.UI.Utils;

namespace Panopticon.PerfmonPlugin.UI
{
    internal partial class ConfigPanel
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                                        typeof(PerfmonSettingsVeiwModel),
                                        typeof(ConfigPanel),
                                        new PropertyMetadata(Settings_Changed));

        private ISelectableItem shiftStartItem;

        private static readonly HashSet<Key> allowedKeysToHandle = new HashSet<Key>
                                                                    {
                                                                        Key.Up,
                                                                        Key.Down,
                                                                        Key.Home,
                                                                        Key.End,
                                                                        Key.Space,
                                                                        Key.Left,
                                                                        Key.Right,
                                                                        Key.PageUp,
                                                                        Key.PageDown,
                                                                        Key.A
                                                                    };

        public ConfigPanel()
        {
            InitializeComponent();
        }

        public PerfmonSettingsVeiwModel Settings
        {
            get { return (PerfmonSettingsVeiwModel)GetValue(SettingsProperty); }
            set
            {
                SetValue(SettingsProperty, value);
                try
                {
                    Settings.RefreshCategories();
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message);
                    MessageBox.Show(string.Format(Properties.Resources.UiConnectionFailed, ex.Message),
                                    null, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private static void Settings_Changed(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged(
                (PerfmonSettingsVeiwModel)args.OldValue,
                (PerfmonSettingsVeiwModel)args.NewValue);
        }

        protected void OnSettingsChanged(
            PerfmonSettingsVeiwModel oldSettings, PerfmonSettingsVeiwModel newSettings)
        {
            DataContext = newSettings;
        }

        private void TreeView_PreviewMouseDown(object sender, RoutedEventArgs args)
        {
            if (args.OriginalSource is Grid)
            {
                DeselectAllItems();
                shiftStartItem = null;
                return;
            }

            ToggleButton toggleButton =
                Utils.FindParent<ToggleButton>((DependencyObject)args.OriginalSource);
            if (toggleButton != null) return;

            TreeViewItem newTreeViewItem =
                Utils.FindParent<TreeViewItem>((DependencyObject)args.OriginalSource);
            if (newTreeViewItem == null) return;

            ISelectableItem selectedItem = newTreeViewItem.Header as ISelectableItem;
            if (selectedItem == null) return;

            newTreeViewItem.Focus();
            args.Handled = true;

            if (selectedItem.IsSelected && !IsCtrlPressed && !IsShiftPressed)
            {
                DeselectAllItems();
                selectedItem.IsSelected = true;
                shiftStartItem = selectedItem;
                return;
            }

            //Unless the ctrl button is pressed, clear any existing selections.
            //Also deselect all items if shift is pressed.
            if (!IsCtrlPressed || IsShiftPressed)
            {
                DeselectAllItems();
            }
            
            if (IsShiftPressed)
            {
                SelectItemsWithShiftBehavior(selectedItem, shiftStartItem);
                return;
            }

            selectedItem.IsSelected = !IsCtrlPressed || !selectedItem.IsSelected;

            shiftStartItem = selectedItem;
        }

        private void TreeView_PreviewKeyDown(object sender, RoutedEventArgs args)
        {
            KeyEventArgs keyEventArgs = (KeyEventArgs)args;

            if (!allowedKeysToHandle.Contains(keyEventArgs.Key)) return;
            if (keyEventArgs.Key == Key.A && !IsCtrlPressed) return;
            if (!(args.OriginalSource is TreeViewItem)) return;

            TreeViewItem currentTreeViewItem = (TreeViewItem)args.OriginalSource;
            if (currentTreeViewItem == null) return;

            ISelectableItem currentItem = (ISelectableItem)currentTreeViewItem.Header;
            if (currentItem == null) return;

            if ((keyEventArgs.Key == Key.Right && !currentTreeViewItem.IsExpanded)
             || (keyEventArgs.Key == Key.Left && currentTreeViewItem.IsExpanded)) return;
            
            args.Handled = true;

            if (keyEventArgs.Key == Key.Space)
            {
                currentItem.IsSelected = !currentItem.IsSelected;
                shiftStartItem = currentItem;
                return;
            }

            IEnumerable<ISelectableItem> items = Settings.AllCategories;
            IList<ISelectableItem> flattenedItems = FlattenTreeStructure(items, true);
            ISelectableItem newSelectedItem;
            int currentItemIndex = flattenedItems.IndexOf(currentItem);

            switch (keyEventArgs.Key)
            {
                case Key.Down:
                    newSelectedItem =
                        HandleUpDown(currentItem, flattenedItems, flattenedItems.LastOrDefault(), currentItemIndex + 1);
                    break;
                case Key.End:
                case Key.PageDown:
                    newSelectedItem =
                        HandleUpDown(currentItem, flattenedItems, flattenedItems.LastOrDefault(), flattenedItems.Count - 1);
                    break;
                case Key.Up:
                    newSelectedItem =
                        HandleUpDown(currentItem, flattenedItems, flattenedItems.FirstOrDefault(), currentItemIndex - 1);
                    break;
                case Key.Home:
                case Key.PageUp:
                    newSelectedItem =
                        HandleUpDown(currentItem, flattenedItems, flattenedItems.FirstOrDefault(), 0);
                    break;
                case Key.Left:
                    TreeViewItem parentTreeViewItem = FindAncestor<TreeViewItem>(currentTreeViewItem);
                    if (parentTreeViewItem != null)
                    {
                        newSelectedItem = (ISelectableItem)parentTreeViewItem.Header;
                    }
                    else
                    {
                        return;
                    }
                    break;
                case Key.Right:
                    TreeViewItem firstChildTreeViewItem =
                        (TreeViewItem)currentTreeViewItem.ItemContainerGenerator.ContainerFromIndex(0);
                    newSelectedItem = (ISelectableItem)firstChildTreeViewItem.Header;
                    break;
                default: //Ctrl + A
                    HandleCtrlA(FlattenTreeStructure(items));
                    return;
            }

            if (newSelectedItem == currentItem && (IsShiftPressed || IsCtrlPressed)) return;

            TreeViewItem newTreeViewItem = GetTreeViewItem((TreeView)sender, newSelectedItem);
            if (newTreeViewItem != null)
            {
                newTreeViewItem.Focus();
            }

            if (IsCtrlPressed)
            {
                return;
            }

            DeselectAllItems();

            if (IsShiftPressed)
            {
                SelectItemsWithShiftBehavior(newSelectedItem, shiftStartItem);
                return;
            }

            newSelectedItem.IsSelected = true;
            shiftStartItem = newSelectedItem;
        }

        private static T FindAncestor<T>(DependencyObject dependencyObject) where T : class
        {
            while (true)
            {
                var parent = VisualTreeHelper.GetParent(dependencyObject);

                if (parent == null) return null;

                T parentT = parent as T;
                if (parentT != null) return parentT;
                dependencyObject = parent;
            }
        }

        private void DeselectAllItems()
        {
            foreach (CategoryViewModel category in Settings.AllCategories)
            {
                foreach (CounterViewModel counter in category.Counters)
                {
                    counter.IsSelected = false;
                }

                category.IsSelected = false;
            }
        }

        private static ISelectableItem HandleUpDown(
            ISelectableItem currentItem, IList<ISelectableItem> flattenedItems,
            ISelectableItem boundaryItem, int newItemIndex)
        {
            return boundaryItem == currentItem ? currentItem : flattenedItems[newItemIndex];
        }

        private static void HandleCtrlA(IEnumerable<ISelectableItem> flattenedItems)
        {
            foreach (var flattenedItem in
                flattenedItems.Where(flattenedItem => !flattenedItem.IsSelected))
            {
                flattenedItem.IsSelected = true;
            }
        }

        private static TreeViewItem GetTreeViewItem(ItemsControl control, ISelectableItem item)
        {
            TreeViewItem treeViewItem =
                (TreeViewItem)control.ItemContainerGenerator.ContainerFromItem(item);
            if (treeViewItem != null)
            {
                return treeViewItem;
            }

            for (int i = 0; i < control.Items.Count; i++)
            {
                treeViewItem = (TreeViewItem)control.ItemContainerGenerator.ContainerFromIndex(i);

                if (treeViewItem == null || !treeViewItem.IsExpanded) continue;

                TreeViewItem innerTreeViewItem = GetTreeViewItem(treeViewItem, item);
                if (innerTreeViewItem != null)
                {
                    return innerTreeViewItem;
                }
            }

            return null;
        }

        private void SelectItemsWithShiftBehavior(ISelectableItem selectedItem,
            ISelectableItem lastSelectedItem)
        {
            List<ISelectableItem> flattenedItems = FlattenTreeStructure(Settings.AllCategories);

            int startIndex = flattenedItems.IndexOf(lastSelectedItem);
            startIndex = Math.Max(0, startIndex);
            int endIndex = flattenedItems.IndexOf(selectedItem);

            //swap the values if the start is greater than the end.
            if (startIndex > endIndex)
            {
                var temp = startIndex;
                startIndex = endIndex;
                endIndex = temp;
            }

            for (int i = startIndex; i < endIndex + 1; i++)
            {
                ISelectableItem item = flattenedItems[i];

                if (item is CategoryViewModel &&
                    (selectedItem is CounterViewModel ||
                     selectedItem is CategoryViewModel && item != selectedItem))
                {
                    continue;
                }

                item.IsSelected = true;
            }
        }

        private List<ISelectableItem> FlattenTreeStructure(
            IEnumerable<ISelectableItem> items, bool expandedOnly = false)
        {
            List<ISelectableItem> flattenedItems = new List<ISelectableItem>();
            foreach (ISelectableItem item in items)
            {
                flattenedItems.Add(item);

                if (expandedOnly && !GetTreeViewItem(CategoriesTreeView, item).IsExpanded)
                {
                    continue;
                }

                IEnumerable<ISelectableItem> children = ((CategoryViewModel) item).Counters;

                flattenedItems.AddRange(children);
            }

            return flattenedItems;
        }
        
        private static bool IsCtrlPressed
        {
            get
            {
                return Keyboard.IsKeyDown(Key.LeftCtrl)
                    || Keyboard.IsKeyDown(Key.RightCtrl);
            }
        }

        private static bool IsShiftPressed
        {
            get
            {
                return Keyboard.IsKeyDown(Key.LeftShift)
                    || Keyboard.IsKeyDown(Key.RightShift);
            }
        }

        private void Connect_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                Settings.RefreshCategories();
            }
            catch(Exception ex)
            {
                Log.Error(ex.Message);
                MessageBox.Show(string.Format(Properties.Resources.UiConnectionFailed, ex.Message),
                                null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Connect_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Settings != null && Settings.CanConnect;
        }

        private void AddCounters_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                Settings.AddCounters();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, null, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void AddCounters_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Settings != null && Settings.CanAddCounters;
        }

        private void AddedCounters_PreviewKeyDown(object sender, KeyEventArgs args)
        {
            if (args.Key == Key.Delete)
            {
                Settings.RemoveCounters();
            }
        }

        private void SetTreeViewFocus(object sender, RoutedEventArgs args)
        {
            CategoriesTreeView.Focus();
        }
    }
}
