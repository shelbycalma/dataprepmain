﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Panopticon.PerfmonPlugin.UI.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Panopticon.PerfmonPlugin.UI.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maximum counters number is {0}.
        /// </summary>
        public static string ExMaxCounterNumber {
            get {
                return ResourceManager.GetString("ExMaxCounterNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Add.
        /// </summary>
        public static string UiAddCoutners {
            get {
                return ResourceManager.GetString("UiAddCoutners", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Added counters.
        /// </summary>
        public static string UiAddedCounters {
            get {
                return ResourceManager.GetString("UiAddedCounters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;All instances&gt;.
        /// </summary>
        public static string UiAllInstances {
            get {
                return ResourceManager.GetString("UiAllInstances", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Available counters.
        /// </summary>
        public static string UiAvailableCounter {
            get {
                return ResourceManager.GetString("UiAvailableCounter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string UiCancelButtonText {
            get {
                return ResourceManager.GetString("UiCancelButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connect.
        /// </summary>
        public static string UiConnect {
            get {
                return ResourceManager.GetString("UiConnect", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Connection failed: {0}.
        /// </summary>
        public static string UiConnectionFailed {
            get {
                return ResourceManager.GetString("UiConnectionFailed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description.
        /// </summary>
        public static string UiDescription {
            get {
                return ResourceManager.GetString("UiDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Id Column Name.
        /// </summary>
        public static string UiIdColumnName {
            get {
                return ResourceManager.GetString("UiIdColumnName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Instances of selected object.
        /// </summary>
        public static string UiInstancesOfSelectedObject {
            get {
                return ResourceManager.GetString("UiInstancesOfSelectedObject", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to OK.
        /// </summary>
        public static string UiOkButtonText {
            get {
                return ResourceManager.GetString("UiOkButtonText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Real-time Limit (ms).
        /// </summary>
        public static string UiRealTimeLimit {
            get {
                return ResourceManager.GetString("UiRealTimeLimit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Remove.
        /// </summary>
        public static string UiRemoveCounters {
            get {
                return ResourceManager.GetString("UiRemoveCounters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select counters from computer.
        /// </summary>
        public static string UiSelectCountersFromComputer {
            get {
                return ResourceManager.GetString("UiSelectCountersFromComputer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Id Column Name.
        /// </summary>
        public static string UiTimeIdColumnName {
            get {
                return ResourceManager.GetString("UiTimeIdColumnName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Window (s).
        /// </summary>
        public static string UiTimeWindow {
            get {
                return ResourceManager.GetString("UiTimeWindow", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Perfmon Connection.
        /// </summary>
        public static string UiWindowTitle {
            get {
                return ResourceManager.GetString("UiWindowTitle", resourceCulture);
            }
        }
    }
}
