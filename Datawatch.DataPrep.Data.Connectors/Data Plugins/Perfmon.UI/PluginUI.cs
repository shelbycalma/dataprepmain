﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.PerfmonPlugin.UI.ViewModels;

namespace Panopticon.PerfmonPlugin.UI
{
    public class PluginUI : PluginUIBase<Plugin, PerfmonSettings>
    {
        public PluginUI(Plugin plugin, Window owner) : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            PerfmonSettings perfmonSettings = new PerfmonSettings();
            PerfmonSettingsVeiwModel viewModel =
                new PerfmonSettingsVeiwModel(perfmonSettings, parameters);
            viewModel.RefreshCategories();

            ConfigWindow window = new ConfigWindow(viewModel);
            window.Owner = owner;

            if (window.ShowDialog() == true)
            {
                return perfmonSettings.ToPropertyBag();
            }
            return null;
        }

        public override object GetConfigElement(PropertyBag bag, IEnumerable<ParameterValue> parameters)
        {
            return new ConfigPanel
            {
                Settings = new PerfmonSettingsVeiwModel(new PerfmonSettings(bag), parameters)
            };
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            return cp.Settings.Settings.ToPropertyBag();
        }
    }
}
