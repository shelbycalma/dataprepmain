﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using AMPS.Client;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Exceptions = Datawatch.DataPrep.Data.Core.Connectors.Exceptions;

namespace Panopticon.AMPSPlugin
{
    public class ColumnDefinitionGenerator
    {
        private int messagesReceived;
        private readonly Dictionary<string, ColumnDefinition> generatedColumns =
            new Dictionary<string, ColumnDefinition>();

        public void GenerateColumns(AMPSSettings settings)
        {
            if (settings == null) throw Exceptions.ArgumentNull("settings");

            Client client;
            string uriString = settings.Uri;

            using (client = new Client("Panopticon.AMPSSettings-" + Guid.NewGuid()))
            {
                client.connect(uriString);
                client.logon();

                messagesReceived = 0;
                generatedColumns.Clear();
                foreach (ColumnDefinition columnDefinition in
                    settings.ParserSettings.Columns)
                {
                    generatedColumns[columnDefinition.Name] = columnDefinition;
                }

                DateTime startTime = DateTime.Now;
                CommandId commandId;

                switch (settings.SubscriptionMode)
                {
                    case SubscriptionMode.Sow:
                    case SubscriptionMode.SowAndSubscribe:
                    case SubscriptionMode.SowAndDeltaSubscribe:
                        commandId = client.sow(
                            GenerateColumnDefinitions,
                            settings.Topic, 0L);
                        break;
                    default:
                        commandId = client.subscribe(
                            GenerateColumnDefinitions,
                            settings.Topic, 0L);
                        break;
                }

                lock (this)
                {
                    while (messagesReceived < 10 &&
                            (DateTime.Now - startTime).Seconds < 10)
                    {
                        Monitor.Wait(this, 200);
                    }
                    client.unsubscribe(commandId);
                    client.close();
                }

            }
            List<ColumnDefinition> list =
                new List<ColumnDefinition>(generatedColumns.Values);
            list.Sort(new ColumnDefinitionComparer());
            settings.ParserSettings.AddColumnDefinitions(list);
        }

        private void GenerateColumnDefinitions(Message message)
        {
            if (!(message.Command == Message.Commands.SOW || 
                message.Command == Message.Commands.Publish))
            {
                return;
            }

            Dictionary<string, string> fix = FixSplitter.Split(message.Data);
            
            foreach (string fixtag in fix.Keys)
            {
                string str = fix[fixtag];
                ColumnType type = ColumnType.Text;
                double d;

                if (Double.TryParse(str, NumberStyles.Any,
                    CultureInfo.InvariantCulture, out d))
                {
                    type = ColumnType.Numeric;
                }
                else
                {
                    try
                    {
                        Convert.ToDateTime(str);
                        type = ColumnType.Time;
                    }
                    catch (FormatException)
                    {
                    }
                }
                
                if (!generatedColumns.ContainsKey(fixtag))
                {
                    FixColumnDefinition fixColumnDefinition =
                        new FixColumnDefinition(new PropertyBag());

                    fixColumnDefinition.FixTag = fixtag;
                    fixColumnDefinition.Name = fixtag;
                    fixColumnDefinition.Type = type;

                    generatedColumns[fixColumnDefinition.Name] =
                        fixColumnDefinition;
                }
                else
                {
                    if (generatedColumns[fixtag].Type != ColumnType.Text &&
                        type == ColumnType.Text)
                    {
                        generatedColumns[fixtag].Type = ColumnType.Text;
                    }
                }
            }            
            lock (this)
            {
                messagesReceived++;
                Monitor.Pulse(this);
            }
        }

        private class ColumnDefinitionComparer : IComparer<ColumnDefinition>
        {
            public int Compare(ColumnDefinition x, ColumnDefinition y)
            {
                if (x.Type == y.Type)
                {
                    return x.Name.CompareTo(y.Name);
                }
                if (x.Type == ColumnType.Text)
                {
                    return -1;
                }
                if (y.Type == ColumnType.Text)
                {
                    return 1;
                }
                if (x.Type == ColumnType.Numeric)
                {
                    return -1;
                }
                if (y.Type == ColumnType.Numeric)
                {
                    return 1;
                }
                return 1;
            }
        }
    }
}
