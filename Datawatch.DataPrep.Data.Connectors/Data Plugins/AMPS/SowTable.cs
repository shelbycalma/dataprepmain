﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.AMPSPlugin
{
    public class SowTable : ParameterTable
    {
        private bool sowLoaded;

        public SowTable(IEnumerable<ParameterValue> parameters)
            : base(parameters)
        {
        }

        public bool IsSowLoaded
        {
            get { return sowLoaded; }
            internal set { sowLoaded = value; }
        }
    }
}
