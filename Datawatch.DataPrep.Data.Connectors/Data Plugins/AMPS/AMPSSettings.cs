﻿using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.MessageParsing;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.AMPSPlugin
{
    public class AMPSSettings : MessageQueueSettingsBase
    {
        private const string UriTemplate = "tcp://{0}:{1}/{2}";
        private const int DefaultFixPort = 9004;
        private const int DefaultXmlPort = 9005;
        private const int DefaultNvFixPort = 9006;
        private const int DefaultTimeout = 5000;
        public const string SowKey = "[AMPS SowKey]";
        public const string Timestamp = "[AMPS Timestamp]";

        public AMPSSettings(IPluginManager pluginManager)
            : base(pluginManager)
        {
        }

        public AMPSSettings(IPluginManager pluginManager, PropertyBag bag)
            : base(pluginManager, bag)
        {
        }

        public int BatchSize
        {
            get { return GetInternalInt("BatchSize", 100); }
            set { SetInternalInt("BatchSize", value);}
        }

        public string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value);}
        }

        public int Port
        {
            get { return GetInternalInt("Port", GetDefaultPort()); }          
            set { SetInternalInt("Port", value); }
        }

        private int GetDefaultPort()
        {
            switch (MessageType)
            {
                case MessageType.Fix:
                    return DefaultFixPort;
                case MessageType.Xml:
                    return DefaultXmlPort;
                case MessageType.NvFix:
                    return DefaultNvFixPort;
            }
            return DefaultFixPort;
        }

        public MessageType MessageType
        {
            get
            {
                return (MessageType) 
                    Enum.Parse(typeof (MessageType), 
                        GetInternal("MessageType", "Fix"));
            }
            set
            {
                MessageType oldMessageType = MessageType;
                SetInternal("MessageType", value.ToString());
                FirePropertyChanged("Port");
                if (GetParserType(oldMessageType) != GetParserType(value))
                {
                    if (parserSettings != null)
                    {
                        parserSettings.Dispose();
                        parserSettings = null;
                    }
                    FirePropertyChanged("ParserSettings");                    
                }
            }
        }

        private static Type GetParserType(MessageType messageType)
        {
            return messageType == AMPSPlugin.MessageType.Fix || 
                messageType == AMPSPlugin.MessageType.NvFix
                       ? typeof (FixParser)
                       : typeof (XPathParser);
        }

        public MessageType[] MessageTypes
        {
            get
            {
                return (MessageType[]) Enum.GetValues(typeof (MessageType));
            }
        }

        public string Filter
        {
            get { return GetInternal("Filter"); }
            set { SetInternal("Filter", value); }
        }

        public SubscriptionMode SubscriptionMode
        {
            get 
            { 
                return (SubscriptionMode) Enum.Parse(typeof (SubscriptionMode), 
                    GetInternal("SubscriptionMode", "SowAndDeltaSubscribe")); 
            }
            set
            {
                SetInternal("SubscriptionMode", value.ToString());
                UpdateIdCandidates();
            }
        }

        public SubscriptionMode[] SubscriptionModes
        {
            get
            {
                return (SubscriptionMode[]) 
                    Enum.GetValues(typeof (SubscriptionMode));
            }
        }

        public int Timeout
        {
            get { return GetInternalInt("Timeout", DefaultTimeout); }
            set { SetInternalInt("Timeout", value);}
        }

        protected override string DefaultParserPluginId
        {
            get { return "Fix"; }
        }

        public override ParserSettings ParserSettings
        {
            get
            {
                if (parserSettings != null) return parserSettings;

                switch (MessageType)
                {

                    case AMPSPlugin.MessageType.NvFix:
                    case AMPSPlugin.MessageType.Fix:
                        parserSettings = new FixParserSettings(parserSettingsBag);
                        break;
                    case AMPSPlugin.MessageType.Xml:
                        parserSettings = new XPathParserSettings(parserSettingsBag);
                        break;
                }
                return parserSettings;
            }
        }

        public override void UpdateIdCandidates()
        {
            if (SubscriptionMode == SubscriptionMode.Sow ||
                SubscriptionMode == SubscriptionMode.SowAndDeltaSubscribe ||
                SubscriptionMode == SubscriptionMode.SowAndSubscribe)
            {
                base.UpdateIdCandidates(SowKey, Timestamp);
                return;
            }
            base.UpdateIdCandidates(null, Timestamp);
        }

        public string Uri
        {
            get
            {
                return string.Format(UriTemplate, Host, Port, MessageType);                
            }
        }

        public override bool ShowTestConnectorButton
        {
            get { return true; }
        }

        public override bool ShowGenerateColumnButton
        {
            get { return true; }
        }

        /// <summary>
        /// Gets a value indicating wether the current AMPSSettings 
        /// instance can be okeyed.
        /// </summary>
        public override bool IsOk
        {
            get
            {
                // If Connection exists and has a valid Host, Topic and IdColumn, 
                // return true.
                return !string.IsNullOrEmpty(Host) &&
                       !string.IsNullOrEmpty(Topic) &&
                       !string.IsNullOrEmpty(IdColumn) &&
                       IsParserSettingsOk;
            }
        }
        
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return Equals(obj as AMPSSettings);
        }

        public bool Equals(AMPSSettings other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return base.Equals(other) &&
                   Equals(other.Host, Host) &&
                   other.Port == Port &&
                   other.BatchSize == BatchSize &&
                   Equals(other.MessageType, MessageType) &&
                   Equals(other.Filter, Filter) &&
                   Equals(other.SubscriptionMode, SubscriptionMode) &&
                   Equals(other.Timeout, Timeout);
        }

        public override int GetHashCode()
        {
            int code = base.GetHashCode();

            if (!string.IsNullOrEmpty(Host))
            {
                code += 23 * code + Host.GetHashCode();
            }
            code += 23 * code + Port.GetHashCode();
            code += 23 * code + MessageType.GetHashCode();
            if (!string.IsNullOrEmpty(Filter))
            {
                code += 23*code + Filter.GetHashCode();
            }
            code += 23 * code + SubscriptionMode.GetHashCode();
            code += 23 * code + Timeout.GetHashCode();
            code += 23 * code + BatchSize.GetHashCode();
            return code;
       }

    }
}
