﻿using System;
using System.Collections.Generic;
using System.Threading;
using AMPS.Client;
using AMPS.Client.Exceptions;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Connectors;
using Panopticon.AMPSPlugin.Properties;

namespace Panopticon.AMPSPlugin
{
    public class AMPSDataAdapter :
        MessageQueueAdapterBase<SowTable, AMPSSettings, Dictionary<string, object>>,
        ClientDisconnectHandler
    {
        private Client client;
        private CommandId subscriptionId;
        private int messagesQueued;
        private bool combineEvents;
        private const int QueuedMessageLimit = 10000;

        public override void Start()
        {
            settings.ParserSettings.Parameters = table.Parameters;
            base.Start();
            table.Changed += table_Changed;

            client = new Client("Panopticon.AMPSDataAdapter-" + Guid.NewGuid());

            string uriString = settings.Uri;

            client.setDisconnectHandler(this);
            client.setExceptionListener(client_Exception);

            string filter = DataUtils.ApplyParameters(settings.Filter, table.Parameters);
            string topic = DataUtils.ApplyParameters(settings.Topic, table.Parameters);

            SubscriptionMode actualSubscriptionMode = settings.SubscriptionMode;

            if (table.IsSowLoaded)
            {
                switch (settings.SubscriptionMode)
                {
                    case SubscriptionMode.SowAndSubscribe:
                        actualSubscriptionMode = SubscriptionMode.Subscribe;
                        break;
                    case SubscriptionMode.SowAndDeltaSubscribe:
                        actualSubscriptionMode = SubscriptionMode.DeltaSubscribe;
                        break;
                }
            }

            combineEvents =
                settings.SubscriptionMode == SubscriptionMode.SowAndDeltaSubscribe ||
                settings.SubscriptionMode == SubscriptionMode.DeltaSubscribe;

            Log.Info(Properties.Resources.LogStartingAMPS, actualSubscriptionMode,
                uriString, topic, filter);
            try
            {
                client.connect(uriString);
                client.logon();

                switch (actualSubscriptionMode)
                {
                    case SubscriptionMode.Sow:
                        subscriptionId = client.sow(
                            client_MessageHandler,
                            topic, filter, settings.BatchSize, true,
                            settings.Timeout);
                        break;
                    case SubscriptionMode.SowAndSubscribe:
                        subscriptionId = client.sowAndSubscribe(
                            client_MessageHandler,
                            topic, filter, settings.BatchSize, true,
                            settings.Timeout);
                        break;
                    case SubscriptionMode.SowAndDeltaSubscribe:
                        subscriptionId = client.sowAndDeltaSubscribe(
                            client_MessageHandler,
                            topic, filter, settings.BatchSize, true, false,
                            settings.Timeout);
                        break;
                    case SubscriptionMode.Subscribe:
                        subscriptionId = client.subscribe(
                            client_MessageHandler,
                            topic, filter, settings.Timeout);
                        break;
                    case SubscriptionMode.DeltaSubscribe:
                        subscriptionId = client.deltaSubscribe(
                            client_MessageHandler,
                            topic, filter, settings.Timeout);
                        break;
                }
            }
            catch (AMPSException e)
            {
                Log.Error(Resources.UiMessageConnectionError, e.Message);
                throw e;
            }
        }

        private void client_Exception(Exception e)
        {
            Log.Exception(e);
        }

        private void client_MessageHandler(Message message)
        {
            if (message.Command == Message.Commands.GroupEnd)
            {
                table.IsSowLoaded = true;
            }
            if (!(message.Command == Message.Commands.SOW ||
                message.Command == Message.Commands.Publish ||
                message.Command == Message.Commands.OOF))
            {
                return;
            }

            Dictionary<string, object> fields = parser.Parse(message.Data);
            if (fields.Count == 0) return;

            object id;

            if (idColumn == AMPSSettings.SowKey)
            {
                id = message.SowKey;
            }
            else
            {
                fields.TryGetValue(idColumn, out id);
            }

            if (id == null) return;

            if (timeIdColumn == AMPSSettings.Timestamp)
            {
                fields.Add(AMPSSettings.Timestamp, message.Timestamp);
            }

            //Log.Info("{0}", message);
            switch (message.Command)
            {
                case Message.Commands.SOW:
                case Message.Commands.Publish:
                    updater.EnqueueUpdate(id.ToString(), fields);
                    break;
                case Message.Commands.OOF:
                    updater.EnqueueDelete(id.ToString(), fields);
                    break;
            }
            lock (this)
            {
                messagesQueued++;
                if (messagesQueued > QueuedMessageLimit)
                {
                    Log.Info(Properties.Resources.LogAMPSMessageCount,
                        QueuedMessageLimit);
                    while (messagesQueued > 0)
                    {
                        Monitor.Wait(this, 200);
                    }
                }
            }
        }

        private void table_Changed(object sender, TableChangedEventArgs e)
        {
            lock (this)
            {
                if (messagesQueued > QueuedMessageLimit)
                {
                    Log.Info(Properties.Resources.LogAMPSTableUpdated);
                }
                messagesQueued = 0;
                Monitor.Pulse(this);
            }
        }

        public override void Stop()
        {
            base.Stop();
            if (client == null) return;

            string uriString = settings.Uri;

            Log.Info(Properties.Resources.LogStoppingAMPS, uriString,
                DataUtils.ApplyParameters(settings.Topic, table.Parameters),
                DataUtils.ApplyParameters(settings.Filter, table.Parameters));

            lock (this)
            {
                if (messagesQueued > QueuedMessageLimit)
                {
                    messagesQueued = 0;
                    Monitor.Pulse(this);
                }
            }

            client.unsubscribe(subscriptionId);
            client.Dispose();
            client = null;
            table.Changed -= table_Changed;
        }

        public void invoke(Client c)
        {
            Log.Info(Properties.Resources.LogAMPSDisconnected);
            client = null;
            updater.ConnectionLost(true);
        }

        public override bool DoCombineEvents
        {
            get { return combineEvents; }
        }

    }
}