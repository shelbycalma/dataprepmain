﻿namespace Panopticon.AMPSPlugin
{
    public enum SubscriptionMode
    {
        Sow,
        SowAndSubscribe,
        SowAndDeltaSubscribe,
        Subscribe,
        DeltaSubscribe
    }
}
