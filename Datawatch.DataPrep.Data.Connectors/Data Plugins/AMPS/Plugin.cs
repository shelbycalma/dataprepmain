﻿using System.Collections.Generic;
using System.ComponentModel;
using System;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Table;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.AMPSPlugin.Properties;

namespace Panopticon.AMPSPlugin
{
    /// <summary>
    /// DataPlugin for AMPS.
    /// </summary>
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin :
        MessageQueuePluginBase<SowTable, AMPSSettings,
            Dictionary<string, object>, AMPSDataAdapter>
    {
        internal const string PluginId = "AMPS";
        internal const string PluginTitle = "AMPS";
        internal const string PluginType = DataPluginTypes.Streaming;
        internal const bool PluginIsObsolete = false;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override AMPSSettings CreateConnectionSettings(IEnumerable<ParameterValue> parameters)
        {
            return new AMPSSettings(pluginManager);
        }

        public override AMPSSettings CreateConnectionSettings(PropertyBag bag)
        {
            return new AMPSSettings(pluginManager, bag);
        }

        protected override ITable CreateTable(AMPSSettings s,
            IEnumerable<ParameterValue> parameters, string dataDirectory)
        {
            DateTime start = DateTime.Now;
            SowTable table = new SowTable(parameters);

            table.BeginUpdate();

            try
            {
                if (s.IdColumn == AMPSSettings.SowKey)
                {
                    table.AddColumn(new TextColumn(AMPSSettings.SowKey));
                }
                if (s.TimeIdColumn == AMPSSettings.Timestamp)
                {
                    table.AddColumn(new TimeColumn(AMPSSettings.Timestamp));
                }
                DataPluginUtils.AddColumns(table, s.ParserSettings.Columns);
            }
            finally
            {
                table.EndUpdate();
            }
            Log.Info(Resources.LogCreateTableCompleted,
                table.ColumnCount, (DateTime.Now - start).TotalSeconds);

            return table;
        }

        public override bool IsObsolete
        {
            get
            {
                return PluginIsObsolete;
            }
        }

        public override string DataPluginType
        {
            get
            {
                return DataPluginTypes.GetLocalized(PluginType);
            }
        }
    }
}
