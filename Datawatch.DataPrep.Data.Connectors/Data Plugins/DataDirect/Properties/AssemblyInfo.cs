﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using DB2 = Panopticon.DataDirect.DB2.Plugin;
using DBase = Panopticon.DataDirect.dBase.Plugin;
using GreenPlum = Panopticon.DataDirect.Greenplum.Plugin;
using HadoopHive = Panopticon.DataDirect.HadoopHive.Plugin;
using Impala = Panopticon.DataDirect.Impala.Plugin;
using Informix = Panopticon.DataDirect.Informix.Plugin;
using MySql = Panopticon.DataDirect.MySql.Plugin;
using Oracle = Panopticon.DataDirect.Oracle.Plugin;
using PostgreSql = Panopticon.DataDirect.PostgreSQL.Plugin;
using Redshift = Panopticon.DataDirect.Redshift.Plugin;
using SparkSql = Panopticon.DataDirect.SparkSql.Plugin;
using MSSQL = Panopticon.DataDirect.SqlServer.Plugin;
using SybaseIQ = Panopticon.DataDirect.SybaseIQ.Plugin;
using Teradata = Panopticon.DataDirect.Teradata.Plugin;
using MongoDB = Panopticon.DataDirect.MongoDB.Plugin;
using Salesforce = Panopticon.DataDirect.Salesforce.Plugin;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("Panopticon.DataDirect.dll")]
[assembly: AssemblyDescription("Datawatch DataDirect Plug-in")]// General Information about an assembly is controlled through the following 

//[assembly: PluginDescription("DataDirect", "Database")]
[assembly: PluginDescription(DB2.PluginTitle, DB2.PluginType, DB2.PluginId, DB2.PluginIsObsolete)]
[assembly: PluginDescription(DBase.PluginTitle, DBase.PluginType, DBase.PluginId, DBase.PluginIsObsolete)]
[assembly: PluginDescription(GreenPlum.PluginTitle, GreenPlum.PluginType, GreenPlum.PluginId, GreenPlum.PluginIsObsolete)]
[assembly: PluginDescription(HadoopHive.PluginTitle, HadoopHive.PluginType, HadoopHive.PluginId, HadoopHive.PluginIsObsolete)]
[assembly: PluginDescription(Impala.PluginTitle, Impala.PluginType, Impala.PluginId, Impala.PluginIsObsolete)]
[assembly: PluginDescription(Informix.PluginTitle, Informix.PluginType, Informix.PluginId, Informix.PluginIsObsolete)]
[assembly: PluginDescription(MySql.PluginTitle, MySql.PluginType, MySql.PluginId, MySql.PluginIsObsolete)]
[assembly: PluginDescription(Oracle.PluginTitle, Oracle.PluginType, Oracle.PluginId, Oracle.PluginIsObsolete)]
[assembly: PluginDescription(PostgreSql.PluginTitle, PostgreSql.PluginType, PostgreSql.PluginId, PostgreSql.PluginIsObsolete)]
[assembly: PluginDescription(Redshift.PluginTitle, Redshift.PluginType, Redshift.PluginId, Redshift.PluginIsObsolete)]
[assembly: PluginDescription(SparkSql.PluginTitle, SparkSql.PluginType, SparkSql.PluginId, SparkSql.PluginIsObsolete)]
[assembly: PluginDescription(MSSQL.PluginTitle, MSSQL.PluginType, MSSQL.PluginId, MSSQL.PluginIsObsolete)]
[assembly: PluginDescription(SybaseIQ.PluginTitle, SybaseIQ.PluginType, SybaseIQ.PluginId, SybaseIQ.PluginIsObsolete)]
[assembly: PluginDescription(Teradata.PluginTitle, Teradata.PluginType, Teradata.PluginId, Teradata.PluginIsObsolete)]
[assembly: PluginDescription(MongoDB.PluginTitle, MongoDB.PluginType, MongoDB.PluginId, MongoDB.PluginIsObsolete)]
[assembly: PluginDescription(Salesforce.PluginTitle, Salesforce.PluginType, Salesforce.PluginId, Salesforce.PluginIsObsolete)]