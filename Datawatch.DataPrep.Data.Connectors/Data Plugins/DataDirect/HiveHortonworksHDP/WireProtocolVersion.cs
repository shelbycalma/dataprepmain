﻿
namespace Panopticon.DataDirect.HiveHortonworksHDP
{
    public enum WireProtocolVersion
    {
        Autodetect = 0,
        HiveServer1 = 1,
        HiveServer2 = 2
    }
}
