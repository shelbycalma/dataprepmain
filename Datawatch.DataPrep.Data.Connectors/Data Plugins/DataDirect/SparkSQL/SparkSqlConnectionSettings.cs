﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;


namespace Panopticon.DataDirect.SparkSql
{
    [DataHelpKey("HadoopHive")]
    public class SparkSqlConnectionSettings : DataDirectSettingsBase
    {
        private const string commandTerminator = ";";
        private const string tablesRequestFormat = "show tables in {0}" + commandTerminator;

        public SparkSqlConnectionSettings()
            : this(new PropertyBag(), null)
        {
            //set default Port number
            Port = "10000";
        }

        public SparkSqlConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.SparkSql)
        {
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "SHOW DATABASES";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "10000");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Apache Spark SQL";
            }
        }

        protected override string DriverVersion
        {
            get { return "8.0 "; }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSparkSqlPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSparkSqlWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override SchemaAndTable[] GetTablesAndViews(string connectionString)
        {
            List<SchemaAndTable> list = new List<SchemaAndTable>();
            var tables = new DataTable();

            using (DbConnection connection =
                Executor.GetConnection(connectionString))
            {
                connection.Open();
                using(var command = connection.CreateCommand())
                {
                    command.CommandText = string.Format(tablesRequestFormat, DatabaseName);
                    tables.Load(command.ExecuteReader());
                }
            }
            list.AddRange(GetSchemaAndTable(tables));

            //The views should be added as well, if supported
            return list.ToArray();
        }

        public override List<SchemaAndTable> GetSchemaAndTable(DataTable result)
        {
            List<SchemaAndTable> list = new List<SchemaAndTable>();
            for (int i = 0; i < result.Rows.Count; i++)
            {
                DataRow dataRow = result.Rows[i];
                string table = dataRow["tableName"].ToString();

                if (FilterTable(table))
                {
                    continue;
                }

                list.Add(new SchemaAndTable(null, table));
            }

            return list;
        }

        public override bool FilterSchema(string schema)
        {
            return !DatabaseName.ToUpper().Equals(schema.ToUpper());
        }
    }
}
