﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace Panopticon.DataDirect.HivePivotalHD
{

    public class EnumLocalizedConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter,
                                System.Globalization.CultureInfo culture)
        {
            if (value != null)
            {
                // Build a resource key based on the enum type and value
                string Key = "UiServerTypeOption" + value.ToString();
                // Try to find the translation in the current application resources
                String translation = Panopticon.DataDirect.Properties.Resources.UiServerTypeOptionAutoDetect;
                switch (Key)
                {
                    case "UiServerTypeOptionAutodetect":
                        translation = Panopticon.DataDirect.Properties.Resources.UiServerTypeOptionAutoDetect;
                        break;
                    case "UiServerTypeOptionHiveServer1":
                        translation = Panopticon.DataDirect.Properties.Resources.UiServerTypeOptionHiveServer1;
                        break;
                    case "UiServerTypeOptionHiveServer2":
                        translation = Panopticon.DataDirect.Properties.Resources.UiServerTypeOptionHiveServer2;
                        break;
                }
                return translation;
            }
            // return empty string if value is null
            return String.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
                                    System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
}
