﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.DataDirect.HivePivotalHD
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : OdbcPluginBase<HivePivotalHDConnectionSettings>
    {
        internal const string PluginId = "DDOdbcHivePivotalHD";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Pivotal HD Hive";
        internal const string PluginType = DataPluginTypes.Database;

        public override HivePivotalHDConnectionSettings GetSettings(PropertyBag properties,
            IEnumerable<ParameterValue> parameters)
        {
            return new HivePivotalHDConnectionSettings(properties, parameters);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override string DriverName
        {
            get { return "Datawatch 7.1.5 Apache Hive Wire Protocol"; }
        }
}
}
