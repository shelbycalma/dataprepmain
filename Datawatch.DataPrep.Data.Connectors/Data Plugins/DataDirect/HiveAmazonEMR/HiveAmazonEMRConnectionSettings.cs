﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.DataDirect.HiveAmazonEMR
{
    [DataHelpKey("HiveAmazonEMR")]
    public class HiveAmazonEMRConnectionSettings : DataDirectSettingsBase
    {

        public HiveAmazonEMRConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public HiveAmazonEMRConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.HadoopHive)
        {
            //set default Port number
            Port = "10000";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "SHOW DATABASES";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "10000");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public WireProtocolVersion WireProtocolVersion
        {
            get
            {
                return (WireProtocolVersion)Enum.Parse(typeof(WireProtocolVersion),
                       GetInternal("WireProtocolVersion", "Autodetect"));
            }
            set
            {
                SetInternal("WireProtocolVersion", value.ToString());
                FirePropertyChanged("WireProtocolVersion");
            }
        }

        public WireProtocolVersion[] WireProtocolVersions
        {
            get
            {
                return (WireProtocolVersion[])
                    Enum.GetValues(typeof(WireProtocolVersion));
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Apache Hive Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiHiveAmazonEMRPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiHiveAmazonEMRWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                return string.Format("WPV={0:D};UseNativeCatalogFunctions=1",
                    Enum.Parse(typeof(WireProtocolVersion),
                    WireProtocolVersion.ToString()));
            }
        }

        public override bool FilterSchema(string schema)
        {
            return !DatabaseName.ToUpper().Equals(schema.ToUpper());
        }

    }
}
