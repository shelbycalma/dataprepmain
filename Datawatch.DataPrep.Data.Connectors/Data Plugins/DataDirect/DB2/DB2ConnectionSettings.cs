﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.DB2
{
    [DataHelpKey("IBM_DB2")]
    public class DB2ConnectionSettings : DataDirectSettingsBase
    {

        public DB2ConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public DB2ConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.DB2)
        {
            //set default Port number
            Port = "50000";
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "50000");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string PluginDriverName
        {
            get 
            {
                return "DB2 Wire Protocol";
            }
        }

        protected override string HostKey
        {
            get
            {
                return "ip";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiDB2PluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiDB2WindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

    }
}
