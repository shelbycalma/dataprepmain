﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.Oracle
{
    [DataHelpKey("OracleDatabase")]
    public class OracleConnectionSettings : DataDirectSettingsBase
    {
        private List<EncryptionMethod> encryptionMethodChoices = new List<EncryptionMethod>();
        private EncryptionMethod encryptionOption = new EncryptionMethod();
        private EncryptionMethod selectedEncryptionMethod = new EncryptionMethod();

        public OracleConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public OracleConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.Oracle)
        {
            //set default Port number
            //Port = "1521";       
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "1521");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (ConnectionType == Oracle.ConnectionType.Standard)
                {
                    if (string.IsNullOrEmpty(Host) ||
                       string.IsNullOrEmpty(Port))
                    {
                        return false;
                    }

                    if (StandardConnectionMode ==
                        Oracle.StandardConnectionMode.SID &&
                        string.IsNullOrEmpty(SID))
                    {
                        return false;
                    }
                    else if (StandardConnectionMode ==
                        Oracle.StandardConnectionMode.ServiceName &&
                        string.IsNullOrEmpty(ServiceName))
                    {
                        return false;
                    }
                }
                else if (string.IsNullOrEmpty(ServerName) ||
                        string.IsNullOrEmpty(TNSNamesFile))
                {
                    return false;
                }

                return true;
            }
        }

        public ConnectionType ConnectionType
        {
            get
            {
                return (ConnectionType)Enum.Parse(typeof(ConnectionType),
                    GetInternal("ConnectionType", "Standard"));
            }
            set
            {
                SetInternal("ConnectionType", value.ToString());
            }
        }

        public string SID
        {
            get { return GetInternal("SID"); }
            set { SetInternal("SID", value); }
        }

        public string ServiceName
        {
            get { return GetInternal("ServiceName"); }
            set { SetInternal("ServiceName", value); }
        }

        public string ServerName
        {
            get { return GetInternal("ServerName"); }
            set { SetInternal("ServerName", value); }
        }

        public string TNSNamesFile
        {
            get { return GetInternal("TNSNamesFile"); }
            set { SetInternal("TNSNamesFile", value); }
        }

        public string EditionName
        {
            get { return GetInternal("EditionName"); }
            set { SetInternal("EditionName", value); }
        }

        public StandardConnectionMode StandardConnectionMode
        {
            get
            {
                return (StandardConnectionMode)Enum.Parse(typeof(StandardConnectionMode),
                    GetInternal("StandardConnectionMode", "SID"));
            }
            set
            {
                SetInternal("StandardConnectionMode", value.ToString());
                FirePropertyChanged("StandardConnectionText");
            }
        }

        public string StandardConnectionText
        {
            get
            {
                return StandardConnectionMode == StandardConnectionMode.SID
                    ? SID
                    : ServiceName;
            }
            set
            {
                if (StandardConnectionMode == StandardConnectionMode.SID)
                {
                    SID = value;
                }
                else
                {
                    ServiceName = value;
                }
            }
        }

        public StandardConnectionMode[] StandardConnectionModes
        {
            get
            {
                return (StandardConnectionMode[])Enum.GetValues(typeof(StandardConnectionMode));
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Oracle Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiOraclePluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiOracleWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool FilterSchema(string schema)
        {
            List<string> schemas = base.RestrictedSchemas;
            schemas.Add("ANONYMOUS");
            schemas.Add("APEX_PUBLIC_USER");
            schemas.Add("CTXSYS");
            schemas.Add("DBSNMP");
            schemas.Add("DIP");
            schemas.Add("EXFSYS");
            schemas.Add("FLOWS_%");
            schemas.Add("FLOWS_FILES");
            schemas.Add("LBACSYS");
            schemas.Add("MDDATA");
            schemas.Add("MDSYS");
            schemas.Add("MGMT_VIEW");
            schemas.Add("OLAPSYS");
            schemas.Add("ORACLE_OCM");
            schemas.Add("ORDDATA");
            schemas.Add("ORDPLUGINS");
            schemas.Add("ORDSYS");
            schemas.Add("OUTLN");
            schemas.Add("OWBSYS");
            schemas.Add("SI_INFORMTN_SCHEMA");
            schemas.Add("SPATIAL_CSW_ADMIN_USR");
            schemas.Add("SPATIAL_WFS_ADMIN_USR");
            schemas.Add("SYS");
            schemas.Add("SYSMAN");
            schemas.Add("SYSTEM");
            schemas.Add("WKPROXY");
            schemas.Add("WKSYS");
            schemas.Add("WK_TEST");
            schemas.Add("WMSYS");
            schemas.Add("XDB");
            schemas.Add("XS$NULL");
            schemas.Add("APEX_[0-9]*");
            return schemas.Any(s => Regex.IsMatch(schema.ToUpper(), s));
        }

        public virtual string TrustStore
        {
            get { return GetInternal("TrustStore", ""); }
            set { SetInternal("TrustStore", value); }
        }

        public virtual string TrustStorePassword
        {
            get { return GetInternal("TrustStorePassword", ""); }
            set { SetInternal("TrustStorePassword", value); }
        }
        public virtual string KeyStore
        {
            get { return GetInternal("KeyStore", ""); }
            set { SetInternal("KeyStore", value); }
        }

        public virtual string KeyStorePassword
        {
            get { return GetInternal("KeyStorePassword", ""); }
            set { SetInternal("KeyStorePassword", value); }
        }
        public virtual string KeyPassword
        {
            get { return GetInternal("KeyPassword", ""); }
            set { SetInternal("KeyPassword", value); }
        }

        public virtual string HostNameInCert
        {
            get { return GetInternal("HostNameInCert", ""); }
            set { SetInternal("HostNameInCert", value); }
        }

        public EncryptionMethod SelectedEncryptionMethod
        {
            get
            {
                if (selectedEncryptionMethod.EncryptionCode == null)
                {
                    SelectedEncryptionMethodID = GetInternal("SelectectedEncyptionMethodID", "0");
                    selectedEncryptionMethod = EncryptionMethodChoices.Find(x => x.EncryptionCode == SelectedEncryptionMethodID);
                }
                else
                    SelectedEncryptionMethodID = selectedEncryptionMethod.EncryptionCode;
                IsSSL = (selectedEncryptionMethod.EncryptionCode == "0") ? false : true;
                IsSSLAuto = (selectedEncryptionMethod.EncryptionCode == "1") ? true : false;
                return selectedEncryptionMethod;
            }
            set
            {
                selectedEncryptionMethod = value;
                SelectedEncryptionMethodID = value.EncryptionCode;
                IsSSL = (value.EncryptionCode == "0") ? false : true;
                IsSSLAuto = (selectedEncryptionMethod.EncryptionCode == "1") ? true : false;
                IsValidatedServer = (IsSSL && ValidateServer) ? true : false;
                FirePropertyChanged("SelectedEncryptionMethodID");
                FirePropertyChanged("ValidateServer");
            }
        }

        public bool IsValidatedServer
        {
            get { return Convert.ToBoolean(GetInternal("IsValidatedServer", "false")); }
            set { SetInternal("IsValidatedServer", value.ToString()); }
        }

        public virtual bool ValidateServer
        {
            get
            {
                return Convert.ToBoolean(GetInternal("ValidateServer", "false"));
            }
            set
            {
                SetInternal("ValidateServer", value.ToString());
                IsValidatedServer = (IsSSL && value) ? true : false;
            }
        }

        public String SelectedEncryptionMethodID
        {
            get { return GetInternal("SelectectedEncyptionMethodID", "0"); }
            set { SetInternal("SelectectedEncyptionMethodID", value); }
        }

        public List<EncryptionMethod> EncryptionMethodChoices
        {
            get
            {
                if (encryptionMethodChoices.Count == 0)
                {
                    encryptionOption.EncryptionCode = "0";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionNone;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "1";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionSSL;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "3";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionSSL3;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "4";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionSSL2;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "5";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionTLS1;
                    encryptionMethodChoices.Add(encryptionOption);
                    //encryptionOption = new EncryptionMethod();
                    //encryptionOption.EncryptionCode = "6";
                    //encryptionOption.Encryption = Properties.Resources.UiEncryptionRequestSSL;
                    //encryptionMethodChoices.Add(encryptionOption);
                    //encryptionOption = new EncryptionMethod();
                    //encryptionOption.EncryptionCode = "7";
                    //encryptionOption.Encryption = Properties.Resources.UiEncryptionLogInSSL;
                    //encryptionMethodChoices.Add(encryptionOption);
                }
                return (encryptionMethodChoices);
            }
        }

        public virtual bool TLSv12
        {
            get { return Convert.ToBoolean(GetInternal("TLSv12", "true")); }
            set { SetInternal("TLSv12", value.ToString()); }
        }

        public virtual bool TLSv11
        {
            get { return Convert.ToBoolean(GetInternal("TLSv11", "true")); }
            set { SetInternal("TLSv11", value.ToString()); }
        }

        public virtual bool TLSv1
        {
            get { return Convert.ToBoolean(GetInternal("TLSv1", "true")); }
            set { SetInternal("TLSv1", value.ToString()); }
        }

        public virtual bool SSLv3
        {
            get { return Convert.ToBoolean(GetInternal("SSLv3", "true")); }
            set { SetInternal("SSLv3", value.ToString()); }
        }

        public virtual bool SSLv2
        {
            get { return Convert.ToBoolean(GetInternal("SSLv2", "false")); }
            set { SetInternal("SSLv2", value.ToString()); }
        }

        public bool IsSSL
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsSSL", "false"));
            }
            set
            {
                SetInternal("IsSSL", value.ToString());
                //FirePropertyChanged("IsValidatedServer");
            }
        }

        public bool IsSSLAuto
        {
            get
            {
                return Convert.ToBoolean(GetInternal("IsSSLAuto", "false"));
            }
            set
            {
                SetInternal("IsSSLAuto", value.ToString());
                //FirePropertyChanged("IsValidatedServer");
            }
        }

        protected override string GetConnectionString()
        {
            StringBuilder connectionString = new StringBuilder();
            connectionString.Append(@"Driver={");

            connectionString.Append(DriverPrefix);
            connectionString.Append(DriverVersion);

            connectionString.Append(PluginDriverName + "}");

            if (ConnectionType == ConnectionType.Standard)
            {
                if (!string.IsNullOrEmpty(Host))
                {
                    connectionString.Append(";HOST=" + Host);
                }
                if (!string.IsNullOrEmpty(Port))
                {
                    connectionString.Append(";PORT=" + Port);
                }
                if (StandardConnectionMode ==
                    Oracle.StandardConnectionMode.SID &&
                    !string.IsNullOrEmpty(SID))
                {
                    connectionString.Append(";SID=" + SID);
                }
                else if (StandardConnectionMode ==
                    Oracle.StandardConnectionMode.ServiceName &&
                    !string.IsNullOrEmpty(ServiceName))
                {
                    connectionString.Append(";SN=" + ServiceName);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ServerName))
                {
                    connectionString.Append(";SRVR=" + ServerName);
                }
                if (!string.IsNullOrEmpty(TNSNamesFile))
                {
                    connectionString.Append(";TNF=" + TNSNamesFile);
                }
            }

            if (!string.IsNullOrEmpty(UserId))
            {
                connectionString.Append(";UID=" + UserId);
            }
            if (!string.IsNullOrEmpty(Password))
            {
                connectionString.Append(";PWD=" + Password);
            }


            if (!string.IsNullOrEmpty(EditionName))
            {
                connectionString.Append(";EN=" + EditionName);
            }

            if (!string.IsNullOrEmpty(SelectedEncryptionMethodID))
            {
                connectionString.Append(";EncryptionMethod=" + SelectedEncryptionMethodID);
            }

            string cryptoProtocol = string.Empty;
            if (SelectedEncryptionMethodID == "1")
            {
                if (TLSv12)
                    cryptoProtocol += "TLSv1.2";
                if (TLSv11)
                    cryptoProtocol += (cryptoProtocol.Length > 0) ? ", TLSv1.1" : "TLSv1.1";
                if (TLSv1)
                    cryptoProtocol += (cryptoProtocol.Length > 0) ? ", TLSv1" : "TLSv1";
                if (SSLv3)
                    cryptoProtocol += (cryptoProtocol.Length > 0) ? ", SSLv3" : "SSLv3";
                if (SSLv2)
                    cryptoProtocol += (cryptoProtocol.Length > 0) ? ", SSLv2" : "SSLv2";
                if (cryptoProtocol.Length > 0)
                    connectionString.Append(";CryptoProtocolVersion=" + cryptoProtocol);
            }

            if (ValidateServer && IsSSL)
            {
                connectionString.Append(";ValidateServerCertificate=1");

                if (TrustStore.Length > 0)
                    connectionString.Append(";Truststore=" + TrustStore);

                if (TrustStorePassword.Length > 0)
                    connectionString.Append(";TruststorePassword=" + TrustStorePassword);

                if (HostNameInCert.Length > 0)
                    connectionString.Append(";HostNameInCertificate=" + HostNameInCert);
            }

            if (IsSSL)
            {
                if (KeyStore.Length > 0)
                    connectionString.Append(";Keystore=" + KeyStore);
                if (KeyStorePassword.Length > 0)
                    connectionString.Append(";KeystorePassword=" + KeyStorePassword);
                if (KeyPassword.Length > 0)
                    connectionString.Append(";KeyPassword=" + KeyPassword);
            }

            // To stop unwanted error/exeption messageboxes.
            connectionString.Append(";DEB=1");
            connectionString.Append(";EnableNcharSupport=1");

            // Add other user configured connection options.
            // http://media.datadirect.com/download/docs/odbc/allodbc/help.html?_ga=1.244316421.1076730174.1395324411#page/userguide/rfi1363234100172.html
            if (!string.IsNullOrEmpty(ConnectionOptions))
            {
                connectionString.Append(";" + ConnectionOptions);
            }

            return connectionString.ToString();
        }                
    }
}
