﻿namespace Panopticon.DataDirect.Oracle
{
    public enum StandardConnectionMode
    {
        SID,
        ServiceName
    }
}
