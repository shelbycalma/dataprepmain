﻿using System.Collections.Generic;
using System.Linq;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.Informix
{
    [DataHelpKey("Informix")]
    public class InformixConnectionSettings : DataDirectSettingsBase
    {

        private IEnumerable<ParameterValue> parameters;

        public InformixConnectionSettings()
            : this(new PropertyBag(), null)
        {
            restrictedTables.Add("SYS");
        }

        public override string SystemDatabaseName
        {
            get { return "sysmaster"; }
        }

        public InformixConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.Informix)
        {
            //set default Port number
            Port = "9091";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "select name from sysdatabases";
            }
        }

        public override string Host
        {
            get { return GetInternal("Host", "localhost"); }
            set { SetInternal("Host", value); }
        }

        public string Server
        {
            get { return GetInternal("Server"); }
            set { SetInternal("Server", value); }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Informix Wire Protocol";
            }
        }

        public override bool FilterTable(string table)
        {
            return RestrictedTables.Any(s => table.ToUpper().StartsWith(s));
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiInformixPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiInformixWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                return "SRVR=" + Server;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "9091");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

    }
}
