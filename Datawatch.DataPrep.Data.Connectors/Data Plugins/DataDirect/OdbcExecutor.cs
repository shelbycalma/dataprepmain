﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Odbc;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework;
using Datawatch.DataPrep.Data.Framework.Model;

namespace Panopticon.DataDirect
{
    internal class OdbcExecutor : OdbcExecutorBase
    {
        private static HashSet<string> unlockedDrivers = new HashSet<string>();
        // These queries contains prefix and passwords created in Progress partlink site.
        // D9 prefix is used in both 64 and 32 bit branding.
        private const string unlockQuery32Bit = "{IVD9.LIC,1Dr67H_e6YgV!8u#675lV9S2}";
        private const string unlockQuery64Bit = "{DDD9.LIC,1Dr67H_e6YgV!8u#675lV9S2}";

        public override DbDataAdapter GetAdapter(string queryString,
            string connectionString)
        {
            OdbcDataAdapter adapter =
                new OdbcDataAdapter(queryString, connectionString);
            DoUnlock(adapter.SelectCommand.Connection);
            return adapter;
        }

        public override DbConnection GetConnection(string connectionString)
        {
            OdbcConnection connection = new OdbcConnection(connectionString);
            DoUnlock(connection);
            return connection;
        }

        private void DoUnlock(OdbcConnection connection)
        {
            DbConnectionStringBuilder csb = new DbConnectionStringBuilder();
            csb.ConnectionString = connection.ConnectionString;
            string driver = null;
            if (csb.ContainsKey("driver"))
            {
                driver = csb["driver"].ToString();
            }
            if (!string.IsNullOrEmpty(driver))
            {
                if (unlockedDrivers.Contains(driver)) return;
            }

            OdbcCommand cmd =
                new OdbcCommand(Environment.Is64BitProcess ?
                    unlockQuery64Bit : unlockQuery32Bit,
                    connection);
            bool doCloseConnection = false;

            try
            {
                if (connection.State != ConnectionState.Open)
                {
                    connection.Open();
                    doCloseConnection = true;
                }
                //first thing to do is to execuete the unlock
                cmd.ExecuteNonQuery();

            }
            catch (OdbcException exception)
            {
                // TODO: This error handling needs to be optimised.
                // It is so far based on suggestion from Progress
                // And should be fixed when they correct some API behaviour.
                for (int i = 0; i < exception.Errors.Count; i++)
                {
                    //in case of DEB=1 i connection string 
                    if (exception.Errors[i].NativeError.Equals(6091))
                    {
                        Log.Exception(Properties.Resources.UiSqlServerPluginTitle +
                            ": Unlocking Failed");                        
                    }
                    if (exception.Errors[i].NativeError.Equals(6082) &&
                        exception.Errors[i].Message.Contains(
                        "successfully unlocked"))
                    {
                        Log.Exception(exception.Errors[i].Message);
                        //Eureka
                    }
                    else if(exception.Errors[i].Message.Contains(
                        "Incorrect syntax near '{'") ||
                        exception.Errors[i].Message.Contains("D9.LIC"))
                    {
                        Log.Exception(exception.Errors[i].Message);
                        // TODO: Should find a better way to handle this.
                        // Weird random error while executing unlock query.
                    }
                    else if (exception.Errors[i].Message.Contains("126:") &&
                             exception.Errors[i].Message.Contains("Teradata"))
                    {
                        Log.Exception(Properties.Resources.ExTeradataConnectionError);
                        throw new Exception(Properties.Resources.ExTeradataConnectionError);
                    }

                    else
                    {
                        Log.Exception(exception.Errors[i].Message);
                        throw;
                    }
                }
            }
            if (doCloseConnection)
            {
                connection.Close();
            }

            unlockedDrivers.Add(driver);
        }
    }
}
