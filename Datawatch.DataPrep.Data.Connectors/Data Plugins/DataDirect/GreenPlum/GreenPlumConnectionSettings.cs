﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DataDirect.Greenplum
{
    [DataHelpKey("MySQL")]
    public class GreenplumConnectionSettings : DataDirectSettingsBase
    {
        private IEnumerable<ParameterValue> parameters;

        public GreenplumConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public GreenplumConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.PostgreSQL)
        {
            this.parameters = parameters;

            //set default Port number
            Port = "5432";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "SELECT datname FROM pg_database" +
                    " WHERE datistemplate = false;";
            }
        }

        public override string SystemDatabaseName
        {
            get { return "template1"; }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "5432");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public virtual bool IsLegacy
        {
            get { return false; }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Greenplum Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiGreenplumPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiGreenPlumWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }
    }
}
