﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.SqlServer
{
    [DataHelpKey("SQLServer")]
    public class SqlServerConnectionSettings : DataDirectSettingsBase
    {
        private List<EncryptionMethod> encryptionMethodChoices = new List<EncryptionMethod>();
        private EncryptionMethod encryptionOption = new EncryptionMethod();
        private EncryptionMethod selectedEncryptionMethod = new EncryptionMethod();

        public SqlServerConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public SqlServerConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.SQLServer)
        {
            //set default Port number
            Port = "1433";
        }


        protected override string DatabaseListQuery
        {
            get
            {
                return "Select name from sys.databases";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "1433");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public virtual bool IsLegacy
        {
            get { return false; }
        }

        public override string PluginDriverName
        {
            get 
            {
                if (IsLegacy)
                {
                    return "SQL Server Legacy Wire Protocol";
                }
                return "SQL Server Wire Protocol";
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                // To enable quoted identifier,
                // our SQL Server dialect adds qotes to column/table names.
                string authentication = string.Empty;
                if (Authentication == AuthenticationType.Windows)
                    authentication = "AM=4";

                string trustStoreDetails = string.Empty;
                if (ValidateServer && IsSSL)
                {
                    if (authentication.Length == 0)
                        authentication = "ValidateServerCertificate=1";
                    else
                        authentication = authentication + ";ValidateServerCertificate=1";

                    if (TrustStore.Length > 0)
                        trustStoreDetails = ";Truststore=" + TrustStore;

                    if (TrustStorePassword.Length > 0)
                        trustStoreDetails += ";TruststorePassword=" + TrustStorePassword;
                }
                else
                {
                    authentication += (authentication.Length == 0) ? "ValidateServerCertificate=0" : ";ValidateServerCertificate=0";
                }

                string encryptionMethodID = string.Empty;
                if (SelectedEncryptionMethodID.Length > 0)
                {
                    encryptionMethodID = ";EncryptionMethod=" + SelectedEncryptionMethodID;
                }

                string cryptoProtocol = string.Empty;
                if (SelectedEncryptionMethodID != "0")
                {
                    if (TLSv12)
                        cryptoProtocol += "TLSv1.2";
                    if (TLSv11)
                        cryptoProtocol += (cryptoProtocol.Length > 0) ? ", TLSv1.1" : "TLSv1.1";
                    if (TLSv1)
                        cryptoProtocol += (cryptoProtocol.Length > 0) ? ", TLSv1" : "TLSv1";
                    if (SSLv3)
                        cryptoProtocol += (cryptoProtocol.Length > 0) ? ", SSLv3" : "SSLv3";
                    if (SSLv2)
                        cryptoProtocol += (cryptoProtocol.Length > 0) ? ", SSLv2" : "SSLv2";
                    if (cryptoProtocol.Length > 0)
                        cryptoProtocol = ";CryptoProtocolVersion=" + cryptoProtocol;
                }
                return "EQI=1;" + authentication + trustStoreDetails + encryptionMethodID + cryptoProtocol;
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSqlServerPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSQLServerWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool ShowTimeSpanBaseTime
        {
            get
            {
                return true;
            }
        }

        public AuthenticationType Authentication
        {
            get
            {
                string s = GetInternal("Authentication");
                if (s != null)
                {
                    try
                    {
                        return (AuthenticationType)Enum.Parse(
                            typeof(AuthenticationType), s);
                    }
                    catch
                    {
                    }
                }
                return AuthenticationType.Windows;
            }

            set
            {
                if(value == AuthenticationType.Windows)
                {
                    UserId = string.Empty;
                    Password = string.Empty;
                }
                SetInternal("Authentication", value.ToString());
            }
        }

        public virtual bool TLSv12
        {
            get { return Convert.ToBoolean(GetInternal("TLSv12", "true")); }
            set { SetInternal("TLSv12", value.ToString()); }
        }

        public virtual bool TLSv11
        {
            get { return Convert.ToBoolean(GetInternal("TLSv11", "true")); }
            set { SetInternal("TLSv11", value.ToString()); }
        }

        public virtual bool TLSv1
        {
            get { return Convert.ToBoolean(GetInternal("TLSv1", "true")); }
            set { SetInternal("TLSv1", value.ToString()); }
        }

        public virtual bool SSLv3
        {
            get { return Convert.ToBoolean(GetInternal("SSLv3", "true")); }
            set { SetInternal("SSLv3", value.ToString()); }
        }

        public virtual bool SSLv2
        {
            get { return Convert.ToBoolean(GetInternal("SSLv2", "false")); }
            set { SetInternal("SSLv2", value.ToString()); }
        }

        public virtual string TrustStore
        {
            get { return GetInternal("TrustStore", ""); }
            set { SetInternal("TrustStore", value); }
        }

        public virtual string TrustStorePassword
        {
            get { return GetInternal("TrustStorePassword", ""); }
            set { SetInternal("TrustStorePassword", value); }
        }

        public bool IsSSL
        {
            get { return Convert.ToBoolean(GetInternal("IsSSL", "false")); }
            set
            {
                SetInternal("IsSSL", value.ToString());
                FirePropertyChanged("IsValidatedServer");
            }
        }

        public bool IsValidatedServer
        {
            get { return Convert.ToBoolean(GetInternal("IsValidatedServer", "false")); }
            set { SetInternal("IsValidatedServer", value.ToString()); }
        }

        public EncryptionMethod SelectedEncryptionMethod
        {
            get
            {
                if (selectedEncryptionMethod.EncryptionCode == null)
                {
                    SelectedEncryptionMethodID = GetInternal("SelectectedEncyptionMethodID", "0");
                    selectedEncryptionMethod = EncryptionMethodChoices.Find(x => x.EncryptionCode == SelectedEncryptionMethodID);
                }
                else
                    SelectedEncryptionMethodID = selectedEncryptionMethod.EncryptionCode;
                IsSSL = (selectedEncryptionMethod.EncryptionCode == "0") ? false : true;
                return selectedEncryptionMethod;
            }
            set
            {
                selectedEncryptionMethod = value;
                SelectedEncryptionMethodID = value.EncryptionCode;
                IsSSL = (value.EncryptionCode == "0") ? false : true;
                IsValidatedServer = (IsSSL && ValidateServer) ? true : false;
                FirePropertyChanged("SelectedEncryptionMethodID");
                FirePropertyChanged("ValidateServer");
            }
        }

        public String SelectedEncryptionMethodID
        {
            get { return GetInternal("SelectectedEncyptionMethodID", "0"); }
            set { SetInternal("SelectectedEncyptionMethodID", value); }
        }

        public virtual bool ValidateServer
        {
            get { return Convert.ToBoolean(GetInternal("ValidateServer", "false")); }
            set
            {
                SetInternal("ValidateServer", value.ToString());
                IsValidatedServer = (IsSSL && value) ? true : false;
            }
        }

        public List<EncryptionMethod> EncryptionMethodChoices
        {
            get
            {
                if (encryptionMethodChoices.Count == 0)
                {
                    encryptionOption.EncryptionCode = "0";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionNone;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "1";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionSSL;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "6";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionRequestSSL;
                    encryptionMethodChoices.Add(encryptionOption);
                    encryptionOption = new EncryptionMethod();
                    encryptionOption.EncryptionCode = "7";
                    encryptionOption.Encryption = Properties.Resources.UiEncryptionLogInSSL;
                    encryptionMethodChoices.Add(encryptionOption);
                }
                return (encryptionMethodChoices);
            }
        }

    }
}
