﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Panopticon.DataDirect.SqlServer
{
    public class EncryptionMethod
    {
        public string EncryptionCode
        {
            get;
            set;
        }
        public string Encryption
        {
            get;
            set;
        }
    }
}
