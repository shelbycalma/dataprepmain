﻿using System.Collections.Generic;
using System.Text;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DataDirect
{
    public abstract class DataDirectSettingsBase : DatabaseSettingsBase
    {
        public const string DriverPrefix = "Datawatch ";

        public DataDirectSettingsBase(PropertyBag bag,
            IEnumerable<ParameterValue> parameters, SqlDialect sqlDialect)
            : base(bag, parameters, sqlDialect, new OdbcExecutor())
        {
        }

        protected virtual string DriverVersion
        {
            get { return "7.1.5 "; }
        }

        public abstract string PluginDriverName { get; }

        public override string DriverName
        {
            get { return DriverPrefix + DriverVersion + PluginDriverName; }
        }

        // Should be overridden by plugins to show text/hide textbox to set TimeSpanBaseTime.
        // Looks like needed only in SQL Server.
        public virtual bool ShowTimeSpanBaseTime
        {
            get
            {
                return false;
            }
        }

        public string ConnectionOptions
        {
            get { return GetInternal("ConnectionOptions"); }
            set { SetInternal("ConnectionOptions", value); }
        }

        protected override string GetConnectionString(bool useSystemDatabase)
        {
            StringBuilder connectionString =
                new StringBuilder(base.GetConnectionString(useSystemDatabase));

            // To stop unwanted error/exeption messageboxes.
            connectionString.Append(";DEB=1");

            // Add other user configured connection options.
            // http://media.datadirect.com/download/docs/odbc/allodbc/help.html?_ga=1.244316421.1076730174.1395324411#page/userguide/rfi1363234100172.html
            if (!string.IsNullOrEmpty(ConnectionOptions))
            {
                connectionString.Append(";" + ConnectionOptions);
            }

            return connectionString.ToString();
        }
    }
}
