﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.DataDirect.Impala
{
    [DataHelpKey("ClouderaImpala")]
    public class ImpalaConnectionSettings : DataDirectSettingsBase
    {

        public ImpalaConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public ImpalaConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.Impala)
        {
            //set default Port number
            Port = "21050";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "Show databases";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "21050");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string PluginDriverName
        {
            get 
            {
                return "Impala Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiImpalaPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiImpalaWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

    }
}
