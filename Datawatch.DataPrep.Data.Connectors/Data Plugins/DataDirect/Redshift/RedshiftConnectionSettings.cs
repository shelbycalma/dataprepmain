﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.Redshift
{
    [DataHelpKey("AmazonRedshift")]
    public class RedshiftConnectionSettings : DataDirectSettingsBase
    {

        public RedshiftConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public RedshiftConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.Redshift)
        {
            //set default Port number
            Port = "5439";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "SELECT datname FROM pg_database" +
                    " WHERE datistemplate = false;";
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "5439");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string SystemDatabaseName
        {
            get { return "dev"; }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Amazon Redshift Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiRedshiftPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiRedshiftWindowTitle;
            }
        }

        public override bool FilterSchema(string schema)
        {
            List<string> schemas = base.RestrictedSchemas;
            schemas.Add("PG_CATALOG");
            schemas.Add("PG_TEMP_X");
            schemas.Add("PG_TOAST");
            return schemas.Contains(schema.ToUpper());
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }
    }
}
