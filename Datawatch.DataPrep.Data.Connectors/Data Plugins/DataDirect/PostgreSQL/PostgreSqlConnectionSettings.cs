﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.PostgreSQL
{
    [DataHelpKey("PostgreSQL")]
    public class PostgreSqlConnectionSettings : DataDirectSettingsBase
    {

        public PostgreSqlConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }
        //what should be the sqldialect for this?
        public PostgreSqlConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.PostgreSQL)
        {
            //set default Port number
            Port = "5432";
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "SELECT datname FROM pg_database" +
                    " WHERE datistemplate = false;";
            }
        }

        public override string SystemDatabaseName
        {
            get { return "postgres"; }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "5432");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "PostgreSQL Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiPostgreSQLPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiPostgreSQLWindowTitle;
            }
        }

        public override bool FilterSchema(string schema)
        {
            List<string> schemas = base.RestrictedSchemas;
            schemas.Add("PG_CATALOG");
            schemas.Add("PG_TEMP_X");
            schemas.Add("PG_TOAST");
            return schemas.Contains(schema.ToUpper());
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }
    }
}
