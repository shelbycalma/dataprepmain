﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Query;


namespace Panopticon.DataDirect.dBase
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : OdbcPluginBase<dBaseConnectionSettings>
    {
        internal const string PluginId = "DDOdbcdBase";
        internal const bool PluginIsObsolete = true;
        internal const string PluginTitle = "dBase";
        internal const string PluginType = DataPluginTypes.Database;

        private dBaseConnectionSettings dBaseSettings = null;

        public override string DriverName
        {
            get { return "Datawatch 7.1.5 dBASEFile (*.dbf)"; }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override string[] GetDataFiles(PropertyBag settings)
        {
            dBaseSettings = new dBaseConnectionSettings(settings, null);
            SchemaAndTable selectedTable = dBaseSettings.QuerySettings.SelectedTable;

            if (!string.IsNullOrEmpty(dBaseSettings.Database) && !string.IsNullOrEmpty(selectedTable.Table))
            {
                string path = dBaseSettings.Database + "\\" + selectedTable.Table + ".dbf";
                return new[] { path };
            }

            return null;
        }

        public override dBaseConnectionSettings GetSettings(PropertyBag properties,
                                                           IEnumerable<ParameterValue> parameters)
        {
            return new dBaseConnectionSettings(properties, parameters);
        }
        public override void UpdateFileLocation(PropertyBag settings,
            string oldPath, string newPath, string workbookPath)
        {
            dBaseSettings = new dBaseConnectionSettings(settings, null);
            dBaseSettings.Database = Path.GetDirectoryName(newPath);
            dBaseSettings.QuerySettings.SetSelectedTable(Path.GetFileNameWithoutExtension(newPath));
        }
}
}
