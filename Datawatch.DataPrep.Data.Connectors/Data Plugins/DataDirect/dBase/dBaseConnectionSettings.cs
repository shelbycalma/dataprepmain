﻿using System.Text;
using System.IO;

using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DataDirect.dBase
{
    [DataHelpKey("dBase")]
    public class dBaseConnectionSettings : DataDirectSettingsBase
    {
        public dBaseConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public dBaseConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.dBase)
        {
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override bool IsFreeformSQLSupported
        {
            get
            {
                return false;
            }
        }

        public override bool IsOndemandSupported
        {
            get
            {
                return false;
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UidBaseDBPluginTitle;
            }
        }
        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UidBaseWindowTitle;
            }
        }

        public string Database
        {
            get
            {
                return GetInternal("Database");
            }
            set
            {
                SetInternal("Database", value);
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "dBASEFile (*.dbf)";
            }
        }

        protected override string GetConnectionString()
        {
            StringBuilder connectionString = new StringBuilder();
            connectionString.Append(@"Driver={");
            connectionString.Append(DriverPrefix);
            connectionString.Append(DriverVersion);
            connectionString.Append(PluginDriverName);
            connectionString.Append("}");

            if (!string.IsNullOrEmpty(Database))
            {
                connectionString.Append(";Database=" + Database);
            }

            connectionString.Append(";DEB=1");
            return connectionString.ToString();
        }

        public override SchemaAndTable[] GetTablesAndViews(string connectionString)
        {
            string[] DBFFiles = Directory.GetFiles(Database, "*.dbf");
            List<SchemaAndTable> list = new List<SchemaAndTable>();

            for (int i = 0; i < DBFFiles.Length; i++)
            {
                list.Add(new SchemaAndTable(null, Path.GetFileNameWithoutExtension(DBFFiles[i])));
            }
            return list.ToArray();
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Database))
                {
                    return false;
                }
                return true;
            }
        }
    }
}
