﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Connectors.Database;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;

namespace Panopticon.DataDirect.SybaseIQ
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : OdbcPluginBase<SybaseIQConnectionSettings>
    {
        internal const string PluginId = "DDOdbcSybaseIQ";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Sybase IQ";
        internal const string PluginType = DataPluginTypes.Database;

        public override SybaseIQConnectionSettings GetSettings(PropertyBag properties,
            IEnumerable<ParameterValue> parameters)
        {
            return new SybaseIQConnectionSettings(properties, parameters);
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override string Id
        {
            get { return PluginId; }
        }

        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string Title
        {
            get { return PluginTitle; }
        }

        public override string DriverName
        {
            get { return "Datawatch 7.1.5 Sybase IQ Wire Protocol"; }
        }
    }
}