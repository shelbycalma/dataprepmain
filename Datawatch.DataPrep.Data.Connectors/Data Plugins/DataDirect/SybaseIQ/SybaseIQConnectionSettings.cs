﻿using System.Collections.Generic;

using System.Text;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.SybaseIQ
{
    public class SybaseIQConnectionSettings : DataDirectSettingsBase
    {
        public SybaseIQConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public SybaseIQConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.SybaseAnywhere)
        {
            //set default Port number
            Port = "2638";
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "2638");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string PluginDriverName
        {
            get 
            {
                return "Sybase IQ Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiSybaseIQPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiSybaseIQWindowTitle;
            }
        }

        public override bool FilterSchema(string schema)
        {
            List<string> schemas = base.RestrictedSchemas;
            schemas.Add("DBO");
            schemas.Add("RS_SYSTABGROUP");
            return schemas.Contains(schema.ToUpper());
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        protected override string GetConnectionString()
        {
            StringBuilder connectionString = new StringBuilder();
            connectionString.Append(@"Driver={");
            connectionString.Append(DriverPrefix);
            connectionString.Append(DriverVersion);
            connectionString.Append(PluginDriverName);
            connectionString.Append("}");
            connectionString.AppendFormat(";NA={0}, {1};DB={4};UID={2};PWD={3}",
                Host, Port, UserId, Password, DatabaseName);
            // To stop unwanted error/exeption messageboxes.
            connectionString.Append(";DEB=1");
            // Add other user configured connection options.
            // http://media.datadirect.com/download/docs/odbc/allodbc/help.html?_ga=1.244316421.1076730174.1395324411#page/userguide/rfi1363234100172.html
            if (!string.IsNullOrEmpty(ConnectionOptions))
            {
                connectionString.Append(";");
                connectionString.Append(ConnectionOptions);
            }

            return connectionString.ToString();
        }
    }
}
