﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DataDirect.Teradata
{
    public class TeradataConnectionSettings : DataDirectSettingsBase
    {
        private IEnumerable<ParameterValue> parameters;
        public TeradataConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public TeradataConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.Teradata)
        {
            this.parameters = parameters;
            //set default Port number
            Port = "1025";
        }

        public bool IsIntegratedSecurity
        {
            get
            {
                return bool.Parse(GetInternal("IsIntegratedSecurity", false.ToString()));
            }
            set
            {
                SetInternal("IsIntegratedSecurity", value.ToString());
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "Teradata";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiTeradataPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiTeradataWindowTitle;
            }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "1025");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public SecurityMechanism[] SecurityMechanisms
        {
            get
            {
                return (SecurityMechanism[])
                    Enum.GetValues(typeof(SecurityMechanism));
            }
        }

        public string SecurityMechanism
        {
            get
            {
                return GetInternal("SecurityMechanism");
            }
            set
            {
                SetInternal("SecurityMechanism", value.ToString());
                FirePropertyChanged("SecurityMechanism");
            }
        }

        public string SecurityParameter
        {
            get
            {
                return GetInternal("SecurityParameter");
            }
            set
            {
                SetInternal("SecurityParameter", value);
            }
        }

        public string Realm
        {
            get
            {
                return GetInternal("Realm");
            }
            set
            {
                SetInternal("Realm", value);
            }
        }

        public string TDProfile
        {
            get
            {
                return GetInternal("TDProfile");
            }
            set
            {
                SetInternal("TDProfile", value);
            }
        }

        public string TDRole
        {
            get
            {
                return GetInternal("TDRole");
            }
            set
            {
                SetInternal("TDRole", value);
            }
        }

        public string TDUserName
        {
            get
            {
                return GetInternal("TDUserName");
            }
            set
            {
                SetInternal("TDUserName", value);
            }
        }

        protected override string HostKey
        {
            get { return "DBCName"; }
        }

        public string AccountString
        {
            get
            {
                return GetInternal("AccountString");
            }
            set
            {
                SetInternal("AccountString", value);
            }
        }

        public override bool CanLoadTables
        {
            get
            {
                if (string.IsNullOrEmpty(Host) ||
                       string.IsNullOrEmpty(UserId))
                {
                    return false;
                }

                return true;
            }
        }

        protected override string GetConnectionString()
        {
            StringBuilder connectionString = new StringBuilder();
            connectionString.Append(@"Driver={");

            connectionString.Append(DriverPrefix);
            connectionString.Append(DriverVersion);

            connectionString.Append(PluginDriverName + "}");

            if (!string.IsNullOrEmpty(Host))
            {
                connectionString.Append(";" + HostKey + "=" + Host);
            }

            if(!IsIntegratedSecurity)
            {
                if(!string.IsNullOrEmpty(SecurityParameter))
                {
                    connectionString.Append(";SP=" + SecurityParameter);
                }

                if (!string.IsNullOrEmpty(UserId))
                {
                    connectionString.Append(";uid=" + UserId);
                }
                if (!string.IsNullOrEmpty(Password))
                {
                    connectionString.Append(";pwd=" + Password);
                }
                if (!string.IsNullOrEmpty(SecurityMechanism))
                {
                    SecurityMechanism sm =
                        (SecurityMechanism)Enum.Parse(typeof(SecurityMechanism),
                           SecurityMechanism);
                    connectionString.Append(";SECM=" + SecurityMechanism);
                    switch (sm)
                    {
                        case Teradata.SecurityMechanism.KRB5:
                        case Teradata.SecurityMechanism.KRB5C:
                        case Teradata.SecurityMechanism.NTLM:
                        case Teradata.SecurityMechanism.NTLMC:
                            if (!string.IsNullOrEmpty(Realm))
                            {
                                connectionString.Append(";AD=" + Realm);
                            }
                            break;
                        case Teradata.SecurityMechanism.ldap:
                            if (!string.IsNullOrEmpty(Realm))
                            {
                                connectionString.Append(";AD=" + Realm);
                            }
                            if (!string.IsNullOrEmpty(TDUserName))
                            {
                                connectionString.Append(";TDUN=" + TDUserName);
                            }
                            if (!string.IsNullOrEmpty(TDProfile))
                            {
                                connectionString.Append(";TDP=" + TDProfile);
                            }
                            if (!string.IsNullOrEmpty(TDRole))
                            {
                                connectionString.Append(";TDR=" + TDRole);
                            }
                            break;
                    }
                }

            }
            if (!string.IsNullOrEmpty(Port))
            {
                connectionString.Append(";port=" + Port);
            }
            if (!string.IsNullOrEmpty(DatabaseName))
            {
                connectionString.Append(";db=" + DatabaseName);
            }
            if (!string.IsNullOrEmpty(AccountString))
            {
                connectionString.Append(";AS=" + AccountString);
            }

            // To stop unwanted error/exeption messageboxes.
            connectionString.Append(";DEB=1");

            // Add other user configured connection options.
            // http://media.datadirect.com/download/docs/odbc/allodbc/help.html?_ga=1.244316421.1076730174.1395324411#page/userguide/rfi1363234559603.html#
            if (!string.IsNullOrEmpty(ConnectionOptions))
            {
                connectionString.Append(";" + ConnectionOptions);
            }

            return connectionString.ToString();
        }

        public override SchemaAndTable[] GetTablesAndViews(string connectionString)
        {
            List<SchemaAndTable> list = new List<SchemaAndTable>();
            var tables = new DataTable();
            OdbcExecutor executor = new OdbcExecutor();

            using (DbConnection connection =
                executor.GetConnection(connectionString))
            {
                connection.Open();
                    tables = connection.GetSchema("Tables");
            }
            list.AddRange(GetSchemaAndTable(tables));
            return list.ToArray();
        }


    }
}
