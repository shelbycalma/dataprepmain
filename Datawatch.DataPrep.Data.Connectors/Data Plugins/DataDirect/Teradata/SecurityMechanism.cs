﻿
namespace Panopticon.DataDirect.Teradata
{
    public enum SecurityMechanism
    {
        KRB5, KRB5C, ldap, NTLM,
        NTLMC, TD1, TD2
    }
}
