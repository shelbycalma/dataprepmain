﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;


namespace Panopticon.DataDirect.MySql
{
    [DataHelpKey("MySQL")]
    public class MySqlConnectionSettings : DataDirectSettingsBase
    {
        private IEnumerable<ParameterValue> parameters;

        public MySqlConnectionSettings()
            : this(new PropertyBag(), null)
        {
            //set default Port number
            Port = "3306";
        }

        public MySqlConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.MySQL)
        {
            this.parameters = parameters;
        }

        protected override string DatabaseListQuery
        {
            get
            {
                return "Show databases";
            }
        }

        public override string SystemDatabaseName
        {
            get { return "mysql"; }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "3306");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public virtual bool IsLegacy
        {
            get { return false; }
        }

        public override string PluginDriverName
        {
            get
            {
                return "MySQL Wire Protocol";
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiMySqlPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiMySQLWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public override string[] SchemaRestrictions
        {
            get
            {
                return new string[] { DataUtils.ApplyParameters(
                DatabaseName, parameters, false) };
            }
        }
    }
}
