﻿
namespace Panopticon.DataDirect.HiveClouderaCDH
{
    public enum WireProtocolVersion
    {
        Autodetect = 0,
        HiveServer1 = 1,
        HiveServer2 = 2
    }
}
