﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Help;
using Datawatch.DataPrep.Data.Core.Query;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.DataDirect.MongoDB
{
    [DataHelpKey("MongoDB")]
    public class MongoDBConnectionSettings : DataDirectSettingsBase
    {
        public MongoDBConnectionSettings()
            : this(new PropertyBag(), null)
        {
        }

        public MongoDBConnectionSettings(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(bag, parameters, SqlDialectFactory.AnsiSql)
        {
             //set default Port number
            Port = "27017";
       }

        protected override string DriverVersion
        {
            get { return "8.0 "; }
        }

        public override string Port
        {
            get
            {
                return GetInternal("Port", "27017");
            }
            set
            {
                SetInternal("Port", value);
            }
        }

        public override string PluginDriverName
        {
            get
            {
                return "MongoDB";
            }
        }

        public override string PluginConnectionString
        {
            get
            {
                return "sd=" + SchemaDefinitionPath;
            }
        }

        public override string PluginTitle
        {
            get
            {
                return Properties.Resources.UiMongoDBPluginTitle;
            }
        }

        public override string WindowTitle
        {
            get
            {
                return Properties.Resources.UiMongoDBWindowTitle;
            }
        }

        public override bool IsPluginSettingsOk
        {
            get
            {
                return true;
            }
        }

        public string SchemaDefinitionPath
        {
            get
            {
                return GetInternal("SchemaDefinitionPath");
            }
            set
            {
                SetInternal("SchemaDefinitionPath", value);
            }
        }

    }
}
