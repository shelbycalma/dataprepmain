﻿using System;
using System.Collections.Generic;
using System.Xml;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;

namespace Panopticon.SMARTSPlugin
{
    public class ConnectionSettings : DatabasePlugin.DatabaseConnectionSettings
    {
        public ConnectionSettings(PropertyBag bag, 
            IEnumerable<ParameterValue> parameters) : base(bag,parameters)
        {
        }

        public bool IsServerSideOnly
        {
            get
            {
                string s= GetInternal("ServerSideOnly");
                if (string.IsNullOrEmpty(s)) return false;
                try
                {
                    return XmlConvert.ToBoolean(s);
                } 
                catch (FormatException)
                {
                }
                return false;
            }
            set
            {
                SetInternal("ServerSideOnly", XmlConvert.ToString(value));
            }
        }
    }
}
