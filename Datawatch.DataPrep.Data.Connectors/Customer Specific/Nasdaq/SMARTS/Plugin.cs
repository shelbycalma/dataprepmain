﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Table;

namespace Panopticon.SMARTSPlugin
{
    [LicenseProvider(typeof (DatawatchXmlLicenseProvider))]
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    public class Plugin : DatabasePlugin.Plugin, ILocationSpecific
    {
        internal const string PluginId = "SMARTSPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "Nasdaq SMARTS";
        internal const string PluginType = DataPluginTypes.Database;

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        /// <summary>
        /// Gets the plug-in id.
        /// </summary>
        public override string Id
        {
            get { return PluginId; }
        }

        /// <summary>
        /// Gets a value indicating whether the plug-in has a valid license.
        /// </summary>
        public override bool IsLicensed
        {
            get { return LicenseCache.IsValid(GetType(), this); }
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        /// <summary>
        /// Gets a value indicating the title of the plug-in.
        /// </summary>
        public override string Title
        {
            get { return PluginTitle; }
        }

        public override ITable GetData(string workbookDir, string dataDir, PropertyBag settings, IEnumerable<ParameterValue> parameters, int rowcount,
            bool applyRowFilteration)
        {
            return base.GetData(workbookDir, dataDir, settings, parameters, rowcount);
        }

        public DataLocation GetEnabledLocation(PropertyBag settings)
        {
            return new ConnectionSettings(settings, null).IsServerSideOnly ? 
                DataLocation.ServerSide : DataLocation.Any;
        }
    }
}