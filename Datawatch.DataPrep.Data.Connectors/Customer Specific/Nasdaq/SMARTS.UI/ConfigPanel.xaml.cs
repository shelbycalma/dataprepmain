﻿using System.ComponentModel;

using System.Windows;
using System.Windows.Controls;
using Panopticon.DatabasePlugin;
using Panopticon.DatabasePlugin.UI;

namespace Panopticon.SMARTSPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    internal partial class ConfigPanel :
        UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static readonly DependencyProperty ConnectionSettingsProperty =
            DependencyProperty.Register("ConnectionSettings",
                typeof(ConnectionSettings), typeof(ConfigPanel),
                new PropertyMetadata(new PropertyChangedCallback(
                    ConnectionSettings_Changed)));

        private JavaConnectionMode javaConnectionMode = 
            JavaConnectionMode.JndiName;
        
        public ConfigPanel(DatabaseConnectionSettings settings)
            : this()
        {
            ConnectionSettings = settings;
        }

        public ConfigPanel()
        {
            InitializeComponent();
            DataContext = this;
        }

        public DatabaseConnectionSettings ConnectionSettings
        {
            get { return (DatabaseConnectionSettings)GetValue(ConnectionSettingsProperty); }
            set { SetValue(ConnectionSettingsProperty, value); }
        }

        private static void ConnectionSettings_Changed(DependencyObject obj,
            DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnConnectionSettingsChanged(
                (DatabaseConnectionSettings)args.OldValue,
                (DatabaseConnectionSettings)args.NewValue);
        }

        private void OnConnectionSettingsChanged(DatabaseConnectionSettings oldSettings,
            DatabaseConnectionSettings newSettings)
        {

            if (oldSettings != null)
            {
                newSettings.PropertyChanged -=
                    new PropertyChangedEventHandler(
                        ConnectionSettings_PropertyChanged);
            }
            if (newSettings != null)
            {
                newSettings.PropertyChanged +=
                    new PropertyChangedEventHandler(
                        ConnectionSettings_PropertyChanged);
             
                JavaConnectionMode = string.IsNullOrEmpty(newSettings.JavaUrl)
                    ? JavaConnectionMode.JndiName : JavaConnectionMode.Url;

            }
        }

        private void ConnectionSettings_PropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "ConnectionStringActual")
            {
                //clear tables list
                if (ConnectionSettings != null)
                {
                    ConnectionSettings.QuerySettings.SelectedTable = null;
                }
                FireChanged("Tables");

            }
        }

        private void editConnectionButton_Click(object sender, RoutedEventArgs e)
        {
            string newConnectionString =
                Utils.ApplyParameters(ConnectionSettings.ConnectionString,
                                      ConnectionSettings.Parameters, false);
            newConnectionString =
                ConnectionDialogHelper.EditConnectionString(newConnectionString);

            if (newConnectionString != null)
            {
                ConnectionSettings.ConnectionString = newConnectionString;
            }
        }

        protected void FireChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        
        public bool IsOkey
        {
            get
            {
                if (ConnectionSettings == null)
                {
                    return false;
                }
                return queryPanel.IsOkey;
            }
        }

        public JavaConnectionMode JavaConnectionMode
        {
            get { return javaConnectionMode; }
            set
            {
                if (value == javaConnectionMode) return;
                JavaConnectionText = null;
                javaConnectionMode = value;
                FireChanged("JavaConnectionMode");
                FireChanged("JavaConnectionText");
            }
        }

        public string JavaConnectionText
        {
            get
            {
                if (ConnectionSettings == null)
                {
                    return null;
                }
                return javaConnectionMode == JavaConnectionMode.Url
                    ? ConnectionSettings.JavaUrl
                    : ConnectionSettings.JavaJndiResourceName;
            }
            set
            {
                if (javaConnectionMode == JavaConnectionMode.Url)
                {
                    ConnectionSettings.JavaUrl = value;
                }
                else
                {
                    ConnectionSettings.JavaJndiResourceName = value;
                }
            }
        }
    }    
}
