﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.DatabasePlugin.UI;

namespace Panopticon.SMARTSPlugin.UI
{
    public class PluginUI: DatabasePlugin.UI.PluginUI
    {
        public PluginUI(Plugin plugin, Window owner)
            : base(plugin, owner)
        {
        }

        public override PropertyBag DoConnect(IEnumerable<ParameterValue> parameters)
        {
            CheckLicense();

            parametersForTitle = parameters;
            string connectionString = ConnectionDialogHelper.ShowConnectionDialog();
            if (string.IsNullOrEmpty(connectionString))
            {
                return null;
            }
            ConnectionSettings connectionSettings =
                new ConnectionSettings(new PropertyBag(), parameters);
            connectionSettings.QuerySettings.QueryMode = QueryMode.Table;
            connectionSettings.InitConnectionString(connectionString);
            QueryWindow win =
                new QueryWindow(connectionSettings)
                {
                    Owner = Application.Current.MainWindow
                };

            return DataPluginUtils.ShowDialog(win.Owner, win, win.ConnectionSettings);
        }

        public override object GetConfigElement(PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            ConnectionSettings connectionSettings =
                new ConnectionSettings(bag, parameters);
            return new ConfigPanel(connectionSettings);
        }
        
        public override PropertyBag GetSetting(object obj)
        {
            return ((ConfigPanel)obj).ConnectionSettings.ToPropertyBag();
        }
    }
}
