﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using Panopticon.DatabasePlugin;

namespace Panopticon.SMARTSPlugin.UI
{
    /// <summary>
    /// Interaction logic for QueryWindow.xaml
    /// </summary>
    internal partial class QueryWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static RoutedCommand OkCommand
            = new RoutedCommand("Ok", typeof(QueryWindow));

        private DatabaseConnectionSettings connectionSettings;

        public QueryWindow() : this(null)
        {
        }

        public QueryWindow(DatabaseConnectionSettings settings)            
        {
            ConnectionSettings = settings;
            InitializeComponent();
        }


        public DatabaseConnectionSettings ConnectionSettings
        {
            get { return connectionSettings; }
            set
            {
                if (connectionSettings != value)
                {
                    connectionSettings = value;
                    if (PropertyChanged != null)
                    {
                        PropertyChanged(this, 
                            new PropertyChangedEventArgs("ConnectionSettings"));
                    }
                }
            }
        }

        private void Ok_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = configPanel.IsOkey;
        }

        private void Ok_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
