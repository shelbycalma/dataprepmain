﻿using System.Windows;
using System.Windows.Controls;
using Panopticon.KafkaPlugin;

namespace Panopticon.KafkaSBEPlugin.UI
{
    /// <summary>
    /// Interaction logic for ConfigPanel.xaml
    /// </summary>
    public partial class ConfigPanel : UserControl
    {
        public static DependencyProperty SettingsProperty =
            DependencyProperty.Register("Settings",
                typeof(KafkaSettings),
                typeof(ConfigPanel), new PropertyMetadata(SettingsChanged));

        public ConfigPanel()
            : this(null)
        {
        }

        public ConfigPanel(KafkaSettings settings)
        {
            InitializeComponent();
            Settings = settings;
        }

        public KafkaSettings Settings
        {
            get { return (KafkaSettings)GetValue(SettingsProperty); }
            set
            {
                SetValue(SettingsProperty, value);
            }
        }

        private static void SettingsChanged(
            DependencyObject obj, DependencyPropertyChangedEventArgs args)
        {
            ((ConfigPanel)obj).OnSettingsChanged((KafkaSettings)args.NewValue);
        }

        protected void OnSettingsChanged(KafkaSettings newSettings)
        {
            DataContext = newSettings;
        }
    }
}