﻿using System.Collections.Generic;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Connectors;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Panopticon.KafkaPlugin;

namespace Panopticon.KafkaSBEPlugin.UI
{
    public class PluginUI : MessageQueuePluginUIBase<ParameterTable, KafkaSettings,
        Dictionary<string, object>, KafkaDataAdapter, Plugin>
    {
        public PluginUI(Plugin plugin, Window owner) :
            base(plugin, owner, "pack://application:,,,/Panopticon.KafkaPlugin.UI;component/aws-small.png")
        {
        }

        public override Window CreateConnectionWindow(
           KafkaSettings kafSettings)
        {
            return new ConfigWindow(kafSettings);
        }

        public override object GetConfigElement(
            PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
        {
            return
                new ConfigPanel(Plugin.CreateConnectionSettings(bag));
        }

        public override PropertyBag GetSetting(object obj)
        {
            ConfigPanel cp = (ConfigPanel)obj;
            return cp.Settings.ToPropertyBag();
        }

    }
}
