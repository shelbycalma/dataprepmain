﻿using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.KafkaSBEPlugin;

[assembly: AssemblyTitle("Panopticon.KafkaSBEPlugin.dll")]
[assembly: AssemblyDescription("Datawatch Kafka SBE Plug-in")]

[assembly: PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
