﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaSBEPlugin
{
    class EuronextSBEParserPlugin : IParserPlugin
    {
        protected PropertyBag settings;
        private const string Name = "EuronextSBEParser";

        public void Dispose()
        {

        }

        public string Id { get { return Name; } }

        public bool IsLicensed { get { return true; } }

        public bool IsObsolete { get { return false; } }

        public PropertyBag Settings { get { return settings; } }

        public string Title { get { return Name; } }

        public ParserSettings CreateParserSettings(PropertyBag bag)
        {
            return new EuronextSBEParserSettings(bag);
        }

        public Type GetParserType()
        {
            return typeof (EuronextSBEParser);
        }
    }
}