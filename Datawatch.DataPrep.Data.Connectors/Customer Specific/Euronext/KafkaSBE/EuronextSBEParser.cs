﻿using System;
using System.Collections.Generic;
using Adaptive.SimpleBinaryEncoding;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Euronext.Com;

namespace Panopticon.KafkaSBEPlugin
{
    internal class EuronextSBEParser : IParser
    {
        private static readonly MessageHeader MessageHeader = new MessageHeader();

        public Dictionary<string, object> Decode(byte[] encoded)
        {
            DirectBuffer directBuffer = new DirectBuffer(encoded);
            // first we decode the header (in a real world scenario you would need the header to decide which SBE decoder you are going to use
            const short messageTemplateVersion = 0;
            int bufferOffset = 0;

            // position the MessageHeader object at the beginning of the array
            MessageHeader.Wrap(directBuffer, bufferOffset, messageTemplateVersion);

            // Extract infos from the header
            // In a real app you would use that to lookup the applicable flyweight to decode this type of message based on templateId and version.
            int actingBlockLength = MessageHeader.BlockLength;
            int schemaId = MessageHeader.SchemaId;
            int actingVersion = MessageHeader.Version;

            bufferOffset += MessageHeader.Size;

            // now we decode the message
            return Parse(new NewOrder(), directBuffer, bufferOffset, 
                actingBlockLength, actingVersion);
        }

        private Dictionary<string, object> Parse(NewOrder newOrder,
            DirectBuffer directBuffer,
            int bufferOffset,
            int actingBlockLength,
            int actingVersion)
        {
            Dictionary<string,object> dict = new Dictionary<string, object>();

            newOrder.WrapForDecode(directBuffer, bufferOffset, 
                actingBlockLength, actingVersion);

            //TODO: Should use reflection here also, as in EuronextSBEParserSettings.            
            dict.Add("AccountType", newOrder.AccountType);
            dict.Add("ClientOrderID", newOrder.ClientOrderID);
            dict.Add("ClMsgSeqNum", newOrder.ClMsgSeqNum);
            dict.Add("ExecutionInstruction", newOrder.ExecutionInstruction);
            dict.Add("FirmID", newOrder.FirmID);
            dict.Add("Limit", newOrder.Limit);
            dict.Add("MarketModel", newOrder.MarketModel);
            dict.Add("OrderPx", newOrder.OrderPx);
            dict.Add("OrderQty", newOrder.OrderQty);
            dict.Add("OrderSide", newOrder.OrderSide);
            dict.Add("OrderType", newOrder.OrderType);
            dict.Add("RoutingInfo", newOrder.RoutingInfo);
            dict.Add("SendingTime", newOrder.SendingTime);
            dict.Add("SymbolIndex", newOrder.SymbolIndex);
            dict.Add("TimeInForce", newOrder.TimeInForce);
            dict.Add("Size", newOrder.Size);

            return dict;
        }

        public Dictionary<string, object> Parse(string message)
        {
            try
            {
                return this.Decode(System.Text.Encoding.Default.GetBytes(message));
            }
            catch (Exception ex)
            {
                throw new Exception("EuronextSBEParser: Cannot parse string messages!");
            }
        }
    }
}