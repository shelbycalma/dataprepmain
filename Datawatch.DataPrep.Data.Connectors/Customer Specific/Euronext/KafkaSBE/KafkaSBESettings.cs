﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.KafkaPlugin;

namespace Panopticon.KafkaSBEPlugin
{
    public class KafkaSBESettings : KafkaSettings
    {
        public KafkaSBESettings(IPluginManager pluginHost)
            : this(pluginHost, new PropertyBag())
        {
        }

        public KafkaSBESettings(IPluginManager pluginHost, PropertyBag bag)
            : this(pluginHost, bag, null)
        {
        }

        public KafkaSBESettings(
            IPluginManager pluginHost, PropertyBag bag,
            IEnumerable<ParameterValue> parameters)
            : base(pluginHost, bag)
        {
            base.Parameters = parameters;
        }

        public override bool CanGenerateColumns()
        {
            return true;
        }

        public override bool ShowGenerateColumnButton
        {
            get { return false; }
        }

        public override bool ShowAddColumnButton
        {
            get
            {
                return false;
            }
        }
        
        protected override string DefaultParserPluginId
        {
            get
            {
                return "EuronextSBEParser";
            }
        }

        public override IEnumerable<IParserPlugin> ParserPlugins
        {
            get { return new[] {new EuronextSBEParserPlugin()}; }
        }

        public override string WindowTitle
        {
            get { return Panopticon.KafkaSBEPlugin.Properties.Resources.UiConnectionWindowTitle; }
        }
    }
}
