﻿using System.Collections.Generic;
using System.Reflection;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaSBEPlugin
{
    public class EuronextSBEParserSettings : ParserSettings
    {
        public EuronextSBEParserSettings(PropertyBag bag)
            : base(bag)
        {
            if (Columns != null && ColumnCount > 0)
            {
                return;
            }
            // TODO: Populate the columns, could be done through 
            // reflection of all public properties in the NewOrder class.
            AddColumnDefinition(CreateTextColumn("AccountType"));
            AddColumnDefinition(CreateNumericColumn("ClientOrderID"));
            AddColumnDefinition(CreateNumericColumn("ClientOrderID"));
            AddColumnDefinition(CreateNumericColumn("ClMsgSeqNum"));
            AddColumnDefinition(CreateTextColumn("ExecutionInstruction"));
            AddColumnDefinition(CreateNumericColumn("FirmID"));
            AddColumnDefinition(CreateNumericColumn("Limit"));
            AddColumnDefinition(CreateTextColumn("MarketModel"));
            AddColumnDefinition(CreateNumericColumn("OrderPx"));
            AddColumnDefinition(CreateNumericColumn("OrderQty"));
            AddColumnDefinition(CreateTextColumn("OrderSide"));
            AddColumnDefinition(CreateTextColumn("OrderType"));
            AddColumnDefinition(CreateNumericColumn("RoutingInfo"));
            AddColumnDefinition(CreateNumericColumn("SendingTime"));
            AddColumnDefinition(CreateNumericColumn("SymbolIndex"));
            AddColumnDefinition(CreateTextColumn("TimeInForce"));
            AddColumnDefinition(CreateNumericColumn("Size"));
        }

        private ColumnDefinition CreateTextColumn(string name)
        {
            ColumnDefinition columnDefinition = CreateColumnDefinition(new PropertyBag());
            columnDefinition.Name = name;
            columnDefinition.Type = ColumnType.Text;
            return columnDefinition;
        }

        private ColumnDefinition CreateNumericColumn(string name)
        {
            ColumnDefinition columnDefinition = CreateColumnDefinition(new PropertyBag());
            columnDefinition.Name = name;
            columnDefinition.Type = ColumnType.Numeric;
            return columnDefinition;
        }

        public override string GetDescription()
        {
            return null;
        }

        public override ColumnDefinition CreateColumnDefinition(PropertyBag bag)
        {
            return new SBEColumnDefinition(bag);
        }

        public override IParser CreateParser()
        {
            return new EuronextSBEParser();
        }
    }
}