﻿using System.Collections.Generic;
using System.ComponentModel;
using Datawatch.DataPrep.Data.Core.Licensing;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Panopticon.KafkaPlugin;

namespace Panopticon.KafkaSBEPlugin
{
    [PluginDescription(Plugin.PluginTitle, Plugin.PluginType, Plugin.PluginId, Plugin.PluginIsObsolete)]
    [LicenseProvider(typeof(DatawatchXmlLicenseProvider))]
    public class Plugin : KafkaPlugin.Plugin
    {
        internal const string PluginId = "KafkaSBEPlugin";
        internal const bool PluginIsObsolete = false;
        internal const string PluginTitle = "KafkaSBE";
        internal const string PluginType = DataPluginTypes.Streaming;

        public Plugin()
            : base(PluginId, PluginTitle)
        {
        }

        public override bool IsObsolete
        {
            get { return PluginIsObsolete; }
        }

        public override string DataPluginType
        {
            get { return DataPluginTypes.GetLocalized(PluginType); }
        }

        public override KafkaSettings CreateConnectionSettings(
            IEnumerable<ParameterValue> parameters)
        {
            return new KafkaSBESettings(pluginManager);
        }

        public override KafkaSettings CreateConnectionSettings(
            PropertyBag bag)
        {
            return new KafkaSBESettings(pluginManager, bag);
        }
    }
}
