/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum TimeInForce_enum : byte
    {
        Day = 0,
        Good_Till_Cancel = 1,
        Valid_for_Auction = 2,
        Immediate_or_Cancel = 3,
        Fill_or_Kill = 4,
        Good_till_Time = 5,
        Good_till_Date = 6,
        Valid_for_Closing_auction = 7,
        Valid_for_Session = 8,
        NULL_VALUE = 255
    }
}
