/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public sealed partial class Ack
    {
    public const ushort BlockLength = (ushort)119;
    public const ushort TemplateId = (ushort)25858;
    public const ushort SchemaId = (ushort)0;
    public const ushort Schema_Version = (ushort)0;
    public const string SemanticType = "";

    private readonly Ack _parentMessage;
    private DirectBuffer _buffer;
    private int _offset;
    private int _limit;
    private int _actingBlockLength;
    private int _actingVersion;

    public int Offset { get { return _offset; } }

    public Ack()
    {
        _parentMessage = this;
    }

    public void WrapForEncode(DirectBuffer buffer, int offset)
    {
        _buffer = buffer;
        _offset = offset;
        _actingBlockLength = BlockLength;
        _actingVersion = Schema_Version;
        Limit = offset + _actingBlockLength;
    }

    public void WrapForDecode(DirectBuffer buffer, int offset, int actingBlockLength, int actingVersion)
    {
        _buffer = buffer;
        _offset = offset;
        _actingBlockLength = actingBlockLength;
        _actingVersion = actingVersion;
        Limit = offset + _actingBlockLength;
    }

    public int Size
    {
        get
        {
            return _limit - _offset;
        }
    }

    public int Limit
    {
        get
        {
            return _limit;
        }
        set
        {
            _buffer.CheckLimit(value);
            _limit = value;
        }
    }


    public const int MsgSeqNumId = 1;

    public static string MsgSeqNumMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong MsgSeqNumNullValue = 0xffffffffffffffffUL;

    public const ulong MsgSeqNumMinValue = 0x0UL;

    public const ulong MsgSeqNumMaxValue = 0xfffffffffffffffeUL;

    public ulong MsgSeqNum
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 0);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 0, value);
        }
    }


    public const int FirmIDId = 2;

    public static string FirmIDMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const uint FirmIDNullValue = 4294967294U;

    public const uint FirmIDMinValue = 0U;

    public const uint FirmIDMaxValue = 4294967293U;

    public uint FirmID
    {
        get
        {
            return _buffer.Uint32GetLittleEndian(_offset + 8);
        }
        set
        {
            _buffer.Uint32PutLittleEndian(_offset + 8, value);
        }
    }


    public const int NetworkINId = 2;

    public static string NetworkINMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong NetworkINNullValue = 0xffffffffffffffffUL;

    public const ulong NetworkINMinValue = 0x0UL;

    public const ulong NetworkINMaxValue = 0xfffffffffffffffeUL;

    public ulong NetworkIN
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 12);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 12, value);
        }
    }


    public const int OEGINFromMemberId = 3;

    public static string OEGINFromMemberMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OEGINFromMemberNullValue = 0xffffffffffffffffUL;

    public const ulong OEGINFromMemberMinValue = 0x0UL;

    public const ulong OEGINFromMemberMaxValue = 0xfffffffffffffffeUL;

    public ulong OEGINFromMember
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 20);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 20, value);
        }
    }


    public const int OEGOUTTimeToMEId = 4;

    public static string OEGOUTTimeToMEMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OEGOUTTimeToMENullValue = 0xffffffffffffffffUL;

    public const ulong OEGOUTTimeToMEMinValue = 0x0UL;

    public const ulong OEGOUTTimeToMEMaxValue = 0xfffffffffffffffeUL;

    public ulong OEGOUTTimeToME
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 28);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 28, value);
        }
    }


    public const int BookInId = 5;

    public static string BookInMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong BookInNullValue = 0xffffffffffffffffUL;

    public const ulong BookInMinValue = 0x0UL;

    public const ulong BookInMaxValue = 0xfffffffffffffffeUL;

    public ulong BookIn
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 36);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 36, value);
        }
    }


    public const int BookOutToOEGId = 6;

    public static string BookOutToOEGMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong BookOutToOEGNullValue = 0xffffffffffffffffUL;

    public const ulong BookOutToOEGMinValue = 0x0UL;

    public const ulong BookOutToOEGMaxValue = 0xfffffffffffffffeUL;

    public ulong BookOutToOEG
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 44);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 44, value);
        }
    }


    public const int OEGINFromMEId = 7;

    public static string OEGINFromMEMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OEGINFromMENullValue = 0xffffffffffffffffUL;

    public const ulong OEGINFromMEMinValue = 0x0UL;

    public const ulong OEGINFromMEMaxValue = 0xfffffffffffffffeUL;

    public ulong OEGINFromME
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 52);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 52, value);
        }
    }


    public const int OEGOUTToMemberId = 8;

    public static string OEGOUTToMemberMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OEGOUTToMemberNullValue = 0xffffffffffffffffUL;

    public const ulong OEGOUTToMemberMinValue = 0x0UL;

    public const ulong OEGOUTToMemberMaxValue = 0xfffffffffffffffeUL;

    public ulong OEGOUTToMember
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 60);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 60, value);
        }
    }


    public const int ClientOrderIDId = 9;

    public static string ClientOrderIDMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong ClientOrderIDNullValue = 0xffffffffffffffffUL;

    public const ulong ClientOrderIDMinValue = 0x0UL;

    public const ulong ClientOrderIDMaxValue = 0xfffffffffffffffeUL;

    public ulong ClientOrderID
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 68);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 68, value);
        }
    }


    public const int SymbolIndexId = 10;

    public static string SymbolIndexMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const uint SymbolIndexNullValue = 4294967294U;

    public const uint SymbolIndexMinValue = 0U;

    public const uint SymbolIndexMaxValue = 4294967293U;

    public uint SymbolIndex
    {
        get
        {
            return _buffer.Uint32GetLittleEndian(_offset + 76);
        }
        set
        {
            _buffer.Uint32PutLittleEndian(_offset + 76, value);
        }
    }


    public const int MarketModelId = 11;

    public static string MarketModelMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public MarketModel_enum MarketModel
    {
        get
        {
            return (MarketModel_enum)_buffer.Uint8Get(_offset + 80);
        }
        set
        {
            _buffer.Uint8Put(_offset + 80, (byte)value);
        }
    }


    public const int OrderIDId = 12;

    public static string OrderIDMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const uint OrderIDNullValue = 4294967294U;

    public const uint OrderIDMinValue = 0U;

    public const uint OrderIDMaxValue = 4294967293U;

    public uint OrderID
    {
        get
        {
            return _buffer.Uint32GetLittleEndian(_offset + 81);
        }
        set
        {
            _buffer.Uint32PutLittleEndian(_offset + 81, value);
        }
    }


    public const int OrderSideId = 13;

    public static string OrderSideMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public OrderSide_enum OrderSide
    {
        get
        {
            return (OrderSide_enum)_buffer.Uint8Get(_offset + 85);
        }
        set
        {
            _buffer.Uint8Put(_offset + 85, (byte)value);
        }
    }


    public const int OrderPxId = 14;

    public static string OrderPxMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const long OrderPxNullValue = -9223372036854775808L;

    public const long OrderPxMinValue = -9223372036854775807L;

    public const long OrderPxMaxValue = 9223372036854775807L;

    public long OrderPx
    {
        get
        {
            return _buffer.Int64GetLittleEndian(_offset + 86);
        }
        set
        {
            _buffer.Int64PutLittleEndian(_offset + 86, value);
        }
    }


    public const int OrderQtyId = 15;

    public static string OrderQtyMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OrderQtyNullValue = 0xffffffffffffffffUL;

    public const ulong OrderQtyMinValue = 0x0UL;

    public const ulong OrderQtyMaxValue = 0xfffffffffffffffeUL;

    public ulong OrderQty
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 94);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 94, value);
        }
    }


    public const int OrderFilledQtyId = 16;

    public static string OrderFilledQtyMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OrderFilledQtyNullValue = 0xffffffffffffffffUL;

    public const ulong OrderFilledQtyMinValue = 0x0UL;

    public const ulong OrderFilledQtyMaxValue = 0xfffffffffffffffeUL;

    public ulong OrderFilledQty
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 102);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 102, value);
        }
    }


    public const int OrderFilledAmtId = 17;

    public static string OrderFilledAmtMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OrderFilledAmtNullValue = 0xffffffffffffffffUL;

    public const ulong OrderFilledAmtMinValue = 0x0UL;

    public const ulong OrderFilledAmtMaxValue = 0xfffffffffffffffeUL;

    public ulong OrderFilledAmt
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 110);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 110, value);
        }
    }


    public const int AckTypeId = 18;

    public static string AckTypeMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public AckType_enum AckType
    {
        get
        {
            return (AckType_enum)_buffer.Uint8Get(_offset + 118);
        }
        set
        {
            _buffer.Uint8Put(_offset + 118, (byte)value);
        }
    }

    }
}
