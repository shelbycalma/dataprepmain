/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum AckType_enum : byte
    {
        New_Order_Ack = 0,
        Replace_Ack = 1,
        Open_Order_Request_Ack = 2,
        Stop_Triggered_Ack = 3,
        Collar_Confirmation_Ack = 4,
        NULL_VALUE = 255
    }
}
