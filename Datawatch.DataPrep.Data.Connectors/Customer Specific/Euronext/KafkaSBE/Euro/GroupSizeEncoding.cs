/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public sealed partial class GroupSizeEncoding
    {
        private DirectBuffer _buffer;
        private int _offset;
        private int _actingVersion;

        public void Wrap(DirectBuffer buffer, int offset, int actingVersion)
        {
            _offset = offset;
            _actingVersion = actingVersion;
            _buffer = buffer;
        }

        public const int Size = 2;

    public const byte BlockLengthNullValue = (byte)255;

    public const byte BlockLengthMinValue = (byte)0;

    public const byte BlockLengthMaxValue = (byte)254;

    public byte BlockLength
    {
        get
        {
            return _buffer.Uint8Get(_offset + 0);
        }
        set
        {
            _buffer.Uint8Put(_offset + 0, value);
        }
    }


    public const byte NumInGroupNullValue = (byte)255;

    public const byte NumInGroupMinValue = (byte)0;

    public const byte NumInGroupMaxValue = (byte)254;

    public byte NumInGroup
    {
        get
        {
            return _buffer.Uint8Get(_offset + 1);
        }
        set
        {
            _buffer.Uint8Put(_offset + 1, value);
        }
    }

    }
}
