/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum ExecutionInstruction_enum : byte
    {
        STP_resting_order = 1,
        STP_incoming_order = 2,
        AON = 3,
        PEG_TBD___Future_use_1 = 4,
        PEG_TBD___Future_use_2 = 5,
        PEG_TBD___Future_use_3 = 6,
        PEG_TBD___Future_use_4 = 7,
        PEG_TBD___Future_use_5 = 8,
        NULL_VALUE = 255
    }
}
