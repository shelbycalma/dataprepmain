/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum AccountType_enum : byte
    {
        Client = 1,
        House = 2,
        RLO = 3,
        RO = 4,
        Liquidity_Provider = 6,
        Related_Party = 7,
        Riskless_Principal = 8,
        Clearing_Members_House_Account = 9,
        SI_Order = 10,
        NULL_VALUE = 255
    }
}
