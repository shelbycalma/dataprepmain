/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum TradeInputSource_enum : byte
    {
        Undefined = 85,
        Manual = 77,
        Automated = 65,
        Generated = 71,
        NULL_VALUE = 0
    }
}
