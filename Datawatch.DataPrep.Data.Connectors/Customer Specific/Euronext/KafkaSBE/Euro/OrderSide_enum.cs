/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum OrderSide_enum : byte
    {
        Buy = 1,
        Sell = 2,
        Cross = 3,
        NULL_VALUE = 255
    }
}
