/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public sealed partial class NewOrder
    {
    public const ushort BlockLength = (ushort)52;
    public const ushort TemplateId = (ushort)25857;
    public const ushort SchemaId = (ushort)0;
    public const ushort Schema_Version = (ushort)0;
    public const string SemanticType = "";

    private readonly NewOrder _parentMessage;
    private DirectBuffer _buffer;
    private int _offset;
    private int _limit;
    private int _actingBlockLength;
    private int _actingVersion;

    public int Offset { get { return _offset; } }

    public NewOrder()
    {
        _parentMessage = this;
    }

    public void WrapForEncode(DirectBuffer buffer, int offset)
    {
        _buffer = buffer;
        _offset = offset;
        _actingBlockLength = BlockLength;
        _actingVersion = Schema_Version;
        Limit = offset + _actingBlockLength;
    }

    public void WrapForDecode(DirectBuffer buffer, int offset, int actingBlockLength, int actingVersion)
    {
        _buffer = buffer;
        _offset = offset;
        _actingBlockLength = actingBlockLength;
        _actingVersion = actingVersion;
        Limit = offset + _actingBlockLength;
    }

    public int Size
    {
        get
        {
            return _limit - _offset;
        }
    }

    public int Limit
    {
        get
        {
            return _limit;
        }
        set
        {
            _buffer.CheckLimit(value);
            _limit = value;
        }
    }


    public const int ClMsgSeqNumId = 1;

    public static string ClMsgSeqNumMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const uint ClMsgSeqNumNullValue = 4294967294U;

    public const uint ClMsgSeqNumMinValue = 0U;

    public const uint ClMsgSeqNumMaxValue = 4294967293U;

    public uint ClMsgSeqNum
    {
        get
        {
            return _buffer.Uint32GetLittleEndian(_offset + 0);
        }
        set
        {
            _buffer.Uint32PutLittleEndian(_offset + 0, value);
        }
    }


    public const int FirmIDId = 2;

    public static string FirmIDMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const uint FirmIDNullValue = 4294967294U;

    public const uint FirmIDMinValue = 0U;

    public const uint FirmIDMaxValue = 4294967293U;

    public uint FirmID
    {
        get
        {
            return _buffer.Uint32GetLittleEndian(_offset + 4);
        }
        set
        {
            _buffer.Uint32PutLittleEndian(_offset + 4, value);
        }
    }


    public const int SendingTimeId = 3;

    public static string SendingTimeMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong SendingTimeNullValue = 0xffffffffffffffffUL;

    public const ulong SendingTimeMinValue = 0x0UL;

    public const ulong SendingTimeMaxValue = 0xfffffffffffffffeUL;

    public ulong SendingTime
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 8);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 8, value);
        }
    }


    public const int ClientOrderIDId = 2;

    public static string ClientOrderIDMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong ClientOrderIDNullValue = 0xffffffffffffffffUL;

    public const ulong ClientOrderIDMinValue = 0x0UL;

    public const ulong ClientOrderIDMaxValue = 0xfffffffffffffffeUL;

    public ulong ClientOrderID
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 16);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 16, value);
        }
    }


    public const int SymbolIndexId = 3;

    public static string SymbolIndexMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const uint SymbolIndexNullValue = 4294967294U;

    public const uint SymbolIndexMinValue = 0U;

    public const uint SymbolIndexMaxValue = 4294967293U;

    public uint SymbolIndex
    {
        get
        {
            return _buffer.Uint32GetLittleEndian(_offset + 24);
        }
        set
        {
            _buffer.Uint32PutLittleEndian(_offset + 24, value);
        }
    }


    public const int MarketModelId = 4;

    public static string MarketModelMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public MarketModel_enum MarketModel
    {
        get
        {
            return (MarketModel_enum)_buffer.Uint8Get(_offset + 28);
        }
        set
        {
            _buffer.Uint8Put(_offset + 28, (byte)value);
        }
    }


    public const int RoutingInfoId = 5;

    public static string RoutingInfoMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ushort RoutingInfoNullValue = (ushort)65535;

    public const ushort RoutingInfoMinValue = (ushort)0;

    public const ushort RoutingInfoMaxValue = (ushort)65534;

    public ushort RoutingInfo
    {
        get
        {
            return _buffer.Uint16GetLittleEndian(_offset + 29);
        }
        set
        {
            _buffer.Uint16PutLittleEndian(_offset + 29, value);
        }
    }


    public const int OrderSideId = 6;

    public static string OrderSideMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public OrderSide_enum OrderSide
    {
        get
        {
            return (OrderSide_enum)_buffer.Uint8Get(_offset + 31);
        }
        set
        {
            _buffer.Uint8Put(_offset + 31, (byte)value);
        }
    }


    public const int OrderPxId = 7;

    public static string OrderPxMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const long OrderPxNullValue = -9223372036854775808L;

    public const long OrderPxMinValue = -9223372036854775807L;

    public const long OrderPxMaxValue = 9223372036854775807L;

    public long OrderPx
    {
        get
        {
            return _buffer.Int64GetLittleEndian(_offset + 32);
        }
        set
        {
            _buffer.Int64PutLittleEndian(_offset + 32, value);
        }
    }


    public const int OrderQtyId = 8;

    public static string OrderQtyMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public const ulong OrderQtyNullValue = 0xffffffffffffffffUL;

    public const ulong OrderQtyMinValue = 0x0UL;

    public const ulong OrderQtyMaxValue = 0xfffffffffffffffeUL;

    public ulong OrderQty
    {
        get
        {
            return _buffer.Uint64GetLittleEndian(_offset + 40);
        }
        set
        {
            _buffer.Uint64PutLittleEndian(_offset + 40, value);
        }
    }


    public const int OrderTypeId = 9;

    public static string OrderTypeMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public OrderType_enum OrderType
    {
        get
        {
            return (OrderType_enum)_buffer.Uint8Get(_offset + 48);
        }
        set
        {
            _buffer.Uint8Put(_offset + 48, (byte)value);
        }
    }


    public const int TimeInForceId = 10;

    public static string TimeInForceMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public TimeInForce_enum TimeInForce
    {
        get
        {
            return (TimeInForce_enum)_buffer.Uint8Get(_offset + 49);
        }
        set
        {
            _buffer.Uint8Put(_offset + 49, (byte)value);
        }
    }


    public const int AccountTypeId = 11;

    public static string AccountTypeMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public AccountType_enum AccountType
    {
        get
        {
            return (AccountType_enum)_buffer.Uint8Get(_offset + 50);
        }
        set
        {
            _buffer.Uint8Put(_offset + 50, (byte)value);
        }
    }


    public const int ExecutionInstructionId = 12;

    public static string ExecutionInstructionMetaAttribute(MetaAttribute metaAttribute)
    {
        switch (metaAttribute)
        {
            case MetaAttribute.Epoch: return "unix";
            case MetaAttribute.TimeUnit: return "nanosecond";
            case MetaAttribute.SemanticType: return "";
        }

        return "";
    }

    public ExecutionInstruction_enum ExecutionInstruction
    {
        get
        {
            return (ExecutionInstruction_enum)_buffer.Uint8Get(_offset + 51);
        }
        set
        {
            _buffer.Uint8Put(_offset + 51, (byte)value);
        }
    }


    private readonly FreeTextSectionGroup _freeTextSection = new FreeTextSectionGroup();

    public const long FreeTextSectionId = 13;


    public FreeTextSectionGroup FreeTextSection
    {
        get
        {
            _freeTextSection.WrapForDecode(_parentMessage, _buffer, _actingVersion);
            return _freeTextSection;
        }
    }

    public FreeTextSectionGroup FreeTextSectionCount(int count)
    {
        _freeTextSection.WrapForEncode(_parentMessage, _buffer, count);
        return _freeTextSection;
    }

    public sealed partial class FreeTextSectionGroup
    {
        private readonly GroupSizeEncoding _dimensions = new GroupSizeEncoding();
        private NewOrder _parentMessage;
        private DirectBuffer _buffer;
        private int _blockLength;
        private int _actingVersion;
        private int _count;
        private int _index;
        private int _offset;

        public void WrapForDecode(NewOrder parentMessage, DirectBuffer buffer, int actingVersion)
        {
            _parentMessage = parentMessage;
            _buffer = buffer;
            _dimensions.Wrap(buffer, parentMessage.Limit, actingVersion);
            _blockLength = _dimensions.BlockLength;
            _count = _dimensions.NumInGroup;
            _actingVersion = actingVersion;
            _index = -1;
            _parentMessage.Limit = parentMessage.Limit + SbeHeaderSize;
        }

        public void WrapForEncode(NewOrder parentMessage, DirectBuffer buffer, int count)
        {
            _parentMessage = parentMessage;
            _buffer = buffer;
            _dimensions.Wrap(buffer, parentMessage.Limit, _actingVersion);
            _dimensions.BlockLength = (byte)18;
            _dimensions.NumInGroup = (byte)count;
            _index = -1;
            _count = count;
            _blockLength = 18;
            parentMessage.Limit = parentMessage.Limit + SbeHeaderSize;
        }

        public const int SbeBlockLength = 18;
        public const int SbeHeaderSize = 2;
        public int ActingBlockLength { get { return _blockLength; } }

        public int Count { get { return _count; } }

        public bool HasNext { get { return (_index + 1) < _count; } }

        public FreeTextSectionGroup Next()
        {
            if (_index + 1 >= _count)
            {
                throw new InvalidOperationException();
            }

            _offset = _parentMessage.Limit;
            _parentMessage.Limit = _offset + _blockLength;
            ++_index;

            return this;
        }

        public const int FreeTextId = 1;

        public static string FreeTextMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte FreeTextNullValue = (byte)0;

        public const byte FreeTextMinValue = (byte)32;

        public const byte FreeTextMaxValue = (byte)126;

        public const int FreeTextLength  = 18;

        public byte GetFreeText(int index)
        {
            if (index < 0 || index >= 18)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 0 + (index * 1));
        }

        public void SetFreeText(int index, byte value)
        {
            if (index < 0 || index >= 18)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 0 + (index * 1), value);
        }

    public const string FreeTextCharacterEncoding = "UTF-8";

        public int GetFreeText(byte[] dst, int dstOffset)
        {
            const int length = 18;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 0, dst, dstOffset, length);
            return length;
        }

        public void SetFreeText(byte[] src, int srcOffset)
        {
            const int length = 18;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 0, src, srcOffset, length);
        }
    }

    private readonly OptionalFieldsGroup _optionalFields = new OptionalFieldsGroup();

    public const long OptionalFieldsId = 14;


    public OptionalFieldsGroup OptionalFields
    {
        get
        {
            _optionalFields.WrapForDecode(_parentMessage, _buffer, _actingVersion);
            return _optionalFields;
        }
    }

    public OptionalFieldsGroup OptionalFieldsCount(int count)
    {
        _optionalFields.WrapForEncode(_parentMessage, _buffer, count);
        return _optionalFields;
    }

    public sealed partial class OptionalFieldsGroup
    {
        private readonly GroupSizeEncoding _dimensions = new GroupSizeEncoding();
        private NewOrder _parentMessage;
        private DirectBuffer _buffer;
        private int _blockLength;
        private int _actingVersion;
        private int _count;
        private int _index;
        private int _offset;

        public void WrapForDecode(NewOrder parentMessage, DirectBuffer buffer, int actingVersion)
        {
            _parentMessage = parentMessage;
            _buffer = buffer;
            _dimensions.Wrap(buffer, parentMessage.Limit, actingVersion);
            _blockLength = _dimensions.BlockLength;
            _count = _dimensions.NumInGroup;
            _actingVersion = actingVersion;
            _index = -1;
            _parentMessage.Limit = parentMessage.Limit + SbeHeaderSize;
        }

        public void WrapForEncode(NewOrder parentMessage, DirectBuffer buffer, int count)
        {
            _parentMessage = parentMessage;
            _buffer = buffer;
            _dimensions.Wrap(buffer, parentMessage.Limit, _actingVersion);
            _dimensions.BlockLength = (byte)37;
            _dimensions.NumInGroup = (byte)count;
            _index = -1;
            _count = count;
            _blockLength = 37;
            parentMessage.Limit = parentMessage.Limit + SbeHeaderSize;
        }

        public const int SbeBlockLength = 37;
        public const int SbeHeaderSize = 2;
        public int ActingBlockLength { get { return _blockLength; } }

        public int Count { get { return _count; } }

        public bool HasNext { get { return (_index + 1) < _count; } }

        public OptionalFieldsGroup Next()
        {
            if (_index + 1 >= _count)
            {
                throw new InvalidOperationException();
            }

            _offset = _parentMessage.Limit;
            _parentMessage.Limit = _offset + _blockLength;
            ++_index;

            return this;
        }

        public const int OrderExpirationTimeId = 1;

        public static string OrderExpirationTimeMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const uint OrderExpirationTimeNullValue = 4294967294U;

        public const uint OrderExpirationTimeMinValue = 0U;

        public const uint OrderExpirationTimeMaxValue = 4294967293U;

        public uint OrderExpirationTime
        {
            get
            {
                return _buffer.Uint32GetLittleEndian(_offset + 0);
            }
            set
            {
                _buffer.Uint32PutLittleEndian(_offset + 0, value);
            }
        }


        public const int TradingSessionId = 2;

        public static string TradingSessionMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public TradingSessionValidity_enum TradingSession
        {
            get
            {
                return (TradingSessionValidity_enum)_buffer.Uint8Get(_offset + 4);
            }
            set
            {
                _buffer.Uint8Put(_offset + 4, (byte)value);
            }
        }


        public const int StopPxId = 3;

        public static string StopPxMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const long StopPxNullValue = -9223372036854775808L;

        public const long StopPxMinValue = -9223372036854775807L;

        public const long StopPxMaxValue = 9223372036854775807L;

        public long StopPx
        {
            get
            {
                return _buffer.Int64GetLittleEndian(_offset + 5);
            }
            set
            {
                _buffer.Int64PutLittleEndian(_offset + 5, value);
            }
        }


        public const int PegOffsetId = 4;

        public static string PegOffsetMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const long PegOffsetNullValue = -9223372036854775808L;

        public const long PegOffsetMinValue = -9223372036854775807L;

        public const long PegOffsetMaxValue = 9223372036854775807L;

        public long PegOffset
        {
            get
            {
                return _buffer.Int64GetLittleEndian(_offset + 13);
            }
            set
            {
                _buffer.Int64PutLittleEndian(_offset + 13, value);
            }
        }


        public const int MinOrderQtyId = 5;

        public static string MinOrderQtyMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const uint MinOrderQtyNullValue = 4294967294U;

        public const uint MinOrderQtyMinValue = 0U;

        public const uint MinOrderQtyMaxValue = 4294967293U;

        public uint MinOrderQty
        {
            get
            {
                return _buffer.Uint32GetLittleEndian(_offset + 21);
            }
            set
            {
                _buffer.Uint32PutLittleEndian(_offset + 21, value);
            }
        }


        public const int RefillQtyId = 6;

        public static string RefillQtyMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const uint RefillQtyNullValue = 4294967294U;

        public const uint RefillQtyMinValue = 0U;

        public const uint RefillQtyMaxValue = 4294967293U;

        public uint RefillQty
        {
            get
            {
                return _buffer.Uint32GetLittleEndian(_offset + 25);
            }
            set
            {
                _buffer.Uint32PutLittleEndian(_offset + 25, value);
            }
        }


        public const int ShortSellId = 7;

        public static string ShortSellMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte ShortSellNullValue = (byte)0;

        public const byte ShortSellMinValue = (byte)32;

        public const byte ShortSellMaxValue = (byte)126;

        public byte ShortSell
        {
            get
            {
                return _buffer.CharGet(_offset + 29);
            }
            set
            {
                _buffer.CharPut(_offset + 29, value);
            }
        }


        public const int OrderExpirationDateId = 8;

        public static string OrderExpirationDateMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const ushort OrderExpirationDateNullValue = (ushort)65535;

        public const ushort OrderExpirationDateMinValue = (ushort)0;

        public const ushort OrderExpirationDateMaxValue = (ushort)65534;

        public ushort OrderExpirationDate
        {
            get
            {
                return _buffer.Uint16GetLittleEndian(_offset + 30);
            }
            set
            {
                _buffer.Uint16PutLittleEndian(_offset + 30, value);
            }
        }


        public const int TechnicalOriginId = 9;

        public static string TechnicalOriginMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public TechnicalOrigin_enum TechnicalOrigin
        {
            get
            {
                return (TechnicalOrigin_enum)_buffer.Uint8Get(_offset + 32);
            }
            set
            {
                _buffer.Uint8Put(_offset + 32, (byte)value);
            }
        }


        public const int TradeInputDeviceId = 10;

        public static string TradeInputDeviceMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte TradeInputDeviceNullValue = (byte)0;

        public const byte TradeInputDeviceMinValue = (byte)32;

        public const byte TradeInputDeviceMaxValue = (byte)126;

        public const int TradeInputDeviceLength  = 3;

        public byte GetTradeInputDevice(int index)
        {
            if (index < 0 || index >= 3)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 33 + (index * 1));
        }

        public void SetTradeInputDevice(int index, byte value)
        {
            if (index < 0 || index >= 3)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 33 + (index * 1), value);
        }

    public const string TradeInputDeviceCharacterEncoding = "UTF-8";

        public int GetTradeInputDevice(byte[] dst, int dstOffset)
        {
            const int length = 3;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 33, dst, dstOffset, length);
            return length;
        }

        public void SetTradeInputDevice(byte[] src, int srcOffset)
        {
            const int length = 3;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 33, src, srcOffset, length);
        }

        public const int TradeInputSourceId = 11;

        public static string TradeInputSourceMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public TradeInputSource_enum TradeInputSource
        {
            get
            {
                return (TradeInputSource_enum)_buffer.CharGet(_offset + 36);
            }
            set
            {
                _buffer.CharPut(_offset + 36, (byte)value);
            }
        }

    }

    private readonly ClearingFieldsGroup _clearingFields = new ClearingFieldsGroup();

    public const long ClearingFieldsId = 15;


    public ClearingFieldsGroup ClearingFields
    {
        get
        {
            _clearingFields.WrapForDecode(_parentMessage, _buffer, _actingVersion);
            return _clearingFields;
        }
    }

    public ClearingFieldsGroup ClearingFieldsCount(int count)
    {
        _clearingFields.WrapForEncode(_parentMessage, _buffer, count);
        return _clearingFields;
    }

    public sealed partial class ClearingFieldsGroup
    {
        private readonly GroupSizeEncoding _dimensions = new GroupSizeEncoding();
        private NewOrder _parentMessage;
        private DirectBuffer _buffer;
        private int _blockLength;
        private int _actingVersion;
        private int _count;
        private int _index;
        private int _offset;

        public void WrapForDecode(NewOrder parentMessage, DirectBuffer buffer, int actingVersion)
        {
            _parentMessage = parentMessage;
            _buffer = buffer;
            _dimensions.Wrap(buffer, parentMessage.Limit, actingVersion);
            _blockLength = _dimensions.BlockLength;
            _count = _dimensions.NumInGroup;
            _actingVersion = actingVersion;
            _index = -1;
            _parentMessage.Limit = parentMessage.Limit + SbeHeaderSize;
        }

        public void WrapForEncode(NewOrder parentMessage, DirectBuffer buffer, int count)
        {
            _parentMessage = parentMessage;
            _buffer = buffer;
            _dimensions.Wrap(buffer, parentMessage.Limit, _actingVersion);
            _dimensions.BlockLength = (byte)68;
            _dimensions.NumInGroup = (byte)count;
            _index = -1;
            _count = count;
            _blockLength = 68;
            parentMessage.Limit = parentMessage.Limit + SbeHeaderSize;
        }

        public const int SbeBlockLength = 68;
        public const int SbeHeaderSize = 2;
        public int ActingBlockLength { get { return _blockLength; } }

        public int Count { get { return _count; } }

        public bool HasNext { get { return (_index + 1) < _count; } }

        public ClearingFieldsGroup Next()
        {
            if (_index + 1 >= _count)
            {
                throw new InvalidOperationException();
            }

            _offset = _parentMessage.Limit;
            _parentMessage.Limit = _offset + _blockLength;
            ++_index;

            return this;
        }

        public const int OpenCloseId = 1;

        public static string OpenCloseMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public OpenClose_enum OpenClose
        {
            get
            {
                return (OpenClose_enum)_buffer.Uint8Get(_offset + 0);
            }
            set
            {
                _buffer.Uint8Put(_offset + 0, (byte)value);
            }
        }


        public const int StrategyOpenCloseId = 2;

        public static string StrategyOpenCloseMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte StrategyOpenCloseNullValue = (byte)0;

        public const byte StrategyOpenCloseMinValue = (byte)32;

        public const byte StrategyOpenCloseMaxValue = (byte)126;

        public byte StrategyOpenClose
        {
            get
            {
                return _buffer.CharGet(_offset + 1);
            }
            set
            {
                _buffer.CharPut(_offset + 1, value);
            }
        }


        public const int FreeTextId = 3;

        public static string FreeTextMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte FreeTextNullValue = (byte)0;

        public const byte FreeTextMinValue = (byte)32;

        public const byte FreeTextMaxValue = (byte)126;

        public const int FreeTextLength  = 18;

        public byte GetFreeText(int index)
        {
            if (index < 0 || index >= 18)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 2 + (index * 1));
        }

        public void SetFreeText(int index, byte value)
        {
            if (index < 0 || index >= 18)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 2 + (index * 1), value);
        }

    public const string FreeTextCharacterEncoding = "UTF-8";

        public int GetFreeText(byte[] dst, int dstOffset)
        {
            const int length = 18;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 2, dst, dstOffset, length);
            return length;
        }

        public void SetFreeText(byte[] src, int srcOffset)
        {
            const int length = 18;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 2, src, srcOffset, length);
        }

        public const int ClearingInstructionId = 4;

        public static string ClearingInstructionMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public ClearingInstruction_enum ClearingInstruction
        {
            get
            {
                return (ClearingInstruction_enum)_buffer.Uint8Get(_offset + 20);
            }
            set
            {
                _buffer.Uint8Put(_offset + 20, (byte)value);
            }
        }


        public const int DMAFirmIDId = 5;

        public static string DMAFirmIDMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte DMAFirmIDNullValue = (byte)0;

        public const byte DMAFirmIDMinValue = (byte)32;

        public const byte DMAFirmIDMaxValue = (byte)126;

        public const int DMAFirmIDLength  = 8;

        public byte GetDMAFirmID(int index)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 21 + (index * 1));
        }

        public void SetDMAFirmID(int index, byte value)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 21 + (index * 1), value);
        }

    public const string DMAFirmIDCharacterEncoding = "UTF-8";

        public int GetDMAFirmID(byte[] dst, int dstOffset)
        {
            const int length = 8;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 21, dst, dstOffset, length);
            return length;
        }

        public void SetDMAFirmID(byte[] src, int srcOffset)
        {
            const int length = 8;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 21, src, srcOffset, length);
        }

        public const int AccountNumberId = 6;

        public static string AccountNumberMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte AccountNumberNullValue = (byte)0;

        public const byte AccountNumberMinValue = (byte)32;

        public const byte AccountNumberMaxValue = (byte)126;

        public const int AccountNumberLength  = 14;

        public byte GetAccountNumber(int index)
        {
            if (index < 0 || index >= 14)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 29 + (index * 1));
        }

        public void SetAccountNumber(int index, byte value)
        {
            if (index < 0 || index >= 14)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 29 + (index * 1), value);
        }

    public const string AccountNumberCharacterEncoding = "UTF-8";

        public int GetAccountNumber(byte[] dst, int dstOffset)
        {
            const int length = 14;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 29, dst, dstOffset, length);
            return length;
        }

        public void SetAccountNumber(byte[] src, int srcOffset)
        {
            const int length = 14;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 29, src, srcOffset, length);
        }

        public const int AccountTypeId = 7;

        public static string AccountTypeMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public AccountType_enum AccountType
        {
            get
            {
                return (AccountType_enum)_buffer.Uint8Get(_offset + 43);
            }
            set
            {
                _buffer.Uint8Put(_offset + 43, (byte)value);
            }
        }


        public const int ClearingFirmIDId = 8;

        public static string ClearingFirmIDMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte ClearingFirmIDNullValue = (byte)0;

        public const byte ClearingFirmIDMinValue = (byte)32;

        public const byte ClearingFirmIDMaxValue = (byte)126;

        public const int ClearingFirmIDLength  = 8;

        public byte GetClearingFirmID(int index)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 44 + (index * 1));
        }

        public void SetClearingFirmID(int index, byte value)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 44 + (index * 1), value);
        }

    public const string ClearingFirmIDCharacterEncoding = "UTF-8";

        public int GetClearingFirmID(byte[] dst, int dstOffset)
        {
            const int length = 8;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 44, dst, dstOffset, length);
            return length;
        }

        public void SetClearingFirmID(byte[] src, int srcOffset)
        {
            const int length = 8;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 44, src, srcOffset, length);
        }

        public const int TraderId = 9;

        public static string TraderMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte TraderNullValue = (byte)0;

        public const byte TraderMinValue = (byte)32;

        public const byte TraderMaxValue = (byte)126;

        public const int TraderLength  = 8;

        public byte GetTrader(int index)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 52 + (index * 1));
        }

        public void SetTrader(int index, byte value)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 52 + (index * 1), value);
        }

    public const string TraderCharacterEncoding = "UTF-8";

        public int GetTrader(byte[] dst, int dstOffset)
        {
            const int length = 8;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 52, dst, dstOffset, length);
            return length;
        }

        public void SetTrader(byte[] src, int srcOffset)
        {
            const int length = 8;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 52, src, srcOffset, length);
        }

        public const int AlgoId = 10;

        public static string AlgoMetaAttribute(MetaAttribute metaAttribute)
        {
            switch (metaAttribute)
            {
                case MetaAttribute.Epoch: return "unix";
                case MetaAttribute.TimeUnit: return "nanosecond";
                case MetaAttribute.SemanticType: return "";
            }

            return "";
        }

        public const byte AlgoNullValue = (byte)0;

        public const byte AlgoMinValue = (byte)32;

        public const byte AlgoMaxValue = (byte)126;

        public const int AlgoLength  = 8;

        public byte GetAlgo(int index)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            return _buffer.CharGet(_offset + 60 + (index * 1));
        }

        public void SetAlgo(int index, byte value)
        {
            if (index < 0 || index >= 8)
            {
                throw new IndexOutOfRangeException("index out of range: index=" + index);
            }

            _buffer.CharPut(_offset + 60 + (index * 1), value);
        }

    public const string AlgoCharacterEncoding = "UTF-8";

        public int GetAlgo(byte[] dst, int dstOffset)
        {
            const int length = 8;
            if (dstOffset < 0 || dstOffset > (dst.Length - length))
            {
                throw new IndexOutOfRangeException("dstOffset out of range for copy: offset=" + dstOffset);
            }

            _buffer.GetBytes(_offset + 60, dst, dstOffset, length);
            return length;
        }

        public void SetAlgo(byte[] src, int srcOffset)
        {
            const int length = 8;
            if (srcOffset < 0 || srcOffset > (src.Length - length))
            {
                throw new IndexOutOfRangeException("srcOffset out of range for copy: offset=" + srcOffset);
            }

            _buffer.SetBytes(_offset + 60, src, srcOffset, length);
        }
    }
    }
}
