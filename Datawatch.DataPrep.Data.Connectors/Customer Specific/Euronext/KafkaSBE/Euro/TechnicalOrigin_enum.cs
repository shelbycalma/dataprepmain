/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum TechnicalOrigin_enum : byte
    {
        Index_trading_arbitrage = 1,
        Portfolio_strategy = 2,
        Unwind_order = 3,
        Other_orders___default_ = 4,
        Cross_margining = 5,
        NULL_VALUE = 255
    }
}
