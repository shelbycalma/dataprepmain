/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum OpenClose_enum : byte
    {
        Open = 1,
        Close = 2,
        NULL_VALUE = 255
    }
}
