/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum TradingSessionValidity_enum : byte
    {
        Session_1 = 1,
        Session_2 = 2,
        Session_3 = 3,
        NULL_VALUE = 255
    }
}
