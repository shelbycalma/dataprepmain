/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum OrderType_enum : byte
    {
        Market = 1,
        Limit = 2,
        Stop___Stop_on_quote = 3,
        Stop_limit___Stop_on_quote_limit = 4,
        Pegged = 5,
        Market_to_limit___Market_on_opening = 6,
        NULL_VALUE = 255
    }
}
