/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum MarketModel_enum : byte
    {
        Regulated_Market = 1,
        Dark = 2,
        SI = 3,
        Quote_Driven = 4,
        Off_Market = 5,
        IPO = 6,
        Primary_Market = 7,
        Investment = 8,
        NULL_VALUE = 255
    }
}
