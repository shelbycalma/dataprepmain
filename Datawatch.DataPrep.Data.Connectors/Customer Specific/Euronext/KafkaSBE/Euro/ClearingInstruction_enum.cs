/* Generated SBE (Simple Binary Encoding) message codec */

#pragma warning disable 1591 // disable warning on missing comments
using System;
using Adaptive.SimpleBinaryEncoding;

namespace Euronext.Com
{
    public enum ClearingInstruction_enum : byte
    {
        Systematic_posting = 1,
        Manual_mode = 2,
        Automatic_extraction = 3,
        Automatic_allocation = 4,
        NULL_VALUE = 255
    }
}
