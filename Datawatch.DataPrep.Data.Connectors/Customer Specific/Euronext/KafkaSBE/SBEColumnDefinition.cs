﻿using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;

namespace Panopticon.KafkaSBEPlugin
{
    public class SBEColumnDefinition : ColumnDefinition
    {
        public SBEColumnDefinition(PropertyBag bag)
            : base(bag)
        {
        }

        public override bool IsValid
        {
            get {
                return !string.IsNullOrEmpty(Name);
            }
        }
    }
}