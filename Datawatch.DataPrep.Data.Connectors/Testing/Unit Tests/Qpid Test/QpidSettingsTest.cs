﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Xunit;

namespace Panopticon.QpidPlugin
{
    public class QpidSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            QpidSettings a = new QpidSettings(new DummyPluginHost());
            QpidSettings b = new QpidSettings(new DummyPluginHost());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            QpidSettings a = new QpidSettings(new DummyPluginHost(), new PropertyBag(), null);
            QpidSettings b = new QpidSettings(new DummyPluginHost(), new PropertyBag(), null);

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Exchange_Test()
        {
            QpidSettings a = new QpidSettings(new DummyPluginHost());
            a.Exchange = "exchange";

            QpidSettings b = new QpidSettings(new DummyPluginHost());
            b.Exchange = "exchange";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Exchange = "otherexchange";
            Assert.NotEqual(a, b);
        }        
        
        [Fact]
        public void Equals_RoutingKey_Test()
        {
            QpidSettings a = new QpidSettings(new DummyPluginHost());
            a.RoutingKey = "routingkey";

            QpidSettings b = new QpidSettings(new DummyPluginHost());
            b.RoutingKey = "routingkey";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.RoutingKey = "otherroutingkey";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            QpidSettings a = new QpidSettings(new DummyPluginHost());
            a.UserName = "username";
            a.TimeWindowSeconds = 7;

            QpidSettings b = new QpidSettings(new DummyPluginHost());
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        class DummyPluginHost : IPluginManager
        {
            private readonly PluginManager pluginManager;

            public DummyPluginHost()
            {
                pluginManager = new PluginManager
                    (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                pluginManager.LoadPlugins();
                pluginManager.InitializeDataPlugins(null, new PropertyBag());
            }

            public void Dispose()
            {
                pluginManager.Dispose();
            }

            public void LoadPlugins()
            {
                pluginManager.LoadPlugins();
            }

            public void InitializeDataPlugins(Window owner, PropertyBag globalSettings)
            {
                pluginManager.InitializeDataPlugins(owner, globalSettings);
            }

            public IPluginUI<IDataPlugin> GetPluginUI(string id)
            {
                return pluginManager.GetPluginUI(id);
            }

            public IDataPlugin GetDataPlugin(string id)
            {
                return pluginManager.GetDataPlugin(id);
            }

            public IParserPlugin GetParserPlugin(string id)
            {
                return pluginManager.GetParserPlugin(id);
            }

            public void InitializePluginUIs(Window owner)
            {
                pluginManager.InitializePluginUIs(owner);
            }

            public string[] EnabledDataPlugins { get; set; }

            public string[] SkipDataPlugins { get; set; }

            public string[] SkipDataTypes
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            public bool RemoveFromConnectToData
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            public bool BlockAtDesignTimeOnly
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            public IEnumerable<IPluginUI<IDataPlugin>> PluginUIs
            {
                get { throw new NotImplementedException(); }
            }

            public IEnumerable<IDataPlugin> DataPlugins
            {
                get { throw new NotImplementedException(); }
            }

            public IEnumerable<IParserPlugin> ParserPlugins
            {
                get { throw new NotImplementedException(); }
            }

            public IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList
            {
                get { throw new NotImplementedException(); }
            }

            public PropertyBag PluginSettings
            {
                get { throw new NotImplementedException(); }
                set { throw new NotImplementedException(); }
            }

            public IPluginErrorReportingService ErrorReporter
            {
                get
                {
                    return this.pluginManager.ErrorReporter;
                }
            }
        }
    }
}
