﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.LiveViewPlugin
{
    public class LiveViewSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            LiveViewSettings a = new LiveViewSettings();
            LiveViewSettings b = new LiveViewSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            LiveViewSettings a = new LiveViewSettings(new PropertyBag());
            LiveViewSettings b = new LiveViewSettings(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_EncloseParameterInQuote_Test()
        {
            LiveViewSettings a = new LiveViewSettings();
            a.EncloseParameterInQuote = true;
            
            LiveViewSettings b = new LiveViewSettings();
            b.EncloseParameterInQuote = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.EncloseParameterInQuote = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IdColumnName_Test()
        {
            LiveViewSettings a = new LiveViewSettings();
            a.IdColumnName = "idcolumnname";
            
            LiveViewSettings b = new LiveViewSettings();
            b.IdColumnName = "idcolumnname";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IdColumnName = "otheridcolumnname";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Predicate_Test()
        {
            LiveViewSettings a = new LiveViewSettings();
            a.Predicate = "predicate";
            
            LiveViewSettings b = new LiveViewSettings();
            b.Predicate = "predicate";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Predicate = "otherpredicate";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_PrimaryURL_Test()
        {
            LiveViewSettings a = new LiveViewSettings();
            a.PrimaryURL = "primary";
            
            LiveViewSettings b = new LiveViewSettings();
            b.PrimaryURL = "primary";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.PrimaryURL = "otherprimary";
            Assert.NotEqual(a, b);
        }

        //[Fact]
        //public void Equals_Table_Test()
        //{
        //    LiveViewSettings a = new LiveViewSettings();
        //    a.Table = "table";
            
        //    LiveViewSettings b = new LiveViewSettings();
        //    b.Table = "table";

        //    Assert.Equal(a, b);
        //    Assert.Equal(a.GetHashCode(), b.GetHashCode());

        //    b.Table = "othertable";
        //    Assert.NotEqual(a, b);
        //}

        [Fact]
        public void TimeZoneHelperTest()
        {
            LiveViewSettings a = new LiveViewSettings();
            a.TimeZoneHelper.SelectedTimeZone = "timezone";
            
            LiveViewSettings b = new LiveViewSettings();
            b.TimeZoneHelper.SelectedTimeZone = "timezone";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeZoneHelper.SelectedTimeZone = "othertimezone";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            LiveViewSettings a = new LiveViewSettings();
            a.Predicate = "predicate";
            a.TimeWindowSeconds = 7;
            
            LiveViewSettings b = new LiveViewSettings();
            b.Predicate = "predicate";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }
    }
}
