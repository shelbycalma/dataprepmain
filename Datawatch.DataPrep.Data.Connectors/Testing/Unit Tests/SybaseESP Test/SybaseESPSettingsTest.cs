﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.SybaseESPPlugin
{
    public class SybaseESPSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            SybaseESPSettings b = new SybaseESPSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings(new PropertyBag(), null);
            SybaseESPSettings b = new SybaseESPSettings(new PropertyBag(), null);

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Application_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Application = "application";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Application = "application";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Application = "otherapplication";
            Assert.NotEqual(a, b);
        }

        [Fact(Skip = "Enable again when all the c libs are in the solution")]
        public void Equals_ConnectionMode_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.ConnectionMode = ConnectionMode.Query;
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.ConnectionMode = ConnectionMode.Query;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ConnectionMode = ConnectionMode.Stream;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Host_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Host = "host";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Host = "host";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Host = "otherhost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Password_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Password = "passwd";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Password = "passwd";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "otherpasswd";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Port_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Port = "7";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Port = "7";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Port = "8";
            Assert.NotEqual(a, b);
        }

        [Fact(Skip = "Enable again when all the c libs are in the solution")]
        public void Equals_Query_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Query = "query";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Query = "query";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Query = "otherquery";
            Assert.NotEqual(a, b);
        }

        [Fact (Skip = "Enable again when all the c libs are in the solution")]
        public void Equals_Stream_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Stream = "stream";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Stream = "stream";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Stream = "otherstream";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Username_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Username = "username";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Username = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Username = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Workspace_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Workspace = "workspace";
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Workspace = "workspace";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Workspace = "otherworkspace";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.Username = "username";
            a.TimeWindowSeconds = 7;
            
            SybaseESPSettings b = new SybaseESPSettings();
            b.Username = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsEncrypted_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.IsEncrypted = true;

            //check for if the Uri is being formed correct
            Assert.True(a.Uri.StartsWith("esps://", System.StringComparison.Ordinal));

            SybaseESPSettings b = new SybaseESPSettings();
            b.IsEncrypted = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsEncrypted = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ServerQueue_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.ServerQueue = 1200;

            SybaseESPSettings b = new SybaseESPSettings();
            b.ServerQueue = 1200;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ServerQueue = 1300;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ServerWait_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.ServerWait = 800;

            SybaseESPSettings b = new SybaseESPSettings();
            b.ServerWait = 800;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ServerWait = 1300;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_PulseInterval_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.PulseInterval = 8;

            SybaseESPSettings b = new SybaseESPSettings();
            b.PulseInterval = 8;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.PulseInterval = 13;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsDroppable_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.IsDroppable = true;

            SybaseESPSettings b = new SybaseESPSettings();
            b.IsDroppable = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsDroppable = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsLossy_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.IsLossy = true;

            SybaseESPSettings b = new SybaseESPSettings();
            b.IsLossy = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsLossy = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsDeltasOnly_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.IsDeltasOnly = true;

            SybaseESPSettings b = new SybaseESPSettings();
            b.IsDeltasOnly = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsDeltasOnly = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsCallback_Test()
        {
            SybaseESPSettings a = new SybaseESPSettings();
            a.IsCallback = true;

            SybaseESPSettings b = new SybaseESPSettings();
            b.IsCallback = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsCallback = false;
            Assert.NotEqual(a, b);
        }
    }
}
