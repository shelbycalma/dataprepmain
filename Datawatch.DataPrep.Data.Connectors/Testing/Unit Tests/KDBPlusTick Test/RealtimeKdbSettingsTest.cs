﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.KDBPlugin.Realtime
{
    public class RealtimeKdbSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            RealtimeKdbSettings b = new RealtimeKdbSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings(new PropertyBag());
            RealtimeKdbSettings b = new RealtimeKdbSettings(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Host_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.Host = "host";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.Host = "host";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Host = "otherhost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Port_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.Port = "7";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.Port = "7";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Port = "8";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Username_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.UserName = "username";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }
                
        [Fact]
        public void Equals_Password_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.Password = "passwd";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.Password = "passwd";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "otherpasswd";
            Assert.NotEqual(a, b);
        }        

        [Fact]
        public void Equals_Service_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.Service = "service";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.Service = "service";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Service = "otherservice";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Table_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.Table = "table";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.Table = "table";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Table = "othertable";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Symbol_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.Symbol = "symbol";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.Symbol = "symbol";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Symbol = "othersymbol";
            Assert.NotEqual(a, b);
        }
       
        [Fact]
        public void Equals_BaseClass_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.UserName = "username";
            a.TimeWindowSeconds = 7;

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }


        [Fact]
        public void Equals_HistoricSettingsHost_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.HistoricSettings.Host = "host";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.HistoricSettings.Host = "host";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HistoricSettings.Host = "localhost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_HistoricSettingsPort_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.HistoricSettings.Port = "4";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.HistoricSettings.Port = "4";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HistoricSettings.Port = "5";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_HistoricSettingsUserName_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.HistoricSettings.UserName = "user1";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.HistoricSettings.UserName = "user1";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HistoricSettings.UserName = "user2";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_HistoricSettingsPassword_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.HistoricSettings.Password = "pwd";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.HistoricSettings.Password = "pwd";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HistoricSettings.Password = "xyz";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_HistoricSettingsQuery_Test()
        {
            RealtimeKdbSettings a = new RealtimeKdbSettings();
            a.HistoricSettings.Query = "select";

            RealtimeKdbSettings b = new RealtimeKdbSettings();
            b.HistoricSettings.Query = "select";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HistoricSettings.Query = "xyz";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SchemaColumnsSettings_Test()
        {
            PropertyBag bag = new PropertyBag();
            bag.Values["SchemaColumnsAsJsonString"] = "[{\"ColumnName\":\"PRICE\"}]";
            RealtimeKdbSettings a = new RealtimeKdbSettings(bag);

            PropertyBag bag2 = new PropertyBag();
            bag2.Values["SchemaColumnsAsJsonString"] = "[{\"ColumnName\":\"PRICE\"}]";//"[{\"IsValid\":true,\"ColumnName\":\"PRICE\",\"ColumnType\":0,\"Selected\":false,\"UserDefined\":false,\"AllowPredicate\":true,\"AggregateTypes\":[5,16,4,2,0]}]";

            RealtimeKdbSettings b = new RealtimeKdbSettings(bag2);
            
            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            bag2.Values["SchemaColumnsAsJsonString"] = "[{\"ColumnName\":\"PRICEOTHER\"}]"; // "[{\"IsValid\":true,\"ColumnName\":\"PRICEOTHER\",\"ColumnType\":0,\"Selected\":false,\"UserDefined\":false,\"AllowPredicate\":true,\"AggregateTypes\":[5,16,4,2,0]}]";
            
            Assert.NotEqual(a, b);
        }
    }
}
