﻿using Xunit;

namespace Panopticon.DatabasePlugin
{
    public class ConnectionSettingsTest
    {
        [Fact]
        public void ConstructorTest()
        {
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
        }

        [Fact]
        public void ConstructorTest2()
        {
            string expectedConnStr = @"Provider=SQLNCLI10;Server=MINIME\SQLEXPRESS;Database=testing;Uid=sa; Pwd=***;";
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
            cs.InitConnectionString(expectedConnStr);
            Assert.Equal(expectedConnStr, cs.ConnectionString);
        }

        [Fact]
        public void ConnectionStringTest()
        {
            string expectedConnStr = "Provider=SQLNCLI10;";
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
            cs.InitConnectionString(expectedConnStr);
            Assert.Equal(expectedConnStr, cs.ConnectionString);

            expectedConnStr = @"Provider=SQLNCLI10;Server=MINIME\SQLEXPRESS;";
            cs.ConnectionString = expectedConnStr;
            Assert.Equal(expectedConnStr, cs.ConnectionString);
        }

        [Fact]
        public void DatabaseConnectionStringTest()
        {
            // If ConnectionStringSettingsName is null then
            // ConnectionStringActual should be same as the ConnectionString.
            string expectedConnStr = "Provider=SQLNCLI10;";
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
            cs.InitConnectionString(expectedConnStr);
            Assert.Equal(expectedConnStr, cs.ConnectionString);
            Assert.Null(cs.ConnectionStringSettingsName);

            Assert.Equal(expectedConnStr, cs.DatabaseConnectionString);
        }

        [Fact]
        public void DatabaseConnectionString2Test()
        {
            // If ConnectionStringSettingsName is set but there is no such
            // ConnectionStringSetting found by the ConfigurationManager, then
            // ConnectionStringActual should be same as the ConnectionString.
            string expectedConnStr = "Provider=SQLNCLI10;";
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
            cs.InitConnectionString(expectedConnStr);
            Assert.Equal(expectedConnStr, cs.ConnectionString);

            string expectedConnStrName = "this conn str name can not exist";
            cs.ConnectionStringSettingsName = expectedConnStrName;
            Assert.Equal(expectedConnStrName, cs.ConnectionStringSettingsName);

            Assert.Equal(expectedConnStr, cs.DatabaseConnectionString);
        }

        [Fact]
        public void ConnectionStringSettingsNameTest()
        {
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
            cs.InitConnectionString("Provider=SQLNCLI10;");
            Assert.Null(cs.ConnectionStringSettingsName);

            string expectedConnStrName = "this conn str name can not exist";
            cs.ConnectionStringSettingsName = expectedConnStrName;
            Assert.Equal(expectedConnStrName, cs.ConnectionStringSettingsName);
        }

        [Fact]
        public void TimeZoneHelper_SelectedTimeZoneTest()
        {
            DatabaseConnectionSettings cs = new DatabaseConnectionSettings();
            cs.InitConnectionString("Provider=SQLNCLI10;");
            Assert.False(cs.QuerySettings.TimeZoneHelper.TimeZoneSelected);

            string expectedTimeZone = "Central European Standard Time";
            cs.QuerySettings.TimeZoneHelper.SelectedTimeZone = expectedTimeZone;
            Assert.Equal(expectedTimeZone, cs.QuerySettings.TimeZoneHelper.SelectedTimeZone);
            Assert.True(cs.QuerySettings.TimeZoneHelper.TimeZoneSelected);
        }
    }
}
