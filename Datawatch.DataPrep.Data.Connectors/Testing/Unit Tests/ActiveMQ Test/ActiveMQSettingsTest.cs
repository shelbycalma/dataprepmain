﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Markup;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Xunit;
using Panopticon.ActiveMQPlugin;
using System;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace ActiveMQ_Test
{
    public class ActiveMqSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager(), new PropertyBag(), null);
            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager(), new PropertyBag(), null);

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_IsCombineEvents_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.IsCombineEvents = true;

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.IsCombineEvents = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsCombineEvents = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsDurableConsumer_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.IsDurableConsumer = true;

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.IsDurableConsumer = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsDurableConsumer = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Broker_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.Broker = "tcp://localhost:61616";

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.Broker = "tcp://localhost:61616";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Broker = "tcp://somehost:61616";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Password_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.Password = "mySecret";

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.Password = "mySecret"; 

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "mySecretHacked";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Topic_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.Topic = "pano.>";

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.Topic = "pano.>";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Topic = "activeMq";
            Assert.NotEqual(a, b);
        }

        

        [Fact]
        public void Equals_UserName_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.UserName = "username";

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            ActiveMQSettings a = new ActiveMQSettings(new DummyPluginManager());
            a.UserName = "username";
            a.TimeWindowSeconds = 7;

            ActiveMQSettings b = new ActiveMQSettings(new DummyPluginManager());
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        class DummyPluginManager : IPluginManager
        {
            private readonly PluginManager pluginManager;

            public DummyPluginManager()
            {
                pluginManager = new PluginManager
                    (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                pluginManager.LoadPlugins();
                pluginManager.InitializeDataPlugins(null, new PropertyBag());
            }

            public void Dispose()
            {
                pluginManager.Dispose();
            }

            public void LoadPlugins()
            {
                pluginManager.LoadPlugins();
            }

            public void InitializeDataPlugins(Window owner, PropertyBag globalSettings)
            {
                pluginManager.InitializeDataPlugins(owner, globalSettings);
            }

            public IPluginUI<IDataPlugin> GetPluginUI(string id)
            {
                return pluginManager.GetPluginUI(id);
            }

            public IDataPlugin GetDataPlugin(string id)
            {
                return pluginManager.GetDataPlugin(id);
            }

            public IParserPlugin GetParserPlugin(string id)
            {
                return pluginManager.GetParserPlugin(id);
            }

            public void InitializePluginUIs(Window owner)
            {
                pluginManager.InitializePluginUIs(owner);
            }

            public string[] EnabledDataPlugins { get; set; }

            public string[] SkipDataPlugins { get; set; }

            public string[] SkipDataTypes
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool RemoveFromConnectToData
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool BlockAtDesignTimeOnly
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IPluginUI<IDataPlugin>> PluginUIs
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IDataPlugin> DataPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IParserPlugin> ParserPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList
            {
                get { throw new System.NotImplementedException(); }
            }

            public PropertyBag PluginSettings
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IPluginErrorReportingService ErrorReporter
            {
                get
                {
                    return this.pluginManager.ErrorReporter;
                }
            }
        }
    }
}
