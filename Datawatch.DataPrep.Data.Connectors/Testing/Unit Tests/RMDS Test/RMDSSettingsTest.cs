﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.RMDSPlugin
{
    public class RMDSSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            RMDSSettings a = new RMDSSettings();
            RMDSSettings b = new RMDSSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            RMDSSettings a = new RMDSSettings(new PropertyBag());
            RMDSSettings b = new RMDSSettings(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_DictLocationType_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.DictLocationType = DictionaryLocationType.File;

            RMDSSettings b = new RMDSSettings();
            b.DictLocationType = DictionaryLocationType.File;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.DictLocationType = DictionaryLocationType.Network;
            Assert.NotEqual(a, b);
        }
        
        [Fact]
        public void Equals_Host_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.Host = "host";

            RMDSSettings b = new RMDSSettings();
            b.Host = "host";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Host = "otherhost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IdColumnName_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.IdColumnName = "columnname";

            RMDSSettings b = new RMDSSettings();
            b.IdColumnName = "columnname";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IdColumnName = "othercolumnname";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Port_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.Port = "14002";

            RMDSSettings b = new RMDSSettings();
            b.Port = "14002";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Port = "8081";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ProcessLinks_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.ProcessLinks = true;

            RMDSSettings b = new RMDSSettings();
            b.ProcessLinks = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ProcessLinks = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Service_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.Service = "service";

            RMDSSettings b = new RMDSSettings();
            b.Service = "service";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Service = "otherservice";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Symbol_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.Symbol = "symbol";

            RMDSSettings b = new RMDSSettings();
            b.Symbol = "symbol";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Symbol = "othersymbol";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_UserName_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.UserName = "username";

            RMDSSettings b = new RMDSSettings();
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SelectedColumns_Test()
        {
            RMDSField field1 = new RMDSField("name", "longname", 1, 1);
            RMDSField field2 = new RMDSField("othername", "longname", 1, 1);

            RMDSSettings a = new RMDSSettings();
            a.SelectedColumns = new RMDSField[]{ field1 };

            RMDSSettings b = new RMDSSettings();
            b.SelectedColumns = new RMDSField[] { field1 };

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.SelectedColumns = new RMDSField[] { field2 };
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            RMDSSettings a = new RMDSSettings();
            a.UserName = "username";
            a.TimeWindowSeconds = 7;

            RMDSSettings b = new RMDSSettings();
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void SelectedColumnsTest()
        {
            RMDSSettings a = new RMDSSettings();
            Assert.NotNull(a.SelectedColumns);
            Assert.Equal(0, a.SelectedColumns.Length);

            RMDSField field0 = new RMDSField("name000", "longname000", 1, 2);
            RMDSField field1 = new RMDSField("name111", "longname111", 3, 4);
            a.SelectedColumns = new RMDSField[] { field0, field1 };
            Assert.Equal(2, a.SelectedColumns.Length);
            Assert.Equal("name000", a.SelectedColumns[0].Name);
            Assert.Equal("longname000", a.SelectedColumns[0].LongName);
            Assert.Equal(1, a.SelectedColumns[0].FieldId);
            Assert.Equal(2, a.SelectedColumns[0].Type);
            Assert.Equal("name111", a.SelectedColumns[1].Name);
            Assert.Equal("longname111", a.SelectedColumns[1].LongName);
            Assert.Equal(3, a.SelectedColumns[1].FieldId);
            Assert.Equal(4, a.SelectedColumns[1].Type);

            a.SelectedColumns = new RMDSField[0];
            Assert.Equal(0, a.SelectedColumns.Length);
        }

        [Fact]
        public void SelectedColumnsDoesNotAcceptNull()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                RMDSSettings a = new RMDSSettings();
                a.SelectedColumns = null;
            });
        }
    }
}
