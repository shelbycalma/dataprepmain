﻿using System;
using System.Globalization;
using System.Reflection;

using Xunit;

using ThomsonReuters.RFA.Common;
using ThomsonReuters.RFA.Data;
using ThomsonReuters.RFA.RDM;
using Exception = System.Exception;

namespace Panopticon.RMDSPlugin
{
    public class RMDSFieldTest
    {
        [Fact]
        public void Equals_Test()
        {
            RMDSField a = new RMDSField("name", "longname", 1, 1);
            RMDSField b = new RMDSField("name", "longname", 1, 1);

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_Name_Test()
        {
            RMDSField a = new RMDSField("name", "longname", 1, 1);
            RMDSField b = new RMDSField("othername", "longname", 1, 1);

            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_LongName_Test()
        {
            RMDSField a = new RMDSField("name", "longname", 1, 1);
            RMDSField b = new RMDSField("name", "otherlongname", 1, 1);

            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_FieldId_Test()
        {
            RMDSField a = new RMDSField("name", "longname", 1, 1);
            RMDSField b = new RMDSField("name", "longname", 0, 1);

            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Type_Test()
        {
            RMDSField a = new RMDSField("name", "longname", 1, 1);
            RMDSField b = new RMDSField("name", "longname", 1, 0);

            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Decode_Real_Test()
        {
            RDMFieldDictionary fieldDictionary = RDMFieldDictionary.Create();
            fieldDictionary.ReadRDMFieldDictionary(
                new RFA_String("RDMFieldDictionary"));
            
            RDMFidDef fidDef = fieldDictionary.GetFidDef(6);
            Assert.Equal("TRDPRC_1", fidDef.Name.ToString());
            
            // 10101010 10101010 10101010 10101010 10101010 10101010 10101010 10101010
            const long longValue = -6148914691236517206L;
            const double expectedValue = (double) longValue;

            Real realValue = new Real();
            realValue.MagnitudeType = MagnitudeTypeEnum.Exponent0;
            realValue.Value = longValue;

            DataBuffer dataBuffer = new DataBuffer();
            dataBuffer.Real = realValue;

            Assert.Equal(DataBuffer.DataBufferEnum.Real,
                dataBuffer.DataBufferType);

            RMDSField rmdsField = new RMDSField(fidDef, dataBuffer, true);

            fieldDictionary.Destroy();

            double actualValue = (double) rmdsField.Value;

            Assert.Equal(expectedValue, actualValue);
        }

        [Fact]
        public void Validate_MagnitudeTypeEnum_Test()
        {
            // The new Real parsing is using a lookup table based on the
            // members and values in MagnitudeTypeEnum.
            // Make sure these dont change.

            FieldInfo[] fieldInfos = typeof(MagnitudeTypeEnum).GetFields();
            Assert.True(31 == fieldInfos.Length,
                "Need to update RMDSField.MagnitudeTypeToScale");

            Assert.Equal(0, MagnitudeTypeEnum.ExponentNeg14);
            Assert.Equal(1, MagnitudeTypeEnum.ExponentNeg13);
            Assert.Equal(2, MagnitudeTypeEnum.ExponentNeg12);
            Assert.Equal(3, MagnitudeTypeEnum.ExponentNeg11);
            Assert.Equal(4, MagnitudeTypeEnum.ExponentNeg10);
            Assert.Equal(5, MagnitudeTypeEnum.ExponentNeg9);
            Assert.Equal(6, MagnitudeTypeEnum.ExponentNeg8);
            Assert.Equal(7, MagnitudeTypeEnum.ExponentNeg7);
            Assert.Equal(8, MagnitudeTypeEnum.ExponentNeg6);
            Assert.Equal(9, MagnitudeTypeEnum.ExponentNeg5);
            Assert.Equal(10, MagnitudeTypeEnum.ExponentNeg4);
            Assert.Equal(11, MagnitudeTypeEnum.ExponentNeg3);
            Assert.Equal(12, MagnitudeTypeEnum.ExponentNeg2);
            Assert.Equal(13, MagnitudeTypeEnum.ExponentNeg1);
            Assert.Equal(14, MagnitudeTypeEnum.Exponent0);
            Assert.Equal(15, MagnitudeTypeEnum.ExponentPos1);
            Assert.Equal(16, MagnitudeTypeEnum.ExponentPos2);
            Assert.Equal(17, MagnitudeTypeEnum.ExponentPos3);
            Assert.Equal(18, MagnitudeTypeEnum.ExponentPos4);
            Assert.Equal(19, MagnitudeTypeEnum.ExponentPos5);
            Assert.Equal(20, MagnitudeTypeEnum.ExponentPos6);
            Assert.Equal(21, MagnitudeTypeEnum.ExponentPos7);
            Assert.Equal(22, MagnitudeTypeEnum.Divisor1);
            Assert.Equal(23, MagnitudeTypeEnum.Divisor2);
            Assert.Equal(24, MagnitudeTypeEnum.Divisor4);
            Assert.Equal(25, MagnitudeTypeEnum.Divisor8);
            Assert.Equal(26, MagnitudeTypeEnum.Divisor16);
            Assert.Equal(27, MagnitudeTypeEnum.Divisor32);
            Assert.Equal(28, MagnitudeTypeEnum.Divisor64);
            Assert.Equal(29, MagnitudeTypeEnum.Divisor128);
            Assert.Equal(30, MagnitudeTypeEnum.Divisor256);
        }

        [Fact]
        public void CompareDecodeRealWithReferenceImplementationTest()
        {
            // Compare our lookup implementation of parsing Real to the
            // ported sample implementation sent by
            // klitos.kyriacou@thomsonreuters.com.

            ValidateAllMagnitudeTypes(0);
            ValidateAllMagnitudeTypes(1);
            ValidateAllMagnitudeTypes(2059);

            for (int i = 0; i < 64; i++)
            {
                long value = (long) (0xFFFFFFFFFFFFFFFF >> (63 - i));
                ValidateAllMagnitudeTypes(value);
            }

            for (int i = 0; i < 64; i++)
            {
                long value = 1L << i;
                ValidateAllMagnitudeTypes(value);
            }

            Random random = new Random(0);
            for (int i = 0; i < 10000; i++)
            {
                long value = ((long) random.Next()) << 32 + random.Next();
                ValidateAllMagnitudeTypes(value);
            }
        }

        private static void ValidateAllMagnitudeTypes(long value)
        {
            const double relativeError = 1e-15;
            Real real = new Real();
            real.Value = value;
            for (byte magType = 0; magType <= 30; magType++)
            {
                real.MagnitudeType = magType;
                double referenceResult = ParseRealReferenceCodeFromTR(real);
                double decodeRealResult = RMDSField.DecodeReal(real);
                double delta = Math.Abs(referenceResult * relativeError);
                Assert.True(
                    Math.Abs(referenceResult - decodeRealResult) <= delta,
                    string.Format(
                        "Real: value={0}, magType={1}",
                        real.Value, real.MagnitudeType));

                double stringParseResult = OldVersionUsingStringParse(real);
                if (!double.IsNaN(stringParseResult))
                {
                    delta = Math.Abs(referenceResult * relativeError);
                    Assert.True(
                        Math.Abs(referenceResult - stringParseResult) <= delta,
                        "compare to string parse");
                }
            }
        }

        // Here is the reference implementation from TR.
        // And below is the port to c# (ParseRealReferenceCodeFromTR).
        //
        //double RfaContext::getValue(const Real64 & r)
        //{
        //      long double value = r.getValue();
        //      UInt8 magtyp = r.getMagnitudeType();
        //      if (magtyp < Divisor1) {
        //            // Decimal value
        //            int exponent = magtyp - Exponent0;
        //            if (exponent < 0)
        //                  while (exponent++ != 0)
        //                        value /= 10;
        //            else
        //                  while (exponent-- != 0)
        //                        value *= 10;
        //      }
        //      else {
        //            // Fractional representation with a power-of-two divisor
        //            for (int exponent = magtyp - Divisor1; exponent > 0; --exponent)
        //                  value /= 2;
        //      }
        //      return static_cast<double>(value);
        //}

        private static double ParseRealReferenceCodeFromTR(Real r)
        {
            double value = r.Value;
            byte magtyp = r.MagnitudeType;
            if (magtyp < MagnitudeTypeEnum.Divisor1)
            {
                // Decimal value
                int exponent = magtyp - MagnitudeTypeEnum.Exponent0;
                if (exponent < 0)
                    while (exponent++ != 0)
                        value /= 10;
                else
                    while (exponent-- != 0)
                        value *= 10;
            }
            else
            {
                // Fractional representation with a power-of-two divisor
                for (int exponent = magtyp - MagnitudeTypeEnum.Divisor1; exponent > 0; --exponent)
                    value /= 2;
            }
            return (double)value;
        }

        private static double OldVersionUsingStringParse(Real r)
        {
            DataBuffer dataBuffer = new DataBuffer();
            dataBuffer.Real = r;
            RFA_String rfaString = dataBuffer.GetAsString();
            string valueString = rfaString.ToString();
            
            // not able to parse these strings
            if (valueString == "" || valueString.Contains("/"))
            {
                return double.NaN;
            }

            try {
                double value = Convert.ToDouble(valueString,
                    CultureInfo.InvariantCulture);
                return value;
            }
            catch (Exception)
            {
                Console.WriteLine("OldVersionUsingStringParse: failed to parse {0}",
                    valueString);
                return double.NaN;
            }
        }
    }
}
