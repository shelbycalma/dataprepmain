﻿using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Parameters;
using Datawatch.DataPrep.Data.Framework.Parameters;
using Xunit;

namespace Panopticon.RMDSPlugin
{
    public class UtilTest
    {
        [Fact]
        public void IsValidLinkFid_RefCountZero_Test()
        {
            int refCount = 0;

            int tooLowFid = Util.LinkFid[0] - 1;
            Assert.False(Util.IsValidLinkFid(tooLowFid, refCount));

            foreach (int fid in Util.LinkFid)
            {
                Assert.False(Util.IsValidLinkFid(fid, refCount));
            }

            int tooHighFid = Util.LinkFid[Util.LinkFid.Count - 1] + 1;
            Assert.False(Util.IsValidLinkFid(tooHighFid, refCount));
        }

        [Fact]
        public void IsValidLinkFid_RefCount14_Test()
        {
            int refCount = 14;

            int tooLowFid = Util.LinkFid[0] - 1;
            Assert.False(Util.IsValidLinkFid(tooLowFid, refCount));

            foreach (int fid in Util.LinkFid)
            {
                Assert.True(Util.IsValidLinkFid(fid, refCount));
            }

            int tooHighFid = Util.LinkFid[Util.LinkFid.Count - 1] + 1;
            Assert.False(Util.IsValidLinkFid(tooHighFid, refCount));
        }

        [Fact]
        public void IsValidLinkFid_RefCount1_Test()
        {
            int refCount = 1;

            int tooLowFid = Util.LinkFid[0] - 1;
            Assert.False(Util.IsValidLinkFid(tooLowFid, refCount));

            for (int i = 0; i < refCount; i++)
            {
                Assert.True(Util.IsValidLinkFid(Util.LinkFid[i], refCount));
            }

            Assert.False(Util.IsValidLinkFid(Util.LinkFid[refCount], refCount));

            int tooHighFid = Util.LinkFid[Util.LinkFid.Count - 1] + 1;
            Assert.False(Util.IsValidLinkFid(tooHighFid, refCount));
        }

        [Fact]
        public void IsValidLongFid_RefCountZero_Test()
        {
            int refCount = 0;

            Assert.False(Util.IsValidLongFid(Util.LongFid[0] - 1, refCount));

            foreach (int fid in Util.LongFid)
            {
                Assert.False(Util.IsValidLongFid(fid, refCount));
            }

            int tooHighFid = Util.LongFid[Util.LongFid.Count - 1] + 1;
            Assert.False(Util.IsValidLongFid(tooHighFid, refCount));
        }

        [Fact]
        public void IsValidLongFid_RefCount14_Test()
        {
            int refCount = 14;

            int tooLowFid = Util.LongFid[0] - 1;
            Assert.False(Util.IsValidLongFid(tooLowFid, refCount));

            foreach (int fid in Util.LongFid)
            {
                Assert.True(Util.IsValidLongFid(fid, refCount));
            }

            int tooHighFid = Util.LongFid[Util.LongFid.Count - 1] + 1;
            Assert.False(Util.IsValidLongFid(tooHighFid, refCount));
        }

        [Fact]
        public void IsValidLongFid_RefCount1_Test()
        {
            int refCount = 1;

            int tooLowFid = Util.LongFid[0] - 1;
            Assert.False(Util.IsValidLongFid(tooLowFid, refCount));

            for (int i = 0; i < refCount; i++)
            {
                Assert.True(Util.IsValidLongFid(Util.LongFid[i], refCount));
            }

            Assert.False(Util.IsValidLongFid(Util.LongFid[refCount], refCount));

            int tooHighFid = Util.LongFid[Util.LongFid.Count - 1] + 1;
            Assert.False(Util.IsValidLongFid(tooHighFid, refCount));
        }

        [Fact]
        public void ApplyParametersToSymbol_Single_RIC_Null_Params_Test()
        {
            string source = "MSFT.O";

            string[] subscribedSymbols =
                Util.ApplyParametersToSymbol(source, null);
            
            Assert.NotNull(subscribedSymbols);
            Assert.Equal(1, subscribedSymbols.Length);
            Assert.Equal("MSFT.O", subscribedSymbols[0]);
        }

        [Fact]
        public void ApplyParametersToSymbol_Single_RIC_No_Params_Test()
        {
            string source = "MSFT.O";

            ParameterValueCollection pvc = new ParameterValueCollection();

            string[] subscribedSymbols =
                Util.ApplyParametersToSymbol(source, pvc);
            
            Assert.NotNull(subscribedSymbols);
            Assert.Equal(1, subscribedSymbols.Length);
            Assert.Equal("MSFT.O", subscribedSymbols[0]);
        }

        [Fact]
        public void ApplyParametersToSymbol_Two_RICs_No_Params_Test()
        {
            string source = "MSFT.O,SAABb.ST";
            
            ParameterValueCollection pvc = new ParameterValueCollection();

            string[] subscribedSymbols =
                Util.ApplyParametersToSymbol(source, pvc);
            
            Assert.NotNull(subscribedSymbols);
            Assert.Equal(2, subscribedSymbols.Length);
            Assert.Equal("MSFT.O", subscribedSymbols[0]);
            Assert.Equal("SAABb.ST", subscribedSymbols[1]);
        }

        [Fact]
        public void ApplyParametersToSymbol_One_Param_Value_Test()
        {
            string source = "{ricparam}";

            ArrayParameterValue arrayParameterValue = new ArrayParameterValue();
            arrayParameterValue.Values.Add(new StringParameterValue("MSFT.O"));
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc.Add("ricparam", arrayParameterValue);
            
            string[] subscribedSymbols =
                Util.ApplyParametersToSymbol(source, pvc);
            
            Assert.NotNull(subscribedSymbols);
            Assert.Equal(1, subscribedSymbols.Length);
            Assert.Equal("MSFT.O", subscribedSymbols[0]);
        }

        [Fact]
        public void ApplyParametersToSymbol_Two_Param_Value_Test()
        {
            string source = "{ricparam}";

            ArrayParameterValue arrayParameterValue = new ArrayParameterValue();
            arrayParameterValue.Values.Add(new StringParameterValue("MSFT.O"));
            arrayParameterValue.Values.Add(new StringParameterValue("SAABb.ST"));
            ParameterValueCollection pvc = new ParameterValueCollection();
            pvc.Add("ricparam", arrayParameterValue);
            
            string[] subscribedSymbols =
                Util.ApplyParametersToSymbol(source, pvc);
            
            Assert.NotNull(subscribedSymbols);
            Assert.Equal(2, subscribedSymbols.Length);
            Assert.Equal("MSFT.O", subscribedSymbols[0]);
            Assert.Equal("SAABb.ST", subscribedSymbols[1]);
        }
    }
}
