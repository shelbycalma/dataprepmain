﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Xunit;
using Panopticon.KafkaPlugin;
using System;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Kafka_Test
{
    public class KafkaSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            KafkaSettings b = new KafkaSettings(new DummyPluginManager());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager(), new PropertyBag());
            KafkaSettings b = new KafkaSettings(new DummyPluginManager(), new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_IsCombineEvents_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.IsCombineEvents = true;

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.IsCombineEvents = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsCombineEvents = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Host_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.Host = "http://localhost";

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.Host = "http://localhost";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Host = "http://somehost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Port_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.Port = "9092";

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.Port = "9092";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Port = "9093";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Password_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.Password = "mySecret";

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.Password = "mySecret"; 

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "mySecretHacked";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Topic_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.Topic = "pano.>";

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.Topic = "pano.>";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Topic = "kafka";
            Assert.NotEqual(a, b);
        }

        

        [Fact]
        public void Equals_UserName_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.UserName = "username";

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            KafkaSettings a = new KafkaSettings(new DummyPluginManager());
            a.UserName = "username";
            a.TimeWindowSeconds = 7;

            KafkaSettings b = new KafkaSettings(new DummyPluginManager());
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        class DummyPluginManager : IPluginManager
        {
            private readonly PluginManager pluginManager;

            public DummyPluginManager()
            {
                pluginManager = new PluginManager
                    (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                pluginManager.LoadPlugins();
                pluginManager.InitializeDataPlugins(null, new PropertyBag());
            }
            
            public void Dispose()
            {
                pluginManager.Dispose();
            }

            public void LoadPlugins()
            {
                pluginManager.LoadPlugins();
            }

            public void InitializeDataPlugins(Window owner, PropertyBag globalSettings)
            {
                pluginManager.InitializeDataPlugins(owner, globalSettings);
            }

            public IPluginUI<IDataPlugin> GetPluginUI(string id)
            {
                throw new System.NotImplementedException();
            }
            
            public IDataPlugin GetDataPlugin(string id)
            {
                return pluginManager.GetDataPlugin(id);
            }

            public IParserPlugin GetParserPlugin(string id)
            {
                return pluginManager.GetParserPlugin(id);
            }

            public void InitializePluginUIs(Window owner)
            {
                pluginManager.InitializePluginUIs(owner);
            }

            public string[] EnabledDataPlugins { get; set; }

            public string[] SkipDataPlugins { get; set; }

            public string[] SkipDataTypes
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool RemoveFromConnectToData
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool BlockAtDesignTimeOnly
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IPluginUI<IDataPlugin>> PluginUIs
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IDataPlugin> DataPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IParserPlugin> ParserPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList
            {
                get { throw new System.NotImplementedException(); }
            }

            public PropertyBag PluginSettings
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IPluginErrorReportingService ErrorReporter
            {
                get
                {
                    return this.pluginManager.ErrorReporter;
                }
            }
        }
    }
}
