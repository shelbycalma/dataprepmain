﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;

namespace Panopticon.AnalysisServicesPlugin
{
    class StefansQueryBuilder
    {
        public string BuildQuery(OnDemandParameters qod)
        {
            StringBuilder query = new StringBuilder();

            query.Append("SELECT");

            string[] columnAttributes = GetColumnAttributes(qod);

            query.Append(" {");
            if (columnAttributes.Length > 0)
            {
                //query.Append("    ");
                query.Append(string.Join(",", columnAttributes));
                //query.AppendLine();
            }
            query.Append("}");
            query.Append(" DIMENSION PROPERTIES LEVEL_NUMBER");
            query.Append(" ON COLUMNS,");

            string[] rowAttributes = GetRowFilteredAttributes(qod)
                .Union(GetRowAttributes(qod)).ToArray();

            if (rowAttributes.Length > 1)
            {
                query.Append("NON EMPTY CROSSJOIN(");
                //query.Append("    ");
                query.Append(string.Join(", ", rowAttributes));
                //query.AppendLine();
                query.Append(")");
            }
            else if (rowAttributes.Length == 1)
            {
                //query.Append("  ");
                query.Append(rowAttributes[0]);
            }
            else
            {
                query.Append("  { Geography.Geography.DefaultMember }");
            }
            query.Append(" ON ROWS ");

            query.Append("FROM ");

            Predicate[] predicates = qod.Predicates ?? new Predicate[0];
            query.Append(BuildPredicatedSubCube(predicates, 0));

            string[] slicerAttributes = GetSlicerAttributes(qod);
            if (slicerAttributes.Length > 0)
            {
                query.Append("WHERE (");
                //query.Append("    ");
                query.Append(string.Join(",", slicerAttributes));
                //query.AppendLine();
                query.Append("  )");
            }

            return query.ToString();
        }

        private string[] GetColumnAttributes(OnDemandParameters qod)
        {
            // Measures on columns.
            OnDemandAggregate[] aggregates =
                qod.Aggregates ?? new OnDemandAggregate[0];

            IEnumerable<string> measures = aggregates
                .Where(a => a.FunctionType == FunctionType.External)
                .Select(a => a.Columns.Single());

            return measures.ToArray();
        }

        private string[] GetRowAttributes(OnDemandParameters qod)
        {
            // Only dimension attributes from active part of breakdown (below
            // the drill path, and above the depth).
            string[] breakdown = qod.BreakdownColumns ?? new string[0];
            int drillDepth = qod.DrillPath != null ? qod.DrillPath.Length : 0;

            IEnumerable<string> attributes = breakdown
                .Take(qod.Depth).Skip(drillDepth)
                .Select(c => string.Format("{0}.MEMBERS", c));

            return attributes.ToArray();
        }

        private string[] GetRowFilteredAttributes(OnDemandParameters qod)
        {
            // Dimension attributes from drill path that we cannot use as
            // slicers (member is different from caption).
            string[] drill = qod.DrillPath ?? new string[0];
            string[] breakdown = qod.BreakdownColumns ?? new string[0];

            IEnumerable<string> slicers = drill
                .Zip(breakdown,
                    (path, column) => TryCreateFilter(column, path))
                .Where(s => s != null);

            return slicers.ToArray();
        }

        private string[] GetSlicerAttributes(OnDemandParameters qod)
        {
            // Only dimension attributes from drill path that we can use as
            // slicers (member name is same as caption).
            string[] drill = qod.DrillPath ?? new string[0];
            string[] breakdown = qod.BreakdownColumns ?? new string[0];

            IEnumerable<string> slicers = drill
                .Zip(breakdown,
                    (path, column) => TryCreateSlicer(column, path))
                .Where(s => s != null);

            return slicers.ToArray();
        }

        private string TryCreateSlicer(string column, string path)
        {
            if (!IsCaptionSameAsName(column))
            {
                return null;
            }
            return string.Format("{0}.&[{1}]", column, path);
        }

        private string TryCreateFilter(string column, string path)
        {
            if (IsCaptionSameAsName(column))
            {
                return null;
            }
            return string.Format("Filter({0}.MEMBERS, " +
                "{0}.CurrentMember.MEMBER_CAPTION = \"{1}\")",
                column, path);
        }

        private bool IsCaptionSameAsName(string column)
        {
            return column == "Geography.Country";
        }

        private string BuildPredicatedSubCube(
            Predicate[] predicates, int index)
        {
            if (index == predicates.Length)
            {
                return "[Adventure Works]";
            }

            Predicate predicate = predicates[index];
            Comparison comparison = predicate as Comparison;
            if (comparison == null ||
                (comparison.Operator != Operator.In &&
                 comparison.Operator != Operator.Like))
            {
                throw new NotImplementedException(
                    "Sorry, only 'IN' and 'LIKE' comparisons for now.");
            }

            ColumnParameter column = (ColumnParameter)comparison.Left;

            StringBuilder query = new StringBuilder();
            string indent = new string(' ', 4 * index);
            query.AppendLine("(");
            query.Append(indent);
            query.AppendLine("    SELECT");
            query.Append(indent);
            query.Append("      Filter(");
            query.Append(column.ColumnName);
            query.AppendLine(".MEMBERS,");
            query.Append(indent);
            query.Append("        ");
            if (comparison.Operator == Operator.In)
            {
                ListParameter values = (ListParameter)comparison.Right;
                query.Append(string.Join(
                    " OR\r\n" + indent + "        ",
                    values.Values.Select(v => string.Format(
                        "{0}.CurrentMember.MEMBER_CAPTION = \"{1}\"",
                        column.ColumnName, v))));
            }
            else
            {
                StringParameter pattern = (StringParameter)comparison.Right;
                string cleanPattern = CleanLikePattern(pattern.TextValue);
                query.Append("InStr(");
                query.Append(column.ColumnName);
                query.Append(".CurrentMember.MEMBER_CAPTION, ");
                query.Append("\"");
                query.Append(cleanPattern);
                query.Append("\") = 1");
            }
            query.AppendLine();
            query.Append(indent);
            query.AppendLine("      ) ON COLUMNS");
            query.Append(indent);
            query.Append("    FROM ");
            query.AppendLine(BuildPredicatedSubCube(predicates, index + 1));
            query.Append(indent);
            query.Append("  )");

            return query.ToString();
        }

        private string CleanLikePattern(string pattern)
        {
            if (pattern.Length < 2 ||
                pattern.Last() != '%')
            {
                throw new NotImplementedException(
                    "Invalid LIKE pattern \"" + pattern + "\".");
            }
            string clean = pattern.Substring(0, pattern.Length - 1);
            if (clean.IndexOf('%') >= 0)
            {
                throw new NotImplementedException(
                    "Multiple '%' in LIKE pattern \"" + pattern + "\".");
            }
            return clean;
        }

    }
}
