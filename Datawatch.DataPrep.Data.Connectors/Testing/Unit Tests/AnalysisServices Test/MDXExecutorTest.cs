﻿using System;
using System.Collections.Generic;
using Datawatch.DataPrep.Data.Core.Tables.Standalone;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Query;
using Xunit;
using Panopticon.AnalysisServicesPlugin.MDX;
using Panopticon.AnalysisServicesPlugin;

namespace Panopticon.AnalysisServicesPlugin
{
    public class MDXExecutorTest
    {
        private const string SingleColumnOnBreakdownMdx =
            "SELECT {\r\n\r\n} ON COLUMNS,\r\nHierarchize(Geography.Country.Members) " +
            "ON ROWS \r\nFROM [Adventure Works] \r\n";

        [Fact]
        public void SingleColumnOnBreakdownTest()
        {
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            MDXConnectionSettings settings = new MDXConnectionSettings()
            {
                CubeName = "Adventure Works"
            };

            onDemandParameters.BreakdownColumns = new[]{"Geography.Country"};

            QueryBuilder queryBuilder = 
                MDXExecutor.BuildOnDemandQuery(settings, onDemandParameters);
            //queryBuilder.Axises[0].DimensionProperties.Add("LEVEL_NUMBER");
            string result = queryBuilder.ToExpression();
            Assert.Equal(SingleColumnOnBreakdownMdx, result, StringComparer.OrdinalIgnoreCase);
        }

        private const string TwoColumnsOnBreakdownMdx =
            "SELECT {\r\n\r\n} DIMENSION PROPERTIES LEVEL_NUMBER ON COLUMNS,\r\n" +
            "Crossjoin(Hierarchize(Geography.Country.Members), Hierarchize(Product.Category.Members)) ON ROWS" +
            " \r\nFROM [Adventure Works] \r\n";

        [Fact]
        public void TwoColumnOnBreakdownTest()
        {
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            MDXConnectionSettings settings = new MDXConnectionSettings() { CubeName = "Adventure Works" };

            onDemandParameters.BreakdownColumns = 
                new[] { "Geography.Country", "Product.Category" };

            QueryBuilder queryBuilder =
                MDXExecutor.BuildOnDemandQuery(settings, onDemandParameters);
            queryBuilder.Axises[0].DimensionProperties.Add("LEVEL_NUMBER");
            string result = queryBuilder.ToExpression();
            Assert.Equal(TwoColumnsOnBreakdownMdx, result);
        }

        private const string DimensionPropertiesMdx =
            "SELECT {\r\n\r\n} ON COLUMNS,\r\nCrossjoin(Hierarchize(Geography.Country.Members), " +
            "Hierarchize(Product.Category.Members)) DIMENSION PROPERTIES " +
            "MEMBER_CAPTION,LEVEL_NUMBER ON ROWS \r\nFROM [Adventure Works] \r\n";

        [Fact]
        public void DimensionPropertiesTest()
        {
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            MDXConnectionSettings settings = new MDXConnectionSettings()
            {
                CubeName = "Adventure Works"
            };

            onDemandParameters.BreakdownColumns =
                new[] { "Geography.Country", "Product.Category" };

            QueryBuilder queryBuilder =
                MDXExecutor.BuildOnDemandQuery(settings, onDemandParameters);

            queryBuilder.Axises[1].DimensionProperties.Add("MEMBER_CAPTION");
            queryBuilder.Axises[1].DimensionProperties.Add("LEVEL_NUMBER");

            Assert.Equal(DimensionPropertiesMdx, queryBuilder.ToExpression());            
        }

        private const string PredicatesMdx =
           "SELECT Hierarchize((\r\n[Customer].[City].MEMBERS," +
           "\r\n[Customer].[Education].MEMBERS,\r\n" +
           "[Customer].[Gender].MEMBERS\r\n)) ON COLUMNS," +
           "{ Filter([Customer].[Email Address].MEMBERS," +
           "[Customer].[Email Address].CurrentMember.MemberValue " +
           "= \"aaron22@adventure-works.com\")} ON ROWS FROM [Adventure Works]";

        // TODO: Generated query lacks braces around Filter
        [Fact(Skip = "Temporarily disabled - fixing required")]
        public void PredicatesTest()
        {
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            MDXConnectionSettings settings = new MDXConnectionSettings()
            {
                CubeName = "Adventure Works",
            };

            onDemandParameters.AuxiliaryColumns = new string[]
            {
                "[Customer].[City]",
                "[Customer].[Education]",
                "[Customer].[Gender]"
            };

            onDemandParameters.BreakdownColumns = new string[]
            {
                "[Customer].[City]"
            };

            onDemandParameters.Predicates = new Predicate[]
            {
                new Comparison(Operator.In, 
                    new ColumnParameter("[Customer].[Email Address]"), 
                    new ListParameter(new []{"aaron22@adventure-works.com"})), 
            };
            QueryBuilder queryBuilder =
                MDXExecutor.BuildOnDemandQuery(settings, onDemandParameters);

            Assert.Equal(queryBuilder.ToExpression(), PredicatesMdx, StringComparer.OrdinalIgnoreCase);
        }


        private const string AggregatesMdx = "WITH MEMBER [Measures].[End of Day Rate] AS [Measures].[End of Day Rate] " +
                                             "MEMBER [Measures].[Gross Profit] AS [Measures].[End of Day Rate] SELECT " +
                                             "Crossjoin(Hierarchize([Account].[Account Type].Members), Hierarchize([Account].[Accounts].Members), " +
                                             "Hierarchize([Account].[Account Number].Members)) ON ROWS,\r\n{\r\n[Measures].[End of Day Rate],\r\n[Measures].[Gross Profit]\r\n} ON COLUMNS " +
                                             "\r\nFROM [Adventure Works] \r\n";

        [Fact]
        public void AggregatesTest()
        {
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            MDXConnectionSettings settings = new MDXConnectionSettings
            {
                CubeName = "Adventure Works",
            };

            onDemandParameters.BreakdownColumns = new[]
            {
                "[Account].[Account Type]",
                "[Account].[Accounts]",
                "[Account].[Account Number]"
            };

            onDemandParameters.Aggregates = new[]
            {
                new OnDemandAggregate(FunctionType.External, new List<NumericColumn>
                {
                    new NumericColumn("[Measures].[End of Day Rate]")
                }, "[Measures].[End of Day Rate]"), 
                new OnDemandAggregate(FunctionType.External, new List<NumericColumn>
                {
                    new NumericColumn("[Measures].[End of Day Rate]")
                }, "[Measures].[Gross Profit]"), 
            };

            QueryBuilder queryBuilder =
                MDXExecutor.BuildOnDemandQuery(settings, onDemandParameters);
            Assert.Equal(queryBuilder.ToExpression(), AggregatesMdx);
        }
    }
}
