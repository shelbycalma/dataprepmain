﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Panopticon.Dashboards.Model;
using Panopticon.MDXQoDPlugin.MDX;

namespace Panopticon.MDXQoDPlugin
{
    class ExecutorTest
    {
        private const string singleColumnOnBreakdownMDX =
            "SELECT {[Measures].[Internet Sales Count]} ON COLUMNS , " + 
            "NON EMPTY Hierarchize(DrilldownLevel({[Product].[Product Categories].[All Products]},,,INCLUDE_CALC_MEMBERS)) DIMENSION PROPERTIES PARENT_UNIQUE_NAME,HIERARCHY_UNIQUE_NAME ON ROWS  " + 
            "FROM [Internet Sales] CELL PROPERTIES VALUE, FORMAT_STRING";

        [Fact]
        public void SingleColumnOnBreakdownTest()
        {
            OnDemandParameters onDemandParameters = new OnDemandParameters();
            QueryBuilder queryBuilder = new QueryBuilder();            
            Executor.BuildOnDemandQuery(queryBuilder, onDemandParameters);
            Assert.AreEqual(singleColumnOnBreakdownMDX, queryBuilder.BuildExpression());
        }
    }
}
