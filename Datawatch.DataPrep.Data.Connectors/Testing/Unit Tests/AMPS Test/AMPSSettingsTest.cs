﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Datawatch.DataPrep.Data.Framework.Plugin.MessageParsing;
using Xunit;

namespace Panopticon.AMPSPlugin
{
    public class AMPSSettingsTest
    {

        [Fact]
        public void Equals_BaseClass_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.Host = "host";
            a.TimeWindowSeconds = 7;

            var b = new AMPSSettings(new DummyPluginManager());
            b.Host = "host";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager(), new PropertyBag());
            var b = new AMPSSettings(new DummyPluginManager(), new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            var b = new AMPSSettings(new DummyPluginManager());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_BatchSize_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.BatchSize = 100;

            var b = new AMPSSettings(new DummyPluginManager());
            b.BatchSize = 100;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.BatchSize = 200;

            Assert.NotEqual(a, b);
            Assert.NotEqual(a.GetHashCode(), b.GetHashCode());
        }

        [Fact]
        public void Equals_Filter_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.Filter = "filter";

            var b = new AMPSSettings(new DummyPluginManager());
            b.Filter = "filter";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Filter = "otherfilter";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Host_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.Host = "host";

            var b = new AMPSSettings(new DummyPluginManager());
            b.Host = "host";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Host = "otherhost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_MessageType_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.MessageType = MessageType.Fix;

            var b = new AMPSSettings(new DummyPluginManager());
            b.MessageType = MessageType.Fix;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.MessageType = MessageType.NvFix;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ParserSettings_MessageType_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.MessageType = MessageType.Fix;
            ColumnDefinition cda = a.ParserSettings.
                CreateColumnDefinition(new PropertyBag());
            cda.Name = "columndefinition";
            a.ParserSettings.AddColumnDefinition(cda);

            var b = new AMPSSettings(new DummyPluginManager());
            b.MessageType = MessageType.Fix;
            ColumnDefinition cdb = b.ParserSettings.
                CreateColumnDefinition(new PropertyBag());
            cdb.Name = "columndefinition";
            b.ParserSettings.AddColumnDefinition(cdb);
            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.MessageType = MessageType.Xml;

            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ParserSettings_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.MessageType = MessageType.Fix;
            ColumnDefinition cda = a.ParserSettings.
                CreateColumnDefinition(new PropertyBag());
            cda.Name = "columndefinition";
            a.ParserSettings.AddColumnDefinition(cda);

            var b = new AMPSSettings(new DummyPluginManager());
            b.MessageType = MessageType.Fix;
            ColumnDefinition cdb = b.ParserSettings.
                CreateColumnDefinition(new PropertyBag());
            cdb.Name = "columndefinition";
            b.ParserSettings.AddColumnDefinition(cdb);
            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            ColumnDefinition cdb2 = b.ParserSettings.
                CreateColumnDefinition(new PropertyBag());
            cda.Name = "columndefinition2";
            b.ParserSettings.AddColumnDefinition(cdb2);

            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Port_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.Port = 7;

            var b = new AMPSSettings(new DummyPluginManager());
            b.Port = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Port = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SubscriptionMode_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.SubscriptionMode = SubscriptionMode.SowAndDeltaSubscribe;

            var b = new AMPSSettings(new DummyPluginManager());
            b.SubscriptionMode = SubscriptionMode.SowAndDeltaSubscribe;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.SubscriptionMode = SubscriptionMode.DeltaSubscribe;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Timout_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.Timeout = 1000;

            var b = new AMPSSettings(new DummyPluginManager());
            b.Timeout = 1000;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Timeout = 2000;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Topic_Test()
        {
            var a = new AMPSSettings(new DummyPluginManager());
            a.Topic = "topic";

            var b = new AMPSSettings(new DummyPluginManager());
            b.Topic = "topic";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Topic = "othertopic";
            Assert.NotEqual(a, b);
        }

        class DummyPluginManager : IPluginManager
        {
            private readonly PluginManager pluginManager;

            public DummyPluginManager()
            {
                pluginManager = new PluginManager
                    (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                pluginManager.LoadPlugins();
                pluginManager.InitializeDataPlugins(null,new PropertyBag());
            }

            public void Dispose()
            {
                pluginManager.Dispose();
            }

            public void LoadPlugins()
            {
                pluginManager.LoadPlugins();
            }

            public void InitializeDataPlugins(Window owner, PropertyBag globalSettings)
            {
                pluginManager.InitializeDataPlugins(owner, globalSettings);
            }

            public IPluginUI<IDataPlugin> GetPluginUI(string id)
            {
                return pluginManager.GetPluginUI(id);
            }

            public IDataPlugin GetDataPlugin(string id)
            {
                return pluginManager.GetDataPlugin(id);
            }

            public IParserPlugin GetParserPlugin(string id)
            {
                return pluginManager.GetParserPlugin(id);
            }

            public void InitializePluginUIs(Window owner)
            {
                pluginManager.InitializePluginUIs(owner);
            }

            public string[] EnabledDataPlugins { get; set; }

            public string[] SkipDataPlugins { get; set; }

            public string[] SkipDataTypes
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool RemoveFromConnectToData
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool BlockAtDesignTimeOnly
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IPluginUI<IDataPlugin>> PluginUIs
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IDataPlugin> DataPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IParserPlugin> ParserPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList
            {
                get { throw new System.NotImplementedException(); }
            }

            public PropertyBag PluginSettings
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IPluginErrorReportingService ErrorReporter
            {
                get
                {
                    return this.pluginManager.ErrorReporter;
                }
            }

            public IEnumerable<PluginLoadingFailureInfo> FailedToLoadPlugins
            {
                get
                {
                    throw new NotImplementedException();
                }
            }
        }
    }
}
