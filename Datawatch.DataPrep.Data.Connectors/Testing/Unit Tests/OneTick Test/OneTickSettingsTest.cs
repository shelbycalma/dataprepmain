﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.OneTickPlugin
{
    public class OneTickSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test() {
            OneTickSettings a = new OneTickSettings();
            OneTickSettings b = new OneTickSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void equals_Blank2_Test() {
            OneTickSettings a = new OneTickSettings(new PropertyBag());
            OneTickSettings b = new OneTickSettings(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_ContextName_Test() {
            OneTickSettings a = new OneTickSettings();
            a.ContextName = "x";

            OneTickSettings b = new OneTickSettings();
            b.ContextName = "x";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ContextName = "y";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_EncloseParameterInQuote_Test() {
            OneTickSettings a = new OneTickSettings();
            a.EncloseParameterInQuote = true;

            OneTickSettings b = new OneTickSettings();
            b.EncloseParameterInQuote = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.EncloseParameterInQuote = false;
            Assert.NotEqual(a, b);
        }
        
        [Fact]
        public void Equals_ProcedureName_Test() {
            OneTickSettings a = new OneTickSettings();
            a.ProcedureName = "x";

            OneTickSettings b = new OneTickSettings();
            b.ProcedureName = "x";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ProcedureName = "y";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Sql_Test() {
            OneTickSettings a = new OneTickSettings();
            a.Sql = "x";

            OneTickSettings b = new OneTickSettings();
            b.Sql = "x";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Sql = "y";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SymbolList_Test() {
            OneTickSettings a = new OneTickSettings();
            a.SymbolList = "x";

            OneTickSettings b = new OneTickSettings();
            b.SymbolList = "x";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.SymbolList = "y";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_StartTime_Test() {
            OneTickSettings a = new OneTickSettings();
            a.StartTime = "2006-06-01";

            OneTickSettings b = new OneTickSettings();
            b.StartTime = "2006-06-01";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.StartTime = "2006-08-22";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_EndTime_Test() {
            OneTickSettings a = new OneTickSettings();
            a.EndTime = "2009-09-09";

            OneTickSettings b = new OneTickSettings();
            b.EndTime = "2009-09-09";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.EndTime = "3333-03-03";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ShowLocalOTQs_Test()
        {
            OneTickSettings a = new OneTickSettings();
            a.ShowLocalOTQs = true;

            OneTickSettings b = new OneTickSettings();
            b.ShowLocalOTQs = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ShowLocalOTQs = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ShowRemoteOTQs_Test()
        {
            OneTickSettings a = new OneTickSettings();
            a.ShowRemoteOTQs = true;

            OneTickSettings b = new OneTickSettings();
            b.ShowRemoteOTQs = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ShowLocalOTQs = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SeparateDBName_Test()
        {
            OneTickSettings a = new OneTickSettings();
            a.SeparateDBName = true;

            OneTickSettings b = new OneTickSettings();
            b.SeparateDBName = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ShowLocalOTQs = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_ShowSymbolErrorAsWarnings_Test()
        {
            OneTickSettings a = new OneTickSettings();
            a.ShowSymbolErrorAsWarnings = true;

            OneTickSettings b = new OneTickSettings();
            b.ShowSymbolErrorAsWarnings = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ShowSymbolErrorAsWarnings = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_InputParameters_Test() {
            OneTickSettings a = new OneTickSettings();
            OneTickParameter o1 = new OneTickParameter("x1");
            o1.Value = "v1";
            OneTickParameter o2 = new OneTickParameter("x2");
            o2.Value = "v2";
            OneTickParameter[] otpArr1 = {o1, o2};
            a.InputParameters = otpArr1;

            OneTickSettings b = new OneTickSettings();
            OneTickParameter o3 = new OneTickParameter("x1");
            o3.Value = "v1";
            OneTickParameter o4 = new OneTickParameter("x2");
            o4.Value = "v2";
            OneTickParameter[] otpArr2 = {o3, o4};
            b.InputParameters = otpArr2;
        
            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            OneTickSettings c = new OneTickSettings();
            OneTickParameter o5 = new OneTickParameter("x1");
            o5.Value = "v1";
            OneTickParameter o6 = new OneTickParameter("x2");
            o6.Value = "v3";
            OneTickParameter[] otpArr3 = {o5, o6};
            c.InputParameters = otpArr3;
        
            Assert.NotEqual(a, c);
            Assert.NotEqual(a.GetHashCode(), c.GetHashCode());
        
            OneTickSettings d = new OneTickSettings();
            OneTickParameter o7 = new OneTickParameter("x1");
            o5.Value = "v1";
            OneTickParameter o8 = new OneTickParameter("x3");
            o6.Value = "v2";
            OneTickParameter[] otpArr4 = {o7, o8};
            d.InputParameters = otpArr4;
        
            Assert.NotEqual(a, d);
            Assert.NotEqual(a.GetHashCode(), d.GetHashCode());

            Assert.NotEqual(c, d);
            Assert.NotEqual(c.GetHashCode(), d.GetHashCode());
        }

        [Fact]
        public void Equals_SchemaColumnsSettings_Test()
        {
            PropertyBag bag = new PropertyBag();
            bag.Values["SchemaColumnsAsJsonString"] = "[{\"ColumnName\":\"PRICE\"}]";
            OneTickSettings a = new OneTickSettings(bag);

            PropertyBag bag2 = new PropertyBag();
            bag2.Values["SchemaColumnsAsJsonString"] = "[{\"ColumnName\":\"PRICE\"}]";//"[{\"IsValid\":true,\"ColumnName\":\"PRICE\",\"ColumnType\":0,\"Selected\":false,\"UserDefined\":false,\"AllowPredicate\":true,\"AggregateTypes\":[5,16,4,2,0]}]";

            OneTickSettings b = new OneTickSettings(bag2);

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            bag2.Values["SchemaColumnsAsJsonString"] = "[{\"ColumnName\":\"PRICEOTHER\"}]"; // "[{\"IsValid\":true,\"ColumnName\":\"PRICEOTHER\",\"ColumnType\":0,\"Selected\":false,\"UserDefined\":false,\"AllowPredicate\":true,\"AggregateTypes\":[5,16,4,2,0]}]";

            Assert.NotEqual(a, b);
        }
    }
}
