﻿using aleri_pubsubconst;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.AleriPlugin
{
    public class AleriSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            AleriSettings a = new AleriSettings();
            AleriSettings b = new AleriSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            AleriSettings a = new AleriSettings(new PropertyBag());
            AleriSettings b = new AleriSettings(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_AuthenticationType_Test()
        {
            AleriSettings a = new AleriSettings();
            a.AuthenticationType = SpAuthType.AUTH_KERBV5;
            
            AleriSettings b = new AleriSettings();
            b.AuthenticationType = SpAuthType.AUTH_KERBV5;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.AuthenticationType = SpAuthType.AUTH_NONE;
            Assert.NotEqual(a, b);
        }
        
        [Fact]
        public void Equals_ConnectionMode_Test()
        {
            AleriSettings a = new AleriSettings();
            a.ConnectionMode = ConnectionMode.Query;
            
            AleriSettings b = new AleriSettings();
            b.ConnectionMode = ConnectionMode.Query;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.ConnectionMode = ConnectionMode.Stream;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_EncloseParameterInQuote_Test()
        {
            AleriSettings a = new AleriSettings();
            a.EncloseParameterInQuote = true;
            
            AleriSettings b = new AleriSettings();
            b.EncloseParameterInQuote = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.EncloseParameterInQuote = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Host_Test()
        {
            AleriSettings a = new AleriSettings();
            a.Host = "host";
            
            AleriSettings b = new AleriSettings();
            b.Host = "host";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Host = "otherhost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_HotSpareHost_Test()
        {
            AleriSettings a = new AleriSettings();
            a.HotSpareHost = "hotsparehost";
            
            AleriSettings b = new AleriSettings();
            b.HotSpareHost = "hotsparehost";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HotSpareHost = "otherhotsparehost";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_HotSparePort_Test()
        {
            AleriSettings a = new AleriSettings();
            a.HotSparePort = 7;
            
            AleriSettings b = new AleriSettings();
            b.HotSparePort = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.HotSparePort = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IgnoreNulls_Test()
        {
            AleriSettings a = new AleriSettings();
            a.IgnoreNulls = true;
            
            AleriSettings b = new AleriSettings();
            b.IgnoreNulls = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IgnoreNulls = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_IsEncrypted_Test()
        {
            AleriSettings a = new AleriSettings();
            a.IsEncrypted = true;
            
            AleriSettings b = new AleriSettings();
            b.IsEncrypted = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.IsEncrypted = false;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Password_Test()
        {
            AleriSettings a = new AleriSettings();
            a.Password = "passwd";
            
            AleriSettings b = new AleriSettings();
            b.Password = "passwd";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "otherpasswd";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Port_Test()
        {
            AleriSettings a = new AleriSettings();
            a.Port = 7;
            
            AleriSettings b = new AleriSettings();
            b.Port = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Port = 8;
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Query_Test()
        {
            AleriSettings a = new AleriSettings();
            a.Query = "query";
            
            AleriSettings b = new AleriSettings();
            b.Query = "query";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Query = "otherquery";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_RsaKeyFile_Test()
        {
            AleriSettings a = new AleriSettings();
            a.RsaKeyFile = "rsakeyfile";
            
            AleriSettings b = new AleriSettings();
            b.RsaKeyFile = "rsakeyfile";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.RsaKeyFile = "otherrsakeyfile";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Stream_Test()
        {
            AleriSettings a = new AleriSettings();
            a.Stream = "stream";
            
            AleriSettings b = new AleriSettings();
            b.Stream = "stream";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Stream = "otherstream";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_UserName_Test()
        {
            AleriSettings a = new AleriSettings();
            a.UserName = "username";
            
            AleriSettings b = new AleriSettings();
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            AleriSettings a = new AleriSettings();
            a.UserName = "username";
            a.TimeWindowSeconds = 7;
            
            AleriSettings b = new AleriSettings();
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }
    }
}
