﻿using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.StreamBasePlugin
{
    public class StreamBaseSettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            StreamBaseSettings b = new StreamBaseSettings();

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings(new PropertyBag());
            StreamBaseSettings b = new StreamBaseSettings(new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_PrimaryURL_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.PrimaryURL = "primary";
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.PrimaryURL = "primary";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.PrimaryURL = "otherprimary";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Password_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.Password = "passwd";
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.Password = "passwd";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Password = "otherpasswd";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SecondaryURL_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.SecondaryURL = "secondary";
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.SecondaryURL = "secondary";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.SecondaryURL = "othersecondary";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_Predicate_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.Predicate = "predicate";
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.Predicate = "predicate";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.Predicate = "otherpredicate";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_EncloseParameterInQuote_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.EncloseParameterInQuote = true;
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.EncloseParameterInQuote = true;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.EncloseParameterInQuote = false;
            Assert.NotEqual(a, b);
        }

        //[Fact]
        //public void Equals_Stream_Test()
        //{
        //    StreamBaseSettings a = new StreamBaseSettings();
        //    a.UpdateIdCandidatesEnabled = false;
        //    a.Stream = "stream";
            
        //    StreamBaseSettings b = new StreamBaseSettings();
        //    b.UpdateIdCandidatesEnabled = false;
        //    b.Stream = "stream";

        //    Assert.Equal(a, b);
        //    Assert.Equal(a.GetHashCode(), b.GetHashCode());

        //    b.Stream = "otherstream";
        //    Assert.NotEqual(a, b);
        //}

        [Fact]
        public void Equals_UserName_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.UserName = "username";
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.UserName = "username";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.UserName = "otherusername";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_TimeZone_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.TimeZone = "timezone";
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.TimeZone = "timezone";

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeZone = "othertimezone";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_BaseClass_Test()
        {
            StreamBaseSettings a = new StreamBaseSettings();
            a.UserName = "username";
            a.TimeWindowSeconds = 7;
            
            StreamBaseSettings b = new StreamBaseSettings();
            b.UserName = "username";
            b.TimeWindowSeconds = 7;

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.TimeWindowSeconds = 8;
            Assert.NotEqual(a, b);
        }
    }
}
