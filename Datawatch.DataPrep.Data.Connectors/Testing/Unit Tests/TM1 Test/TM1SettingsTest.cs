﻿using System;
using Datawatch.DataPrep.Data.Framework.Model;
using Xunit;

namespace Panopticon.TM1Plugin
{
    public class TM1SettingsTest
    {
        [Fact]
        public void ConstuctorTest()
        {
            PropertyBag bag = new PropertyBag();
            TM1Settings settings = new TM1Settings(bag);
        }

        [Fact]
        public void ConstuctorTestNullBag()
        {
            PropertyBag bag = null;
            Assert.Throws<ArgumentNullException>(
                () => new TM1Settings(bag));
        }

        [Fact]
        public void ParameterizeTest()
        {
            PropertyBag bag = new PropertyBag();
            TM1Settings settings = new TM1Settings(bag);
            Assert.Equal(false, settings.Parameterize);
            settings.Parameterize = true;
            Assert.Equal(true, settings.Parameterize);
        }

        [Fact]
        public void UserTest()
        {
            PropertyBag bag = new PropertyBag();
            TM1Settings settings = new TM1Settings(bag);
            Assert.Null(settings.User);
            settings.User = "someuser";
            Assert.Equal("someuser", settings.User);
        }

        [Fact(Skip = "Ignored")]
        public void DbTitleTest1()
        {
            PropertyBag bag = new PropertyBag();
            TM1Settings settings = new TM1Settings(bag);
            settings.User = "someuser";
            Assert.Equal("someuser", settings.User);

            DbTitle title = new DbTitle(bag);
            title.TitleName = "user";
            title.AppliedParameterName = "someparam";
            Assert.Equal("user", title.TitleName);
            Assert.Equal("someparam", title.AppliedParameterName);

            Assert.Equal("someuser", settings.User);
        }

        [Fact(Skip = "Ignored")]
        public void DbTitleTest2()
        {
            PropertyBag bag = new PropertyBag();
            TM1Settings settings = new TM1Settings(bag);
            settings.User = "someuser";
            Assert.Equal("someuser", settings.User);

            DbTitle title = new DbTitle(bag);
            title.TitleName = "User";
            title.AppliedParameterName = "someparam";
            Assert.Equal("User", title.TitleName);
            Assert.Equal("someparam", title.AppliedParameterName);

            Assert.Equal("someuser", settings.User);
        }
    }
}
