﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Panopticon.Connectors.PolicyEnforcer
{
    public class PluginChecks
    {
        private Enforcer enforcer;

        public PluginChecks()
        {
            this.enforcer = new Enforcer(@"..\..\..\..\..\..\Binaries");
            this.enforcer.Analyze();
        }

        [Fact]
        public void NoPluginsWithNoId()
        {
            var plugins = enforcer.GetPluginsWithNoId();
            Assert.Null(plugins);
        }

        [Fact]
        public void NoPluginsWithNoTitle()
        {
            var plugins = enforcer.GetPluginsWithNoTitle();
            Assert.Null(plugins);
        }

        [Fact]
        public void NoPluginsWithNonUniqueId()
        {
            var plugins = enforcer.GetPluginsWithNonUniqueId();
            Assert.Null(plugins);
        }

        [Fact]
        public void NoPluginsWithNonUniqueTitle()
        {
            var plugins = enforcer.GetPluginsWithNonUniqueTitle();
            Assert.Null(plugins);
        }

        // Add checks:
        // - Plugin has PluginDescriptionAttribute with id/title equal to those from properties
        // - Plugin class is public
    }
}
