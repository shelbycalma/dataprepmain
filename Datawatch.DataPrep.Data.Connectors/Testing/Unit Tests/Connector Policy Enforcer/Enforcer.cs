﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datawatch.DataPrep.Data.Core.Plugin;

namespace Panopticon.Connectors.PolicyEnforcer
{
    public class Enforcer : PluginManager
    {
        private PluginReportGenerator reportGenerator;

        public Enforcer(string path)
            : base(path)
        {

        }

        public void Analyze()
        {
            LoadPlugins();
            this.reportGenerator = new PluginReportGenerator(this);
            this.reportGenerator.GenerateReport();
        }

        public string GetPluginsWithNoId()
        {
            var plugins = this.reportGenerator.PluginsWithNoId;
            if (plugins == null || plugins.Any() == false)
                return null;

            return CreateCollectionInfo("PLUGINS WITHOUT ID", plugins);
        }

        public string GetPluginsWithNoTitle()
        {
            var plugins = this.reportGenerator.PluginsWithNoTitle;
            if (plugins == null || plugins.Any() == false)
                return null;

            return CreateCollectionInfo("PLUGINS WITHOUT TITLE", plugins);
        }

        public string GetPluginsWithNonUniqueId()
        {
            var plugins = this.reportGenerator.PluginsWithNonUniqueId;
            if (plugins == null || plugins.Any() == false)
                return null;

            return CreateCollectionInfo("PLUGINS WITH NON-UNIQUE ID", plugins);
        }

        public string GetPluginsWithNonUniqueTitle()
        {
            var plugins = this.reportGenerator.DataPluginsWithNonUniqueTitle;
            if (plugins == null || plugins.Any() == false)
                return null;

            return CreateCollectionInfo("PLUGINS WITH NON-UNIQUE TITLE", plugins);
        }

        protected string CreateCollectionInfo(string header, IDictionary<string, List<string>> dic)
        {
            var sb = new StringBuilder();
            string count = dic.Any()
                ? dic.SelectMany(x => x.Value).Count().ToString()
                : null;
            this.AppendNewBlock(sb, header, count);
            if (dic.Any() == true)
            {
                foreach (var item in dic)
                {
                    sb.AppendLine(item.Key);
                    item.Value.ForEach(s => sb.AppendLine("\t" + s));
                    sb.AppendLine();
                }
            }
            else
            {
                sb.AppendLine("None");
            }

            return sb.ToString();
        }

        protected string CreateCollectionInfo(
            string header, IEnumerable<string> collection, bool multilineEntries = false)
        {
            var sb = new StringBuilder();
            this.AppendNewBlock(sb, header,
                collection != null ? collection.Count().ToString() : null);

            if (collection != null && collection.Any() == true)
            {
                foreach (var item in collection)
                {
                    sb.AppendLine(item);
                    if (multilineEntries == true)
                        sb.AppendLine();
                }
            }
            else
            {
                sb.AppendLine("None");
            }

            return sb.ToString();
        }

        protected void AppendNewBlock(StringBuilder sb, string header, string count = null)
        {
            if (string.IsNullOrWhiteSpace(count) == true)
            {
                sb.AppendLine(header + ":");
            }
            else
            {
                sb.AppendFormat("{0} ({1}):{2}",
                    header,
                    count,
                    Environment.NewLine);
            }

            sb.AppendLine();
        }
    }
}
