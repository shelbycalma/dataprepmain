﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using Datawatch.DataPrep.Data.Core.Plugin;
using Datawatch.DataPrep.Data.Framework.Model;
using Datawatch.DataPrep.Data.Framework.Plugin;
using Xunit;

namespace Panopticon.StreamingOSISoftPlugin
{
    public class StreamingOSISettingsTest
    {
        [Fact]
        public void Equals_Blank_Test()
        {
            StreamingOSISoftSettings a 
                = new StreamingOSISoftSettings(new DummyPluginHost());
            StreamingOSISoftSettings b 
                = new StreamingOSISoftSettings(new DummyPluginHost());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_Blank2_Test()
        {
            StreamingOSISoftSettings a 
                = new StreamingOSISoftSettings(new DummyPluginHost(),
                    new PropertyBag());
            StreamingOSISoftSettings b 
                = new StreamingOSISoftSettings(new DummyPluginHost(),
                    new PropertyBag());

            Assert.Equal(a, b);
        }

        [Fact]
        public void Equals_SystemDatabase_Test()
        {
            StreamingOSISoftSettings a
                = new StreamingOSISoftSettings(new DummyPluginHost())
                {
                    SystemName = "TestPISystem",
                    Database = "TestDatabase"

                };

            StreamingOSISoftSettings b
                = new StreamingOSISoftSettings(new DummyPluginHost())
                {
                    SystemName = "TestPISystem",
                    Database = "TestDatabase"
                };

            Assert.Equal(a, b);
            Assert.Equal(a.GetHashCode(), b.GetHashCode());

            b.SystemName = "AnotherTestPISystem";
            b.Database = "AnotherDatabase";
            Assert.NotEqual(a, b);
        }

        [Fact]
        public void Equals_SelectionMode_Test()
        {
            StreamingOSISoftSettings a
                = new StreamingOSISoftSettings(new DummyPluginHost())
            {
                IsTreeSelectionMode = true,
                ElementPaths = "ELEPATH",
                Attributes = "ATTRS"
            };

            StreamingOSISoftSettings b
                = new StreamingOSISoftSettings(new DummyPluginHost())
            {
                IsManualSelectionMode = true,
                ManualElementPaths = "ELEPATH",
                ManualAttributes = "ATTRS"
            };

            Assert.NotEqual(a, b);

            b.IsTreeSelectionMode = true;
            b.IsManualSelectionMode = false;

            Assert.NotEqual(a, b);

            b.Attributes = b.ManualAttributes;
            b.ElementPaths = b.ManualElementPaths;

            Assert.Equal(a,b);
        }

        class DummyPluginHost : IPluginManager
        {
            private readonly PluginManager pluginManager;

            public DummyPluginHost()
            {
                pluginManager = new PluginManager
                    (Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));
                pluginManager.LoadPlugins();
                pluginManager.InitializeDataPlugins(null, new PropertyBag());
            }

            public void Dispose()
            {
                pluginManager.Dispose();
            }

            public void LoadPlugins()
            {
                pluginManager.LoadPlugins();
            }

            public void InitializeDataPlugins(Window owner, PropertyBag globalSettings)
            {
                pluginManager.InitializeDataPlugins(owner, globalSettings);
            }

            public IPluginUI<IDataPlugin> GetPluginUI(string id)
            {
                return pluginManager.GetPluginUI(id);
            }

            public IDataPlugin GetDataPlugin(string id)
            {
                return pluginManager.GetDataPlugin(id);
            }

            public IParserPlugin GetParserPlugin(string id)
            {
                return pluginManager.GetParserPlugin(id);
            }

            public void InitializePluginUIs(Window owner)
            {
                pluginManager.InitializePluginUIs(owner);
            }

            public string[] EnabledDataPlugins { get; set; }

            public string[] SkipDataPlugins { get; set; }

            public string[] SkipDataTypes
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool RemoveFromConnectToData
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public bool BlockAtDesignTimeOnly
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IPluginUI<IDataPlugin>> PluginUIs
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IDataPlugin> DataPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<IParserPlugin> ParserPlugins
            {
                get { throw new System.NotImplementedException(); }
            }

            public IEnumerable<PluginLoadingFailureInfo> NotLoadedPluginList
            {
                get { throw new System.NotImplementedException(); }
            }

            public PropertyBag PluginSettings
            {
                get { throw new System.NotImplementedException(); }
                set { throw new System.NotImplementedException(); }
            }

            public IPluginErrorReportingService ErrorReporter
            {
                get
                {
                    return this.pluginManager.ErrorReporter;
                }
            }
        }
    }
}
