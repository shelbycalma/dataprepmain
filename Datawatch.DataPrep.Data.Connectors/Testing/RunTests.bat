@echo off
set config=Release
set bit=x86
IF NOT [%1] == [] (
		set config=%1
	)
IF NOT [%2] == [] (
		set bit=%2
	)


set tests="Unit Tests\ActiveMQ Test\bin\%bit%\%config%\ActiveMQ Test.dll"^
 "Unit Tests\Aleri Test\bin\%bit%\%config%\Aleri Test.dll"^
 "Unit Tests\AMPS Test\bin\%bit%\%config%\AMPS Test.dll"^
 "Unit Tests\AnalysisServices Test\bin\%bit%\%config%\AnalysisServices Test.dll"^
 "Unit Tests\Database Test\bin\%bit%\%config%\Database Test.dll"^
 "Unit Tests\JSON Test\bin\%bit%\%config%\JSON Test.dll"^
 "Unit Tests\Kafka Test\bin\%bit%\%config%\Kafka Test.dll"^
 "Unit Tests\KDBPlusTick Test\bin\%bit%\%config%\KDBPlusTick Test.dll"^
 "Unit Tests\OneTick Test\bin\%bit%\%config%\OneTick Realtime Test.dll"^
 "Unit Tests\Qpid Test\bin\%bit%\%config%\Qpid Test.dll"^
 "Unit Tests\RMDS Test\bin\%bit%\%config%\RMDS Test.dll"^
 "Unit Tests\StreamBase Test\bin\%bit%\%config%\StreamBase Test.dll"^
 "Unit Tests\StreamBaseLiveView Test\bin\%bit%\%config%\StreamBaseLiveView Test.dll"^
 "Unit Tests\StreamingOSI Test\bin\%bit%\%config%\StreamingOSI Test.dll"^
 "Unit Tests\SybaseESP Test\bin\%bit%\%config%\SybaseESP Test.dll"^
 "Unit Tests\TM1 Test\bin\%bit%\%config%\TM1 Test.dll"^
 "Unit Tests\Connector Policy Enforcer\bin\%bit%\%config%\Panopticon.Connectors.PolicyEnforcer.dll"
 

echo tests

if "%bit%" == "x64" (
	GOTO find_xunit_x64
	) else (
	GOTO find_xunit_x86
	)


:dotest
if defined xcp (
echo "xunit => %xcp%"
) else (
echo xunit not found
)

mkdir "reports_dotnet_%bit%"
%xcp% %tests% -noshadow -html reports_dotnet_%bit%\DotNetTestReport.html -xml reports_dotnet_%bit%\TestResults_xunit.xml -nunit reports_dotnet_%bit%\TestResults.xml
GOTO end


:find_xunit_x86
 for /r "../../packages" %%a in ('DIR *.* /B /O:-D') do (
	if "%%~nxa"=="xunit.console.x86.exe" set xcp=%%~dpnxa
	)
 GOTO :dotest

:find_xunit_x64
 for /r "../../packages" %%a in ('DIR *.* /B /O:-D') do (
	if "%%~nxa"=="xunit.console.exe" set xcp=%%~dpnxa
	)
 GOTO :dotest

:end