﻿1. WHAT IS IT FOR?

MixedModeConfigPatcher is a console utility for updating configuration file in
order to enable or disable mixed mode assembly usage. 

For example, current tests are compiled for .Net 4.5, but some connectors are 
referencing assemblies, compiled for .Net 2.0. To use these assemblies one has 
to configure the executing module to allow mixed mode assemblies. This is done 
by updating *.config file to have the following element:

  <startup useLegacyV2RuntimeActivationPolicy="true">
    <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/>
  </startup>

Actually, it is the useLegacyV2RuntimeActivationPolicy attribute with the 
value of true which is important. So if startup element is absent utility adds 
the following:
 
 <startup useLegacyV2RuntimeActivationPolicy="true" />

Utility updates the specified config file by either adding startup element with
the attribute mentioned above or updates the existing attribute value.

While it is easy to update the app.config manually, there some cases where it 
is impossible. For example, on build server during the unit test step execution.
In this case it is test runner's config that must be updated, and it is 
restored from NuGet server on each build start.

2. HOW TO USE

MixedModeConfigPatcher.exe <ConfigFilePath> <true|false>

3. WHICH PROJECTS USE IT

Currently utility is used by Aleri Test project.
