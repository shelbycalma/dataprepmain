﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MixedModeConfigPatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2 || args.Length > 3)
            {
                Console.WriteLine("Invalid number of arguments.");
                Console.WriteLine("USAGE: MixedModePatcher <PATH> <true|false> [oldVersion]");
                return;
            }

            try
            {
                var enable = string.CompareOrdinal(args[1].ToUpper(), "TRUE") == 0;
                var patcher = new Patcher();
                patcher.ProcessConfig(args[0], enable, args.Length > 3 ? args[2] : null);
            }
            catch(Exception e)
            {
                Console.WriteLine("Failed to patch configuration file: " + e.Message);
            }
        }
    }
}
