﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace MixedModeConfigPatcher
{
    class Patcher
    {
        /*
          <startup useLegacyV2RuntimeActivationPolicy="true">
            <supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5"/>
            <supportedRuntime version="v2.0.50727"/>
          </startup>
        */

        /// <summary>
        /// Patches the specified configuration file to either enable or
        /// disable mixed mode assembly usage.
        /// </summary>
        /// <param name="path">The path to configuration file.</param>
        /// <param name="enableMixedMode">if set to <c>true</c>, mixed mode 
        /// will be enabled.</param>
        /// <param name="oldVersion">The old version. NOT USED FOR NOW</param>
        /// <exception cref="System.ArgumentException">Specify a valid path to 
        /// config file.</exception>
        public void ProcessConfig(string path, bool enableMixedMode, string oldVersion)
        {
            if (string.IsNullOrWhiteSpace(path) == true)
                throw new ArgumentException("Specify a valid path to config file.");

            if (string.IsNullOrWhiteSpace(oldVersion) == true)
                oldVersion = "v2.0.50727";

            var doc = XDocument.Load(path);
            var startup = doc.Element("configuration")
                .Elements().FirstOrDefault(c => c.Name == "startup");

            if (startup == null)
            {
                if (enableMixedMode == false)
                    return;

                AddStartup(doc, oldVersion);
            }
            else
            {
                if (UpdateStartup(startup, enableMixedMode) == false)
                {
                    Console.WriteLine("Patching is not required.");
                    return;
                }
            }

            doc.Save(path);
            Console.WriteLine("File was successfully patched.");
        }

        private void AddStartup(XDocument doc, string oldVersion)
        {
            var startup = new XElement("startup",
                new XAttribute("useLegacyV2RuntimeActivationPolicy", "true"));
            //startup.Add(new XElement("supportedRuntime", new XAttribute("version", oldVersion)));
            doc.Element("configuration").Add(startup);
        }

        private bool UpdateStartup(XElement startup, bool enableMixedMode)
        {
            var attr = startup.Attribute("useLegacyV2RuntimeActivationPolicy");
            string modeState = enableMixedMode ? "true" : "false";
            if (attr != null)
            {
                if (string.CompareOrdinal(attr.Value, modeState) == 0)
                    return false;

                attr.Value = modeState;
            }
            else
            {
                startup.Add(new XAttribute("useLegacyV2RuntimeActivationPolicy", modeState));
            }

            return true;
        }
    }
}
